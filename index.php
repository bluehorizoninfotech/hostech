<?php
$environment_file_path = dirname(__FILE__) . '/protected/config/env.php';
require_once ($environment_file_path);

// change the following paths if necessary
$yii = dirname(__FILE__) . '/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

@date_default_timezone_set('Asia/Kolkata');

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', ENV_DEBUG);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
