<?php // echo "--------------".Yii::getVersion(); ?>

<!-- BEGIN LOGIN FORM -->
<!--	<form class="login-form" action="index.html" method="post">-->
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'login-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(            ),
        )); ?>        
        
		<h3 class="form-title">Sign In</h3>
        <?php
        //if($model->hasErrors()){ ?>
<!--		<div class="alert alert-danger display-hide" style="display:block">
			<button class="close" data-close="alert"></button>
			<?php echo $form->errorSummary($model); ?>
		</div>-->
        <?php //} ?>
<!-- <div id="statusMsg">
                        <?php
//                        foreach (Yii::app()->user->getFlashes() as $key => $message) {
//                            //$key would be info, success, warning,error 
//                            echo '<div class="message ' . $key . ' alert alert-success alert-dismissable">' . $message . "</div>\n";
//                        }
                        ?>

                    </div>-->
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
<!--			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>-->
                        <?php echo $form->textField($model,'username',array('class'=>'form-control form-control-solid placeholder-no-fix','autocomplete'=>"off", 'placeholder'=>"Username")); ?>
                        <?php echo $form->error($model,'username'); ?>
                </div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
                        <?php echo $form->passwordField($model,'password',array('class'=>'form-control form-control-solid placeholder-no-fix','autocomplete'=>"off", 'placeholder'=>"Password")); ?>
                        <?php echo $form->error($model,'password'); ?>
                </div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Login</button>
			<label class="rememberme check">
                <?php echo $form->checkBox($model,'rememberMe'); ?>
			Remember </label>
<!--			<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>-->
		</div>
	</form>
	 
<?php $this->endWidget(); ?>
	<!-- END FORGOT PASSWORD FORM -->
