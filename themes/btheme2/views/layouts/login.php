<?php
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
   'jquery.js'=>false,
   'jquery.min.js'=>false, 
);
$cs->coreScriptPosition = CClientScript::POS_END;
//Yii::app()->clientScript->coreScriptPosition=CClientScript::POS_END;

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo CHtml::encode(Yii::app()->name); ?> |  Project Management System</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout2.css" rel="stylesheet" type="text/css"/>
          <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/screen.css" rel="stylesheet" type="text/css">

        <!-- END THEME STYLES -->
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo $this->favicon ?>">
        <!-- <link rel="apple-touch-icon" sizes="57x57" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff"> -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo" style="color:white">
            <div id="logo">
                <div class="logoimg">
                    <?php echo CHtml::image($this->logo, Yii::app()->name, array('style' => 'border:0px;max-height:52px;height:52px;'));?>
                </div>
            </div>
        </div>
        <h1>Project Management System (PMS)</h1>

        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <?php echo $content; ?>
        </div>
        <div class="copyright">
             
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/respond.min.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Login.init();
                Demo.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
