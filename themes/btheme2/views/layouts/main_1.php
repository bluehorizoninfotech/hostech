<?php
$tblpx = Yii::app()->db->tablePrefix;
$cs = Yii::app()->clientScript;
$cs->scriptMap = array(
    'jquery.js' => false,
    'jquery.min.js' => false,
);
$cs->coreScriptPosition = CClientScript::POS_END;
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo CHtml::encode(Yii::app()->name); ?> |  Project Management System</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/plugins.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout2.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/style.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/form.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/screen.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/tmsstyle.css" rel="stylesheet" type="text/css">
        <!-- END THEME STYLES -->
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/images/favlogo.png" rel="shortcut icon" type="image/x-icon" />
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body>
        <!-- BEGIN HEADER -->
        <div class="page-wrapper">
            <div class="page-header" >
                <!-- BEGIN HEADER TOP -->
                <div class="page-header-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                            </button>
                        <!-- BEGIN LOGO -->
                            <div class="page-logo">
                                <a href="<?php echo Yii::app()->getBaseUrl(true) . '/'; ?>"><img src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/ps-logo.png" border="0" height="48" alt="logo" class="logo-default1"></a>
                            </div>
                        </div>
                        <!-- END LOGO -->
                        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <div class=" collapse navbar-collapse" id="myNavbar">
                        <div class="nav navbar-nav navbar-right">
                            <ul class="nav navbar-nav">

                                <?php  //$this->widget('NotificationWidget'); ?>

                                <li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"   data-close-others="true">
                                    <i class="icon-user" style="font-size:20px;vertical-align:middle;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <div class="user-name"><?php echo ucfirst(Yii::app()->user->name); ?></div>
                                        </li>
                                        <li>
                                            <?php echo CHtml::link('&nbsp;&nbsp;&nbsp;My Profile', array('/users/myprofile'), array('class' => "icon-user ico")); ?>
                                        </li>
                                        <li>
                                            <?php echo CHtml::link('&nbsp;&nbsp;&nbsp;Logout', array('/site/logout'), array('class' => "icon-key ico")); ?>
                                        </li>
                                    </ul>
                                </li>

                                <!--  end userlist  -->
                             </ul>
                        </div>
                        <!-- END RESPONSIVE MENU TOGGLER -->
                        <div class="nav navbar-nav navbar-right">
                        <?php
                      // print_r(Yii::app()->session['menuauthlist']);die("ggg");
                        $this->widget('zii.widgets.CMenu', array(
                            'activeCssClass' => 'active',
                            'activateParents' => true,
                            'items' => array(
                                array('label' => 'Home', 'url' => array('/site/index2')),
                                array('label' => 'Users', 'url' =>array('/Users/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/users/index', Yii::app()->session['menuauthlist'])))),
                                array('label' => 'Clients', 'url' =>array('/Clients/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/clients/index', Yii::app()->session['menuauthlist'])))),
                                //array('label' => 'Projects', 'url' =>array('/Projects/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/projects/index', Yii::app()->session['menuauthlist'])))),
                                array('label' => 'Projects', 'url' => '#','linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'items' => array(
                                    array('label' => 'Projects', 'url' =>array('/Projects/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/projects/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Gantt Chart', 'url' =>array('/Projects/chart'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/projects/chart', Yii::app()->session['menuauthlist'])))),
                                    
                                ), 'visible' => !Yii::app()->user->isGuest && yii::app()->user->role <= 1),
                               
                                array('label' => 'Tasks', 'url' =>array('/Tasks/index2'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 4 && (in_array('/tasks/index2', Yii::app()->session['menuauthlist'])))),
                                array('label' => 'My Tasks', 'url' =>array('/Tasks/mytask'), 'visible' => (in_array('/tasks/mytask', Yii::app()->session['menuauthlist']))),
                                array('label' => 'Status Entry', 'url' =>array('/TimeEntry/index'), 'visible' => (in_array('/timeEntry/index', Yii::app()->session['menuauthlist']))),
                                
                                array('label' => 'Reports', 'url' => '#','linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'items' => array(
                                    array('label' => 'Status Report', 'url' =>array('/TimeEntry/timereport'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 4 && (in_array('/timeEntry/timereport', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Service Report', 'url' =>array('/Tasks/servicereport'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 3 && (in_array('/tasks/serviceReport', Yii::app()->session['menuauthlist'])))),
                                ), 'visible' => !Yii::app()->user->isGuest && yii::app()->user->role <= 4),
                                
                                array('label' => 'AMS', 'url' => '#','linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown' ), 'visible' => !Yii::app()->user->isGuest && (intval(yii::app()->user->role) === 1 or yii::app()->user->role == 6 or Yii::app()->user->role == 9 or yii::app()->user->role == 10 ), 'items' => array(
                                    array('label' => 'Employee Allocation', 'url' =>array('/usersite/admin'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/usersite/admin', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Attendence Report', 'url' =>array('/accesslog/attreport'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/accesslog/attreport', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Punch Report', 'url' =>array('/punching/accesslog/mylog'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/punching/accesslog/mylog', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Manual Punch Log', 'url' =>array('/punching/custompunch/admin'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/punching/custompunch/admin', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Manual Punch Report', 'url' =>array('/manualentryDate/index'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/manualentryDate/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Employee Allocation Request', 'url' =>array('/employeerequest/admin'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/employeerequest/admin', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Employee Monthly Report', 'url' =>array('/accesslog/getmonthreport'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/accesslog/getmonthreport', Yii::app()->session['menuauthlist'])))),
                                )),
                                
                                array('label' => 'Settings', 'url' => 'javascript:;','linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown', 'data-close-others' => true,  'aria-expanded' => false ), 'items' => array(
                                    array('label' => 'Groups', 'url' =>array('/groups/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/groups/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'User Roles', 'url' =>array('/userRoles/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/userRoles/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Department', 'url' =>array('/department/admin'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/department/admin', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Work Sites', 'url' =>array('/clientsite/admin'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/clientsite/admin', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Punching Devices', 'url' =>array('/punching/devices/index'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 6) && (in_array('/punching/devices/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Role Settings', 'url' =>array('/roleSettings/admin'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1 && (in_array('/roleSettings/admin', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Task Expiry Settings', 'url' =>array('/taskExpiry/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role <= 1  && (in_array('/taskExpiry/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Menu Permissions', 'url' =>array('/menu/menuPermissions/create'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role == 1  && (in_array('/menu/menuPermissions/create', Yii::app()->session['menuauthlist'])))),
                                      
                                ), 'visible' => !Yii::app()->user->isGuest && yii::app()->user->role <= 1),


                                array('label' => 'WPR', 'url' => 'javascript:;','linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown', 'data-close-others' => true,  'aria-expanded' => false ), 'items' => array(
                                    array('label' => 'Assigned Projects', 'url' =>array('/projects/addAssigneetoProjects'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or  yii::app()->user->role == 9) && (in_array('/projects/addAssigneetoProjects', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Daily Work progress', 'url' =>array('/dailyWork/index'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role <= 1 or yii::app()->user->role == 10)&& (in_array('/dailyWork/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'BOQ Progresses', 'url' =>array('/DailyWorkProgress/index'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role == 1 or yii::app()->user->role == 10 or yii::app()->user->role == 9) && (in_array('/dailyWorkProgress/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'All Work Progress', 'url' =>array('/DailyWorkProgress/workReport'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role == 1 or yii::app()->user->role == 10 or yii::app()->user->role == 9)&& (in_array('/dailyWorkProgress/workReport', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Weekly Reports', 'url' =>array('/DailyWorkProgress/reports'), 'visible' => (isset(Yii::app()->user->isGuest) && (yii::app()->user->role == 1 or yii::app()->user->role == 9) && (in_array('/dailyWorkProgress/reports', Yii::app()->session['menuauthlist'])))),
                                  ), 'visible' => !Yii::app()->user->isGuest && yii::app()->user->role <= 1),
                                
                            ),
                            'submenuHtmlOptions' => array('class' => 'dropdown-menu dropdown-menu-default'),
                            'htmlOptions' => array('class' => 'nav navbar-nav'),
                        ));
                        ?>
                        </div>

                        </div>


                    </div>
                </div>
                <!-- END HEADER TOP -->
                <!-- BEGIN HEADER MENU -->
                <div class=""> <!-- class="page-header-menu" -->
                    <div class="container">
                        <!-- BEGIN HEADER SEARCH BOX -->
                        <form class="search-form" action="extra_search.html" method="GET" style="display:none" >
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" name="query">
                                <span class="input-group-btn">
                                    <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                                </span>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- END HEADER MENU -->
            </div>

            <!-- END HEADER -->
            <!-- BEGIN PAGE CONTAINER -->
            <div class="page-container">
                <!-- BEGIN PAGE HEAD -->
                <div class="page-head">
                    <div class="container">
                        <?php if (Yii::app()->user->hasFlash('success')): ?>
                            <div class="alert alert-success alert-dismissable" id="success-alert">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                                <?php echo Yii::app()->user->getFlash('success'); ?>
                            </div>
                            <?php endif; ?>
                            <?php if (Yii::app()->user->hasFlash('error')): ?>
                             <div class="alert alert-danger alert-dismissable" id="success-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Yii::app()->user->getFlash('error'); ?>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <!-- END PAGE HEAD -->
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMB -->

                        <!-- END PAGE BREADCRUMB -->
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="row">
                            <div class="col-md-12">

                                <div class="portlet light">
                                    <div class="portlet-body">
                                        <div class="table-responsive">

                                            <?php
                                            echo $content;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <div class="page-footer">
                    <div class="container">
                        <?php
                        $key = 'url encryption';

                        if(function_exists('mcrypt_create_iv')){
                            $iv = mcrypt_create_iv(
                                    mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM
                            );
                        }else{
                            $iv = openssl_random_pseudo_bytes(
                                openssl_cipher_iv_length('aes-256-cbc')
                            );
                        }

                        if(function_exists('mcrypt_encrypt')){

                            $encrypted = base64_encode(
                                    $iv .
                                    mcrypt_encrypt(
                                            MCRYPT_RIJNDAEL_128, hash('sha256', $key, true), Yii::app()->user->id, MCRYPT_MODE_CBC, $iv
                                    )
                            );
                            $encryptedmain = base64_encode(
                                    $iv .
                                    mcrypt_encrypt(
                                            MCRYPT_RIJNDAEL_128, hash('sha256', $key, true), Yii::app()->user->mainuser_id, MCRYPT_MODE_CBC, $iv
                                    )
                            );
                        }else{
                            $encrypted = base64_encode(openssl_encrypt(Yii::app()->user->id, 'aes-256-cbc', $key, 0, $iv). '::' . $iv);
                            $encryptedmain = base64_encode(openssl_encrypt(Yii::app()->user->mainuser_id, 'aes-256-cbc', $key, 0, $iv). '::' . $iv);
                        }
                      
                        ?>
                        <?php  $this->widget('MasqueradeWidget'); ?>
                            <?php
                            /*
                        //Change user login from Direct admin user
                        if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->mainuser_role) && (Yii::app()->user->mainuser_role == 1)) {
                            ?>
                                <!--                <div>-->
                                <div class="pull-right"><span>Change user session: </span>
                                        <?php
                                        $tblpx = yii::app()->db->tablePrefix;

                                        $sql = "SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                                . "`{$tblpx}user_roles`.`role` "
                                                . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                                . "WHERE status=0 ORDER BY user_type,full_name ASC";
                                        $result = Yii::app()->db->createCommand($sql)->queryAll();
                                        $listdata = CHtml::listData($result, 'userid', 'full_name', 'role');
                                        echo CHtml::dropDownList('shift_userid', '', $listdata, array('options' => array(Yii::app()->user->id => array('selected' => true)),
                                            'ajax' => array(
                                                'type' => 'POST', //request type
                                                'url' => CController::createUrl('/users/usershift'), //url to call.
                                                'success' => 'js:function(data){
                                        location.reload();
                                    }',
                                                'data' => array('shift_userid' => 'js:this.value'),
                                        )));
                                        ?>
                                </div>
                                    <?php //if (Yii::app()->user->mainuser_id != Yii::app()->user->id) { ?>
                                <!--span style="color:red;font-style: italic;text-decoration: blink;margin-left:20px;">(Note: You are working on other user's session)</span-->
                                    <?php //} ?>
                                    <?php
                                }
                                */ 
                                ?>
                        <div class="pull-right"><?php echo date('Y'); ?> &copy; <?php echo Yii::app()->name;?>. All Rights Reserved.
                        </div>
                    </div>
                    </div>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
        </div>
        <!-- END PRE-FOOTER -->
        <!-- BEGIN FOOTER -->

        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/respond.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
        
        <!-- <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script> -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
        <script>
                         jQuery(document).ready(function() {
                           
                             // initiate layout and plugins
                             Metronic.init(); // init metronic core components
                             Layout.init(); // init current layout
                             Demo.init(); // init demo features

                             jQuery('.savepdf').on('click', function(e) {
                                 e.preventDefault();

                                 var linkurl = $(this).attr('href');

                                 var filter_items = '';
                                 $('.filters input[type=text], .filters select').each(function() {

                                     colname = $('thead  tr:eq(0) th:eq(' + $(this).closest("td").index() + ')').text();

                                     if ($(this).prop("tagName").toLowerCase() === 'select') {
                                         //alert($(this).prop("tagName").toLowerCase());
                                         if ($(this).find('option:selected').text() != '') {
                                             filter_items += "<b>" + colname + "</b> : " + $(this).find('option:selected').text() + "<br />";
                                         }



                                     } else {
                                         if ($(this).val() != '') {
                                             filter_items += "<b>" + colname + "</b> : " + $(this).val() + "<br />";
                                         }
                                     }
                                 });
                                 window.location.href = linkurl + "&filter_cr=" + filter_items;

                             });
                             $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                                $("#success-alert").slideUp(500);
                            });
                                $("#danger-alert").fadeTo(2000, 500).slideUp(500, function(){
                                        $("#danger-alert").slideUp(500);
                            });

                         });
        </script>
        
    </body>
    <!-- END BODY -->
</html>
