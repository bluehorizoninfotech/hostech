<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    body {
        margin: 0;
        font-family: Arial, Helvetica, sans-serif;
    }

    .top-container {
        background-color: #f1f1f1;
        padding: 30px;
        text-align: center;
    }

    .header {
        padding: 10px 16px;
        background: #555;
        color: #f1f1f1;

    }

    .content {
        padding: 16px;
    }

    .sticky {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 10;
    }

    .sticky+.content {
        padding-top: 102px;
    }


    /* input[type="text"]{
       border-radius:5px;
      -moz-border-radius:5px;
      -webkit-border-radius:5px; 
     
    } */
</style>
<?php
$tblpx = Yii::app()->db->tablePrefix;
$cs = Yii::app()->clientScript;
$cs->scriptMap = array(
    'jquery.js' => false,
    'jquery.min.js' => false,
);
$cs->coreScriptPosition = CClientScript::POS_END;
$project_array_list = array();
$boq_pending_count = 0;
$time_entry_request_count = 0;
$tasks_id_array = array();
?>
<?php
if (Yii::app()->user->role != 1) {
    $criteria = new CDbCriteria;
    $criteria->select = 'project_id,tskid';
    $criteria->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
    $criteria->group = 'project_id';
    $project_ids = Tasks::model()->findAll($criteria);
    $project_id_array = array();

    foreach ($project_ids as $projid) {
        // echo $projid['project_id'];exit;
        array_push($project_id_array, $projid['project_id']);
        array_push($tasks_id_array, $projid['tskid']);
    }
    // echo '<pre>';print_r($tasks_id_array);exit;       
    if (!empty($project_id_array)) {
        $project_id_array = implode(',', $project_id_array);
        $projects = Projects::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ') AND status =1 ORDER BY name ASC'));
    } else {
        $projects = Projects::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to) AND status =1 ORDER BY name ASC'));
    }
} else {
    $projects = Projects::model()->findAll(array('condition' => 'status =1 ORDER BY name ASC'));
    $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 2'));


    foreach ($tasks as $task) {
        array_push($tasks_id_array, $task['tskid']);
    }
}
foreach ($projects as $project) {
    array_push($project_array_list, $project['pid']);
}

if (in_array(Yii::app()->user->project_id, $project_array_list)) {
    $project_array_list = array(Yii::app()->user->project_id);
}

$project_array_list = implode(',', $project_array_list);

if (!empty($tasks_id_array)) {
    $tasks_id_array = implode(',', $tasks_id_array);
} else {
    $tasks_id_array = NULL;
}



if (!empty($project_array_list))
    $boq_approve_list = DailyWorkProgress::model()->findAll(array('condition' => 'project_id in (' . $project_array_list . ') AND approve_status = 0'));
if (!empty($tasks_id_array))
    $time_entry_list = TimeEntry::model()->findAll(array('condition' => 'tskid in (' . $tasks_id_array . ') AND approve_status = 0'));
if (!empty($time_entry_list)) {
    $time_entry_request_count = count($time_entry_list);
}
if (!empty($boq_approve_list)) {
    $boq_pending_count = count($boq_approve_list);
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title><?php echo CHtml::encode(Yii::app()->name); ?> | <?php echo (defined('TITLE') ? TITLE : "Project Management System") ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/plugins.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout2.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/style.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/pms-style.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/form.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/screen.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/tmsstyle.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
    <!-- END THEME STYLES -->
    <!-- <link rel="apple-touch-icon" sizes="57x57" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/apple-icon-180x180.png"> -->
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo $this->favicon ?>">
    <!-- <link rel="icon" type="image/png" sizes="32x32" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/manifest.json">
    <link href="<?= Yii::app()->theme->baseUrl ?>/assets/custom/dashboard.css" id="style_components" rel="stylesheet" type="text/css">
    <meta name="msapplication-TileColor" content="#ffffff"> -->
    <meta name="msapplication-TileImage" content="<?php echo Yii::app()->getBaseUrl(true); ?>/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">








</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body>
    <!-- BEGIN HEADER -->


    <div class="page-wrapper">

        <div class="page-header" id="myHeader">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- BEGIN LOGO -->
                        <div class="page-logo">
                            <a href="<?php echo Yii::app()->getBaseUrl(true) . '/'; ?>">
                                <?php echo CHtml::image($this->logo, Yii::app()->name, array('style' => 'border:0px;max-height:50px;height:48px;', 'class' => 'logo-default1')); ?>
                            </a>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <div class=" collapse navbar-collapse" id="myNavbar">
                        <div class="nav navbar-nav navbar-right">
                            <ul class="nav navbar-nav">

                                <?php $this->widget('NotificationWidget'); ?>

                                <li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
                                        <i class="icon-user" style="font-size:20px;vertical-align:middle;"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <div class="user-name"><?php echo ucfirst(Yii::app()->user->name); ?></div>
                                        </li>
                                        <li>
                                            <?php echo CHtml::link('&nbsp;&nbsp;&nbsp;My Profile', array('/users/myprofile'), array('class' => "icon-user ico")); ?>
                                        </li>
                                        <li>
                                            <?php echo CHtml::link('&nbsp;&nbsp;&nbsp;Logout', array('/site/logout'), array('class' => "icon-key ico")); ?>
                                        </li>
                                    </ul>
                                </li>

                                <!--  end userlist  -->
                            </ul>
                        </div>
                        <!-- END RESPONSIVE MENU TOGGLER -->
                        <div class="nav navbar-nav navbar-right">
                            <?php
                            // echo "<pre>",  print_r(Yii::app()->session['menuauthlist']);exit;
                            //    print_r(Yii::app()->session['menuauthlist']);die("ggg");
                            $general_settings_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
                            if ($general_settings_model->dashboard_type == 1) {
                                $dashboard_url = '/site/dashboard';
                            } else {
                                $dashboard_url = '/site/index2';
                            }
                            $id = Yii::app()->user->id;
                            $allowed_apps = Controller::getAllowedApps($id);


                            $allowed_apps_menu = array();
                            if (is_array($allowed_apps)) {
                                foreach ($allowed_apps as $app) {

                                    if ($app == 'pms') {
                                        continue;
                                    }

                                    $appName = strtoupper($app);

                                    //$appsImgIcon = CHtml::image('images/' . $app . '.png', $appName);
                                    $appsImgIcon =  $appName;
                                    $allowed_apps_menu[$app] = array(
                                        'label' => $appsImgIcon,
                                        'url' => array('/site/redirect', 'type' => $appName),
                                        'linkOptions' => array('target' => '_blank'),
                                    );
                                }
                            }

                            $this->widget('zii.widgets.CMenu', array(
                                'activeCssClass' => 'active',
                                'encodeLabel' => false,
                                'activateParents' => true,
                                'items' => array(
                                    //array('label' => 'Home', 'url' => array('/site/index2')),
                                    array('label' => 'Home', 'url' => array($dashboard_url)),
                                    array('label' => 'Users', 'url' => array('/Users/index'), 'visible' => ((in_array('/users/index', Yii::app()->session['menuauthlist'])))),
                                    array('label' => 'Clients', 'url' => array('/Clients/index'), 'visible' => ((in_array('/clients/index', Yii::app()->session['menuauthlist'])))),

                                    //array('label' => 'Projects', 'url' =>array('/Projects/index'), 'visible' => (isset(Yii::app()->user->isGuest) && yii::app()->user->role >= 1 && (in_array('/projects/index', Yii::app()->session['menuauthlist'])))),

                                    array('label' => 'Projects', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'items' => array(
                                        array('label' => 'Projects', 'url' => array('/Projects/index'), 'visible' => ((in_array('/projects/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Gantt Chart', 'url' => array('/Projects/chart'), 'visible' => ((in_array('/projects/chart', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Master Schedule', 'url' => array('/Projects/schedules'), 'visible' => ((in_array('/projects/chart', Yii::app()->session['menuauthlist'])))),


                                        array('label' => 'Project Mapping', 'url' => array('/Projects/project_mapping'), 'visible' => ((in_array('/projects/project_mapping', Yii::app()->session['menuauthlist'])) && Tasks::model()->accountPermission() == 1)),

                                        array('label' => 'Item Estimation', 'url' => array('/Tasks/import_task_estimate'), 'visible' => ((in_array('/tasks/itemEstimation', Yii::app()->session['menuauthlist'])) && Tasks::model()->accountPermission() == 1)),

                                        array('label' => 'Warehouse Site Linking', 'url' => array('/clientsite/warehouse_site_mapping'), 'visible' => ((in_array('/clientsite/warehouse_site_mapping', Yii::app()->session['menuauthlist'])) && Tasks::model()->accountPermission() == 1)),

                                        array('label' => 'Weekly Report', 'url' => array('/Reports/weeklyReport'), 'visible' => ((in_array('/reports/weeklyreport', Yii::app()->session['menuauthlist'])))),

                                    ), 'visible' => (in_array('/projects/index', Yii::app()->session['menuauthlist']) || in_array('/projects/chart', Yii::app()->session['menuauthlist']) || in_array('/reports/weeklyreport', Yii::app()->session['menuauthlist']))),



                                    array('label' => 'Monthly Task', 'url' => array('/Tasks/monthlyTask'), 'visible' => ((in_array('/tasks/monthlyTask', Yii::app()->session['menuauthlist'])))),


                                    array('label' => 'Assigned Tasks', 'url' => array('/Tasks/index2'), 'visible' => ((in_array('/tasks/index2', Yii::app()->session['menuauthlist'])))),




                                    array(
                                        'label' => 'My Tasks', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'items' => array(
                                            array('label' => 'My Tasks', 'url' => array('/Tasks/mytask'), 'visible' => (in_array('/tasks/mytask', Yii::app()->session['menuauthlist']))),
                                            array('label' => 'MR', 'url' => array('/MaterialRequisition/index'), 'visible' => ((in_array('/materialRequisition/index', Yii::app()->session['menuauthlist'])) && Tasks::model()->accountPermission() == 1)),
                                        ),
                                        'visible' => ((in_array('/tasks/mytask', Yii::app()->session['menuauthlist']))) ||

                                            ((in_array('/MaterialRequisition/index', Yii::app()->session['menuauthlist'])))

                                    ),
                                    //array('label' => 'My Tasks', 'url' => array('/Tasks/mytask'), 'visible' => (in_array('/tasks/mytask', Yii::app()->session['menuauthlist']))),
                                    array('label' => ($time_entry_request_count != 0 && in_array('/timeEntry/index', Yii::app()->session['menuauthlist'])) ? 'Time Entry <span class="badge notify-badge" style="background-color:#f44336;margin-top: -18px;">' . $time_entry_request_count . '</span>' : 'Time Entry', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'items' => array(
                                        array('label' => 'Time Entry', 'url' => array('/TimeEntry/index'), 'visible' => ((in_array('/timeEntry/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => ($time_entry_request_count != 0 && in_array('/timeEntry/index', Yii::app()->session['menuauthlist'])) ? 'Pending Requests <span class="badge  notify-badge" style="background-color:#f44336;">' . $time_entry_request_count . '</span>' : 'Pending Requests', 'url' => array('/TimeEntry/pendingrequests'), 'visible' => ((in_array('/timeEntry/pendingrequests', Yii::app()->session['menuauthlist'])))),
                                    ), 'visible' => (in_array('/timeEntry/index', Yii::app()->session['menuauthlist']) || in_array('/projects/chart', Yii::app()->session['menuauthlist']))),


                                    array('label' => 'Reports', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'items' => array(
                                        // array('label' => 'Meeting Minutes', 'url' => array('/Reports/meetingminutes'), 'visible' => ((in_array('/reports/meetingminutes', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'Meeting Minutes', 'url' => array('/Reports/mom'), 'visible' => ((in_array('/reports/meetingminutes', Yii::app()->session['menuauthlist'])))),


                                        // array('label' => 'Weekly Report', 'url' => array('/Reports/weeklyReport'), 'visible' => ((in_array('/reports/weeklyreport', Yii::app()->session['menuauthlist'])))),


                                        array('label' => 'Project Report', 'url' => array('/reports/projectreport'), 'visible' => ((in_array('/reports/meetingminutes', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Status Report', 'url' => array('/TimeEntry/timereport'), 'visible' => ((in_array('/timeEntry/timereport', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Service Report', 'url' => array('/Tasks/servicereport'), 'visible' => ((in_array('/tasks/serviceReport', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Employee Monthly Report', 'url' => array('/accesslog/getmonthreport'), 'visible' => ((in_array('/accesslog/getmonthreport', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'Daily Progress Report', 'url' => array('/reports/dailyProgressReport'), 'visible' => ((in_array('/reports/dailyProgressReport', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'On Hold Report', 'url' => array('/reports/onhold_report'), 'visible' => ((in_array('/reports/onhold_report', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'Generate Weekly Report', 'url' => array('/reports/generateweeklyreport'), 'visible' => ((in_array('/reports/generateweeklyreport', Yii::app()->session['menuauthlist'])))),

                                    ), 'visible' => (in_array('/usersite/admin', Yii::app()->session['menuauthlist']) ||
                                        in_array('/TimeEntry/timereport', Yii::app()->session['menuauthlist']) ||
                                        in_array('/Tasks/servicereport', Yii::app()->session['menuauthlist']) ||

                                        in_array('/reports/onhold_report', Yii::app()->session['menuauthlist']) ||

                                        in_array('/accesslog/getmonthreport', Yii::app()->session['menuauthlist']))),
                                        
                                        array(
                                        'label' => 'AMS', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'),
                                        'visible' => ((in_array('/photoPunch/view', Yii::app()->session['menuauthlist']) ||
                                            in_array('/photoPunch/view', Yii::app()->session['menuauthlist']) ||
                                            in_array('/attendance/list', Yii::app()->session['menuauthlist']) ||
                                            in_array('/attendanceGenerator/list', Yii::app()->session['menuauthlist']) ||
                                            in_array('/punchingReport/list', Yii::app()->session['menuauthlist']) ||
                                            in_array('/attendanceRequest/list', Yii::app()->session['menuauthlist']) ||
                                            in_array('/manualPunch/request', Yii::app()->session['menuauthlist']) ||
                                            in_array('/photoPunch/request', Yii::app()->session['menuauthlist']) ||
                                            in_array('/reports/mypunches', Yii::app()->session['menuauthlist']) ||
                                            in_array('/manualPunch/report', Yii::app()->session['menuauthlist']) ||
                                            in_array('/attendanceRequest/list', Yii::app()->session['menuauthlist']) ||
                                            in_array('/manualPunch/list', Yii::app()->session['menuauthlist']))), 'items' => array(



                                            // array('label' => 'Employee Allocation', 'url' =>array('/usersite/admin'), 'visible' => ((in_array('/usersite/admin', Yii::app()->session['menuauthlist'])))),
                                            // array('label' => 'Attendence Report', 'url' =>array('/accesslog/attreport'), 'visible' => ((in_array('/accesslog/attreport', Yii::app()->session['menuauthlist'])))),
                                            // array('label' => 'Punch Report', 'url' =>array('/punching/accesslog/mylog'), 'visible' => ((in_array('/punching/accesslog/mylog', Yii::app()->session['menuauthlist'])))),
                                            // array('label' => 'Manual Punch Log', 'url' =>array('/punching/custompunch/admin'), 'visible' => ((in_array('/punching/custompunch/admin', Yii::app()->session['menuauthlist'])))),
                                            // array('label' => 'Manual Punch Report', 'url' =>array('/manualentryDate/index'), 'visible' => ((in_array('/manualentryDate/index', Yii::app()->session['menuauthlist'])))),
                                            // array('label' => 'Employee Allocation Request', 'url' =>array('/employeerequest/admin'), 'visible' => ((in_array('/employeerequest/admin', Yii::app()->session['menuauthlist'])))),

                                            array('label' => 'Photo Punch', 'url' => array('/PhotoPunch/default/index'), 'visible' => ((in_array('/photoPunch/view', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Attendance', 'url' => array('/attendance/attendance/index'), 'visible' => ((in_array('/attendance/list', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Daily Punch log', 'url' => array('/punch/generateresult'), 'visible' => ((in_array('/attendanceGenerator/list', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Punching Report', 'url' => array('/reports/finereport'), 'visible' => ((in_array('/punchingReport/list', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Attendance Request', 'url' => array('/AttendanceRequest/index'), 'visible' => ((in_array('/attendanceRequest/list', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Manual Punch', 'url' => array('/attendance/IgnorePunches/ManageShiftPunches'), 'visible' => ((in_array('/manualPunch/list', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Manual Punch request', 'url' => array('/manualEntryReportTbl/index'), 'visible' => ((in_array('/manualPunch/request', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Photo Punch request', 'url' => array('/reports/photoPunchReport', 'https'), 'visible' => ((in_array('/photoPunch/request', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'My punch request report', 'url' => array('/reports/mypunches', 'https'), 'visible' => ((in_array('/reports/mypunches', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Manual Punch Report', 'url' => array('/attendance/IgnorePunches/EntryReport'), 'visible' => ((in_array('/manualPunch/report', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'My Attendance', 'url' => array('/attendance/attendance/Myattendance'), 'visible' => ((in_array('/photoPunch/view', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Holidays', 'url' => array('/punching/holidays/index'), 'visible' => ((in_array('/holiday/view', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Leaves', 'url' => array('/leave/default/index'), 'visible' => ((in_array('/leave/apply', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Leave Requests', 'url' => array('/leave/default/manageleaves'), 'visible' => ((in_array('/leave/view', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Apply Comp Off', 'url' => array('/leave/default/compoff'), 'visible' => ((in_array('/leave/apply-compoff', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Comp Off Request', 'url' => array('/leave/default/compoff-request'), 'visible' => ((in_array('/leave/apply-compoff', Yii::app()->session['menuauthlist'])))),
                                        )
                                    ),
                                    array('label' => 'Settings', 'url' => 'javascript:;', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown', 'data-close-others' => true, 'aria-expanded' => false), 'items' => array(
                                        array('label' => 'PDF Layouts', 'url' => array('/ReportsTemplate/index'), 'visible' => ((in_array('/reportsTemplate/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Calendar', 'url' => array('/masters/calender/index'), 'visible' => ((in_array('/masters/calender/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Contractor', 'url' => array('/masters/contractors/index'), 'visible' => ((in_array('/masters/contractors/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Department', 'url' => array('/department/admin'), 'visible' => ((in_array('/department/admin', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Email Notification', 'url' => array('/mailSettings/emailnotification'), 'visible' => ((in_array('/mailSettings/emailnotification', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'General Settings', 'url' => array('/mailSettings/genearlSettings'), 'visible' => ((in_array('/mailSettings/genearlSettings', Yii::app()->session['menuauthlist'])))),


                                        array('label' => 'App Settings', 'url' => array('/AppSettings/default/index'), 'visible' => ((in_array('/appSettings/default/index', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'Groups', 'url' => array('/groups/index'), 'visible' => ((in_array('/groups/index', Yii::app()->session['menuauthlist'])))),



                                        array('label' => 'Project ID Prefix', 'url' => array('/projects/projectPrefix'), 'visible' => ((in_array('/projects/projectPrefix', Yii::app()->session['menuauthlist'])))),



                                        array('label' => 'Gallery', 'url' => array('/DailyWorkProgress/gallery'), 'visible' => ((in_array('/dailyWorkProgress/gallery', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'Email template', 'url' => array('/mailSettings/emailtemplate'), 'visible' => ((in_array('/mailSettings/emailtemplate', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'Template', 'url' => array('/Template/index'), 'visible' => ((in_array('/template/index', Yii::app()->session['menuauthlist'])) && Tasks::model()->accountPermission() == 1)),

                                        array('label' => 'Holidays', 'url' => array('/masters/holidays/index'), 'visible' => ((in_array('/masters/holidays/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Menu Permissions', 'url' => array('/menu/menuPermissions/create'), 'visible' => ((in_array('/menu/menuPermissions/create', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Milestone', 'url' => array('/masters/milestone/index'), 'visible' => ((in_array('/masters/milestone/index', Yii::app()->session['menuauthlist'])))),



                                        array('label' => 'Blog', 'url' => array('/Blog/post/index'), 'visible' => ((in_array('/blog/post/index', Yii::app()->session['menuauthlist'])))),

                                        array('label' => 'Site Calender', 'url' => array('/projectCalender/create'), 'visible' => ((in_array('/projectCalender/create', Yii::app()->session['menuauthlist'])))),


                                        array('label' => 'Project Log', 'url' => array('/projects/log'), 'visible' => ((in_array('/projects/log', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Punching Devices', 'url' => array('/punching/devices/index'), 'visible' => ((in_array('/punching/devices/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Role Settings', 'url' => array('/roleSettings/admin'), 'visible' => ((in_array('/roleSettings/admin', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Task Expiry', 'url' => array('/taskExpiry/index'), 'visible' => ((in_array('/taskExpiry/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Task Log', 'url' => array('/tasks/tasklog'), 'visible' => ((in_array('/tasks/tasklog', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Unit', 'url' => array('/masters/unit/index'), 'visible' => ((in_array('/masters/unit/index', Yii::app()->session['menuauthlist'])))),


                                        array('label' => 'Resources', 'url' => array('/masters/resources/index'), 'visible' => ((in_array('/masters/resources/index', Yii::app()->session['menuauthlist'])))),


                                        array('label' => 'User Roles', 'url' => array('/userRoles/index'), 'visible' => ((in_array('/userRoles/index', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Work Sites', 'url' => array('/clientsite/admin'), 'visible' => ((in_array('/clientsite/admin', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'General Sites ', 'url' => array('/clientsite/generalSite'), 'visible' => ((in_array('/clientsite/admin', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Work Type', 'url' => array('/workType/index'), 'visible' => ((in_array('/workType/create', Yii::app()->session['menuauthlist'])))),
                                    ), 'visible' => (in_array('/masters/calender/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/masters/contractors/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/department/admin', Yii::app()->session['menuauthlist']) ||
                                        in_array('/masters/holidays/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/menu/menuPermissions/create', Yii::app()->session['menuauthlist']) ||
                                        in_array('/masters/milestone/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/projects/logn', Yii::app()->session['menuauthlist']) ||
                                        in_array('/punching/devices/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/taskExpiry/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/masters/unit/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/userRoles/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/clientsite/admin', Yii::app()->session['menuauthlist']) ||
                                        in_array('/workType/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/projectCalender/create', Yii::app()->session['menuauthlist']) ||
                                        in_array('/appSettings/default/index', Yii::app()->session['menuauthlist']) ||
                                        //here


                                        in_array('/groups/index', Yii::app()->session['menuauthlist']))),
                                    array('label' => ($boq_pending_count != 0 && in_array('/dailyWorkProgress/pendingrequests', Yii::app()->session['menuauthlist'])) ? 'WPR<span class="badge  notify-badge notify-number" style="background-color:#f44336;margin-top: -18px;">' . $boq_pending_count . '</span>' : 'WPR', 'url' => 'javascript:;', 'linkOptions' => array('class' => 'dropdown-toggle caretiden notify-number', 'data-toggle' => 'dropdown', 'data-close-others' => true, 'aria-expanded' => false), 'items' => array(
                                        // array('label' => 'Assigned Projects', 'url' =>array('/projects/addAssigneetoProjects'), 'visible' => ((in_array('/projects/addAssigneetoProjects', Yii::app()->session['menuauthlist'])))),
                                        array('label' => ($boq_pending_count != 0 && in_array('/dailyWorkProgress/pendingrequests', Yii::app()->session['menuauthlist'])) ? 'Pending Requests<span class="badge  notify-badge" style="background-color:#f44336;">' . $boq_pending_count . '</span>' : 'Pending Requests', 'url' => array('/DailyWorkProgress/pendingrequests'), 'visible' => ((in_array('/dailyWorkProgress/pendingrequests', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Daily Labour Report', 'url' => array('/dailyWork/index'), 'visible' => ((in_array('/dailyWork/index', Yii::app()->session['menuauthlist'])))),
                                        // array('label' => 'Daily Work Progresses Old', 'url' => array('/DailyWorkProgress/index'), 'visible' => ((in_array('/dailyWorkProgress/index', Yii::app()->session['menuauthlist'])))),

                                        // array('label' => 'Daily Work Progresses ', 'url' => array('/DailyWorkProgress/new'), 'visible' => ((in_array('/dailyWorkProgress/index', Yii::app()->session['menuauthlist'])))),
                                        
                                          array('label' => 'Daily Work Progresses ', 'url' => array('/DailyWorkProgress/wpr'), 'visible' => ((in_array('/dailyWorkProgress/index', Yii::app()->session['menuauthlist'])))),
                                        

                                        array('label' => 'All Work Progress', 'url' => array('/DailyWorkProgress/workReport'), 'visible' => ((in_array('/dailyWorkProgress/workReport', Yii::app()->session['menuauthlist'])))),
                                        array('label' => 'Reports', 'url' => array('/DailyWorkProgress/reports'), 'visible' => ((in_array('/dailyWorkProgress/reports', Yii::app()->session['menuauthlist'])))),
                                    ), 'visible' => (in_array('/projects/addAssigneetoProjects', Yii::app()->session['menuauthlist']) ||
                                        in_array('/dailyWork/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/DailyWorkProgress/index', Yii::app()->session['menuauthlist']) ||
                                        in_array('/DailyWorkProgress/workReport', Yii::app()->session['menuauthlist']) ||
                                        in_array('/DailyWorkProgress/reports', Yii::app()->session['menuauthlist']))),

                                    array(
                                        'label' => 'Leave', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'items' => array(
                                            array('label' => 'My leaves', 'url' => array('/leave/default/index'), 'visible' => ((in_array('/leaverulesController/index', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Leave Request', 'url' => array('/leave/default/manageleaves'), 'visible' => ((in_array('/leaverulesController/manageleaves', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Comp Off Request', 'url' => array('/leave/default/managecompoff'), 'visible' => ((in_array('/leaverulesController/managecompoff', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Apply Leave', 'url' => array('/leave/default/create'), 'visible' => ((in_array('/leaverulesController/create', Yii::app()->session['menuauthlist'])))),
                                            array('label' => 'Apply Comp off', 'url' => array('/leave/default/compoff'), 'visible' => ((in_array('/leaverulesController/compoff', Yii::app()->session['menuauthlist'])))),
                                        ),
                                        'visible' => ((in_array('/leaverulesController/index', Yii::app()->session['menuauthlist']))) ||
                                            ((in_array('/leaverulesController/index', Yii::app()->session['menuauthlist']))) ||
                                            ((in_array('/leaverulesController/manageleaves', Yii::app()->session['menuauthlist']))) ||
                                            ((in_array('/leaverulesController/managecompoff', Yii::app()->session['menuauthlist']))) ||
                                            ((in_array('/leaverulesController/create', Yii::app()->session['menuauthlist']))) ||
                                            ((in_array('/leaverulesController/compoff', Yii::app()->session['menuauthlist'])))
                                    ),
                                    array(
                                        'label' => 'Accounts', 'url' => '#', 'visible' => (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != ''), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                        'items' => $allowed_apps_menu,
                                        'itemCssClass' => 'app_switchmenu',
                                        'htmlOptions' => array('class' => 'nav navbar-nav  navbar-right'),
                                    ),
                                ),
                                'submenuHtmlOptions' => array('class' => 'dropdown-menu dropdown-menu-default settings-nav-style'),
                                'htmlOptions' => array('class' => 'nav navbar-nav'),
                            ));
                            ?>
                        </div>

                    </div>


                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="">
                <!-- class="page-header-menu" -->
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <form class="search-form" action="extra_search.html" method="GET" style="display:none">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                            </span>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>

        <!-- END HEADER -->
        <!-- BEGIN PAGE CONTAINER -->
        <div class="page-container">
            <!-- BEGIN PAGE HEAD -->
            <div class="page-head">
                <div class="container">
                    <?php if (Yii::app()->user->hasFlash('success')) : ?>
                        <div class="alert alert-success alert-dismissable" id="success-alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::app()->user->hasFlash('error')) : ?>
                        <div class="alert alert-danger alert-dismissable" id="success-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Yii::app()->user->getFlash('error'); ?>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
            <!-- END PAGE HEAD -->
            <!-- BEGIN PAGE CONTENT -->
            <div class="page-content-image"></div>
            <div class="page-content">
                <!-- <div class="blur_bg"></div> -->
                <div class="container-fluid content_sec">
                    <div class="loaderdiv" style="margin: 0 auto;display: none">
                    </div>
                    <!-- BEGIN PAGE BREADCRUMB -->

                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <!-- <div class="row"> -->
                    <!-- <div class="col-md-12"> -->

                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="table-responsive"> -->

                            <?php
                            echo $content;
                            ?>
                            <!-- </div> -->
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <!-- </div> -->

                    <!-- </div> -->
                </div>
            </div>

            <div class="page-footer">
                <div class="container clearfix">



                    <div class='easytransitions'>
                        <div class='easytransitions_transition'>
                            <div class='div easytransitions_transition__part-1 none'></div>
                            <div class='div easytransitions_transition__part-2 none'></div>
                            <div class='div easytransitions_transition__part-3 none'></div>
                            <div class='div easytransitions_transition__part-4 none'></div>
                            <div class='div easytransitions_transition__part-5 none'></div>
                            <div class='div easytransitions_transition__part-6 none'></div>
                            <div class='div easytransitions_transition__part-7 none'></div>
                            <div class='div easytransitions_transition__part-8 none'></div>
                        </div>

                        <div class="pull-left expand-projects btn_hover active_slide split_diagonal" id="box" data-transition='split_diagonal'>
                            <i class="glyphicon tasks toggle-expand-icon close-button">
                                <span class="project_text">PROJECTS</span>
                            </i>
                            <div id="expand-projects" style="display:none;">
                                <input type="hidden" id="text_id">


                                <div class="row margin-top-20 padding-bottom-20 dashboard-top-row">

                                    <div class="container display-flex grid-col-gap-10">
                                        <div>
                                            <input type="text" name="search" value="" class="input-medium search-query" placeholder="Search" id="project_name">

                                        </div>
                                        <div>
                                            <input type="button" name="button" value="search" class="btn btn-small btn-primary" id="search_project">
                                        </div>
                                    </div>
                                </div>



                                <div class="container custom-container" id="text">
                                    <div class="row">
                                        <?php
                                        foreach ($projects as $key => $value) {
                                        ?>

                                            <div class="col-xs-6 col-md-3 col-xl-2">
                                                <?php
                                                $image_path = Yii::app()->basePath . '/../uploads/project/' . $value['img_path'];
                                                if ($value['img_path'] != '') {
                                                    $path = Yii::app()->baseUrl . '/uploads/project/' . $value['img_path'];
                                                }
                                                if (!file_exists($image_path) || $value['img_path'] == '') {
                                                    $caption_class = 'thumb-background';
                                                } else {
                                                    //$caption_class = 'thumbnail-caption';
                                                    $caption_class = 'thumb-background';
                                                }
                                                ?>
                                                <div class="thumbnail <?php
                                                                        echo $caption_class;
                                                                        ?> " data-id="<?php echo $value['pid']; ?>" style="background-image: url('<?php echo Yii::app()->request->baseUrl . "/uploads/project/" . $value['img_path']; ?>')">
                                                    <div class="caption">
                                                        <h2 class="text-center"><?php echo $value['name']; ?></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php
                    $key = 'url encryption';

                    if (function_exists('mcrypt_create_iv')) {
                        $iv = @mcrypt_create_iv(
                            mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
                            MCRYPT_DEV_URANDOM
                        );
                    } else {
                        $iv = openssl_random_pseudo_bytes(
                            openssl_cipher_iv_length('aes-256-cbc')
                        );
                    }

                    if (function_exists('mcrypt_encrypt')) {

                        $encrypted = base64_encode(
                            $iv .
                                @mcrypt_encrypt(
                                    MCRYPT_RIJNDAEL_128,
                                    hash('sha256', $key, true),
                                    Yii::app()->user->id,
                                    MCRYPT_MODE_CBC,
                                    $iv
                                )
                        );
                        $encryptedmain = base64_encode(
                            $iv .
                                @mcrypt_encrypt(
                                    MCRYPT_RIJNDAEL_128,
                                    hash('sha256', $key, true),
                                    Yii::app()->user->mainuser_id,
                                    MCRYPT_MODE_CBC,
                                    $iv
                                )
                        );
                    } else {
                        $encrypted = base64_encode(openssl_encrypt(Yii::app()->user->id, 'aes-256-cbc', $key, 0, $iv) . '::' . $iv);
                        $encryptedmain = base64_encode(openssl_encrypt(Yii::app()->user->mainuser_id, 'aes-256-cbc', $key, 0, $iv) . '::' . $iv);
                    }
                    ?>
                    <?php

                    if (Yii::app()->user->role == 1) {
                        $this->widget('MasqueradeWidget');
                    }
                    ?>
                    <?php
                    /*
                          //Change user login from Direct admin user
                          if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->mainuser_role) && (Yii::app()->user->mainuser_role == 1)) {
                          ?>
                          <!--                <div>-->
                          <div class="pull-right"><span>Change user session: </span>
                          <?php
                          $tblpx = yii::app()->db->tablePrefix;

                          $sql = "SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                          . "`{$tblpx}user_roles`.`role` "
                          . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                          . "WHERE status=0 ORDER BY user_type,full_name ASC";
                          $result = Yii::app()->db->createCommand($sql)->queryAll();
                          $listdata = CHtml::listData($result, 'userid', 'full_name', 'role');
                          echo CHtml::dropDownList('shift_userid', '', $listdata, array('options' => array(Yii::app()->user->id => array('selected' => true)),
                          'ajax' => array(
                          'type' => 'POST', //request type
                          'url' => CController::createUrl('/users/usershift'), //url to call.
                          'success' => 'js:function(data){
                          location.reload();
                          }',
                          'data' => array('shift_userid' => 'js:this.value'),
                          )));
                          ?>
                          </div>
                          <?php //if (Yii::app()->user->mainuser_id != Yii::app()->user->id) { ?>
                          <!--span style="color:red;font-style: italic;text-decoration: blink;margin-left:20px;">(Note: You are working on other user's session)</span-->
                          <?php //} ?>
                          <?php
                          }
                         */
                    ?>
                    <div class="footer-text footer-align"><?php echo date('Y'); ?> &copy; <?php echo Yii::app()->name; ?>. All Rights
                        Reserved.
                    </div>
                </div>
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    </div>
    <!-- END PRE-FOOTER -->
    <!-- BEGIN FOOTER -->

    <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/respond.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->

    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/scripts/metronic.js" type="text/javascript">
    </script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/demo.js" type="text/javascript">
    </script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/project.js" type="text/javascript">
    </script>

    <script>
        jQuery(document).ready(function() {
            $('.loaderdiv').hide();
            // initiate layout and plugins
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            Demo.init(); // init demo features

            jQuery('.savepdf').on('click', function(e) {
                e.preventDefault();

                var linkurl = $(this).attr('href');

                var filter_items = '';
                $('.filters input[type=text], .filters select').each(function() {

                    colname = $('thead  tr:eq(0) th:eq(' + $(this).closest("td").index() + ')')
                        .text();

                    if ($(this).prop("tagName").toLowerCase() === 'select') {
                        //alert($(this).prop("tagName").toLowerCase());
                        if ($(this).find('option:selected').text() != '') {
                            filter_items += "<b>" + colname + "</b> : " + $(this).find(
                                'option:selected').text() + "<br />";
                        }



                    } else {
                        if ($(this).val() != '') {
                            filter_items += "<b>" + colname + "</b> : " + $(this).val() + "<br />";
                        }
                    }
                });
                window.location.href = linkurl + "&filter_cr=" + filter_items;

            });
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#success-alert").slideUp(500);
            });
            $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#danger-alert").slideUp(500);
            });
            var trigger_status = "<?php echo yii::app()->user->trigger_status; ?>";
            if (trigger_status == 1000) {
                if ($('#box').find('i').hasClass('tasks')) {
                    $('.project_text').hide();
                    $('#box').addClass('fullScreen');
                    var active_slide = $('.active_slide')
                    set_transition(active_slide);
                    $('#expand-projects').show();

                    $('.thumbnail').addClass('ease_out');
                    $('#box').find('i').removeClass('tasks').addClass('glyphicon-remove');

                    $(".page-content-image").css({
                        "filter": "blur(4px)",
                        "height": "-webkit-fill-available"
                    });
                    $(".ui-dialog").css({
                        "visibility": "hidden"
                    });
                    $(".content_sec").hide();
                    $('.navbar-nav').hide();
                    $('.page-header').hide();
                    $('.switch-user-custom').hide();
                    $('.footer-text').hide();
                    $('.scroll-to-top').hide();

                    $('.page-footer').css('background', 'transparent');
                    $('#text_id').val(1);
                    if (trigger_status == 1) {
                        $.ajax({
                            'url': "index.php?r=users/changetrigger",
                            data: {
                                status: trigger_status
                            },
                            dataType: 'Json',
                            type: 'POST',
                            success: function(result) {}
                        })
                    }
                } else {
                    $('#box').removeClass('fullScreen');
                    $('.project_text').show();
                    $('#expand-projects').hide();
                    $('#box').find('i').removeClass('glyphicon-remove').addClass('tasks');
                    $(".page-content-image").css({
                        "filter": "blur(0px)",
                        "height": "auto"
                    });
                    $(".ui-dialog").css({
                        "visibility": "visible"
                    });
                    $(".content_sec").show();
                    $('.navbar-nav').show();
                    $('.page-header').show();
                    $('.switch-user-custom').show();
                    $('.scroll-to-top').show();
                    $('.footer-text').show();
                    $('.page-footer').css('background', '#3b434c');
                }
            }

            $('#box').click(function() {
                var url = window.location.href;
                if (url != "") {
                    $.ajax({
                        'url': "index.php?r=site/urlredirection",
                        data: {
                            url: url
                        },
                        type: 'POST',
                        success: function() {}
                    })
                }
                if ($(this).find('i').hasClass('tasks')) {
                    $('.project_text').hide();
                    $(this).addClass('fullScreen');
                    $(this).addClass('split_screen');
                    var active_slide = $('.active_slide')
                    set_transition(active_slide);
                    $('#expand-projects').show();
                    $('.thumbnail').addClass('ease_out');
                    $(this).find('i').removeClass('tasks').addClass('glyphicon-remove');

                    $(".page-content-image").css({
                        "filter": "blur(4px)",
                        "height": "-webkit-fill-available"
                    });
                    $(".ui-dialog").css({
                        "visibility": "hidden"
                    });
                    $(".content_sec").hide();
                    $('.navbar-nav').hide();
                    $('.page-header').hide();
                    $('.switch-user-custom').hide();
                    $('.footer-text').hide();
                    $('.scroll-to-top').hide();
                    $('.page-footer').css('background', 'transparent');
                    $('body').css('overflow', 'hidden')
                    $('#text_id').val(1);
                    // var active_slide = $('.active_slide');
                    // set_transition('#box');
                    // setTimeout(function(){
                    // $('.active_slide').show()
                    // });              

                }
                // else {
                //     $(this).removeClass('fullScreen');
                //     $('.project_text').show();
                //     $('#expand-projects').hide();
                //     $(this).find('i').removeClass('glyphicon-remove').addClass('tasks');
                //     $(".page-content-image").css({
                //         "filter": "blur(0px)",
                //         "height": "auto"
                //     });
                //     $(".ui-dialog").css({
                //         "visibility": "visible"
                //     });
                //     $(".content_sec").show();
                //     $('.navbar-nav').show();
                //     $('.page-header').show();
                //     $('.switch-user-custom').show();
                //     $('.footer-text').show();
                //     $('.scroll-to-top').show();
                //     $('.page-footer').css('background', '#3b434c');
                //     $('body').css('overflow', 'auto')
                // }
            });
            $(document).on('click', '.thumbnail', function(e) {
                e.preventDefault();
                var val = $(this).attr("data-id");
                if (val != "") {
                    $.ajax({
                        'url': "index.php?r=users/assignprojectsession",
                        data: {
                            project_id: val
                        },
                        // dataType:'Json',
                        type: 'GET',
                        success: function(result) {
                            if (result != "") {
                                location.href = result;
                            }

                        }
                    })
                }
            })

            $(".close-button").click(function() {
                var text_value = $('#text_id').val();
                if (text_value == 1) {
                    window.location.reload();
                }
            });
        });

        function set_transition(tran) {
            var transition_type = tran.data('transition')
            $('.easytransitions_transition div').each(function() {
                $(this).removeClass(this.className.split(' ').pop());
                setTimeout(function() {
                    $('.easytransitions_transition div').addClass(transition_type)
                }, 100)

            })
        }

        $('.navbar-toggle').click(function() {
            $(".page-header").toggleClass("mobile-fixed");
        });
        // search project

        $("#search_project").click(function() {

            var project_name = $('#project_name').val();


            if (project_name != "") {
                $.ajax({
                    type: 'POST',
                    url: "index.php?r=projects/searchProject",
                    data: {
                        project_name: project_name
                    },
                    success: function(data) {
                        $('#text_id').val(1);
                        $('#expand-projects').html(data);





                    },
                    error: function(data) {
                        alert('Error occured.please try again');
                    },


                });
            }
        });
    </script>
    <div class="loading">&#8230;</div>
</body>
<style>
    .footer-text.footer-align {
        float: right;
    }

    .loaderdiv {
        margin: 0 auto;

        position: fixed;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;

        height: 100vh;
        background: url('./images/loading.gif') no-repeat center;
        background-color: #fff;
        z-index: 2;
        opacity: 0.8;
    }

    .loading {
        position: fixed;
        z-index: 999;
        height: 2em;
        width: 2em;
        overflow: visible;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        display: none;
    }

    /* Transparent Overlay */
    .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.3);
    }

    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
    }

    /* .tasks{visibility:hidden;} */

    .loading:not(:required):after {
        content: '';
        display: block;
        font-size: 10px;
        width: 1em;
        height: 1em;
        margin-top: -0.5em;
        -webkit-animation: spinner 1500ms infinite linear;
        -moz-animation: spinner 1500ms infinite linear;
        -ms-animation: spinner 1500ms infinite linear;
        -o-animation: spinner 1500ms infinite linear;
        animation: spinner 1500ms infinite linear;
        border-radius: 0.5em;
        -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
        box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
    }

    /* Animation */

    @-webkit-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-moz-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-o-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
</style>
<!-- END BODY -->
<style>
    @import url(https://fonts.googleapis.com/css?family=Nunito:400,700,300);

    body,
    html {
        background-color: transparent;
    }

    body .easytransitions_transition div {
        width: 200px;
        height: 200px;
        background: rgb(25, 25, 25);
        position: absolute;
        -webkit-transform: scale(0);
        transform: scale(0);
        z-index: 1;
    }

    body .easytransitions_transition .split_diagonal,
    body .easytransitions_transition .split_diagonal_alt,
    body .easytransitions_transition .split_diamond {
        -webkit-animation: diamond 1.2s;
        animation: diamond 1.2s;
        -webkit-transform: scale(0) rotate(45deg);
        transform: scale(0) rotate(45deg);
    }

    body .easytransitions_transition .split_horizontal,
    body .easytransitions_transition .wipe_top,
    body .easytransitions_transition .wipe_bottom {
        -webkit-transform: scaleY(0);
        transform: scaleY(0);
    }

    body .easytransitions_transition .split_vertical,
    body .easytransitions_transition .wipe_left,
    body .easytransitions_transition .wipe_right {
        -webkit-transform: scaleX(0);
        transform: scaleX(0);
    }

    body .easytransitions_transition__part-1 {
        position: absolute;
        left: -100px;
        bottom: 600px;
    }

    body .easytransitions_transition__part-1.split_diagonal_alt,
    body .easytransitions_transition__part-1.split_vertical,
    body .easytransitions_transition__part-1.split_horizontal {
        display: none;
    }

    body .easytransitions_transition__part-2 {
        position: absolute;
        bottom: -100px;
        left: -100px;

    }

    body .easytransitions_transition__part-2.split_diagonal {
        display: none;
    }

    body .easytransitions_transition__part-3 {
        position: absolute;
        top: -100px;
        right: -100px;
    }

    body .easytransitions_transition__part-3.split_diagonal {
        display: none;
    }

    body .easytransitions_transition__part-4 {
        position: absolute;
        bottom: -100px;
        right: -100px;
    }

    body .easytransitions_transition__part-4.split_diagonal_alt {
        display: none;
    }

    body .easytransitions_transition__part-5 {
        height: 100% !important;
        display: none;
        left: 0px;
        -webkit-transform-origin: 0px 200px;
        transform-origin: 0px 200px;
        -webkit-animation: vertical 1.2s forwards;
        animation: vertical 1.2s forwards;
    }

    body .easytransitions_transition__part-5.split_vertical,
    body .easytransitions_transition__part-5.wipe_left {
        display: block;
    }

    body .easytransitions_transition__part-6 {
        height: 100% !important;
        display: none;
        right: -200px;
        -webkit-transform-origin: 0px 200px;
        transform-origin: 0px 200px;
        -webkit-animation: vertical_r 1.2s forwards;
        animation: vertical_r 1.2s forwards;
    }

    body .easytransitions_transition__part-6.split_vertical,
    body .easytransitions_transition__part-6.wipe_right {
        display: block;
    }

    body .easytransitions_transition__part-7 {
        width: 100% !important;
        display: none;
        bottom: -100px;
        -webkit-animation: horizontal 1s forwards;
        animation: horizontal 1s forwards;
    }

    body .easytransitions_transition__part-7.split_horizontal,
    body .easytransitions_transition__part-7.wipe_bottom {
        display: block;
    }

    body .easytransitions_transition__part-8 {
        width: 100% !important;
        display: none;
        top: -100px;
        -webkit-animation: horizontal 1s forwards;
        animation: horizontal 1s forwards;
    }

    body .easytransitions_transition__part-8.split_horizontal,
    body .easytransitions_transition__part-8.wipe_top {
        display: block;
    }

    @-webkit-keyframes diamond {
        0% {
            -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
        }

        50% {
            -webkit-transform: rotate(45deg) scale(10);
            transform: rotate(45deg) scale(10);
        }

        100% {
            -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
        }
    }

    @keyframes diamond {
        0% {
            -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
        }

        50% {
            -webkit-transform: rotate(45deg) scale(10);
            transform: rotate(45deg) scale(10);
        }

        100% {
            -webkit-transform: rotate(45deg) scale(0);
            transform: rotate(45deg) scale(0);
        }
    }

    body .easytransitions .active_slide {
        display: block;
    }

    @media(min-width: 1300px) {
        body .easytransitions_transition__part-1 {
            position: absolute;
            left: -100px;
            bottom: 800px;
        }
    }
</style>

<script>
    window.onscroll = function() {
        myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
</script>




</html>