<?php

class AttendanceGeneratorController extends Controller
{

    public $allpunchs = array();
    public $getpunches = false;
    public $punchresult = array();
    public $empdevid = array();
    public $layout = '//layouts/column2';
    public $employees = array();
    public $punches = array();
    public $pdate = '';  //Punchdate
    public $numdays = 0;
    public $devices = '';
    public $interchange = array();

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {

        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->session['pmsmenuall'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuall'])) {
                $accessArr = Yii::app()->session['pmsmenuall'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuauth'])) {
                $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuguest'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuguest'])) {
                $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
            }
        }
        $access_privlg = count($accessauthArr);



        return array(
            array(
                'allow',
                'actions' => array('PunchReportDaily', 'Testmail', 'updateShiftTemplateMonthly', 'Monthlyleaveupdate', 'UpdateAttendance', 'UpdateOtAttendance'),
                'users' => array('*')
            ),
            array(
                'allow',
                'actions' => $accessArr,
                'users' => array('*'),
                //  'expression' => "$access_privlg > 0",
            ),
            array(
                'allow',
                'actions' => $accessauthArr,
                'users' => array('*'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow',
                'actions' => $accessauthArr,
                'users' => array('*'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function timetosec($time)
    {
        $parsed = date_parse($time);
        return $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
    }

    public function setEmployees($sdate = '', $edate = '')
    {

        $company = '';
        $comp = '';
        $where = '';
        $shift_users = '';
        $shift_emp = '';

        $designation_where = "";
        $skill_where = "";
        $group_filter = "";

        if (!isset($_GET['cron'])) {


            if (!isset(Yii::app()->user->site_userids) and isset(Yii::app()->user->site_types) and Yii::app()->user->site_types > 0) {

                $this->getprojectsiteusers();
            }




            // if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) {
            //     $where = $company;
            // } else if (!empty($usersall)) {
            //     $where = 'and emp_id = ' . Yii::app()->user->id . ' or report_to =' . Yii::app()->user->id . ' or emp_id in (' . $usersall . ') and ' . $comp;
            // } else {
            //     $where = 'and emp_id = ' . Yii::app()->user->id . ' or report_to =' . Yii::app()->user->id;
            //     if ($comp != '') {
            //         $where = $company;
            //     }
            // }
            // print_r($where);exit;

            if (isset(Yii::app()->user->shift_id) and Yii::app()->user->shift_id > 0) {

                $shiftid = Yii::app()->user->shift_id;
                $user = array();
                $shift = Yii::app()->db->createCommand("SELECT user_id FROM `pms_shift_assign` WHERE `shift_id`=" . $shiftid . " and `att_date`='" . $this->pdate . "'")->queryAll();
                foreach ($shift as $data) {
                    $user[] = $data['user_id'];
                }

                $shift_users = implode(",", $user);
            }


            if (isset(Yii::app()->user->designation_id_filter) and Yii::app()->user->designation_id_filter > 0) {

                $designation_id = Yii::app()->user->designation_id_filter;
                $designation_where = 'and designation = ' . $designation_id;
            }



            if (isset(Yii::app()->user->group_filter) and Yii::app()->user->group_filter > 0) {

                $group_id = Yii::app()->user->group_filter;
                if ($group_id != '') {

                    $grp = $this->getChildGroups($group_id);
                    if (!empty($grp)) {
                        $grpcon = ' group_id in (' . $grp . ')';
                    } else {
                        $grpcon = ' group_id =' . $group_id;
                    }
                }

                $group_user_id = $this->findUserId($sdate, $grpcon);
                $group_user_id = implode(",", $group_user_id);
                $group_filter = "and userid in (" . $group_user_id . ")";
            }



            if (isset(Yii::app()->user->skill_id_filter) and Yii::app()->user->skill_id_filter > 0) {

                $skill_id = Yii::app()->user->skill_id_filter;
                $skill_where = 'and `skill_id` = ' . $skill_id;
            }

            $con = ' or';
            if (Yii::app()->user->role == 1) {
                $con = ' and';
            }
            if ($shift_users != '') {
                $shift_emp = $con . ' emp_id in (' . $shift_users . ') ';
            } else if (isset(Yii::app()->user->shift_id)) {
                $shift_emp = $con . ' emp_id in (0) ';
            }

            if (isset(Yii::app()->user->shift_role) && Yii::app()->user->shift_role == 1) {
                $where = '';
                $shift_emp = '';
            }
            if (isset($_GET['manual'])) {
                $where = $company;
            }
        }


        $empsql = "select concat_ws(' ', e.first_name,e.last_name) as fullname,
    if(resignation_date >= '$sdate' and resignation_date<='$edate',1,0) as resigned_month,
    joining_date, u.label as user_type,e.userid as userid, e.reg_ip,pms_company.name as company_name,e.setting_template from pms_users as e left join pms_employee_default_data as u on u.default_data_id=e.user_type
               
               left join pms_company on pms_company.company_id = e.company_id
                group by e.userid order by fullname asc";
        // print_r($empsql);exit;

        $this->employees = Yii::app()->db->createCommand($empsql)->queryAll();

        foreach ($this->employees as $employee) {
            extract($employee);
            $this->punchresult[$userid]['empdetails'] = $employee;
            $this->punchresult[$userid]['punches'] = array();
            if(isset($deviceandcardid))
            {
                $devcardid = json_decode("{" . "$deviceandcardid" . "}", true);

                foreach ($devcardid as $deviceid => $card_emp) {
                    $this->empdevid[$deviceid][$card_emp[0]] = $card_emp[1];
                }
            }
            
        }
    }

    public function actionGetpunchesbyrange($pdate = '', $edate = '', $numdays = 0, $id = 0, $date = 0)
    {


        //  $pdate = date('Y-m-d', strtotime('-1 day', strtotime($pdate)));
        //$pdate ='2018-09-29';
        //  $pdate = date('Y-m-d', strtotime('-1 day', strtotime($pdate)));

        $this->pdate = $pdate;
        $this->numdays = $numdays + 2;
        $this->getDaywiseresult();

        if ($numdays > 0) {
            for ($i = 0; $i < $this->numdays; $i++) {
                if ($i > 0) {
                    $date = new DateTime($this->pdate);
                    $date->add(new DateInterval('P1D'));
                    $this->pdate = $date->format('Y-m-d');
                }
                $this->calculateResult();
            }
        }

        $model = new IgnorePunches;
        $this->performAjaxValidation($model);

        if (isset($_POST['IgnorePunches']) || $id != 0) {

            $emp_id = ($id != 0) ? $id : $_POST['IgnorePunches']['empid'];
            $date = ($date != 0) ? $date : $_POST['IgnorePunches']['date'];

            $prev_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
            $next_date = date('Y-m-d', strtotime('+1 day', strtotime($date)));


            $ignored = array();
            $log = array();
            $devices = DeviceAccessids::model()->findAll('userid = ' . $emp_id);

            foreach ($devices as $data) {

                $logdetails = Yii::app()->db->createCommand("SELECT al.punchlogid,al.sqllogid,al.empid,al.log_time,al.device_id,
                    al.deviceid,al.logid,pms_ignore_punches.ig_id ,pms_ignore_punches.date,comment ,pms_ignore_punches.status,decision_by,decision_date FROM  pms_punch_log AS al INNER JOIN  pms_punching_devices AS pd ON pd.device_id = al.device_id INNER JOIN pms_clientsite AS cs ON  cs.id = pd.site_id INNER JOIN pms_device_accessids AS did ON  did.deviceid = al.device_id AND empid = did.accesscard_id left join  pms_ignore_punches on  (pms_ignore_punches.`empid`= al.empid and al.log_time = pms_ignore_punches.date and al.device_id= pms_ignore_punches.device_id )  WHERE DATE_FORMAT(log_time, '%Y-%m-%d') between  '" . $prev_date . "' and
                '" . $next_date . "' and did.userid=" . $data->userid . " and al.device_id = " . $data->deviceid . " group by log_time ORDER BY log_time, empid ")->queryAll();

                foreach ($logdetails as $key => $value) {
                    $log[] = $value;
                }
            }
            $manualentry = Yii::app()->db->createCommand("SELECT *,date as log_time,emp_id as empid FROM `pms_manual_entry` WHERE `emp_id`= " . $emp_id . " and  date(`shift_date`) between  '" . $prev_date . "' and '" . $next_date . "' ")->queryAll();

            $newarr = array_merge($log, $manualentry);

            //echo '<pre>';print_r($newarr);exit;


            $log1 = array();
            foreach ($newarr as $val) {
                $log1[strtotime($val['log_time'])] = $val;
            }
            ksort($log1);
        } else {

            $log = array();
            $ignored = array();
            $emp_id = null;
            $date = null;
            $log1 = array();
        }

        $newmodel = new IgnorePunches('search');
        // $arr = $this->getIndividualResult(13);
        //  echo '<pre>';print_r($arr);die;
        $this->render(
            'shiftpunches',
            array(
                'newmodel' => $newmodel,
                'model' => $model,
                'logmodel' => $log1,
                'userid' => $emp_id,
                'date' => $date,
                'ignored' => $ignored
            )
        );
    }

    public function actionGenerateresult($pdate = '', $numdays = 0, $export = 0, $excel = 0)
    {
        if (!isset(Yii::app()->user->reg_ip)) {
            $this->ipStore();
        }

        if (isset($_REQUEST['shift_id']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('shift_id', intval($_REQUEST['shift_id']));
            if (Yii::app()->user->shift_id == 0) {
                unset(Yii::app()->user->shift_id);
            }
            die('1');
        }
        if (isset($_REQUEST['designation_id']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('designation_id_filter', intval($_REQUEST['designation_id']));
            if (Yii::app()->user->designation_id_filter == 0) {
                unset(Yii::app()->user->designation_id);
            }
            die('1');
        }

        if (isset($_REQUEST['attendance_type']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('attendance_type', intval($_REQUEST['attendance_type']));
            if (Yii::app()->user->attendance_type == 0) {
                unset(Yii::app()->user->attendance_type);
            }
            die('1');
        }

        if (isset($_REQUEST['group']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('group_filter', intval($_REQUEST['group']));
            if (Yii::app()->user->group_filter == 0) {
                unset(Yii::app()->user->group_filter);
            }
            die('1');
        }
        if (isset($_REQUEST['skill_id']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('skill_id_filter', intval($_REQUEST['skill_id']));
            if (Yii::app()->user->skill_id_filter == 0) {
                unset(Yii::app()->user->skill_id_filter);
            }
            die('1');
        }
        $shiftid = 0;
        if (isset(Yii::app()->user->shift_id) and Yii::app()->user->shift_id > 0) {
            $shiftid = Yii::app()->user->shift_id;
        }

        if ($pdate == '') {
            $cdate = date('Y-m-d');
            if (isset($_GET['days']) and intval($_GET['days']) < 0) {
                $cddate = new DateTime($cdate);
                $cddate->sub(new DateInterval('P' . abs($_GET['days']) . 'D'));
                $pdate = $cddate->format('Y-m-d');
            } else {
                $pdate = $cdate;
            }
            if (isset($_REQUEST['reportdate'])) {
                $date2 = date_create($_REQUEST['reportdate']);
                $date1 = date_create(date('Y-m-d'));
                $diff = date_diff($date1, $date2);
                $this->redirect(array('AttendanceGenerator/generateresult&days=' . ($diff->days * -1)));
                Yii::app()->end;
            }
        }

        if (isset(Yii::app()->user->project_site) and Yii::app()->user->project_site != "") {
            $sql = "SELECT user_id FROM `pms_usersite` "
                . "WHERE site_id = " . Yii::app()->user->project_site
                . " AND(('" . $pdate . "' BETWEEN date_from AND date_to) "
                . "OR ('" . $pdate . "'>=date_from and  date_to IS NULL))";

            $allids = Yii::app()->db->createCommand($sql)->queryAll();
            $site_userids = array_column($allids, 'user_id');
            $site_userids = implode(',', $site_userids);
            Yii::app()->user->setState('site_userids', $site_userids);
        }

        $this->pdate = $pdate;
        $this->numdays = $numdays + 1;
        $this->getDaywiseresult();
        $this->calculateResult();
        $this->getInterchangeuser();

            //    echo '<pre>';
            //    print_r($this->punchresult);
            //    exit();
               
        $data = array(
            'punchresult' => $this->punchresult,
        );

        if ($export == 1) {
            $data_output = $this->renderPartial('punchreportexport', '', true);
            $this->export_reportpdf($data_output, $pdate);
        } elseif ($excel == 1) {
            $this->export_excel($data, $pdate);
        } else {
            $this->render('punchreport');
        }
    }

    public function getDaywiseresult()
    {
        $date = new DateTime($this->pdate);
        $date->add(new DateInterval('P' . ($this->numdays + 1) . 'D'));
        $tilldate = $date->format('Y-m-d');
        // echo $this->pdate; exit;
        $this->setEmployees($this->pdate, $tilldate); //setting $this->employees , $this->empdevid and $this->punchresult
        $this->setPunchingDevices();
        $this->getpunches($this->pdate, $tilldate);
    }

    public function setPunchingDevices()
    {
        $sql = 'Select * , shot_name as device_name from pms_punching_devices';
        $devices = Yii::app()->db->createCommand($sql)->queryAll();
        $this->devices = CHtml::listData($devices, 'device_id', 'device_name');
    }

    // shift details
    public function getShiftData($userid, $att_date)
    {

        $date = date('Y-m-d', strtotime($att_date));
        $next = date('Y-m-d', strtotime('+1 day', strtotime($date)));

        $shiftsql = "SELECT pms_shift_assign.shift_id,shift_name, user_id, att_date,is_continuous_shift,continuous_shift_end,full_ot, "
            . "shift_starts, shift_ends,grace_period_before,grace_period_after,color_code,no_ot,ot_offset,apply_ot_offset,min_ot_value,max_ot_value,	fIxed_ot , min_ot_intime  "
            . "FROM `pms_shift_assign`left join "
            . "pms_employee_shift on pms_employee_shift.shift_id = pms_shift_assign. shift_id "
            . "WHERE `user_id` = " . $userid . " and `att_date` = '" . $date . "' ";

        $res = Yii::app()->db->createCommand($shiftsql)->queryRow();

        // echo '<pre>';print_r($res);exit;

        if (strtotime($res['shift_starts']) < strtotime($res['shift_ends'])) {
            $shiftend_date = $res['att_date'] . " " . $res['shift_ends'] . ":00";
        } else {
            $next = date('Y-m-d', strtotime('+1 day', strtotime($res['att_date'])));
            $shiftend_date = $next . " " . $res['shift_ends'] . ":00";
        }
        $prev = date('Y-m-d', strtotime('-1 day', strtotime($res['att_date'])));

        $gracetime_before = floatval($res['grace_period_before']) * 60 * 60;
        $gracetime_after = floatval($res['grace_period_after']) * 60 * 60;

        $shift_start = ($res['shift_starts'] != '') ? date('Y-m-d H:i:s', strtotime($res['att_date'] . " " . $res['shift_starts'] . ":00") - $gracetime_before) : '';
        $shift_end = ($res['shift_ends'] != '') ? date('Y-m-d H:i:s', strtotime($shiftend_date) + $gracetime_after) : '';
        $shift_end_without_grace = ($res['shift_ends'] != '') ? date('Y-m-d H:i:s', strtotime($shiftend_date)) : '';

        $nextdayres = Yii::app()->db->createCommand("SELECT shift_name, user_id, att_date, shift_starts, shift_ends,
    grace_period_before,grace_period_after,color_code FROM `pms_shift_assign`left join pms_employee_shift
    on pms_employee_shift.shift_id = pms_shift_assign. shift_id WHERE `user_id` = " . $userid . "
    and `att_date` = '" . $next . "' ")->queryRow();

        $prevdayres = Yii::app()->db->createCommand("SELECT shift_name, user_id, att_date, shift_starts, shift_ends,
    grace_period_before,grace_period_after,color_code FROM `pms_shift_assign`left join pms_employee_shift
    on pms_employee_shift.shift_id = pms_shift_assign. shift_id WHERE `user_id` = " . $userid . "
    and `att_date` = '" . $prev . "' ")->queryRow();


        if (strtotime($nextdayres['shift_starts']) < strtotime($nextdayres['shift_ends'])) {
            $shiftend_datenew = $nextdayres['att_date'] . " " . $nextdayres['shift_ends'] . ":00";
        } else {
            $next = date('Y-m-d', strtotime('+1 day', strtotime($nextdayres['att_date'])));
            $shiftend_datenew = $next . " " . $nextdayres['shift_ends'] . ":00";
        }
        /*         * previous shift */
        if (strtotime($prevdayres['shift_starts']) < strtotime($prevdayres['shift_ends'])) {
            $shiftend_dateprev = $prevdayres['att_date'] . " " . $prevdayres['shift_ends'] . ":00";
        } else {
            $next_prev = date('Y-m-d', strtotime('+1 day', strtotime($prevdayres['att_date'])));
            $shiftend_dateprev = $next_prev . " " . $prevdayres['shift_ends'] . ":00";
        }

        $gracetime_beforenew = floatval($nextdayres['grace_period_before']) * 60 * 60;
        $gracetime_afternew = floatval($nextdayres['grace_period_after']) * 60 * 60;
        /*         * previous shift */
        $gracetime_beforeprev = floatval($prevdayres['grace_period_before']) * 60 * 60;
        $gracetime_afterprev = floatval($prevdayres['grace_period_after']) * 60 * 60;

        $next_start = ($nextdayres['shift_starts'] != '') ? date('Y-m-d H:i:s', strtotime($nextdayres['att_date'] . " " . $nextdayres['shift_starts'] . ":00") - $gracetime_beforenew) : '';
        $next_end = ($nextdayres['shift_ends'] != '') ? date('Y-m-d H:i:s', strtotime($shiftend_datenew) + $gracetime_afternew) : '';
        /*         * previous shift */
        $prev_start = ($prevdayres['shift_starts'] != '') ? date('Y-m-d H:i:s', strtotime($prevdayres['att_date'] . " "
            . $prevdayres['shift_starts'] . ":00") - $gracetime_beforeprev) : '';
        $prev_end = ($prevdayres['shift_ends'] != '') ? date('Y-m-d H:i:s', strtotime($shiftend_dateprev) +
            $gracetime_afterprev) : '';

        $result = array(
            'shift_id' => $res['shift_id'], 'shift' => $res['shift_name'], 'shift_date' => $res['att_date'],
            'start_time' => $shift_start, 'end_time' => $shift_end, 'grace_period_before' => $res['grace_period_before'],
            'grace_period_after' => $res['grace_period_after'], 'nxt_shift_start' => $next_start, 'nxt_shift_end' => $next_end,
            'prev_shift_start' => $prev_start, 'prev_shift_end' => $prev_end,
            'color_code' => $res['color_code'], 'shift_start' => $res['shift_starts'], 'shift_end' => $res['shift_ends'],
            'is_continuous' => $res['is_continuous_shift'], 'full_ot' => $res['full_ot'], 'cs_end' => $res['continuous_shift_end'], 'no_ot' => $res['no_ot'], 'cur_shift_end' => $shiftend_date, 'shift_end_without_grace' => $shift_end_without_grace, 'ot_offset' => $res['ot_offset'], 'apply_ot_offset' => $res['apply_ot_offset'], 'min_ot_value' => $res['min_ot_value'], 'max_ot_value' => $res['max_ot_value'], 'fIxed_ot' => $res['fIxed_ot'], 'min_ot_intime' => $res['min_ot_intime']
        );


        return $result;
    }

    public function getDailyPunches($userid, $punchtime, $empid = 0)
    {
        if ($empid != 0) {
            $userid = $empid;    //stand by punches
        }

        $sql_direct_punch = "SELECT  al . device_id, al . empid, al . sqllogid, al . log_time,
        DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate    
        FROM pms_photopunch as al where date(log_time) between '" . $punchtime . "'
        and '" . $punchtime . "' and empid = " . $userid;
        $directpunches = Yii::app()->db->createCommand($sql_direct_punch)->queryAll();

        $sql_manual_punch = "SELECT *,date as log_time FROM `pms_manual_entry` WHERE `emp_id`=" . $userid .
            " and shift_date ='" . $punchtime . "' and date(date) between '" . $punchtime . "' and  '" . $punchtime . "'
         and status = 1";

        $manual_punches = Yii::app()->db->createCommand($sql_manual_punch)->queryAll();



        $query = "SELECT *,date as log_time FROM `pms_shift_entry` WHERE `emp_id`= " . $userid . "  and
        date(`shift_date`)='" . $punchtime . "'  and status = 1 ";
        $shift_punches = Yii::app()->db->createCommand($query)->queryAll();

        $addedpunches = array_merge($manual_punches, $shift_punches);

        $allpunches = array_merge($directpunches, $addedpunches);


        $punches = array();
        if (!empty($allpunches)) {
            foreach ($allpunches as $data) {


                $device_id = (isset($data['device_id'])) ? $data['device_id'] : "";
                $dname = "Photo Punch";

                $ignoredpunch = Yii::app()->db->createCommand("SELECT * FROM `pms_ignore_punches` WHERE `userid`=" . $userid . " and `date`='" . $data['log_time'] . "' and status = 1 ")->queryRow();
                $shift_entry = Yii::app()->db->createCommand("SELECT * FROM `pms_shift_entry` WHERE `emp_id`=" . $userid . " and  date ='" . $data['log_time'] . "' and status = 1 and type=1 ")->queryRow();

                $msg = '';
                if (!empty($ignoredpunch) || !empty($shift_entry)) {
                    $msg = '#a2a2a2';
                    $dname = "Photo punch";
                    $device_id = 0;
                }
                if (isset($data['entry_id'])) {
                    // $msg = 'green';
                    $msg = '';
                    $dname = 'Manual';
                    $device_id = 1;
                }

                if (isset($data['shiftentry_id'])) {
                    // $msg = 'green';
                    $msg = '';
                    $dname = 'Added to shift';
                    $device_id = 3;
                }

                $punches[strtotime($data['log_time'])] = json_encode(array('logdate' => strtotime($data['log_time']), 'date' => $data['log_time'], 'device_id' => $device_id, 'dname' => $dname, 'ignore' => $msg));
            }
        }

        ksort($punches);


        return $punches;
    }

    public function getShiftPunches($userid, $punchtime, $empid = 0)
    {

        $res = $this->getShiftData($userid, $punchtime);

        if (isset($res['start_time']) && $res['start_time'] != '') {

            if ($empid != 0) {

                $userid = $empid;    //stand by punches
            }
            if (!empty($res['cs_end']) && $res['is_continuous'] == 1) {

                if (strtotime($res['cs_end']) > strtotime($res['shift_end'])) {
                    $cs_end = $res['shift_date'] . " " . $res['cs_end'] . ":00";
                } else {
                    $next = date('Y-m-d', strtotime('+1 day', strtotime($res['shift_date'])));
                    $cs_end = $next . " " . $res['cs_end'] . ":00";
                }

                $sql_direct_punch = "SELECT site_id, site_name, al . device_id, al . empid, al . sqllogid, al . log_time,
            DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did . deviceid, did . accesscard_id
            FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd ON pd . device_id = al . device_id
            INNER JOIN pms_clientsite as cs ON cs . id = pd . site_id  INNER JOIN pms_device_accessids as did
            ON did . deviceid = al . device_id and empid = did . accesscard_id where log_time between '" . $res['start_time'] . "'
            and '" . $cs_end . "' and userid = " . $userid;
            } else {
                $sql_direct_punch = "SELECT site_id, site_name, al . device_id, al . empid, al . sqllogid, al . log_time,
            DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did . deviceid, did . accesscard_id
            FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd ON pd . device_id = al . device_id
            INNER JOIN pms_clientsite as cs ON cs . id = pd . site_id  INNER JOIN pms_device_accessids as did
            ON did . deviceid = al . device_id and empid = did . accesscard_id where log_time between '" . $res['start_time'] . "'
            and '" . $res['end_time'] . "' and userid = " . $userid;
            }


            $directpunches = Yii::app()->db->createCommand($sql_direct_punch)->queryAll();
            $sql_manual_punch = "SELECT *,date as log_time FROM `pms_manual_entry` WHERE `emp_id`=" . $userid .
                " and shift_date ='" . $punchtime . "' and date between '" . $res['start_time'] . "' and  '" . $res['end_time'] . "'
         and status = 1";
            $manual_punches = Yii::app()->db->createCommand($sql_manual_punch)->queryAll();


            // $shift_punches = Yii::app()->db->createCommand("SELECT *,date as log_time FROM `pms_shift_entry` WHERE `emp_id`= " . $userid . "  and date(`shift_date`)='".$punchtime."' and date between '".$res['start_time']."' and  '".$res['end_time']."' and status = 1 ")->queryAll();
            $query = "SELECT *,date as log_time FROM `pms_shift_entry` WHERE `emp_id`= " . $userid . "  and
        date(`shift_date`)='" . $punchtime . "'  and status = 1 ";
            $shift_punches = Yii::app()->db->createCommand($query)->queryAll();
            // echo '<pre>';print_r($directpunches);exit;
            $addedpunches = array_merge($manual_punches, $shift_punches);

            $allpunches = array_merge($directpunches, $addedpunches);

            $punches = array();
            if (!empty($allpunches)) {
                foreach ($allpunches as $data) {

                    $device_id = (isset($data['device_id'])) ? $data['device_id'] : "";
                    $dname = (isset($data['device_id'])) ? $this->devices[$data['device_id']] : "Photo Punch";

                    $ignoredpunch = Yii::app()->db->createCommand("SELECT * FROM `pms_ignore_punches` WHERE `userid`=" . $userid . " and `date`='" . $data['log_time'] . "' and status = 1 ")->queryRow();
                    $shift_entry = Yii::app()->db->createCommand("SELECT * FROM `pms_shift_entry` WHERE `emp_id`=" . $userid . " and  date ='" . $data['log_time'] . "' and status = 1 and type=1 ")->queryRow();

                    $msg = '';
                    if (!empty($ignoredpunch) || !empty($shift_entry)) {
                        $msg = '#a2a2a2';
                        $dname = isset($this->devices[$device_id]) ? $this->devices[$device_id] : "";
                        $device_id = 0;
                    }
                    if (isset($data['entry_id'])) {
                        $msg = 'green';
                        $dname = 'Manual';
                        $device_id = 1;
                    }

                    if (isset($data['shiftentry_id'])) {
                        $msg = 'green';
                        $dname = 'Added to shift';
                        $device_id = 3;
                    }

                    $punches[strtotime($data['log_time'])] = array('logdate' => strtotime($data['log_time']), 'date' => $data['log_time'], 'device_id' => $device_id, 'dname' => $dname, 'ignore' => $msg);
                }
            }
            ksort($punches);


            return $punches;
        }
    }

    public function getOutsidePunches($userid, $punchtime)
    {


        $allpunches = array();
        $res = $this->getShiftData($userid, $punchtime);

        //echo '<pre>';print_r($res);exit;



        if (isset($res['start_time']) && $res['start_time'] != '') {

            $dt = new DateTime($res['end_time']);
            $shift_end_date = $dt->format('Y-m-d');
            $dt_nxt = new DateTime($res['nxt_shift_start']);

            $shift_date = $res['shift_date'];

            /*             * ***************************** */
            if (new DateTime($shift_date) == new DateTime($shift_end_date)) {


                $next_day = date('Y-m-d', strtotime('+1 day', strtotime($res['shift_date'])));
                if (isset($res['nxt_shift_start']) && $res['nxt_shift_start'] != '') {
                    $next_shift_date = new DateTime($res['nxt_shift_start']);
                    $next_shift_date = $next_shift_date->format('Y-m-d');
                } else {
                    $next_shift_date = date('Y-m-d', strtotime('+1 day', strtotime($res['shift_date'])));
                }

                $allpunches = $this->testot($res, $next_shift_date, $next_day, $shift_date, $userid);
            } else {


                if (isset($res['nxt_shift_start']) && $res['nxt_shift_start'] != '') {
                    $next_shift_date = new DateTime($res['nxt_shift_start']);
                    $next_shift_date = $next_shift_date->format('Y-m-d');

                    if ($next_shift_date == $shift_end_date) {
                        $next_day = $next_shift_date;
                    } else {
                        $next_day = date('Y-m-d', strtotime('+1 day', strtotime($next_shift_date)));
                    }
                } else {
                    $next_day = date('Y-m-d', strtotime('+1 day', strtotime($shift_end_date)));
                    $next_shift_date = date('Y-m-d', strtotime('+1 day', strtotime($res['shift_date'])));
                }
                $allpunches = $this->twodateot($res, $next_shift_date, $next_day, $shift_date, $userid);
            }


            $sql = "SELECT *,date as log_time FROM `pms_manual_entry` WHERE `emp_id`=" . $userid . "
            and shift_date ='" . $punchtime . "' and date between '" . $res['end_time'] . "' and  '" . $res['nxt_shift_start'] . "'
            and status = 1";

            $manual_punches = Yii::app()->db->createCommand($sql)->queryAll();

            $sql = "SELECT *,date as log_time FROM `pms_shift_entry` WHERE `emp_id`= " . $userid .
                "  and date(`shift_date`)='" . $punchtime . "' and date between '" . $res['end_time'] . "' and
            '" . $res['nxt_shift_start'] . "' and status = 1 ";

            $shift_punches = Yii::app()->db->createCommand($sql)->queryAll();


            $addedpunches = array_merge($manual_punches, $shift_punches);

            $allpunches = array_merge($allpunches, $addedpunches);

            $punches = array();
            if (!empty($allpunches)) {
                foreach ($allpunches as $data) {
                    $punches[] = $data['log_time'];
                }
            }



            return $punches;
        }
    }

    public function twodateot($res, $next_shift_date, $next_day, $shift_date, $userid)
    {
        //before OT
        if (!empty($res['prev_shift_end'])) {

            if (new DateTime($res['prev_shift_end']) > new DateTime($shift_date . ' 06:00:00')) {

                $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
            DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
            did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
            ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
            pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
            between '" . $res['prev_shift_end'] . "' and '" . $res['start_time'] . "' and userid = " . $userid;

                $before_punch = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
            } else {
                $start = $shift_date . ' 05:35:00';
                $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
            DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
            did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
            ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
            pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
            between '" . $start . "' and '" . $res['start_time'] . "' and userid = " . $userid;
                $before_punch = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
            }
        } else {
            $start = $shift_date . ' 05:35:00';
            $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
        DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
        did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
        ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
        pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
        between '" . $start . "' and '" . $res['start_time'] . "' and userid = " . $userid;
            $before_punch = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
        }

        if (count($before_punch) % 2 == 1) {
            $before_punch[]['log_time'] = $res['start_time'];
        }

        // after OT

        if (isset($res['nxt_shift_start']) && !empty($res['nxt_shift_start'])) {


            if ($next_shift_date == $next_day) {
                // echo '<pre>';print_r($res);exit;
                if (new DateTime($res['nxt_shift_start']) <= new DateTime($next_day . ' 06:00:00') && isset($res['nxt_shift_start'])) {

                    $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
                DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
                did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
                ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
                pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
                between '" . $res['end_time'] . "' and '" . $res['nxt_shift_start'] . "' and userid = " . $userid;

                    $out_p = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
                    // echo '<pre>';print_r($sql_all_punch);exit;
                } else {

                    $end = $next_day . ' 05:35:00';
                    $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
                DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
                did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
                ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
                pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
                between '" . $res['end_time'] . "' and '" . $end . "' and userid = " . $userid;
                    $out_p = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
                }
            } else {

                $end = $next_day . ' 05:35:00';
                $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
            DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
            did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
            ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
            pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
            between '" . $res['end_time'] . "' and '" . $end . "' and userid = " . $userid;
                $out_p = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
            }
        }
        // echo '<pre>';print_r($out_p);exit;
        if (!empty($out_p)) {
            if (count($out_p) % 2 == 1) {

                $out_p[count($out_p)]['log_time'] = $res['end_time'];
            }
            $before_punch = array_merge($before_punch, $out_p);
        }
        //  echo '<pre>';print_r($before_punch);exit;

        return $before_punch;
    }

    public function testot($res, $next_shift_date, $next_day, $shift_date, $userid)
    {

        $allpunches = array();

        if (new DateTime($res['start_time']) > new DateTime($shift_date . ' 05:35:00')) {


            $start = $shift_date . ' 05:35:00';
            // $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
            // DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
            // did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
            // ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
            // pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
            // between '" . $start . "' and '" .$res['start_time']. "' and userid = ".$userid;
            // $allpunches = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
            // echo '<pre>';print_r($allpunches);exit;
            // if(count($allpunches)%2 == 1 && count($allpunches) == 1){
            //     $allpunches[]['log_time'] = $res['start_time'];
            //
            // } elseif(count($allpunches)%2 == 1){
            //
            //
            //     // $allpunches[count($allpunches)]['log_time'] = $res['start_time'];
            // }


            /*             * *******out punches */
            if (isset($next_shift_date)) {


                if ($next_shift_date == $next_day) {



                    if (new DateTime($res['nxt_shift_start']) < new DateTime($next_day . ' 06:00:00')) {

                        $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
                    DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
                    did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
                    ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
                    pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
                    between '" . $res['end_time'] . "' and '" . $res['nxt_shift_start'] . "' and userid = " . $userid;
                        $out_p = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
                    } else {


                        $end = $next_day . ' 05:35:00';
                        $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
                    DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
                    did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
                    ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
                    pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
                    between '" . $res['shift_end_without_grace'] . "' and '" . $end . "' and userid = " . $userid;
                        $out_p = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
                        // echo '<pre>';print_r($out_p);exit;
                    }
                } else {


                    $end = $next_day . ' 05:35:00';
                    $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
                DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
                did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
                ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
                pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
                between '" . $res['end_time'] . "' and '" . $end . "' and userid = " . $userid;
                    $out_p = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
                }
            } else {


                $end = $next_day . ' 05:35:00';
                $sql_all_punch = "SELECT site_id, site_name, al.device_id, al.empid, al.sqllogid, al.log_time,
            DATE_FORMAT(al . log_time, '%Y-%m-%d') as logdate, pd . device_name, did . userid, did.deviceid,
            did.accesscard_id FROM pms_punch_log as al INNER JOIN pms_punching_devices as pd
            ON pd . device_id = al.device_id INNER JOIN pms_clientsite as cs ON cs.id = pd.site_id  INNER JOIN
            pms_device_accessids as did ON did.deviceid = al.device_id and empid = did.accesscard_id where log_time
            between '" . $res['end_time'] . "' and '" . $end . "' and userid = " . $userid;
                $out_p = Yii::app()->db->createCommand($sql_all_punch)->queryAll();
            }


            if (!empty($out_p)) {
                // if(count($out_p)%2 == 1){
                //
                //     $out_p[count($out_p)]['log_time'] = $res['end_time'];
                //
                // }

                $out_p[count($out_p)]['log_time'] = $res['cur_shift_end'];
                $allpunches = array_merge($allpunches, $out_p);
            }
        }
        return $allpunches;
    }

    public function getbreakPunches($userid, $punchtime, $empid, $shift_id)
    {

        $punchdate = date('Y-m-d', strtotime($punchtime));
        if ($shift_id != '') {

            $shiftdetails = Yii::app()->db->createCommand("SELECT * FROM `pms_shiftbreak_time` WHERE `shift_id`=" . $shift_id)->queryAll();
            $cond = '';
            foreach ($shiftdetails as $data) {

                $cond .= " ( log_time between  '$punchdate " . $data['time_start'] . "' and '$punchdate " . $data['time_end'] . "' and empid = " . $empid . " ) or ";
            }
            if (!empty($cond)) {

                $cond = preg_replace('/\W\w+\s*(\W*)$/', '$1', $cond);
                $allpunches = Yii::app()->db->createCommand("SELECT * FROM `pms_punch_log` WHERE " . $cond)->queryAll();
                $punches = array();
                if (!empty($allpunches)) {
                    foreach ($allpunches as $data) {
                        $punches[strtotime($data['log_time'])] = $data['log_time'];
                    }
                }
                return $punches;
            }
        }
    }

    public function getManualEntry($userid, $punchtime)
    {
        $punchdate = date('Y-m-d', strtotime($punchtime));
        $sql = "SELECT * FROM `pms_manual_entry` "
            . "WHERE `emp_id`=" . $userid . " and  "
            . "shift_date ='" . $punchdate . "' and status = 1";
        $manual_punches = Yii::app()->db->createCommand($sql)->queryAll();
        $sql = "SELECT * FROM `pms_shift_entry` "
            . "WHERE `emp_id`= " . $userid . "  and "
            . "date(`shift_date`)='" . $punchdate . "' and status = 1 ";
        $shift_punches = Yii::app()->db->createCommand($sql)->queryAll();
        $allpunches = array_merge($manual_punches, $shift_punches);
        $punches = array();
        foreach ($allpunches as $data) {
            $punches[strtotime($data['date'])] = $data['date'];
        }

        return $punches;
    }

    public function getIgnoredPunches($userid, $punchtime)
    {
        $punchdate = date('Y-m-d', strtotime($punchtime));
        $sql = "SELECT * FROM `pms_ignore_punches` "
            . "WHERE `userid`=" . $userid . " and "
            . "date(`date`)='" . $punchdate . "' and status = 1 ";

        $allpunches = Yii::app()->db->createCommand($sql)->queryAll();
        $sql = "SELECT * FROM `pms_shift_entry` "
            . "WHERE `emp_id`=" . $userid . " and "
            . "date(date) ='" . $punchdate . "' and status = 1 and type=1 ";

        $shift_entry = Yii::app()->db->createCommand($sql)->queryAll();
        $allpunches = array_merge($allpunches, $shift_entry);

        $punches = array();
        foreach ($allpunches as $data) {
            $punches[strtotime($data['date'])] = $data['date'];
        }
        return $punches;
    }

    public function getpunches($sdate, $tdate)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("log_time> :sdate and log_time< :tdate");
        $criteria->order = "log_time asc";
        $criteria->params = array(':sdate' => $sdate, ':tdate' => $tdate);
        $allpunchitems = Photopunch::model()->findAll($criteria);
        $shift_id = '';

        foreach ($allpunchitems as $punchitem) {
            if (isset($this->empdevid[$punchitem->device_id][$punchitem->empid])) {
                $user_id = $this->empdevid[$punchitem->device_id][$punchitem->empid];
                $logtime_tstamp = strtotime($punchitem->log_time);
                $sql = "SELECT * FROM `pms_ignore_punches` "
                    . "WHERE `userid`=" . $user_id . " and "
                    . "`date`='" . $punchitem->log_time . "' and status = 1 ";

                $ignoredpunch = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($ignoredpunch)) {
                    $this->punchresult[$user_id]['punches'][date('Ymd', $logtime_tstamp)]['day_punch'][$logtime_tstamp] = json_encode(array('devid' => 0, 'logdate' => strtotime($ignoredpunch['date']), 'date' => $punchitem->log_time, 'dname' => $this->devices[$punchitem->device_id]));
                } else {
                    if (isset($this->punchresult[$user_id]) && isset($this->devices[$punchitem->device_id])) {
                        $this->punchresult[$user_id]['punches'][date('Ymd', $logtime_tstamp)]['day_punch'][$logtime_tstamp] = json_encode(array('devid' => $punchitem->device_id, 'logdate' => strtotime($punchitem->log_time), 'date' => $punchitem->log_time, 'dname' => $this->devices[$punchitem->device_id]));
                    }
                }
            }
        }
        $sql = "SELECT *, DATE(log_time) DateOnly "
            . "FROM `pms_photopunch` "
            . "WHERE `log_time` between '" . $sdate . "' and '" . $tdate . "' "
            . "GROUP BY log_time,empid ";
        //    die; 
        $allpunches = Yii::app()->db->createCommand($sql)->queryAll();
        //         echo '<pre>';print_r($allpunches);exit;

        $newuserid = array();
        foreach ($allpunches as $data) {
            extract($data);
            if (isset($this->empdevid[$device_id][$empid])) {
                $newuserid[] = $this->empdevid[$device_id][$empid];
                $user_id = $this->empdevid[$device_id][$empid];
                $sdatenew = strtotime(date('Y-m-d', strtotime($log_time)));
                $punchdate = date('Y-m-d', strtotime($log_time));

                $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['day_punch'] = $this->getDailyPunches($user_id, $punchdate);

                $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['shift_details'] = $this->getShiftData($user_id, $punchdate);
                $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['shift_punch'] = $this->getShiftPunches($user_id, $punchdate);
                $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['outside_punch'] = $this->getOutsidePunches($user_id, $punchdate);
                $shift_id = $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['shift_details']['shift_id'];

                $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['break_punches'] = $this->getbreakPunches($user_id, $punchdate, $user_id, $shift_id);
                $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['manual_entry'] = $this->getmanualentry($user_id, $punchdate);
                $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['ignored_punches'] = $this->getIgnoredPunches($user_id, $punchdate);
                // $this->punchresult[$user_id]['punches'][date('Ymd', $sdatenew)]['interchange'] =
                // $this->getInterchangeuser($user_id, $punchdate);
            }
        }

        // new section only manual entries

        $newuserids = implode(',', $newuserid);

        foreach ($this->employees as $user) {

            if (!in_array($user['userid'], $newuserid)) {
                $this->punchresult[$user['userid']]['punches'][date('Ymd', strtotime($this->pdate))]['shift_details'] = $this->getShiftData($user['userid'], $this->pdate);
                $this->punchresult[$user['userid']]['punches'][date('Ymd', strtotime($this->pdate))]['manual_entry'] = $this->getmanualentry($user['userid'], $this->pdate);
                $this->punchresult[$user['userid']]['punches'][date('Ymd', strtotime($this->pdate))]['day_punch'] = $this->getDailyPunches($user['userid'], $this->pdate);
            }
        }

        $this->getStandbypunches($sdate, $tdate);
        $this->getpunches = true;
    }

    public function calculateResult()
    {
        $finalresult = array();
        $i = 1;
        $pdatetoindex = date('Ymd', strtotime($this->pdate));

        foreach ($this->punchresult as $userid => $res) {

            $indexid = (Yii::app()->user->id == $res['empdetails']['userid'] ? 0 : $i);
            $individual_res = $this->getIndividualResult($userid);

            $finalresult[$indexid] = $individual_res;
            $this->punchresult[$userid]['punches'][$pdatetoindex]['presult'] = $individual_res;
            $i++;
        }
        //        echo '<pre>';
        //        print_r($this->punchresult[156]);
        //        exit;
    }

    public function getIndividualResult($userid)
    {

        $finalres = array();
        $punch_array = array();
        $shift_model = 0;
        $staus = '-';
        $intime = 0;
        $outtime = 0;
        $totlpun = 0;
        $userpunchs = array();
        $manual_entry = array();
        $ignored_entry = array();
        $userpunchset = isset($this->punchresult[$userid]['punches']) ? $this->punchresult[$userid]['punches'] : "";
        if (isset($userpunchset[date('Ymd', strtotime($this->pdate))])) {
            $shift_id__ = $userpunchset[date('Ymd', strtotime($this->pdate))]['shift_details']['shift_id'];
            $shift_model = Employeeshift::model()->findByPK($shift_id__);
        }
        $userpdateitems = (isset($userpunchset[date('Ymd', strtotime($this->pdate))])) ? $userpunchset[date('Ymd', strtotime($this->pdate))] : '';
        $shift_id = isset($userpdateitems['shift_details']['shift_id']) ? $userpdateitems['shift_details']['shift_id'] : "";
        $shift_start = isset($userpdateitems['shift_details']['shift_start']) ? $userpdateitems['shift_details']['shift_start'] : "";
        if (!empty($shift_model) && $shift_model['is_continuous_shift'] == 1) {
            $shift_end = isset($userpdateitems['shift_details']['shift_end']) ? $userpdateitems['shift_details']['shift_end'] : "";
        }


        $shift_end = isset($userpdateitems['shift_details']['shift_end']) ? $userpdateitems['shift_details']['shift_end'] : "";
        if (isset($userpdateitems['shift_punch'])) {
            $userpunchs = $userpdateitems['shift_punch'];
        } else {
            if (isset($userpdateitems['day_punch'])) {
                $userpunchs = $userpdateitems['day_punch'];
            }
        }

        if (isset($userpdateitems['ignored_punches'])) {
            $ignored_entry = $userpdateitems['ignored_punches'];
        }

        //day punches
        //  if(($shift_id=='' && isset($userpdateitems['day_punch'])) || ($shift_id!='' && empty($userpdateitems['shift_punch'])) && $shift_id!=13 && $shift_id!=14 && $shift_id !=15 ){
        //      $userpunchs =  isset($userpdateitems['day_punch'])?$userpdateitems['day_punch']:array();
        //  }

        $userpunchs = isset($userpdateitems['day_punch']) ? $userpdateitems['day_punch'] : array();

        if (empty($userpunchs) && isset($userpdateitems['manual_entry']) && !empty($userpdateitems['manual_entry'])) {

            $userpunchs = isset($userpdateitems['manual_entry']) ? $userpdateitems['manual_entry'] : array();
        }

        //  echo '<pre>';print_r($userpunchs);exit;
        //standby punches

        if (isset($userpdateitems['standby_punches'])) {

            $userpunchs = $userpdateitems['standby_punches'];
        }


        foreach ($userpunchs as $key => $data) {

            if (isset($ignored_entry[$key])) {
                unset($userpunchs[$key]);
            }
        }
        ksort($userpunchs);

        $totlpun = count($userpunchs);
        $finalres['totpunch'] = $totlpun;


        if ($totlpun == 0) {
            $finalres['status'] = '-';
        } else {
            $finalres['status'] = ($totlpun % 2 == 1 ? 'IN' : 'OUT');
        }


        if ($totlpun > 1) {
            $finalres['firstpunch'] = date('Y-m-d h:i:s', key($userpunchs));
            $finalres['firstpunch_full'] = key($userpunchs);
            $punch_array['firstpunch'] = date('Y-m-d H:i:s', key($userpunchs));
            $punch_array['firstpunch_full'] = key($userpunchs);
            $time1 = key($userpunchs);

            $item = 0;
            $pointer = 0;

            foreach ($userpunchs as $punchtime => $deviceid) {
                if ($pointer == 0) {
                    $pointer++;
                    continue;
                }

                $item++;
                if ($item % 2 == 1) {
                    if ($item < $totlpun) {
                        $intime += $punchtime - $time1;
                    }
                } else {
                    $outtime += $punchtime - $time1;
                }
                $time1 = $punchtime;
                //$finalres['lastpunch'] = date('Y-m-d h:i:s', $punchtime);
                $finalres['lastpunch'] = date('Y-m-d H:i:s', $punchtime);
                $finalres['lastpunch_full'] = $punchtime;
                $punch_array['lastpunch'] = date('Y-m-d H:i:s', $punchtime);
                $punch_array['lastpunch_full'] = $punchtime;
            }

            //echo '<pre>';print_r($finalres);exit;
        } else if ($totlpun == 1 && !empty($userpdateitems['shift_punch'])) {
            $finalres['firstpunch'] = date('Y-m-d :i:s a', key($userpunchs));
            $finalres['firstpunch_full'] = key($userpunchs);
            $finalres['lastpunch'] = date('Y-m-d h:i:s a', key($userpunchs));
            $finalres['lastpunch_full'] = key($userpunchs);

            $punch_array['firstpunch'] = date('Y-m-d H:i:s a', key($userpunchs));
            $punch_array['firstpunch_full'] = key($userpunchs);
            $punch_array['lastpunch'] = date('Y-m-d H:i:s a', key($userpunchs));
            $punch_array['lastpunch_full'] = key($userpunchs);
        } else if ($totlpun == 1 && empty($userpdateitems['shift_punch'])) {
            $finalres['firstpunch'] = date('Y-m-d h:i:s a', key($userpunchs));
            $finalres['firstpunch_full'] = key($userpunchs);
            $finalres['lastpunch'] = date('Y-m-d h:i:s a', key($userpunchs));
            $finalres['lastpunch_full'] = key($userpunchs);

            $punch_array['firstpunch'] = date('Y-m-d H:i:s a', key($userpunchs));
            $punch_array['firstpunch_full'] = key($userpunchs);
            $punch_array['lastpunch'] = date('Y-m-d H:i:s a', key($userpunchs));
            $punch_array['lastpunch_full'] = key($userpunchs);
        } else {
            $finalres['firstpunch'] = '-';
            $finalres['firstpunch_full'] = 0;
            $finalres['lastpunch'] = '-';
            $finalres['lastpunch_full'] = 0;

            $punch_array['firstpunch'] = '-';
            $punch_array['firstpunch_full'] = 0;
            $punch_array['lastpunch'] = '-';
            $punch_array['lastpunch_full'] = 0;
        }

        //  echo '<pre>';print_r($finalres);exit;

        if ($totlpun % 2 == 1) {
            $cur_punchdate = date('Y-m-d', $finalres['lastpunch_full']);
            $today_date = date('Y-m-d');
            if ($cur_punchdate == $today_date && $finalres['lastpunch_full'] < strtotime($today_date . " 19:30:00")) {
                $intime += time() - $finalres['lastpunch_full'];
            }
        }
        $finalres['intime'] = $intime;
        $finalres['outtime'] = $outtime;

        $punch_array['intime'] = $intime;
        $punch_array['outtime'] = $outtime;


        $finalres['outside_punch'] = 0;

        $punch_array['outside_punch'] = 0;
        if ($totlpun >= 1) {
            $ottime = $this->newotCalc1($userpdateitems, $intime, $punch_array);
            //  $ottime = $this->newotCalc($userpdateitems, $intime);
            $finalres['outside_punch'] = $ottime;
        }

        $finalres['break_time'] = 0;
        if (isset($userpdateitems['break_punches'])) {
            $break_punchs = $userpdateitems['break_punches'];
            if (!empty($break_punchs)) {
                $shift_id = $userpdateitems['shift_details']['shift_id'];
                $break = $this->Totalbreaktime($userid, $shift_id);
                $finalres['break_time'] = $break['breakused'];
            } else {
                $finalres['break_time'] = 0;
            }
        }

        $finalres['att_type'] = '';
        if ($totlpun != 0) {
            $finalres['att_type'] = $this->getattendance($userid, $finalres['firstpunch'], $finalres['intime'], $shift_start, $finalres['lastpunch']);
        }

        $finalres['late_time'] = 0;
        $fp = strtotime(date('H:i', strtotime($finalres['firstpunch'])));

        if ($shift_start != '' && $totlpun != 0 && $fp > strtotime($shift_start) && $fp < strtotime($shift_end)) {
            $finalres['late_time'] = $fp - strtotime($shift_start);
        } else {
            $finalres['late_time'] = 0;
        }


        //standby
        $finalres['standby_id'] = 0;
        if (isset($userpdateitems['standby_id'])) {
            $finalres['standby_id'] = $userpdateitems['standby_id'];
        }
        if (!isset($userpdateitems['shift_punch']) && isset($userpdateitems['day_punch'])) {
            //$finalres['outside_punch']  =  $finalres['intime'] ;
            // $finalres['outside_punch']  =  0;
        }
        // echo '<pre>';print_r($finalres);exit;
        return $finalres;
    }

    public function getInterchangeuser()
    {
        $sql = "SELECT  change_id,emp1,change_to ,date_from,date_to ,c.status ,
                c.type,concat_ws('',e.first_name,e.last_name) as name1,
                concat_ws('',e2.first_name,e2.last_name) as name2 FROM `pms_shift_changes` c 
                inner join pms_users as e on  e.userid = c.emp1 
                inner join pms_users as e2 on  e2.userid = c.change_to
                where c.type=0 and c.status=1 and '" . $this->pdate . "' between date_from  and date_to";

        $changes = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($changes as $change) {
            $this->interchange[$change['emp1']] = $change['name2'];
            $this->interchange[$change['change_to']] = $change['name1'];
        }

        // echo '<pre>';print_r($this->interchange);exit;
        return true;
    }

    public function newotCalc($items, $intime)
    {

        // echo '<pre>';print_r(gmdate('H:i',32400));exit;

        $ottime = 0;


        $min_ot_value = (isset($items['shift_details']['min_ot_value']) && $items['shift_details']['min_ot_value'] != '') ? $items['shift_details']['min_ot_value'] : 0;
        $max_ot_value = (isset($items['shift_details']['max_ot_value']) && $items['shift_details']['max_ot_value'] != '') ? $items['shift_details']['max_ot_value'] : 0;
        $fixed_ot = (isset($items['shift_details']['fIxed_ot']) && $items['shift_details']['fIxed_ot'] != '') ? $items['shift_details']['fIxed_ot'] : 0;
        $apply_ot_offset = (isset($items['shift_details']['apply_ot_offset']) && $items['shift_details']['apply_ot_offset'] == 1) ? 1 : 0;
        $ot_offset = (isset($items['shift_details']['ot_offset']) && $items['shift_details']['ot_offset'] != '') ? $items['shift_details']['ot_offset'] : 0;
        $min_intime = (isset($items['shift_details']['min_ot_intime']) && $items['shift_details']['min_ot_intime'] != '') ? $items['shift_details']['min_ot_intime'] : 0;
        //$apply_no_ot = (isset($items['shift_details']['no_ot']) && $items['shift_details']['no_ot']==1)?1:0;

        $outtime = 0;
        if ($min_intime != 0) {
            $min_intime = $min_intime * 60 * 60;
            $outtime = $intime - $min_intime;
        }
        // echo $intime;exit;


        if ($outtime > 0) {

            $out_hour = gmdate('G', $outtime);
            $out_min = gmdate('i', $outtime);

            if ($fixed_ot != 0) {
                $fixed_ot = $fixed_ot / 60;
            }

            if ($apply_ot_offset == 1) {
                $ot_offset = $ot_offset;
            }

            if ($min_ot_value != 0) {

                if ($out_hour == 0 && $out_min >= $min_ot_value) {
                    $ottime = $ot_offset + $fixed_ot;
                }
                if ($out_hour > 0 && $out_min >= $min_ot_value) {
                    $ottime = $ot_offset + $out_hour + $fixed_ot;
                }

                if ($out_hour > 0 && $out_min < $min_ot_value) {
                    $ottime = $ot_offset + $out_hour;
                }
            }

            if ($apply_ot_offset == 1) {

                $min_ot_value = $min_ot_value / 60;
                if ($out_hour > $min_ot_value) {
                    $ottime = $ot_offset + $fixed_ot * $out_hour;
                }
            }
        }
        return $ottime;
    }

    public function newotCalc1($items, $intime, $finalres)
    {



        $ottime = 0;


        $min_ot_value = (isset($items['shift_details']['min_ot_value']) && $items['shift_details']['min_ot_value'] != '') ? $items['shift_details']['min_ot_value'] : 0;
        $max_ot_value = (isset($items['shift_details']['max_ot_value']) && $items['shift_details']['max_ot_value'] != '') ? $items['shift_details']['max_ot_value'] : 0;
        $fixed_ot = (isset($items['shift_details']['fIxed_ot']) && $items['shift_details']['fIxed_ot'] != '') ? $items['shift_details']['fIxed_ot'] : 0;
        $apply_ot_offset = (isset($items['shift_details']['apply_ot_offset']) && $items['shift_details']['apply_ot_offset'] == 1) ? 1 : 0;
        $ot_offset = (isset($items['shift_details']['ot_offset']) && $items['shift_details']['ot_offset'] != '') ? $items['shift_details']['ot_offset'] : 0;
        $min_intime = (isset($items['shift_details']['min_ot_intime']) && $items['shift_details']['min_ot_intime'] != '') ? $items['shift_details']['min_ot_intime'] : 0;
        //$apply_no_ot = (isset($items['shift_details']['no_ot']) && $items['shift_details']['no_ot']==1)?1:0;
        $gracetime_before = (isset($items['shift_details']['grace_period_before']) && $items['shift_details']['grace_period_before'] != '') ? $items['shift_details']['grace_period_before'] : 0;
        $gracetime_after = (isset($items['shift_details']['grace_period_after']) && $items['shift_details']['grace_period_after'] != '') ? $items['shift_details']['grace_period_after'] : 0;

        $shift_start = (isset($items['shift_details']['shift_start']) && $items['shift_details']['shift_start'] != '') ? $items['shift_details']['shift_start'] : 0;
        $shift_end = (isset($items['shift_details']['shift_end']) && $items['shift_details']['shift_end'] != '') ? $items['shift_details']['shift_end'] : 0;


        $first_punch = $finalres['firstpunch'];
        $last_punch = $finalres['lastpunch'];
        $first_punch = strtotime($first_punch);
        $last_punch = strtotime($last_punch);
        $first_punch_time = date('H:i:s', $first_punch);
        $last_punch_time = date('H:i:s', $last_punch);

        $first_punch1 = Date("H:i:s", $first_punch);
        $last_punch1 = Date("H:i:s", $last_punch);

        $a = strtotime($shift_start);
        $shift_start1 = date("H:i:s", $a);

        // return $shift_start1;

        if (strtotime($first_punch_time) < strtotime($shift_start1)) {

            $first_punch_consider_ot = strtotime($shift_start) - ($gracetime_before * 60 * 60);

            if (strtotime($first_punch_time) <= strtotime(date("H:i:s", $first_punch_consider_ot))) {
                $first_punch1 = $first_punch_time;
                //  return $first_punch1;
            } else {

                $a = strtotime($shift_start);
                $first_punch1 = Date("H:i:s", $a);
                //return $first_punch;
            }

            //    return $first_punch1; 
        }

        // return $first_punch;
        $a = strtotime($shift_end);
        $shift_end1 = date("H:i:s", $a);


        if (strtotime($last_punch_time) > strtotime($shift_end1)) {
            $last_punch_consider_ot = strtotime($shift_end) + ($gracetime_after * 60 * 60);


            if (strtotime($last_punch_time) >= strtotime(date("H:i:s", $last_punch_consider_ot))) {
                $last_punch1 = $last_punch_time;
            } else {
                $a = strtotime($shift_end);
                $last_punch1 = date("H:i:s", $a);
            }
            // return $last_punch1;
        }

        $last_punch = new DateTime(($last_punch1));
        $first_punch = new DateTime(($first_punch1));

        $intime = $last_punch->diff($first_punch)->format("%h:%i:%s");

        $total_in = $intime;


        $total_arry = explode(":", $total_in);
        $hours = $total_arry[0] * 60 * 60;
        $minutes = $total_arry[1] * 60;
        $seconds = $total_arry[2];
        $total_in_time = $hours + $minutes + $seconds;
        $intime = $total_in_time;
        $outtime = 0;
        if ($min_intime != 0) {
            $min_intime = $min_intime * 60 * 60;
            $outtime = $intime - $min_intime;
        }

        //reduse outtime






        if ($outtime > 0) {

            $out_hour = gmdate('G', $outtime);
            $out_min = gmdate('i', $outtime);

            if ($fixed_ot != 0) {
                $fixed_ot = $fixed_ot / 60;
            }

            if ($apply_ot_offset == 1) {
                $ot_offset = $ot_offset;
            }

            if ($min_ot_value != 0) {

                if ($out_hour == 0 && $out_min >= $min_ot_value) {
                    $ottime = $ot_offset + $fixed_ot;
                }
                if ($out_hour > 0 && $out_min >= $min_ot_value) {
                    $ottime = $ot_offset + $out_hour + $fixed_ot;
                }

                if ($out_hour > 0 && $out_min < $min_ot_value) {
                    $ottime = $ot_offset + $out_hour;
                }
            }

            if ($apply_ot_offset == 1) {

                $min_ot_value = $min_ot_value / 60;
                if ($out_hour > $min_ot_value) {
                    $ottime = $ot_offset + $fixed_ot * $out_hour;
                }
            }
        }
        return $ottime;
    }

    public function inoutcalc($rawpunches)
    {

        // ksort($rawpunches);
        // echo '<pre>';print_r($rawpunches);exit;
        $punches = array();
        foreach ($rawpunches as $key => $val) {
            $punches[strtotime($val)] = $val;
        }
        ksort($punches);
        // echo '<pre>';print_r($punches);exit;
        $punches = array_values($punches);

        $date = substr($punches[0], 0, 10);

        $todaydate = date("Y-m-d");
        $fp = '';
        $lp = '';
        $in = 0;
        $out = 0;
        $tot = 0;
        $totpun = count($punches);

        if ($totpun > 1) {
            $fp = $punches[0];

            $lp = $punches[$totpun - 1];
            for ($i = 1; $i < count($punches); $i++) {
                $prev_punch = $i - 1;
                $curpunch = $i;
                $punchdiff = strtotime($punches[$i]) - strtotime($punches[$i - 1]);
                $tot += $punchdiff;

                if ($prev_punch % 2 == 0) {
                    $in += $punchdiff;
                } else {
                    $out += $punchdiff;
                }
            }
        } else {

            $fp = $lp = $punches[0];
        }

        $res = array(
            'in' => $in,
            //'totpun' => $totpun,
            // 'out' => $out,
            // 'tot' => $tot,
            // 'fp' => $fp,
            // 'lp' => $lp,
        );


        return $res;
    }

    public function Totalbreaktime($userid, $shift_id)
    {

        $punchdate = $this->pdate;

        $shiftdetails = Yii::app()->db->createCommand("SELECT * FROM `pms_shiftbreak_time` WHERE `shift_id`=" . $shift_id)->queryAll();


        $cond = array();
        $tot_break = 0;
        $result = array();
        $punchtime = array();
        foreach ($shiftdetails as $data) {
            $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $data['break_time']);
            sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
            $time_seconds = $hours * 60 + $seconds;
            $tot_break += $time_seconds;
            $cond = " ( log_time between  '$punchdate " . $data['time_start'] . "' and '$punchdate " . $data['time_end'] . "' and userid = " . $userid . " ) ";


            if (!empty($cond)) {

                $sql = "SELECT site_id, site_name,al.device_id, al.empid, al.sqllogid, al.log_time,
            DATE_FORMAT(al.log_time, '%Y-%m-%d') AS logdate, pd.device_name, did.userid, did.deviceid,
            did.accesscard_id FROM  pms_punch_log AS al INNER JOIN  pms_punching_devices AS pd ON
            pd.device_id = al.device_id INNER JOIN pms_clientsite AS cs ON  cs.id = pd.site_id
            INNER JOIN pms_device_accessids AS did ON  did.deviceid = al.device_id AND empid = did.accesscard_id where " . $cond;
                $logdetails = Yii::app()->db->createCommand($sql)->queryAll();

                $availpunch = array();
                foreach ($logdetails as $key => $prec) {

                    if (isset($prec['site_name'])) {
                        $site_name = $prec['site_name'];
                    } else {
                        $site_name = 'M';
                    }
                    $availpunch[] = $prec['log_time'];
                    $punchtime[strtotime($prec['log_time'])] = '(' . (isset($entry_id) ? 'M' : $site_name) . ') ' . substr($prec['log_time'], -8);
                }
                if (!empty($availpunch)) {
                    $result1 = $this->inoutcalc($availpunch, '');
                    $result[] = array('result' => $result1, 'punches' => $punchtime, 'tot_break' => $tot_break);
                }
            }
        }


        $tot_break = 0;
        $intime = 0;

        if (!empty($result)) {
            foreach ($result as $dat) {
                $tot_break += $dat['tot_break'];
                $intime += $dat['result']['in'];
            }
        }
        $resultnew = array('breakused' => $intime);
        return $resultnew;
    }

    public function getStandbyPunches($sdate, $tdate)
    {


        $begin = new DateTime($sdate);
        $end = new DateTime($tdate);
        $end = $end->modify('+1 day');
        $interval = new DateInterval('P1D');
        $period = new DatePeriod($begin, $interval, $end);

        $arr = array();

        foreach ($period as $key => $date) {

            $punchdate = $date->format('Y-m-d');
            $arr[] = $punchdate;
            $changes = Yii::app()->db->createCommand("SELECT employee_code,shift_starts,shift_ends,grace_period,grace_period_before,grace_period_after,sh.shift_id,ch.*,concat_ws(' ',emp.first_name,emp.last_name) as fulname FROM `pms_shift_changes` as ch left join pms_users as emp on emp.userid = ch.change_to left join pms_shift_assign as sh on ( sh.user_id = ch.emp1 and att_date ='$punchdate') left join pms_employee_shift as em on em.shift_id= sh.shift_id  WHERE ( (`date_from` = '$punchdate' and `date_to` = '$punchdate') or (`date_from` <= '$punchdate' and `date_to` >= '$punchdate' ) )  and ch.type= 1  and ch.status= 1 and shift_starts is not null ")->queryAll();


            foreach ($changes as $data) {
                if (isset($this->punchresult[$data['emp1']]['punches'])) {
                    $res = $this->getShiftPunches($data['emp1'], $punchdate, $data['change_to']);
                    $this->punchresult[$data['emp1']]['punches'][date('Ymd', strtotime($punchdate))]['standby_id'] = $data['change_to'];
                    $this->punchresult[$data['emp1']]['punches'][date('Ymd', strtotime($punchdate))]['standby_punches'] = $res;
                }
            }
        }
    }

    public function ipStore()
    {

        if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {

            $localip = $_COOKIE['localIP'];

            $pos = strpos($localip, 'perhaps ');
            if ($pos !== false) {
                $localip = trim(substr($localip, $pos), ' or perhaps ');
            }

            if ($pos !== false) {
                $localip = substr($_COOKIE['localIP'], $pos);
            } else {
                //echo "The string '$findme' was not found in the string '$mystring'";
            }

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
            // print_r($localipparsed);
            $sql = 'update pms_users set reg_ip="' . (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]) . '" where userid=' . intval(Yii::app()->user->id);
            Yii::app()->db->createCommand($sql)->query();

            Yii::app()->user->setState('reg_ip', (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]));
        }

        $regip = '00';

        // echo $_COOKIE['localIP'];
        if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {
            //echo $_COOKIE['localIP'];

            $localip = $_COOKIE['localIP'];

            $pos = strpos($localip, 'perhaps ');
            if ($pos !== false) {
                $localip = trim(substr($localip, $pos), 'perhaps ');
            }

            if ($pos !== false) {
                $localip = substr($_COOKIE['localIP'], $pos);
            } else {
                //echo "The string '$findme' was not found in the string '$mystring'";
            }

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
            //  print_r($localipparsed);

            Yii::app()->db->createCommand('update pms_users set reg_ip="' . (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]) . '" where userid=' . intval(Yii::app()->user->id))->query();
            Yii::app()->user->setState('reg_ip', (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]));
            return $regip = Yii::app()->user->reg_ip;
            //die();
        }

        if (isset($_COOKIE['localIP'])) {

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $matches);
            //  print_r($matches);

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
            //  print_r($localipparsed);

            $regip = (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]);
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
    }

    public function actionPunchresult()
    {
        $tottimeentry = 1;
        if (Yii::app()->user->role == 3) {

            $sql = "select *,date_format(log_time,'%y-%m-%d') as date from pms_punch_log inner join pms_device_accessids as da on da.accesscard_id=empid where da.userid=" . Yii::app()->user->id . " and log_time<curdate() order by log_time desc limit 20";
            $entry = Yii::app()->db->createCommand($sql)->queryRow();
            // echo '<pre>';
            $timeentrycount = 0;

            $sql2 = "select count(*) as tot  from pms_time_entry where entry_date='" . $entry['date'] . "' and user_id=" . Yii::app()->user->id;
            $tottimeentry = Yii::app()->db->createCommand($sql2)->queryScalar();
        }
        //        echo '</pre>';
        //        die;




        $this->ipStore();
        $regip = '00';

        // echo $_COOKIE['localIP'];
        if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {
            //echo $_COOKIE['localIP'];

            $localip = $_COOKIE['localIP'];

            $pos = strpos($localip, 'perhaps ');
            if ($pos !== false) {
                $localip = trim(substr($localip, $pos), 'perhaps ');
            }

            if ($pos !== false) {
                $localip = substr($_COOKIE['localIP'], $pos);
            } else {
                //echo "The string '$findme' was not found in the string '$mystring'";
            }

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
            //  print_r($localipparsed);

            Yii::app()->db->createCommand('update pms_users set reg_ip="' . (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]) . '" where userid=' . intval(Yii::app()->user->id))->query();
            Yii::app()->user->setState('reg_ip', (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]));
            $regip = Yii::app()->user->reg_ip;
            //die();
        }

        if (isset($_COOKIE['localIP'])) {

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $matches);
            //  print_r($matches);

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
            //  print_r($localipparsed);

            $regip = (!isset($localipparsed[0]) ? "0.0.0.0" : $localipparsed[0]);
        }

        // $this->layout = '//layouts/gebomain';
        $model = new Accesslog();

        //die("<center><h1>Under Maintenance</h1></center>");

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        //echo date("Y-m-d");
        //echo date("Y-m-d H:i:s");
        $punchdate = strtotime(date("Y-m-d")) + ($days * 60 * 60 * 24);

        //  $predate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24));
        //$nextdate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + (($days + 1) * 60 * 60 * 24));


        if (isset($_REQUEST['logsearch']) && !empty($_REQUEST['logdate'])) {


            $date2 = date_create($_REQUEST['logdate']);
            $date1 = date_create(date('Y-m-d'));
            $diff = date_diff($date1, $date2);

            $this->redirect(array('accesslog/punchlog&days=-' . $diff->days));
            die;

            $format = 'Y-m-d';
            $logdate = $_REQUEST['logdate'];
            $usersTS = strtotime($logdate);
            $logdatenxt = date($format, strtotime('+1 day', $usersTS));

            $predate = $logdate;
            $nextdate = $logdatenxt;
            // $predate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24));
            // $nextdate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + (($days + 1) * 60 * 60 * 24));
        } else {
            $predate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24));
            $nextdate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + (($days + 1) * 60 * 60 * 24));
        }


        //die();
        // $datewithtime = date("Y-m-d H:i:s", $system_Date);
        //echo "<br />";

        $datetimesettings = array();
        $datetimesettings['shift_time_starts'] = "07:00:00";
        $datetimesettings['shift_end'] = "05:00:00";

        $datetimesettings['date'] = $predate;

        $datetimesettings['nextday'] = $nextdate;

        $datetimesettings['in_late'] = "$predate 09:30:00";
        $datetimesettings['out_early'] = "$predate 16:00:00";
        $datetimesettings['shift_ends'] = "$predate 23:59:00";

        $datetimesettings['late_halfday'] = "11:00:01"; //First punch Half day
        $datetimesettings['ee_halfday'] = "16:00:00";  //Early Exit Half day

        extract($datetimesettings);

        $excludeemp = '';
        if (yii::app()->user->role != 1) {
            // $excludeemp = ' and empid not in (4,5,27) ';
        }

        $sql = "SELECT al.*, u.userid,concat_ws(' ',first_name,last_name) as full_name, u.status,reg_ip, accesscard_id,reg_ip, "
            . "if(userid='" . Yii::app()->user->id . "',1,0) AS matchip  FROM (SELECT logid ,sqllogid ,empid , log_time, device_id "
            . "FROM pms_punch_log WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
            . " AND  (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end' ) $excludeemp ) AS al "
            . "RIGHT JOIN pms_users AS u ON empid=accesscard_id WHERE u.status=0  &&  accesscard_id!=''  GROUP BY userid, log_time ORDER BY matchip DESC, "
            . "full_name ASC,userid ASC,log_time ASC ";

        //        echo $sql = "SELECT  logid ,sqllogid ,empid , log_time,u.userid,concat_ws(' ',first_name,last_name) as full_name, "
        //                . "u.status,reg_ip, accesscard_id,reg_ip, if(userid='".Yii::app()->user->id."',1,0) as matchip"
        //                . " FROM `pms_punch_log` as al right join pms_users as u on empid=accesscard_id "
        //                . "WHERE (u.status=0 or log_time between '$date $shift_time_starts' and '$nextday $shift_end') "
        //                . " AND  log_time between '$date $shift_time_starts' and '$nextday $shift_end' GROUP BY userid, log_time order by matchip desc, full_name asc,userid asc,log_time asc";

        $logdetails = Yii::app()->db->createCommand($sql);
        $logemp = $logdetails->queryAll();

            //    echo '<pre>';
            //    print_r($logemp);
            //    echo '</pre>';
            //    die();
        

        $difflog = array();

        $inout = array(0, 0);
        $result_log = array();

        $allpunch = array();

        $device = array(18 => '\P\l\a\c\e2', '14' => ' \P\l\a\c\e1');

        foreach ($logemp as $k => $row) {
            //            echo '<br />';
            $empid = $row['accesscard_id'];
            $result_log[$empid]['outearly'] = 0;
            $till_time = 0;

            if (Yii::app()->user->id == 6) {
                if (!isset($row['device_id'])) {
                    //print_r($row);
                }
            }


            $allpunch["user_" . $empid][] = date(" h:i:s ", strtotime($row['log_time']));
            //            echo '<br />';
            if (!isset($result_log[$empid]['accesscard_id'])) {
                $punch_time = $prev_value = $row['log_time'];
                $i = 1;
                $result_log[$empid]['accesscard_id'] = $row['accesscard_id'];
                $result_log[$empid]['first_punch'] = $punch_time;
                $result_log[$empid]['last_punch'] = $row['log_time'];
                $result_log[$empid]['emp_name'] = $row['full_name'];
                $result_log[$empid]['userid'] = $row['userid'];
                $result_log[$empid]['ip'] = $row['reg_ip'];
                $result_log[$empid]['count'] = $i;
                $result_log[$empid]['reg_ip'] = $row['reg_ip'];
                $result_log[$empid]['myip'] = $row['matchip'];
                $result_log[$empid]['device_id'] = $row['device_id'];

                //                echo $result_log[$empid]['accesscard_id']."=====".$in_late."-----------==".$row['log_time'];
                //                echo '<br />';
                //                die();

                if (strtotime($in_late) < strtotime($row['log_time'])) {
                    $result_log[$empid]['inlate'] = 1;
                }

                $result_log[$empid]['insec'] = strtotime(date("Y-m-d")) - strtotime($row['log_time']);

                if (!isset($logemp[$k + 1]) or ($logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {
                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$empid]['insec'] = $till_time;
                }

                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['outsec'] = 0;
                $result_log[$empid]['outhrs'] = 0;

                $result_log[$empid]['status'] = $i;
                //  echo "<h1>first-IN-$i</h1>";
                $result_log[$empid]['status'] = 1;
                continue;
            }
            $result_log[$empid]['ldevice_id'] = $row['device_id'];
            $result_log[$empid]['last_punch'] = $row['log_time'];

            // print_r($result_log);
            //if(!isset($i)){
            //                die("i is not set");
            //            }
            //

            if ($i % 2 == 1) {
                // echo '<h2>TEST   -OUT-->'.$i.'</h2>';
                $addinsecs = 0;
                if ($i > 1) {
                    $addinsecs = $result_log[$empid]['insec'];
                    // echo '<h3>yyyyy -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                }
                //                else{
                //                    echo '<h3>XXX -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                //                }
                $result_log[$empid]['insec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['status'] = 0;
            } else {

                if (!isset($logemp[$k + 1]) or ($logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {

                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$empid]['insec'] = $result_log[$empid]['insec'] + $till_time;
                }

                //echo '<h2>TTTEST - IN -->'.$i.'</h2>';
                $addinsecs = $result_log[$empid]['outsec'];
                $result_log[$empid]['outsec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['outhrs'] = gmdate('H:i:s', $result_log[$empid]['outsec']);
                $result_log[$empid]['status'] = 1;
            }

            $i++;
            $prev_value = $row['log_time'];
            $result_log[$empid]['count'] = $i;

            if (strtotime($out_early) > strtotime($row['log_time'])) {
                $result_log[$empid]['outearly'] = 1;
            }
        }

        $noentries = 0;
        if (isset($i)) {
            $result_log[$empid]['status'] = $i % 2;
            $noentries = 1;
        }

        $this->render('punchlog_new', array(
            'noentries' => $noentries, 'logresult' => $result_log,
            'allpunch' => $allpunch, 'punchdate' => $predate, 'datetimesettings' => $datetimesettings, 'tottimeentry' => $tottimeentry
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Punchresult;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Punchresult'])) {
            $model->attributes = $_POST['Punchresult'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Punchresult'])) {
            $model->attributes = $_POST['Punchresult'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Punchresult('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Punchresult']))
            $model->attributes = $_GET['Punchresult'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Punchresult::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'punchresult-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function getattendance($userid, $fp, $intime, $shift_start, $lp)
    {


        $half = 0;
        $absent = 0;
        $type = 1;

        $userdata_sql = "SELECT `setting_template` FROM `pms_users` WHERE `userid`=" . $userid;
        $userdata = Yii::app()->db->createCommand($userdata_sql)->queryRow();

        $absent_late = 0;
        $halfday_late = 0;

        if ($userdata['setting_template'] != '') {
            $punch_settings = Yii::app()->db->createCommand("SELECT * FROM `pms_setting_template` WHERE `id`=" . $userdata['setting_template'])->queryRow();
            $absent_below = $punch_settings['absent_below'];
            $halfday_below = $punch_settings['halfday_below'];
            $absent_late = $punch_settings['absent_late'];
            $halfday_late = $punch_settings['halfday_late'];
            $minimum_intime_half_day = $punch_settings['minimum_in_time_half_day'];
            $minimum_in_time_full_day = $punch_settings['minimum_in_time_full_day'];
        } else {
            $punch_settings = Yii::app()->db->createCommand("SELECT * FROM `pms_punch_settings` WHERE `setting_id`=1")->queryRow();
            $absent_below = $punch_settings['absent'];
            $halfday_below = $punch_settings['halfday'];
            $minimum_intime_half_day = $punch_settings['minimum_in_time_half_day'];
            $minimum_in_time_full_day = $punch_settings['minimum_in_time_full_day'];
            $absent_late = 0;
            $halfday_late = 0;
            //  $absent_late = $punch_settings['late_fullday'];
            //$halfday_late = $punch_settings['late_halfday'];
        }




        /*  punch settings intime */

        if (!empty($halfday_below) && $halfday_below != 0) {
            $parts = explode(':', $halfday_below);

            $htime = $parts[0] + floor(($parts[1] / 60) * 100) / 100 . PHP_EOL;

            $half = floatval($htime) * 60 * 60;
        }
        if (!empty($absent_below) && $absent_below != 0) {
            $parts = explode(':', $absent_below);
            $atime = $parts[0] + floor(($parts[1] / 60) * 100) / 100 . PHP_EOL;
            $absent = floatval($atime) * 60 * 60;
        }

        $intime_in_hour = (($intime / 60) / 60);

        $first_punch_time = date("H:i:s", strtotime($fp));


        if ($fp == $lp) {
            $type = 8; // single punch
        } elseif (strtotime($first_punch_time) > strtotime($halfday_below) && $intime_in_hour < $minimum_intime_half_day) {
            $type = 4;
        } elseif (strtotime($first_punch_time) > strtotime($halfday_below)) {
            $type = 3;
        } else if ($intime_in_hour < $minimum_intime_half_day) {
            $type = 4;
        } else if ($intime_in_hour >= $minimum_intime_half_day && $intime_in_hour < $minimum_in_time_full_day) {
            $type = 3;
        } elseif (($intime_in_hour >= $minimum_in_time_full_day)) {

            $type = 1;   // ul fullday leave
        }



        /*  punch settings late */

        if ($shift_start != '') {


            $half = 0;
            $absent = 0;


            if (!empty($halfday_late) && $halfday_late != "") {
                $parts = explode(':', $halfday_late);
                $htime = $parts[0] + floor(($parts[1] / 60) * 100) / 100 . PHP_EOL;
                $half = floatval($htime) * 60 * 60;
            }
            if (!empty($absent_late) && $absent_late != "") {
                $parts = explode(':', $absent_late);
                $atime = $parts[0] + floor(($parts[1] / 60) * 100) / 100 . PHP_EOL;
                $absent = floatval($atime) * 60 * 60;
            }

            $parts = explode(':', date('H:i:s', strtotime($fp)));
            $atime = $parts[0] + floor(($parts[1] / 60) * 100) / 100 . PHP_EOL;
            $firstpunch_time = floatval($atime) * 60 * 60;

            $parts = explode(':', date('H:i:s', strtotime($shift_start)));
            $atime = $parts[0] + floor(($parts[1] / 60) * 100) / 100 . PHP_EOL;
            $shiftstart_time = floatval($atime) * 60 * 60;

            $shift_absent = $shiftstart_time + $absent;
            $shift_half = $shiftstart_time + $half;

            if ($firstpunch_time > $shift_half && $half != 0) {
                $type = 3; // uhl half day leave
            }
            if ($intime > 0) {

                $type = 1;   // ul fullday leave
            }
            if ($intime == 0) {
                $type = 8; // single punch
            }
        }
        return $type;
    }

    public function actionPunchReportDaily($date = '')
    {


        $msg = '';
        if (isset($_GET['cron'])) {
            $msg = 'cron';
        } else {
            $msg = 'attendance';
        }

        $insert = "INSERT INTO `pms_cron`(`id`, `date`, `type`, `function`)
   VALUES (null,'" . date('Y-m-d H:i:s') . "',0,'{$msg}') ";
        Yii::app()->db->createCommand($insert)->query();
        $insert_id = Yii::app()->db->getLastInsertID();
        /* cron check end  */




        if (isset($_GET['date'])) {
            $punchdate = date('Y-m-d', strtotime($_GET['date']));
            if ($punchdate > date('Y-m-d')) {
                $punchdate = date('Y-m-d', strtotime("-1 day"));
            }
        } else {
            $punchdate = date('Y-m-d', strtotime("-1 day"));
        }
        if (!isset($_GET['cron'])) {
            Yii::app()->user->setState('shift_role', 1);
        }

        $site_users = '';
        if (isset(Yii::app()->user->site_userids) && !empty(Yii::app()->user->site_userids)) {
            $site_users = ' and user_id in (' . Yii::app()->user->site_userids . ')';
        }
        // echo '<pre>';print_r(Yii::app()->user->site_userids);exit;
        // echo $punchdate;exit;
        $created_user = 0;
        if (!Yii::app()->user->isGuest) {
            $created_user = Yii::app()->user->id;
        }

        $transaction = Yii::app()->db->beginTransaction();
        // try {
        $sql = "INSERT INTO 
    `pms_manual_gen_attendance_log`
    (`id`, `att_date`, `gen_by`, `gen_date`,
     `gen_success_date`, `gstatus`)
      VALUES (null,'$punchdate','$created_user','" . date('Y-m-d H:i:s') . "',
      '" . date('Y-m-d H:i:s') . "',0)";


        Yii::app()->db->createCommand($sql)->query();


        $numdays = 0;
        $this->pdate = $punchdate;
        $this->numdays = $numdays + 0;
        $this->getDaywiseresult();

        $this->calculateResult();


        $pdateindex = date('Ymd', strtotime($this->pdate));
        $attentry = array();
        $querys = array();
        $attentry_old = array();
        $tot = 0;
        $timeentry = array();
        $otentry = array();
        $old_entries = array();
        $old_otentries = array();
        $gendate = strtotime($punchdate);
        $employee_shift_query = "SELECT shift_id,pt_p,pt_ul,pt_uhl,pt_hp
    FROM pms_employee_shift";
        $newrry = Yii::app()->db->createCommand($employee_shift_query)->queryAll();
        $shiftarry = array();
        foreach ($newrry as $dat) {
            $shiftarry[$dat['shift_id']] = array(
                'pt_p' => $dat['pt_p'],
                'pt_ul' => $dat['pt_ul'],
                'pt_uhl' => $dat['pt_uhl'],
                'pt_hp' => $dat['pt_hp'],
            );
        }


        $delete_atta_flag = 0;
        $comments = 'From Punch record';

        //  echo '<pre>';print_r($this->punchresult);exit;

        foreach ($this->punchresult as $item) {


            $punchres = $item['punches'][$pdateindex];
            extract($item['empdetails']);

            extract($punchres['presult']);



            $shift_id = (isset($punchres['shift_details']['shift_id'])) ? $punchres['shift_details']['shift_id'] : 0;
            $standby_id = (isset($punchres['standby_id'])) ? $punchres['standby_id'] : 0;

            $tot = $intime + $outtime;

            $querys[] = "(null,'{$punchdate}','{$userid}','{$firstpunch}','{$lastpunch}','{$intime}','{$outtime}','{$tot}','{$outside_punch}','{$late_time}','{$break_time}','{$totpunch}','{$shift_id}', '{$standby_id}')";



            // attendance
            if ($totpunch != 0) {

                $delete_atta_flag++;

                $delete_query = "delete from pms_attendance
                    where att_date = '$punchdate' and `att_entry` in (1,8,4,3,6) and type=1 " . $site_users . " ";
                Yii::app()->db->createCommand($delete_query)->execute();

                $attendence_sql = "select * from pms_attendance where att_date = '$punchdate' ";

                $getallattentry = Yii::app()->db->createCommand($attendence_sql)->queryAll();

                $old_attndentry = array();
                foreach ($getallattentry as $eattnd) {
                    $old_attndentry[] = $eattnd['att_date'] . "#" . $eattnd['user_id'];
                    $old_entries[$eattnd['user_id'] . "_" . $gendate] = $eattnd['user_id'];
                }

                if (in_array(($punchdate . "#" . $userid), $old_attndentry) === false) {
                    $attentry[$userid . "_" . $gendate] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$att_type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
                    // $attentry_old[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$att_type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
                }
            }


            // intime  attendance
            if ($totpunch != 0 and !in_array($userid, $old_entries)) {


                Yii::app()->db->createCommand("delete from pms_time_attendance where att_date = '$punchdate' and `att_entry` in (1,8,4,3) and type=0 ")->execute();


                $getallentry = Yii::app()->db->createCommand("select * from pms_time_attendance where att_date = '$punchdate' ")->queryAll();
                $old_entry = array();
                foreach ($getallentry as $attnd) {
                    $old_entry[] = $attnd['att_date'] . "#" . $attnd['user_id'];
                }
                if (in_array(($punchdate . "#" . $userid), $old_entry) == false) {

                    $type = 0;
                    if ($att_type == 1) {
                        $type = (isset($shiftarry[$shift_id]['pt_p']) && $shiftarry[$shift_id]['pt_p'] != '') ? $shiftarry[$shift_id]['pt_p'] : 0;
                    }
                    if ($att_type == 8) {
                        $type = (isset($shiftarry[$shift_id]['pt_ul']) && $shiftarry[$shift_id]['pt_ul'] != '') ? $shiftarry[$shift_id]['pt_ul'] : 0;
                    }
                    if ($att_type == 9) {
                        $type = (isset($shiftarry[$shift_id]['pt_uhl']) && $shiftarry[$shift_id]['pt_uhl'] != '') ? $shiftarry[$shift_id]['pt_uhl'] : 0;
                    }
                    $timeentry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$att_type}','{$type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
                }
            }





            $twop = $this->get2pStatus($shift_id);
            // if($totpunch!=0 && !in_array($userid,$old_entries)){
            if ($twop == 1) {

                $check = $this->checkweeklyoffavilable($userid, $punchdate);
                $sunday_enable_check = $this->sundayEnableCheck($shift_id);
                $is_holiday = $this->checkHoliday($punchdate);
                $type = 0;
                if ($att_type == 1) {
                    $type = 7;
                } elseif ($att_type == 8) {
                    $type = 8;
                } elseif ($att_type == 9) {
                    $type = (isset($shiftarry[$shift_id]['pt_uhl']) && $shiftarry[$shift_id]['pt_uhl'] != '') ? $shiftarry[$shift_id]['pt_uhl'] : 0;
                } elseif ($att_type == 3) {
                    $type = 3;
                } elseif ($sunday_enable_check == 1) {
                    $check_sunday = $this->checkSunday($punchdate);
                    if ($check_sunday == 1) {
                        if ($check >= 5) {
                            $type = 6; // Sunday
                        } else {
                            $type = 4; // UL
                            $comments = 'Weekly off need 5 presents in 6 previous days';
                        }
                    } else {
                        $type = 4;
                    }
                } elseif ($is_holiday == true) {
                    $type = 5;
                } else {
                    $type = 4;
                }

                $attentry[$userid . "_" . $gendate] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
                // $timeentry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
                // $otentry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
            } else {
                $check = $this->checkweeklyoffavilable($userid, $punchdate);
                $sunday_enable_check = $this->sundayEnableCheck($shift_id);
                $is_holiday = $this->checkHoliday($punchdate);
                $check_sunday = $this->checkSunday($punchdate);
                $type = 0;
                if ($att_type == 1) {
                    $type = 1;
                } elseif ($att_type == 8) {
                    $type = 8;
                } elseif ($att_type == 9) {
                    $type = (isset($shiftarry[$shift_id]['pt_uhl']) && $shiftarry[$shift_id]['pt_uhl'] != '') ? $shiftarry[$shift_id]['pt_uhl'] : 0;
                } elseif ($att_type == 3) {
                    $type = 3;
                } elseif ($check_sunday == 1) {

                    if ($sunday_enable_check == 1) {
                        if ($check >= 5) {
                            $type = 6; // Sunday
                        } else {
                            $type = 4; // UL
                            $comments = 'Weekly off need 5 presents in 6 previous days';
                        }
                    } else {
                        $type = 4;
                    }
                } elseif ($is_holiday == true) {
                    $type = 5;
                } else {
                    $type = 4;
                }



                $attentry[$userid . "_" . $gendate] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
                // $timeentry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
                // $otentry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
            }


            if ($shift_id == 13 or $shift_id == 14) {
                if ($shift_id == 13 && $firstpunch != '') {
                    $check = $this->checkweeklyoffavilable($userid, $punchdate);
                    if ($att_type == 1) {
                        $type = 7;
                    } elseif ($att_type == 8) {
                        $type = (isset($shiftarry[$shift_id]['pt_ul']) && $shiftarry[$shift_id]['pt_ul'] != '') ? $shiftarry[$shift_id]['pt_ul'] : 0;
                    } elseif ($att_type == 9) {
                        $type = (isset($shiftarry[$shift_id]['pt_uhl']) && $shiftarry[$shift_id]['pt_uhl'] != '') ? $shiftarry[$shift_id]['pt_uhl'] : 0;
                    } elseif ($att_type == 3) {
                        $type = 3;
                    } elseif ($check >= 5) {
                        $type = 6; // Sunday
                    } else {
                        $type = 4; // UL
                        $comments = 'Weekly off need 5 presents in 6 previous days';
                    }
                } else if ($shift_id == 14) {
                    $type = 5; // holiday
                }

                $attentry[$userid . "_" . $gendate] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";

                $timeentry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";

                $otentry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
            }







            if ($totpunch != 0) {

                $delete_atta_flag++;

                $delete_query = "delete from pms_attendance
                    where att_date = '$punchdate' and `att_entry` in (1,8,4,3,6) and type=1 " . $site_users . " ";
                Yii::app()->db->createCommand($delete_query)->execute();

                $attendence_sql = "select * from pms_attendance where att_date = '$punchdate' ";

                $getallattentry = Yii::app()->db->createCommand($attendence_sql)->queryAll();

                $old_attndentry = array();
                foreach ($getallattentry as $eattnd) {
                    $old_attndentry[] = $eattnd['att_date'] . "#" . $eattnd['user_id'];
                }
                if (in_array(($punchdate . "#" . $userid), $old_attndentry) == false) {
                    //  $attentry[$userid . "_" . $gendate] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$att_type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
                }
            }
            // }




            if ($totpunch != 0) {


                Yii::app()->db->createCommand("delete from pms_ot_attendance where att_date = '$punchdate' and `att_entry` in (1,8,4,3) and type=0 ")->execute();


                $getentry = Yii::app()->db->createCommand("select * from pms_ot_attendance where att_date = '$punchdate' ")->queryAll();

                $old_otentry = array();
                $old_otentries = array();

                foreach ($getentry as $oattnd) {
                    $old_otentry[] = $oattnd['att_date'] . "#" . $oattnd['user_id'];
                    $old_otentries[$oattnd['user_id'] . "_" . $gendate] = $oattnd['user_id'];
                }

                // $outside_time = gmdate('H',$outside_punch);

                $outside_time = $outside_punch;
                $otentry[$userid . "_" . $gendate] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$att_type}','{$outside_time}','{$outside_time}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
            }
        }

        // echo '<pre>';print_r($attentry);exit;


        foreach ($old_entries as $k => $v) {

            if (isset($attentry[$k])) {
                unset($attentry[$k]);
            }
        }

        foreach ($old_otentries as $k => $v) {

            if (isset($otentry[$k])) {
                unset($otentry[$k]);
            }
        }





        if (count($querys)) {
            $del = Yii::app()->db->createCommand("delete from pms_punchreport where logdate = '$punchdate' ")->execute();
            $insreport = Yii::app()->db->createCommand("INSERT INTO `pms_punchreport`(`id`, `logdate`, `resource_id`, `firstpunch`, `lastpunch`, `intime`, `outtime`, `totaltime`, `ot_hours`, `late_hrs`, `break_hrs`, `total_punch`, `shift_id`,`standby_id`) VALUES " . implode(",", $querys))->query();
            $del_ot = Yii::app()->db->createCommand("delete from pms_ot_report where date = '$punchdate' ")->execute();
            //punches to attendance entry
            if (count($attentry)) {





                //    print_r($attentry13); exit;
                Yii::app()->db->createCommand('REPLACE into pms_attendance (att_id,user_id,att_date,comments,att_entry,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $attentry))->query();
                // Yii::app()->db->createCommand('REPLACE into pms_attendance (att_id,user_id,att_date,comments,att_entry,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $attentry13))->query();


                Yii::app()->db->createCommand("INSERT INTO `pms_attendance_reject_log`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  " . implode(",", $attentry) . " ")->query();
            }

            //punches to in time attendance entry
            if (count($timeentry)) {
                Yii::app()->db->createCommand('REPLACE into pms_time_attendance (att_id,user_id,att_date,comments,att_entry,att_time,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $timeentry))->query();
            }
            //punches to ot time attendance entry
            if (count($otentry)) {
                $ot_sql = 'REPLACE into pms_ot_attendance (att_id,user_id,att_date,comments,att_entry,ot_time,system_ot,created_by,
            created_date,modified_date,type) ' . ' values ' . implode(",", $otentry);
                Yii::app()->db->createCommand($ot_sql)->query();
            }
        }

        $sql = "INSERT INTO 
   `pms_manual_gen_attendance_log`
   (`id`, `att_date`, `gen_by`, `gen_date`,
    `gen_success_date`, `gstatus`)
     VALUES (null,'$punchdate',$created_user,'" . date('Y-m-d H:i:s') . "',
     '" . date('Y-m-d H:i:s') . "',1)";

        Yii::app()->db->createCommand($sql)->query();

        // holidays to attendance
        $holidays = Yii::app()->db->createCommand("select date_format(`holiday_date`, '%d-%m-%Y') as 'date' from pms_holidays where `holiday_date` = '" . $punchdate . "' ")->queryAll();
        $attentrylist = array();
        $timeentrylist = array();
        $otentrylist = array();

        foreach ($holidays as $hol) {
            $date = date('Y-m-d', strtotime($hol['date']));
            $type = 5;

            foreach ($this->employees as $key => $value) {
                extract($value);

                $chk_att_entrydates = "select * from pms_attendance where att_date = '$date' ";
                $getallattentry = Yii::app()->db->createCommand($chk_att_entrydates)->queryAll();

                $old_attndentry = array();
                foreach ($getallattentry as $eattnd) {
                    $old_attndentry[] = $eattnd['att_date'] . "#" . $eattnd['user_id'];
                }
                if (in_array(($date . "#" . $userid), $old_attndentry) == false) {

                    $attentrylist[] = "(NULL,'{$userid}','{$date}','{$comments}','{$type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
                }

                //in time attendance

                $getallentry = Yii::app()->db->createCommand("select * from pms_time_attendance where att_date = '$date' ")->queryAll();
                $old_entry = array();
                foreach ($getallentry as $attnd) {
                    $old_entry[] = $attnd['att_date'] . "#" . $attnd['user_id'];
                }
                if (in_array(($date . "#" . $userid), $old_entry) == false) {

                    $timeentrylist[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
                }


                //ot time pms_attendance

                $getentry = Yii::app()->db->createCommand("select * from pms_ot_attendance where att_date = '$date' ")->queryAll();
                $old_otentry = array();
                foreach ($getentry as $oattnd) {
                    $old_otentry[] = $oattnd['att_date'] . "#" . $oattnd['user_id'];
                }
                if (in_array(($date . "#" . $userid), $old_otentry) == false) {
                    $outside_time = $outside_punch;
                    $otentrylist[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
                }
            }
        }



        if (count($attentrylist)) {
            $attreort2 = Yii::app()->db->createCommand('REPLACE into pms_attendance (att_id,user_id,att_date,comments,att_entry,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $attentrylist))->query();
            Yii::app()->db->createCommand("INSERT INTO `pms_attendance_reject_log`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  " . $attentrylist . " ")->query();
        }

        if (count($timeentrylist)) {
            Yii::app()->db->createCommand('REPLACE into pms_time_attendance (att_id,user_id,att_date,comments,att_entry,att_time,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $timeentrylist))->query();
        }
        //punches to ot time attendance entry
        if (count($otentrylist)) {
            Yii::app()->db->createCommand('REPLACE into pms_ot_attendance (att_id,user_id,att_date,comments,att_entry,ot_time,system_ot,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $otentrylist))->query();
        }

        // holidays to attendance end


        /* Sunday to attendance */

        $att_entry = array();
        $timeentrylist = array();
        $otentrylist = array();
        $shiftsassign = Yii::app()->db->createCommand("select *,user_id as userid from pms_shift_assign as sh left join pms_employee_shift as ems on ems.shift_id = sh.shift_id where att_date = '$punchdate' ")->queryAll();

        foreach ($shiftsassign as $key => $value) {

            extract($value);
            if ($shift_id == 13 or $shift_id == 14) {

                if ($shift_id == 13 && $firstpunch != '') {
                    $check = $this->checkweeklyoffavilable($userid, $punchdate);
                    if ($check >= 5) {
                        $type = 6; // weekly off
                    } else {
                        $type = 4;
                        $comments = 'Weekly off need 5 presents in 6 previous days';
                    }
                } else if ($shift_id == 14) {
                    $type = 5; // holiday
                }

                $chk_att_entrydates = "select * from pms_attendance where att_date = '$att_date' ";
                $getallattentry = Yii::app()->db->createCommand($chk_att_entrydates)->queryAll();

                $old_attndentry = array();
                foreach ($getallattentry as $eattnd) {
                    $old_attndentry[] = $eattnd['att_date'] . "#" . $eattnd['user_id'];
                }


                if (in_array(($att_date . "#" . $userid), $old_attndentry) == false) {
                    $att_entry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
                }


                //in time attendance

                $getallentry = Yii::app()->db->createCommand("select * from pms_time_attendance where att_date = '$att_date' ")->queryAll();
                $old_entry = array();
                foreach ($getallentry as $attnd) {
                    $old_entry[] = $attnd['att_date'] . "#" . $attnd['user_id'];
                }
                if (in_array(($att_date . "#" . $userid), $old_entry) == false) {

                    $timeentrylist[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
                }


                //ot time pms_attendance

                $getentry = Yii::app()->db->createCommand("select * from pms_ot_attendance where att_date = '$att_date' ")->queryAll();
                $old_otentry = array();
                foreach ($getentry as $oattnd) {
                    $old_otentry[] = $oattnd['att_date'] . "#" . $oattnd['user_id'];
                }
                if (in_array(($att_date . "#" . $userid), $old_otentry) == false) {
                    $outside_time = $outside_punch;
                    $otentrylist[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',0,0,1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0)";
                }
            }
        }

        if (count($att_entry)) {
            //To attendance entry
            $attentry1 = 'REPLACE into pms_attendance (att_id,user_id,att_date,comments,att_entry,created_by,created_date,modified_date,type) '
                . ' values ' . implode(",", $att_entry);
            Yii::app()->db->createCommand("INSERT INTO `pms_attendance_reject_log`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  " . implode(",", $att_entry))->query();


            $attreort = Yii::app()->db->createCommand($attentry1)->query();
        }

        if (count($timeentrylist)) {
            Yii::app()->db->createCommand('REPLACE into pms_time_attendance (att_id,user_id,att_date,comments,att_entry,
        att_time,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $timeentrylist))->query();
        }
        //punches to ot time attendance entry
        if (count($otentrylist)) {
            Yii::app()->db->createCommand('REPLACE into pms_ot_attendance (att_id,user_id,att_date,comments,att_entry,ot_time,system_ot,created_by,created_date,modified_date,type) ' . ' values ' . implode(",", $otentrylist))->query();
        }

        // no punch users - LOP

        $company = '';

        if (!isset($_GET['cron'])) {
            if (isset(Yii::app()->user->site_userids) && !empty(Yii::app()->user->site_userids)) {

                $company = ' and emp_id in (' . Yii::app()->user->site_userids . ')';
            }
        }

        $employeesql = "SELECT * FROM `pms_users` WHERE `status` = 28 $company";
        $allemployees = Yii::app()->db->createCommand($employeesql)->queryAll();
        $emp_array = array_column($allemployees, 'emp_id');
        $att_sql = "SELECT * FROM `pms_attendance` WHERE `att_date` ='" . $punchdate . "'";
        $all_attendance = Yii::app()->db->createCommand($att_sql)->queryAll();
        $attendance_array = array_column($all_attendance, 'user_id');
        $diff_result = array_diff($emp_array, $attendance_array);
        $attendance_entry = array();
        $comments = 'From Punch Record';
        $type = 4;
        foreach ($diff_result as $userid) {
            $attendance_entry[] = "(NULL,'{$userid}','{$punchdate}','{$comments}','{$type}',1,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',1)";
        }

        if (count($attendance_entry)) {

            $attendance_entry_sql = 'REPLACE into pms_attendance (att_id,user_id,att_date,comments,att_entry,created_by,created_date,modified_date,type) '
                . ' values ' . implode(",", $attendance_entry);

            $attendance_reject_sql = 'REPLACE into pms_attendance_reject_log (att_id,user_id,att_date,comments,att_entry,created_by,created_date,modified_date,type) '
                . ' values ' . implode(",", $attendance_entry);


            Yii::app()->db->createCommand($attendance_entry_sql)->query();
            Yii::app()->db->createCommand($attendance_reject_sql)->query();
        }
        /* weekly of end */
        /* cron check */

        $update = " UPDATE `pms_cron` SET `type`=1 where `id`=" . $insert_id;
        Yii::app()->db->createCommand($update)->query();

        if (!isset($_GET['cron'])) {

            if (isset(Yii::app()->user->shift_role)) {
                unset(Yii::app()->user->shift_role);
            }
        }

        /* cron check end */

        $transaction->commit();

        if (isset($_GET['cron']) and $_GET['cron'] == 2) {
            echo 'success';
        }
        //     } catch (Exception $e) {
        //         $transaction->rollBack();
        //         if (isset($_GET['cron']) and $_GET['cron'] == 2) {
        //             echo 'failed';
        //         }
        //         $sql = "INSERT INTO 
        //    `pms_manual_gen_attendance_log`
        //    (`id`, `att_date`, `gen_by`, `gen_date`,
        //     `gen_success_date`, `gstatus`,`details`)
        //      VALUES (null,'$punchdate','$created_user','" . date('Y-m-d H:i:s') . "',
        //      '" . date('Y-m-d H:i:s') . "',0,'" . addslashes($e->getMessage()) . "')";
        //         Yii::app()->db->createCommand($sql)->query();
        //     }

        if (isset($_GET['cron'])) {
            echo "Cron executed successfully for " . date("d-m-Y", strtotime($punchdate));
            exit;
        }
        if (isset($_GET['date'])) {
            $this->redirect(Yii::app()->createUrl('attendance/attendance/index'));
        }





        if (isset($_GET['manual'])) {
            Yii::app()->user->setFlash('cron-success', "Cron ran successfully");
            $this->redirect(Yii::app()->createUrl('generalsettings/default'));
        }
    }

    public function get2pStatus($shift_id)
    {

        $sql = " SELECT * FROM  `pms_employee_shift` WHERE `shift_id`=" . $shift_id;
        $shift = Yii::app()->db->createCommand($sql)->queryRow();
        $id = $shift['twoP'];
        if ($id == 1)
            return 1;
        else
            return 2;
    }

    public function sundayEnableCheck($shift_id)
    {

        return 1;
    }

    public function checkSunday($date)
    {

        $time_stamp = strtotime($date);
        $week = date("l", $time_stamp);

        if ($week == "Sunday") {
            return 1;
        } else {
            return 2;
        }
    }

    public function checkweeklyoffavilable($userid, $punchdate)
    {


        $previous_day = date('Y-m-d', strtotime('-1 day', strtotime($punchdate)));
        $previous_wk_start_day = date('Y-m-d', strtotime('-5 day', strtotime($previous_day)));
        $tbl = Yii::app()->db->tablePrefix;

        $sql = " SELECT  SUM(IF(att_entry = 1, 1, 0)) AS present,SUM(IF(att_entry = 2, 1, 0)) AS paid_leave,SUM(IF(att_entry = 3, 1, 0)) AS half_present,  SUM(IF(att_entry = 4, 1, 0)) AS lop,SUM(IF(att_entry =5, 1, 0)) AS holiday,SUM(IF(att_entry =6, 1, 0)) AS sunday,SUM(IF(att_entry =7, 1, 0)) AS two_present,SUM(IF(att_entry =8, 1, 0)) AS single_punch FROM {$tbl}attendance WHERE user_id='$userid' AND  att_date BETWEEN '$previous_wk_start_day' AND '$previous_day'";
        $attendance = Yii::app()->db->createCommand($sql)->queryRow();

        $present = $attendance['present'];
        $paid_leave = $attendance['paid_leave'];
        $half_present = $attendance['half_present'];
        $lop = $attendance['lop'];
        $holiday = $attendance['holiday'];
        $sunday = $attendance['sunday'];
        $two_present = $attendance['two_present'];
        $single_punch = $attendance['single_punch'];

        $present_days = $present + $half_present / 2 + $two_present + $holiday;
        return 7;
    }

    public function actionmonthlylog()
    {

        $month = 0;
        $code = 0;
        if (isset($_GET['months'])) {
            $month = intval($_GET['months']);
        }
        if (isset($_GET['code'])) {
            $code = intval($_GET['code']);
            if ($code == 0)
                $code = Yii::app()->user->id;
        } else {
            $code = Yii::app()->user->id;
        }

        $page_date = mktime(0, 0, 0, date("m") + $month, date("d"), date("Y"));
        $total_days = date('t', $page_date);
        $month_yr = date('m-Y', $page_date);
        $logmonth = date('F Y', $page_date);

        $date_from = date("Y-m-01", $page_date);
        $date_till = date("Y-m-t", $page_date);
        $prev_date_till = date('Y-m-d', strtotime('-1 day', strtotime($date_till)));
        $cur_array = array();
        if ($date_from != date('Y-m-01')) {
            $prev_date_till = $date_till;
        } else {

            $this->pdate = date('Y-m-d');
            $this->getDaywiseresult();
            $this->calculateResult();
            $curdate = date('Ymd', strtotime($this->pdate));

            $cur_punchreport = isset($this->punchresult[$code]['punches'][$curdate]['presult']) ? $this->punchresult[$code]['punches'][$curdate]['presult'] : "";


            if ($cur_punchreport != "") {
                $shift_details = (isset($this->punchresult[$code][$curdate]['punches'])) ? $this->punchresult[$code]['punches'][$curdate]['shift_details'] : "";
                extract($cur_punchreport);
                if ($shift_details != "") {
                    extract($shift_details);
                }
                $cur_array[] = array('logdate' => $this->pdate, 'firstpunch' => $firstpunch, 'lastpunch' => $lastpunch, 'intime' => $intime, 'outtime' => $outtime, 'total_punch' => $totpunch, 'shift_name' => isset($shift) ? $shift : "", 'shift_starts' => isset($shift_start) ? $shift_start : "", 'shift_ends' => isset($shift_end) ? $shift_end : "", 'color_code' => isset($color_code) ? $color_code : "");
            }
        }

        $punchreport = Yii::app()->db->createCommand("SELECT pms_punchreport.*,shift_name,shift_starts,shift_ends,color_code FROM `pms_punchreport` left join pms_employee_shift on pms_employee_shift.shift_id= pms_punchreport.shift_id WHERE `logdate` between '" . $date_from . "' and '" . $prev_date_till . "' and resource_id = " . $code)->queryAll();

        if (!empty($cur_array)) {

            $punchreport = array_merge($punchreport, $cur_array);
        }

        $result_log = array();
        foreach ($punchreport as $data) {
            extract($data);

            $earlyexit = 0;
            $result_log['log_' . date('j', strtotime($logdate))] = array(
                'outearly' => 0,
                'accesscard_id' => 0,
                'first_punch' => !empty($firstpunch) ? $firstpunch : '',
                'last_punch' => !empty($lastpunch) ? $lastpunch : '',
                'emp_name' => 'test',
                'userid' => 13,
                'insec' => !empty($intime) ? $intime : 0,
                'outsec' => !empty($outtime) ? $outtime : 0,
                'outhrs' => !empty($outtime) ? $outtime : 0,
                'count' => !empty($total_punch) ? $total_punch : 0,
                'status' => 1,
                'shift' => (isset($shift_name)) ? $shift_name : '',
                'color_code' => (isset($color_code)) ? $color_code : '',
                'shift_starts' => (isset($shift_starts)) ? $shift_starts : 0,
                'shift_end' => (isset($shift_ends)) ? $shift_ends : '',
                'ee_halfday' => (!empty($late_time)) ? gmdate('H:i:s', $late_time) : 0, //First punch Half day
                'late_halfday' => (!empty($earlyexit)) ? gmdate('H:i:s', $earlyexit) : 0, //Early Exit Half day
            );
        }

        // echo '<pre>';print_r($result_log);exit;


        $userdata = Yii::app()->db->createCommand("SELECT concat_ws(' ',first_name,last_name) AS full_name FROM pms_users WHERE emp_id='" . $code . "'")->queryScalar();

        $company = '';
        if (isset(Yii::app()->user->company_id)) {
            $company = ' and company_id = ' . Yii::app()->user->company_id;
        }


        $acessusersql = "SELECT pms_users.emp_id, concat_ws(' ',first_name,last_name) AS full_name FROM pms_users, "
            . "pms_device_accessids WHERE pms_device_accessids.userid = pms_users.emp_id $company  "
            . " ORDER BY TRIM(full_name) ";
        $accessuserdata = Yii::app()->db->createCommand($acessusersql)->queryAll();;


        $data = array(
            'page_date' => $page_date,
            'total_days' => $total_days,
            'month_yr' => $month_yr,
            'logmonth' => $logmonth,
            'result_log' => $result_log,
            'userdata' => $userdata,
            'accessuserdata' => $accessuserdata,
            'code' => $code,
        );

        $this->render('monthlylog', $data);
    }

    public function actionDeleteManualentry()
    {

        $date = date('Y-m-d H:i:s', strtotime($_REQUEST['date']));
        $userid = $_REQUEST['userid'];
        $check = Yii::app()->db->createCommand("SELECT * FROM `pms_manual_entry` WHERE date='" . $date . "' and `emp_id`= " . $userid)->queryRow();
        $table = "pms_manual_entry";

        if (empty($check)) {
            $check = Yii::app()->db->createCommand("SELECT * FROM `pms_shift_entry` WHERE date='" . $date . "' and `emp_id`= " . $userid)->queryRow();

            $table = "pms_shift_entry";
        }

        $datamodel = new ManualEntryLog;
        $datamodel->emp_id = $check['emp_id'];
        $datamodel->shift_date = $check['shift_date'];
        $datamodel->date = $check['date'];
        $datamodel->comment = $check['comment'];
        $datamodel->status = $check['status'];
        $datamodel->decision_by = $check['decision_by'];
        $datamodel->decision_date = $check['decision_date'];
        $datamodel->created_date = $check['created_date'];
        $datamodel->created_by = $check['created_by'];

        $datamodel->save();

        Yii::app()->db->createCommand("DELETE FROM $table WHERE date='" . $date . "' and `emp_id`= " . $userid)->execute();


        $this->actionPunchReportDaily(date('Y-m-d', strtotime($check['shift_date'])));

        // Yii::import('application.controllers.PunchresultController');
        // PunchresultController::actionPunchReportDaily(date('Y-m-d', strtotime($check['date'])));

        echo "1";
        exit;
    }

    public function actionmovetoshift()
    {

        if (isset($_POST)) {

            $cu_date = date('Y-m-d H:i:s', strtotime($_POST['cur_date']));
            $shift_date = date('Y-m-d', strtotime($_POST['shift_date']));
            $userid = $_POST['userid'];

            Yii::app()->db->createCommand("DELETE FROM `pms_shift_entry`  WHERE shift_date='" . $shift_date . "' and date='" . $cu_date . "' and `emp_id`= " . $userid)->execute();


            $model = new ShiftEntry;
            $model->emp_id = $userid;
            $model->date = $cu_date;
            //$model->comment = $comment;
            $model->shift_date = $shift_date;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->type = 1;
            $model->save();

            $msg = 'Shifted Successfully';


            echo $msg;
            exit;
        }
    }

    public function actionTestmail()
    {
        $mail = new JPhpMailer();
        $subject = "" . Yii::app()->name . " :Daybook Payment Alert!";
        $headers = "" . Yii::app()->name . "";
        $bodyContent = "<p>Hi</p>";
        $mail->IsSMTP();
        $mail->Host = "ssl://mail.carris.xyz:465";
        $mail->SMTPSecure = "ssl";
        $mail->SMTPAuth = true;
        $mail->Username = "info@carris.xyz";
        $mail->Password = "CarrisBhi@2018";
        $mail->setFrom("info@carris.xyz", Yii::app()->name);
        $mail->addAddress("surumi.ja@bluehorizoninfotech.com");
        $mail->isHTML(true);
        $mail->port = 465;
        $mail->Subject = "" . Yii::app()->name;
        $mail->Body = $bodyContent;
        if ($mail->Send()) {
            echo 'ask';
        } else {
            echo '87698790';
        }
        exit;
    }

    public function actionupdateShiftTemplateMonthly()
    {


        //weekly generation

        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d', strtotime('+6 day', strtotime($start_date)));


        $begin = new DateTime($start_date);
        $end = new DateTime($end_date);
        $end = $end->modify('+1 day');
        $interval = new DateInterval('P1D');
        $period = new DatePeriod($begin, $interval, $end);

        $empsql = "SELECT emp_id,temp_type FROM pms_users where temp_type is not null";
        $employees = Yii::app()->db->createCommand($empsql)->queryAll();

        $month = date('F Y', strtotime($start_date));
        $second_sat = date('Y-m-d', strtotime('second sat of ' . $month));
        $fourth_sat = date('Y-m-d', strtotime('fourth sat of ' . $month));


        foreach ($employees as $emp) {
            $sql1 = "SELECT * FROM pms_template WHERE `id`= " . $emp['temp_type'];
            $template = Yii::app()->db->createCommand($sql1)->queryRow();


            foreach ($period as $key => $value) {

                $attdate = $value->format('Y-m-d');
                $sql2 = "SELECT count(*) as cnt, assign_shift_id FROM `pms_shift_assign`"
                    . " WHERE `att_date`='" . $attdate . "' and `user_id`= " . $emp['emp_id'];
                $exist = Yii::app()->db->createCommand($sql2)->queryRow();

                if ($exist['cnt'] != 0) {
                    $sql3 = "DELETE FROM `pms_shift_assign` WHERE `assign_shift_id` = " . $exist['assign_shift_id'];
                    Yii::app()->db->createCommand($sql3)->execute();
                }

                $day = lcfirst(date('l', strtotime($attdate)));
                $new = new AssignShift();
                $new->shift_id = $template[$day];
                $new->user_id = $emp['emp_id'];
                $new->att_date = $attdate;
                $new->created_by = -1;
                $new->created_date = date('Y-m-d');

                if ($template['second_saturday'] != '' && $second_sat == $attdate) {
                    $new->shift_id = $template['second_saturday'];
                }
                if ($template['fourth_saturday'] != '' && $fourth_sat == $attdate) {
                    $new->shift_id = $template['fourth_saturday'];
                }
                $new->save();
            }
        }
    }

    public function actionMonthlyleaveupdate()
    {

        $sdate = date('Y-m-01');
        $tdate = date('Y-m-t');

        $users = Employee::model()->findAll('status = 28 and system_user!=1');

        foreach ($users as $data) {


            $present_days = $this->getpresentdays($data['emp_id'], $sdate, $tdate);
            if ($present_days >= 20) {

                $sql = "select * from pms_balance_leave where userid=" . $data['emp_id'];
                $udata = Yii::app()->db->createCommand($sql)->queryRow();

                $clcr = 1;
                $elcr = 0;
                $rhcr = 0;
                $mlcr = 0;

                //add to leave log
                $coment = 'Auto credit CL/ML:' . $clcr . ', EL:' . $elcr . ' ,RH:' . $rhcr . ' <br />Avail Credits CL/ML:'
                    . ($udata['cl_ml'] + $clcr) . ' ,EL:' . ($udata['earned'] + $elcr) . ' ,RH:' . ($udata['rh'] + $rhcr);
                $leavelog_data = array('cl' => $clcr, 'el' => $elcr, 'rh' => $rhcr, 'ml' => $mlcr, 'coment' => $coment);
                $this->addtoLeaveLog($data['emp_id'], $leavelog_data);
            }
        }
    }

    public function getpresentdays($userid = 0, $start, $end)
    {


        $tbl = Yii::app()->db->tablePrefix;
        $sql = " SELECT  SUM(IF(att_entry = 1, 1, 0)) AS present,SUM(IF(att_entry = 2, 1, 0)) AS paid_leave,SUM(IF(att_entry = 3, 1, 0)) AS half_present,  SUM(IF(att_entry = 4, 1, 0)) AS lop,SUM(IF(att_entry =5, 1, 0)) AS holiday,SUM(IF(att_entry =6, 1, 0)) AS sunday,SUM(IF(att_entry =7, 1, 0)) AS two_present,SUM(IF(att_entry =8, 1, 0)) AS single_punch FROM {$tbl}attendance WHERE user_id='$userid' AND  att_date BETWEEN '$start' AND '$end'";
        $attendance = Yii::app()->db->createCommand($sql)->queryRow();

        $present = $attendance['present'];
        $paid_leave = $attendance['paid_leave'];
        $half_present = $attendance['half_present'];
        $lop = $attendance['lop'];
        $holiday = $attendance['holiday'];
        $sunday = $attendance['sunday'];
        $two_present = $attendance['two_present'];
        $single_punch = $attendance['single_punch'];

        $present_days = $present + $paid_leave + $half_present / 2 + $sunday + $holiday + $two_present;
        return $present_days;
    }

    public function actionUpdateAttendance()
    {
        $date = $_REQUEST['date'];
        $sql = "select att_entry,user_id from `pms_attendance_requests` where att_date=" . $date . "and status=1";
        $requests = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($requests as $request) {
            $sql = "update pms_attendance set att_entry=" . $request['att_entry'] . " ,type=2 where user_id ="
                . $request['user_id'] . " and att_date=" . $date;
            $requests = Yii::app()->db->createCommand($sql)->query();
        }

        $sql = "select att_entry,user_id from `pms_attendance_requests` where att_date=" . $date . "and status=0";
        $requests = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($requests as $request) {
            $sql = "update pms_attendance set att_entry=9 ,type=2 where user_id ="
                . $request['user_id'] . " and att_date=" . $date;
            $requests = Yii::app()->db->createCommand($sql)->query();
        }


        echo "completed";
    }

    public function actionUpdateOtAttendance()
    {
        $date = $_REQUEST['date'];
        $sql = "select ot_time,user_id from `pms_ot_requests` where att_date=" . $date . "and status=1";
        $requests = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($requests as $request) {
            $sql = "update pms_ot_attendance set ot_time=" . $request['ot_time'] . " ,type=1 where user_id ="
                . $request['user_id'] . " and att_date=" . $date;
            $requests = Yii::app()->db->createCommand($sql)->query();

            $sql = "select * from pms_ot_attendance where user_id ="
                . $request['user_id'] . " and att_date=" . $date;

            $sql_test = Yii::app()->db->createCommand($sql)->query();
            if (count($sql_test) == 0) {
                $sql1 = "INSERT INTO `pms_ot_attendance`(`att_id`, `user_id`, `att_date`, `comments`, `att_entry`, `ot_time`, `created_by`, `created_date`, `modified_date`, `type`, `system_ot`, `pending_status`)
              VALUES (null," . $request['user_id'] . "," . $date . ",'manual entry','1'," . $request['ot_time'] . ",'1','" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "','1','0','2')";
                $sql_test = Yii::app()->db->createCommand($sql1)->query();
            }
        }



        echo "completed";
    }

    public function getChildGroups($parent_id)
    {

        function fetchGroupTree1($parent = 'NULL', $spacing = '', $user_tree_array = '')
        {

            if (!is_array($user_tree_array))
                $user_tree_array = array();
            if ($parent == 'NULL') {
                $group = Yii::app()->db->createCommand("SELECT * FROM `pms_employee_group` WHERE  parent_id IS NULL")->queryAll();
            } else {
                $group = Yii::app()->db->createCommand("SELECT * FROM `pms_employee_group` WHERE  parent_id=" . $parent)->queryAll();
            }
            foreach ($group as $key => $value) {

                $user_tree_array[] = array("group_id" => $value['group_id'], "group_name" => $spacing . $value['group_name']);
                $user_tree_array = fetchGroupTree1($value['group_id'], $spacing . '&nbsp;&nbsp; -', $user_tree_array);
            }


            return $user_tree_array;
        }

        $groupList = fetchGroupTree1($parent_id);

        $grp_ids = array();
        if (!empty($groupList)) {
            foreach ($groupList as $data) {
                $grp_ids[] = $data['group_id'];
            }
            array_push($grp_ids, $parent_id);
            $grp_ids = implode(',', $grp_ids);
        }
        return $grp_ids;
    }

    public function export_reportpdf($data, $pdate)
    {


        $site_file = "";
        if (isset(Yii::app()->user->company_id)) {

            $sql = "SELECT `name` FROM `pms_company` WHERE company_id =" . Yii::app()->user->company_id;
            $user_sites = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($user_sites as $site) {
                $site_file = $site['name'];
            }
        }

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4-L');
        $mPDF1->WriteHTML($stylesheet, 1);
        // $data = $this->renderPartial('report_pdf', $data, true);
        $mPDF1->WriteHTML($data);
        $filename = " Daily Punching Report_" . $site_file . "_" . $pdate;
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function export_excel($data)
    {

        $site_file = "";
        if (isset(Yii::app()->user->company_id)) {

            $sql = "SELECT `name` FROM `pms_company` WHERE company_id =" . Yii::app()->user->company_id;
            $user_sites = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($user_sites as $site) {
                $site_file = $site['name'];
            }
        }
        $punchresult = $data['punchresult'];


        // for ($i = 0; $i < $interval->days + 1; $i++) {
        //     $datehead = new DateTime($date_from);
        //     $datehead->add(new DateInterval('P' . $i . 'D'));
        //     $labelhead = (string)$datehead->format('d-m-Y');
        //     $finaldata[0][] = $labelhead;
        // }
        if (isset(Yii::app()->user->attendance_type)) {
            $astatus = Yii::app()->user->attendance_type;
        } else
            $astatus = 3;

        if (isset(Yii::app()->user->company_id)) {
            $my_sql = "SELECT * FROM `pms_company` where company_id=" . Yii::app()->user->company_id;
            $show_company_name = Yii::app()->db->createCommand($my_sql)->queryRow();
        } else {
            $show_company_name['name'] = "Not Selected";
        }
        if (isset(Yii::app()->user->shift_id)) {
            $my_sql = "SELECT * FROM `pms_employee_shift` where shift_id=" . Yii::app()->user->shift_id;

            $show_shift_name = Yii::app()->db->createCommand($my_sql)->queryRow();
        } else {
            $show_shift_name['shift_name'] = "Not Selected";
        }

        $k = 1;
        $m = 1;
        $i = 4;

        $arraylabel = array(
            '#',
            'Emp Code',
            'Name',
            'Status',
            'First Punch',
            'Last Punch',
            'time', 'OUT time',
            'Tot Time',
            'Tot Punch',
            'Tot Break Time',
            'Over Time',
            'Shift'
        );

        foreach ($punchresult as $item) {
            $j = 0;
            $pdateindex = date('Ymd', strtotime($this->pdate));
            $punchres = $item['punches'][$pdateindex];
            extract($item['empdetails']);
            extract($punchres['presult']);
            $tottime = $intime + $outtime;
            $shift_temp_no_ot = (isset($punchres['shift_details']['no_ot']) && $punchres['shift_details']['no_ot'] == 1) ? 1 : 0;
            $shift = (isset($punchres['shift_details']['shift'])) ? $punchres['shift_details']['shift'] : 'No shift';
            if (!isset($punchres['shift_details']['shift'])) {
                $res = Yii::app()->db->createCommand("SELECT pms_shift_assign.shift_id,shift_name, user_id, att_date, shift_starts, shift_ends,grace_period_before,grace_period_after,color_code FROM `pms_shift_assign`left join pms_employee_shift on pms_employee_shift.shift_id = pms_shift_assign. shift_id WHERE `user_id` = " . $userid . " and `att_date` = '" . date('Y-m-d', strtotime($pdateindex)) . "' ")->queryRow();
                if (!empty($res)) {
                    $shift = $res['shift_name'];
                }
            }
            if ($firstpunch != '-') {
                if ($astatus == 2 || $astatus == 3) {
                    $finaldata[0][0] = "Daily Punch Report";
                    $finaldata[1][0] = "Date";
                    $finaldata[1][1] = date('d/m/Y', strtotime($this->pdate));
                    $finaldata[2][0] = "Selected Site";
                    $finaldata[2][1] = $show_company_name['name'];
                    $finaldata[2][2] = "Selected Shift";
                    $finaldata[2][3] = $show_shift_name['shift_name'];
                    $finaldata[3][0] = "#";
                    $finaldata[3][1] = "Emp Code";
                    $finaldata[3][2] = "Name";
                    $finaldata[3][3] = "Status";
                    $finaldata[3][4] = "First Punch";
                    $finaldata[3][5] = "Last Punch";
                    $finaldata[3][6] = "time";
                    $finaldata[3][7] = "OUT time";
                    $finaldata[3][8] = "Tot Time";
                    $finaldata[3][9] = "Tot Punch";
                    $finaldata[3][10] = "Tot Break Time";
                    $finaldata[3][11] = "Over Time";
                    $finaldata[3][12] = "Shift";
                    $finaldata[$i][$j++] = $m++;
                    $finaldata[$i][$j++] = ($empcode == '') ? '' : $empcode;
                    $finaldata[$i][$j++] = $fullname;
                    $finaldata[$i][$j++] = $status;
                    if ($firstpunch != '-') {
                        $finaldata[$i][$j++] = date('h:i:s', strtotime($firstpunch));
                        $finaldata[$i][$j++] = date('h:i:s', strtotime($lastpunch));
                    } else {
                        $finaldata[$i][$j++] = "-";
                        $finaldata[$i][$j++] = "-";
                    }
                    $finaldata[$i][$j++] = ($intime > 0 ? gmdate("H:i:s", $intime) : '-');
                    $finaldata[$i][$j++] = ($outtime > 0 ? gmdate("H:i:s", $outtime) : '-');
                    $finaldata[$i][$j++] = ($tottime > 0 ? gmdate("H:i:s", $tottime) : '-');
                    $finaldata[$i][$j++] = ($totpunch > 0 ? $totpunch : '-');
                    $finaldata[$i][$j++] = (isset($break_time) && $break_time != '') ? gmdate("H:i:s", $break_time) : ' - ';
                    if ($shift_temp_no_ot == 0) {
                        if (isset($manual_ot['manual_ot']) && isset($manual_ot['manual_ot']) != '') {
                            $finaldata[$i][$j++] = (($manual_ot['manual_ot'] != 0) ? gmdate("H:i:s", $manual_ot['manual_ot']) : 0) . " (M)";
                        } else {
                            //echo (isset($outside_punch) && $outside_punch != '') ? gmdate("H:i:s", $outside_punch) : ' - ';

                            $finaldata[$i][$j++] = (isset($outside_punch) && $outside_punch != '') ? $outside_punch : ' - ';
                        }
                    } else {
                        $finaldata[$i][$j++] = ' - ';
                    }
                    $finaldata[$i][$j++] = $shift;

                    $i++;
                }
            } else {
                if ($astatus == 1 || $astatus == 3) {
                    $finaldata[0][0] = "Daily Punch Report";
                    $finaldata[1][0] = "Date";
                    $finaldata[1][1] = date('d/m/Y', strtotime($this->pdate));
                    $finaldata[2][0] = "Selected Site";
                    $finaldata[2][1] = $show_company_name['name'];
                    $finaldata[2][2] = "Selected Shift";
                    $finaldata[2][3] = $show_shift_name['shift_name'];
                    $finaldata[3][0] = "#";
                    $finaldata[3][1] = "Emp Code";
                    $finaldata[3][2] = "Name";
                    $finaldata[3][3] = "Status";
                    $finaldata[3][4] = "First Punch";
                    $finaldata[3][5] = "Last Punch";
                    $finaldata[3][6] = "time";
                    $finaldata[3][7] = "OUT time";
                    $finaldata[3][8] = "Tot Time";
                    $finaldata[3][9] = "Tot Punch";
                    $finaldata[3][10] = "Tot Break Time";
                    $finaldata[3][11] = "Over Time";
                    $finaldata[3][12] = "Shift";
                    $finaldata[$i][$j++] = $m++;
                    $finaldata[$i][$j++] = ($empcode == '') ? '' : $empcode;
                    $finaldata[$i][$j++] = $fullname;
                    $finaldata[$i][$j++] = $status;
                    if ($firstpunch != '-') {
                        $finaldata[$i][$j++] = date('h:i:s', strtotime($firstpunch));
                        $finaldata[$i][$j++] = date('h:i:s', strtotime($lastpunch));
                    } else {
                        $finaldata[$i][$j++] = "-";
                        $finaldata[$i][$j++] = "-";
                    }
                    $finaldata[$i][$j++] = ($intime > 0 ? gmdate("H:i:s", $intime) : '-');
                    $finaldata[$i][$j++] = ($outtime > 0 ? gmdate("H:i:s", $outtime) : '-');
                    $finaldata[$i][$j++] = ($tottime > 0 ? gmdate("H:i:s", $tottime) : '-');
                    $finaldata[$i][$j++] = ($totpunch > 0 ? $totpunch : '-');
                    $finaldata[$i][$j++] = (isset($break_time) && $break_time != '') ? gmdate("H:i:s", $break_time) : ' - ';
                    if ($shift_temp_no_ot == 0) {
                        if (isset($manual_ot['manual_ot']) && isset($manual_ot['manual_ot']) != '') {
                            $finaldata[$i][$j++] = (($manual_ot['manual_ot'] != 0) ? gmdate("H:i:s", $manual_ot['manual_ot']) : 0) . " (M)";
                        } else {
                            //echo (isset($outside_punch) && $outside_punch != '') ? gmdate("H:i:s", $outside_punch) : ' - ';

                            $finaldata[$i][$j++] = (isset($outside_punch) && $outside_punch != '') ? $outside_punch : ' - ';
                        }
                    } else {
                        $finaldata[$i][$j++] = ' - ';
                    }
                    $finaldata[$i][$j++] = $shift;

                    $i++;
                }
            }
        }

        // echo '<pre>';print_r($finaldata);exit;

        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();

        $encode = "\xEF\xBB\xBF"; // UTF-8 BOM
        $contentdata = $encode . $csv->toCSV();

        $csvfilename = 'Daily Punching Report_' . $site_file . "_" . date($this->pdate) . '.xls';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/xls", false);
        exit();
    }

    function findUserId($date, $grpcon)
    {
        $sql = "SELECT user_id FROM `pms_usersite` WHERE " . $grpcon . " AND(
        ('" . $date . "' BETWEEN date_from AND date_to) OR ('" . $date . "'>=date_from and  date_to IS NULL)  )";
        $allids = Yii::app()->db->createCommand($sql)->queryAll();
        $site_userids = array_column($allids, 'user_id');
        return $site_userids;
    }

    public function checkHoliday($date)
    {
        // echo $date; exit;
        $holiday_sql = "select * from pms_holidays where holiday_date='" . $date . "' and status=1";
        $holiday = Yii::app()->db->createCommand($holiday_sql)->queryAll();

        $count = count($holiday);
        if ($count >= 1) {
            $is_holiday = true;
        } else {
            $is_holiday = false;
        }
        return $is_holiday;
    }
}
