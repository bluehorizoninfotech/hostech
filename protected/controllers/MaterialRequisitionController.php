<?php

class MaterialRequisitionController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'create', 'update', 'adddItem', 'deleteMR',
                    'getRelatedUnit', 'getMRItems', 'updateItem',
                    'approveentry', 'reject_mr'
                ),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $data = MaterialRequisitionItems::model()->findAll(array("condition" => "mr_id = '" . $id . "'", "order" => "id DESC"));
        $material_req_item_arr = [];
        foreach ($data as $req_items) {
            $item_id = $req_items->item_id;
            if ($item_id == 0) {
                $item_name =    $req_items->item_name;
            } else {
                $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                    . " FROM jp_specification "
                    . " WHERE id=" . $item_id . "";
                $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
                $cat_sql = "SELECT * FROM jp_purchase_category "
                    . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_sql = "SELECT brand_name "
                        . " FROM jp_brand "
                        . " WHERE id=" . $specification['brand_id'] . "";
                    $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
            }

            array_push($material_req_item_arr, [
                'item_name' => $item_name,
                'item_unit' => $req_items->item_unit,
                'item_quantity' => $req_items->item_quantity,
                'item_req_date' => $req_items->item_req_date,
                'item_remarks' => $req_items->item_remarks

            ]);
        }


        $this->render('view', array(
            'model' => $this->loadModel($id),
            'material_req_item_arr' => $material_req_item_arr
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id,$type)
    {
       

        $model = new MaterialRequisition('search');
        //  print_r($model);die;
        $model->unsetAttributes();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['req_no'])) {
            $model = new MaterialRequisition;

            $mapping = Yii::app()->db->createCommand()
            ->select('acc_project_id')
            ->from('pms_acc_project_mapping')
            ->where('pms_project_id=:pms_project_id', array(':pms_project_id' => $_POST['project_id']))
            ->queryScalar();
            $accounts_project_id = "";
            if($mapping){
                $accounts_project_id = $mapping ;
            }
            $task = Tasks::model()->findByPk( $_POST['task_id']);

            $model->requisition_project_id = $_POST['project_id'];
            $model->requisition_task_id = $_POST['task_id'];
            $model->requisition_no = $_POST['req_no'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->project_id = $accounts_project_id ;
            $model->task_description = $task->title;
            if ($model->save()) {

                $json_data = array('status' => 1, 'mr_id' => $model->requisition_id);
                echo  json_encode($json_data);
                exit;
            }
        }


        $task = Tasks::model()->findByPK($id);
        $task_name = $task->title;
        $project_name = $task->project->name;
        $mr_count = count(MaterialRequisition::model()->findAll()) + 1;
        $unqid = str_pad($mr_count, 6, '0', STR_PAD_LEFT);
        $requisition_no = 'MR' . $unqid;

        $account_items = $this->getAccountItems();
        $unit_sql = "SELECT * FROM jp_unit";
        $unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll();
        $mr_for_task = MaterialRequisition::model()->findAll(array('condition' => 'requisition_task_id=' . $id));

        if (isset($_GET['MaterialRequisition']))
            $model->attributes = $_GET['MaterialRequisition'];

        $this->render('create', array(
            'model' => $model, 'task_name' => $task_name, 'project_name' => $project_name,
            'requisition_no' => $requisition_no, 'task_id' => $id, 'project_id' => $task->project_id,
            'mr_for_task' => $mr_for_task,
            'account_items' => $account_items,
            'unit_list' => $unit_list,
            'type'=>$type

        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MaterialRequisition'])) {
            $model->attributes = $_POST['MaterialRequisition'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->requisition_id));
        }
        $mr = $task = MaterialRequisition::model()->findByPK($id);
        $task = Tasks::model()->findByPK($mr->requisition_task_id);
        $task_name = $task->title;
        $project_name = $task->project->name;
        $account_items = $this->getAccountItems();
        $unit_sql = "SELECT * FROM jp_unit";
        $unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll();
        $data = MaterialRequisitionItems::model()->findAll(array("condition" => "mr_id = '" . $id . "'", "order" => "id DESC"));
        $material_req_item_arr = [];
        foreach ($data as $req_items) {
            $item_id = $req_items->item_id;
            if ($item_id == 0) {
                $item_name =    $req_items->item_name;
            } else {
                $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                    . " FROM jp_specification "
                    . " WHERE id=" . $item_id . "";
                $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
                $cat_sql = "SELECT * FROM jp_purchase_category "
                    . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_sql = "SELECT brand_name "
                        . " FROM jp_brand "
                        . " WHERE id=" . $specification['brand_id'] . "";
                    $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
            }

            array_push($material_req_item_arr, [
                'item_name' => $item_name,
                'item_unit' => $req_items->item_unit,
                'item_quantity' => $req_items->item_quantity,
                'item_req_date' => $req_items->item_req_date,
                'item_remarks' => $req_items->item_remarks

            ]);
        }

        $items_model = new MaterialRequisitionItems('search');

        $this->render('update', array(
            'model' => $model,
            'task_name' => $task_name,
            'project_name' => $project_name,
            'project_id' => $task->project_id,
            'task_name' => $task_name,
            'task_id' => $task->tskid,
            'account_items' => $account_items,
            'unit_list' => $unit_list,
            'material_req_item_arr' => $material_req_item_arr,
            'items_model' => $items_model,
            'id' => $id

        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {

        $model = new MaterialRequisition('search');

        $model->unsetAttributes();

        if (isset($_GET['MaterialRequisition']))
            $model->attributes = $_GET['MaterialRequisition'];

        $this->render('index', array(
            'model' => $model,

        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new MaterialRequisition('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MaterialRequisition']))
            $model->attributes = $_GET['MaterialRequisition'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = MaterialRequisition::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'material-requisition-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actionadddItem()
    {
        $model = new MaterialRequisitionItems();
        $model->mr_id = $_POST['material_req_id'];
        if ($_POST['requisition_item'] == 'other') {

            $model->item_id = 0;
        } else {
            $model->item_id = $_POST['requisition_item'];
        }

        $model->item_name = $_POST['requisition_item_name'];
        $model->item_unit = $_POST['requisition_unit'];
        $model->item_quantity = $_POST['requisition_quantity'];
        $model->item_req_date = date('Y-m-d', strtotime($_POST['requisition_date']));
        $model->item_remarks = $_POST['requisition_remarks'];
        $model->created_by = yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->updated_by = yii::app()->user->id;
        $model->updated_date = date('Y-m-d');
        $model->save();
        $data = MaterialRequisitionItems::model()->findAll(array("condition" => "mr_id = '" . $_POST['material_req_id'] . "'", "order" => "id DESC"));
        $material_req_item_arr = [];
        foreach ($data as $req_items) {
            $item_id = $req_items->item_id;
            if ($item_id == 0) {
                $item_name =    $req_items->item_name;
            } else {
                $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                    . " FROM jp_specification "
                    . " WHERE id=" . $item_id . "";
                $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
                $cat_sql = "SELECT * FROM jp_purchase_category "
                    . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_sql = "SELECT brand_name "
                        . " FROM jp_brand "
                        . " WHERE id=" . $specification['brand_id'] . "";
                    $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
            }

            array_push($material_req_item_arr, [
                'item_name' => $item_name,
                'item_unit' => $req_items->item_unit,
                'item_quantity' => $req_items->item_quantity,
                'item_req_date' => $req_items->item_req_date,
                'item_remarks' => $req_items->item_remarks

            ]);
        }

        $result = $this->renderPartial('material_req_items', array(
            'model' => $model,
            'data' => $material_req_item_arr
        ), true);
        echo json_encode($result);
    }
    public function actiondeleteMR()
    {
        $mr_id = $_POST['mr_id'];
        MaterialRequisitionItems::model()->deleteAll('mr_id = :mr_id', array(':mr_id' => $mr_id));
        MaterialRequisition::model()->deleteAll('requisition_id = :mr_id', array(':mr_id' => $mr_id));

        $json_data = array('status' => 1);
        echo  json_encode($json_data);
    }
    public function getAccountItems()
    {

        $data = array();
        $final = array();


        $spec_sql = "SELECT id, specification, brand_id, cat_id "
            . " FROM jp_specification ";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();

        foreach ($specification as $key => $value) {
            $brand = '';
            if ($value['brand_id'] != NULL) {
                $brand_sql = "SELECT brand_name "
                    . " FROM jp_brand "
                    . " WHERE id=" . $value['brand_id'] . "";
                $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            }

            if ($value['cat_id'] != "") {
                $result = $this->GetParent($value['cat_id']);
            }
            $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
            $final[$key]['id']   = $value['id'];
        }
        return $final;
    }
    public function GetParent($id)
    {

        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM jp_purchase_category WHERE id=" . $id . "")->queryRow();
        return $category;
    }
    public function actiongetRelatedUnit()
    {
        $item_id = $_POST['item_id'];
        $html['html'] = '';
        $html['unit'] = '';
        if ($item_id != '' && $item_id != 'other') {
            $unit_purch_list = Yii::app()->db->createCommand("SELECT unit,id FROM jp_specification  WHERE id=" . $item_id)->queryRow();
            $unit_sql = "SELECT * FROM jp_unit_conversion "
                . " LEFT JOIN jp_unit "
                . " ON jp_unit_conversion.conversion_unit=jp_unit.unit_name "
                . " WHERE jp_unit_conversion.item_id=" . $item_id;
            $unit_list = Yii::app()->db->createCommand($unit_sql)->queryAll();

            if (!empty($unit_list)) {


                $html['html'] .= '<option value="" >Select Unit</option>';
                foreach ($unit_list as $key => $value) {
                    $html['html'] .= '<option value="' . $value['conversion_unit'] . '" >' . $value['unit_name'] . '</option>';
                }
                $html['unit'] .= $unit_purch_list['unit'];
            } else {

                $html['html'] .= '<option value="" >Select Unit</option>';
                $html['html'] .= '<option value="' . $unit_purch_list['unit'] . '" >' . $unit_purch_list['unit'] . '</option>';
                $html['unit'] .= $unit_purch_list['unit'];
            }
        } else {
            $unit_sql = "SELECT * FROM jp_unit";
            $unit_list1 = Yii::app()->db->createCommand($unit_sql)->queryAll();
            $html['html'] .= '<option value="" >Select Unit</option>';
            foreach ($unit_list1 as $key => $value) {
                $html['html'] .= '<option value="' . $value['unit_name'] . '" >' . $value['unit_name'] . '</option>';
            }
        }
        echo json_encode($html);
    }
    public function actiongetMRItems()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = MaterialRequisitionItems::model()->findByPk($id);
            if (!empty($model)) {
                $model_array = array(
                    'stat' => 1, 'id' => $model->id,
                    'item_id' => $model->item_id, 'item_name' => $model->item_name,
                    'item_unit' => $model->item_unit,
                    'item_date' => date('d-M-y', strtotime($model->item_req_date)),
                    'item_qty' => $model->item_quantity,
                    'item_remarks' => $model->item_remarks

                );
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo  json_encode($model_array);
        exit;
    }
    public function actionupdateItem()
    {

        $id = $_POST['id'];

        if ($id == "") {
            $model = new MaterialRequisitionItems;
            $type = 0;
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
        } else {
            $model = MaterialRequisitionItems::model()->findByPk($id);
            $type = 1;
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
        }

        if ($_POST['requisition_item'] == 'other') {

            $model->item_id = 0;
        } else {
            $model->item_id = $_POST['requisition_item'];
        }
        $model->mr_id = $_POST['material_req_id'];
        $model->item_name = $_POST['requisition_item_name'];
        $model->item_unit = $_POST['requisition_unit'];
        $model->item_quantity = $_POST['requisition_quantity'];
        $model->item_req_date = date('Y-m-d', strtotime($_POST['requisition_date']));
        $model->item_remarks = $_POST['requisition_remarks'];


        if ($model->save()) {
            $response = array('status' => 1, 'type' => $type);
        } else {
            $response = array('status' => 0, 'type' => $type);
        }
        echo json_encode($response);
    }
    public function actionapproveEntry()
    {
        $id = $_POST['id'];
        $model = MaterialRequisition::model()->findByPk($id);
        $model->requisition_status = 1;
        $model->approved_rejected_by = yii::app()->user->id;
        if ($model->save()) {
            $response = array('status' => 1, 'message' => "Succcessfully Approved");
        } else {
            $response = array('status' => 0, 'message' => 'An error occured');
        }
        echo json_encode($response);
    }
    public function actionreject_mr()
    {

        if (isset($_POST['reason'])) {

            $id = $_POST['mr_id'];
            $model = MaterialRequisition::model()->findByPk($id);
            $model->requisition_status = 2;
            $model->reject_reason = $_POST['reason'];
            $model->approved_rejected_by = yii::app()->user->id;
            if ($model->save()) {
                $response = array('status' => 1, 'message' => "Rejected Succcessfully");
            } else {
                $response = array('status' => 0, 'message' => "Something went wrong");
            }
            echo  json_encode($response);
            exit;
        }


        $result = $this->renderPartial('_reject_mr', array(
            'id' => $_POST['id']
        ), true);
        echo json_encode($result);
    }
}
