<?php

class AttendanceRequestController extends Controller
{
		/**
		 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
		 * using two-column layout. See 'protected/views/layouts/column2.php'.
		 */
		public $layout = '//layouts/column2';
	
		/**
		 * @return array action filters
		 */
		public function filters() {
			return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
			);
		}
	
		/**
		 * Specifies the access control rules.
		 * This method is used by the 'accessControl' filter.
		 * @return array access control rules
		 */
		public function accessRules() {
	
	
		  $accessArr = array();
		  $accessauthArr = array();
		  $accessguestArr = array();
		  $controller = Yii::app()->controller->id;
		  if(isset(Yii::app()->session['pmsmenuall'])){
			  if(array_key_exists($controller, Yii::app()->session['pmsmenuall'])){
				  $accessArr = Yii::app()->session['pmsmenuall'][$controller];
			  }
		  }
	
		  if(isset(Yii::app()->session['pmsmenuauth'])){
			  if(array_key_exists($controller, Yii::app()->session['pmsmenuauth'])){
				  $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
			  }
		  }
	
		  if (isset(Yii::app()->session['pmsmenuguest'])) {
			  if(array_key_exists($controller, Yii::app()->session['pmsmenuguest'])){
				  $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
			  }
		  }
		  $access_privlg = count($accessauthArr);
	
		  return array(
			  array(
				  'allow',
				  'actions' => $accessArr,
				  'users' => array('@'),
				//  'expression' => "$access_privlg > 0",
	
			  ),
			  array(
				  'allow',
				  'actions' => $accessauthArr,
				  'users' => array('@'),
				  'expression' => "$access_privlg > 0",
	
			  ),
			  array('allow',
				  'actions' => array('attendanceaction','Approved','Pending','Rejected'),
				  'users' => array('@'),
			  ),
	
			  array(
				  'deny',
				  'users' => array('*'),
			  ),
		  );
	
		  // return array(
		  //       array('allow', // allow authenticated user to perform 'create' and 'update' actions
		  //           'actions' => array('create', 'update', 'index', 'view', 'ShiftAction'),
		  //           'users' => array('@'),
		  //       ),
		  //       array('allow', // allow admin user to perform 'admin' and 'delete' actions
		  //           'actions' => array('admin', 'delete'),
		  //           'users' => array('admin'),
		  //       ),
		  //       array('deny', // deny all users
		  //           'users' => array('*'),
		  //       ),
		  //   );
		}
	
		/**
		 * Displays a particular model.
		 * @param integer $id the ID of the model to be displayed
		 */
		public function actionView($id) {
			$this->render('view', array(
				'model' => $this->loadModel($id),
			));
		}
	
		/**
		 * Creates a new model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 */
		public function actionCreate() {
			$model = new AttendanceRequests;
	
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
	
			if (isset($_POST['AttendanceRequests'])) {
				$model->attributes = $_POST['AttendanceRequests'];
				if ($model->save())
					$this->redirect(array('view', 'id' => $model->att_id));
			}
	
			$this->render('create', array(
				'model' => $model,
			));
		}
	
		/**
		 * Updates a particular model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 * @param integer $id the ID of the model to be updated
		 */
		public function actionUpdate($id) {
			$model = $this->loadModel($id);
	
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
	
			if (isset($_POST['AttendanceRequests'])) {
				$model->attributes = $_POST['AttendanceRequests'];
				if ($model->save())
					$this->redirect(array('view', 'id' => $model->att_id));
			}
	
			$this->render('update', array(
				'model' => $model,
			));
		}
	
		/**
		 * Deletes a particular model.
		 * If deletion is successful, the browser will be redirected to the 'admin' page.
		 * @param integer $id the ID of the model to be deleted
		 */
		public function actionDelete($id) {
			$this->loadModel($id)->delete();
	
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	
		/**
		 * Lists all models.
		 */
		public function actionIndex() {
	
			$model1 = new AttendanceRequests;
			
			if (isset($_GET['reportpagesize'])) {
				// pageSize will be set on user's state
				Yii::app()->user->setState('reportpagesize', (int) $_GET['reportpagesize']);
				// unset the parameter as it
				// would interfere with pager
				// and repetitive page size change
				unset($_GET['reportpagesize']);
			}
			$this->performAjaxValidation($model1);
			$skill="";
			$designation_id="";
	
			if (isset($_POST['AttendanceRequests'])) {
	
				$model1->attributes = $_POST['AttendanceRequests'];
	
				$user_id = $_POST['AttendanceRequests']['user_id'];
				$att_date = $_POST['AttendanceRequests']['att_date'];
				$checksql = "SELECT *  FROM pms_attendance_requests WHERE `user_id` = ".$user_id." and att_date ='".$att_date."' and status in (0,1)";
				$check    = Yii::app()->db->createCommand($checksql)->queryRow();
	
				// if(!empty($check)){
				//   Yii::app()->user->setFlash('error', "Request already exist");
	
				// }else{
	
				  $model1->created_by = Yii::app()->user->id;
				  $model1->created_date = date('Y-m-d H:i:s');
				  
				  if ($model1->save()) {
					$att_arr[] =  "(NULL,'{$model1->user_id}','{$model1->att_date}','{$model1->comments}','9',$model1->created_by,'" .$model1->created_date. "','" .$model1->created_date."',".$model1->type.")";
					$att_arr1[] =  "(NULL,'{$model1->user_id}','{$model1->att_date}','{$model1->comments}','{$model1->att_entry}',$model1->created_by,'" .$model1->created_date. "','" .$model1->created_date."',".$model1->type.")";
				   
					$att_arr = implode(',', $att_arr); 
					$att_arr1 = implode(',', $att_arr1); 
					Yii::app()->db->createCommand("Replace INTO `pms_attendance`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  ".$att_arr." ")->query();
					Yii::app()->db->createCommand("INSERT INTO `pms_attendance_reject_log`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  ".$att_arr1." ")->query();
				   
					$this->sendEmail($model1->att_id);
					  $this->redirect(array('index'));
				//   }
	
				}
			}
	
			$model = new AttendanceRequests('search');
			$model->unsetAttributes();  // clear any default values
			if (isset($_GET['AttendanceRequests']))
				$model->attributes = $_GET['AttendanceRequests'];
	
			if(isset($_REQUEST['skill'])){
			$skill=$_REQUEST['skill'];
			}
			if(isset($_REQUEST['designation'])){
			$designation_id=$_REQUEST['designation'];
			}
	
			$this->render('index', array(
				'model' => $model, 'model1' => $model1,'skill'=>$skill,'designation_id'=>$designation_id
			));
		}
	
		public function actionApproved()
		{
			$skill="";
			$designation_id="";
			$model1 = new AttendanceRequests;
			if (isset($_GET['reportpagesize'])) {
				// pageSize will be set on user's state
				Yii::app()->user->setState('reportpagesize', (int) $_GET['reportpagesize']);
				// unset the parameter as it
				// would interfere with pager
				// and repetitive page size change
				unset($_GET['reportpagesize']);
			}
			$this->performAjaxValidation($model1);
			$model = new AttendanceRequests('search');
			$model->unsetAttributes();  // clear any default values
			if (isset($_GET['AttendanceRequests']))
				$model->attributes = $_GET['AttendanceRequests'];
	
				if(isset($_REQUEST['skill'])){
					$skill=$_REQUEST['skill'];
					}
					if(isset($_REQUEST['designation'])){
					$designation_id=$_REQUEST['designation'];
					}
			$this->render('approved', array(
				'model' => $model, 'model1' => $model1,'skill'=>$skill,'designation_id'=>$designation_id
		
			));
			
		}
	
		public function actionPending()
		{
			$skill="";
			$designation_id="";
	
			$model1 = new AttendanceRequests;
			if (isset($_GET['reportpagesize'])) {
				// pageSize will be set on user's state
				Yii::app()->user->setState('reportpagesize', (int) $_GET['reportpagesize']);
				// unset the parameter as it
				// would interfere with pager
				// and repetitive page size change
				unset($_GET['reportpagesize']);
			}
			$this->performAjaxValidation($model1);
			$model = new AttendanceRequests('search');
			$model->unsetAttributes();  // clear any default values
			if (isset($_GET['AttendanceRequests']))
				$model->attributes = $_GET['AttendanceRequests'];
	
				if(isset($_REQUEST['skill'])){
					$skill=$_REQUEST['skill'];
					}
					if(isset($_REQUEST['designation'])){
					$designation_id=$_REQUEST['designation'];
					}
			$this->render('pending', array(
				'model' => $model, 'model1' => $model1,'skill'=>$skill,'designation_id'=>$designation_id
		
			));
			
		}
	
	
		public function actionRejected()
		{
	
			$skill="";
			$designation_id="";
			$model1 = new AttendanceRequests;
			if (isset($_GET['reportpagesize'])) {
				// pageSize will be set on user's state
				Yii::app()->user->setState('reportpagesize', (int) $_GET['reportpagesize']);
				// unset the parameter as it
				// would interfere with pager
				// and repetitive page size change
				unset($_GET['reportpagesize']);
			}
			$this->performAjaxValidation($model1);
			$model = new AttendanceRequests('search');
			$model->unsetAttributes();  // clear any default values
			if (isset($_GET['AttendanceRequests']))
				$model->attributes = $_GET['AttendanceRequests'];
	
				if(isset($_REQUEST['skill'])){
					$skill=$_REQUEST['skill'];
					}
					if(isset($_REQUEST['designation'])){
					$designation_id=$_REQUEST['designation'];
					}
			$this->render('rejected', array(
				'model' => $model, 'model1' => $model1,'skill'=>$skill,'designation_id'=>$designation_id
		
			));
			
		}
	
		public function sendEmail($id = 0) {
	
			$attdata = AttendanceRequests::model()->findByPk($id);
	
			$userdetails = Users::model()->findByPk($attdata['user_id']);
			$createdetails = Users::model()->findByPk(Yii::app()->user->id);
			$settings = GeneralSettings::model()->findByPk(1);
			$subject = 'Manual Attendance Request';
			$name = $userdetails['firstname'] . ' ' . $userdetails['lastname'];
			$from_name = $createdetails['firstname'] . ' ' . $createdetails['lastname'];
			$var = array('{user}', '{from_name}');
			$data = array($name, $from_name);
			$mail = new JPhpMailer();
			$message = str_replace($var, $data, nl2br($settings['mail_temp_att']));
			$body = $this->renderPartial('email', array('message' => $message), true);
	
	
			if ($this->paramsval('email_notify')) {
	
				if ($_SERVER['HTTP_HOST'] != 'localhost') {
	
					$mail->IsSMTP();
					$mail->Host = $settings->smtp_host;
					$mail->SMTPSecure = $settings->smtp_secure;
					$mail->SMTPAuth = $settings->smtp_auth;
					$mail->Username = $settings->smtp_username;
					$mail->Password = $settings->smtp_password;
					$mail->Port       = $settings->smtp_port;
	
					$mail->setFrom($settings->smtp_email_from, 'Aquatech');
				} else {
					$smtpdetails = $this->paramsval('smtpmailconfig');
					extract($smtpdetails);
					$mail->IsSMTP();
					$mail->Host = $mailHost;
					$mail->SMTPAuth = $mailSMTPAuth;
					$mail->Username = $mailUsername;
					$mail->Password = $mailPassword;
					$mail->SMTPSecure = $mailSMTPSecure;
					$mail->setFrom($mailsetFrom, $from_name);
				}
	
				$maindata = Users::model()->findByPk(1);
				$to_email = $maindata['personal_emailid'];
	
				$mail->addAddress($to_email);  //Add a recipient
				$mail->isHTML(true);
				$mail->Subject = $subject;
				$mail->Body = $body;
				$mail->send();
			}
		}
	
		/**
		 * Manages all models.
		 */
		public function actionAdmin() {
			$model = new AttendanceRequests('search');
			$model->unsetAttributes();  // clear any default values
			if (isset($_GET['AttendanceRequests']))
				$model->attributes = $_GET['AttendanceRequests'];
	
			$this->render('admin', array(
				'model' => $model,
			));
		}
	
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return AttendanceRequests the loaded model
		 * @throws CHttpException
		 */
		public function loadModel($id) {
			$model = AttendanceRequests::model()->findByPk($id);
			if ($model === null)
				throw new CHttpException(404, 'The requested page does not exist.');
			return $model;
		}
	
		/**
		 * Performs the AJAX validation.
		 * @param AttendanceRequests $model the model to be validated
		 */
		protected function performAjaxValidation($model) {
			if (isset($_POST['ajax']) && $_POST['ajax'] === 'attendance-requests-form') {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
	
		public function actionShiftAction() {
	
			$atttbl = AttendanceRequests::model()->tableSchema->rawName;
	
			if (isset($_REQUEST['id'])) {
				extract($_REQUEST);
	
	
				$status = array('Approve' => 1, 'Reject' => 2);
				if ($req) {
					$status = $status[$req];
					$reason=$_REQUEST['reason'];
	
					if ($status == 1) {
						$finalstatus = 'Approved';
					} else {
						$finalstatus = 'Rejected';
					}
	
					Yii::app()->db->createCommand("UPDATE $atttbl SET `reason`='{$reason}' ,`status`=" . $status . ",`decision_by`=" . Yii::app()->user->id . ",`decision_date`='" . date('Y-m-d') . "' WHERE `att_id`=" . $id)->execute();
					if ($status == 1) {
	
						$data = AttendanceRequests::model()->findByPk($id);
						Yii::app()->db->createCommand("Replace INTO pms_attendance (`att_id`, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES(NULL, " . $data['user_id'] . ", '" . $data['att_date'] . "', '" . $data['comments'] . "',  " . $data['att_entry'] . ", " . $data['created_by'] . ", '" . $data['created_date'] . "', '" . $data['decision_date'] . "'," . $data['type'] . ")")->execute();
					}
					else{
	
						$data = AttendanceRequests::model()->findByPk($id);
						$updatesql = "UPDATE  `pms_attendance_reject_log` SET `pending_status`='1' WHERE `user_id` =".$data['user_id']." and `att_date` ='".$data['att_date']."' ORDER BY att_id DESC LIMIT 1";
						Yii::app()->db->createCommand($updatesql)->query();
						Yii::app()->db->createCommand("Replace INTO pms_attendance (`att_id`, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES(NULL, " . $data['user_id'] . ", '" . $data['att_date'] . "', '" . $data['comments'] . "',  " . $data['att_entry'] . ", " . $data['created_by'] . ", '" . $data['created_date'] . "', '" . $data['decision_date'] . "'," . $data['type'] . ")")->execute();
						$updated_attendance_id=  Yii::app()->db->getLastInsertId();
						$sql = "SELECT * FROM `pms_attendance_reject_log` WHERE `user_id` =".$data['user_id']." and `att_date` ='".$data['att_date']."' ORDER BY att_id DESC LIMIT 1,1";
						$check   = Yii::app()->db->createCommand($sql)->queryRow();
						if($check['att_id']!="")
						{
							if($check['pending_status']=='1'){
								$delete_sql="DELETE FROM `pms_attendance` WHERE `att_id`=$updated_attendance_id";
								Yii::app()->db->createCommand($delete_sql)->query();
								
							}
							else{
								$sql = "SELECT * FROM `pms_attendance_reject_log` WHERE `user_id` =".$data['user_id']." and `att_date` ='".$data['att_date']."' ORDER BY att_id DESC LIMIT 1,1";
								$log_value   = Yii::app()->db->createCommand($sql)->queryRow();
								$entry=$log_value['att_entry'];
								$type=$log_value['type'];
								$updatesql = "UPDATE `pms_attendance` SET `att_entry`=".$entry.",`type`=2  where `att_id`= $updated_attendance_id";
								Yii::app()->db->createCommand($updatesql)->query();
							}
						}
						else{
							$delete_sql="DELETE FROM `pms_attendance` WHERE `att_id`=$updated_attendance_id";
							Yii::app()->db->createCommand($delete_sql)->query();
								
						}
									
					}
	
					$ret['msg'] = 'Manual Entry ' . $finalstatus . ' Successfully';
					$ret['error'] = '';
				}
				echo json_encode($ret);
				exit;
			}
		}
	
		public function actionattendanceaction(){
	
			$atttbl = AttendanceRequests::model()->tableSchema->rawName;
			if(isset($_REQUEST['id'])) {
				extract($_REQUEST);
				$status = array('Approve' => 1, 'Reject' => 2);
				if ($req) {
				   $sts    = $status[$req];
				   if ($sts == 1) {
					   $finalstatus = 'Approved';
				   } else {
					   $finalstatus = 'Rejected';
				   }
	
				   $reason = $_REQUEST['reason'];
				   $allids = $_REQUEST['id'];
	
				   foreach ($allids as $key => $id) {
					  Yii::app()->db->createCommand("UPDATE $atttbl SET `status`=" . $sts . ",`decision_by`=" . Yii::app()->user->id . ",`decision_date`='" . date('Y-m-d') . "' , reason='".$reason."' WHERE `att_id`=" . $id)->execute();
					  if ($sts == 1) {
	
						  $data = AttendanceRequests::model()->findByPk($id);
						  Yii::app()->db->createCommand("Replace INTO pms_attendance (`att_id`, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES(NULL, " . $data['user_id'] . ", '" . $data['att_date'] . "', '" . $data['comments'] . "',  " . $data['att_entry'] . ", " . $data['created_by'] . ", '" . $data['created_date'] . "', '" . $data['decision_date'] . "'," . $data['type'] . ")")->execute();
					  }
					  else{
	
						$data = AttendanceRequests::model()->findByPk($id);
						$updatesql = "UPDATE  `pms_attendance_reject_log` SET `pending_status`='1' WHERE `user_id` =".$data['user_id']." and `att_date` ='".$data['att_date']."' ORDER BY att_id DESC LIMIT 1";
						Yii::app()->db->createCommand($updatesql)->query();
						Yii::app()->db->createCommand("Replace INTO pms_attendance (`att_id`, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES(NULL, " . $data['user_id'] . ", '" . $data['att_date'] . "', '" . $data['comments'] . "',  " . $data['att_entry'] . ", " . $data['created_by'] . ", '" . $data['created_date'] . "', '" . $data['decision_date'] . "'," . $data['type'] . ")")->execute();
						$updated_attendance_id=  Yii::app()->db->getLastInsertId();
						$sql = "SELECT * FROM `pms_attendance_reject_log` WHERE `user_id` =".$data['user_id']." and `att_date` ='".$data['att_date']."' ORDER BY att_id DESC LIMIT 1,1";
						$check   = Yii::app()->db->createCommand($sql)->queryRow();
						if($check['att_id']!="")
						{
							if($check['pending_status']=='1'){
								$delete_sql="DELETE FROM `pms_attendance` WHERE `att_id`=$updated_attendance_id";
								Yii::app()->db->createCommand($delete_sql)->query();
								
							}
							else{
								$sql = "SELECT * FROM `pms_attendance_reject_log` WHERE `user_id` =".$data['user_id']." and `att_date` ='".$data['att_date']."' ORDER BY att_id DESC LIMIT 1,1";
								$log_value   = Yii::app()->db->createCommand($sql)->queryRow();
								$entry=$log_value['att_entry'];
								$type=$log_value['type'];
								$updatesql = "UPDATE `pms_attendance` SET `att_entry`=".$entry.",`type`=".$type." where `att_id`= $updated_attendance_id";
								Yii::app()->db->createCommand($updatesql)->query();
							}
						}
						else{
							$delete_sql="DELETE FROM `pms_attendance` WHERE `att_id`=$updated_attendance_id";
							Yii::app()->db->createCommand($delete_sql)->query();
								
						}
									
					}
					  
				   }
	
				   $ret['msg'] = 'Manual Entry ' . $finalstatus . ' Successfully';
				   $ret['error'] = '';
				}
	
	
				echo json_encode($ret);  exit;
			}
	
	
		}
	
	}
	