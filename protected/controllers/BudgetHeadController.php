<?php
Yii::app()->getModule('masters');
class BudgetHeadController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'getmodel', 'getbudgetheaddates', 'delete'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    // public function actionCreate()
    // {
    // 	$model=new BudgetHead;

    // 	// Uncomment the following line if AJAX validation is needed
    // 	// $this->performAjaxValidation($model);

    // 	if(isset($_POST['BudgetHead']))
    // 	{
    // 		$model->attributes=$_POST['BudgetHead'];
    // 		if($model->save())
    // 			$this->redirect(array('view','id'=>$model->budget_head_id));
    // 	}

    // 	$this->render('create',array(
    // 		'model'=>$model,
    // 	));
    // }

    public function actionCreate()
    {
        $project_drpdwn_html = '';
        $this->layout = '//layouts/iframe';
        $model = new BudgetHead;
        if (isset($_POST['BudgetHead']['id']) && !empty($_POST['BudgetHead']['id'])) {
            $id = $_POST['BudgetHead']['id'];
            $model = $this->loadModel($id);
        } else {
            $model->setScenario(('create'));
        }
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_POST['BudgetHead'])) {

            $model->attributes = $_POST['BudgetHead'];




            if ($model->start_date != "") {
                $model->start_date = date('Y-m-d', strtotime($model->start_date));
            } else {
                $model->start_date = NULL;
            }
            if ($model->end_date != "") {
                $model->end_date = date('Y-m-d', strtotime($model->end_date));
            } else {
                $model->end_date = NULL;
            }




            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');

            if ($model->save()) {

                $budget_head_condition = "status = 1 AND project_id = " . $model->project_id;
                $budget_head_data = BudgetHead::model()->findAll(array(
                    'condition' => $budget_head_condition,
                    'order' => 'budget_head_title ASC',
                    'distinct' => true
                ));
                $budget_head_result = CHtml::listData($budget_head_data, 'budget_head_id', 'budget_head_title');
                $budget_head_html = "<option value=''>Choose a Budget Head</option>";

                $budget_head_start_date = date('d-M-y', strtotime($model->start_date));
                $budget_head_end_date = date('d-M-y', strtotime($model->end_date));

                foreach ($budget_head_result as $budget_head_key => $budget_head_value) {
                    $budget_head_html .= "<option value=" . $budget_head_key . ">" . $budget_head_value . "</option>";
                }
                if (isset($_POST['submit_type'])) {
                    if ($_POST['submit_type'] == 'save_btn') {
                        $return_result = array('status' => 1, 'next_level' => 0, 'budget_head_html' => $budget_head_html);
                    } elseif ($_POST['submit_type'] == 'save_cnt') {

                        $project_drpdwn_html .= "<option value=" . $model->project_id . ">" . $model->project->name . "</option>";
                        $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html, 'budget_head_html' => $budget_head_html, 'budget_head_start_date' => $budget_head_start_date, 'budget_head_end_date' => $budget_head_end_date);
                    } else {
                        $return_result = array('status' => 1, 'next_level' => 0, 'budget_head_start_date' => $budget_head_start_date, 'budget_head_end_date' => $budget_head_end_date);
                    }
                    echo json_encode($return_result);
                    exit;
                }
            } else {
                print_r($model->getErrors());
                exit;
                $return_result = array('status' => 112, 'next_level' => 0);
                echo json_encode($return_result);
                exit;
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['BudgetHead'])) {
            $model->attributes = $_POST['BudgetHead'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->budget_head_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */

    public function actionDelete($id)
    {
        // $this->loadModel($id)->delete();

        // // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        // if (!isset($_GET['ajax']))
        // 	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();

        try {

            $budget_head_used = Milestone::model()->findByAttributes(array(
                'budget_id' => $id,

            ));

            if ($budget_head_used) {
                $success_status = 2;
            } else {
                if (!$model->delete()) {
                    $success_status = 0;
                    throw new Exception(json_encode($model->getErrors()));
                } else {
                    $success_status = 1;
                }
            }


            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else if ($success_status == 2) {
                echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {

                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('BudgetHead');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new BudgetHead('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['BudgetHead']))
            $model->attributes = $_GET['BudgetHead'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = BudgetHead::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'budget-head-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actiongetModel()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = BudgetHead::model()->findByPk($id);
            if (!empty($model)) {
                $start_date = "";
                $end_date = "";
                if ($model->start_date != "") {
                    $start_date = date('d-M-y', strtotime($model->start_date));
                }
                if ($model->end_date != "") {
                    $end_date = date('d-M-y', strtotime($model->end_date));
                }

                $model_array = array('stat' => 1, 'id' => $model->budget_head_id, 'budget_head_title' => $model->budget_head_title, 'ranking' => $model->ranking, 'status' => $model->status, 'start_date' => $start_date, 'end_date' => $end_date);
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo  json_encode($model_array);
        exit;
    }

    public function actiongetBudgetHeadDates()
    {
        if (isset($_POST['id'])) {
            $budget_head_id = $_POST['id'];
            $budget_head_model = $this->loadModel($budget_head_id);
            if (!empty($budget_head_model)) {
                $result_array = array('status' => '1', 'start' => date('d-M-y', strtotime($budget_head_model['start_date'])), 'end' => date('d-M-y', strtotime($budget_head_model['end_date'])));
            } else {
                $result_array = array('status' => '2');
            }

            echo json_encode($result_array);
        }
    }
}
