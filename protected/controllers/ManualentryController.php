<?php

class ManualEntryController extends Controller {

    public $allpunchs = array();
    public $getpunches = false;
    public $punchresult = array();
    public $empdevid = array();
    public $layout = '//layouts/column2';
    public $employees = array();
    public $punches = array();
    public $pdate = '';  //Punchdate
    public $numdays = 0;
    public $devices = '';
    public $interchange = array();

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->session['pmsmenuall'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuall'])) {
                $accessArr = Yii::app()->session['pmsmenuall'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuauth'])) {
                $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuguest'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuguest'])) {
                $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
            }
        }
        $access_privlg = count($accessauthArr);


        return array(
            array(
                'allow',
                'actions' => $accessArr,
                'users' => array('@'),
            //'expression' => "$access_privlg > 0",
            ),
            array(
                'allow',
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array('allow',
                'actions' => array('manualentryaction', 'Approved', 'Rejected', 'Pending'),
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new ManualEntry;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ManualEntry'])) {
            $model->attributes = $_POST['ManualEntry'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->entry_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ManualEntry'])) {
            $model->attributes = $_POST['ManualEntry'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->entry_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new ManualEntry('search');
        if (isset(Yii::app()->user->project_site) && isset(Yii::app()->user->site_userids) && !empty(Yii::app()->user->site_userids)) {
            $filtered_employee = Yii::app()->user->site_userids;
        } else {
            $filtered_employee = "";
        }
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ManualEntry']))
            $model->attributes = $_GET['ManualEntry'];

        $company = '';

        if ($filtered_employee != "") {
            $my_query = "WHERE pms_manual_entry.emp_id IN (" . $filtered_employee . ")";
        } else {
            $my_query = "";
        }

        $entry = array();
        $sql = "SELECT pms_manual_entry.* 
		FROM `pms_manual_entry`  left join pms_employee 
		on pms_employee.emp_id = pms_manual_entry.emp_id " . $company
                . $my_query . " order by `status` asc ";

        $manualdata = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($manualdata as $key => $value) {
            $entry[$key]['entry_id'] = $value['entry_id'];
            $entry[$key]['emp_id'] = $value['emp_id'];
            $entry[$key]['date'] = $value['date'];
            $entry[$key]['comment'] = $value['comment'];
            $entry[$key]['status'] = $value['status'];
            $entry[$key]['type'] = 'm';
            $entry[$key]['created_by'] = $value['created_by'];
            $entry[$key]['decision_by'] = $value['decision_by'];
        }

        if ($filtered_employee != "") {
            $my_query = "WHERE userid IN (" . $filtered_employee . ")";
        } else {
            $my_query = "";
        }

        $a = count($manualdata) + 1;
        $sql = "SELECT pms_ignore_punches.* 
			FROM `pms_ignore_punches` left join pms_employee 
			on pms_employee.emp_id = pms_ignore_punches.userid  " . $company
                . $my_query . " order by `ig_id` desc";

        $manualdata1 = Yii::app()->db->createCommand($sql)->queryAll();



        foreach ($manualdata1 as $value) {

            $entry[$a]['entry_id'] = $value['ig_id'];
            $entry[$a]['emp_id'] = $value['userid'];
            $entry[$a]['date'] = $value['date'];
            $entry[$a]['comment'] = $value['comment'];
            $entry[$a]['status'] = $value['status'];
            $entry[$a]['type'] = 'i';
            $entry[$a]['created_by'] = $value['created_by'];
            $entry[$a]['decision_by'] = $value['decision_by'];

            $a++;
        }

        if ($filtered_employee != "") {
            $my_query = "WHERE pms_shift_entry.emp_id IN (" . $filtered_employee . ")";
        } else {
            $my_query = "";
        }

        $new = count($manualdata1) + 1;
        $b = $a + $new;
        $sql = "SELECT pms_shift_entry.* FROM `pms_shift_entry`
		 left join pms_employee 
		 on pms_employee.emp_id = pms_shift_entry.emp_id " . $company
                . $my_query . "  order by `shiftentry_id` desc ";
        $shiftdata = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($shiftdata as $value) {

            $entry[$b]['entry_id'] = $value['shiftentry_id'];
            $entry[$b]['emp_id'] = $value['emp_id'];
            $entry[$b]['date'] = $value['date'];
            $entry[$b]['comment'] = $value['comment'];
            $entry[$b]['status'] = $value['status'];
            $entry[$b]['type'] = 's';
            $entry[$b]['created_by'] = $value['created_by'];
            $entry[$b]['decision_by'] = $value['decision_by'];
            $b++;
        }

        function date_compare($a, $b) {
            $t1 = strtotime($a['date']);
            $t2 = strtotime($b['date']);
            return $t2 - $t1;
        }

        function status_sort($a, $b) {
            $t1 = strtotime($a['status']);
            $t2 = strtotime($b['status']);
            return $t2 - $t1;
        }

        // print_r($entry);
        // usort($entry, 'date_compare');
        // print_r($entry); exit;

        $status = array_column($entry, 'status');

        array_multisort($status, SORT_ASC, $entry);

        // arsort($entry); 



        $dataprovider = new CArrayDataProvider($entry, array(
            'id' => 'entry_id',
            'keyField' => 'entry_id',
            //'pagination' => false,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));




        $this->render('index', array(
            'model' => $model, 'dataprovider' => $dataprovider,
        ));
    }

    public function actionApproved() {

        $model = new ManualEntry('search');

        if (isset(Yii::app()->user->project_site) && isset(Yii::app()->user->site_userids) &&
                !empty(Yii::app()->user->site_userids)) {
            $filtered_employee = Yii::app()->user->site_userids;
        } else {
            $filtered_employee = "";
        }


        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ManualEntry']))
            $model->attributes = $_GET['ManualEntry'];



        $company = '';


        if ($filtered_employee != "") {
            $my_query = " pms_manual_entry.emp_id IN (" . $filtered_employee . ") and ";
        } else {
            $my_query = "";
        }

        $entry = array();
        $sql = "SELECT pms_manual_entry.* 
		FROM `pms_manual_entry`  left join pms_employee 
		on pms_employee.emp_id = pms_manual_entry.emp_id " . $company
                . $my_query . " and pms_manual_entry.status=1 order by `status` asc ";
        $manualdata = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($manualdata as $key => $value) {

            $entry[$key]['entry_id'] = $value['entry_id'];
            $entry[$key]['emp_id'] = $value['emp_id'];
            $entry[$key]['date'] = $value['date'];
            $entry[$key]['comment'] = $value['comment'];
            $entry[$key]['status'] = $value['status'];
            $entry[$key]['type'] = 'm';
            $entry[$key]['created_by'] = $value['created_by'];
            $entry[$key]['decision_by'] = $value['decision_by'];
        }

        if ($filtered_employee != "") {
            $my_query = "WHERE userid IN (" . $filtered_employee . ")";
        } else {
            $my_query = "";
        }

        $a = count($manualdata) + 1;
        $sql = "SELECT pms_ignore_punches.* 
			FROM `pms_ignore_punches` left join pms_employee 
			on pms_employee.emp_id = pms_ignore_punches.userid  " . $company
                . $my_query . " and pms_ignore_punches.status=1 order by `ig_id` desc";
        $manualdata1 = Yii::app()->db->createCommand($sql)->queryAll();



        foreach ($manualdata1 as $value) {

            $entry[$a]['entry_id'] = $value['ig_id'];
            $entry[$a]['emp_id'] = $value['userid'];
            $entry[$a]['date'] = $value['date'];
            $entry[$a]['comment'] = $value['comment'];
            $entry[$a]['status'] = $value['status'];
            $entry[$a]['type'] = 'i';
            $entry[$a]['created_by'] = $value['created_by'];
            $entry[$a]['decision_by'] = $value['decision_by'];

            $a++;
        }

        if ($filtered_employee != "") {
            $my_query = "WHERE pms_shift_entry.emp_id IN (" . $filtered_employee . ")";
        } else {
            $my_query = "";
        }

        $new = count($manualdata1) + 1;
        $b = $a + $new;
        $sql = "SELECT pms_shift_entry.* FROM `pms_shift_entry`
		 left join pms_employee 
		 on pms_employee.emp_id = pms_shift_entry.emp_id " . $company
                . $my_query . "  order by `shiftentry_id` desc ";
        $shiftdata = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($shiftdata as $value) {

            $entry[$b]['entry_id'] = $value['shiftentry_id'];
            $entry[$b]['emp_id'] = $value['emp_id'];
            $entry[$b]['date'] = $value['date'];
            $entry[$b]['comment'] = $value['comment'];
            $entry[$b]['status'] = $value['status'];
            $entry[$b]['type'] = 's';
            $entry[$b]['created_by'] = $value['created_by'];
            $entry[$b]['decision_by'] = $value['decision_by'];
            $b++;
        }

        function date_compare1($a, $b) {
            $t1 = strtotime($a['date']);
            $t2 = strtotime($b['date']);
            return $t2 - $t1;
        }

        function status_sort1($a, $b) {
            $t1 = strtotime($a['status']);
            $t2 = strtotime($b['status']);
            return $t2 - $t1;
        }

        // print_r($entry);
        // usort($entry, 'date_compare');
        // print_r($entry); exit;

        $status = array_column($entry, 'status');

        array_multisort($status, SORT_ASC, $entry);

        // arsort($entry); 



        $dataprovider = new CArrayDataProvider($entry, array(
            'id' => 'entry_id',
            'keyField' => 'entry_id',
            //'pagination' => false,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));

        $this->render('approved', array(
            'model' => $model, 'dataprovider' => $dataprovider,
        ));
    }

    public function actionRejected() {
        $model = new ManualEntry('search');
        if (isset(Yii::app()->user->project_site) && isset(Yii::app()->user->site_userids) && !empty(Yii::app()->user->site_userids)) {
            $filtered_employee = Yii::app()->user->site_userids;
        } else {
            $filtered_employee = "";
        }


        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ManualEntry']))
            $model->attributes = $_GET['ManualEntry'];



        $company = '';


        if ($filtered_employee != "") {
            $my_query = " pms_manual_entry.emp_id IN (" . $filtered_employee . ") and ";
        } else {
            $my_query = "";
        }

        $entry = array();
        $sql = "SELECT pms_manual_entry.* 
		FROM `pms_manual_entry`  left join pms_employee 
		on pms_employee.emp_id = pms_manual_entry.emp_id " . $company
                . ' where ' . $my_query . " pms_manual_entry.status=2 order by `status` asc ";
        $manualdata = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($manualdata as $key => $value) {

            $entry[$key]['entry_id'] = $value['entry_id'];
            $entry[$key]['emp_id'] = $value['emp_id'];
            $entry[$key]['date'] = $value['date'];
            $entry[$key]['comment'] = $value['comment'];
            $entry[$key]['status'] = $value['status'];
            $entry[$key]['type'] = 'm';
            $entry[$key]['created_by'] = $value['created_by'];
            $entry[$key]['decision_by'] = $value['decision_by'];
        }

        if ($filtered_employee != "") {
            $my_query = " userid IN (" . $filtered_employee . ") and ";
        } else {
            $my_query = "";
        }

        $a = count($manualdata) + 1;
        $sql = "SELECT pms_ignore_punches.* 
			FROM `pms_ignore_punches` left join pms_employee 
			on pms_employee.emp_id = pms_ignore_punches.userid  " . $company
                . ' where ' . $my_query . "  pms_ignore_punches.status=2 order by `ig_id` desc";
        $manualdata1 = Yii::app()->db->createCommand($sql)->queryAll();



        foreach ($manualdata1 as $value) {

            $entry[$a]['entry_id'] = $value['ig_id'];
            $entry[$a]['emp_id'] = $value['userid'];
            $entry[$a]['date'] = $value['date'];
            $entry[$a]['comment'] = $value['comment'];
            $entry[$a]['status'] = $value['status'];
            $entry[$a]['type'] = 'i';
            $entry[$a]['created_by'] = $value['created_by'];
            $entry[$a]['decision_by'] = $value['decision_by'];

            $a++;
        }

        if ($filtered_employee != "") {
            $my_query = " where pms_shift_entry.emp_id IN (" . $filtered_employee . ") ";
        } else {
            $my_query = "";
        }

        $new = count($manualdata1) + 1;
        $b = $a + $new;
        $sql = "SELECT pms_shift_entry.* FROM `pms_shift_entry`
		 left join pms_employee 
		 on pms_employee.emp_id = pms_shift_entry.emp_id " . $company
                . $my_query . "  order by `shiftentry_id` desc ";
        $shiftdata = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($shiftdata as $value) {

            $entry[$b]['entry_id'] = $value['shiftentry_id'];
            $entry[$b]['emp_id'] = $value['emp_id'];
            $entry[$b]['date'] = $value['date'];
            $entry[$b]['comment'] = $value['comment'];
            $entry[$b]['status'] = $value['status'];
            $entry[$b]['type'] = 's';
            $entry[$b]['created_by'] = $value['created_by'];
            $entry[$b]['decision_by'] = $value['decision_by'];
            $b++;
        }

        function date_compare2($a, $b) {
            $t1 = strtotime($a['date']);
            $t2 = strtotime($b['date']);
            return $t2 - $t1;
        }

        function status_sort2($a, $b) {
            $t1 = strtotime($a['status']);
            $t2 = strtotime($b['status']);
            return $t2 - $t1;
        }

        // print_r($entry);
        // usort($entry, 'date_compare');
        // print_r($entry); exit;

        $status = array_column($entry, 'status');

        array_multisort($status, SORT_ASC, $entry);

        // arsort($entry); 



        $dataprovider = new CArrayDataProvider($entry, array(
            'id' => 'entry_id',
            'keyField' => 'entry_id',
            //'pagination' => false,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));




        $this->render('rejected', array(
            'model' => $model, 'dataprovider' => $dataprovider,
        ));
    }

    public function actionPending() {

        $model = new ManualEntry('search');

        if (isset(Yii::app()->user->project_site) && isset(Yii::app()->user->site_userids) && !empty(Yii::app()->user->site_userids)) {
            $filtered_employee = Yii::app()->user->site_userids;
        } else {
            $filtered_employee = "";
        }


        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ManualEntry']))
            $model->attributes = $_GET['ManualEntry'];



        $company = '';


        if ($filtered_employee != "") {
            $my_query = " pms_manual_entry.emp_id IN (" . $filtered_employee . ") and ";
        } else {
            $my_query = "";
        }

        $entry = array();
        $sql = "SELECT pms_manual_entry.* 
		FROM `pms_manual_entry`  left join pms_employee 
		on pms_employee.emp_id = pms_manual_entry.emp_id " . $company
                . ' where ' . $my_query . " pms_manual_entry.status=0 order by `status` asc ";
        $manualdata = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($manualdata as $key => $value) {

            $entry[$key]['entry_id'] = $value['entry_id'];
            $entry[$key]['emp_id'] = $value['emp_id'];
            $entry[$key]['date'] = $value['date'];
            $entry[$key]['comment'] = $value['comment'];
            $entry[$key]['status'] = $value['status'];
            $entry[$key]['type'] = 'm';
            $entry[$key]['created_by'] = $value['created_by'];
            $entry[$key]['decision_by'] = $value['decision_by'];
        }

        if ($filtered_employee != "") {
            $my_query = " userid IN (" . $filtered_employee . ") and ";
        } else {
            $my_query = "";
        }

        $a = count($manualdata) + 1;
        $sql = "SELECT pms_ignore_punches.* 
			FROM `pms_ignore_punches` left join pms_employee 
			on pms_employee.emp_id = pms_ignore_punches.userid  " . $company
                . ' where ' . $my_query . "  pms_ignore_punches.status=0 order by `ig_id` desc";
        $manualdata1 = Yii::app()->db->createCommand($sql)->queryAll();



        foreach ($manualdata1 as $value) {

            $entry[$a]['entry_id'] = $value['ig_id'];
            $entry[$a]['emp_id'] = $value['userid'];
            $entry[$a]['date'] = $value['date'];
            $entry[$a]['comment'] = $value['comment'];
            $entry[$a]['status'] = $value['status'];
            $entry[$a]['type'] = 'i';
            $entry[$a]['created_by'] = $value['created_by'];
            $entry[$a]['decision_by'] = $value['decision_by'];

            $a++;
        }

        if ($filtered_employee != "") {
            $my_query = " where pms_shift_entry.emp_id IN (" . $filtered_employee . ") ";
        } else {
            $my_query = "";
        }

        $new = count($manualdata1) + 1;
        $b = $a + $new;
        $sql = "SELECT pms_shift_entry.* FROM `pms_shift_entry`
		 left join pms_employee 
		 on pms_employee.emp_id = pms_shift_entry.emp_id " . $company
                . $my_query . "  order by `shiftentry_id` desc ";
        $shiftdata = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($shiftdata as $value) {

            $entry[$b]['entry_id'] = $value['shiftentry_id'];
            $entry[$b]['emp_id'] = $value['emp_id'];
            $entry[$b]['date'] = $value['date'];
            $entry[$b]['comment'] = $value['comment'];
            $entry[$b]['status'] = $value['status'];
            $entry[$b]['type'] = 's';
            $entry[$b]['created_by'] = $value['created_by'];
            $entry[$b]['decision_by'] = $value['decision_by'];
            $b++;
        }

        function date_compare3($a, $b) {
            $t1 = strtotime($a['date']);
            $t2 = strtotime($b['date']);
            return $t2 - $t1;
        }

        function status_sort3($a, $b) {
            $t1 = strtotime($a['status']);
            $t2 = strtotime($b['status']);
            return $t2 - $t1;
        }

        // print_r($entry);
        // usort($entry, 'date_compare');
        // print_r($entry); exit;

        $status = array_column($entry, 'status');

        array_multisort($status, SORT_ASC, $entry);

        // arsort($entry); 
        // if(count($entry)==0)
        // {
        // 	$entry[1]['entry_id'] = "nothin";
        // 	$entry[1]['emp_id'] = "";
        // 	$entry[1]['date'] = "";
        // 	$entry[1]['comment'] = "";
        // 	$entry[1]['status'] = "";
        // 	$entry[1]['type'] = '';
        // 	$entry[1]['created_by'] = "";
        // 	$entry[1]['decision_by'] = "";
        // }


        $dataprovider = new CArrayDataProvider($entry, array(
            'id' => 'entry_id',
            'keyField' => 'entry_id',
            //'pagination' => false,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));






        $this->render('pending', array(
            'model' => $model, 'dataprovider' => $dataprovider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ManualEntry('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ManualEntry']))
            $model->attributes = $_GET['ManualEntry'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ManualEntry the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = ManualEntry::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ManualEntry $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'manual-entry-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionShiftAction() {

        $connection = Yii::app()->db;
        $manualrentrytbl = ManualEntry::model()->tableSchema->rawName;

        if (isset($_REQUEST['id'])) {
            extract($_REQUEST);

            $status = array('Approve' => 1, 'Reject' => 2);
            if ($req) {
                $status = $status[$req];

                if ($status == 1) {
                    $finalstatus = 'Approved';
                } else {
                    $finalstatus = 'Rejected';
                }
                $reason = $_REQUEST['reason'];
                $connection->createCommand("UPDATE $manualrentrytbl SET `reason`='{$reason}',  `status`=" . $status . ",`decision_by`=" . Yii::app()->user->id . ",`decision_date`='" . date('Y-m-d') . "' WHERE `entry_id`=" . $id)->execute();

                
                    $model = ManualEntry::model()->findByPk($id);
                    Yii::app()->user->setState('emp_ids', array($model->emp_id));

                    // $this->PunchReportDaily(date('Y-m-d', strtotime($model->shift_date)));

                  
                
                $ret['msg'] = 'Manual Entry ' . $finalstatus . ' Successfully';
                $ret['error'] = '';
            }
            echo json_encode($ret);
            exit;
        }
    }

    public function actionManualEntryAction() {

        $atttbl = ManualEntry::model()->tableSchema->rawName;
        if (isset($_REQUEST['id'])) {
            extract($_REQUEST);
            $status = array('Approve' => 1, 'Reject' => 2);
            if ($req) {
                $sts = $status[$req];
                if ($sts == 1) {
                    $finalstatus = 'Approved';
                } else {
                    $finalstatus = 'Rejected';
                }

                $reason = $_REQUEST['reason'];
                $allids = $_REQUEST['id'];
                $data_type = $_REQUEST['data_type'];

                foreach ($allids as $key => $id) {
                    if ($data_type[$key] == "m") {
                        Yii::app()->db->createCommand("UPDATE $atttbl SET `status`=" . $sts . ",`reason`='" . $reason . "',`decision_by`=" . Yii::app()->user->id . ",`decision_date`='" . date('Y-m-d') . "'  WHERE `entry_id`=" . $id)->execute();
                        $model = ManualEntry::model()->findByPk($id);

                        // $this->PunchReportDaily(date('Y-m-d', strtotime($model->date)));
                    
                    }
                    if ($data_type[$key] == "i") {
                        $IgnorePunchestbl = IgnorePunches::model()->tableSchema->rawName;
                        Yii::app()->db->createCommand("UPDATE $IgnorePunchestbl SET `status`=" . $sts . ",`decision_by`=" . Yii::app()->user->id . ",`decision_date`='" . date('Y-m-d') . "' WHERE `ig_id`=" . $id)->execute();
                      
                            $model = IgnorePunches::model()->findByPk($id);

                            // $this->PunchReportDaily(date('Y-m-d', strtotime($model->date)));
                        
                    }
                }

                $ret['msg'] = 'Manual Entry ' . $finalstatus . ' Successfully';
                $ret['error'] = '';
            }


            echo json_encode($ret);
            exit;
        }
    }

}
