<?php

class WorkTypeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	
		public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );

	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new WorkType;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['WorkType']))
		{
			$model->attributes=$_POST['WorkType'];
			if ($model->save())
			{
				 
            //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index', 'id' => $model->userid));
                }
        	}
        }
        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])){
         
            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------

        $this->render('create', array(
            'model' => $model,
        ));

		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['WorkType']))
		{
			$model->attributes=$_POST['WorkType'];
			
			if($model->save()){
				Yii::app()->user->setFlash('success', "Data Saved!");
				$this->redirect(array('index'));
			}
				
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete1($id)
	{
		
		//echo "select  * FROM pms_time_entry WHERE work_type =".$id."";
		 $worktype = Yii::app()->db->createCommand("select count(*) FROM pms_time_entry WHERE work_type =".$id."")->queryScalar();
	
		 if($worktype==0){
			$this->loadModel($id)->delete();
			 Yii::app()->user->setFlash('success', "Data Deleted!");
				$this->redirect(array('index'));
		}else{
			 Yii::app()->user->setFlash('error','Work type is already in use.');
			  $this->redirect(array('index'));
			
		 }
		
			
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new WorkType('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['WorkType']))
			$model->attributes=$_GET['WorkType'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=WorkType::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='work-type-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}