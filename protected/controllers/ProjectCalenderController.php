<?php

class ProjectCalenderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete','test','deleteHoliday'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProjectCalender;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProjectCalender']))
		{
			$model->attributes=$_POST['ProjectCalender'];
			$model->created_by = $model->updated_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d");
			//echo "<pre>",print_r($model->attributes);exit;

		
            

			if ($model->validate()) {
				if($model->save())
				{

					foreach($_POST['holiday_date'] as $key => $date)
					{
                      if($date != "")
					  {
						$holiday_model=new CalendarHolidays;
						$holiday_model->project_calendar_id=$model->id;
						$holiday_model->date=date('Y-m-d', strtotime($date));
						$holiday_model->remarks=$_POST['holiday_remarks'][$key];
						$holiday_model->created_by = $holiday_model->updated_by = Yii::app()->user->id;
                        $holiday_model->created_date = $holiday_model->updated_date = date("Y-m-d");
						$holiday_model->save();
					  }
					}
					Yii::app()->user->setFlash('success', 'Successfully created.');
					$model->unsetAttributes();
					$this->redirect(array('create'));
				}
				else
				{
					Yii::app()->user->setFlash('error', "something went wrong");
				}
			}
			
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProjectCalender']))
		{
			$model->attributes=$_POST['ProjectCalender'];
			if($model->save())



			$delete=CalendarHolidays::model()->deleteAll("project_calendar_id ='" . $id . "'");

			if(isset($_POST['holiday_date']))
                {
			foreach($_POST['holiday_date'] as $key => $date)
					{
                      if($date != "")
					  {
						$holiday_model=new CalendarHolidays;
						$holiday_model->project_calendar_id=$model->id;
						$holiday_model->date=date('Y-m-d', strtotime($date));
						$holiday_model->remarks=$_POST['holiday_remarks'][$key];
						$holiday_model->created_by = $holiday_model->updated_by = Yii::app()->user->id;
                        $holiday_model->created_date = $holiday_model->updated_date = date("Y-m-d");
						$holiday_model->save();
					  }
					}
				}


			Yii::app()->user->setFlash('success', 'Successfully updated.');
				
		}

		$holiday=CalendarHolidays::model()->findAllByAttributes([
			'project_calendar_id' => $id,
			
		]);

		

		$this->render('update',array(
			'model'=>$model,
			'holiday'=>$holiday
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// $this->loadModel($id)->delete();

		// // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		// if(!isset($_GET['ajax']))
		// 	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		
		$model = $this->loadModel($id);
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if (!$model->delete()) {
				$success_status = 0;
				throw new Exception(json_encode($model->getErrors()));
			} else {
				$success_status = 1;
			}
			$transaction->commit();
		} catch (Exception $error) {
			$transaction->rollBack();
			$success_status = 0;
		} finally {
			if ($success_status == 1) {
				echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
			} else {
				if ($error->errorInfo[1] == 1451) {
					echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
				}
			}
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ProjectCalender');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ProjectCalender('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProjectCalender']))
			$model->attributes=$_GET['ProjectCalender'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ProjectCalender::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-calender-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actiontest()
	{
		$begin = new DateTime( "2022-11-16" );
        $end   = new DateTime( "2022-12-16" );
		$start_date="2022-11-16";
		$end_date="2022-12-16";
        $site_calender=10;
        $calender=ProjectCalender::model()->findByPK($site_calender);
        $duration=0;
		$sat_count=0;
		$date_array=[];
		$saturday_array=[];

		for($i = $begin; $i <= $end; $i->modify('+1 day')){
            $day= $i->format("l");
            $date=$i->format("Y-m-d");

			if($day=="Sunday" && $calender->sunday==1)
            {
                $duration ++;
				array_push($date_array,$date);
            }
            else if($day=="Monday" && $calender->monday==1)
            {
                $duration ++;
				array_push($date_array,$date);
            }
             else if($day=="Tuesday" && $calender->tuesday==1)
            {
                $duration ++;
				array_push($date_array,$date);
            }
             else if($day=="Wednesday" && $calender->wednesday==1)
            {
                $duration ++;
				array_push($date_array,$date);
            }
             else if($day=="Thursday" && $calender->thursday==1)
            {
                $duration ++;  
				array_push($date_array,$date);
            }
             else if($day=="Friday" && $calender->friday==1)
            {
                $duration ++;
				array_push($date_array,$date);  
            }
             else if($day=="Saturday" && $calender->saturday==1)
            {
				
               

			   $duration ++;  
			   
			   array_push($date_array,$date);
			

			}

			   $first_sat_s_date="01";
			   $first_sat_e_date="7";
			   $second_sat_s_date="08";
               $second_sat_e_date="14";
			   $third_sat_s_date="15";
			   $third_sat_e_date="21";
               $fourth_sat_s_date="22";
               $fourth_sat_e_date="28";

			 if($day=="Saturday" && $calender->second_saturday==0 && strtotime($date) >= strtotime($i->format("Y-m-". $second_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-". $second_sat_e_date)))
			{
				$sat_count ++;

				array_push($saturday_array,$date);
			}
			if($day=="Saturday" && $calender->fourth_saturday==0 && strtotime($date) >= strtotime($i->format("Y-m-". $fourth_sat_s_date)) &&  strtotime($date) <= strtotime($i->format("Y-m-". $fourth_sat_e_date)))
			{
				$sat_count ++;
				array_push($saturday_array,$date);
			}


			/////////////////////////////////////////////////

			if($day=="Saturday" && $calender->second_saturday==1 && strtotime($date) >= strtotime($i->format("Y-m-". $second_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-". $second_sat_e_date)))
			{
				array_push($date_array,$date); 
			}

			if($day=="Saturday" && $calender->fourth_saturday==1 && strtotime($date) >= strtotime($i->format("Y-m-". $fourth_sat_s_date)) &&  strtotime($date) <= strtotime($i->format("Y-m-". $fourth_sat_e_date)))
			{
				array_push($date_array,$date); 
			}



			///////////////////////////////////////////////////


			

			
		}
		//echo $duration-$sat_count;

		

		$available_date_arr=array_diff($date_array,$saturday_array);

		

		$criteria = new CDbCriteria;
        $criteria->condition = "date >= '$start_date' AND date <= '$end_date' AND project_calendar_id=".$site_calender;
        $holidays = CalendarHolidays::model()->findAll($criteria);
		$holiday_array=[];
		foreach($holidays as $holiday)
		{
			$holiday_array[]=$holiday->date;
		}

		//echo "<pre>",print_r($date_array);

		//echo "<pre>",print_r($available_date_arr);

		$total_working_days=array_diff($available_date_arr,$holiday_array);

		echo "<pre>",print_r($total_working_days);

		//echo count($total_working_days);
	}
	public function actiondeleteHoliday()
	{
		$id = $_POST['id'];
		$remove=CalendarHolidays::model()->deleteAll("id ='" . $id . "'");
		if ($remove == 1) {
            echo '1';
        } else {
            echo '2';
        }
		
	}
}
