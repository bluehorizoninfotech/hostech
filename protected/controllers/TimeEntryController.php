<?php

class TimeEntryController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $week_start_date = null;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
     public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('allow', 
				    'actions'=>array('statusentry','approveentry','getworytype','getprogress','gettasks','datevalidation','createtimeentry','addtimeentry','createexpiretimeentry','Approverejectdata'),
				    'users'=>array('@'),
			        ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

    public function actionSubmitsheet(){

       echo json_encode($_POST);

    }

    public function actionTimeReport()
    {   $model = new TimeReport;
        $model->unsetAttributes();  // clear any default values
        $taskmodel = new Tasks;

        if (isset($_REQUEST['TimeReport']))
        {
            //print_r($_REQUEST['TimeReport']);exit;

            $model->attributes = $_REQUEST['TimeReport'];
            $taskmodel->attributes = $_REQUEST['TimeReport'];
        }
//        else{
//            die("else");
//        }



        $this->render('timereport',array('model'=>$model,'taskmodel'=>$taskmodel));
    }


    //Weeks column generation
    public function x_week_range($date) {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 1) ? $ts : strtotime('last monday', $ts);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next sunday', $start)), date('F d, Y', $start));
    }

    public function add_date($orgDate, $date) {
        $cd = strtotime($orgDate);
        $retDAY = date('d/m/Y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $date, date('Y', $cd)));
        return $retDAY;
    }

    public function get_week_date($orgDate, $moveto) {
        $cd = strtotime($orgDate);

        if ($moveto == 'prev')
            $retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) - 7, date('Y', $cd)));
        elseif ($moveto == 'next')
            $retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + 7, date('Y', $cd)));
        else
            $retDAY = $orgDate;

        return $retDAY;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionEntry() {
        if (isset($_POST['resource_id']))
            $resource_id = Yii::app()->session['resource_id'] = $_POST['resource_id'];
        elseif (isset(Yii::app()->session['resource_id']))
            $resource_id = Yii::app()->session['resource_id'];
        else
            $resource_id = 0;

        if (isset(Yii::app()->session['start_date']) && isset($_POST['moveto'])) {
            //Yii::app()->session['start_date'];
            $this->week_start_date = $this->get_week_date(Yii::app()->session['start_date'], $_POST['moveto']);
        } else {
            Yii::app()->session['start_date'] = date("Y-m-d");
            $this->week_start_date = date("Y-m-d");
        }

        //Initialize the week's table entry structure
        list($start_date, $end_date, $week_title) = $this->x_week_range($this->week_start_date);

        Yii::app()->session['start_date'] = $start_date;

        $this->renderPartial('entry', array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'week_title' => $week_title,
            'resource_id' => $resource_id,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionAddtime() {
        $model = new TimeEntry;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
	$model->unsetAttributes();  // clear any default values
        if(!isset($model->billable))
        {
        $model->billable = 3;
        }
        if (isset($_POST['TimeEntry'])) {
            $model->attributes = $_POST['TimeEntry'];
            if (Yii::app()->user->role > 2)
                $model->user_id = Yii::app()->user->id;

            $model->created_by = $model->updated_by = Yii::app()->user->id;
            $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
            if ($model->save()) {
                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#yt1').click();");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index'));
                }
            }
        }
        //----- begin new code --------------------
        if (!empty($_GET['asDialog']))
            $this->layout = '//layouts/iframe';
        //----- end new code --------------------

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionAddtasktime($tskid) {
        $model = new TimeEntry;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
	$model->unsetAttributes();  // clear any default values
        if(!isset($model->billable))
        {
        $model->billable = 3;
        }
        if (isset($_POST['TimeEntry'])) {
            $model->attributes = $_POST['TimeEntry'];
            if (Yii::app()->user->role > 2)
                $model->user_id = Yii::app()->user->id;

            $model->created_by = $model->updated_by = Yii::app()->user->id;
            $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
            $model->entry_date = date('Y-m-d',strtotime($model->entry_date));
            if ($model->save()) {
                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#yt1').click();");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('tasks/mytask'));
                }
            }
        }
        //----- begin new code --------------------
        if (!empty($_GET['asDialog']))
            $this->layout = '//layouts/iframe';
        //----- end new code --------------------
        if (!empty($_GET['layout']))
            $this->layout = false;

        $this->render('addtasktime', array(
            'model' => $model,'tskid' => $tskid
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new TimeEntry;            
        if(isset($_GET['layout']))
               $this->layout = false;
               if(!empty($_GET['id']))
               $model->tskid = $_GET['id'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TimeEntry'])) {
            $model->attributes = $_POST['TimeEntry'];          
            if (Yii::app()->user->role > 2)
                $model->user_id = Yii::app()->user->id;

            $model->created_by = $model->updated_by = Yii::app()->user->id;
            $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
            $model->entry_date = date('Y-m-d',strtotime($model->entry_date));
            $model->work_type = '';
            $model->timeentry_type =1;
            $start_time = strtotime(date('Y-m-d ') . $model->start_time . ":00");
            $end_time = strtotime(date('Y-m-d ') . $model->end_time . ":00");
            $starting_time = new DateTime($model->start_time);
            $ending_time = new DateTime($model->end_time);
            $since_start = $starting_time->diff($ending_time);
            $hours_in_minutes = $since_start->h * 60;
            $minutes_only = $since_start->i;
            $total_minutes = $hours_in_minutes + $minutes_only;
            $hours = floor($total_minutes / 60).':'.($total_minutes -   floor($total_minutes / 60) * 60);
            $model->hours=$hours;
            $general_settings_model = GeneralSettings::model()->find(['condition' => 'id = 1']);

            if($general_settings_model->add_time_entry_approval==2){         
                $model->approve_status = 1;
                $task_model = Tasks::model()->findByPk($model->tskid);
             }
            
            if ($model->save() && !isset($_GET['type'])){                
                if(isset($task_model) && !empty($task_model) && !empty($model->current_status)){
                    $task_model->status = $model->current_status;
                     $task_model->update();
                 }
               Yii::app()->user->setFlash('success', 'Successfully created.');
                 $this->redirect(array('index'));
            }
                   
                if(isset($_GET['type'])){
                    $this->redirect(array('projects/chart','id'=>$_GET['project_id']));
                }
        }
        if(isset($_GET['type'])&& $_GET['type'] == 1){
            $this->render('create', array(
                'model' => $model,
                'type'=>1,
                'project_id'=>$_REQUEST['project_id'],
                'task_id'=>$_REQUEST['id']
                
            ));
        }else{
            $this->render('create', array(
                'model' => $model,
                'type'=>0,
                
            ));
        }
       
    }

    

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $type=0;
        $model = $this->loadModel($id);
        
        if(isset($_GET['asDialog']))
        $this->layout = false;
      
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TimeEntry'])) {
            
            $model->attributes = $_POST['TimeEntry'];
            if (Yii::app()->user->role > 2)
                $model->user_id = Yii::app()->user->id;

            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date("Y-m-d H:i:s");
            $model->entry_date = date('Y-m-d',strtotime($model->entry_date));
            $model->work_type = '';
            $model->timeentry_type =1;
            $start_time = strtotime(date('Y-m-d ') . $model->start_time . ":00");
            $end_time = strtotime(date('Y-m-d ') . $model->end_time . ":00");
            $starting_time = new DateTime($model->start_time);
            $ending_time = new DateTime($model->end_time);
            $since_start = $starting_time->diff($ending_time);
            $hours_in_minutes = $since_start->h * 60;
            $minutes_only = $since_start->i;
            $total_minutes = $hours_in_minutes + $minutes_only;
            $hours = floor($total_minutes / 60).':'.($total_minutes -   floor($total_minutes / 60) * 60);
            $model->hours=$hours;

            if ($model->save()) {
                // $task_model = Tasks::model()->findByPk($model->tskid);
                // if(!empty($task_model)){
                //     $task_model->status = $model->current_status;
                //     $task_model->update();
                    Yii::app()->user->setFlash('success', 'Successfully updated.');
                //}
                
                
                //----- begin new code --------------------
                // if (!empty($_GET['asDialog'])) {
                //     //Close the dialog, reset the iframe and update the grid
                //     echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#yt1').click();");
                //     Yii::app()->end();
                // } else {
                    //----- end new code --------------------
                    $this->redirect(array('index'));
             //   }
            }
            //$this->redirect(array('view', 'id' => $model->teid));
        }

        //----- begin new code --------------------
       // if (!empty($_GET['asDialog']))
           // $this->layout = '//layouts/iframe';
        //----- end new code --------------------

        $this->render('update', array(
            'model' => $model,'type'=>$type
        ));
    }

    // public function actionCDelete($id) {
    //     $this->loadModel($id)->delete();
    //     echo true;
    // }
    public function actionCDelete()
    {
        $id = $_REQUEST['datid'];
        $this->loadModel($id)->delete();
        echo '1';
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * get time entries.
     */
    public function getConsolidEntries($start_date, $end_date, $taskid = '0') {
        

        if (isset(Yii::app()->session['resource_id']))
            $resource_id = Yii::app()->session['resource_id'];
        else
            $resource_id = Yii::app()->user->id;

        $criteria = new CDbCriteria();
        $criteria->select = 'tskid, billable, sum( `hours` ) AS hours, date_format(`entry_date`, "%d/%m/%Y") as tentry_date ';
        $criteria->condition = 'user_id = :userid AND entry_date BETWEEN :start_date AND :end_date ' . ($taskid != 0 ? 'AND tskid=' . $taskid : '');
        //$criteria->addBetweenCondition('entry_date', $start_date, $end_date);
        $criteria->group = 'entry_date, tskid, billable';
        $criteria->order = 'tskid, billable, entry_date ASC';
        $criteria->params = array('userid' => $resource_id, 'start_date' => $start_date, 'end_date' => $end_date);

        return $timeEntry = TimeEntry::model()->findAll($criteria);
    }

    /**
     * get time entries.
     */
    public function weekDetailEntries($start_date, $end_date) {             
        if(Yii::app()->user->role == 1){            
            $criteria = new CDbCriteria();
            $criteria->select = 'teid,t.tskid, entry_date as tentry_date , hours, t.billable, work_type, t.description, DATEDIFF( entry_date, NOW()) AS ddate,approve_status,current_status,timeentry_type';            
            $criteria->join .= ' LEFT JOIN `pms_tasks` `tsk` ON tsk.tskid=t.tskid';
            $criteria->join .= ' LEFT JOIN `pms_projects` `pro` ON pro.pid=tsk.project_id';
           
            if (isset(Yii::app()->session['resource_id'])){
                $resource_id = Yii::app()->session['resource_id'];
                $criteria->condition = 'user_id = :userid AND entry_date BETWEEN :start_date AND :end_date';
                $criteria->order = 'entry_date DESC';
                $criteria->params = array('userid' => $resource_id,'start_date' => $start_date, 'end_date' => $end_date);
            } else{
                $criteria->condition = 'entry_date BETWEEN :start_date AND :end_date';
                $criteria->order = 'entry_date DESC';
                $criteria->params = array('start_date' => $start_date, 'end_date' => $end_date);
            }  
            if(Yii::app()->user->project_id !=""){
                $criteria->addCondition('pro.pid = '.Yii::app()->user->project_id.'');
            }

            // echo '<pre>'            ;print_r($criteria);exit;
            //$criteria->addBetweenCondition('entry_date', $start_date, $end_date);
           
        }else{
            if (isset(Yii::app()->session['resource_id']))
            $resource_id = Yii::app()->session['resource_id'];
            else
                $resource_id = Yii::app()->user->id;

            $criteria = new CDbCriteria();
            $criteria->select = 'teid,t.tskid, entry_date as tentry_date , hours, t.billable, work_type, t.description, DATEDIFF( entry_date, NOW()) AS ddate,approve_status,current_status,timeentry_type';
            $criteria->join .= ' LEFT JOIN `pms_tasks` `tsk` ON tsk.tskid=t.tskid';
            $criteria->join .= ' LEFT JOIN `pms_projects` `pro` ON pro.pid=tsk.project_id';
            if(Yii::app()->user->project_id !=""){
                $criteria->addCondition('pro.pid = '.Yii::app()->user->project_id.'');
            }
            $criteria->condition = ' entry_date BETWEEN :start_date AND :end_date AND(user_id = :userid OR coordinator = :userid)';           
            $criteria->order = 'entry_date DESC';
            $criteria->params = array('userid' => $resource_id, 'start_date' => $start_date, 'end_date' => $end_date);
        }  
        // echo '<pre>'            ;print_r($criteria);exit;         
        return $timeEntry = TimeEntry::model()->findAll($criteria);
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {         
        Yii::app()->session['start_date'] = date("Y-m-d");

        unset(Yii::app()->session['resource_id']);


        //Initialize the week's table entry structure
        list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-d"));

        $this->render('index');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new TimeEntry('search');
        $model->unsetAttributes();  // clear any default values



         // new filter back and forth
        $class_name= get_class($model);
        if (isset($_GET[ $class_name])){
           Yii::app()->user->setState('prevfilter' , $_GET[$class_name]);
        }

        if(isset(Yii::app()->user->prevfilter)){
           $model->attributes = Yii::app()->user->prevfilter;
        }

        // end

        if (isset($_GET['TimeEntry']))
            $model->attributes = $_GET['TimeEntry'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = TimeEntry::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionGetTask()
    {      
        $tblpx = Yii::app()->db->tablePrefix;
        $where = " AND '".Yii::app()->session['start_date']."'  between start_date and due_date";
            if(Yii::app()->user->project_id !=""){
            $where .= "  AND project_id =".Yii::app()->user->project_id."";
        }
        
        $data = Yii::app()->db->createCommand("select tskid, title FROM {$tblpx}tasks WHERE assigned_to=".$_POST['userid']." AND task_type = 2  AND status='6' ".$where."")->queryAll();
		$data=CHtml::listData($data,'tskid','title');
		echo "<option value=''>Select task</option>";
		foreach($data as $value=>$title)
		{
			echo CHtml::tag('option', array('value'=>$value),CHtml::encode($title),true);
		}
    }
    
    public function actionStatusentry(){  
        if(isset($_GET['layout']))
        $this->layout = false;
        $model = new TimeEntry;   
        $type = $_REQUEST['type'] ;
        
      $task = Tasks::model()->findByPk($_REQUEST['id']);
      $this->performAjaxValidation($model);

      if (isset($_POST['TimeEntry'])) {
          $model->attributes = $_POST['TimeEntry'];
          if (Yii::app()->user->role > 2)
              $model->user_id = Yii::app()->user->id;

          $model->created_by = $model->updated_by = Yii::app()->user->id;
          $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
          if ($model->save() && !isset($_GET['type']))
              $this->redirect(array('index'));
              if(isset($_GET['type'])){
                  $this->redirect(array('projects/chart','id'=>$_GET['project_id']));
              }
      }
      $model->tskid = $task['tskid'];
        $this->render('status_entry', array(
            'model' => $model,
            'type'=>$type ,
            'task_id' => $model->tskid  ,
            'project_id'=>$_REQUEST['project_id'],                 
        ));             

    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'time-entry-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionApproveentry(){
        if(isset($_REQUEST['entry_id'])){
            $id = $_REQUEST['entry_id'];
            $time_entry_model = $this->loadModel($id);
            $time_entry_model->approve_status = 1;
            if($time_entry_model->update()){
                $task_model = Tasks::model()->findByPk($time_entry_model->tskid);
                if(!empty($task_model) && !empty($time_entry_model->current_status)){
                   $task_model->status = $time_entry_model->current_status;
                    if($task_model->update()){
                    }
                }
                echo '1';
            }else{
                echo '2';
            }
        }
    }

    public function actiongetworytype(){
        $tskid = $_REQUEST['tskid'];
        $task = Tasks::model()->findByPk($tskid);
        if(!empty($task)){
            //echo $task->work_type_id;
            $work_type = WorkType::model()->findByPk($task->work_type_id);
           // $html = "<option value=''>Select work type</option>";
            //foreach($work_type as $key => $value){
                $html .= "<option value=".$work_type['wtid'].">".$work_type['work_type']."</option>";
           // }
            echo json_encode(array('id'=> $task->work_type_id,'option' => $html));
        }else{
            $html = "<option value=''>Select work type</option>";
            echo json_encode(array('id'=> "",'option' =>$html));
        }
    }

    public function actionGetprogress()
	{ 
        $progress =0;
        
        if(isset($_REQUEST['progress'])&& isset($_REQUEST['task_id'])){
            $progress = $_REQUEST['progress'];
            $task_id = $_REQUEST['task_id'];
            $tblpx = Yii::app()->db->tablePrefix;
            $sql_response3 = Yii::app()->db->createCommand("select SUM(completed_percent) as progress FROM {$tblpx}time_entry WHERE tskid='".$task_id."'")->queryRow();
            $total_progress = $sql_response3['progress']+$progress;
            if($total_progress >100){
                $progress =1;
                $balance_progress=100-$sql_response3['progress'];
            }else{
                $progress =0;
                $balance_progress=100-$sql_response3['progress'];
            }
        }
		//echo $progress;	
        $result_array = array('progress' => $progress,'balance_progress'=>$balance_progress);
        echo json_encode($result_array);
      
    }
    public function actiongettasks(){
        $userid = $_REQUEST['userid'];
        if(empty($userid)){
            $userid = Yii::app()->user->id;
        }
        $where = "  AND 1=1";
        if(Yii::app()->user->project_id !=""){
           $where = "  AND project_id =".Yii::app()->user->project_id."";
        }
       $tasks = Tasks::model()->findAll(array("condition"=>"task_type = 2 AND CURDATE() between start_date and due_date And status IN (6,9) And assigned_to=".$userid." ".$where."",'order' => 'title ASC'));
          
        if(!empty($tasks)){                    
           $html = "<option value=''>Select task</option>";
            foreach($tasks as $task){
                $html .= "<option value=".$task['tskid'].">".$task['title']."</option>";
           }
            echo json_encode(array('option' => $html));
        }else{
            $html = "<option value=''>Select task</option>";
            echo json_encode(array('id'=> "",'option' =>$html));
        }
    }

    public function actionPendingRequests(){
      
        $model = new TimeEntry('pendingrequests');
        $model->unsetAttributes();  // clear any default values



         // new filter back and forth
        $class_name= get_class($model);
        if (isset($_GET[ $class_name])){
           Yii::app()->user->setState('prevfilter' , $_GET[$class_name]);
        }

        if(isset(Yii::app()->user->prevfilter)){
           $model->attributes = Yii::app()->user->prevfilter;
        }

        // end

        if (isset($_GET['TimeEntry']))
            $model->attributes = $_GET['TimeEntry'];

        $this->render('pending_requests', array(
            'model' => $model,
        ));
	}

    public function actionDatevalidation()
    {
        $tskid = $_REQUEST['taskid'];
        $edate = $_REQUEST['date']; 
        $selected_date = date('Y-m-d',strtotime($edate));
        $sql = "SELECT concat_ws(' & ',`start_date`,`due_date`) as date_range FROM pms_tasks WHERE ( '$selected_date' BETWEEN `start_date` AND `due_date`) AND tskid=" . intval($tskid);
                    
        $date_range = Yii::app()->db->createCommand($sql)->queryScalar();

        if(empty($date_range)){
            
            $sql = "SELECT `start_date`,`due_date` FROM pms_tasks WHERE tskid=" . intval($tskid);
            $dates = Yii::app()->db->createCommand($sql)->queryRow();
            $start_date=date('d-M-y',strtotime($dates['start_date']));
            $end_date=date('d-M-y',strtotime($dates['due_date']));

            $result_array = array('status' => 1,'start_date'=>$start_date,'end_date'=>$end_date);
        }
        else
        {
                        $result_array = array('status' => 2,'start_date'=>'','end_date'=>'');
        }
        echo json_encode($result_array);
    }

public function actioncreatetimeentry()
{
   $taskid=$_REQUEST['taskid'];
   $task_det=Tasks::model()->findByPk($taskid);

   $project_id=$task_det->project_id;
    $model = new TimeEntry;            
    if(isset($_GET['layout']))
           $this->layout = false;
           if(!empty($_GET['id']))
           $model->tskid = $_GET['id'];

    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model);

    if (isset($_POST['TimeEntry'])) {
        $model->attributes = $_POST['TimeEntry'];          
        if (Yii::app()->user->role > 2)
            $model->user_id = Yii::app()->user->id;

        $model->created_by = $model->updated_by = Yii::app()->user->id;
        $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
        $model->entry_date = date('Y-m-d',strtotime($model->entry_date));



        $start_time = strtotime(date('Y-m-d ') . $model->start_time . ":00");
        $end_time = strtotime(date('Y-m-d ') . $model->end_time . ":00");
        $starting_time = new DateTime($model->start_time);
        $ending_time = new DateTime($model->end_time);
        $since_start = $starting_time->diff($ending_time);
        $hours_in_minutes = $since_start->h * 60;
        $minutes_only = $since_start->i;
        $total_minutes = $hours_in_minutes + $minutes_only;
        $hours = floor($total_minutes / 60).':'.($total_minutes -   floor($total_minutes / 60) * 60);
        $model->hours=$hours;

         if ($model->save() && !isset($_GET['type'])){                
          
           Yii::app()->user->setFlash('success', 'Successfully created.');
           $this->redirect(array('Reports/weeklyReport', 'project_id'=>$project_id));
        }
               
            if(isset($_GET['type'])){
                $this->redirect(array('projects/chart','id'=>$_GET['project_id']));
            }
    }
    // if(isset($_GET['type'])&& $_GET['type'] == 1){
    //     $this->render('create', array(
    //         'model' => $model,
    //         'type'=>1,
    //         'project_id'=>$_REQUEST['project_id'],
    //         'task_id'=>$_REQUEST['id']
            
    //     ));
    // }else{
        $this->render('create_time_entry', array(
            'model' => $model,
            'type'=>0,
            'taskid'=> $taskid
            
        ));
    // }
}
public function actionaddtimeentry()
{
    $taskid=$_REQUEST['taskid'];
    $task_det=Tasks::model()->findByPk($taskid);
 
    $project_id=$task_det->project_id;
     $model = new TimeEntry;            
     if(isset($_GET['layout']))
        $this->layout = false;
        if(!empty($_GET['id']))
        $model->tskid = $_GET['id'];
 
     // Uncomment the following line if AJAX validation is needed
     $this->performAjaxValidation($model);
 
     if (isset($_POST['TimeEntry'])) {
        $model->attributes = $_POST['TimeEntry'];          
            $model->user_id = Yii::app()->user->id;
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
            $model->entry_date = date('Y-m-d',strtotime($model->entry_date));
            $model->work_type = '';
            $start_time = strtotime(date('Y-m-d ') . $model->start_time . ":00");
            $end_time = strtotime(date('Y-m-d ') . $model->end_time . ":00");
            $starting_time = new DateTime($model->start_time);
            $ending_time = new DateTime($model->end_time);
            $since_start = $starting_time->diff($ending_time);
            $hours_in_minutes = $since_start->h * 60;
            $minutes_only = $since_start->i;
            $total_minutes = $hours_in_minutes + $minutes_only;
            $hours = floor($total_minutes / 60).':'.($total_minutes -   floor($total_minutes / 60) * 60);
            $model->hours=$hours;
            $general_settings_model = GeneralSettings::model()->find(['condition' => 'id = 1']);

            if($general_settings_model->add_time_entry_approval==2){
                $model->approve_status = 1;
                $task_model = Tasks::model()->findByPk($model->tskid);
             }
          if ($model->save() && !isset($_GET['type'])){                
            if(isset($task_model) && !empty($task_model) && !empty($model->current_status)){
                $task_model->status = $model->current_status;
                 $task_model->update();
             }
            Yii::app()->user->setFlash('success', 'Successfully created.');
            if($monthlyTask=Yii::app()->session['current_url']){
                $urlComponents = parse_url($monthlyTask);
                parse_str($urlComponents['query'], $queryParams);
                $monthlyTask = isset($queryParams['monthly_task_page'])?$monthlyTask:$monthlyTask . '&' . http_build_query(['monthly_task_page' => 1]);
                $this->redirect($monthlyTask);
           }else{
                $this->redirect(array('monthlyTask'));
           }
            // $this->redirect(array('Tasks/monthlyTask'));
         }else{
            //echo '<pre>';print_r($model->getErrors());exit;
         }
    }
     
    $this->render('add_time_entry', array(
             'model' => $model,
             'type'=>0,
             'taskid'=> $taskid
             
    ));
    
 }

 public function actionCreateExpireTimeEntry($id=''){
    if($id){
        $model = TimeEntry::model()->findByPk($id); 
    }else{
        $model = new TimeEntry; 
    }
    
    $model->scenario = 'expiretimeentry';            
    $this->layout = false;
    $task_id = isset($_POST['taskid'])?$_POST['taskid']:'';
    $model->tskid =$task_id;
    
    // Uncomment the following line if AJAX validation is needed
    $this->performAjaxValidation($model);

    if (isset($_POST['TimeEntry'])) {
        
        $model->attributes = $_POST['TimeEntry']; 
                
        if (Yii::app()->user->role > 2)
            $model->user_id = Yii::app()->user->id;

        $model->created_by = $model->updated_by = Yii::app()->user->id;
        $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
        $model->timeentry_type =2;
        $model->entry_date = date('Y-m-d',strtotime($model->entry_date));
        $model->work_type = '';
        $start_time = strtotime(date('Y-m-d ') . $model->start_time . ":00");
        $end_time = strtotime(date('Y-m-d ') . $model->end_time . ":00");
        $starting_time = new DateTime($model->start_time);
        $ending_time = new DateTime($model->end_time);
        $since_start = $starting_time->diff($ending_time);
        $hours_in_minutes = $since_start->h * 60;
        $minutes_only = $since_start->i;
        $total_minutes = $hours_in_minutes + $minutes_only;
        $hours = floor($total_minutes / 60).':'.($total_minutes -   floor($total_minutes / 60) * 60);
        $model->hours=$hours;
        if ($model->save()){ 
            if($id){
                Yii::app()->user->setFlash('success', 'Successfully Updated.');
            } else{
                Yii::app()->user->setFlash('success', 'Successfully created.');
            }              
        }else{
            Yii::app()->user->setFlash('error', 'Something went wrong in time entry.');
        }
        $this->redirect(array('tasks/view', 'id'=>$model->tskid));
    }
}

public function actionApproverejectdata()
    {
        $result = 0;
        $entry_id = isset($_POST['entry_id']) ? $_POST['entry_id'] : '';
        $reason = isset($_POST['reason']) ? $_POST['reason'] : '';
        $model = $this->loadModel($entry_id);
            if (!empty($reason)) {
                $model->reason_rejection = $reason;
                $model->approve_status = 2;
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', 'Rejected Successfully.');
                    $result = 1;    
                }
            }
        echo $result;
    }
}