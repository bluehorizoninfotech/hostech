<?php

class GroupsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $week_start_date = null;
    public $accessArr = array();
    public $accessauthArr = array();
    public $accessguestArr = array();

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
   public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('allow', // allow authenticated user to perform 'create' and 'update' actions
                        'actions'=>array('countgroup'),
                        'users'=>array('@'),
                    ),
                   array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

    /**
     * Lists all models.
     */
    public function actionIndex() {		
	$tblpx = Yii::app()->db->tablePrefix;			
        $users = Yii::app()->db->createCommand()
                   ->select([''.$tblpx.'users.userid',''.$tblpx.'users.first_name', ''.$tblpx.'users.last_name',''.$tblpx.'users.user_type',''.$tblpx.'user_roles.id as id',''.$tblpx.'user_roles.role as role',''.$tblpx.'users.status as status'])                   
                   ->from(''.$tblpx.'users')
                   ->leftJoin(''.$tblpx.'user_roles', ''.$tblpx.'user_roles.id = '.$tblpx.'users.user_type')
                   ->where(array('and', 'id>=1', 'status=0'))
                   ->order('first_name')
                   ->queryAll();
      
        $groups = Yii::app()->db->createCommand()
                ->select(['*'])
                ->from(''.$tblpx.'groups')
                ->order('group_id  desc')
                ->queryAll();
		$model= new Users('memberlistsearch');
        $model->unsetAttributes();  // clear any default values

        

         // new filter back and forth
        $class_name= get_class($model);
        if (isset($_GET[ $class_name])){
           Yii::app()->user->setState('prevfilter' , $_GET[$class_name]);
        }

        if(isset(Yii::app()->user->prevfilter)){
           $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 
        
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];
					
        if(!isset($_SESSION["gpid"])){
            $_SESSION["gpid"] = 0;
        }
        $this->render('index', array('users' => $users, 'groups' => $groups, 'model' => $model));
    }
    
    
    
    
    public function actionAddmembers() {
	 $this->layout = '//layouts/iframe'; 
       
        $model= new Users('memberlistsearch');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
		{
            $model->attributes = $_GET['Users'];   
		}
        
		$this->renderPartial('memeberlist', array('model'=>$model));
    }
    public function actionListdepartment(){    
        $department = Yii::app()->db->createCommand()
                    ->select(['*'])                   
                    ->from('pms_user_roles')
                    ->queryAll();
        
        echo json_encode($department);
    }
    
    
    public function actionCreategroup(){
        $tblpx = Yii::app()->db->tablePrefix;		
        $groupid = $_REQUEST['group_id'];
        $groupname =  $_REQUEST['group_name'];
        $grouplead =  $_REQUEST['group_lead'];
        $status =  $_REQUEST['status'];
        $description =  $_REQUEST['description'];
        
        $data =  array();
        $command = Yii::app()->db->createCommand()
                ->select(['group_name'])
                ->from(''.$tblpx.'groups')
                ->where('group_name=:name', array(':name'=>$groupname))
                ->queryAll();
        if($groupid == 0){ // create new group if id == 0
            
            $data['action'] = "create";
            if(count($command) ==0){ // check if group name already exists
                $model = new Groups;
                $model->group_name = $groupname;
                $model->group_lead = $grouplead;
                $model->description = $description;
                $model->status = $status;
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
                if($model->save()){
                    $_SESSION["gpid"] = Yii::app()->db->getLastInsertID();
                    $data['message'] = "success";
                    //$newgroup = Groups::model()->find('group_id ='.Yii::app()->db->getLastInsertID());
                   /* $criteria=new CDbCriteria;
                    $criteria->select='*';  // only select the 'title' column
                    $criteria->condition='group_id=:postID';
                    $criteria->params=array(':postID'=>Yii::app()->db->getLastInsertID());
                    $newgroup=Groups::model()->find($criteria); // $params is not needed*/
                     $newgroup = Yii::app()->db->createCommand()
                            ->select(['*'])
                            ->from('pms_groups')
                            ->where('group_id=:id', array(':id'=> Yii::app()->db->getLastInsertID()))
                            ->queryAll();
                    //print_r($newgroup);die();
                    $data['record'] = $newgroup;
                     
                }
                else{
                    // data not saved
                    $data['message'] = "Group cannot be created";
                }
            }
            else{
                //group name already exists
                $data['message'] = "Group name already exists";
            }
            
            
    
        } 
        else{ //update group details
            $data['action'] = "update";
                $model = Groups::model()->findByPk($groupid);
                if($groupname!= ''){
                    if( $model->group_name != $groupname){ // if new group name != old
                        if(count($command) == 0 ){ // if new group name not exists
                            $model=Groups::model()->updateByPk($groupid,array("group_name"=>$groupname, "group_lead" => $grouplead, "description" => $description,
                                                                    "status" => $status, "updated_by" => Yii::app()->user->id, "updated_date" => date('Y-m-d H:i:s') ));
                            $data['message'] = "success";
                        }else{
                            $model=Groups::model()->updateByPk($groupid,array("group_lead" => $grouplead, "description" => $description,
                                                                    "status" => $status, "updated_by" => Yii::app()->user->id, "updated_date" => date('Y-m-d H:i:s')));
                            $data["message"] = "Group name already exists";
                        }
                    }
                    else{ // update all except group name
                        $model=Groups::model()->updateByPk($groupid,array("group_lead" => $grouplead, "description" => $description,
                                                                    "status" => $status, "updated_by" => Yii::app()->user->id, "updated_date" => date('Y-m-d H:i:s')));
                        $data['message'] = "success";
                    }
                }
                else{
                    $model=Groups::model()->updateByPk($groupid,array("group_lead" => $grouplead, "description" => $description,
                                                                "status" => $status, "updated_by" => Yii::app()->user->id, "updated_date" => date('Y-m-d H:i:s')));
                    $data['message'] = "Group name cannot be empty";
                }
                
                
            $newgroup = Yii::app()->db->createCommand()
                        ->select(['*'])
                        ->from('pms_groups')
                        ->where('group_id=:id', array(':id'=> $groupid))
                        ->queryAll();
            
            $model = Groups::model()->findByPk($groupid);
            if(isset($model) && (count($model)!=NULL)){
                $userid = $model['group_lead'];
                $groupsmemeberlist = Groups_members::model()->deleteAll(
                    array(
                        'select' => 'group_id,group_members',
                        'condition' => 'group_members='.$userid. ' AND group_id=' . $groupid,
                 ));
            }
            

            $data['record'] = $newgroup;
            
        }
        
        echo json_encode($data);  
        
    }

       public function actionAddmemeberlist() {
		$this->layout = '//layouts/column2';   
        $gpid = $_SESSION["gpid"];
        $userid = $_REQUEST['userid'];
        $gpmemebr = array();
        $groupsmemeberlist = Groups_members::model()->findAll(
                    array(
                        'select' => 'group_id,group_members',
                        'condition' => 'group_members=' .$userid. ' AND group_id=' . $gpid,
            ));
            if($groupsmemeberlist==NULL){
                $newmember                  = new Groups_members();
                $newmember['group_id']      =    $gpid;
                $newmember['group_members'] =    $userid;
                $newmember['created_date']  =    date('Y-m-d H:i:s');
                $newmember['created_by']    =    Yii::app()->user->id;
                $newmember->save();
            
        }
		
		$this->redirect(array('/groups/memeberlist'));
		
		
        
    }
	public function actionAddmemeberlistall() {
        $gpid = $_SESSION["gpid"];
        $userid = $_REQUEST['userid'];
        $gpmemebr = array();
        foreach($userid as $user){
            $groupsmemeberlist = Groups_members::model()->findAll(
                    array(
                        'select' => 'group_id,group_members',
                        'condition' => 'group_members=' .$user. ' AND group_id=' . $gpid,
            ));
            if($groupsmemeberlist==NULL){
                $newmember                  = new Groups_members();
                $newmember['group_id']      =    $gpid;
                $newmember['group_members'] =    $user;
                $newmember['created_date']  =    date('Y-m-d H:i:s');
                $newmember['created_by']    =    Yii::app()->user->id;
                $newmember->save();
            }
        }
		$this->layout ="iframe";
		$model= new Users('grp_memberlistsearch');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];        
        
		$this->render('indexmemeberlist', array('model'=>$model));
       /* $gpmemebr = array();
        $groupsmemeberlists = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members',
                    'condition' => 'group_id='.$gpid, // static group id
        ));
        foreach ($groupsmemeberlists as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }
        $users = Yii::app()->db->createCommand()
                ->select(['pms_users.first_name', 'pms_users.last_name', 'pms_users.user_type',
                    'pms_user_roles.id as id', 'pms_user_roles.role', 'pms_users.status as status',
                    'pms_users.userid'])
                ->from('pms_users')
                ->leftJoin('pms_user_roles', 'pms_user_roles.id = pms_users.user_type')
                ->where(array('and', 'id>1', 'status=0'))
                //->where(array('not in', 'id', $gpmemebr))
                ->queryAll();

        $this->renderPartial('indexmemeberlist', array('groupmembers' => $gpmemebr, 'users' => $users));*/
    }
    

   /* public function actionMemeberlist() {
        $gpid = $_SESSION["gpid"];
        $gpmemebr = array();
        $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members',
                    'condition' => 'group_id='.$gpid, // static group id
					'order'=>'id'
        ));
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }
        $users = Yii::app()->db->createCommand()
                ->select(['pms_users.first_name', 'pms_users.last_name', 'pms_users.user_type',
                    'pms_user_roles.id as id', 'pms_user_roles.role', 'pms_users.status as status',
                    'pms_users.userid'])
                ->from('pms_users')
                ->leftJoin('pms_user_roles', 'pms_user_roles.id = pms_users.user_type')
                ->where(array('and', 'id>1', 'status=0'))
                //->where(array('not in', 'id', $gpmemebr))
                ->queryAll();

        $this->renderPartial('indexmemeberlist', array('groupmembers' => $gpmemebr, 'users' => $users,));
    }*/

    public function actionMemeberlist()
	{
		$this->layout ="false";
		$model= new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];        
        
		$this->render('indexmemeberlist', array('model'=>$model));
	}
    
    public function actionGetgroupdetails(){
        
        $groupid = $_REQUEST['groupid'];
        $_SESSION["gpid"] = $groupid;
        $group = Yii::app()->db->createCommand()
                    ->select(['*'])                   
                    ->from('pms_groups')
                    ->where('group_id=:id', array(':id'=>$groupid))
                    ->queryAll();
        
        echo json_encode($group);
    }
     public function actionDeletegroupmember() {
		
		$gpid = $_SESSION["gpid"];
		$userid=$_GET['memeberid'];
		//echo "gpid=".$gpid."uid=".$userid;die;
		 Yii::app()->db->createCommand()->delete('pms_groups_members',
                           "group_id = :group_id AND group_members = :group_members",
                           array(':group_id' => $gpid, ':group_members' => $userid)
                       );
		$this->redirect(array('/groups/memeberlist'));			   
        //$this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        /*if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));*/
    }
    public function actionDeletegroup(){
        $groupid = $_REQUEST['group_id'];
        $groupname = $_REQUEST['group_name'];
        $groupmem = Yii::app()->db->createCommand()
                    ->select(['*'])                   
                    ->from('pms_groups_members')
                    ->where('group_id=:id', array(':id'=>$groupid))
                    ->queryAll();
        if(count($groupmem) == 0){ //check if members are empty
            $del = Groups::model()->deleteByPk($groupid);
            if($del){
                $data['message'] = $groupname." is Deleted";
                $data['id'] = $groupid;
                $data['status']  = "success";
            }
            else{
                $data['message'] = $groupname." couldn't be deleted. Please delete all group members from this group and then delete.";
                $data['id'] = $groupid;
                $data['status']  = "error";
            }
            
        }
        else{
            //could not delete
            $data['message'] = $groupname." couldn't be deleted. Please delete all group members from this group and then delete.";
            $data['status']  = "error";
            $data['id'] = $groupid;
            
        }
        echo  json_encode($data);
        
    }
public function actionDeletememberall(){
        $memeberid  = $_REQUEST['userid'];
        $groupid    =  $_SESSION["gpid"];
        if(isset($memeberid) && isset($groupid)){
            foreach($memeberid as $member){
             $groupsmemeberlist = Groups_members::model()->deleteAll(
                    array(
                        'select' => 'group_id,group_members',
                        'condition' => 'group_members=' .$member. ' AND group_id=' . $groupid,
            ));
            }
           $gpid = $_SESSION["gpid"];
		   $this->layout ="iframe";
		$model= new Users('grp_memberlistsearch');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];        
        
		$this->render('indexmemeberlist', array('model'=>$model));
        /*$gpmemebr = array();
        $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members',
                    'condition' => 'group_id='.$gpid, // static group id
        ));
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }
        $users = Yii::app()->db->createCommand()
                ->select(['pms_users.first_name', 'pms_users.last_name', 'pms_users.user_type',
                    'pms_user_roles.id as id', 'pms_user_roles.role', 'pms_users.status as status',
                    'pms_users.userid'])
                ->from('pms_users')
                ->leftJoin('pms_user_roles', 'pms_user_roles.id = pms_users.user_type')
                ->where(array('and', 'id>1', 'status=0'))
                //->where(array('not in', 'id', $gpmemebr))
                ->queryAll();

        $this->renderPartial('indexmemeberlist', array('groupmembers' => $gpmemebr, 'users' => $users,));*/
        }
    }

    
    public function actionDeletemember(){
        $memeberid  = $_GET['memeberid'];
        $groupid    =  $_SESSION["gpid"];
        if(isset($memeberid) && isset($groupid)){
            
             $groupsmemeberlist = Groups_members::model()->deleteAll(
                    array(
                        'select' => 'group_id,group_members',
                        'condition' => 'group_members=' .$memeberid. ' AND group_id=' . $groupid,
            ));
           $gpid = $_SESSION["gpid"];
		   $this->layout ="iframe";
		$model= new Users('grp_memberlistsearch');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];        
        
		$this->render('indexmemeberlist', array('model'=>$model));
        /*$gpmemebr = array();
        $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members',
                    'condition' => 'group_id='.$gpid, // static group id
        ));
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }
        $users = Yii::app()->db->createCommand()
                ->select(['pms_users.first_name', 'pms_users.last_name', 'pms_users.user_type',
                    'pms_user_roles.id as id', 'pms_user_roles.role', 'pms_users.status as status',
                    'pms_users.userid'])
                ->from('pms_users')
                ->leftJoin('pms_user_roles', 'pms_user_roles.id = pms_users.user_type')
                ->where(array('and', 'id>1', 'status=0'))
                //->where(array('not in', 'id', $gpmemebr))
                ->queryAll();

        $this->renderPartial('indexmemeberlist', array('groupmembers' => $gpmemebr, 'users' => $users,));*/
        }
    }
    

    public  function actionCountgroup(){
        $groups = Groups::model()->findAll();
        $countgp=count($groups);
        if($countgp==0){
            echo 'true';
        }else{
            echo 'false';
        }
    }
    
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = TimeEntry::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'time-entry-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
