<?php

class UsersiteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
        public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions'=>array('Showsearchdata'),
                        'users'=>array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );

	}
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Usersite;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Usersite']))
		{
			$model->attributes=$_POST['Usersite'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		//$model=$this->loadModel();
                //$model=new Usersite;
                echo "<pre>";
                print_r($_REQUEST['formdata']);
                
                $data = json_decode($_REQUEST['formdata'], true);
                
                print_r($data);
                //echo json_decode($_REQUEST['formdata']);
                die;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

//		if(isset($_POST['Usersite']))
//		{
//			$model->attributes=$_POST['Usersite'];
//			if($model->save())
//				$this->redirect(array('update'));
//		}
//
//		$this->render('update',array(
//			'model'=>$model,
//		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Usersite');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Usersite('search');
                $tbl = Yii::app()->db->tablePrefix;                
                $users = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from("{$tbl}users")                        
                        ->queryAll();                
                $clientsite = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from("{$tbl}clientsite")                        
                        ->queryAll();                   
                        //if(isset($_POST['submitusersite']) && isset($_POST['users1'])){ 
                        if(isset($_POST['submitusersite'])){                                                                                                         
                        extract($_POST);                                                        
                        $res = array();                          
                        $site = $site['new'];
                        $assigned_date = $Usersite['assigned_date']['new'];
                        if(!empty($site) && !empty($assigned_date)){                                                                                          
                        for($i=0;$i<count($users1);$i++){
                        $uid = $users1[$i];
                        $sql1 = "SELECT * FROM {$tbl}usersite WHERE user_id = '$uid'";                         
                        $result1 = Yii::app()->db->createCommand($sql1)->queryAll();                                                                                                
                        if(!empty($result1)){
                        $exist_id =  $result1[0]['id'];    
                        $sql2 = "UPDATE {$tbl}usersite SET site_id = '$site',assigned_date = '$assigned_date' WHERE id = '$exist_id' AND user_id = '$uid'";
                        $result2 = Yii::app()->db->createCommand($sql2)->execute();                                                        
                        }else{                
                        $res[$i] = "('$uid','$site','$assigned_date')";    
                        }                        
                        }                        
                        }
                        

//                        if(!empty($site['new']) && !empty($Usersite['assigned_date']['new'])){                           
//                        Yii::app()->db->createCommand()->truncateTable(Usersite::model()->tableName());    
//                        $sitenew =  (isset($site['new']) ? $site['new'] : '' ); 
//                        $assigned_date =  (isset($Usersite['assigned_date']['new']) ? $Usersite['assigned_date']['new'] : ''); 
//                        
//                        for($j=0;$j<count($users1);$j++){
//                        $id = (isset($users1[$j]) ? $users1[$j] : '' );
//                        $res[$j] = "('$id','$sitenew','$assigned_date')";    
//                        }                                                
//                        }else{                                                       
//                        for($i=0;$i<count($users1);$i++){                                                            
//                        $id = (isset($users1[$i]) ? $users1[$i] : '' );                                                                                                         
//                        $sitenew =  (isset($site[$id]) ? $site[$id] : '' ); 
//                        $assigned_date =  $Usersite['assigned_date'][$id];                      
//                                               
//                        if(!empty($id) && !empty($sitenew) && !empty($assigned_date)){
//                        
//                        $sql1 = "SELECT * FROM {$tbl}usersite WHERE user_id = $id";
//                        $result1 = Yii::app()->db->createCommand($sql1)->queryAll();                                                 
//                                               
//                        if(!empty($result1)){
//                        $exist_id =  $result1[0]['id'];    
//                        $sql2 = "UPDATE {$tbl}usersite SET site_id = '$sitenew',assigned_date = '$assigned_date' WHERE id = '$exist_id' AND user_id = '$id'";
//                        $result2 = Yii::app()->db->createCommand($sql2)->execute();                                                        
//                        }else{                
//                        $res[$i] = "('$id','$sitenew','$assigned_date')";    
//                        }
//                        }
//                        Yii::app()->user->setFlash('success', "Data updated successfully!");                        
//                        }    
//                        }
                        
                        
                        if(!empty($res)){                            
                        $str = implode(",",$res);                        
                        $sql = "INSERT INTO {$tbl}usersite (`user_id`,`site_id`,`assigned_date`) VALUES {$str}";                   
                        $result = Yii::app()->db->createCommand($sql)->execute();       
                        }
                        $this->redirect(array('admin'));
                        }
                        
                        $sql = "SELECT u.userid,u.first_name,u.last_name,us.*,u.designation,u.employee_id,c.* FROM {$tbl}users u INNER JOIN {$tbl}usersite us ON u.userid = us.user_id "
                        . " INNER JOIN {$tbl}clientsite c ON c.id = us.site_id";
                        $usersitedetails = Yii::app()->db->createCommand($sql);
                        $userlist = $usersitedetails->queryAll(); 
   
		$this->render('admin',array(
			'model'=>$model,'users'=>$users,'clientsite' => $clientsite,'ulist'=>$userlist 
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Usersite::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='usersite-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
      public function actionShowsitedata(){
        $model=new Usersite('search');  
        $tbl = Yii::app()->db->tablePrefix; 
        $clientsite = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from("{$tbl}clientsite")                        
                        ->queryAll();      
        $siteid = $_REQUEST['siteid'];
        if(!empty($siteid)){
        $userlist = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from("{$tbl}usersite")                        
                        ->where('site_id=:id', array(':id' => $siteid))
                        ->queryAll();
                                                               
          $sql = "SELECT u.userid,u.first_name,u.last_name,u.designation,u.employee_id,us.*,c.* FROM {$tbl}users u INNER JOIN {$tbl}usersite us ON u.userid = us.user_id "
                        . " INNER JOIN {$tbl}clientsite c ON c.id = us.site_id WHERE us.site_id = '$siteid'";
                        $usersitedetails = Yii::app()->db->createCommand($sql);
                        $userlist = $usersitedetails->queryAll(); 

        if(!empty($userlist)){
        //echo json_encode($userlist);        
        $list = $this->renderPartial('tableview',array('users'=>$userlist,'clientsite'=> $clientsite,'model'=>$model),true);
        echo $list;
        }else{
        echo "2";    
        }                                       
        }else{
        echo "2";    
        }          
        }
        public function actionShowuserdata(){
        $model=new Usersite('search');  
        $tbl = Yii::app()->db->tablePrefix; 
        $clientsite = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from("{$tbl}clientsite")                        
                        ->queryAll();      
        $userid = $_REQUEST['userid'];
        if(!empty($userid)){
        $userlist = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from("{$tbl}usersite")                        
                        ->where('user_id=:id', array(':id' => $userid))
                        ->queryAll();
                                                               
        $sql = "SELECT u.userid,u.first_name,u.last_name,u.designation,u.employee_id,us.*,c.* FROM {$tbl}users u INNER JOIN {$tbl}usersite us ON u.userid = us.user_id "
                        . " INNER JOIN {$tbl}clientsite c ON c.id = us.site_id WHERE u.userid = '$userid'";
                        $usersitedetails = Yii::app()->db->createCommand($sql);
                        $userlist = $usersitedetails->queryAll(); 

        if(!empty($userlist)){
        //echo json_encode($userlist);        
        $list = $this->renderPartial('tableview',array('users'=>$userlist,'clientsite'=> $clientsite,'model'=>$model),true);
        echo $list;
        }else{
        echo "2";    
        }                                       
        }else{
        echo "2";    
        }          
        }
         public function actionShowsearchdata(){
        $model=new Usersite('search');  
        $tbl = Yii::app()->db->tablePrefix; 
               
        $user       =   $_REQUEST['user'];
        $site       =   $_REQUEST['site'];
        $searchval  =   $_REQUEST['searchval'];
        
        $usercondition = '';
        if(!empty($user)){
        $usercondition = " AND u.userid = '$user'";
        }
        
        $sitecondition = '';
        if(!empty($site)){
        $sitecondition = " AND us.site_id = '$site'";    
        }
        
        $commoncondition = '';
        if(!empty($searchval)){
        $commoncondition = " AND u.designation LIKE '%$searchval%' OR u.employee_id LIKE '%$searchval%'";
        }
                                                               
        $sql = "SELECT u.userid,u.first_name,u.last_name,u.designation,u.employee_id,us.*,c.* FROM {$tbl}users u INNER JOIN {$tbl}usersite us ON u.userid = us.user_id "
                        . " INNER JOIN {$tbl}clientsite c ON c.id = us.site_id WHERE u.userid <> '' "
                        . " $usercondition $sitecondition $commoncondition";
                        $usersitedetails = Yii::app()->db->createCommand($sql);
                        $userlist = $usersitedetails->queryAll(); 

        if(!empty($userlist)){
        //echo json_encode($userlist);        
        $list = $this->renderPartial('tableview',array('users'=>$userlist,'model'=>$model),true);
        echo $list;
        }else{
        echo "2";    
        }                                                      
        }      
        
}
