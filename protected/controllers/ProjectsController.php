<?php

Yii::app()->getModule('masters');

class ProjectsController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;


        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow',
                'actions' => array(
                    'projectprogresscron',
                    'calandersheet',
                    'analyzerlink',
                    'downloadpdf',
                    'downloadexcel',
                    'createclient',
                    'assigneebehindmailcron',
                    'contractorbehindmailcron',
                    'minutesupdate',
                    'updaterank',
                    'searchproject',
                    'getCloneDetails',
                    'setCloneMilestone',
                    'setCloneArea',
                    'setCloneWorkType',
                    'setTasks',
                    'setWorkSite',
                    'meetingDetailedReport',
                    'updatemeetingreport',
                    'updatemeetingdetreport',
                    'savemeetingdetreport',
                    'gettaskForm',
                    'saveTask',
                    'test',
                    'add_project_mapping',
                    'addmom',
                    'momupdate',
                    'mom_meetingDetailedReport',
                    'projectPrefix',
                    'addProjectPrefix',
                    'sendDailyProgressEmail',
                    'getProjectParticipants',
                    'addParticipant',
                    'viewStep3FormData',
                    'getaddTaskModal'
                ),
                'users' => array('*'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    /*
      public function actionView($id) {
      $this->render('view', array(
      'model' => $this->loadModel($id),
      ));
      }
     */
    public function actionView($id)
    {
        $pid = $id;

        $milestones = Milestone::model()->findAll(array('condition' => 'project_id = ' . $id));
        $sites = Clientsite::model()->findAll(array('condition' => 'pid = ' . $id));
        $project = Projects::model()->findbyPk($id);
        $tblpx = Yii::app()->db->tablePrefix;




        $project_data_array = [];


        $milestone_det = Yii::app()->db->createCommand("select m.id,m.milestone_title,bh.budget_head_title,bh.budget_head_id,m.start_date,m.end_date from {$tblpx}milestone as m
        LEFT JOIN {$tblpx}budget_head as bh ON bh.budget_head_id = m.budget_id where m.project_id=" . $id . " GROUP BY m.id,m.budget_id")->queryAll();





        foreach ($milestone_det as $milestone) {

            $project_data_array[$milestone['budget_head_title']][] = array(
                'milestone_name' => $milestone['milestone_title'],
                'milestone_id' => $milestone['id'],
                'start_date' => $milestone['start_date'],
                'end_date' => $milestone['end_date']

            );
        }


        $date1 = $project->start_date;
        $date2 = $project->end_date;
        $output = [];
        $time = strtotime($date1);
        $last = date('M-Y', strtotime($date2));

        do {
            $month = date('M-Y', $time);
            //$month = date('Y-M', $time);
            $total = date('t', $time);

            $date_output[] = $month;

            $time = strtotime('+1 month', $time);
        } while ($month != $last);





        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
                'pid' => $pid,
                'milestones' => $milestones,
                'sites' => $sites,
                'project_data_array' => $project_data_array,
                'date_output' => $date_output
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Projects;
        $site_model = new Clientsite;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Projects'])) {

            $model->attributes = $_POST['Projects'];

            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');

            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');


            if ($model->save())
                $this->redirect(array('index'));
        }
        $this->render(
            'create',
            array(
                'model' => $model,
                'site_model' => $site_model,
            )
        );
    }

    public function actionUpdateRank()
    {
        if (isset($_POST['rankingitems']) && count($_POST['rankingitems'])) {
            $items = $_POST['rankingitems'];

            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();
            try {
                $sql = "update " . Milestone::model()->tableName() . " set ";
                foreach ($items as $i => $item) {
                    $milestone_model = Milestone::model()->findByPk($item[0]);
                    if ($milestone_model === null)
                        throw new Exception('No Milestone ID [' . $item[0] . "] exists");

                    $milestone_model->ranking = $item[1];
                    if (!$milestone_model->save()) {
                        throw new Exception($milestone_model->getErrors());
                    }
                }
                $transaction->commit();
                $returnmsg = array('status_color' => 'green', 'msg' => 'Rank updated successfully');
            } catch (Exception $e) {
                $returnmsg = array('status_color' => 'red', 'msg' => $e->getMessage());
                $transaction->rollback();
            } finally {
                echo json_encode($returnmsg);
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $project_drpdwn_html = '';
        $milestone_model = new Milestone('search');
        $milestone_model->unsetAttributes();  // clear any default values
        if (isset($_GET['Milestone']))
            $milestone_model->attributes = $_GET['Milestone'];

        $budget_head_model = '';

        if (in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) {

            $budget_head_model = new BudgetHead('search');
            $budget_head_model->unsetAttributes();  // clear any default values
            if (isset($_GET['BudgetHead']))

                $budget_head_model->attributes = $_GET['BudgetHead'];
        }

        $area_model = new Area('search');
        $area_model->unsetAttributes();  // clear any default values
        if (isset($_GET['Area']))
            $area_model->attributes = $_GET['Area'];
        $work_type_model = new ProjectWorkType('search');
        $work_type_model->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectWorkType']))
            $work_type_model->attributes = $_GET['ProjectWorkType'];
        $work_site_model = new Clientsite('search');
        if (isset($_GET['Clientsite']))
            $work_site_model->attributes = $_GET['Clientsite'];
        $user_assign_model = new ClientsiteAssign;
        $task = new Tasks('mainTaskSearch');
        $task->setscenario('mainTasks');
        $task->unsetAttributes();  // clear any default values
        if (isset($_GET['Tasks']))
            $task->attributes = $_GET['Tasks'];
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_POST['Projects'])) {

            $img_path = $model->img_path;
            $report_image = $model->report_image;
            $model->attributes = $_POST['Projects'];
            $model->template_id = $_POST['Projects']['template_id'];
            if (isset($_POST['Projects']['assigned_to'])) {
                $data = implode(',', $_POST['Projects']['assigned_to']);
                $model->assigned_to = $data;
            }
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->start_date = date('Y-m-d', strtotime($_POST['Projects']['start_date']));
            $model->end_date = date('Y-m-d', strtotime($_POST['Projects']['end_date']));
            $images_path = realpath(Yii::app()->basePath . '/../uploads/project');

            $images_path2 = realpath(Yii::app()->basePath . '/../uploads/project/thumbnail');

            if ($_FILES['Projects']['name']['img_path']) {
                $filename = CUploadedFile::getInstance($model, 'img_path');
                $newfilename = rand(1000, 9999) . time();
                $newfilename = md5($newfilename); //optional
                $extension = $filename->getExtensionName();
                $model->img_path = $newfilename . "." . $extension;
            }
            if ($_FILES['Projects']['name']['report_image']) {
                $filename_report = CUploadedFile::getInstance($model, 'report_image');
                $newfilename_report = rand(1000, 9999) . time();
                $newfilename_report = md5($newfilename_report); //optional
                $extension_report = $filename_report->getExtensionName();
                $model->report_image = $newfilename_report . "." . $extension_report;
            }

            if ($model->save()) {
                Yii::app()->user->setState('project_id', $model->pid);
                if ($_FILES['Projects']['name']['img_path']) {
                    $path = realpath(Yii::app()->basePath . '/../uploads/project/' . $img_path);
                    $path2 = realpath(Yii::app()->basePath . '/../uploads/project/thumbnail/' . $img_path);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                    if (file_exists($path2)) {
                        unlink($path2);
                    }
                    if ($_FILES['Projects']['name']['img_path']) {
                        $uploadfile = $filename->saveAs($images_path . '/' . $newfilename . "." . $extension);
                        $image = new EasyImage($images_path . '/' . $newfilename . "." . $extension);
                        $image->resize(60, 60);
                        $image->save($images_path2 . '/' . $newfilename . "." . $extension);
                    }
                    if ($_FILES['Projects']['name']['report_image']) {
                        $uploadfile = $filename_report->saveAs($report_images_path . '/' . $newfilename_report . "." . $extension_report);
                        $image = new EasyImage($report_images_path . '/' . $newfilename_report . "." . $extension_report);
                        $image->resize(60, 60);
                        $image->save($images_path2 . '/' . $newfilename_report . "." . $extension_report);
                    }
                }

                if (isset($_POST['submit_type'])) {
                    if ($_POST['submit_type'] == 'save_btn') {
                        $return_result = array('status' => 1, 'next_level' => 0);
                    } elseif ($_POST['submit_type'] == 'save_cnt') {
                        $project_drpdwn_html .= "<option value=" . $model->pid . ">" . $model->name . "</option>";
                        $project_start_date = date('d-M-y', strtotime($model->start_date));
                        $project_end_date = date('d-M-y', strtotime($model->end_date));
                        $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html, 'project_start' => $project_start_date, 'project_end' => $project_end_date);
                    } else {
                        $return_result = array('status' => 1, 'next_level' => 0);
                    }
                    echo json_encode($return_result);
                    exit;
                }
            } else {
                $return_result = array('status' => 2, 'next_level' => 0);
                echo json_encode($return_result);
                exit;
            }
        }
        if (isset($model->assigned_to)) {
            $assigned_to = $model->assigned_to;
        } else {
            $assigned_to = '';
        }
        $template = Template::model()->findAll();
        $project_no = $model->project_no;
        $project_participants = ProjectParticipants::model()->findAll(
            array("condition" => "project_id =  $id", "order" => "project_id")
        );

        if (count($project_participants) == 0) {
            $body_array = [

                array("", "", "", "", ""),
                array("", "", "", "", ""),
                array("", "", "", "", ""),

            ];
        }

        foreach ($project_participants as $participants) {
            $body_array[] = array(
                $participants->participants,
                $participants->participant_initial,
                $participants->designation,
                $participants->organization_name,
                $participants->organization_initial,
                $participants->id

            );
        }
        $body_array = json_encode($body_array);
        $this->render(
            'update',
            array(
                'model' => $model,
                'assigned_to' => $assigned_to,
                'milestone_model' => $milestone_model,
                'area_model' => $area_model,
                'work_type_model' => $work_type_model,
                'work_site_model' => $work_site_model,
                'user_assign_model' => $user_assign_model,
                'task' => $task,
                'template' => $template,
                'budget_head_model' => $budget_head_model,
                'project_no' => $project_no,
                'body_array' => $body_array
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {

        $model = new Projects('search');
        $model->unsetAttributes();  // clear any default values
        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 

        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];

        $this->render(
            'index',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Projects('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    public function actionaddproject()
    {
        $project_clone_id = Yii::app()->session['project_clone_id'];
        $project_drpdwn_html = '';
        $model = new Projects;
        $existing_status = 0;
        $prev_assigned_managers = '';
        $previous_template_id = '';
        $milestone_model = new Milestone();
        $milestone_model->unsetAttributes();  // clear any default values  
        if (isset($_GET['Milestone']))
            $milestone_model->attributes = $_GET['Milestone'];

        $budget_head_model = '';
        if (in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) {

            $budget_head_model = new BudgetHead();
            $budget_head_model->unsetAttributes();  // clear any default values
            if (isset($_GET['BudgetHead']))
                $budget_head_model->attributes = $_GET['BudgetHead'];
        }


        $area_model = new Area('search');
        $area_model->unsetAttributes();  // clear any default values
        if (isset($_GET['Area']))
            $area_model->attributes = $_GET['Area'];
        $work_site_model = new Clientsite('search');
        if (isset($_GET['Clientsite']))
            $work_site_model->attributes = $_GET['Clientsite'];
        $user_assign_model = new ClientsiteAssign;
        $model2 = new Clients;
        $task = new Tasks('mainTaskSearch');
        $task->setscenario('mainTasks');
        $task->unsetAttributes();  // clear any default values
        if (isset($_GET['Tasks']))
            $task->attributes = $_GET['Tasks'];
        if (isset($_POST['Projects']['pid']) && !empty($_POST['Projects']['pid'])) {
            $existing_status = 1;
            $id = $_POST['Projects']['pid'];
            $model = $this->loadModel($id);
            $previous_template_id = $model->template_id;
            $prev_assigned_managers = $model->assigned_to;
            $project_no = $model->project_no;
        } else {
            $tblpx = Yii::app()->db->tablePrefix;
            $maxData = Yii::app()->db->createCommand("SELECT MAX(CAST(pid as signed)) as largenumber FROM {$tblpx}projects")->queryRow();
            $maxNo = $maxData["largenumber"];
            $newPONo = $maxNo + 1;
            $prNo = $newPONo;
            $digit = strlen((string) $prNo);
            if ($digit == 1) {
                $project_id = '000' . $prNo;
            } else if ($digit == 2) {
                $project_id = '00' . $prNo;
            } else {
                $project_id = '0' . $prNo;
            }
            $project_no = " HPMC-PR" . $project_id . '-' . date('Y') . '-' . date('m');
        }
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $work_type_model = new ProjectWorkType('search');
        $work_type_model->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectWorkType']))
            $work_type_model->attributes = $_GET['ProjectWorkType'];
        if (isset($_POST['Projects'])) {

            $filename = CUploadedFile::getInstance($model, "img_path");
            if (isset($_POST['Projects']['assigned_to']) && !empty($_POST['Projects']['assigned_to'])) {
                $data = implode(',', $_POST['Projects']['assigned_to']);
                $model->assigned_to = $data;
            }
            if (isset($_POST['Projects']['template_id'])) {
                $model->template_id = $_POST['Projects']['template_id'];
            }

            // if (isset($_POST['Projects']['project_Id'])) {
            //     $model->project_Id = $_POST['Projects']['project_Id'];
            // }

            $images_path = realpath(Yii::app()->basePath . '/../uploads/project');
            $images_path2 = realpath(Yii::app()->basePath . '/../uploads/project/thumbnail');
            $report_images_path = realpath(Yii::app()->basePath . '/../uploads/project/report_imgs');

            if ($_FILES['Projects']['name']['img_path']) {
                $filename = CUploadedFile::getInstance($model, 'img_path');
                $newfilename = rand(1000, 9999) . time();
                $newfilename = md5($newfilename); //optional
                $extension = $filename->getExtensionName();
                $model->img_path = $newfilename . "." . $extension;
            }
            if ($_FILES['Projects']['name']['report_image']) {
                $filename_report = CUploadedFile::getInstance($model, 'report_image');
                $newfilename_report = rand(1000, 9999) . time();
                $newfilename_report = md5($newfilename_report); //optional
                $extension_report = $filename_report->getExtensionName();
                $model->report_image = $newfilename_report . "." . $extension_report;
            }
            $model->attributes = $_POST['Projects'];
            // $site_model->attributes = $_POST['Clientsite'];
            $model->assigned_to = $data;
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            // $model->project_no = $project_no;
            $model->project_no = $_POST['Projects']['project_no'];
            if (isset($_POST['Projects']['architect'])) {
                $model->architect = $_POST['Projects']['architect'];
            }
            if (isset($_POST['Projects']['site_calendar_id'])) {
                $model->site_calendar_id = $_POST['Projects']['site_calendar_id'];
            }
            if (isset($_POST['Projects']['project_clone_name'])) {
                if ($_POST['Projects']['project_clone_name'] != '') {
                    $model->clone_id = $_POST['Projects']['project_clone_name'];
                    $clone_project = Projects::model()->findByPk($model->clone_id);
                    $model->start_date = $clone_project->start_date;
                    $model->end_date = $clone_project->end_date;
                    $model->clone_start_date = date('Y-m-d', strtotime($_POST['Projects']['start_date']));
                    $model->clone_end_date = date('Y-m-d', strtotime($_POST['Projects']['end_date']));
                } else {

                    if (!empty($_POST['Projects']['start_date'])) {
                        $model->start_date = date('Y-m-d', strtotime($_POST['Projects']['start_date']));
                    }
                    if (!empty($_POST['Projects']['end_date'])) {
                        $model->end_date = date('Y-m-d', strtotime($_POST['Projects']['end_date']));
                    } else {
                        $model->end_date = NULL;
                    }
                }
            } else {

                if (!empty($_POST['Projects']['start_date'])) {
                    $model->start_date = date('Y-m-d', strtotime($_POST['Projects']['start_date']));
                }
                if (!empty($_POST['Projects']['end_date'])) {
                    $model->end_date = date('Y-m-d', strtotime($_POST['Projects']['end_date']));
                }
            }

            //$transaction = Yii::app()->db->beginTransaction();
            try {
                if ($model->save()) {

                    if (isset($_POST['participants'])) {
                        $project_id = $model->pid;
                        if (isset($id) && $id) {
                            $project_participants = ProjectParticipants::model()->findAll(
                                array("select" => "id", "condition" => "project_id =  $id", "order" => "project_id")
                            );
                            $participant_ids = CHtml::listData($project_participants, 'id', 'id');
                        }
                        $participants = json_decode($_POST['participants']);
                        foreach ($participants as $participant) {
                            if (isset($participant[5]) && isset($participant_ids) && in_array($participant[5], $participant_ids)) {
                                unset($participant_ids[$participant[5]]);
                            }
                            if ($participant[0] != '' and $participant[2] != '') {
                                $this->saveProjectParticipants($project_id, $participant);
                            }
                        }
                        if (isset($participant_ids) && count($participant_ids)) {
                            $result = ProjectParticipants::model()->deleteAll(
                                array('condition' => 'id  IN (' . implode(',', $participant_ids) . ')', )
                            );
                        }


                    }

                    if ($model->template_id != "") {
                        $template_model = Template::model()->findByPK($model->template_id);
                        $template_model->status = 1;
                        $template_model->save();
                    }

                    if ($previous_template_id != "") {
                        $Criteria = new CDbCriteria();
                        $Criteria->condition = "template_id = '" . $previous_template_id . "' ";
                        $already_used = Projects::model()->findAll($Criteria);

                        if (count($already_used) == 0) {
                            $template_model = Template::model()->findByPK($previous_template_id);
                            $template_model->status = 0;
                            $template_model->save();
                        }
                    }


                    Yii::app()->user->setState('project_id', $model->pid);
                    if (!isset($_POST['Projects']['pid'])) {

                        if ($model->clone_id != '') {

                            $new_project_start_date = date('Y-m-d', strtotime($_POST['Projects']['start_date']));
                            $new_project_end_date = date('Y-m-d', strtotime($_POST['Projects']['end_date']));
                            $this->setCloneMilestone($model->pid, $model->clone_id);
                            $this->setCloneArea($model->pid, $model->clone_id);
                            $this->setCloneWorkType($model->pid, $model->clone_id);
                            $this->setTasks($model->pid, $model->clone_id, $new_project_start_date, $new_project_end_date);
                            $this->setWorkSite($model->pid, $model->clone_id);
                        }
                    }

                    if ($_FILES['Projects']['name']['img_path']) {
                        $uploadfile = $filename->saveAs($images_path . '/' . $newfilename . "." . $extension);
                        $image = new EasyImage($images_path . '/' . $newfilename . "." . $extension);
                        $image->resize(60, 60);
                        $image->save($images_path2 . '/' . $newfilename . "." . $extension);
                    }
                    if ($_FILES['Projects']['name']['report_image']) {
                        $uploadfile = $filename_report->saveAs($report_images_path . '/' . $newfilename_report . "." . $extension_report);
                        $image = new EasyImage($report_images_path . '/' . $newfilename_report . "." . $extension_report);
                        $image->resize(60, 60);
                        $image->save($images_path2 . '/' . $newfilename_report . "." . $extension_report);
                    }


                    if (in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) {


                        if (isset($_POST['submit_type'])) {
                            if ($_POST['submit_type'] == 'save_btn') {
                                if ($existing_status == 1) {
                                    $return_result = array('status' => 1, 'next_level' => 0, 'assigned_users' => $model->assigned_to, 'new_record' => $existing_status, 'stage' => 'budget');
                                } else {
                                    $return_result = array('status' => 1, 'next_level' => 0, 'new_record' => $existing_status, 'stage' => 'budget');
                                }
                            } elseif ($_POST['submit_type'] == 'save_cnt') {

                                $project_drpdwn_html .= "<option value=" . $model->pid . ">" . $model->name . "</option>";



                                if ($model->clone_id != '') {
                                    $project_start_date = date('d-M-y', strtotime($model->clone_start_date));
                                    $project_end_date = date('d-M-y', strtotime($model->clone_end_date));
                                } else {
                                    $project_start_date = date('d-M-y', strtotime($model->start_date));
                                    $project_end_date = date('d-M-y', strtotime($model->end_date));
                                }


                                $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html, 'project_start' => $project_start_date, 'project_end' => $project_end_date, 'stage' => 'budget');
                            } else {
                                $return_result = array('status' => 1, 'next_level' => 0, 'stage' => 'budget');
                            }
                        }
                    } else {
                        if (isset($_POST['submit_type'])) {
                            if ($_POST['submit_type'] == 'save_btn') {
                                if ($existing_status == 1) {
                                    $return_result = array('status' => 1, 'next_level' => 0, 'assigned_users' => $model->assigned_to, 'new_record' => $existing_status, 'stage' => 'milestone');
                                } else {
                                    $return_result = array('status' => 1, 'next_level' => 0, 'new_record' => $existing_status, 'stage' => 'milestone');
                                }
                            } elseif ($_POST['submit_type'] == 'save_cnt') {

                                $project_drpdwn_html .= "<option value=" . $model->pid . ">" . $model->name . "</option>";



                                if ($model->clone_id != '') {
                                    $project_start_date = date('d-M-y', strtotime($model->clone_start_date));
                                    $project_end_date = date('d-M-y', strtotime($model->clone_end_date));
                                } else {
                                    $project_start_date = date('d-M-y', strtotime($model->start_date));
                                    $project_end_date = date('d-M-y', strtotime($model->end_date));
                                }


                                $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html, 'project_start' => $project_start_date, 'project_end' => $project_end_date, 'stage' => 'milestone');
                            } else {
                                $return_result = array('status' => 0, 'next_level' => 0, 'stage' => 'milestone');
                            }
                        }
                    }
                    //email notification for assigned project managers                
                    if (!empty($model->assigned_to)) {
                        $mail_send = $this->assignedManagersMail($model, $prev_assigned_managers);
                        if ($mail_send !== true) {
                            throw new Exception($mail_send);
                        }
                    }
                } else {
                    throw new Exception($model->getErrors());
                    $return_result = array('status' => 2, 'next_level' => 0, 'error' => $model->getErrors());
                }
                //$transaction->commit();
            } catch (Exception $error) {
                // $transaction->rollback();
                // $return_result = array(
                //     'status' => 2,
                //     'next_level' => 0, 'error' => $error->getMessage() . 'khdkj'
                // );
                if ($model->clone_id != '') {
                    $project_start_date = date('d-M-y', strtotime($model->clone_start_date));
                    $project_end_date = date('d-M-y', strtotime($model->clone_end_date));
                } else {
                    $project_start_date = date('d-M-y', strtotime($model->start_date));
                    $project_end_date = date('d-M-y', strtotime($model->end_date));
                }
                // $project_drpdwn_html .= "<option value=" . $model->pid . ">" . $model->name . "</option>";
                $project_drpdwn_html .= "";



                if (in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) {

                    if ($_POST['submit_type'] == 'save_btn') {
                        $return_result = array('status' => 1, 'next_level' => 0, 'html' => $project_drpdwn_html, 'project_start' => $project_start_date, 'project_end' => $project_end_date, 'stage' => 'budget');
                    } elseif ($_POST['submit_type'] == 'save_cnt') {
                        $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html, 'project_start' => $project_start_date, 'project_end' => $project_end_date, 'stage' => 'budget');
                    }
                } else {

                    if ($_POST['submit_type'] == 'save_btn') {
                        $return_result = array('status' => 1, 'next_level' => 0, 'html' => $project_drpdwn_html, 'project_start' => $project_start_date, 'project_end' => $project_end_date, 'stage' => 'milestone');
                    } elseif ($_POST['submit_type'] == 'save_cnt') {
                        $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html, 'project_start' => $project_start_date, 'project_end' => $project_end_date, 'stage' => 'milestone');
                    }
                }




                echo json_encode($return_result);
                exit;
            } finally {
                echo json_encode($return_result);
                exit;
            }
        }
        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])) {

            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------




        $template = Template::model()->findAll();

        $tblpx = Yii::app()->db->tablePrefix;
        $maxData = Yii::app()->db->createCommand("SELECT MAX(CAST(pid as signed)) as largenumber FROM {$tblpx}projects")->queryRow();
        $maxNo = $maxData["largenumber"];
        $newPONo = $maxNo + 1;
        $prNo = $newPONo;
        $digit = strlen((string) $prNo);
        if ($digit == 1) {
            $project_id = '000' . $prNo;
        } else if ($digit == 2) {
            $project_id = '00' . $prNo;
        } else {
            $project_id = '0' . $prNo;
        }


        $tableSchema = "SHOW TABLES  LIKE '%pms_project_prefix%'";

        $databasetbl = Yii::app()->db->createCommand($tableSchema)->queryRow();

        if ($databasetbl == null) {
            $project_no = " HPMC-PR" . $project_id . '-' . date('Y') . '-' . date('m');
        } else {

            $query = "SELECT * FROM pms_project_prefix ";
            $data = Yii::app()->db->createCommand($query)->queryRow();

            $prefix = $data['prefix'];
            $project_no = $prefix . "-" . $project_id . '-' . date('Y') . '-' . date('m');
        }

        $body_array = [

            array("", "", "", "", ""),
            array("", "", "", "", ""),
            array("", "", "", "", ""),

        ];
        $body_array = json_encode($body_array);
        $this->render(
            'create',
            array(
                'model' => $model,
                'model2' => $model2,
                'milestone_model' => $milestone_model,
                'area_model' => $area_model,
                'work_site_model' => $work_site_model,
                'user_assign_model' => $user_assign_model,
                'work_type_model' => $work_type_model,
                'task' => $task,
                'template' => $template,
                'budget_head_model' => $budget_head_model,
                'project_no' => $project_no,
                'body_array' => $body_array

            )
        );
    }

    public function actionaddAssigneetoProjects()
    {
        $model = new Projects('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];

        $this->render(
            'assigneetoprojects',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Projects::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'projects-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAutocomplete()
    {
        $res = array();


        if (isset($_GET['term'])) {

            $qtxt = "SELECT name FROM {{projects}} WHERE name LIKE :name";
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":name", '%' . $_GET['term'] . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    public function actionassigneecreate()
    {
        $this->layout = false;
        $model = new Projects;
        if (isset($_POST['Projects'])) {

            $pid = $_POST['Projects']['name'];
            $model = $this->loadModel($pid);
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);
            $model->assigned_to = $_POST['Projects']['assigned_to'];
            $model->start_date = date('Y-m-d', strtotime($_POST['Projects']['start_date']));
            $model->end_date = date('Y-m-d', strtotime($_POST['Projects']['end_date']));
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated.');
                $this->redirect(array('addAssigneetoProjects'));
            }
        }

        $this->render(
            'assigneecreate',
            array(
                'model' => $model,
            )
        );
    }

    public function actionassigneeUpdate($id)
    {

        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Projects'])) {
            $model->attributes = $_POST['Projects'];
            $model->name = $_POST['Projects']['name'];
            $model->assigned_to = $_POST['Projects']['assigned_to'];
            $model->start_date = date('Y-m-d', strtotime($_POST['Projects']['start_date']));
            $model->end_date = date('Y-m-d', strtotime($_POST['Projects']['end_date']));
            if ($model->save()) {
                $this->redirect(array('addAssigneetoProjects'));
            }
        }

        $this->render(
            'assigneeupdate',
            array(
                'model' => $model,
            )
        );
    }

    public function actionwprprojectsview($id)
    {
        $this->render(
            'wpr_projects_view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    public function actionChart($id = false)
    {
        if (!empty($_GET['id'])) {
            $this->render('chart', ['id' => $_GET['id']]);
        } else {
            $this->render('chart');
        }
    }

    public function ResultData($projects)
    {
        foreach ($projects as $project) {
            $tasks = Tasks::model()->findAll(['condition' => '`trash`=1 AND project_id="' . $project['pid'] . '"', 'order' => 'start_date']);
            if (!empty($tasks)) {
                $project['start_date'] = date('m/d/Y', strtotime($tasks[0]['start_date']));
                $project['project_end'] = date('m/d/Y', strtotime($tasks[0]['due_date']));
                $status_array = array();
                if (!empty($tasks)) {
                    foreach ($tasks as $task) {
                        $status_array[] = $task->status;
                        if ($task['due_date'] > $project['project_end'])
                            $project['project_end'] = $task['due_date'];
                        if ($task->status == '7') {
                            $status = 'STATUS_DONE';
                        } elseif ($task->status == '6' || $task->status == '73' || $task->status == '74') {
                            $status = 'STATUS_UNDEFINED';
                        } elseif ($task->status == '76' || $task->status == '72') {
                            $status = 'STATUS_SUSPENDED';
                            $project_status = 'STATUS_SUSPENDED';
                        } elseif ($task->status == '5') {
                            $status = 'STATUS_FAILED';
                        } elseif ($task->status == '77') {
                            $project_status = 'STATUS_ACTIVE';
                            $status = 'STATUS_ACTIVE';
                        }

                        if (array_unique($status_array) === array('5')) {
                            $project_status = 'STATUS_FAILED';
                            $project_status_id = 81;
                        } elseif (array_unique($status_array) === array('76')) {
                            $project_status = 'STATUS_SUSPENDED';
                            $project_status_id = 80;
                        } elseif (array_unique($status_array) === array('7')) {
                            $project_status = 'STATUS_DONE';
                            $project_status_id = 82;
                        } else {
                            $project_status = 'STATUS_UNDEFINED';
                            $project_status_id = 78;
                        }

                        //  var_dump($task['description']);
                        //  $decription = trim(preg_replace('/\s\s+/', ' ', $task['description']));
                        $decription = json_encode($task['description']);
                        $project_tasks[] = ["title" => $task['title'], 'start_date' => $task['start_date'], 'due_date' => $task['due_date'], 'assignee' => $task->assignedTo->first_name, 'id' => $task['tskid'], 'assign_id' => $task['assigned_to'], 'desc' => $decription, 'status' => $status];
                    }

                    $project['project_status'] = $project_status;

                    $count_values = array_count_values($status_array);
                    if ((!empty($count_values['77'])) && ((count($status_array) / 2) <= $count_values['77'])) {
                        $project_status = 'STATUS_ACTIVE';
                        $project_status_id = 79;
                    } elseif ((!empty($count_values['5'])) && ((count($status_array) / 2) <= $count_values['5'])) {

                        $project_status = 'STATUS_FAILED';
                        $project_status_id = 81;
                    }

                    $project_model = Projects::model()->find(['condition' => 'pid="' . $project['pid'] . '"']);
                    if (!empty($project_model)) {
                        $project_model->project_status = $project_status_id;
                        $project_model->save();
                    }
                    $project['project_status'] = $project_status;
                }
            }
            $data[] = array_merge(array($project_tasks), $project);
            $project_tasks = '';
        }

        return $data;
    }

    public function actionProjectProgressCron()
    {

        $today = date('Y-m-d');
        $tasks = Yii::app()->db->createCommand("SELECT * FROM `pms_tasks` WHERE `task_type` = 1 AND
        `due_date` >= '$today' AND `start_date` <= '$today' AND `trash`= 1 AND `status` = 6 ")->queryAll();
        if (count($tasks) > 0) {
            foreach ($tasks as $task) {
                $data[] = $this->taskprogressnotificationcron($task);
            }
            $this->sendAdminMail($data, '1');
        }
    }

    public function actionAssigneeBehindMailCron()
    {

        $today = date('Y-m-d');

        $assigned_user_ids = Yii::app()->db->createCommand("SELECT assigned_to FROM `pms_tasks`  WHERE  `task_type` = 1 AND
            `due_date` >= '$today' AND `start_date` <= '$today' AND `trash`= 1 AND `status` = 6 GROUP BY assigned_to")->queryAll();
        $assigned_user_ids = array_column($assigned_user_ids, 'assigned_to');

        foreach ($assigned_user_ids as $assigned_user_id) {
            $tasks = '';

            $tasks = Yii::app()->db->createCommand("SELECT * FROM `pms_tasks`  WHERE assigned_to = $assigned_user_id AND `task_type` = 1 AND
                `due_date` >= '$today' AND `start_date` <= '$today' AND `trash`= 1 AND `status` = 6")->queryAll();
            $data = array();
            foreach ($tasks as $task) {

                $data[] = $this->taskprogressnotificationcron($task);
            }
            $model2 = new MailLog;
            $today = date('Y-m-d H:i:s');
            $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
            $mail_data = $this->renderPartial('_task_progress_mail', array('data' => $data), true);
            $mail = new JPhpMailer();
            $headers = "Hostech";
            $bodyContent = $mail_data;

            $mail->IsSMTP();
            $mail->Host = $mail_model['smtp_host'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
            $mail->Subject = "Task Progress Notification";
            $assigned_model = Users::model()->findByPk($assigned_user_id);
            if (!empty($assigned_model->email)) {
                $mail->addAddress($assigned_model->email); // Add a recipient
                // $mail->addAddress('surumi.ja@bluehorizoninfotech.com');
            }
            $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
            $dataJSON = json_encode($data);
            $mail_send_byJSON = json_encode($mail_send_by);
            $subject = $mail->Subject;
            $model2->send_to = $assigned_model->email;
            $model2->send_by = $mail_send_byJSON;
            $model2->send_date = $today;
            $model2->message = htmlentities($bodyContent);
            $model2->description = $mail->Subject;
            $model2->created_date = $today;
            $model2->created_by = Yii::app()->user->getId();
            $model2->mail_type = $subject;
            $mail->isHTML(true);
            $mail->MsgHTML($bodyContent);
            $mail->Body = $bodyContent;
            if (!empty($assigned_model->email)) {
                if ($mail->Send()) {
                    $model2->sendemail_status = 1;
                } else {
                    $model2->sendemail_status = 0;
                }
            } else {
                $model2->sendemail_status = 0;
            }

            $model2->save();
        }
    }

    protected function taskprogressnotificationcron($task)
    {

        $progress_data = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $task['tskid'] . "' AND approve_status = 1")->queryRow();

        $daily_target = $task['daily_target'];
        $per_day_percenatge = ($daily_target / $task['quantity']) * 100;
        $start = new DateTime($task['start_date']);
        $end = new DateTime($task['due_date']);
        $workingdays = 0;
        $days_gone = 0;
        if ($start == $end) {
            $workingdays = 1;
            $days_gone = 1;
            $todays_expected_percentage = 100;
        } else {
            if (!empty($task['clientsite_id'])) {
                $site_id = $task['clientsite_id'];
                $status = 1;
            } else {
                $site_id = 1;
                $status = 0;
            }
            $start_date = $task['start_date'];
            $due_date = $task['due_date'];
            $Criteria = new CDbCriteria();
            $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
            $Criteria->addCondition('site_id = ' . $site_id);
            $Criteria->addCondition('status = ' . $status);
            $events = CalenderEvents::model()->findAll($Criteria);
            $event_array = array();
            foreach ($events as $event) {
                array_push($event_array, $event->event_date);
            }

            $days_gone = 0;
            $start_date_data = new DateTime($task['start_date']);
            $todays_date = new DateTime(date('Y-m-d'));
            $todays_date->modify('+1 day');
            $period = new DatePeriod(
                $start_date_data,
                new DateInterval('P1D'),
                $todays_date
            );

            foreach ($period as $key => $value) {
                if (!in_array($value->format('Y-m-d'), $event_array)) {
                    $days_gone++;
                }
            }
            $todays_expected_percentage = $days_gone * $per_day_percenatge;
            if ($todays_expected_percentage > 100) {
                $todays_expected_percentage = 100;
            }
        }


        if (!empty($progress_data['work_progress'])) {

            $data = $this->addprogressnotificationcron($days_gone, $task, $todays_expected_percentage, $progress_data['work_progress']);
        } else {

            $data = $this->addprogressnotificationcron($days_gone, $task, $todays_expected_percentage, 0);
        }

        return $data;
    }

    protected function taskprogressnotification($task, $time_entry_array = false)
    {
        $days_gone = 0;
        /* Event array getting starts */
        if (!empty($task['clientsite_id'])) {
            $site_id = $task['clientsite_id'];
            $status = 1;
        } else {
            $site_id = 1;
            $status = 0;
        }
        $Criteria = new CDbCriteria();
        $Criteria->addBetweenCondition("event_date", $task['start_date'], $task['due_date'], 'AND');
        $Criteria->addCondition('site_id = ' . $site_id);
        $Criteria->addCondition('status = ' . $status);
        $events = CalenderEvents::model()->findAll($Criteria);
        $event_array = array();
        foreach ($events as $event) {
            array_push($event_array, $event->event_date);
        }
        /* Events array ends */
        $daily_target = $task['daily_target'];
        $per_day_percenatge = 0;

        // new changes by jitha starts

        // if (!empty($task['quantity'])) {
        //   $per_day_percenatge = ($daily_target / $task['quantity']) * 100;
        // }

        if ($task['task_type'] == 1) {
            if (!empty($task['quantity'])) {
                $per_day_percenatge = ($daily_target / $task['quantity']) * 100;
            }
        } else {

            // $date1 = new DateTime($task['start_date']);
            // $date2 = new DateTime($task['due_date']);
            // $interval = $date1->diff($date2);
            // $days = $interval->days;
            // $per_day_percenatge = 100 / $days;

            $per_day_percenatge = (1 / $task['task_duration']) / 100;
        }

        // changes by jitha end





        $startDate = strtotime($task['start_date']);
        $endDate = strtotime($task['due_date']);
        $start = new DateTime($task['start_date']);
        $end = new DateTime($task['due_date']);
        $today_ = new DateTime(date('Y-m-d'));
        $interval = DateInterval::createFromDateString('1 day');
        $end->setTime(0, 0, 1);
        $today_->setTime(0, 0, 1);
        $period = new DatePeriod($start, $interval, $end);
        $period_today = new DatePeriod($start, $interval, $today_);
        $workingdays = 0;
        $days_gone = 0;
        if ($startDate > strtotime(date('Y-m-d'))) {
            $todays_expected_percentage = 0;
        } else {
            if ($start == $end) {
                $workingdays = 1;
                $days_gone = 1;
                $todays_expected_percentage = 100;
            } else {
                $days_gone = 0;
            }
        }

        $result_data = array();

        foreach ($period as $day) {

            /* days gone finding start */
            if ($days_gone == 0) {
                $start_date_data = new DateTime($task['start_date']);
                $todays_date = new DateTime($day->format('Y-m-d'));
                $todays_date->modify('+1 day');
                $period_dates = new DatePeriod(
                    $start_date_data,
                    new DateInterval('P1D'),
                    $todays_date
                );

                foreach ($period_dates as $key => $value) {
                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                        $days_gone++;
                    }
                }
                $todays_expected_percentage = $days_gone * $per_day_percenatge;
            }

            /* days gone finding ends */




            if ($endDate < strtotime(date('Y-m-d'))) {
                $today_ = new DateTime($day->format('Y-m-d'));
                $period_today = new DatePeriod($start, $interval, $today_);
                $days_gone = 1;
                foreach ($period_today as $day_) {

                    // $day is not saturday nor sunday
                    if (!in_array($day_->format('w'), [0, 6])) {
                        $days_gone++;
                    }
                }

                $tolerance_percentage = 100 / $days_gone;
                // echo '<pre>';
                // print_r($tolerance_percentage);
                $todays_expected_percentage = ($days_gone * $tolerance_percentage) - $tolerance_percentage;
            }
            $entry_check_date = $day->format('Y-m-d');

            if ($task['task_type'] == 1) {
                $progress_data = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $task['tskid'] . "' AND approve_status = 1 AND date = '" . $entry_check_date . "'")->queryRow();
            } else {
                $progress_data = Yii::app()->db->createCommand("SELECT SUM(completed_percent) as work_progress FROM pms_time_entry WHERE tskid ='" . $task['tskid'] . "' AND approve_status = 1 AND entry_date = '" . $entry_check_date . "'")->queryRow();
            }

            // echo '<pre>';
            // print_r($progress_data);
            // $time_enries = Yii::app()->db->createCommand(" SELECT * FROM pms_time_entry WHERE tskid = ".$task['tskid']."
            // AND entry_date = '".$entry_check_date."' AND approve_status = 1 ORDER BY updated_date DESC LIMIT 1")->queryRow();  
            if (!empty($progress_data['work_progress'])) {
                $data = $this->addprogressnotification($task, $todays_expected_percentage, $progress_data, $entry_check_date);
            } else {
                $data = $this->addprogressnotification($task, $todays_expected_percentage, 0, $entry_check_date);
            }

            $result_data[$entry_check_date] = $data;
        }

        return $result_data;
    }

    protected function addprogressnotification($task, $todays_expected_percentage, $progress_data, $entry_check_date)
    {

        // $admin_users = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `user_type` = 1")->queryColumn();
        if ($progress_data['work_progress'] != 0 && !empty($progress_data['work_progress'])) {
            if ($progress_data['work_progress'] < $todays_expected_percentage) {
                $data = [
                    'id' => $task['tskid'],
                    'title' => $task['title'],
                    'current_percentage' => number_format($progress_data['work_progress'], 2),
                    'required_percentage' => number_format($todays_expected_percentage, 2),
                    'due_date' => $task['due_date'],
                    'assignee' => $task['assigned_to'],
                    'start_date' => $task['start_date'],
                    'percent_date' => $entry_check_date
                ];
                return $data;
            } else {
                $data = [
                    'id' => $task['tskid'],
                    'title' => $task['title'],
                    'current_percentage' => number_format($progress_data['work_progress'], 2),
                    'required_percentage' => number_format($todays_expected_percentage, 2),
                    'due_date' => $task['due_date'],
                    'assignee' => $task['assigned_to'],
                    'start_date' => $task['start_date'],
                    'percent_date' => $entry_check_date
                ];
                return $data;
            }
        } else {
            $data = [
                'id' => $task['tskid'],
                'title' => $task['title'],
                'current_percentage' => 0,
                'required_percentage' => number_format($todays_expected_percentage, 2),
                'due_date' => $task['due_date'],
                'assignee' => $task['assigned_to'],
                'start_date' => $task['start_date'],
                'percent_date' => $entry_check_date
            ];
            return $data;
        }
    }

    protected function addprogressnotificationcron($days_gone, $task, $todays_expected_percentage, $progress_percentage)
    {

        if (!empty($task['project_id'])) {
            $project_model = Projects::model()->findByPk($task['project_id']);
        }
        //         echo $todays_expected_percentage.' expected <br>';
        //   echo $progress_percentage.' progress';
        // $admin_users = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `user_type` = 1")->queryColumn();

        if ($task['task_type'] == 1) {
            $tblpx = Yii::app()->db->tablePrefix;
            $sql_response3 = Yii::app()->db->createCommand("select SUM(qty) as total_quantity FROM {$tblpx}daily_work_progress WHERE taskid='" . $task['tskid'] . "' AND approve_status != 2")->queryRow();
            $quantity = $task['quantity'] - $sql_response3['total_quantity'];
            $no_of_days = ($task['task_duration'] - ($days_gone));
            $perday_quantity = ($quantity / $no_of_days);
            $work_type = WorkType::model()->findByPk($task['work_type_id']);
            $daily_throughput = $work_type->daily_throughput;
            $workers_perday = $perday_quantity / $daily_throughput;
            // echo $workers_perday.'<br>';
            if ($workers_perday < 1) {
                $workers_no = 1;
            } else {
                //$workers_no = round($workers_perday);
                $workers_no = ceil($workers_perday);
            }
        } else {
            $workers_no = '';
            $quantity = '';
        }

        if ($progress_percentage != 0) {

            if ($progress_percentage < $todays_expected_percentage) {


                $data = ['id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => number_format($progress_percentage, 2), 'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'], 'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'workers_required' => $workers_no, 'remaining_qty' => $quantity, 'project_title' => $project_model->name];
                return $data;
            }
        } else {
            $data = ['id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => 0, 'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'], 'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'workers_required' => $workers_no, 'remaining_qty' => $quantity, 'project_title' => $project_model->name];
            return $data;
        }
    }

    public function sendAdminMail($data, $type)
    {
        $model2 = new MailLog;
        $today = date('Y-m-d H:i:s');
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);

        $user_model = Users::model()->findaLl(['condition' => 'user_type <=2']);
        if ($type == '1') {
            $mail_data = $this->renderPartial('_task_progress_mail', array('data' => $data), true);
        } elseif ($type == '2') {
            $mail_data = $this->renderPartial('_project_progress_mail', array('data' => $data), true);
        } else {

            $mail_data = '';
        }
        $mail = new JPhpMailer();
        $headers = "Hostech";
        $bodyContent = $mail_data;
        $mail->IsSMTP();
        $mail->Host = $mail_model['smtp_host'];
        $mail->Port = $mail_model['smtp_port'];
        $mail->SMTPSecure = $mail_model['smtp_secure'];
        $mail->SMTPAuth = $mail_model['smtp_auth'];
        $mail->Username = $mail_model['smtp_username'];
        $mail->Password = $mail_model['smtp_password'];
        $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);


        if ($type == '1') {
            $mail->Subject = "Task Progress Notification";
        } elseif ($type == '2') {
            $mail->Subject = "Project Progress Notification";
        } else {
            $mail->Subject = '';
        }

        $receippients = explode(',', $mail_model['behind_task_mail_recipients']);

        foreach ($receippients as $receippient) {

            $mail->addAddress($receippient); // Add a recipient
        }


        $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
        $dataJSON = json_encode($data);
        $receippientsJSON = json_encode($receippients);
        $mail_send_byJSON = json_encode($mail_send_by);
        $subject = $mail->Subject;


        // $mail->addAddress('tony.xa@bluehorizoninfotech.com ');

        $model2->send_to = $receippientsJSON;
        $model2->send_by = $mail_send_byJSON;
        $model2->send_date = $today;
        $model2->message = htmlentities($bodyContent);
        $model2->description = $mail->Subject;
        $model2->created_date = $today;
        $model2->created_by = Yii::app()->user->getId();
        $model2->mail_type = $subject;
        $mail->isHTML(true);
        $mail->MsgHTML($bodyContent);
        $mail->Body = $bodyContent;
        if ($mail->Send()) {
            $model2->sendemail_status = 1;
        } else {
            $model2->sendemail_status = 0;
        }
        $model2->save();
    }

    public function getAssigneedeatils($id)
    {
        $user_model = Users::model()->findByPk($id);
        return $user_model['first_name'] . ' ' . $user_model['last_name'];
    }

    public function actionCalandersheet_backup()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));
        $this->layout = 'false';
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-t');
        $company = '';
        $milestone_value = '';
        $where = " where 1=1";
        if (isset(Yii::app()->user->company_id)) {
            $company = ' and ed.company_id = ' . Yii::app()->user->company_id;
        }
        $project_tasks = Tasks::model()->findAll(['condition' => 'trash =1 AND task_type = 1 AND project_id = ' . $_REQUEST['pid']]);
        $tasks = array();
        $i = 1;
        foreach ($project_tasks as $task) {
            if ($task['parent_tskid'] == NULL) {
                if (!empty($task['milestone_id'])) {
                    $milestone_value = $task->milestone->milestone_title;
                }
            }

            if ($i == 1) {
                $start_date = date('Y-m-01', strtotime($task['start_date']));
                $end_date = date('Y-m-t', strtotime($task['due_date']));
            }
            if (strtotime($end_date) < strtotime($task['due_date'])) {
                $end_date = date('Y-m-t', strtotime($task['due_date']));
            }
            if (strtotime($task['start_date']) < strtotime($start_date)) {
                $start_date = date('Y-m-01', strtotime($task['start_date']));
            }

            $time_entry_array = array();
            $task_work_status = $this->taskprogressnotification($task, $time_entry_array);
            $data = $this->buildTree($task_work_status);

            $time_enries = Yii::app()->db->createCommand(" SELECT * FROM pms_time_entry WHERE tskid = " . $task['tskid'] . "
            ORDER BY updated_date DESC LIMIT 1")->queryRow();
            if (!empty($time_enries) && $time_enries['approve_status'] == 1) {
                $complete_percent = $time_enries['completed_percent'];
            }

            $arr = array(
                "id" => $task['tskid'],
                "parent_id" => $task['parent_tskid'],
                "title" => $task['title'],
                'start_date' => $task['start_date'],
                'due_date' => $task['due_date'],
                /* 'status'=>$status, 'current_percent' => $complete_percent, */ 'percent_date' => $time_enries['entry_date'],
                /* 'required_percentage'=>$task_work_status['required_percentage'], */ 'task_status' => $task['status'],
                'mile_stone_id' => $task['milestone_id']
            );
            array_push($arr, $task_work_status);
            array_push($tasks, $arr);
            $i++;
        }

        if (count($this->buildTree($tasks)) == 0) {
            $data = $tasks;
        } else {
            $data = $this->buildTree($tasks);
        }

        $this->render('calendertable', array('start_date' => $start_date, 'end_date' => $end_date, 'tasks' => $data, 'milestone_value' => $milestone_value));
    }

    public function actionCalandersheet()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));
        $this->layout = 'false';
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-t');
        $company = '';
        $milestone_value = '';
        $where = " where 1=1";
        if (isset(Yii::app()->user->company_id)) {
            $company = ' and ed.company_id = ' . Yii::app()->user->company_id;
        }
        $project_tasks = Tasks::model()->findAll(['condition' => 'trash =1 AND gantt_chart_status = 1 AND project_id = ' . $_REQUEST['pid']]);
        $tasks = array();
        $data = array();
        $i = 1;

        foreach ($project_tasks as $task) {

            $filter_type = $_REQUEST['type'];


            if ($filter_type != "" || $filter_type != null) {
                $task = $this->getFilteredTasks($task, $filter_type);
            }
            if (isset($task)) {

                if ($task['parent_tskid'] == NULL) {
                    if (!empty($task['milestone_id'])) {
                        $milestone_value = $task->milestone->milestone_title;
                    }
                }



                if ($i == 1) {
                    $start_date = date('Y-m-01', strtotime($task['start_date']));
                    $end_date = date('Y-m-t', strtotime($task['due_date']));
                }
                if (strtotime($end_date) < strtotime($task['due_date'])) {
                    $end_date = date('Y-m-t', strtotime($task['due_date']));
                }
                if (strtotime($task['start_date']) < strtotime($start_date)) {
                    $start_date = date('Y-m-01', strtotime($task['start_date']));
                }

                $time_entry_array = array();
                $task_work_status = $this->taskprogressnotification($task, $time_entry_array);



                $time_enries = Yii::app()->db->createCommand('SELECT approve_status,completed_percent, entry_date FROM pms_time_entry WHERE tskid = ' . $task['tskid'] . '
            ORDER BY updated_date DESC LIMIT 1')->queryRow();
                if (!empty($time_enries) && $time_enries['approve_status'] == 1) {
                    $complete_percent = $time_enries['completed_percent'];
                }

                $arr = array(
                    "id" => $task['tskid'],
                    "parent_id" => $task['parent_tskid'],
                    "title" => $task['title'],
                    'start_date' => $task['start_date'],
                    'due_date' => $task['due_date'],
                    'percent_date' => $time_enries['entry_date'],
                    'task_status' => $task['status'],
                    'mile_stone_id' => $task['milestone_id']
                );
                array_push($arr, $task_work_status);
                array_push($tasks, $arr);

                $i++;
            }
            if (count($this->buildTree($tasks)) == 0) {
                $data = $tasks;
            } else {
                $data = $this->buildTree($tasks);
            }
        }

        $this->render('calendertable', array('start_date' => $start_date, 'end_date' => $end_date, 'tasks' => $data, 'milestone_value' => $milestone_value));
    }

    protected function buildTree(array $elements, $parentId = 0)
    {


        $branch = array();
        foreach ($elements as $element) {
            if (isset($element['parent_id'])) {
                if ($element['id'] == $element['parent_id'])
                    $element['parent_id'] = NULL;
                if ($element['parent_id'] == $parentId) {
                    $children = $this->buildTree($elements, $element['id']);
                    if ($children) {

                        $element['children'] = $children;
                    }
                    $branch[] = $element;
                }
            }
        }
        return $branch;
    }

    public function add_date($orgDate, $date)
    {
        $cd = strtotime($orgDate);
        $retDAY = date('d/m/Y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $date, date('Y', $cd)));
        return $retDAY;
    }

    public function getcountBetween2Dates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return count($days);
    }

    public function getDatesBetween2Dates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    protected function printArrayList($tasks, $totdays, $week_dates, $child)
    {
        $cent_percent_status = 0;
        $task_percen = '';
        $task_status = '';
        foreach ($tasks as $task) {

            $time_entry_array = $task[0];
            if ($task['task_status'] == 6) {
                $task_status = 'grey';
            } elseif ($task['task_status'] == 9) {
                $task_status = 'green';
            } elseif ($task['task_status'] == 5) {
                $task_status = 'orange';
            } elseif ($task['task_status'] == 8) {
                $task_status = 'violet';
            } elseif ($task['task_status'] == 7) {
                $task_status = 'blue';
            } elseif ($task['task_status'] == 72) {
                $task_status = 'dark_blue';
            }
            $left_index = $child * 10 + 15;
            echo ' <tr id="">
        <td  width="20"></td>                           
        <td colspan="3"  class=""  style="width:auto; padding-left:' . $left_index . 'px">' . $task["title"] . '#       
        </td>                            
        </th>';
            $task_start = strtotime($task['start_date']);
            $task_end = strtotime($task['due_date']);
            for ($p = 0; $p < $totdays; $p++) {
                $var = $week_dates[$p];
                $todd = date('d/m/Y');
                $datecur = str_replace('/', '-', $var);
                $datecur_ = str_replace('/', '-', $todd);
                $current_date = strtotime($datecur);
                $date_check_in_time_entry = date("Y-m-d", strtotime($datecur));
                $todays_date = strtotime($datecur_);
                if (array_search($date_check_in_time_entry, array_column($time_entry_array, 'percent_date')) !== False) {
                    $percentage_array_of_date = $time_entry_array[$date_check_in_time_entry];
                    if (($percentage_array_of_date['required_percentage'] == 100) && ($percentage_array_of_date['current_percentage'] != $percentage_array_of_date['required_percentage'])) {
                        $task_percen = 'red_border';
                    } else {

                        if ($percentage_array_of_date['required_percentage'] != 0) {

                            $half_required_perce = $percentage_array_of_date['required_percentage'] / 2;

                            if ($percentage_array_of_date['current_percentage'] == 0) {
                                $task_percen = 'red_border';
                            } elseif ($percentage_array_of_date['current_percentage'] < $half_required_perce) {
                                $task_percen = 'red_border';
                            } elseif ($percentage_array_of_date['current_percentage'] >= $percentage_array_of_date['required_percentage']) {
                                $task_percen = 'green_border';
                            } elseif ($percentage_array_of_date['current_percentage'] == 100) {
                                $task_percen = 'green_border';
                                $cent_percent_status = 1;
                            } else {
                                $task_percen = 'yellow_border';
                            }
                        } else {
                            if ($percentage_array_of_date['current_percentage'] == 100) {
                                $task_percen = 'green_border';
                                $cent_percent_status = 1;
                            } elseif ($percentage_array_of_date['current_percentage'] != 0) {
                                $task_percen = 'green_border';
                            } else {
                                $task_percen = 'red_border';
                            }
                        }
                    }
                    if ($percentage_array_of_date['percent_date'] != '') {

                        $time_entry_date = str_replace('/', '-', $percentage_array_of_date['percent_date']);

                        $time_entry_date_ = strtotime($time_entry_date);
                        $current_perc = $percentage_array_of_date['current_percentage'] . '%';
                        if ($child == 1) {
                        }
                    } else {
                        $time_entry_date_ = 0;
                    }
                } else {
                    $current_perc = '';
                    $time_entry_date_ = 0;
                }
                if ($cent_percent_status == 1) {
                    $task_percen = 'green_border';
                }

                echo '<td width="20" data-toggle="modal"  id="c_"' . $current_date . '" data-backdrop="static" data-target="#entry"';
                echo 'class="coldate ';
                if (($current_date >= $task_start) && ($current_date <= $task_end)) {
                    echo $task_status;
                }
                if ((($current_date >= $task_start) && ($current_date <= $task_end) && ($current_date <= $todays_date) && ($current_date == $time_entry_date_))) {
                    echo ' ' . $task_percen;
                }
                if ($current_date == $todays_date) {
                    echo ' today_highlight';
                }
                echo '" data-value=""  data-coldate="">';
                if (( /* $task['percent_date'] != '' && */ ($current_date == $time_entry_date_ || $time_entry_date_ > $task_end))) {
                    echo $current_perc;
                }

                echo '</td>';
            }
            echo '</tr>';
            if (!empty($task['children'])) {
                $child++;
                $this->printArrayList($task['children'], $totdays, $week_dates, $child);
                continue;
            }
        }
    }

    protected function printArrayListpdf($tasks, $totdays, $week_dates, $child)
    {
        $cent_percent_status = 0;
        $task_percen = '';

        foreach ($tasks as $task) {
            $time_entry_array = $task[0];
            if ($task['task_status'] == 6) {
                $task_status = ' background-color: #807b7b;';
            } elseif ($task['task_status'] == 9) {
                $task_status = 'background-color: #3BBF67;';
            } elseif ($task['task_status'] == 5) {
                $task_status = 'background-color: #f9c154;';
            } elseif ($task['task_status'] == 8) {
                $task_status = 'background-color: #7a3e9ab8;';
            } elseif ($task['task_status'] == 7) {
                $task_status = 'background-color: #6EBEF4;';
            } elseif ($task['task_status'] == 72) {
                $task_status = ' background-color: #1d22bf;';
                ;
            }
            $left_index = $child * 10 + 15;
            echo '<tr id="">
        <td  width="20"></td>                           
        <td colspan="3" width="300" class="" align="center" style="padding-left: 30px">
       
       ' . $task["title"] . '#
       
        </td>';
            $task_start = strtotime($task['start_date']);
            $task_end = strtotime($task['due_date']);
            for ($p = 0; $p < $totdays; $p++) {
                $var = $week_dates[$p];
                $todd = date('d/m/Y');
                $datecur = str_replace('/', '-', $var);
                $datecur_ = str_replace('/', '-', $todd);
                $current_date = strtotime($datecur);
                $date_check_in_time_entry = date("Y-m-d", strtotime($datecur));
                $todays_date = strtotime($datecur_);
                if (array_search($date_check_in_time_entry, array_column($time_entry_array, 'percent_date')) !== False) {
                    $percentage_array_of_date = $time_entry_array[$date_check_in_time_entry];
                    if (($percentage_array_of_date['required_percentage'] == 100) && ($percentage_array_of_date['current_percentage'] != $percentage_array_of_date['required_percentage'])) {
                        $task_percen = ' border: 2.5px solid red;';
                    } else {

                        if ($percentage_array_of_date['required_percentage'] != 0) {

                            $half_required_perce = $percentage_array_of_date['required_percentage'] / 2;

                            if ($percentage_array_of_date['current_percentage'] == 0) {
                                $task_percen = ' border: 2.5px solid red;';
                            } elseif ($percentage_array_of_date['current_percentage'] < $half_required_perce) {
                                $task_percen = ' border: 2.5px solid red;';
                            } elseif ($percentage_array_of_date['current_percentage'] == $percentage_array_of_date['required_percentage']) {
                                $task_percen = 'border: 3px solid #058a05;';
                            } elseif ($percentage_array_of_date['current_percentage'] == 100) {
                                $task_percen = 'border: 3px solid #058a05 ;';
                                $cent_percent_status = 1;
                            } else {
                                $task_percen = 'border: 2.5px solid yellow;';
                            }
                        } else {
                            if ($percentage_array_of_date['current_percentage'] == 100) {
                                $task_percen = 'border: 3px solid #058a05;';
                                $cent_percent_status = 1;
                            } elseif ($percentage_array_of_date['current_percentage'] != 0) {
                                $task_percen = 'border: 3px solid #058a05;';
                            } else {
                                $task_percen = ' border: 2.5px solid red;';
                            }
                        }
                    }
                    if ($percentage_array_of_date['percent_date'] != '') {

                        $time_entry_date = str_replace('/', '-', $percentage_array_of_date['percent_date']);

                        $time_entry_date_ = strtotime($time_entry_date);
                        $current_perc = $percentage_array_of_date['current_percentage'] . '%';
                        if ($child == 1) {
                        }
                    } else {
                        $time_entry_date_ = 0;
                    }
                } else {
                    $current_perc = '';
                    $time_entry_date_ = 0;
                }
                if ($cent_percent_status == 1) {
                    $task_percen = 'border: 3px solid #058a05;';
                }


                echo '<td width="20" data-toggle="modal"  data-backdrop="static" data-target="#entry"';

                echo 'class="coldate "';
                echo 'style="';
                if (($current_date >= $task_start) && ($current_date <= $task_end)) {
                    echo $task_status;
                }
                if ((($current_date >= $task_start) && ($current_date <= $task_end) && ($current_date <= $todays_date) && ($current_date == $time_entry_date_))) {
                    echo $task_percen;
                }
                if ($current_date == $todays_date) {
                    echo ' today_highlight';
                }
                echo '"';

                echo ' data-value=""  data-coldate="">';
                if (( /* $task['percent_date'] != '' && */ ($current_date == $time_entry_date_ || $time_entry_date_ > $task_end))) {
                    echo $current_perc;
                }

                echo '</td>';
            }
            echo '</tr>';
            if (!empty($task['children'])) {
                $child++;
                $this->printArrayListpdf($task['children'], $totdays, $week_dates, $child);
                continue;
            }
        }
    }

    public function actionAnalyzerlink($id)
    {
        $settings = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $parameters = array('path' => '/' . $id);
        $headers = array(
            'Authorization: Bearer ' . $settings['dropbox_token'],
            'Content-Type: application/json'
        );

        $curlOptions = array(
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($parameters),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_VERBOSE => true
        );
        $ch = curl_init('https://api.dropboxapi.com/2/sharing/get_shared_links');
        curl_setopt_array($ch, $curlOptions);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        if (!empty($response['links'])) {
            $url = $response['links']['0']['url'];
        } else {

            $ch = curl_init('https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings');
            curl_setopt_array($ch, $curlOptions);
            $response = curl_exec($ch);
            $response = json_decode($response, true);

            if (!empty($response['url'])) {
                $url = $response['url'];
            } else {
                $url = 'index.php?r=site/error';
            }
        }
        curl_close($ch);
        $this->redirect($url);
    }

    public function actiondownloadpdf()
    {
        ini_set('memory_limit', '-1');
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));
        $this->layout = 'false';
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-t');
        $company = '';
        $where = " where 1=1";
        if (isset(Yii::app()->user->company_id)) {
            $company = ' and ed.company_id = ' . Yii::app()->user->company_id;
        }
        if (!empty($_REQUEST['id'])) {
            $model = Projects::model()->findByPk($_REQUEST['id']);
            $project_tasks = Yii::app()->db->createCommand("SELECT * FROM `pms_tasks` 
                WHERE  `pms_tasks`.trash =1 AND gantt_chart_status = 1 AND `project_id` =" . $_REQUEST['id'] . " ORDER BY updated_date ASC")->queryAll();
        }
        $filter_type = $_GET['type'];

        $tasks = array();
        $data = array();

        $i = 1;
        if (!empty($project_tasks)) {
            foreach ($project_tasks as $task) {
                if ($filter_type != "" || $filter_type != null) {
                    $task = $this->getFilteredTasks($task, $filter_type);
                }
                if (isset($task)) {
                    if ($i == 1) {
                        $start_date = date('Y-m-d', strtotime($task['start_date']));
                        $end_date = date('Y-m-d', strtotime($task['due_date']));
                    }
                    if (strtotime($end_date) < strtotime($task['due_date'])) {
                        $end_date = date('Y-m-d', strtotime($task['due_date']));
                    }
                    if (strtotime($task['start_date']) < strtotime($start_date)) {
                        $start_date = date('Y-m-d', strtotime($task['start_date']));
                    }

                    $time_entry_array = array();
                    $task_work_status = $this->taskprogressnotification($task, $time_entry_array);
                    $time_enries = Yii::app()->db->createCommand(" SELECT * FROM pms_time_entry WHERE tskid = " . $task['tskid'] . "
            ORDER BY updated_date DESC LIMIT 1")->queryRow();

                    $arr = array(
                        "id" => $task['tskid'],
                        "parent_id" => $task['parent_tskid'],
                        "title" => $task['title'],
                        'start_date' => $task['start_date'],
                        'due_date' => $task['due_date'],
                        /* 'status'=>$status, */ 'current_percent' => $time_enries['completed_percent'],
                        'percent_date' => $time_enries['entry_date'],
                        /* 'required_percentage'=>$task_work_status['required_percentage'], */ 'task_status' => $task['status']
                    );
                    array_push($arr, $task_work_status);

                    array_push($tasks, $arr);
                    $i++;
                }
            }
        }


        if (count($this->buildTree($tasks)) == 0) {
            $data = $tasks;
        } else {
            $data = $this->buildTree($tasks);
        }
        // $this->render('calendertable', array('start_date' => $start_date, 'end_date' => $end_date,'tasks'=>$data));
        //    echo Yii::getPathOfAlias('webroot.css') . '/custom.css';exit;

        $tasklist = $this->renderPartial('pdf_file', array(
            'tasks' => $data,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'model' => $model
        ), true);
        //    $this->renderPartial('pdf_file', array(
        //         'tasks' => $data,'start_date'=> $start_date,'end_date' => $end_date
        //             ));
        //    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdf.css');
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-L');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($tasklist);


        $mPDF1->Output($model['name'] . '.pdf', 'D');
    }

    public function actiondownloadexcel()
    {
        ini_set('memory_limit', '-1');
        $project_id = $_GET['id'];
        $filter_type = $_GET['type'];
        $project_model = Projects::model()->findByPk($project_id);
        if (empty($project_model)) {
            throw new Exception("Something unexpected happened.");
        }

        $header_style = array(
            'font' => array(
                'color' => array('rgb' => 'ffffff'),
                'size' => 12
            )
        );
        $dates_font = array(
            'font' => array(
                'color' => array('rgb' => 'ffffff'),
                'size' => 9
            )
        );
        $percentage_font = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 9
            )
        );
        $task_name_font = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 9
            )
        );

        $array_data = $this->projectdata($project_id, $filter_type); /* get task array , start and end date */
        //echo count($array_data);exit;
        $start_date = $array_data['start_date'];
        $end_date = $array_data['end_date'];
        $tasks = $array_data['data'];
        //echo "<pre>",print_r($tasks);exit;
        /* month display */
        $date1 = new DateTime($start_date);
        $date2 = new DateTime($end_date);
        $diff = $date2->diff($date1)->format("%a");
        $totdays = $diff + 1;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '');

        $week_month_result = $this->WeekAndMonthDaysArray($start_date, $totdays);
        $week_dates = $week_month_result['week_dates'];
        $monthdayarr = $week_month_result['monthdayarr'];
        $endmonth = explode('-', $end_date);
        $startdatelast = $endmonth[0] . '-' . $endmonth[1] . '-01';
        $monthdayarr[$endmonth[1]] = $this->getcountBetween2Dates($startdatelast, $end_date);
        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
        $char = 'B';

        $char = $this->setCalenderMonthExcel($monthdayarr, $totdays, $char, $objPHPExcel, $header_style, $monthsarr); /* seting calender month in excel */

        /* Days display */
        $objPHPExcel->getActiveSheet()->setCellValue('A2', '');
        $days = $this->getDatesBetween2Dates($start_date, $end_date);
        $global_assign = '';
        $char = 'B';
        $char = $this->setCalenderDaysExcel($days, $char, $objPHPExcel, $dates_font);

        /* display task name */
        $objPHPExcel->getActiveSheet()->mergeCells('A1:A3');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Task Name');

        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '44709C')
                )
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($header_style);
        $i = 4;
        foreach ($tasks as $task) {
            Yii::app()->session['index_value'] = 0;
            /* setting status color */

            $status_return_array = $this->taskStatusAndBorderCss($task, $percentage_font); /* for task status and border color */
            $task_status = $status_return_array['task_status'];
            $red_border = $status_return_array['red_border'];
            $green_border = $status_return_array['green_border'];
            $yellow_border = $status_return_array['yellow_border'];
            $percentage_font = $status_return_array['percentage_font'];

            $time_entry_array = $task[0];
            //echo "<pre>",print_r( $time_entry_array);
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $task['title']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray($task_name_font);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            /* checking percentage and display percentage */

            $task_start = strtotime($task['start_date']);
            $task_end = strtotime($task['due_date']);
            $task_percen = '';
            $cent_percent_status = 0;
            $date_char = 'A';

            for ($p = 0; $p < $totdays; $p++) {
                $column_new = PHPExcel_Cell::stringFromColumnIndex(PHPExcel_Cell::columnIndexFromString($date_char));
                $var = $week_dates[$p];
                $todd = date('d/m/Y');
                $datecur = str_replace('/', '-', $var);
                $datecur_ = str_replace('/', '-', $todd);
                $current_date = strtotime($datecur);
                $date_check_in_time_entry = date("Y-m-d", strtotime($datecur));
                $todays_date = strtotime($datecur_);
                // if (array_search($date_check_in_time_entry, array_column($time_entry_array, 'percent_date')) !== False) {
                //     $percentage_array_of_date = $time_entry_array[$date_check_in_time_entry];
                //     if (($percentage_array_of_date['required_percentage'] == 100) && ($percentage_array_of_date['current_percentage'] != $percentage_array_of_date['required_percentage'])) {
                //         $task_percen = $red_border;
                //     } else {
                //         if ($percentage_array_of_date['required_percentage'] != 0) {
                //             $half_required_perce = $percentage_array_of_date['required_percentage'] / 2;
                //             if ($percentage_array_of_date['current_percentage'] == 0) {
                //                 $task_percen = $red_border;
                //             } elseif ($percentage_array_of_date['current_percentage'] < $half_required_perce) {
                //                 $task_percen = $red_border;
                //             } elseif ($percentage_array_of_date['current_percentage'] == $percentage_array_of_date['required_percentage']) {
                //                 $task_percen = $green_border;
                //             } elseif ($percentage_array_of_date['current_percentage'] == 100) {
                //                 $task_percen = $green_border;
                //                 $cent_percent_status = 1;
                //             } else {
                //                 $task_percen = $yellow_border;
                //             }
                //         } else {
                //             if ($percentage_array_of_date['current_percentage'] == 100) {
                //                 $task_percen = $green_border;
                //                 $cent_percent_status = 1;
                //             } elseif ($percentage_array_of_date['current_percentage'] != 0) {
                //                 $task_percen = $green_border;
                //             } else {
                //                 $task_percen = $red_border;
                //             }
                //         }
                //     }
                //     if ($percentage_array_of_date['percent_date'] != '') {
                //         $time_entry_date = str_replace('/', '-', $percentage_array_of_date['percent_date']);
                //         $time_entry_date_ = strtotime($time_entry_date);
                //         $current_perc = $percentage_array_of_date['current_percentage'] . '%';
                //     } else {
                //         $time_entry_date_ = 0;
                //     }
                // }




                if (array_search($date_check_in_time_entry, array_column($time_entry_array, 'percent_date')) !== False) {
                    $percentage_array_of_date = $time_entry_array[$date_check_in_time_entry];

                    if (($percentage_array_of_date['required_percentage'] == 100) && ($percentage_array_of_date['current_percentage'] != $percentage_array_of_date['required_percentage'])) {
                        $task_percen = $red_border;
                    } else {
                        if ($percentage_array_of_date['required_percentage'] != 0) {

                            $half_required_perce = $percentage_array_of_date['required_percentage'] / 2;
                            if ($percentage_array_of_date['current_percentage'] == 0) {
                                $task_percen = $red_border;
                            } elseif ($percentage_array_of_date['current_percentage'] < $half_required_perce) {
                                $task_percen = $red_border;
                            } elseif (($percentage_array_of_date['current_percentage'] >= $percentage_array_of_date['required_percentage'])) {
                                $task_percen = $green_border;
                            } elseif ($percentage_array_of_date['current_percentage'] == 100) {
                                $task_percen = $green_border;
                                $cent_percent_status = 1;
                            } else {
                                $task_percen = $yellow_border;
                            }
                        } else {
                            if ($percentage_array_of_date['current_percentage'] == 100) {
                                $task_percen = $green_border;
                                $cent_percent_status = 1;
                            } elseif ($percentage_array_of_date['current_percentage'] != 0) {
                                $task_percen = $green_border;
                            } else {
                                $task_percen = $red_border;
                            }
                        }
                    }
                    if ($percentage_array_of_date['percent_date'] != '') {
                        $time_entry_date = str_replace('/', '-', $percentage_array_of_date['percent_date']);
                        $time_entry_date_ = strtotime($time_entry_date);
                        $current_perc = $percentage_array_of_date['current_percentage'] . '%';
                    } else {
                        $time_entry_date_ = 0;
                    }
                } else {
                    $current_perc = '';
                    $time_entry_date_ = 0;
                }
                if ($cent_percent_status == 1) {
                    $task_percen = $green_border;
                }
                if ($current_date == $time_entry_date_) {
                    $objPHPExcel->getActiveSheet()->setCellValue($column_new . $i, $current_perc);
                    $objPHPExcel->getActiveSheet()->getStyle($column_new . $i)->applyFromArray($task_status);
                    $objPHPExcel->getActiveSheet()->getStyle($column_new . $i)->applyFromArray($percentage_font);
                }
                if (($current_date >= $task_start) && ($current_date <= $task_end) && ($current_date <= $todays_date) && ($current_date == $time_entry_date_)) {
                    $objPHPExcel->getActiveSheet()->getStyle($column_new . $i)->applyFromArray($task_percen);
                }
                $date_char++;
            }

            if (!empty($task['children'])) {

                foreach ($task['children'] as $task) {

                    Yii::app()->session['index_value'] = 0;
                    $i++;
                    $child = 0;
                    $this->printArrayListexcel($task, $totdays, $week_dates, $child, $objPHPExcel, $i);
                    if (Yii::app()->session['index_value'] != 0 && Yii::app()->session['index_value'] > $i) {
                        $i = Yii::app()->session['index_value'];
                    }
                }
            }

            $i++;
        }


        $objPHPExcel->getActiveSheet()->freezePane('B' . $i);
        $objPHPExcel->getActiveSheet()->setTitle($project_model['name']);

        $objPHPExcel->setActiveSheetIndex(0);

        ob_end_clean();
        ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $project_model['name'] . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    protected function projectdata($project_id, $filter_type)
    {
        //jesna;
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-t');
        $company = '';
        $where = " where 1=1";

        $project_tasks = Yii::app()->db->createCommand("SELECT * FROM `pms_tasks` 
    WHERE  `pms_tasks`.trash =1 AND gantt_chart_status = 1 AND `project_id` = " . $project_id . " ORDER BY updated_date ASC")->queryAll();
        $tasks = array();
        $data = array();



        $i = 1;
        foreach ($project_tasks as $task) {

            if ($filter_type != "" || $filter_type != null) {
                $task = $this->getFilteredTasks($task, $filter_type);
            }
            if (isset($task)) {

                if ($i == 1) {
                    $start_date = date('Y-m-d', strtotime($task['start_date']));
                    $end_date = date('Y-m-d', strtotime($task['due_date']));
                }
                if (strtotime($end_date) < strtotime($task['due_date'])) {
                    $end_date = date('Y-m-d', strtotime($task['due_date']));
                }
                if (strtotime($task['start_date']) < strtotime($start_date)) {
                    $start_date = date('Y-m-d', strtotime($task['start_date']));
                }

                $time_entry_array = array();
                $task_work_status = $this->taskprogressnotification($task, $time_entry_array);
                $time_enries = Yii::app()->db->createCommand(" SELECT * FROM pms_time_entry WHERE tskid = " . $task['tskid'] . "
        ORDER BY updated_date DESC LIMIT 1")->queryRow();

                $arr = array(
                    "id" => $task['tskid'],
                    "parent_id" => $task['parent_tskid'],
                    "title" => $task['title'],
                    'start_date' => $task['start_date'],
                    'due_date' => $task['due_date'],
                    /* 'status'=>$status, */ 'current_percent' => $time_enries['completed_percent'],
                    'percent_date' => $time_enries['entry_date'],
                    /* 'required_percentage'=>$task_work_status['required_percentage'], */ 'task_status' => $task['status']
                );
                array_push($arr, $task_work_status);

                array_push($tasks, $arr);
                $i++;
            }
        }


        if (count($this->buildTree($tasks)) == 0) {
            $data = $tasks;
        } else {
            $data = $this->buildTree($tasks);
        }
        // $data = $this->buildTree($tasks);

        $return_data = array('start_date' => $start_date, 'end_date' => $end_date, 'data' => $data);
        return $return_data;
    }

    protected function printArrayListexcel($task, $totdays, $week_dates, $child, $objPHPExcel, $i)
    {


        $header_style = array(
            'font' => array(
                'color' => array('rgb' => 'ffffff'),
                'size' => 12
            )
        );
        $dates_font = array(
            'font' => array(
                'color' => array('rgb' => 'ffffff'),
                'size' => 9
            )
        );
        $percentage_font = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 9
            )
        );
        $task_name_font = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 9
            )
        );

        $cent_percent_status = 0;
        $task_percen = '';

        // foreach($tasks as $task){

        $time_entry_array = $task[0];

        $status_return_array = $this->taskStatusAndBorderCss($task, $percentage_font); /* for task status and border color */
        $task_status = $status_return_array['task_status'];
        $red_border = $status_return_array['red_border'];
        $green_border = $status_return_array['green_border'];
        $yellow_border = $status_return_array['yellow_border'];
        $percentage_font = $status_return_array['percentage_font'];

        $left_index = $child * 1 + 1;

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $task['title']);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray($task_name_font);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->getAlignment()->setIndent($left_index);

        $task_start = strtotime($task['start_date']);
        $task_end = strtotime($task['due_date']);
        $date_char = 'A';
        for ($p = 0; $p < $totdays; $p++) {
            $column_new = PHPExcel_Cell::stringFromColumnIndex(PHPExcel_Cell::columnIndexFromString($date_char));
            $var = $week_dates[$p];
            $todd = date('d/m/Y');
            $datecur = str_replace('/', '-', $var);
            $datecur_ = str_replace('/', '-', $todd);
            $current_date = strtotime($datecur);
            $date_check_in_time_entry = date("Y-m-d", strtotime($datecur));
            $todays_date = strtotime($datecur_);
            if (array_search($date_check_in_time_entry, array_column($time_entry_array, 'percent_date')) !== False) {
                $percentage_array_of_date = $time_entry_array[$date_check_in_time_entry];
                if (($percentage_array_of_date['required_percentage'] == 100) && ($percentage_array_of_date['current_percentage'] != $percentage_array_of_date['required_percentage'])) {
                    $task_percen = $red_border;
                } else {

                    if ($percentage_array_of_date['required_percentage'] != 0) {

                        $half_required_perce = $percentage_array_of_date['required_percentage'] / 2;

                        if ($percentage_array_of_date['current_percentage'] == 0) {
                            $task_percen = $red_border;
                        } elseif ($percentage_array_of_date['current_percentage'] < $half_required_perce) {
                            $task_percen = $red_border;
                        } elseif ($percentage_array_of_date['current_percentage'] == $percentage_array_of_date['required_percentage']) {
                            $task_percen = $green_border;
                        } elseif ($percentage_array_of_date['current_percentage'] == 100) {
                            $task_percen = $green_border;
                            $cent_percent_status = 1;
                        } else {
                            $task_percen = $yellow_border;
                        }
                    } else {
                        if ($percentage_array_of_date['current_percentage'] == 100) {
                            $task_percen = $green_border;
                            $cent_percent_status = 1;
                        } elseif ($percentage_array_of_date['current_percentage'] != 0) {
                            $task_percen = $green_border;
                        } else {
                            $task_percen = $red_border;
                        }
                    }
                }
                if ($percentage_array_of_date['percent_date'] != '') {

                    $time_entry_date = str_replace('/', '-', $percentage_array_of_date['percent_date']);

                    $time_entry_date_ = strtotime($time_entry_date);
                    $current_perc = $percentage_array_of_date['current_percentage'] . '%';
                    if ($child == 1) {
                    }
                } else {
                    $time_entry_date_ = 0;
                }
            } else {
                $current_perc = '';
                $time_entry_date_ = 0;
            }
            if ($cent_percent_status == 1) {
                $task_percen = $green_border;
            }
            if ($current_date == $time_entry_date_) {
                $objPHPExcel->getActiveSheet()->setCellValue($column_new . $i, $current_perc);
                $objPHPExcel->getActiveSheet()->getStyle($column_new . $i)->applyFromArray($task_status);
                $objPHPExcel->getActiveSheet()->getStyle($column_new . $i)->applyFromArray($percentage_font);
            }
            if (($current_date >= $task_start) && ($current_date <= $task_end) && ($current_date <= $todays_date) && ($current_date == $time_entry_date_)) {
                $objPHPExcel->getActiveSheet()->getStyle($column_new . $i)->applyFromArray($task_percen);
            }
            $date_char++;
        }

        if (!empty($task['children']) && $task['children'] != '') {

            foreach ($task['children'] as $task) {
                // echo '<pre>';print_r($task);exit;     
                $child++;
                $i++;
                $i = $this->printArrayListexcel($task, $totdays, $week_dates, $child, $objPHPExcel, $i);
            }
        } else {
            Yii::app()->session['index_value'] = $i;
        }

        // }
    }

    /* Excel functions */

    function WeekAndMonthDaysArray($start_date, $totdays)
    {
        $monthdayarr = '';
        for ($i = 0; $i < $totdays; $i++) {
            $week_date = $this->add_date($start_date, $i);
            $week_datenext = $this->add_date($start_date, $i + 1);
            $week_day = explode('/', $week_date);
            $dat_next = explode('/', $week_datenext);
            $fromday = $this->add_date($start_date, 0);
            if ($fromday == $week_date) {
                $flag = 1;
            }
            $countmonth = 0;
            if ($week_day[1] != $dat_next[1]) {

                $endmonthday = $week_day[0];
                $startmothday = explode('-', $start_date);
                $monthdaysdiff = ($endmonthday - $dat_next[0]) + 1;
                if ($flag == 1) {
                    $monthdaysdiff = ($endmonthday - $startmothday[2]) + 1;
                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                    $countmonth++;
                    $flag = 0;
                } else {
                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                }
            }
            $week_dates[] = $week_date;
        }
        $return_array = array('week_dates' => $week_dates, 'monthdayarr' => $monthdayarr);
        return $return_array;
    }

    function setCalenderDaysExcel($days, $char, $objPHPExcel, $dates_font)
    {
        foreach ($days as $key => $value) {
            $strtime = strtotime($value);
            $prev_date = date('Y-m-d', strtotime('-2 day'));
            $objPHPExcel->getActiveSheet()->setCellValue($char . '3', '');
            $objPHPExcel->getActiveSheet()->getStyle($char . '3')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '44709C')
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->setCellValue($char . '2', substr(date('D', strtotime($value)), 0, 2) . "-" . date('d', strtotime($value)));
            $objPHPExcel->getActiveSheet()->getStyle($char . '2')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '44709C')
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($char . '2')->applyFromArray($dates_font);
            // $objPHPExcel->getActiveSheet()->getStyle($char.'2')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension($char)->setAutoSize(true);
            $char++;
        }
        return $char;
    }

    function setCalenderMonthExcel($monthdayarr, $totdays, $char, $objPHPExcel, $header_style, $monthsarr)
    {
        foreach ($monthdayarr as $month => $diffdays) {
            if (count($monthdayarr) == 1) {
                $diffdays = $totdays;
            }
            $objPHPExcel->getActiveSheet()->setCellValue($char . '1', $monthsarr[$month]);

            $merge_col = $char + $diffdays;
            $column = PHPExcel_Cell::stringFromColumnIndex(PHPExcel_Cell::columnIndexFromString($char) + $diffdays - 2);
            $objPHPExcel->getActiveSheet()->mergeCells($char . "1:" . $column . "1");
            $objPHPExcel->getActiveSheet()->getStyle($char . "1:" . $column . "1")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '44709C')
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($char . "1:" . $column . "1")->applyFromArray($header_style);
            $char = $column;
            $char++;
        }
        return $char;
    }

    function taskStatusAndBorderCss($task, $percentage_font)
    {

        $task_status = [];

        $percentage_font = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 9
            )
        );
        if ($task['task_status'] == 6) {
            $task_status = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'c2c2c2')
                )
            );
        } elseif ($task['task_status'] == 9) {
            $task_status = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '3BBF67')
                )
            );
        } elseif ($task['task_status'] == 5) {
            $task_status = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'f9c154')
                )
            );
        } elseif ($task['task_status'] == 8) {
            $task_status = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '763A96')
                )
            );
        } elseif ($task['task_status'] == 7) {
            $task_status = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '6EBEF4')
                )
            );
        } elseif ($task['task_status'] == 72) {
            $task_status = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '1d22bf')
                )
            );
            $percentage_font = array(
                'font' => array(
                    'color' => array('rgb' => 'ffffff'),
                    'size' => 9
                )
            );
        }

        $red_border = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => 'ff0000')
                )
            )
        );
        $green_border = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => '058a05')
                )
            )
        );
        $yellow_border = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => 'ffff00')
                )
            )
        );
        $return_array = array(
            'task_status' => $task_status,
            'red_border' => $red_border,
            'green_border' => $green_border,
            'yellow_border' => $yellow_border,
            'percentage_font' => $percentage_font
        );
        return $return_array;
    }

    public function actioncreateclient()
    {
        $client_name = $_GET['client_name'];
        $project_type = $_GET['project_type'];
        $model = new Clients;
        $model->name = $client_name;
        $model->project_type = $project_type;
        $model->status = 1;
        $model->created_by = yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->updated_by = yii::app()->user->id;
        $model->updated_date = date('Y-m-d');
        if ($model->save()) {
            $last_id = $model->cid;
            $html = "<option value=''>Select client</option>";
            $clients = Clients::model()->findAll(array('condition' => 'status = 1'));
            foreach ($clients as $key => $value) {
                $html .= "<option value=" . $value['cid'] . ">" . $value['name'] . "</option>";
            }
            echo json_encode(array('status' => 1, 'option' => $html, 'lastid' => $last_id));
        } else {
            echo json_encode(array('status' => 2));
        }
    }

    public function actionlog()
    {
        $model = new ProjectLog('search');
        $model->unsetAttributes();  // clear any default values
        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 

        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];

        $this->render(
            'log',
            array(
                'model' => $model,
            )
        );
    }

    public function actionSchedules()
    {
        $project_id = $start_date = $end_date = '';
        $final_result = $project = array();
        if (isset($_POST['project_id'])) {
            $project_id = $_POST['project_id'];
        } else {

            if (Yii::app()->user->project_id != "") {
                $project_id = Yii::app()->user->project_id;
            }
        }
        if (!empty($project_id)) {
            $project = Projects::model()->findByPk($project_id);
            $project_tasks = Tasks::model()->findAll(['condition' => 'trash =1 AND project_schedule_status = 1 AND project_id = ' . $project_id . '  order by ranking ASC']);
            $tasks = array();

            $i = 1;
            foreach ($project_tasks as $task) {
                if ($task['parent_tskid'] == NULL) {
                    if (!empty($task['milestone_id'])) {
                        $milestone_value = $task->milestone->milestone_title;
                    }
                }

                if ($i == 1) {
                    $start_date = date('Y-m-01', strtotime($task['start_date']));
                    $end_date = date('Y-m-t', strtotime($task['due_date']));
                }
                if (strtotime($end_date) < strtotime($task['due_date'])) {
                    $end_date = date('Y-m-t', strtotime($task['due_date']));
                }
                if (strtotime($task['start_date']) < strtotime($start_date)) {
                    $start_date = date('Y-m-01', strtotime($task['start_date']));
                }

                $arr = array(
                    "id" => $task['tskid'],
                    "parent_id" => $task['parent_tskid'],
                    "title" => $task['title'],
                    'start_date' => $task['start_date'],
                    'due_date' => $task['due_date'],
                    /* 'status'=>$status, */ 'current_percent' => '',
                    'percent_date' => '',
                    /* 'required_percentage'=>$task_work_status['required_percentage'], */ 'task_status' => $task['status'],
                    'mile_stone_id' => $task['milestone_id'],
                    'project_id' => $task['project_id'],
                    'ranking' => $task['ranking']
                );
                // array_push($arr, $task_work_status);

                array_push($tasks, $arr);
                $i++;
            }

            // $data = $this->buildTree($tasks);
            $data = $tasks;

            foreach ($data as $key => $item) {
                $final_result[$item['mile_stone_id']][$key] = $item;
            }
            ksort($final_result, SORT_NUMERIC);
        }
        $this->render(
            'schedules',
            array(
                'project_id' => $project_id,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'tasks' => $final_result,
                'project' => $project
            )
        );
    }

    protected function reportchildarraylistmonth($tasks, $totdays, $week_dates, $child)
    {
        foreach ($tasks as $task) {
            $time_entry_array = $task[0];
            if ($task['task_status'] == 6) {
                $task_status = 'grey';
            } elseif ($task['task_status'] == 9) {
                $task_status = 'green';
            } elseif ($task['task_status'] == 5) {
                $task_status = 'orange';
            } elseif ($task['task_status'] == 8) {
                $task_status = 'violet';
            } elseif ($task['task_status'] == 7) {
                $task_status = 'blue';
            } elseif ($task['task_status'] == 72) {
                $task_status = 'dark_blue';
            }
            $left_index = $child * 10 + 15;
            echo ' <tr id="">
                         
        <td colspan="4"  class=""  style="width:auto; padding-left:' . $left_index . 'px">' . $task["title"] . '#       
        </td>                            
        </th>';
            $task_start = strtotime($task['start_date']);
            $task_end = strtotime($task['due_date']);
            for ($p = 0; $p < count($week_dates); $p++) {
                // echo '<pre>';print_r($week_dates);exit;
                $task_start_month = date('m', strtotime($task['start_date']));
                $task_end_month = date('m', strtotime($task['due_date']));
                $task_start_year = date('Y', strtotime($task['start_date']));
                $task_end_year = date('Y', strtotime($task['due_date']));
                $data_split = explode('-', $week_dates[$p]);

                if ($task_start_month == $data_split[1] && $task_start_year = $data_split[2]) {
                    $task_status = 'yellow';
                } elseif ($task_end_month == $data_split[1] && $task_end_year == $data_split[2]) {
                    $task_status = 'solid_red';
                } else {
                    $task_status = 'green';
                }
                echo '<td width="20" data-toggle="modal"  id="" data-backdrop="static" data-target="#entry"';
                echo 'class="coldate ';
                if (($task_start_month == $data_split[1] && $task_start_year == $data_split[2]) || ($task_end_month == $data_split[1] && $task_end_year == $data_split[2])) {
                    echo $task_status;
                }

                echo '" data-value=""  data-coldate="">';


                echo '</td>';
            }
            echo '</tr>';
            if (!empty($task['children'])) {
                $child++;
                $this->reportchildarraylistmonth($task['children'], $totdays, $week_dates, $child);
                continue;
            }
        }
    }

    protected function reportchildarraylistyear($tasks, $totdays, $week_dates, $child)
    {
        foreach ($tasks as $task) {
            $left_index = $child * 10 + 15;
            echo ' <tr id="">
                         
        <td colspan="4"  class=""  style="width:auto; padding-left:' . $left_index . 'px">' . $task["title"] . '#       
        </td>                            
        </th>';
            $task_start = strtotime($task['start_date']);
            $task_end = strtotime($task['due_date']);
            for ($p = 0; $p < count($week_dates); $p++) {
                // echo '<pre>';
                // print_r($week_dates);
                // exit;
                $task_start_year = date('Y', strtotime($task['start_date']));
                $task_end_year = date('Y', strtotime($task['due_date']));
                if ($task_start_year != $task_end_year) {
                    if ($task_start_year == $week_dates) {
                        $task_status = 'yellow';
                    } elseif ($week_dates == $task_end_year) {
                        $task_status = 'solid_red';
                    } else {
                        $task_status = 'green';
                    }
                } else {
                    $task_status = 'solid_red';
                }
                echo '<td width="20" data-toggle="modal"  id=""data-backdrop="static" data-target="#entry"';
                echo 'class="coldate ';
                if (($task_start_year == $week_dates[$p]) || ($task_end_year == $week_dates[$p])) {
                    echo $task_status;
                }

                echo '" data-value=""  data-coldate="">';


                echo '</td>';
            }
            echo '</tr>';
            if (!empty($task['children'])) {
                $child++;
                $this->reportchildarraylistyear($task['children'], $totdays, $week_dates, $child);
                continue;
            }
        }
    }

    protected function reportchildarraylistweek($tasks, $totdays, $week_dates, $child)
    {
        foreach ($tasks as $task) {
            $task_status = 'grey';
            $left_index = $child * 10 + 15;
            echo ' <tr id="">
                         
        <td colspan="4"  class=""  style="width:auto; padding-left:' . $left_index . 'px">' . $task["title"] . '#       
        </td>                            
        </th>';
            $task_start = strtotime($task['start_date']);
            $task_end = strtotime($task['due_date']);
            for ($p = 0; $p < count($week_dates); $p++) {
                $color = $this->getcolumncolor($week_dates[$p]['start_date'], $week_dates[$p]['end_date'], $task['start_date'], $task['due_date']);
                $task_status = $color;
                $start_date_data = strtotime($week_dates[$p]['start_date']);
                $end_date_data = strtotime($week_dates[$p]['end_date']);

                echo '<td width="20" data-toggle="modal"  id=""  data-backdrop="static" data-target="#entry"';
                echo 'class="coldate ';
                if (($start_date_data >= $task_start && $start_date_data <= $task_end) || ($end_date_data >= $task_start && $end_date_data <= $task_end)) {
                    echo $task_status;
                }

                echo '" data-value=""  data-coldate="">';


                echo '</td>';
            }
            echo '</tr>';
            if (!empty($task['children'])) {
                $child++;
                $this->reportchildarraylistweek($task['children'], $totdays, $week_dates, $child);
                continue;
            }
        }
    }

    public function actionContractorBehindMailCron()
    {
        $today = date('Y-m-d');
        $contractor_ids = Yii::app()->db->createCommand("SELECT contractor_id FROM `pms_tasks`  WHERE  `task_type` = 1 AND
        `due_date` >= '$today' AND `start_date` <= '$today' AND `trash`= 1 AND `status` = 6 GROUP BY contractor_id")->queryAll();

        $contractor_ids = array_column($contractor_ids, 'contractor_id');
        // echo '<pre>';print_r($contractor_ids);exit;
        foreach ($contractor_ids as $contractor_id) {
            $tasks = '';
            if (!empty($contractor_id)) {
                $tasks = Yii::app()->db->createCommand("SELECT * FROM `pms_tasks`  WHERE contractor_id = $contractor_id AND `task_type` = 1 AND
                `due_date` >= '$today' AND `start_date` <= '$today' AND `trash`= 1 AND `status` = 6")->queryAll();
                $data = array();
                foreach ($tasks as $task) {

                    $data[] = $this->taskprogressnotificationcron($task);
                }
                $model2 = new MailLog;
                $today = date('Y-m-d H:i:s');
                $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
                $mail_data = $this->renderPartial('_task_progress_mail_contractor', array('data' => $data), true);
                $mail = new JPhpMailer();
                $headers = "Hostech";
                $bodyContent = $mail_data;

                $mail->IsSMTP();
                $mail->Host = $mail_model['smtp_host'];
                $mail->Port = $mail_model['smtp_port'];
                $mail->SMTPSecure = $mail_model['smtp_secure'];
                $mail->SMTPAuth = $mail_model['smtp_auth'];
                $mail->Username = $mail_model['smtp_username'];
                $mail->Password = $mail_model['smtp_password'];
                $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
                $mail->Subject = "Task Progress Notification";
                $contractor_model = Contractors::model()->findByPk($contractor_id);
                if (!empty($contractor_model->email_id)) {
                    $mail->addAddress($contractor_model->email_id); // Add a recipient
                }
                $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
                $dataJSON = json_encode($data);
                $mail_send_byJSON = json_encode($mail_send_by);
                $subject = $mail->Subject;
                $model2->send_to = $contractor_model->email_id;
                $model2->send_by = $mail_send_byJSON;
                $model2->send_date = $today;
                $model2->message = htmlentities($bodyContent);
                $model2->description = $mail->Subject;
                $model2->created_date = $today;
                $model2->created_by = Yii::app()->user->getId();
                $model2->mail_type = $subject;
                $mail->isHTML(true);
                $mail->MsgHTML($bodyContent);
                $mail->Body = $bodyContent;
                //    echo '<pre>';print_r($bodyContent);
                if (!empty($contractor_model->email_id)) {
                    if ($mail->Send()) {
                        $model2->sendemail_status = 1;
                    } else {
                        $model2->sendemail_status = 0;
                    }
                } else {
                    $model2->send_to = 'Invalid/Empty mail id';
                }

                $model2->save();
            }
        }
    }

    public function assignedManagersMail($model, $prev_assigned_managers)
    {

        $assigned_managers = explode(',', $model->assigned_to);
        if (!empty($prev_assigned_managers)) {
            $prev_assigned_managers = explode(',', $prev_assigned_managers);
        } else {
            $prev_assigned_managers = array();
        }

        $result_data = array();
        $managers_ids = array();
        foreach ($assigned_managers as $assign) {
            $user_datas = array();
            $user_model = Users::model()->findByPk($assign);
            if (!empty($user_model)) {
                $user_datas = array('name' => $user_model->first_name, 'email' => $user_model->email, 'id' => $user_model->userid);
                array_push($result_data, $user_model->first_name);
                array_push($managers_ids, $user_datas);
            }
        }
        $mangers_list = implode(',', $result_data);
        $return_status = true;
        $app_root = YiiBase::getPathOfAlias('webroot');
        $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
        $logo = $theme_asset_url . 'default-logo.png';
        if (file_exists($app_root . "/themes/assets/logo.png")) {
            $logo = $theme_asset_url . 'logo.png';
        }

        try {

            foreach ($managers_ids as $assigned_manager) {
                if (!in_array($assigned_manager['id'], $prev_assigned_managers)) {
                    $model2 = new MailLog;
                    $today = date('Y-m-d H:i:s');
                    $mail_data = '
					<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
					<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $assigned_manager['name'] . '</span></h3>
					<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
					<tr><td colspan="2" style="background-color: #ffff;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $logo . '"  width="50" height="50"  alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
					</tr>
					<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>PMS - New Project created</h2></div></td>
					</tr>
					<tr><td colspan="2">This is a notification to inform that new project #' . $model->project_no . ' has been created: </td></tr>
					<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project Name: </td><td style="border:1px solid #f5f5f5;">' . $model->name . '</td></tr>                
					<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($model->start_date)) . ' - ' . date('d-M-y', strtotime($model->end_date)) . '</td></tr>
					
					<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Assigned Managers:</td><td style="border:1px solid #f5f5f5;">' . $mangers_list . '</td></tr>              
					<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Created by: </td><td style="border:1px solid #f5f5f5;">' . $model->createdBy->first_name . '</td></tr>                              
					<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Description:</td> <td style="border:1px solid #f5f5f5;">' . nl2br($model->description) . '</td></tr>
					<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;" colspan="2">' . CHtml::link('Click here to see this task', $this->createAbsoluteUrl('projects/view', array('id' => $model->pid))) . '</td></tr>
					</table>
					
					<p>Sincerely,  <br />
					' . Yii::app()->name . '</p>
					</div>';
                    $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
                    $mail = new JPhpMailer(true);
                    $headers = "Hostech";
                    $bodyContent = $mail_data;
                    $mail->IsSMTP();
                    $mail->Host = $mail_model['smtp_host'];
                    $mail->Port = $mail_model['smtp_port'];
                    $mail->SMTPSecure = $mail_model['smtp_secure'];
                    $mail->SMTPAuth = $mail_model['smtp_auth'];
                    $mail->Username = $mail_model['smtp_username'];
                    $mail->Password = $mail_model['smtp_password'];
                    $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
                    $mail->Subject = "Project Assigned";
                    if (!empty($assigned_manager['email']))
                        $mail->addAddress($assigned_manager['email']); // Add a recipient                                                             
                    $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);

                    $mail_send_byJSON = json_encode($mail_send_by);
                    $subject = $mail->Subject;
                    $model2->send_to = $assigned_manager['email'];
                    $model2->send_by = $mail_send_byJSON;
                    $model2->send_date = $today;
                    $model2->message = htmlentities($bodyContent);
                    $model2->description = $mail->Subject;
                    $model2->created_date = $today;
                    $model2->created_by = Yii::app()->user->getId();
                    $model2->mail_type = $subject;
                    $mail->isHTML(true);
                    $mail->MsgHTML($bodyContent);
                    $mail->Body = $bodyContent;
                    $model2->sendemail_status = 0;


                    if (!empty($assigned_manager['email']) && $mail->send()) {
                        $model2->sendemail_status = 1;
                    }
                    if (!$model2->save()) {
                        throw new Exception($model2->getErrors());
                    }
                }
            }
        } catch (phpmailerException $e) {
            $return_status = $e->getMessage();
        } catch (Exception $e) {
            $return_status = $e->getMessage();
        } finally {
            return $return_status;
        }
    }

    public function actionAddmeeting()
    {


        $projects = array();
        $projects = $this->getProjects();
        $site_model = new Clientsite();
        $model = new SiteMeetings();
        $next_meeting_data = new SiteMeetings();
        $meeting_points_model = new MeetingMinutes();
        $report_model = new MeetingDetailedReports();
        $model->setScenario('meeting_start');
        $next_meeting_data->setScenario('meeting_final');
        $participant_model = new MeetingParticipants();
        $this->performAjaxValidation1($model);
        $this->performAjaxValidation3($next_meeting_data);
        $this->performAjaxValidation2($meeting_points_model);
        $this->render(
            '_meeting_form',
            array(
                'site_model' => $site_model,
                'model' => $model,
                'participant_model' => $participant_model,
                'meeting_points_model' => $meeting_points_model,
                'next_meeting_data' => $next_meeting_data,
                'projects' => $projects,
                'report_model' => $report_model,
                'approved_by' => ''
            )
        );
    }

    public function getProjects()
    {
        $criteria_proj = new CDbCriteria;
        $criteria_proj->select = 'project_id';
        $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
        $criteria_proj->group = 'project_id';
        $project_ids = Tasks::model()->findAll($criteria_proj);
        $project_id_array = array();
        foreach ($project_ids as $projid) {
            array_push($project_id_array, $projid['project_id']);
        }
        if (!empty($project_id_array)) {
            $project_id_array = implode(',', $project_id_array);
            if (Yii::app()->user->role != 1) {
                if (!empty($project_id_array)) {
                    $proj_condition = "status =1 AND (find_in_set(" . Yii::app()->user->id . ",assigned_to) OR pid IN (" . $project_id_array . ") )";
                } else {
                    $proj_condition = "status =1 AND (assigned_to IN (" . Yii::app()->user->id . "))";
                }
            } else {
                $proj_condition = " status = 1";
            }
        } else {
            if (Yii::app()->user->role != 1) {
                $proj_condition = "status =1 AND (find_in_set(" . Yii::app()->user->id . ",assigned_to))";
            } else {
                $proj_condition = " status = 1";
            }
        }
        $projects = CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC ')), 'pid', 'name');
        return $projects;
    }

    protected function performAjaxValidation1($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'site-meetings-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function performAjaxValidation3($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'site-meetings-form-2') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function performAjaxValidation2($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'meeting-minutes-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function getcolumncolor($week_start, $week_end, $start_date, $end_date)
    {
        if (strtotime($start_date) >= strtotime($week_start) && strtotime($end_date) <= strtotime($week_end)) {
            $color = 'solid_red';
        } elseif (strtotime($start_date) >= strtotime($week_start) && strtotime($start_date) <= strtotime($week_end)) {
            $color = 'yellow';
        } elseif (strtotime($end_date) >= strtotime($week_start) && strtotime($end_date) <= strtotime($week_end)) {
            $color = 'solid_red';
        } else {
            $color = 'green';
        }
        return $color;
    }
    function getcolourcode($start_date, $due_date, $date)
    {
        $start_date_obj = strtotime($start_date);
        $due_date_obj = strtotime($due_date);
        $date_object = strtotime($date);

        if ($date_object == $start_date_obj) {
            $color = 'yellow';
        } else if ($date_object == $due_date_obj) {
            $color = 'solid_red';
        } else if ($date_object >= $start_date_obj && $date_object <= $due_date_obj) {
            $color = 'green';
        } else {
            $color = '';
        }


        return $color;



    }

    public function actionminutesUpdate($id)
    {
        $meeting_model = SiteMeetings::model()->findByPk($id);
        $next_meeting_data = SiteMeetings::model()->findByPk($id);
        $meeting_model->setScenario('meeting_start');
        $next_meeting_data->setScenario('meeting_final');
        $projects = $this->getProjects();
        $site_model = new Clientsite();
        $meeting_points_model = new MeetingMinutes();
        $report_model = new MeetingDetailedReports();
        $meeting_points_model->meeting_id = $id;
        $this->performAjaxValidation1($meeting_model);
        $this->performAjaxValidation3($next_meeting_data);
        $this->performAjaxValidation2($meeting_points_model);
        if (isset($meeting_model->approved_by)) {
            $approved_by = $meeting_model->approved_by;
        } else {
            $approved_by = '';
        }
        if (!empty($meeting_model)) {
            $this->render(
                '_update_meeting_form',
                array(
                    'model' => $meeting_model,
                    'site_model' => $site_model,
                    'meeting_points_model' => $meeting_points_model,
                    'next_meeting_data' => $next_meeting_data,
                    'projects' => $projects,
                    'report_model' => $report_model,
                    'approved_by' => $approved_by
                )
            );
        }
    }
    public function actionsearchProject()
    {

        $tbl = Yii::app()->db->tablePrefix;
        if (isset($_POST['project_name'])) {
            $projects = Yii::app()->db->createCommand("SELECT * FROM {$tbl}projects
                 WHERE `name` LIKE '%{$_POST['project_name']}%'
                 ")->queryAll();


            echo "<div class='container custom-container'>";
            echo "<div class='row'>";
            foreach ($projects as $key => $value) {
                echo "<input type='hidden' id='text_id' value=1>";

                $image_url = Yii::app()->request->baseUrl . "/uploads/project/" . $value['img_path'];
                echo "<div class='col-xs-6 col-md-3 col-xl-2'>";
                $image_path = Yii::app()->basePath . '/../uploads/project/' . $value['img_path'];
                if ($value['img_path'] != '') {
                    $path = Yii::app()->baseUrl . '/uploads/project/' . $value['img_path'];
                }
                if (!file_exists($image_path) || $value['img_path'] == '') {
                    $caption_class = 'thumb-background';
                } else {
                    $caption_class = 'thumbnail-caption';
                }
                echo "<div class='thumbnail " . $caption_class . " ease_out'  data-id=" . $value['pid'] . " style='background-image:url($image_url)'>";

                echo "<div class='caption'>";

                echo "<h2 class='text-center'>" . $value['name'] . "</h2>";


                echo "</div>";
                echo "</div>";
                echo "</div>";
            }

            echo "</div>";
            echo "</div>";
        }
    }
    public function actiongetCloneDetails()
    {
        $clone_project_id = $_POST['clone_project_id'];
        Yii::app()->session['project_clone_id'] = $clone_project_id;
        $project_details = Projects::model()->findByPk($clone_project_id);
        $response_data = array(
            'pid' => $project_details->pid,
            'project_name' => $project_details->name,
            'budget' => $project_details->budget,
            'sqft' => $project_details->total_square_feet,
            'description' => $project_details->description,
            'status' => $project_details->status,
            'billable' => $project_details->billable,
            'start_date' => date('d-M-Y', strtotime($project_details->start_date)),
            'end_date' => date('d-M-Y', strtotime($project_details->end_date))

        );
        echo json_encode($response_data);
    }
    public function setCloneMilestone($project_id, $project_clone_id)
    {
        $project = Projects::model()->findByPk($project_id);
        $criteria = new CDbCriteria;
        $criteria->addCondition('project_id = ' . $project_clone_id . '');
        $milestone = Milestone::model()->findAll($criteria);
        $model = new Milestone();
        foreach ($milestone as $value) {
            $model = new Milestone();
            $model->project_id = $project_id;
            $model->milestone_title = $value->milestone_title;
            $model->status = $value->status;
            $model->start_date = $value->start_date;
            $model->end_date = $value->end_date;
            $model->ranking = $value->ranking;
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->save();
        }
    }
    public function setCloneArea($project_id, $project_clone_id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('project_id = ' . $project_clone_id . '');
        $area = Area::model()->findAll($criteria);
        $model = new Area();
        foreach ($area as $value) {
            $model = new Area();
            $model->area_title = $value->area_title;
            $model->status = 1;
            $model->project_id = $project_id;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
            $model->save();
        }
    }
    public function setCloneWorkType($project_id, $project_clone_id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('project_id = ' . $project_clone_id . '');
        $work_type = ProjectWorkType::model()->findAll($criteria);
        foreach ($work_type as $type) {
            $model = new ProjectWorkType();
            $model->project_id = $project_id;
            $model->work_type_id = $type->work_type_id;
            $model->ranking = $type->ranking;
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_date = date('Y-m-d H:i:s');
            $model->save();
        }
    }
    public function setTasks($project_id, $project_clone_id)
    {
        $project = Projects::model()->findByPk($project_id);
        $criteria = new CDbCriteria;
        $criteria->addCondition('project_id = ' . $project_clone_id . '');
        $tasks = Tasks::model()->findAll($criteria);
        $parent_task_id = "";
        foreach ($tasks as $tasks) {
            $clone_area_id = $tasks->area;
            $area_id = "";

            if (isset($clone_area_id)) {
                $clone_area_name = $tasks->area0->area_title;
                $area_det = Area::model()->findByAttributes(
                    array(
                        'area_title' => $clone_area_name,
                        'project_id' => $project_id
                    )
                );
                $area_id = $area_det->id;
            }
            $milestone_id = '';
            $clone_milestone_id = $tasks->milestone_id;
            $clone_milestone_name = $tasks->milestone->milestone_title;
            $milestone_det = Milestone::model()->findByAttributes(
                array(
                    'milestone_title' => $clone_milestone_name,
                    'project_id' => $project_id
                )
            );

            if ($milestone_det) {
                $milestone_id = $milestone_det->id;
            }

            $model = new Tasks();
            $model->setscenario('mainTasks');
            $model->project_id = $project_id;
            $model->clientsite_id = "";
            $model->title = $tasks->title;
            $model->start_date = $tasks->start_date;
            $model->due_date = $tasks->due_date;
            $model->status = 75;
            $model->priority = $tasks->priority;
            $model->assigned_to = '';
            $model->report_to = '';
            $model->coordinator = '';
            $model->billable = $tasks->billable;
            $model->total_hrs = $tasks->total_hrs;
            $model->hourly_rate = $tasks->hourly_rate;
            $model->description = $tasks->description;
            $model->trash = 1;
            $model->parent_tskid = $tasks->parent_tskid;
            $model->progress_percent = '';
            $model->acknowledge_by = $tasks->acknowledge_by;
            $model->quantity = $tasks->quantity;
            $model->unit = $tasks->unit;
            $model->rate = $tasks->rate;
            $model->amount = $tasks->amount;
            $model->milestone_id = $milestone_id;
            $model->contractor_id = $tasks->contractor_id;
            $model->task_duration = $tasks->task_duration;
            $model->daily_target = $tasks->daily_target;
            $model->work_type_id = $tasks->work_type_id;
            $model->allowed_workers = $tasks->allowed_workers;
            $model->required_workers = $tasks->required_workers;
            $model->email = '';
            $model->task_type = $tasks->task_type;
            $model->area = $area_id;
            $model->ranking = $tasks->ranking;
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_date = date('Y-m-d');
            $model->save();
        }
        $this->updateParentTask($project_id);

        $new_project = Projects::model()->findByPk($project_id);

        Yii::app()->db->createCommand()
            ->update(
                'pms_tasks',
                array(
                    'start_date' => $new_project->clone_start_date,
                    'due_date' => $new_project->clone_end_date
                ),
                'project_id=:project_id',
                array(':project_id' => $project_id)
            );

        Yii::app()->db->createCommand()
            ->update(
                'pms_milestone',
                array(
                    'start_date' => $new_project->clone_start_date,
                    'end_date' => $new_project->clone_end_date
                ),
                'project_id=:project_id',
                array(':project_id' => $project_id)
            );

        Yii::app()->db->createCommand()
            ->update(
                'pms_projects',
                array(
                    'start_date' => $new_project->clone_start_date,
                    'end_date' => $new_project->clone_end_date
                ),
                'pid=:project_id',
                array(':project_id' => $project_id)
            );
    }
    public function updateParentTask($project_id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('project_id = ' . $project_id . '');
        $criteria->addCondition('t.parent_tskid IS NOT NULL');
        $tasks = Tasks::model()->findAll($criteria);

        foreach ($tasks as $task) {
            $parent_id = $task->parent_tskid;

            $clone_parent_task_det = Tasks::model()->findByAttributes(
                array(
                    'tskid' => $parent_id,
                )
            );

            $clone_parent_name = $clone_parent_task_det->title;

            $parent_task_det = Tasks::model()->findByAttributes(
                array(
                    'title' => $clone_parent_name,
                    'project_id' => $project_id
                )
            );
            if ($parent_task_det) {
                $parent_task_id = $parent_task_det->tskid;
            }
            $update = "UPDATE pms_tasks SET parent_tskid=$parent_task_id WHERE project_id= $project_id 
AND parent_tskid=$parent_id";
            Yii::app()->db->createCommand($update)->execute();
        }
    }
    public function setWorkSite($project_id, $project_clone_id)
    {

        $model = new Clientsite;
        $criteria = new CDbCriteria;
        $criteria->addCondition('pid = ' . $project_clone_id . '');
        $client_site = Clientsite::model()->findAll($criteria);
        foreach ($client_site as $site) {
            $model = new Clientsite();
            $model->pid = $project_id;
            $model->site_name = " " . $site->site_name;
            $model->latitude = $site->latitude;
            $model->longitude = $site->longitude;
            $model->distance = $site->distance;
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_date = date('Y-m-d');
            $model->save();
        }
    }
    public function actionmeetingDetailedReport()
    {


        if (isset($_POST['report_id'])) {
            $report_id = $_POST['report_id'];
            $report_meeting_id = $_POST['report_meeting_id'];

            $meeting_det_report_model = new MeetingDetailedReports();
            $task_model = new Tasks();
            $project_report_model = ProjectReport::model()->findByPk($report_id);
            $criteria = new CDbCriteria;
            $criteria->addBetweenCondition(
                "start_date",
                $project_report_model->from_date,
                $project_report_model->to_date,
                'AND'
            );
            $criteria->addBetweenCondition(
                "due_date",
                $project_report_model->from_date,
                $project_report_model->to_date,
                'OR'
            );
            $criteria->compare('project_id', $project_report_model->project_id);
            $project_tasks = Tasks::model()->findAll($criteria);
            $milestone_datas = $this->getMilestones($project_tasks, $project_report_model->project_id);



            $tasklist = $this->renderPartial('meeting_detailed_report', array(
                'model' => $meeting_det_report_model,
                'project_report_model' => $project_report_model,
                'task_model' => $task_model,
                'project_tasks' => $project_tasks,
                'milestone_datas' => $milestone_datas,
                'report_id' => $report_id,
                'report_meeting_id' => $report_meeting_id
            ), true);
            echo json_encode($tasklist);
        }
    }
    public function actionsavemeetingdetreport()
    {
        $count = $_POST['count'];
        $milestone = $_POST['milestone_arr'];
        $contractor = $_POST['contractor_arr'];
        $task = $_POST['main_task_arr'];
        $sub_task = $_POST['sub_task_arr'];
        $area = $_POST['area_arr'];
        $work_type = $_POST['work_type_arr'];
        $total_qty = $_POST['total_qty_arr'];
        $target_qty = '';
        $achieved_qty = '';
        $balance_qty = '';
        $progress = '';
        $status = '';
        $description = '';
        $comment = '';
        if (isset($_POST['target_qty_arr'])) {
            $target_qty = $_POST['target_qty_arr'];
        }

        if (isset($_POST['achieved_qty_arr'])) {
            $achieved_qty = $_POST['achieved_qty_arr'];
        }
        if (isset($_POST['balance_qty_arr'])) {

            $balance_qty = $_POST['balance_qty_arr'];
        }
        if (isset($_POST['progress_arr'])) {
            $progress = $_POST['progress_arr'];
        }
        if (isset($_POST['status_arr'])) {
            $status = $_POST['status_arr'];
        }
        if (isset($_POST['description_arr'])) {
            $description = $_POST['description_arr'];
        }
        if (isset($_POST['comment_arr'])) {
            $comment = $_POST['comment_arr'];
        }
        $report_meeting_id = $_POST['report_meeting_id'];
        $report_id = $_POST['report_id'];
        $condition = new CDbCriteria();
        $condition->addInCondition('meeting_id', [$report_meeting_id]);
        $condition->addInCondition('report_id', [$report_id]);
        $already_exist = MeetingDetailedReports::model()->findAll($condition);
        if (count($already_exist) > 0) {
            $return_result = array('status' => 0);
        } else {
            for ($i = 0; $i < $count; $i++) {
                $model = new MeetingDetailedReports();
                $model->meeting_id = $report_meeting_id;
                $model->report_id = $report_id;
                $model->milestone = $milestone[$i];
                $model->contractor = $contractor[$i];
                $model->main_task = $task[$i];
                $model->sub_task = $sub_task[$i];
                $model->area = $area[$i];
                $model->work_type = $work_type[$i];
                $model->total_quantity = $total_qty[$i];
                $model->target_quantity = $target_qty[$i];
                $model->achieved_quantity = $achieved_qty[$i];
                $model->balance_quantity = $balance_qty[$i];
                $model->progress = $progress[$i];
                $model->status = $status[$i];
                $model->description = $description[$i];
                $model->comments = $comment[$i];
                $model->save();
            }
            $return_result = array('status' => 1);
        }
        echo json_encode($return_result);
    }
    public function actionupdatemeetingreport($id, $meeting_id)
    {
        $Criteria = new CDbCriteria();
        $Criteria->condition = "report_id =" . $id . " and meeting_id=" . $meeting_id . " and milestone IS NOT NULL";
        $model = MeetingDetailedReports::model()->findAll($Criteria);
        $this->render(
            'update_meeting_details_report',
            array(
                'model' => $model,
                'meeting_id' => $meeting_id

            )
        );
    }
    public function actionviewStep3FormData()
    {
        $id = $_GET['id'];
        $meeting_id = $_GET['meeting_id'];

        $Criteria = new CDbCriteria();
        $Criteria->condition = "report_id =" . $id . " and meeting_id=" . $meeting_id . " and milestone IS NOT NULL";
        $model = MeetingDetailedReports::model()->findAll($Criteria);

        $htmlContent = $this->renderPartial('_mom_step_3_view', array(
            'model' => $model,
            'meeting_id' => $meeting_id
        ), true);

        echo json_encode(['html' => $htmlContent]);
    }

    public function actionupdatemeetingdetreport()
    {
        if (isset($_POST['id'])) {
            $update = Yii::app()->db->createCommand()
                ->update(
                    'pms_meeting_detailed_reports',
                    array(
                        'comments' => $_POST['comment'],

                    ),
                    'id=:id',
                    array(':id' => $_POST['id'])
                );
            if ($update) {
                $return_result = array('status' => 1);
                echo json_encode($return_result);
            }
        }
    }
    public function actiongettaskForm()
    {
        $model = new Tasks();
        $project_id = $_GET['asDialog'];
        $location = [];

        $this->layout = '//layouts/iframe';
        $this->render(
            'meeting_task_form',
            array(
                'model' => $model,
                'project_id' => $project_id,
                'location' => $location
            )
        );
    }
    public function actionsaveTask()
    {
        $task_model = new Tasks();
        $task_model->project_id = $_POST['project_id'];
        $task_model->clientsite_id = $_POST['task_client_site'];
        $task_model->title = $_POST['task_title'];
        $task_model->start_date = date('Y-m-d', strtotime($_POST['task_start_date']));
        $task_model->due_date = date('Y-m-d', strtotime($_POST['task_end_date']));
        $task_model->status = $_POST['task_status'];
        $task_model->priority = $_POST['task_prority'];
        $task_model->assigned_to = $_POST['task_assigned_to'];
        $task_model->report_to = $_POST['task_report_to'];
        $task_model->coordinator = $_POST['task_coordinator'];
        $task_model->billable = 3;
        $task_model->description = $_POST['task_description'];
        $task_model->parent_tskid = $_POST['main_task_id'];
        $task_model->quantity = $_POST['task_qty'];
        $task_model->unit = $_POST['task_unit'];
        $task_model->rate = $_POST['task_rate'];
        $task_model->amount = $_POST['task_amount'];
        $task_model->milestone_id = $_POST['task_milestone'];
        $task_model->contractor_id = $_POST['task_contractor'];
        $task_model->task_duration = $_POST['task_duration'];
        $task_model->daily_target = $_POST['task_target'];
        $task_model->work_type_id = $_POST['task_work_type'];
        $task_model->allowed_workers = $_POST['tasks_allowed_workers'];
        $task_model->required_workers = $_POST['task_required_workers'];
        $task_model->email = $_POST['task_email'];
        $task_model->task_type = $_POST['task_type'];
        $task_model->area = $_POST['task_area'];
        $task_model->meeting_status = 1;
        $task_model->created_by = yii::app()->user->id;
        $task_model->created_date = date('Y-m-d');
        $task_model->updated_by = yii::app()->user->id;

        if ($task_model->save()) {
            $this->meetingdependancyBasedDate($_POST['task_ids'], $_POST['percentage_values'], $_POST['depndant_on_types'], $task_model);
            $this->milestonedatechange($task_model);
            $return_result = array('status' => 1);
            echo json_encode($return_result);
        }
    }
    public function actionProject_mapping()
    {
        $model = new Projects('project_mapping');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];

        $this->render(
            'project_mapping',
            array(
                'model' => $model,
            )
        );
    }
    public function actionAdd_project_mapping()
    {
        if ($_POST['pms_project_id'] != "" && $_POST['acc_project_id'] != "") {
            $pms_project_id = $_POST['pms_project_id'];
            $acc_project_id = $_POST['acc_project_id'];
            $created_by = Yii::app()->user->id;
            $date = date('Y-m-d H:i:s');

            $sql = "INSERT INTO 
        `pms_acc_project_mapping`
        (`id`,`pms_project_id`, `acc_project_id`,`created_from`,`created_by`,`created_date`)
          VALUES (null,'$pms_project_id','$acc_project_id','0','$created_by','$date')";
            Yii::app()->db->createCommand($sql)->query();

            $return_result = array('status' => 1);

            echo json_encode($return_result);
        }
    }
    public function actionaddmom()
    {
        $projects = array();
        $projects = $this->getProjects();
        $site_model = new Clientsite();
        $model = new SiteMeetings();
        $next_meeting_data = new SiteMeetings();
        $meeting_points_model = new MeetingMinutes();
        $report_model = new MeetingDetailedReports();
        $model->setScenario('meeting_start');
        $next_meeting_data->setScenario('meeting_final');
        $participant_model = new MeetingParticipants();
        $this->performAjaxValidation1($model);
        $this->performAjaxValidation3($next_meeting_data);
        $this->performAjaxValidation2($meeting_points_model);

        $body_array = [

            array("", "", "", "", "", ""),
            array("", "", "", "", "", ""),
            array("", "", "", "", "", ""),

        ];
        $body_array = json_encode($body_array);
        $this->render(
            '_mom_form',
            array(
                'site_model' => $site_model,
                'model' => $model,
                'participant_model' => $participant_model,
                'meeting_points_model' => $meeting_points_model,
                'next_meeting_data' => $next_meeting_data,
                'projects' => $projects,
                'report_model' => $report_model,
                'approved_by' => '',
                'body_array' => $body_array
            )
        );
    }



    public function actionmomupdate($id)
    {
        $meeting_model = SiteMeetings::model()->findByPk($id);
        $next_meeting_data = SiteMeetings::model()->findByPk($id);
        $meeting_model->setScenario('meeting_start');
        $next_meeting_data->setScenario('meeting_final');
        $projects = $this->getProjects();
        $site_model = new Clientsite();
        $meeting_points_model = new MeetingMinutes();
        $report_model = new MeetingDetailedReports();
        $meeting_points_model->meeting_id = $id;
        $this->performAjaxValidation1($meeting_model);
        $this->performAjaxValidation3($next_meeting_data);
        $this->performAjaxValidation2($meeting_points_model);
        if (isset($meeting_model->approved_by)) {
            $approved_by = $meeting_model->approved_by;
        } else {
            $approved_by = '';
        }


        $meeting_minutes_data = MeetingMinutes::model()->with('meetingMinutesUsers')->findAll(
            array("condition" => "meeting_id =  $id")
        );
        if (count($meeting_minutes_data) == 0) {
            $body_array = [

                array("", "", "", "", ""),
                array("", "", "", "", ""),
                array("", "", "", "", ""),

            ];
        }

        foreach ($meeting_minutes_data as $data) {
            $body_array[] = array(
                $data->points_discussed,
                $data->meeting_minutes_status,
                $data->meetingMinutesUsers->user_id,
                $data->end_date,
                $data->id,
                $data->task_id ? $data->task_id : '',
            );
        }
        $body_array = json_encode($body_array);
        // print_r($body_array);die;
        if (!empty($meeting_model)) {
            $this->render(
                '_update_mom_form',
                array(
                    'model' => $meeting_model,
                    'site_model' => $site_model,
                    'meeting_points_model' => $meeting_points_model,
                    'next_meeting_data' => $next_meeting_data,
                    'projects' => $projects,
                    'report_model' => $report_model,
                    'approved_by' => $approved_by,
                    'body_array' => $body_array
                )
            );
        }
    }


    public function actionmom_meetingDetailedReport()
    {


        if (isset($_POST['report_id'])) {
            $report_id = $_POST['report_id'];
            $report_meeting_id = $_POST['report_meeting_id'];

            $meeting_det_report_model = new MeetingDetailedReports();
            $task_model = new Tasks();
            $project_report_model = ProjectReport::model()->findByPk($report_id);
            $criteria = new CDbCriteria;
            $criteria->addBetweenCondition(
                "start_date",
                $project_report_model->from_date,
                $project_report_model->to_date,
                'AND'
            );
            $criteria->addBetweenCondition(
                "due_date",
                $project_report_model->from_date,
                $project_report_model->to_date,
                'OR'
            );
            $criteria->compare('project_id', $project_report_model->project_id);
            $project_tasks = Tasks::model()->findAll($criteria);
            $milestone_datas = $this->getMilestones($project_tasks, $project_report_model->project_id);



            $tasklist = $this->renderPartial('mom_meeting_detailed_report', array(
                'model' => $meeting_det_report_model,
                'project_report_model' => $project_report_model,
                'task_model' => $task_model,
                'project_tasks' => $project_tasks,
                'milestone_datas' => $milestone_datas,
                'report_id' => $report_id,
                'report_meeting_id' => $report_meeting_id
            ), true);
            echo json_encode($tasklist);
        }
    }

    public function actionprojectPrefix()
    {
        // $this->render('project_prefix_form');
        $tbl_px = Yii::app()->db->tablePrefix;
        $query1 = "SELECT * FROM {$tbl_px}project_prefix  ";
        $data = Yii::app()->db->createCommand($query1)->queryRow();

        $this->render(
            'project_prefix',
            array(
                'data' => $data,


            )
        );
    }
    public function actionaddProjectPrefix()
    {
        $tbl_px = Yii::app()->db->tablePrefix;

        if (isset($_POST['prefix']) && isset($_POST['prefix_id'])) {
            // $update = " UPDATE {$tbl_px}project_prefix SET prefix=". $_POST['prefix'] ."where id=" . $_POST['prefix_id'];
            // Yii::app()->db->createCommand($update)->query();

            $updatesql = "UPDATE `pms_project_prefix` SET `prefix` = '" . $_POST['prefix'] . "' WHERE `pms_project_prefix`.`id` = 2;";
            Yii::app()->db->createCommand($updatesql)->query();

            $return_data = array('status' => 1);
            echo json_encode($return_data);
            exit;
        }


        $id = $_POST['id'];
        $tbl_px = Yii::app()->db->tablePrefix;
        $query1 = "SELECT * FROM {$tbl_px}project_prefix  where id=" . $id;
        $data = Yii::app()->db->createCommand($query1)->queryRow();

        $data_res = $this->renderPartial('project_prefix_form', array(

            'data' => $data
        ), true);

        echo json_encode($data_res);
    }
    public function actionsendDailyProgressEmail123()
    {

        $date = date('Y-m-d');
        $criteria = new CDbCriteria;
        $criteria->select = "pid,name";
        $criteria->join = "INNER JOIN pms_daily_work_progress d ON (d.project_id=t.pid)";


        $criteria->group = 'd.project_id';

        $project = Projects::model()->findAll($criteria);
        $project_data_array = array();

        foreach ($project as $projects) {

            $items_array = [];
            $task_array = array();
            // $task_array_data=array();
            $tasks = Tasks::model()->findAll(
                array("condition" => "project_id =  $projects->pid and task_type=1")
            );

            foreach ($tasks as $task) {


                $unit = isset($task->unit) ? $task->unit0->unit_title : '';
                $rate = isset($task->rate) ? $task->rate : 0;
                $task_name = $task->title;


                // array_push($task_array_data,$task_array);

                $sql = "SELECT qty,id FROM pms_daily_work_progress where taskid=" . $task->tskid . " and date='$date'";
                $work_progress = Yii::app()->db->createCommand($sql)->queryRow();
                $quantity = "";

                if ($work_progress) {

                    $quantity = $work_progress['qty'];

                    $wpr_id = $work_progress['id'];



                    $item_sql = "SELECT * "
                        . " FROM pms_wpr_item_used INNER JOIN jp_warehousestock ON jp_warehousestock.warehousestock_id=pms_wpr_item_used.item_rate_id where wpr_id=" . $wpr_id;
                    $items_usesd = Yii::app()->db->createCommand($item_sql)->queryAll();

                    foreach ($items_usesd as $items) {
                        $item_rate = $items['rate'];
                        $item_unit = $items['warehousestock_unit'];
                        $item_id = $items['item_id'];
                        $item_quantity = $items['item_count'];

                        $spec_sql = "SELECT specification, brand_id, cat_id "
                            . " FROM jp_specification "
                            . " WHERE id=" . $item_id . "";
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
                        $brand = '';
                        $cat_sql = "SELECT * FROM jp_purchase_category "
                            . " WHERE id='" . $specification['cat_id'] . "'";
                        $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                        if ($specification['brand_id'] != NULL) {
                            $brand_sql = "SELECT brand_name "
                                . " FROM jp_brand "
                                . " WHERE id=" . $specification['brand_id'] . "";
                            $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                            $brand = '-' . $brand_details['brand_name'];
                        }

                        $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];


                        $items_array[] = array(
                            'item_name' => $item_name,
                            'item_rate' => $item_rate,
                            'item_unit' => $item_unit,
                            'item_quantity' => $item_quantity

                        );
                    }
                }
                $task_array[] = array(
                    'task_unit' => $unit,
                    'task_rate' => $rate,
                    'task_name' => $task_name
                );
            }



            $project_name = $projects->name;

            $project_data_array = array(
                'task_array' => $task_array,
                'item_array' => $items_array
            );


            echo "<pre>", print_r($project_data_array);


            //$this->dailyProgressEmailSend($projects,$project_data_array);



        }
    }
    public function actionsendDailyProgressEmail()
    {

        $date = date('Y-m-d');
        $criteria = new CDbCriteria;
        $criteria->select = "pid,name,assigned_to";
        $criteria->join = "INNER JOIN pms_daily_work_progress d ON (d.project_id=t.pid)";


        $criteria->group = 'd.project_id';

        $project = Projects::model()->findAll($criteria);


        foreach ($project as $projects) {
            $project_array_data = array();
            $items_array_data = array();
            $task_array_data = array();
            $tasks = Tasks::model()->findAll(
                array("condition" => "project_id =  $projects->pid and task_type=1")
            );

            foreach ($tasks as $task) {


                $unit = isset($task->unit) ? $task->unit0->unit_title : '';
                $rate = isset($task->rate) ? $task->rate : 0;
                $task_name = $task->title;

                $sql = "SELECT qty,id,approve_status FROM pms_daily_work_progress where taskid=" . $task->tskid . " and date='$date'";
                $work_progress = Yii::app()->db->createCommand($sql)->queryRow();
                $quantity = "";
                $approve_status = "";

                if ($work_progress) {

                    $quantity = $work_progress['qty'];

                    $wpr_id = $work_progress['id'];

                    $approve_status = $work_progress['approve_status'];



                    $item_sql = "SELECT * "
                        . " FROM pms_wpr_item_used INNER JOIN jp_warehousestock ON jp_warehousestock.warehousestock_id=pms_wpr_item_used.item_rate_id where wpr_id=" . $wpr_id;
                    $items_usesd = Yii::app()->db->createCommand($item_sql)->queryAll();




                    $utilised_resource_sql_query = "SELECT  pms_wpr_resource_used.*,pms_unit.unit_title,pms_resources.resource_name,pms_resources.resource_rate "
                        . " FROM pms_wpr_resource_used INNER JOIN pms_unit ON pms_unit.id = pms_wpr_resource_used.resource_unit
        INNER JOIN pms_resources ON pms_resources.id = pms_wpr_resource_used.resource_id 
         where wpr_id=" . $wpr_id;

                    $utilised_resource = Yii::app()->db->createCommand($utilised_resource_sql_query)
                        ->queryAll();

                    foreach ($utilised_resource as $resources) {
                        $project_array_data[$projects->pid]['resources'][] = array(
                            'resource_name' => $resources['resource_name'],
                            'resource_unit' => $resources['unit_title'],
                            'resource_rate' => $resources['resource_rate'],
                            'resource_quantity' => $resources['resource_qty'],
                            'total_amount' => $resources['amount']


                        );
                    }


                    foreach ($items_usesd as $items) {
                        $item_rate = $items['rate'];
                        $item_unit = $items['warehousestock_unit'];
                        $item_id = $items['item_id'];
                        $item_quantity = $items['item_count'];

                        $spec_sql = "SELECT specification, brand_id, cat_id "
                            . " FROM jp_specification "
                            . " WHERE id=" . $item_id . "";
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
                        $brand = '';
                        $cat_sql = "SELECT * FROM jp_purchase_category "
                            . " WHERE id='" . $specification['cat_id'] . "'";
                        $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                        if ($specification['brand_id'] != NULL) {
                            $brand_sql = "SELECT brand_name "
                                . " FROM jp_brand "
                                . " WHERE id=" . $specification['brand_id'] . "";
                            $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                            $brand = '-' . $brand_details['brand_name'];
                        }

                        $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];

                        $project_array_data[$projects->pid]['items'][] = array(
                            'item_name' => $item_name,
                            'item_rate' => $item_rate,
                            'item_unit' => $item_unit,
                            'item_quantity' => $item_quantity

                        );
                        //array_push($items_array_data,$items_array);
                    }
                }

                // labour used

                $labour_sql = "SELECT * FROM pms_daily_work where taskid=" . $task->tskid . " and date='$date'";
                $labour_data = Yii::app()->db->createCommand($labour_sql)->queryRow();

                if ($labour_data) {
                    $labours_used_sql = "SELECT jp_labours.labour_label,jp_labours.id,pms_daily_work_labours_used.number_of_labour,pms_daily_work_labours_used.labour_wage,pms_daily_work_labours_used.labour_amount,pms_daily_work_labours_used.total_amount"
                        . " FROM pms_daily_work_labours_used INNER JOIN jp_labours ON jp_labours.id=pms_daily_work_labours_used.labour_id where pms_daily_work_labours_used.daily_work_id=" . $labour_data['id'];

                    $labours_used_data = Yii::app()->db->createCommand($labours_used_sql)
                        ->queryAll();

                    foreach ($labours_used_data as $labour) {
                        $project_array_data[$projects->pid]['labour'][] = array(
                            'labour_name' => $labour['labour_label'],
                            'labour_count' => $labour['number_of_labour'],
                            'labour_wage' => $labour['labour_wage'],
                            'labour_amount' => $labour['labour_amount'],
                            'total_amount' => $labour['total_amount']


                        );
                    }
                }

                // end labour used

                $project_array_data[$projects->pid]['tasks'][] = array(
                    'task_unit' => $unit,
                    'task_rate' => $rate,
                    'task_name' => $task_name,
                    'task_id' => $task->tskid,
                    'approve_status' => $approve_status,
                    'quantity' => $quantity
                );
            }

            //  echo '<pre>';print_r($project_array_data);


            $accounts_project_sql = "SELECT acc_project_id "
                . " FROM pms_acc_project_mapping where pms_project_id=" . $projects->pid;
            $accounts_project = Yii::app()->db->createCommand($accounts_project_sql)->queryRow();

            $accounts_project_id = $accounts_project['acc_project_id'];

            if ($accounts_project_id != "") {

                $labour_sql = "SELECT * FROM jp_dailyreport where projectid=" . $accounts_project_id;
                $labour_report = Yii::app()->db->createCommand($labour_sql)->queryAll();
            }

            $this->dailyProgressEmailSend($projects, $project_array_data);
        }
    }
    public function dailyProgressEmailSend($projects, $project_data_array)
    {

        $mail_data = $this->renderPartial('_daily_progress_mail', array('data_array' => $project_data_array, 'project' => $projects), true);

        //  echo  $mail_data; 

        $subject = 'Daily Progress Report -' . date('d-M-Y');

        $project_assigned_to = $projects->assigned_to;



        $project_assigned_to = Yii::app()->db->createCommand()
            ->select(' email')
            ->from('pms_users')
            ->where('userid IN (' . $project_assigned_to . ")")
            ->queryAll();



        foreach ($project_assigned_to as $assigned_to) {
            $toemail[] = $assigned_to['email'];
        }

        $toemail = implode(',', $toemail);
        $this->sendEmailToRecipients($toemail, $subject, $mail_data);
    }



    public function getFilteredTasks($task, $filter_type)
    {

        $on_scheduled_task = array();
        $behind_schedule_task = array();
        if ($filter_type == 1) {

            if ($task['task_type'] == 1) {

                if ($task['task_duration'] != "" && $task['daily_target'] != "") {

                    $work_progress = Yii::app()->db->createCommand('SELECT SUM(qty) as completed_qty FROM pms_daily_work_progress WHERE taskid = ' . $task['tskid'] . ' and approve_status=1
          ')->queryRow();
                    $achieved_qty = 0;

                    if ($work_progress) {

                        $achieved_qty = $work_progress['completed_qty'];

                        if ($achieved_qty != 0) {

                            if ($task['due_date'] > date('Y-m-d')) {

                                $task_duration = $this->getTaskDuration($task['start_date'], date('Y-m-d'), $task['project_id'], $task['clientsite_id']);

                                $target_till_date = $task_duration * $task['daily_target'];

                                if ($achieved_qty >= $target_till_date || $achieved_qty == $task['quantity']) {
                                    $on_scheduled_task = $task;
                                }
                            } else {

                                $task_duration = $this->getTaskDuration($task['start_date'], $task['due_date'], $task['project_id'], $task['clientsite_id']);

                                $target_till_date = $task_duration * $task['daily_target'];

                                if ($achieved_qty >= $target_till_date || $achieved_qty == $task['quantity']) {

                                    $on_scheduled_task = $task;
                                }
                            }
                        }
                    }
                }
            } else {

                $time_enries = Yii::app()->db->createCommand('SELECT approve_status,completed_percent, entry_date FROM pms_time_entry WHERE tskid = ' . $task['tskid'] . ' and approve_status=1
            ORDER BY updated_date DESC LIMIT 1')->queryRow();
                $complete_percent = 0;
                if (!empty($time_enries)) {
                    $complete_percent = $time_enries['completed_percent'];
                }

                $per_day_percentage = (1 / $task['task_duration']) * 100;

                if ($complete_percent != 0) {
                    if ($task['due_date'] > date('Y-m-d')) {

                        $task_duration = $this->getTaskDuration($task['start_date'], date('Y-m-d'), $task['project_id'], $task['clientsite_id']);

                        $target_till_date = $per_day_percentage * $task_duration;

                        if ($complete_percent >= $target_till_date) {
                            $on_scheduled_task = $task;
                        }
                    } else {
                        $task_duration = $this->getTaskDuration($task['start_date'], $task['due_date'], $task['project_id'], $task['clientsite_id']);
                        $target_till_date = $per_day_percentage * $task_duration;

                        if ($complete_percent >= $target_till_date) {
                            $on_scheduled_task = $task;
                        }

                    }
                }

            }

            if (count($on_scheduled_task) > 0) {

                return $on_scheduled_task;
            }
        } else {

            if ($task['task_type'] == 1) {

                if ($task['task_duration'] != "" && $task['daily_target'] != "") {

                    $work_progress = Yii::app()->db->createCommand('SELECT SUM(qty) as completed_qty FROM pms_daily_work_progress WHERE taskid = ' . $task['tskid'] . ' and approve_status=1
                ')->queryRow();

                    if ($work_progress) {

                        $achieved_qty = $work_progress['completed_qty'];

                        if ($achieved_qty != 0) {

                            if ($task['due_date'] > date('Y-m-d')) {

                                $task_duration = $this->getTaskDuration($task['start_date'], date('Y-m-d'), $task['project_id'], $task['clientsite_id']);

                                $target_till_date = $task_duration * $task['daily_target'];

                                if ($achieved_qty < $target_till_date && $task['quantity'] != $achieved_qty) {
                                    $behind_schedule_task = $task;
                                }
                            } else {
                                $task_duration = $this->getTaskDuration($task['start_date'], $task['due_date'], $task['project_id'], $task['clientsite_id']);

                                $target_till_date = $task_duration * $task['daily_target'];

                                if ($achieved_qty < $target_till_date && $task['quantity'] != $achieved_qty) {
                                    $behind_schedule_task = $task;
                                }
                            }
                        } else {
                            $behind_schedule_task = $task;
                        }
                    }
                }

            } else {

                $time_enries = Yii::app()->db->createCommand('SELECT approve_status,completed_percent, entry_date FROM pms_time_entry WHERE tskid = ' . $task['tskid'] . ' and approve_status=1
            ORDER BY updated_date DESC LIMIT 1')->queryRow();
                $complete_percent = 0;

                if (!empty($time_enries)) {
                    $complete_percent = $time_enries['completed_percent'];
                }

                $per_day_percentage = (1 / $task['task_duration']) * 100;

                if ($complete_percent != 0) {
                    if ($task['due_date'] > date('Y-m-d')) {

                        $task_duration = $this->getTaskDuration($task['start_date'], date('Y-m-d'), $task['project_id'], $task['clientsite_id']);

                        $target_till_date = $per_day_percentage * $task_duration;
                        if ($complete_percent < $target_till_date) {
                            $behind_schedule_task = $task;
                        }
                    } else {

                        $task_duration = $this->getTaskDuration($task['start_date'], $task['due_date'], $task['project_id'], $task['clientsite_id']);
                        $target_till_date = $per_day_percentage * $task_duration;

                        if ($complete_percent < $target_till_date) {
                            $behind_schedule_task = $task;
                        }

                    }
                } else {

                    $behind_schedule_task = $task;
                }
            }
            if (count($behind_schedule_task) > 0) {

                return $behind_schedule_task;
            }
        }
    }
    public function saveProjectParticipants($pojectId, $participants)
    {
        $participant_model = new ProjectParticipants;
        if (isset($participants[5]) && ($participants[5])) {
            $participant_model = ProjectParticipants::model()->findByPk($participants[5]);
        }
        $participant_model->contractor_id = '';
        $participant_model->project_id = $pojectId;
        $participant_model->participants = $participants[0];
        $participant_model->participant_initial = $participants[1];
        $participant_model->designation = $participants[2];
        $participant_model->organization_name = $participants[3];
        $participant_model->organization_initial = $participants[4];
        if ($participant_model->isNewRecord) {
            $participant_model->created_by = yii::app()->user->id;
            $participant_model->created_date = date('Y-m-d');
        }
        $participant_model->updated_by = yii::app()->user->id;
        $participant_model->updated_date = date('Y-m-d H:i:s');
        if($participant_model->save()){
            $result=isset($participants['add_participant'])?$participant_model->id:true;
        }else{
            $result= false;
        }
        return $result;
       
    }
    public function actiongetProjectParticipants()
    {

        if (!empty($_REQUEST['project_id'])) {
            $projectId = $_REQUEST['project_id'];
            $participants = ProjectParticipants::model()->findAll(
                array(
                    'select' => 'id, participants', // Select only id and name attributes
                    'condition' => 'project_id = :projectId',
                    'params' => array(':projectId' => $projectId),
                    'order' => 'project_id'
                )
            );
            $projectNO = Projects::model()->findByPk($projectId)->project_no;
            $html = "<option value=''>Choose Participant</option>";
            foreach ($participants as $participant) {
                $participantCount = 0;
                if (!empty($_REQUEST['meeting_id'])) {
                    $participantCount = MeetingParticipants::model()->count(
                        array(
                            "condition" => "meeting_id = {$_REQUEST['meeting_id']} AND participant_id = $participant->id"
                        )
                    );
                }
                $html .= "<option value=" . $participant->id . " " . ($participantCount > 0 ? 'selected' : '') . ">" . $participant->participants . "</option>";
            }
        } else {
            $html = "<option value='5'>Choose Participant</option>";
            $projectNO = '-- N / A --';
        }
        echo json_encode(array('option' => $html, 'project_no' => $projectNO));


    }
    public function actionaddParticipant()
    {

        $projectId = $_POST['project_id'];
        unset($_POST['project_id']);
        $participant = array_values($_POST);
        $participant['add_participant'] =true;

        $result = $this->saveProjectParticipants($projectId, $participant);
        if ($result) {
            echo json_encode(array('status' => 1,'name'=>$_POST['name'],'id'=>$result));
        } else {
            echo json_encode(array('status' => 2, 'error' => 'Cant add new participant'));
        }
    }
    public function actiongetaddTaskModal()
    {
        $meeting_minutes_id = $_GET['meeting_minutes_id'];
        $meeting_minutes_model = MeetingMinutes::model()->with('meeting','meetingMinutesUsers')->findByPk($meeting_minutes_id);
        $condition = 'status = 1 ';
        if (!empty($meeting_minutes_model->meeting->project_id)) {
            $condition .= 'AND project_id = ' . $meeting_minutes_model->meeting->project_id;
        }
        $row_data="{$meeting_minutes_model->points_discussed},{$meeting_minutes_model->meeting_minutes_status},{$meeting_minutes_model->meetingMinutesUsers->user_id},{$meeting_minutes_model->end_date},{$meeting_minutes_id}";
        $data=['row_data'=>$row_data,'project_id'=>$meeting_minutes_model->meeting->project_id,'meeting_id'=>$meeting_minutes_model->meeting_id,'reprted_to'=>$meeting_minutes_model->meetingMinutesUsers->user_id];
        echo $this->renderPartial('_add_task_modal', array('condition' => $condition, 'data' => $data));

    }
}
