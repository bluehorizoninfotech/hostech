<?php

class RoleSettingsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
					),
					array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>array('getsettings'),
						'users'=>array('@'),
					),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            if (Yii::app()->request->isAjaxRequest && isset($_POST['RoleSettings'])) {
    //                        echo "<pre>";
    //                        print_r($_POST['RoleSettings']);
    //                        echo "</pre>";die;

                if ($_POST['RoleSettings']['rset_id'] == 0) {
                    $model = new RoleSettings;
                } else {
                    $rsetid = $_POST['RoleSettings']['rset_id'];
                    $model = RoleSettings::model()->findByPk($rsetid);
                }



                $model->attributes = $_POST['RoleSettings'];



                $weeklyoff2 = $_POST['RoleSettings']['weekly_off2_count'];
                $model->weekly_off2_count = (!empty($weeklyoff2) ? implode(",", $_POST['RoleSettings']['weekly_off2_count']) : "");
                // die;


                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d");

                if ($model->save())
                    echo json_encode(array('status' => 'success', 'message' => 'Category settings saved successfully!'));
                else
                    echo json_encode(array('status' => 'error', 'errors' => $model->getErrors()));
                //print_r($model->getErrors());
            }else{
                $this->layout = false;
		$this->render('rolesettings',array(
			'model'=>$this->loadModel($id),
		));
            }
        }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new RoleSettings;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RoleSettings']))
		{
			$model->attributes=$_POST['RoleSettings'];
//                        echo "<pre>";
//                        print_r($_POST['RoleSettings']);
//                        echo "</pre>";
                        
                        
                        
                        $model->weekly_off2_count = implode(",", $_POST['RoleSettings']['weekly_off2_count']);
                       // die;
                        
                        
                        $model->created_by = Yii::app()->user->id;
                        $model->created_date = date("Y-m-d");
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->rset_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RoleSettings']))
		{
			$model->attributes=$_POST['RoleSettings'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->rset_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('RoleSettings');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new RoleSettings('search');
		$model->unsetAttributes();  // clear any default values

        // new filter back and forth
        $class_name= get_class($model);
        if (isset($_GET[ $class_name])){
           Yii::app()->user->setState('prevfilter' , $_GET[$class_name]);
        }

        if(isset(Yii::app()->user->prevfilter)){
           $model->attributes = Yii::app()->user->prevfilter;
        }

		// end 

		if(isset($_GET['RoleSettings']))
			$model->attributes=$_GET['RoleSettings'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RoleSettings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='role-settings-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
        public function actionGetsettings() {
            if (isset($_GET['roleid']) && !empty($_GET['roleid'])) {
                $roleid = $_GET['roleid'];

                $result = RoleSettings::model()->find(array('condition' => "role_id = '$roleid'"));
                if (!empty($result)) {

                    echo json_encode(array('data' => $result->attributes));
                }
            }
        }
        
        public function actionRolesettings() {
            $model = new RoleSettings;

            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);

            if (Yii::app()->request->isAjaxRequest && isset($_POST['RoleSettings'])) {
    //                        echo "<pre>";
    //                        print_r($_POST['RoleSettings']);
    //                        echo "</pre>";die;

                if ($_POST['RoleSettings']['rset_id'] == 0) {
                    $model = new RoleSettings;
                } else {
                    $rsetid = $_POST['RoleSettings']['rset_id'];
                    $model = RoleSettings::model()->findByPk($rsetid);
                }



                $model->attributes = $_POST['RoleSettings'];



                $weeklyoff2 = $_POST['RoleSettings']['weekly_off2_count'];
                $model->weekly_off2_count = (!empty($weeklyoff2) ? implode(",", $_POST['RoleSettings']['weekly_off2_count']) : "");
                // die;


                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d");

                if ($model->save())
                    echo json_encode(array('status' => 'success', 'message' => 'Category settings saved successfully!'));
                else
                    echo json_encode(array('status' => 'error', 'errors' => $model->getErrors()));
                //print_r($model->getErrors());
            }else {
                $this->layout = false;
                $this->render('rolesettings', array(
                    'model' => $model,
                ));
            }
        }
}
