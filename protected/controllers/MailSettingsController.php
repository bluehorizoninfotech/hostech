<?php

class MailSettingsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	
	public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);
				return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
					),
					array('allow',  // allow all users to perform 'index' and 'view' actions
							'actions'=>array('getusers','savepermissions','maillogview','addsliders','deleteSlider','getImages', 'backlogdays','emailnotification','emailtemplate','accountPermission','setDashboard','sendEmail','approvalTimeEntry'),
							'users'=>array('*'),
						),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}
	
	 

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MailSettings;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$tbl = Yii::app()->db->tablePrefix;
		$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
				. "`{$tbl}user_roles`.`role` "
				. "FROM `{$tbl}users` `t` JOIN {$tbl}user_roles ON {$tbl}user_roles.id= t.user_type "
				. "WHERE status=0  AND pms_access='1' AND t.user_type IN (1,9) ORDER BY user_type,full_name ASC";
				//echo $sql; 
		$users = Yii::app()->db->createCommand($sql)->queryAll();
                
                //echo "<pre>";
                //print_r($users );
                //die;
		if(isset($_POST['MailSettings']))
		{
			$model->attributes=$_POST['MailSettings'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,'users'=>$users,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MailSettings']))
		{
			$model->attributes=$_POST['MailSettings'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MailSettings');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MailSettings('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MailSettings']))
			$model->attributes=$_GET['MailSettings'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	
	public function loadModel($id)
	{
		$model=MailSettings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadmaillogModel($id)
	{
		$model=MailLog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function maillogModelload($id){
		$model=MailLog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mail-settings-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionGetusers() {
        $tbl = Yii::app()->db->tablePrefix;

        if (isset($_POST['type'])) {
            if ($_POST['type'] == "individual") {
				
				
				$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
				. "`{$tbl}user_roles`.`role` "
				. "FROM `{$tbl}users` `t` JOIN {$tbl}user_roles ON {$tbl}user_roles.id= t.user_type "
				. "WHERE status=0  AND pms_access='1' AND t.user_type IN (1,9) ORDER BY user_type,full_name ASC";
				//echo $sql; 
				$users = Yii::app()->db->createCommand($sql)->queryAll();
               // $users = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users WHERE status = 0 AND pms_access = 1")->queryAll();
                echo "<ul class='users-listview' style='list-style:none'>";

                foreach ($users as $user) {
					$userid = $user['userid'];
					$newqry = Yii::app()->db->createCommand("SELECT * FROM {$tbl}mail_settings WHERE user_id = $userid AND status = 1")->queryRow();
					if($newqry != NULL){
                    echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><input type='checkbox' value=" . $user['userid'] . " checked >" . $user['full_name'] . "</li>";
					}else{
						
					 echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><input type='checkbox' value=" . $user['userid'] . " >" . $user['full_name'] . "</li>";	
					}
					}

                echo "</ul>";
                //print_r($users);
            } else if ($_POST['type'] == "profile") {

                $sql = "SELECT * FROM {$tbl}user_roles 
                INNER JOIN {$tbl}users ON {$tbl}users.user_type = {$tbl}user_roles.id  
                WHERE {$tbl}users.status = 0 AND {$tbl}users.pms_access = '1' AND {$tbl}users.user_type IN (1,9) group by {$tbl}user_roles.id
                ";
                $profiles = Yii::app()->db->createCommand($sql)->queryAll();
                
                echo "<ul class='users-listview' style='list-style:none'>";

                foreach ($profiles as $profile) {
					$profileid = $profile['id'];
					
					$count1 = Yii::app()->db->createCommand("SELECT count(*) FROM {$tbl}mail_settings WHERE  role_id = $profileid 
					AND status=1")->queryScalar();
					
					$count2 = Yii::app()->db->createCommand("SELECT count(*) FROM {$tbl}users WHERE  user_type = $profileid 
					AND status=0 && pms_access = '1' ")->queryScalar();
					
					if($count1 == $count2 && $count1 != 0 && $count2 != 0){
					echo "<li class='profileid' name='userid' data-id=" . $profile['id'] . "><input type='checkbox' value=" . $profile['id'] . " checked>" . $profile['role'] . "</li>"; 
					}else{
					echo "<li class='profileid' name='userid' data-id=" . $profile['id'] . "><input type='checkbox' value=" . $profile['id'] . " >" . $profile['role'] . "</li>"; 
					}
				}

                echo "</ul>";
            } else {
                
            }
        }
    }
    
     public function actionSavepermissions() {

        $tbl = Yii::app()->db->tablePrefix;
      
        //with userid
        if (isset($_POST['userids'])){
			
            $userids = $_POST['userids'];
            if (!empty($userids)) {
				
				Yii::app()->db->createCommand("UPDATE  {$tbl}mail_settings SET status = 0")->execute();
				foreach($userids as $userid)
				{	
					//get existing permissions
					$existquery = Yii::app()->db->createCommand("SELECT * FROM {$tbl}mail_settings WHERE user_id = '$userid'")->queryAll();
					if($existquery == NULL)
					{
						$model=new MailSettings;
						$model->user_id = $userid;						
						$sql = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users WHERE userid = '$userid'")->queryRow();
						$model->role_id = $sql['user_type'];
						$model->status = 1;
						$model->save();
					}else{
						//die('success');
						$model =  MailSettings::model()->find(array("condition" => "user_id = '$userid'"));
						$model->user_id = $userid;					
						$sql = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users WHERE userid = '$userid'")->queryRow();
						$model->role_id = $sql['user_type'];
						$model->status = 1;
						$model->save();
						
						
					}
						
				}

                    
        }
        }
		 //with profile id
        if (isset($_POST['profileids'])){
			
			Yii::app()->db->createCommand("UPDATE  {$tbl}mail_settings SET status = 0")->execute();
            $profileids = $_POST['profileids'];
            if (!empty($profileids)) {
				
				$getallusers = Yii::app()->db->createCommand("SELECT role_id FROM {$tbl}mail_settings ")->queryAll();
				//print_r($profileids);
				//print_r($getallusers); die;
				
				foreach($profileids as $profileid)
				{	
					
					$getusers = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users WHERE user_type = $profileid AND status=0 && pms_access = '1' ")->queryAll();
					//print_r($getusers); die;
					foreach($getusers as $user1)
					{
						$newuserid = $user1['userid'];
						$existquery = Yii::app()->db->createCommand("SELECT * FROM {$tbl}mail_settings WHERE user_id = $newuserid AND role_id = $profileid")->queryAll();
						if($existquery == NULL)
						{
							$model=new MailSettings;
							$model->user_id = $newuserid;						
							$model->role_id = $profileid;
							$model->status = 1;
							$model->save();
						}else{
							//die('success');
							$model =  MailSettings::model()->find(array("condition" => "user_id = $newuserid AND role_id = $profileid "));
							$model->user_id = $newuserid;					
							$model->role_id = $profileid;
							$model->status = 1;
							$model->save();
							
							
						}
						
					}
				}
				   
			}
        }
        
        if(empty($_POST['userids']) && empty($_POST['profileids']))
        {
			Yii::app()->db->createCommand("UPDATE  {$tbl}mail_settings SET status = 0")->execute();
			
		}
        
      



       
	}
	
	public function actionGenearlSettings(){
		// echo '<pre>';print_r(Yii::app()->session['menuauthlist']);exit;
		$model=GeneralSettings::model()->find(['condition'=>'id = 1']);
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GeneralSettings']))
		{
			
			
			
			
			$model->attributes=$_POST['GeneralSettings'];

			// $model->experience_year=$_POST['GeneralSettings']['experience_year'];
			// $model->experience_title=$_POST['GeneralSettings']['experience_title'];
			// $model->project_count=$_POST['GeneralSettings']['project_count'];
			// $model->project_title=$_POST['GeneralSettings']['project_title'];
			// $model->customers_count=$_POST['GeneralSettings']['customers_count'];
			// $model->customers_title=$_POST['GeneralSettings']['customers_title'];
			// $model->engineers_count=$_POST['GeneralSettings']['engineers_count'];
			// $model->engineers_title=$_POST['GeneralSettings']['engineers_title'];
			// $model->ongoing_project_count=$_POST['GeneralSettings']['ongoing_project_count'];
			// $model->ongoing_project_title=$_POST['GeneralSettings']['ongoing_project_title'];
			if (isset($_POST['footer_permission'])) {
		     $model->dashboard_status=$_POST['footer_permission'];
			}
			else{
				$model->dashboard_status="";
			}

			if(isset($_POST['GeneralSettings']['onedrive_clientId']))
			{
				$model->onedrive_clientId=$_POST['GeneralSettings']['onedrive_clientId'];
			}
		

			$model->updated_by = Yii::app()->user->id;;
			if($model->save()){
				Yii::app()->user->setFlash('success', 'Successfully updated.');
			}else{
				Yii::app()->user->setFlash('error', 'An error occurred.');
			}
				
		}
		$account_permission = Yii::app()->db->createCommand()
		            ->from('pms_account_permission')
					->queryRow();
     				


		$this->render('_general_settings',array(
			'model'=>$model,
			'account_permission'=>$account_permission
		));
	}
	public function actionmaillogview($id)
	{
		$model=$this->maillogModelload($id);
		
		if($model->mail_type == 'Task Progress Notification'){

			// $data=json_decode($model->message);
			
			// $this->renderPartial('_task_progress_mail',array('data'=>$data),true);
			$this->render('view_maillog',array(
			'model'=>$model,
		));
		}else{
			
			$this->render('view_maillog',array(
			'model'=>$model,
		));
		}
		
		
		
	}

	public function actionaddsliders(){
		$model=GeneralSettings::model()->find(['condition'=>'id = 1']);
		$images = CUploadedFile::getInstancesByName('image');		
		// echo Yii::app()->basePath.'/../uploads/slider/';exit;
		// echo '<pre>';print_r($images);exit;
		foreach ($images as $image => $pic) {
			$files =  CFileHelper::findFiles(Yii::app()->basePath."/../uploads/sliders"); 
			$extension_name = explode('.',$pic->name);
			$count = count($files);
			$count++;
			$file_name = $count.'.'.$pic->getExtensionName();
			// echo '<pre>';print_r($pic->getExtensionName());exit;
			$file = Yii::app()->basePath .'/../uploads/sliders/'.$file_name;
			if(!file_exists($file))
			$pic->saveAs(Yii::app()->basePath  .'/../uploads/sliders/'.$file_name);
						
		}
		$this->redirect(Yii::app()->request->urlReferrer);

	}
	
	public function actionDeleteSlider(){
		$file_name = $_REQUEST['file_name'];
		$file = Yii::app()->basePath  .'/../uploads/sliders/'. $file_name;
			if(file_exists($file)){
					if(unlink($file)){
						echo '1';
					}else{
						echo '2';
					}
			}
		
	}
	public function actionGetImages(){
		$files =  CFileHelper::findFiles(Yii::app()->basePath."/../uploads/sliders"); 
		$url = Yii::app()->request->baseUrl;
		// $file_name = '[';
		foreach($files as $key=>$val){
			if(basename($val) == '.gitkeep'){
				continue;
			}			
			$file_name[]= $url.'/uploads/sliders/'.basename($val).'?'.date('YmdHis');
			$file[] = basename($val);
		}
		
		// $file_name .= ']';
		echo  json_encode($file_name);
    }
    
    public function actionbacklogdays(){
        $model=GeneralSettings::model()->find(['condition'=>'id = 1']);
		if(isset($_POST['GeneralSettings']))
		{
			$model->attributes=$_POST['GeneralSettings'];
			$model->updated_by = Yii::app()->user->id;;
			if($model->save()){
				Yii::app()->user->setFlash('success', 'Successfully updated.');
			}else{
				Yii::app()->user->setFlash('error', 'An error occurred.');
			}
				
		}

		$this->render('_general_settings',array(
			'model'=>$model,
		));
	}
	
	public function actionemailnotification(){
		$interval = (isset($_GET['interval']) ? $_GET['interval'] : 0); 
		$this->render('emailnotification',array(
			//'model'=>$model,
			'interval' => $interval
		));
	}
	public function actionemailtemplate()
	{
		$model=new MailTemplate();
		$this->render('emailtemplate', array('model'=>$model
           
        ));
	}
	public function actionaccountPermission()
	{
		$id=$_POST['id'];
		
		$result = Yii::app()->db->createCommand()
		            ->from('pms_account_permission')
					 ->queryRow();
	if($result)
	{
		$status=$result['status'];
		if($status==1)
		{
			$status=0;
		}
		else
		{
			$status=1;
		}
   					 
		$date=date('Y-m-d H:i:s');
		$sql="UPDATE pms_account_permission SET status=".$status.",updated_by=".Yii::app()->user->id.",updated_date= '$date' ";
		Yii::app()->db->createCommand($sql)->execute();
	}
	else
	{
		$sql = "INSERT INTO pms_account_permission (status,created_by,created_date) VALUES (:id,:created_by,:created_date)";
                $parameters = array(":id" => $id, ":created_by" => Yii::app()->user->id, ":created_date" => date('Y-m-d H:i:s'));
                Yii::app()->db->createCommand($sql)->execute($parameters);
	}
	
	echo json_encode( array('status' => '1'));
	}
	
public function actionsetDashboard()
{
	$value= $_POST['val'];
	$model=GeneralSettings::model()->find(['condition'=>'id = 1']);
    $model->dashboard_type=$value;
	if($model->save()){
		echo json_encode( array('status' => '1'));
	}
	else
	{
		echo json_encode( array('status' => '0'));
	}
}
public function actionsendEmail()
{
	$email= $_POST['GeneralSettings']['email'];



	$app_root = YiiBase::getPathOfAlias('webroot');
	$theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
	$logo = $theme_asset_url . 'default-logo.png';
	if (file_exists($app_root . "/themes/assets/logo.png")) {
		$logo = $theme_asset_url . 'logo.png';
	}
	$task_model=Tasks::model()->findByPk(175);

	if($email != "")
	{
		try
		{
			$model2 = new MailLog;
		    $mail_data ="Test Mail";
			// $mail_data=$this->renderPartial('_mail_body', array('logo' => $logo,'model'=>$task_model,'id'=>$task_model->tskid), true);

			// echo $mail_data;exit;
			$mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
            $mail = new JPhpMailer(true);
			$today = date('Y-m-d H:i:s');
			$bodyContent = $mail_data;
			$mail->IsSMTP();
			$mail->Port = $mail_model['smtp_port']; 
			$mail->Host = $mail_model['smtp_host'];
			$mail->SMTPSecure = $mail_model['smtp_secure'];
			$mail->SMTPAuth = $mail_model['smtp_auth'];
			$mail->Username = $mail_model['smtp_username'];
			$mail->Password = $mail_model['smtp_password'];
			$mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
			$mail->Subject = "Test Mail";
			$mail->addAddress($email);
			$mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
			$mail_send_byJSON = json_encode($mail_send_by);
            $subject = $mail->Subject;
            $model2->send_to = $email;
            $model2->send_by = $mail_send_byJSON;
            $model2->send_date = $today;
            $model2->message = htmlentities($bodyContent);
            $model2->description = $mail->Subject;
            $model2->created_date = $today;
            $model2->created_by = Yii::app()->user->getId();
            $model2->mail_type = $subject;
            $mail->isHTML(true);
            $mail->MsgHTML($bodyContent);
            $mail->Body = $bodyContent;
            $model2->sendemail_status = 0;
			if ($mail->send()) {
				$model2->sendemail_status = 1;
				$response_msg="Mail sent Successfully";
				$msg='success';
			}
			
			if (!$model2->save()) {
				$response_msg=$model2->getErrors();
				$msg='error';
			}
			
		}
		catch(phpmailerException $e){
			$response_msg=$e->getMessage();
			$msg='error';
			if (!$model2->save()) {
				$response_msg=$model2->getErrors();
				$msg='error';
			}
		}
		Yii::app()->user->setFlash($msg, $response_msg);
		$this->redirect(array('genearlSettings'));
		//here
	}
}
	public function actionapprovalTimeEntry(){
		$value= $_POST['add_time_entry'];
		$model=GeneralSettings::model()->find(['condition'=>'id = 1']);
		$model->add_time_entry_approval=$value;
		
		if($model->save()){
			Yii::app()->user->setFlash('success', 'Time Entry Approval Successfully updated.');
		}else{
			Yii::app()->user->setFlash('error', 'An error occurred.');
		}
		$this->redirect(array('genearlSettings'));


	}

}