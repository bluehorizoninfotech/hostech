<?php

class ManualentryDateController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
    public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}
	
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ManualentryDate;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ManualentryDate']))
		{
			$model->attributes=$_POST['ManualentryDate'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            
           
		$model=$this->loadModel($id);
                
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
                $tbl = Yii::app()->db->tablePrefix;  
                $getsite = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from("{$tbl}manualentry_date")
                            ->where('id=:id',
                                    array(':id'=>$id))    
                            ->queryRow();
                $site = $getsite['site_id'];
                $date = $getsite['date'];
                $users = Yii::app()->db->createCommand()
                            ->select("{$tbl}usersite.user_id,{$tbl}usersite.site_id,{$tbl}users.first_name,{$tbl}users.last_name,{$tbl}manualentry_date.date")
                            ->from("{$tbl}usersite")
                            ->join("{$tbl}users", "{$tbl}users.userid={$tbl}usersite.user_id")
                            ->join("{$tbl}manualentry_date", "{$tbl}manualentry_date.site_id={$tbl}usersite.site_id")
                            ->where("{$tbl}usersite.site_id=:site AND {$tbl}manualentry_date.date=:date",
                                    array(':site'=>$site,':date'=>$date))    
                            ->queryAll();
                         
		if(isset($_POST['ManualentryDate']))
		{
                      // echo "<pre>";
                      // print_r($_POST); die;
                        $user_id = $_POST['user_id'];
                        
                        $first_punch = $_POST['first_punch'];
                        $last_punch = $_POST['last_punch'];
                        $total_out_time = $_POST['total_out_time'];
                        $entry_id = (isset($_POST['entry_id']) ? $_POST['entry_id'] : "");
                        
                            for ($i=0; $i<sizeof($user_id); $i++)
				{
                                    $t1 = date( "H:i:s", strtotime( $first_punch[$i] ) );
                                    $t1 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t1);
                                    sscanf($t1, "%d:%d:%d", $hours, $minutes, $seconds);
                                    $time_seconds1 = $hours * 3600 + $minutes * 60 + $seconds;
 
                                    $t2 = date( "H:i:s", strtotime( $last_punch[$i] ) );
                                    $t2 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t2);
                                    sscanf($t2, "%d:%d:%d", $hours, $minutes, $seconds);
                                    $time_seconds2 = $hours * 3600 + $minutes * 60 + $seconds;
                                    
                                    $t3 = date( "H:i:s", strtotime( $total_out_time[$i] ) );
                                    $t3 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t3);
                                    sscanf($t3, "%d:%d:%d", $hours, $minutes, $seconds);
                                    $time_seconds3 = $hours * 3600 + $minutes * 60 + $seconds;
                                    
                                    $avg = ($time_seconds1+$time_seconds2)/2;
                                    $middle = $avg+$time_seconds3;
                                    
                                    $diff = $time_seconds2-$time_seconds1;
                                   // die($diff);
                                    
                                    $time1 = gmdate("H:i:s", $time_seconds1);
                                    $time2 = gmdate("H:i:s", $avg);
                                    $time3 = gmdate("H:i:s", $middle);
                                    $time4 = gmdate("H:i:s", $time_seconds2);
                                    
                                    //echo $time1.'/'.$time2.'/'.$time3.'/'.$time4; die;
                                    $result = Yii::app()->db->createCommand()
                                        ->select("device_id")
                                        ->from("{$tbl}punching_devices")
                                        ->where("{$tbl}punching_devices.site_id=:site_id AND {$tbl}punching_devices.device_name=:device_name",
                                                array(':site_id' => $site,':device_name' => 'No Device'))    
                                        ->queryAll();
                                    
                                    if(!is_null($result))
                                    {
                                        $device_id = $result[0]['device_id'];
                                        $qry = Yii::app()->db->createCommand()
                                            ->select("*")
                                            ->from("{$tbl}device_accessids")
                                            ->where("{$tbl}device_accessids.userid=:userid AND {$tbl}device_accessids.deviceid=:deviceid AND {$tbl}device_accessids.accesscard_id=:accesscard_id",
                                                    array(':userid' => $user_id[$i],':deviceid' => $device_id,':accesscard_id' => $user_id[$i]) )  
                                            ->queryAll();
                                        if(empty($qry)){

                                            $new=new DeviceAccessids;
                                            $new->userid = $user_id[$i];
                                            $new->deviceid = $device_id;
                                            $new->accesscard_id = $user_id[$i];
                                            $new->save(); 
                                        }
                                    
                                    
                                    }else{
                                       
                                        $devicemodel=new PunchingDevices;
                                        
                                        
                                                $punchmodel = new PunchingDevices;                                                
                                                $criteria=new CDbCriteria;
                                                $criteria->select='max(device_id) AS device_id';
                                                $row = $punchmodel->model()->find($criteria);
                                                $maxdeviceid = $row['device_id'];
                                                if($maxdeviceid >= 1000 )
                                                {
												$devicemodel->device_id = $maxdeviceid+1;  
                                                }else{
												$devicemodel->device_id = 1000;           
                                                }
                                        
                                       
                                        $devicemodel->device_name = 'No Device';
                                        $devicemodel->shot_name = 'No Device';
                                        $devicemodel->site_id = $site;
                                        $devicemodel->start_date = date('Y-m-d');
                                        $devicemodel->nodevice_status = 1;
                                        
                                        
                                        
                                        $devicemodel->save();
                                            
                                            
                                        $device_id = $devicemodel->device_id;
                                        $qry = Yii::app()->db->createCommand()
                                            ->select("*")
                                            ->from("{$tbl}device_accessids")
                                            ->where("{$tbl}device_accessids.userid=:userid AND {$tbl}device_accessids.deviceid=:deviceid AND {$tbl}device_accessids.accesscard_id=:accesscard_id",
                                                    array(':userid' => $user_id[$i],':deviceid' => $device_id,':accesscard_id' => $user_id[$i]) )  
                                            ->queryAll();
                                        if(empty($qry)){

                                            $new=new DeviceAccessids;
                                            $new->userid = $user_id[$i];
                                            $new->deviceid = $device_id;
                                            $new->accesscard_id = $user_id[$i];
                                            $new->save(); 
                                        }
                                    }
                                    
                                   
                                    
                                    if($entry_id[$i] == NULL)
                                    {
                                        $newmodel=new Manualentry;
                                        $newmodel->site_id = $site;
                                        $newmodel->user_id = $user_id[$i];
                                        $newmodel->first_punch = $first_punch[$i];
                                        $newmodel->last_punch = $last_punch[$i];
                                        $newmodel->total_out_time = $total_out_time[$i];
                                        $newmodel->added_date = $date;
                                        $newmodel->created_by = yii::app()->user->id;
                                        $newmodel->created_date = date('Y-m-d');
                                        $newmodel->updated_by = yii::app()->user->id;
                                        $newmodel->updated_date = date('Y-m-d');
                                        $newmodel->manual_date_id = $id;
                                        $newmodel->save();
                                        $last_id = Yii::app()->db->getLastInsertID();   
                                        

                                        /* inserting entries to accesslog */

                                        for ($j=1; $j<5; $j++)
                                        {
                                        $model1=new Accesslog;
                                        $model1->logid     =   '';
                                        $model1->empid      =   $user_id[$i];
                                        if($j==1)
                                        {
                                        $model1->log_time   =   $date.' '.$time1;
                                        }
                                        if($j==2)
                                        {
                                        $model1->log_time   =   $date.' '.$time2;
                                        }
                                        if($j==3)
                                        {
                                        $model1->log_time   =   $date.' '.$time3;
                                        }
                                        if($j==4)
                                        {
                                        $model1->log_time   =   $date.' '.$time4;
                                        }
                                        $model1->status     =   0;
                                        $model1->device_id  =   $device_id;
                                        $model1->manual_entry_status = $last_id;
                                        $model1->save();
                                        }


                                    }else{
                                       
                                    $newmodel = Manualentry::model()->findByPk($entry_id[$i]);
                                    $newmodel->site_id = $site;
                                    $newmodel->user_id = $user_id[$i];
                                    $newmodel->first_punch = $first_punch[$i];
                                    $newmodel->last_punch = $last_punch[$i];
                                    $newmodel->total_out_time = $total_out_time[$i];
                                    $newmodel->added_date = $date;
                                    $newmodel->updated_by = yii::app()->user->id;
                                    $newmodel->updated_date = date('Y-m-d');
                                    $newmodel->manual_date_id = $id;
                                    $newmodel->save();
                                    $last_id = $entry_id[$i];
                                    
                                  
                                    $newarr = Yii::app()->db->createCommand()
                                    ->select("*")
                                    ->from("{$tbl}accesslog")
                                    ->where('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id[$i].' AND manual_entry_status = '.$last_id)    
                                    ->queryAll();
                                    
                                    
                                    if(!empty($newarr))
                                    {
                                            Accesslog::model()->deleteAll('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id[$i].' AND manual_entry_status ='.$last_id);
                                           
                                           
                                               
                                            for ($j=1; $j<5; $j++)
                                            {
                                            $model1=new Accesslog;
                                            $model1->logid     =   NULL;
                                            $model1->empid      =   $user_id[$i];
                                            if($j==1)
                                            {
                                            $model1->log_time   =   $date.' '.$time1;
                                            }
                                            if($j==2)
                                            {
                                            $model1->log_time   =   $date.' '.$time2;
                                            }
                                            if($j==3)
                                            {
                                            $model1->log_time   =   $date.' '.$time3;
                                            }
                                            if($j==4)
                                            {
                                            $model1->log_time   =   $date.' '.$time4;
                                            }
                                            $model1->status     =   0;
                                            $model1->device_id  =   $device_id;
                                            $model1->manual_entry_status = $last_id;
                                            $model1->save();
                                            }
                                    }else{
                                        
                                       for ($j=1; $j<5; $j++)
                                       {
                                       $model1=new Accesslog;
                                       $model1->logid     =  NULL;
                                       $model1->empid      =   $user_id[$i];
                                       if($j==1)
                                        {
                                        $model1->log_time   =   $date.' '.$time1;
                                        }
                                        if($j==2)
                                        {
                                        $model1->log_time   =   $date.' '.$time2;
                                        }
                                        if($j==3)
                                        {
                                        $model1->log_time   =   $date.' '.$time3;
                                        }
                                        if($j==4)
                                        {
                                        $model1->log_time   =   $date.' '.$time4;
                                        }
                                       $model1->status     =   0;
                                       $model1->device_id  =   $device_id;
                                       $model1->manual_entry_status = $last_id;
                                       $model1->save();
                                       }
                                    
                                        
                                    }


                                  }

                                    
                            }
                            $this->redirect(array('index'));
		}
                
                $entries =   Yii::app()->db->createCommand()
                                    ->select("*")
                                    ->from("{$tbl}manualentry")
                                    ->join("{$tbl}users", "{$tbl}users.userid={$tbl}manualentry.user_id")
                                    ->join("{$tbl}clientsite", "{$tbl}clientsite.ID={$tbl}manualentry.site_id")
                                    ->where('manual_date_id = '.$id)    
                                    ->queryAll();
               // echo "<pre>";
               // print_r($entries); die;
		$this->render('update',array(
			'model'=>$model,'users'=>$users,'entries'=>$entries,
		));
	}
        
    public function actionMobileUpdate($id)
    {
            
           
        $model=$this->loadModel($id);
                
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
                $tbl = Yii::app()->db->tablePrefix;  
                $getsite = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from("{$tbl}manualentry_date")
                            ->where('id=:id',
                                    array(':id'=>$id))    
                            ->queryRow();
                $site = $getsite['site_id'];
                $date = $getsite['date'];
/* //start code change for assigned date condition */
                $todaysdate = date('Y-m-d');
                $users = Yii::app()->db->createCommand()
                           ->select("{$tbl}usersite.user_id,{$tbl}usersite.site_id,{$tbl}users.first_name,{$tbl}users.last_name,{$tbl}manualentry_date.date")
                           ->from("{$tbl}usersite")
                           ->join("{$tbl}users", "{$tbl}users.userid={$tbl}usersite.user_id")
                           ->join("{$tbl}manualentry_date", "{$tbl}manualentry_date.site_id={$tbl}usersite.site_id")
                           ->where("{$tbl}usersite.site_id=:site AND {$tbl}manualentry_date.date=:date AND {$tbl}usersite.assigned_date <= :todaysdate",
                                   array(':site'=>$site,':date'=>$date ,':todaysdate'=>$todaysdate))    
                           ->queryAll();
/* //end code change for assigned date condition */             
        if(isset($_POST['ManualentryDate']))
        {
                      // echo "<pre>";
                      // print_r($_POST); die;
                        $user_id = $_POST['user_id'];
                        
                        $first_punch = $_POST['first_punch'];
                        $last_punch = $_POST['last_punch'];
                        $total_out_time = $_POST['total_out_time'];
                        $entry_id = (isset($_POST['entry_id']) ? $_POST['entry_id'] : "");
                        
                            for ($i=0; $i<sizeof($user_id); $i++)
                {
                                    $t1 = date( "H:i:s", strtotime( $first_punch[$i] ) );
                                    $t1 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t1);
                                    sscanf($t1, "%d:%d:%d", $hours, $minutes, $seconds);
                                    $time_seconds1 = $hours * 3600 + $minutes * 60 + $seconds;
 
                                    $t2 = date( "H:i:s", strtotime( $last_punch[$i] ) );
                                    $t2 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t2);
                                    sscanf($t2, "%d:%d:%d", $hours, $minutes, $seconds);
                                    $time_seconds2 = $hours * 3600 + $minutes * 60 + $seconds;
                                    
                                    $t3 = date( "H:i:s", strtotime( $total_out_time[$i] ) );
                                    $t3 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t3);
                                    sscanf($t3, "%d:%d:%d", $hours, $minutes, $seconds);
                                    $time_seconds3 = $hours * 3600 + $minutes * 60 + $seconds;
                                    
                                    $avg = ($time_seconds1+$time_seconds2)/2;
                                    $middle = $avg+$time_seconds3;
                                    
                                    $diff = $time_seconds2-$time_seconds1;
                                   // die($diff);
                                    
                                    $time1 = gmdate("H:i:s", $time_seconds1);
                                    $time2 = gmdate("H:i:s", $avg);
                                    $time3 = gmdate("H:i:s", $middle);
                                    $time4 = gmdate("H:i:s", $time_seconds2);
                                    
                                    //echo $time1.'/'.$time2.'/'.$time3.'/'.$time4; die;
                                    $result = Yii::app()->db->createCommand()
                                        ->select("device_id")
                                        ->from("{$tbl}punching_devices")
                                        ->where("{$tbl}punching_devices.site_id=:site_id AND {$tbl}punching_devices.device_name=:device_name",
                                                array(':site_id' => $site,':device_name' => 'No Device'))    
                                        ->queryAll();
                                        
                                  
                                   if(!is_null($result))
                                    {
                                        $device_id = $result[0]['device_id'];
                                        $qry = Yii::app()->db->createCommand()
                                            ->select("*")
                                            ->from("{$tbl}device_accessids")
                                            ->where("{$tbl}device_accessids.userid=:userid AND {$tbl}device_accessids.deviceid=:deviceid AND {$tbl}device_accessids.accesscard_id=:accesscard_id",
                                                    array(':userid' => $user_id[$i],':deviceid' => $device_id,':accesscard_id' => $user_id[$i]) )  
                                            ->queryAll();
                                        if(empty($qry)){

                                            $new=new DeviceAccessids;
                                            $new->userid = $user_id[$i];
                                            $new->deviceid = $device_id;
                                            $new->accesscard_id = $user_id[$i];
                                            $new->save(); 
                                        }
                                    
                                    
                                    }else{
                                       
												$devicemodel=new PunchingDevices;
                                        
                                        
                                                $punchmodel = new PunchingDevices;
                                                $criteria=new CDbCriteria;
                                                $criteria->select='max(device_id) AS device_id';
                                                $row = $punchmodel->model()->find($criteria);
                                                $maxdeviceid = $row['device_id'];
                                                if($maxdeviceid >= 1000 )
                                                {
												$devicemodel->device_id = $maxdeviceid+1;  
                                                }else{
												$devicemodel->device_id = 1000;           
                                                }
                                        
                                        
                                        $devicemodel->device_name = 'No Device';
                                        $devicemodel->shot_name = 'No Device';
                                        $devicemodel->site_id = $site;
                                        $devicemodel->start_date = date('Y-m-d');
                                        $devicemodel->nodevice_status = 1;
                                        $devicemodel->save();
                                            
                                            
                                        $device_id = $devicemodel->device_id;
                                        $qry = Yii::app()->db->createCommand()
                                            ->select("*")
                                            ->from("{$tbl}device_accessids")
                                            ->where("{$tbl}device_accessids.userid=:userid AND {$tbl}device_accessids.deviceid=:deviceid AND {$tbl}device_accessids.accesscard_id=:accesscard_id",
                                                    array(':userid' => $user_id[$i],':deviceid' => $device_id,':accesscard_id' => $user_id[$i]) )  
                                            ->queryAll();
                                        if(empty($qry)){

                                            $new=new DeviceAccessids;
                                            $new->userid = $user_id[$i];
                                            $new->deviceid = $device_id;
                                            $new->accesscard_id = $user_id[$i];
                                            $new->save(); 
                                        }
                                    }
                                    
                                    
                                    
                                    if($entry_id[$i] == NULL)
                                    {
                                        $newmodel=new Manualentry;
                                        $newmodel->site_id = $site;
                                        $newmodel->user_id = $user_id[$i];
                                        $newmodel->first_punch = $first_punch[$i];
                                        $newmodel->last_punch = $last_punch[$i];
                                        $newmodel->total_out_time = $total_out_time[$i];
                                        $newmodel->added_date = $date;
                                        $newmodel->created_by = yii::app()->user->id;
                                        $newmodel->created_date = date('Y-m-d');
                                        $newmodel->updated_by = yii::app()->user->id;
                                        $newmodel->updated_date = date('Y-m-d');
                                        $newmodel-> manual_date_id = $id;
                                        $newmodel->save();
                                        $last_id = Yii::app()->db->getLastInsertID();   
                                        

                                        /* inserting entries to accesslog */

                                        for ($j=1; $j<5; $j++)
                                        {
                                        $model1=new Accesslog;
                                        $model1->logid     =   '';
                                        $model1->empid      =   $user_id[$i];
                                        if($j==1)
                                        {
                                        $model1->log_time   =   $date.' '.$time1;
                                        }
                                        if($j==2)
                                        {
                                        $model1->log_time   =   $date.' '.$time2;
                                        }
                                        if($j==3)
                                        {
                                        $model1->log_time   =   $date.' '.$time3;
                                        }
                                        if($j==4)
                                        {
                                        $model1->log_time   =   $date.' '.$time4;
                                        }
                                        $model1->status     =   0;
                                        $model1->device_id  =   $device_id;
                                        $model1->manual_entry_status = $last_id;
                                        $model1->save();
                                        }


                                    }else{
                                       
                                    $newmodel = Manualentry::model()->findByPk($entry_id[$i]);
                                    $newmodel->site_id = $site;
                                    $newmodel->user_id = $user_id[$i];
                                    $newmodel->first_punch = $first_punch[$i];
                                    $newmodel->last_punch = $last_punch[$i];
                                    $newmodel->total_out_time = $total_out_time[$i];
                                    $newmodel->added_date = $date;
                                    $newmodel->updated_by = yii::app()->user->id;
                                    $newmodel->updated_date = date('Y-m-d');
                                    $newmodel->manual_date_id = $id;
                                    $newmodel->save();
                                    $last_id = $entry_id[$i];
                                    
                                  
                                    $newarr = Yii::app()->db->createCommand()
                                    ->select("*")
                                    ->from("{$tbl}accesslog")
                                    ->where('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id[$i].' AND manual_entry_status = '.$last_id)    
                                    ->queryAll();
                                    
                                    if(!empty($newarr))
                                    {
                                            Accesslog::model()->deleteAll('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id[$i].' AND manual_entry_status ='.$last_id);
                                           
                                           
                                               
                                            for ($j=1; $j<5; $j++)
                                            {
                                            $model1=new Accesslog;
                                            $model1->logid     =   NULL;
                                            $model1->empid      =   $user_id[$i];
                                            if($j==1)
                                            {
                                            $model1->log_time   =   $date.' '.$time1;
                                            }
                                            if($j==2)
                                            {
                                            $model1->log_time   =   $date.' '.$time2;
                                            }
                                            if($j==3)
                                            {
                                            $model1->log_time   =   $date.' '.$time3;
                                            }
                                            if($j==4)
                                            {
                                            $model1->log_time   =   $date.' '.$time4;
                                            }
                                            $model1->status     =   0;
                                            $model1->device_id  =   $device_id;
                                            $model1->manual_entry_status = $last_id;
                                            $model1->save();
                                            }
                                    }else{
                                        
                                       for ($j=1; $j<5; $j++)
                                       {
                                       $model1=new Accesslog;
                                       $model1->logid     =  NULL;
                                       $model1->empid      =   $user_id[$i];
                                       if($j==1)
                                        {
                                        $model1->log_time   =   $date.' '.$time1;
                                        }
                                        if($j==2)
                                        {
                                        $model1->log_time   =   $date.' '.$time2;
                                        }
                                        if($j==3)
                                        {
                                        $model1->log_time   =   $date.' '.$time3;
                                        }
                                        if($j==4)
                                        {
                                        $model1->log_time   =   $date.' '.$time4;
                                        }
                                       $model1->status     =   0;
                                       $model1->device_id  =   $device_id;
                                       $model1->manual_entry_status = $last_id;
                                       $model1->save();
                                       }
                                    
                                        
                                    }


                                  }

                                    
                            }
                            $this->redirect(array('index'));
        }
                
                $entries =   Yii::app()->db->createCommand()
                                    ->select("*")
                                    ->from("{$tbl}manualentry")
                                    ->join("{$tbl}users", "{$tbl}users.userid={$tbl}manualentry.user_id")
                                    ->join("{$tbl}clientsite", "{$tbl}clientsite.ID={$tbl}manualentry.site_id")
                                    ->where('manual_date_id = '.$id)    
                                    ->queryAll();
               // echo "<pre>";
               // print_r($entries); die;
        $this->render('mobile_form',array(
            'model'=>$model,'users'=>$users,'entries'=>$entries,
        ));
    }  
    public function actionSaveindividualdata($id)
    {
		/*  get site name and date */
		
				$model=$this->loadModel($id);
                
                $tbl = Yii::app()->db->tablePrefix;  
                $getsite = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from("{$tbl}manualentry_date")
                            ->where('id=:id',
                                    array(':id'=>$id))    
                            ->queryRow();
                $site = $getsite['site_id'];
                $date = $getsite['date'];

		/*  get site name and date ends */
		
		$user_id = (isset($_POST['userId']) ? $_POST['userId'] : "");
		$first_punch = (isset($_POST['first']) ? $_POST['first'] : "");
		$last_punch = (isset($_POST['last']) ? $_POST['last'] : "");
		$total_out_time = (isset($_POST['total']) ? $_POST['total'] : "");
		$entry_id = (isset($_POST['entryId']) ? $_POST['entryId'] : "");
		
		
			$t1 = date( "H:i:s", strtotime( $first_punch ) );
			$t1 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t1);
			sscanf($t1, "%d:%d:%d", $hours, $minutes, $seconds);
			$time_seconds1 = $hours * 3600 + $minutes * 60 + $seconds;

			$t2 = date( "H:i:s", strtotime( $last_punch ) );
			$t2 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t2);
			sscanf($t2, "%d:%d:%d", $hours, $minutes, $seconds);
			$time_seconds2 = $hours * 3600 + $minutes * 60 + $seconds;
			
			$t3 = date( "H:i:s", strtotime( $total_out_time ) );
			$t3 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $t3);
			sscanf($t3, "%d:%d:%d", $hours, $minutes, $seconds);
			$time_seconds3 = $hours * 3600 + $minutes * 60 + $seconds;
			
			$avg = ($time_seconds1+$time_seconds2)/2;
			$middle = $avg+$time_seconds3;
			
			$diff = $time_seconds2-$time_seconds1;
			
			$time1 = gmdate("H:i:s", $time_seconds1);
			$time2 = gmdate("H:i:s", $avg);
			$time3 = gmdate("H:i:s", $middle);
			$time4 = gmdate("H:i:s", $time_seconds2);
			
			
			$result = Yii::app()->db->createCommand()
			->select("device_id")
			->from("{$tbl}punching_devices")
			->where("{$tbl}punching_devices.site_id=:site_id AND {$tbl}punching_devices.device_name=:device_name",
					array(':site_id' => $site,':device_name' => 'No Device'))    
			->queryAll();
			
			
			
            if(!empty($result))
			{
				
				$device_id = $result[0]['device_id'];
				$qry = Yii::app()->db->createCommand()
					->select("*")
					->from("{$tbl}device_accessids")
					->where("{$tbl}device_accessids.userid=:userid AND {$tbl}device_accessids.deviceid=:deviceid AND {$tbl}device_accessids.accesscard_id=:accesscard_id",
							array(':userid' => $user_id,':deviceid' => $device_id,':accesscard_id' => $user_id) )  
					->queryAll();
				if(empty($qry)){

					$new=new DeviceAccessids;
					$new->userid = $user_id;
					$new->deviceid = $device_id;
					$new->accesscard_id = $user_id;
					$new->save(); 
				}
			
			
			}else{
                                       
				$devicemodel=new PunchingDevices;
                       
				$punchmodel = new PunchingDevices;
				$criteria=new CDbCriteria;
				$criteria->select='max(device_id) AS device_id';
				$row = $punchmodel->model()->find($criteria);
				$maxdeviceid = $row['device_id'];
				if($maxdeviceid >= 1000 )
				{
				$devicemodel->device_id = $maxdeviceid+1;  
				}else{
				$devicemodel->device_id = 1000;           
				}
                                        
                     
                                        
				$devicemodel->device_name = 'No Device';
				$devicemodel->shot_name = 'No Device';
				$devicemodel->site_id = $site;
				$devicemodel->start_date = date('Y-m-d');
				$devicemodel->nodevice_status = 1;
				
				$devicemodel->save();
					
					
				$device_id = $devicemodel->device_id;
				$qry = Yii::app()->db->createCommand()
					->select("*")
					->from("{$tbl}device_accessids")
					->where("{$tbl}device_accessids.userid=:userid AND {$tbl}device_accessids.deviceid=:deviceid AND {$tbl}device_accessids.accesscard_id=:accesscard_id",
							array(':userid' => $user_id,':deviceid' => $device_id,':accesscard_id' => $user_id) )  
					->queryAll();
				if(empty($qry)){

					$new=new DeviceAccessids;
					$new->userid = $user_id;
					$new->deviceid = $device_id;
					$new->accesscard_id = $user_id;
					$new->save(); 
				}
				}
				
				
				
				
				
								if($entry_id == NULL)
                                    {
                                        $newmodel=new Manualentry;
                                        $newmodel->site_id = $site;
                                        $newmodel->user_id = $user_id;
                                        $newmodel->first_punch = $first_punch;
                                        $newmodel->last_punch = $last_punch;
                                        $newmodel->total_out_time = $total_out_time;
                                        $newmodel->added_date = $date;
                                        $newmodel->created_by = yii::app()->user->id;
                                        $newmodel->created_date = date('Y-m-d');
                                        $newmodel->updated_by = yii::app()->user->id;
                                        $newmodel->updated_date = date('Y-m-d');
                                        $newmodel->manual_date_id = $id;
                                        $newmodel->save();
                                        $last_id = Yii::app()->db->getLastInsertID();   
                                        

                                        /* inserting entries to accesslog */
										if($total_out_time == '00:00'){
											
										
											for ($j=1; $j<3; $j++)
											{
											$model1=new Accesslog;
											$model1->logid     =   '';
											$model1->empid      =   $user_id;
											if($j==1)
											{
											$model1->log_time   =   $date.' '.$time1;
											}
											if($j==2)
											{
											$model1->log_time   =   $date.' '.$time4;
											}
											
											$model1->status     =   0;
											$model1->device_id  =   $device_id;
											$model1->manual_entry_status = $last_id;
											$model1->save();	   
											}
											
										}else{
											
											for ($j=1; $j<5; $j++)
											{
											$model1=new Accesslog;
											$model1->logid     =   '';
											$model1->empid      =   $user_id;
											if($j==1)
											{
											$model1->log_time   =   $date.' '.$time1;
											}
											if($j==2)
											{
											$model1->log_time   =   $date.' '.$time2;
											}
											if($j==3)
											{
											$model1->log_time   =   $date.' '.$time3;
											}
											if($j==4)
											{
											$model1->log_time   =   $date.' '.$time4;
											}
											$model1->status     =   0;
											$model1->device_id  =   $device_id;
											$model1->manual_entry_status = $last_id;
											$model1->save();	   
											}
											
										}
                                        
                                        echo json_encode(array('response' => 'success', 'f_punch' => $first_punch , 'l_punch' => $last_punch, 't_out' =>$total_out_time, 't_entry_id'=> $last_id ));	
										//}else{
										//echo json_encode(array('response' => 'fail'));		
										//}


                                    }else{
                                       
                                    $newmodel = Manualentry::model()->findByPk($entry_id);
                                    $newmodel->site_id = $site;
                                    $newmodel->user_id = $user_id;
                                    $newmodel->first_punch = $first_punch;
                                    $newmodel->last_punch = $last_punch;
                                    $newmodel->total_out_time = $total_out_time;
                                    $newmodel->added_date = $date;
                                    $newmodel->updated_by = yii::app()->user->id;
                                    $newmodel->updated_date = date('Y-m-d');
                                    $newmodel->manual_date_id = $id;
                                    $newmodel->save();
                                    $last_id = $entry_id;
                                    
                                  
                                    $newarr = Yii::app()->db->createCommand()
                                    ->select("*")
                                    ->from("{$tbl}accesslog")
                                    ->where('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id.' AND manual_entry_status = '.$last_id)    
                                    ->queryAll();
                                    
                                    if(!empty($newarr))
                                    {
                                            Accesslog::model()->deleteAll('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id.' AND manual_entry_status ='.$last_id);
                                           
                                           
                                            if($total_out_time == '00:00'){
											
										
													for ($j=1; $j<3; $j++)
													{
													$model1=new Accesslog;
													$model1->logid     =   '';
													$model1->empid      =   $user_id;
													if($j==1)
													{
													$model1->log_time   =   $date.' '.$time1;
													}
													if($j==2)
													{
													$model1->log_time   =   $date.' '.$time4;
													}
													
													$model1->status     =   0;
													$model1->device_id  =   $device_id;
													$model1->manual_entry_status = $last_id;
													$model1->save();	   
													}
											
										 }else{ 
											   
                                            for ($j=1; $j<5; $j++)
                                            {
                                            $model1=new Accesslog;
                                            $model1->logid     =   NULL;
                                            $model1->empid      =   $user_id;
                                            if($j==1)
                                            {
                                            $model1->log_time   =   $date.' '.$time1;
                                            }
                                            if($j==2)
                                            {
                                            $model1->log_time   =   $date.' '.$time2;
                                            }
                                            if($j==3)
                                            {
                                            $model1->log_time   =   $date.' '.$time3;
                                            }
                                            if($j==4)
                                            {
                                            $model1->log_time   =   $date.' '.$time4;
                                            }
                                            $model1->status     =   0;
                                            $model1->device_id  =   $device_id;
                                            $model1->manual_entry_status = $last_id;
                                            $model1->save();
                                            }
                                            
										}
                                            
                                            echo json_encode(array('response' => 'success', 'f_punch' => $first_punch , 'l_punch' => $last_punch, 't_out' =>$total_out_time, 't_entry_id'=> $last_id));	
                                    }else{
                                       
                                       
                                       
                                       if($total_out_time == '00:00'){
											
											
												for ($j=1; $j<3; $j++)
												{
												$model1=new Accesslog;
												$model1->logid     =   '';
												$model1->empid      =   $user_id;
												if($j==1)
												{
												$model1->log_time   =   $date.' '.$time1;
												}
												if($j==2)
												{
												$model1->log_time   =   $date.' '.$time4;
												}
												
												$model1->status     =   0;
												$model1->device_id  =   $device_id;
												$model1->manual_entry_status = $last_id;
												$model1->save();	   
												}
											
									}else{
										   for ($j=1; $j<5; $j++)
										   {
										   $model1=new Accesslog;
										   $model1->logid     =  NULL;
										   $model1->empid      =   $user_id;
										   if($j==1)
											{
											$model1->log_time   =   $date.' '.$time1;
											}
											if($j==2)
											{
											$model1->log_time   =   $date.' '.$time2;
											}
											if($j==3)
											{
											$model1->log_time   =   $date.' '.$time3;
											}
											if($j==4)
											{
											$model1->log_time   =   $date.' '.$time4;
											}
										   $model1->status     =   0;
										   $model1->device_id  =   $device_id;
										   $model1->manual_entry_status = $last_id;
										   $model1->save();
										   }
                                       
								   }
                                       
                                       echo json_encode(array('response' => 'success', 'f_punch' => $first_punch , 'l_punch' => $last_punch, 't_out' =>$total_out_time, 't_entry_id'=> $last_id));	
                                    
                                        
                                    }


                                  }
                                  
                                  
       
    }

        
        /**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionDeleteindividualdata($id)
	{
		 
		/*  get site name and date */
		
		$model=$this->loadModel($id);
                
                $tbl = Yii::app()->db->tablePrefix;  
                $getsite = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from("{$tbl}manualentry_date")
                            ->where('id=:id',
                                    array(':id'=>$id))    
                            ->queryRow();
                $site = $getsite['site_id'];
                $date = $getsite['date'];

		/*  get site name and date ends */
		
		$user_id = (isset($_POST['userId']) ? $_POST['userId'] : "");
		$entry_id = (isset($_POST['entryId']) ? $_POST['entryId'] : "");
		
	    if($entry_id == NULL){

            }else{
               
            $newmodel = Manualentry::model()->findByPk($entry_id);
            if(!is_null($newmodel)){
             $newmodel->delete(); // delete the mannual entry
            } 

            $newarr = Yii::app()->db->createCommand()
            ->select("*")
            ->from("{$tbl}accesslog")
            ->where('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id.' AND manual_entry_status = '.$entry_id)    
            ->queryAll();

            if(!empty($newarr))
            {
                    Accesslog::model()->deleteAll('DATE(log_time)  = "'.$date.'" AND empid ='.$user_id.' AND manual_entry_status ='.$entry_id);
                    echo json_encode(array('response' => 'success'));	
            }else{
                echo json_encode(array('response' => 'success'));	
            }

          }
       
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{       
            
                $model2 = new ManualentryDate;
                
                $this->performAjaxValidation($model2);
                  
                if (isset($_POST['ManualentryDate'])) {
                 /* $model2->attributes = $_POST['ManualentryDate'];
                   $model2->created_by = yii::app()->user->id;
                   $model2->created_date = date('Y-m-d');
                   $model2->updated_by = yii::app()->user->id;
                   $model2->updated_date = date('Y-m-d');
                   if($model2->save()){
                   $this->redirect(array('index'));
                   }
                 } */
                    $date = $_POST['ManualentryDate']['date'];
                    $site = $_POST['ManualentryDate']['site_id'];
                    $tbl = Yii::app()->db->tablePrefix;   
                    $chkentry = Yii::app()->db->createCommand()
                           ->select('*')
                           ->from("{$tbl}manualentry_date")
                           ->where('date=:date and site_id=:site',
                                   array(':date'=>$date,':site'=>$site))    
                           ->queryAll();
                   // print_r($chkentry); die;
                   if($chkentry == NULL)
                   {
                   $model2->attributes = $_POST['ManualentryDate'];
                   $model2->created_by = yii::app()->user->id;
                   $model2->created_date = date('Y-m-d');
                   $model2->updated_by = yii::app()->user->id;
                   $model2->updated_date = date('Y-m-d');
                   if($model2->save())
                   {
                  // $this->redirect(array('update','id'=>$model2->id));
                   $this->redirect(array('mobileupdate','id'=>$model2->id));
                   }
                   }else{
                    Yii::app()->user->setFlash('error', "Already Entered !");
                    $this->redirect(array('index'));
    
                   }
                }
                $model=new ManualentryDate('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ManualentryDate']))
			$model->attributes=$_GET['ManualentryDate'];

		$this->render('index',array(
			'model'=>$model, 'model2' => $model2,
		));
               

        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ManualentryDate('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ManualentryDate']))
			$model->attributes=$_GET['ManualentryDate'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ManualentryDate::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manualentry-date-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
