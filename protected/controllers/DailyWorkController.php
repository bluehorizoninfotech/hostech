<?php

class DailyWorkController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
					),
					array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('gettasks','getAllTasks','gettaskdetails','checkdate','getLabourCharges','deleteLaboursUsed'),
						'users'=>array('@'),
					),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

	 
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
		$model=new DailyWork;
 
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['DailyWork']))
		{
			$sl_no = $_POST['DailyWork']['item_id'];
			$taskid= $_POST['DailyWork']['taskid'];
			$projectid= $_POST['DailyWork']['project_id'];
			$itemdetails = $_POST['newdetails'];
			
			$sl_no = strtok($sl_no, ",");

			$projectid = $_POST['DailyWork']['project_id'];
			
			$qry = Items::model()->find(array("condition" => "sl_no = '$sl_no' AND projectid=".$projectid));
			$item_id = $qry['id']; 
			$qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND projectid=".$projectid));
			$model->assigned_to = $qry2['assigned_to'];
			$model->attributes=$_POST['DailyWork'];
			$model->item_id= $item_id;
			$model->taskid=  $taskid;
			$model->work_type=  $_POST['DailyWork']['work_type'];

			if($model->save(false)){
				
				$this->redirect(array('view','id'=>$model->id));
			}
			
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DailyWork']))
		{
			$model->attributes=$_POST['DailyWork'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id = 0)
	{
		$model=new DailyWork('search');
		
		if ($id == 0) {
            $model2 = new DailyWork;
           // $msg = 'Successfully updated.';
           $task ="";
		   $labour_data=[];
        } else {
            $model2 = $this->loadModel($id);
            $tblpx = Yii::app()->db->tablePrefix;
            $pid = $model2->project_id;
            $condition1 = '';
            if(Yii::app()->user->role != 1){                    
                       $condition1 = 'WHERE project_id = '.$pid.'  AND assigned_to ='. Yii::app()->user->id.' or coordinator='.Yii::app()->user->id .' or created_by='.Yii::app()->user->id;
            }else{
                $condition1 = 'WHERE project_id = '.$pid.' ';
            }
            $task = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks ".$condition1)->queryAll();

			$sql = "SELECT jp_labours.labour_label,jp_labours.id,pms_daily_work_labours_used.number_of_labour,pms_daily_work_labours_used.labour_wage"
			. " FROM pms_daily_work_labours_used INNER JOIN jp_labours ON jp_labours.id=pms_daily_work_labours_used.labour_id where pms_daily_work_labours_used.daily_work_id=" . $id ;
		  $labour_data = Yii::app()->db->createCommand($sql)->queryAll();



		 
        }
        
        if(isset($_POST['DailyWork']))
		{	
			$this->performAjaxValidation($model2);
			$tasks = Tasks::model()->findByPk($_POST['DailyWork']['taskid']);
			$required_workers=$tasks->required_workers;
			$allowed_workers=$tasks->allowed_workers;
			$total_workers=$required_workers+$allowed_workers;			
			if($tasks->task_type == 1){		
			$task_id=$_POST['DailyWork']['taskid'];
			$task_date=date('Y-m-d',strtotime($_POST['DailyWork']['date']));
			$already_exist=Yii::app()->db->createCommand("select * FROM pms_daily_work WHERE taskid='".$task_id. "'AND date='".$task_date."'")->queryAll();
			if(count($already_exist)>0 && $id == 0)
			{
				
				Yii::app()->user->setFlash('error', "Task already entered");
			}
			else
			{
				if($total_workers<$_POST['DailyWork']['skilled_workers']+$_POST['DailyWork']['unskilled_workers'])
				{
					Yii::app()->user->setFlash('error', "Sum of skilled workers and unskilled workers should be less than or equal to ".$total_workers);
				}
				else
				{
			$model2->attributes=$_POST['DailyWork'];		
            $model2->work_type=  $_POST['DailyWork']['work_type'];
            $model2->skilled_workers=  $_POST['DailyWork']['skilled_workers'];
            $model2->unskilled_workers=  $_POST['DailyWork']['unskilled_workers'];
            $model2->taskid=  $_POST['DailyWork']['taskid'];
			
			$model2->date= date('Y-m-d',strtotime($_POST['DailyWork']['date']));
			$model2->created_by= Yii::app()->user->id;		
			
				if($model2->save(false))

				$delete_query = "delete from pms_daily_work_labours_used
				where daily_work_id = '$id' ";
			$delete=Yii::app()->db->createCommand($delete_query)->execute();

foreach($_POST['DailyWork']['labour_type'] as $key=> $labour_type)
{
	if($labour_type!= "" && $_POST['DailyWork']['labour_count'][$key] != "" && $_POST['DailyWork']['labour_wage'][$key])
	{
      


		$project_id = Yii::app()->db->createCommand()
		->select('acc_project_id')
		->from('pms_acc_project_mapping')
		->where('pms_project_id=:pms_project_id', array(':pms_project_id' => $model2->project_id))
		->queryScalar();
      

	   $labour_amount = Yii::app()->db->createCommand()
		->select('labour_rate')
		->from('jp_project_labour')
		->where('labour_id=:labour_id AND project_id=:project_id', array(':labour_id' => $labour_type,':project_id'=>$project_id))
		->queryScalar();

		

	  $total_amount= $_POST['DailyWork']['labour_count'][$key] * $_POST['DailyWork']['labour_wage'][$key] *   $labour_amount;
	  
	  $labour_count=$_POST['DailyWork']['labour_count'][$key];

	  $wage=$_POST['DailyWork']['labour_wage'][$key];

	  $sql = "INSERT INTO 
        `pms_daily_work_labours_used`
        (`daily_work_id`, `labour_id`, `number_of_labour`, `labour_wage`,
         `labour_amount`, `total_amount`)
          VALUES ('$model2->id','$labour_type','$labour_count','$wage','$labour_amount','  $total_amount')";

    Yii::app()->db->createCommand($sql)->query();

	}
}



				Yii::app()->user->setFlash('success', "Successfully created..");
					$this->redirect(array('index'));
			
			}
			}
		}
			else{
				$model3 = new TimeEntry;
				$model3->tskid =  $_POST['DailyWork']['taskid'];
				$model3->work_type =  $_POST['DailyWork']['work_type'];
				$model3->entry_date =  date('Y-m-d',strtotime($_POST['DailyWork']['date']));
				$model3->description =  $_POST['DailyWork']['description'];
				$model3->user_id = Yii::app()->user->id;
				$model3->created_by = Yii::app()->user->id;
				$model3->created_date = date("Y-m-d H:i:s");
				$model3->updated_by = Yii::app()->user->id;
				$model3->updated_date = date("Y-m-d H:i:s");
				$model3->save(false);
				$this->redirect(array('index'));
			}
		}

        
        
        
		$model=new DailyWork('search');
		$model->unsetAttributes();  // clear any default values
		if(Yii::app()->user->project_id !=""){          
            $model->project_id = Yii::app()->user->project_id;    
        }
		if(isset($_GET['DailyWork']))
            $model->attributes=$_GET['DailyWork'];
            if(isset($_GET['DailyWork']['date']) && $_GET['DailyWork']['date'] !=""){
                $model->date=date('Y-m-d',strtotime($_GET['DailyWork']['date']));
            }

		$this->render('index',array(
			'model'=>$model,'model2'=>$model2,'task' =>$task,'labour_data'=>$labour_data
		));
	}

		/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DailyWork::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='daily-work-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionGettasks()
	{ 
		
		if($_REQUEST['project_id']!=""){
			$project_id=$_REQUEST['project_id'];
			$item=$_REQUEST['item_id'];
			$tblpx = Yii::app()->db->tablePrefix;
			$sql_response = Yii::app()->db->createCommand("select * FROM {$tblpx}items WHERE sl_no='".$item. "'AND projectid='".$project_id."'")->queryAll();
			//print_r($sql_response);
			if($sql_response !=null){
				foreach($sql_response as $mainkey =>$mainvalue)
				{
					foreach($mainvalue as $key =>$value)
					{
						if($key=='description'){
							$data['item_description']=$value;
						}elseif($key=='taskid'){
							$data['taskid']=$value;
							$taskid=$value;
							$sql_response2 = Yii::app()->db->createCommand("select `title` FROM {$tblpx}tasks WHERE tskid='".$taskid."'")->queryAll();
							if($sql_response2 !=null){
								foreach($sql_response2 as $key =>$value){
									$data['task_name']=$value['title'];
								}
							}
						}elseif($key=='qty'){
							$data['total_qty']=$value;
						}elseif($key=='id'){
							$data['main_id']=$value;
						}
					}
				}
				
			}else{
					$data['main_id']=0;
					$data['task_name']='No Task is associated with this Item';
					
				}
		
	}else{
		$data['main_id']=0;
		$data['task_name']='No Task is associated with this Item';
		
	}
		print(json_encode($data,JSON_PRETTY_PRINT));
    }
    
    public function actiongetAllTasks(){
        {
            $tblpx = Yii::app()->db->tablePrefix;
            if(isset($_REQUEST['project_id'])){
                $pid = $_REQUEST['project_id'];
            }
            $condition1 = '';
            if(Yii::app()->user->role != 1){                    
                       $condition1 = 'WHERE task_type = 1 AND project_id = '.$pid.' AND assigned_to ='. Yii::app()->user->id.' or coordinator='.Yii::app()->user->id .' or created_by='.Yii::app()->user->id;
            }else{
                $condition1 = 'WHERE project_id = '.$pid.' AND task_type = 1';
            }
            $data = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks ".$condition1)->queryAll();
            $data=CHtml::listData($data,'tskid','title');
    
                echo "<option value=''>Select Task</option>";
                foreach($data as $value=>$site_name)
                {
                    echo CHtml::tag('option', array('value'=>$value),CHtml::encode($site_name),true);
                }
        }
    }

	public function actiongetLabourCharges()
	{
		$pid = $_REQUEST['project_id'];

		$mapping = Yii::app()->db->createCommand()
		->select('acc_project_id')
		->from('pms_acc_project_mapping')
		->where('pms_project_id=:pms_project_id', array(':pms_project_id' => $pid))
		->queryScalar();
		echo "<option value=''>Select Labour</option>";
		if($mapping)
		{
			$acc_project_id= $mapping;

			$sql = "SELECT jp_labours.id, jp_labours.labour_label, jp_project_labour.labour_rate "
			. " FROM jp_project_labour INNER JOIN jp_labours ON jp_labours.id=jp_project_labour.labour_id where jp_project_labour.project_id=" . $acc_project_id ;
		  $labour_data = Yii::app()->db->createCommand($sql)->queryAll();

		  if($labour_data)
		  {
			
                foreach($labour_data as $value=>$data)
                {
                    echo CHtml::tag('option', array('value'=>$data['id']),CHtml::encode($data['labour_label']),true);
                }
		  }
		}
		
	}

    public function actiongettaskdetails(){
        $tblpx = Yii::app()->db->tablePrefix;
        $taskid = $_REQUEST['taskid'];
        $sql_response2 = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks WHERE tskid='".$taskid."'")->queryRow();
		//echo json_encode(array('taskid' => $sql_response2['tskid']));
		if(!empty($sql_response2)){
            $work_type = WorkType::model()->findByPk($sql_response2['work_type_id']);
            $html .= "<option value=".$work_type['wtid'].">".$work_type['work_type']."</option>";
            echo json_encode(array('wtid'=> $work_type['wtid'],'option' => $html,'taskid' =>$sql_response2['tskid']));
        }else{
            $html = "<option value=''>Select work type</option>";
            echo json_encode(array('id'=> "",'option' =>$html));
        }
	}
	
	public function actioncheckdate(){
		if(isset($_POST['date']) && isset($_POST['project_id'])){
			$date = date('Y-m-d',strtotime($_POST['date']));			
			$criteria = new CDbCriteria;
			$criteria->condition = "task_type = 1 AND status NOT IN (5,72) AND project_id = ".$_POST['project_id']." AND start_date <= '$date' AND due_date >= '$date'";
			$data = Tasks::model()->findAll($criteria);
			$data_array = array();
			$tsk_id = "";
			$work_type = "";
			$tsk_id = "";
			$quantity = "";
			$unit = "";
			$work_type_html = "";
			if(count($data) ==1){
				foreach($data as $key=> $value){
					array_push($data_array,$value->tskid);
				}
				$tsk_id = implode(" ",$data_array);
				$task = Tasks::model()->findByPk($tsk_id);
				$work_type = $task->work_type_id;
				$work_types = WorkType::model()->findByPk($work_type);
			    $work_type_html .= "<option value=".$work_types['wtid'].">".$work_types['work_type']."</option>";
				$tsk_id = $tsk_id;
				$quantity = $task->quantity;
				if($task->unit !=0){
					$units_data = Unit::model()->findByPk($task->unit);
					$unit = $units_data->unit_code;
				}
				
			}			
			$html = '';
			if(!empty($data)){
			  $html .= '<option value="">Choose a task</option>';
			  foreach($data as $key => $value) {
				  $html .= '<option value='.$value['tskid'].'>'.$value['title'].'</option>';
				}
		  } else {
			  $html .='';
		  }
			echo json_encode(array('html' =>$html,'tsk_id' => $tsk_id, 'work_type' => $work_type,'quantity' => $quantity,'unit' => $unit, 'work_type_html' => $work_type_html));
		}

	}
	public function actiondeleteLaboursUsed()
	{
		$id=$_POST['id'];
		$delete_query = "delete from pms_daily_work_labours_used
				where id = '$id' ";
			$delete=Yii::app()->db->createCommand($delete_query)->execute();
			
			if ($delete == 1) {
		  echo '1';
		} else {
		  echo '2';
		}
	}

}