<?php

class ClientsiteController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;


        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('ajaxreturn', 'getModel', 'generalSite', 'create', 'warehouse_site_mapping', 'getWarehouse'),
                'users' => array('@'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Clientsite;
        $assigned_users_prev = array();
        $assigned_users = array();


        if (isset($_POST['Clientsite']['id']) && !empty($_POST['Clientsite']['id'])) {
            $id = $_POST['Clientsite']['id'];
            $model = $this->loadModel($id);
            $site_users = ClientsiteAssign::model()->findAll(array('condition' => 'site_id = ' . $model->id));
            $user_ids = array();
            foreach ($site_users as $site_user) {
                array_push($user_ids, $site_user->user_id);
            }
            if (!empty($user_ids)) {
                $user_ids = implode(',', $user_ids);
            }
            $assigned_users_prev = $user_ids;
        }
        $this->performAjaxValidation($model);

        if (isset($_POST['Clientsite'])) {
            $model->attributes = $_POST['Clientsite'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = $this->datetime();
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = $this->datetime();

            try {
                $transaction = Yii::app()->db->beginTransaction();
                if ($model->save()) {
                    if (isset($_POST['user_assign_model']['user_id'])) {
                        $user_array = $_POST['user_assign_model']['user_id'];

                        if (!empty($user_array)) {
                            if (empty($id)) {
                                $id = Yii::app()->db->getLastInsertID();
                            }

                            if (isset($id)) {
                                ClientsiteAssign::model()->deleteAll(array('condition' => 'site_id = ' . $id));
                            }

                            foreach ($user_array as $user) {
                                $client_model = new ClientsiteAssign;
                                $client_model->site_id =  $id;
                                $client_model->user_id = $user;
                                array_push($assigned_users, $client_model->user_id);
                                $client_model->save();
                                // }
                            }
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Successfully updated.');
                    if (isset($_POST['submit_type'])) {
                        if ($_POST['submit_type'] == 'save_btn') {
                            $return_result = array('status' => 1, 'next_level' => 0);
                        } elseif ($_POST['submit_type'] == 'save_cnt') {

                            $return_result = array('status' => 1, 'next_level' => 1, 'redirect' => Yii::app()->createUrl('Projects/index'));
                        } else {
                            $return_result = array('status' => 1, 'next_level' => 0);
                        }
                    }
                    echo json_encode($return_result);
                    if (!empty($assigned_users)) {
                        // $this->assignedUsersMail($model, $assigned_users, $assigned_users_prev);
                    }
                    $transaction->commit();
                    exit;
                } else {
                    $return_result = array('status' => 2, 'next_level' => 0, 'error' => $model->getErrors());

                    echo json_encode($return_result);
                    exit;
                }
            } catch (Exception $error) {
                $transaction->rollback();
                $return_result = array('status' => 3, 'next_level' => 0, 'error' => $error);
                echo json_encode($return_result);
                exit;
            }
        }
    }
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Clientsite'])) {
            $model->attributes = $_POST['Clientsite'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        // $this->loadModel($id)->delete();

        // // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        // if (!isset($_GET['ajax']))
        // 	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();

        try {

            $site_used = Tasks::model()->findByAttributes(array(
                'clientsite_id' => $id,

            ));

            if ($site_used) {
                $success_status = 2;
            } else {
                if (!$model->delete()) {
                    $success_status = 0;
                    throw new Exception(json_encode($model->getErrors()));
                } else {
                    $success_status = 1;
                }
            }


            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else if ($success_status == 2) {
                echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Clientsite');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($id = 0)
    {
        $actmodel = (intval($id) != 0 ? Clientsite::model()->findByPk($id) : Null);
        $project  = $this->actionGetProjects();
        if ($actmodel === Null) {
            $actmodel = new Clientsite;
        }

        if (isset($_POST['Clientsite'])) {
            $actmodel->attributes = $_POST['Clientsite'];
            $actmodel->latitude = $_POST['Clientsite']['latitude'];
            $actmodel->longitude = $_POST['Clientsite']['longitude'];
            $actmodel->pid = $_POST['Clientsite']['pid'];
            $actmodel->distance = $_POST['Clientsite']['distance'];
            $actmodel->created_by=yii::app()->user->id;
            $actmodel->created_date=date("Y-m-d");
            $actmodel->setScenario('project_site');
            if ($actmodel->save()) {
                if ($id == 0) {
                    Yii::app()->user->setFlash('success', 'Successfully added.');
                } else {
                    Yii::app()->user->setFlash('success', 'Successfully updated.');
                }
                $this->redirect(array('admin'));
            }
            
        }



        $model = new Clientsite('search');
        $model->unsetAttributes();  // clear any default values



        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 

        if (isset($_GET['Clientsite']))
            $model->attributes = $_GET['Clientsite'];

        $this->render('admin', array(
            'model' => $model, 'actmodel' => $actmodel, 'project' => $project
        ));
    }


    public function actionGetProjects()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (Yii::app()->user->role == 1) {
            $proj_condition = 'status="1"';
        } else {
            $proj_condition = 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)';
        }
        $data  = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE $proj_condition ORDER BY name")->queryAll();
        return $data;
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Clientsite::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actiongetModel()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = Clientsite::model()->findByPk($id);
            $client_users = ClientsiteAssign::model()->findAll(array('condition' => 'site_id = ' . $id));
            $users_array = array();
            foreach ($client_users as $user) {
                array_push($users_array, $user['user_id']);
            }
            if (!empty($users_array)) {
                $users_array = implode(',', $users_array);
            }
            if (!empty($model)) {
                $model_array = array('stat' => 1, 'id' => $model->id, 'site_name' => $model->site_name, 'latitude' => $model->latitude, 'longitude' => $model->longitude, 'users_array' => $users_array, 'distance' => $model->distance);
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo  json_encode($model_array);
        exit;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'clientsite-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAssigntoUser()
    {


        $id = $_REQUEST['id'];
        $check = "Select user_id from pms_clientsite_assigned where site_id=$id";
        $command = Yii::app()->db->createCommand($check);
        $command->execute();
        $check = $command->queryAll();
        $userEx = array();
        foreach ($check as $value) {
            $userEx[] = $value['user_id'];
        }
        $model = new ClientsiteAssign;
        /* $query = "SELECT CONCAT_WS(' ', `first_name`, `last_name`) AS `whole_name`,userid FROM `pms_users` WHERE status=0 ORDER BY whole_name";*/

        $query = "SELECT CONCAT_WS(' ', `first_name`, `last_name`) AS `whole_name`,userid,role 
              FROM `pms_users` left join pms_user_roles as rol on rol.id = pms_users.user_type
              WHERE status=0 ORDER BY whole_name";


        //$query = "SELECT CONCAT_WS(' ', `first_name`, `last_name`) AS `whole_name`,userid FROM `pms_users` WHERE status=0 AND user_type IN (1,2,10)ORDER BY whole_name";
        // admin //manager // site engineers // 
        $command = Yii::app()->db->createCommand($query);
        $command->execute();
        $users = $command->queryAll();

        if (isset($_POST['ClientsiteAssign'])) {
            $model->attributes = $_POST['ClientsiteAssign'];
            //$model->updated_by = yii::app()->user->id;
            //$model->updated_date</td> = date('Y-m-d');

            if ($model->validate() && $model->save())
                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script(" parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index'));
                }
        }

        //echo "<pre>";print_r($users);echo "</pre>";die();
        // Uncomment the following line if AJAX validation is needed


        if (!empty($_GET['asDialog']))
            $this->layout = false;
        $this->render('assignuser', array(
            'model' => $model,
            'users' => $users,
            'id' => $id,
            'check' => $userEx
        ));
    }

    public function actionAjaxReturn()
    {
        $id = ($_REQUEST['id']);
        $check = "Select user_id from pms_clientsite_assigned where site_id=$id";
        $command = Yii::app()->db->createCommand($check);
        $command->execute();
        $check = $command->queryAll();
        $arr = array();
        foreach ($check as $chk) {
            $arr[] = $chk['user_id'];
        }

        $user = array();
        if (isset($_REQUEST['users']))
            $user = ($_REQUEST['users']);
        foreach ($user as $users) {
            if (!in_array($users, $arr)) {
                $query = "INSERT INTO pms_clientsite_assigned (site_id,user_id) values('$id', '$users')";
                $parameters = array(":id" => $id, ":user" => $users);
                Yii::app()->db->createCommand($query)->execute();
            }
        }
        foreach ($arr as $array) {

            if (!in_array($array, $user)) {
                echo "Are you sure You want Delete the assigned users";
                $query = "DELETE FROM pms_clientsite_assigned WHERE site_id=$id AND user_id=$array";
                $parameters = array(":id" => $id, ":user" => $array);
                Yii::app()->db->createCommand($query)->execute();
            }
        }
        if ($user == "")
            foreach ($check as $checked) {
                $query = "DELETE FROM pms_clientsite_assigned WHERE site_id=$id AND user_id=$checked";
                $parameters = array(":id" => $id, ":user" => $checked);
                Yii::app()->db->createCommand($query)->execute();
            }
    }
    public function assignedUsersMail($model, $assigned_users, $assigned_users_prev)
    {
        if (!empty($assigned_users_prev)) {
            $assigned_users_prev = explode(',', $assigned_users_prev);
        } else {
            $assigned_users_prev = array();
        }
        $result_data = array();
        $users_ids = array();
        foreach ($assigned_users as $assign) {
            $user_datas = array();
            $user_model = Users::model()->findByPk($assign);
            if (!empty($user_model)) {
                $user_datas = array('name' => $user_model->first_name, 'email' => $user_model->email, 'id' => $user_model->userid);
                array_push($result_data, $user_model->first_name);
                array_push($users_ids, $user_datas);
            }
        }
        $uers_list = implode(',', $result_data);
        foreach ($users_ids as $assigned_user) {
            if (!in_array($assigned_user['id'], $assigned_users_prev)) {
                $model2 = new MailLog;
                $today = date('Y-m-d H:i:s');
                $mail_data = '
                <div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
                <h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $assigned_user['name'] . '</span></h3>
                <table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
                <tr><td colspan="2" style="background-color: #000;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $this->logo . '" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
                </tr>
                <tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>PMS - New Project created</h2></div></td>
                </tr>
                <tr><td colspan="2">This is a notification to inform that new project #' . $model->p->project_no . ' has been created: </td></tr>
                <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project Name: </td><td style="border:1px solid #f5f5f5;">' . $model->p->name . '</td></tr>                
                <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($model->p->start_date)) . ' - ' . date('d-M-y', strtotime($model->p->end_date)) . '</td></tr>
                
                <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Assigned Users:</td><td style="border:1px solid #f5f5f5;">' . $uers_list . '</td></tr>                                                           
                <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Description:</td> <td style="border:1px solid #f5f5f5;">' . nl2br($model->p->description) . '</td></tr>
                <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;" colspan="2">' . CHtml::link('Click here to see this task', $this->createAbsoluteUrl('projects/view', array('id' => $model->pid))) . '</td></tr>
                </table>
                
                <p>Sincerely,  <br />
                ' . Yii::app()->name . '</p>
                </div>';
                $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
                $mail = new JPhpMailer();
                $headers = "Hostech";
                $bodyContent = $mail_data;
                $mail->IsSMTP();
                $mail->Host = $mail_model['smtp_host'];
                $mail->Port = $mail_model['smtp_port'];
                $mail->SMTPSecure = $mail_model['smtp_secure'];
                $mail->SMTPAuth = $mail_model['smtp_auth'];
                $mail->Username = $mail_model['smtp_username'];
                $mail->Password = $mail_model['smtp_password'];
                $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
                $mail->Subject = "Project Assigned";
                if (!empty($assigned_user['email']))
                    $mail->addAddress($assigned_user['email']); // Add a recipient                                                             
                $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);

                $mail_send_byJSON = json_encode($mail_send_by);
                $subject = $mail->Subject;
                $model2->send_to = $assigned_user['email'];
                $model2->send_by = $mail_send_byJSON;
                $model2->send_date = $today;
                $model2->message = htmlentities($bodyContent);
                $model2->description = $mail->Subject;
                $model2->created_date = $today;
                $model2->created_by = Yii::app()->user->getId();
                $model2->mail_type = $subject;
                $mail->isHTML(true);
                $mail->MsgHTML($bodyContent);
                $mail->Body = $bodyContent;
                if (!empty($assigned_user['email'])) {
                    if ($mail->Send()) {
                        $model2->sendemail_status = 1;
                    } else {
                        $model2->sendemail_status = 0;
                    }
                } else {
                    $model2->sendemail_status = 0;
                }

                $model2->save();
            }
        }
    }

    public function actiongeneralSite($id = 0)
    {
        $actmodel = (intval($id) != 0 ? Clientsite::model()->findByPk($id) : Null);
        $project  = $this->actionGetProjects();
        if ($actmodel === Null) {
            $actmodel = new Clientsite;
        }

        if (isset($_POST['Clientsite'])) {

            $actmodel->attributes = $_POST['Clientsite'];
            $actmodel->latitude = $_POST['Clientsite']['latitude'];
            $actmodel->longitude = $_POST['Clientsite']['longitude'];
            $actmodel->distance = $_POST['Clientsite']['distance'];
            $actmodel->site_name = $_POST['Clientsite']['site_name'];
            $actmodel->created_by = yii::app()->user->id;
            $actmodel->created_date = $this->datetime();
            $actmodel->pid = null;
            $actmodel->setScenario('general_site');
            if ($actmodel->save()) {
                if ($id == 0) {
                    Yii::app()->user->setFlash('success', 'Successfully added.');
                } else {
                    Yii::app()->user->setFlash('success', 'Successfully updated.');
                }
                $this->redirect(array('generalSite'));
            }
        }



        $model = new Clientsite('search');
        $model->unsetAttributes();  // clear any default values



        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 

        if (isset($_GET['Clientsite']))
            $model->attributes = $_GET['Clientsite'];

        $this->render('general', array(
            'model' => $model, 'actmodel' => $actmodel, 'project' => $project
        ));
    }
    public function actionWarehouse_site_mapping()
    {
        if (isset($_POST['site_id']) && isset($_POST['warehouse_id'])) {
            $model = Clientsite::model()->findByPk($_POST['site_id']);
            $model->account_warehouse_id = $_POST['warehouse_id'];
            if ($model->save()) {
                $model_array = array('stat' => 1);
            } else {
                $model_array = array('stat' => 0);
            }
            echo json_encode($model_array);
            exit;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('account_warehouse_id IS NULL');
        $client_site = Clientsite::model()->findAll($criteria);
        $this->render('warehouse_site_mapping', array(
            'client_site' => $client_site
        ));
    }
    public function actiongetWarehouse()
    {
        $site_id = $_POST['site_id'];
        $client_site = Clientsite::model()->findByPk($site_id);
        $html['html'] = '';
        $html['warehouse'] = '';

        $mapping = Yii::app()->db->createCommand()
            ->select('acc_project_id')
            ->from('pms_acc_project_mapping')
            ->where('pms_project_id=:pms_project_id', array(':pms_project_id' => $client_site->pid))
            ->queryScalar();



        if ($mapping) {

            $account_project_id = $mapping;

            $warehouse = Yii::app()->db->createCommand()
                ->from('jp_warehouse')
                ->where('project_id=:project_id', array(':project_id' => $account_project_id))
                ->queryAll();


            if (!empty($warehouse)) {

                $html['html'] .= '<option value="" >Select Warehouse</option>';
                foreach ($warehouse as $key => $value) {
                    $html['html'] .= '<option value="' . $value['warehouse_id'] . '" >' . $value['warehouse_name'] . '</option>';
                }
            }

            $result = array(
                'status' => 1,
                'html' => $html

            );
        } else {
            $result = array(
                'status' => 0,
                'html' => $html

            );
        }
        echo (json_encode($result));
    }
}
