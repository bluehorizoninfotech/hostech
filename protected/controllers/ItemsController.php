<?php

class ItemsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);
				return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
					),
					array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => array('importcsv'),
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Items;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Items']))
		{
			$model->attributes=$_POST['Items'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id,$projectid)
	{
		
		$this->layout =false;
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Items']))
		{
			//echo $id; die;
			$model->attributes=$_POST['Items'];
			$model->projectid = $projectid;
			if($model->save())
				$this->redirect(array('importcsv','projectid'=>$model->projectid));
		}

		$this->render('update',array(
			'model'=>$model,'projectid'=>$projectid
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id,$projectid)
	{
		$model=$this->loadModel($id);
		$sl_no = $model->sl_no;
		
		
		$this->loadModel($id)->delete();
		$qry = Yii::app()->db->createCommand("select `id` from `wpr_items` where  sl_no like '".$sl_no.".%' AND projectid=".$projectid)->queryAll();
		if($qry != null){
		foreach($qry as $q){
			Yii::app()->db->createCommand("DELETE FROM wpr_items WHERE id=" . $q['id'])->execute();
		}
		}
			
		/*$sql = Yii::app()->db->createCommand()
                    ->select(['parentid'])
                    ->from('wpr_items')
                    ->where('id ='.$id.' AND projectid='.$projectid)
                    ->queryRow();
        if($sql['parentid'] == null)
        {
			$this->loadModel($id)->delete();
			$qry = Yii::app()->db->createCommand("select `id` from `wpr_items` where  sl_no like '".$sl_no.".%' AND projectid=".$projectid)->queryAll();
			if($qry != null){
			foreach($qry as $q){
				Yii::app()->db->createCommand("DELETE FROM wpr_items WHERE id=" . $q['id'])->execute();
			}
			}
			
		}else{
			
			$this->loadModel($id)->delete();
			$qry = Yii::app()->db->createCommand("select `id` from `wpr_items` where  sl_no like '".$sl_no.".%' AND projectid=".$projectid)->queryAll();
			if($qry != null){
			foreach($qry as $q){
				Yii::app()->db->createCommand("DELETE FROM wpr_items WHERE id=" . $q['id'])->execute();
			}	
			}		
		} */
		
		echo json_encode(array('response'=> 'success'));

	}

	/**
	 * Lists all models.
	 */
	 
	public function actionIndex($projectid)
	{	
		$newmodel=new ImportForm() ;
		$newmodel->unsetAttributes();
		$model=new Items('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Items']))
			$model->attributes=$_GET['Items'];

		$this->render('index',array(
			'model'=>$model,'newmodel'=>$newmodel,'projectid'=>$projectid,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Items('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Items']))
			$model->attributes=$_GET['Items'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Items::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='items-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	 public function actionDownloadsample() {
       $doc = "sample-items-csv.csv";
       $path = Yii::getPathOfAlias('webroot') . '/uploads/';
       //die($path);
       $file = $path . '' . $doc;
       if (file_exists($file)) {
           Yii::app()->getRequest()->sendFile($doc, file_get_contents($file));
       }
   }
   
   
   
	public function actionImportcsv($projectid) {
		$newarr = array();
		$newmodel=new ImportForm() ;
        $model=new Items('search');
		$model->unsetAttributes(); 
		$newmodel->unsetAttributes();  // clear any default values
		if(isset($_GET['Items']))
			$model->attributes=$_GET['Items'];
		
		 if(isset($_POST['ImportForm'])){
			
               $newmodel->attributes=$_POST['ImportForm']; 
               $amount_data            = "";
                if($newmodel->validate()){
					$parentid = '';
                    $csvFile=CUploadedFile::getInstance($newmodel,'file');  
                    $tempLoc=$csvFile->getTempName();
                    try{

                        $transaction = Yii::app()->db->beginTransaction();
                        $handle 	 = fopen("$tempLoc", "r");
                        $row 		 = 1;
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
							if($row>1){
								
								$tblpx = Yii::app()->db->tablePrefix;
								$a = $data[0];
								$tskid_csv = $data[1];
								//echo $tskid_csv;
								$sql = Items::model()->findAll(array("condition" => "sl_no = '$a' AND projectid=".$projectid));
								
								//print_r($sql1);die;
								

									if($sql == null){
										$exp = explode(".", $data[0]);
										$count = count($exp);
										//echo "<br>1exp";print_r($exp);
										$sql1 = Tasks::model()->findAll(array("condition" => "tskid = '$tskid_csv' AND project_id=".$projectid));
										// if($sql1 == null){
										// 		array_push($newarr, "Sl No.".$a."- Task Id ".$tskid_csv." is not Part of this project!");
										// }else{
											//echo $data[1];die;
											if($data[1] == null && $data[2] == null && $data[3] == null && $data[4] == null && $data[5] == null && $data[6] == null){	
												if($sql1 != null){
													array_push($newarr, "Parent Item (Sl No.".$a.")-Doesnot contain Task Id! ");
												}else{
													$first = $exp[0];
													$new = Items::model()->findAll(array("condition" => "sl_no = '$first' AND projectid=".$projectid));
													if($new ==null){
														$itemmodel 					= new Items();
														$itemmodel->sl_no 	=	  "$data[0]";
														$itemmodel->description 	=	  $data[2];
														$itemmodel->qty 	= 	  trim($data[3]);
														$itemmodel->unit 	= 	  $data[4];
														$itemmodel->rate 	= 	  trim($data[5]);
														$amount_data            =  (floatval(trim($data[3])) * floatval(trim($data[5])));
														$itemmodel->amount 	= 	(trim($data[6])!="") ?  str_replace(",","",trim($data[6])) : (($amount_data!=0) ? $amount_data: "");
														$itemmodel->taskid 	= 	  $data[1];
														$itemmodel->projectid 	= 	  $projectid;
														$parentid = $data[0];
														$itemmodel->save();
												}else{
													
														$itemmodel 					= new Items();
														$itemmodel->sl_no 	=	  "$data[0]";
														$itemmodel->description 	=	  $data[2];
														$itemmodel->qty 	= 	  trim($data[3]);
														$itemmodel->unit 	= 	  $data[4];
														$itemmodel->rate 	= 	  trim($data[5]);
														$amount_data            =  (floatval(trim($data[3])) * floatval(trim($data[5])));
														$itemmodel->amount 	= 	(trim($data[6])!="") ?  str_replace(",","",trim($data[6])) : (($amount_data!=0) ? $amount_data: "");
														$itemmodel->taskid 	= 	  $data[1];
														$itemmodel->parentid 	= $first;
														$itemmodel->projectid 	= 	  $projectid;
														$parentid = $data[0];
														$itemmodel->save();	
													
													
												}
											
											}
										}else{	
											if($sql1 == null){
													array_push($newarr, "Sl No.".$a."- Task Id ".$tskid_csv." is not Part of this project!");
												
												}else{
											
												$exp1 = explode(".", $data[0]);
												$exp2 = explode(".", $parentid);
												//echo "<br>2exp1";print_r($exp1);
												//echo "<br>3exp2";print_r($exp2);
												if($exp1[0] == $exp2[0]){
													if(is_numeric($data[3]) && is_numeric($data[5])){
														$itemmodel 					= new Items();
														$itemmodel->sl_no 	=	  "$data[0]";
														$itemmodel->description 	=	  $data[2];
														$itemmodel->qty 	= 	  trim($data[3]);
														$itemmodel->unit 	= 	  $data[4];
														$itemmodel->rate 	= 	  trim($data[5]);
														$amount_data            =  (floatval(trim($data[3])) * floatval(trim($data[5])));
														$itemmodel->amount 	= 	(trim($data[6])!="") ?  str_replace(",","",trim($data[6])) : (($amount_data>0) ? $amount_data: "");
														$itemmodel->taskid 	= 	  $data[1];
														$itemmodel->parentid 	= $parentid;
														$itemmodel->projectid 	= 	  $projectid;
														$itemmodel->save();
														
													}else{
													
														if(is_numeric($data[3])){
														$st = $data[0]."-rate-".$data[5]." Not a Number";
														array_push($newarr, $st);
														}elseif(is_numeric($data[5])){
															
															$st = $data[0]."-quantity-".$data[3]." Not a Number";
															array_push($newarr, $st);	
														}else{
															$st = $data[0]."-quantity-".$data[3]."-rate-".$data[5]." Not a Number";
															array_push($newarr, $st);			
														}
												
													}
												}else{
													
													$st = $data[0]." parent missing";
													array_push($newarr, $st);
													
													
												}
											
										}
									}
								// }
									
								}else{
										
										
										array_push($newarr, $a." is Already Exist!");
									
								}
										
							
						}
                            $row++;              
                        }
                        //die;
                        $transaction->commit();
                        $this->render('index',array(
							'model'=>$model,'newmodel'=>$newmodel,'newarr'=>$newarr,'projectid'=>$projectid,
						));
                    }
                    
                    catch(Exception $error){
						$transaction->rollback();
                    } 
                }else{
					
					$this->render('index',array(
						'model'=>$model,'newmodel'=>$newmodel,'projectid'=>$projectid,
					));
					
				}
			
			
                 
             }else{

				$this->render('index',array(
					'model'=>$model,'newmodel'=>$newmodel,'projectid'=>$projectid,
				));
             }
           
       
    }
}