<?php

class AccesslogController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    // public $total_devices = "0"; // 1: first_punch_device, 0:all devices

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;


        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
        /*  return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'viewall', 'month', 'view3', 'receivelog', 'exportExcel','getmonthreport', 'logsender','attreport', 'monthlyreport', 'generate', 'punchlog', 'monthlyreportnew', 'dailyreport', 'attendancereport', 'newotcalculation', 'monthly_report'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );  */
    }

    public function actionLogsender()
    {

        $url = "//localhost/accesslog/timelog/index.php?r=accesslog/receivelog";

        $array = array('a' => 'apple', 'b' => 'balal', 2, 2, 2, 3, 3, 3, 3, 3);
        //open connection
        $ch = curl_init();

        $string = 'logitems=' . json_encode($array);
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $string);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
    }

    public function actionReceivelog()
    {
        if (isset($_REQUEST['logitems'])) {
            print_r(json_decode($_POST['logitems'], true));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $lastlogid = 0;

        if (isset($_POST['newlog'])) {
            //$query_part = str_replace("],[", '),(', str_replace(']]', ')', str_replace('[[', '(', $_POST['newlog'])));
            $logarray = json_decode($_POST['newlog'], true);

            $logids = array_keys($logarray);
            if (count($logids)) {
                $loglistid = implode(',', $logids);
                $existids = Yii::app()->db->createCommand("select logid from pms_punch_log where logid in ({$loglistid})")->queryAll();
                $duplicate_ids = array();

                foreach ($existids as $dids) {
                    $duplicate_ids[] = $dids['logid'];
                    unset($logarray[$dids['logid']]);
                }
                $logids = json_encode(array_values($logarray));
                $query_part = str_replace("],[", '),(', str_replace(']]', ')', str_replace('[[', '(', $logids)));
            }

            //$loglist = $_POST['newlog'];
            if ($query_part != '[]') {
                if (Yii::app()->db->createCommand("INSERT INTO `pms_punch_log` VALUES  {$query_part}")->query()) {
                    $lastlogid = Yii::app()->db->getLastInsertID();
                } else {
                    $lastlogid = "-1";
                }
            }
        }
        //  print_r($_POST);
        die($lastlogid);
    }

    protected function getEmployeeName($data, $row)
    {

        //        $model = Users::model()->findByAttributes;
        //        if ($model === null)
        //            throw new CHttpException(404, 'The requested page does not exist.');
        //        
        //        
        //        return $data->empid;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionMonth()
    {
        $model = new Accesslog();

        $hdays = 5;

        if (isset($_GET['hdays'])) {
            $hdays = intval($_GET['hdays']);
        }

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        date_default_timezone_set('Asia/Kolkata');

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        


        for ($i1 = 0; $i1 <= $hdays; $i1++) {

            $days = 0;
            if (isset($_GET['days']) && intval($_GET['days'])) {
                $days = intval($_GET['days']) + $i1;
            }
            if ($model->log_time != "") {
                $seldate = strtotime($model->log_time);
                $todaydate = strtotime(date("Y-m-d"));
                if ($seldate < $todaydate) {
                    $days = ($todaydate - $seldate) / (-3600 * 24);
                }
            }

            $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

            $precent_date = date("Y-m-d", $system_Date);
            $datewithtime = date("Y-m-d H:i:s", $system_Date);
            //        echo "<br />";

            $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,"
                . "concat(first_name,' ', last_name) as username, "
                . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
                . "and empid=accesscard_id) as count_punch ,(select log_time "
                . "from pms_punch_log as pa where log_time  like '$precent_date%' "
                . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
                . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
                . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
                . "where accesscard_id!=0 and first_name not In ('Bala' , 'Amal', 'Sudesh') group by accesscard_id order by first_name";
            //die();
            $command = Yii::app()->db->createCommand($sql);
            $logemp = $command->queryAll();

            $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
                . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
                . "group by empid,log_time order by empid asc, log_time asc";

            $command2 = Yii::app()->db->createCommand($sql2);
            $logemp2 = $command2->queryAll();

            $logrec = array();
            $userlog = array();

            $userid = 0;
            $prev_time = 0;
            $flag = 0;
            $outtime = array();
            foreach ($logemp2 as $rec) {
                //            /$logrec[$rec['empid']][] = ($rec['log_time']);
                //            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                //continue;            
                if ($userid != $rec['empid']) {
                    $flag = 0;
                    $userid = $rec['empid'];
                    $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                    $prev_time = strtotime($rec['log_time']);
                    //echo "<br/>";
                    $flag++;
                } else {
                    //echo $flag."<br/>";
                    //echo strtotime($rec['log_time']) . '-' . $prev_time;
                    //                die();
                    $timediffin = strtotime($rec['log_time']) - $prev_time;

                    $prev_time = strtotime($rec['log_time']);

                    if ($flag % 2 == 1) {
                        $userlog[$userid]['IN'] += $timediffin;
                    } else {
                        $userlog[$userid]['OUT'] += $timediffin;
                        $empid1 = $rec['empid'];
                        $outtime[$empid1] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                    }

                    // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                    $flag++;
                }
            }

            //print_r($userlog);
            //        die();
            //        echo "<pre>";
            //print_r($outtime);
            //  print_r($userlog);
            //        die();
            echo $this->renderPartial('monthly', array(
                'model' => $model,
                'logemp' => $logemp, 'inout' => $userlog,
                'days' => $days, 'outtime' => $outtime,
                'precent_date' => $precent_date
            ), true);
            echo "<hr />";
        }
        //         echo "<pre>";
        //         print_r($logrec);
        //         echo "</pre>";
    }

    public function actionView3()
    {
        $model = new Accesslog();

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        date_default_timezone_set('Asia/Kolkata');


        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
        //        echo "<br />";

        $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,"
            . "concat(first_name,' ', last_name) as username, "
            . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
            . "and empid=accesscard_id) as count_punch ,(select log_time "
            . "from pms_punch_log as pa where log_time  like '$precent_date%' "
            . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
            . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
            . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
            . "where accesscard_id!=0 and first_name not In ('Bala' , 'Amal', 'Sudesh') and `status`=0 group by accesscard_id order by first_name";
        die($sql);
        $command = Yii::app()->db->createCommand($sql);
        $logemp = $command->queryAll();

        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
            . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
            . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();



        $logrec = array();
        $userlog = array();

        $userid = 0;
        $prev_time = 0;
        $flag = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
            //            /$logrec[$rec['empid']][] = ($rec['log_time']);
            //            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
            //continue;            
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];
                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                //echo "<br/>";
                $flag++;
            } else {
                //echo $flag."<br/>";
                //echo strtotime($rec['log_time']) . '-' . $prev_time;
                //                die();
                $timediffin = strtotime($rec['log_time']) - $prev_time;

                $prev_time = strtotime($rec['log_time']);

                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;
                } else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                    $outtime[$empid1] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                }

                // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                $flag++;
            }
        }

        //print_r($userlog);
        //        die();
        //        echo "<pre>";
        //print_r($outtime);
        //  print_r($userlog);
        //        die();
        $this->render('view', array(
            'model' => $model,
            'logemp' => $logemp, 'inout' => $userlog,
            'days' => $days, 'outtime' => $outtime,
            'precent_date' => $precent_date
        ));

        //         echo "<pre>";
        //         print_r($logrec);
        //         echo "</pre>";
    }

    public function actionView()
    {
        $model = new Accesslog();

        //die("<center><h1>Under Maintenance</h1></center>");

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
        //        echo "<br />";

        $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,"
            . "concat(first_name,' ', last_name) as username, "
            . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
            . "and empid=accesscard_id) as count_punch ,(select log_time "
            . "from pms_punch_log as pa where log_time  like '$precent_date%' "
            . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
            . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
            . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
            . "where accesscard_id!=0 and first_name not In ('Bala1' , 'Amal1', 'Sudesh1') and `status`=0 group by accesscard_id order by first_name";
        //die();
        $command = Yii::app()->db->createCommand($sql);
        $logemp = $command->queryAll();

        $lastsysncsql = Yii::app()->db->createCommand('SELECT date_format(last_sync_time,"%d-%m-%Y %T")  FROM `pms_punch_log_last_sync`');
        $lastsync = $lastsysncsql->queryScalar();

        //print_r(  $lastsync); 

        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate('" . date("Y-m-d") . "', INTERVAL $days day) "
            . "and log_time<=adddate('" . date("Y-m-d") . "', INTERVAL " . ($days + 1) . " day) "
            . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();



        $logrec = array();
        $userlog = array();

        $userid = 0;
        $prev_time = 0;
        $flag = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
            //            /$logrec[$rec['empid']][] = ($rec['log_time']);
            //            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
            //continue;            
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];
                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                //echo "<br/>";
                $flag++;
            } else {
                //echo $flag."<br/>";
                //echo strtotime($rec['log_time']) . '-' . $prev_time;
                //                die();
                $timediffin = strtotime($rec['log_time']) - $prev_time;

                $prev_time = strtotime($rec['log_time']);

                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;
                } else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                    $outtime[$empid1] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                }

                // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                $flag++;
            }
        }

        //print_r($userlog);
        //        die();
        //        echo "<pre>";
        //print_r($outtime);
        //  print_r($userlog);
        //        die();
        $this->render('view', array(
            'model' => $model,
            'logemp' => $logemp, 'inout' => $userlog,
            'days' => $days, 'outtime' => $outtime,
            'precent_date' => $precent_date, 'lastsync' => $lastsync
        ));

        //         echo "<pre>";
        //         print_r($logrec);
        //         echo "</pre>";
    }

    public function actionViewall()
    {
        $model = new Accesslog();

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        date_default_timezone_set('Asia/Kolkata');
        $ch = curl_init();

        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL => '//192.168.1.100/getlog.php',
                CURLOPT_RETURNTRANSFER => true
            )
        );
        $output = curl_exec($ch);
        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
        //        echo "<br />";

        $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,"
            . "concat(first_name,' ', last_name) as username, "
            . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
            . "and empid=accesscard_id) as count_punch ,(select log_time "
            . "from pms_punch_log as pa where log_time  like '$precent_date%' "
            . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
            . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
            . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
            . "where accesscard_id!=0 and first_name not In ('Bala1' , 'Amal1', 'Sudesh1') and `status`=0 group by accesscard_id order by first_name";
        //die();
        $command = Yii::app()->db->createCommand($sql);
        $logemp = $command->queryAll();

        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate('" . date("Y-m-d") . "', INTERVAL $days day) "
            . "and log_time<=adddate('" . date("Y-m-d") . "', INTERVAL " . ($days + 1) . " day) "
            . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();

        $logrec = array();
        $userlog = array();

        $userid = 0;
        $prev_time = 0;
        $flag = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
            //            /$logrec[$rec['empid']][] = ($rec['log_time']);
            //            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
            //continue;            
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];
                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                //echo "<br/>";
                $flag++;
            } else {
                //echo $flag."<br/>";
                //echo strtotime($rec['log_time']) . '-' . $prev_time;
                //                die();
                $timediffin = strtotime($rec['log_time']) - $prev_time;

                $prev_time = strtotime($rec['log_time']);

                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;
                } else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                    $outtime[$empid1] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                }

                // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                $flag++;
            }
        }

        //print_r($userlog);
        //        die();
        //        echo "<pre>";
        //print_r($outtime);
        //  print_r($userlog);
        //        die();
        $this->render('view', array(
            'model' => $model,
            'logemp' => $logemp, 'inout' => $userlog,
            'days' => $days, 'outtime' => $outtime,
            'precent_date' => $precent_date
        ));

        //         echo "<pre>";
        //         print_r($logrec);
        //         echo "</pre>";
    }

    public function calculate_time_span($seconds)
    {
        $months = floor($seconds / (3600 * 24 * 30));
        $day = floor($seconds / (3600 * 24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours * 3600)) / 60);
        $secs = floor($seconds % 60);

        return $hours . ":" . $mins . ":" . $secs;

        $ret = array('h' => 0, 'm' => 0, 's' => 0);


        if ($hours >= 1) {
            $ret['h'] = $hours;
        }

        if ($seconds < 60)
            $time = $secs . " seconds ago";
        else if ($seconds < 60 * 60)
            $time = $mins . " min ago";
        else if ($seconds < 24 * 60 * 60)
            $time = $hours . " hours ago";
        else if ($seconds < 24 * 60 * 60)
            $time = $day . " day ago";
        else
            $time = $months . " month ago";

        return $time;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Accesslog;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Accesslog'])) {
            $model->attributes = $_POST['Accesslog'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->logid));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Accesslog'])) {
            $model->attributes = $_POST['Accesslog'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->logid));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Accesslog('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Accesslog']))
            $model->attributes = $_GET['Accesslog'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Accesslog::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'accesslog-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /* added by minu on 22-07-17 */

    function TimeToSec($time)
    {
        $sec = 0;
        foreach (array_reverse(explode(':', $time)) as $k => $v)
            $sec += pow(60, $k) * $v;
        return $sec;
    }

    public function actionMonthlyreportnew()
    {
        $totalot = 0;
        $tblpx = Yii::app()->db->tablePrefix;
        $total_devices = 1;
        if (isset($_REQUEST['TimeEntry'])) {
            $date1 = new DateTime($_REQUEST['TimeEntry']['date_from']);
            $date2 = new DateTime($_REQUEST['TimeEntry']['date_till']);
            if (isset($_REQUEST['TimeEntry']['checkbox'])) {
                $total_devices = $_REQUEST['TimeEntry']['checkbox'];
            } else {
                $total_devices = 0;
            }
            Yii::app()->session['total_devices'] = $total_devices;
            Yii::app()->session['start_date'] = $_REQUEST['TimeEntry']['date_from'];
            Yii::app()->session['end_date'] = $_REQUEST['TimeEntry']['date_till'];

            list($start_date, $end_date, $week_title) = $this->x_week_range($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
        } elseif (isset(Yii::app()->session['start_date']) && (Yii::app()->session['end_date'])) {
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates(Yii::app()->session['start_date'], Yii::app()->session['end_date']);
            $start_date = Yii::app()->session['start_date'];
            $end_date = Yii::app()->session['end_date'];
        } else {

            list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-d"));
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($start_date, $end_date);
            Yii::app()->session['start_date'] = $start_date;
            Yii::app()->session['end_date'] = $end_date;
        }




        $resultot = array();

        $resultar = $this->generate($start_date, $end_date);
        // echo '<pre>';
        // print_r($resultar);die();
        $newar = $resarr = array();

        foreach ($resultar as $key => $result) {
            foreach ($result as $key1 => $res) {
                $device_id = $res['device_id'];
                $newar[$key][$device_id][] = $res;
            }
        }

        // echo '<pre>';
        // print_r($newar);die();

        $shifttime = "08:00:00";
        $shiftdevicear = array();
        $ottime = 0;
        if ($newar != NULL) {

            foreach ($newar as $keyuser => $user) {
                $otgndtotal = 0;

                foreach ($user as $key2 => $devicear) {
                    $countp = 0;
                    $ottotal = 0;
                    $otimes = "";
                    //$othrs = 0;

                    if ($key2 != '') {
                        foreach ($devicear as $key3 => $device) {
                            //                            echo '<pre>';
                            //                            print_r($device);//die();

                            $othrs = $device['othours'];
                            if (($othrs != '') && ($othrs != '05:30:00') && ($device['first_punch'] != $device['last_punch'])) {
                                // echo '----'.$othrs;
                                $otimes .= ',' . $device['othours'];
                                $seconds = strtotime($othrs) - strtotime('TODAY');
                                //$seconds = mktime($hours,$mins,$secs) - mktime(0,0,0);
                                $ottotal += $seconds;
                                $otgndtotal += $seconds;
                                //echo '-------'.$this->calculate_time_span($ottotal);
                            }



                            if (strtotime($device['inhrs']) >= strtotime($shifttime)) {

                                $countp++;
                            }
                        }
                        // echo '----------------' . $countp;
                    }
                    $shiftdevicear[$keyuser][$key2]['othrs'] = $this->calculate_time_span($ottotal);
                    $shiftdevicear[$keyuser][$key2]['totalpunch'] = $countp;
                    $shiftdevicear[$keyuser][$key2]['otimes'] = $otimes;
                    $shiftdevicear[$keyuser]['totalot'] = $this->calculate_time_span($otgndtotal);
                    //                     echo '<br>';
                    //                     echo $ottotal;
                }
            }
        }

        // echo '<pre>';
        // print_r($shiftdevicear);
        // die();


        $resarr = array();

        $users = Yii::app()->db->createCommand("SELECT us.*, dept.department_name FROM {$tblpx}users as us  LEFT JOIN {$tblpx}department as dept ON dept.department_id = us.department_id  WHERE us.status = 0 AND us.accesscard_id != ''    ORDER BY dept.department_id")->queryAll(); //echo "<pre>"; print_r($users);and accesscard_id = 42
        $devicesdata = Devices::model()->findAll();

        $devices = array();

        foreach ($devicesdata as $devicedata) {
            $devices[$devicedata->device_id]['id'] = $devicedata->device_id;
            $devices[$devicedata->device_id]['device_name'] = $devicedata->device_name;
        }


        if (!empty($users)) {
            $j = 0;
            $totpunch = 0;
            $resarr = array();

            foreach ($users as $user) {
                $resarr[$j]['totalpunch'] = 0;
                $accesscardid = $user['accesscard_id']; //$accesscardid = 58;
                $empid = $user['accesscard_id'];
                if ($shiftdevicear != NULL) {
                    foreach ($shiftdevicear as $keyuser => $ottime) {
                        if ($accesscardid == $keyuser) {
                            foreach ($ottime as $keytime => $ott) {
                                //                                 echo '<pre>';
                                //                                print_r($ott);die();
                                if (is_numeric($keytime)) {
                                    // echo '<pre>';
                                    // print_r($ott);


                                    $resarr[$j]["total_ot_" . $keytime] = $ott['othrs'];
                                    $resarr[$j]['totalpunch_' . $keytime] = $ott['totalpunch'];
                                } else {
                                    $resarr[$j]['totalot'] = $ott;
                                }
                            }
                            continue;
                        }
                    }
                }
                $resarr[$j]['empid'] = $accesscardid;
                $resarr[$j]['employee_id'] = $user['employee_id'];
                $resarr[$j]['name'] = $user['first_name'] . ' ' . $user['last_name'];
                $resarr[$j]['designation'] = $user['designation'];
                $resarr[$j]['department_id'] = $user['department_id'];
                $resarr[$j]['department_name'] = $user['department_name'];
                $j++;
            }
        }

        //die;

        $this->render("monthlyreport", array('users' => $users, 'start_date' => $start_date, 'end_date' => $end_date, 'results' => $resarr, 'devices' => $devices, 'total_devices' => $total_devices));
    }

    public function actionAttendancereport()
    {
        $totalot = 0;
        $tblpx = Yii::app()->db->tablePrefix;


        $total_devices = 1;

        if (isset($_GET['code'])) {
            $code = intval($_GET['code']);
            if ($code <= 0) {
                $code = 0;
            }
        } else {
            $code = 0;
        }


        if (isset($_REQUEST['TimeEntry'])) {
            $date1 = new DateTime($_REQUEST['TimeEntry']['date_from']);
            $date2 = new DateTime($_REQUEST['TimeEntry']['date_till']);

            //            if (isset($_REQUEST['TimeEntry']['checkbox'])) {
            //            $total_devices = $_REQUEST['TimeEntry']['checkbox'];
            //            } else {
            //            $total_devices = 0;
            //            }                        
            //Yii::app()->session['total_devices'] = $total_devices;

            Yii::app()->session['start_date'] = $_REQUEST['TimeEntry']['date_from'];
            Yii::app()->session['end_date'] = $_REQUEST['TimeEntry']['date_till'];

            list($start_date, $end_date, $week_title) = $this->x_week_range($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
        } elseif (isset(Yii::app()->session['start_date']) && (Yii::app()->session['end_date'])) {

            Yii::app()->session['days_count'] = $this->getcountBetween2Dates(Yii::app()->session['start_date'], Yii::app()->session['end_date']);
            $start_date = Yii::app()->session['start_date'];
            $end_date = Yii::app()->session['end_date'];
        } else {

            list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-d"));
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($start_date, $end_date);
            Yii::app()->session['start_date'] = $start_date;
            Yii::app()->session['end_date'] = $end_date;
        }


        $resultot = array();
        $start_date = Yii::app()->session['start_date'];
        $end_date = Yii::app()->session['end_date'];
        //die;
        $resultar = $this->generate($start_date, $end_date);

        //        echo '<pre>';
        //        print_r($resultar);die();


        $newar = $resarr = array();

        foreach ($resultar as $key => $result) {
            foreach ($result as $key1 => $res) {
                //$device_id = $res['device_id'];
                $resource_id = $res['resource_id'];
                //$newar[$key][$resource_id][] = $res;
                $newar[$resource_id][$resource_id][] = $res;
            }
        }

        //        echo '<pre>';
        //        print_r($newar);die();

        $shifttime = "08:00:00";
        $shiftdevicear = array();
        $ottime = 0;
        if ($newar != NULL) {

            foreach ($newar as $keyuser => $user) {
                $otgndtotal = 0;

                foreach ($user as $key2 => $devicear) {
                    $countp = 0;
                    $ottotal = 0;
                    $otimes = "";
                    //$othrs = 0;

                    if ($key2 != '') {
                        foreach ($devicear as $key3 => $device) {
                            //                            echo '<pre>';
                            //                            print_r($device);

                            $othrs = $device['othours'];
                            if (($othrs != '') && ($othrs != '05:30:00') && ($device['first_punch'] != $device['last_punch'])) {
                                // echo '----'.$othrs;
                                $otimes .= ',' . $device['othours'];
                                $seconds = strtotime($othrs) - strtotime('TODAY');
                                //$seconds = mktime($hours,$mins,$secs) - mktime(0,0,0);
                                $ottotal += $seconds;
                                $otgndtotal += $seconds;
                                //echo '-------'.$this->calculate_time_span($ottotal);
                            }



                            if (strtotime($device['inhrs']) >= strtotime($shifttime)) {

                                $countp++;
                            }
                        }
                        // echo '----------------' . $countp;
                    }
                    //                    echo $keyuser.'----'; 
                    //                    echo $key2; 
                    //                    echo "<br>";
                    $shiftdevicear[$key2][$key2]['othrs'] = $this->calculate_time_span($ottotal);
                    $shiftdevicear[$key2][$key2]['totalpunch'] = $countp;
                    $shiftdevicear[$key2][$key2]['otimes'] = $otimes;
                    $shiftdevicear[$key2]['totalot'] = $this->calculate_time_span($otgndtotal);
                    //                     echo '<br>';
                    //                     echo $ottotal;
                }
            }
        }

        //        echo '<pre>';
        //        print_r($shiftdevicear);
        //        die();

        $resarr = array();
        //$users = Yii::app()->db->createCommand("SELECT us.*, dept.department_name FROM {$tblpx}users as us"
        //. " LEFT JOIN {$tblpx}department as dept ON dept.department_id = us.department_id "
        //. " WHERE us.status = 0  ORDER BY dept.department_id")->queryAll(); //echo "<pre>"; print_r($users);and accesscard_id = 42

        $usercondition = '';
        if (empty($_REQUEST['code'])) {
            $usercondition = 'GROUP BY us.userid';

            $sql = "SELECT us.*,m.site_id,site_name,m.added_date FROM {$tblpx}users as us"
                . " LEFT JOIN {$tblpx}manualentry as m ON m.user_id = us.userid AND m.added_date BETWEEN '$start_date' AND '$end_date'"
                . " left join {$tblpx}clientsite cs on cs.id=m.site_id  "
                . " $usercondition ORDER BY m.id";
            //die;
            $users = Yii::app()->db->createCommand($sql)->queryAll();
        } else {
            $code = $_REQUEST['code'];
            $usercondition = 'GROUP BY us.userid';

            $sql = "SELECT us.*,pd.site_id AS site_id,site_name FROM {$tblpx}users as us"
                //. " LEFT JOIN {$tblpx}manualentry as m ON m.user_id = us.userid AND m.added_date BETWEEN '$start_date' AND '$end_date' "
                . " INNER JOIN {$tblpx}device_accessids da ON da.userid = us.userid"
                . " INNER JOIN {$tblpx}punching_devices pd ON da.deviceid = pd.device_id AND pd.site_id = $code"
                . " AND pd.start_date BETWEEN '$start_date' AND '$end_date'  left join {$tblpx}clientsite cs on cs.id=m.site_id  "
                . " $usercondition ORDER BY us.userid";

            $users = Yii::app()->db->createCommand($sql)->queryAll();
        }
        //
        //      echo "<pre>";
        //      print_r($users); die;


        $devicesdata = Devices::model()->findAll();
        $devices = array();

        foreach ($devicesdata as $devicedata) {
            $devices[$devicedata->device_id]['id'] = $devicedata->device_id;
            $devices[$devicedata->device_id]['device_name'] = $devicedata->device_name;
        }


        //        echo '<pre>';
        //        print_r($users);
        //        die();



        if (!empty($users)) {
            $j = 0;
            $totpunch = 0;
            $resarr = array();

            foreach ($users as $user) {

                //               echo "<pre>";
                //               print_r($user);
                //               die;
                $resarr[$j]['totalpunch'] = 0;
                $resarr[$j]['totalpunch1'] = 0;
                $resarr[$j]['total_ot1'] = 0;
                $accesscardid = $user['userid']; //$accesscardid = 58;
                $empid = $user['userid'];


                if ($shiftdevicear != NULL) {
                    foreach ($shiftdevicear as $keyuser => $ottime) {
                        if ($accesscardid == $keyuser) {
                            foreach ($ottime as $keytime => $ott) {
                                //                                echo '<pre>';
                                //                                print_r($ott);die();
                                if (is_numeric($keytime)) {
                                    // echo '<pre>';
                                    // print_r($ott);

                                    $resarr[$j]["total_ot_" . $keytime] = $ott['othrs'];
                                    //$resarr[$j]['totalpunch_' . $keytime] = $ott['totalpunch'];
                                    $resarr[$j]['totalpunch_' . $keytime] = $ott['totalpunch'];
                                    $resarr[$j]['totalpunch1'] = $resarr[$j]['totalpunch1'] + $ott['totalpunch'];
                                    $resarr[$j]['total_ot1'] = $ott['othrs'];
                                } else {
                                    $resarr[$j]['totalot'] = $ott;
                                }
                            }
                            continue;
                        }
                    }
                }

                $resarr[$j]['empid'] = $accesscardid;
                $resarr[$j]['employee_id'] = $user['employee_id'];
                $resarr[$j]['name'] = $user['first_name'] . ' ' . $user['last_name'];
                $resarr[$j]['designation'] = $user['designation'];
                $resarr[$j]['site_name'] = $user['site_name'];
                $resarr[$j]['site_id'] = $user['site_id'];
                // $resarr[$j]['department_id'] = $user['department_id'];
                //$resarr[$j]['department_name'] = $user['department_name'];
                $j++;
            }
        }

        //     echo "<pre>";    
        //    print_r($resarr);
        //    die;

        if (!empty($_REQUEST['code']) && $_REQUEST['code'] > 0) {
            $code = $_REQUEST['code'];
            $newarr = $this->search_array($resarr, $code);
            $resarr = $newarr;
        }

        function compareByName($a, $b)
        {
            return strcmp($a["name"], $b["name"]);
        }

        usort($resarr, 'compareByName');

        //echo "<pre>";    
        //print_r($resarr);
        //die;  

        $totalOTsec = '';
        for ($k = 0; $k < count($resarr); $k++) {
            $time = (isset($resarr[$k]['totalot']) ? $resarr[$k]['totalot'] : '0:0:0') . '<br>';
            $parsed = date_parse($time);
            $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'] . '<br>';
            $totalOTsec = $totalOTsec + $seconds;
        }

        if (!empty($totalOTsec)) {
            $totalhrs = gmdate('H:i:s', $totalOTsec);
        } else {
            $totalhrs = '00:00:00';
        }



        $sitesql = "SELECT * FROM {$tblpx}clientsite";
        $sitedetails = Yii::app()->db->createCommand($sitesql);
        $sitedata = $sitedetails->queryAll();

        $this->render("attendancereport", array('users' => $users, 'start_date' => $start_date, 'end_date' => $end_date, 'results' => $resarr, 'devices' => $devices, 'total_devices' => $total_devices, 'sitedata' => $sitedata, 'code' => $code, 'totalhrs' => $totalhrs));
    }

    public function search_array($array, $needle)
    {
        $results = array();

        foreach ($array as $subarray) {
            if (array_search($needle, $subarray, true) !== false) {
                $results[] = $subarray;
            }
        }

        return $results;
    }

    public function actionDailyreport()
    {
        $newdate = '2017-06-27';

        $result_arr[$newdate] = $this->calcHours2($newdate);
        //    echo "<pre>";
        //    print_r($result_arr);
        //    die;

        foreach ($result_arr as $key => $value) {

            foreach ($value as $key1 => $value1) {

                // print_r("<pre>");
                // print_r($value1);
                // die;

                $empid = $value1['accesscard_id'];
                $empname = $value1['emp_name'];
                $date = date('Y-m-d', strtotime($value1['first_punch']));
                //$final_arr[$empid][$empid.'-'.$date]   =   $value1;
                //$final_arr[$empid][]   =   $value1;

                if (empty($value1['first_punch'])) {
                    $logdate = date('Y-m-d', strtotime($key));
                } else {
                    $logdate = date('Y-m-d', strtotime($value1['first_punch']));
                }

                // print_r("<pre>");
                // print_r($value1);
                // print_r("</pre>");
                // die;

                $resource_id = $value1['accesscard_id'];
                $intime = $value1['inhrs'];
                $outtime = $value1['outhrs'];
                $totaltime = gmdate('H:i:s', $value1['insec'] + $value1['outsec']);


                $late_halfday = $date . " 11:00:01";
                $ee_halfday = $date . " 16:00:00";

                $HL = 'HL';

                if (strtotime($value1['first_punch']) > strtotime($late_halfday)) {
                    $value1['first_punch'] = date('H:i:s', strtotime($value1['first_punch'])) . " " . $HL;
                } else {
                    $value1['first_punch'] = date('H:i:s', strtotime($value1['first_punch']));
                }

                if (strtotime($value1['last_punch']) < strtotime($ee_halfday) && date('H:i:s', strtotime($value1['last_punch'])) != '05:30:00') {
                    $value1['last_punch'] = date('H:i:s', strtotime($value1['last_punch'])) . " " . $HL;
                } else {
                    $value1['last_punch'] = date('H:i:s', strtotime($value1['last_punch']));
                }


                $firstpunch = $value1['first_punch'] == '' ? '' : $value1['first_punch'];
                $lastpunch = $value1['last_punch'] == '' ? '' : $value1['last_punch'];



                $sql = "INSERT INTO pms_punchreport (logdate,resource_id,firstpunch,lastpunch,intime,outtime,totaltime) VALUES (:logdate,:resource_id,:firstpunch,:lastpunch,:intime,:outtime,:totaltime)";
                $parameters = array(":logdate" => $logdate, ":resource_id" => $resource_id, ":firstpunch" => $firstpunch, ":lastpunch" => $lastpunch, ":intime" => $intime, ":outtime" => $outtime, ":totaltime" => $totaltime);
                Yii::app()->db->createCommand($sql)->execute($parameters);
            }
            continue;
        }

        $this->render("dailyreport");
    }

    public function x_week_range($date)
    {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 1) ? $ts : strtotime('first day of this month', $ts);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('last day of this month', $start)), date('F d, Y', $start));
    }

    function getcountBetween2Dates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return count($days);
    }

    public function generate($sdate = "", $edate = "")
    {
        $tblpx = Yii::app()->db->tablePrefix;

        if (!empty($sdate) && !empty($edate)) {
            $newsdate = $sdate;
            $newedate = $edate;


            $date1 = date_create($newsdate);
            $date2 = date_create($newedate);

            $diff = date_diff($date1, $date2);
            $datecnt2 = $diff->format("%a");

            $startdate = $date1->format('Y-m-d');
            //$startdate = "2017-02-01";

            for ($j = 0; $j <= $datecnt2; $j++) {
                $newdate = $startdate;
                $result_arr[$newdate] = $this->calcHours2new($newdate);
                $date1->modify('+1 day');
                $startdate = $date1->format('Y-m-d');
            }
            //            echo '<pre>';
            //            print_r($result_arr);
            //            die();
            //$result_arr = $this->calculatetime($result_arr);
            $result_arr = $this->newotcalculation($result_arr);

            //            echo '<pre>';
            //            print_r($result_arr);
            //            die();

            $groupar = array();
            foreach ($result_arr as $key => $result) {

                foreach ($result as $key1 => $value) {


                    // if ($value['device_id'] != '') {
                    $groupar[$key1][$key]['logdate'] = $key;
                    $groupar[$key1][$key]['resource_id'] = $value['userid'];
                    $groupar[$key1][$key]['device_id'] = $value['device_id'];
                    $groupar[$key1][$key]['first_punch'] = $value['first_punch'];
                    $groupar[$key1][$key]['last_punch'] = $value['last_punch'];
                    $groupar[$key1][$key]['inhrs'] = $value['inhrs'];
                    $groupar[$key1][$key]['outhrs'] = $value['outhrs'];
                    $groupar[$key1][$key]['othours'] = $value['othours'];

                    if ($value['first_punch'] != $value['last_punch']) {
                        $groupar[$key1][$key]['attn'] = 1;
                    } else {
                        $groupar[$key1][$key]['attn'] = 0;
                    }
                    $groupar[$key1][$key]['site_id'] = $value['site_id'];
                    //}
                }
            }
            //            echo '<pre>';
            //            print_r($result_arr);
            //            die();
            $resultmon = array();

            foreach ($groupar as $keyval => $finalresult) {
                foreach ($finalresult as $keydate => $final) {
                    $resultmon[$keyval][$keydate]['logdate'] = $final['logdate'];
                    $resultmon[$keyval][$keydate]['resource_id'] = $final['resource_id'];
                    $resultmon[$keyval][$keydate]['device_id'] = $final['device_id'];
                    $resultmon[$keyval][$keydate]['first_punch'] = $final['first_punch'];
                    $resultmon[$keyval][$keydate]['last_punch'] = $final['last_punch'];
                    $resultmon[$keyval][$keydate]['inhrs'] = $final['inhrs'];
                    $resultmon[$keyval][$keydate]['outhrs'] = $final['outhrs'];
                    $resultmon[$keyval][$keydate]['first_punch'] = $final['first_punch'];

                    $workhours = "08:00:00";
                    $intime = $final['inhrs'];
                    if (strtotime($intime) > strtotime($workhours) && $intime != "18:30:00") {

                        $now = new DateTime($intime);
                        $then = new DateTime($workhours);
                        $diff = $now->diff($then);
                        $hours = $diff->format('%h');
                        $min = $hours * 60;
                        $minutes = $diff->format('%i');
                        $seconds = $diff->format('%s');
                        $othrs_total = $hours . ":" . $minutes . ":" . $seconds;
                        $otmin = $min + $minutes;
                    } else {
                        $othrs_total = NULL;
                        $otmin = 0;
                    }
                    $insecs = strtotime($final['inhrs']) - strtotime('TODAY');
                    $outsecs = strtotime($final['outhrs']) - strtotime('TODAY');
                    $resultmon[$keyval][$keydate]['totaltime'] = gmdate("H:i:s", strtotime($insecs + $outsecs));
                    $resultmon[$keyval][$keydate]['othours'] = $othrs_total;
                }
            }


            //            echo '<pre>';
            //            print_r($groupar);
            //            die();

            return $groupar;
        }
    }


    public function getsitename($device_id)
    {
    }

    public function Newotcalculation($result_arr = array(), $resource_id = "")
    {
        $result = array();
        foreach ($result_arr as $key => $value) {
            foreach ($value as $key1 => $value1) {
                if ($value1['first_punch'] == $value1['last_punch']) {
                    $value1['insec'] = 0;
                }
                if (($resource_id != "") && ($value1['accesscard_id'] != $resource_id)) {
                    break;
                }


                $empid = $value1['accesscard_id'];
                $empname = $value1['emp_name'];
                $date = date('Y-m-d', strtotime($value1['first_punch']));
                if (empty($value1['first_punch'])) {
                    $logdate = date('Y-m-d', strtotime($key));
                } else {
                    $logdate = date('Y-m-d', strtotime($value1['first_punch']));
                }
                //$resource_id = $value1['accesscard_id'];
                $intime = $intimesec = $value1['insec'];
                $outtime = $value1['outsec'];
                $totaltime = gmdate('H:i:s', $value1['insec'] + $value1['outsec']);
                //                $late_halfday = $date . " 11:00:01";
                //                $ee_halfday = $date . " 16:00:00";
                //                $starttime = $date . " 10:00:00";
                //                $endtime = $date . " 18:00:00";
                //                $HL = 'HL';
                $ot_formula = 0;
                $othrs_total = 0;
                $uid = $value1['userid'];
                $user = Users::model()->find(array('condition' => "userid = $uid"));

                $role = $user['user_type'];
                $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));
                //PT
                if (!empty($rsettings)) {
                    $ot_formula = $rsettings->ot_formula;
                    if ($ot_formula == 1) {
                        $othrs_total = 0;

                        if ($intime > (8 * 60 * 60)) {
                            $newTot = ($intime + (1 * 60 * 60)) - $outtime;
                            $newOT = ($newTot - (9 * 60 * 60));
                            if ($newOT > 0) {
                                $othrs_total = gmdate('H:i:s', $newOT);
                            }
                        } else {
                            $othrs_total = 0;
                        }
                    } else {
                        $othrs_total = 0;
                        //                 $workhours = "08:00:00";
                        //                        $workhours= strtotime('08:00:00') - strtotime('TODAY');
                        //                        if ($intimesec > $workhours && $intime != "18:30:00") {
                        //                           $seconds = $intimesec -$workhours;
                        //                            $othrs_total = gmdate("H:i:s",$seconds);
                        //                        } else {
                        //                            $othrs_total = 0;
                        //                            $otmin = 0;
                        //                        }    
                    }
                }
                $firstpunch = $value1['first_punch'] == '' ? '' : $value1['first_punch'];
                $lastpunch = $value1['last_punch'] == '' ? '' : $value1['last_punch'];
                $value1['intime'] = $intime;
                $value1['outtime'] = $outtime;
                $value1['totaltime'] = $totaltime;
                $value1['othours'] = $othrs_total;
                $result[$key][$key1] = $value1;
            }
            continue;
        }
        return $result;
    }

    public function Calculatetime($result_arr = array(), $resource_id = "")
    {
        //echo "<pre>";print_r($result_arr);die;
        $result = array();
        foreach ($result_arr as $key => $value) {
            foreach ($value as $key1 => $value1) {
                if ($value1['first_punch'] == $value1['last_punch']) {
                    $value1['insec'] = 0;
                }

                //                echo '<pre>';
                //                print_r($value1);   
                //                die();

                if (($resource_id != "") && ($value1['accesscard_id'] != $resource_id)) {
                    break;
                }
                $empid = $value1['accesscard_id'];
                $empname = $value1['emp_name'];
                $date = date('Y-m-d', strtotime($value1['first_punch']));
                //$final_arr[$empid][$empid.'-'.$date]   =   $value1;
                //$final_arr[$empid][]   =   $value1;

                if (empty($value1['first_punch'])) {
                    $logdate = date('Y-m-d', strtotime($key));
                } else {
                    $logdate = date('Y-m-d', strtotime($value1['first_punch']));
                }
                //$resource_id = $value1['accesscard_id'];
                $intime = $intimesec = $value1['insec'];
                $outtime = $value1['outsec'];
                $totaltime = gmdate('H:i:s', $value1['insec'] + $value1['outsec']);

                $late_halfday = $date . " 11:00:01";
                $ee_halfday = $date . " 16:00:00";

                $starttime = $date . " 10:00:00";
                $endtime = $date . " 18:00:00";

                $HL = 'HL';


                /* apply role settings here */
                $ot_formula = 0;
                $othrs_total = 0;



                $uid = $value1['userid'];
                $user = Users::model()->find(array('condition' => "userid = $uid"));

                $role = $user['user_type'];
                $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));
                //                echo '<pre>';
                //                print_r($rsettings);//die();
                //echo '<br>empid--------'.$empid.'firstpunch'.$value1['first_punch'].'last_punch'.$value1['last_punch'];
                //                echo '<pre>';
                //                print_r($rsettings);
                //                
                if (!empty($rsettings)) {



                    //OT
                    $ot_formula = $rsettings->ot_formula;
                    $min_othrs = 0;
                    $max_othrs = 1440;
                    if ($ot_formula == 1) {
                        $min_othrs = $rsettings->min_ot;
                        if ($rsettings->max_ot_yes) {
                            $max_othrs = $rsettings->max_ot;
                        }
                    }


                    //Consider Early coming punch & Late going punch
                    $early_coming_punch = $rsettings->early_coming_punch;
                    $late_going_punch = $rsettings->late_going_punch;

                    $intime = gmdate("H:i:s", $intime);
                    //Deduct break hours from work duration
                    $deduct_break = $rsettings->deduct_breakhours;
                    if ($deduct_break == 1) {
                        $intime = $intimesec = $intime - (strtotime('01:00:00') - strtotime('TODAY'));
                        $intime = gmdate("H:i:s", $intime);
                    }

                    //echo 'dfdf'.$intime;
                    $diff = (strtotime($endtime) - strtotime($starttime));


                    if ($value1['insec'] >= $diff) { //total intime should be 8 hours then only ot calculation will work
                        //OT hours start
                        $total_ot = 0;
                        $min_ot1 = 0;
                        $min_ot2 = 0;
                        if ($ot_formula == 1) {
                            $othrs_total = 0;
                            $ot1 = 0;
                            $ot2 = 0;
                            if ($early_coming_punch == 1) {
                                $min_ot1 = 0;
                                $fpunch = str_replace("HL", "", $value1['first_punch']);
                                if (strtotime($fpunch) < strtotime($starttime)) {
                                    $min_ot1 = strtotime($starttime) - strtotime($fpunch);
                                }
                            }
                            if ($late_going_punch == 1) {
                                $min_ot2 = 0;
                                $lpunch = str_replace("HL", "", $value1['last_punch']);
                                // echo '<br>first lpunch---'.$lpunch;
                                if (strtotime($lpunch) > strtotime($endtime)) {
                                    $min_ot2 = strtotime($lpunch) - strtotime($endtime);
                                }
                            }
                            $total_ot = gmdate("H:i:s", ($min_ot1 + $min_ot2));

                            if (($this->minutes($total_ot) > $min_othrs) && ($this->minutes($total_ot) < $max_othrs)) {
                                // echo '<br>'.$this->minutes($total_ot).'ms'.$max_othrs;
                                $othrs_total = $total_ot;
                            } else if ($this->minutes($total_ot) > $max_othrs) {
                                $othrs_total = gmdate("H:i:s", ($max_othrs * 60));
                            } else {
                                $othrs_total = 0;
                            }
                            //  echo '<br>first----'.$empid.'-------'.$key.'---------intime'.gmdate("H:i:s",$intimesec).'overtime'.$othrs_total ;
                        } else {
                            $workhours = "08:00:00";
                            $workhours = strtotime('08:00:00') - strtotime('TODAY');
                            if ($intimesec > $workhours && $intime != "18:30:00") {
                                $seconds = $intimesec - $workhours;
                                $othrs_total = gmdate("H:i:s", $seconds);
                            } else {
                                $othrs_total = 0;
                                $otmin = 0;
                            }
                            // echo '<br>second lpunch---'. $value1['last_punch'];
                            // echo '<br>second----'.$empid.'-------'.$key.'---------intime'.gmdate("H:i:s",$intimesec).'workinghrs'.gmdate("H:i:s",$workhours).'overtime'.$othrs_total ;
                        }
                        // end OT calculations
                    } else {
                        $othrs_total = 0;
                    }
                    /* apply role settings here--end */
                }
                $firstpunch = $value1['first_punch'] == '' ? '' : $value1['first_punch'];
                $lastpunch = $value1['last_punch'] == '' ? '' : $value1['last_punch'];
                $value1['intime'] = $intime;
                $value1['outtime'] = $outtime;
                $value1['totaltime'] = $totaltime;
                $value1['othours'] = $othrs_total;
                $result[$key][$key1] = $value1;
                //            echo '<pre>';
                //            print_r($result);die();
                //            return $result;
            }
            continue;
        }
        //        echo '<pre>';
        //        print_r($result);die();
        return $result;
    }

    function minutes($time)
    {
        $time = explode(':', $time);
        return ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
    }

    public function calcHours2($logdate)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $format = 'Y-m-d';
        // $logdate    =  '2017-02-01';  //$_REQUEST['logdate'];
        $usersTS = strtotime($logdate);
        $logdatenxt = date($format, strtotime('+1 day', $usersTS)); //'2017-02-28';//
        $predate = $logdate;
        $nextdate = $logdatenxt;




        $datetimesettings = array();
        $datetimesettings['shift_time_starts'] = "07:00:00";
        $datetimesettings['shift_end'] = "05:00:00";

        $datetimesettings['date'] = $predate;

        $datetimesettings['nextday'] = $nextdate;

        $datetimesettings['in_late'] = "$predate 09:30:00";
        $datetimesettings['out_early'] = "$predate 16:00:00";
        $datetimesettings['shift_ends'] = "$predate 23:59:00";

        $datetimesettings['late_halfday'] = "11:00:01"; //First punch Half day
        $datetimesettings['ee_halfday'] = "16:00:00";  //Early Exit Half day

        extract($datetimesettings);


        $sql = "SELECT al.*, u.userid,concat_ws(' ',first_name,last_name) as full_name, u.status,reg_ip, accesscard_id,reg_ip,"
            . "if(userid='" . Yii::app()->user->id . "',1,0) AS matchip FROM (SELECT logid ,sqllogid ,empid , log_time,device_id "
            . "FROM {$tblpx}accesslog WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
            . " AND  (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end')) AS al "
            . "RIGHT JOIN {$tblpx}users AS u ON empid=accesscard_id WHERE u.status=0 && accesscard_id!='' && log_time!=''   GROUP BY userid, log_time ORDER BY matchip DESC, "
            . "full_name ASC,userid ASC,device_id ASC,log_time ASC";


        $logdetails = Yii::app()->db->createCommand($sql);
        $logemp = $logdetails->queryAll();


        /* $devicecomp = "SELECT concat('',deviceid,dev.accesscard_id) as accesscard_id,dev.userid FROM `{$tblpx}device_accessids` as dev "
          . "INNER JOIN {$tblpx}users ON {$tblpx}users.userid=dev.userid ORDER BY dev.userid ASC";
          $devicedetails = Yii::app()->db->createCommand($devicecomp);
          $devices = $devicedetails->queryAll();

          $devfinal=array();
          if($devices!=NULL){
          foreach($devices as $key=>$dev){
          if($dev['userid']==$devices[$key+1]['userid']){
          $user= $dev['userid'];
          }
          }
          }

          echo '<pre>';
          print_r($devfinal);die();


          $sql = "SELECT *,concat('',device_id,empid) as  accesscomp "
          . "FROM {$tblpx}accesslog WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
          . "GROUP BY accesscomp ORDER BY device_id ASC,log_time ASC";

          //        echo '<pre>';
          //        print_r($logemp);
          //        die(); */
        $difflog = array();

        $inout = array(0, 0);
        $result_log = array();

        $allpunch = array();
        $allpunch1 = array();
        $allpunch2 = array();
        $allpunch3 = array();
        $useridarr = array();
        $usrflag = array();

        $empidpre = "";
        $device_idpre = "";
        $addinsecs = $addoutsecs = 0;
        foreach ($logemp as $k => $row) {
            $userid = $row['userid'];
            $empid = $row['accesscard_id'];
            $device_id = $row['device_id'];
            $till_time = 0;
            $allpunch["user_" . $empid][] = date('h:i:s', strtotime($row['log_time']));


            if (!isset($result_log[$empid]['accesscard_id']) && (!(isset($result_log[$empid]['device_id'])) || (isset($device_id) && $device_id != $row['device_id']))) {

                if (($empidpre != $empid) || ($device_idpre != $device_id)) {
                    $addinsecs = $addoutsecs = 0;
                    if ((!isset($punch_time)) || (isset($punch_time) && ($punch_time > $row['log_time']))) {
                        $fpid = $row['device_id'];
                    }

                    $punch_time = $prev_value = $row['log_time'];
                    $empidpre = $empid;
                    $device_idpre = $device_id;
                    $result_log[$empid]['fpid'] = $fpid;
                    $result_log[$empid][$device_id]['insec'] = 0;
                    $result_log[$empid][$device_id]['inhrs'] = 0;
                    $result_log[$empid][$device_id]['outsec'] = 0;
                    $result_log[$empid][$device_id]['outhrs'] = 0;
                    $result_log[$empid][$device_id]['inpunches'] = '';
                    $result_log[$empid][$device_id]['outpunches'] = '';
                    $i = 1;
                }



                $result_log[$empid][$device_id]['resource_id'] = $row['userid'];
                $result_log[$empid][$device_id]['device_id'] = $row['device_id'];
                $result_log[$empid][$device_id]['accesscard_id'] = $row['accesscard_id'];
                $result_log[$empid][$device_id]['first_punch'] = $punch_time;
                $result_log[$empid][$device_id]['last_punch'] = $row['log_time'];
                $result_log[$empid][$device_id]['emp_name'] = $row['full_name'];
            }

            $result_log[$empid][$device_id]['last_punch'] = $row['log_time'];

            if (($i % 2 == 1) && (isset($logemp[$k + 1]['accesscard_id']) && ($empid == $logemp[$k + 1]['accesscard_id'])) && (isset($logemp[$k + 1]['device_id']) && ($device_idpre == $logemp[$k + 1]['device_id']))) {
                if ($i > 1) {
                    $prevsentry = strtotime($row['log_time']);
                } else {
                    $prevsentry = strtotime($punch_time);
                }

                $result_log[$empid][$device_id]['inpunches'] = $result_log[$empid][$device_id]['inpunches'] . ',' . $logemp[$k + 1]['log_time'];
                $addinsecs = $result_log[$empid][$device_id]['insec'] = ((strtotime($logemp[$k + 1]['log_time']) - $prevsentry)) + $addinsecs;
                $result_log[$empid][$device_id]['inhrs'] = gmdate('H:i:s', $result_log[$empid][$device_id]['insec']);
            } else if ((isset($logemp[$k + 1]['accesscard_id']) && ($empid == $logemp[$k + 1]['accesscard_id'])) && (isset($logemp[$k + 1]['device_id']) && ($device_idpre == $logemp[$k + 1]['device_id']))) {
                $result_log[$empid][$device_id]['outpunches'] = $result_log[$empid][$device_id]['outpunches'] . ',' . $logemp[$k + 1]['log_time'];
                $addoutsecs = $result_log[$empid][$device_id]['outsec'] = (strtotime($logemp[$k + 1]['log_time']) - strtotime($row['log_time'])) + $addoutsecs;
                $result_log[$empid][$device_id]['outhrs'] = gmdate('H:i:s', $result_log[$empid][$device_id]['outsec']);
            } elseif ($i == 1) {

                $logemp[$k + 1]['log_time'] = "$date 23:59:00";
                $prevsentry = strtotime($punch_time);

                //$result_log[$empid][$device_id]['inpunches'] = $result_log[$empid][$device_id]['inpunches'] . ',' . $logemp[$k + 1]['log_time'];
                $addinsecs = $result_log[$empid][$device_id]['insec'] = ((strtotime($logemp[$k + 1]['log_time']) - $prevsentry)) + $addinsecs;
                $result_log[$empid][$device_id]['inhrs'] = gmdate('H:i:s', $result_log[$empid][$device_id]['insec']);
                $result_log[$empid][$device_id]['outhrs'] = gmdate('H:i:s', 0);
            }
            $i++;
        }

        $noentries = 0;
        if (isset($i)) {
            //$result_log[$empid]['status'] = $i % 2;
            $noentries = 1;
        }
        //         echo '<pre>';
        //        print_r($result_log);
        // die();
        $result_log = $this->devicecal($result_log);
        //echo '<pre>';
        //print_r($result_log); die();
        return $result_log;
    }

    public function calcHours2new($logdate)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $format = 'Y-m-d';
        //$logdate    =  '2017-07-03';  //$_REQUEST['logdate'];
        $usersTS = strtotime($logdate);
        $logdatenxt = date($format, strtotime('+1 day', $usersTS)); //'2017-02-28';//
        $predate = $logdate;
        $nextdate = $logdatenxt;
        $datetimesettings = array();
        $datetimesettings['shift_time_starts'] = "07:00:00";
        $datetimesettings['shift_end'] = "05:00:00";

        $datetimesettings['date'] = $predate;

        $datetimesettings['nextday'] = $nextdate;

        $datetimesettings['in_late'] = "$predate 09:30:00";
        $datetimesettings['out_early'] = "$predate 16:00:00";
        $datetimesettings['shift_ends'] = "$predate 23:59:00";

        $datetimesettings['late_halfday'] = "11:00:01"; //First punch Half day
        $datetimesettings['ee_halfday'] = "16:00:00";  //Early Exit Half day

        extract($datetimesettings);

        //        $sql = "SELECT al.*, u.userid,concat_ws(' ',first_name,last_name) as full_name, u.status,reg_ip, accesscard_id,reg_ip,"
        //                . "if(userid='" . Yii::app()->user->id . "',1,0) AS matchip FROM (SELECT logid ,sqllogid ,empid , log_time,device_id "
        //                . "FROM {$tblpx}accesslog WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
        //                . " AND  (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end')) AS al "
        //                . "RIGHT JOIN {$tblpx}users AS u ON empid=accesscard_id "
        //                . "WHERE u.status=0 && accesscard_id!='' && log_time!=''   "
        //                . "GROUP BY userid, log_time ORDER BY matchip DESC, "
        //                . "full_name ASC,userid ASC,device_id ASC,log_time ASC";
        //                

        $punching_devices = Devices::model()->findAll();
        $devices = array();
        foreach ($punching_devices as $pdevice) {
            $devices[$pdevice->device_id] = $pdevice->device_name;
        }

        //$punchtable = Yii::app()->db->tablePrefix.'accesslog';
        $punchtable = 'pms_punch_log';
        $tbl = Yii::app()->db->tablePrefix;

        $devid = 0;
        if (isset(Yii::app()->user->device_id) and Yii::app()->user->device_id > 0) {
            $devid = Yii::app()->user->device_id;
        }

        $excludeemp = '';
        if (yii::app()->user->role != 1) {
            // $excludeemp = ' and empid not in (4,5,27) ';
        }

        $by_device = '';
        if ($devid > 0) {
            $by_device = ' where device_id=' . $devid;
        }

        $sitecondition = '';
        if (isset($_REQUEST['code']) && $_REQUEST['code'] > 0) {
            $code = $_REQUEST['code'];
            //        $start_date = Yii::app()->session['start_date'];
            //        $end_date = Yii::app()->session['end_date'];                
            //        $sitecondition = " AND m.site_id = '$code' "
            //        . "AND m.added_date BETWEEN '$start_date' AND '$end_date' ";  
            $sitecondition = "WHERE if(al.manual_entry_status IS NULL,pd.site_id = '$code',m.site_id = '$code' )";
        } else {
            $code = 1;
        }

        //$sitecondition = " INNER JOIN {$tbl}punching_devices pd ON dv.deviceid = pd.device_id"        
        //. " AND pd.site_id = '$code'";                                   

        $setbigselects = Yii::app()->db->createCommand("SET SQL_BIG_SELECTS=1")->execute();  //quick fix for - Syntax error or access violation: 1104 The SELECT would examine more than MAX_JOIN_SIZE rows; check your WHERE and use SET SQL_BIG_SELECTS=1 or SET MAX_JOIN_SIZE=# if the SELECT is okay. 

        $sql = "SELECT al.*,pd.*,al.device_id,if(al.manual_entry_status IS NULL,pd.site_id,m.site_id) AS site_id,"
            . "dv.userid,concat_ws(' ',first_name,last_name) as full_name,"
            . " u.status,reg_ip, dv.accesscard_id,reg_ip,empid, "
            . "if(dv.userid='" . Yii::app()->user->id . "',1,0) AS matchip  FROM (SELECT logid ,sqllogid ,empid , log_time, device_id,manual_entry_status "
            . "FROM {$punchtable} WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
            . " AND  (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end' ) $excludeemp  ) AS al "
            . "Left JOIN pms_device_accessids AS dv ON dv.accesscard_id=empid "
            . "INNER JOIN pms_users AS u ON u.userid=dv.userid LEFT JOIN {$tbl}punching_devices pd"
            . " ON al.device_id = pd.device_id "
            . "AND ((DATE(al.log_time) >= pd.start_date AND DATE(al.log_time) <= pd.end_date) OR (DATE(al.log_time) >= pd.start_date)) "
            . "AND pd.site_id = '$code'"
            . " LEFT JOIN {$tbl}manualentry m "
            . "ON al.manual_entry_status = m.id AND m.site_id = '$code'  "
            . "$by_device $sitecondition  GROUP BY empid, log_time ORDER BY matchip DESC, full_name ASC,empid ASC,log_time ASC ";

        $logdetails = Yii::app()->db->createCommand($sql);
        $logemp = $logdetails->queryAll();

        //        echo "<pre>";
        //        print_r($logemp);
        //        die;

        $difflog = array();
        $inout = array(0, 0);
        $result_log = array();

        $allpunch = array();
        foreach ($logemp as $k => $row) {
            //            echo '<br />';
            $empid = $row['empid'];
            $result_log[$empid]['outearly'] = 0;
            $till_time = 0;


            $device_name = ((isset($row['device_id']) and intval($row['device_id']) != 0) ? "(" . (isset($devices[$row['device_id']]) ? $devices[$row['device_id']] : '') . ") " : '');
            $log_time = ($row['log_time'] != '' ? $row['log_time'] : '0');
            $allpunch["user_" . $empid][] = $device_name . date("h:i:s ", strtotime($log_time));
            //            echo '<br />';

            if (!isset($result_log[$empid]['accesscard_id'])) {
                $punch_time = $prev_value = $row['log_time'];
                $i = 1;
                $result_log[$empid]['accesscard_id'] = $row['empid'];;
                $result_log[$empid]['first_punch'] = $punch_time;
                $result_log[$empid]['last_punch'] = $row['log_time'];
                $result_log[$empid]['emp_name'] = ($row['full_name'] == '' ? $row['empid'] : $row['full_name']);
                $result_log[$empid]['userid'] = $row['userid'];
                //$result_log[$empid]['ip'] = $row['reg_ip'];
                $result_log[$empid]['count'] = $i;
                //$result_log[$empid]['reg_ip'] = $row['reg_ip'];
                //$result_log[$empid]['myip'] = $row['matchip'];
                $result_log[$empid]['device_id'] = $row['device_id'];
                $result_log[$empid]['site_id'] = $row['site_id'];
                if (!empty($row['manual_entry_status'])) {
                    $result_log[$empid]['entrytype'] = $row['manual_entry_status'];
                } else {
                    $result_log[$empid]['entrytype'] = NULL;
                }



                //                echo $result_log[$empid]['accesscard_id']."=====".$in_late."-----------==".$row['log_time'];
                //                echo '<br />';
                //                die();

                if (strtotime($in_late) < strtotime($row['log_time'])) {
                    $result_log[$empid]['inlate'] = 1;
                }

                $result_log[$empid]['insec'] = strtotime(date("Y-m-d")) - strtotime($row['log_time']);

                if (!isset($logemp[$k + 1]) or ($logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {
                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$empid]['insec'] = $till_time;
                }

                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['outsec'] = 0;
                $result_log[$empid]['outhrs'] = 0;

                $result_log[$empid]['status'] = $i;
                //  echo "<h1>first-IN-$i</h1>";
                $result_log[$empid]['status'] = 1;
                continue;
            }
            $result_log[$empid]['ldevice_id'] = $row['device_id'];
            $result_log[$empid]['last_punch'] = $row['log_time'];

            // print_r($result_log);
            //if(!isset($i)){
            //                die("i is not set");
            //            }
            //            

            if ($i % 2 == 1) {
                // echo '<h2>TEST   -OUT-->'.$i.'</h2>';
                $addinsecs = 0;
                if ($i > 1) {
                    $addinsecs = $result_log[$empid]['insec'];
                    // echo '<h3>yyyyy -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                }
                //                else{
                //                    echo '<h3>XXX -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                //                }
                $result_log[$empid]['insec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['status'] = 0;
            } else {

                if (!isset($logemp[$k + 1]) or ($logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {

                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$empid]['insec'] = $result_log[$empid]['insec'] + $till_time;
                }

                //echo '<h2>TTTEST - IN -->'.$i.'</h2>';
                $addinsecs = $result_log[$empid]['outsec'];
                $result_log[$empid]['outsec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['outhrs'] = gmdate('H:i:s', $result_log[$empid]['outsec']);
                $result_log[$empid]['status'] = 1;
            }

            $i++;
            $prev_value = $row['log_time'];
            $result_log[$empid]['count'] = $i;

            if (strtotime($out_early) > strtotime($row['log_time'])) {
                $result_log[$empid]['outearly'] = 1;
            }
        }

        $noentries = 0;
        if (isset($i)) {
            $result_log[$empid]['status'] = $i % 2;
            $noentries = 1;
        }

        //        echo '<pre>';
        //        print_r($result_log);
        //        die(); 
        //$result_log = $this->devicecal($result_log);
        //echo '<pre>';
        //print_r($result_log); die();
        return $result_log;
    }

    // created by minu
    // calculate total in , out and total hours based on device settings

    public function devicecal($result_log)
    {
        $total_devices = 1; // 0:both devices  1: first punch device 
        if ($total_devices == 0) {
            $result = array();
            $sum_arrays = array("resource_id", "device_id", "accesscard_id", "emp_name");
            foreach ($result_log as $key => $value) {
                $result[$key]['insec'] = 0;
                $result[$key]['inhrs'] = "00:00:00";
                $result[$key]['outsec'] = 0;
                $result[$key]['outhrs'] = "00:00:00";
                $result[$key]['inpunches'] = "";
                $result[$key]['outpunches'] = "";
                $result[$key]['resource_id'] = "";
                $result[$key]['device_id'] = "";
                $result[$key]['accesscard_id'] = "";
                $result[$key]['emp_name'] = "";
                $result[$key]['first_punch'] = "00:00:00";
                $result[$key]['last_punch'] = "00:00:00";
                $insecs = $inhrs = $outsec = $outhrs = $inhrstotal = $outhrstotal = 0;
                foreach ($value as $key1 => $value1) {

                    if (!is_numeric($key1)) {
                        $fpid = $value1;
                    } else {

                        //if ($value1['first_punch'] != $value1['last_punch']) {
                        foreach ($value1 as $k => $value) {

                            switch ($k) {
                                case 'device_id':
                                    $device_id = $result[$key][$k] = $value;
                                    break;
                                case 'resource_id':
                                    $result[$key][$k] = $value;
                                    break;
                                case 'accesscard_id':
                                    $result[$key][$k] = $value;
                                    break;
                                case 'emp_name':
                                    $result[$key][$k] = $value;
                                    break;
                                case 'first_punch':
                                    if ($device_id == $fpid) {
                                        $result[$key][$k] = $value;
                                    } else {
                                        $result[$key][$k] = "00:00:00";
                                    }
                                    break;
                                case 'last_punch':
                                    if ($device_id == $fpid) {
                                        $result[$key][$k] = $value;
                                    } else {
                                        $result[$key][$k] = "00:00:00";
                                    }
                                    break;
                                case 'insec':
                                    if (!isset($result[$key][$k])) {
                                        $value = 0;
                                    }
                                    $result[$key][$k] += $value;
                                    break;
                                case 'outsec':
                                    if (!isset($result[$key][$k])) {
                                        $value = 0;
                                    }
                                    $result[$key][$k] += $value;
                                    break;



                                default:
                                    $result[$key][$k] = "," . $value;
                            }
                        }
                        // }
                    }
                }
                $result[$key]['fpid'] = $fpid;
                $result[$key]['inhrs'] = $this->calculate_time_span($result[$key]['insec']);
                $result[$key]['outhrs'] = $this->calculate_time_span($result[$key]['outsec']);
            }
        } else {
            $result = array();

            foreach ($result_log as $key => $value) {

                $insecs = $inhrs = $outsec = $outhrs = $inhrstotal = $outhrstotal = 0;
                foreach ($value as $key1 => $value1) {

                    if (!is_numeric($key1)) {
                        $fpid = $value1;
                    } else {
                        //                            echo '<pre>';
                        //                            print_r($value1);
                        //                            die();
                        if ($value1['device_id'] == $fpid) {
                            $result[$key] = $value1;
                        } else {
                            continue;
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function actionPunchlog()
    {
        // $this->ipStore();
        $regip = '00';

        // echo $_COOKIE['localIP'];
        /* if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {
          //echo $_COOKIE['localIP'];

          $localip = $_COOKIE['localIP'];

          $pos = strpos($localip, 'perhaps ');
          if ($pos !== false) {
          $localip = trim(substr($localip, $pos), 'perhaps ');
          }

          if ($pos !== false) {
          $localip = substr($_COOKIE['localIP'], $pos);
          } else {
          //echo "The string '$findme' was not found in the string '$mystring'";
          }

          preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
          //  print_r($localipparsed);

          Yii::app()->db->createCommand('update pms_users set reg_ip="' . $localipparsed[0] . '" where userid=' . intval(Yii::app()->user->id))->query();
          Yii::app()->user->setState('reg_ip', $localipparsed[0]);
          $regip = Yii::app()->user->reg_ip;
          //die();
          }

          if (isset($_COOKIE['localIP'])) {

          preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $matches);
          //  print_r($matches);

          preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
          //  print_r($localipparsed);

          $regip = $localipparsed[0];
          }
         */
        // $this->layout = '//layouts/gebomain';
        $model = new Accesslog();

        //die("<center><h1>Under Maintenance</h1></center>");

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        //echo date("Y-m-d");
        //echo date("Y-m-d H:i:s");
        $date = "2017-06-15";
        $datetime = "2017-06-15 00:00:00";
        $punchdate = strtotime(date($date)) + ($days * 60 * 60 * 24);

        $predate = date("Y-m-d", strtotime(date($datetime)) + ($days * 60 * 60 * 24));
        $nextdate = date("Y-m-d", strtotime(date($datetime)) + (($days + 1) * 60 * 60 * 24));

        //die();
        // $datewithtime = date("Y-m-d H:i:s", $system_Date);
        //echo "<br />";

        $datetimesettings = array();
        $datetimesettings['shift_time_starts'] = "07:00:00";
        $datetimesettings['shift_end'] = "05:00:00";

        $datetimesettings['date'] = $predate;

        $datetimesettings['nextday'] = $nextdate;

        $datetimesettings['in_late'] = "$predate 09:30:00";
        $datetimesettings['out_early'] = "$predate 16:00:00";
        $datetimesettings['shift_ends'] = "$predate 23:59:00";

        $datetimesettings['late_halfday'] = "11:00:01"; //First punch Half day
        $datetimesettings['ee_halfday'] = "16:00:00";  //Early Exit Half day

        extract($datetimesettings);

        $excludeemp = '';
        if (yii::app()->user->role != 1) {
            // $excludeemp = ' and empid not in (4,5,27) ';
        }

        $sql = "SELECT al.*, u.userid,concat_ws(' ',first_name,last_name) as full_name, u.status, accesscard_id, "
            . "if(userid='" . Yii::app()->user->id . "',1,0) AS matchip  FROM (SELECT logid ,sqllogid ,empid , log_time, device_id "
            . "FROM pms_punch_log WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
            . " AND  (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end' ) $excludeemp ) AS al "
            . "RIGHT JOIN pms_users AS u ON empid=accesscard_id WHERE u.status=0  &&  accesscard_id!=''  GROUP BY userid, log_time ORDER BY matchip DESC, "
            . "full_name ASC,userid ASC,log_time ASC ";


        //        echo $sql = "SELECT  logid ,sqllogid ,empid , log_time,u.userid,concat_ws(' ',first_name,last_name) as full_name, "
        //                . "u.status,reg_ip, accesscard_id,reg_ip, if(userid='".Yii::app()->user->id."',1,0) as matchip"
        //                . " FROM `pms_punch_log` as al right join pms_users as u on empid=accesscard_id "
        //                . "WHERE (u.status=0 or log_time between '$date $shift_time_starts' and '$nextday $shift_end') "
        //                . " AND  log_time between '$date $shift_time_starts' and '$nextday $shift_end' GROUP BY userid, log_time order by matchip desc, full_name asc,userid asc,log_time asc";

        $logdetails = Yii::app()->db->createCommand($sql);
        $logemp = $logdetails->queryAll();

        //        echo '<pre>';
        //        print_r($logemp);
        //        echo '</pre>';
        //        die();
        //        

        $difflog = array();

        $inout = array(0, 0);
        $result_log = array();

        $allpunch = array();

        $device = array(18 => '\P\l\a\c\e2', '14' => ' \P\l\a\c\e1');

        foreach ($logemp as $k => $row) {
            if ($row['accesscard_id'] != "") {
                //            echo '<br />';
                $empid = $row['accesscard_id'];
                $result_log[$empid]['outearly'] = 0;
                $till_time = 0;
                $allpunch["user_" . $empid][] = date('h:i:s', strtotime($row['log_time']));
                //$allpunch["user_" . $empid][] = date("({$device[$row['device_id']]}) h:i:s ", strtotime($row['log_time']));
                //            echo '<br />';
                if (!isset($result_log[$empid]['accesscard_id'])) {
                    $punch_time = $prev_value = $row['log_time'];
                    $i = 1;
                    $result_log[$empid]['accesscard_id'] = $row['accesscard_id'];
                    $result_log[$empid]['first_punch'] = $punch_time;
                    $result_log[$empid]['last_punch'] = $row['log_time'];
                    $result_log[$empid]['emp_name'] = $row['full_name'];
                    $result_log[$empid]['userid'] = $row['userid'];
                    // $result_log[$empid]['ip'] = $row['reg_ip'];
                    $result_log[$empid]['count'] = $i;
                    // $result_log[$empid]['reg_ip'] = $row['reg_ip'];
                    // $result_log[$empid]['myip'] = $row['matchip'];
                    $result_log[$empid]['device_id'] = $row['device_id'];

                    //                echo $result_log[$empid]['accesscard_id']."=====".$in_late."-----------==".$row['log_time'];
                    //                echo '<br />';
                    //                die();

                    if (strtotime($in_late) < strtotime($row['log_time'])) {
                        $result_log[$empid]['inlate'] = 1;
                    }

                    $result_log[$empid]['insec'] = strtotime(date("Y-m-d")) - strtotime($row['log_time']);

                    if (!isset($logemp[$k + 1]) or ($logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {
                        $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                        $result_log[$empid]['insec'] = $till_time;
                    }

                    $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                    $result_log[$empid]['outsec'] = 0;
                    $result_log[$empid]['outhrs'] = 0;

                    $result_log[$empid]['status'] = $i;
                    //  echo "<h1>first-IN-$i</h1>";
                    $result_log[$empid]['status'] = 1;
                    continue;
                }
                $result_log[$empid]['ldevice_id'] = $row['device_id'];
                $result_log[$empid]['last_punch'] = $row['log_time'];

                // print_r($result_log);
                //if(!isset($i)){
                //                die("i is not set");
                //            }
                //            

                if ($i % 2 == 1) {
                    // echo '<h2>TEST   -OUT-->'.$i.'</h2>';
                    $addinsecs = 0;
                    if ($i > 1) {
                        $addinsecs = $result_log[$empid]['insec'];
                        // echo '<h3>yyyyy -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                    }
                    //                else{
                    //                    echo '<h3>XXX -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                    //                }
                    $result_log[$empid]['insec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                    $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                    $result_log[$empid]['status'] = 0;
                } else {

                    if (!isset($logemp[$k + 1]) or ($logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {

                        $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                        $result_log[$empid]['insec'] = $result_log[$empid]['insec'] + $till_time;
                    }

                    //echo '<h2>TTTEST - IN -->'.$i.'</h2>';
                    $addinsecs = $result_log[$empid]['outsec'];
                    $result_log[$empid]['outsec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                    $result_log[$empid]['outhrs'] = gmdate('H:i:s', $result_log[$empid]['outsec']);
                    $result_log[$empid]['status'] = 1;
                }

                $i++;
                $prev_value = $row['log_time'];
                $result_log[$empid]['count'] = $i;

                if (strtotime($out_early) > strtotime($row['log_time'])) {
                    $result_log[$empid]['outearly'] = 1;
                }
            }
        }

        $noentries = 0;
        if (isset($i)) {
            $result_log[$empid]['status'] = $i % 2;
            $noentries = 1;
        }

        $this->render('punchlog_new', array(
            'noentries' => $noentries, 'logresult' => $result_log,
            'allpunch' => $allpunch, 'punchdate' => $predate, 'datetimesettings' => $datetimesettings
        ));
    }

    /* public function ipStore() {
      // echo '<br />';
      //echo $_COOKIE['localIP'];
      //echo '<br />';
      // echo "<b>".Yii::app()->user->reg_ip."</b>";
      if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {
      //echo $_COOKIE['localIP'];

      $localip = $_COOKIE['localIP'];

      $pos = strpos($localip, 'perhaps ');
      if ($pos !== false) {
      $localip = trim(substr($localip, $pos), ' or perhaps ');
      }

      if ($pos !== false) {
      $localip = substr($_COOKIE['localIP'], $pos);
      } else {
      //echo "The string '$findme' was not found in the string '$mystring'";
      }

      preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
      // print_r($localipparsed);
      $sql = 'update pms_users set reg_ip="' . $localipparsed[0] . '" where userid=' . intval(Yii::app()->user->id);
      Yii::app()->db->createCommand($sql)->query();

      Yii::app()->user->setState('reg_ip', $localipparsed[0]);


      //die();
      }
      /* else{
      die('test');
      }

      } */

    public function actionImportpunchlog()
    {
        $tbl = Yii::app()->db->tablePrefix; // table prefixforpms

        $start = $_GET['startdate'];
        $end = $_GET['enddate'];

        $fromdate = $start;
        $todate = $end;

        /* Get data from accesslog and save */

        $date = $fromdate;
        $users = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users us LEFT JOIN {$tbl}employment_details ed ON ed.user_id = us.userid WHERE us.user_type != 1 AND us.user_type != 5 AND us.accesscard_id != '' AND (ed.date_of_joining<= '$fromdate' OR ed.date_of_joining <= '$todate' OR ed.date_of_joining IS NULL) AND (ed.date_of_resignation >= '$fromdate' OR ed.date_of_resignation >= '$todate' OR ed.date_of_resignation IS NULL) ORDER BY first_name asc")->queryAll();
        $weekly_off1_days = array();
        $weekly_off2_days = array();
        $satsundays = array();
        $sdate = $fromdate;
        $end_date = $todate;
        for ($i = 0; $i <= (strtotime($sdate) <= strtotime($end_date)); $i++) {

            if (date("D", strtotime($sdate)) == 'Sat' || date("D", strtotime($sdate)) == 'Sun') {
                array_push($satsundays, $sdate);
            }

            $sdate = date("Y-m-d", strtotime("+1 day", strtotime($sdate)));
        }


        foreach ($users as $user) {
            $accessid = $user['accesscard_id'];
            $role = $user['user_type'];
            $offdays = array();
            if ($role) {
                $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));

                if (!empty($rsettings)) {

                    while (strtotime($date) <= strtotime($todate)) {
                        //weekly off1
                        $weekly_off1_yes = $rsettings->weekly_off1_yes;
                        if (!empty($weekly_off1_yes)) {
                            $weekly_off1 = $rsettings->weekly_off1;
                            if (date("l", strtotime($date)) == $weekly_off1) {
                                array_push($weekly_off1_days, $date);
                            }
                        }

                        //weekly off2
                        $weekly_off2_yes = $rsettings->weekly_off2_yes;
                        if (!empty($weekly_off2_yes)) {
                            $weekly_off2 = $rsettings->weekly_off2;
                            $weekly_off2_count = $rsettings->weekly_off2_count; //echo date('d',strtotime('+1 week '.$weekly_off2.''));

                            $weekly_off2_array = explode(",", $weekly_off2_count);
                            $wk = date("M Y", strtotime($date));


                            foreach ($weekly_off2_array as $warr) {
                                if ($warr == 1) {
                                    $num = "first";
                                } else if ($warr == 2) {
                                    $num = "second";
                                } else if ($warr == 3) {
                                    $num = "third";
                                } else if ($warr == 4) {
                                    $num = "fourth";
                                } else if ($warr == 5) {
                                    $num = "last";
                                } else {
                                    $num = "";
                                }
                                $dt = date("Y-m-d", strtotime("$num $weekly_off2 of $wk"));
                                if (!in_array($dt, $weekly_off2_days)) {
                                    array_push($weekly_off2_days, date("Y-m-d", strtotime("$num $weekly_off2 of $wk")));
                                }
                            }
                        }
                        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                    }
                }
            }
            $offdays[$accessid] = array_merge($weekly_off1_days, $weekly_off2_days);
        }

        $delete = Yii::app()->db->createCommand("DELETE FROM {$tbl}attendance WHERE att_date BETWEEN '$fromdate' AND '$todate' ")->execute();
        $fromdate = $start;
        $todate = $end;
        $date = $fromdate;
        $result_arr = array();
        $weeklyoffdays = array();
        $attn = Yii::app()->createController('Accesslog');
        while (strtotime($date) <= strtotime($todate)) {

            //if (date("D", strtotime($date)) != 'Sat' && date("D", strtotime($date)) != 'Sun') {

            $result_arr[$date] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$date' ")->queryAll();

            // $result_arr[$date] = $attn[0]->calcHours2($date);
            foreach ($result_arr as $result) {
                foreach ($result as $res) {
                    $accesscardid = $res['resource_id'];
                    $firstpunch = str_replace("HL", "", $res['firstpunch']);
                    $lastpunch = str_replace("HL", "", $res['lastpunch']);
                    $intime = $res['intime'];
                    $outtime = $res['outtime'];
                    $legendid = 0;
                    //$accesscardid = 81;
                    $user = Users::model()->find(array('condition' => "accesscard_id = $accesscardid"));
                    if (!empty($user)) {
                        if (array_key_exists($accesscardid, $offdays)) {
                            $weeklyoffdays = $offdays[$accesscardid];
                        } else {
                            $weeklyoffdays = $satsundays;
                        }

                        $userid = $user['userid'];
                        $role = $user['user_type'];
                        if ($role) {
                            $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));

                            if (!empty($rsettings)) {



                                $latecoming_gt = $rsettings->late_coming_gt;
                                $earlygoing_gt = $rsettings->early_going_gt;
                                $timeto_in = strtotime('09:00:00');
                                if (!is_null($latecoming_gt)) {
                                    $grace_inpunch = date("H:i:s", strtotime('+' . $latecoming_gt . ' minutes', $timeto_in));
                                } else {
                                    $grace_inpunch = "09:00:00";
                                }
                                $timeto_out = strtotime('06:00:00');
                                if (!is_null($earlygoing_gt)) {
                                    $grace_outpunch = date("H:i:s", strtotime('-' . $earlygoing_gt . ' minutes', $timeto_out));
                                } else {
                                    $grace_outpunch = '06:00:00';
                                }


                                if (!in_array($date, $weeklyoffdays)) {
                                    if (!empty($firstpunch) && $firstpunch != "05:30:00") {

                                        $legend = Legends::model()->find(array('condition' => "short_note = 'P'"));
                                        $legendid = $legend->leg_id; //echo 'P';
                                        //Calculate half day if work duration is less than given value
                                        if ($rsettings->halfday_by_duration == 1) {

                                            $time = explode(':', $intime);
                                            $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                            if ($mins < $rsettings->halfday_duration_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        } else if ($rsettings->absent_by_duration == 1) {
                                            //Calculate absent if work duration is less than given value

                                            $time = explode(':', $intime);
                                            $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);


                                            //echo $intime."--".$mins; echo "<br/>";
                                            if ($mins < $rsettings->absent_duration_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }



                                        //Mark Half Day if late by given time
                                        if ($rsettings->halfday_by_late == 1) {
                                            $now = new DateTime($grace_inpunch);
                                            $then = new DateTime($firstpunch);
                                            $diff = $now->diff($then);
                                            $mins = $diff->format('%i');
                                            if ($mins >= $rsettings->halfday_late_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }

                                        //Mark Half Day if early going by given time
                                        if ($rsettings->halfday_by_earlygoing == 1) {
                                            $now = new DateTime($grace_outpunch);
                                            $then = new DateTime($lastpunch);
                                            $diff = $now->diff($then);
                                            $mins = $diff->format('%i');


                                            if ($mins >= $rsettings->halfday_earlygoing_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }







                                        //On partial day, Calculate half day if work duration is less than given value
                                        if ($rsettings->partial_halfday_by_duration == 1) {
                                            if ($firstpunch > $grace_inpunch) {
                                                $time = explode(':', $intime);
                                                $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                                if ($mins < $rsettings->partial_halfday_duration_mins) {
                                                    $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                    $legendid = $legend->leg_id;
                                                }
                                            }
                                        } else if ($rsettings->partial_absent_by_duration == 1) {
                                            //On partial day, Calculate absent if work duration is less than given value
                                            if ($firstpunch > $grace_inpunch) {
                                                $time = explode(':', $intime);
                                                $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                                if ($mins < $rsettings->partial_absent_duration_mins) {
                                                    $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                    $legendid = $legend->leg_id;
                                                }
                                            }
                                        }
                                    } else {
                                        //Absent
                                        $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                        $legendid = $legend->leg_id;
                                    }
                                } else {
                                    //case of weekly holidays
                                    $result_arr2 = array();
                                    $hdate = $date;
                                    $prevdate = date('Y-m-d', strtotime('-1 day', strtotime($hdate))); //echo $prevdate; //die;
                                    if (!in_array($prevdate, $weeklyoffdays)) {

                                        //$result_arr2[$prevdate] = $attn[0]->calcHours2($prevdate);
                                        $result_arr2[$prevdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$prevdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        //print_r($result_arr2);
                                        if (!empty($result_arr2)) {
                                            $prevarr = $result_arr2[$prevdate];
                                            $fpunch = $prevarr['firstpunch'];
                                            if (empty($fpunch) || $fpunch == "05:30:00") {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }
                                    }


                                    $result_arr3 = array();
                                    $nextdate = date('Y-m-d', strtotime('+1 day', strtotime($hdate)));
                                    //echo $nextdate; //die;
                                    if (!in_array($nextdate, $weeklyoffdays)) {
                                        echo "in";
                                        $result_arr3[$nextdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$nextdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        if (!empty($result_arr3)) {
                                            $nextarr = $result_arr3[$nextdate];
                                            $fpunch = $nextarr['firstpunch'];
                                            if (empty($fpunch) || $fpunch == "05:30:00") {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }
                                    }

                                    $result_arr4 = array();
                                    $result_arr5 = array();
                                    if (!in_array($prevdate, $weeklyoffdays) && !in_array($nextdate, $weeklyoffdays)) {
                                        $result_arr4[$prevdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$prevdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        $result_arr5[$nextdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$nextdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        if (!empty($result_arr4) || !empty($result_arr5)) {
                                            $prevarr = $result_arr4[$prevdate];
                                            $fpunchprev = $prevarr['firstpunch'];

                                            $nextarr = $result_arr5[$nextdate];
                                            $fpunchnext = $nextarr['firstpunch'];
                                            if (empty($fpunchprev) || $fpunchprev == "05:30:00" && empty($fpunchnext) || $fpunchnext == "05:30:00") {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }
                                    }
                                }
                            } //end if role settings
                        } //end if role
                        else {

                            if (!empty($firstpunch) && $firstpunch != "05:30:00") {
                                if (strtotime($inhrs) < strtotime("04:00:00")) {
                                    //Half day
                                    $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                    $legendid = $legend->leg_id; //echo 'HP';
                                } else {
                                    $legend = Legends::model()->find(array('condition' => "short_note = 'P'"));
                                    $legendid = $legend->leg_id; //echo 'P';
                                }
                            } else {
                                //Absent
                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                $legendid = $legend->leg_id; //echo "CL";
                            }
                        }

                        $check = Attendance::model()->find(array('condition' => "user_id = $userid AND att_date = '$date'"));
                        if (count($check) == 0) {
                            $attmodel = new Attendance();
                        } else {
                            $attmodel = Attendance::model()->findByPk($check->att_id);
                        }


                        if ($legendid) {
                            $attmodel->user_id = $userid;
                            $attmodel->att_date = $date;
                            $attmodel->att_entry = $legendid;
                            $attmodel->created_by = Yii::app()->user->id;
                            $attmodel->created_date = date("Y-m-d");
                            $attmodel->save();
                        }
                    } //end if user
                } //end foreach
            } //die;
            //}

            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
        echo true;
        die();
    }

    public function actionMonthly_report()
    {
        $totalot = 0;
        $tblpx = Yii::app()->db->tablePrefix;
        $total_devices = 1;
        if (isset($_REQUEST['TimeEntry'])) {
            $date1 = new DateTime($_REQUEST['TimeEntry']['date_from']);
            $date2 = new DateTime($_REQUEST['TimeEntry']['date_till']);
            if (isset($_REQUEST['TimeEntry']['checkbox'])) {
                $total_devices = $_REQUEST['TimeEntry']['checkbox'];
            } else {
                $total_devices = 0;
            }
            Yii::app()->session['total_devices'] = $total_devices;
            Yii::app()->session['start_date'] = $_REQUEST['TimeEntry']['date_from'];
            Yii::app()->session['end_date'] = $_REQUEST['TimeEntry']['date_till'];

            list($start_date, $end_date, $week_title) = $this->x_week_range($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
        } elseif (isset(Yii::app()->session['start_date']) && (Yii::app()->session['end_date'])) {
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates(Yii::app()->session['start_date'], Yii::app()->session['end_date']);
            $start_date = Yii::app()->session['start_date'];
            $end_date = Yii::app()->session['end_date'];
        } else {

            list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-d"));
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($start_date, $end_date);
            Yii::app()->session['start_date'] = $start_date;
            Yii::app()->session['end_date'] = $end_date;
        }
        $resultot = array();
        $resultar = $this->generate($start_date, $end_date);

        $total_sites = Clientsite::model()->findAll();

        $users = Yii::app()->db->createCommand("SELECT us.userid,us.employee_id,us.designation,concat_ws(' ',us.first_name,us.last_name) as fullname,dept.department_name FROM {$tblpx}users as us  "
            . "LEFT JOIN {$tblpx}department as dept ON dept.department_id = us.department_id  "
            . "WHERE us.userid != '' ORDER BY us.userid")->queryAll(); //echo "<pre>"; print_r($users);and accesscard_id = 42
        //        echo "<pre>";
        //        print_r($users);
        //        die;

        $newar = array();
        foreach ($resultar as $key => $result) {
            foreach ($result as $key1 => $res) {
                $userid = $res['resource_id'];
                $newar[$key][$userid][] = $res;
            }
        }

        $finalarr = '';
        foreach ($newar as $value) {
            foreach ($value as $value1) {
                $othrs = 0;
                $ottotal = 0;
                $present = 0;

                for ($i = 0; $i < count($value1); $i++) {
                    //$userid = $value1[$i]['site_id'];
                    foreach ($total_sites as $site) {
                        if ($site['id'] == $value1[$i]['site_id']) {
                            $othrs = $value1[$i]['othours'];
                            $parsed = date_parse($othrs);
                            $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                            $ottotal += $seconds;
                            $present += $value1[$i]['attn'];
                        }
                    }
                    //            echo $present;
                    //            die;
                    //        echo $value1[$i]['resource_id'];            
                    $finalarr[]['totpresent'] = $present;
                    $finalarr[]['totOT'] = $this->calculate_time_span($ottotal);
                }
                //        echo "<pre>";
                //        print_r($finalarr);
                //        die;
            }
        }
        $this->render("monthly_report", array(
            'users' => $users,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'results' => $resultar,
            'total_sites' => $total_sites
        ));
    }


    public function actionAttReport()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $sdate = (!isset($_GET['sdate']) ? date('Y-m-01') : $_GET['sdate']);
        $tdate = (!isset($_GET['tdate']) ? date('Y-m-t') : $_GET['tdate']);
        $deptment = (isset($_GET['deptment']) ? intval($_GET['deptment']) : 0);
        $siteid = (isset($_GET['site']) ? intval($_GET['site']) : 0);
        $depwhere = '';
        if ($deptment > 0) {
            $depwhere = "and dept.department_id=" . $deptment;
        }
        $sitecon = '';
        if ($siteid > 0) {
            $sitecon = "and pds.site_id=" . $siteid;
        }

        $sql = 'select *,date_format(log_time, "%Y-%m-%d") as punchdate FROM ' .
            $tblpx . 'accesslog WHERE log_time BETWEEN "' . $sdate . '" AND "' . $tdate . " 23:59:59" . '" '
            . 'GROUP BY empid,punchdate,device_id order by empid asc';

        $accesslog = Yii::app()->db->createCommand($sql)->queryAll();
        $userplog = array();

        foreach ($accesslog as $k => $acl) {
            $userplog[$acl['empid']][] = $acl;
        }
        //    echo '<pre>';
        //    print_r($userplog);
        //    echo '</pre>';

        $userslist = "SELECT us.userid,us.employee_id,us.designation,"
            . "concat_ws(' ',us.first_name,us.last_name) as fullname,dept.department_name,da.deviceid,da.accesscard_id  "
            . " FROM {$tblpx}users as us left join pms_device_accessids as da on da.userid=us.userid  "
            . " LEFT JOIN {$tblpx}department as dept ON dept.department_id = us.department_id "
            . " inner join {$tblpx}punching_devices as pds on pds.device_id=da.deviceid "
            . " WHERE us.userid != '' $depwhere $sitecon ORDER BY userid";

        $users = Yii::app()->db->createCommand($userslist)->queryAll();

        $useritems = array();
        $userdata = array();
        $r = 0;
        foreach ($users as $key => $user) {
            $otarray = $this->getot($user['accesscard_id'], $user['deviceid'], $sdate, $tdate);

            if ($user['userid'] == 11) {
                //                echo '<pre>';
                //                print_r($user);
                //                print_r($otarray);
                //                echo '</pre>';
                ////            die;
                //                if(count($otarray)){
                //                    die();
                //                }            
            }
            $r++;

            if (!isset($useritems[$user['userid']]) or count($otarray) > 0) {
                $userdata = array(
                    'userid' => $user['userid'],
                    'empid' => $user['employee_id'],
                    'designation' => $user['designation'],
                    'fullname' => $user['fullname'],
                    'dept' => $user['department_name'],
                    'accesscard_id' => $user['accesscard_id'],
                    'present_count' => (isset($userplog[$user['accesscard_id']]) ? count($userplog[$user['accesscard_id']]) : 0),
                    'otarray' => $otarray
                );
                $useritems[$user['userid']] = $userdata;
            }
        }
        //        echo '<pre>';
        //        print_r($useritems);
        //        echo '</pre>';
        //        die;

        if (isset($_GET['ajaxcall'])) {
            echo $retarray = $this->renderPartial("_attreport", array(
                'users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems
            ), true);
        } else {
            $this->render("_attreport", array(
                'users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems
            ));
        }

        //        echo '<pre>';
        //        print_r($useritems);
        //        echo '</pre>';
        //        echo $this->getot(17);
    }



    public function getot($ac_id, $deviceid, $sdate, $tdate)
    {  //above 9 hrs
        $tblpx = Yii::app()->db->tablePrefix;

        $sql = 'select *,date_format(log_time, "%Y-%m-%d") as punchdate FROM ' .
            $tblpx . 'accesslog left join ' . $tblpx . 'punching_devices as pd using (device_id) '
            . ' INNER JOIN ' . $tblpx . 'clientsite as s on s.id=pd.site_id'
            . ' WHERE pd.device_id=' . intval($deviceid) . ' AND log_time BETWEEN "' . $sdate . " 00:00:00" . '" '
            . 'AND "' . $tdate . " 23:59:59" . '" AND empid=' . intval($ac_id)
            . ' GROUP BY empid,site_id,log_time ORDER BY empid,site_id,log_time asc';
        //die;
        // echo '<hr/>';
        $accesslog = $origlog = Yii::app()->db->createCommand($sql)->queryAll();
        $userplog = array();
        $resarray = array();
        //        echo '<pre>';
        //        print_r($accesslog);
        //         echo '</pre>';
        //        die;

        if (count($accesslog) == 1) {
            $accesslog[] = $accesslog[0];
        }
        //        echo '<pre>';
        //        print_r($accesslog);
        //         echo '</pre>';
        ////        die;

        $time = 0;
        $flg = 0;
        $intime = 0;
        $pdate = '';
        $bydate = array();
        $odd_entries = array();
        $totot = 0;

        foreach ($accesslog as $k => $acl) {
            if ($pdate != $acl['punchdate']) {
                if ($intime != 0) {
                    $ot = $intime - 32400;
                    $otcalc = ($ot > 0 ? $ot : 0);
                    $totot += $otcalc;
                    $bydate['totot'] = $totot;
                    $bydate[$acl['site_id']]['ot'][] = $otcalc;
                    $bydate[$acl['site_id']]['daywise'][strtotime($pdate)]['time'] = gmdate('H:i', $intime);
                    $bydate[$acl['site_id']]['daywise'][strtotime($pdate)]['ot'] = $otcalc;
                    $bydate[$acl['site_id']]['daywise'][strtotime($pdate)]['date'] = $pdate;
                    $bydate[$acl['site_id']]['daywise'][strtotime($pdate)]['site_name'] = $acl['site_name'];

                    //                    echo '<pre>';
                    //                    print_r($bydate);
                    //                    echo $pdate;
                    //                    die();
                }

                $pdate = $acl['punchdate'];

                $site_id = $acl['site_id'];
                $site_name = $acl['site_name'];
                $time = 0;
                $flg = 0;
                $intime = 0;
            }

            if (count($origlog) == 1) {
                $ot = 0;
                $otcalc = ($ot > 0 ? $ot : 0);
                $totot += $otcalc;
                $bydate['totot'] = $totot;
                $bydate[$acl['site_id']]['ot'][] = $otcalc;
                $bydate[$acl['site_id']]['daywise'][strtotime($acl['punchdate'])]['time'] = gmdate('H:i', $intime);
                $bydate[$acl['site_id']]['daywise'][strtotime($acl['punchdate'])]['ot'] = $otcalc;
                $bydate[$acl['site_id']]['daywise'][strtotime($acl['punchdate'])]['date'] = $acl['punchdate'];
                $bydate[$acl['site_id']]['daywise'][strtotime($acl['punchdate'])]['site_name'] = $acl['site_name'];

                return $bydate;
            } else if (($time == 0 and $flg == 0) or ($flg % 2 == 0)) {
                // $intime = strtotime($acl['log_time']) - strtotime($time);
                $time = $acl['log_time'];
                //$intime = strtotime($acl['log_time']) - strtotime($time);
                $flg++;
                continue;
            }

            //echo $k;
            //echo '<br/>';
            //echo $intime;
            //   echo '<br/>';
            $intime += strtotime($acl['log_time']) - strtotime($time);
            // echo '<hr/>';
            //echo $acl['log_time'];

            //die;

            $flg++;
            //  echo '-------'.$acl['log_time']."---".$time.'---------';
            // die;
            //$userplog[$acl['empid']][] = $acl;
        }

        if ($intime != 0) {
            $bydate[$site_id]['daywise'][strtotime($pdate)]['time'] = gmdate('H:i', $intime);
            $bydate[$site_id]['daywise'][strtotime($pdate)]['site_name'] = $site_name;
            $ot = $intime - 32400;
            $otcalc = ($ot > 0 ? $ot : 0);
            $totot += $otcalc;
            $bydate['totot'] = $totot;
            $bydate[$site_id]['ot'][] = $otcalc;
            $bydate[$site_id]['daywise'][strtotime($pdate)]['ot'] = $otcalc;
            $bydate[$site_id]['daywise'][strtotime($pdate)]['date'] = $pdate;
        }

        return $bydate;

        //        echo '<pre>';
        //        print_r($bydate);
        //        echo '</pre>';
        //        echo gmdate('H:i:s', $intime);
        //        die;
    }



    public function actionGetMonthReport()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $sdate = (!isset($_GET['sdate']) ? date('Y-m-01') : $_GET['sdate']);
        $tdate = (!isset($_GET['tdate']) ? date('Y-m-t') : $_GET['tdate']);
        $deptment = (isset($_GET['deptment']) ? intval($_GET['deptment']) : 0);
        $depwhere = '';
        if ($deptment > 0) {
            $depwhere = "and dept.department_id=" . $deptment;
        }

        $sql = 'select *,date_format(log_time, "%Y-%m-%d") as punchdate FROM ' .
            $tblpx . 'accesslog WHERE log_time BETWEEN "' . $sdate . '" AND "' . $tdate . " 23:59:59" . '" '
            . 'GROUP BY empid,punchdate,device_id order by empid asc';

        $accesslog = Yii::app()->db->createCommand($sql)->queryAll();
        $userplog = array();

        foreach ($accesslog as $k => $acl) {
            $userplog[$acl['empid']][] = $acl;
        }
        //    echo '<pre>';
        //    print_r($userplog);
        //    echo '</pre>';

        $userslist = "SELECT us.userid,us.employee_id,us.designation,"
            . "concat_ws(' ',us.first_name,us.last_name) as fullname,dept.department_name,da.deviceid,da.accesscard_id  "
            . "FROM {$tblpx}users as us left join pms_device_accessids as da on da.userid=us.userid  "
            . "LEFT JOIN {$tblpx}department as dept ON dept.department_id = us.department_id  "
            . "WHERE us.userid != '' $depwhere ORDER BY userid";
        //die;
        $users = Yii::app()->db->createCommand($userslist)->queryAll();

        $useritems = array();
        $userdata = array();
        $r = 0;
        foreach ($users as $key => $user) {
            $otarray = $this->getot($user['accesscard_id'], $user['deviceid'], $sdate, $tdate);

            if ($user['userid'] == 11) {
                //                echo '<pre>';
                //                print_r($user);
                //                print_r($otarray);
                //                echo '</pre>';
                ////            die;
                //                if(count($otarray)){
                //                    die();
                //                }            
            }
            $r++;


            if (!isset($useritems[$user['userid']]) or count($otarray) > 0) {
                $userdata = array(
                    'userid' => $user['userid'],
                    'empid' => $user['employee_id'],
                    'designation' => $user['designation'],
                    'fullname' => $user['fullname'],
                    'dept' => $user['department_name'],
                    'accesscard_id' => $user['accesscard_id'],
                    'present_count' => (isset($userplog[$user['accesscard_id']]) ? count($userplog[$user['accesscard_id']]) : 0),
                    'otarray' => $otarray
                );
                $useritems[$user['userid']] = $userdata;
            }
        }


        //        echo '<pre>';
        //        print_r($useritems);
        //        echo '</pre>';
        //        die;

        if (isset($_GET['ajaxcall'])) {
            echo $retarray = $this->renderPartial("_monthly_report", array(
                'users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems
            ), true);
        } else {
            $this->render("_monthly_report", array(
                'users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems
            ));
        }

        //        echo '<pre>';
        //        print_r($useritems);
        //        echo '</pre>';
        //        echo $this->getot(17);
    }



    public function actionExportExcel()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $sdate = (!isset($_GET['startdate']) ? date('Y-m-01') : $_GET['startdate']);
        $tdate = (!isset($_GET['tilldate']) ? date('Y-m-t') : $_GET['tilldate']);
        $deptment = (isset($_GET['deptment']) ? intval($_GET['deptment']) : 0);
        //echo $sdate.'/'.$tdate.'/'.$deptment;

        $depwhere = '';
        if ($deptment > 0) {
            $depwhere = "and dept.department_id=" . $deptment;
        }

        $sql = 'select *,date_format(log_time, "%Y-%m-%d") as punchdate FROM ' .
            $tblpx . 'accesslog WHERE log_time BETWEEN "' . $sdate . '" AND "' . $tdate . " 23:59:59" . '" '
            . 'GROUP BY empid,punchdate,device_id order by empid asc';

        $accesslog = Yii::app()->db->createCommand($sql)->queryAll();
        $userplog = array();

        foreach ($accesslog as $k => $acl) {
            $userplog[$acl['empid']][] = $acl;
        }
        $userslist = "SELECT us.userid,us.employee_id,us.designation,"
            . "concat_ws(' ',us.first_name,us.last_name) as fullname,dept.department_name,da.deviceid,da.accesscard_id  "
            . "FROM {$tblpx}users as us left join pms_device_accessids as da on da.userid=us.userid  "
            . "LEFT JOIN {$tblpx}department as dept ON dept.department_id = us.department_id  "
            . "WHERE us.userid != '' $depwhere ORDER BY userid";
        $users = Yii::app()->db->createCommand($userslist)->queryAll();

        $useritems = array();
        $userdata = array();
        $r = 0;
        foreach ($users as $key => $user) {
            $otarray = $this->getot($user['accesscard_id'], $user['deviceid'], $sdate, $tdate);
            if ($user['userid'] == 11) {
            }
            $r++;


            if (!isset($useritems[$user['userid']]) or count($otarray) > 0) {
                $userdata = array(
                    'userid' => $user['userid'],
                    'empid' => $user['employee_id'],
                    'designation' => $user['designation'],
                    'fullname' => $user['fullname'],
                    'dept' => $user['department_name'],
                    'accesscard_id' => $user['accesscard_id'],
                    'present_count' => (isset($userplog[$user['accesscard_id']]) ? count($userplog[$user['accesscard_id']]) : 0),
                    'otarray' => $otarray
                );
                $useritems[$user['userid']] = $userdata;
            }
        }


        $objPHPExcel = new PHPExcel();
        $row_count = 0;

        $daytitl = date("F d Y", strtotime($sdate)) . ' to ' . date("F d Y", strtotime($tdate));

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('A1:Z2');
        $objPHPExcel->getActiveSheet()
            ->getCell('A1')
            ->setValue('Present & OT Report      ' . $daytitl . '');
        $objPHPExcel->getActiveSheet()->getStyle('A1:Z2')->getFont()->setSize(16);


        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('A3:A6');
        $objPHPExcel->getActiveSheet()
            ->getCell('A3')
            ->setValue('Sl No.');

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('B3:B6');
        $objPHPExcel->getActiveSheet()
            ->getCell('B3')
            ->setValue('Employee Code');

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('C3:C6');
        $objPHPExcel->getActiveSheet()
            ->getCell('C3')
            ->setValue('Employee Name');

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('D3:D6');
        $objPHPExcel->getActiveSheet()
            ->getCell('D3')
            ->setValue('Department');

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('E3:E6');
        $objPHPExcel->getActiveSheet()
            ->getCell('E3')
            ->setValue('Designation');



        $rows = $row = 'F';
        $col = 3;
        $cell = 4;

        $sites = CHtml::listData(Clientsite::model()->findAll(), 'id', 'site_name');
        //$sites = CHtml::listData(Clientsite::model()->findAll(array('limit'=>4)), 'id', 'site_name');
        $site_header = '';
        $sites['-1'] = 'Total';
        $count = count($sites);


        $row++;
        foreach ($sites as $site) {
            $row1 = $row++;
            $objPHPExcel->setActiveSheetIndex()
                ->mergeCells($rows . $col . ':' . $row1 . $cell);
            $objPHPExcel->getActiveSheet()
                ->getCell($rows . $col)
                ->setValue($site);

            $objPHPExcel->getActiveSheet()
                ->getStyle($rows . $col . ':' . $row1 . $cell)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $rows = $row1;
            $rows++;
            $row++;
        }

        $column = 'F';
        for ($i = 1; $i <= $count; $i++) {

            $objPHPExcel->setActiveSheetIndex()
                ->mergeCells($column . '5:' . $column . '6');
            $objPHPExcel->getActiveSheet()
                ->getCell($column . '5')
                ->setValue('Presence');
            $column++;
            $objPHPExcel->setActiveSheetIndex()
                ->mergeCells($column . '5:' . $column . '6');
            $objPHPExcel->getActiveSheet()
                ->getCell($column . '5')
                ->setValue('OT');
            $column++;
        }

        $grandpresent = 0;
        $grandot = 0;

        $stotpr = array();
        $sitetotot = array();

        $m = 1;
        if (!empty($useritems)) {

            $ots = array();


            $alpha = 'A';
            $cel = 7;

            foreach ($useritems as $puserid => $res) {

                //die($alpha.$cel);
                $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                    ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                    ->getCell($alpha . $cel)
                    ->setValue($m++);
                $alpha++;
                $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                    ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                    ->getCell($alpha . $cel)
                    ->setValue($res['empid']);
                $alpha++;
                $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                    ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                    ->getCell($alpha . $cel)
                    ->setValue($res['fullname']);
                $alpha++;

                $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                    ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                    ->getCell($alpha . $cel)
                    ->setValue($res['dept']);
                $alpha++;
                $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                    ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                    ->getCell($alpha . $cel)
                    ->setValue($res['designation']);
                $alpha++;
                /* */




                $j = 1;
                $totpresent = 0;
                $tot_ot = 0;


                foreach ($sites as $siteid => $site_name) {
                    $tpresent = 0;
                    if (isset($res['otarray'][$siteid])) {
                        $tpresent = count($res['otarray'][$siteid]['daywise']);
                    }

                    $site_ot = ((isset($res['otarray'][$siteid]) and isset($res['otarray'][$siteid]['ot'])) ? array_sum($res['otarray'][$siteid]['ot']) : 0);

                    $tot_ot += $site_ot;
                    $ots[$res['userid']][] = (isset($res['otarray']['totot']) ? $res['otarray']['totot'] : 0);



                    $presentcount = ((count($res['otarray']) > 0 && isset($res['otarray'][$siteid]) && isset($res['otarray'][$siteid]['daywise'])) ? count($res['otarray'][$siteid]['daywise']) : 0);
                    $grandpresent += $presentcount;
                    $totpresent += $presentcount;

                    $ottotl = $site_ot;
                    if ($siteid != -1) {

                        if (!isset($stotpr[$siteid])) {
                            $stotpr[$siteid] = 0;
                        }
                        $stotpr[$siteid] += $presentcount;

                        if (!isset($sitetotot[$siteid])) {
                            $sitetotot[$siteid] = 0;
                        }
                        $sitetotot[$siteid] += $site_ot;

                        //                                        if($tot_ot>0){
                        //                                        echo $tot_ot;
                        //                                        die;
                        //                                    }
                        $grandot += $site_ot;

                        if ($presentcount != 0) {

                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue($presentcount);

                            $objPHPExcel->getActiveSheet()
                                ->getStyle($alpha . $cel)
                                ->getFill()
                                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('b7ddc7');

                            $alpha++;
                        } else {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue($presentcount);
                            $alpha++;
                        }

                        if ($ottotl != 0 or strlen($ottotl) > 1) {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue(($site_ot == 0 ? "0" : gmdate('H:i', $site_ot)));

                            $objPHPExcel->getActiveSheet()
                                ->getStyle($alpha . $cel)
                                ->getFill()
                                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('b7ddc7');

                            $alpha++;
                        } else {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue(($site_ot == 0 ? "0" : gmdate('H:i', $site_ot)));
                            $alpha++;
                        }
                    } else {

                        if ($totpresent != 0) {

                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue($totpresent);

                            $objPHPExcel->getActiveSheet()
                                ->getStyle($alpha . $cel)
                                ->getFill()
                                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('b7ddc7');

                            $alpha++;
                        } else {

                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue($totpresent);
                            $alpha++;
                        }

                        if ($tot_ot != 0 or strlen($tot_ot) > 1) {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue(gmdate('H:i', $tot_ot));

                            $objPHPExcel->getActiveSheet()
                                ->getStyle($alpha . $cel)
                                ->getFill()
                                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('b7ddc7');

                            $alpha++;
                        } else {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($alpha . $cel)
                                ->setValue(gmdate('H:i', $tot_ot));
                            $alpha++;
                        }
                    }
                    $j++;
                }

                $alpha = 'A';
                $cel++;
            }




            $newcel = $cel;
            $newrow = 'F';
            //echo $newcel; die;


            $objPHPExcel->setActiveSheetIndex()
                ->mergeCells('A' . $newcel . ':E' . $newcel);
            $objPHPExcel->getActiveSheet()
                ->getCell('A' . $newcel)
                ->setValue('Total');

            $objPHPExcel->getActiveSheet()
                ->getStyle('A' . $newcel . ':E' . $newcel)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $grandpresent = 0;
            $grandot = 0;

            //print_r($stotpr); die;

            foreach ($stotpr as $key => $present) {

                $grandot += $sitetotot[$key];
                $grandpresent += $present;


                $objPHPExcel->getActiveSheet()->getColumnDimension($newrow)
                    ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                    ->getCell($newrow . $newcel)
                    ->setValue($present);
                $newrow++;


                $objPHPExcel->getActiveSheet()->getColumnDimension($newrow)
                    ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                    ->getCell($newrow . $newcel)
                    ->setValue($this->sectotime($sitetotot[$key]));
                $newrow++;
            }


            $objPHPExcel->getActiveSheet()->getColumnDimension($newrow)
                ->setAutoSize(true);
            $objPHPExcel->getActiveSheet()
                ->getCell($newrow . $cel)
                ->setValue($grandpresent);
            $newrow++;

            $objPHPExcel->getActiveSheet()->getColumnDimension($newrow)
                ->setAutoSize(true);
            $objPHPExcel->getActiveSheet()
                ->getCell($newrow . $cel)
                ->setValue($this->sectotime($grandot));
            $newrow++;
        }

        if (count($useritems) == 0) {


            $objPHPExcel->setActiveSheetIndex()
                ->mergeCells('A7:D8');
            $objPHPExcel->getActiveSheet()
                ->getCell('A7')
                ->setValue('No users found.');
        }






        ob_end_clean();
        ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Employee Monthly Report.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }


    public function sectotime($seconds)
    {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");

        $days = $dtF->diff($dtT)->format('%a');
        $hrs = $dtF->diff($dtT)->format('%H') + ($days * 24);
        $mins = $dtF->diff($dtT)->format('%i');

        return $hrs . ":" . $mins;
    }
}
