<?php
Yii::app()->getModule('masters');
class SiteMeetingsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'create',
                    'update',
                    'meetingend',
                    'approveMeeting',
                    'sendMail',
                    'test',
                    'create_mom'
                ),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new SiteMeetings;
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        if (!empty($_POST['SiteMeetings']['id'])) {
            $id = $_POST['SiteMeetings']['id'];
            $model = $this->loadModel($id);
        }
        $model->setScenario('meeting_start');

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        try {
            if (isset($_POST['SiteMeetings'])) {
                $model = $this->saveMeeting($_POST['SiteMeetings'], $model);
                if ($model->save()) {
                    Yii::app()->user->setState('site_meeting_id', $model->id);
                    if (empty($_POST['SiteMeetings']['id'])) {
                        $template = MailTemplate::model()->findByPK(3);
                        $approved_by = explode(",", $model->approved_by);
                        $users = $this->getUsers($approved_by);
                        $string = "";
                        foreach ($users as $user) {
                            $mail_to = $user->email;
                            $folder = "";
                            if ($template) {
                                $string = $template->temp_content;
                            }

                            $search = array('{name}', '{meeting_number}', '{link}', '{from_name}');
                            $from_name = "Ashly";
                            $replace = array($user->first_name, $model->meeting_number, CHtml::link('Click here to login', $this->createAbsoluteUrl('site/index2')), $from_name);
                            $body = str_replace($search, $replace, $string, $count);
                            //$this->sendEmail($mail_to, $body, $folder);
                            $model2 = new MailLog;
                            $model2->send_to = $mail_to;
                            $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
                            $mail_send_byJSON = json_encode($mail_send_by);
                            $model2->send_by = $mail_send_byJSON;
                            $model2->send_date = date('Y-m-d H:i:s');
                            $model2->message = htmlentities($body);
                            $model2->description = "Meeting Created";
                            $model2->created_date = date('Y-m-d');
                            $model2->created_by = Yii::app()->user->id;
                            $model2->mail_type = "Meeting Created";
                            $model2->sendemail_status = 0;
                            $model2->save();
                        }
                    }


                    $milestone_condition = "status = 1 AND project_id = " . $model->project_id;
                    $milestone_data = Milestone::model()->findAll(
                        array(
                            'condition' => $milestone_condition,
                            'order' => 'milestone_title ASC',
                            'distinct' => true
                        )
                    );
                    $milestone_result = CHtml::listData($milestone_data, 'id', 'milestone_title');
                    $milestone_html = "<option value=''>Choose a Milestone</option>";

                    foreach ($milestone_result as $milestone_key => $milestone_value) {
                        $milestone_html .= "<option value=" . $milestone_key . ">" . $milestone_value . "</option>";
                    }
                    if (isset($_POST['MeetingParticipants'])) {
                        $contractors = $_POST['MeetingParticipants']['contractor_id'];
                        $this->saveParticipants($contractors, $model);
                    }

                    $criteria = new CDbCriteria;
                    $criteria = "report_type = 2 AND from_date != '1970-01-01' 
                     AND to_date != '1970-01-01' AND project_id = " . $model->project_id;

                    $result_data = ProjectReport::model()->findAll($criteria);

                    $report_html = "<option value=''>Choose a report</option>";

                    foreach ($result_data as $key => $value) {
                        $report_html .= "<option value=" . $value['id'] . ">" . 'Detailed Report(' . $value['from_date'] . " - " . $value['to_date'] . ")" . "</option>";
                    }
                    $return_result = array(
                        'status' => 1,
                        'next_level' => 1,
                        'model_id' => $model->id,
                        'milestones' => $milestone_html,
                        'report_html' => $report_html,
                        'meeting_status' => $model->approved_status,
                        'project_id' => $model->project_id
                    );
                } else {
                    $return_result = array('status' => 0, 'next_level' => 0);
                }
            } else {
                $return_result = array('status' => 0, 'next_level' => 0);
            }
        } catch (ErrorException $e) {
            $return_result = array('status' => 0, 'next_level' => 0, 'msg' => $e);
        }

        echo json_encode($return_result);
        exit;

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    public function saveMeeting($data, $model)
    {
        $model->attributes = $data;
        if (isset($_POST['Meeting']['approved_by'])) {
            $approved_users = $_POST['Meeting']['approved_by'];
            $model->approved_by = implode(',', $approved_users);
        }
        $model->created_by = yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->updated_by = yii::app()->user->id;
        $model->date = date('Y-m-d', strtotime($data['date']));
        $model->updated_date = date('Y-m-d H:i:s');
        return $model;
    }

    public function saveParticipants($contractors, $model)
    {
        foreach ($contractors as $key => $value) {
            $participant_model = new MeetingParticipants;
            $participant_model->meeting_id = $model->id;
            $participant_model->contractor_id = $value;
            if (!empty($_POST['MeetingParticipants']['participants'][$key])) {
                $participant_model->participants = $_POST['MeetingParticipants']['participants'][$key];
            } else {
                $participant_model->participants = '';
            }
            if (!empty($_POST['MeetingParticipants']['designation'][$key])) {
                $participant_model->designation = $_POST['MeetingParticipants']['designation'][$key];
            } else {
                $participant_model->designation = '';
            }
            $participant_model->created_by = yii::app()->user->id;
            $participant_model->created_date = date('Y-m-d');
            $participant_model->updated_by = yii::app()->user->id;
            $participant_model->updated_date = date('Y-m-d H:i:s');
            $participant_model->save();
        }
    }

    public function actionmeetingEnd()
    {
        $next_meeting_data = new SiteMeetings();
        $next_meeting_data->setScenario('meeting_final');
        $this->performAjaxValidation($next_meeting_data);

        if (!empty($_POST['SiteMeetings']['meeting_id'])) {
            $id = $_POST['SiteMeetings']['meeting_id'];
            $next_meeting_data = $this->loadModel($id);
            if (!empty($next_meeting_data)) {
                $next_meeting_data->attributes = $_POST['SiteMeetings'];
                $next_meeting_data->next_meeting_date = date('Y-m-d', strtotime($_POST['SiteMeetings']['next_meeting_date']));
                $next_meeting_data->next_meeting_time = $next_meeting_data->next_meeting_time ? $next_meeting_data->next_meeting_time : '00:00:00';
                if ($next_meeting_data->save()) {
                    $return_result = array('status' => 1, 'redirect' => Yii::app()->createUrl('Reports/mom'));
                } else {
                    $return_result = array('status' => 0);
                }
            } else {
                $return_result = array('status' => 0);
            }
        } else {
            $return_result = array('status' => 0);
        }
        echo json_encode($return_result);
        exit;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['SiteMeetings'])) {
            $model->attributes = $_POST['SiteMeetings'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('SiteMeetings');
        $this->render(
            'index',
            array(
                'dataProvider' => $dataProvider,
            )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new SiteMeetings('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['SiteMeetings']))
            $model->attributes = $_GET['SiteMeetings'];

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = SiteMeetings::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'site-meetings-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actionapproveMeeting()
    {

        $meeting_tbl = SiteMeetings::model()->tableSchema->rawName;
        $template = MailTemplate::model()->findByPK(2);
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        if (isset($_REQUEST['id'])) {
            extract($_REQUEST);
            $status = array('Approve' => 1, 'Reject' => 2);
            if ($req) {
                $sts = $status[$req];
                if ($sts == 1) {
                    $finalstatus = 'Approved';
                } else {
                    $finalstatus = 'Rejected';
                }
                $reason = $_REQUEST['reason'];
                $allids = $_REQUEST['id'];
                foreach ($allids as $key => $id) {
                    $update_sql = "UPDATE $meeting_tbl SET 
					`approved_status`=" . $sts . ",`reason`='" .
                        $reason . "',`approve_decision_by`=" . Yii::app()->user->id . ",
					`approve_decision_date`='" . date('Y-m-d h:i:s') . "'  WHERE `id`=" . $id;
                    Yii::app()->db->createCommand($update_sql)->execute();
                    $model = SiteMeetings::model()->findByPk($id);
                    $participants = MeetingParticipants::model()->findAll(['condition' => 'meeting_id = ' . $id]);
                    $minutes_points = MeetingMinutes::model()->findAll(['condition' => 'meeting_id = ' . $id]);
                    $project_det = Projects::model()->findByPk($model->project_id);

                    $tasklist = $this->renderPartial('//reports/minutes_view', array(
                        'model' => $model,
                        'participants' => $participants,
                        'minutes_points' => $minutes_points,
                        'project' => $project_det,
                        'pdf' => 1
                    ), true);
                    $mPDF1 = Yii::app()->ePdf->mPDF();
                    $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-P');
                    $mPDF1->WriteHTML($stylesheet, 1);
                    $mPDF1->WriteHTML($tasklist);
                    $file = 'meeting' . $id . '.pdf';
                    $path = realpath(Yii::app()->basePath . '/../uploads/meeting_pdf/');
                    $folder = $path . "/" . $file;
                    $mPDF1->Output($folder, 'F');
                    $meetingUsers = $this->meetingUsers($id);
                    $site_meeting = SiteMeetings::model()->findByPK($id);
                    $string = "";
                    foreach ($meetingUsers as $user) {

                        $mail_to = $user['email'];
                        if ($template) {
                            $string = $template->temp_content;
                        }

                        $search = array('{name}', '{meeting_date}', '{approved_by}', '{from_name}');
                        $approved_by = $site_meeting->createdBy->first_name . " " . $site_meeting->createdBy->last_name;
                        $from_name = "Ashly";
                        $replace = array($user['first_name'], $site_meeting->date, $approved_by, $from_name);
                        $body = str_replace($search, $replace, $string, $count);
                        //	$this->sendEmail($mail_to, $body, $folder);
                        $model = new MailLog;
                        $model->send_to = $mail_to;
                        $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
                        $mail_send_byJSON = json_encode($mail_send_by);
                        $model->send_by = $mail_send_byJSON;
                        $model->send_date = date('Y-m-d H:i:s');
                        $model->message = htmlentities($body);
                        $model->description = "Meeting Approved";
                        $model->created_date = date('Y-m-d');
                        $model->created_by = Yii::app()->user->id;
                        $model->mail_type = "Meeting Approved";
                        $model->sendemail_status = 0;
                        $model->save();
                    }
                }

                $ret = array('status' => 1);
            }

            echo json_encode($ret);
        }
    }
    public function getActionBy($minutes_id)
    {

        $user = Yii::app()->db->createCommand()
            ->select(['concat(first_name," ",last_name) as name'])
            ->from('pms_users u')
            ->join('pms_meeting_minutes_users p', 'u.userid=p.user_id')
            ->where('meeting_minutes_id=:meeting_minutes_id', array(':meeting_minutes_id' => $minutes_id))
            ->queryRow();
        echo $user['name'];
    }

    public function sendEmail($mail_to, $body, $folder)
    {
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $mail = new JPhpMailer();
        try {

            //$mail->SMTPDebug = 2;
            $mail->isSMTP();
            $mail->Host = $mail_model['smtp_host'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->setFrom($mail_model['smtp_email_from'], 'Ashly');
            $mail->addAddress($mail_to);
            $mail->isHTML(true);
            $mail->Subject = 'Subject';
            $mail->Body = $body;
            if ($folder != "") {
                $mail->AddAttachment($folder);
            }
            $mail->send();
            //echo "Mail has been sent successfully!";
        } catch (Exception $e) {
            //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";

            return true;
        }
    }

    public function meetingUsers($id)
    {
        $users = Yii::app()->db->createCommand()
            ->from('pms_users u')
            //->select('*')			
            ->join('pms_meeting_minutes_users p', 'u.userid=p.user_id')
            ->join('pms_meeting_minutes m', 'p.meeting_minutes_id=m.id')
            ->group('user_id')
            ->where('m.meeting_id=:meeting_minutes_id', array(':meeting_minutes_id' => $id))
            ->queryAll();
        return $users;
    }
    public function actionsendMail()
    {
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $mail = new JPhpMailer(true);
        try {
            $mail->SMTPDebug = 2;
            $mail->isSMTP();
            $mail->Host = $mail_model['smtp_host'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->setFrom($mail_model['smtp_email_from'], 'Ashly');
            $mail->addAddress('jitha.ma@bluehorizoninfotech.com');
            $mail->isHTML(true);
            $mail->Subject = 'Subject';
            $mail->Body = 'Test';
            $mail->send();
            echo "Mail has been sent successfully!";
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
    public function getUsers($approved_by)
    {
        $condition = new CDbCriteria();
        $condition->addInCondition('userid', $approved_by);
        $users = Users::model()->findAll($condition);
        return $users;
    }
    public function actiontest()
    {
        $id = 164;
        // $condition = new CDbCriteria();
        // $condition->join = 'LEFT JOIN pms_meeting_minutes_users ON pms_meeting_minutes_users.user_id = t.userid';
        // $condition->addInCondition('meeting_minutes_id', [$id]);
        // $users = Users::model()->findAll($condition);
        // foreach($users as $user)
        // {
        // 	echo $user->first_name."--";
        // }
        $users = Yii::app()->db->createCommand()
            ->from('pms_users u')
            ->select('*')
            ->join('pms_meeting_minutes_users p', 'u.userid=p.user_id')
            ->join('pms_meeting_minutes m', 'p.meeting_minutes_id=m.id')
            ->group('user_id')
            ->where('m.meeting_id=:meeting_minutes_id', array(':meeting_minutes_id' => $id))
            ->queryAll();
        foreach ($users as $user) {
            echo $user['first_name'] . "--";
        }
    }
    public function actioncreate_mom()
    {
        $model = new SiteMeetings;
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        if (!empty($_POST['model_id'])) {
            $id = $_POST['model_id'];
            $model = $this->loadModel($id);
        }

        $model->setScenario('meeting_start');

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        try {
            if (isset($_POST['project_id'])) {



                $model->project_id = $_POST['project_id'];
                $model->site_id = $_POST['clientsite'];
                $model->date = $_POST['date'];
                $model->venue = $_POST['venue'];
                $model->objective = $_POST['objective'];
                $model->meeting_time = $_POST['time'];
                $model->meeting_number = $_POST['meeting_no'];
                $model->distribution = $_POST['distribution'];
                $model->approved_by = implode(',', $_POST['approved_by']);
                $model->created_by = yii::app()->user->id;
                $model->created_date = date('Y-m-d');
                $model->updated_by = yii::app()->user->id;
                $model->date = date('Y-m-d', strtotime($_POST['date']));
                $model->updated_date = date('Y-m-d H:i:s');
                $model->meeting_ref = $_POST['meeting_ref'];
                $model->meeting_title = $_POST['meeting_title'];


                if ($model->save()) {
                    Yii::app()->user->setState('site_meeting_id', $model->id);
                    if (empty($_POST['model_id'])) {
                        $template = MailTemplate::model()->findByPK(3);
                        $approved_by = explode(",", $model->approved_by);
                        $users = $this->getUsers($approved_by);
                        $string = "";
                        foreach ($users as $user) {
                            $mail_to = $user->email;
                            $folder = "";
                            if ($template) {
                                $string = $template->temp_content;
                            }
                            $search = array('{name}', '{meeting_number}', '{link}', '{from_name}');
                            $from_name = "Ashly";
                            $replace = array($user->first_name, $model->meeting_number, CHtml::link('Click here to login', $this->createAbsoluteUrl('site/index2')), $from_name);
                            $body = str_replace($search, $replace, $string, $count);
                            //$this->sendEmail($mail_to, $body, $folder);
                            $model2 = new MailLog;
                            $model2->send_to = $mail_to;
                            $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
                            $mail_send_byJSON = json_encode($mail_send_by);
                            $model2->send_by = $mail_send_byJSON;
                            $model2->send_date = date('Y-m-d H:i:s');
                            $model2->message = htmlentities($body);
                            $model2->description = "Meeting Created";
                            $model2->created_date = date('Y-m-d');
                            $model2->created_by = Yii::app()->user->id;
                            $model2->mail_type = "Meeting Created";
                            $model2->sendemail_status = 0;
                            $model2->save();
                        }
                    }
                    $milestone_condition = "status = 1 AND project_id = " . $model->project_id;
                    $milestone_data = Milestone::model()->findAll(
                        array(
                            'condition' => $milestone_condition,
                            'order' => 'milestone_title ASC',
                            'distinct' => true
                        )
                    );
                    $milestone_result = CHtml::listData($milestone_data, 'id', 'milestone_title');
                    $milestone_html = "<option value=''>Choose a Milestone</option>";
                    foreach ($milestone_result as $milestone_key => $milestone_value) {
                        $milestone_html .= "<option value=" . $milestone_key . ">" . $milestone_value . "</option>";
                    }
                    // meeting participant code here
                    $participants = ProjectParticipants::model()->findAllByAttributes(
                        array(
                            'id' => $_POST['records'],
                        )
                    );
                 

                    if (!empty($id)) {
                        $delete = MeetingParticipants::model()->deleteAll("meeting_id ='" . $id . "'");
                    }
                    if (!empty($participants)) {
                        foreach ($participants as $participant) {
                            $this->saveMomParticipants($model->id, $participant);
                        }
                    }



                    // end meeting participant code 

                    $criteria = new CDbCriteria;
                    $criteria = "report_type = 2 AND from_date != '1970-01-01' 
                     AND to_date != '1970-01-01' AND project_id = " . $model->project_id;

                    $result_data = ProjectReport::model()->findAll($criteria);

                    $report_html = "<option value=''>Choose a report</option>";

                    foreach ($result_data as $key => $value) {
                        $report_html .= "<option value=" . $value['id'] . ">" . 'Detailed Report(' . $value['from_date'] . " - " . $value['to_date'] . ")" . "</option>";
                    }
                    $return_result = array(
                        'status' => 1,
                        'next_level' => 1,
                        'model_id' => $model->id,
                        'milestones' => $milestone_html,
                        'report_html' => $report_html,
                        'meeting_status' => $model->approved_status,
                        'project_id' => $model->project_id
                    );
                } else {
                    $return_result = array('status' => 0, 'next_level' => 0);
                }
            } else {
                $return_result = array('status' => 0, 'next_level' => 0);
            }
        } catch (ErrorException $e) {
            $return_result = array('status' => 011, 'next_level' => 0, 'msg' => $e);
        }

        echo json_encode($return_result);
        exit;

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }
    public function saveMomParticipants($meeting_id, $participants)
    {
        $participant_model = new MeetingParticipants;
        $participant_model->contractor_id = '';
        $participant_model->meeting_id = $meeting_id;
        $participant_model->participant_id = isset($participants->id) ? $participants->id : '';
        $participant_model->participants = $participants->participants;
        $participant_model->participant_initial = $participants->participant_initial;
        $participant_model->designation = $participants->designation;
        $participant_model->organization_name = $participants->organization_name;
        $participant_model->organization_initial = $participants->organization_initial;
        $participant_model->created_by = yii::app()->user->id;
        $participant_model->created_date = date('Y-m-d');
        $participant_model->updated_by = yii::app()->user->id;
        $participant_model->updated_date = date('Y-m-d H:i:s');
        $participant_model->save();
    }
}
