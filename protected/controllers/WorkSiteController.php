<?php

class WorkSiteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
		
		
		
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
		$this->layout = false;
		$model=new WorkSite;

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);
		
		if(isset($_POST['WorkSite']))
		{
			//print_r($_POST['WorkSite']['site_name']); die;
			$model->attributes=$_POST['WorkSite'];
			$model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if($model->validate() && $model->save())
			{
			$data = null;
			print_r(json_encode($data));
			}
			else
			{
			$error = $model->getErrors();
			print_r(json_encode($error['site_name']));
			}
			
		}

		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		$id = $_POST['id'];
		$value = $_POST['value'];
		//echo $value; die;
		$model=$this->loadModel($id);
		$model->site_name = $value;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if($model->save())
		{
			$data = null;
			print_r(json_encode($data));
		}else
		{
			$error = $model->getErrors(); 
			print_r(json_encode($error['site_name']));
		}
		
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{	
		
		$ws1 = Yii::app()->db->createCommand()
                    ->select(['work_site'])
                    ->from('tms_stock_details')
                    ->where('work_site ='.$id)
                    ->queryAll();
		$ws2 = Yii::app()->db->createCommand()
					->select(['allocated_site'])
					->from('tms_tool_allocation')
					->where('allocated_site ='.$id)
					->queryAll();


		if($ws1 == NULL && $ws2 == NULL)
		{  
			//echo "hi"; die;
			$this->loadModel($id)->delete();
			echo json_encode(array('response'=> 'success'));
 		}
		else
		{	
			//echo "hello"; die;
 			echo json_encode(array('response'=> 'fail'));
 		}
		
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(array('workSite/admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('WorkSite');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new WorkSite('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['WorkSite']))
			$model->attributes=$_GET['WorkSite'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=WorkSite::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='work-site-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
