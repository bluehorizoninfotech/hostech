<?php

class ReportsTemplateController extends Controller
{
	public $layout = '//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		$accessArr = array();
		$accessauthArr = array();
		$accessguestArr = array();
		$controller = Yii::app()->controller->id;

		if (isset(Yii::app()->session['menuauth'])) {
			if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
				$accessauthArr = Yii::app()->session['menuauth'][$controller];
			}
		}

		$access_privlg = count($accessauthArr);

		return array(
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('SaveTemplateData', 'Preview'),
				'users' => array('@'),
			),
			array(
				'deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$model = new ReportsTemplate('search');
		if (isset($_POST['template_id']) && $_POST['template_id']) {
			$model = $this->loadModel($_POST['template_id']);
		}
		// $model->unsetAttributes();  // clear any default values
		$template_types = ReportsTemplate::model()->findAll(array("condition" => " status = '1'", "select" => "template_id,template_name"));
		$this->render(
			'index',
			array(
				'model' => $model,
				'dataProvider' => $model->search(),
				'template_types' => $template_types
			)
		);
	}
	public function actionSaveTemplateData($id, $type = 'quotation')
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$model = ($id != '') ? $this->loadModel($id) : new ReportsTemplate();
		$response = array();
		$response['success'] = false;
		$response['errors'] = "No data";
		if (Yii::app()->request->isAjaxRequest && isset($_POST['ReportsTemplate'])) {
			$model->attributes = $_POST['ReportsTemplate'];
			$str = $_POST['ReportsTemplate']['template_format'];
			$clean_content = $this->cleanHtmlContent($str);
			$encoded_data = htmlentities($clean_content);
			$model->template_format = $encoded_data;
			$dom = new DOMDocument();
			libxml_use_internal_errors(true);
			if ($_POST['ReportsTemplate']['template_format']) {
				$dom->loadHTML($_POST['ReportsTemplate']['template_format']);
				$errors = libxml_get_errors();
			}
			if (isset($errors) && $errors) {
				$response['success'] = false;
				$response['errors'] = isset($errors[0]->line) && $errors[0]->line ? "error at line " . $errors[0]->line . " in template" : "Invalid template format. Please check!";
			} else {
				if ($model->save()) {
					$response['message'] = "Template updated successfully!";
					$response['success'] = true;
				} else {
					$response['errors'] = implode(PHP_EOL, array_map(function ($attributeErrors) {
						return implode(PHP_EOL, $attributeErrors);
					}, $model->getErrors()));
				}
			}
		}
		echo json_encode($response);
		exit;
	}
	public function actionPreview()
	{

		if (isset($_POST['template_data'])) {
			$template_data = $_POST['template_data'];
			$template_id = $_POST['template_id'];
			$sample_template_data = $_POST['sample_template_data'];
			if ($template_id == 1) {
				echo $this->setHtmlContent($template_data, $sample_template_data);
			}else if($template_id == 2) {
				echo $this->setWeeklyReportContent($template_data, $sample_template_data);
			}
		} else {
			return;
		}
	}


	public function loadModel($id)
	{
		$model = ReportsTemplate::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'template-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


}