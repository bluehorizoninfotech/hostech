<?php

Yii::app()->getModule('masters');


class TasksController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;


        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'savetopdf',
                    'getlocation',
                    'reportsearch',
                    'mytask',
                    'test',
                    'filterbygroup',
                    'fact',
                    'getclientlocationbyid',
                    'getassignedperson',
                    'view',
                    'getclientlocation',
                    'gettaskdetails',
                    'changestatus',
                    'getprojectbytaskid',
                    'getMainTasks',
                    'checktaskquantity',
                    'changedate',
                    'createproject',
                    'createunit',
                    'createmilestone',
                    'durationcalculation',
                    'dependantdata',
                    'taskdatevalidation',
                    'getmaintaskdate',
                    'dateChangeValidation',
                    'getdependancydate',
                    'workerslimit',
                    'expiredconsolidatedmail',
                    'createcontractor',
                    'createworktype',
                    'getmodel',
                    'addmaintask',
                    'gettaskForm',
                    'addItemEstimate',
                    'import_task_estimate',
                    'CreateHandsontable',
                    'checkProject',
                    'getHandsontbl',
                    'generateItemEstimation',
                    'monthlyTask',
                    'createWeeklytask',
                    'gettaskclientsite',
                    'getbudgetheadmilestone',
                    'getmilestone',
                    'getparentaskmilestone',
                    'getparenttask',
                    'searchSession',
                    'unsetSession',
                    'calcdurationnotification',
                    'getTaskData',
                    'unsetTaskSession',
                    'createexpire',
                    'gettasks',
                    'createtimeentry',
                    'UpdateExpire',
                    'createexpireexternaltask',
                    'getAssignedUser',
                    'createTask',
                    'createColumnSession',
                    'createExpandSession',
                    'getHighestRank'

                ),
                'users' => array('@'),
            ),
            array(
                'allow',
                'actions' => array(
                    'senddailytasknotificationcron',
                ),
                'users' => array('*'),
            ),

            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewpartial($id)
    {
        $this->renderPartial(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    public function actionAssignproject()
    {
        if (!isset($_POST['pid']) or !isset($_POST['checkd_tsk']))
            echo "Service unavailable";
        else {
            $command = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tasks set project_id="' . $_POST['pid'] . '",updated_date = now(),
                  updated_by= ' . yii::app()->user->id . '
                  WHERE tskid in (' . $_POST['checkd_tsk'] . ')');
            $num = $command->execute();  //Get number of affected records after update
            echo "Successfully $num Task(s) assigned.";
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $boq_model = new DailyWorkProgress('task_search');
        $boq_model->taskid = $id;


        if (isset($_REQUEST['DailyWorkProgress'])) {
            $boq_model->attributes = $_REQUEST['DailyWorkProgress'];
        }
        if (Yii::app()->request->isAjaxRequest) {
            //outputProcessing = true because including css-files ...
            $this->renderPartial('view', array(
                'model' => $model,
                'boq_model' => $boq_model
            ), false, true);
            //js-code to open the dialog
            if (!empty($_GET['asDialog']))
                echo CHtml::script('$("#dlg-tasks-view").dialog("open")');
            Yii::app()->end();
        } else
            $this->render(
                'view',
                array(
                    'model' => $model,
                    'boq_model' => $boq_model
                )
            );
    }

    public function createTaskMail($id)
    {

        $model = $this->loadModel($id);
        $coordinator = "";
        $contractor_title = "";
        $contractor_mail = "";
        if (isset($model->coordinator)) {
            $coordinator = $model->coordinator0->first_name;
        }
        if (isset($model->contractor_id)) {
            $contractor_title = $model->contractor->contractor_title;
        }
        $app_root = YiiBase::getPathOfAlias('webroot');
        $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
        $logo = $theme_asset_url . 'default-logo.png';
        if (file_exists($app_root . "/themes/assets/logo.png")) {
            $logo = $theme_asset_url . 'logo.png';
        }



        $mail_body = '
<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
<h3 style="padding:0 0 6px 0;margin: 0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $model->assignedTo->first_name . '</span></h3>
<table style="margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
<tr><td colspan="2" style="background-color: #ffff;color: #ffff;"><div style="margin-left:20px;"><img src="' . $logo . '" width="50" height="50" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
</tr>
<tr><td colspan="2"><div style="text-align:center;"><h2>PMS - New Task created</h2></div></td>
</tr>
<tr><td colspan="2" style="color:blue;">This is a notification to inform that new task #' . $id . ' has been created: </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Task Name: </td><td style="border:1px solid #f5f5f5;padding:20px">' . $model->title . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Project Name: </td><td style="border:1px solid #f5f5f5;padding:20px">' . $model->project->name . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;padding:20px">' . date('d-M-y', strtotime($model->start_date)) . ' - ' . date('d-M-y', strtotime($model->due_date)) . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Priority:</td><td style="border:1px solid #f5f5f5;padding:20px">' . $model->priority0->caption . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Assigned To:</td><td style="border:1px solid #f5f5f5;padding:20px">' . $model->assignedTo->first_name . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Coordinator:</td><td style="border:1px solid #f5f5f5;padding:20px">' . $coordinator . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Created by: </td><td style="border:1px solid #f5f5f5;padding:20px">' . $model->createdBy->first_name . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Billable:</td> <td style="border:1px solid #f5f5f5;padding:20px">' . $model->billable0->caption . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Quantity: </td><td style="border:1px solid #f5f5f5;padding:20px">' . $model->quantity . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Task status:</td> <td style="border:1px solid #f5f5f5;padding:20px">' . $model->status0->caption . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Description:</td> <td style="border:1px solid #f5f5f5;padding:20px">' . nl2br($model->description) . '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;" colspan="2">' . CHtml::link('Click here to see this task', $this->createAbsoluteUrl('tasks/view', array('id' => $id))) . '</td></tr>
</table>

<p>Sincerely,  <br />
' . Yii::app()->name . '</p>
</div>';


        $cbody = '
<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $coordinator . '</span></h3>
<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
<tr><td colspan="2" style="background-color: #ffff;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $logo . '" width="50" height="50" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
</tr>
<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>PMS - New Task created</h2></div></td>
</tr>
<tr><td colspan="2">This is a notification to inform that new task #' . $id . ' has been created to <b>' . $model->assignedTo->first_name . '</b>: </td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $model->title . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project Name: </td><td style="border:1px solid #f5f5f5;">' . $model->project->name . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($model->start_date)) . ' - ' . date('d-M-y', strtotime($model->due_date)) . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Priority:</td><td style="border:1px solid #f5f5f5;">' . $model->priority0->caption . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Assigned To:</td><td style="border:1px solid #f5f5f5;">' . $model->assignedTo->first_name . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Coordinator:</td><td style="border:1px solid #f5f5f5;">' . $coordinator . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Created by: </td><td style="border:1px solid #f5f5f5;">' . $model->createdBy->first_name . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Billable:</td> <td style="border:1px solid #f5f5f5;">' . $model->billable0->caption . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Quantity: </td><td style="border:1px solid #f5f5f5;">' . $model->quantity . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task status:</td> <td style="border:1px solid #f5f5f5;">' . $model->status0->caption . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Description:</td> <td style="border:1px solid #f5f5f5;">' . nl2br($model->description) . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;" colspan="2">' . CHtml::link('Click here to see this task', $this->createAbsoluteUrl('tasks/view', array('id' => $id))) . '</td></tr>
</table>

<p>Sincerely,  <br />
' . Yii::app()->name . '</p>
</div>';
        /* mail to contractor body starts */
        $contractorbody = '
<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $contractor_title . '</span></h3>
<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
<tr><td colspan="2" style="background-color: #ffff;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $logo . '" width="50" height="50" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
</tr>
<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>PMS - New Task created</h2></div></td>
</tr>
<tr><td colspan="2">This is a notification to inform  that new task #' . $id . ' has been created <b></b>: </td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $model->title . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project Name: </td><td style="border:1px solid #f5f5f5;">' . $model->project->name . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($model->start_date)) . ' - ' . date('d-M-y', strtotime($model->due_date)) . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Priority:</td><td style="border:1px solid #f5f5f5;">' . $model->priority0->caption . '</td></tr>


<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Created by: </td><td style="border:1px solid #f5f5f5;">' . $model->createdBy->first_name . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Billable:</td> <td style="border:1px solid #f5f5f5;">' . $model->billable0->caption . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Quantity: </td><td style="border:1px solid #f5f5f5;">' . $model->quantity . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task status:</td> <td style="border:1px solid #f5f5f5;">' . $model->status0->caption . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Description:</td> <td style="border:1px solid #f5f5f5;">' . nl2br($model->description) . '</td></tr>
<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;" colspan="2">' . CHtml::link('Click here to see this task', $this->createAbsoluteUrl('tasks/view', array('id' => $id))) . '</td></tr>
</table>

<p>Sincerely,  <br />
' . Yii::app()->name . '</p>
</div>';
        /* mail to contractor body ends */


        /* assignee mail */
        $asubject = '[Task Assigned] #' . $id . ': ' . $model->title;
        $asubject_view = 'Task Assigned #' . $id . ': ' . $model->title;


        /* assignee mail log */
        $assignee_mail_log = $this->saveMailLog($model->assignedTo->email, $asubject, $mail_body);
        /* end assignee mail mlog */

        /* assignee mail */

        $this->send_mail($model->assignedTo->first_name, $model->assignedTo->email, $asubject, $mail_body, $asubject_view, $assignee_mail_log);

        /* assignee mail  */


        /* coordinator mail log*/
        $csubject = '[New Task] #' . $id . ' Created to ' . $model->assignedTo->first_name . ': ' . $model->title;
        $csubject_view = 'New Task #' . $id . ': ' . $model->title;
        $created_mail_to = $model->reportTo->email;
        if (!empty($model->coordinator0->email)) {
            $created_mail_to .= ',' . $model->coordinator0->email;
        }

        /*  coordinator mail log */

        if ($created_mail_to != "") {
            $coordinator_mail_log = $this->saveMailLog($created_mail_to, $csubject, $cbody);
            $this->send_mail($coordinator, $created_mail_to, $csubject, $cbody, $csubject_view, $coordinator_mail_log);
        }

        /* end coordinator mail */

        /* contractor mail */
        $contractor_subject = '[New Task] #' . $id . ' Created ' . ': ' . $model->title;
        $contractor_subject_view = 'New Task #' . $id . ': ' . $model->title;

        if (!empty($model->contractor->email_id)) {
            // $contractor_mail .= ',' . $model->contractor->email_id;
            $contractor_mail .= $model->contractor->email_id;
        }
        if (!empty($model->contractor->contractor_title)) {
            $contractor_title .= ',' . $model->contractor->contractor_title;
        }

        if ($contractor_mail != "") {

            /*  contractor mail mlog */
            $contractor_mail_log = $this->saveMailLog($contractor_mail, $contractor_subject, $contractorbody);
            /* end contractor mail */

            $this->send_mail($contractor_title, $contractor_mail, $contractor_subject, $contractorbody, $contractor_subject_view, $contractor_mail_log);
        }
    }

    public function saveMailLog($tomail, $subject, $body)
    {
        $model2 = new MailLog;
        $today = date('Y-m-d H:i:s');
        $bodyContent = $body;
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);

        $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);


        $mail_send_byJSON = json_encode($mail_send_by);

        $recepients_mailids = explode(',', $tomail);

        if (!empty($recepients_mailids)) {
            $to_mailids = implode(',', $recepients_mailids);
        }

        $model2->send_to = $to_mailids;
        $model2->send_by = $mail_send_byJSON;
        $model2->send_date = $today;

        $model2->message = htmlentities($bodyContent);
        $model2->description = $subject;
        $model2->created_date = $today;
        $model2->created_by = Yii::app()->user->getId();
        $model2->mail_type = $subject;
        $model2->sendemail_status = 115;
        $model2->save();
        return $model2->log_id;
    }

    private function send_mail123($toname, $tomail, $subject, $body, $subject_view)
    {

        $model2 = new MailLog;
        $today = date('Y-m-d H:i:s');
        $mail = new JPhpMailer();
        $headers = "Ashly";
        $bodyContent = $body;
        // echo '<pre>';print_r($bodyContent);exit;
        $mail->IsSMTP();
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $mail->Host = $mail_model['smtp_host'];
        $mail->SMTPSecure = $mail_model['smtp_secure'];
        $mail->SMTPAuth = $mail_model['smtp_auth'];
        $mail->Username = $mail_model['smtp_username'];
        $mail->Password = $mail_model['smtp_password'];
        $mail->Port = $mail_model['smtp_port'];
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
        $mail->Subject = $subject;
        $assigned_model = Users::model()->findByPk(isset($assigned_user_id));
        $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
        $recepients_mailids = explode(',', $tomail);

        if (!empty($recepients_mailids)) {
            $to_mailids = implode(',', $recepients_mailids);

            foreach ($recepients_mailids as $mailid) {

                if (!empty($mailid)) {
                    $mail->addAddress($mailid); // Add a recipient
                }
            }
        }

        $mail_send_byJSON = json_encode($mail_send_by);
        $subject = $mail->Subject;
        $model2->send_to = $to_mailids;
        $model2->send_by = $mail_send_byJSON;
        $model2->send_date = $today;
        $model2->message = htmlentities($bodyContent);
        $model2->description = $mail->Subject;
        $model2->created_date = $today;
        $model2->created_by = Yii::app()->user->getId();
        $model2->mail_type = $subject;
        $mail->isHTML(true);
        $mail->MsgHTML($bodyContent);
        $mail->Body = $bodyContent;
        if (!empty($recepients_mailids)) {
            if (@$mail->Send()) {

                $model2->sendemail_status = 1;
            } else {
                $model2->sendemail_status = 0;
            }
        } else {
            $model2->sendemail_status = 0;
        }
        if (@$mail->Send()) {

            $model2->sendemail_status = 1;
        } else {
            $model2->sendemail_status = 0;
        }
        $model2->save();
    }
    private function send_mail($toname, $tomail, $subject, $body, $subject_view, $mail_log_id)
    {


        $model2 = MailLog::model()->findByPk($mail_log_id);
        $today = date('Y-m-d H:i:s');
        $mail = new JPhpMailer();
        $headers = "Ashly";
        $bodyContent = $body;
        // echo '<pre>';print_r($bodyContent);exit;
        $mail->IsSMTP();
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $mail->Host = $mail_model['smtp_host'];
        $mail->SMTPSecure = $mail_model['smtp_secure'];
        $mail->SMTPAuth = $mail_model['smtp_auth'];
        $mail->Username = $mail_model['smtp_username'];
        $mail->Password = $mail_model['smtp_password'];
        $mail->Port = $mail_model['smtp_port'];
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
        $mail->Subject = $subject;
        $assigned_model = Users::model()->findByPk(isset($assigned_user_id));
        $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
        $recepients_mailids = explode(',', $tomail);

        if (!empty($recepients_mailids)) {
            $to_mailids = implode(',', $recepients_mailids);

            foreach ($recepients_mailids as $mailid) {

                if (!empty($mailid)) {
                    $mail->addAddress($mailid); // Add a recipient
                }
            }
        }

        $mail_send_byJSON = json_encode($mail_send_by);
        $subject = $mail->Subject;
        $mail->isHTML(true);
        $mail->MsgHTML($bodyContent);
        $mail->Body = $bodyContent;
        if (!empty($recepients_mailids)) {
            if (@$mail->Send()) {

                $model2->sendemail_status = 1;
            } else {
                $model2->sendemail_status = 0;
            }
        } else {
            $model2->sendemail_status = 0;
        }
        if (@$mail->Send()) {

            $model2->sendemail_status = 1;
        } else {
            $model2->sendemail_status = 0;
        }
        $model2->save();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Tasks;
        $location = array();

        $assigned_to = array();

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tasks'])) {

            $model->attributes = $_POST['Tasks'];
            $model->email = $_POST['Tasks']['email'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->entry_date = date('y-m-d', strtotime($model->entry_date));

            if ($model->save()) {
                //  $this->milestonedatechange($model);
                $this->createTaskMail($model->tskid); //send mail
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent..update('tasks-grid');");
                    Yii::app()->end();
                } else {
                    $this->redirect(array('index'));
                }
            }
        }
        //----- begin new code --------------------
        if (!empty($_GET['asDialog']))
            $this->layout = '//layouts/iframe';
        //----- end new code --------------------

        $this->render(
            'create',
            array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to,
            )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {


        $model = $this->loadModel($id);
        $assigned_id_val = $model->assigned_to;
        $previous_template_id = $model->template_id;
        $contractor_id_old = $model->contractor_id;
        $cordinator_id = $model->coordinator;
        $task_type_prev = $model->task_type;
        $location = array();
        $assigned_to = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $c_site = Yii::app()->db->createCommand("select  project_id,clientsite_id FROM {$tblpx}tasks WHERE tskid =" . $id . "")->queryRow();

        //$location = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite WHERE id =".$c_site['clientsite_id']."")->queryRow();
        if ($c_site['project_id'] != '') {
            $location = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite WHERE pid=" . $c_site['project_id'] . "")->queryAll();
        }

        if ($c_site['clientsite_id'] != '') {
            $assigned_to = Yii::app()->db->createCommand("SELECT pms_users.first_name, pms_clientsite_assigned.user_id, pms_users.last_name, pms_users.userid, CONCAT_WS(' ', pms_users.first_name, pms_users.last_name) AS whole_name FROM pms_clientsite_assigned INNER JOIN pms_users ON pms_clientsite_assigned.user_id= pms_users.userid WHERE site_id = " . $c_site['clientsite_id'] . "")->queryAll();
        }
        //print_r($location);
        //$client_site =
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tasks'])) {

            $status = $model->status;
            $model->attributes = $_POST['Tasks'];
            $model->updated_date = date('Y-m-d');
            $model->start_date = date('Y-m-d', strtotime($model->start_date));
            $model->due_date = date('Y-m-d', strtotime($model->due_date));
            $model->email = $_POST['Tasks']['email'];
            $model->task_type = $_POST['Tasks']['task_type'];
            if ($model->coordinator == Yii::app()->user->id || Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
                $model->status = $_POST['Tasks']['status'];
            } else {
                $model->status = $status;
            }

            if ($model->assigned_to != "" && $model->status == 75) {
                $model->status = 6;
            }
            //update parent task id
            $model->parent_tskid = $_POST['Tasks']['parent_tskid'];

            if ($task_type_prev != $model->task_type) {

                if ($task_type_prev == 1) {
                    $boq_check = DailyWorkProgress::model()->findAll(array('condition' => 'taskid =' . $model->tskid));
                    if (!empty($boq_check)) {
                        $model->task_type = $task_type_prev;
                    }
                } else {
                    $time_entry = TimeEntry::model()->findAll(array('condition' => 'tskid =' . $model->tskid));
                    if (!empty($time_entry)) {
                        $model->task_type = $task_type_prev;
                    }
                }
            }

            if ($model->task_type == 2) {
                if (isset($_POST['ganttchart']) && $_POST['ganttchart'] == 1) {
                    $model->gantt_chart_status = 1;
                } else {
                    $model->gantt_chart_status = 0;
                }

                if (isset($_POST['project_schedule']) && $_POST['project_schedule'] == 1) {
                    $model->project_schedule_status = 1;
                } else {
                    $model->project_schedule_status = 0;
                }
            }
            if ($model->task_type == 1 && $model->parent_tskid == "") {

                if (isset($_POST['Tasks']['wpr_status']) && $_POST['Tasks']['wpr_status'] == 1) {
                    $model->wpr_status = 0;
                } else {
                    $model->wpr_status = 1;
                }

            }

            if ($model->save()) {





                if (isset($_POST['item_qty']) && count($_POST['item_qty']) > 0) {
                    $this->addItemEstimation($model->tskid, $model->project_id, $_POST['item_qty'], $_POST['item_name']);
                }



                // update dependancyBasedDate
                $data = TaskDependancy::model()->find(array('condition' => 'task_id =' . $id));
                if (empty($data)) {
                    $this->dependancyBasedDate($_POST, $model);
                } else {
                    if (TaskDependancy::model()->deleteAll('task_id', $id)) {
                        $this->dependancyBasedDate($_POST, $model);
                    }
                }
                if ($model->status == '7') {
                    $this->taskCompletionMail($model->tskid); //send mail
                }

                if ($model->assigned_to != $assigned_id_val) {

                    $this->dataChangeMail($model->tskid, 1); //1=>assignee change
                }
                if ($model->coordinator != $cordinator_id) {

                    $this->dataChangeMail($model->tskid, 2); //2=>cordinator change
                }
                if ($model->contractor_id != $contractor_id_old) {

                    $this->dataChangeMail($model->tskid, 3); //3 =>contractor change
                }

                if ($model->contractor_id != "") {
                    $this->taskUpdateEmail($model->tskid);
                }



                //$this->milestonedatechange($model);
                Yii::app()->user->setFlash('success', 'Successfully updated.');
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {


                    //----- end new code --------------------
                    if (Yii::app()->user->role == 1) {
                        Yii::app()->user->setFlash('success', 'Successfully updated.');
                        $this->redirect(array('index2', 'id' => $model->tskid));
                    } else {
                        Yii::app()->user->setFlash('success', 'Successfully updated.');
                        $this->redirect(array('mytask', 'id' => $model->tskid));
                    }
                }
            }
            // else{
            //     echo '<pre>';print_r($model->getErrors());exit;
            // }
        }
        //----- begin new code --------------------
        if (!empty($_GET['asDialog']))
            $this->layout = '//layouts/iframe';
        //----- end new code --------------------

        $template = Template::model()->findAll();

        $this->render(
            'update',
            array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to,
                'template' => $template
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        // $model = $this->loadModel($id);
        // $model->trash = 0;
        // $model->save(false);
        // //$this->loadModel($id)->delete();
        // // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        // if (!isset($_GET['ajax']))
        //     $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index2'));

        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $parent_tasks = Tasks::model()->findByAttributes(
                array(
                    'parent_tskid' => $id,
                )
            );
            $wpr = DailyWorkProgress::model()->findByAttributes(
                array(
                    'taskid' => $id,
                )
            );
            $time_entry = TimeEntry::model()->findByAttributes(
                array(
                    'tskid' => $id,
                )
            );
            if ($parent_tasks) {
                $success_status = 2;
            } else if ($wpr) {
                $success_status = 2;
            } else if ($time_entry) {

                $success_status = 2;
            } else {

                if (!$model->delete()) {
                    $success_status = 0;
                    throw new Exception(json_encode($model->getErrors()));
                } else {
                    $success_status = 1;
                }
            }



            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {

                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else if ($success_status == 2) {
                echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
            } else {

                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($type = '')
    {
        $model = new Tasks('search');

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tasks']))
            $model->attributes = $_GET['Tasks'];

        $this->render(
            'index',
            array(
                'model' => $model,
                'type' => $type
            )
        );
    }

    public function actionIndex2($type = '', $type_closed = '', $unassigned = '')
    {
        //$this->allowPermission('4');
        $model = new Tasks('search');
        $pmodel = new ImportForm();
        $model->unsetAttributes();  // clear any default values
        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end

        if (Yii::app()->user->project_id != "") {
            $model->project_id = Yii::app()->user->project_id;
        }
        if ($type == 'unassigned') {
            $model->status = 75;
        }
        if (isset($_GET['Tasks'])) {
            $model->attributes = $_GET['Tasks'];
            if ($type == 'unassigned' && !empty($_GET['Tasks']['status'])) {
                $type = '';
            }
        }

        if ($model->project_id != '' && $model->project_id != 0) {
            $unassigned_tasks = Tasks::model()->findAll(['condition' => 'project_id = ' . $model->project_id . ' AND status = 75']);
            $unassigned_tasks_count = count($unassigned_tasks);
        } else {
            if (Yii::app()->user->role == 1) {
                $unassigned_tasks = Tasks::model()->findAll(['condition' => 'status = 75']);
                $unassigned_tasks_count = count($unassigned_tasks);
            } else {
                $criteria = new CDbCriteria();
                $condition = 't.created_by=' . Yii::app()->user->id . ' and status = 75'
                    . ' OR assigned_to=' . Yii::app()->user->id . ' and status = 75'
                    . ' OR coordinator=' . Yii::app()->user->id . ' and status = 75'
                    . ' OR report_to = ' . Yii::app()->user->id . ' and status = 75';
                $criteria->addCondition($condition);
                $unassigned_tasks = Tasks::model()->findAll($criteria);
                $unassigned_tasks_count = count($unassigned_tasks);
            }
        }

        $expmodel = TaskExpiry::model()->findByPk(1);
        $fut_date = strtotime(date('Y-m-d'));
        if ($expmodel) {
            $date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));
            $fut_date = strtotime($date);
        }

        $this->render(
            'index2',
            array(
                'model' => $model,
                'type' => $type,
                'pmodel' => $pmodel,
                'unassigned_tasks_count' => $unassigned_tasks_count,
                'type_closed' => $type_closed,
                'fut_date' => $fut_date,
                'projects_list' => array(),
                'assigned_to_list' => array(),
                'co_ordinator_list' => array(),
                'status_list' => array(),
                'task_owner_list' => array(),
                'contractor_list' => array(),

            )
        );
    }

    /**
     * List only assigend tasks.
     */
    public function actionMytask($type = '')
    {
        //die;
        //$model = new Tasks('assinged_task');
        $model = new Tasks('assigned_task');

        $model->unsetAttributes();  // clear any default values
        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end
        if (Yii::app()->user->project_id != "") {

            $model->project_id = Yii::app()->user->project_id;
        }
        if (isset($_REQUEST['Tasks'])) {
            $model->attributes = $_REQUEST['Tasks'];
        }

        $this->render(
            'mytask',
            array(
                'model' => $model,
                'type' => $type
            )
        );
    }

    public function actionAutocomplete()
    {
        $res = array();
        $gpmemebr = array();
        $where = '1 = 1';
        if (isset(Yii::app()->user->group_id)) {

            $groupid = intval(Yii::app()->user->group_id);
            $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members,id',
                    'condition' => 'group_id=' . $groupid,
                )
            );
            foreach ($groupsmemeberlist as $groupsmemeber) {
                $gpmemebr[] = $groupsmemeber['group_members'];
                //$criteria->compare('assigned_to', $gpmemebr, true, 'OR');
            }
            $gpmem = implode(',', $gpmemebr);



            if (!empty($gpmemebr)) {
                $where .= ' AND (assigned_to in(' . $gpmem . ') OR assigned_to=' . Yii::app()->user->id . ')';
            }
        }
        $gpmem = array();
        if (Yii::app()->user->role > 1) {
            $members = Groups::model()->findAll(
                array(
                    'select' => 'group_id',
                    'condition' => 'group_lead=' . Yii::app()->user->id,
                )
            );
            foreach ($members as $data) {
                $list = Groups_members::model()->findAll(
                    array(
                        'select' => 'group_id,group_members,id',
                        'condition' => 'group_id=' . $data['group_id'],
                    )
                );
                foreach ($list as $groupsmem) {
                    $gpmem[] = $groupsmem['group_members'];
                }
            }
            $gpmem = implode(',', $gpmem);
            if (!empty($gpmem)) {
                $where .= ' AND (assigned_to in(' . $gpmem . ') OR assigned_to=' . Yii::app()->user->id . ')';
            }
        }
        if (Yii::app()->user->role != 1) {

            $where .= ' AND (created_by=' . Yii::app()->user->id . ' OR assigned_to=' . Yii::app()->user->id . ' OR coordinator=' . Yii::app()->user->id . ')';
        }
        $where .= ' AND trash=1';
        if (isset($_GET['term'])) {


            $qtxt = "SELECT title FROM {{tasks}} WHERE title LIKE '%" . $_GET['term'] . "%' AND $where ORDER BY title ";
            $command = Yii::app()->db->createCommand($qtxt); //$command->bindValue(":title", '%'.$_GET['term'].'%', PDO::PARAM_STR);
            $res = $command->queryColumn();
            //echo '<pre>';print_r($res);exit;
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    public function actionAutocompletetask()
    {
        $res = array();



        $where = ' (created_by=' . Yii::app()->user->id . ' OR assigned_to=' . Yii::app()->user->id . ' OR coordinator=' . Yii::app()->user->id . ') AND trash = 1';

        if (isset($_GET['term'])) {


            $qtxt = "SELECT title FROM {{tasks}} WHERE title LIKE '%" . $_GET['term'] . "%' AND $where ORDER BY title ";
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":title", '%' . $_GET['term'] . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
            //echo '<pre>';print_r($res);exit;
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Tasks('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tasks']))
            $model->attributes = $_GET['Tasks'];

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    public function actionaddtask()
    {

        $model = new Tasks;
        $model->setscenario('create');
        $location = array();
        $assigned_to = array();
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tasks'])) {

            $model->attributes = $_POST['Tasks'];
            if (!empty($_POST['Tasks']['unit'])) {
                $model->unit = $_POST['Tasks']['unit'];
            }
            $model->start_date = date('Y-m-d', strtotime($_POST['Tasks']['start_date']));
            $model->due_date = date('Y-m-d', strtotime($_POST['Tasks']['due_date']));
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->work_type_id = $_POST['Tasks']['work_type_id'];
            $model->allowed_workers = $_POST['Tasks']['allowed_workers'];
            //new parent task id
            $model->parent_tskid = $_POST['Tasks']['parent_tskid'];
            $model->email = $_POST['Tasks']['email'];
            $model->task_type = $_POST['Tasks']['task_type'];

            if ($model->task_type == 2) {
                if (isset($_POST['ganttchart']) && $_POST['ganttchart'] == 1) {
                    $model->gantt_chart_status = 1;
                } else {
                    $model->gantt_chart_status = 0;
                }

                if (isset($_POST['project_schedule']) && $_POST['project_schedule'] == 1) {
                    $model->project_schedule_status = 1;
                } else {
                    $model->project_schedule_status = 0;
                }
            }




            if ($model->save()) {
                if (isset($_POST['item_qty']) && count($_POST['item_qty']) > 0) {
                    $this->addItemEstimation($model->tskid, $model->project_id, $_POST['item_qty'], $_POST['item_name']);
                }
                $this->dependancyBasedDate($_POST, $model);
                //$this->milestonedatechange($model);
                // project session
                if (yii::app()->user->id == $_POST['Tasks']['assigned_to']) {
                    Yii::app()->user->setState('project_id', $_POST['Tasks']['project_id']);
                }
                $this->createTaskMail($model->tskid); //send mail
                Yii::app()->user->setFlash('success', 'Successfully created.');

                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {

                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index2', 'id' => $model->tskid));
                }
            }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])) {

            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------
        $unit_list = Unit::model()->findAll(['condition' => 'status = 1']);
        $template = Template::model()->findAll();
        $this->render(
            'create',
            array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to,
                'unit_list' => $unit_list,
                'template' => $template
            )
        );
    }

    protected function dependancyBasedDate($post_data, $task_model)
    {
        if (!empty($post_data['dependant_taskid']) && !empty($post_data['dependancy_percenatge'])) {
            foreach ($post_data['dependant_taskid'] as $key => $value) {
                if (!empty($value)) {
                    $model = new TaskDependancy;
                    $model->task_id = $task_model->tskid;
                    $model->dependant_task_id = $value;
                    $model->dependency_percentage = $post_data['dependancy_percenatge'][$key];
                    $model->dependant_on = $post_data['dependant_on'][$key];
                    $model->save();
                }
            }
        }
    }

    public function actionsubtask($tskid)
    {


        $model = $this->loadModel($tskid);

        $taskmodel = new Tasks;

        $location = array();
        $assigned_to = array();

        $tblpx = Yii::app()->db->tablePrefix;
        $c_site = Yii::app()->db->createCommand("select  project_id,clientsite_id FROM {$tblpx}tasks WHERE tskid =" . $tskid . "")->queryRow();

        if ($c_site['project_id'] != '') {
            $location = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite WHERE pid=" . $c_site['project_id'] . "")->queryAll();
        }
        if ($c_site['clientsite_id'] != '') {
            $assigned_to = Yii::app()->db->createCommand("SELECT pms_users.first_name, pms_clientsite_assigned.user_id, pms_users.last_name, pms_users.userid, CONCAT_WS(' ', pms_users.first_name, pms_users.last_name) AS whole_name FROM pms_clientsite_assigned INNER JOIN pms_users ON pms_clientsite_assigned.user_id= pms_users.userid WHERE site_id = " . $c_site['clientsite_id'] . "")->queryAll();
        }

        $this->performAjaxValidation($model);

        if (isset($_POST['Tasks'])) {

            $taskmodel->attributes = $_POST['Tasks'];
            $taskmodel->created_by = yii::app()->user->id;
            $taskmodel->created_date = date('Y-m-d');
            $taskmodel->updated_by = yii::app()->user->id;
            $taskmodel->updated_date = date('Y-m-d');
            $taskmodel->start_date = date('Y-m-d', strtotime($taskmodel->start_date));
            $taskmodel->due_date = date('Y-m-d', strtotime($taskmodel->due_date));

            //add parent task id
            $taskmodel->parent_tskid = $_POST['Tasks']['parent_tskid'];

            if ($taskmodel->save()) {
                $this->createTaskMail($taskmodel->tskid); //send mail

                if (!empty($_GET['asDialog'])) {

                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {

                    Yii::app()->user->setFlash('success', 'Successfully Added.');
                    // $this->redirect(array('index2', 'id' => $taskmodel->tskid));
                    $this->redirect(array('mytask'));
                }
            }
        }

        if (!empty($_GET['asDialog']))
            $this->layout = '//layouts/iframe';


        $this->render(
            'subtask',
            array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to,
            )
        );
    }

    public function actionAddnewTask()
    {

        $model = new Tasks;
        $location = array();
        $assigned_to = array();
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tasks'])) {

            $model->attributes = $_POST['Tasks'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->assigned_to = yii::app()->user->id;
            $model->report_to = yii::app()->user->id;
            $model->coordinator = yii::app()->user->id;
            $model->start_date = date('Y-m-d', strtotime($model->start_date));
            $model->due_date = date('Y-m-d', strtotime($model->due_date));


            //$model->project_id  = 306;
            //$model->clientsite_id = 1;


            if ($model->save()) {

                $this->createTaskMail($model->tskid); //send mail
                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {

                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('mytask'));
                }
            }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])) {

            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------

        $this->render(
            'newtask',
            array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Tasks::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {



        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tasks-form') {

            echo CActiveForm::validate($model);

            Yii::app()->end();
        }
    }

    public function actionSaveToPdf()
    {
        $data = array();
        $dataids = $_SESSION['taskobject'];
        $taskmodel = new Tasks();
        //        $arraylabel = array('Tskid', 'Title', 'Assigned To', 'Coordinator', 'Progress %', 'Status');

        $arraylabel = array(
            'SNo',
            'Task Id',
            'Project',
            'Title',
            'Target Quantity',
            'Task Duration',
            'Progress %',
            'Daily Target',
            'Required workers',
            'Start Date',
            'Due Date',
            'Current Status',
            'Task Type',
            'Task Owner',
            'Assigned To',
            'Coordinator',
            'Contractor'
        );


        foreach ($dataids->getData() as $record) {
            $data[] = $record;
        }


        $task_type_list = array(1 => 'External', 2 => 'Internal');
        $finaldata = array();
        foreach ($data as $key => $datavalue) {
            $progress = $datavalue->getcompleted_percent($datavalue->tskid);
            $finaldata[$key][] = $key + 1;
            $finaldata[$key][] = $datavalue->tskid;
            $finaldata[$key][] = $datavalue->project->name;
            $finaldata[$key][] = $datavalue->title;
            $finaldata[$key][] = $datavalue->quantity . " (" . $datavalue->unit0->unit_title . ")";
            $finaldata[$key][] = $datavalue->task_duration . " days";
            $finaldata[$key][] = $progress;
            $finaldata[$key][] = $datavalue->daily_target;
            $finaldata[$key][] = $datavalue->required_workers;
            $finaldata[$key][] = date('d-M-y', strtotime($datavalue->start_date));
            $finaldata[$key][] = date('d-M-y', strtotime($datavalue->due_date));
            $finaldata[$key][] = $datavalue->status0->caption;
            $finaldata[$key][] = $task_type_list[$datavalue->task_type];

            $finaldata[$key][] = $datavalue->reportTo->first_name . ' ' . $datavalue->reportTo->last_name;
            $finaldata[$key][] = $datavalue->assignedTo->first_name . ' ' . $datavalue->assignedTo->last_name;
            $finaldata[$key][] = $datavalue->coordinator0->first_name . ' ' . $datavalue->coordinator0->last_name;
            $finaldata[$key][] = $datavalue->contractor->contractor_title;
        }


        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Assigned Task report' . date('d_M_Y His') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);

        die;

        //        echo "<pre>";print_r($finaldata);die();

        $tasklist = $this->renderPartial('taskspdf', array(
            'model' => $finaldata,
            'arraylabel' => $arraylabel,
            'title' => 'Task Report',
            'filter_cr' => (isset($_GET['filter_cr']) ? $_GET['filter_cr'] : '')
        ), true);

        // $tasklist = 'Hasi';
        //$mpdf = new mPDF('c', 'A4-L');

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A1');
        //        $mPDF1->WriteHTML('default_font_size', 9);
        //        'default_font_size' => 9,
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($tasklist);


        $mPDF1->Output('tasks.pdf', 'D');
    }

    public function actionGetLocation()
    {
        $data = $this->actionGetclientsite($_REQUEST['project_id']);
        $html = '';
        if (!empty($data)) {
            $html .= '<option value="">select location</option>';
            foreach ($data as $key => $value) {
                $html .= '<option value=' . $value['id'] . '>' . $value['site_name'] . '</option>';
            }
        } else {
            $html .= '';
        }
        echo json_encode($html);
    }

    public function actionGetclientsite($project_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite WHERE pid=" . $project_id . "")->queryAll();
        return $data;
    }

    public function actionGetclientlocation()
    {
        if (!empty($_POST['pid'])) {
            $tblpx = Yii::app()->db->tablePrefix;
            $data = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite WHERE pid=" . $_POST['pid'] . "")->queryAll();
            $data = CHtml::listData($data, 'id', 'site_name');

            echo "<option value=''>Select clientsite</option>";
            foreach ($data as $value => $site_name) {
                echo CHtml::tag('option', array('value' => $value), CHtml::encode($site_name), true);
            }
        } else {
            echo "<option value=''>Select clientsite</option>";
        }
    }

    public function actionGetclientlocationbyid()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite cs JOIN {$tblpx}clientsite_assigned ca ON ca.site_id = cs.id WHERE cs.pid=" . $_POST['pid'] . " AND ca.user_id = '" . Yii::app()->user->id . "'")->queryAll();
        $data = CHtml::listData($data, 'id', 'site_name');

        echo "<option value=''>Select clientsite</option>";
        foreach ($data as $value => $site_name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($site_name), true);
        }
    }

    public function actionGetprojectbytaskid()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks t JOIN {$tblpx}projects p ON t.project_id = p.pid WHERE t.tskid='" . $_POST['pid'] . "'")->queryAll();
        $data = CHtml::listData($data, 'pid', 'name');

        echo "<option value=''>Select project</option>";
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

    public function actionServiceReport()
    {
        $model = new Tasks;
        $model->unsetAttributes();  // clear any default values

        if (isset($_REQUEST['Tasks'])) {
            $model->attributes = $_REQUEST['Tasks'];
        }

        $this->render('servicereport', array('model' => $model));
    }

    public function actionGetassignedperson()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("SELECT pms_users.first_name, pms_clientsite_assigned.user_id, pms_users.last_name, pms_users.userid, CONCAT_WS(' ', pms_users.first_name, pms_users.last_name) AS whole_name FROM pms_clientsite_assigned INNER JOIN pms_users ON pms_clientsite_assigned.user_id= pms_users.userid WHERE site_id = " . $_REQUEST['id'] . "")->queryAll();
        $data = CHtml::listData($data, 'user_id', 'whole_name');

        if (count($data) > 1) {
            echo "<option value=''>Choose a resource</option>";
        }


        foreach ($data as $value => $whole_name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($whole_name), true);
        }
    }

    public function actionfilterbygroup()
    {

        Yii::app()->user->setState('group_id', intval($_POST['group_id']));
        Yii::app()->end();
    }

    public function actionDownloadCSVsample()
    {

        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');

        $doc = 'items.csv';
        $path = Yii::getPathOfAlias('webroot') . '/uploads/';
        $file = $path . '' . $doc;
        if (file_exists($file)) {
            Yii::app()->getRequest()->sendFile($doc, file_get_contents($file));
        }
    }

    public function actionImport()
    {


        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Tasks('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['PurchaseItems']))
            $model->attributes = $_GET['PurchaseItems'];

        $pmodel = new ImportForm();

        $model->unsetAttributes();  // clear any default values
        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end


        if (isset($_GET['Tasks']))
            $model->attributes = $_GET['Tasks'];

        $newarr = array();
        $duparr = array();




        if (isset($_POST['ImportForm'])) {
            $pmodel->attributes = $_POST['ImportForm'];

            if ($pmodel->validate()) {
                //die('succes');

                $csvFile = CUploadedFile::getInstance($pmodel, 'file');
                $tempLoc = $csvFile->getTempName();

                //print_r($tempLoc );exit;

                try {

                    $transaction = Yii::app()->db->beginTransaction();
                    $handle = fopen("$tempLoc", "r");
                    $row = 1;



                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $data = array_map("utf8_encode", $data);
                        //print_r($data);exit;


                        if ($row > 1) {


                            $newmodel = new Tasks();

                            $newmodel->title = trim($data[0]);
                            // $start = isset($data[3]) ? date('Y-m-d',strtotime(trim($data[3]))) : 0 ;
                            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", trim($data[3]))) {
                                $newmodel->start_date = trim($data[3]);
                            } else {
                                $newmodel->start_date = 0;
                            }
                            // $end = isset($data[4]) ? date('Y-m-d',strtotime(trim($data[4]))) : 0 ;
                            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", trim($data[4]))) {
                                $newmodel->due_date = trim($data[4]);
                            } else {
                                $newmodel->due_date = 0;
                            }

                            $pro = Yii::app()->db->createCommand("select * FROM {$tblpx}projects WHERE name='" . $data[1] . " ' ")->queryRow();

                            if (!empty($pro)) {

                                $newmodel->project_id = $pro['pid'];
                            } else {
                                $newmodel->project_id = 0;
                            }

                            $newmodel->assigned_to = $this->getusers(trim($data[5]));
                            $newmodel->coordinator = $this->getusers(trim($data[6]));
                            $newmodel->report_to = $this->getusers(trim($data[7]));
                            $newmodel->description = trim($data[8]);

                            $prarray = array('Highest' => 11, 'Low' => 12, 'Medium' => 10);
                            //echo ucfirst(strtolower(trim($data[2])));exit;
                            if (isset($prarray[ucfirst(strtolower(trim($data[2])))])) {
                                $newmodel->priority = $prarray[ucfirst(strtolower(trim($data[2])))];
                            } else {
                                $newmodel->priority = 0;
                            }
                            //echo $newmodel->priority;exit;
                            $newmodel->created_by = yii::app()->user->id;
                            $newmodel->created_date = date('Y-m-d');
                            $newmodel->updated_by = yii::app()->user->id;
                            $newmodel->updated_date = date('Y-m-d');

                            $check = Yii::app()->db->createCommand('SELECT * FROM ' . $tblpx . 'tasks WHERE `project_id`= ' . $newmodel->project_id . ' and `title`= "' . $newmodel->title . '" and `start_date`= "' . $newmodel->start_date . '" and `due_date`= "' . $newmodel->due_date . '" and `assigned_to`= "' . $newmodel->assigned_to . '" and `report_to`="' . $newmodel->report_to . '" and `coordinator`= "' . $newmodel->coordinator . '" ')->queryRow();


                            if (empty($check) && $newmodel->project_id != 0 && $newmodel->assigned_to != 0 && $newmodel->coordinator != 0 && $newmodel->report_to != 0 && $newmodel->priority != 0 && $newmodel->start_date != 0 && $newmodel->due_date != 0) {

                                $newmodel->save();
                                if (!$newmodel->save()) {
                                    $errarray[] = $row; //error row
                                }
                            } else {

                                if (empty($check)) {

                                    if ($newmodel->project_id == 0) {
                                        $newarr[] = ($data[1] != '') ? 'Row ' . $row . ' : " ' . $data[1] . ' " is not a valid Project' : 'Row ' . $row . ' : Project field is empty ';
                                    }
                                    if ($newmodel->assigned_to == 0) {
                                        $newarr[] = ($data[5] != '') ? 'Row ' . $row . ' : " ' . $data[5] . ' " is not a valid  User ' : 'Row ' . $row . ' :  Assigned to field is empty ';
                                    }
                                    if ($newmodel->coordinator == 0) {
                                        $newarr[] = ($data[6] != '') ? 'Row ' . $row . ' : " ' . $data[6] . ' " is not a valid User' : 'Row ' . $row . ' :   Coordinator field is empty ';
                                    }
                                    if ($newmodel->report_to == 0) {
                                        $newarr[] = ($data[7] != '') ? 'Row ' . $row . ' : " ' . $data[7] . ' " is not a valid User' : 'Row ' . $row . ' :  Task owner field is empty ';
                                    }
                                    if ($newmodel->priority == 0) {
                                        $newarr[] = ($data[2] != '') ? 'Row ' . $row . ' : " ' . $data[2] . ' " is not  a vaild Priority ' : 'Row ' . $row . ' : Priority field is empty';
                                    }
                                    if ($newmodel->start_date == 0) {
                                        $newarr[] = ($data[3] != '') ? 'Row ' . $row . ' : " ' . $data[3] . ' " is not a valid date format' : 'Row ' . $row . ' : Start date is empty';
                                    }
                                    if ($newmodel->due_date == 0) {
                                        $newarr[] = ($data[4] != '') ? 'Row ' . $row . ' : " ' . $data[4] . ' "  is not a valid date format' : 'Row ' . $row . ' :  Due date is empty';
                                    }
                                } else {

                                    $duparr[] = 'Duplicate entry in rows ' . $row;
                                }
                            }
                        }
                        $row++;
                    }
                    $transaction->commit();
                    $rowarr1 = '';
                    if (!empty($newarr)) {

                        if (!empty($duparr)) {

                            $rowarr1 = implode('<br/>', $duparr);
                        }

                        $arra = array();
                        foreach ($newarr as $key => $value) {
                            $dat = explode(" :", $value);

                            $arra[$dat[0]] = (isset($arra[$dat[0]]) ? $arra[$dat[0]] . ", " . $dat[1] : $value);
                        }

                        $rowarr = implode('<br/>', $arra);
                        $rowarr = $rowarr . '<br/>' . $rowarr1;
                        Yii::app()->user->setFlash('danger', $rowarr);
                    } else if (!empty($duparr)) {

                        $rowarr1 = implode('<br/>', $duparr);
                        Yii::app()->user->setFlash('danger', $rowarr1);
                    } else {
                        Yii::app()->user->setFlash('success', "All task's created");
                    }

                    $this->redirect(array('index2'));
                } catch (Exception $error) {
                    print_r($error);
                    $transaction->rollback();
                }
            }
            /* else{
              print_r($pmodel->getErrors());exit;

              } */
        }

        $type = '';
        $this->render(
            'index2',
            array(
                'model' => $model,
                'pmodel' => $pmodel,
                'type' => $type,
            )
        );
    }

    public function getusers($id)
    {

        $tblpx = Yii::app()->db->tablePrefix;

        $users = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}users WHERE   '" . $id . "' LIKE Concat(Concat('%',first_name),'%') AND '" . $id . "' LIKE  Concat(Concat('%',last_name),'%') ")->queryRow();
        if (!empty($users)) {
            return $users['userid'];
        } else {
            return 0;
        }
    }

    public function actionTest()
    {

        //   $id=163;

        //   $model = $this->loadModel($id);

        //   $this->taskUpdateEmail($model->tskid);

        //   //Yii::app()->user->setFlash('success', 'Succ');

        //   echo 123;




        // $sql="CREATE TABLE Persons (
        //     col1 int NOT NULL,
        //     col2 varchar(50) NOT NULL,
        //     col3 int,

        //     PRIMARY KEY (col1)
        // )";
        // Yii::app()->db->createCommand($sql)->execute();

        // $sql2="CREATE TABLE Orders (
        //     col1 int NOT NULL,
        //     col2 int NOT NULL,
        //     col3 int,

        //     PRIMARY KEY (col1),
        //     FOREIGN KEY (col3) REFERENCES Persons(col1)
        // )";
        // Yii::app()->db->createCommand($sql2)->execute();


        // working code
        // $columns_str="123";
        // $cols = str_split($columns_str);


        // $colQuery = ' CREATE TABLE Orders (id int(11) NOT NULL AUTO_INCREMENT,

        // col1 int NOT NULL,
        // col2 int NOT NULL,
        // col3 int,
        // ';
        // foreach($cols as $col)
        // {
        //     $colQuery .= "
        //         `$col` TEXT NOT NULL,";
        // }
        // $colQuery .= "
        // FOREIGN KEY (col3) REFERENCES Persons(col1),
        // PRIMARY KEY (`id`))";


        // Yii::app()->db->createCommand($colQuery)->execute();

        // end working code


        $template_name = 'Test';
        $column_count_calculation = 3;
        $column_count_result = 2;
        $total_col_count = $column_count_calculation + $column_count_result;

        $this->createTable($template_name, $total_col_count);
    }
    public function createTable($template_name, $total_col_count)
    {

        $sql = 'CREATE TABLE ' . $template_name . '(
            id int(11) NOT NULL AUTO_INCREMENT,
            boq_id int(11) NULL,
            measurement_id int(11) NULL,
            attribute_id int(11) NULL,
            mb_id int(11) NULL,
            attribute_name VARCHAR(200) NULL,
            deleted_by int(11) NULL,
           
        ';

        for ($i = 0; $i < $total_col_count; $i++) {
            $col = "field_" . $i;
            $sql .= "
        `$col` DOUBLE  NULL,";
        }
        $sql .= "
    created_at timestamp NULL,
    updated_at timestamp NULL,
    PRIMARY KEY (`id`)
    )";


        Yii::app()->db->createCommand($sql)->execute();
    }

    public function taskUpdateEmail($id)
    {
        $model = $this->loadModel($id);
        $subject = '[Task Updated] #' . $id . ': ' . $model->title;
        $name = $model->coordinator0->first_name;
        $toemail = $model->coordinator0->email;
        $app_root = YiiBase::getPathOfAlias('webroot');
        $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
        $logo = $theme_asset_url . 'default-logo.png';
        if (file_exists($app_root . "/themes/assets/logo.png")) {
            $logo = $theme_asset_url . 'logo.png';
        }

        $mail_body = '
        <div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">           
        <h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $name . '</span></h3>
        <table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
        <tr><td colspan="2" style="background-color: #ffff;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $logo . '"  width="50" height="50" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
        </tr>
        <tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>PMS -  Task updated</h2></div></td>
        </tr>
        <tr><td colspan="2">This is a notification to inform that new task #' . $id . ' has been updated: </td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $model->title . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project Name: </td><td style="border:1px solid #f5f5f5;">' . $model->project->name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($model->start_date)) . ' - ' . date('d-M-y', strtotime($model->due_date)) . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Priority:</td><td style="border:1px solid #f5f5f5;">' . $model->priority0->caption . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Assigned To:</td><td style="border:1px solid #f5f5f5;">' . $model->assignedTo->first_name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Coordinator:</td><td style="border:1px solid #f5f5f5;">' . $model->coordinator0->first_name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Created by: </td><td style="border:1px solid #f5f5f5;">' . $model->createdBy->first_name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Billable:</td> <td style="border:1px solid #f5f5f5;">' . $model->billable0->caption . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Quantity: </td><td style="border:1px solid #f5f5f5;">' . $model->quantity . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task status:</td> <td style="border:1px solid #f5f5f5;">' . $model->status0->caption . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Description:</td> <td style="border:1px solid #f5f5f5;">' . nl2br($model->description) . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;" colspan="2">' . CHtml::link('Click here to see this task', $this->createAbsoluteUrl('tasks/view', array('id' => $id))) . '</td></tr>
        </table>

        <p>Sincerely,  <br />
        ' . Yii::app()->name . '</p>
        </div>';
        $this->sendEmailToRecipients($toemail, $subject, $mail_body);
    }

    public function actionexport()
    {

        $model = json_decode($_POST['model'], TRUE);
        $arraylabel = array(
            'SNo',
            'Task Id',
            'Project',
            'Title',
            'Target Quantity',
            'Task Duration',
            'Progress',
            'Daily Target',
            'Required workers',
            'Start Date',
            'Due Date',
            'Current Status',
            'Task Type',
            'Task Owner',
            'Assigned To',
            'Coordinator',
            'Contractor'
        );
        $finaldata = array();

        foreach ($arraylabel as $head) {
            $finaldata[0][] = '';
        }

        $k = 1;
        $task_type_list = array(1 => 'External', 2 => 'Internal');
        foreach ($model as $data) {
            //print_r($data);
            $tasks = Tasks::model()->findByPk($data['tskid']);

            $project = $tasks->project->name;
            $report_to = Users::model()->findByPk($data['report_to']);
            $assigned_to = Users::model()->findByPk($data['assigned_to']);
            $coordinator = Users::model()->findByPk($data['coordinator']);
            $progress = $tasks->getcompleted_percent($data['tskid']);
            $status = Status::model()->findByPk($tasks->status);

            $finaldata[$k][] = $k;
            $finaldata[$k][] = $data['tskid'];
            $finaldata[$k][] = $project;
            $finaldata[$k][] = $tasks['title'];
            $finaldata[$k][] = $tasks['quantity'] . " (" . $tasks->unit0->unit_title . ")";
            $finaldata[$k][] = $tasks['task_duration'] . " days";
            $finaldata[$k][] = $progress . "%";
            $finaldata[$k][] = $tasks['daily_target'];
            $finaldata[$k][] = $tasks['required_workers'];
            $finaldata[$k][] = date('d-M-y', strtotime($tasks['start_date']));
            $finaldata[$k][] = date('d-M-y', strtotime($tasks['due_date']));
            $finaldata[$k][] = $status->caption;
            $finaldata[$k][] = $task_type_list[$tasks['task_type']];
            $finaldata[$k][] = $report_to->first_name . ' ' . $report_to->last_name;
            $finaldata[$k][] = $assigned_to->first_name . ' ' . $assigned_to->last_name;
            $finaldata[$k][] = $coordinator->first_name . ' ' . $coordinator->last_name;
            $finaldata[$k][] = $tasks->contractor->contractor_title;

            $k++;
        }
        //        
        //        print_r($finaldata);
        //        die;

        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Mytasks' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function getcompleted_percent($id)
    {

        $percentage = Tasks::model()->findByAttributes(array('tskid' => $id, 'trash' => 1));
        return isset($percentage->progress_percent) ? $percentage->progress_percent . '%' : '0%';
    }

    public function actionupdateacknowledge($id)
    {


        $model = $this->loadModel($id);
        $model->acknowledge_by = Yii::app()->user->id;
        $model->save();
        Yii::app()->user->setFlash('success', "Task acknowledged successfully");
        $this->redirect(array('tasks/view&id=' . $id));
    }

    public function actionchangestatus()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $status_id = $_POST["status_id"];
            $task_id = $_POST["task_ids"];
            $task_id_array = $_POST['task_ids'];
            $description = $_POST['description'];
            $task_id = implode(',', $task_id);
            $status = Status::model()->find(array('condition' => 'status_type="task_status" AND sid =' . $status_id . ''));
            try {
                $transaction = Yii::app()->db->beginTransaction();
                $query = Tasks::model()->updateAll(array('status' => $status_id), 'tskid IN (' . $task_id . ')');
                if ($query) {
                    foreach ($task_id_array as $task_id_data) {
                        $comments = new ProjectComments;
                        $comments->section_id = $task_id_data;
                        $comments->comment = 'Task status change to ' . $status->caption;
                        $comments->created_date = date('Y-m-d H:i:s');
                        $comments->user_id = yii::app()->user->id;
                        $comments->status_change_description = $description;
                        $comments->type = 2;
                        $comments->save();
                    }
                }
                $transaction->commit();
                $result = array('status' => '1', 'message' => 'Succesfully updated');
            } catch (Exception $error) {
                $transaction->rollback();
                print_r($error->getMessage());

                die;
                $result = array('status' => '2', 'message' => 'An error occured');
            }
            echo json_encode($result);
        }
    }

    public function actiongettaskdetails()
    {
        $task_id = $_POST['task_id'];
        $model = $this->loadModel($task_id);
        $taskResult = Yii::app()->db->createCommand("SELECT * FROM pms_tasks LEFT JOIN pms_users ON pms_tasks.assigned_to=pms_users.userid where pms_tasks.tskid=" . $task_id . "")->queryRow();
        $taskComment = Yii::app()->db->createCommand("SELECT * FROM pms_project_comments WHERE section_id=" . $task_id . " AND type=2 ORDER BY comment_id DESC")->queryAll();
        $result = '';
        $result .= '<div class="panel panel-primary">';
        $result .= '<div class="panel-heading">';
        $result .= '<div class="clearfix">';
        $result .= '<h3 class="pull-left panel-title">' . $taskResult['title'] . '</h3>';
        $result .= '<a class="closebtn pull-right closeform" onclick="closeform("task_details")">X</a>';
        $result .= '</div>';
        $result .= '</div>';
        $result .= '<div class="panel-body"><a target="_blank" class="viewtask" href=' . Yii::app()->createAbsoluteUrl('tasks/view', array('id' => $task_id)) . '>Details</a>';
        $result .= '<table class="viewtable">';
        $result .= '<tr>';
        $result .= '<td colspan="2">Description <br><b>' . $taskResult['description'] . '</b></td>';
        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td>Assigned To </td>';
        $result .= '<td class="text-right"><b>' . $model->assignedTo->first_name . " " . $model->assignedTo->last_name . '</td>';
        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td>Coordinator</td>';
        $result .= '<td class="text-right"><b>' . $model->coordinator0->first_name . " " . $model->coordinator0->last_name . '</td>';
        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td>Priority</td>';
        $result .= '<td class="text-right"><b>' . $model->priority0->caption . '</b> </td>';
        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td>Est</td>';
        $result .= '<td class="text-right"><b>' . ($model->total_hrs ? $model->total_hrs . " hrs" : "Not set") . '</b></td>';
        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td>Starts</td>';
        $result .= '<td class="text-right"><b>' . $model->status0->caption . '</b></td>';
        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td>Owner</td>';
        $result .= '<td class="text-right"><b>' . $model->reportTo->first_name . " " . $model->reportTo->last_name . '</b></td>';
        $result .= '</tr>';
        //                        $result .= '<tr>';
        //                            $result .= '<td>Approved</td>';
        //                            $result .= '<td><b>Admin</b></td>';
        //                        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td>Created</td>';
        $result .= '<td class="text-right"><b>' . $model->createdBy->first_name . " " . $model->createdBy->last_name . "On " . $model->created_date . '</b></td>';
        $result .= '</tr>';
        $result .= '<tr>';
        $result .= '<td colspan="2" class="comments_sec"><div class="clearfix apply_gap"><div class="pull-left"><h4 style="margin-top:0px;margin-bottom:0px;">Comments</h4></div><a class="show_hide pull-right">Show</a></div>';
        $result .= '<div id="wrapper"><ul class="list-unstyled commentslist" style="display:none;"><div id="scroller">';
        if (!empty($taskComment)) {
            foreach ($taskComment as $key => $value) {
                $user = Users::model()->findByPk($value['user_id']);
                $result .= '<li><b>' . $value['comment'] . '</b><div class="clearfix comments_footer"><div class="pull-left">' . $user->first_name . '</div><div class="pull-right">' . date('d-M-Y H:i:s', strtotime($value['created_date'])) . '</div></div></li>';
            }
        } else {
            $result .= '<li> No comments  found. </li>';
        }
        $result .= '</div></div></ul>';
        $result .= '</td>';
        $result .= ' </tr>';
        $result .= '</table>';
        $result .= '</div>';
        $result .= ' </div>';

        echo json_encode(array('html' => $result));
    }

    public function actionGetMainTasks()
    {
        if (!empty($_REQUEST['project_id'])) {
            $tblpx = Yii::app()->db->tablePrefix;
            if (isset($_REQUEST['task_id']) && $_REQUEST['task_id']) {
                $tid = $_REQUEST['task_id'];
                $pid = $_REQUEST['project_id'];
                $tasks = Tasks::model()->findByPk($tid);
                $start_date = date('d-M-y', strtotime($tasks->start_date));
                $end_date = date('d-M-y', strtotime($tasks->due_date));
            } else if (isset($_REQUEST['project_id'])) {
                $pid = $_REQUEST['project_id'];
                $projects = Projects::model()->findByPk($pid);
                $start_date = date('d-M-y', strtotime($projects->start_date));
                $end_date = date('d-M-y', strtotime($projects->end_date));
            } else {
                $start_date = "";
                $end_date = "";
            }
            if (!empty($_REQUEST['task_type'])) {
                $task_type = $_REQUEST['task_type'];
            } else {
                $task_type = 1;
            }
            $condition1 = '';
            if (Yii::app()->user->role != 1) {
                $condition1 = 'WHERE project_id = ' . $pid . ' AND task_type = ' . $task_type . ' AND (assigned_to =' . Yii::app()->user->id . ' or coordinator=' . Yii::app()->user->id . ' or created_by=' . Yii::app()->user->id . ')';
            } else {
                $condition1 = 'WHERE project_id = ' . $pid . ' AND task_type = ' . $task_type;
            }
            $data = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks " . $condition1)->queryAll();
            $data = CHtml::listData($data, 'tskid', 'title');
            $html = "<option value=''>Select Main Task</option>";
            foreach ($data as $value => $site_name) {
                $html .= "<option value=" . $value . ">" . $site_name . "</option>";
                //echo CHtml::tag('option', array('value'=>$value),CHtml::encode($site_name),true);
            }
            $milestone_condition = "status = 1 AND project_id = " . $pid;
            $milestone_data = Milestone::model()->findAll(
                array(
                    'condition' => $milestone_condition,
                    'order' => 'milestone_title ASC',
                    'distinct' => true
                )
            );
            $milestone_result = CHtml::listData($milestone_data, 'id', 'milestone_title');
            $milestone_html = "<option value=''>Choose a Milestone</option>";

            foreach ($milestone_result as $milestone_key => $milestone_value) {
                $milestone_html .= "<option value=" . $milestone_key . ">" . $milestone_value . "</option>";
            }
            $area_html = $this->getAreas($pid);
            $work_types_html = $this->getProjectWorkTypes($pid);
            echo json_encode(array('option' => $html, 'milestones' => $milestone_html, 'start_date' => $start_date, 'end_date' => $end_date, 'areas' => $area_html, 'work_types_html' => $work_types_html));
        } else {
            $html = "<option value=''>Select Main Task</option>";
            $start_date = "";
            $end_date = "";
            echo json_encode(array('option' => $html, 'start_date' => $start_date, 'end_date' => $end_date));
        }
    }

    public function getAreas($project_id)
    {
        $area_data = Area::model()->findAll(
            array(
                'condition' => 'status = 1 AND project_id = ' . $project_id,
                'order' => 'area_title ASC',
                'distinct' => true
            )
        );
        $area_result = CHtml::listData($area_data, 'id', 'area_title');
        $area_html = "<option value=''>Choose Area</option>";
        foreach ($area_result as $area_key => $area_value) {
            $area_html .= "<option value=" . $area_key . ">" . $area_value . "</option>";
        }
        return $area_html;
    }

    public function getProjectWorkTypes($project_id)
    {
        $work_type_data = $work_types = WorkType::model()->findAll(
            array(
                'select' => array('wtid,work_type'),
                'order' => 'work_type ASC',
                'distinct' => true
            )
        );
        $work_type_result = CHtml::listData($work_type_data, 'wtid', 'work_type');
        $work_type_html = "<option value=''>Choose Work Type</option>";
        $linked_work_types = ProjectWorkType::model()->getWorktypes($project_id);
        foreach ($work_type_result as $work_type_key => $work_type_value) {
            if (!in_array($work_type_key, $linked_work_types)) {
                $work_type_html .= "<option class='bg_color_grey' title='Work Type not defined in project' value=" . $work_type_key . ">" . $work_type_value . "</option>";
            } else {
                $work_type_html .= "<option value=" . $work_type_key . ">" . $work_type_value . "</option>";
            }
        }
        return $work_type_html;
    }

    public function actionChecktaskquantity()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['task_id']) && isset($_REQUEST['main_task_id'])) {
            $main_task_id = $_REQUEST['main_task_id'];
            if (!empty($main_task_id)) {
                $task_id = $_REQUEST['task_id'];
                if ($task_id != 0) {
                    $sub_task_total_qunatity = Yii::app()->db->createCommand("select SUM(quantity) FROM {$tblpx}tasks WHERE parent_tskid = " . $main_task_id . ' AND tskid != ' . $task_id)->queryScalar();
                    $main_task_qunatity = Yii::app()->db->createCommand("select quantity FROM {$tblpx}tasks WHERE tskid = " . $main_task_id)->queryScalar();
                    $result_array = array('parent_quantity' => $main_task_qunatity, 'sub_task_total_qty' => $sub_task_total_qunatity);
                } else {
                    $sub_task_total_qunatity = Yii::app()->db->createCommand("select SUM(quantity) FROM {$tblpx}tasks WHERE parent_tskid = " . $main_task_id)->queryScalar();
                    $main_task_qunatity = Yii::app()->db->createCommand("select quantity FROM {$tblpx}tasks WHERE tskid = " . $main_task_id)->queryScalar();
                    $result_array = array('parent_quantity' => $main_task_qunatity, 'sub_task_total_qty' => $sub_task_total_qunatity);
                }
            } else {
                $task_id = $_REQUEST['task_id'];
                $sub_task_total_qunatity = Yii::app()->db->createCommand("select SUM(quantity) FROM {$tblpx}tasks WHERE parent_tskid = " . $task_id)->queryScalar();
                $result_array = array('sub_task_total_qty' => $sub_task_total_qunatity, 'parent_quantity' => '');
            }
        }
        $result_array = json_encode($result_array);
        print_r($result_array);
    }

    public function actionchangedate()
    {
        $final_response = array();
        $task_ids = $_REQUEST['id'];
        $change_type = $_POST['change_type']; /* 1=> prepone, 2=> postpone */
        $days_count = $_POST['changed_value'];
        $date_type = $_POST['date_type']; /* 1=> start_date, 2=> due_date */
        if (!empty($task_ids)) {
            foreach ($task_ids as $key => $tskid) {
                $model = $this->loadModel($tskid);
                $start_due_date_array = array('start_date' => $model->start_date, 'due_date' => $model->due_date);
                $start_date = $model->start_date;
                $due_date = $model->due_date;
                if ($change_type == 1 && $date_type == 1) {
                    $start_date = date('Y-m-d', strtotime('-' . $days_count . ' day', strtotime($model->start_date)));
                } elseif ($change_type == 1 && $date_type == 2) {
                    $due_date = date('Y-m-d', strtotime('-' . $days_count . ' day', strtotime($model->due_date)));
                } elseif ($change_type == 2 && $date_type == 1) {
                    $start_date = date('Y-m-d', strtotime('+' . $days_count . ' day', strtotime($model->start_date)));
                } elseif ($change_type == 2 && $date_type == 2) {
                    $due_date = date('Y-m-d', strtotime('+' . $days_count . ' day', strtotime($model->due_date)));
                }

                /* remove holidays  */
                if (!empty($model->clientsite_id)) {
                    $site_id = $model->clientsite_id;
                    $status = 1;
                } else {
                    $site_id = 1;
                    $status = 0;
                }
                $Criteria = new CDbCriteria();
                $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                $Criteria->addCondition('site_id = ' . $site_id);
                $Criteria->addCondition('status = ' . $status);
                $events = CalenderEvents::model()->findAll($Criteria);
                $event_array = array();
                foreach ($events as $event) {
                    array_push($event_array, $event->event_date);
                }
                $duration = 0;
                $start_date_data = new DateTime($start_date);
                $due_date_data = new DateTime($due_date);
                $due_date_data->modify('+1 day');
                $period = new DatePeriod(
                    $start_date_data,
                    new DateInterval('P1D'),
                    $due_date_data
                );

                foreach ($period as $key => $value) {
                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                        $duration++;
                    }
                }

                $target = round($model->quantity / $duration, 2);
                // var_dump($model->quantity." ---- ".$duration);
                $model->updated_by = yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
                $model->start_date = date('Y-m-d', strtotime($start_date));
                $model->due_date = date('Y-m-d', strtotime($due_date));
                $model->daily_target = $target;
                $model->task_duration = $duration;


                if (($change_type == 2 || $change_type == 1)) {
                    $dependant_or_not_checking = $this->ismodeldependant($model, $change_type, $date_type);

                    $validation = $this->BetweenDateValidationChecking($model, $date_type, $duration, $event_array, $change_type);

                    $dependant_tasks_validation = $this->dependanttaskvalidation($model, $event_array, $days_count, $change_type, $date_type, $start_due_date_array);
                    $validation_data = $this->ChangeDateValidationChecking($model, $duration, $event_array);


                    if (!empty($dependant_or_not_checking)) {

                        $result = array('task_id' => $tskid, 'msg' => $dependant_or_not_checking['msg']);
                    } elseif ($validation['status'] == 1) {

                        $result = array('change_type' => 1, 'task_id' => $tskid, 'msg' => $validation['msg']);
                    } elseif ($dependant_tasks_validation['status'] == 1) { /* Error in dependant task section */
                        $result = array('change_type' => 1, 'task_id' => $tskid, 'msg' => $dependant_tasks_validation['msg']);
                    } else {

                        if ($validation_data['status'] == 1) {
                            if (!empty($dependant_tasks_validation['data'])) {
                                $this->dependantdatasave($dependant_tasks_validation['data']);
                            }

                            if ($model->save()) {
                                $this->milestonedatechange($model);
                                $result = array(
                                    'change_type' => 1,
                                    'task_id' => $tskid,
                                    'start_date' => date('d-M-y', strtotime($model->start_date)),
                                    'due_date' => date('d-M-y', strtotime($model->due_date)),
                                    'duration' => $model->task_duration
                                );
                            } else {
                                //                                echo '<pre>';
                                //                                print_r($model->attributes);
                                $error_data = $model->getErrors();
                                // var_dump($error_data);
                                //die;

                                foreach ($error_data as $key => $value) {
                                    //                                    if ($key in 'due_date' || $key == 'start_date' || ) {
                                    $result = array('change_type' => 1, 'task_id' => $tskid, 'msg' => $value[0]);
                                    //                                    }
                                }
                            }
                        } else {
                            $result = array('change_type' => 1, 'task_id' => $tskid, 'msg' => $validation_data['msg']);
                        }
                    }
                }
                // else{                    
                //         $model->save();  
                //         $result = array('change_type'=>2,'task_id'=>$tskid);   
                //    } 



                $final_response[$tskid] = $result;

                if (!empty($dependant_tasks_validation['data'])) {
                    foreach ($dependant_tasks_validation['data'] as $dependant_data) {
                        $dependant_result = $result = array('task_id' => $dependant_data['task_id'], 'start_date' => date('d-M-y', strtotime($dependant_data['start_date'])), 'due_date' => date('d-M-y', strtotime($dependant_data['due_date'])), 'duration' => $dependant_data['task_duration']);
                        if (!empty($dependant_result))
                            $final_response[$dependant_data['task_id']] = $dependant_result;
                    }
                }
            }
            $final_response['change_type'] = $change_type;
            echo json_encode($final_response, true);
        }
    }

    public function ismodeldependant($model, $change_type, $date_type)
    {

        $result_data = array();
        $model_dependant = TaskDependancy::model()->find(['condition' => 'task_id = ' . $model->tskid]);

        if (!empty($model_dependant)) {

            if ($model_dependant->dependant_on == 1 && $change_type == 1 && $date_type == 1) {
                $result_data = array('task_id' => $model->tskid, 'msg' => 'Start date  of dependant task #' . $model->tskid . ' cannot be preponed');
            } elseif ($model_dependant->dependant_on == 2 && $change_type == 2 && $date_type == 2) {
                $result_data = array('task_id' => $model->tskid, 'msg' => 'Due date of dependant task #' . $model->tskid . ' cannot be postponed');
            }
        }

        return $result_data;
    }

    public function dependanttaskvalidation($model_main, $event_array, $days_count, $change_type, $date_type, $start_due_date_array)
    {

        //    echo '<pre>';print_r($start_due_date_array);exit;
        // if($model_main->parent_tskid == ""){
        //     $model_main->save();
        // }
        $dependant_task_array = array();
        $dependant_tasks = TaskDependancy::model()->findAll(['condition' => 'dependant_task_id = ' . $model_main->tskid]);
        if (!empty($dependant_tasks)) {
            foreach ($dependant_tasks as $dependant_task) {

                $data_array = array();
                $model = $this->loadModel($dependant_task->task_id);
                $start_date = $model->start_date;
                $due_date = $model->due_date;
                if ($change_type == 1 && $date_type == 1 && $dependant_task->dependant_on == 1) {
                    $start_date = date('Y-m-d', strtotime('-' . $days_count . ' day', strtotime($model->start_date)));
                } elseif ($change_type == 1 && $date_type == 2 && $dependant_task->dependant_on == 2) {
                    $due_date = date('Y-m-d', strtotime('-' . $days_count . ' day', strtotime($model->due_date)));
                } elseif ($change_type == 2 && $date_type == 1 && $dependant_task->dependant_on == 1) {
                    $start_date = date('Y-m-d', strtotime('+' . $days_count . ' day', strtotime($model->start_date)));
                } elseif ($change_type == 2 && $date_type == 2 && $dependant_task->dependant_on == 2) {
                    $due_date = date('Y-m-d', strtotime('+' . $days_count . ' day', strtotime($model->due_date)));
                }
                /* remove holidays  */
                if (!empty($model->clientsite_id)) {
                    $site_id = $model->clientsite_id;
                    $status = 1;
                } else {
                    $site_id = 1;
                    $status = 0;
                }
                $Criteria = new CDbCriteria();
                $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                $Criteria->addCondition('site_id = ' . $site_id);
                $Criteria->addCondition('status = ' . $status);
                $events = CalenderEvents::model()->findAll($Criteria);
                $event_array = array();
                foreach ($events as $event) {
                    array_push($event_array, $event->event_date);
                }
                $duration = 0;
                $start_date_data = new DateTime($start_date);
                $due_date_data = new DateTime($due_date);
                // $due_date_data->modify('+1 day');            
                $period = new DatePeriod(
                    $start_date_data,
                    new DateInterval('P1D'),
                    $due_date_data
                );

                foreach ($period as $key => $value) {
                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                        $duration++;
                    }
                }
                $target = round($model->quantity / $duration);
                $model->updated_by = yii::app()->user->id;
                $model->updated_date = date('Y-m-d');
                $model->start_date = date('Y-m-d', strtotime($start_date));
                $model->due_date = date('Y-m-d', strtotime($due_date));
                $model->daily_target = $target;
                $model->task_duration = $duration;

                $validation = $this->BetweenDateValidationChecking($model, $date_type, $duration, $event_array, $change_type);
                if ($validation['status'] == 1) {
                    $dependancy_result = array('status' => 1, 'msg' => $validation['msg'] . ' in dependant tasks ' . $model->tskid);
                    // $model_main->start_date = $start_due_date_array['start_date'];
                    // $model_main->due_date = $start_due_date_array['due_date'];
                    // $model_main->save();
                    return $dependancy_result;
                    break;
                } else {
                    $validation_data = $this->ChangeDateValidationChecking($model, $duration, $event_array);
                    if ($validation_data['status'] == 0) {
                        $dependancy_result = array('status' => 1, 'msg' => $validation_data['msg'] . ' in dependant tasks ' . $model->tskid, 'dependant_task_id' => $model->tskid);
                        // $model_main->start_date = $start_due_date_array['start_date'];
                        // $model_main->due_date = $start_due_date_array['due_date'];
                        // $model_main->save();
                        return $dependancy_result;
                        break;
                    } else {
                        $data_array = array('task_id' => $model->tskid, 'updated_by' => yii::app()->user->id, 'updated_date' => date('Y-m-d'), 'start_date' => $model->start_date, 'due_date' => $model->due_date, 'daily_target' => $model->daily_target, 'task_duration' => $model->task_duration);
                    }
                }
                if (!empty($data_array)) {
                    array_push($dependant_task_array, $data_array);
                }
            }
            $dependancy_result = array('status' => 0, 'data' => $dependant_task_array);
            // $model_main->start_date = $start_due_date_array['start_date'];
            // $model_main->due_date = $start_due_date_array['due_date'];
            // $model_main->save();

            return $dependancy_result;
            // if(!empty($dependant_task_array)){
            //     $this->dependantdatasave($dependant_task_array);
            // }             
        }
    }

    public function dependantdatasave($dependant_task_array)
    {
        foreach ($dependant_task_array as $data) {
            $model = $this->loadModel($data['task_id']);
            if (!empty($model)) {
                $model->start_date = $data['start_date'];
                $model->due_date = $data['due_date'];
                $model->daily_target = $data['daily_target'];
                $model->task_duration = $data['task_duration'];
                $model->updated_by = $data['updated_by'];
                $model->updated_date = $data['updated_date'];
                $model->save();
            }
            // echo '<pre>';print_r($data);exit;
        }
    }

    public function ChangeDateValidationChecking($model, $duration, $event_array)
    {
        $result_data = array('status' => 0, 'msg' => '');
        $current_qty = Yii::app()->db->createCommand()
            ->select('sum(qty) as qty')
            ->from('pms_daily_work_progress')
            ->where('taskid = ' . $model->tskid)
            ->queryRow();
        $current_completed_qty = $current_qty['qty'];
        $remaining_qty = ($model->quantity - $current_completed_qty);

        if ($model->parent_tskid != null) {
            if (!isset($model->worktype->daily_throughput)) {
                return $result_data = array('status' => 0, 'msg' => 'Work type is not specified in Task: ' . $model->title);
            }

            $daily_throughput = isset($model->worktype->daily_throughput) ? $model->worktype->daily_throughput : 0;
            $maximum_allowed_workers = $model->allowed_workers;
            $today = strtotime(date('Y-m-d'));
            $start_date = strtotime($model->start_date);
            if ($today > $start_date) {

                $duration = 0;
                $today_data = new DateTime(date('Y-m-d'));
                $start_date_data = new DateTime($model->start_date);
                $due_date_data = new DateTime($model->due_date);
                $due_date_data->modify('+1 day');
                $period = new DatePeriod(
                    $start_date_data,
                    new DateInterval('P1D'),
                    $due_date_data
                );

                foreach ($period as $key => $value) {
                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                        $duration++;
                    }
                }

                if (!empty($duration)) {
                    $daily_target = $remaining_qty / $duration;
                    $number_of_workers_needed = $daily_target / $daily_throughput;
                    if ($number_of_workers_needed < 1) {
                        $number_of_workers_needed = 1;
                    } else {
                        $number_of_workers_needed = round($number_of_workers_needed);
                    }
                    if ($number_of_workers_needed <= $maximum_allowed_workers) {
                        $result_data = array('status' => 1);
                    } else {
                        $extra_workers_count = $number_of_workers_needed - $maximum_allowed_workers;
                        $result_data = array('status' => 0, 'msg' => 'Required number of workers exceded maximum allowed workers by' . $extra_workers_count);
                    }
                }
            } else {

                if (!empty($model->task_duration)) {
                    $daily_target = $remaining_qty / $model->task_duration;
                    $number_of_workers_needed = $daily_target / $daily_throughput;
                    if ($number_of_workers_needed < 1) {
                        $number_of_workers_needed = 1;
                    } else {
                        $number_of_workers_needed = round($number_of_workers_needed);
                    }
                    if ($number_of_workers_needed <= $maximum_allowed_workers) {
                        // save to db
                        $result_data = array('status' => 1, 'msg' => 'No of workers changed to' . $number_of_workers_needed);
                    } else {
                        $extra_workers_count = $number_of_workers_needed - $maximum_allowed_workers;
                        $result_data = array('status' => 0, 'msg' => 'Required number of workers exceded maximum allowed workers by ' . $extra_workers_count);
                    }
                }
            }
        } else {
            $result_data['status'] = 1;
        }
        return $result_data;
    }

    public function BetweenDateValidationChecking($model, $date_type, $change_type)
    {

        $flag = 0;
        $flag2 = 0;
        $msg = "";
        $flag_status = 0;
        $start_date = $model->start_date;
        $end_date = $model->due_date;
        $projects = Projects::model()->findByPk($model->project_id);

        if ($model->parent_tskid == "") {
            $Criteria = new CDbCriteria();
            $Criteria->condition = "start_date < $model->start_date OR due_date > $model->due_date AND parent_tskid = $model->tskid";
            $sub_tasks = Tasks::model()->findAll($Criteria);


            if ($start_date < $end_date) {

                if ($projects->start_date != $start_date || $projects->end_date != $end_date) {
                    if ($projects->start_date > $start_date) {
                        $flag = 1;
                        $msg = "Date not specified within " . $projects->name . " start date";
                        $flag_status = 1;
                    }
                    if ($projects->end_date < $end_date) {
                        $flag2 = 1;
                        $msg = "Date not specified within " . $projects->name . " end date";
                        $flag_status = 1;
                    }
                    if ($flag == 1 && $flag2 == 1) {
                        $msg = "Date not specified within " . $projects->name . " dates";
                        $flag_status = 1;
                    }
                }
            } elseif (!empty($sub_tasks)) {
                $flag = 1;
                if ($date_type == 1) {
                    $msg = "Start date cannot be postponed, it has sub tasks.";
                } elseif ($date_type == 1) {
                    $msg = "Due date cannot be preponed, it has sub tasks.";
                }

                $flag_status = 1;
            } else {

                $msg = "Duration become zero";
                $flag_status = 1;
            }
        } else {

            // echo $start_date;
            // echo $end_date;exit;
            $tasks = Tasks::model()->findByPk($model->parent_tskid);
            if ($start_date < $end_date) {

                if ($tasks->start_date != $start_date || $tasks->due_date != $end_date) {


                    // echo $end_date;
                    if ($change_type == 1) {
                        if ($tasks->start_date > $start_date) {
                            $flag = 1;
                            $msg = "Start date of #" . $model->tskid . " preceeded the start date of task #" . $tasks->tskid;
                            $flag_status = 1;
                        }
                    }
                    if ($change_type == 2) {
                        if ($tasks->due_date < $end_date) {
                            $flag2 = 1;
                            $msg = "Due date of #" . $model->tskid . " exceeded the due date of task #" . $tasks->tskid;
                            $flag_status = 1;
                        }
                    }

                    if ($flag == 1 && $flag2 == 1) {
                        $msg = "Date not specified within #" . $tasks->tskid . " dates";
                        $flag_status = 1;
                    }
                }
            } else {
                $msg = "Duration become zero";
                $flag_status = 1;
            }
        }
        if ($model->milestone_id != "") {
            $milestone_model = Milestone::model()->findByPk($model->milestone_id);

            if (!empty($milestone_model)) {
                if (strtotime($model->start_date) < strtotime($milestone_model['start_date'])) {
                    $msg = "Date not specified within milestone start date";
                    $flag_status = 1;
                }
                if (strtotime($model->due_date) > strtotime($milestone_model['end_date'])) {
                    $msg = $model->title . "'s Due date [" . $model->due_date . "] is not specified within milestone end date: " . $milestone_model['end_date'];
                    $flag_status = 1;
                }
            }
        }
        return $result_data = array('status' => $flag_status, 'msg' => $msg);
    }

    public function actionDurationcalculationnew()
    {
        $duration = 0;
        if (isset($_REQUEST['start_date']) && isset($_REQUEST['due_date'])) {
            $start_date = date('Y-m-d', strtotime($_REQUEST['start_date']));
            $due_date = date('Y-m-d', strtotime($_REQUEST['due_date']));

            if (empty($_REQUEST['site_id']) || $_REQUEST['site_id'] == 1) {
                $Criteria = new CDbCriteria();
                $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                $Criteria->addCondition('site_id = 1');
                $Criteria->addCondition('status = 0');
                $events = CalenderEvents::model()->findAll($Criteria);
                $event_array = array();
                foreach ($events as $event) {
                    array_push($event_array, $event->event_date);
                }
            } else {


                $site_id = $_REQUEST['site_id'];

                $Criteria = new CDbCriteria();
                $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                $Criteria->addCondition('site_id = ' . $site_id);
                $Criteria->addCondition('status = 1');
                $events = CalenderEvents::model()->findAll($Criteria);
                $event_array = array();
                foreach ($events as $event) {
                    array_push($event_array, $event->event_date);
                }
            }
            $start_date_data = new DateTime($start_date);
            $due_date_data = new DateTime($due_date);
            $due_date_data->modify('+1 day');
            $period = new DatePeriod(
                $start_date_data,
                new DateInterval('P1D'),
                $due_date_data
            );

            foreach ($period as $key => $value) {
                if (!in_array($value->format('Y-m-d'), $event_array)) {
                    $duration++;
                }
            }
            echo $duration;
        } else {
        }
    }

    public function actionDurationcalculation()
    {

        if (isset($_REQUEST['start_date']) && isset($_REQUEST['due_date'])) {
            $start_date = date('Y-m-d', strtotime($_REQUEST['start_date']));
            $due_date = date('Y-m-d', strtotime($_REQUEST['due_date']));
            $project_id = $_REQUEST['project_id'];

            $project = Projects::model()->findByPK($project_id);
            $site_calender = $project->site_calendar_id;

            if ($site_calender != "") {

                $get_wworking_days = $this->getWorkingDays($start_date, $due_date, $site_calender);

                echo $get_wworking_days;
            } else {

                $duration = 0;
                if (isset($_REQUEST['start_date']) && isset($_REQUEST['due_date'])) {
                    $start_date = date('Y-m-d', strtotime($_REQUEST['start_date']));
                    $due_date = date('Y-m-d', strtotime($_REQUEST['due_date']));

                    if (empty($_REQUEST['site_id']) || $_REQUEST['site_id'] == 1) {
                        $Criteria = new CDbCriteria();
                        $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                        $Criteria->addCondition('site_id = 1');
                        $Criteria->addCondition('status = 0');
                        $events = CalenderEvents::model()->findAll($Criteria);
                        $event_array = array();
                        foreach ($events as $event) {
                            array_push($event_array, $event->event_date);
                        }
                    } else {


                        $site_id = $_REQUEST['site_id'];

                        $Criteria = new CDbCriteria();
                        $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                        $Criteria->addCondition('site_id = ' . $site_id);
                        $Criteria->addCondition('status = 1');
                        $events = CalenderEvents::model()->findAll($Criteria);
                        $event_array = array();
                        foreach ($events as $event) {
                            array_push($event_array, $event->event_date);
                        }
                    }
                    $start_date_data = new DateTime($start_date);
                    $due_date_data = new DateTime($due_date);
                    $due_date_data->modify('+1 day');
                    $period = new DatePeriod(
                        $start_date_data,
                        new DateInterval('P1D'),
                        $due_date_data
                    );

                    foreach ($period as $key => $value) {
                        if (!in_array($value->format('Y-m-d'), $event_array)) {
                            $duration++;
                        }
                    }
                    echo $duration;
                } else {
                }
            } //site calendar not assigned to project
        }
    }

    public function getWorkingDays($start_date, $due_date, $site_calender)
    {



        $begin = new DateTime($start_date);
        $end = new DateTime($due_date);
        $start_date = $start_date;
        $end_date = $due_date;

        $calender = ProjectCalender::model()->findByPK($site_calender);
        $duration = 0;
        $sat_count = 0;
        $date_array = [];
        $saturday_array = [];

        for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
            $day = $i->format("l");
            $date = $i->format("Y-m-d");

            if ($day == "Sunday" && $calender->sunday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Monday" && $calender->monday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Tuesday" && $calender->tuesday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Wednesday" && $calender->wednesday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Thursday" && $calender->thursday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Friday" && $calender->friday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Saturday" && $calender->saturday == 1) {



                $duration++;

                array_push($date_array, $date);
            }

            $first_sat_s_date = "01";
            $first_sat_e_date = "7";
            $second_sat_s_date = "08";
            $second_sat_e_date = "14";
            $third_sat_s_date = "15";
            $third_sat_e_date = "21";
            $fourth_sat_s_date = "22";
            $fourth_sat_e_date = "28";

            if ($day == "Saturday" && $calender->second_saturday == 0 && strtotime($date) >= strtotime($i->format("Y-m-" . $second_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $second_sat_e_date))) {
                $sat_count++;

                array_push($saturday_array, $date);
            }
            if ($day == "Saturday" && $calender->fourth_saturday == 0 && strtotime($date) >= strtotime($i->format("Y-m-" . $fourth_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $fourth_sat_e_date))) {
                $sat_count++;
                array_push($saturday_array, $date);
            }


            /////////////////////////////////////////////////

            if ($day == "Saturday" && $calender->second_saturday == 1 && strtotime($date) >= strtotime($i->format("Y-m-" . $second_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $second_sat_e_date))) {
                array_push($date_array, $date);
            }

            if ($day == "Saturday" && $calender->fourth_saturday == 1 && strtotime($date) >= strtotime($i->format("Y-m-" . $fourth_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $fourth_sat_e_date))) {
                array_push($date_array, $date);
            }



            ///////////////////////////////////////////////////





        }
        //echo $duration-$sat_count;



        $available_date_arr = array_diff($date_array, $saturday_array);
        $criteria = new CDbCriteria;
        $criteria->condition = "date >= '$start_date' AND date <= '$end_date' AND project_calendar_id=" . $site_calender;
        $holidays = CalendarHolidays::model()->findAll($criteria);
        $holiday_array = [];
        foreach ($holidays as $holiday) {
            $holiday_array[] = $holiday->date;
        }



        $total_working_days = array_diff($available_date_arr, $holiday_array);


        return count($total_working_days);
    }


    public function actioncreateproject()
    {
        $Projects_name = $_POST['Projects_name'];
        $model = new Projects;
        $tblpx = Yii::app()->db->tablePrefix;
        $maxData = Yii::app()->db->createCommand("SELECT MAX(CAST(pid as signed)) as largenumber FROM {$tblpx}projects")->queryRow();
        $maxNo = $maxData["largenumber"];
        $newPONo = $maxNo + 1;
        $prNo = $newPONo;
        $digit = strlen((string) $prNo);
        if ($digit == 1) {
            $project_id = '000' . $prNo;
        } else if ($digit == 2) {
            $project_id = '00' . $prNo;
        } else {
            $project_id = '0' . $prNo;
        }


        $tableSchema = "SHOW TABLES  LIKE '%pms_project_prefix%'";

        $databasetbl = Yii::app()->db->createCommand($tableSchema)->queryRow();

        if ($databasetbl == null) {
            $project_no = " HPMC-PR" . $project_id . '-' . date('Y') . '-' . date('m');
        } else {

            $query = "SELECT * FROM pms_project_prefix ";
            $data = Yii::app()->db->createCommand($query)->queryRow();

            $prefix = $data['prefix'];
            $project_no = $prefix . "-" . $project_id . '-' . date('Y') . '-' . date('m');
        }
        $data = implode(',', $_POST['managers']);

        $model->assigned_to = $data;
        $model->created_by = yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->updated_by = yii::app()->user->id;
        $model->updated_date = date('Y-m-d');
        $model->project_no = $project_no;
        $model->name = $Projects_name;
        $model->start_date = date('Y-m-d', strtotime($_POST['project_start_date']));
        $model->end_date = date('Y-m-d', strtotime($_POST['project_end_date']));
        $model->status = 1;
        $model->billable = 3;
        if ($model->save()) {
            $last_id = $model->pid;
            $html = "<option value=''>Select project</option>";
            $clients = Projects::model()->findAll(array('condition' => 'status = 1'));
            foreach ($clients as $key => $value) {
                $html .= "<option value=" . $value['pid'] . ">" . $value['name'] . "</option>";
            }
            echo json_encode(array('status' => 1, 'option' => $html, 'lastid' => $last_id));
        } else {

            echo json_encode(array('status' => 2));
        }
    }

    public function actioncreateunit()
    {
        Yii::app()->getModule('masters');
        $title = $_GET['title'];
        $code = $_GET['code'];
        $model = new Unit;
        $model->created_by = Yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->unit_title = $title;
        $model->unit_code = $code;
        $model->status = 1;
        if ($model->save()) {
            $last_id = $model->id;
            $html = "<option value=''>Select project</option>";
            $clients = Unit::model()->findAll(array('condition' => 'status = 1'));
            foreach ($clients as $key => $value) {
                $html .= "<option value=" . $value['id'] . ">" . $value['unit_title'] . "</option>";
            }
            echo json_encode(array('status' => 1, 'option' => $html, 'lastid' => $last_id));
        } else {
            echo json_encode(array('status' => 2));
        }
    }

    public function actioncreatecontractor()
    {
        Yii::app()->getModule('masters');
        $title = $_GET['title'];
        $model = new Contractors;
        $model->created_by = Yii::app()->user->id;
        $model->updated_by = Yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->updated_date = date('Y-m-d H:i:s');
        $model->contractor_title = $title;
        $model->status = 1;
        if ($model->save()) {
            $last_id = $model->id;
            $html = "<option value=''>Select Contractor</option>";
            $contractors = Contractors::model()->findAll(array('condition' => 'status = 1'));
            foreach ($contractors as $key => $value) {
                $html .= "<option value=" . $value['id'] . ">" . $value['contractor_title'] . "</option>";
            }
            echo json_encode(array('status' => 1, 'option' => $html, 'lastid' => $last_id));
        } else {
            echo json_encode(array('status' => 2));
        }
    }

    public function actioncreateworktype()
    {

        $title = $_GET['title'];
        $through_put = $_GET['through_put'];
        $unit = $_GET['unit'];
        $model = new WorkType;
        $model->work_type = $title;
        $model->daily_throughput = $through_put;
        $model->unit_id = $unit;
        if ($model->save()) {
            $last_id = $model->wtid;
            $html = "<option value=''>Select Work Type</option>";
            $work_types = WorkType::model()->findAll();
            foreach ($work_types as $key => $value) {
                $html .= "<option value=" . $value['wtid'] . ">" . $value['work_type'] . "</option>";
            }
            echo json_encode(array('status' => 1, 'option' => $html, 'lastid' => $last_id));
        } else {
            echo json_encode(array('status' => 2));
        }
    }

    public function actioncreatemilestone()
    {
        Yii::app()->getModule('masters');
        $title = $_GET['title'];
        $project_id = $_GET['project_id'];
        $ranking = $_GET['ranking'];
        $model = new Milestone;
        $model->setScenario(('create'));
        $model->created_by = Yii::app()->user->id;
        $model->updated_by = Yii::app()->user->id;
        $model->created_date = date('Y-m-d');
        $model->milestone_title = $title;
        $model->project_id = $project_id;
        $model->ranking = $ranking;
        if (!empty($model->project_id)) {
            $project_model = Projects::model()->findByPk($model->project_id);
            if (!empty($project_model)) {
                $model->start_date = $project_model['start_date'];
                $model->end_date = $project_model['end_date'];
            }
        }

        $model->status = 1;
        if ($model->save()) {
            $last_id = $model->id;
            $html = "<option value=''>Select project</option>";
            $clients = Milestone::model()->findAll(array('condition' => 'status = 1 AND project_id =' . $model->project_id));
            foreach ($clients as $key => $value) {
                $html .= "<option value=" . $value['id'] . ">" . $value['milestone_title'] . "</option>";
            }
            echo json_encode(array('status' => 1, 'option' => $html, 'lastid' => $last_id));
        } else {
            // echo '<pre>';
            // print_r($model->getErrors());
            // exit;
            echo json_encode(array('status' => 2, 'error' => $model->getErrors()));
        }
    }

    public function actiondependantdata()
    {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['task_ids']) && isset($_POST['percentage_values'])) {
                $project_model = Projects::model()->findByPk($_POST['project_id']);
                $start_date_array = array();
                $end_date_array = array();
                foreach ($_POST['task_ids'] as $key => $value) {
                    $percentage_value = $_POST['percentage_values'][$key];
                    $depend_on = $_POST['depndant_on_types'][$key];
                    $dependant_task_model = Tasks::model()->findByPk($value);
                    $task_progress_data = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress, max(date) as max_date, sum(qty) as completed_qty FROM pms_daily_work_progress WHERE taskid ='" . $dependant_task_model['tskid'] . "' AND approve_status = 1")->queryRow();
                    if (!empty($dependant_task_model)) {
                        $Criteria = new CDbCriteria();
                        if ($dependant_task_model['clientsite_id'] == 1 || $dependant_task_model['clientsite_id'] == NULL) {
                            $site_id = 1;
                            $Criteria->addCondition('status = 0');
                        } else {
                            $site_id = $dependant_task_model['clientsite_id'];
                            $Criteria->addCondition('status = 1');
                        }
                        $Criteria->addBetweenCondition("event_date", $dependant_task_model['start_date'], $dependant_task_model['due_date'], 'AND');
                        $Criteria->addCondition('site_id = ' . $site_id);
                        $CalenderEvents = CalenderEvents::model()->findAll($Criteria);
                        $event_array = array();
                        foreach ($CalenderEvents as $events) {
                            array_push($event_array, $events->event_date);
                        }
                    }
                    /* Having BOQ approved entry */
                    if (!empty($task_progress_data['work_progress'])) {
                        if ($task_progress_data['work_progress'] >= 100 || $percentage_value <= $task_progress_data['work_progress']) {
                            if ($depend_on == 1) {
                                $start_date_task = strtotime("1 day", strtotime($task_progress_data['max_date']));
                                $start_date_task = date('d-M-y', $start_date_task);
                                array_push($start_date_array, $start_date_task);
                            } else {
                                $start_date_task = date('d-M-y', strtotime($task_progress_data['max_date']));
                                array_push($end_date_array, $start_date_task);
                            }
                            break;
                        } else {
                            /* BOQ work progress less than required dependant % */
                            $remaining_qty = $dependant_task_model['quantity'] - $task_progress_data['completed_qty'];

                            $next_start_date = new DateTime($task_progress_data['max_date']);

                            $due_date = new DateTime($dependant_task_model['due_date']);
                            $due_date->modify('+1 day');
                            $period = new DatePeriod(
                                $next_start_date,
                                new DateInterval('P1D'),
                                $due_date
                            );

                            $remaining_days = 0;
                            foreach ($period as $key => $value) {
                                if (!in_array($value->format('Y-m-d'), $event_array)) {
                                    $remaining_days++;
                                }
                            }

                            $new_daily_target = $remaining_qty / $remaining_days;
                            $per_day_percentage = ($new_daily_target / $remaining_qty) * 100;
                            $percentage_val = $percentage_value - $task_progress_data['work_progress'];
                            $number_of_days_required = $percentage_val / $per_day_percentage;

                            $start_date = new DateTime($task_progress_data['max_date']);
                            $due_date = new DateTime($dependant_task_model['due_date']);

                            $due_date->modify('+1 day');


                            $period = new DatePeriod(
                                $start_date,
                                new DateInterval('P1D'),
                                $due_date
                            );
                            $i = 0;
                            $number_of_days_required = ceil($number_of_days_required);

                            if ($number_of_days_required > 0) {
                                foreach ($period as $key => $value) {
                                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                                        $i++;
                                        if ($i == $number_of_days_required) {
                                            $start_date_task = $value->format('d-M-y');
                                            if ($depend_on == 1) {
                                                $start_date_task = strtotime("1 day", strtotime($start_date_task));
                                                $start_date_task = date("d-M-y", $start_date_task);
                                                array_push($start_date_array, $start_date_task);
                                            } else {

                                                array_push($end_date_array, $start_date_task);
                                            }

                                            break;
                                        }
                                    }
                                }
                            } else {

                                array_push($start_date_array, date('d-M-y', strtotime($dependant_task_model['start_date'])));
                            }
                        }
                    } else {
                        if (!empty($dependant_task_model)) {
                            $duration = $dependant_task_model['task_duration'];
                            $total_qty = $dependant_task_model['quantity'];
                            $daily_target = $dependant_task_model['daily_target'];
                            $per_day_percentage = ($daily_target / $total_qty) * 100;
                            $number_of_days_required = $percentage_value / $per_day_percentage;

                            $start_date = new DateTime($dependant_task_model['start_date']);
                            $due_date = new DateTime($dependant_task_model['due_date']);

                            $due_date->modify('+1 day');


                            $period = new DatePeriod(
                                $start_date,
                                new DateInterval('P1D'),
                                $due_date
                            );
                            $i = 0;
                            $number_of_days_required = ceil($number_of_days_required);
                            if ($number_of_days_required > 0) {
                                foreach ($period as $key => $value) {
                                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                                        $i++;
                                        if ($i == $number_of_days_required) {
                                            $start_date_task = $value->format('d-M-y');
                                            if ($depend_on == 1) {
                                                $start_date_task = strtotime("1 day", strtotime($start_date_task));
                                                $start_date_task = date("d-M-y", $start_date_task);
                                                array_push($start_date_array, $start_date_task);
                                            } else {

                                                array_push($end_date_array, $start_date_task);
                                            }

                                            break;
                                        }
                                    }
                                }
                            } else {

                                array_push($start_date_array, date('d-M-y', strtotime($dependant_task_model['start_date'])));
                            }
                        }
                    }
                }

                /* foreach ends */

                if (!empty($start_date_array)) {
                    $max = max(array_map('strtotime', $start_date_array));
                }
                if (!empty($end_date_array)) {
                    $end_date_max = max(array_map('strtotime', $end_date_array));
                }

                $project_due_date = strtotime($project_model['end_date']);
                if ($project_due_date > $max) {

                    if (!empty($max)) {
                        $start_array = array('start_date' => date('d-M-y', $max));
                    } else {
                        if (!empty($_POST['parent_task_id'])) {
                            $parent_task_model = Tasks::model()->findByPk($_POST['parent_task_id']);
                            if (!empty($parent_task_model)) {

                                $start_array = array('start_date' => date('d-M-y', strtotime($parent_task_model['start_date'])));
                            } elseif (!empty($_POST['project_id'])) {

                                if (!empty($project_model)) {
                                    $start_array = array('start_date' => date('d-M-y', strtotime($project_model['start_date'])));
                                }
                            } else {

                                $start_array = array('start_date' => date('d-M-y'));
                            }
                        } else {
                            $start_array = array('start_date' => date('d-M-y', strtotime($project_model['start_date'])));
                        }
                    }
                } else {

                    $start_array = array('error' => 'Date exceeded project limit', 'start_date' => date('d-M-y', $max));
                }
                if ($project_due_date >= $end_date_max) {
                    if (isset($number_of_days_required) && !empty($end_date_max)) {
                        $end_array = array('end_date' => date('d-M-y', $end_date_max));
                    } else {
                        if (!empty($_POST['parent_task_id'])) {
                            $parent_task_model = Tasks::model()->findByPk($_POST['parent_task_id']);
                            if (!empty($parent_task_model)) {

                                $end_array = array('end_date' => date('d-M-y', strtotime($parent_task_model['due_date'])));
                            } elseif (!empty($_POST['project_id'])) {

                                if (!empty($project_model)) {
                                    $end_array = array('end_date' => date('d-M-y', strtotime($project_model['end_date'])));
                                }
                            } else {

                                $end_array = array('end_date' => date('d-M-y'));
                            }
                        } else {
                            $end_array = array('end_date' => date('d-M-y', strtotime($project_model['end_date'])));
                        }
                    }
                } else {
                    $end_array = array('error_end' => 'Date exceeded project limit', 'end_date' => date('d-M-y', $end_date_max));
                }

                $result_array = $start_array + $end_array;

                echo json_encode($result_array);
            }
        }
    }

    public function actiontaskdatevalidation()
    {
        $project_id = $_REQUEST['project_id'];
        $start_date = date('Y-m-d', strtotime($_REQUEST['start_date']));
        $end_date = date('Y-m-d', strtotime($_REQUEST['end_date']));
        $main_task = $_REQUEST['main_task'];
        $projects = Projects::model()->findByPk($project_id);
        $flag = 0;
        $flag2 = 0;
        $msg = "";
        $flag_status = 0;
        if ($main_task == "") {
            if ($projects->start_date != $start_date || $projects->end_date != $end_date) {
                if ($projects->start_date > $start_date) {
                    $flag = 1;
                    $msg = "Date not specified within " . $projects->name . " start date";
                    $flag_status = 1;
                }
                if ($projects->end_date < $end_date) {
                    $flag2 = 1;
                    $msg = "Date not specified within " . $projects->name . " end date";
                    $flag_status = 1;
                }
                if ($flag == 1 && $flag2 == 1) {
                    $msg = "Date not specified within " . $projects->name . " dates";
                    $flag_status = 1;
                }
            }
        } else {
            $tasks = Tasks::model()->findByPk($main_task);
            if ($tasks->start_date != $start_date || $tasks->due_date != $end_date) {
                if ($tasks->start_date > $start_date) {
                    $flag = 1;
                    $msg = "Date not specified within " . $tasks->title . " start date";
                    $flag_status = 1;
                }
                if ($tasks->due_date < $end_date) {
                    $flag2 = 1;
                    $msg = "Date not specified within " . $tasks->title . " end date";
                    $flag_status = 1;
                }
                if ($flag == 1 && $flag2 == 1) {
                    $msg = "Date not specified within " . $tasks->title . " dates";
                    $flag_status = 1;
                }
            }
        }
        echo json_encode(array('flag' => $flag_status, 'msg' => $msg));
    }

    public function actiongetmaintaskdate()
    {
        if (!empty($_REQUEST['maintask_id'])) {
            $tasks = Tasks::model()->findByPk($_REQUEST['maintask_id']);
            $milestone_id_val = "";
            if (!empty($tasks)) {
                $start_date = date('d-M-y', strtotime($tasks->start_date));
                $end_date = date('d-M-y', strtotime($tasks->due_date));
                if (!empty($tasks->milestone_id)) {
                    $milestone_id_val = $tasks->milestone_id;
                }
            }
        } elseif (!empty($_REQUEST['project_id'])) {
            $project = Projects::model()->findByPk($_REQUEST['project_id']);
            if (!empty($project)) {
                $start_date = date('d-M-y', strtotime($project->start_date));
                $end_date = date('d-M-y', strtotime($project->end_date));
            }
        } else {
            $start_date = "";
            $end_date = "";
        }

        echo json_encode(array('start_date' => $start_date, 'end_date' => $end_date, 'milestone_val' => $milestone_id_val));
    }

    public function actiondateChangeValidation()
    {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['task_ids']) && isset($_POST['percentage_values']) && isset($_POST['current_date'])) {
                foreach ($_POST['task_ids'] as $key => $value) {
                    $percentage_value = $_POST['percentage_values'][$key];
                    $dependant_task_model = Tasks::model()->findByPk($value);
                    if (!empty($dependant_task_model)) {
                        $duration = $dependant_task_model['task_duration'];
                        $total_qty = $dependant_task_model['quantity'];
                        $daily_target = $dependant_task_model['daily_target'];
                        $per_day_percentage = ($daily_target / $total_qty) * 100;
                        $number_of_days_required = $percentage_value / $per_day_percentage;
                        $Criteria = new CDbCriteria();
                        if ($dependant_task_model['clientsite_id'] == 1 || $dependant_task_model['clientsite_id'] == NULL) {
                            $site_id = 1;
                            $Criteria->addCondition('status = 0');
                        } else {
                            $site_id = $dependant_task_model['clientsite_id'];
                            $Criteria->addCondition('status = 1');
                        }
                        $Criteria->addBetweenCondition("event_date", $dependant_task_model['start_date'], $dependant_task_model['due_date'], 'AND');
                        $Criteria->addCondition('site_id = ' . $site_id);
                        $CalenderEvents = CalenderEvents::model()->findAll($Criteria);
                        $event_array = array();
                        foreach ($CalenderEvents as $events) {
                            array_push($event_array, $events->event_date);
                        }
                        $start_date = new DateTime($dependant_task_model['start_date']);
                        $due_date = new DateTime($dependant_task_model['due_date']);
                        $due_date->modify('+1 day');
                        $period = new DatePeriod(
                            $start_date,
                            new DateInterval('P1D'),
                            $due_date
                        );
                        $i = 0;
                        $number_of_days_required = round($number_of_days_required);
                        foreach ($period as $key => $value) {
                            if (!in_array($value->format('Y-m-d'), $event_array)) {
                                $i++;
                                if ($i == $number_of_days_required) {
                                    $start_date_task = $value->format('d-M-y');
                                    $start_date_task = strtotime("1 day", strtotime($start_date_task));
                                    $start_date_task = date("d-M-y", $start_date_task);
                                    $start_date_task = strtotime($start_date_task);
                                    $current_date = strtotime($_POST['current_date']);
                                    if ($current_date < $start_date_task) {
                                        echo $percentage_value . '% of #' . $dependant_task_model->tskid . ' will not be achieved in this date';
                                        exit;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function taskCompletionMail($id)
    {
        $tasks = Tasks::model()->findByPk($id);
        $projects = Projects::model()->findByPk($tasks->project_id);
        $tblpx = Yii::app()->db->tablePrefix;
        $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $id . "' AND approve_status = 1")->queryRow();
        if ($tasks->status == 7 && $sql_response2['work_progress'] == '100') {
            $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
            $model2 = new MailLog;
            $mail = new JPhpMailer();
            $today = date('Y-m-d H:i:s');
            $headers = "Ashly";
            //$mail_data = "<p>Project : ".$projects->name."<p><p>Task : ".$tasks->title."<p><p>Date : ".$tasks->start_date." to ".$tasks->due_date."<p><p>Closeing date of task : ".date('Y-m-d')."</p>";

            $app_root = YiiBase::getPathOfAlias('webroot');
            $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
            $logo = $theme_asset_url . 'default-logo.png';
            if (file_exists($app_root . "/themes/assets/logo.png")) {
                $logo = $theme_asset_url . 'logo.png';
            }

            $mail_data = '
		<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
		<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $tasks->assignedTo->first_name . '</span></h3>
		<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
		<tr><td colspan="2" style="background-color: #ffff;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $logo . '" width="50" height="50" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
		</tr>
		<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>Task # ' . $id . ' : Completion notification</h2></div></td>
		</tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project: </td><td style="border:1px solid #f5f5f5;">' . $projects->name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $tasks->title . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Quantity: </td><td style="border:1px solid #f5f5f5;">' . $tasks->quantity . '</td></tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start & Due dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($tasks->start_date)) . ' & ' . date('d-M-y', strtotime($tasks->due_date)) . '</td></tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Closing date of task:</td><td style="border:1px solid #f5f5f5;">' . date('d-M-y') . '</td></tr>
		</table>
		<p>Sincerely,  <br />
		' . Yii::app()->name . '</p>
		</div>';

            $bodyContent = $mail_data;
            $mail->IsSMTP();
            //.':'.$mail_model['smtp_port']
            $mail->Host = $mail_model['smtp_host'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->Subject = 'Task # ' . $id . ' : Completion notification';
            $receippients = explode(',', $tasks->email);
            foreach ($receippients as $receippient) {
                $mail->addAddress($receippient); // Add a recipient
            }
            $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
            $dataJSON = json_encode($mail_data);
            $receippientsJSON = json_encode($receippients);
            $mail_send_byJSON = json_encode($mail_send_by);
            $subject = $mail->Subject;
            $model2->send_to = $receippientsJSON;
            $model2->send_by = $mail_send_byJSON;
            $model2->send_date = $today;
            $model2->message = htmlentities($bodyContent);
            $model2->description = $mail->Subject;
            $model2->created_date = $today;
            $model2->created_by = Yii::app()->user->getId();
            $model2->mail_type = $subject;
            $mail->isHTML(true);
            $mail->MsgHTML($bodyContent);
            $mail->Body = $bodyContent;
            if ($mail->Send()) {

                $model2->sendemail_status = 1;
            } else {
                $model2->sendemail_status = 0;
            }
            $model2->save();
        }
    }

    public function actiongetdependancydate()
    {
        $task_id = $_REQUEST['task_id'];
        $dependancy = $_REQUEST['dependancy'];
        $project_id = $_REQUEST['project_id'];
        $parent_task_id = $_REQUEST['parent_task_id'];
        if (!empty($parent_task_id)) {
            $parent_task_model = Tasks::model()->findByPk($parent_task_id);
            if (!empty($parent_task_model)) {
                $start_date = date('d-M-y', strtotime($parent_task_model->start_date));
                $end_date = date('d-M-y', strtotime($parent_task_model->due_date));
            }
        } elseif (!empty($project_id)) {
            $project_model = Projects::model()->findByPk($project_id);
            if (!empty($project_model)) {
                $start_date = date('d-M-y', strtotime($project_model->start_date));
                $end_date = date('d-M-y', strtotime($project_model->end_date));
            }
        }
        if (!empty($task_id)) {
            $tasks = Tasks::model()->findByPk($task_id);
            if ($dependancy == 1) {
                $start_date = date('d-M-y', strtotime($tasks->start_date));
            } else {
                $end_date = date('d-M-y', strtotime($tasks->due_date));
            }
        }



        echo json_encode(array('start_date' => $start_date, 'end_date' => $end_date));
    }

    public function actionworkerslimit()
    {
        $result = array();
        if (!empty($_POST['daily_target']) && !empty($_POST['selected_work_type'])) {
            $worktype_model = WorkType::model()->findByPk($_POST['selected_work_type']);
            if (!empty($worktype_model->daily_throughput)) {
                $minimum_workers_count = $_POST['daily_target'] / $worktype_model->daily_throughput;
            } else {
                $minimum_workers_count = 1;
            }
            // if(!empty($_POST['workers_max_count'])){
            //     if($_POST['workers_max_count'] < $minimum_workers_count){
            //         $result = array('status'=>1,'allowed'=>$_POST['workers_max_count'],'msg'=>'Minimum allowed workers is '.ceil($minimum_workers_count));
            //     }else{
            //         $result = array('status'=>1,'allowed'=>$_POST['workers_max_count'],'msg'=>'');
            //     }
            // }
            // else{
            //     $result = array('status'=>1,'allowed'=>ceil($minimum_workers_count),'msg'=>'');
            // }
            $result = array('status' => 1, 'required' => ceil($minimum_workers_count), 'msg' => '');
        } else {
            $result = array('status' => 0, 'required' => 0, 'msg' => '');
        }
        echo json_encode($result);
    }

    protected function milestonedatechange($task_model)
    {
        Yii::app()->getModule('masters');
        if (!empty($task_model->milestone_id) || $task_model->milestone_id != 0) {
            $result = Yii::app()->db->createCommand()
                ->select('MIN(start_date) as start_date,MAX(due_date) as end_date')
                ->from('pms_tasks')
                ->where('milestone_id =' . $task_model->milestone_id . ' AND project_id=' . $task_model->project_id)
                ->queryRow();
            $milestone_model = Milestone::model()->find(array('condition' => 'id = ' . $task_model->milestone_id . ' AND project_id = ' . $task_model->project_id));
            if (!empty($milestone_model)) {
                if (!empty($result['start_date'])) {
                    $milestone_model->start_date = $result['start_date'];
                }
                if (!empty($result['end_date'])) {
                    $milestone_model->end_date = $result['end_date'];
                }
                $milestone_model->save();
            }
        }
    }

    public function actionTaskLog()
    {
        $model = new TaskLog('search');
        $pmodel = new ImportForm();

        $type = '';
        $model->unsetAttributes();  // clear any default values
        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end


        if (isset($_GET['Tasks']))
            $model->attributes = $_GET['Tasks'];

        $this->render(
            'tasks_log',
            array(
                'model' => $model,
                'type' => $type,
                'pmodel' => $pmodel,
            )
        );
    }

    public function dataChangeMail($id, $change_type)
    {

        $app_root = YiiBase::getPathOfAlias('webroot');
        $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
        $logo = $theme_asset_url . 'default-logo.png';
        if (file_exists($app_root . "/themes/assets/logo.png")) {
            $logo = $theme_asset_url . 'logo.png';
        }

        $model = $this->loadModel($id);
        if ($change_type == 1) {
            $name = isset($model->assignedTo->first_name) ? $model->assignedTo->first_name : '';
            $toemail = $model->assignedTo->email;
            $subject = '[Task Assigned] #' . $id . ': ' . $model->title;
            $subject_view = 'Task Assigned #' . $id . ': ' . $model->title;
        } elseif ($change_type == 2) {
            $subject = '[New Task] #' . $id . ' Created to ' . $model->assignedTo->first_name . ': ' . $model->title;
            $subject_view = 'New Task #' . $id . ': ' . $model->title;
            $name = $model->coordinator0->first_name;
            $toemail = $model->coordinator0->email;
        } elseif ($change_type == 3) {
            $name = $model->contractor->contractor_title;
            $toemail = $model->contractor->email_id;
            $subject = '[New Task] #' . $id . ' Created ' . ': ' . $model->title;
            $subject_view = 'New Task #' . $id . ': ' . $model->title;
        }
        $mail_body = '
        <div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">           
        <h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $name . '</span></h3>
        <table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
        <tr><td colspan="2" style="background-color: #ffff;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $logo . '" width="50" height="50" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
        </tr>
        <tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>PMS - New Task created</h2></div></td>
        </tr>
        <tr><td colspan="2">This is a notification to inform that new task #' . $id . ' has been created: </td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $model->title . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project Name: </td><td style="border:1px solid #f5f5f5;">' . $model->project->name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($model->start_date)) . ' - ' . date('d-M-y', strtotime($model->due_date)) . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Priority:</td><td style="border:1px solid #f5f5f5;">' . $model->priority0->caption . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Assigned To:</td><td style="border:1px solid #f5f5f5;">' . $model->assignedTo->first_name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Coordinator:</td><td style="border:1px solid #f5f5f5;">' . $model->coordinator0->first_name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Created by: </td><td style="border:1px solid #f5f5f5;">' . $model->createdBy->first_name . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Billable:</td> <td style="border:1px solid #f5f5f5;">' . $model->billable0->caption . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Quantity: </td><td style="border:1px solid #f5f5f5;">' . $model->quantity . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task status:</td> <td style="border:1px solid #f5f5f5;">' . $model->status0->caption . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Description:</td> <td style="border:1px solid #f5f5f5;">' . nl2br($model->description) . '</td></tr>
        <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;" colspan="2">' . CHtml::link('Click here to see this task', $this->createAbsoluteUrl('tasks/view', array('id' => $id))) . '</td></tr>
        </table>

        <p>Sincerely,  <br />
        ' . Yii::app()->name . '</p>
        </div>';
        $asubject = '[Task Assigned] #' . $id . ': ' . $model->title;
        $asubject_view = 'Task Assigned #' . $id . ': ' . $model->title;
        $mail_log = $this->saveMailLog($toemail, $asubject, $mail_body);
        $this->send_mail($name, $toemail, $asubject, $mail_body, $asubject_view, $mail_log);
    }

    public function actionExpiredConsolidatedMail()
    {
        $task_owners = $this->getTaskOwners();
        if (!empty($task_owners)) {
            foreach ($task_owners as $task_owner) {
                $criteria = new CDbCriteria;
                $criteria->addCondition('status NOT IN (7)');
                $criteria->addCondition("due_date < '" . date('Y-m-d') . "' ");
                $criteria->addCondition("report_to = " . $task_owner);
                $expired_tasks = Tasks::model()->findAll($criteria);
                $owner_model = Users::model()->findByPk($task_owner);
                if (count($expired_tasks) >= 1) {
                    $expired_mail_data = $this->renderPartial('_task_expired_mail', array('data' => $expired_tasks, 'owner' => $owner_model), true);
                    $model2 = new MailLog;
                    $today = date('Y-m-d H:i:s');
                    $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);

                    $mail = new JPhpMailer();
                    $headers = "Hostech";
                    $bodyContent = $expired_mail_data;
                    $mail->IsSMTP();
                    $mail->Host = $mail_model['smtp_host'];
                    $mail->Port = $mail_model['smtp_port'];
                    $mail->SMTPSecure = $mail_model['smtp_secure'];
                    $mail->SMTPAuth = $mail_model['smtp_auth'];
                    $mail->Username = $mail_model['smtp_username'];
                    $mail->Password = $mail_model['smtp_password'];
                    $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
                    $mail->Subject = "Expired Task Notification";

                    // echo '<pre>';print_r($owner_model);exit;
                    if (!empty($owner_model->email)) {
                        $mail->addAddress($owner_model->email); // Add a recipient
                        // $mail->addAddress('surumi.ja@bluehorizoninfotech.com');
                    }
                    $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
                    $mail_send_byJSON = json_encode($mail_send_by);
                    $subject = $mail->Subject;
                    $model2->send_to = $owner_model->email;
                    $model2->send_by = $mail_send_byJSON;
                    $model2->send_date = $today;
                    $model2->message = htmlentities($bodyContent);
                    $model2->description = $mail->Subject;
                    $model2->created_date = $today;
                    $model2->created_by = Yii::app()->user->getId();
                    $model2->mail_type = $subject;
                    $mail->isHTML(true);
                    $mail->MsgHTML($bodyContent);
                    $mail->Body = $bodyContent;
                    if (!empty($owner_model->email)) {
                        if ($mail->Send()) {
                            $model2->sendemail_status = 1;
                        } else {
                            $model2->sendemail_status = 0;
                        }
                    } else {
                        $model2->sendemail_status = 0;
                    }

                    $model2->save();
                }
            }
        }
    }

    public function getTaskOwners()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'report_to';
        $criteria->condition = 'due_date < "' . date('Y-m-d') . '"';
        $task_owners = Tasks::model()->findAll($criteria);
        $task_owners = array_column($task_owners, 'report_to');
        $owner_array = array();
        foreach ($task_owners as $user) {
            if (!array_key_exists($user, $owner_array))
                $owner_array[$user] = $user;
        }
        return $owner_array;
    }

    public function actionchangeAssignee()
    {
        if (Yii::app()->request->isAjaxRequest) {
            // echo '<pre>';
            // print_r($_POST);
            // exit;
            $final_response = array();
            $assignee_id = $_POST["assignee_id"];
            $task_id = $_POST["task_ids"];
            $task_id_array = $_POST['task_ids'];
            foreach ($task_id_array as $task_id) {
                $model = $this->loadModel($task_id);
                $assigned_id_val = $model->assigned_to;
                $model->assigned_to = $assignee_id;
                if ($model->save()) {
                    if ($model->assigned_to != $assigned_id_val) {
                        try {
                            $this->dataChangeMail($model->tskid, 1); //1=>assignee change
                        } catch (Exception $e) {
                            $exceptionError = trim(strip_tags($e->errorMessage()));
                            if ($exceptionError == "SMTP Error: Could not connect to SMTP host." or (defined('SEND_EMAIL') and SEND_EMAIL == false)) {
                                $result = array('task_id' => $task_id, 'assigned_to' => $model->assignedTo->first_name, 'status' => 1);
                            } else {
                            }
                        }
                    }

                    $result = array('task_id' => $task_id, 'assigned_to' => $model->assignedTo->first_name, 'status' => 1);
                } else {
                    $error_data = $model->getErrors();
                    foreach ($error_data as $key => $value) {
                        // if ($key == 'assigned_to') {
                        $result = array('task_id' => $task_id, 'msg' => $value[0], 'status' => 2);
                        // }
                    }
                }
                $final_response[$task_id] = $result;
            }


            echo json_encode($final_response);
        }
    }

    public function actionaddMainTask()
    {
        $project_drpdwn_html = '';
        $model = new Tasks('mainTaskSearch');
        if (isset($_POST['Tasks']['tskid']) && !empty($_POST['Tasks']['tskid'])) {
            $id = $_POST['Tasks']['tskid'];
            $model = $this->loadModel($id);
        }
        $model->setscenario('mainTasks');
        $this->performAjaxValidation($model);
        if (isset($_POST['Tasks'])) {

            $model->attributes = $_POST['Tasks'];
            $model->status = 75;
            $model->start_date = date('Y-m-d', strtotime($model->start_date));
            $model->due_date = date('Y-m-d', strtotime($model->due_date));
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_date = date('Y-m-d');
            if($_POST['submit_type'] == 'save_minutes_task'){
                $model->ranking= $this->actiongetHighestRank($model->milestone_id);
            }
            if ($model->save()) {
                if (isset($_POST['submit_type'])) {
                    if ($_POST['submit_type'] == 'save_btn') {
                        $return_result = array('status' => 1, 'next_level' => 0);
                    } elseif ($_POST['submit_type'] == 'save_cnt') {
                        $project_drpdwn_html .= "<option value=" . $model->project_id . ">" . $model->project->name . "</option>";
                        $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html);
                    } elseif ($_POST['submit_type'] == 'save_minutes_task') {
                        $return_result = array('status' => 1, 'next_level' => 1, 'task_id' => $model->tskid);
                    } else {
                        $return_result = array('status' => 1, 'next_level' => 0);
                    }
                    echo json_encode($return_result);
                    exit;
                }
            } else {
                $return_result = array('status' => 2, 'next_level' => 0,'error'=>$model->getErrors());
                echo json_encode($return_result);
                exit;
            }
        }
    }

    public function actiongetModel()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = Tasks::model()->findByPk($id);
            if (!empty($model)) {
                $model_array = array('stat' => 1, 'id' => $model->tskid, 'title' => $model->title, 'ranking' => $model->ranking, 'milestone_id' => $model->milestone_id, 'start_date' => date('d-M-y', strtotime($model->start_date)), 'end_date' => date('d-M-y', strtotime($model->due_date)));
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo json_encode($model_array);
        exit;
    }
    public function actiongettaskForm()
    {
        $model = new Tasks();
        $project_id = $_GET['asDialog'];
        $location = [];

        $this->layout = '//layouts/iframe';
        $this->render(
            'meeting_task_form',
            array(
                'model' => $model,
                'project_id' => $project_id,
                'location' => $location
            )
        );
    }
    public function actionitemEstimation($id, $project_id)
    {

        $task = Tasks::model()->findByPK($id);
        $project = Projects::model()->findByPK($project_id);
        $template_id = $project->template_id;
        $template_items = TemplateItems::model()->findAll(array('condition' => 'temp_id = ' . $template_id));
        $this->render(
            '_task_item_estimate',
            array(
                'task' => $task,
                'template_items' => $template_items,
                'project' => $project
            )
        );
    }
    public function getItemEstimate($task_id, $item_id)
    {

        $item_estimate = Yii::app()->db->createCommand()

            ->from('pms_task_item_estimation')
            ->where('task_id =' . $task_id . ' AND item_id=' . $item_id)
            ->queryRow();
        return $item_estimate;
    }
    public function actionaddItemEstimate()
    {
        $estimate_id = $_POST['estimate_id'];
        $item_id = $_POST['item_id'];
        $item_estimate = $_POST['item_estimate'];
        $template_id = $_POST['template_id'];
        $task_id = $_POST['task_id'];


        if ($estimate_id != "") {
            $model = TaskItemEstimation::model()->findByPK($estimate_id);
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
        } else {
            $model = new TaskItemEstimation;
        }
        $model->item_id = $item_id;
        $model->item_quantity_required = $item_estimate;
        $model->template_id = $template_id;
        $model->task_id = $task_id;
        $model->created_by = Yii::app()->user->id;
        $model->created_date = date('Y-m-d H:i:s');
        if ($model->save()) {
            $response = array('status' => 1);
        } else {
            $response = array('status' => 0);
        }
        echo json_encode($response);
    }
    public function actionimport_task_estimate()
    {
        if (Yii::app()->user->role != 1) {
            $proj_condition = "status =1 AND (find_in_set(" . Yii::app()->user->id . ",assigned_to)  )";
        } else {
            $proj_condition = " status = 1";
        }
        $project = Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC '));

        $template = Template::model()->findAll();

        $this->render(
            'import_task_estimate',
            array(
                'project' => $project,
                'template' => $template
            )
        );
    }
    public function actionCreateHandsontable()
    {

        $records = $_POST['records'];
        $head = $_POST['head'];
        $count = count($head);
        $template_id = $_POST['template_id'];
        $result = [];
        foreach ($records as $data) {
            $insert_data[] = array_combine($head, $data);

            $res_data = array_combine($head, $data);

            foreach ($res_data as $keys => $result_datas) {
                $item_det = TemplateItems::model()->findByAttributes(
                    array(
                        'item_name' => $keys,
                        'temp_id' => $template_id
                    )
                );
                if ($item_det) {
                    $result[] = array(
                        'item_id' => $item_det->id,
                        'item_quantity_required' => $result_datas,
                        'task_id' => $data[0]
                    );
                }
            }
        }


        foreach ($result as $res) {
            if ($res['item_id'] != "" && $res['task_id'] != "" && $res['item_quantity_required'] != "") {


                $already_exist = TaskItemEstimation::model()->findByAttributes(
                    array(
                        'task_id' => $res['task_id'],
                        'item_id' => $res['item_id'],
                        'template_id' => $template_id
                    )
                );

                if ($already_exist) {
                    $model = TaskItemEstimation::model()->findByAttributes(
                        array(
                            'task_id' => $res['task_id'],
                            'item_id' => $res['item_id'],
                            'template_id' => $template_id
                        )
                    );
                    $model->updated_by = Yii::app()->user->id;
                    $model->updated_date = date('Y-m-d H:i:s');
                } else {
                    $model = new TaskItemEstimation;
                }
                $model->item_id = $res['item_id'];
                $model->item_quantity_required = $res['item_quantity_required'];
                $model->template_id = $template_id;
                $model->task_id = $res['task_id'];
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $result = array('status' => '1', 'message' => 'success');
                } else {
                    $result = array('status' => '2', 'message' => 'An error occured');
                }
            }
        }
        echo json_encode($result);
    }
    public function actioncheckProject()
    {
        $project_id = $_POST['project_id'];
        $project = Projects::model()->findByPk($project_id);
        if ($project->template_id != "") {
            $response = array('status' => '1');
        } else {
            $response = array('status' => '2');
        }
        echo json_encode($response);
    }
    public function actiongetHandsontbl()
    {
        $project_id = $_POST['project_id'];
        $project = Projects::model()->findByPk($project_id);
        $template_id = $project->template_id;
        $tem_det = TemplateItems::model()->findAllByAttributes(
            array(
                'temp_id' => $template_id,
            )
        );
        $item_array = array("task id", "task name");
        foreach ($tem_det as $items) {

            array_push($item_array, $items->item_name);
            $item_id_array[] = $items->id;
        }
        $task_details = Tasks::model()->findAllByAttributes(
            array(
                'project_id' => $project_id,
            )
        );

        $task_array = [];

        foreach ($task_details as $tasks) {

            $task_array[] = array($tasks->tskid, $tasks->title);
        }


        foreach ($task_array as $key => $value) {

            foreach ($item_id_array as $id) {
                $item_estimate = TaskItemEstimation::model()->findByAttributes(
                    array(
                        'item_id' => $id,
                        'task_id' => $value[0]
                    )
                );

                if ($item_estimate) {
                    $value[] = $item_estimate->item_quantity_required;
                } else {
                    $value[] = '';
                }
            }
            $task_array[$key] = $value;
        }
        // echo "<pre>",print_r($task_array);
        if (count($task_array) > 0) {
            $result = array(
                'count' => count($tem_det) + 2,
                'data' => $item_array,
                'task_array' => $task_array,
                'template_id' => $template_id,
                'status' => 1

            );
        } else {
            $result = array(
                'count' => count($tem_det) + 2,
                'data' => $item_array,
                'task_array' => $task_array,
                'template_id' => $template_id,
                'status' => 0

            );
        }
        echo (json_encode($result));
    }
    public function actiongenerateItemEstimation()
    {
        $projectid = $_POST['projectid'];
        $project = Projects::model()->findByPk($projectid);
        if ($project->template_id != "") {
            $template_id = $project->template_id;
            $template_items = TemplateItems::model()->findAll(array('condition' => 'temp_id = ' . $template_id));
            $tasklist = $this->renderPartial('_task_item_estimation_form', array(
                'template_items' => $template_items,
                // 'projectid' => $projectid,

            ), true);
            echo json_encode($tasklist);
        }
    }
    public function addItemEstimation($taskid, $project_id, $item_qty, $item_name)
    {
        foreach ($item_qty as $keys => $value) {
            if ($value != "") {
                $project = Projects::model()->findByPK($project_id);
                $template_id = $project->template_id;
                $already_exist = TaskItemEstimation::model()->findByAttributes(
                    array(
                        'task_id' => $taskid,
                        'item_id' => $item_name[$keys],
                        'template_id' => $template_id
                    )
                );
                if ($already_exist) {
                    $estimation_model = TaskItemEstimation::model()->findByAttributes(
                        array(
                            'task_id' => $taskid,
                            'item_id' => $item_name[$keys],
                            'template_id' => $template_id
                        )
                    );
                    $estimation_model->updated_by = Yii::app()->user->id;
                    $estimation_model->updated_date = date('Y-m-d H:i:s');
                } else {
                    $estimation_model = new TaskItemEstimation;
                }
                $estimation_model->item_id = $_POST['item_name'][$keys];
                $estimation_model->item_quantity_required = $value;
                $estimation_model->template_id = $template_id;
                $estimation_model->task_id = $taskid;
                $estimation_model->created_by = Yii::app()->user->id;
                $estimation_model->created_date = date('Y-m-d H:i:s');
                $estimation_model->save();
            }
        }
    }

    public function actionmonthlyTask1()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data_array = [];
        $model = new Tasks;
        $criteria = new CDbCriteria;
        $criteria->select = "t.id,t.milestone_title,t.start_date,t.end_date,pj.name,bh.budget_head_title";
        $criteria->join = 'LEFT  JOIN {{budget_head}} bh ON bh.budget_head_id=t.budget_id ';
        $criteria->join .= 'INNER JOIN {{projects}} pj ON pj.pid=t.project_id ';
        $criteria->join .= 'LEFT  JOIN {{tasks}} tsk ON tsk.milestone_id=t.id ';



        if (isset(Yii::app()->session['monthly_task_project_id'])) {
            $criteria->compare('t.project_id', Yii::app()->session['monthly_task_project_id']);
        }


        if (isset(Yii::app()->session['monthly_task_owner'])) {

            $criteria->compare('tsk.report_to', Yii::app()->session['monthly_task_owner']);
        }

        if (isset(Yii::app()->session['monthly_task_coordinator'])) {

            $criteria->compare('tsk.coordinator', Yii::app()->session['monthly_task_coordinator']);
        }

        if (isset(Yii::app()->session['monthly_task_priority'])) {

            $criteria->compare('tsk.priority', Yii::app()->session['monthly_task_priority']);
        }


        if (isset($_GET['Tasks']['project_id'])) {
            $criteria->compare('t.project_id', $_GET['Tasks']['project_id']);
        }

        if (isset($_GET['Tasks']['report_to'])) {
            $criteria->compare('tsk.report_to', $_GET['Tasks']['report_to']);
        }
        if (isset($_GET['Tasks']['coordinator'])) {
            $criteria->compare('tsk.coordinator', $_GET['Tasks']['coordinator']);
        }
        if (isset($_GET['Tasks']['priority'])) {
            $criteria->compare('tsk.priority', $_GET['Tasks']['priority']);
        }

        $criteria->group = 'tsk.milestone_id';

        $data = Milestone::model()->findAll($criteria);
        foreach ($data as $details) {
            $data_array[$details->name][$details->budget_head_title][$details->milestone_title] = array('start_date' => $details->start_date, 'end_date' => $details->end_date, 'milestone_id' => $details->id);
        }

        // echo "<pre>",print_r($data_array);exit;


        $this->render(
            'monthly_task',
            array(
                'data_array' => $data_array,
                'model' => $model

            )
        );
    }

    public function actionmonthlyTask()
    {

        $month = date('m');
        $year = date("Y");
        $date = date('Y-m-d');

        // $current_task = Yii::app()->db->createCommand("SELECT tskid,project_id,due_date,status FROM `pms_tasks` WHERE  MONTH(`start_date`) <=  $month and MONTH(due_date) >=  $month and  YEAR(due_date)>=$year ")->queryAll();

        $current_task = Yii::app()->db->createCommand("SELECT tskid,project_id,due_date,status FROM `pms_tasks` WHERE  MONTH(`start_date`) <=  12 and MONTH(due_date) <= 12 and  YEAR(due_date)>=$year ")->queryAll();

        $task_id_array = [];
        $project_id_array = [];
        foreach ($current_task as $tasks) {
            $due_date = $tasks['due_date'];
            $status = $tasks['status'];
            $expired_condition = $due_date < date('Y-m-d');



            if (!$expired_condition && $status != 7) {
                $project_id_array[] = $tasks['project_id'];
                $task_id_array[] = $tasks['tskid'];
            }

            if (!$expired_condition && $status == 7) {
                $project_id_array[] = $tasks['project_id'];
                $task_id_array[] = $tasks['tskid'];
            }
        }



        $expired_task = Yii::app()->db->createCommand("SELECT tskid,project_id FROM `pms_tasks` WHERE `due_date`< '$date' and status != 7 and project_id != ''
          ")->queryAll();

        foreach ($expired_task as $expired) {

            array_push($project_id_array, $expired['project_id']);
            array_push($task_id_array, $expired['tskid']);
        }



        $project_id_array = array_unique($project_id_array);
        $task_id_array = array_unique($task_id_array);
        sort($project_id_array);



        $data_array = [];
        $model = new Tasks;
        $criteria = new CDbCriteria;
        $criteria->select = "t.id,t.milestone_title,t.start_date,t.end_date,pj.name,bh.budget_head_title,t.project_id,tsk.status,tsk.title,tsk.tskid,tsk.start_date,tsk.due_date";
        $criteria->join = 'LEFT  JOIN {{budget_head}} bh ON bh.budget_head_id=t.budget_id ';
        $criteria->join .= 'INNER JOIN {{projects}} pj ON pj.pid=t.project_id ';
        $criteria->join .= 'LEFT  JOIN {{tasks}} tsk ON tsk.milestone_id=t.id ';
        $criteria->addInCondition('t.project_id', $project_id_array);

        // if (isset(Yii::app()->session['monthly_task_project_id'])) {

        //     $criteria->compare('t.project_id', Yii::app()->session['monthly_task_project_id']);
        // }


        // if (isset(Yii::app()->session['monthly_task_owner'])) {

        //     $criteria->compare('tsk.report_to', Yii::app()->session['monthly_task_owner']);
        // }

        // if (isset(Yii::app()->session['monthly_task_coordinator'])) {

        //     $criteria->compare('tsk.coordinator', Yii::app()->session['monthly_task_coordinator']);
        // }

        // if (isset(Yii::app()->session['monthly_task_priority'])) {

        //     $criteria->compare('tsk.priority', Yii::app()->session['monthly_task_priority']);
        // }


        if (isset($_GET['Tasks']['project_id'])) {
            $criteria->compare('t.project_id', $_GET['Tasks']['project_id']);
        }


        if (isset($_GET['Tasks']['report_to'])) {
            $criteria->compare('tsk.report_to', $_GET['Tasks']['report_to']);
        }

        if (isset($_GET['Tasks']['coordinator'])) {
            $criteria->compare('tsk.coordinator', $_GET['Tasks']['coordinator']);
        }

        if (isset($_GET['Tasks']['priority'])) {
            $criteria->compare('tsk.priority', $_GET['Tasks']['priority']);
        }

        $criteria->group = 'tsk.milestone_id';

        $data = Milestone::model()->findAll($criteria);


        foreach ($data as $details) {

            $due_date = $details['due_date'];
            $task_status = $details['status'];
            $task_expired_condition = $due_date < date('Y-m-d');


            if ($details['tskid'] != "") {
                if ($task_expired_condition && $task_status != 7) {
                    $data_array[$details->name][$details->budget_head_title][$details->milestone_title] = array('start_date' => $details->start_date, 'end_date' => $details->end_date, 'milestone_id' => $details->id, 'task_date' => $details['start_date'] . "---" . $details['due_date'], 'task_id' => $details['tskid']);
                }
                if (!$task_expired_condition) {
                    $data_array[$details->name][$details->budget_head_title][$details->milestone_title] = array('start_date' => $details->start_date, 'end_date' => $details->end_date, 'milestone_id' => $details->id, 'task_date' => $details['start_date'] . "---" . $details['due_date'], 'task_id' => $details['tskid']);
                }
            }


            // $data_array[$details->name][$details->budget_head_title][$details->milestone_title] = array('start_date' => $details->start_date, 'end_date' => $details->end_date, 'milestone_id' => $details->id,'task_date'=>$details['start_date']."---".$details['due_date'],'task_id'=>$details['tskid']);
        }

        $this->render(
            'monthly_task',
            array(
                'data_array' => $data_array,
                'model' => $model,
                'task_id_array' => $task_id_array

            )
        );
    }
    public function actioncreateWeeklytask()
    {

        $model = new Tasks;
        $location = array();
        $assigned_to = array();


        if (isset($_POST['pro_name'])) {
            $project_name = $_POST['pro_name'];

            $task = $this->renderPartial('_create_weekly_task', array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to

            ), true);

            echo json_encode($task);
        }
    }

    public function actionaddweeklytask()
    {


        $model = new Tasks;
        $model->setscenario('create');
        $location = array();
        $assigned_to = array();
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tasks'])) {

            $model->attributes = $_POST['Tasks'];
            if (!empty($_POST['Tasks']['unit'])) {
                $model->unit = $_POST['Tasks']['unit'];
            }
            $model->start_date = date('Y-m-d', strtotime($_POST['Tasks']['start_date']));
            $model->due_date = date('Y-m-d', strtotime($_POST['Tasks']['due_date']));
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->work_type_id = $_POST['Tasks']['work_type_id'];
            $model->allowed_workers = $_POST['Tasks']['allowed_workers'];
            //new parent task id
            $model->parent_tskid = $_POST['Tasks']['parent_tskid'];
            $model->email = $_POST['Tasks']['email'];
            $model->task_type = $_POST['Tasks']['task_type'];




            if ($model->save()) {

                if (isset($_POST['item_qty']) && count($_POST['item_qty']) > 0) {
                    $this->addItemEstimation($model->tskid, $model->project_id, $_POST['item_qty'], $_POST['item_name']);
                }
                $this->dependancyBasedDate($_POST, $model);
                //$this->milestonedatechange($model);
                // project session
                if (yii::app()->user->id == $_POST['Tasks']['assigned_to']) {
                    Yii::app()->user->setState('project_id', $_POST['Tasks']['project_id']);
                }
                $this->createTaskMail($model->tskid); //send mail
                Yii::app()->user->setFlash('success', 'Successfully created.');

                unset(Yii::app()->session['parent_task_id']);

                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    echo CHtml::script(" parent.location.reload();
                    window.parent.$('#cru-dialog').dialog('close');
                    window.parent.$('#cru-frame').attr('src','');
                   ");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index2', 'id' => $model->tskid));
                }

                $this->redirect(array('monthlyTask'));
            }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])) {

            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------
        $project_name = $_GET['project'];
        $get_project = Projects::model()->findByAttributes(array('name' => $project_name));
        $budget_head_id = "";
        $budget_head = "";
        $milestoneid = "";
        $parent_task_id = "";
        $sub_task_id = "";
        if (isset($_GET['budget_head'])) {
            $budget_head = $_GET['budget_head'];
        }


        if ($budget_head != "") {
            $get_budget_head = BudgetHead::model()->findByAttributes(array('budget_head_title' => $budget_head));
            $budget_head_id = $get_budget_head->budget_head_id;
        }
        if (isset($_GET['milestone_id'])) {
            $milestoneid = $_GET['milestone_id'];
        }

        if (isset($_GET['parent_task_id'])) {
            $parent_task_id = $_GET['parent_task_id'];
            Yii::app()->session['parent_task_id'] = $parent_task_id;
        }
        if (isset($_GET['sub_task_id'])) {

            $sub_task_id = $_GET['sub_task_id'];

            Yii::app()->session['parent_task_id'] = $sub_task_id;
        }

        $unit_list = Unit::model()->findAll(['condition' => 'status = 1']);
        $template = Template::model()->findAll();
        $this->render(
            '_create_weekly_task',
            array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to,
                'unit_list' => $unit_list,
                'template' => $template,
                'task_project_id' => $get_project->pid,
                'task_budget_head_id' => $budget_head_id,
                'task_milestone_id' => $milestoneid,
                'task_parent_task_id' => $parent_task_id,
                'task_sub_task_id' => $sub_task_id
            )
        );
    }

    public function actiongettaskclientsite()
    {
        if (!empty($_REQUEST['project_id'])) {
            $tblpx = Yii::app()->db->tablePrefix;

            if (Yii::app()->user->role == 10) {


                $data = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite cs JOIN {$tblpx}clientsite_assigned ca ON ca.site_id = cs.id WHERE cs.pid=" . $_REQUEST['project_id'] . " AND ca.user_id = '" . Yii::app()->user->id . "'")->queryAll();
                $data = CHtml::listData($data, 'id', 'site_name');

                $html = "<option value=''>Select clientsite</option>";

                foreach ($data as $value => $site_name) {
                    $html .= "<option value=" . $value . ">" . $site_name . "</option>";
                }
            } else {

                $data = Yii::app()->db->createCommand("select * FROM {$tblpx}clientsite WHERE pid=" . $_REQUEST['project_id'] . "")->queryAll();
                $data = CHtml::listData($data, 'id', 'site_name');


                $html = "<option value=''>Select clientsite</option>";

                foreach ($data as $value => $site_name) {
                    $html .= "<option value=" . $value . ">" . $site_name . "</option>";
                }
            }


            echo json_encode(array('option' => $html));

            //echo json_encode(array('clientsite'=>$client_site));
        }
    }

    public function actiongetbudgetheadmilestone()
    {
        $tblpx = Yii::app()->db->tablePrefix;

        $data = Yii::app()->db->createCommand("select * FROM {$tblpx}milestone WHERE budget_id=" . $_REQUEST['task_budget_head_id'] . "")->queryAll();
        $data = CHtml::listData($data, 'id', 'milestone_title');

        if (count($data) > 1) {
            $html = "<option value=''>Select milestone</option>";

            foreach ($data as $value => $milestone_title) {
                $html .= "<option value=" . $value . ">" . $milestone_title . "</option>";
            }
        } else {
            foreach ($data as $value => $milestone_title) {
                $html = "<option value=" . $value . ">" . $milestone_title . "</option>";
            }
        }

        echo json_encode(array('option' => $html));
    }
    public function actiongetmilestone()
    {
        $tblpx = Yii::app()->db->tablePrefix;

        $data = Yii::app()->db->createCommand("select * FROM {$tblpx}milestone WHERE id=" . $_REQUEST['task_milestone_id'] . "")->queryAll();
        $data = CHtml::listData($data, 'id', 'milestone_title');


        // $html = "<option value=''>Select milestone</option>";

        foreach ($data as $value => $milestone_title) {
            $html = "<option value=" . $value . ">" . $milestone_title . "</option>";
        }
        echo json_encode(array('option' => $html));
    }

    public function actiongetparentaskmilestone()
    {
        $tblpx = Yii::app()->db->tablePrefix;

        $data = Yii::app()->db->createCommand("select * FROM {$tblpx}milestone as m
        JOIN {$tblpx}tasks as t ON t.milestone_id = m.id
         WHERE tskid=" . $_REQUEST['task_parent_task_id'] . "")->queryAll();

        foreach ($data as $value => $milestone_title) {
            $html = "<option value=" . $milestone_title['id'] . ">" . $milestone_title['milestone_title'] . "</option>";
        }
        echo json_encode(array('option' => $html));
    }
    public function actiongetparenttask()
    {
        $tblpx = Yii::app()->db->tablePrefix;

        $data = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks WHERE  tskid=" . $_REQUEST['task_sub_task_id'] . "")->queryAll();
        $data = CHtml::listData($data, 'tskid', 'title');

        $html = "";


        foreach ($data as $value => $title) {
            $html .= "<option value=" . $value . ">" . $title . "</option>";
        }
        echo json_encode(array('option' => $html));
    }

    public function actionsearchSession()
    {
        // $project_id = $_POST['monthly_task_project_id'];
        // $task_owner = $_POST['monthly_task_owner'];
        // $task_coordinator = $_POST['monthly_task_coordinator'];
        // $task_priority = $_POST['monthly_task_priority'];
        // if ($project_id != "") {
        //     Yii::app()->session['monthly_task_project_id'] = $project_id;
        // }
        // if ($task_owner != "") {
        //     Yii::app()->session['monthly_task_owner'] = $task_owner;
        // }
        // if ($task_coordinator != "") {
        //     Yii::app()->session['monthly_task_coordinator'] = $task_coordinator;
        // }
        // if ($task_priority != "") {
        //     Yii::app()->session['monthly_task_priority'] = $task_priority;
        // }
    }
    public function actionunsetSession()
    {
        // unset(Yii::app()->session['monthly_task_project_id']);
        // unset(Yii::app()->session['monthly_task_owner']);
        // unset(Yii::app()->session['monthly_task_coordinator']);
        // unset(Yii::app()->session['monthly_task_priority']);
        // Yii::app()->session['monthly_task_project_id'] = null;
        // Yii::app()->session['monthly_task_owner'] = null;
        // Yii::app()->session['monthly_task_coordinator'] = null;
        // Yii::app()->session['monthly_task_priority'] = null;
    }

    public function actionSendDailyTaskNotificationCron()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data_array = Yii::app()->db->createCommand("select assigned_to FROM {$tblpx}tasks where assigned_to!='' group by assigned_to")->queryAll();
        $app_root = YiiBase::getPathOfAlias('webroot');
        $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
        $logo = $theme_asset_url . 'default-logo.png';
        if (file_exists($app_root . "/themes/assets/logo.png")) {
            $logo = $theme_asset_url . 'logo.png';
        }
        foreach ($data_array as $data) {
            $mytasks = $this->assignedtasks($data['assigned_to']);
            $mailids = $this->gettomailds($mytasks);
            $milestones = $this->gettaskmilestones($mytasks);
            $upcoming_tasks = $this->getupcomingtasks($data['assigned_to']);
            $delayed_tasks = $this->getdelayedtasks($data['assigned_to']);
            $model2 = new MailLog;
            $today = date('Y-m-d H:i:s');
            $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
            $mail_data = $this->renderPartial('_daily_task_notification', array('mytasks' => $mytasks, 'milestones' => $milestones, 'upcoming_tasks' => $upcoming_tasks, 'delayed_tasks' => $delayed_tasks, 'logo' => $logo), true);

            //echo '<pre>';print_r($mail_data);exit;
            $mail = new JPhpMailer();
            $headers = "Hostech";
            $bodyContent = $mail_data;
            $mail->IsSMTP();
            $mail->Host = $mail_model['smtp_host'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
            $mail->Subject = "Daily PMS Notification";
            $to_mailids = '';
            if (!empty($mailids)) {
                $to_mailids = implode(',', $mailids);
                foreach ($mailids as $value) {
                    $mail->addAddress($value);
                }
            }
            $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
            $dataJSON = json_encode($data);
            $mail_send_byJSON = json_encode($mail_send_by);
            $subject = $mail->Subject;
            $model2->send_to = $to_mailids;
            $model2->send_by = $mail_send_byJSON;
            $model2->send_date = $today;
            $model2->message = htmlentities($bodyContent);
            $model2->description = $mail->Subject;
            $model2->created_date = $today;
            $model2->created_by = Yii::app()->user->getId();
            $model2->mail_type = $subject;
            $mail->isHTML(true);
            $mail->MsgHTML($bodyContent);
            $mail->Body = $bodyContent;
            if (!empty($mailids)) {
                if ($mail->Send()) {
                    $model2->sendemail_status = 1;
                } else {
                    $model2->notsent_reason = 'Technical Error';
                    $model2->sendemail_status = 0;
                }
            } else {
                $model2->sendemail_status = 0;
                $model2->notsent_reason = 'Recipient mail id empty';
            }
            $model2->save();
        }
    }

    function assignedTasks($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data_array = Yii::app()->db->createCommand("select t.*,u.email as assignedemail,cu.email as cordinatoremail,p.name as project_name,s.caption,s.sid FROM {$tblpx}tasks t left join {$tblpx}users u on u.userid = t.assigned_to left join {$tblpx}users cu on cu.userid = t.coordinator  left join {$tblpx}projects p on p.pid = t.project_id left join {$tblpx}status s on s.sid = t.status where t.assigned_to ='" . $id . "' and DATE(NOW()) BETWEEN t.start_date and t.due_date")->queryAll();
        return $data_array;
    }

    function getTaskMilestones($mytasks)
    {
        $milestoneArray = array();
        $tblpx = Yii::app()->db->tablePrefix;
        foreach ($mytasks as $key => $entry) {
            if (!in_array($entry['milestone_id'], $milestoneArray)) {
                array_push($milestoneArray, $entry['milestone_id']);
            }
        }
        $milestone_ids = "'" . implode("','", $milestoneArray) . "'";
        $data_array = Yii::app()->db->createCommand("select m.*,p.name as project_name FROM {$tblpx}milestone m left join {$tblpx}projects p on p.pid = m.project_id  where id IN($milestone_ids) AND  DATE(NOW()) BETWEEN m.start_date and m.end_date")->queryAll();
        return $data_array;
    }

    function getUpcomingTasks($id)
    {
        $milestoneArray = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $date = date('Y-m-d');
        $weekOfdays = array();
        for ($i = 1; $i <= 5; $i++) {
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
            $weekOfdays[] = date('Y-m-d', strtotime($date));
        }
        $start_date = $weekOfdays[0];
        $end_date = $weekOfdays[4];
        $data_array = Yii::app()->db->createCommand("select t.*,p.name as project_name FROM {$tblpx}tasks t left join  {$tblpx}projects p on p.pid = t.project_id where t.assigned_to='" . $id . "' AND t.start_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' ")->queryAll();
        return $data_array;
    }

    function getToMailds($mytasks)
    {
        $mailids = array();
        foreach ($mytasks as $key => $entry) {
            if ($entry['assignedemail']) {
                array_push($mailids, $entry['assignedemail']);
            }
            if ($entry['cordinatoremail']) {
                array_push($mailids, $entry['cordinatoremail']);
            }
        }
        $ids = array_unique($mailids);
        return $ids;
    }
    function getDelayedTasks($assigned_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data_array = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks where assigned_to ='" . $assigned_id . "' and quantity !='' AND  DATE(NOW()) BETWEEN start_date and due_date")->queryAll();
        $current_date = date('Y-m-d');
        $delayed_array = array();
        foreach ($data_array as $value) {
            $perday_percentage = ($value['daily_target'] / $value['quantity']) * 100;
            $day_duration = $this->calcdurationnotification($value['start_date'], $current_date, $value['project_id'], $value['clientsite_id']);
            $progress_data = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $value['tskid'] . "' AND approve_status = 1")->queryRow();
            $progress_percentage = isset($progress_data['work_progress']) ? $progress_data['work_progress'] : 0;
            $uptotodaypercentage = $day_duration * $perday_percentage;
            if ($progress_percentage < $uptotodaypercentage) {
                array_push($delayed_array, $value);
            }
        }
        return $delayed_array;
    }
    function getDelayedTasksStatus($taskid, $dailytarget, $quantity, $project_id, $clientsite_id, $start_date)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $current_date = date('Y-m-d');
        $perday_percentage = ($dailytarget / $quantity) * 100;
        $day_duration = $this->calcdurationnotification($start_date, $current_date, $project_id, $clientsite_id);
        $progress_data = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $taskid . "' AND approve_status = 1")->queryRow();
        $progress_percentage = isset($progress_data['work_progress']) ? $progress_data['work_progress'] : 0;
        $uptotodaypercentage = $day_duration * $perday_percentage;
        if ($progress_percentage < $uptotodaypercentage) {
            return 'Delayed';
        } else {
            return 'On Schedule';
        }
    }

    public function CalcDurationNotification($start_date, $due_date, $project_id, $site_id)
    {

        if (isset($start_date) && isset($due_date)) {
            $project = Projects::model()->findByPK($project_id);
            $site_calender = $project->site_calendar_id;
            if ($site_calender != "") {
                $get_wworking_days = $this->getWorkingDays($start_date, $due_date, $site_calender);
                return $get_wworking_days;
            } else {
                $duration = 0;
                if (isset($start_date) && isset($due_date)) {
                    if (empty($site_id) || $site_id == 1) {
                        $Criteria = new CDbCriteria();
                        $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                        $Criteria->addCondition('site_id = 1');
                        $Criteria->addCondition('status = 0');
                        $events = CalenderEvents::model()->findAll($Criteria);
                        $event_array = array();
                        foreach ($events as $event) {
                            array_push($event_array, $event->event_date);
                        }
                    } else {
                        $Criteria = new CDbCriteria();
                        $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                        $Criteria->addCondition('site_id = ' . $site_id);
                        $Criteria->addCondition('status = 1');
                        $events = CalenderEvents::model()->findAll($Criteria);
                        $event_array = array();
                        foreach ($events as $event) {
                            array_push($event_array, $event->event_date);
                        }
                    }
                    $start_date_data = new DateTime($start_date);
                    $due_date_data = new DateTime($due_date);
                    $due_date_data->modify('+1 day');
                    $period = new DatePeriod(
                        $start_date_data,
                        new DateInterval('P1D'),
                        $due_date_data
                    );
                    foreach ($period as $key => $value) {
                        if (!in_array($value->format('Y-m-d'), $event_array)) {
                            $duration++;
                        }
                    }
                    return $duration;
                } else {
                }
            } //site calendar not assigned to project
        }
    }

    public function actiongetTaskData()
    {

        if (!empty($_REQUEST['project_id'])) {
            $tblpx = Yii::app()->db->tablePrefix;
            if (isset($_REQUEST['project_id'])) {
                $pid = $_REQUEST['project_id'];
                $projects = Projects::model()->findByPk($pid);
                $start_date = date('d-M-y', strtotime($projects->start_date));
                $end_date = date('d-M-y', strtotime($projects->end_date));
            } else {
                $start_date = "";
                $end_date = "";
            }
            if (!empty($_REQUEST['task_type'])) {
                $task_type = $_REQUEST['task_type'];
            } else {
                $task_type = 1;
            }
            $condition1 = '';
            $task_id = Yii::app()->session['parent_task_id'];
            if (Yii::app()->user->role != 1) {
                $condition1 = 'WHERE project_id = ' . $pid . 'AND tskid= ' . $task_id . ' AND task_type = ' . $task_type . ' AND (assigned_to =' . Yii::app()->user->id . ' or coordinator=' . Yii::app()->user->id . ' or created_by=' . Yii::app()->user->id . ')';
            } else {
                $condition1 = 'WHERE project_id = ' . $pid . ' AND task_type = ' . $task_type . ' AND tskid= ' . $task_id;
            }
            $data = Yii::app()->db->createCommand("select * FROM {$tblpx}tasks " . $condition1)->queryAll();
            $data = CHtml::listData($data, 'tskid', 'title');
            //$html = "<option value=''>Select Main Task</option>";
            $html = "";
            foreach ($data as $value => $site_name) {
                $html = "<option value=" . $value . ">" . $site_name . "</option>";
                //echo CHtml::tag('option', array('value'=>$value),CHtml::encode($site_name),true);
            }
            $milestone_condition = "status = 1 AND project_id = " . $pid;
            $milestone_data = Milestone::model()->findAll(
                array(
                    'condition' => $milestone_condition,
                    'order' => 'milestone_title ASC',
                    'distinct' => true
                )
            );
            $milestone_result = CHtml::listData($milestone_data, 'id', 'milestone_title');
            $milestone_html = "<option value=''>Choose a Milestone</option>";

            foreach ($milestone_result as $milestone_key => $milestone_value) {
                $milestone_html .= "<option value=" . $milestone_key . ">" . $milestone_value . "</option>";
            }
            $area_html = $this->getAreas($pid);
            $work_types_html = $this->getProjectWorkTypes($pid);
            echo json_encode(array('option' => $html, 'milestones' => $milestone_html, 'start_date' => $start_date, 'end_date' => $end_date, 'areas' => $area_html, 'work_types_html' => $work_types_html));
        } else {
            $html = "<option value=''>Select Main Task</option>";
            $start_date = "";
            $end_date = "";
            echo json_encode(array('option' => $html, 'start_date' => $start_date, 'end_date' => $end_date));
        }
    }

    public function actionunsetTaskSession()
    {
        unset(Yii::app()->session['parent_task_id']);
    }

    public function actioncreateexpire()
    {
        $model = new TimeEntry;
        $this->layout = false;
        $task_id = isset($_POST['taskid']) ? $_POST['taskid'] : '';
        $model->tskid = $task_id;
        $this->render(
            'createxpire',
            array(
                'model' => $model,
                'type' => 0,
                'task_id' => $task_id
            )
        );
    }


    public function actionCreateTimeEntry()
    {
        $model = new TimeEntry;
        $this->layout = false;
        $task_id = isset($_POST['taskid']) ? $_POST['taskid'] : '';
        $model->tskid = $task_id;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TimeEntry'])) {
            $model->attributes = $_POST['TimeEntry'];
            if (Yii::app()->user->role > 2)
                $model->user_id = Yii::app()->user->id;

            $model->created_by = $model->updated_by = Yii::app()->user->id;
            $model->created_date = $model->updated_date = date("Y-m-d H:i:s");
            $model->entry_date = date('Y-m-d', strtotime($model->entry_date));
            $model->work_type = '';
            $start_time = strtotime(date('Y-m-d ') . $model->start_time . ":00");
            $end_time = strtotime(date('Y-m-d ') . $model->end_time . ":00");
            $starting_time = new DateTime($model->start_time);
            $ending_time = new DateTime($model->end_time);
            $since_start = $starting_time->diff($ending_time);
            $hours_in_minutes = $since_start->h * 60;
            $minutes_only = $since_start->i;
            $total_minutes = $hours_in_minutes + $minutes_only;
            $hours = floor($total_minutes / 60) . ':' . ($total_minutes - floor($total_minutes / 60) * 60);
            $model->hours = $hours;
            if ($model->save()) {

                Yii::app()->user->setFlash('success', 'Successfully created.');
                $this->redirect(array('tasks/view', 'id' => $model->tskid));
            }
        }
    }

    public function actiongettasks()
    {
        $userid = $_REQUEST['userid'];
        $taskid = $_REQUEST['taskid'];
        if (empty($userid)) {
            $userid = Yii::app()->user->id;
        }
        $where = "  AND 1=1";
        if ($taskid != "") {
            $where = " tskid =" . $taskid . "";
        }
        $tasks = Tasks::model()->findAll(array("condition" => "" . $where . "", 'order' => 'title ASC'));
        $html = "";
        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $html .= "<option value=" . $task['tskid'] . ">" . $task['title'] . "</option>";
            }
            echo json_encode(array('option' => $html));
        } else {
            $html = "<option value=''>Select task</option>";
            echo json_encode(array('id' => "", 'option' => $html));
        }
    }
    public function actionUpdateExpire($id)
    {
        $type = 0;
        $model = TimeEntry::model()->findByPk($id);
        $this->layout = false;
        $this->render(
            'updateexpire',
            array(
                'model' => $model,
                'type' => $type
            )
        );
    }

    public function actioncreateexpireexternaltask()
    {
    }
    public function actiongetAssignedUser()
    {

        if (!empty($_REQUEST['project_id'])) {
            $action = $_REQUEST['action'];
            if (Yii::app()->user->role != 1) {
                $gpmem = array();
                $allprojects = Tasks::model()->findAll('created_by = ' . Yii::app()->user->id . ' OR assigned_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id);

                if (!empty($allprojects)) {
                    foreach ($allprojects as $key => $value) {
                        if ($value['assigned_to'] != '') {
                            $gpmem[] = $value['assigned_to'];
                        }
                    }
                }

                $project_assigned_users = Projects::model()->findByPk($_REQUEST['project_id']);

                if ($project_assigned_users->assigned_to != "") {
                    $gpmem = explode(',', $project_assigned_users->assigned_to);
                }

                $con = '1=1';

                if (Yii::app()->user->role == 2 || Yii::app()->user->role == 4) {
                    $members = Groups::model()->findAll(
                        array(
                            'select' => 'group_id',
                            'condition' => 'group_lead=' . Yii::app()->user->id,
                        )
                    );
                    foreach ($members as $data) {
                        $list = Groups_members::model()->findAll(
                            array(
                                'select' => 'group_id,group_members,id',
                                'condition' => 'group_id=' . $data['group_id'],
                            )
                        );
                        foreach ($list as $groupsmem) {
                            $gpmem[] = $groupsmem['group_members'];
                        }
                    }
                    $gpmem[] = Yii::app()->user->id;

                    $gpmem = implode(',', $gpmem);
                    $con = "userid in(" . $gpmem . ")";
                } else {
                    $con = "userid = " . Yii::app()->user->id;
                }

                if ($action == "create") {
                    $condition = "status=0 AND $con";
                } else {
                    $condition = "$con";
                }
                $user_data = Users::model()->findAll(
                    array(
                        'condition' => $condition,
                        'select' => 'userid,CONCAT_ws(" ",first_name,last_name) as full_name',
                        'order' => 'first_name ASC',
                        'distinct' => true
                    )
                );

                $user_result = CHtml::listData($user_data, 'userid', 'full_name');

                $html = "<option value=''>Choose a resource</option>";

                foreach ($user_result as $key => $value) {
                    $html .= "<option value=" . $key . ">" . $value . "</option>";
                }
                echo json_encode(array('option' => $html));
            }
        }
    }
    public function actioncreateTask()
    {

        $model = new Tasks;
        $model->setscenario('create');
        $location = array();
        $assigned_to = array();
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tasks'])) {



            $model->attributes = $_POST['Tasks'];
            if (!empty($_POST['Tasks']['unit'])) {
                $model->unit = $_POST['Tasks']['unit'];
            }
            $model->start_date = date('Y-m-d', strtotime($_POST['Tasks']['start_date']));
            $model->due_date = date('Y-m-d', strtotime($_POST['Tasks']['due_date']));
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->work_type_id = $_POST['Tasks']['work_type_id'];
            $model->allowed_workers = $_POST['Tasks']['allowed_workers'];
            //new parent task id
            $model->parent_tskid = $_POST['Tasks']['parent_tskid'];
            $model->email = $_POST['Tasks']['email'];
            $model->task_type = $_POST['Tasks']['task_type'];

            if ($model->task_type == 2) {
                if (isset($_POST['ganttchart']) && $_POST['ganttchart'] == 1) {
                    $model->gantt_chart_status = 1;
                } else {
                    $model->gantt_chart_status = 0;
                }
                if (isset($_POST['project_schedule']) && $_POST['project_schedule'] == 1) {
                    $model->project_schedule_status = 1;
                } else {
                    $model->project_schedule_status = 0;
                }
            }

            if ($model->task_type == 1 && $model->parent_tskid == "") {

                if (isset($_POST['Tasks']['wpr_status']) && $_POST['Tasks']['wpr_status'] == 1) {
                    $model->wpr_status = 0;
                } else {
                    $model->wpr_status = 1;
                }

            }




            if ($model->save()) {
                if (isset($_POST['item_qty']) && count($_POST['item_qty']) > 0) {
                    $this->addItemEstimation($model->tskid, $model->project_id, $_POST['item_qty'], $_POST['item_name']);
                }
                $this->dependancyBasedDate($_POST, $model);
                //$this->milestonedatechange($model);
                // project session
                if (yii::app()->user->id == $_POST['Tasks']['assigned_to']) {
                    Yii::app()->user->setState('project_id', $_POST['Tasks']['project_id']);
                }
                $this->createTaskMail($model->tskid); //send mail
                Yii::app()->user->setFlash('success', 'Successfully created.');

                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {

                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    if (isset($_GET['project'])) {
                        if ($monthlyTask = Yii::app()->session['current_url']) {
                            $urlComponents = parse_url($monthlyTask);
                            parse_str($urlComponents['query'], $queryParams);
                            $monthlyTask = isset($queryParams['monthly_task_page']) ? $monthlyTask : $monthlyTask . '&' . http_build_query(['monthly_task_page' => 1]);
                            $this->redirect($monthlyTask);
                        } else {
                            $this->redirect(array('monthlyTask'));
                        }
                    } else {
                        $this->redirect(array('index2', 'id' => $model->tskid));
                    }

                }
            }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])) {

            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------



        // for monthly task starts


        $task_project_id = "";
        $task_milestone_id = "";
        $task_parent_task_id = "";
        $task_sub_task_id = "";
        $task_budget_head_id = "";
        $parent_task_type = "";
        if (isset($_GET['project'])) {
            $project_name = $_GET['project'];
            $get_project = Projects::model()->findByAttributes(array('name' => $project_name));
            $task_project_id = $get_project->pid;
        }
        if (isset($_GET['budget_head'])) {
            $budget_head = $_GET['budget_head'];
            $get_budget_head = BudgetHead::model()->findByAttributes(array('budget_head_title' => $budget_head));
            if ($get_budget_head) {
                $task_budget_head_id = $get_budget_head->budget_head_id;
            }

        }

        if (isset($_GET['milestone_id'])) {
            $task_milestone_id = $_GET['milestone_id'];
        }
        if (isset($_GET['parent_task_id'])) {
            $task_parent_task_id = $_GET['parent_task_id'];
            Yii::app()->session['parent_task_id'] = $task_parent_task_id;
            $tasks = Tasks::model()->findByPk($task_parent_task_id);
            $parent_task_type = $tasks->task_type;
        }
        if (isset($_GET['sub_task_id'])) {

            $task_sub_task_id = $_GET['sub_task_id'];

            Yii::app()->session['parent_task_id'] = $task_sub_task_id;
            $tasks = Tasks::model()->findByPk($task_sub_task_id);
            $parent_task_type = $tasks->task_type;

        }

        // for monthly task end



        $unit_list = Unit::model()->findAll(['condition' => 'status = 1']);
        $template = Template::model()->findAll();
        $this->render(
            'create_task',
            array(
                'model' => $model,
                'location' => $location,
                'assigned_to' => $assigned_to,
                'unit_list' => $unit_list,
                'template' => $template,
                'task_project_id' => $task_project_id,
                'task_budget_head_id' => $task_budget_head_id,
                'task_milestone_id' => $task_milestone_id,
                'task_parent_task_id' => $task_parent_task_id,
                'task_sub_task_id' => $task_sub_task_id,
                'parent_task_type' => $parent_task_type
            )
        );
    }
    public function actioncreateColumnSession()
    {
        $session = Yii::app()->session;
        $classNames = Yii::app()->request->getPost('options', array());
        $session['monthly_task_table_class'] = $classNames;
        echo json_encode(['success' => 'success']);
        Yii::app()->end();

    }
    public function actioncreateExpandSession()
    {
        $session = Yii::app()->session;
        $expandedTargets = Yii::app()->request->getPost('expandedTargets', array());
        $currentUrl = Yii::app()->request->getPost('currentUrl', array());
        $session['current_url'] = $currentUrl;

        $session['expanded_targets'] = $expandedTargets;
        echo json_encode(['success' => 'success']);
        Yii::app()->end();
    }
    public function actiongetHighestRank($milestone_id)
    {
        $highest_ranking = Tasks::model()->find(
            array(
                'select' => 'ranking',
                'condition' => 'milestone_id = :milestone_id',
                'params' => array(':milestone_id' => $milestone_id),
                'order' => 'ranking DESC',
            )
        );

        if ($highest_ranking !== null) {
            $rank = $highest_ranking->ranking;
        } else {
            $rank = 0;
        }
        return $rank+1;
       
    }

}
