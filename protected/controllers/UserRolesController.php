<?php

class UserRolesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	 public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = '//layouts/iframe';
		$model=new UserRoles;

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['UserRoles']))
		{
			$model->attributes=$_POST['UserRoles'];
			if($model->save()){
				Yii::app()->user->setFlash('success', 'Successfully Created.');
				echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
			}				 
		}

		$this->render('create',array(
			'model'=>$model,
		));

		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = '//layouts/iframe';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['UserRoles']))
		{
			$model->attributes=$_POST['UserRoles'];
			if($model->save()){
                             echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog-edit').dialog('close');window.parent.$('#cru-frame-edit').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                        }
				//$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));


	
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
//	public function actionDelete($id)
//	{
//		$this->loadModel($id)->delete();
//
//		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if(!isset($_GET['ajax']))
//			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new UserRoles('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UserRoles']))
			$model->attributes=$_GET['UserRoles'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=UserRoles::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-roles-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actiondelete($id)
	{
        
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();

        try {

            $role_used = Users::model()->findByAttributes(array(
                'user_type' => $id,

            ));

            if ($role_used) {
                $success_status = 2;
            } else {
                if (!$model->delete()) {
                    $success_status = 0;
                    throw new Exception(json_encode($model->getErrors()));
                } else {
                    $success_status = 1;
                }
            }


            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else if ($success_status == 2) {
                echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
            } else {
                if ($error->errorInfo[1] == 1451) {
					
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {

                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }
}
