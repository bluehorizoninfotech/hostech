<?php

class ClientsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $accessArr = array();
    public $accessauthArr = array();
    public $accessguestArr = array();
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewpartial($id) {
        $this->renderPartial('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionView1($id) {
        if (Yii::app()->request->isAjaxRequest) {
            //outputProcessing = true because including css-files ...
            $this->renderPartial('view', array(
                'model' => $this->loadModel($id),
                    ), false, true);
            //js-code to open the dialog    
            if (!empty($_GET['asDialog']))
                echo CHtml::script('$("#dlg-clients-view").dialog("open")');
            Yii::app()->end();
        } else
            $listproject = Projects::model()->findAllByAttributes(array(
                'client_id' => $id,
            ));

        $this->render('view', array(
            'model' => $this->loadModel($id), 'getdata' => $listproject, 
        ));
    }
  public function actionView($id) {
//        	if ( isset( $_GET[ 'pageSizes' ] ) )
//{
//Yii::app()->user->setState( 'pageSizes', (int) $_GET[ 'pageSizes' ] );
//unset( $_GET[ 'pageSizes' ] );
//} 

  
        if (Yii::app()->request->isAjaxRequest) {
            //outputProcessing = true because including css-files ...
            $this->renderPartial('view', array(
                'model' => $this->loadModel($id),
                    ), false, true);
            //js-code to open the dialog    
            if (!empty($_GET['asDialog']))
                echo CHtml::script('$("#dlg-clients-view").dialog("open")');
            Yii::app()->end();
        } else
        $Criteria = new CDbCriteria();
        $Criteria->select = "*";

        $Criteria->compare('clntid', $id);
        $Criteria->group = 'pjctid';
        $count_deal = PaymentReport::model()->count($Criteria);


        $pages = new CPagination($count_deal);
        $pages->pageSize =5;
        $pages->applyLimit($Criteria);
        $listproject = PaymentReport::model()->findAll($Criteria);
        $this->render('view', array(
            'model' => $this->loadModel($id), 'getdata' => $listproject, 'pages' => $pages, 'clid' => $id
        ));
    }
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Clients;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);


        if (isset($_POST['Clients'])) {
            $model->attributes = $_POST['Clients'];

            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');

            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');

            if ($model->save(false))
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else
                  Yii::app()->user->setFlash('success', 'Successfully updated.');
                    $this->redirect(array('clients/index'));
            //----- end new code --------------------
            // $this->redirect(array('Viewpartial','cid'=>$model->Clients));
            //$this->redirect(array('view','id'=>$model->cid));
        }
//----- begin new code --------------------
        if (!empty($_GET['asDialog']))
            $this->layout = '//layouts/iframe';
        //----- end new code --------------------
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    /* public function actionUpdate($id)
      {
      $model=$this->loadModel($id);

      // Uncomment the following line if AJAX validation is needed
      $this->performAjaxValidation($model);

      if(isset($_POST['Clients']))
      {
      $model->attributes=$_POST['Clients'];

      $model->updated_by = yii::app()->user->id;
      $model->updated_date = date('Y-m-d');

      if($model->save())
      $this->redirect(array('view','id'=>$model->cid));
      }

      $this->render('update',array(
      'model'=>$model,
      ));
      }
     */


    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Clients'])) {
            $model->attributes = $_POST['Clients'];
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');

            if ($model->save())
            //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                }else{
                //----- end new code --------------------
                  //  $this->redirect(array('viewpartial', 'cid' => $model->cid));
                     Yii::app()->user->setFlash('success', 'Successfully updated.');
                    $this->redirect(array('clients/index'));
             }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog']))
            $this->layout = '//layouts/iframe';
        //----- end new code --------------------

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Clients('search');
      //  print_r($model);die;
        $model->unsetAttributes();  // clear any default values

         // new filter back and forth
        $class_name= get_class($model);
        if (isset($_GET[ $class_name])){
           Yii::app()->user->setState('prevfilter' , $_GET[$class_name]);
        }

        if(isset(Yii::app()->user->prevfilter)){
           $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 
        
        if (isset($_GET['Clients']))
            $model->attributes = $_GET['Clients'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Clients('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Clients']))
            $model->attributes = $_GET['Clients'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Clients::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    public function actionaddclient()
    {
        $model = new Clients;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Clients'])) {
            $model->attributes = $_POST['Clients'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
			
            if ($model->save())
			{
				 
            //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index', 'id' => $model->pid));
                }
        }
        }
         //----- begin new code --------------------
        if (!empty($_GET['asDialog'])){
         
            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------

        $this->render('create', array(
            'model' => $model,
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'clients-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionAutocomplete() { 
       $res =array();

      
      if (isset($_GET['term'])) { 
       
       $qtxt ="SELECT name FROM {{clients}} WHERE name LIKE :name"; $command =Yii::app()->db->createCommand($qtxt); $command->bindValue(":name", '%'.$_GET['term'].'%', PDO::PARAM_STR); 
       $res =$command->queryColumn();
     } 
       echo CJSON::encode($res); 
       Yii::app()->end(); 
    }

}
