<?php

Yii::app()->getModule('masters');
class TestController extends Controller
{

    public function actionAbcd()
    {
        $this->render('testchart');
    }
    public function actionCalenderdataold()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));
        $this->layout = 'false';
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-t');

        $project_id = $_POST['project_id'];
        //$project_id= 22;
        $milestone_value = '';
        $tasks = array();
        $project_tasks = Tasks::model()->findAll(['condition' => 'trash =1 AND task_type = 1 AND project_id = ' . $project_id]);
        $i = 1;
        foreach ($project_tasks as $task) {


            if ($i == 1) {
                $start_date = date('Y-m-01', strtotime($task['start_date']));
                $end_date = date('Y-m-t', strtotime($task['due_date']));
            }
            if (strtotime($end_date) < strtotime($task['due_date'])) {
                $end_date = date('Y-m-t', strtotime($task['due_date']));
            }
            if (strtotime($task['start_date']) < strtotime($start_date)) {
                $start_date = date('Y-m-01', strtotime($task['start_date']));
            }

            $arr = array(
                "id" => $task['tskid'], "parent_id" => $task['parent_tskid'],
                "title" => $task['title'], 'start_date' => $task['start_date'], 'due_date' => $task['due_date'],

                'task_status' => $task['status'], 'mile_stone_id' => $task['milestone_id']
            );

            array_push($tasks, $arr);
        }




        $this->render('testcalendertable', array('tasks' => $tasks, 'start_date' => $start_date, 'end_date' => $end_date));
    }



    protected function taskprogressnotification($task, $time_entry_array = false)
    {
        $days_gone = 0;
        /* Event array getting starts */
        if (!empty($task['clientsite_id'])) {
            $site_id = $task['clientsite_id'];
            $status = 1;
        } else {
            $site_id = 1;
            $status = 0;
        }
        $Criteria = new CDbCriteria();
        $Criteria->addBetweenCondition("event_date", $task['start_date'], $task['due_date'], 'AND');
        $Criteria->addCondition('site_id = ' . $site_id);
        $Criteria->addCondition('status = ' . $status);
        $events = CalenderEvents::model()->findAll($Criteria);
        $event_array = array();
        foreach ($events as $event) {
            array_push($event_array, $event->event_date);
        }
        /* Events array ends */
        $daily_target = $task['daily_target'];
        $per_day_percenatge = 0;
        if (!empty($task['quantity'])) {
            $per_day_percenatge = ($daily_target / $task['quantity']) * 100;
        }



        $startDate = strtotime($task['start_date']);
        $endDate = strtotime($task['due_date']);
        $start = new DateTime($task['start_date']);
        $end = new DateTime($task['due_date']);
        $today_ = new DateTime(date('Y-m-d'));
        $interval = DateInterval::createFromDateString('1 day');
        $end->setTime(0, 0, 1);
        $today_->setTime(0, 0, 1);
        $period = new DatePeriod($start, $interval, $end);
        $period_today = new DatePeriod($start, $interval, $today_);
        $workingdays = 0;
        $days_gone = 0;
        if ($startDate > strtotime(date('Y-m-d'))) {
            $todays_expected_percentage = 0;
        } else {
            if ($start == $end) {
                $workingdays = 1;
                $days_gone = 1;
                $todays_expected_percentage = 100;
            } else {
                $days_gone = 0;
            }
        }

        $result_data = array();

        foreach ($period as $day) {

            /* days gone finding start */
            if ($days_gone == 0) {
                $start_date_data = new DateTime($task['start_date']);
                $todays_date = new DateTime($day->format('Y-m-d'));
                $todays_date->modify('+1 day');
                $period_dates = new DatePeriod(
                    $start_date_data,
                    new DateInterval('P1D'),
                    $todays_date
                );

                foreach ($period_dates as $key => $value) {
                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                        $days_gone++;
                    }
                }
                $todays_expected_percentage = $days_gone * $per_day_percenatge;
            }
            /* days gone finding ends */




            if ($endDate < strtotime(date('Y-m-d'))) {
                $today_ = new DateTime($day->format('Y-m-d'));
                $period_today = new DatePeriod($start, $interval, $today_);
                $days_gone = 1;
                foreach ($period_today as $day_) {

                    // $day is not saturday nor sunday
                    if (!in_array($day_->format('w'), [0, 6])) {
                        $days_gone++;
                    }
                }

                $tolerance_percentage = 100 / $days_gone;

                $todays_expected_percentage = ($days_gone * $tolerance_percentage) - $tolerance_percentage;
            }
            $entry_check_date = $day->format('Y-m-d');
            $progress_data = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $task['tskid'] . "' AND approve_status = 1 AND date = '" . $entry_check_date . "'")->queryRow();

            if (!empty($progress_data['work_progress'])) {
                $data = $this->addprogressnotification($task, $todays_expected_percentage, $progress_data, $entry_check_date);
            } else {
                $data = $this->addprogressnotification($task, $todays_expected_percentage, 0, $entry_check_date);
            }

            $result_data[$entry_check_date] = $data;
        }

        return $result_data;
    }

    protected function addprogressnotification($task, $todays_expected_percentage, $progress_data, $entry_check_date)
    {


        if ($progress_data['work_progress'] != 0 && !empty($progress_data['work_progress'])) {
            if ($progress_data['work_progress'] < $todays_expected_percentage) {
                $data = [
                    'id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => number_format($progress_data['work_progress'], 2),
                    'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'],
                    'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'percent_date' => $entry_check_date
                ];
                return $data;
            } else {
                $data = [
                    'id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => number_format($progress_data['work_progress'], 2),
                    'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'],
                    'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'percent_date' => $entry_check_date
                ];
                return $data;
            }
        } else {
            $data = [
                'id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => 0,
                'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'],
                'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'percent_date' => $entry_check_date
            ];
            return $data;
        }
    }

    protected function buildTree(array $elements, $parentId = 0)
    {


        $branch = array();
        foreach ($elements as $element) {
            if (isset($element['parent_id'])) {
                if ($element['id'] == $element['parent_id'])
                    $element['parent_id'] = NULL;
                if ($element['parent_id'] == $parentId) {
                    $children = $this->buildTree($elements, $element['id']);
                    if ($children) {

                        $element['children'] = $children;
                    }
                    $branch[] = $element;
                }
            }
        }
        return $branch;
    }
    public function add_date($orgDate, $date)
    {
        $cd = strtotime($orgDate);
        $retDAY = date('d/m/Y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $date, date('Y', $cd)));
        return $retDAY;
    }
    public function getcountBetween2Dates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return count($days);
    }
    public function getDatesBetween2Dates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    public function actionCalenderdata()
    {
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));
        $this->layout = 'false';

        $project_id = 22;
        $project_tasks = Tasks::model()->findAll(['condition' => 'trash =1 AND task_type = 1 AND project_id = ' . $project_id]);
        $tasks = array();
        $i = 1;
        $count = 1;
        $result_array = [];
        $milestone_value = '';
        $task_array = [];
        $task_details = [];
        foreach ($project_tasks as $task) {
            if ($task['parent_tskid'] == NULL) {
                if (!empty($task['milestone_id'])) {
                    $milestone_value = $task->milestone->milestone_title;
                }
            }

            if ($i == 1) {
                $start_date = date('Y-m-01', strtotime($task['start_date']));
                $end_date = date('Y-m-t', strtotime($task['due_date']));
            }
            if (strtotime($end_date) < strtotime($task['due_date'])) {
                $end_date = date('Y-m-t', strtotime($task['due_date']));
            }
            if (strtotime($task['start_date']) < strtotime($start_date)) {
                $start_date = date('Y-m-01', strtotime($task['start_date']));
            }

            $task_array[] = $task->title;

            $task_details[] = array(
                'id' => $task->tskid,
                'title' => $task->title,
                'start_date' => $task->start_date,
                'due_date' => $task->due_date,
                'daily_target' => $task->daily_target,
                'quantity' => $task->quantity,
                'assigned_to' => $task->assigned_to,
                'clientsite_id' => $task->clientsite_id


            );
            Yii::app()->session['task_details'] = $task_details;
        }


        $this->render('testcalendertable', array(
            'result_array' => $result_array, 'start_date' => $start_date, 'end_date' => $end_date, 'milestone_value' => $milestone_value,
            'task_array' => $task_array
        ));
    }


    public function actiontaskProgressDetails()
    {
        $task_details = Yii::app()->session['task_details'];

        $start_date = date('Y-m-d');
        $end_date = date('Y-m-t');

        foreach ($task_details as $task) {
            $start_date = $task['start_date'];
            $due_date = $task['due_date'];
            $time_entry_array = array();
            $task_work_status = $this->eachtaskprogressnotification($task['clientsite_id'], $start_date, $due_date, $task['daily_target'], $task['quantity'], $task['id']);
        }
    }

    public function eachtaskprogressnotification($clientsite_id, $start_date, $due_date, $daily_target, $quantity, $task_id)
    {
        $days_gone = 0;
        /* Event array getting starts */
        if (!empty($clientsite_id)) {
            $site_id = $clientsite_id;
            $status = 1;
        } else {
            $site_id = 1;
            $status = 0;
        }

        $Criteria = new CDbCriteria();
        $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
        $Criteria->addCondition('site_id = ' . $site_id);
        $Criteria->addCondition('status = ' . $status);
        $events = CalenderEvents::model()->findAll($Criteria);
        $event_array = array();
        foreach ($events as $event) {
            array_push($event_array, $event->event_date);
        }
        /* Events array ends */

        $daily_target = $daily_target;
        $per_day_percenatge = 0;
        if (!empty($quantity)) {
            $per_day_percenatge = ($daily_target / $quantity) * 100;
        }


        $startDate = strtotime($start_date);
        $endDate = strtotime($due_date);
        $start = new DateTime($start_date);
        $end = new DateTime($due_date);
        $today_ = new DateTime(date('Y-m-d'));
        $interval = DateInterval::createFromDateString('1 day');
        $end->setTime(0, 0, 1);
        $today_->setTime(0, 0, 1);
        $period = new DatePeriod($start, $interval, $end);
        $period_today = new DatePeriod($start, $interval, $today_);
        $workingdays = 0;
        $days_gone = 0;
        if ($startDate > strtotime(date('Y-m-d'))) {
            $todays_expected_percentage = 0;
        } else {
            if ($start == $end) {
                $workingdays = 1;
                $days_gone = 1;
                $todays_expected_percentage = 100;
            } else {
                $days_gone = 0;
            }
        }

        $result_data = array();

        foreach ($period as $day) {
            /* days gone finding start */
            if ($days_gone == 0) {
                $start_date_data = new DateTime($start_date);
                $todays_date = new DateTime($day->format('Y-m-d'));
                $todays_date->modify('+1 day');
                $period_dates = new DatePeriod(
                    $start_date_data,
                    new DateInterval('P1D'),
                    $todays_date
                );

                foreach ($period_dates as $key => $value) {
                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                        $days_gone++;
                    }
                }
                $todays_expected_percentage = $days_gone * $per_day_percenatge;
            }
            /* days gone finding ends */



            if ($endDate < strtotime(date('Y-m-d'))) {
                $today_ = new DateTime($day->format('Y-m-d'));
                $period_today = new DatePeriod($start, $interval, $today_);
                $days_gone = 1;
                foreach ($period_today as $day_) {

                    // $day is not saturday nor sunday
                    if (!in_array($day_->format('w'), [0, 6])) {
                        $days_gone++;
                    }
                }

                $tolerance_percentage = 100 / $days_gone;

                $todays_expected_percentage = ($days_gone * $tolerance_percentage) - $tolerance_percentage;
            }

            $entry_check_date = $day->format('Y-m-d');
            $progress_data = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $task_id . "' AND approve_status = 1 AND date = '" . $entry_check_date . "'")->queryRow();

            if (!empty($progress_data['work_progress'])) {
                $data = $this->eachaddprogressnotification($task, $todays_expected_percentage, $progress_data, $entry_check_date);
            } else {
                $data = $this->eachaddprogressnotification($task, $todays_expected_percentage, 0, $entry_check_date);
            }

            $result_data[$entry_check_date] = $data;
        }
    }
    public function eachaddprogressnotification()
    {
      if ($progress_data['work_progress'] != 0 && !empty($progress_data['work_progress'])) {
        if ($progress_data['work_progress'] < $todays_expected_percentage) {
            $data = [
                'id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => number_format($progress_data['work_progress'], 2),
                'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'],
                'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'percent_date' => $entry_check_date
            ];
            return $data;
        } else {
            $data = [
                'id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => number_format($progress_data['work_progress'], 2),
                'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'],
                'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'percent_date' => $entry_check_date
            ];
            return $data;
        }
    } else {
        $data = [
            'id' => $task['tskid'], 'title' => $task['title'], 'current_percentage' => 0,
            'required_percentage' => number_format($todays_expected_percentage, 2), 'due_date' => $task['due_date'],
            'assignee' => $task['assigned_to'], 'start_date' => $task['start_date'], 'percent_date' => $entry_check_date
        ];
        return $data;
    }
    }
}
