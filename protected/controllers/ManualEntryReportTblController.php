<?php

class ManualEntryReportTblController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->session['pmsmenuall'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuall'])) {
                $accessArr = Yii::app()->session['pmsmenuall'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuauth'])) {
                $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuguest'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuguest'])) {
                $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
            }
        }
        $access_privlg = count($accessauthArr);


        return array(
            array(
                'allow',
                'actions' => $accessArr,
                'users' => array('@'),
            //'expression' => "$access_privlg > 0",
            ),
            array(
                'allow',
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array('allow',
                'actions' => array('manualentryaction', 'Approved', 'Rejected', 'Pending', 'setSkillFilter','SetDesignationFilter'),
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionSetSkillFilter($skill_id, $ajax = 0) {
        if (intval($skill_id) == 0 and isset(Yii::app()->user->skill_filter_id)) {
            unset(Yii::app()->user->skill_filter_id);
        } else {
            Yii::app()->user->setState('skill_filter_id', $skill_id);
            $empmodel = Employee::model()->findAll(
                    array(
                        'select' => 'emp_id, concat_ws(" ",firstname,lastname) as firstname',
                        'condition' => 'skill_id=' . $skill_id,
            ));

            $empids = CHtml::listData($empmodel, 'emp_id', 'firstname');
            if (count($empids)) {
                Yii::app()->user->setState('skill_emp_ids', implode(',', array_keys($empids) ));
            } else if (isset(Yii::app()->user->skill_emp_ids)) {
                Yii::app()->user->skill_emp_ids=0;
            }
        }
        echo 1;
    }

    public function actionSetDesignationFilter($desig_id, $ajax = 0) {
        if (intval($desig_id) == 0 and isset(Yii::app()->user->desig_filter_id)) {
            unset(Yii::app()->user->desig_filter_id);
        } else {
            Yii::app()->user->setState('desig_filter_id', $desig_id);
            $empmodel = Employee::model()->findAll(
                    array(
                        'select' => 'emp_id, concat_ws(" ",firstname,lastname) as firstname',
                        'condition' => 'designation=' . $desig_id,
            ));

            $empids = CHtml::listData($empmodel, 'emp_id', 'firstname');
            if (count($empids)) {
                Yii::app()->user->setState('desig_emp_ids', implode(',', array_keys($empids) ));
            } else if (isset(Yii::app()->user->desig_emp_ids)) {
                Yii::app()->user->desig_emp_ids=0;
            }
        }
        echo 1;
    }
    
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new ManualEntryReportTbl;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ManualEntryReportTbl'])) {
            $model->attributes = $_POST['ManualEntryReportTbl'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->entry_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ManualEntryReportTbl'])) {
            $model->attributes = $_POST['ManualEntryReportTbl'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->entry_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        $model = new ManualEntryReportTbl('search');

        // page size drop down changed
        if (isset($_GET['reportpagesize'])) {
            // pageSize will be set on user's state
            Yii::app()->user->setState('reportpagesize', (int) $_GET['reportpagesize']);
            // unset the parameter as it
            // would interfere with pager
            // and repetitive page size change
            unset($_GET['reportpagesize']);
        }

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ManualEntryReportTbl']))
            $model->attributes = $_GET['ManualEntryReportTbl'];
        if ($model->status == '') {
            if (isset($_GET['type']) and $_GET['type'] != 'All') {
                $model->status = $_GET['type'];
            }
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function setIdVal($data) {
        return print_r($data, true);
    }
    
    public function checkvisible($data){
        return true;
        return print_r($data->status, true);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ManualEntryReportTbl('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ManualEntryReportTbl']))
            $model->attributes = $_GET['ManualEntryReportTbl'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ManualEntryReportTbl the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = ManualEntryReportTbl::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ManualEntryReportTbl $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'manual-entry-report-tbl-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
