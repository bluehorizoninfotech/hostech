<?php

class SiteController extends Controller
{

    /**
     * Declares class-based actions.
     */
    public $defaultAction = 'login';

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        if (!isset(Yii::app()->user->role))
            $this->redirect(array('/site/login'));

        $this->render('index');
    }
    public function actionTable()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        if (!isset(Yii::app()->user->role))
            $this->redirect(array('/site/login'));

        $this->render('tables');
    }

    /**
     * This is the action to handle external exceptions.
     */
    /* public function actionError() {
       
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    } */

    public function actionError()
    {

        if ($error = Yii::app()->errorHandler->error) {

            if ($error['type'] == "CDbException") {
                $this->layout = "//layouts/iframe";
                // die('Internal server error: '.$error['message']);
            }

            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else {
                if (!Yii::app()->user->mainuser_id) {
                    $this->render('error', $error);
                } else {

                    $this->render('error', $error);
                }
            }
        } else {
            $error = '404';
            $this->render('error', array('code' => $error, 'message' => 'Page Not Found!'));
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }



    public function actionLogin()
    {

        $this->menupermissions();

        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('tasks/mytask'));
        }

        $this->layout = "//layouts/login";
        $global_user_id =  NULL;
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_GET['switch_token']) && isset($_SERVER['HTTP_REFERER'])) {
            $switch_token = $_GET['switch_token'];
            if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) !== false) {
                $hostname = $_SERVER['HTTP_HOST'];
                $url_expiry_time = time() + (30 - time() % 30);
                $current_app = '/pms';
                $token_condition_in_db = "md5(concat('{$hostname}','{$current_app}', '{$url_expiry_time}', userid)) = '{$switch_token}'";
                $landing_user = Users::model()->find($token_condition_in_db);

                if ($landing_user !== null) {
                    $_POST['LoginForm'] = array();
                    if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
                        $global_user_id = $_POST['LoginForm']['global_user_id'] = $landing_user['global_user_id'];
                    }
                    $_POST['LoginForm']['username'] = $landing_user['username'];
                    $_POST['LoginForm']['password'] = $landing_user['password'];
                }
            }
        }
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
                $model->global_user_id = $global_user_id;
            }
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->menupermissions();
                //$this->redirect(Yii::app()->user->returnUrl);
                //$this->redirect(array('site/index2'));
                $general_settings_model = GeneralSettings::model()->find(['condition' => 'id = 1']);

                if ($general_settings_model->dashboard_type == 1) {
                    $this->redirect(array('site/dashboard'));
                } else {
                    $this->redirect(array('site/index2'));
                }
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }




    public function actionIndex2($data = false)
    {

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'      
        if (!isset(Yii::app()->user->role))
            $this->redirect(array('/site/login'));
        $projects = array();
        $client_projects_list = array();
        if (Yii::app()->user->role != 1) {
            $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
            if (isset($projects) && !empty($projects['client_id'])) {
                $client_projects_list =  Projects::model()->findAll(array('condition' => "client_id = " . $projects['client_id'] . " AND pid !=" . $projects['pid'] . ' AND FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to)'));
            }
        } else {
            if (isset(Yii::app()->user->project_id)) {
                if (Yii::app()->user->project_id != "") {
                    $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
                    if (isset($projects) && !empty($projects['client_id'])) {
                        $client_projects_list =  Projects::model()->findAll(array('condition' => "client_id = " . $projects['client_id'] . " AND pid !=" . $projects['pid']));
                    }
                }
            }
        }
        $tasks = new Tasks('assignedtask');
        $dataprovider = $tasks->assignedtask($type = '');
        $task_count = $dataprovider->getTotalItemCount();
        $model = new Tasks('search');
        $model->status = 75;
        if (!empty(Yii::app()->user->project_id)) {
            $model->project_id = Yii::app()->user->project_id;
        }
        $dataprovider = $model->search($type = 'unassigned');
        $unassigned_task_count = $dataprovider->getTotalItemCount();

        $today = date('Y-m-d');
        $criteria = new CDbCriteria;

        if (Yii::app()->user->role != 1) {
            $condition = Yii::app()->user->id . " IN(created_by,assigned_to,report_to,coordinator)";
            $criteria->addCondition($condition);
        }
        $criteria->addCondition('t.start_date >= "' . $today . '"');
        $criteria->addCondition('t.trash = 1');
        $tasks_to_start = count(Tasks::model()->findAll($criteria));
        $this->menupermissions();
        $this->render('index2', array('projects' => $projects, 'client_projects_list' => $client_projects_list, 'task_count' => $task_count, 'unassigned_task_count' => $unassigned_task_count, 'tasks_to_start' => $tasks_to_start));
    }


    /**
     * Displays the login page
     */
    public function actionLogin1()
    {


        $this->layout = '//layouts/login';

        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('site/index'));
        }

        $model = new LoginForm;
        //echo "<pre>";print_r($_POST);echo "</pre>";

        //print_r($_POST);
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        // collect user input data
        if (isset($_POST['LoginForm'])) {


            $model->attributes = $_POST['LoginForm'];

            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->menupermissions();
            $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->getBaseUrl(true));
    }


    /* test mail */
    public function actionTestemail()
    {
        /*
         $from = 'info@bhiapp.com';
         $toname ='Testing';
         $tomail = 'tony.xa@bluehorizoninfotech.com';
         $subject = 'test mail subject';
         $body ='test mail content';
            $name='=?UTF-8?B?'.base64_encode($toname).'?=';
                $subject='=?UTF-8?B?'.base64_encode($subject).'?=';
                $headers="From: $name <{$from}>\r\n".
                    "Reply-To: {$from}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/html; charset=UTF-8";
   
                    mail($tomail,$subject,$body,$headers);
        */
    }
    public function menupermissions()
    {
        $tbl = Yii::app()->db->tablePrefix;

        // set access rules in session for all users

        $session = new CHttpSession;
        $session->open();
        $user_id = Yii::app()->user->id;
        //$controller =  Yii::app()->controller->id;
        $ctrls = Yii::app()->db->createCommand("SELECT distinct(controller) as controller FROM {$tbl}menu WHERE status = 0 AND showmenu = 2 AND parent_id = 0")->queryAll();


        $newarr2 = array();
        $menualllist = array();
        $i = 0;
        foreach ($ctrls as $ctrl) {
            $controller = $ctrl['controller'];
            $menuitems_all = Yii::app()->db->createCommand("SELECT * FROM {$tbl}menu WHERE status = 0 AND showmenu = 2 AND parent_id != 0 AND controller = '$controller'")->queryAll();


            //$controller = lcfirst($controller);

            foreach ($menuitems_all as $menuall) {

                $ctrller = $menuall['controller'];
                $action = $menuall['action'];

                //echo $ctrller."--".$action;
                // if($ctrller == $controller){
                $controller = lcfirst($ctrller);
                $action = lcfirst($action);
                $newarr1[$controller][$i] = $action;

                $menuitem = '/' . $controller . '/' . $action;
                array_push($menualllist, $menuitem);

                // }
                $i++;
            }
        }
        //        echo "<pre>";
        //        print_r($menualllist);die;

        $session['menuall'] = $newarr1;
        $session['menualllist'] = $menualllist;
        // set access rules in session for all users \\//\\//\\//
        // set access rules in session for guest users
        if (Yii::app()->user->isGuest) {

            $newarr2 = array();
            $menuguestlist = array();
            $i = 0;
            foreach ($ctrls as $ctrl) {
                $controller = $ctrl['controller'];

                $menuitems_guest = Yii::app()->db->createCommand("SELECT * FROM {$tbl}menu_permissions mp LEFT JOIN {$tbl}menu me ON mp.menu_id = me.menu_id  WHERE me.status = 0 AND me.showmenu = 1 AND me.parent_id != 0 AND me.controller = '$controller'")->queryAll();

                foreach ($menuitems_guest as $menuall) {

                    $ctrller = $menuall['controller'];
                    $action = $menuall['action'];
                    //echo $ctrller."--".$action;
                    // if($ctrller == $controller){
                    $controller = lcfirst($ctrller);
                    $action = lcfirst($action);
                    $newarr2[$controller][$i] = $action;

                    $menuitem = '/' . $controller . '/' . $action;
                    array_push($menuguestlist, $menuitem);

                    //}
                    $i++;
                }
            }

            //            echo "<pre>";          
            //            print_r($newarr2);
            //            echo "</pre>";

            $session['menuguest'] = $newarr2;
            $session['menuguestlist'] = $menuguestlist;
        }

        // set access rules in session for guest users \\//\\//\\//
        // set access rules for authenticated users
        $session = new CHttpSession;
        $session->open();

        $user_id = Yii::app()->user->id;

        //$controller =  Yii::app()->controller->id;
        $ctrls = Yii::app()->db->createCommand("SELECT distinct(controller) as controller FROM {$tbl}menu WHERE status = 0 AND showmenu = 2 AND parent_id = 0")->queryAll();



        $newarr3 = array();
        $menuauthlist = array();
        $i = 0;

        foreach ($ctrls as $ctrl) {



            $controller = $ctrl['controller'];
            $menuitems_auth = Yii::app()->db->createCommand("SELECT * FROM {$tbl}menu_permissions mp LEFT JOIN {$tbl}menu me ON mp.menu_id = me.menu_id  WHERE mp.user_id = '$user_id' AND me.status = 0 AND me.showmenu = 2 AND me.parent_id != 0 AND me.controller = '$controller'")->queryAll();

            $actionArr = array();
            foreach ($menuitems_auth as $menuall) {

                $ctrller = $menuall['controller'];
                $action = $menuall['action'];
                //echo $ctrller."--".$action;
                //if($ctrller == $controller){
                $controller = lcfirst($controller);
                $action = lcfirst($action);
                $newarr3[$controller][$i] = $action;



                $menuitem = '/' . $controller . '/' . $action;
                array_push($menuauthlist, $menuitem);

                $i++;
                //}
            }
        }

        //echo "<pre>";          
        //print_r($menuauthlist);
        //echo "</pre>"; die;
        $session['menuauth'] = $newarr3;
        $session['menuauthlist'] = $menuauthlist;


        // set access rules fro authenticated users \\//\\//\\//\\//
    }
    public function actionTasksheet($id = false)
    {
        $id = $_REQUEST['pid'];
        // $id = 2;
        $tbl = Yii::app()->db->tablePrefix;
        if (!empty($id)) {
            $projects =  Yii::app()->db->createCommand('SELECT * FROM pms_projects WHERE pid=' . $id)->queryAll();
        } else {
            $projects =  Yii::app()->db->createCommand('SELECT * FROM pms_projects where status=1 ORDER BY pid DESC LIMIT 1')->queryAll();
        }
        //   echo '<pre>';print_r($projects);exit;
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        if (!isset(Yii::app()->user->role))
            $this->redirect(array('/site/login'));

        $this->menupermissions();  // Modified by kripa on 04-10-2016
        $this->renderPartial('_task_sheet', array('id' => $projects[0]['pid']));
    }

    public function actionurlredirection()
    {
        if (!empty($_POST['url'])) {
            Yii::app()->session['redirect_url'] = $_POST['url'];
        }
    }

    public function actionChangeProject()
    {
        if (isset($_POST['project_id']) && !empty($_POST['project_id'])) {
            Yii::app()->user->setState('project_id', $_POST['project_id']);
            echo '1';
        } else {
            echo '0';
        }
    }
    public function actiondashboard()
    {

        $tbl_px = Yii::app()->db->tablePrefix;

        if (Yii::app()->session['session_start_date'] == "" &&  Yii::app()->session['session_end_date'] == "") {
            $date_range = date('Y-m-d', strtotime('-30 days'));

            Yii::app()->session['session_start_date'] = $date_range;

            Yii::app()->session['session_end_date'] =  date('Y-m-d');

            Yii::app()->session['days'] =  30;
        }
        if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
            $start_date = Yii::app()->session['session_start_date'];

            $end_date = Yii::app()->session['session_end_date'];
        }
        $available_task_criteria = new CDbCriteria;
        $available_task_criteria->select = "tskid";
        
        $available_task_criteria->condition = "((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)";

        $available_task_criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );

        $all_available_task = Tasks::model()->findAll($available_task_criteria);



        if (Yii::app()->user->role == 1) {
            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
                $start_date = Yii::app()->session['session_start_date'];

                $end_date = Yii::app()->session['session_end_date'];
            }

            $model = new Tasks('search');
            $model->status = 75;
            $unassigned_dataprovider = $model->search($type = 'unassigned');
            $unassigned_task_count = $unassigned_dataprovider->getTotalItemCount();
            $array = [];



            //array_push($array, array('x' => "unassigned", 'value' => $unassigned_task_count));
            array_push($array, array('value' => $unassigned_task_count, 'category' => "unassigned"));


            $task_model = new Tasks('search');
            $task_model->status = 7;
            $closed_dataprovider = $task_model->search($type = '', $type_closed = 'closed');
            $closed_task_count = $closed_dataprovider->getTotalItemCount();

            // array_push($array, array('x' => "closed", 'value' => $closed_task_count));

            array_push($array, array('value' => $closed_task_count, 'category' => "closed"));

            $task = new Tasks('search');
            $task->status = 9;
            $in_progress_dataprovider = $task->search($type = 'in-progress');
            $in_progress_task_count = $in_progress_dataprovider->getTotalItemCount();


            // array_push($array, array('x' => "in-progress", 'value' => $in_progress_task_count));

            array_push($array, array('value' => $in_progress_task_count, 'category' => "in-progress"));


            $pie_chart_array = json_encode($array);
            
            $photo_punch = Photopunch::model()->findAll(
                array("condition" => "approved_status
    =  0 and log_time >= '" . $start_date . "' and DATE(log_time) <= '" . $end_date . "' ")
            );
            $pending_photo_punch = count($photo_punch);



            $criteria = new CDbCriteria;
            $criteria->addCondition("approved_status = 0 and date >= '" . $start_date . "' and date <= '" . $end_date . "'");
            $meeting_minutes = SiteMeetings::model()->findAll($criteria);
            $pending_meeting_minutes = count($meeting_minutes);



            $daily_work_progress = DailyWorkProgress::model()->findAll(
                array("condition" => "approve_status
    =  0 and date >= '" . $start_date . "' and date <= '" . $end_date . "'")
            );
            $wpr_pending_count = count($daily_work_progress);

            $user_role = 4;

            $user = Users::model()->findAll(array("condition" => "user_type
 = 4"));
            $project_array = [];
            foreach ($user as $users) {
                $user_id = $users->userid;
                $sql = "SELECT status,name  FROM pms_projects WHERE FIND_IN_SET(" . $user_id . ",assigned_to)   ";
                $data    = Yii::app()->db->createCommand($sql)->queryAll();

                foreach ($data as $data) {
                    if ($data['status'] == 1) {
                        $status = 'Active';
                    } else {
                        $status = 'Inactive';
                    }

                    $project_array[] = array(
                        'user' => $users->first_name . " " . $users->last_name,
                        'project' => $data['name'],
                        'status' => $status

                    );
                }
            }
            $project_model = new Tasks();
            // $project_data = $project_model->projects();




            $criteria = new CDbCriteria;
            // if (Yii::app()->user->project_id != "") {

            //     $criteria->addCondition('t.pid = ' . Yii::app()->user->project_id);
            // }
            if (Yii::app()->user->role != 1) {
                $criteria_proj = new CDbCriteria;
                $criteria_proj->select = 'project_id';
                $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria_proj->group = 'project_id';
                $project_ids = Tasks::model()->findAll($criteria_proj);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                }
                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $proj_condition = "OR pid IN (" . $project_id_array . ") AND status =1";
                    $criteria->addCondition('pid IN (' . $project_id_array . ')', 'OR');
                }
            }
            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {


                $start_date = Yii::app()->session['session_start_date'];

                $end_date = Yii::app()->session['session_end_date'];


                // $criteria->addCondition("start_date >= '" . $start_date . "' and end_date <= '" . $end_date . "'");

                $criteria->condition = "((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= end_date AND end_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= end_date)";

                $criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );
            }

            $count_deal = Projects::model()->count($criteria);

            $pages = new CPagination($count_deal);
            $pages->pageSize = 5;
            $pages->applyLimit($criteria);

            $project_data = Projects::model()->findAll($criteria);
            $start_date_array = [];
            $end_date_array = [];
            $projects_details = [];
            $date_output = [];
            $completed_project_id_array = [];

            foreach ($project_data as $project) {
                $start_date_array[] = $project->start_date;
                $end_date_array[] = $project->end_date;

                $projects_details[] = array(
                    'project_name' => $project->name,
                    'project_start_date' => date('M-Y', strtotime($project->start_date)),
                    'project_end_date' => date('M-Y', strtotime($project->end_date)),
                    's_date' => $project->start_date,
                    'e_date' => $project->end_date
                );

                // completed task

                $alltasks = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from("{$tbl_px}tasks")
                    ->where('project_id=' . $project->pid)
                    ->queryAll();
                $task_count = count($alltasks);
                if ($task_count > 0) {

                    $completed_count = 0;


                    foreach ($alltasks as $each_tasks) {
                        if ($each_tasks['status'] == 7) {
                            $completed_count = $completed_count + 1;
                        }
                    }
                    if ($task_count == $completed_count) {
                        $completed_project_id_array[] = $project->pid;
                    }
                }


                // end completed task

            }



            if (count($start_date_array) > 0 && count($end_date_array) > 0) {
                $project_start_date = min($start_date_array);
                $project_end_date = max($end_date_array);
                $date1  = $project_start_date;
                $date2  = $project_end_date;
                $output = [];
                $time   = strtotime($date1);
                $last   = date('M-Y', strtotime($date2));

                do {
                    $month = date('M-Y', $time);
                    //$month = date('Y-M', $time);
                    $total = date('t', $time);

                    $date_output[] = $month;

                    $time = strtotime('+1 month', $time);
                } while ($month != $last);
            }



            $tsk_model = new Tasks();

            $total_task_count = count($tsk_model->totalTask($status=""));

            $completed_task_count = count($tsk_model->totalTask($status=7));

            $inprogress_task_count = count($tsk_model->totalTask($status=9));
            
            $open_task_count = count($tsk_model->totalTask($status=6));
           

          

            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
                $start_date = Yii::app()->session['session_start_date'];
                $end_date = Yii::app()->session['session_end_date'];
            }




            $recently_updated_sql = "select t.quantity, sum(d.qty) as total_quantity,t.title,MIN(`date`) min_date,MAX(`date`) AS max_date,t.project_id,t.tskid,t.start_date,t.due_date,u.first_name,u.last_name,t.created_date,p.name from pms_tasks t

left join pms_daily_work_progress d on ( t.tskid = d.taskid)

left join pms_users u on (t.created_by = u.userid )

left join pms_projects p on ( t.project_id = p.pid)

where task_type=1 and t.status !=7 and ((t.start_date BETWEEN '" . $start_date . "' and '" . $end_date . "') OR (t.due_date BETWEEN '" . $start_date . "' and '" . $end_date . "') OR (t.start_date > '" . $start_date . "' AND t.due_date < '" . $start_date . "') 
    OR 
(t.start_date < '" . $end_date . "' AND t.due_date > '" . $end_date . "'))  group by t.tskid ORDER by min_date DESC";

            $recently_updated_task = Yii::app()->db->createCommand($recently_updated_sql)->queryAll();







            $available_user = Users::model()->findAll(
                array("condition" => "status =  0", "order" => "userid")
            );
            $workload_array = [];
            foreach ($available_user as $users) {
                $user_id = $users->userid;
                //echo  $user_id."--";
                $total_task_criteria = new CDbCriteria;
                $total_task_criteria->select = "tskid";

                $total_task_condition = 't.created_by=' . $user_id
                    . ' OR assigned_to=' . $user_id
                    . ' OR coordinator=' . $user_id
                    . ' OR report_to = ' . $user_id;
                $total_task_criteria->addCondition($total_task_condition);

                // $total_task_criteria->addCondition("start_date >='" . $start_date . "' and due_date <='" . $end_date . "'");

                $total_task_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $total_task_criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );

                $total_tasks = Tasks::model()->findAll($total_task_criteria);




                $completed_criteria = new CDbCriteria;
                $completed_criteria->select = "tskid";
                $completed_task_condition = 't.created_by=' . $user_id
                    . ' OR assigned_to=' . $user_id
                    . ' OR coordinator=' . $user_id
                    . ' OR report_to = ' . $user_id;
                $completed_criteria->addCondition($completed_task_condition);

                $completed_criteria->addCondition("status=7");

                $completed_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $completed_criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );

                $completed_tasks = Tasks::model()->findAll($completed_criteria);


                $inprogress_criteria = new CDbCriteria;
                $inprogress_criteria->select = "tskid";
                $inprogress_task_condition = 't.created_by=' . $user_id
                    . ' OR assigned_to=' . $user_id
                    . ' OR coordinator=' . $user_id
                    . ' OR report_to = ' . $user_id;
                $inprogress_criteria->addCondition($inprogress_task_condition);

                $inprogress_criteria->addCondition("status=9");

                $inprogress_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $inprogress_criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );


                $inprogress_tasks = Tasks::model()->findAll($inprogress_criteria);


                $workload_array[] = array(
                    'user_id' => $user_id,
                    'user_name' => $users->first_name . " " . $users->last_name,
                    'total' => count($total_tasks),
                    'completed' => count($completed_tasks),
                    'inprogress' => count($inprogress_tasks),
                    'all_task' => count($all_available_task),
                    

                );
            }


            //echo "<pre>",print_r($workload_array);exit;



            $this->render('admin_dashboard', array(
                'pie_chart_array' => $pie_chart_array, 'array' => $array, 'wpr_pending_count' => $wpr_pending_count, 'pending_photo_punch' => $pending_photo_punch, 'pending_meeting_minutes' => $pending_meeting_minutes,
                'project_array' => $project_array,
                'projects_details' => $projects_details,
                'date_output' => $date_output,
                'completed_project' => count($completed_project_id_array),
                'project_data' => $project_data,
                'total_task_count' => $total_task_count,
                'recently_updated_task' => $recently_updated_task,
                'pages' => $pages,
                'workload_array' => $workload_array,
                'completed_task_count'=>$completed_task_count,
                'inprogress_task_count' =>$inprogress_task_count, 
                'open_task_count'=>$open_task_count
            ));
        } elseif (Yii::app()->user->role == 10) {



            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
                $start_date = Yii::app()->session['session_start_date'];

                $end_date = Yii::app()->session['session_end_date'];
            }

            $criteria = new CDbCriteria;
            $criteria->select = 'project_id,tskid';
            $criteria->condition = '  (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ")";
            $criteria->group = 'project_id';
            $project_ids = Tasks::model()->findAll($criteria);
            $project_id_array = array();

            foreach ($project_ids as $projid) {
                array_push($project_id_array, $projid['project_id']);
            }
            if (!empty($project_id_array)) {
                $project_id_list = implode(',', $project_id_array);
            } else {
                $project_id_list = 'NULL';
            }

            $daily_work_progress = DailyWorkProgress::model()->findAll(
                array("condition" => "approve_status
    =  0 and date >= '" . $start_date . "' and date <= '" . $end_date . "'  and project_id IN (" . $project_id_list . ")")
            );
            $wpr_pending_count = count($daily_work_progress);

            $criteria = new CDbCriteria;
            $criteria->addCondition("approved_status = 0 and date >= '" . $start_date . "' and date <= '" . $end_date . "' and approved_by=" . Yii::app()->user->id . " OR created_by=" . Yii::app()->user->id);

            $meeting_minutes = SiteMeetings::model()->findAll($criteria);
            $pending_meeting_minutes = count($meeting_minutes);

            $photo_punch = Photopunch::model()->findAll(
                array("condition" => "approved_status
        =  0 and log_time >= '" . $start_date . "' and DATE(log_time) <= '" . $end_date . "' and empid=" . Yii::app()->user->id)
            );
            $pending_photo_punch = count($photo_punch);

            $project_model = new Tasks();
            // $project_data = $project_model->projects(); 

            $criteria = new CDbCriteria;
            // if (Yii::app()->user->project_id != "") {

            //     $criteria->addCondition('t.pid = ' . Yii::app()->user->project_id);
            // }
            if (Yii::app()->user->role != 1) {
                $criteria_proj = new CDbCriteria;
                $criteria_proj->select = 'project_id';
                $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria_proj->group = 'project_id';
                $project_ids = Tasks::model()->findAll($criteria_proj);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                }
                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $proj_condition = "OR pid IN (" . $project_id_array . ") AND status =1";
                    $criteria->addCondition('pid IN (' . $project_id_array . ')', 'OR');
                }
            }
            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {


                $start_date = Yii::app()->session['session_start_date'];

                $end_date = Yii::app()->session['session_end_date'];
                //   echo $start_date."--". $end_date;

                // $criteria->addCondition("start_date >= '" . $start_date . "' and end_date <= '" . $end_date . "'");

                $criteria->condition = "((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= end_date AND end_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= end_date)";

                $criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );
            }

            $count_deal = Projects::model()->count($criteria);

            $pages = new CPagination($count_deal);
            $pages->pageSize = 5;
            $pages->applyLimit($criteria);

            $project_data = Projects::model()->findAll($criteria);

            $start_date_array = [];
            $end_date_array = [];
            $projects_details = [];
            $date_output = [];
            $completed_project_id_array = [];

            foreach ($project_data as $project) {
                $start_date_array[] = $project->start_date;
                $end_date_array[] = $project->end_date;

                $projects_details[] = array(
                    'project_name' => $project->name,
                    'project_start_date' => date('M-Y', strtotime($project->start_date)),
                    'project_end_date' => date('M-Y', strtotime($project->end_date)),
                    's_date' => $project->start_date,
                    'e_date' => $project->end_date
                );

                // completed task

                $alltasks = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from("{$tbl_px}tasks")
                    ->where('project_id=' . $project->pid)
                    ->queryAll();
                $task_count = count($alltasks);
                if ($task_count > 0) {

                    $completed_count = 0;


                    foreach ($alltasks as $each_tasks) {
                        if ($each_tasks['status'] == 7) {
                            $completed_count = $completed_count + 1;
                        }
                    }
                    if ($task_count == $completed_count) {
                        $completed_project_id_array[] = $project->pid;
                    }
                }
            }

            if (count($start_date_array) > 0 && count($end_date_array) > 0) {
                $project_start_date = min($start_date_array);
                $project_end_date = max($end_date_array);
                $date1  = $project_start_date;
                $date2  = $project_end_date;
                $output = [];
                $time   = strtotime($date1);
                $last   = date('M-Y', strtotime($date2));

                do {
                    $month = date('M-Y', $time);
                    //$month = date('Y-M', $time);
                    $total = date('t', $time);

                    $date_output[] = $month;

                    $time = strtotime('+1 month', $time);
                } while ($month != $last);
            }

            $tsk_model = new Tasks();

            $total_task_count = count($tsk_model->totalTask($status=""));

            $completed_task_count = count($tsk_model->totalTask($status=7));

            $inprogress_task_count = count($tsk_model->totalTask($status=9));
            
            $open_task_count = count($tsk_model->totalTask($status=6));

            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
                $start_date = Yii::app()->session['session_start_date'];
                $end_date = Yii::app()->session['session_end_date'];
            }



            $recently_updated_sql = "select  t.quantity,  sum(d.qty) as total_quantity,t.title,MIN(`date`) min_date,MAX(`date`) AS max_date,t.project_id,t.tskid,t.start_date,t.due_date,u.first_name,u.last_name,t.created_date,p.name from pms_tasks t 

   left join pms_daily_work_progress d on ( t.tskid = d.taskid)

   left join pms_users u on (t.created_by = u.userid )

   left join pms_projects p on ( t.project_id = p.pid)
   
   where task_type=1 and t.status !=7 and ((t.start_date BETWEEN '" . $start_date . "' and '" . $end_date . "') OR (t.due_date BETWEEN '" . $start_date . "' and '" . $end_date . "') OR (t.start_date > '" . $start_date . "' AND t.due_date < '" . $start_date . "') 
    OR 
(t.start_date < '" . $end_date . "' AND t.due_date > '" . $end_date . "'))

    and (t.created_by=" . Yii::app()->user->id . " or t.assigned_to=" . Yii::app()->user->id . " or coordinator=" . Yii::app()->user->id . " or t.report_to=" . Yii::app()->user->id . ")
     group by t.tskid ORDER by min_date DESC";

            $recently_updated_task = Yii::app()->db->createCommand($recently_updated_sql)->queryAll();



            $workload_array = [];

            $total_task_criteria = new CDbCriteria;
            $total_task_criteria->select = "tskid";

            $total_task_condition = 't.created_by=' . Yii::app()->user->id
                . ' OR assigned_to=' . Yii::app()->user->id
                . ' OR coordinator=' . Yii::app()->user->id
                . ' OR report_to = ' . Yii::app()->user->id;
            $total_task_criteria->addCondition($total_task_condition);

            // $total_task_criteria->addCondition("start_date >='" . $start_date . "' and due_date <='" . $end_date . "'");

            $total_task_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

            $total_task_criteria->params = array(
                ':startDateRange' => $start_date,
                ':endDateRange' => $end_date,

            );



            $total_tasks = Tasks::model()->findAll($total_task_criteria);




            $completed_criteria = new CDbCriteria;
            $completed_criteria->select = "tskid";
            $completed_task_condition = 't.created_by=' . Yii::app()->user->id
                . ' OR assigned_to=' . Yii::app()->user->id
                . ' OR coordinator=' . Yii::app()->user->id
                . ' OR report_to = ' . Yii::app()->user->id;
            $completed_criteria->addCondition($completed_task_condition);

            $completed_criteria->addCondition("status=7");

            $completed_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

            $completed_criteria->params = array(
                ':startDateRange' => $start_date,
                ':endDateRange' => $end_date,

            );

            $completed_tasks = Tasks::model()->findAll($completed_criteria);


            $inprogress_criteria = new CDbCriteria;
            $inprogress_criteria->select = "tskid";
            $inprogress_task_condition = 't.created_by=' . Yii::app()->user->id
                . ' OR assigned_to=' . Yii::app()->user->id
                . ' OR coordinator=' . Yii::app()->user->id
                . ' OR report_to = ' . Yii::app()->user->id;
            $inprogress_criteria->addCondition($inprogress_task_condition);

            $inprogress_criteria->addCondition("status=9");

            $inprogress_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

            $inprogress_criteria->params = array(
                ':startDateRange' => $start_date,
                ':endDateRange' => $end_date,

            );

            $inprogress_tasks = Tasks::model()->findAll($inprogress_criteria);


            $workload_array[] = array(

                'user_name' => Yii::app()->user->name,
                'total' => count($total_tasks),
                'completed' => count($completed_tasks),
                'inprogress' => count($inprogress_tasks),
                'all_task' => count($all_available_task)

            );

            // echo "<pre>",print_r($workload_array);exit;


            $this->render('site_engineer_dashboard', array(
                'wpr_pending_count' => $wpr_pending_count,
                'pending_meeting_minutes' => $pending_meeting_minutes,
                'pending_photo_punch' => $pending_photo_punch,
                'completed_project' => count($completed_project_id_array),
                'project_data' => $project_data,
                'total_task_count' => $total_task_count,
                'recently_updated_task' => $recently_updated_task,
                'date_output' => $date_output,
                'projects_details' => $projects_details,
                'pages' => $pages,
                'workload_array' => $workload_array,
                'completed_task_count'=>$completed_task_count,
                'inprogress_task_count'=>$inprogress_task_count,
                'open_task_count'=>$open_task_count





            ));
        } else {

            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
                $start_date = Yii::app()->session['session_start_date'];

                $end_date = Yii::app()->session['session_end_date'];
            }
            $criteria = new CDbCriteria;
            $criteria->select = 'project_id,tskid';
            $criteria->condition = '  (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ")";
            $criteria->group = 'project_id';
            $project_ids = Tasks::model()->findAll($criteria);
            $project_id_array = array();

            foreach ($project_ids as $projid) {
                array_push($project_id_array, $projid['project_id']);
            }
            if (!empty($project_id_array)) {
                $project_id_list = implode(',', $project_id_array);
            } else {
                $project_id_list = 'NULL';
            }



            $daily_work_progress = DailyWorkProgress::model()->findAll(
                array("condition" => "approve_status
    =  0 and date >= '" . $start_date . "' and date <= '" . $end_date . "'  and project_id IN (" . $project_id_list . ")")
            );
            $wpr_pending_count = count($daily_work_progress);



            $criteria = new CDbCriteria;
            $criteria->addCondition("approved_status = 0 and date >= '" . $start_date . "' and date <= '" . $end_date . "' and approved_by=" . Yii::app()->user->id . " OR created_by=" . Yii::app()->user->id);

            $meeting_minutes = SiteMeetings::model()->findAll($criteria);
            $pending_meeting_minutes = count($meeting_minutes);


            $photo_punch = Photopunch::model()->findAll(
                array("condition" => "approved_status
=  0 and log_time >= '" . $start_date . "' and DATE(log_time) <= '" . $end_date . "' and empid=" . Yii::app()->user->id)
            );
            $pending_photo_punch = count($photo_punch);


            // workload of team

            $members = Users::model()->findAll(
                array("condition" => "reporting_person =" . Yii::app()->user->id)
            );

            foreach ($members as $member) {
                $team_members[] = $member->userid;
            }
            if (!empty($team_members)) {
                $each_member = implode(',', $team_members);

                $condition = new CDbCriteria;
                $condition = 't.created_by IN(' . $each_member . ')'
                    . ' OR assigned_to IN(' . $each_member . ')'
                    . ' OR coordinator IN(' . $each_member . ')'
                    . ' OR report_to IN(' . $each_member . ')';
                $tasks = Tasks::model()->findAll($condition);
            }


            // $project_model = new Tasks();
            // $project_data = $project_model->projects(); 
            $criteria = new CDbCriteria;
            // if (Yii::app()->user->project_id != "") {

            //     $criteria->addCondition('t.pid = ' . Yii::app()->user->project_id);
            // }
            if (Yii::app()->user->role != 1) {
                $criteria_proj = new CDbCriteria;
                $criteria_proj->select = 'project_id';
                $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria_proj->group = 'project_id';
                $project_ids = Tasks::model()->findAll($criteria_proj);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                }
                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $proj_condition = "OR pid IN (" . $project_id_array . ") AND status =1";
                    $criteria->addCondition('pid IN (' . $project_id_array . ')', 'OR');
                }
            }
            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {


                $start_date = Yii::app()->session['session_start_date'];

                $end_date = Yii::app()->session['session_end_date'];
                //   echo $start_date."--". $end_date;

                // $criteria->addCondition("start_date >= '" . $start_date . "' and end_date <= '" . $end_date . "'");

                $criteria->condition = "((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= end_date AND end_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= end_date)";

                $criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );
            }

            $count_deal = Projects::model()->count($criteria);

            $pages = new CPagination($count_deal);
            $pages->pageSize = 5;
            $pages->applyLimit($criteria);

            $project_data = Projects::model()->findAll($criteria);


            $start_date_array = [];
            $end_date_array = [];
            $projects_details = [];
            $date_output = [];
            $completed_project_id_array = [];
            foreach ($project_data as $project) {
                $start_date_array[] = $project->start_date;
                $end_date_array[] = $project->end_date;

                $projects_details[] = array(
                    'project_name' => $project->name,
                    'project_start_date' => date('M-Y', strtotime($project->start_date)),
                    'project_end_date' => date('M-Y', strtotime($project->end_date)),
                    's_date' => $project->start_date,
                    'e_date' => $project->end_date
                );

                // completed task

                $alltasks = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from("{$tbl_px}tasks")
                    ->where('project_id=' . $project->pid)
                    ->queryAll();
                $task_count = count($alltasks);
                if ($task_count > 0) {

                    $completed_count = 0;


                    foreach ($alltasks as $each_tasks) {
                        if ($each_tasks['status'] == 7) {
                            $completed_count = $completed_count + 1;
                        }
                    }
                    if ($task_count == $completed_count) {
                        $completed_project_id_array[] = $project->pid;
                    }
                }
            }
            if (count($start_date_array) > 0 && count($end_date_array) > 0) {
                $project_start_date = min($start_date_array);
                $project_end_date = max($end_date_array);
                $date1  = $project_start_date;
                $date2  = $project_end_date;
                $output = [];
                $time   = strtotime($date1);
                $last   = date('M-Y', strtotime($date2));

                do {
                    $month = date('M-Y', $time);
                    //$month = date('Y-M', $time);
                    $total = date('t', $time);

                    $date_output[] = $month;

                    $time = strtotime('+1 month', $time);
                } while ($month != $last);
            }

            $tsk_model = new Tasks();

            $total_task_count = count($tsk_model->totalTask($status=""));

            $completed_task_count = count($tsk_model->totalTask($status=7));

            $inprogress_task_count = count($tsk_model->totalTask($status=9));
            
            $open_task_count = count($tsk_model->totalTask($status=6));

            if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
                $start_date = Yii::app()->session['session_start_date'];
                $end_date = Yii::app()->session['session_end_date'];
            }



            $recently_updated_sql = "select  t.quantity,  sum(d.qty) as total_quantity,t.title,MIN(`date`) min_date,MAX(`date`) AS max_date,t.project_id,t.tskid,t.start_date,t.due_date,u.first_name,u.last_name,t.created_date,p.name from pms_tasks t 

   left join pms_daily_work_progress d on ( t.tskid = d.taskid)

   left join pms_users u on (t.created_by = u.userid )

   left join pms_projects p on ( t.project_id = p.pid)
   
   where task_type=1 and t.status !=7 and ((t.start_date BETWEEN '" . $start_date . "' and '" . $end_date . "') OR (t.due_date BETWEEN '" . $start_date . "' and '" . $end_date . "') OR (t.start_date > '" . $start_date . "' AND t.due_date < '" . $start_date . "') 
    OR 
(t.start_date < '" . $end_date . "' AND t.due_date > '" . $end_date . "'))

    and (t.created_by=" . Yii::app()->user->id . " or t.assigned_to=" . Yii::app()->user->id . " or coordinator=" . Yii::app()->user->id . " or t.report_to=" . Yii::app()->user->id . ")
     group by t.tskid ORDER by min_date DESC";

            $recently_updated_task = Yii::app()->db->createCommand($recently_updated_sql)->queryAll();




            $available_user = Users::model()->findAll(
                array("condition" => "status =  0 and reporting_person=" . Yii::app()->user->id, "order" => "userid")
            );
            $workload_array = [];
            foreach ($available_user as $users) {
                $user_id = $users->userid;

                $total_task_criteria = new CDbCriteria;
                $total_task_criteria->select = "tskid";

                $total_task_condition = 't.created_by=' . $user_id
                    . ' OR assigned_to=' . $user_id
                    . ' OR coordinator=' . $user_id
                    . ' OR report_to = ' . $user_id;
                $total_task_criteria->addCondition($total_task_condition);

                // $total_task_criteria->addCondition("start_date >='" . $start_date . "' and due_date <='" . $end_date . "'");

                $total_task_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $total_task_criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );



                $total_tasks = Tasks::model()->findAll($total_task_criteria);




                $completed_criteria = new CDbCriteria;
                $completed_criteria->select = "tskid";
                $completed_task_condition = 't.created_by=' . $user_id
                    . ' OR assigned_to=' . $user_id
                    . ' OR coordinator=' . $user_id
                    . ' OR report_to = ' . $user_id;
                $completed_criteria->addCondition($completed_task_condition);

                $completed_criteria->addCondition("status=7");

                $completed_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $completed_criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );

                $completed_tasks = Tasks::model()->findAll($completed_criteria);


                $inprogress_criteria = new CDbCriteria;
                $inprogress_criteria->select = "tskid";
                $inprogress_task_condition = 't.created_by=' . $user_id
                    . ' OR assigned_to=' . $user_id
                    . ' OR coordinator=' . $user_id
                    . ' OR report_to = ' . $user_id;
                $inprogress_criteria->addCondition($inprogress_task_condition);

                $inprogress_criteria->addCondition("status=9");

                $inprogress_criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $inprogress_criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );

                $inprogress_tasks = Tasks::model()->findAll($inprogress_criteria);


                $workload_array[] = array(
                    'user_id' => $user_id,
                    'user_name' => $users->first_name . " " . $users->last_name,
                    'total' => count($total_tasks),
                    'completed' => count($completed_tasks),
                    'inprogress' => count($inprogress_tasks),
                    'all_task' => count($all_available_task),

                );
            }

            // echo "<pre>",print_r($workload_array);exit; 

            $this->render(
                'supervisor_dashboard',
                array(
                    'wpr_pending_count' => $wpr_pending_count,
                    'pending_meeting_minutes' => $pending_meeting_minutes,
                    'pending_photo_punch' => $pending_photo_punch,
                    'completed_project' => count($completed_project_id_array),
                    'project_data' => $project_data,
                    'total_task_count' => $total_task_count,
                    'recently_updated_task' => $recently_updated_task,
                    'date_output' => $date_output,
                    'projects_details' => $projects_details,
                    'pages' => $pages,
                    'workload_array' => $workload_array,
                    'completed_task_count'=>$completed_task_count,
                    'inprogress_task_count'=>$inprogress_task_count,
                    'open_task_count'=>$open_task_count
                )
            );
        }
    }
    public function actiontaskPieChart()
    {
        $array = [];





        $unassigned_task_count = $this->getTaskStatus($type = 75);

        array_push($array, array('value' => $unassigned_task_count, 'category' => "not started"));



        $closed_task_count = $this->getTaskStatus($type = 7);



        array_push($array, array('value' => $closed_task_count, 'category' => "completed"));





        $in_progress_task_count = $this->getTaskStatus($type = 9);

        array_push($array, array('value' => $in_progress_task_count, 'category' => "in-progress"));



        echo json_encode($array);
    }
    public function actionsetDateRange()
    {
        $days = $_POST['days'];
        $date_range = date('Y-m-d', strtotime('-' . $days . 'days'));

        Yii::app()->session['session_start_date'] = $date_range;

        Yii::app()->session['session_end_date'] =  date('Y-m-d');

        Yii::app()->session['days'] =   $days;
    }
    public function taskStatus($project_id, $status)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('trash=1');
        $criteria->addCondition('status=' . $status);
        $criteria->addCondition('project_id=' . $project_id);

        if (Yii::app()->user->role != 1) {
            $condition = 't.created_by=' . Yii::app()->user->id
                . ' OR assigned_to=' . Yii::app()->user->id
                . ' OR coordinator=' . Yii::app()->user->id
                . ' OR report_to = ' . Yii::app()->user->id;
            $criteria->addCondition($condition);
        }


        if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
            $start_date = Yii::app()->session['session_start_date'];
            $end_date = Yii::app()->session['session_end_date'];
            // $criteria->addCondition("start_date >= '" . $start_date . "' and due_date <= '" . $end_date . "'");

            $criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );
        }

        $tasks = Tasks::model()->findAll($criteria);
        return  $tasks;
    }
    public function getTaskStatus($type)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('trash=1');

        if (Yii::app()->user->role != 1) {
            $condition = 't.created_by=' . Yii::app()->user->id
                . ' OR assigned_to=' . Yii::app()->user->id
                . ' OR coordinator=' . Yii::app()->user->id
                . ' OR report_to = ' . Yii::app()->user->id;
            $criteria->addCondition($condition);
        }

        $criteria->addCondition("status=" . $type);


        if (isset(Yii::app()->session['session_start_date']) && (Yii::app()->session['session_end_date'])) {
            $start_date = Yii::app()->session['session_start_date'];
            $end_date = Yii::app()->session['session_end_date'];
            // $criteria->addCondition("start_date >= '" . $start_date . "' and due_date <= '" . $end_date . "'");
            $criteria->addCondition("((:startDateRange <= start_date AND start_date <= :endDateRange)
                OR (:startDateRange <= due_date AND due_date <= :endDateRange))
                OR (start_date <= :endDateRange AND :startDateRange <= due_date)");

                $criteria->params = array(
                    ':startDateRange' => $start_date,
                    ':endDateRange' => $end_date,

                );
        }

        $tasks = Tasks::model()->findAll($criteria);
        return  count($tasks);
    }

    public function actionRedirect($type)
    {
        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
            $query1 = "SELECT system_access_types "
                . "FROM `global_users` WHERE  global_user_id = '" . Yii::app()->user->id . "'";
            $command1 = Yii::app()->db->createCommand($query1);
            $result1 = $command1->queryRow();
        } else {
            $query1 = "SELECT system_access_types "
                . "FROM `hrms_employee` WHERE  emp_id = '" . Yii::app()->user->id . "'";
            $command1 = Yii::app()->db->createCommand($query1);
            $result1 = $command1->queryRow();
        }

        $allowed_apps = explode(',', $result1['system_access_types']);
        if (in_array($type, $allowed_apps)) {
            $hostname = $_SERVER['HTTP_HOST'];
            $url_expiry_time = time() + (30 - time() % 30);
            $encrypted_token = md5($hostname . '/' . strtolower($type) . $url_expiry_time . Yii::app()->user->id);
            $id = Yii::app()->user->id;
            $query = "SELECT url FROM global_settings WHERE access_types = '" . $type . "'"; // Fetch prefix from global settings table.
            $command = Yii::app()->db->createCommand($query);
            $result = $command->queryRow();
            $url = $result['url'];
            if ($url)
                $this->redirect($url . '&switch_token=' . $encrypted_token);
        } else {
            $this->redirect(Yii::app()->request->urlReferrer);
        }
    }
}
