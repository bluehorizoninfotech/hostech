<?php

class MeetingMinutesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$accessArr = array();
		$accessauthArr = array();
		$accessguestArr = array();
		$controller = Yii::app()->controller->id;


		if (isset(Yii::app()->session['menuauth'])) {
			if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
				$accessauthArr = Yii::app()->session['menuauth'][$controller];
			}
		}

		$access_privlg = count($accessauthArr);

		return array(
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('getmodel', 'create','savemeetingMinutesData','checkTaskExist'),
				'users' => array('@'),
			),
			array(
				'deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render(
			'view',
			array(
				'model' => $this->loadModel($id),
			)
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		if (isset($_POST['MeetingMinutes'])) {
			$minutes_data = $_POST['MeetingMinutes']['minutes_data'];
			$meeting_id = $_POST['MeetingMinutes']['meeting_id'];
			if (isset($_POST['submit_type'])) {
				if ($_POST['submit_type'] == 'add_new') {
					$return_result = array('status' => 1, 'next_level' => 0, 'meeting_id' => $meeting_id);
				} else {
					$return_result = array('status' => 1, 'next_level' => 1, 'meeting_id' => $meeting_id);
				}
			}
			$minutes_data_json = json_decode($minutes_data);
			foreach ($minutes_data_json as $data) {
				$result = $this->saveMinutesData($meeting_id, $data);
				if (isset($result['error'])) {
					$return_result = array('status' => 0, 'next_level' => 0, 'error' => $result['error']);
					break;
				}
			}
		} else {
			$return_result = array('status' => 0, 'next_level' => 0);
		}
		echo json_encode($return_result);
		exit;

		
	}
	public function actionsavemeetingMinutesData(){
		$row_data=explode(',',$_POST['row_data']);
		$result = $this->saveMinutesData($_POST['meeting_id'], $row_data,$_POST['Tasks']);
		if (isset($result['error'])) {
			$return_result = array('status' => 0, 'next_level' => 0, 'error' => $result['error']);
		}else{
			$return_result = array('status' => 1, 'next_level' => 0, 'meeting_minutes_id' => $result ,'due_date' =>$row_data[3]);
		}
		echo json_encode($return_result);
		exit;
	}
	// public function actionCreate()
	// {
	// 	$model = new MeetingMinutes;
	// 	$this->performAjaxValidation($model);

	// 	if (isset($_POST['MeetingMinutes']['meeting_minutes_id']) && !empty($_POST['MeetingMinutes']['meeting_minutes_id'])) {
	// 		$id = $_POST['MeetingMinutes']['meeting_minutes_id'];
	// 		$model = $this->loadModel($id);
	// 	}

	// 	if (isset($_POST['MeetingMinutes'])) {

	// 		$model->attributes = $_POST['MeetingMinutes'];
	// 		$model->start_date = date('Y-m-d', strtotime($_POST['MeetingMinutes']['start_date']));
	// 		$model->end_date = date('Y-m-d', strtotime($_POST['MeetingMinutes']['end_date']));
	// 		$model->created_by = yii::app()->user->id;
	// 		$model->created_date = date('Y-m-d');
	// 		$model->updated_by = yii::app()->user->id;
	// 		$model->updated_date = date('Y-m-d H:i:s');
	// 		if (isset($_POST['MeetingMinutes']['meeting_minutes_status'])) {
	// 			$model->meeting_minutes_status = $_POST['MeetingMinutes']['meeting_minutes_status'];
	// 		}

	// 		// echo '<pre>';
	// 		// print_r($_POST['MeetingMinutes']['create_task_status']);
	// 		// exit;
	// 		$transaction = Yii::app()->db->beginTransaction();
	// 		try {
	// 			if ($model->save()) {
	// 				Yii::app()->session['site_meeting_id'] = $model->meeting_id;
	// 				if (isset($_POST['MeetingMinutes'])) {
	// 					$users = $_POST['MeetingMinutes']['users'];
	// 					$user_save = $this->saveUsers($users, $model);
	// 					if ($user_save !== true) {
	// 						throw new Exception($user_save);
	// 					}
	// 				}
	// 				if (isset($_POST['submit_type'])) {
	// 					if ($_POST['submit_type'] == 'add_new') {
	// 						$return_result = array('status' => 1, 'next_level' => 0, 'meeting_id' => $model->meeting_id);
	// 					} else {
	// 						$return_result = array('status' => 1, 'next_level' => 1, 'meeting_id' => $model->meeting_id);
	// 					}
	// 				}
	// 			} else {
	// 				throw new Exception('sdfsdf');
	// 			}
	// 			$transaction->commit();
	// 		} catch (Exception $error) {
	// 			$transaction->rollback();
	// 			$return_result = array('status' => 0, 'next_level' => 0, 'error' => $error->getMessage());
	// 		}
	// 	} else {
	// 		$return_result = array('status' => 0, 'next_level' => 0);
	// 	}
	// 	echo json_encode($return_result);
	// 	exit;

	// 	$this->render('create', array(
	// 		'model' => $model,
	// 	)
	// 	);
	// }
	public function saveUsers($users, $minutes_model)
	{

		foreach ($users as $key => $value) {
			$model = new MeetingMinutesUsers;
			$model->meeting_minutes_id = $minutes_model->id;
			$model->user_id = $value;
			if (!$model->save()) {
				$error = json_encode($model->getErrors());
				throw new Exception($error);
			}
		}
		return true;
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['MeetingMinutes'])) {
			$model->attributes = $_POST['MeetingMinutes'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('MeetingMinutes');
		$this->render(
			'index',
			array(
				'dataProvider' => $dataProvider,
			)
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new MeetingMinutes('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['MeetingMinutes']))
			$model->attributes = $_GET['MeetingMinutes'];

		$this->render(
			'admin',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = MeetingMinutes::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'meeting-minutes-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actiongetModel()
	{
		if (!empty($_POST['id'])) {
			$id = $_POST['id'];
			$model = $this->loadModel($id);
			$users = MeetingMinutesUsers::model()->findAll(array('condition' => 'meeting_minutes_id = ' . $id));
			$users_list = array();
			foreach ($users as $user) {
				array_push($users_list, $user->user_id);
			}
			$users_list = implode(',', $users_list);

			if (!empty($model)) {
				$model_array = array('stat' => 1, 'data' => $model->attributes, 'users' => $users_list);
			} else {
				$model_array = array('stat' => 0);
			}
		} else {
			$model_array = array('stat' => 0);
		}
		echo json_encode($model_array);
		exit;
	}
	public function saveMinutesData($meeting_id, $data,$task_data = array())
	{
		$model = new MeetingMinutes;
		if ($data[4]) {
			$model = $this->loadModel($data[4]);
		}

		$model->meeting_id = $meeting_id;
		$model->created_by = yii::app()->user->id;
		$model->created_date = date('Y-m-d');
		$model->updated_by = yii::app()->user->id;
		$model->updated_date = date('Y-m-d H:i:s');
		$model->points_discussed = isset($data[0]) ? $data[0] : '';
		$model->meeting_minutes_status = isset($data[1]) ? $data[1] : '';
		$model->end_date = isset($data[3]) ? date("Y-m-d", strtotime(str_replace('/', '-', $data[3]))) : '';
		
		if(!empty($task_data)){
			$model->attributes = $task_data;
			$model->create_task_status=1;
			$model->milestone_id=isset($task_data['milestone_id'])?$task_data['milestone_id']:'';
			$model->title=isset($task_data['title'])?$task_data['title']:'';
			$model->task_id=isset($task_data['task_id'])?$task_data['task_id']:'';
			$model->start_date=isset($task_data['start_date'])?$task_data['start_date']:'';
			$model->end_date=isset($task_data['due_date'])?$task_data['due_date']:'';
		}
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if ($model->save()) {
				Yii::app()->session['site_meeting_id'] = $model->meeting_id;
				if ($data[2]) {
					$meeting_minutes_model = new MeetingMinutesUsers;
					$criteria = new CDbCriteria;
					$criteria->addColumnCondition(array(
						'meeting_minutes_id' => $model->id,
					));
					
					if (MeetingMinutesUsers::model()->exists($criteria)) {			
						$meeting_minutes_model=MeetingMinutesUsers::model()->find($criteria);
					}
					$meeting_minutes_model->meeting_minutes_id = $model->id;
					$meeting_minutes_model->user_id = $data[2];
					if (!$meeting_minutes_model->save()) {
						throw new Exception("Error saving record: " . implode(", ", $meeting_minutes_model->getErrors()));
					}
				}
			} else {
				throw new Exception("Error saving record: " . implode(", ", $model->getErrors()));
			}
			$transaction->commit();
			if(!empty($task_data)){
				return $model->id;
			}
			return true;
		} catch (Exception $error) {
			$transaction->rollback();
			return array('error' => $error->getMessage());
		}

	}
	public function actioncheckTaskExist(){
		if(isset($_GET['meeting_minutes_id']) && $_GET['meeting_minutes_id']){
			$model=$this->loadModel($_GET['meeting_minutes_id']);
			if($model->task_id){
				$status=2;
			}else{
				$status=1;
			}
		}else{
			$status=1;
		}
		echo json_encode(['status'=>$status]);
	}

}
