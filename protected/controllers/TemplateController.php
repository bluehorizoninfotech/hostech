<?php

class TemplateController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'UpdateTemplateItem', 'checkDuplicate','template_excel'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {

        $data = TemplateItems::model()->findAll(array("condition" => "temp_id = '" . $id . "'", "order" => "id DESC"));
        $this->render('view', array(
            'model' => $this->loadModel($id),
            'data' => $data
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Template;

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Template']) && !empty($_POST['Template'])) {



            $item_count = count($_POST['Template']['item_name']);
            $model->attributes = $_POST['Template'];
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');



            if ($model->save()) {
                $template_id = $model->template_id;
                for ($count = 0; $count < $item_count; $count++) {
                    if (isset($_POST['template_purchase_item'][$count])) {
                        $item_name = $_POST['Template']['item_name'][$count];
                        $ref_column = $_POST['Template']['reference_column'][$count];
                        $template_purchase_item = implode(",", $_POST['template_purchase_item'][$count]);
                        $items_model = new TemplateItems;
                        $items_model->temp_id = $model->template_id;
                        $items_model->ref_column = $ref_column;
                        $items_model->item_name = $item_name;
                        $items_model->purchase_item =  $template_purchase_item;
                        $items_model->created_by = Yii::app()->user->id;
                        $items_model->created_date = date('Y-m-d H:i:s');
                        $items_model->save();
                    }
                }
            }
            Yii::app()->user->setFlash('success', 'Successfully created.');
            $this->redirect(array('index'));
        }

        $account_items = $this->getAccountItems();

        $this->render('create', array(
            'model' => $model, 'account_items' => $account_items
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Template'])) {
            $model->attributes = $_POST['Template'];
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
            if ($model->save()) {
                $item_count = count($_POST['Template']['item_name']);

                if ($_POST['Template']['id'] != "") {
                    $items_model = TemplateItems::model()->findByPk($_POST['Template']['id']);
                } else {
                    $items_model = new TemplateItems;
                }


                for ($count = 0; $count < $item_count; $count++) {
                    if (isset($_POST['template_purchase_item'][$count])) {
                        $item_name = $_POST['Template']['item_name'][$count];
                        $template_purchase_item = implode(",", $_POST['template_purchase_item'][$count]);
                        $ref_column = $_POST['Template']['reference_column'][$count];
                        $items_model->temp_id = $model->template_id;
                        $items_model->ref_column = $ref_column;
                        $items_model->item_name = $item_name;
                        $items_model->purchase_item =  $template_purchase_item;
                        $items_model->created_by = Yii::app()->user->id;
                        $items_model->created_date = date('Y-m-d H:i:s');
                        $items_model->updated_by = Yii::app()->user->id;
                        $items_model->updated_date = date('Y-m-d H:i:s');
                        $items_model->save();
                    }
                }
            }
            Yii::app()->user->setFlash('success', 'Successfully updated.');
        }
        $account_items = $this->getAccountItems();
        $data = TemplateItems::model()->findAll(array("condition" => "temp_id = '" . $id . "'", "order" => "id DESC"));
        $this->render('update', array(
            'model' => $model,
            'account_items' => $account_items,
            'data' => $data
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        // $dataProvider=new CActiveDataProvider('Template');
        // $this->render('index',array(
        // 	'dataProvider'=>$dataProvider,
        // ));

        $model = new Template('search');
        $model->unsetAttributes();
        if (isset($_GET['Template'])) {
            $model->attributes = $_GET['Template'];
        }
        $this->render('index', array('model' => $model));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Template('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Template']))
            $model->attributes = $_GET['Template'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Template::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'template-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function getAccountItems()
    {

        $data = array();
        $final = array();


        $spec_sql = "SELECT id, specification, brand_id, cat_id "
            . " FROM jp_specification ";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();

        foreach ($specification as $key => $value) {
            $brand = '';
            if ($value['brand_id'] != NULL) {
                $brand_sql = "SELECT brand_name "
                    . " FROM jp_brand "
                    . " WHERE id=" . $value['brand_id'] . "";
                $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            }

            if ($value['cat_id'] != "") {
                $result = $this->GetParent($value['cat_id']);
            }
            $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
            $final[$key]['id']   = $value['id'];
        }
        return $final;
    }
    public function GetParent($id)
    {

        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM jp_purchase_category WHERE id=" . $id . "")->queryRow();
        return $category;
    }
    public function actionUpdateTemplateItem()
    {
        if (isset($_POST['item_id'])) {
            $item_id = $_POST['item_id'];
            $items = TemplateItems::model()->findByPK($item_id);
            if (!empty($items)) {
                $result_array = array(
                    'status' => 1, 'item_name' => $items->item_name,
                    'purchase_item' => $items->purchase_item,
                    'ref_column' => $items->ref_column,
                    'id' => $items->id
                );
                echo  json_encode($result_array);
            }
        }
    }
    public function actioncheckDuplicate()
    {
        if (isset($_POST)) {

            $ref_col = $_POST['ref_col'];
            $template_id = $_POST['template_id'];

            $criteria = new CDbCriteria();
            $criteria->condition = "ref_column = '" . $ref_col . "' AND temp_id = '" . $template_id . " ' ";
            $reference_column = TemplateItems::model()->findAll($criteria);

            $count = count($reference_column);
            echo $count;
        }
    }
    public function actiontemplate_excel($id)
    {

        $data = TemplateItems::model()->findAll(
            array("condition"=>"temp_id =  $id"));

            $objPHPExcel = new PHPExcel();
     $cell=1;
       foreach($data as $datas)
       {
        $objPHPExcel->getActiveSheet()->getColumnDimension($datas->ref_column)
        ->setAutoSize(true);
    $objPHPExcel->getActiveSheet()
        ->getCell($datas->ref_column . $cell)
        ->setValue($datas->item_name);
   
       }

       
      
         ob_end_clean();
            ob_start();
    
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Reports.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }
}
