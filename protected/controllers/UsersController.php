<?php

class UsersController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    private $access_rules = array();

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xEBF4FB,
                'fontFile' => dirname(dirname(__FILE__)) . '\extensions\fonts\nimbus.ttf',
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;

        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('passchange', 'myProfile', 'Editprofile', 'usershift', 'changeprojectsession', 'assignprojectsession', 'changetrigger'),
                'users' => array('@'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionEditprofile()
    {
        $model = $this->loadModel(Yii::app()->user->id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];

            $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET first_name='" . $model->first_name . "',last_name='" . $model->last_name . "',email='" . $model->email . "', last_modified=now() WHERE userid=" . Yii::app()->user->id);

            if ($command->execute())
                Yii::app()->user->setFlash('success', 'Successfully updated.');
            else
                Yii::app()->user->setFlash('error', 'Update failed. Please try again.');

            $this->redirect(array('myprofile'));
        }

        $this->render('editprofile', array(
            'model' => $model,
        ));
    }

    public function actionUsershift()
    {
        $sftuser = Users::model()->findByPk($_POST['shift_userid']);
        //Set current user to new user details
        Yii::app()->user->id = $_POST['shift_userid'];
        Yii::app()->user->role = $sftuser->user_type;
        Yii::app()->user->firstname = $sftuser->first_name;
        Yii::app()->user->name = $sftuser->username;
        $assigned_tasks = Tasks::model()->find(array('condition' => 'assigned_to =' . $_POST['shift_userid'] . ' AND trash =1', 'order' => 'tskid DESC'));
        if ($assigned_tasks != null) {
            Yii::app()->user->setState('project_id', $assigned_tasks->project_id);
        } else {
            Yii::app()->user->setState('project_id', "");
        }

        $process = Yii::app()->createController('Site'); //create instance of FirstController
        $process = $process[0];
        $process->menupermissions(); //call function 


        echo "session_changed";
    }

    public function actionPasschange_old()
    {
        $model = new Users;

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                $newpasword = md5(base64_encode($_POST['Users']['password']));
                $old_password = md5(base64_encode($_POST['Users']['old_password']));

                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET password='$newpasword', last_modified=now() WHERE userid = '" . Yii::app()->user->id . "' AND password='$old_password'");
                if ($command->execute()) {
                    Yii::app()->user->setFlash('success', 'Your new password updated successfully.');
                    $this->redirect(array('users/myprofile'));
                } else {
                    Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ');
                    $this->redirect(array('users/message_page'));
                }
                Yii::app()->end();
            }
        }
        $this->render("_change_pwd_form", array('model' => $model));
        Yii::app()->end();
    }

    public function actionPasschange()
    {
        $model = new Users;
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                $newpasword = md5(base64_encode($_POST['Users']['password']));
                $old_password = md5(base64_encode($_POST['Users']['old_password']));
                $is_password_changed = ($newpasword != $old_password) ? 1 : 0;
                $id = Yii::app()->user->id;
                $transaction = Yii::app()->db->beginTransaction();
                try {

                    $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET password='$newpasword', last_modified=now() WHERE userid = '" . Yii::app()->user->id . "' AND password='$old_password'");


                    if ($command->execute()) {

                        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {

                            if ($is_password_changed) {

                                $allowed_apps = Controller::getAllowedApps($id);

                                $globalcommand = Yii::app()->db->createCommand("UPDATE global_users SET passwd='$newpasword',updated_by='" . Yii::app()->user->id . "', updated_date=now() WHERE global_user_id = '" . Yii::app()->user->id . "' AND passwd='$old_password'");
                                if ($globalcommand->execute()) {

                                    if (is_array($allowed_apps)) {
                                        foreach ($allowed_apps as $value) {
                                            $value = strtoupper($value);
                                            $query = "SELECT table_prefix FROM global_settings "
                                                . "WHERE access_types = '" . $value . "'";
                                            $command = Yii::app()->db->createCommand($query);
                                            $result = $command->queryRow();
                                            switch ($value) {

                                                case 'HRMS':
                                                    $user_exist = Yii::app()->db->createCommand()
                                                        ->select('*')
                                                        ->from("{$result['table_prefix']}employee")
                                                        ->where('emp_id=:emp_id', array(':emp_id' => $id))
                                                        ->queryRow();
                                                    if (!empty($user_exist)) {
                                                        $sql =  "UPDATE " . $result['table_prefix'] . "employee "
                                                            . " SET passwd = '" . $newpasword . "' WHERE emp_id = " . $id;
                                                        $hrmscommand = Yii::app()->db->createCommand($sql);
                                                        if (!$hrmscommand->execute()) {
                                                            throw new Exception('some error occured');
                                                        }
                                                    }
                                                    break;

                                                case 'ACCOUNTS':
                                                    $user_exist = Yii::app()->db->createCommand()
                                                        ->select('*')
                                                        ->from("{$result['table_prefix']}users")
                                                        ->where('userid=:userid', array(':userid' => $id))
                                                        ->queryRow();
                                                    if (!empty($user_exist)) {
                                                        $sql =  "UPDATE " . $result['table_prefix'] . "users "
                                                            . " SET password = '" . $newpasword . "' WHERE userid = " . $id;
                                                        $pmscommand = Yii::app()->db->createCommand($sql);
                                                        if (!$pmscommand->execute()) {
                                                            throw new Exception('some error occured');
                                                        }
                                                    }
                                                    break;
                                                case 'CRM':
                                                    break;
                                            }
                                        }
                                    }
                                } else {
                                    throw new Exception('some error occured');
                                }
                            }
                        }

                        $transaction->commit();
                        Yii::app()->user->setFlash('success', 'Your new password updated successfully');
                        $this->redirect(array('users/myprofile'));
                    } else {
                        Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ');
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ');
                }
                $this->redirect(array('users/passchange'));
                Yii::app()->end();
            }
        }
        $this->render("_change_pwd_form", array('model' => $model));
        Yii::app()->end();
    }

    public function actionMessage_page()
    {
        $this->render('message_page');
    }

    public function actionMyProfile()
    {
        $model = new Users;
        $myid = Yii::app()->user->id;
        $this->render('myprofile', array('model' => $this->loadModel($myid),));
    }

    public function actionPwdreset()
    {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('/'));
        }

        $model = new Users;
        $show_form = 0;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-pwdrest-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Users'])) {
            $show_form = 2;
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {

                $vkey_email = (substr($_GET['vkey'], 32));

                $newpasword = md5(base64_encode($_POST['Users']['new_pwd']));

                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET password='$newpasword', last_modified=now() WHERE md5( concat( \"" . Yii::app()->params['enc_key'] . "\", \"^\", `email` )) = '$vkey_email'");
                if ($command->execute()) {
                    Yii::app()->user->setFlash('success', 'Your new password updated successfully. Now you can login with your new password.');
                    $this->redirect(array('site/login'));
                } else {
                    Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ' . CHtml::link('Please try again', array('users/pwdrecovery')) . ' OR ' . CHtml::link('Go Home', array('/')));
                    $this->redirect(array('users/message_page'));
                }
                Yii::app()->end();
            }
        }

        if (isset($_GET['vkey']) && strlen($_GET['vkey']) == 64) {

            $vkey_email = (substr($_GET['vkey'], 32));
            $verification_key = (substr($_GET['vkey'], 0, 32));

            $row = Users::model()->find("status = 1 AND email_activation=1 AND md5( concat( \"" . Yii::app()->params['enc_key'] . "\", \"^\", `email` )) = '$vkey_email'");

            if (count($row) == 1) {
                $vkey_match = md5(base64_encode($row->email . "-" . $row->activation_key . "-" . $row->last_modified));
                if ($vkey_match == $verification_key) {
                    $show_form = 1;
                    $this->render('pwdreset', array('model' => $model, 'act_status' => $show_form));
                }
            }
        }

        if ($show_form == 0) {
            Yii::app()->user->setFlash('error', "Verification code is not matching with any record. Please try again.");
            $this->redirect(array('users/pwdrecovery'));
            Yii::app()->end();
        } elseif ($show_form == 2) {
            $this->render('pwdreset', array('model' => $model));
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id),));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Users;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {

            $current_date = date('Y-m-d H:i:s');

            $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));
            $model->attributes = $_POST['Users'];

            $model->reg_ip = $_SERVER['REMOTE_ADDR'];
            $model->reg_date = $current_date;
            $activation_key = md5(base64_encode($_POST['Users']['email'] . "-" . Yii::app()->params['enc_key']));
            $model->activation_key = $activation_key;
            $model->email_activation = 1;

            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionadduser()
    {
        $model = new Users;
        $model->username = '';
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {


            $current_date = date('Y-m-d H:i:s');

            $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));
            $model->attributes = $_POST['Users'];
            if ($model->type == 2) {
                $model->employee_id = NULL;
                $model->username = NULL;
                $model->password = NULL;
            }
            $model->reg_ip = $_SERVER['REMOTE_ADDR'];
            $model->reg_date = $current_date;
            $activation_key = md5(base64_encode($_POST['Users']['email'] . "-" . Yii::app()->params['enc_key']));
            $model->activation_key = $activation_key;
            $model->email_activation = 1;
            if ($_POST['Users']['user_type'] == 11) {
                $model->pms_access = NULL;
                $model->tms_access = NULL;
                $model->wpr_access = NULL;
                $model->username = NULL;
                $model->password = NULL;
            } else {
                $model->pms_access = NULL;
                $model->tms_access = NULL;
                $model->wpr_access = NULL;
            }


            $model->created_by = yii::app()->user->id;
            $model->created_date = $this->datetime();
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = $this->datetime();

            if ($model->save()) {

                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    Yii::app()->user->setFlash('success', 'Created successfully');
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index', 'id' => $model->userid));
                }
            }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])) {

            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate_old($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {

            if (trim($_POST['Users']['password']) == '')
                $_POST['Users']['password'] = $model->password;
            else
                $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));

            $model->attributes = $_POST['Users'];
            if ($model->type != $_POST['Users']['type']) {
                $criteria = new CDbCriteria;
                $condition = $id . " IN(created_by,assigned_to,report_to,coordinator)";
                $criteria->addCondition($condition);
                $tasks_assigned = count(Tasks::model()->findAll($criteria));
                if ($tasks_assigned > 0) {
                    Yii::app()->user->setFlash('error', 'There are ' . $tasks_assigned .  ' task(s) assigned to this user,So unable to update Type.');
                } else {
                    $model->type = $_POST['Users']['type'];
                }
            }

            if ($_POST['Users']['user_type'] == 11) {
                $model->pms_access = NULL;
                $model->tms_access = NULL;
                $model->wpr_access = NULL;
                // $model->username = NULL;
                // $model->password = NULL;
            } else {

                $model->pms_access = NULL;
                $model->tms_access = NULL;
                $model->wpr_access = NULL;
            }

            if ($model->validate()) {
                if ($model->save()) {
                    $this->redirect(array('index'));
                } else {
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);
        if (isset($_POST['Users'])) {

            $is_login_changed = 0;

            if ((trim($_POST['Users']['password']) != '' && (md5(base64_encode($_POST['Users']['password'])) != $model->password)) || ($model->username != $_POST['Users']['username'])) {
                $is_login_changed = 1;
            }



            if (trim($_POST['Users']['password']) == '')
                $_POST['Users']['password'] = $model->password;
            else
                $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));

            $model->attributes = $_POST['Users'];

            $transaction = Yii::app()->db->beginTransaction();

            try {
                if ($model->type != $_POST['Users']['type']) {
                    $criteria = new CDbCriteria;
                    $condition = $id . " IN(created_by,assigned_to,report_to,coordinator)";
                    $criteria->addCondition($condition);
                    $tasks_assigned = count(Tasks::model()->findAll($criteria));
                    if ($tasks_assigned > 0) {
                        Yii::app()->user->setFlash('error', 'There are ' . $tasks_assigned .  ' task(s) assigned to this user,So unable to update Type.');
                    } else {
                        $model->type = $_POST['Users']['type'];
                    }
                }

                if ($_POST['Users']['user_type'] == 11) {
                    $model->pms_access = NULL;
                    $model->tms_access = NULL;
                    $model->wpr_access = NULL;
                } else {

                    $model->pms_access = NULL;
                    $model->tms_access = NULL;
                    $model->wpr_access = NULL;
                }

                if ($model->validate()) {
                    $model->wpr_access = NULL;
                    if ($model->save()) {

                        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
                            if ($is_login_changed) {
                                $globalmodel = GlobalUsers::model()->findByPk($id);

                                $allowed_apps = Controller::getAllowedApps($id);

                                $globalmodel->username = isset($_POST['Users']['username']) ? $_POST['Users']['username'] : $globalmodel->username;
                                $globalmodel->passwd = $_POST['Users']['password'];
                                $globalmodel->firstname = isset($_POST['Users']['first_name']) ? $_POST['Users']['first_name'] : $globalmodel->firstname;

                                if ($globalmodel->save()) {
                                    if (is_array($allowed_apps)) {
                                        foreach ($allowed_apps as $value) {
                                            $value = strtoupper($value);
                                            $query = "SELECT table_prefix FROM global_settings "
                                                . "WHERE access_types = '" . $value . "'";
                                            $command = Yii::app()->db->createCommand($query);
                                            $result = $command->queryRow();

                                            switch ($value) {

                                                case 'HRMS':
                                                    $user_exist = Yii::app()->db->createCommand()
                                                        ->select('*')
                                                        ->from("{$result['table_prefix']}employee")
                                                        ->where('emp_id=:emp_id', array(':emp_id' => $id))
                                                        ->queryRow();
                                                    if (!empty($user_exist)) {
                                                        $sql =  "UPDATE " . $result['table_prefix'] . "employee "
                                                            . "SET username = '" . $globalmodel->username . "'"
                                                            . " , passwd = '" . $globalmodel->passwd . "' WHERE emp_id = " . $id;
                                                        $hrmsusermodel = Yii::app()->db->createCommand($sql);
                                                        if (!$hrmsusermodel->execute()) {
                                                            throw new Exception("Some error occured");
                                                        }
                                                    }
                                                    break;

                                                case 'ACCOUNTS':

                                                    $user_exist = Yii::app()->db->createCommand()
                                                        ->select('*')
                                                        ->from("{$result['table_prefix']}users")
                                                        ->where('userid=:userid', array(':userid' => $id))
                                                        ->queryRow();
                                                    if (!empty($user_exist)) {
                                                        $sql =  "UPDATE " . $result['table_prefix'] . "users "
                                                            . "SET username = '" . $globalmodel->username . "'"
                                                            . " , password = '" . $globalmodel->passwd . "' WHERE userid = " . $id;
                                                        $pmsusermodel = Yii::app()->db->createCommand($sql);
                                                        if (!$pmsusermodel->execute()) {
                                                            throw new Exception("Some error occured");
                                                        }
                                                    }
                                                    break;

                                                case 'CRM':
                                                    break;
                                            }
                                        }
                                    }
                                } else {
                                    throw new Exception($globalmodel->getErrors());
                                }
                            }
                        }
                    } else {
                        throw new Exception($model->getErrors());
                    }
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', "Updated successfully");
                    $this->redirect(array('index'));
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('error', "An error occurred!");
                $this->redirect(array('index'));
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {

        //$this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        //if (!isset($_GET['ajax']))
        try {
            $criteria = new CDbCriteria;
            $condition = $id . " IN(created_by,assigned_to,report_to,coordinator)";
            $criteria->addCondition($condition);
            $task_assigned = count(Tasks::model()->findAll($criteria));
            if ($task_assigned > 0) {
                Yii::app()->user->setFlash('error', 'There are ' . $task_assigned .  ' task(s) assigned to this user,So unable to delete.');
            } else {
                $this->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', 'Successfully deleted.');
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('error', 'Cannot delete due to foreign key constraint fails.');
        }
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        // new filter back and forth
        $class_name = get_class($model);
        if (isset($_GET[$class_name])) {
            Yii::app()->user->setState('prevfilter', $_GET[$class_name]);
        }

        if (isset(Yii::app()->user->prevfilter)) {
            $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 

        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionUserlog()
    {



        $model = new Users('search');
        $model->unsetAttributes();
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('userlog', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAutocomplete()
    {
        $res = array();

        if (isset($_GET['term'])) {

            $qtxt = "SELECT CONCAT_WS(' ',first_name,last_name) as full_name FROM {{users}} WHERE first_name LIKE :first_name";
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":first_name", '%' . $_GET['term'] . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    public function actionAutouser()
    {
        $res = array();

        if (isset($_GET['term'])) {

            $qtxt = "SELECT username as full_name FROM {{users}} WHERE username LIKE :username";
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":username", '%' . $_GET['term'] . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    public function actionAutoemail()
    {
        $res = array();

        if (isset($_GET['term'])) {

            $qtxt = "SELECT email as email FROM {{users}} WHERE email LIKE :email";
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":email", '%' . $_GET['term'] . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    public function actionAutodesig()
    {
        $res = array();

        if (isset($_GET['term'])) {

            $qtxt = "SELECT designation as designation FROM {{users}} WHERE designation LIKE :designation ";
            print_r($qtxt);
            exit;
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":designation", '%' . $_GET['term'] . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    public function actionAutoemployee()
    {
        $res = array();

        if (isset($_GET['term'])) {

            $qtxt = "SELECT employee_id as employee_id FROM {{users}} WHERE employee_id LIKE :employee_id";
            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":employee_id", '%' . $_GET['term'] . '%', PDO::PARAM_STR);
            $res = $command->queryColumn();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

    public function actionchangeprojectsession()
    {
        $project_id = $_REQUEST['project_id'];
        $projects = Projects::model()->findByPk($project_id);
        if ($projects != null) {
            Yii::app()->user->setState('project_id', $projects->pid);
        } else {
            Yii::app()->user->setState('project_id', "");
        }
        Yii::app()->user->setState('trigger_status', 0);
    }

    public function actionassignprojectsession()
    {
        $project_id = $_REQUEST['project_id'];
        $projects = Projects::model()->findByPk($project_id);
        if ($projects != null) {
            Yii::app()->user->setState('project_id', $projects->pid);
        } else {
            Yii::app()->user->setState('project_id', "");
        }
        if (isset(Yii::app()->session['redirect_url'])) {
            echo Yii::app()->session['redirect_url'];
        }
        Yii::app()->user->setState('trigger_status', 0);
    }

    public function actionchangetrigger()
    {
        $status = $_REQUEST['status'];
        if ($status == 1) {
            Yii::app()->user->setState('trigger_status', 0);
        }
    }
}
