<?php

Yii::app()->getModule('masters');

class ReportsController extends Controller
{

  public $layout = '//layouts/column2';

  public function filters()
  {
    return array(
      'accessControl', // perform access control for CRUD operations
      'postOnly + delete', // we only allow deletion via POST request
    );
  }

  public function accessRules()
  {
    $accessArr = array();
    $accessauthArr = array();
    $accessguestArr = array();
    $controller = Yii::app()->controller->id;


    if (isset(Yii::app()->session['menuauth'])) {
      if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
        $accessauthArr = Yii::app()->session['menuauth'][$controller];
      }
    }

    $access_privlg = count($accessauthArr);

    return array(
      array(
        'allow', // allow admin user to perform 'admin' and 'delete' actions
        'actions' => $accessauthArr,
        'users' => array('@'),
        'expression' => "$access_privlg > 0",
      ),
      array(
        'allow', // allow admin user to perform 'admin' and 'delete' actions
        'actions' => array('finereport', 'photoPunchReport', 'photoPunchReportAction', 'minutesview', 'projectreport', 'projectreportview', 'projectreportpdf', 'getAreadetails', 'meetingpdf', 'mom', 'momview', 'momPdf', 'weeklyReport', 'generateDailyProgressReport', 'view_daily_progress_report', 'daily_progress_report_pdf', 'weekly_report_view', 'weekly_report_pdf', 'generateprojectreport', 'onhold_report', 'update_weekly_report'),
        'users' => array('@'),
      ),
      array(
        'deny', // deny all users
        'users' => array('*'),
      ),
    );
  }

  public function actionFinereport()
  {

    $date_from = date('Y-m-01');
    $date_till = date('Y-m-d', strtotime("-1 days"));
    if (isset($_GET['sdate']) and $_GET['sdate'] != '') {
      $date_from = date('Y-m-d', strtotime($_GET['sdate']));
    }
    if (isset($_GET['edate']) and $_GET['edate'] != '') {
      $date_till = date('Y-m-d', strtotime($_GET['edate']));
    }

    $code = 0;

    $codelan = '';

    if (isset($_GET['code']) && $_GET['code'] != '') {

      $code = $_GET['code'];
      $code = implode(',', $code);
      $codelan = ' and userid in ( ' . $code . ' ) ';
    }




    $punchreport_sql = "SELECT rep.*,concat_ws(' ',first_name, last_name) as name FROM `pms_punchreport` as rep left join pms_users as emp on emp.userid = rep.resource_id WHERE `logdate` between   '" . $date_from . "' and '" . $date_till . "'  $codelan";
    $punchreport = Yii::app()->db->createCommand($punchreport_sql)->queryAll();

    //    print_r( $punchreport);
    $result = array();

    foreach ($punchreport as $key => $value) {
      extract($value);
      $date = date('d-m-Y', strtotime($logdate));
      $result[$resource_id][$date] = array(
        'name' => $name,
        'first' => ($firstpunch != '-') ? date('H:i:s', strtotime($firstpunch)) : 'No punch',
        'last' => ($lastpunch != '-') ? date('H:i:s', strtotime($lastpunch)) : 'No punch',
        'in' => $intime,
        'out' => $outtime,
        'tot' => $totaltime,
        'userid' => $resource_id,
        'tot_entry' => $total_punch,
        'absol_punch' => $total_punch,
        'missed_punch' => 0,
      );
    }

    $holidays = Yii::app()->db->createCommand("select date_format(`holiday_date`, '%d-%m-%Y') as 'date' from pms_holidays where `holiday_date` between '{$date_from}' and '{$date_till}'")->queryAll();

    $allhol = array();
    $attentrylist = array();
    foreach ($holidays as $hol) {
      $allhol[] = $hol['date'];
    }
    $datetime1 = new DateTime($date_from);
    $datetime2 = new DateTime($date_till);
    $interval = $datetime1->diff($datetime2);
    $interval->format('%R%a days');

    $data = array(
      'final_result' => $result,
      'interval' => $interval,
      'date_from' => $date_from,
      'datetill' => $date_till,
      'holiday' => $allhol,
      'code' => $code
    );

    if (isset($_GET['exportpdf'])) {
      $this->export_reportpdf($data, $date_from, $date_till);
    }

    if (isset($_GET['exportexcel'])) {
      $this->export_excel($data, $date_till);
    }

    $this->render('finereport', $data);
  }

  public function export_reportpdf($data, $date_from, $date_till)
  {


    $data = array(
      'final_result' => $data['final_result'],
      'interval' => $data['interval'],
      'date_from' => $data['date_from'],
      'datetill' => $data['datetill'],
      'holiday' => $data['holiday'],
      'code' => $data['code']
    );
    $site_file = "";
    if (isset(Yii::app()->user->company_id)) {

      $sql = "SELECT `name` FROM `pms_company` WHERE company_id =" . Yii::app()->user->company_id;
      $user_sites = Yii::app()->db->createCommand($sql)->queryAll();
      foreach ($user_sites as $site) {
        $site_file = $site['name'];
      }
    }


    $mPDF1 = Yii::app()->ePdf->mPDF();
    $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4-L');
    $mPDF1->WriteHTML($stylesheet, 1);
    $data = $this->renderPartial('report_pdf', $data, true);
    $mPDF1->WriteHTML($data);
    $filename = "Punching Report_" . $date_from . "_" . $date_till . "_" . $site_file;
    $mPDF1->Output($filename . '.pdf', 'D');
  }

  public function export_excel($data, $date_till)
  {

    $interval = $data['interval'];
    $date_from = $data['date_from'];
    $final_result = $data['final_result'];
    $holiday = $data['holiday'];

    $site_file = "";
    if (isset(Yii::app()->user->company_id)) {

      $sql = "SELECT `name` FROM `pms_company` WHERE company_id =" . Yii::app()->user->company_id;
      $user_sites = Yii::app()->db->createCommand($sql)->queryAll();
      foreach ($user_sites as $site) {
        $site_file = $site['name'];
      }
    }


    $finaldata = array();
    $arraylabel = array('', '', 'Punching Report', '', '', '', '');

    $finaldata[0][] = ' Resource Name';
    for ($i = 0; $i < $interval->days + 1; $i++) {
      $datehead = new DateTime($date_from);
      $datehead->add(new DateInterval('P' . $i . 'D'));
      $labelhead = (string) $datehead->format('d-m-Y');

      $finaldata[0][] = $labelhead;
    }

    $k = 1;
    foreach ($final_result as $userid => $result) {
      foreach ($result as $pudate => $fitem) {
        $m = 0;
        if ($m == 0) {
          $finaldata[$k][] = $fitem['name'];
        }
        $finaldata[$k + 1][] = "First Punch";
        $finaldata[$k + 2][] = "Last Punch";
        $finaldata[$k + 3][] = "IN Time";
        $finaldata[$k + 4][] = "OUT Time";
        $finaldata[$k + 5][] = "Total Time";
        $finaldata[$k + 6][] = "Total Punches";
        $finaldata[$k + 7][] = "Missed Punches";
        $finaldata[$k + 8][] = "";

        $resdate = array_keys($result);
        for ($i = 0; $i <= $interval->days; $i++) {

          $date = new DateTime($date_from);
          $date->add(new DateInterval('P' . $i . 'D'));
          $caldate = (string) $date->format('d-m-Y');
          $pundate = (string) $pudate;
          if (in_array($caldate, $holiday) == 1 && in_array($caldate, $resdate) != 1) {
            $finaldata[$k + 1][] = "H";
            $finaldata[$k + 2][] = "H";
            $finaldata[$k + 3][] = "H";
            $finaldata[$k + 4][] = "H";
            $finaldata[$k + 5][] = "H";
            $finaldata[$k + 6][] = "H";
            $finaldata[$k + 7][] = "H";
            $finaldata[$k + 8][] = "";
          } else {
            $nopunch = 'No Punch';
            $finaldata[$k + 1][] = (in_array($caldate, $resdate) == 1) ? $result[$caldate]['first'] : $nopunch;
            $finaldata[$k + 2][] = (in_array($caldate, $resdate) == 1) ? $result[$caldate]['last'] : $nopunch;
            $finaldata[$k + 3][] = (in_array($caldate, $resdate) == 1) ? gmdate('H:i:s', $result[$caldate]['in']) : $nopunch;
            $finaldata[$k + 4][] = (in_array($caldate, $resdate) == 1) ? gmdate('H:i:s', $result[$caldate]['out']) : $nopunch;
            $finaldata[$k + 5][] = (in_array($caldate, $resdate) == 1) ? gmdate('H:i:s', $result[$caldate]['tot']) : $nopunch;
            $finaldata[$k + 6][] = (in_array($caldate, $resdate) == 1) ? intval($result[$caldate]['absol_punch']) : $nopunch;
            $finaldata[$k + 7][] = (in_array($caldate, $resdate) == 1 && $result[$caldate]['missed_punch'] == 1) ? 1 : "";
            $finaldata[$k + 8][] = "";
          }


          $m++;
        }
        break;
      }

      $k = $k + 9;
    }

    // echo '<pre>';print_r($finaldata);exit;

    Yii::import('ext.ECSVExport');
    $csv = new ECSVExport($finaldata);
    $csv->setHeaders($arraylabel);
    $csv->setToAppend();

    $encode = "\xEF\xBB\xBF"; // UTF-8 BOM
    $contentdata = $encode . $csv->toCSV();

    $csvfilename = "Punching Report_" . $date_from . "_" . $date_till . "_" . $site_file . ".xls";
    Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/xls", false);
    exit();
  }

  public function actionMypunches($date = "")
  {
    $user = Yii::app()->user->id;
    $employee_id = Yii::app()->db->createCommand(
      "select employee_id, first_name from pms_users where userid = '" . $user . "'"
    )->queryRow();
    $this->render(
      'mypunches',
      array(
        'employee_id' => $employee_id,
      )
    );
  }

  public function actionphotoPunchReport($date = "")
  {
    if (isset($_REQUEST['reportdate'])) {
      $date = $_REQUEST['reportdate'];
    }

    if ($date != "") {
      $date = date("Y-m-d", strtotime($date));
    }
    if ($date == "") {
      $date = date("Y-m-d");
    }
    if (isset($_GET['type'])) {
      $status_type = $_GET['type'];
    }

    if (!isset($status_type) || $status_type == "") {
      $status_type = "All";
    }
    $model = new Photopunch();
    if (isset($_GET['Photopunch']))
      $model->attributes = $_GET['Photopunch'];
    // $model->unsetAttributes(); 
    $this->render(
      'photopunch',
      array(
        'model' => $model,
        'date' => $date,
        'status_type' => $status_type
      )
    );
  }

  public function actionphotoPunchReportAction()
  {
    $photo_tbl = Photopunch::model()->tableSchema->rawName;
    if (isset($_REQUEST['id'])) {
      extract($_REQUEST);
      $status = array('Approve' => 1, 'Reject' => 2);
      if ($req) {
        $sts = $status[$req];
        if ($sts == 1) {
          $finalstatus = 'Approved';
        } else {
          $finalstatus = 'Rejected';
        }

        $reason = $_REQUEST['reason'];
        $allids = $_REQUEST['id'];

        if (isset($_REQUEST['data_type'])) {
          $data_type = $_REQUEST['data_type'];
        }

        foreach ($allids as $key => $id) {

          $update_sql = "UPDATE $photo_tbl SET 
				`approved_status`=" . $sts . ",`reason`='" .
            $reason . "',`decision_by`=" . Yii::app()->user->id . ",
				`decision_date`='" . date('Y-m-d') . "'  WHERE `aid`=" . $id;
          Yii::app()->db->createCommand($update_sql)->execute();
        }



        $ret['msg'] = 'Photo punch ' . $finalstatus . ' Successfully';
        $ret['error'] = '';
      }


      echo json_encode($ret);
      exit;
    }
  }


  public function actionMeetingminutes()
  {

    $model = new SiteMeetings('search');
    $model->unsetAttributes();
    if (isset($_GET['SiteMeetings'])) {
      $model->attributes = $_GET['SiteMeetings'];
    }
    $this->render('meeting_index', array('model' => $model));
  }

  public function actionminutesview($id)
  {
    $model = array();
    if (!empty($id)) {
      $model = SiteMeetings::model()->findByPk($id);
      $project_det = Projects::model()->findByPk($model->project_id);
      $participants = MeetingParticipants::model()->findAll(['condition' => 'meeting_id = ' . $id]);
      $minutes_points = MeetingMinutes::model()->findAll(['condition' => 'meeting_id = ' . $id]);
    }
    $this->render(
      'minutes_view',
      array(
        'model' => $model,
        'participants' => $participants,
        'minutes_points' => $minutes_points,
        'project' => $project_det,
        'pdf' => 2
      )
    );
  }

  public function actionprojectReport()
  {


    $model = new ProjectReport;

    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['ProjectReport'])) {
      $model->attributes = $_GET['ProjectReport'];
    }
    // if (isset($_REQUEST['ProjectReport'])) {
    //   $model->attributes = $_REQUEST['ProjectReport'];
    //   $model->from_date = date('Y-m-d', strtotime(($model->from_date)));
    //   $model->to_date = date('Y-m-d', strtotime(($model->to_date)));
    //   $model->created_by = Yii::app()->user->id;
    //   $model->updated_by = Yii::app()->user->id;
    //   $model->created_date = date('Y-m-d');
    //   $model->updated_date = date('Y-m-d');
    //   $criteria = new CDbCriteria;
    //   $criteria->addBetweenCondition("start_date", $model->from_date, $model->to_date, 'AND');
    //   $criteria->addBetweenCondition("due_date", $model->from_date, $model->to_date, 'OR');
    //   $criteria->compare('project_id', $model->project_id);
    //   if ($model->report_type == 1) {

    //     $criteria->group = 'contractor_id';
    //     $project_tasks = Tasks::model()->findAll($criteria);
    //     $result_data = $this->getConsolidatedResult($project_tasks, $model->project_id);
    //   } elseif ($model->report_type  == 2) {
    //     $project_tasks = Tasks::model()->findAll($criteria);
    //     $result_data = $this->getDetailedResult($project_tasks, $model->project_id, $model->from_date, $model->to_date);
    //   } elseif ($model->report_type  == 3) {
    //     $criteria->compare('status', 9);
    //     $project_tasks = Tasks::model()->findAll($criteria);
    //     $result_data = '';
    //   }

    //   $model->report_data = json_encode($result_data);
    //   if ($model->save()) {
    //     $model = new ProjectReport;
    //     Yii::app()->user->setFlash('success', "Report successfully generated");
    //   }
    // }


    $this->render('project_report', array('model' => $model));
  }

  public function getDetailedResult($project_tasks, $project_id, $from_date, $to_date)
  {
    $result_array = array();
    if (!empty($project_tasks)) {
      foreach ($project_tasks as $task) {
        $data = array();
        $data['project_id'] = $task->project_id;
        $data['contractor_id'] = $task->contractor_id;
        // $data['contractor_title'] = $task->contractor->contractor_title;
        if (isset($task->contractor_id)) {
          $data['contractor_title'] = $task->contractor->contractor_title;
        } else {
          $data['contractor_title'] = '';
        }
        $data['milestone_id'] = $task->milestone_id;
        $data['milestone_title'] = $task->milestone->milestone_title;
        $data['task_title'] = $task->title;
        if (!empty($task->parent_tskid)) {
          $data['sub_task'] = $task->title;
          $parent_task_model = Tasks::model()->findByPk($task->parent_tskid);
          $data['task_title'] = $parent_task_model->title;
        }
        if (isset($task->area)) {
          $data['area'] = $task->area0->area_title;
        } else {
          $data['area'] = '';
        }
        if (isset($task->work_type_id)) {
          $data['work_type'] = $task->worktype->work_type;
        } else {
          $data['work_type'] = '';
        }

        $data['total_qty'] = $task->quantity;
        $data['achieved_qty'] = Tasks::model()->getCompletedQuantityByPeriod($task->tskid, $from_date, $to_date);
        $start_date = new DateTime($task->start_date);
        $due_date = new DateTime($to_date);
        if (strtotime($to_date) < strtotime($task->due_date)) {
          $due_date->modify('+1 day');

          $period = new DatePeriod(
            $start_date,
            new DateInterval('P1D'),
            $due_date
          );
          $duration = 0;
          foreach ($period as $key => $value) {
            $duration++;
          }
          $targeted_qty = round($duration * $task->daily_target);
        } else {
          $duration = $task->task_duration;
          $targeted_qty = $task->quantity;
        }
        $data['targeted_qty'] = $targeted_qty;
        $data['balance'] = $data['targeted_qty'] - $data['achieved_qty'];
        $data['progres_percentage'] = Tasks::model()->getCompletedPercentageByPeriod($task->tskid, $from_date, $to_date);
        $data['status'] = $task->status0->caption;
        $boq_desc = DailyWorkProgress::model()->find(array('condition' => 'taskid = ' . $task->tskid . ' AND approve_status = 1 AND (date >= "' . $from_date . '" AND date <= "' . $to_date . '") order by approved_date desc'));

        $data['description'] = !empty($boq_desc) ? $boq_desc->description : '';
        array_push($result_array, $data);
      }
      return $result_array;
    }
  }
  public function actionprojectReportView($id)
  {
    $task_model = new Tasks();
    if (!empty($id)) {
      $model = ProjectReport::model()->findByPk($id);

      if ($model->report_type == 3) {

        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition("start_date", $model->from_date, $model->to_date, 'AND');
        $criteria->addBetweenCondition("due_date", $model->from_date, $model->to_date, 'OR');
        $criteria->compare('project_id', $model->project_id);
        $criteria->addCondition('t.parent_tskid IS NULL');
        $all_project_tasks = Tasks::model()->findAll($criteria);
        $task_id = [];
        foreach ($all_project_tasks as $prj_task) {
          $task_id[] = $prj_task->tskid;
        }

        $condition = new CDbCriteria();
        $condition->addInCondition('parent_tskid', $task_id, 'AND');
        $condition->addInCondition('tskid', $task_id, 'OR');
        $condition->addInCondition('t.status', [6, 9]);
        $progress_tasks = Tasks::model()->findAll($condition);
        $tasks = array();
        $progress_task_id = [];
        foreach ($progress_tasks as $progressTask) {
          if ($progressTask->parent_tskid != NULL) {
            $progress_task_id[] = $progressTask['parent_tskid'];
          } else {
            $progress_task_id[] = $progressTask['tskid'];
          }
        }
        $condition1 = new CDbCriteria();
        $condition->join = 'LEFT JOIN pms_contractors ON pms_contractors.id = t.contractor_id';
        $condition1->addInCondition('parent_tskid', $progress_task_id, 'AND');
        $condition1->addInCondition('tskid', $progress_task_id, 'OR');
        $condition1->addInCondition('t.task_type', [1]);
        $in_progress_task = Tasks::model()->findAll($condition1);
        foreach ($in_progress_task as $task) {
          if (isset($task->contractor_id)) {
            $contractor = $task->contractor->contractor_title;
          } else {
            $contractor = '';
          }
          $arr = array(
            "id" => $task['tskid'],
            "parent_id" => $task['parent_tskid'],
            "title" => $task['title'],
            'start_date' => $task['start_date'],
            'due_date' => $task['due_date'],
            'contractor_name' => $contractor,
            'work_type_id' => $task['work_type_id'],
            'task_status' => $task['status'],
            'mile_stone_id' => $task['milestone_id'],
            'project_id' => $task['project_id'],
            'ranking' => $task['ranking'],
            'contractor_id' => $task->contractor_id
          );
          array_push($tasks, $arr);
        }
        $project_tasks = $tasks;
        $this->render(
          'project_progress_report_view',
          array(
            'model' => $model,
            'task_model' => $task_model,
            'project_tasks' => $project_tasks,
            'pdf' => 0
          )
        );
      } else {
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition(
          "start_date",
          $model->from_date,
          $model->to_date,
          'AND'
        );
        $criteria->addBetweenCondition(
          "due_date",
          $model->from_date,
          $model->to_date,
          'OR'
        );
        $criteria->compare('project_id', $model->project_id);
        $project_tasks = Tasks::model()->findAll($criteria);
        $milestone_datas = $this->getMilestones($project_tasks, $model->project_id);
        $this->render(
          'project_report_view',
          array(
            'model' => $model,
            'task_model' => $task_model,
            'project_tasks' => $project_tasks,
            'milestone_datas' => $milestone_datas,
            'pdf' => 0
          )
        );
      }
    }
  }

  public function getConsolidatedResult($project_tasks, $project_id)
  {
    $result_array = array();
    if (!empty($project_tasks)) {
      foreach ($project_tasks as $task) {
        $data = array();
        $data['project_id'] = $task->project_id;
        $data['contractor_id'] = $task->contractor_id;
        if (isset($task->contractor->contractor_title)) {
          $data['contractor_title'] = $task->contractor->contractor_title;
        }
        $data['milestone_id'] = $task->milestone_id;
        $data['milestone_title'] = $task->milestone->milestone_title;
        if (isset($task->contractor_id)) {
          $progress_data = Tasks::model()->contractorProgressPercentage($project_id, $task->contractor_id);
          $data['progress_perc'] = $progress_data['percentage'];
          $data['status'] = $progress_data['status'];
        }

        array_push($result_array, $data);
      }
    }
    return $result_array;
  }

  public function getMilestones($project_tasks, $project_id)
  {
    $milstone_data = array();
    foreach ($project_tasks as $task) {

      if (!array_key_exists($task->milestone_id, $milstone_data)) {
        if (isset($task->contractor_id)) {
          $contractor_title = $task->contractor->contractor_title;
        } else {
          $contractor_title = '';
        }
        $milstone_data[$task->milestone_id] = array('id' => $task->milestone_id, 'title' => $task->milestone->milestone_title, 'contractor' => $contractor_title, 'project_id' => $project_id, 'ranking' => $task->milestone->ranking);
        if (!empty($task->area)) {
          $area['title'] = $task->area0->area_title;
          $area['id'] = $task->area;
          $milstone_data[$task->milestone_id]['areas'][] = $area;
        } else {
          $milstone_data[$task->milestone_id]['areas'] = array();
        }
        $milstone_data[$task->milestone_id]['work_type'] = array();
        $milstone_data[$task->milestone_id]['work_type'] = $this->setProjectWorkTypes($milstone_data[$task->milestone_id]['work_type'], $project_id, $task->work_type_id, $task->milestone_id);
        if (isset($milstone_data[$task->milestone_id]['progress_data'])) {
          $milstone_data[$task->milestone_id]['progress_data'] = $this->getProgressPercentage($milstone_data[$task->milestone_id]['progress_data'], $task);
        }
      } else {
        if (isset($task->contractor_id)) {
          $contractor_title = $task->contractor->contractor_title;
        } else {
          $contractor_title = '';
        }
        $milstone_data[$task->milestone_id]['contractor'] .= $this->updateContractorTitle($milstone_data[$task->milestone_id]['contractor'], $contractor_title);
        if (isset($milstone_data[$task->milestone_id]['work_type'])) {
          $milstone_data[$task->milestone_id]['work_type'] = $this->setProjectWorkTypes($milstone_data[$task->milestone_id]['work_type'], $project_id, $task->work_type_id, $task->milestone_id);
        }
        if (isset($milstone_data[$task->milestone_id]['progress_data'])) {
          $milstone_data[$task->milestone_id]['progress_data'] = $this->getProgressPercentage($milstone_data[$task->milestone_id]['progress_data'], $task);

          array_push($milstone_data[$task->milestone_id]['task_datas'], $task->attributes);
        } else {
          $milstone_data[$task->milestone_id]['work_type'] = $this->setProjectWorkTypes($milstone_data[$task->milestone_id]['work_type'], $project_id, $task->work_type_id, $task->milestone_id);
        }
        if (!empty($task->area)) {
          $area_key = array_search($task->area, array_column($milstone_data[$task->milestone_id]['areas'], 'id'));
          if ($area_key === false) {
            $area['title'] = $task->area0->area_title;
            $area['id'] = $task->area;
            array_push($milstone_data[$task->milestone_id]['areas'], $area);
          }
        }
      }
    }
    $ranking = array_column($milstone_data, 'ranking');
    array_multisort($ranking, SORT_ASC, $milstone_data);
    return $milstone_data;
  }

  public function updateContractorTitle($contractor_list, $contractor_title)
  {
    $contractor_list = explode(',', $contractor_list);
    if (!in_array($contractor_title, $contractor_list)) {
      return ',' . $contractor_title;
    } else {
      return '';
    }
  }

  public function setProjectWorkTypes($milestone_data, $project_id, $work_type_id, $milestone_id)
  {

    if (empty($milestone_data)) {
      $milestone_data = array();
    }



    if (!isset($work_type_id)) {
      $project_work_types = ProjectWorkType::model()->findAll(array('condition' => 'project_id = ' . $project_id . ' AND work_type_id IS NULL'));
    } else {
      $project_work_types = ProjectWorkType::model()->findAll(array('condition' => 'project_id = ' . $project_id . ' AND work_type_id = ' . $work_type_id));
    }

    foreach ($project_work_types as $project_work_type) {


      $key = array_search($project_work_type->ranking, array_column($milestone_data, 'ranking'));
      $id_check = array_search($work_type_id, array_column($milestone_data, 'id'));
      if ($key === false) {
        $data = array('ranking' => $project_work_type->ranking, 'title' => $project_work_type->workType->work_type, 'id' => $project_work_type->work_type_id, 'status' => '1');
        array_push($milestone_data, $data);
      } else {
        foreach ($milestone_data as $key_val => $dat) {
          if ($id_check === false) {
            if ($dat['ranking'] == $project_work_type->ranking) {
              $milestone_data[$key_val]['status'] = '0';
            }
            $others_key = array_search('0', array_column($milestone_data, 'ranking'));
            if ($others_key === false && count($milestone_data) != 1) {
              $data = array('ranking' => '0', 'title' => 'others', 'id' => $work_type_id, 'status' => 1);
              array_push($milestone_data, $data);
            }
          }
        }
      }
    }


    // print_r($milestone_data);
    // echo '<pre>';
    // print_r($milestone_data);
    return $milestone_data;
  }

  public function getProgressPercentage($progress_data, $task)
  {
    $percentage = $task->getcompleted_percent($task->tskid);
    $progress_data[] = $percentage;

    return $progress_data;
  }

  public function actionprojectReportpdfold($id)
  {
    $task_model = new Tasks();
    $milestone_datas = '';
    if (!empty($id)) {
      $model = ProjectReport::model()->findByPk($id);
      if (!empty($model)) {
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition("start_date", $model->from_date, $model->to_date, 'AND');
        $criteria->addBetweenCondition("due_date", $model->from_date, $model->to_date, 'OR');
        $criteria->compare('project_id', $model->project_id);
        $project_tasks = Tasks::model()->findAll($criteria);
        $milestone_datas = $this->getMilestones($project_tasks, $model->project_id);
      }
    }

    $progress_report_pdf_parts = array();

    for ($i = 1; $i <= 3; $i++) {
      $tasklist = $this->renderPartial('project_report_view', array('model' => $model, 'milestone_datas' => $milestone_datas, 'task_model' => $task_model, 'filepart' => $i), true);
      $mPDF1 = Yii::app()->ePdf->mPDF();
      $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-P');
      $mPDF1->WriteHTML($stylesheet, 1);
      $mPDF1->WriteHTML($tasklist);
      $filename = "projectpdfexport/project_reprt_{$id}_{$i}.pdf";
      $mPDF1->Output($filename, 'D');
      //            @chmod($filename, 777);
      if ($i < 3 or (strpos($tasklist, 'No milestone worktype data') === false and $i == 3)) {
        $progress_report_pdf_parts[] = $filename;
      }
    }

    $report_range = 'project_progress_report-' . $model->from_date . "-" . $model->to_date . ".pdf";
    if (count($progress_report_pdf_parts)) {
      $this->mergePDFFiles($progress_report_pdf_parts, $report_range);
    }
  }
  public function actionprojectReportpdf($id)
  {
    $task_model = new Tasks();
    $progress_report_pdf_parts = array();
    if (!empty($id)) {
      $model = ProjectReport::model()->findByPk($id);
      if (!empty($model)) {
        if ($model->report_type != 3) {
          $criteria = new CDbCriteria;
          $criteria->addBetweenCondition("start_date", $model->from_date, $model->to_date, 'AND');
          $criteria->addBetweenCondition("due_date", $model->from_date, $model->to_date, 'OR');
          $criteria->compare('project_id', $model->project_id);
          $project_tasks = Tasks::model()->findAll($criteria);
          $milestone_datas = $this->getMilestones($project_tasks, $model->project_id);
          for ($i = 1; $i <= 3; $i++) {
            $tasklist = $this->renderPartial('project_report_view', array('model' => $model, 'milestone_datas' => $milestone_datas, 'task_model' => $task_model, 'filepart' => $i, 'pdf' => 1), true);
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-P');
            $mPDF1->WriteHTML($stylesheet, 1);
            $mPDF1->WriteHTML($tasklist);
            if ($model->report_type == 1) {
              $filename = "ProjectConsolidatedReport(" . date('d-m-Y', strtotime($model->from_date)) . " to " . date('d-m-Y', strtotime($model->to_date)) . ").pdf";
            } else {
              $filename = "ProjectDetailedReport(" . date('d-m-Y', strtotime($model->from_date)) . " to " . date('d-m-Y', strtotime($model->to_date)) . ").pdf";
            }
            // $filename = "projectpdfexport/project_reprt_{$id}_{$i}.pdf";
            $mPDF1->Output($filename, 'D');
            //            @chmod($filename, 777);
            if ($i < 3 or (strpos($tasklist, 'No milestone worktype data') === false and $i == 3)) {
              $progress_report_pdf_parts[] = $filename;
            }
          }

          $report_range = 'project_progress_report-' . $model->from_date . "-" . $model->to_date . ".pdf";
          if (count($progress_report_pdf_parts)) {
            $this->mergePDFFiles($progress_report_pdf_parts, $report_range);
          }
        } else {
          $criteria = new CDbCriteria;
          $criteria->addBetweenCondition("start_date", $model->from_date, $model->to_date, 'AND');
          $criteria->addBetweenCondition("due_date", $model->from_date, $model->to_date, 'OR');
          $criteria->compare('project_id', $model->project_id);
          $criteria->addCondition('t.parent_tskid IS NULL');
          $all_project_tasks = Tasks::model()->findAll($criteria);
          $task_id = [];
          foreach ($all_project_tasks as $prj_task) {
            $task_id[] = $prj_task->tskid;
          }

          $condition = new CDbCriteria();
          $condition->addInCondition('parent_tskid', $task_id, 'AND');
          $condition->addInCondition('tskid', $task_id, 'OR');
          $condition->addInCondition('t.status', [6, 9]);
          $progress_tasks = Tasks::model()->findAll($condition);
          $tasks = array();
          $progress_task_id = [];
          foreach ($progress_tasks as $progressTask) {
            if ($progressTask->parent_tskid != NULL) {
              $progress_task_id[] = $progressTask['parent_tskid'];
            } else {
              $progress_task_id[] = $progressTask['tskid'];
            }
          }
          $condition1 = new CDbCriteria();
          $condition->join = 'LEFT JOIN pms_contractors ON pms_contractors.id = t.contractor_id';
          $condition1->addInCondition('parent_tskid', $progress_task_id, 'AND');
          $condition1->addInCondition('tskid', $progress_task_id, 'OR');
          $condition1->addInCondition('t.task_type', [1]);
          $in_progress_task = Tasks::model()->findAll($condition1);
          foreach ($in_progress_task as $task) {
            if (isset($task->contractor_id)) {
              $contractor = $task->contractor->contractor_title;
            } else {
              $contractor = '';
            }
            $arr = array(
              "id" => $task['tskid'],
              "parent_id" => $task['parent_tskid'],
              "title" => $task['title'],
              'start_date' => $task['start_date'],
              'due_date' => $task['due_date'],
              'contractor_name' => $contractor,
              'work_type_id' => $task['work_type_id'],
              'task_status' => $task['status'],
              'mile_stone_id' => $task['milestone_id'],
              'project_id' => $task['project_id'],
              'ranking' => $task['ranking'],
              'contractor_id' => $task->contractor_id
            );
            array_push($tasks, $arr);
          }
          $project_tasks = $tasks;
          for ($i = 1; $i <= 3; $i++) {
            $tasklist =
              $this->render('project_progress_report_view', array(
                'model' => $model,
                'task_model' => $task_model,
                'project_tasks' => $project_tasks,
                'pdf' => 1
              ), true);
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-P');
            $mPDF1->WriteHTML($stylesheet, 1);
            $mPDF1->WriteHTML($tasklist);
            $filename = "ProjectProgressReport(" . date('d-m-Y', strtotime($model->from_date)) . " to " . date('d-m-Y', strtotime($model->to_date)) . ").pdf";
            $mPDF1->Output($filename, 'D');
          }
        }
      }
    }
  }
  private function mergePDFFiles(array $filenames, $outFile)
  {

    $mpdf = Yii::app()->ePdf->mPDF();

    if ($filenames) {

      $filesTotal = sizeof($filenames);
      $fileNumber = 1;

      $mpdf->SetImportUse();

      if (!file_exists($outFile)) {
        $handle = fopen($outFile, 'w');
        fclose($handle);
      }
      foreach ($filenames as $fileName) {
        if (file_exists($fileName)) {
          $pagesInFile = $mpdf->SetSourceFile($fileName);
          //print_r($fileName); die;
          for ($i = 1; $i <= $pagesInFile; $i++) {
            $tplId = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($tplId);
            if (($fileNumber < $filesTotal) || ($i != $pagesInFile)) {
              $mpdf->WriteHTML('<pagebreak />');
            }
          }
          //                    unlink($fileName);
        }
        $fileNumber++;
      }

      foreach ($filenames as $fileName) {
        if (file_exists($fileName)) {
          @unlink($fileName);
        }
      }

      $mpdf->Output($outFile, 'D');
    }
  }
  public function getAreadetails($task_id)
  {
    $task_table = Tasks::model()->tableSchema->name;
    $area_table = Area::model()->tableSchema->name;


    $area_sql = "SELECT area.*,tsk.tskid,tsk.work_type_id FROM $area_table as area left join $task_table  as tsk on tsk.area = area.id WHERE `tskid` =    $task_id  or parent_tskid = $task_id and task_type=1 GROUP BY tsk.area";
    $area = Yii::app()->db->createCommand($area_sql)->queryAll();
    return $area;
  }
  public function getworkType($task_id)
  {
    $task_table = Tasks::model()->tableSchema->name;
    $work_type_table = WorkType::model()->tableSchema->name;
    $worktype_sql = "SELECT work.*,tsk.* FROM $work_type_table as work left join $task_table  as tsk on tsk.work_type_id = work.wtid WHERE `tskid` =    $task_id  or parent_tskid = $task_id and task_type=1 GROUP BY tsk.work_type_id";
    $work_type = Yii::app()->db->createCommand($worktype_sql)->queryAll();
    return $work_type;
  }
  public function getImages($task_id)
  {
    $condition = new CDbCriteria();
    $condition->addInCondition('parent_tskid', [$task_id], 'AND');
    $condition->addInCondition('tskid', [$task_id], 'OR');
    $condition->addInCondition('t.status', [6, 9]);
    $project_tasks1 = Tasks::model()->findAll($condition);
    foreach ($project_tasks1 as $tasks) {
      $task_arr[] = $tasks['tskid'];
    }
    $tsk_ids = implode(',', $task_arr);

    $daily_work_table = DailyWorkProgress::model()->tableSchema->name;
    $image_table = ReportImages::model()->tableSchema->name;
    //$images_sql = "SELECT work.* FROM $daily_work_table as work inner join $image_table as img on img.wpr_id=work.id WHERE img.task_id IN($tsk_ids)";

    $images_sql = "SELECT work.* FROM pms_wpr_images as work inner join $image_table as img on img.wpr_image_id=work.id WHERE img.task_id IN($tsk_ids)";



    $images = Yii::app()->db->createCommand($images_sql)->queryAll();
    return $images;
  }
  public function getWorkProgress($work_type, $task_id, $area_id)
  {
    $daily_work_table = DailyWorkProgress::model()->tableSchema->name;
    $task_table = Tasks::model()->tableSchema->name;
    $condition = new CDbCriteria();
    $condition->addInCondition('parent_tskid', [$task_id], 'AND');
    $condition->addInCondition('tskid', [$task_id], 'OR');
    $project_tasks = Tasks::model()->findAll($condition);

    foreach ($project_tasks as $tasks) {
      $task_arr[] = $tasks['tskid'];
    }
    $all_task = implode(',', $task_arr);
    $work_progress = "SELECT SUM(qty) as qty FROM $daily_work_table as work join $task_table as tsk on tsk.tskid=work.taskid WHERE  work.taskid IN ($all_task) AND tsk.area=$area_id  AND work.work_type=$work_type GROUP BY work.work_type";
    $sum = Yii::app()->db->createCommand($work_progress)->queryAll();
    return $sum;
  }
  public function getTotalWorkProgress($task_id, $work_type_id, $area_id, $main_task_id)
  {
    $total_quantity = Tasks::model()->findByPk($task_id);
    $daily_work_table = DailyWorkProgress::model()->tableSchema->name;
    $task_table = Tasks::model()->tableSchema->name;
    $work_progress = "SELECT SUM(qty) as qty FROM $daily_work_table as work left join $task_table as tsk on tsk.tskid=work.taskid WHERE work.taskid=$task_id and work.work_type=$work_type_id and tsk.area=$area_id and approve_status=1";
    $sum = Yii::app()->db->createCommand($work_progress)->queryScalar();

    if (isset($total_quantity->quantity)) {
      $total_qty = $total_quantity->quantity;
      return round(($sum / $total_qty) * 100);
    } else {
      return $sum;
    }
  }
  public function getActionBy($minutes_id)
  {
    $condition = new CDbCriteria();
    $condition->join = 'LEFT JOIN pms_meeting_minutes_users ON pms_meeting_minutes_users.user_id = t.userid';
    $condition->addInCondition('meeting_minutes_id', [$minutes_id]);
    $condition->group = 'userid';
    $user_name[] = "";
    $users = Users::model()->findAll($condition);
    foreach ($users as $name) {
      $user_name[] = $name->first_name . " " . $name->last_name;
    }
    return implode(' / ', $user_name);
  }
  public function actionmeetingPdf($id)
  {
    $model = array();
    if (!empty($id)) {
      $model = SiteMeetings::model()->findByPk($id);
      $participants = MeetingParticipants::model()->findAll(['condition' => 'meeting_id = ' . $id]);
      $minutes_points = MeetingMinutes::model()->findAll(['condition' => 'meeting_id = ' . $id]);
      $project_det = Projects::model()->findByPk($model->project_id);

      for ($i = 1; $i <= 3; $i++) {
        $tasklist = $this->renderPartial('minutes_view', array(
          'model' => $model,
          'participants' => $participants,
          'minutes_points' => $minutes_points,
          'project' => $project_det,
          'pdf' => 1
        ), true);
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-P');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($tasklist);
        $filename = "minutes_of_meeting_{$id}_{$i}.pdf";
        $mPDF1->Output($filename, 'D');
        //            @chmod($filename, 777);
        if ($i < 3 or (strpos($tasklist, 'No milestone worktype data') === false and $i == 3)) {
          $meeting_pdf_parts[] = $filename;
        }
      }

      $report_range = 'meeting_minutes_report-' . $model->from_date . "-" . $model->to_date . ".pdf";
      if (count($meeting_pdf_parts)) {
        $this->mergePDFFiles($meeting_pdf_parts, $report_range);
      }
    }
  }
  public function actionmom()
  {

    $model = new SiteMeetings('search');
    $model->unsetAttributes();
    if (isset($_GET['SiteMeetings'])) {
      $model->attributes = $_GET['SiteMeetings'];
    }
    $this->render('mom_index', array('model' => $model));
  }
  public function actionmomview($id)
  {
    $model = array();
    if (!empty($id)) {
      $model = SiteMeetings::model()->findByPk($id);
      $project_det = Projects::model()->findByPk($model->project_id);
      $participants = MeetingParticipants::model()->findAll(['condition' => 'meeting_id = ' . $id]);
      $minutes_points = MeetingMinutes::model()->with('meetingMinutesUsers', 'tasks')->findAll(['condition' => 'meeting_id = ' . $id]);
    }
    $this->render(
      'mom_view',
      array(
        'model' => $model,
        'participants' => $participants,
        'minutes_points' => $minutes_points,
        'project' => $project_det,
        'pdf' => 2
      )
    );
  }
  public function actionmomPdf($id)
  {
    $model = array();
    if (!empty($id)) {
      $app_data = AppSettings::model()->find();
      $model = SiteMeetings::model()->findByPk($id);
      $participants = MeetingParticipants::model()->findAll(['condition' => 'meeting_id = ' . $id]);
      $minutes_points = MeetingMinutes::model()->with('meetingMinutesUsers')->findAll(['condition' => 'meeting_id = ' . $id, 'select' => 'points_discussed, meeting_minutes_status,end_date']);
      $meeting_minutes_data = array();
      foreach ($minutes_points as $key => $minutes_point) {
        $sts = $minutes_point->meeting_minutes_status;
        if ($sts == 1) {
          $status = "Info";
        } else if ($sts == 2) {
          $status = "Action";
        } else if ($sts == 3) {
          $status = "Decision";
        } else {
          $status = "";
        }
        $owner = $minutes_point->meetingMinutesUsers->user;
        $meeting_minutes_data[] = array(
          'si_no' => $key + 1,
          'points_discussed' => $minutes_point->points_discussed,
          'meeting_minutes_status' => $status,
          'owner' => $owner ? $owner->first_name . ' ' . $owner->last_name : '',
          'due_date' => $minutes_point->end_date,
        );
      }
      $participants_data = array();
      foreach ($participants as $key => $participant) {
        $participants_data[] = array(
          'si_no' => $key + 1,
          'name' => $participant->participants,
          'initial' => $participant->participant_initial,
          'designation' => $participant->designation,
          'organization_name' => $participant->organization_name,
          'organization_initial' => $participant->organization_initial
        );
      }

      $project = Projects::model()->findByPk($model->project_id);
      $template = ReportsTemplate::model()->findByPk(1);
      $app_root = YiiBase::getPathOfAlias('webroot');
      $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
      $pdf_logo = $theme_asset_url . 'default-logo.png';
      if (file_exists($app_root . "/themes/assets/pdf_logo.png")) {
        $pdf_logo = $theme_asset_url . 'pdf_logo.png';
      }
      $sample_data = array(
        'address' => isset($app_data->address) ? $app_data->address : '',
        'number' => isset($app_data->address) ? $app_data->number : '',
        'email' => isset($app_data->address) ? $app_data->email : '',
        'website' => isset($app_data->address) ? $app_data->website : '',
        'project_name' => $project->name,
        'client_name' => isset($project->client) ? $project->client->name : "N/A",
        'meeting_location' => isset($model->site) ? $model->site->site_name : "N/A",
        'meeting_ref' => isset($model->meeting_ref) ? $model->meeting_ref : "N/A",
        'project_code' => isset($project->project_no) ? $project->project_no : "N/A",
        'purpose' => isset($model->meeting_title) ? $model->meeting_title : "N/A",
        'date_time_venue' => date("d-m-Y", strtotime($model->date)) . "," . date("g:i A", strtotime($model->meeting_time)) . "," . $model->venue,
        'mom_no' => isset($model->meeting_number) ? $model->meeting_number : "N/A",
        'recorded_by' => $model->created->first_name . " " . $model->created->last_name,
        'distribution' => isset($model->distribution) ? $model->distribution : "N/A",
        'next_meeting_date' => date("d-m-Y", strtotime($model->next_meeting_date)),
        'participant_data' => $participants_data,
        'meeting_minutes_data' => $meeting_minutes_data,
        'logo_image_path' => $pdf_logo

      );
      $sample_data = json_encode($sample_data, JSON_PRETTY_PRINT);
      $html_content = $this->setHtmlContent($template['template_format'], $sample_data);
      $html_content = CHtml::decode($html_content);
      $mPDF1 = Yii::app()->ePdf->mPDF();
      $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-P');
      $rendered_html_content = $this->renderPartial('mom_pdf', array(
        'html_content' => $html_content,
      ), true);
      $footer = "
      <table width='100%' style='border-collapse: collapse;'>
      <tr>
          <td align='left' style='border: none;'>{$model->meeting_number}</td>
          <td align='right' style='border: none;'>{PAGENO} of {nb}</td>
      </tr>
  </table>
  ";
      $mPDF1->SetHTMLFooter($footer);
      $mPDF1->WriteHTML($rendered_html_content);
      $filename = "minutes_of_meeting_{$id}_.pdf";
      $mPDF1->Output($filename, 'D');

      // $report_range = 'meeting_minutes_report-' . $model->from_date . "-" . $model->to_date . ".pdf";
      // if (count($meeting_pdf_parts)) {
      //   $this->mergePDFFiles($meeting_pdf_parts, $report_range);
      // }
    }
  }
  public function actionweeklyReport()
  {
    $model = new DailyWorkProgress;
    $model->unsetAttributes();  // clear any default values

    if (isset($_REQUEST['DailyWorkProgress'])) {
      $model->attributes = $_REQUEST['DailyWorkProgress'];
    }

    $this->render('weekly_report', array('model' => $model));
  }
  public function actiondailyProgressReport()
  {
    $model = new ProjectReport;

    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['ProjectReport'])) {
      $model->attributes = $_GET['ProjectReport'];
    }

    $this->render('daily_progress_report', array('model' => $model));
  }
  public function actiongenerateDailyProgressReport()
  {


    if ($_POST['project_id'] != "" && $_POST['date'] != "") {
      $model = new ProjectReport;

      $model->project_id = $_POST['project_id'];
      $model->from_date = date('Y-m-d', strtotime(($_POST['date'])));
      $model->to_date = date('Y-m-d', strtotime(($_POST['date'])));
      $model->created_by = Yii::app()->user->id;
      $model->updated_by = Yii::app()->user->id;
      $model->created_date = date('Y-m-d');
      $model->updated_date = date('Y-m-d');
      if ($model->report_type == "") {
        $model->report_type = 4;
      }
      $already_exist = ProjectReport::model()->findByAttributes(array('project_id' => $_POST['project_id'], 'from_date' => date('Y-m-d', strtotime(($_POST['date'])))));
      if ($already_exist) {
        $result = array('status' => 0);
      } else {
        if ($model->save()) {

          $result = array('status' => 1);
        } else {
          $result = array('status' => 2);
        }
      }

      echo json_encode($result);
    }
  }
  public function actionview_daily_progress_report($id, $date)
  {
    $lastWeek = date($date, strtotime("-14 days"));
    $dateTime = new DateTime($date);
    $dateTime->modify('-14 day');
    $week_date = $dateTime->format("Y-m-d");

    $query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,pj.start_date as project_start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type  from pms_tasks p 

    INNER JOIN pms_projects AS pj ON pj.pid=p.project_id

    left outer join pms_daily_work_progress t on (t.taskid = p.tskid) group by p.tskid having ((coalesce(sum(t.qty), 0) < p.quantity) OR (p.start_date >=" . $week_date . ")) AND p.project_id = " . $id . " and p.task_type=1";

    $data = Yii::app()->db->createCommand($query)->queryAll();


    $criteria = new CDbCriteria();
    $criteria->join = 'INNER JOIN pms_tasks a ON t.taskid = a.tskid and a.project_id = ' . $id . ' and visitor_name!=""';

    $visitors = DailyWorkProgress::model()->findAll($criteria);

    $ins_criteria = new CDbCriteria();
    $ins_criteria->join = 'INNER JOIN pms_tasks a ON t.taskid = a.tskid and a.project_id = ' . $id . ' and inspection!=""';

    $test_inspection = DailyWorkProgress::model()->findAll($ins_criteria);

    $incident_criteria = new CDbCriteria();
    $incident_criteria->join = 'INNER JOIN pms_tasks a ON t.taskid = a.tskid and a.project_id = ' . $id . ' and incident!=""';

    $incident = DailyWorkProgress::model()->findAll($incident_criteria);

    $task_id = [];
    foreach ($data as $tasks) {
      if (isset($tasks['id'])) {
        $task_id[] = $tasks['tskid'];
      }
    }




    $wpr_criteria = new CDbCriteria();
    $wpr_criteria->addInCondition("taskid", $task_id);
    $wpr_result = DailyWorkProgress::model()->findAll($wpr_criteria);
    $wpr_id = [];
    foreach ($wpr_result as $wpr) {
      $wpr_id[] = $wpr->id;
    }
    $wpr_ids = implode(',', $wpr_id);
    $images = [];
    if (count($wpr_id) > 0) {
      $images_sql = "SELECT * FROM pms_wpr_images  WHERE  image_status=1 and wpr_id IN($wpr_ids)";
      $images = Yii::app()->db->createCommand($images_sql)->queryAll();
    }
    $this->render(
      'daily_progress_report_view',
      array(
        'data' => $data,
        'visitors' => $visitors,
        'test_inspection' => $test_inspection,
        'incident' => $incident,
        'images' => $images,
        'pdf' => 0
      )
    );
  }
  public function actiondaily_progress_report_pdf($id, $date)
  {
    $lastWeek = date($date, strtotime("-14 days"));
    $dateTime = new DateTime($date);
    $dateTime->modify('-14 day');
    $week_date = $dateTime->format("Y-m-d");

    $query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,pj.start_date as project_start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type  from pms_tasks p 

    INNER JOIN pms_projects AS pj ON pj.pid=p.project_id

    left outer join pms_daily_work_progress t on (t.taskid = p.tskid) group by p.tskid having ((coalesce(sum(t.qty), 0) < p.quantity) OR (p.start_date >=" . $week_date . ")) AND p.project_id = " . $id . " and p.task_type=1";

    $data = Yii::app()->db->createCommand($query)->queryAll();


    $criteria = new CDbCriteria();
    $criteria->join = 'INNER JOIN pms_tasks a ON t.taskid = a.tskid and a.project_id = ' . $id . ' and visitor_name!=""';

    $visitors = DailyWorkProgress::model()->findAll($criteria);

    $ins_criteria = new CDbCriteria();
    $ins_criteria->join = 'INNER JOIN pms_tasks a ON t.taskid = a.tskid and a.project_id = ' . $id . ' and inspection!=""';

    $test_inspection = DailyWorkProgress::model()->findAll($ins_criteria);

    $incident_criteria = new CDbCriteria();
    $incident_criteria->join = 'INNER JOIN pms_tasks a ON t.taskid = a.tskid and a.project_id = ' . $id . ' and incident!=""';

    $incident = DailyWorkProgress::model()->findAll($incident_criteria);

    $task_id = [];
    foreach ($data as $tasks) {
      if (isset($tasks['id'])) {
        $task_id[] = $tasks['tskid'];
      }
    }
    $wpr_criteria = new CDbCriteria();
    $wpr_criteria->addInCondition("taskid", $task_id);
    $wpr_result = DailyWorkProgress::model()->findAll($wpr_criteria);
    $wpr_id = [];
    foreach ($wpr_result as $wpr) {
      $wpr_id[] = $wpr->id;
    }
    $wpr_ids = implode(',', $wpr_id);
    $images = [];
    if (count($wpr_id) > 0) {
      $images_sql = "SELECT * FROM pms_wpr_images  WHERE  image_status=1 and wpr_id IN($wpr_ids)";
      $images = Yii::app()->db->createCommand($images_sql)->queryAll();
    }
    $result_array = $this->renderPartial('daily_progress_report_view', array(
      'data' => $data,
      'visitors' => $visitors,
      'test_inspection' => $test_inspection,
      'incident' => $incident,
      'images' => $images,
      'pdf' => 1
    ), true);

    $mPDF1 = Yii::app()->ePdf->mPDF();
    $mPDF1 = Yii::app()->ePdf->mPDF('c', 'A4-P');
    $mPDF1->WriteHTML($stylesheet, 1);
    $mPDF1->WriteHTML($result_array);
    $filename = "daily_progress.pdf";
    $mPDF1->Output($filename, 'D');
  }
  public function actiongenerateweeklyreportOld()
  {

    $model = new ProjectReport;
    $design_model = new DesignDeliverables;

    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['ProjectReport'])) {
      $model->attributes = $_GET['ProjectReport'];
    }

    if (isset($_POST['project_id']) && $_POST['date']) {
      $model = new ProjectReport;
      $model->project_id = $_POST['project_id'];
      $model->from_date = date('Y-m-d', strtotime(($_POST['date'])));
      $model->to_date = date('Y-m-d', strtotime(($_POST['date'])));
      $model->created_by = Yii::app()->user->id;
      $model->updated_by = Yii::app()->user->id;
      $model->created_date = date('Y-m-d');
      $model->updated_date = date('Y-m-d');
      if ($model->report_type == "") {
        $model->report_type = 5;
      }
      $already_exist = ProjectReport::model()->findByAttributes(array('project_id' => $_POST['project_id'], 'from_date' => date('Y-m-d', strtotime(($_POST['date'])))));
      if ($already_exist) {
        $result = array('status' => 0);
      } else {
        if ($model->save()) {
          if (isset($_POST['drawing_description'])) {
            foreach ($_POST['drawing_description'] as $key => $drawing) {
              $drawing_model = new DesignDeliverables;
              $target_date = date('Y-m-d', strtotime(($_POST['target_date'][$key])));
              $actual_date = date('Y-m-d', strtotime(($_POST['actual_date'][$key])));
              $status = $_POST['status'][$key];
              $remarks = $_POST['remarks'][$key];
              if ($drawing != "" && $_POST['target_date'][$key] != "" && $_POST['actual_date'][$key] != "" && $status != "") {
                $drawing_model->project_id = $model->project_id;
                $drawing_model->weekly_report_id = $model->id;
                $drawing_model->drawing_description = $drawing;
                $drawing_model->target_date = $target_date;
                $drawing_model->actual_date = $actual_date;
                $drawing_model->remarks = $remarks;
                $drawing_model->drawing_status = $status;
                $drawing_model->created_by = Yii::app()->user->id;
                $drawing_model->created_date = date('Y-m-d');
                $drawing_model->save();
              }
            }
          }


          $result = array('status' => 1);
        } else {
          print_r($model->getErrors());
          $result = array('status' => 2);
        }
      }

      echo json_encode($result);
      exit;
    }

    $this->render('generateweeklyreport', array('model' => $model, 'design_model' => $design_model));
  }

  public function actiongenerateweeklyreport()
  {
    // print_r($_POST);die;
    $model = new ProjectReport;
    $design_model = new DesignDeliverables;
    $weekly_report_images = new WeeklyReportImages;
    $inprogresstasks = array();
    $completed_task_array = array();
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['ProjectReport'])) {
      $model->attributes = $_GET['ProjectReport'];
    }
    if (isset($_GET['ProjectReport']['project_id']) && isset($_GET['ProjectReport']['from_date'])) {
      $model = new ProjectReport;
      $model->project_id = $_GET['ProjectReport']['project_id'];
      $model->from_date = date('Y-m-d', strtotime(($_GET['ProjectReport']['from_date'])));
      $model->to_date = date('Y-m-d', strtotime(($_GET['ProjectReport']['from_date'])));
      $model->created_by = Yii::app()->user->id;
      $model->updated_by = Yii::app()->user->id;
      $model->created_date = date('Y-m-d');
      $model->updated_date = date('Y-m-d');
      $model->report_type = 5;
      $already_exist = ProjectReport::model()->findByAttributes(array('project_id' => $_GET['ProjectReport']['project_id'], 'from_date' => date('Y-m-d', strtotime(($_GET['ProjectReport']['from_date']))), 'to_date' => date('Y-m-d', strtotime(($_GET['ProjectReport']['from_date']))), 'report_type' => 5));
      if ($already_exist) {
        Yii::app()->user->setFlash('error', "Report already exist");
        $this->redirect(array('reports/generateweeklyreport'));

      } else {
        if ($model->save()) {
          Yii::app()->user->setFlash('success', 'Successfully generated.');
        }
      }

      $project_id = $_GET['ProjectReport']['project_id'];
      $date = date("Y-m-d", strtotime($_GET['ProjectReport']['from_date']));
      $in_progress_task = Tasks::model()->findAll(
        array("condition" => "project_id =  $project_id and status=9 and task_type=1", "order" => "tskid")
      );

      foreach ($in_progress_task as $in_progress_tasks) {
        if ($in_progress_tasks->start_date <= $date && $in_progress_tasks->due_date >= $date) {
          $status = isset($in_progress_tasks->status0->caption) ? $in_progress_tasks->status0->caption : "";
          $inprogresstasks[] = $in_progress_tasks->title . "--" . $status;
        }
      }
      $completed_query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,pj.start_date as project_start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type,p.due_date,p.tskid,t.approve_status  from pms_tasks p 
      INNER JOIN pms_projects AS pj ON pj.pid=p.project_id
      left outer join pms_daily_work_progress t on (t.taskid = p.tskid) group by p.tskid having ((coalesce(sum(t.qty), 0) = p.quantity) ) AND p.project_id =  $project_id and p.task_type=1 and t.approve_status=1";
      $last_two_week = date("Y-m-d", strtotime($_GET['ProjectReport']['from_date'] . "-14 days"));
      $completed_task = Yii::app()->db->createCommand($completed_query)->queryAll();
      foreach ($completed_task as $task) {
        $completed_date = $task['max_date'];
        if (($completed_date >= $last_two_week) && ($completed_date <= $date)) {
          $completed_task_array[] = $task['title'];
        }
      }
    }
    $created_by = Yii::app()->user->id;
    $created_date = date('Y-m-d H:i:s');
    $update_weekly_report = isset($_POST['weekly_report_update']) && $_POST['weekly_report_update'];
    if (isset($_POST['payment_advice_data'])) {
      $payment_advice_data = json_decode($_POST['payment_advice_data'], true);
      $non_delete_advice = [];
      foreach ($payment_advice_data as $advice) {
        if ($advice[0] && $advice[1] && $advice[2] && $advice[3] && $advice[4] && $advice[5] && $advice[6]) {
          if (isset($advice[7]) && $advice[7]) {
            $advice_model = PaymentAdviceIssued::model()->findByPk($advice[7]);
          } else {
            $advice_model = new PaymentAdviceIssued;
          }
          $advice_model->budget_head = $advice[0];
          $advice_model->description = $advice[1];
          $advice_model->vendor = $advice[2];
          $advice_model->issued_date = date('Y-m-d', strtotime($advice[3]));
          $advice_model->pa_ref = $advice[4];
          $advice_model->pa_value = $advice[5];
          $advice_model->status = $advice[6];
          $advice_model->weekly_report_id = $_POST['report_id'];
          $advice_model->updated_by = $created_by;
          $advice_model->created_by = $created_by;
          $advice_model->created_date = $created_date;
          $advice_model->updated_date = $created_date;
          $advice_model->save();
          $non_delete_advice[] = $advice_model->id;
        }
      }
      if ($update_weekly_report) {
        $condition = 'weekly_report_id = :weekly_report_id ';
        if (isset($non_delete_advice) && $non_delete_advice) {
          $condition .= 'AND id NOT IN (' . implode(',', $non_delete_advice) . ')';
        }
        $params = [':weekly_report_id' => $_POST['report_id']];
        $delete_advice = PaymentAdviceIssued::model()->deleteAll($condition, $params);
      }
    }
    if (isset($_POST['payment_pending_data'])) {
      $payment_pending_data = json_decode($_POST['payment_pending_data'], true);
      $non_delete_pending = [];
      foreach ($payment_pending_data as $pending) {
        if ($pending[0] && $pending[1] && $pending[2] && $pending[3] && $pending[4] && $pending[5] && $pending[6]) {
          if (isset($pending[7]) && $pending[7]) {
            $pending_model = PaymentPending::model()->findByPk($pending[7]);
          } else {
            $pending_model = new PaymentPending;
          }
          $pending_model->budget_head = $pending[0];
          $pending_model->description = $pending[1];
          $pending_model->vendor = $pending[2];
          $pending_model->bill_date = date('Y-m-d', strtotime($pending[3]));
          $pending_model->bill_ref = $pending[4];
          $pending_model->bill_amount = $pending[5];
          $pending_model->remarks = $pending[6];
          $pending_model->weekly_report_id = $_POST['report_id'];
          $pending_model->updated_by = $created_by;
          $pending_model->created_by = $created_by;
          $pending_model->created_date = $created_date;
          $pending_model->updated_date = $created_date;
          $pending_model->save();
          $non_delete_pending[] = $pending_model->id;
        }
      }
      if ($update_weekly_report) {
        $condition = 'weekly_report_id = :weekly_report_id ';
        if (isset($non_delete_pending) && $non_delete_pending) {
          $condition .= ' AND id NOT IN (' . implode(',', $non_delete_pending) . ')';
        }
        $params = [':weekly_report_id' => $_POST['report_id']];
        $delete_advice = PaymentPending::model()->deleteAll($condition, $params);
      }
    }

    $non_delete_images = [];
    if (isset($_POST['weekly_images'])) {
      $uploaded_files = CUploadedFile::getInstancesByName('image_files');
      if (!empty($uploaded_files)) {
        $upload_path = Yii::app()->basePath . '/../uploads/weekly_report/';
        if (!is_dir($upload_path)) {
          mkdir($upload_path, 0777, true);
        }
      }
      foreach ($_POST['weekly_images'] as $key => $image_data) {
        $image_data = json_decode($image_data, true);
        $file_name = $image_data['file_name'];
        if ($file_name) {
          foreach ($uploaded_files as $key => $file) {
            if ($file_name == $file->getName()) {
              $newfilename = rand(1000, 9999) . time();
              $newfilename = md5($newfilename); //optional
              $file_name = $newfilename . '.' . $file->getExtensionName();
              $file_path = $upload_path . $file_name;
              $upload_image = $file->saveAs($file_path);
              unset($uploaded_files[$key]);
              break;
            }
          }
        }
        if (isset($image_data['weekly_image_id']) && !empty($image_data['weekly_image_id'])) {
          $imageModel = WeeklyReportImages::model()->findByPk($image_data['weekly_image_id']);
        } else {
          $imageModel = new WeeklyReportImages();
          $imageModel->created_by = $created_by;
          $imageModel->created_date = $created_date;
        }
        $imageModel->weekly_report_id = $_POST['report_id'];
        if (isset($upload_image) && $upload_image && $file_name) {
          $imageModel->image_path = $file_name;
        }
        $imageModel->image_label = $image_data['label'];
        $imageModel->image_status = 1;
        $imageModel->updated_by = $created_by;
        $imageModel->updated_date = $created_date;
        if ($imageModel->image_path) {
          $save = $imageModel->save();
          if ($save) {
            $non_delete_images[] = $imageModel->id;
          }
        }
      }
    }
    if ($update_weekly_report) {
      $image_condition = 'weekly_report_id = :weekly_report_id';
      if (isset($non_delete_images) && !empty($non_delete_images)) {
        $image_condition .= ' AND id NOT IN (' . implode(',', $non_delete_images) . ')';
      }
      $params = [':weekly_report_id' => $_POST['report_id']];
      $delete_images = WeeklyReportImages::model()->deleteAll($image_condition, $params);
    }
    if (isset($_POST['drawing_description'])) {
      if ($update_weekly_report) {
        $condition = 'weekly_report_id = :weekly_report_id';
        if (isset($_POST['deliverable_id']) && !empty($_POST['deliverable_id'])) {
          $condition .= ' AND id NOT IN (' . implode(',', $_POST['deliverable_id']) . ')';
        }
        $params = [':weekly_report_id' => $_POST['report_id']];
        $delete_deliverables = DesignDeliverables::model()->deleteAll($condition, $params);
      }

      $drawing_description = $_POST['drawing_description'];
      foreach ($drawing_description as $key => $drawing) {
        if (isset($_POST['deliverable_id'][$key]) && $_POST['deliverable_id'][$key]) {
          $drawing_model = DesignDeliverables::model()->findByPk($_POST['deliverable_id'][$key]);
        } else {
          $drawing_model = new DesignDeliverables;
          $drawing_model->created_by = Yii::app()->user->id;
          $drawing_model->created_date = date('Y-m-d');
        }
        $target_date = date('Y-m-d', strtotime(($_POST['target_date'][$key])));
        $actual_date = date('Y-m-d', strtotime(($_POST['actual_date'][$key])));
        $status = $_POST['status'][$key];
        $remarks = $_POST['remarks'][$key];
        if ($drawing != "" && $_POST['target_date'][$key] != "" && $_POST['actual_date'][$key] != "" && $status != "") {
          $drawing_model->project_id = $_POST['project_id'];
          $drawing_model->weekly_report_id = $_POST['report_id'];
          $drawing_model->drawing_description = $drawing;
          $drawing_model->target_date = $target_date;
          $drawing_model->actual_date = $actual_date;
          $drawing_model->remarks = $remarks;
          $drawing_model->drawing_status = $status;
          $drawing_model->save();
        }
      }
      // if(isset($_POST['weekly_report_id']) && $_POST['weekly_report_id']){
      //   $report_data_model = WeeklyReportData::model()->findByPk($_POST['weekly_report_id']);
      // }else{
      //   $report_data_model = new WeeklyReportData;
      // }
      $report_data_model = WeeklyReportData::model()->findByAttributes(array('report_id' => $_POST['report_id']));
      if (!$report_data_model) {
        $report_data_model = new WeeklyReportData;
      }

      $report_data_model->report_id = $_POST['report_id'];
      $report_data_model->type = 1;
      $data_array = array(1 => $_POST['present_status'], 2 => $_POST['key_achievements']);
      $report_data_model->report_data = json_encode($data_array);
      $report_data_model->created_by = Yii::app()->user->id;
      $report_data_model->created_date = date('Y-m-d');
      $report_data_model->save();
      $result = array('status' => 1, 'redirect' => Yii::app()->createUrl('Reports/generateweeklyreport'));
      echo json_encode($result);
      exit;
    }
    $body_array = [
      array("", "", "", "", "", "", ""),
      array("", "", "", "", "", "", ""),
      array("", "", "", "", "", "", ""),
    ];
    $pending_payment_data = json_encode($body_array);
    $advice_payment_data = json_encode($body_array);
    $this->render(
      'generateweeklyreport',
      array(
        'model' => $model,
        'design_model' => $design_model,
        'weekly_report_images' => $weekly_report_images,
        'present_status' => $inprogresstasks,
        'completed_task_array' => $completed_task_array,
        'pending_payment_data' => $pending_payment_data,
        'advice_payment_data' => $advice_payment_data,
      )
    );
  }
  public function actionupdate_weekly_report($project_id, $date, $id)
  {
    $model = ProjectReport::model()->findByPk($id);
    $existing_design = DesignDeliverables::model()->findAll(array('condition' => 'weekly_report_id=' . $id));
    $design_model = new DesignDeliverables;
    $weekly_report_images = new WeeklyReportImages;
    $existing_weekly_report_images = WeeklyReportImages::model()->findAll(array('condition' => 'weekly_report_id=' . $id));
    $weekly_report = WeeklyReportData::model()->findByAttributes(array('report_id' => $id));
    $weekly_report_data = $weekly_report ? json_decode($weekly_report->report_data, true) : '';
    $present_status = empty($weekly_report_data[1]) ? [] : array_filter(explode("\n", $weekly_report_data[1]));
    $key_achievements = empty($weekly_report_data[2]) ? [] : array_filter(explode("\n", $weekly_report_data[2]));

    $body_array = [
      array("", "", "", "", "", "", ""),
      array("", "", "", "", "", "", ""),
      array("", "", "", "", "", "", ""),
    ];
    $payment_pending_data = PaymentPending::model()->findAllByAttributes(array('weekly_report_id' => $id), array('select' => 'budget_head,description,vendor,bill_date,bill_ref,bill_amount,remarks,id'));
    $payment_advice_data = PaymentAdviceIssued::model()->findAllByAttributes(array('weekly_report_id' => $id), array('select' => 'budget_head,description,vendor,issued_date,pa_ref,pa_value,status,id'));
    if (!empty($payment_advice_data)) {
      foreach ($payment_advice_data as $advice) {
        $advice_data[] = [$advice->budget_head, $advice->description, $advice->vendor, $advice->issued_date, $advice->pa_ref, $advice->pa_value, $advice->status, $advice->id];
      }
    } else {
      $advice_data = $body_array;
    }
    if (!empty($payment_pending_data)) {
      foreach ($payment_pending_data as $pending) {
        $pending_data[] = [$pending->budget_head, $pending->description, $pending->vendor, $pending->bill_date, $pending->bill_ref, $pending->bill_amount, $pending->remarks, $pending->id];
      }
    } else {
      $pending_data = $body_array;
    }
    $pending_payment_data = json_encode($pending_data);
    $advice_payment_data = json_encode($advice_data);
    $this->render(
      'updateweeklyreport',
      array(
        'model' => $model,
        'type' => true,
        'existing_design' => $existing_design,
        'design_model' => $design_model,
        'weekly_report_images' => $weekly_report_images,
        'existing_weekly_report_images' => $existing_weekly_report_images,
        'present_status' => $present_status,
        'key_achievements' => $key_achievements,
        'pending_payment_data' => $pending_payment_data,
        'advice_payment_data' => $advice_payment_data,
      )
    );

  }
  public function actionweekly_report_view($project_id, $date, $id)
  {

    $project = Projects::model()->findByPk($project_id);
    function weekOfMonth($date)
    {

      $firstOfMonth = strtotime(date("Y-m-01", $date));

      return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
    }

    function weekOfYear($date)
    {
      $weekOfYear = intval(date("W", $date));
      if (date('n', $date) == "1" && $weekOfYear > 51) {

        return 0;
      } else if (date('n', $date) == "12" && $weekOfYear == 1) {

        return 53;
      } else {

        return $weekOfYear;
      }
    }

    $weekOfMonth = weekOfMonth(strtotime($date));

    $month = date("M", strtotime($date));

    $in_progress_task = Tasks::model()->findAll(
      array("condition" => "project_id =  $project_id and status=9 and task_type=1", "order" => "tskid")
    );


    $two_week = date("Y-m-d", strtotime($date . "+2 week"));

    $task_for_nxt_wk = Tasks::model()->findAll(
      array("condition" => "project_id =  $project_id and  task_type=1 and start_date between '$date' and  '$two_week'", "order" => "tskid")
    );

    $today = date('Y-m-d');

    $query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,pj.start_date as project_start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type,p.due_date,p.tskid  from pms_tasks p 

    INNER JOIN pms_projects AS pj ON pj.pid=p.project_id

    left outer join pms_daily_work_progress t on (t.taskid = p.tskid) group by p.tskid having ((coalesce(sum(t.qty), 0) < p.quantity) ) AND p.project_id = $project_id and p.task_type=1 and p.due_date < '$date'";


    $expired_pending_task = Yii::app()->db->createCommand($query)->queryAll();

    $completed_query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,pj.start_date as project_start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type,p.due_date,p.tskid  from pms_tasks p 

    INNER JOIN pms_projects AS pj ON pj.pid=p.project_id

    left outer join pms_daily_work_progress t on (t.taskid = p.tskid) group by p.tskid having ((coalesce(sum(t.qty), 0) = p.quantity) ) AND p.project_id = $id and p.task_type=1";

    $last_two_week = date("Y-m-d", strtotime($date . "-14 days"));

    $completed_task = Yii::app()->db->createCommand($completed_query)->queryAll();
    $completed_task_array = [];
    foreach ($completed_task as $task) {
      $completed_date = $task['max_date'];
      if (($completed_date >= $last_two_week) && ($completed_date <= $date)) {
        $completed_task_array[] = $task['title'];
      }
    }

    $task = Tasks::model()->findAll(
      array("condition" => "project_id =  $project_id and  task_type=1", "group" => "milestone_id")
    );
    $task_array = [];
    foreach ($task as $tasks) {
      $data = array();
      if ($tasks->milestone_id != "") {
        $sql_query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,p.start_date,p.due_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type,p.due_date,p.tskid  from pms_tasks p 
      left outer join pms_daily_work_progress t on (t.taskid = p.tskid)  where p.milestone_id = $tasks->milestone_id and p.task_type=1 GROUP BY p.tskid ";
        $data = Yii::app()->db->createCommand($sql_query)->queryAll();
      }

      foreach ($data as $datas) {
        $milestone = Milestone::model()->findByPk($tasks->milestone_id);
        $task_array[$milestone->milestone_title][] = array(
          'start_date' => $datas['start_date'],
          'end_date' => $datas['due_date'],
          'min_date' => $datas['min_date'],
          'max_date' => $datas['max_date'],
          'quantity' => $datas['quantity'],
          'total_quantity' => $datas['total_quantity'],
          'task' => $datas['title'],
          'description' => $datas['description']
        );
      }
    }

    $site_visit = DailyWorkProgress::model()->findAll(
      array("condition" => "project_id =  $project_id and visitor_name != ''  ")
    );

    $pending_deliverables = DesignDeliverables::model()->findAll(array("condition" => "weekly_report_id= $id and drawing_status=2"));
    $delivered_deliverables = DesignDeliverables::model()->findAll(array("condition" => "weekly_report_id= $id and drawing_status=1"));
    $weekly_report_data = WeeklyReportData::model()->findByAttributes(array('report_id' => $id));
    $payment_pending_data = PaymentPending::model()->findAllByAttributes(array('weekly_report_id' => $id));
    $payment_advice_data = PaymentAdviceIssued::model()->findAllByAttributes(array('weekly_report_id' => $id));
    $weekly_report_images = WeeklyReportImages::model()->findAllByAttributes(array('weekly_report_id' => $id));

    $report_image_sql = "SELECT *
    FROM pms_wpr_images
    INNER JOIN pms_report_images ON pms_wpr_images.id = pms_report_images.wpr_image_id where project_id=" . $project_id;
    $report_images = Yii::app()->db->createCommand($report_image_sql)->queryAll();

    $this->render(
      'weekly_report_view',
      array(
        'project' => $project,
        'pdf' => 0,
        'weekOfMonth' => $weekOfMonth,
        'month' => $month,
        'in_progress_task' => $in_progress_task,
        'task_for_nxt_wk' => $task_for_nxt_wk,
        'expired_pending_task' => $expired_pending_task,
        'completed_task_array' => $completed_task_array,
        'task_array' => $task_array,
        'site_visit' => $site_visit,
        'pending_deliverables' => $pending_deliverables,
        'delivered_deliverables' => $delivered_deliverables,
        'weekly_report_data' => $weekly_report_data,
        'report_images' => $report_images,
        'weekly_report_images' => $weekly_report_images,
        'payment_pending_data' => $payment_pending_data,
        'payment_advice_data' => $payment_advice_data,
      )
    );
  }
  public function actionweekly_report_pdf($project_id, $date, $id)
  {



    $project = Projects::model()->findByPk($project_id);
    function weekOfMonth($date)
    {

      $firstOfMonth = strtotime(date("Y-m-01", $date));

      return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
    }

    function weekOfYear($date)
    {
      $weekOfYear = intval(date("W", $date));
      if (date('n', $date) == "1" && $weekOfYear > 51) {

        return 0;
      } else if (date('n', $date) == "12" && $weekOfYear == 1) {

        return 53;
      } else {

        return $weekOfYear;
      }
    }

    $weekOfMonth = weekOfMonth(strtotime($date));

    $month = date("M", strtotime($date));

    $in_progress_task = Tasks::model()->findAll(
      array("condition" => "project_id =  $project_id and status=9 and task_type=1", "order" => "tskid")
    );

    $two_week = date("Y-m-d", strtotime($date . "+2 week"));

    $task_for_nxt_wk = Tasks::model()->findAll(
      array("select" => "title", "condition" => "project_id =  $project_id and  task_type=1 and start_date between '$date' and  '$two_week'", "order" => "tskid")
    );
    $today = date('Y-m-d');

    $query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,pj.start_date as project_start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type,p.due_date,p.tskid  from pms_tasks p 

    INNER JOIN pms_projects AS pj ON pj.pid=p.project_id

    left outer join pms_daily_work_progress t on (t.taskid = p.tskid) group by p.tskid having ((coalesce(sum(t.qty), 0) < p.quantity) ) AND p.project_id = $project_id and p.task_type=1 and p.due_date < '$date'";


    $expired_pending_task = Yii::app()->db->createCommand($query)->queryAll();

    $completed_query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,pj.start_date as project_start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type,p.due_date,p.tskid  from pms_tasks p 

    INNER JOIN pms_projects AS pj ON pj.pid=p.project_id

    left outer join pms_daily_work_progress t on (t.taskid = p.tskid) group by p.tskid having ((coalesce(sum(t.qty), 0) = p.quantity) ) AND p.project_id = $project_id and p.task_type=1";

    $last_two_week = date("Y-m-d", strtotime($date . "-14 days"));



    $completed_task = Yii::app()->db->createCommand($completed_query)->queryAll();
    $completed_task_array = [];
    foreach ($completed_task as $task) {
      $completed_date = $task['max_date'];
      if (($completed_date >= $last_two_week) && ($completed_date <= $date)) {
        $completed_task_array[] = $task['title'];
      }
    }
    $task_array = [];
    $sql_query = "select p.project_id, p.quantity, p.tskid, p.start_date, sum(t.qty) as total_quantity,t.id,p.title,p.start_date,p.due_date,MIN(`date`) min_date,MAX(`date`) AS max_date,p.unit,p.work_type_id,p.description,p.task_type,p.due_date,p.tskid,m.id as milestone_id,m.milestone_title  from pms_tasks p 
      left outer join pms_daily_work_progress t on (t.taskid = p.tskid) 
      left outer join pms_milestone m ON (m.id = p.milestone_id)    
      where p.project_id = $project_id and p.task_type=1 GROUP BY p.tskid ";
    $data = Yii::app()->db->createCommand($sql_query)->queryAll();
    if (!empty($data)) {
      foreach ($data as $key => $datas) {
        $progress_percent = $datas['quantity'] ? ($datas['total_quantity'] / $datas['quantity']) * 100 : '';
        $task_array[$datas['milestone_id']][0] = ['milestone' => $datas['milestone_title']];
        $task_array[$datas['milestone_id']][1][] = array(
          'si_no' => $key + 1,
          'description' => $datas['title'],
          'scheduled_start' => $datas['start_date'] ? date("d-m-Y", strtotime($datas['start_date'])) : '',
          'scheduled_finish' => $datas['due_date'] ? date("d-m-Y", strtotime($datas['due_date'])) : '',
          'actual_start' => $datas['min_date'] ? date("d-m-Y", strtotime($datas['min_date'])) : '',
          'actual_finish' => ($progress_percent >= 100 && $datas['max_date']) ? date("d-m-Y", strtotime($datas['max_date'])) : '',
          'completed' => $progress_percent,
          'remarks' => $datas['description']
        );
      }
    }

    // $site_visit = DailyWorkProgress::model()->findAll(array("condition" => "project_id =  $project_id and visitor_name != ''  "));
    $command = Yii::app()->db->createCommand()
      ->select('visitor_name as emplyee_name,designation,date AS visit_date,purpose')
      ->from('pms_daily_work_progress')
      ->where('project_id = :project_id and visitor_name != ""', array(':project_id' => $project_id));
    $site_visit = $command->queryAll();
    $weekly_report_data = WeeklyReportData::model()->findByAttributes(array('report_id' => $id));
    $weekly_report_images = WeeklyReportImages::model()->findAllByAttributes(array('weekly_report_id' => $id));
    $payment_pending_data = PaymentPending::model()->findAllByAttributes(array('weekly_report_id' => $id), array('select' => 'budget_head,description,vendor,bill_date,bill_ref,bill_amount,remarks'));
    $payment_advice_data = PaymentAdviceIssued::model()->findAllByAttributes(array('weekly_report_id' => $id), array('select' => 'budget_head,description,vendor,issued_date,pa_ref,pa_value,status'));
    $report_image_sql = "SELECT *
    FROM pms_wpr_images
    INNER JOIN pms_report_images ON pms_wpr_images.id = pms_report_images.wpr_image_id  where project_id=" . $project_id;
    $report_images = Yii::app()->db->createCommand($report_image_sql)->queryAll();
    $base_path = Yii::app()->getBaseUrl(true);
    $weekly_report_data = $weekly_report_data ? json_decode($weekly_report_data->report_data, true) : '';
    $deliverables = $this->getDeliverables($id);
    $template = ReportsTemplate::model()->findByPk(2);
    $app_root = YiiBase::getPathOfAlias('webroot');
    $theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
    $default_logo_path = $theme_asset_url . 'default-logo.png';
    $logo = file_exists($app_root . "/themes/assets/logo.png") ? $theme_asset_url . 'logo.png' : $default_logo_path;
    $pdf_logo = file_exists($app_root . "/themes/assets/pdf_logo.png") ? $theme_asset_url . 'pdf_logo.png' : $logo;
    $report_img_path = '/uploads/project/report_imgs/' . $project->report_image;
    $project_image = $project->report_image && file_exists($app_root . $report_img_path) ? "<img src='" . $base_path . $report_img_path . "' class='size-auto work-image' />" : '';
    $sample_data = array(
      'week_of_month' => $weekOfMonth . " (" . $month . ")",
      'project_name' => $project->name,
      'client_name' => isset($data->client) ? strtoupper($data->client->name) : "N/A",
      'location' => '',
      'project_code' => strtoupper($project->project_no),
      'project_image' => $project_image,
      'report_logo' => $pdf_logo,
      'footer_logo' => $logo,
      'website' => 'www.projemconsulting.com',
      'weekly_images' => empty($weekly_report_images) ? [] : $this->getWeeklyReportImages($weekly_report_images, '/uploads/weekly_report/'),
      'wpr_images' => empty($report_images) ? [] : $this->getWeeklyReportImages($report_images, '/uploads/dailywork_progress/'),
      'present_status' => empty($weekly_report_data[1]) ? [] : explode("\n", $weekly_report_data[1]),
      'key_achievements' => empty($weekly_report_data[2]) ? [] : explode("\n", $weekly_report_data[2]),
      'key_actions' => empty($task_for_nxt_wk) ? [] : array_map(function ($task) {
        return $task->title;
      }, $task_for_nxt_wk),
      'key_risks' => empty($expired_pending_task) ? [] : array_map(function ($task) {
        return $task['title'];
      }, $expired_pending_task),
      'drawing_delivered' => $deliverables['delivered_deliverables'],
      'drawing_pending' => $deliverables['pending_deliverables'],
      'payment_advice' => empty($payment_advice_data) ? [] : $this->setObjectToArray($payment_advice_data),
      'payment_pending' => empty($payment_pending_data) ? [] : $this->setObjectToArray($payment_pending_data),
      'site_visit' => empty($site_visit) ? [] : $this->setObjectToArray($site_visit),
      'target' => $task_array
    );

    $sample_data = json_encode($sample_data, JSON_PRETTY_PRINT);
    $html_content = $this->setWeeklyReportContent($template['template_format'], $sample_data);
    $html_content = CHtml::decode($html_content);
    $mPDF1 = Yii::app()->ePdf->mpdf('c', [451, 254], 0, 0, 0, 0, 0, 0);
    $html_content .= ' <style> body{background-image: linear-gradient(to bottom,#131e3e,#131e3e 10%,#fff 10%,#fff 90%,#131e3e 90%);}</style>';
    $rendered_html_content = $this->renderPartial('mom_pdf', array('html_content' => $html_content, ), true);
    $html_footer = '
                  <table width="100%" class="full-block page-foot" >
                      <tr>
                          <td class="footer-txt"></td>
                          <td class="footer-txt text-center"></td>
                          <td class="text-right"><img src="' . $logo . '" class="footer-img" /></td>
                      </tr>
                  </table>';

    $html_header = '
                  <table  class="full-block page-head" >
                      <tr>
                          <td class="project-status fw-bold">WEEKLY PROJECT STATUS REPORT</td>
                          <td class="project-name text-right">PROJECT NAME: <span class="fw-bold">' . $project->name . '</span></td>
                      </tr>
                  </table>';
    $mPDF1->setAutoBottomMargin = 'stretch';
    $mPDF1->SetHTMLFooter($html_footer);
    $mPDF1->SetHTMLHeader($html_header);
    $mPDF1->SetAutoPageBreak(true);
    $mPDF1->SetMargins(0, 0, 40, 50);
    $mPDF1->WriteHTML($rendered_html_content);
    $filename = "weekly_report_{$id}_.pdf";
    $mPDF1->Output($filename, 'D');

  }
  public function getDeliverables($id)
  {
    $delivered_deliverables = [];
    $pending_deliverables = [];
    $deliverables = DesignDeliverables::model()->findAllByAttributes(array("weekly_report_id" => $id, "drawing_status" => array(1, 2)));
    if (!empty($deliverables)) {
      foreach ($deliverables as $deliverable) {
        $formatted_deliverable = [
          'drawing_description' => $deliverable->drawing_description,
          'target_date' => $deliverable->target_date ? date("d-m-Y", strtotime($deliverable->target_date)) : '',
          'actual_date' => $deliverable->actual_date ? date("d-m-Y", strtotime($deliverable->actual_date)) : '',
          'remarks' => $deliverable->remarks,
        ];
        if ($deliverable->drawing_status == 1) {
          $formatted_deliverable['si_no'] = count($delivered_deliverables) + 1;
          $delivered_deliverables[] = $formatted_deliverable;
        } elseif ($deliverable->drawing_status == 2) {
          $formatted_deliverable['si_no'] = count($pending_deliverables) + 1;
          $pending_deliverables[] = $formatted_deliverable;
        }
      }
    }
    return ['delivered_deliverables' => $delivered_deliverables, 'pending_deliverables' => $pending_deliverables];
  }
  public function setObjectToArray($model)
  {
    $output_array = [];
    foreach ($model as $key => $data) {
      $array = $data->attributes;
      foreach ($array as $attr_key => $attr_value) {
        if (strtotime($attr_value) !== false) {
          $array[$attr_key] = date('Y-m-d', strtotime($attr_value));
        }
      }
      $array['si_no'] = $key + 1;
      $output_array[] = $array;
    }
    return $output_array;
  }
  public function actiongenerateprojectreport()
  {
    $model = new ProjectReport;
    if (isset($_REQUEST['ProjectReport'])) {
      $model->attributes = $_REQUEST['ProjectReport'];

      if ($model->from_date != "") {
        $model->from_date = date('Y-m-d', strtotime(($model->from_date)));
      }
      if ($model->to_date != "") {
        $model->to_date = date('Y-m-d', strtotime(($model->to_date)));
      }
      $model->created_by = Yii::app()->user->id;
      $model->updated_by = Yii::app()->user->id;
      $model->created_date = date('Y-m-d');
      $model->updated_date = date('Y-m-d');
      $criteria = new CDbCriteria;
      $criteria->addBetweenCondition("start_date", $model->from_date, $model->to_date, 'AND');
      $criteria->addBetweenCondition("due_date", $model->from_date, $model->to_date, 'OR');
      $criteria->compare('project_id', $model->project_id);
      if ($model->report_type == 1) {

        $criteria->group = 'contractor_id';
        $project_tasks = Tasks::model()->findAll($criteria);
        $result_data = $this->getConsolidatedResult($project_tasks, $model->project_id);
      } elseif ($model->report_type == 2) {
        $project_tasks = Tasks::model()->findAll($criteria);
        $result_data = $this->getDetailedResult($project_tasks, $model->project_id, $model->from_date, $model->to_date);
      } elseif ($model->report_type == 3) {
        $criteria->compare('status', 9);
        $project_tasks = Tasks::model()->findAll($criteria);
        $result_data = '';
      }

      $model->report_data = json_encode($result_data);
      if ($model->save()) {
        $return_result = array('status' => 1);
      } else {
        $return_result = array('status' => 2);
      }
      echo json_encode($return_result);
    }
  }
  public function actionOnhold_report()
  {
    $model = new TaskLog;
    $model->unsetAttributes();  // clear any default values


    $this->render('onhold_report', array('model' => $model));
  }
  public function getTaskBalanceQty($task_id, $task_type)
  {
    if ($task_type == 1) {
      $work_progress = DailyWorkProgress::model()->find(
        array(
          'select' => 'SUM(qty) as qty',
          'condition' => 'taskid = ' . $task_id . ' and approve_status = 1'
        )
      );
      return $work_progress->qty;
    } else {
      $time_entry = TimeEntry::model()->find(
        array(
          'select' => 'SUM(completed_percent) as completed_percent',
          'condition' => 'tskid = ' . $task_id . ' and approve_status = 1'
        )
      );
      return $time_entry->completed_percent;
    }
  }
  public function getWeeklyReportImages($weekly_report_images, $path)
  {
    $weekly_images = [];
    $img_path = Yii::app()->getBaseUrl(true) . $path;
    $app_root = YiiBase::getPathOfAlias('webroot') . $path;
    foreach ($weekly_report_images as $key => $image) {
      $index = (int) ($key / 2); // Calculate the index for the $weekly_images array
      $img = $image['image_path'] && file_exists($app_root . $image['image_path']) ? "<img src='" . $img_path . $image['image_path'] . "' class='work-image'alt=''/>" : '';
      if ($key % 2 == 0) {
        $weekly_images[$index]['weekly_image_1'] = $img;
        $weekly_images[$index]['weekly_label_1'] = $img ? $image['image_label'] : '';
        $weekly_images[$index]['weekly_image_2'] = '';
        $weekly_images[$index]['weekly_label_2'] = '';
      } else {
        $weekly_images[$index]['weekly_image_2'] = $img;
        $weekly_images[$index]['weekly_label_2'] = $img ? $image['image_label'] : '';
      }
    }
    return $weekly_images;

  }
}
