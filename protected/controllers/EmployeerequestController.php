<?php

class EmployeerequestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('sendmail'),
						'users'=>array('@'),
					),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Employeerequest;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employeerequest']))
		{
                    $model->ack_message=0;
			$model->attributes=$_POST['Employeerequest'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

                    
		if(isset($_POST['Employeerequest']))
		{
                   
			$model->attributes=$_POST['Employeerequest'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Employeerequest');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                
		$model=new Employeerequest('search');
		$model->unsetAttributes();  // clear any default values


         // new filter back and forth
        $class_name= get_class($model);
        if (isset($_GET[ $class_name])){
           Yii::app()->user->setState('prevfilter' , $_GET[$class_name]);
        }

        if(isset(Yii::app()->user->prevfilter)){
           $model->attributes = Yii::app()->user->prevfilter;
        }

		// end 
		
		if(isset($_GET['Employeerequest']))
			$model->attributes=$_GET['Employeerequest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Employeerequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employeerequest-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionSendmail($id){ 
                            $model2 = new MailLog;
                            $today = date('Y-m-d H:i:s'); 
                            $mail_model=GeneralSettings::model()->find(['condition'=>'id = 1']);
                            $model=$this->loadModel($id);
                            $tbl = Yii::app()->db->tablePrefix;                                                          
                            $sql = "SELECT e.*,u.* FROM {$tbl}users u
                            INNER JOIN {$tbl}employeerequest e "
                            . " ON e.created_by = u.userid WHERE e.id = '$id'";                                                        
                            $empDetails = Yii::app()->db->createCommand($sql)->queryRow();                                                                                     
                            $title = $empDetails['title'];                           
                            $getdata = "SELECT * FROM {$tbl}users u
                            INNER JOIN {$tbl}mail_settings m "
                            . " ON m.user_id = u.userid  "
                            . " AND m.status = 1";
                            $users = Yii::app()->db->createCommand($getdata)->queryAll();                              
                            if ($users == NULL) {                                                            
                            $getdata = "SELECT u.* FROM {$tbl}users u WHERE u.user_type = 1";
                            $users = Yii::app()->db->createCommand($getdata)->queryAll();                            
                            }                            

                            if ($users != NULL) {    
                            foreach ($users as $user) {
                            $email = $user['email']; 
                                                                                    
                            $mail = new JPhpMailer();                             
                            $subject = 'Employee Request';
                            $headers = Yii::app()->name;
                            $bodyContent = "<p>Hello ".$user['first_name'].' '.$user['last_name'].",</p><p>This is a employee request mail sent by ".$empDetails['first_name'].' '.$empDetails['last_name']." on ".$empDetails['request_date']." </p>";
                            //$bodyContent .= $this->renderPartial('_emailform', array('insert_id' => $insert_id), true);                         
                            $msg = nl2br($empDetails['message']);
                            $bodyContent.=" Title : <br>".$title."<br>";
                            $bodyContent.=" Message : <br>".$msg;
                            

               //die($_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
               $server = (($_SERVER['HTTP_HOST'] == 'localhost') ? '0' : '1');
               
	   
               //die($server);
               if($server == 0){
                   $mail->IsSMTP();
                   //$mail->Host = SMTPHOST;
                //    $mail->SMTPSecure = SMTPSECURE;
                //    $mail->SMTPAuth = true;
                //    $mail->Username = SMTPUSERNAME;
                //    $mail->Password = SMTPPASS;
                $mail->Host =$mail_model['smtp_host'];
                $mail->Port   = $mail_model['smtp_port'];
                $mail->SMTPSecure = $mail_model['smtp_secure'];
                $mail->SMTPAuth =$mail_model['smtp_auth'];
                $mail->Username = $mail_model['smtp_username'];
                $mail->Password = $mail_model['smtp_password'];
                  
               }
                $mail_send_by=EMAILFROM;
               $mail->setFrom(EMAILFROM);
               $mail->addAddress($email);   // Add a recipient
               $mail->isHTML(true);

               $mail->Subject = $subject;
               $mail->MsgHTML($bodyContent);
               $mail->Body = $bodyContent;
               
                                $model2->send_to = $email;
                                $model2->send_by =$mail_send_by;
                                $model2->send_date = $today;
                                $model2->message = htmlentities($bodyContent);
                                $model2->description =$subject;
                                $model2->created_date =$today;
                                $model2->created_by= Yii::app()->user->getId();
                                $model2->mail_type =$subject;
                               
                            
                                
               if($mail->Send()) {
                 $model->mail_send_status = 1;
                 $sendemail_status = 1;
                 $model2->sendemail_status = $sendemail_status;
                 $model->acknoledge_status = 1;
                $model2->save();
                            if($model->save()){
                                
                                
                            Yii::app()->user->setFlash('success', 'Mail successfully sent!...');    
                            $this->redirect(array('admin'));    
                            }          
               }else{
                    $sendemail_status=0;
                     $model2->save(); 
               }
                             
                            
                            }
                            }
                            //$allmail = implode(",",$email);                                                        
                            }
     public function actionApprove($id){ 
         $model=$this->loadModel($id);
         $model2 = new MailLog;
         $today = date('Y-m-d H:i:s'); 
         $mail_model=GeneralSettings::model()->find(['condition'=>'id = 1']);
         
         if(isset($_POST['Employeerequest']))
		{
                   
			$model->attributes=$_POST['Employeerequest'];
                        $model->acknoledge_status = 2;
                        
			if($model->save()){
                            
                            $user = Users::model()->findByPK($model->created_by);
                            $fullname = $user->first_name.' '.$user->last_name;
                            
                            $admin = Users::model()->findByPK($model->ack_by);
                            $adminfullname = $admin->first_name.' '.$admin->last_name;
                                                        
                            $email = $user->email; 
                                                                                    
                            $mail = new JPhpMailer();                             
                            $subject = 'Employee Request Approved';
                            $headers = Yii::app()->name;
                            $bodyContent = "<p>Hello ".$fullname.",</p><p>This is a reply mail for the employee request made by you on ".$model['request_date']." </p>";
                            $bodyContent.= "<p>This was acknowledged and approved by ".$adminfullname." on ".$model->ack_date.". </p><p> Message : ";
                            //$bodyContent .= $this->renderPartial('_emailform', array('insert_id' => $insert_id), true);                         
                            $msg = nl2br($model->ack_message);
                            $bodyContent.=$msg;
                            

               //die($_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
               $server = (($_SERVER['HTTP_HOST'] == 'localhost') ? '0' : '1');
               //die($server);
               if($server == 0){
                   $mail->IsSMTP();
            //        $mail->Host = SMTPHOST;
            //        $mail->SMTPSecure = SMTPSECURE;
            //        $mail->SMTPAuth = true;
            //        $mail->Username = SMTPUSERNAME;
            //        $mail->Password = SMTPPASS;
                $mail->Host =$mail_model['smtp_host'];
                $mail->Port       = $mail_model['smtp_port'];
                $mail->SMTPSecure = $mail_model['smtp_secure'];
                $mail->SMTPAuth =$mail_model['smtp_auth'];
                $mail->Username = $mail_model['smtp_username'];
                $mail->Password = $mail_model['smtp_password'];
                }
                $mail_send_by=EMAILFROM;
               $mail->setFrom(EMAILFROM);
               $mail->addAddress($email);   // Add a recipient
               $mail->isHTML(true);

               $mail->Subject = $subject;
               $mail->MsgHTML($bodyContent);
               $mail->Body = $bodyContent;
                            $model2->send_to = $email;
                            $model2->send_by =$mail_send_by;
                            $model2->send_date = $today;
                            $model2->message = htmlentities($bodyContent);
                            $model2->description =$subject;
                            $model2->created_date =$today;
                            $model2->created_by= Yii::app()->user->getId();
                            $model2->mail_type =$subject;
                            
                            
               if($mail->Send()) {
                 $sendemail_status = 1;
                 $model2->sendemail_status = $sendemail_status;
                 $model2->save();
                 //$model->acknoledge_status = 1;
                            //if($model->save()){
                            
                            Yii::app()->user->setFlash('success', 'Mail successfully sent!...');    
                            $this->redirect(array('admin'));    
                }else{
                    $sendemail_status = 0;
                    $model2->sendemail_status = $sendemail_status;
                    $model2->save();
                } 
                                     
                            }
				//$this->redirect(array('admin'));
                        }
                          $this->render('approve',array(
			'id'=>$id,'model'=>$model
		));
     } 
          public function actionDisapprove($id){ 
         $model=$this->loadModel($id);
          $model2 = new MailLog;
         $today = date('Y-m-d H:i:s'); 
         $mail_model=GeneralSettings::model()->find(['condition'=>'id = 1']);
         if(isset($_POST['Employeerequest']))
		{
                   
			$model->attributes=$_POST['Employeerequest'];
                        $model->acknoledge_status = 3;
                        
					if($model->save()){
                            $user = Users::model()->findByPK($model->created_by);
                            $fullname = $user->first_name.' '.$user->last_name;
                            
                            $admin = Users::model()->findByPK($model->ack_by);
                            $adminfullname = $admin->first_name.' '.$admin->last_name;
                                                        
                            $email = $user->email;                            
                                                                                    
                            $mail = new JPhpMailer();                             
                            $subject = 'Employee Request Rejected';
                            $headers = Yii::app()->name;
                            $bodyContent="<p>Hello ".$fullname.",</p><p>This is a reply mail for the employee request made by you on ".$model['request_date']." </p>";
                            $bodyContent.="<p>This was acknowledged and rejected by ".$adminfullname." on ".$model->ack_date.". </p><p> Message : ";
                            //$bodyContent .= $this->renderPartial('_emailform', array('insert_id' => $insert_id), true);                         
                             $msg = nl2br($model->ack_message);
                            $bodyContent.=$msg;
                            

               //die($_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
               $server = (($_SERVER['HTTP_HOST'] == 'localhost') ? '0' : '1');
               //die($server);
               if($server == 0){
                   $mail->IsSMTP();
                //    $mail->Host = SMTPHOST;
                //    $mail->SMTPSecure = SMTPSECURE;
                //    $mail->SMTPAuth = true;
                //    $mail->Username = SMTPUSERNAME;
                //    $mail->Password = SMTPPASS;
                   $mail->Host =$mail_model['smtp_host'];
                   $mail->Port       = $mail_model['smtp_port'];
                    $mail->SMTPSecure = $mail_model['smtp_secure'];
                    $mail->SMTPAuth =$mail_model['smtp_auth'];
                    $mail->Username = $mail_model['smtp_username'];
                    $mail->Password = $mail_model['smtp_password'];
               }
                $mail_send_by=EMAILFROM;  
               $mail->setFrom(EMAILFROM);
               $mail->addAddress($email);   // Add a recipient
               $mail->isHTML(true);

               $mail->Subject = $subject;
               $mail->MsgHTML($bodyContent);
               $mail->Body = $bodyContent;
                            $model2->send_to = $email;
                            $model2->send_by =$mail_send_by;
                            $model2->send_date = $today;
                            $model2->message = htmlentities($bodyContent);
                            $model2->description =$subject;
                            $model2->created_date =$today;
                            $model2->created_by= Yii::app()->user->getId();
                            $model2->mail_type =$subject;
                            
                            
               if($mail->Send()) {
                 $sendemail_status = 1;
                 $model2->sendemail_status = $sendemail_status;
                 $model2->save();
                 
                 //$model->acknoledge_status = 1;
                            //if($model->save()){
                            
                            Yii::app()->user->setFlash('success', 'Mail successfully sent!...');    
                            $this->redirect(array('admin'));    
                            }else{
                                $sendemail_status = 0;
                                $model2->sendemail_status = $sendemail_status;
                                $model2->save();
                            } 
                                    
                            }
		}        
         $this->render('disapprove',array(
			'id'=>$id,'model'=>$model
		));
     } 
}