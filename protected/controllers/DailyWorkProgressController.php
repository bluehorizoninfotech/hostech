<?php

Yii::app()->getModule('masters');

class DailyWorkProgressController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;


        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('getProjectItems', 'getItems', 'entry', 'gettasks', 'getprogress', 'getworkprogress', 'getAllTasks', 'gettaskdetails', 'checkdate', 'approveentry', 'recall', 'getSite', 'bulkreject', 'gallery', 'tasklist', 'getwprtaskdetails', 'checkdateavailable', 'checkItemAvailability', 'checkItemCountAvailability', 'getWarehouseItems', 'getItemRate', 'new', 'create_daily_wpr', 'updatewpr', 'deleteItemConsumed', 'view_wpr', 'approve_wpr_entry', 'Rejectentry_wpr', 'workProgress', 'createwpr', 'monthtaskworkProgress', 'getBalanceQuantity', 'ItemEstimatiomMsg', 'getResourceUnit', 'deleteResourceUtilised', 'getexpiretaskdetails', 'create_expire_daily_wpr', 'updateexpiredwpr', 'newwprform', 'wpr', 'saveWorkProgress', 'saveConsumedItems', 'fetchConsumedItem', 'updateConsumedItems', 'deleteConsumedItem', 'saveResource', 'fetchResource', 'deleteResource', 'wprUpdate', 'removeSession', 'checkItemExist', 'dailyProgress','deleteWPR'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('consolidated_mail', 'statuschange', 'approverejectdata', 'getSite'),
                'users' => array('*'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView()
    {
        $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        $task_model = Tasks::model()->findByPk($model->taskid);
        if (!empty($task_model->unit)) {
            $unit_name = $task_model->unit0->unit_title;
        } else {
            $unit_name = '';
        }
        return ($this->renderPartial('view', array(
            'model' => $model,
            'unit_name' => $unit_name
        )
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new DailyWorkProgress;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {


            $taskid = $_POST['DailyWorkProgress']['taskid'];
            $created_date = date("Y-m-d H:i:s");
            $itemdetails = $_POST['newdetails'];


            $projectid = $_POST['DailyWorkProgress']['project_id'];

            $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
            $item_id = $qry['tskid'];
            $tot_qty = $qry['quantity'];

            $data = Yii::app()->db->createCommand(
                "select sum(qty) as qty from pms_daily_work_progress 
			where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' GROUP BY project_id, taskid"
            )->queryRow();
            $existqty = $data['qty'];
            $newqty = $_POST['DailyWorkProgress']['qty'];
            $qty = $newqty + $existqty;
            $percentage = ($newqty / $tot_qty) * 100;
            $max = $tot_qty - $existqty;


            $model->attributes = $_POST['DailyWorkProgress'];
            $model->taskid = $_POST['DailyWorkProgress']['taskid'];
            //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
            $model->work_type = $_POST['DailyWorkProgress']['work_type'];
            $model->daily_work_progress = round($percentage, 2);
            $model->current_status = $_POST['DailyWorkProgress']['current_status'];
            $model->date = date('Y-m-d', strtotime($_POST['DailyWorkProgress']['date']));
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d H:i:s");
            $model->latitude = $_POST['DailyWorkProgress']['latitude'];
            $model->longitude = $_POST['DailyWorkProgress']['longitude'];
            $model->site_id = $_POST['DailyWorkProgress']['site_id'];
            $model->site_name = $_POST['DailyWorkProgress']['site_name'];
            $created_date_model = $model->created_date;
            if (isset($_POST['DailyWorkProgress']['consumed_item_id'])) {
                if ($_POST['DailyWorkProgress']['consumed_item_id'] != "" && $_POST['DailyWorkProgress']['consumed_item_count'] != "" && $_POST['DailyWorkProgress']['consumed_item_rate'] != "") {

                    $task_details = Tasks::model()->findByPk($_POST['DailyWorkProgress']['taskid']);
                    $project = Projects::model()->findByPK($task_details->project_id);
                    $temp_id = $project->template_id;
                    $model->consumed_item_status = 1;
                    $model->consumed_item_id = $_POST['DailyWorkProgress']['consumed_item_id'];
                    $model->consumed_item_count = $_POST['DailyWorkProgress']['consumed_item_count'];
                    if ($_POST['DailyWorkProgress']['item_count_status'] != "") {

                        $model->consumed_item_status = 0;
                    }
                    $wprItemConsumed = $this->wprItemConsumed($_POST['DailyWorkProgress']['taskid'], $_POST['DailyWorkProgress']['consumed_item_id'], $_POST['DailyWorkProgress']['consumed_item_count'], $_POST['DailyWorkProgress']['consumed_item_rate'], $temp_id);
                    $model->template_item_id = $wprItemConsumed;
                    $model->consumed_item_price_id = $_POST['DailyWorkProgress']['consumed_item_rate'];
                }
            }

            if (isset($_POST['DailyWorkProgress']['incident'])) {
                $model->incident = $_POST['DailyWorkProgress']['incident'];
            }
            if (isset($_POST['DailyWorkProgress']['inspection'])) {
                $model->inspection = $_POST['DailyWorkProgress']['inspection'];
            }
            if (isset($_POST['DailyWorkProgress']['visitor'])) {
                $model->visitor_name = $_POST['DailyWorkProgress']['visitor'];
            }
            if (isset($_POST['DailyWorkProgress']['company'])) {
                $model->company_name = $_POST['DailyWorkProgress']['company'];
            }
            if (isset($_POST['DailyWorkProgress']['designation'])) {
                $model->designation = $_POST['DailyWorkProgress']['designation'];
            }
            if (isset($_POST['DailyWorkProgress']['purpose'])) {
                $model->purpose = $_POST['DailyWorkProgress']['purpose'];
            }




            $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
            if ($qry2['assigned_to']) {
                $assigned_to = $qry2['assigned_to'];
            } else {
                $assigned_to = Yii::app()->user->id;
            }

            if ($qty > $tot_qty) {
                Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                $this->render('index', array(
                    'model' => $model,
                    'itemdetails' => $itemdetails,
                    'interval' => 0,
                )
                );
            } else {

                $images = CUploadedFile::getInstancesByName('image');
                $images_array = array();
                $i = 0;
                foreach ($images as $image => $pic) {
                    $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                    $newfilename = rand(1000, 9999) . time();
                    $newfilename = md5($newfilename); //optional
                    $file_name = $newfilename . '.' . $pic->getExtensionName();

                    $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                    if (!file_exists($file))
                        $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                    array_push($images_array, $file_name);
                    $i++;
                }
                if (!empty($images_array)) {
                    $model->img_paths = json_encode($images_array);
                } else {
                    $model->img_paths = NULL;
                }


                if ($model->save() && $model->validate()) {
                    try {
                        if (isset($_POST['DailyWorkProgress']['consumed_item_id'])) {
                            if ($_POST['DailyWorkProgress']['consumed_item_id'] != "" && $_POST['DailyWorkProgress']['consumed_item_count'] != "" && $_POST['DailyWorkProgress']['consumed_item_rate'] != "") {

                                $update_wpr_item_used = $this->UpdateItemConsumed($model->taskid, $_POST['DailyWorkProgress']['consumed_item_id'], $_POST['DailyWorkProgress']['consumed_item_count'], $_POST['DailyWorkProgress']['consumed_item_rate'], $model->id);
                            }
                        }




                        $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                        $dependancy_data = $this->DependancyData($qry2, $model->date);
                        $final_result = $schedule_data . '<br>' . $dependancy_data;
                        Yii::app()->user->setFlash('error', $final_result);
                        Yii::app()->user->setFlash('success', "Successfully created..");
                    } catch (Exception $e) {

                        // Log::trace("Error : " . $e);
                        // throw new Exception("Error : " . $e);
                        print_r($e);
                    }

                    Yii::app()->user->setFlash('success', "Successfully created..");
                    $model->unsetAttributes();
                    $this->redirect(array('index'));
                }
            }
        }

        $this->render('newindex', array(
            'model' => $model,
        )
        );
    }

    protected function CheckprogressPercentags($task, $date, $percentage, $taskid, $type)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        if ($type == 2) {
            $sql_response = Yii::app()->db->createCommand("SELECT id FROM pms_daily_work_progress WHERE taskid ='" . $taskid . "' AND approve_status ='1' GROUP BY date")->queryAll();
            $total_count = count($sql_response);
            $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $taskid . "' AND approve_status ='1'")->queryRow();
        } else {
            $sql_response = Yii::app()->db->createCommand("SELECT id FROM pms_daily_work_progress WHERE taskid ='" . $taskid . "' AND approve_status ='0' GROUP BY date")->queryAll();
            $total_count = count($sql_response);
            $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $taskid . "' AND approve_status ='0'")->queryRow();
        }

        $tasks = Tasks::model()->findByPk($taskid);
        $daily_target = ($total_count * $tasks->daily_target);
        $per_day_percenatge = ($daily_target / $tasks->quantity) * 100;
        $current_given_percenatge = $sql_response2['work_progress'];

        // echo $per_day_percenatge;
        // exit();
        // $total_qty = $task['quantity'];
        // $task_duration = $task['task_duration'];
        // $daily_target = $task['daily_target'];
        // $per_day_percenatge = ($daily_target/$total_qty)*100;
        // $current_given_percenatge = $percentage;
        if (number_format($current_given_percenatge, 2) < number_format($per_day_percenatge, 2)) {
            $value = number_format($per_day_percenatge, 2) - number_format($current_given_percenatge, 2);
            $result = 'Behind Schedule by ' . number_format($value, 2) . '%. ';
            // Daily througtput
            $tblpx = Yii::app()->db->tablePrefix;
            //$sql_response = Yii::app()->db->createCommand("SELECT COUNT(id) as total_count, SUM(qty) as total_quantity FROM {$tblpx}daily_work_progress WHERE id IN ( SELECT MAX(id) FROM {$tblpx}daily_work_progress GROUP BY date) AND taskid ='".$task->tskid."'")->queryRow();
            $sql_response = Yii::app()->db->createCommand("SELECT id FROM pms_daily_work_progress WHERE taskid ='" . $task->tskid . "' AND approve_status ='1' GROUP BY date")->queryAll();
            $total_count = count($sql_response);
            $sql_response3 = Yii::app()->db->createCommand("select SUM(qty) as total_quantity FROM {$tblpx}daily_work_progress WHERE taskid='" . $task->tskid . "'")->queryRow();
            $no_of_days = ($task->task_duration - ($total_count));

            $work_type = WorkType::model()->findByPk($task['work_type_id']);
            $daily_throughput = $work_type->daily_throughput;
            $quantity = $task['quantity'] - $sql_response3['total_quantity'];
            $perday_quantity = ($quantity / $no_of_days);
            $workers_perday = $perday_quantity / $daily_throughput;
            if ($workers_perday < 1) {
                $workers_no = 1;
            } else {
                //$workers_no = round($workers_perday);
                $workers_no = ceil($workers_perday);
            }
            $allowed_workers = $tasks->allowed_workers;
            if ($workers_no > $allowed_workers) {
                $type = 1;
                $worker = $workers_no - $allowed_workers;
                $result .= " This #" . $tasks->tskid . " (" . $tasks->title . ") cannot be completed on time because required workers exceed maximum allowed workers by " . $worker . "";
            } else {
                $type = 2;
                $worker = $workers_no;
                $result .= " Number of workers required to complete task #" . $tasks->tskid . " (" . $tasks->title . "), on time = " . $workers_no . "";
            }
            // $this->taskBehindSchedule($taskid,$worker,$value, number_format($current_given_percenatge,2), $type); //send mail
        } else {

            $value = number_format($current_given_percenatge) - number_format($per_day_percenatge);
            if ($value > 0) {
                $result = '#' . $tasks->tskid . ' (' . $tasks->title . ') ahead Schedule by ' . $value . '%. ';
            } else {
                $result = '#' . $tasks->tskid . ' (' . $tasks->title . ')  On Schedule. ';
            }
        }
        // echo $result;exit;
        return $result;
    }

    protected function DependancyData($task, $date)
    {
        $dependant_tasks = TaskDependancy::model()->findAll(['condition' => 'dependant_task_id = ' . $task['tskid']]);
        if (!empty($dependant_tasks)) {
            $progress_sum = Yii::app()->db->createCommand(
                "select sum(daily_work_progress) as qty from pms_daily_work_progress 
			where project_id=" . $task['project_id'] . " AND taskid='" . $task['tskid'] . "' GROUP BY project_id, taskid"
            )->queryRow();
            $data = '';
            $progress_percenatge = $progress_sum['qty'];
            foreach ($dependant_tasks as $dependant) {
                $depend_task_progress_sum = Yii::app()->db->createCommand(
                    "select sum(daily_work_progress) as qty from pms_daily_work_progress 
				where  taskid='" . $dependant['task_id'] . "' GROUP BY taskid"
                )->queryRow();
                if ($progress_percenatge >= $dependant['dependency_percentage']) {
                    if ($dependant['dependency_percentage'] == '') {
                        $dependant['dependency_percentage'] = 0;
                    }
                    $date = date('d-M-y', strtotime($date . ' +1 day'));
                    if (empty($depend_task_progress_sum['qty'])) {
                        if (empty($data)) {
                            $data .= 'Task ' . $dependant->task->title . ' starts on ' . $date . ' by ' . $dependant['dependency_percentage'] . '% of ' . $task->title . '.';
                        } else {
                            $data .= ',' . 'Task ' . $dependant->task->title . ' starts on ' . $date . ' by ' . $dependant['dependency_percentage'] . '% of ' . $task->title . '.';
                        }
                    }
                }
            }
            return $data;
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {

        $model = $this->loadModel($id);
        $already_exist_item_id = $model->consumed_item_id;
        $already_exist_item_count = $model->consumed_item_count;
        $task_details = Tasks::model()->findByPk($model->taskid);
        $project = Projects::model()->findByPK($task_details->project_id);
        $temp_id = $project->template_id;

        if (($model->created_by == Yii::app()->user->id) || (Yii::app()->user->role == 1)) {
            $total_qty = '';
            $balance_qty = '';
            $unit = '';
            $task_model = Tasks::model()->findByPk($model->taskid);
            if (!empty($task_model)) {
                $tblpx = Yii::app()->db->tablePrefix;
                $total_qty = $task_model->quantity;
                $boq_qty_sum = Yii::app()->db->createCommand("select sum(qty) as qty_total FROM {$tblpx}daily_work_progress WHERE approve_status != 2 AND taskid='" . $model->taskid . "'")->queryRow();
                if (!empty($boq_qty_sum['qty_total'])) {
                    if ($total_qty >= $boq_qty_sum['qty_total']) {
                        $balance_qty = $total_qty - $boq_qty_sum['qty_total'];
                    } else {
                        $balance_qty = 0;
                    }
                } else {
                    $balance_qty = $total_qty;
                }
                $units_data = Unit::model()->findByPk($task_model->unit);
                if (!empty($units_data))
                    $unit = $units_data->unit_code;
            }
            $this->performAjaxValidation($model);
            if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {

                $taskid = $_POST['DailyWorkProgress']['taskid'];
                $created_date = date("Y-m-d H:i:s");
                $itemdetails = "";
                if (isset($_POST['newdetails'])) {
                    $itemdetails = $_POST['newdetails'];
                }

                $projectid = $_POST['DailyWorkProgress']['project_id'];
                $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
                $item_id = $qry['tskid'];
                $tot_qty = $qry['quantity'];

                $data = Yii::app()->db->createCommand(
                    "select sum(qty) as qty from pms_daily_work_progress 
				where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' AND id != " . $id . " GROUP BY project_id, taskid"
                )->queryRow();
                $existqty = $data['qty'];
                $newqty = $_POST['DailyWorkProgress']['qty'];
                $qty = $newqty + $existqty;
                $percentage = ($newqty / $tot_qty) * 100;

                $max = $tot_qty - $existqty;


                $model->attributes = $_POST['DailyWorkProgress'];

                //$model->item_id= $item_id;
                $model->taskid = $_POST['DailyWorkProgress']['taskid'];
                $model->work_type = $_POST['DailyWorkProgress']['work_type'];
                //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
                $model->daily_work_progress = round($percentage, 2);
                $model->current_status = $_POST['DailyWorkProgress']['current_status'];
                $model->date = date('Y-m-d', strtotime($_POST['DailyWorkProgress']['date']));
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d H:i:s");
                $model->approve_status = 0;
                $created_date_model = $model->created_date;
                $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
                if ($qry2['assigned_to']) {
                    $assigned_to = $qry2['assigned_to'];
                } else {
                    $assigned_to = Yii::app()->user->id;
                }



                if ($_POST['DailyWorkProgress']['consumed_item_id'] != "" && $_POST['DailyWorkProgress']['consumed_item_count'] != "" && $_POST['DailyWorkProgress']['consumed_item_rate'] != "") {


                    if ($already_exist_item_id != $_POST['DailyWorkProgress']['consumed_item_id']) {

                        $Criteria = new CDbCriteria();
                        $Criteria->select = 'id';
                        $Criteria->condition = 'find_in_set(' . $already_exist_item_id . ',purchase_item) and temp_id=' . $temp_id;
                        $data = TemplateItems::model()->find($Criteria);

                        $already_exist_item_criteria = new CDbCriteria();
                        $already_exist_item_criteria->condition = 'item_id=' . $data->id . ' AND task_id=' . $_POST['DailyWorkProgress']['taskid'] . ' AND template_id=' . $temp_id;
                        $already_exist_item_det = TaskItemEstimation::model()->find($already_exist_item_criteria);
                        if ($already_exist_item_det) {

                            $count = $already_exist_item_det->item_quantity_used - $model->consumed_item_count;
                            $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $_POST['DailyWorkProgress']['taskid'], 'item_id' => $data->id, 'template_id' => $temp_id));

                            $update_item->attributes = array('item_quantity_used' => $count);

                            $update_item->save();
                        }
                    }
                    $model->consumed_item_status = 1;
                    $model->consumed_item_id = $_POST['DailyWorkProgress']['consumed_item_id'];
                    $model->consumed_item_count = $_POST['DailyWorkProgress']['consumed_item_count'];
                    $model->consumed_item_price_id = $_POST['DailyWorkProgress']['consumed_item_rate'];
                    if ($_POST['DailyWorkProgress']['item_count_status'] != "") {

                        $model->consumed_item_status = 0;
                    }
                    $sql = "SELECT SUM(consumed_item_count) as sum FROM pms_daily_work_progress "
                        . "WHERE taskid ='" . $_POST['DailyWorkProgress']['taskid'] . "' "
                        . "AND consumed_item_id = '" . $_POST['DailyWorkProgress']['consumed_item_id'] . "' "
                        . "AND id != '" . $id . "' ";
                    $tot_qty = Yii::app()->db->createCommand($sql)->queryRow();
                    $item_used_count = $tot_qty['sum'] + $_POST['DailyWorkProgress']['consumed_item_count'];

                    $criteria = new CDbCriteria();
                    $criteria->select = 'id';
                    $criteria->condition = 'find_in_set(' . $_POST['DailyWorkProgress']['consumed_item_id'] . ',purchase_item) and temp_id=' . $temp_id;
                    $data = TemplateItems::model()->find($criteria);

                    $model->template_item_id = $data->id;


                    $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $_POST['DailyWorkProgress']['taskid'], 'item_id' => $data->id, 'template_id' => $temp_id));

                    $update_item->attributes = array('item_quantity_used' => $item_used_count);

                    $update_item->save();

                    $item_rate = Yii::app()->db->createCommand()
                        ->select('rate')
                        ->from('jp_warehousestock')
                        ->where('warehousestock_id=:id', array(':id' => $_POST['DailyWorkProgress']['consumed_item_rate']))
                        ->queryScalar();


                    $update_item_consumed = 'UPDATE pms_acc_wpr_item_consumed SET item_id=' . $_POST['DailyWorkProgress']['consumed_item_id'] . ',item_rate=' . $item_rate . ',item_qty=' . $_POST['DailyWorkProgress']['consumed_item_count'] . ' where wpr_id=' . $id;

                    Yii::app()->db->createCommand($update_item_consumed)->query();
                }


                if ($qty > $tot_qty) {
                    Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                    $this->render('index', array(
                        'model' => $model,
                        'itemdetails' => $itemdetails,
                        'interval' => 0,
                    )
                    );
                } else {

                    $images = CUploadedFile::getInstancesByName('image');
                    $images_array = array();
                    $i = 0;
                    foreach ($images as $image => $pic) {
                        $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                        $newfilename = rand(1000, 9999) . time();
                        $newfilename = md5($newfilename); //optional
                        $file_name = $newfilename . '.' . $pic->getExtensionName();
                        echo $file_name;
                        // echo '<pre>';print_r($pic->getExtensionName());exit;
                        $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                        if (!file_exists($file))
                            $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                        array_push($images_array, $file_name);
                        $i++;
                    }
                    if (!empty($images_array)) {
                        $model->img_paths = json_encode($images_array);
                    } else {
                        $model->img_paths = NULL;
                    }


                    if ($model->save() && $model->validate()) {
                        Yii::app()->user->setFlash('success', "Successfully created..");
                        try {
                            // $command = Yii::app()->db->createCommand(
                            // "CALL afterdayworkprogress(:first,:second)");					
                            // $command->bindParam(":first", $taskid, PDO::PARAM_INT);
                            // $command->bindParam(":second", $assigned_to, PDO::PARAM_INT);
                            // $command->execute();


                            $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                            $dependancy_data = $this->DependancyData($qry2, $model->date);
                            $final_result = $schedule_data . '<br>' . $dependancy_data;
                            Yii::app()->user->setFlash('error', $final_result);
                        } catch (Exception $e) {

                            Log::trace("Error : " . $e);
                            throw new Exception("Error : " . $e);
                        }

                        // Yii::app()->user->setFlash('success', "Successfully created..");
                        $this->sendEditMail($model);
                        $model->unsetAttributes();

                        $this->redirect(array('index'));
                    }
                }
            }
            $account_items = [];
            if (Tasks::model()->accountPermission() == 1) {
                $account_items = $this->getAccountItems();
            }

            $this->render('update_new', array(
                'model' => $model,
                'id' => $id,
                'quantity' => $total_qty,
                'bal_qty' => $balance_qty,
                'unit' => $unit,
                'account_items' => $account_items
            )
            );
        } else {
            $this->redirect(array('index'));
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        /* $dataProvider=new CActiveDataProvider('DailyWorkProgress');
          $this->render('index',array(
          'dataProvider'=>$dataProvider,
          )); */
        $from_date = '';
        $to_date = '';
        unset(Yii::app()->request->cookies['from_date']);  // first unset cookie for dates
        unset(Yii::app()->request->cookies['to_date']);
        $interval = (isset($_GET['interval']) ? $_GET['interval'] : 0);
        $selected_status = (isset($_GET['type']) ? $_GET['type'] : 'All');
        $model = new DailyWorkProgress('search');
        $model->unsetAttributes();  // clear any default values
        if (!empty($_REQUEST)) {
            if (isset($_POST['from_date'])) {
                Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date', $_POST['from_date']); // define cookie for from_date
                $model->from_date = $_REQUEST['from_date'];
            }

            if (isset($_POST['to_date'])) {
                Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date', $_POST['to_date']);
                $model->to_date = $_REQUEST['to_date'];
            }
        }
        if (isset($_REQUEST['DailyWorkProgress'])) {
            $model->attributes = $_REQUEST['DailyWorkProgress'];
        }
        $account_items = [];
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
        }
        $this->render('index', array(
            'model' => $model,
            'interval' => $interval,
            'selected_status' => $selected_status,
            'account_items' => $account_items
        )
        );
    }

    public function getAccountItems()
    {

        $data = array();
        $final = array();


        $spec_sql = "SELECT id, specification, brand_id, cat_id "
            . " FROM jp_specification ";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();

        foreach ($specification as $key => $value) {
            $brand = '';
            if ($value['brand_id'] != NULL) {
                $brand_sql = "SELECT brand_name "
                    . " FROM jp_brand "
                    . " WHERE id=" . $value['brand_id'] . "";
                $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            }

            if ($value['cat_id'] != "") {
                $result = $this->GetParent($value['cat_id']);
            }
            $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }
    public function GetParent($id)
    {

        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM jp_purchase_category WHERE id=" . $id . "")->queryRow();
        return $category;
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new DailyWorkProgress('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['DailyWorkProgress']))
            $model->attributes = $_GET['DailyWorkProgress'];

        $this->render('admin', array(
            'model' => $model,
        )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = DailyWorkProgress::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'wpr-add-form') {

            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actiongetItems($projectid)
    {

        $item_array = array();
        $data = Yii::app()->db->createCommand(
            "select  CONCAT_WS(', ',sl_no,description,qty,unit,CONCAT_WS('-','parent',parentid)) as value,sl_no as label,id as id from wpr_items 
		where projectid=" . $projectid . " AND qty is NOT NULL AND unit is NOT NULL AND rate is NOT NULL"
        )->queryAll();

        //print_r($data); die;
        if (isset($data)) {
            foreach ($data as $newdata) {
                array_push($item_array, $newdata['value']);
            }
        }
        echo json_encode($item_array);
    }

    public function actionworkReport()
    {
        $project_id = $item_id = '';
        $model = new DailyWorkProgress('newsearch');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['DailyWorkProgress'])) {
            //print_r($_GET['DailyWorkProgress']); die;
            $model->attributes = $_GET['DailyWorkProgress'];
            $model->fromdate = $_GET['DailyWorkProgress']['fromdate'];
            $model->todate = $_GET['DailyWorkProgress']['todate'];
        }


        $this->render('workreport', array(
            'model' => $model,
            'dataProvider' => $model->newsearch($project_id, $item_id),
        )
        );
    }

    public function actiongetProjectItems()
    {


        $project_id = $_POST['project_id'];
        $items = CHtml::listData(Items::model()->findAll(
            array(
                'select' => array('id, sl_no'),
                "condition" => 'projectid=' . $project_id . ' AND qty != "" AND unit != "" AND rate != ""',
                'order' => 'sl_no',
                'distinct' => true
            )
        ), 'id', 'sl_no');

        if (count($items)) {
            $itemlist = '<option value="">Select items</option>';
            foreach ($items as $id => $name) {
                $itemlist .= '<option value="' . $id . '">' . $name . '</option>';
            }
        } else {
            $itemlist = '<option value="">No items exist</option>';
        }
        $itemarray = array('items' => $itemlist);
        echo json_encode($itemarray);
    }

    public function actionReports()
    {
        if (isset($_POST['changeoption']) && isset($_POST['date'])) {
            $type = $_POST['changeoption'];
            $chosen_date = $_POST['date'];
            if ($type == 1) {
                $date = strtotime($_POST['date']);
                $date = date('Y-m-d', $date);
                list($start_date, $end_date, $week_title) = $this->x_month_range($date);
            } elseif ($type == 2) {

                $date = strtotime($_POST['date']);
                $date = date('Y-m-d', $date);

                //Initialize the week's table entry structure
                $type = 2;
                list($start_date, $end_date, $week_title) = $this->x_week_range($date);
            } elseif ($type == 3) {
                $date = strtotime($_POST['date']);
                $start_date = date('Y-m-d', $date);
                $end_date = date('Y-m-d', $date);
                $week_title = date('F d, Y', $date);
            }
        } else {
            Yii::app()->session['start_date'] = date("Y-m-d");
            $date = date("Y-m-d");
            $chosen_date = date("d-M-y");
            //Initialize the week's table entry structure
            $type = 2;
            list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-d"));
        }

        $this->render('reports', array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'week_title' => $week_title,
            'type' => $type,
            'chosen_date' => $chosen_date
        )
        );
    }

    public function actionEntry()
    {


        if (isset(Yii::app()->session['start_date']) && isset($_POST['moveto'])) {
            // echo Yii::app()->session['start_date']; die;
            $week_startdate = $this->get_week_date(Yii::app()->session['start_date'], $_POST['moveto']);
        } else {
            Yii::app()->session['start_date'] = date("Y-m-d");
            $week_startdate = date("Y-m-d");
        }

        //Initialize the week's table entry structure
        $list = list($start_date, $end_date, $week_title) = $this->x_week_range($week_startdate);

        //print_r($list); die;		
        Yii::app()->session['start_date'] = $start_date;
        $this->renderPartial('reports', array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'week_title' => $week_title,
        )
        );
    }

    public function x_week_range($date)
    {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 1) ? $ts : strtotime('last monday', $ts);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next sunday', $start)), date('F d, Y', $start));
    }

    public function x_month_range($date)
    {
        $ts = strtotime($date);


        return array(date('Y-m-01', $ts), date('Y-m-t', $ts), date('F, Y', $ts));
    }

    public function add_date($orgDate, $date)
    {
        $cd = strtotime($orgDate);
        $retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $date, date('Y', $cd)));
        return $retDAY;
    }

    public function get_week_date($orgDate, $moveto)
    {
        $cd = strtotime($orgDate);

        if ($moveto == 'prev')
            $retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) - 7, date('Y', $cd)));
        elseif ($moveto == 'next')
            $retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + 7, date('Y', $cd)));
        else
            $retDAY = $orgDate;

        return $retDAY;
    }

    public function getConsolidEntries($start_date, $end_date)
    {

        /* to find array of task ids in that date range */
        $tblpx = Yii::app()->db->tablePrefix;
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition("date", $start_date, $end_date, 'AND');
        $criteria->addCondition('approve_status = 1');
        if (Yii::app()->user->project_id != "") {
            $criteria->addCondition('project_id = ' . Yii::app()->user->project_id . '');
        }

        $dailY_progress_data = DailyWorkProgress::model()->findAll($criteria);

        $task_id_array = array();
        $parent_task_ids = array();
        foreach ($dailY_progress_data as $task_array) {
            $task_model = Tasks::model()->findByPk($task_array['taskid']);
            if (!empty($task_model)) {
                if (empty($task_model['parent_tskid'])) {
                    $chck_sub_tasks = Tasks::model()->find(['condition' => 'parent_tskid = ' . $task_model['tskid']]);
                    if (empty($chck_sub_tasks)) {
                        array_push($task_id_array, $task_array['taskid']);
                    }
                } else {
                    array_push($task_id_array, $task_array['taskid']);
                }
            }
        }

        /* Listing array datas */
        $criteria1 = new CDbCriteria();
        $criteria1->addInCondition('taskid', $task_id_array);
        $criteria1->addCondition('approve_status = 1');
        $criteria1->addBetweenCondition("date", $start_date, $end_date, 'AND');
        $criteria1->group = 'taskid';
        $criteria1->order = 'project_id,item_id,date ASC';
        $data = DailyWorkProgress::model()->findAll($criteria1);
        $data_array = array('data' => $data, 'task_ids' => $parent_task_ids);
        // print_r($data_array['task_ids']);exit;
        // $data_array= json_encode($data_array);
        return $data_array;
    }

    public function getConsolidEntries1($start_date, $end_date)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        // $qry = "SELECT d.*,p.name,i.sl_no,i.rate,i.qty as itemqty,t.title
        $qry = "SELECT d.*,p.name,i.sl_no,t.rate,t.quantity as itemqty,t.title
			   FROM " . $tblpx . "daily_work_progress as d
			   left join " . $tblpx . "items as i on i.id=d.item_id
			   left join " . $tblpx . "projects as p on p.pid=d.project_id
               left join " . $tblpx . "tasks as t on t.tskid=d.taskid
	           where Date between '" . $start_date . "' and '" . $end_date . "' GROUP BY d.item_id ORDER BY d.project_id,d.item_id,d.date ASC";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        return $data;
    }

    public function getcountEntries($start_date, $end_date, $projectid)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT *
			   FROM " . $tblpx . "daily_work_progress as d			 
			   left join " . $tblpx . "tasks as t on t.tskid=d.taskid
	           where d.approve_status = 1 AND d.project_id = " . $projectid . " AND  Date between '" . $start_date . "' and '" . $end_date . "'  GROUP BY t.tskid ORDER BY d.project_id,d.item_id ASC";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        $i = 0;
        //    echo '<pre>';print_r($data);
        foreach ($data as $dat) {
            if (empty($dat['parent_tskid'])) {
                $chck_sub_tasks = Tasks::model()->find(['condition' => 'parent_tskid = ' . $dat['taskid']]);
                if (empty($chck_sub_tasks)) {
                    $i++;
                }
            } else {
                $i++;
            }
        }
        if ($i == 0)
            $i = 1;
        return $i;
    }

    public function getcountEntries1($start_date, $end_date, $projectid)
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT  count(DISTINCT d.item_id) as cnt
			   FROM " . $tblpx . "daily_work_progress as d
			   left join " . $tblpx . "items as i on i.id=d.item_id
			   left join " . $tblpx . "projects as p on p.project_id=d.project_id
	           where d.project_id = " . $projectid . " AND  Date between '" . $start_date . "' and '" . $end_date . "'  ORDER BY d.project_id,d.item_id ASC";
        $data = Yii::app()->db->createCommand($qry)->queryRow();
        return $data;
    }


    public function actionexportExcel($start_date, $end_date, $week_title, $type)
    {

        $objPHPExcel = new PHPExcel();
        $row_count = 0;
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()
            ->getStyle('A1')
            ->getFont()->setBold(true)
            ->setSize(15)
            ->getColor()->setRGB('6F6F6F');
        $objPHPExcel->getActiveSheet()
            ->getStyle('A1')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('A3:A4');
        $objPHPExcel->getActiveSheet()
            ->getCell('A3')
            ->setValue('Projects');

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells('B3:B4');
        $objPHPExcel->getActiveSheet()
            ->getCell('B3')
            ->setValue('Items');

        $objPHPExcel->getActiveSheet()
            ->getCell('C3')
            ->setValue('Up To Previous Week');

        $objPHPExcel->getActiveSheet()
            ->getCell('C4')
            ->setValue('Progress Percentage(%)');

        if ($type == 2) {
            $week_day = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            $week_dates = array();
            $week_date = '';
            $number = 4;
            $date_num = 1;
            $merge_total_quantity_cell = 'K3:K4';
            $total_quantity_cell = 'K3';
            $progression_merge_cells = 'L3:L4';
            $progression_cell = 'L3';
            $title_merge = 'L';

            for ($i = 'D'; $i <= 'J'; $i++) {

                $objPHPExcel->getActiveSheet()
                    ->getCell($i . $number)
                    ->setValue('Quantity');


                $objPHPExcel->getActiveSheet()
                    ->getCell($i . 3)
                    ->setValue($date_num);


                $date_num++;
            }
        } else if ($type == 1) {

            $number = 4;
            $start = strtotime($start_date); // or your date as well
            $end = strtotime($end_date);
            $datediff = $end - $start;
            $datedif = round($datediff / (60 * 60 * 24));
            $date_num = 1;
            if ($datedif == 29) {

                $start_alphabet = 'D';
                $end_alphabet = 'AH';
                $value = 29;
                $merge_total_quantity_cell = 'AH3:AH4';
                $total_quantity_cell = 'AH3';
                $progression_merge_cells = 'AI3:AI4';
                $progression_cell = 'AI3';
                $title_merge = 'AI';
            } else if ($datedif == 30) {
                $start_alphabet = 'D';
                $end_alphabet = 'AI';
                $value = 30;
                $merge_total_quantity_cell = 'AI3:AI4';
                $total_quantity_cell = 'AI3';
                $progression_merge_cells = 'AJ3:AJ4';
                $progression_cell = 'AJ3';
                $title_merge = 'AJ';
            } else if ($datedif == 27) {
                $start_alphabet = 'D';
                $end_alphabet = 'AF';
                $value = 27;
                $merge_total_quantity_cell = 'AF3:AF4';
                $total_quantity_cell = 'AF3';
                $progression_merge_cells = 'AG3:AG4';
                $progression_cell = 'AG3';
                $title_merge = 'AG';
            } else if ($datedif == 28) {
                $start_alphabet = 'D';
                $end_alphabet = 'AG';
                $value = 28;
                $merge_total_quantity_cell = 'AG3:AG4';
                $total_quantity_cell = 'AG3';
                $progression_merge_cells = 'AH3:AH4';
                $progression_cell = 'AH3';
                $title_merge = 'AH';
            }
            for ($i = $start_alphabet; $i !== $end_alphabet; $i++) {

                $objPHPExcel->getActiveSheet()
                    ->getCell($i . $number)
                    ->setValue('QTY');


                $objPHPExcel->getActiveSheet()
                    ->getCell($i . 3)
                    ->setValue($date_num);


                $date_num++;
            }
        } else {
            $title_merge = 'F';
            $merge_total_quantity_cell = 'E1:E2';
            $total_quantity_cell = 'E1';
            $progression_merge_cells = 'F1:F2';
            $progression_cell = 'F1';
            $objPHPExcel->getActiveSheet()
                ->getCell('D3')
                ->setValue($start_date);

            $objPHPExcel->getActiveSheet()
                ->getCell('D4')
                ->setValue('Quantity');

            $objPHPExcel->getActiveSheet()
                ->getCell('E4')
                ->setValue('Total Quantity');

            $objPHPExcel->getActiveSheet()
                ->getCell('F4')
                ->setValue('Progress Percentage(%)');
        }
        $row_count = 0;
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells("A" . ($row_count + 1) . ":" . $title_merge . ($row_count + 2));
        $objPHPExcel->getActiveSheet()
            ->getCell('A1')
            ->setValue('WorkReports - Week of ' . $week_title);
        $objPHPExcel->getActiveSheet()
            ->getStyle('A1')
            ->getFont()->setBold(true)
            ->setSize(15)
            ->getColor()->setRGB('6F6F6F');
        $objPHPExcel->getActiveSheet()
            ->getStyle('A1')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells($merge_total_quantity_cell);
        $objPHPExcel->getActiveSheet()
            ->getCell($total_quantity_cell)
            ->setValue('Total Quantity');


        $objPHPExcel->setActiveSheetIndex()
            ->mergeCells($progression_merge_cells);
        $objPHPExcel->getActiveSheet()
            ->getCell($progression_cell)
            ->setValue('Progress Percentage(%)');



        $weekEntriesResult = $this->getConsolidEntries($start_date, $end_date);
        $weekEntries = $weekEntriesResult['data'];
        // echo "<pre>";
        // print_r($weekEntries); die;

        if (count($weekEntries) == 0) {
            $objPHPExcel->setActiveSheetIndex()
                ->mergeCells('A5:D5');
            $objPHPExcel->getActiveSheet()
                ->getCell('A5')
                ->setValue('This weeks workreports does not have any work added.');
        } else {

            $cell = 5;
            $column = 'A';
            $project = '';

            foreach ($weekEntries as $data) {


                if ($project != $data['project_id']) {
                    $tblpx = Yii::app()->db->tablePrefix;
                    $projectid = $data['project_id'];
                    $project = $data['project_id'];
                    $item = '';
                }
                if (($project == $data['project_id'])) {
                    $tasks = Tasks::model()->findByPk($data['taskid']);

                    $tblpx = Yii::app()->db->tablePrefix;
                    $projectid = $data['project_id'];


                    $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                        ->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()
                        ->getCell($column . $cell)
                        ->setValue($data->project->name);
                    $column++;

                    $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                        ->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()
                        ->getCell($column . $cell)
                        ->setValue($tasks['title']);
                    $column++;

                    $item_id = $data['taskid'];
                    $projectid = $data['project_id'];


                    $tot_prev = "SELECT sum(qty) as qty , sum(no_of_workers) as workers
						   FROM " . $tblpx . "daily_work_progress 
						   where approve_status = 1 AND date < '" . $start_date . "' AND  project_id = " . $projectid . " AND taskid = '" . $item_id . "'";
                    $new_tot_prev = Yii::app()->db->createCommand($tot_prev)->queryRow();


                    if (isset($data['itemqty'])) {
                        $objPHPExcel->getActiveSheet()
                            ->getCell($column . $cell)
                            ->setValue(($new_tot_prev['qty'] / $data['itemqty']) * 100);
                    }


                    $column++;


                    $qty = 0;
                    $tot_workers = 0;
                    $newdate = strtotime($data['date']);
                    if ($type == 2) {
                        for ($i = 0; $i < 7; $i++) {
                            $week_date = strtotime("+$i day", strtotime($start_date));
                            $weekda = date("Y-m-d", $week_date);

                            $newqry = "SELECT sum(qty) as qty , sum(no_of_workers) as workers,t.*
                                FROM " . $tblpx . "daily_work_progress as d
                                left join " . $tblpx . "tasks as t on t.tskid=d.taskid
                                where approve_status = 1 AND date = '" . $weekda . "' AND  d.project_id = " . $projectid . " AND taskid = " . $item_id;
                            $newsql = Yii::app()->db->createCommand($newqry)->queryRow();
                            $check_child_exist = '';
                            if (empty($newsql['parent_tskid']) && !empty($newsql['tskid'])) {
                                $check_sql = "SELECT tskid
                                 FROM " . $tblpx . "tasks as t                          
                                 where t.parent_tskid = " . $newsql['tskid'];
                                $check = Yii::app()->db->createCommand($check_sql)->queryAll();
                                $task_array = array();
                                foreach ($check as $chck) {
                                    array_push($task_array, $chck['tskid']);
                                }
                                if (!empty($task_array)) {
                                    $criteria1 = new CDbCriteria();
                                    $criteria1->addInCondition('taskid', $task_array);
                                    $criteria1->addCondition('date = "' . $weekda . '" ');
                                    $criteria1->addCondition('approve_status = 1');
                                    $check_child_exist = DailyWorkProgress::model()->findAll($criteria1);
                                }
                            }
                            $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($column . $cell)
                                ->setValue($newsql['qty']);
                            $qty = $qty + $newsql['qty'];
                            $tot_workers = $tot_workers + $newsql['workers'];

                            $column++;
                        }
                        $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                            ->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()
                            ->getCell($column . $cell)
                            ->setValue($qty);
                        $column++;
                    } else if ($type == 1) {
                        $start = strtotime($start_date);
                        $end = strtotime($end_date);
                        $datediff = $end - $start;
                        $datedif = round($datediff / (60 * 60 * 24));
                        for ($i = 1; $i <= $datedif + 1; $i++) {
                            $month = date('m', $start);
                            $year = date('Y', $start);
                            $day = $year . '-' . $month . '-' . $i;
                            $newqry = "SELECT sum(qty) as qty , sum(no_of_workers) as workers,t.*
                                FROM " . $tblpx . "daily_work_progress as d
                                left join " . $tblpx . "tasks as t on t.tskid=d.taskid
                                where approve_status = 1 AND date = '" . $day . "' AND  d.project_id = " . $projectid . " AND taskid = " . $item_id;
                            $newsql = Yii::app()->db->createCommand($newqry)->queryRow();
                            $check_child_exist = '';
                            if (empty($newsql['parent_tskid']) && !empty($newsql['tskid'])) {
                                $check_sql = "SELECT tskid
                                    FROM " . $tblpx . "tasks as t                          
                                    where t.parent_tskid = " . $newsql['tskid'];
                                $check = Yii::app()->db->createCommand($check_sql)->queryAll();
                                $task_array = array();
                                foreach ($check as $chck) {
                                    array_push($task_array, $chck['tskid']);
                                }
                                if (!empty($task_array)) {
                                    $criteria1 = new CDbCriteria();
                                    $criteria1->addInCondition('taskid', $task_array);
                                    $criteria1->addCondition('date = "' . $weekda . '" ');
                                    $criteria1->addCondition('approve_status = 1');
                                    $check_child_exist = DailyWorkProgress::model()->findAll($criteria1);
                                }
                            }
                            $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                                ->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()
                                ->getCell($column . $cell)
                                ->setValue($newsql['qty']);
                            $qty = $qty + $newsql['qty'];
                            $tot_workers = $tot_workers + $newsql['workers'];

                            $column++;
                        }
                        $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                            ->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()
                            ->getCell($column . $cell)
                            ->setValue($qty);
                        $column++;
                    } elseif ($type == 3) {
                        $date = date('Y-m-d', strtotime($start_date));
                        $newqry = "SELECT sum(qty) as qty , sum(no_of_workers) as workers,t.*
                    FROM " . $tblpx . "daily_work_progress as d
                    left join " . $tblpx . "tasks as t on t.tskid=d.taskid
                    where approve_status = 1 AND date = '" . $date . "' AND  d.project_id = " . $projectid . " AND taskid = " . $item_id;
                        $newsql = Yii::app()->db->createCommand($newqry)->queryRow();
                        $check_child_exist = '';
                        if (empty($newsql['parent_tskid']) && !empty($newsql['tskid'])) {
                            $check_sql = "SELECT tskid
                        FROM " . $tblpx . "tasks as t                          
                        where t.parent_tskid = " . $newsql['tskid'];
                            $check = Yii::app()->db->createCommand($check_sql)->queryAll();
                            $task_array = array();
                            foreach ($check as $chck) {
                                array_push($task_array, $chck['tskid']);
                            }
                            if (!empty($task_array)) {
                                $criteria1 = new CDbCriteria();
                                $criteria1->addInCondition('taskid', $task_array);
                                $criteria1->addCondition('date = "' . $weekda . '" ');
                                $criteria1->addCondition('approve_status = 1');
                                $check_child_exist = DailyWorkProgress::model()->findAll($criteria1);
                            }
                        }
                        $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                            ->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()
                            ->getCell($column . $cell)
                            ->setValue($newsql['qty']);
                        $qty = $qty + $newsql['qty'];
                        $tot_workers = $tot_workers + $newsql['workers'];

                        $column++;

                        $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                            ->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()
                            ->getCell($column . $cell)
                            ->setValue($qty);
                        $column++;
                    }

                    $tot_tod = "SELECT sum(qty) as qty , sum(no_of_workers) as workers
                FROM " . $tblpx . "daily_work_progress 
                where approve_status = 1 AND date <= '" . $end_date . "' AND  project_id = " . $projectid . " AND taskid = '" . $item_id . "'";
                    $new_tot_tod = Yii::app()->db->createCommand($tot_tod)->queryRow();


                    $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                        ->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()
                        ->getCell($column . $cell)
                        ->setValue(round(($new_tot_tod['qty'] / $data->tasks->quantity) * 100, 2));
                    $column++;

                    $tot_tod = "SELECT sum(qty) as qty , sum(no_of_workers) as workers
						FROM " . $tblpx . "daily_work_progress 
						where date <= '" . $end_date . "' AND  project_id = " . $projectid . " AND item_id = '" . $item_id . "'";
                    $new_tot_tod = Yii::app()->db->createCommand($tot_tod)->queryRow();

                    $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                        ->setAutoSize(true);
                    if (isset($data['itemqty'])) {
                        $objPHPExcel->getActiveSheet()
                            ->getCell($column . $cell)
                            ->setValue(($new_tot_tod['qty'] / $data['itemqty']) * 100);
                    }
                    $column = 'A';
                    $cell++;
                }
            } //closing of $project == $data['project_id']
        }
        ob_end_clean();
        ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Reports.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function actionGettasks()
    {

        if ($_REQUEST['project_id'] != "") {
            $project_id = $_REQUEST['project_id'];
            $item = $_REQUEST['item_id'];
            $tblpx = Yii::app()->db->tablePrefix;
            //echo $item;
            //echo "select * FROM {$tblpx}items WHERE sl_no='".$item. "'AND projectid='".$project_id."'";

            $sql_response = Yii::app()->db->createCommand("select * FROM {$tblpx}items WHERE sl_no='" . $item . "'AND projectid='" . $project_id . "'")->queryAll();

            if ($sql_response != null) {
                foreach ($sql_response as $mainkey => $mainvalue) {
                    foreach ($mainvalue as $key => $value) {
                        if ($key == 'description') {
                            $data['item_description'] = $value;
                        } elseif ($key == 'taskid') {
                            $data['taskid'] = $value;
                            $taskid = $value;
                            $sql_response2 = Yii::app()->db->createCommand("select `title` FROM {$tblpx}tasks WHERE tskid='" . $taskid . "'")->queryAll();
                            if ($sql_response2 != null) {
                                foreach ($sql_response2 as $key => $value) {
                                    $data['task_name'] = $value['title'];
                                }
                            }
                        } elseif ($key == 'qty') {
                            $data['total_qty'] = $value;
                        } elseif ($key == 'id') {
                            $data['main_id'] = $value;
                        }
                    }
                }
            } else {
                $data['main_id'] = 0;
                $data['task_name'] = 'No Task is associated with this Item';
            }
        } else {
            $data['main_id'] = 0;
            $data['task_name'] = 'No Task is associated with this Item';
        }
        print (json_encode($data, JSON_PRETTY_PRINT));
    }

    public function actionGetprogressold()
    {

        $qty = $_REQUEST['qty'];
        $progress = 0;
        $prev_progress = 0;
        $total_qty = $qty;
        $tblpx = Yii::app()->db->tablePrefix;
        $scenario = $_REQUEST['scenario'];
        if (isset($_REQUEST['item_total_qty']) && isset($_REQUEST['item_id'])) {
            $item_total_qty = $_REQUEST['item_total_qty'];

            $item_id = $_REQUEST['item_id'];
            $sql = "select * FROM {$tblpx}daily_work_progress "
                . "WHERE approve_status != 2 AND taskid='" . $item_id . "'";
            $sql_response3 = Yii::app()->db->createCommand($sql)->queryAll();

            if ($sql_response3 != null) {
                foreach ($sql_response3 as $mainkey => $mainvalue) {
                    foreach ($mainvalue as $key => $value) {
                        if ($key == 'daily_work_progress') {
                            $prev_progress += $value;
                        } elseif ($key == 'qty') {
                            $total_qty += $value;
                        }
                    }
                }
            }
            $task_details = Tasks::model()->findByPk($item_id);
            $message = '';
            if ($item_total_qty != '' && $prev_progress != 100 && $total_qty <= $item_total_qty) {
                $total_progress = $prev_progress;
                $progress = ($qty / $item_total_qty) * 100;
                $total_progress += $progress;
                $progress = number_format($progress, 2);
                $balance_qty = $item_total_qty - $total_qty;
            } elseif ($prev_progress == 100) {
                $progress = 0;
                $message = 'No quantity available';
                $balance_qty = 0;
            } elseif ($total_qty > $item_total_qty) {

                $progress = 0;
                $message = 'Exceeded total quantity';
                $balance_qty = 0;
            } else {
                $message = '';
                $progress = 0;
                $balance_qty = 0;
            }
        }
        $result_array = array('progress' => $progress, 'msg' => $message, 'balance_qty' => $balance_qty);

        echo json_encode($result_array);
        //print_r($sql_response3);
    }
    public function actionGetprogress()
    {

        $qty = $_REQUEST['qty'];
        $progress = 0;
        $prev_progress = 0;
        $total_qty = $qty;
        $total_entered_qty = 0;
        $tblpx = Yii::app()->db->tablePrefix;
        $scenario = $_REQUEST['scenario'];
        $balance_qty = 0;
        $total_wpr_qty = 0;

        if ($scenario != "") {

            if (isset($_REQUEST['item_total_qty']) && isset($_REQUEST['wpr_taskid'])) {
                $item_total_qty = $_REQUEST['item_total_qty'];
                $item_id = $_REQUEST['wpr_taskid'];

                $old_quantity = $_REQUEST['old_quantity'];


                //   extra validation for quantity

                $task = Tasks::model()->findByPk($item_id);

                $parent_task = $task->parent_tskid;

                if ($parent_task != "") {
                    // sub task 

                    $sql_query = "select * FROM {$tblpx}daily_work_progress "
                        . "WHERE approve_status != 2 AND taskid IN ('" . $parent_task . "')";
                    $data = Yii::app()->db->createCommand($sql_query)->queryAll();

                    if ($data != null) {
                        foreach ($data as $key => $val) {
                            foreach ($val as $key1 => $values) {
                                if ($key1 == 'qty') {
                                    $total_wpr_qty += $values;
                                }
                            }
                        }
                    }
                }

                // parent tasks
                $get_sub_tasks = Tasks::model()->findAll(array('condition' => 'parent_tskid =' . $item_id));
                $sub_task_array = array();
                foreach ($get_sub_tasks as $sub_tasks) {
                    $sub_task_array[] = $sub_tasks->tskid;
                }

                if (count($sub_task_array) > 0) {
                    $task_ids = implode(',', $sub_task_array);
                    $sql_query = "select * FROM {$tblpx}daily_work_progress "
                        . "WHERE approve_status != 2 AND taskid IN ('" . $task_ids . "')";
                    $data = Yii::app()->db->createCommand($sql_query)->queryAll();
                    if ($data != null) {
                        foreach ($data as $key => $val) {
                            foreach ($val as $key1 => $values) {
                                if ($key1 == 'qty') {
                                    $total_wpr_qty += $values;
                                }
                            }
                        }
                    }
                }

                // end extra validation for quantity




                $sql = "select * FROM {$tblpx}daily_work_progress "
                    . "WHERE approve_status != 2 AND taskid='" . $item_id . "' and id != '" . $scenario . "'";
                $sql_response3 = Yii::app()->db->createCommand($sql)->queryAll();

                if ($sql_response3 != null) {
                    foreach ($sql_response3 as $mainkey => $mainvalue) {
                        foreach ($mainvalue as $key => $value) {

                            if ($key == 'daily_work_progress') {
                                $prev_progress += $value;
                            } elseif ($key == 'qty') {
                                $total_entered_qty += $value;
                            }
                        }
                    }
                }

                if ($total_wpr_qty != 0) {
                    $total_entered_qty += $total_wpr_qty;
                }



                $task_details = Tasks::model()->findByPk($item_id);
                $message = '';
                if ($item_total_qty != '' && $prev_progress != 100 && $total_entered_qty <= $item_total_qty) {
                    $total_progress = $prev_progress;
                    $progress = ($qty / $item_total_qty) * 100;
                    $total_progress += $progress;
                    $progress = number_format($progress, 2);
                    $balance_quantity = $item_total_qty - $total_entered_qty;

                    $balance_qty = $balance_quantity - $qty;
                    if ($balance_qty < 0) {
                        $message = "Exceeded main task total quantity";
                        $balance_qty = 0;
                    }
                } elseif ($prev_progress == 100) {
                    $progress = 0;
                    $message = 'No quantity available';
                    $balance_qty = 0;
                } elseif ($total_entered_qty > $item_total_qty) {
                    $progress = 0;
                    $message = 'Exceeded total quantity';
                    $balance_qty = 0;
                } else {
                    $message = '';
                    $progress = 0;
                    $balance_qty = 0;
                }
            }
        } else {

            if (isset($_REQUEST['item_total_qty']) && isset($_REQUEST['item_id'])) {
                $item_total_qty = $_REQUEST['item_total_qty'];

                $item_id = $_REQUEST['item_id'];
                $sql = "select * FROM {$tblpx}daily_work_progress "
                    . "WHERE approve_status != 2 AND taskid='" . $item_id . "'";
                $sql_response3 = Yii::app()->db->createCommand($sql)->queryAll();

                // extra validation for quantity

                $task = Tasks::model()->findByPk($item_id);

                $parent_task = $task->parent_tskid;

                if ($parent_task != "") {
                    // sub task 

                    $sql_query = "select * FROM {$tblpx}daily_work_progress "
                        . "WHERE approve_status != 2 AND taskid IN ('" . $parent_task . "')";
                    $data = Yii::app()->db->createCommand($sql_query)->queryAll();

                    if ($data != null) {
                        foreach ($data as $key => $val) {
                            foreach ($val as $key1 => $values) {
                                if ($key1 == 'qty') {
                                    $total_wpr_qty += $values;
                                }
                            }
                        }
                    }
                }
                //else{

                // parent tasks
                $get_sub_tasks = Tasks::model()->findAll(array('condition' => 'parent_tskid =' . $item_id));
                $sub_task_array = array();
                foreach ($get_sub_tasks as $sub_tasks) {
                    $sub_task_array[] = $sub_tasks->tskid;
                }

                if (count($sub_task_array) > 0) {
                    $task_ids = implode(',', $sub_task_array);
                    $sql_query = "select * FROM {$tblpx}daily_work_progress "
                        . "WHERE approve_status != 2 AND taskid IN ('" . $task_ids . "')";
                    $data = Yii::app()->db->createCommand($sql_query)->queryAll();
                    if ($data != null) {
                        foreach ($data as $key => $val) {
                            foreach ($val as $key1 => $values) {
                                if ($key1 == 'qty') {
                                    $total_wpr_qty += $values;
                                }
                            }
                        }
                    }
                }

                //}

                // end extra validation for quantity

                if ($sql_response3 != null) {
                    foreach ($sql_response3 as $mainkey => $mainvalue) {
                        foreach ($mainvalue as $key => $value) {
                            if ($key == 'daily_work_progress') {
                                $prev_progress += $value;
                            } elseif ($key == 'qty') {
                                $total_qty += $value;
                            }
                        }
                    }
                }

                if ($total_wpr_qty != 0) {
                    $total_qty += $total_wpr_qty;
                }


                $task_details = Tasks::model()->findByPk($item_id);
                $message = '';
                if ($item_total_qty != '' && $prev_progress != 100 && $total_qty <= $item_total_qty) {
                    $total_progress = $prev_progress;
                    $progress = ($qty / $item_total_qty) * 100;
                    $total_progress += $progress;
                    $progress = number_format($progress, 2);
                    $balance_qty = $item_total_qty - $total_qty;
                    if ($balance_qty < 0) {
                        $message = "Exceeded main task total quantity";
                    }
                } elseif ($prev_progress == 100) {
                    $progress = 0;
                    $message = 'No quantity available';
                    $balance_qty = 0;
                } elseif ($total_qty > $item_total_qty) {

                    $progress = 0;
                    $message = 'Exceeded total quantity';
                    $balance_qty = 0;
                } else {
                    $message = '';
                    $progress = 0;
                    $balance_qty = 0;
                }
            }
        }
        $result_array = array('progress' => $progress, 'msg' => $message, 'balance_qty' => $balance_qty);

        echo json_encode($result_array);

        //print_r($sql_response3);
    }


    public function actionGetworkprogress()
    {
        $qty = $_REQUEST['qty'];
        $progress = 0;
        $prev_progress = 0;
        $total_qty = $qty;
        $tblpx = Yii::app()->db->tablePrefix;
        $wpr_id = $_REQUEST['item_wpr_id'];

        if (isset($_REQUEST['item_total_qty']) && isset($_REQUEST['item_id'])) {
            $item_total_qty = $_REQUEST['item_total_qty'];
            $item_id = $_REQUEST['item_id'];
            $sql = "select * FROM {$tblpx}daily_work_progress "
                . "WHERE approve_status != 2 AND taskid='" . $item_id . "'";
            $sql_response3 = Yii::app()->db->createCommand($sql)->queryAll();

            $wpr_sql = "select sum(qty) as qty FROM {$tblpx}daily_work_progress "
                . "WHERE approve_status != 2 AND taskid='" . $item_id . "'";

            $wpr_sql_response = Yii::app()->db->createCommand($wpr_sql)->queryRow();

            $wpr_total_qty = $wpr_sql_response['qty'];



            if ($sql_response3 != null) {
                foreach ($sql_response3 as $mainkey => $mainvalue) {
                    foreach ($mainvalue as $key => $value) {
                        if ($key == 'daily_work_progress') {
                            $prev_progress += $value;
                        } elseif ($key == 'qty') {
                            $total_qty += $value;
                        }
                    }
                }
            }
            $task_details = Tasks::model()->findByPk($item_id);
            $message = '';
            $wpr_details = DailyWorkProgress::model()->findByPk($wpr_id);
            $wpr_qty = $wpr_details->qty;

            if ($item_total_qty != '' && $prev_progress != 100) {
                $total_progress = $prev_progress;
                $progress = ($qty / $item_total_qty) * 100;
                $total_progress += $progress;
                $progress = number_format($progress, 2);

                // $balance_qty = $item_total_qty - $total_qty;

                if ($qty > $wpr_qty) {
                    $actual_qty = $item_total_qty - $wpr_total_qty;
                    $bal_quantity = $qty - $wpr_qty;
                    //  $balance_qty =   ($item_total_qty-$wpr_total_qty)-($qty-$wpr_qty);
                    $balance_qty = $actual_qty - $bal_quantity;

                    if ($balance_qty < 0) {
                        $progress = 0;
                        $message = 'Exceeded total quantity';
                        $balance_qty = 0;
                    }
                } else {
                    $actual_qty = $item_total_qty - $wpr_total_qty;

                    $bal_quantity = $wpr_qty - $qty;

                    $balance_qty = $actual_qty + $bal_quantity;

                    $progress = ($qty / $item_total_qty) * 100;
                }
            } elseif ($prev_progress == 100) {

                $progress = 0;
                $message = 'No quantity available';
                $balance_qty = 0;
            }

            // elseif ($total_qty > $item_total_qty) {
            //     echo 456;exit;
            //     $progress = 0;
            //     $message = 'Exceeded total quantity';
            // $balance_qty = 0;
            // } else {
            //     echo 789;exit;
            //     $message = '';
            //     $progress = 0;
            //     $balance_qty = 0;
            // }
        }
        $result_array = array('progress' => $progress, 'msg' => $message, 'balance_qty' => $balance_qty);
        echo json_encode($result_array);
        //print_r($sql_response3);
    }

    public function actiongetAllTasks()
    { {
            $tblpx = Yii::app()->db->tablePrefix;
            if (isset($_REQUEST['project_id'])) {
                $pid = $_REQUEST['project_id'];
            }
            $date = date('Y-m-d');
            if (isset($_GET['date']) and trim($_GET['date']) != '') {
                $date = date('Y-m-d', strtotime($_GET['date']));
            }

            $condition1 = "WHERE task_type = 1 and start_date <= '$date' "
                . "AND due_date >= '$date' AND project_id = " . $pid . "  "
                . "AND parent_tskid is not null and (assigned_to =" . Yii::app()->user->id . ") order by title";

            //            if (Yii::app()->user->role != 1) {
            //                $condition1 .= ' AND (assigned_to =' . Yii::app()->user->id . ' or coordinator=' . Yii::app()->user->id . ' or created_by=' . Yii::app()->user->id . ')';
            //            }
            $sql = "select *, if(parent_tskid is not null, concat(' - ',title), title) as title "
                . "FROM {$tblpx}tasks " . $condition1;
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            $current_date = ($date != '' ? date('Y-m-d') : $date);
            $sql = "SELECT taskid FROM {$tblpx}daily_work_progress "
                . "WHERE date = '" . $current_date . "'";
            $boq_entires = Yii::app()->db->createCommand($sql)->queryAll();
            $taskids = array_column($boq_entires, 'taskid');
            $data = CHtml::listData($data, 'tskid', 'title');
            echo "<option value=''>Select Task</option>";
            $added_tasks = array();
            foreach ($data as $value => $site_name) {
                if (in_array($value, $taskids)) {
                    $added_tasks[$value] = $site_name;
                } else {
                    echo CHtml::tag('option', array('value' => $value), CHtml::encode($site_name), true);
                }
            }
            foreach ($added_tasks as $value => $task_name) {
                echo CHtml::tag('option', array('value' => $value, 'class' => 'green_color'), CHtml::encode($task_name), true);
            }
        }
    }

    public function actiongettaskdetails()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $taskid = $_REQUEST['taskid'];
        $sql_response2 = Tasks::model()->findByPk($taskid);
        $balance_qty = 0;
        $html = '';
        $total_wpr_qty = 0;
        if (!empty($sql_response2)) {
            $boq_qty = Yii::app()->db->createCommand("select sum(qty) as qty FROM {$tblpx}daily_work_progress WHERE approve_status != 2 AND taskid='" . $taskid . "'")->queryRow();


            // extra validation for quantity

            $parent_task = $sql_response2->parent_tskid;

            if ($parent_task != "") {
                // sub task 

                $sql_query = "select * FROM {$tblpx}daily_work_progress "
                    . "WHERE approve_status != 2 AND taskid IN ('" . $parent_task . "')";
                $data = Yii::app()->db->createCommand($sql_query)->queryAll();

                if ($data != null) {
                    foreach ($data as $key => $val) {
                        foreach ($val as $key1 => $values) {
                            if ($key1 == 'qty') {
                                $total_wpr_qty += $values;
                            }
                        }
                    }
                }
            }

            // parent tasks
            $get_sub_tasks = Tasks::model()->findAll(array('condition' => 'parent_tskid =' . $taskid));
            $sub_task_array = array();
            foreach ($get_sub_tasks as $sub_tasks) {
                $sub_task_array[] = $sub_tasks->tskid;
            }

            if (count($sub_task_array) > 0) {
                $task_ids = implode(',', $sub_task_array);
                $sql_query = "select * FROM {$tblpx}daily_work_progress "
                    . "WHERE approve_status != 2 AND taskid IN ('" . $task_ids . "')";
                $data = Yii::app()->db->createCommand($sql_query)->queryAll();
                if ($data != null) {
                    foreach ($data as $key => $val) {
                        foreach ($val as $key1 => $values) {
                            if ($key1 == 'qty') {
                                $total_wpr_qty += $values;
                            }
                        }
                    }
                }
            }


            // end extra validation for quantity



            if (!empty($boq_qty)) {
                $boq_qty_val = $boq_qty['qty'];
                $balance_qty = $sql_response2['quantity'] - $boq_qty_val;
            } else {
                $balance_qty = $sql_response2['quantity'];
            }

            if ($total_wpr_qty != 0) {
                $balance_qty = $balance_qty - $total_wpr_qty;
            }


            $unit = $sql_response2->unit0->unit_code;
            // $unit = $sql_response2['unit'];
            $work_type = WorkType::model()->findByPk($sql_response2['work_type_id']);
            $html .= "<option value=" . $work_type['wtid'] . ">" . $work_type['work_type'] . "</option>";

            echo json_encode(array('taskid' => $sql_response2['tskid'], 'work_type' => $sql_response2['work_type_id'], 'option' => $html, 'total_qty' => $sql_response2['quantity'], 'unit' => $unit, 'balance_qty' => $balance_qty));
        } else {
            $html = "<option value=''>Select work type</option>";
            echo json_encode(array('taskid' => "", 'option' => $html, 'total_qty' => "", 'balance_qty' => $balance_qty));
        }
    }

    public function actioncheckdate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_POST['date']) && isset($_POST['project_id'])) {
            $task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
            $date = date('Y-m-d', strtotime($_POST['date']));
            $criteria = new CDbCriteria;
            if (isset(Yii::app()->user->id)) {
                $criteria->condition = "task_type = 1 AND wpr_status = 0 AND status IN (6,9) "
                    . "AND (assigned_to = " . Yii::app()->user->id . ") "
                    . "AND project_id = " . $_POST['project_id']
                    . " AND start_date <= '$date' "
                    . "AND due_date >= '$date'";
            } else {
                $criteria->condition = "task_type = 1 AND wpr_status = 0 AND status IN (6,9)"
                    . " AND project_id = " . $_POST['project_id']
                    . " AND start_date <= '$date' "
                    . " AND due_date >= '$date' ";
            }
            if (isset($_POST['task_id']) && $_POST['task_id']) {
                $criteria->condition .= " AND (tskid = " . $_POST['task_id'] . ")";
            }
            $criteria->condition .= " order by title";

            $sql = "SELECT taskid FROM {$tblpx}daily_work_progress "
                . "WHERE date = '" . $date . "'";
            $boq_entires = Yii::app()->db->createCommand($sql)->queryAll();
            $taskids = array_column($boq_entires, 'taskid');
            $data = Tasks::model()->findAll($criteria);
            $data_array = array();
            $tsk_id = "";
            $work_type = "";
            $tsk_id = "";
            $quantity = "";
            $unit = "";
            $work_type_html = "";
            if (count($data) == 1) {
                foreach ($data as $key => $value) {
                    array_push($data_array, $value->tskid);
                }
                $tsk_id = implode(" ", $data_array);
                $task = Tasks::model()->findByPk($tsk_id);
                $work_type = $task->work_type_id;
                $work_types = WorkType::model()->findByPk($work_type);
                $work_type_html .= "<option value=" . $work_types['wtid'] . ">" . $work_types['work_type'] . "</option>";
                $tsk_id = $tsk_id;
                $quantity = $task->quantity;
                if ($task->unit != 0) {
                    $units_data = Unit::model()->findByPk($task->unit);
                    $unit = $units_data->unit_code;
                }
                $balance_qty = 0;
                if (!empty($task)) {
                    $boq_qty = Yii::app()->db->createCommand("select sum(qty) as qty FROM {$tblpx}daily_work_progress WHERE approve_status != 2 AND taskid='" . $tsk_id . "'")->queryRow();
                    if (!empty($boq_qty)) {
                        $boq_qty_val = $boq_qty['qty'];
                        $balance_qty = $task->quantity - $boq_qty_val;
                    } else {
                        $balance_qty = $task->quantity;
                    }
                }
                $bal_qty_status = 1;
                $bal_qty_count = $balance_qty;

                // 

            }
            $html = '';
            $added_tasks = array();
            if (!empty($data)) {
                $html .= '<option value="">Choose a task</option>';
                foreach ($data as $key => $value) {
                    if (in_array($value['tskid'], $taskids)) {
                        if ($value['parent_tskid'] != null) {
                            $task_name = "- - " . $value['title'];
                        } else {
                            $task_name = "- " . $value['title'];
                        }

                        $html .= '<option value=' . $value['tskid'] . ' class="green_color">' . $task_name . '</option>';
                    } else {
                        // $html .= '<option value=' . $value['tskid'] . '>' . $value['title'] . '</option>';                     
                        $html .= '<option value=' . $value['tskid'] . '>' . ($value['parent_tskid'] != null ? '- - ' : '- ') . $value['title'] . '</option>';
                    }
                }

                $balance_qty = 0;
                if (count($data) == 1) {
                    $boq_qty = Yii::app()->db->createCommand("select sum(qty) as qty FROM {$tblpx}daily_work_progress WHERE approve_status != 2 AND taskid='" . $tsk_id . "'")->queryRow();
                    if (!empty($boq_qty)) {
                        $boq_qty_val = $boq_qty['qty'];
                        $balance_qty = $task->quantity - $boq_qty_val;
                    } else {
                        $balance_qty = $task->quantity;
                    }

                    $bal_qty_status = 1;

                    $bal_qty_count = $balance_qty;
                } else {
                    $bal_qty_status = 0;
                    $bal_qty_count = '';
                }
            } else {
                $html .= '';
                $bal_qty_status = 0;
                $bal_qty_count = '';
            }
            $response_data = array(
                'html' => $html,
                'tsk_id' => $tsk_id,
                'work_type' => $work_type,
                'quantity' => $quantity,
                'unit' => $unit,
                'work_type_html' => $work_type_html,
                'bal_qty_status' => $bal_qty_status,
                'bal_qty_count' => $bal_qty_count
            );
            echo json_encode($response_data);
        }
    }

    public function taskBehindSchedule($id, $workers_no, $percent, $current_given_percenatge, $type)
    {
        $tasks = Tasks::model()->findByPk($id);
        $projects = Projects::model()->findByPk($tasks->project_id);
        $users = Users::model()->findByPk($tasks->report_to);
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT SUM(daily_work_progress) as work_progress "
            . "FROM pms_daily_work_progress "
            . "WHERE taskid ='" . $id . "' AND approve_status = 1";
        $sql_response2 = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($sql_response2)) {
            $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
            $model2 = new MailLog;
            $mail = new JPhpMailer();
            $today = date('Y-m-d H:i:s');
            $headers = "Hostech";
            $mail_data = '
		<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
		<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $tasks->assignedTo->first_name . '</span></h3>
		<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
		<tr><td colspan="2" style="background-color: #000;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $this->logo . '" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
		</tr>
		<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>Daily Work Progress Alert</h2></div></td>
		</tr>
		<tr><td colspan="2">This is a notification to let you know that Daily Work Progress Alert: </td></tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project: </td><td style="border:1px solid #f5f5f5;">' . $projects->name . '</td></tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $tasks->title . '</td></tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start & Due dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($tasks->start_date)) . ' & ' . date('d-M-y', strtotime($tasks->due_date)) . '</td></tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Current progress:</td><td style="border:1px solid #f5f5f5;">' . number_format($current_given_percenatge, 2) . '%</td></tr>
		<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Current status :</td><td style="border:1px solid #f5f5f5;"> behind by ' . $percent . '% </td></tr> ';
            if ($type == 1) {
                $mail_data .= '<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Additional number of wokers required:</td><td style="border:1px solid #f5f5f5;">' . $workers_no . '</td></tr>';
            } else {
                $mail_data .= '<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Number of wokers required:</td><td style="border:1px solid #f5f5f5;">' . $workers_no . '</td></tr>';
            }
            $mail_data .= '</table>
		<p>Sincerely,  <br />
		' . Yii::app()->name . '</p>
		</div>';
            //$mail_data = "<p>Project : ".$projects->name."</p><p>Task ID : #".$tasks->tskid."</p><p>Task : ".$tasks->title."</p><p>Date : ".$tasks->start_date." to ".$tasks->due_date."</p><p>Current progress : ".number_format($current_given_percenatge,2)."%</p><p>Current status behind by: ".$percent." %</p><p>Number of wokers required : ".$workers_no."</p>";
            $bodyContent = $mail_data;
            $mail->IsSMTP();
            $mail->Host = $mail_model['smtp_host'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
            $mail->Subject = 'Daily Work Progress Alert';
            $receippients = $users->email;
            $mail->addAddress($users->email); // Add a recipient
            $mail_send_by = array(
                'smtp_email_from' => $mail_model['smtp_email_from'],
                'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']
            );
            $dataJSON = json_encode($mail_data);
            $receippientsJSON = json_encode($receippients);
            $mail_send_byJSON = json_encode($mail_send_by);
            $subject = $mail->Subject;
            $model2->send_to = $receippientsJSON;
            $model2->send_by = $mail_send_byJSON;
            $model2->send_date = $today;
            $model2->message = htmlentities($bodyContent);
            $model2->description = $mail->Subject;
            $model2->created_date = $today;
            $model2->created_by = Yii::app()->user->getId();
            $model2->mail_type = $subject;
            $mail->isHTML(true);
            $mail->MsgHTML($bodyContent);
            $mail->Body = $bodyContent;
            if ($mail->Send()) {
                $model2->sendemail_status = 1;
            } else {
                $model2->sendemail_status = 0;
            }
            $model2->save();
        }
    }

    public function actionApproveentry()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['entry_id'])) {
            $id = $_REQUEST['entry_id'];
            $time_entry_model = $this->loadModel($id);
            if (!empty($time_entry_model->current_status) && $time_entry_model->current_status != 0) {
                $task_model = Tasks::model()->findByPk($time_entry_model->taskid);
                if (!empty($task_model)) {
                    // $task_model->status = $time_entry_model->current_status;
                    // $task_model->save();
                    $update_task = Yii::app()->db->createCommand()
                        ->update(
                            'pms_tasks',
                            array('status' => $time_entry_model->current_status),
                            'tskid=:tskid',
                            array(':tskid' => $time_entry_model->taskid)
                        );
                }
            }
            $task_model = Tasks::model()->findByPk($time_entry_model->taskid);
            $balance = $task_model['quantity'];
            $sql = "select sum(qty) as qty "
                . "FROM {$tblpx}daily_work_progress "
                . "WHERE approve_status = 1 "
                . "AND taskid =" . $time_entry_model->taskid;

            $sql_response3 = Yii::app()->db->createCommand($sql)->queryRow();
            if (!empty($sql_response3)) {
                $approved_qty_sum = $sql_response3['qty'];
                if ($task_model['quantity'] > $sql_response3['qty']) {

                    $balance = $task_model['quantity'] - $sql_response3['qty'];
                    $balance = number_format($balance, 2, '.', '');
                }
            }

            if ($time_entry_model->qty <= $balance && $task_model['quantity'] != $approved_qty_sum) {
                $time_entry_model->approve_status = 1;
                if (isset($_REQUEST['reason'])) {
                    $time_entry_model->approve_reject_data = $_REQUEST['reason'];
                }
                $time_entry_model->approved_date = date("Y-m-d H:i:s");
                if ($time_entry_model->save()) {
                    $wpr_image_approve = "UPDATE `pms_wpr_images` SET `image_status`=1 where `wpr_id`= $id";
                    Yii::app()->db->createCommand($wpr_image_approve)->query();
                    $task_model = Tasks::model()->findByPk($time_entry_model->taskid);
                    if ($time_entry_model['daily_work_progress'] >= 100) {
                        $task_model->status = 7;
                        $task_model->save();
                        $this->taskCompletionMail($time_entry_model->taskid);
                    }
                    $schedule_data = $this->CheckprogressPercentags($task_model, $time_entry_model->date, $time_entry_model->daily_work_progress, $time_entry_model->taskid, 2); // 2 => after approve
                    $dependancy_data = $this->DependancyData($task_model, $time_entry_model->date);
                    $return_data = array('status' => 1, 'schedule' => $schedule_data, 'dependant' => $dependancy_data);
                    echo json_encode($return_data);
                } else {
                    $return_data = array('status' => 2);
                    echo json_encode($return_data);
                }
            } else {
                $return_data = array('status' => 2, 'message' => 'Exceeded available quantity');
                echo json_encode($return_data);
            }
        }
    }

    public function actionconsolidated_mail()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT pms_tasks.report_to "
            . "FROM pms_tasks LEFT JOIN pms_users "
            . "ON pms_tasks.report_to = pms_users.userid "
            . "WHERE ('" . date('Y-m-d') . "' between pms_tasks.start_date and pms_tasks.due_date) "
            . "AND pms_tasks.status ='6' AND pms_tasks.trash = '1' "
            . "AND pms_tasks.task_type = '1' "
            . "GROUP BY pms_tasks.report_to";
        $user = Yii::app()->db->createCommand($sql)->queryAll();
        $users = array();
        foreach ($user as $key => $value) {
            array_push($users, $value['report_to']);
        }
        $main_array = array();

        if (!empty($users)) {
            foreach ($user as $key => $value) {
                $user_id = $value['report_to'];

                // pms_daily_work_progress.date ='2019-09-24' AND
                // pms_daily_work_progress.date ='".date('Y-m-d')."'
                $sql = "SELECT pms_tasks.tskid FROM pms_daily_work_progress "
                    . "LEFT JOIN pms_tasks ON pms_daily_work_progress.taskid=pms_tasks.tskid "
                    . "WHERE pms_daily_work_progress.approve_status ='1' "
                    . "AND  pms_tasks.report_to IN ($user_id) "
                    . "AND pms_tasks.status ='6' "
                    . "AND ('" . date('Y-m-d') . "' between pms_tasks.start_date and pms_tasks.due_date) "
                    . "GROUP BY pms_tasks.tskid";
                $tasks = Yii::app()->db->createCommand($sql)->queryAll();
                if (!empty($tasks)) {
                    $main_array[]['user_id'] = $user_id;
                    $behind = array();
                    $ahead = array();
                    $onschedule = array();
                    foreach ($tasks as $key2 => $value) {
                        $main_array[$key]['tasks_id'][] = $value['tskid'];
                        $taskid = $value['tskid'];
                        $model = Tasks::model()->findByPk($taskid);
                        $date = date('Y-m-d');
                        $schedule_data = $this->Checkprogress($model, $date, '10', $taskid);
                        $schedule = json_decode($schedule_data);
                        if ($schedule->status == 'ahead_schedule') {
                            array_push($ahead, $schedule);
                        } else if ($schedule->status == 'behind_schedule') {
                            array_push($behind, $schedule);
                        } else {
                            array_push($onschedule, $schedule);
                        }
                        $main_array[$key]['ahead'] = $ahead;
                        $main_array[$key]['behind'] = $behind;
                        $main_array[$key]['onschedule'] = $onschedule;
                    }
                }
            }
        }
        if (!empty($main_array)) {

            foreach ($main_array as $key => $value) {
                $users = Users::model()->findByPk($value['user_id']);
                $html_data = '';
                if (!empty($value['ahead']) || !empty($value['behind']) || !empty($value['onschedule'])) {
                    $html_data = $this->renderPartial('_ahead', array('ahead' => $value['ahead'], 'behind' => $value['behind'], 'onschedule' => $value['onschedule']), true);
                    $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
                    $model2 = new MailLog;
                    $mail = new JPhpMailer();
                    $today = date('Y-m-d H:i:s');
                    $headers = "Hostech";
                    $mail_data = $html_data;
                    $mail_data .= '
				<p>Sincerely,  <br />
				' . Yii::app()->name . '</p>
				</div>';
                    //$mail_data = "<p>Project : ".$projects->name."</p><p>Task ID : #".$tasks->tskid."</p><p>Task : ".$tasks->title."</p><p>Date : ".$tasks->start_date." to ".$tasks->due_date."</p><p>Current progress : ".number_format($current_given_percenatge,2)."%</p><p>Current status behind by: ".$percent." %</p><p>Number of wokers required : ".$workers_no."</p>";
                    $bodyContent = $mail_data;
                    $mail->IsSMTP();
                    // .':'.$mail_model['smtp_port']
                    $mail->Host = $mail_model['smtp_host'];
                    $mail->Port = $mail_model['smtp_port'];
                    $mail->SMTPSecure = $mail_model['smtp_secure'];
                    $mail->SMTPAuth = $mail_model['smtp_auth'];
                    $mail->Username = $mail_model['smtp_username'];
                    $mail->Password = $mail_model['smtp_password'];
                    $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
                    $mail->Subject = 'Daily Work Report : ' . date('d-M-y') . '';
                    $receippients = $users->email;
                    if (!empty($users->email))
                        $mail->addAddress($users->email); // Add a recipient


                    // $mail->addAddress('surumi.ja@bluehorizoninfotech.com');
                    $mail_send_by = array(
                        'smtp_email_from' => $mail_model['smtp_email_from'],
                        'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']
                    );
                    $dataJSON = json_encode($mail_data);
                    $receippientsJSON = json_encode($receippients);
                    $mail_send_byJSON = json_encode($mail_send_by);
                    $subject = $mail->Subject;
                    $model2->send_to = $users->email;
                    $model2->send_by = $mail_send_byJSON;
                    $model2->send_date = $today;
                    $model2->message = htmlentities($bodyContent);
                    $model2->description = $mail->Subject;
                    $model2->created_date = $today;
                    $model2->created_by = Yii::app()->user->getId();
                    $model2->mail_type = $subject;
                    $mail->isHTML(true);
                    $mail->MsgHTML($bodyContent);
                    $mail->Body = $bodyContent;
                    if (!empty($users->email)) {
                        if ($mail->Send()) {
                            $model2->sendemail_status = 1;
                        } else {
                            $model2->sendemail_status = 0;
                        }
                    } else {
                        $model2->sendemail_status = 0;
                    }

                    $model2->save();
                }
            }
        }
    }

    protected function Checkprogress($task, $date, $percentage, $taskid)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        //$sql_response = Yii::app()->db->createCommand("SELECT COUNT(id) as total_count, SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE id IN ( SELECT MAX(id) FROM pms_daily_work_progress GROUP BY date) AND taskid ='".$taskid."' AND approve_status ='1'")->queryRow();
        $sql_response = Yii::app()->db->createCommand("SELECT id FROM pms_daily_work_progress WHERE taskid ='" . $taskid . "' AND approve_status ='1' GROUP BY date")->queryAll();
        $total_count = count($sql_response);
        $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $taskid . "' AND approve_status ='1'")->queryRow();
        $tasks = Tasks::model()->findByPk($taskid);
        //$daily_target = (1*$tasks->daily_target);
        $daily_target = ($total_count * $tasks->daily_target);
        $per_day_percenatge = ($daily_target / $tasks->quantity) * 100;
        $current_given_percenatge = $sql_response2['work_progress'];

        if (number_format($current_given_percenatge, 2) < number_format($per_day_percenatge, 2)) {
            $value = number_format($per_day_percenatge, 2) - number_format($current_given_percenatge, 2);
            $result = 'Behind Schedule by ' . number_format($value, 2) . '%';
            // Daily througtput
            $tblpx = Yii::app()->db->tablePrefix;
            //$sql_response = Yii::app()->db->createCommand("SELECT COUNT(id) as total_count, SUM(qty) as total_quantity FROM {$tblpx}daily_work_progress WHERE id IN ( SELECT MAX(id) FROM {$tblpx}daily_work_progress GROUP BY date) AND taskid ='".$task->tskid."' AND approve_status ='1'")->queryRow();
            $sql_response = Yii::app()->db->createCommand("SELECT id FROM pms_daily_work_progress WHERE taskid ='" . $task->tskid . "' AND approve_status ='1' GROUP BY date")->queryAll();
            $total_count = count($sql_response);
            $sql_response3 = Yii::app()->db->createCommand("select SUM(qty) as total_quantity FROM {$tblpx}daily_work_progress WHERE taskid='" . $task->tskid . "' AND approve_status ='1'")->queryRow();
            $no_of_days = ($task->task_duration - ($total_count));

            $work_type = WorkType::model()->findByPk($task['work_type_id']);
            $daily_throughput = $work_type->daily_throughput;
            $quantity = $task['quantity'] - $sql_response3['total_quantity'];
            $perday_quantity = ($quantity / $no_of_days);
            $workers_perday = $perday_quantity / $daily_throughput;
            if ($workers_perday < 1) {
                $workers_no = 1;
            } else {
                $workers_no = round($workers_perday);
            }
            $allowed_workers = $tasks->allowed_workers;
            if ($workers_no > $allowed_workers) {
                $type = 1;
                $worker = $workers_no - $allowed_workers;
                $result1 .= "<br/> This task cannot be completed on time because required workers exceed maximum allowed workers by " . $worker . "";
            } else {
                $type = 2;
                $worker = $workers_no;
                $result1 .= "<br/> Number of workers required to complete task on time " . $workers_no . "";
            }
            $result = array('status' => 'behind_schedule', 'tskid' => $taskid, 'worker' => $worker, 'required_percent' => number_format($per_day_percenatge, 2), 'behind_percent' => $value, 'type' => $type);
        } else {
            $value = number_format($current_given_percenatge, 2) - number_format($per_day_percenatge, 2);
            if ($value > 0) {
                $result = array('status' => 'ahead_schedule', 'tskid' => $taskid, 'ahead_percent' => $value, 'required_percent' => number_format($per_day_percenatge, 2));
            } else {
                $result = array('status' => 'on_schedule', 'tskid' => $taskid);
            }
        }
        return json_encode($result);
    }

    public function getAssigneedeatils($id)
    {
        $user_model = Users::model()->findByPk($id);
        return $user_model['first_name'] . ' ' . $user_model['last_name'];
    }

    public function actionRejectentry()
    {
        if (isset($_REQUEST['entry_id'])) {
            $id = $_REQUEST['entry_id'];
            $time_entry_model = $this->loadModel($id);
            $time_entry_model->approve_status = 2;
            if (isset($_REQUEST['reason'])) {
                $time_entry_model->approve_reject_data = $_REQUEST['reason'];
            }

            $time_entry_model->approved_date = date("Y-m-d H:i:s");
            if ($time_entry_model->save()) {
                $messge = 'Successfully Rejected';
                $return_data = array('status' => 1, 'message' => $messge);
                echo json_encode($return_data);
            } else {
                $return_data = array('status' => 2, 'message' => 'An error occured');
                echo json_encode($return_data);
            }
        }
    }

    public function actionRecall()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['entry_id'])) {
            $id = $_REQUEST['entry_id'];
            $time_entry_model = $this->loadModel($id);
            if ($time_entry_model->approve_status == 2) {
                $time_entry_model->approve_status = 0;
                $time_entry_model->approved_date = null;
            }
            if ($time_entry_model->save()) {
                $messge = 'Success';
                $return_data = array('status' => 1, 'message' => $messge);
                echo json_encode($return_data);
            } else {
                $return_data = array('status' => 2, 'message' => 'An error occured');
                echo json_encode($return_data);
            }
        }
    }

    public function actionStatuschange()
    {

        if (isset($_GET['layout']))
            $this->layout = false;
        $model = new DailyWorkProgress;

        $task = Tasks::model()->findByPk($_REQUEST['task_id']);
        if (isset($_REQUEST['status_id']) && $_REQUEST['status_id'] != "") {
            $status_id = $_REQUEST['status_id'];
        } else {
            $status_id = $task['status'];
        }
        $model->taskid = $task['tskid'];
        $project_id = $task['project_id'];
        $task->status = $status_id;
        $task->updated_date = date('Y-m-d');
        $this->performAjaxValidation($model);
        if ($status_id == 72 || $status_id == 5) {
            if (isset($_POST['DailyWorkProgress'])) {
                $model->attributes = $_POST['DailyWorkProgress'];
                $model->project_id = $project_id;
                $model->date = date('Y-m-d');
                $model->taskid = $task['tskid'];
                $model->work_type = $task['work_type_id'];
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d H:i:s");

                if ($model->save() && $task->save()) {
                    $status = Status::model()->find(array('condition' => 'status_type="task_status" AND sid =' . $status_id . ''));
                    $comments = new ProjectComments;
                    $comments->section_id = $task['tskid'];
                    $comments->comment = 'Task status change to ' . $status->caption;
                    $comments->created_date = date('Y-m-d H:i:s');
                    $comments->user_id = yii::app()->user->id;
                    $comments->type = 2;
                    $comments->save();
                    $this->redirect(array('projects/chart', 'id' => $project_id));
                }
            } else {
                $this->render('status_entry', array(
                    'model' => $model,
                    'quantity' => $task['quantity'],
                    'task_id' => $model->taskid,
                    'status_id' => $status_id
                    // 'project_id'=>$_REQUEST['project_id'],                 
                )
                );
            }
        } elseif (isset($_POST['DailyWorkProgress'])) {
            $model->attributes = $_POST['DailyWorkProgress'];
            $model->project_id = $project_id;
            $model->date = date('Y-m-d');
            $model->taskid = $task['tskid'];
            $model->work_type = $task['work_type_id'];
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d H:i:s");
            $model->current_status = $status_id;
            if ($model->save() && $task->save()) {
                $status = Status::model()->find(array('condition' => 'status_type="task_status" AND sid =' . $status_id . ''));
                $comments = new ProjectComments;
                $comments->section_id = $task['tskid'];
                $comments->comment = 'Task status change to ' . $status->caption;
                $comments->created_date = date('Y-m-d H:i:s');
                $comments->user_id = yii::app()->user->id;
                $comments->type = 2;
                $comments->save();
                $this->redirect(array('projects/chart', 'id' => $project_id));
            }
        } elseif (!empty($_REQUEST['status_id'])) {

            if ($task->save()) {
                $status = Status::model()->find(array('condition' => 'status_type="task_status" AND sid =' . $status_id . ''));
                $comments = new ProjectComments;
                $comments->section_id = $task['tskid'];
                $comments->comment = 'Task status change to ' . $status->caption;
                $comments->created_date = date('Y-m-d H:i:s');
                $comments->user_id = yii::app()->user->id;
                $comments->type = 2;
                $comments->save();
                $this->render('status_entry', array(
                    'model' => $model,
                    'quantity' => $task['quantity'],
                    'task_id' => $model->taskid,
                    'status_id' => $status_id
                    // 'project_id'=>$_REQUEST['project_id'],                 
                )
                );
            }
        } else {
            $this->render('status_entry', array(
                'model' => $model,
                'quantity' => $task['quantity'],
                'task_id' => $model->taskid,
                'status_id' => $status_id
                // 'project_id'=>$_REQUEST['project_id'],                 
            )
            );
        }
    }

    public function actionApproverejectdata()
    {
        $id = $_POST['entry_id'];
        $reason = $_POST['reason'];
        $result = 2;
        if (!empty($id) && !empty($reason)) {

            $model = $this->loadModel($id);
            $model->approve_reject_data = $reason;
            $model->approve_status = 2;
            if ($model->save()) {
                $this->rejectMail($model);
                $result = 1;
            } else {
                $result = 2;
            }
        }
        echo $result;
    }

    public function actionPendingRequests()
    {
        $interval = (isset($_GET['interval']) ? $_GET['interval'] : 1);
        $model = new DailyWorkProgress('pendingRequests');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['DailyWorkProgress'])) {
            $model->attributes = $_GET['DailyWorkProgress'];
        }


        $this->render('pending_requests', array(
            'model' => $model,
            'interval' => $interval,
        )
        );
    }

    public function actiongetSite()
    {
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $sites_sql = "select site_name,id,latitude,longitude from pms_clientsite"
            . "left join pms_clientsite_assigned "
            . "ON pms_clientsite.id = pms_clientsite_assigned.site_id "
            . "where user_id=" . Yii::app()->user->getId();
        $sites = Yii::app()->db->createCommand($sites_sql)->queryAll();
        $return_data = array();
        foreach ($sites as $site) {
            $site_latitude = $site['latitude'];
            $site_logitude = $site['longitude'];
            if ($site_logitude != "" && $site_latitude != "") {
                $distance = $this->distance($latitude, $longitude, $site_latitude, $site_logitude, "K");

                if ($distance < 1) {
                    $site_name = $site['site_name'];
                    $site_id = $site['id'];
                    $return_data = array("site_name" => $site_name, 'site_id' => $site_id);
                    break;
                }
            }
        }

        if (count($return_data) == 0) {
            $return_data = array("site_name" => "No Site");
        }
        echo json_encode($return_data);
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    public function rejectMail($activity_model)
    {
        $task_model = Tasks::model()->findByPk($activity_model['taskid']);

        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $model2 = new MailLog;
        try {
            $mail = new JPhpMailer(true);
            $today = date('Y-m-d H:i:s');
            $headers = "Hostech";
            $mail_data = '
	<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
	<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $task_model->assignedTo->first_name . '</span></h3>
	<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
	<tr><td colspan="2" style="background-color: #000;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $this->logo . '" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
	</tr>
	<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>Daily Work Progress Alert</h2></div></td>
	</tr>
	<tr><td colspan="2">This is a notification to let you know that Daily Work Progress Entry is rejected: </td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project: </td><td style="border:1px solid #f5f5f5;">' . $task_model->project->name . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $task_model->title . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Date: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($activity_model['date'])) . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Quantity:</td><td style="border:1px solid #f5f5f5;">' . $activity_model['qty'] . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Description:</td><td style="border:1px solid #f5f5f5;"> ' . $activity_model['description'] . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">REASON FOR REJECTION :</td><td style="border:1px solid #f5f5f5;"> ' . $activity_model['approve_reject_data'] . '</td></tr> ';

            $mail_data .= '</table>
	<p>Sincerely,  <br />
	' . Yii::app()->name . '</p>
	</div>';

            //$mail_data = "<p>Project : ".$projects->name."</p><p>Task ID : #".$tasks->tskid."</p><p>Task : ".$tasks->title."</p><p>Date : ".$tasks->start_date." to ".$tasks->due_date."</p><p>Current progress : ".number_format($current_given_percenatge,2)."%</p><p>Current status behind by: ".$percent." %</p><p>Number of wokers required : ".$workers_no."</p>";
            $bodyContent = $mail_data;
            $mail->IsSMTP();
            $mail->Host = $mail_model['smtp_host'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
            $mail->Subject = 'Daily Work Progress Reject';
            $receippients = $task_model->assignedTo->email;
            $mail->addAddress($task_model->assignedTo->email); // Add a recipient
            $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
            $dataJSON = json_encode($mail_data);
            $receippientsJSON = json_encode($receippients);
            $mail_send_byJSON = json_encode($mail_send_by);
            $subject = $mail->Subject;
            $model2->send_to = $task_model->assignedTo->email;
            $model2->send_by = $mail_send_byJSON;
            $model2->send_date = $today;
            $model2->message = htmlentities($bodyContent);
            $model2->description = $mail->Subject;
            $model2->created_date = $today;
            $model2->created_by = Yii::app()->user->getId();
            $model2->mail_type = $subject;
            $mail->isHTML(true);
            $mail->MsgHTML($bodyContent);
            $mail->Body = $bodyContent;
            if (!empty($task_model->assignedTo->email)) {
                if ($mail->Send()) {
                    $model2->sendemail_status = 1;
                } else {
                    $model2->sendemail_status = 0;
                }
            } else {
                $model2->sendemail_status = 1;
            }

            $model2->save();
        } catch (Exception $e) {
            $exceptionError = trim(strip_tags($e->errorMessage()));
            if ($exceptionError == "SMTP Error: Could not connect to SMTP host." or (defined('SEND_EMAIL') and SEND_EMAIL == false)) {

                return true;
            } else {
                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            }
        }
    }
    public function SendEditMail($model)
    {

        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $task_sql = "select * from pms_tasks left join pms_projects 
		on pms_projects.pid=pms_tasks.project_id
		where tskid=" . $model->taskid;
        $task = Yii::app()->db->createCommand($task_sql)->queryRow();
        $coordinater = $task['coordinator'];
        $report_to = $task['report_to'];

        $updated_by = Users::model()->findByPk($model->created_by);

        $user = Users::model()->findByPk($model->created_by);

        $report_to = Users::model()->findByPk($report_to);
        $task_mod = Tasks::model()->findByPk($model->taskid);
        if ($report_to == "") {
            $report_to = Users::model()->findByPk($coordinater);
        }
        $model2 = new MailLog;
        $mail = new JPhpMailer();
        $today = date('Y-m-d H:i:s');
        $headers = "Hostech";
        $mail_data = '
	<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
	<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $user->first_name . '</span></h3>
	<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
	<tr><td colspan="2" style="background-color: #000;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $this->logo . '" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
	</tr>
	<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>BOQ Entry Edited</h2></div></td>
	</tr>
	<tr><td colspan="2">This is a notification to let you know that a BOQ entry is edited: </td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project: </td><td style="border:1px solid #f5f5f5;">' . $task['name'] . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task: </td><td style="border:1px solid #f5f5f5;">' . $task['title'] . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Edited By: </td><td style="border:1px solid #f5f5f5;">' . $updated_by->first_name . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Date: </td><td style="border:1px solid #f5f5f5;">' . Date("d-m-Y H:i:s a") . '</td></tr>';


        $mail_data .= '</table>
	<p>Sincerely,  <br />
	' . Yii::app()->name . '</p>
	</div>';

        //$mail_data = "<p>Project : ".$projects->name."</p><p>Task ID : #".$tasks->tskid."</p><p>Task : ".$tasks->title."</p><p>Date : ".$tasks->start_date." to ".$tasks->due_date."</p><p>Current progress : ".number_format($current_given_percenatge,2)."%</p><p>Current status behind by: ".$percent." %</p><p>Number of wokers required : ".$workers_no."</p>";
        $bodyContent = $mail_data;
        $mail->IsSMTP();
        $mail->Host = $mail_model['smtp_host'];
        $mail->Port = $mail_model['smtp_port'];
        $mail->SMTPSecure = $mail_model['smtp_secure'];
        $mail->SMTPAuth = $mail_model['smtp_auth'];
        $mail->Username = $mail_model['smtp_username'];
        $mail->Password = $mail_model['smtp_password'];
        $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
        $mail->Subject = 'Edit BOQ Entry';
        $receippients = $task_mod->assignedTo->email;
        $mail->addAddress($report_to->email); // Add a recipient
        $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
        $dataJSON = json_encode($mail_data);
        $receippientsJSON = json_encode($receippients);
        $mail_send_byJSON = json_encode($mail_send_by);
        $subject = $mail->Subject;
        $model2->send_to = $task_mod->assignedTo->email;
        $model2->send_by = $mail_send_byJSON;
        $model2->send_date = $today;
        $model2->message = htmlentities($bodyContent);
        $model2->description = $mail->Subject;
        $model2->created_date = $today;
        $model2->created_by = Yii::app()->user->getId();
        $model2->mail_type = $subject;
        $mail->isHTML(true);
        $mail->MsgHTML($bodyContent);
        $mail->Body = $bodyContent;
        if ($mail->Send() && !empty($task_mod->assignedTo->email)) {
            $model2->sendemail_status = 1;
        } else {
            $model2->sendemail_status = 0;
        }
        $model2->save();
    }

    public function actionbulkReject()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $boq_entry_ids = $_POST['ids'];
            $status_val = '';
            $status = $_POST['req'];
            $reason = $_POST['reason'];
            if ($status == 'Reject') {
                $status_val = 2;
            } else {
                $status_val = 1;
            }
            if (!empty($boq_entry_ids) && !empty($status_val)) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $count = 0;
                    foreach ($boq_entry_ids as $boq_id) {
                        $model = $this->loadModel($boq_id);
                        if (!empty($model)) {
                            if ($model->tasks->status != 7) {
                                $this->performAjaxValidation($model);
                                $model->approve_reject_data = $reason;
                                $model->approve_status = $status_val;
                                $model->approved_date = date("Y-m-d H:i:s");
                                if ($model->save()) {
                                    $count++;
                                    if ($status_val == 2) {
                                        $this->rejectMail($model);
                                    }

                                }
                            }
                        }
                    }
                    $transaction->commit();
                    $messge = $count . ' Out of ' . count($boq_entry_ids) . $status . ',  Successfully ';
                    $return_data = array('status' => 1, 'message' => $messge);
                    echo json_encode($return_data);
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return_data = array('status' => 2, 'message' => 'An error occured');
                    echo json_encode($return_data);
                }
            } else {
                $return_data = array('status' => 2, 'message' => 'An error occured');
                echo json_encode($return_data);
            }
        }
    }
    public function actionGallery()
    {
        $model = new DailyWorkProgress('gallery');
        $model->unsetAttributes();  // clear any default values 

        if (isset($_GET['DailyWorkProgress'])) {
            $model->attributes = $_GET['DailyWorkProgress'];
        }

        $this->render('gallery', array(
            'model' => $model,

        )
        );
    }
    public function actiontaskList()
    {
        $project_id = $_POST['project_id'];
        $task_criteria = new CDbCriteria;
        $task_criteria->condition = "task_type = 1 "
            . "AND project_id = " . $_POST['project_id'];

        $tasks = Tasks::model()->findAll($task_criteria);

        $milestone_criteria = new CDbCriteria;
        $milestone_criteria->condition = "project_id = " . $_POST['project_id'];

        $milestone = Milestone::model()->findAll($milestone_criteria);

        $work_type_criteria = new CDbCriteria;
        $work_type_criteria->join = 'LEFT JOIN pms_tasks ON t.wtid = pms_tasks.work_type_id';
        $work_type_criteria->condition = "project_id = " . $_POST['project_id'];
        $work_type_criteria->group = 't.wtid';


        $work_type = WorkType::model()->findAll($work_type_criteria);

        $area_criteria = new CDbCriteria;

        $area_criteria->condition = "t.project_id = " . $_POST['project_id'];
        $area_criteria->group = 't.id';

        $area = Area::model()->findAll($area_criteria);

        $task_html = '';
        foreach ($tasks as $key => $value) {
            $task_html .= '<option value=' . $value['tskid'] . ' >'
                . $value['title'] . '</option>';
        }

        $milestone_html = '';
        foreach ($milestone as $key => $value) {
            $milestone_html .= '<option value=' . $value['id'] . ' >'
                . $value['milestone_title'] . '</option>';
        }

        $work_type_html = '';
        foreach ($work_type as $work_type) {
            $work_type_html .= '<option value=' . $work_type['wtid'] . ' >'
                . $work_type['work_type'] . '</option>';
        }

        $area_html = '';
        foreach ($area as $area) {
            $area_html .= '<option value=' . $area['id'] . ' >'
                . $area['area_title'] . '</option>';
        }
        $response_data = array(
            'html' => $task_html,
            'milestone_html' => $milestone_html,
            'work_type_html' => $work_type_html,
            'area_html' => $area_html

        );
        echo json_encode($response_data);
    }
    public function taskCompletionMail($id)
    {
        $tasks = Tasks::model()->findByPk($id);
        $projects = Projects::model()->findByPk($tasks->project_id);
        $tblpx = Yii::app()->db->tablePrefix;
        $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='" . $id . "' AND approve_status = 1")->queryRow();
        if ($tasks->status == 7 && $sql_response2['work_progress'] == '100') {
            $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
            $model2 = new MailLog;
            $mail = new JPhpMailer();
            $today = date('Y-m-d H:i:s');
            $headers = "Ashly";
            //$mail_data = "<p>Project : ".$projects->name."<p><p>Task : ".$tasks->title."<p><p>Date : ".$tasks->start_date." to ".$tasks->due_date."<p><p>Closeing date of task : ".date('Y-m-d')."</p>";
            $mail_data = '
            <div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
            <h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $tasks->assignedTo->first_name . '</span></h3>
            <table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
            <tr><td colspan="2" style="background-color: #000;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $this->logo . '" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
            </tr>
            <tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>Task # ' . $id . ' : Completion notification</h2></div></td>
            </tr>
            <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Project: </td><td style="border:1px solid #f5f5f5;">' . $projects->name . '</td></tr>
            <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Task Name: </td><td style="border:1px solid #f5f5f5;">' . $tasks->title . '</td></tr>
            <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Quantity: </td><td style="border:1px solid #f5f5f5;">' . $tasks->quantity . '</td></tr>
            <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Start & Due dates: </td><td style="border:1px solid #f5f5f5;">' . date('d-M-y', strtotime($tasks->start_date)) . ' & ' . date('d-M-y', strtotime($tasks->due_date)) . '</td></tr>
            <tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Closing date of task:</td><td style="border:1px solid #f5f5f5;">' . date('d-M-y') . '</td></tr>
            </table>
            <p>Sincerely,  <br />
            ' . Yii::app()->name . '</p>
            </div>';

            $bodyContent = $mail_data;
            $mail->IsSMTP();
            //.':'.$mail_model['smtp_port']
            $mail->Host = $mail_model['smtp_host'];
            $mail->Port = $mail_model['smtp_port'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->Subject = 'Task # ' . $id . ' : Completion notification';
            $receippients = explode(',', $tasks->email);
            foreach ($receippients as $receippient) {
                $mail->addAddress($receippient); // Add a recipient
            }
            $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
            $dataJSON = json_encode($mail_data);
            $receippientsJSON = json_encode($receippients);
            $mail_send_byJSON = json_encode($mail_send_by);
            $subject = $mail->Subject;
            $model2->send_to = $receippientsJSON;
            $model2->send_by = $mail_send_byJSON;
            $model2->send_date = $today;
            $model2->message = htmlentities($bodyContent);
            $model2->description = $mail->Subject;
            $model2->created_date = $today;
            $model2->created_by = Yii::app()->user->getId();
            $model2->mail_type = $subject;
            $mail->isHTML(true);
            $mail->MsgHTML($bodyContent);
            $mail->Body = $bodyContent;
            if ($mail->Send()) {

                $model2->sendemail_status = 1;
            } else {
                $model2->sendemail_status = 0;
            }
            $model2->save();
        }
    }
    public function actiongetwprtaskdetails()
    {
        $task_id = $_POST['task_id'];
        $model = new DailyWorkProgress;

        $task = Tasks::model()->findByPk($task_id);

        $project = Projects::model()->findByPk($task->project_id);

        $work_type = WorkType::model()->findByPk($task->work_type_id);

        $unit = Unit::model()->findByPk($task->unit);

        $resource_utilised = [];
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
            $resource_utilised = Resources::model()->findAll(
                array("order" => "resource_name ASC")
            );
        }

        $result = $this->renderPartial('create_wpr', array(
            'model' => $model,
            'task' => $task,
            'project' => $project,
            'work_type' => $work_type,
            'unit' => $unit,
            'resource_utilised' => $resource_utilised
        ), true);
        echo json_encode($result);
    }
    public function actioncheckdateavailable()
    {
        $date = date('Y-m-d', strtotime($_POST['date']));
        $task_id = $_POST['wpr_task_id'];

        $criteria = new CDbCriteria;
        $criteria->condition = "task_type = 1 AND status IN (6,9) "
            . "AND (assigned_to = " . Yii::app()->user->id . ") "
            . "AND tskid = " . $task_id
            . " AND start_date <= '$date' "
            . "AND due_date >= '$date' order by title";

        $data = Tasks::model()->findAll($criteria);

        echo json_encode(array('count' => count($data)));
    }
    public function actioncheckItemAvailability()
    {
        $item_id = $_POST['itm_id'];
        $project_id = $_POST['project_id'];



        $project = Projects::model()->findByPk($project_id);

        $template_id = $project->template_id;


        if ($template_id != "") {
            $criteria = new CDbCriteria();
            $criteria->select = 'id';
            $criteria->condition = 'find_in_set(' . $item_id . ',purchase_item) and temp_id=' . $template_id;
            $data = TemplateItems::model()->find($criteria);

            if ($data) {
                $result = array('status' => 1, 'id' => $data->id);
            } else {
                $result = array('status' => 0);
            }
        } else {
            $result = array('status' => 2);
        }


        echo json_encode($result);
    }
    public function actioncheckItemCountAvailability()
    {
        $consumed_item_id = $_POST['itm_id'];
        $task_id = $_POST['task_id'];
        $item_count = $_POST['item_count'];
        $item_rate_id = $_POST['item_rate'];
        $item_already_exist = '';
        if (isset($_POST['already_exist_count'])) {
            $item_already_exist = $_POST['already_exist_count'];
        }

        $task = Tasks::model()->findByPK($task_id);

        $pr_det = Projects::model()->findByPK($task->project_id);

        $clientsite_id = $task->clientsite_id;

        $client_site = Clientsite::model()->findByPk($clientsite_id);

        $warehouse_id = isset($client_site->account_warehouse_id) ? $client_site->account_warehouse_id : "";

        if ($warehouse_id != "") {

            $criteria = new CDbCriteria();
            $criteria->select = 'id';
            $criteria->condition = 'find_in_set(' . $consumed_item_id . ',purchase_item) and temp_id=' . $pr_det->template_id;
            $data = TemplateItems::model()->find($criteria);

            if ($data) {

                if ($pr_det->template_id != "") {
                    $item_id = $data->id;
                    $item_criteria = new CDbCriteria();
                    $item_criteria->select = 'estimation_id,item_quantity_required,item_quantity_used';
                    $item_criteria->condition = 'task_id=' . $task_id . ' AND item_id=' . $item_id . ' AND template_id=' . $pr_det->template_id;

                    $item = TaskItemEstimation::model()->find($item_criteria);

                    if ($item) {



                        $warehouse_item_availability = Yii::app()->db->createCommand()
                            ->select('warehousestock_stock_quantity,rate')
                            ->from('jp_warehousestock')
                            ->where('warehousestock_id=' . $item_rate_id)
                            ->queryRow();



                        $wpr_item_consumed = Yii::app()->db->createCommand()
                            ->select('SUM(item_qty) as qty')
                            ->from('pms_acc_wpr_item_consumed')
                            ->where('warehouse_id=' . $warehouse_id . ' and item_id=' . $consumed_item_id . ' and item_rate LIKE ' . $warehouse_item_availability['rate'])
                            ->queryScalar();


                        $available_qty = $warehouse_item_availability['warehousestock_stock_quantity'] - $wpr_item_consumed;


                        if ($item_count > $available_qty) {
                            $result_array = array('status' => 2, 'message' => 'Item count is greater than available', 'item_required' => $available_qty);
                        } else {
                            $item_required = $item->item_quantity_required;

                            if ($item->item_quantity_used != "") {
                                $item_required = $item->item_quantity_required - $item->item_quantity_used;
                            }
                            if ($item_count > $item_required) {

                                $result_array = array('status' => 1, 'message' => 'Item count is greater than requred count', 'item_required' => $item_required);
                            } else {
                                $result_array = array('status' => 0, 'message' => 'ok');
                            }
                        }
                    } else {

                        $result_array = array('status' => 2, 'message' => 'item estimation not added');
                    }
                } else {
                    $result_array = array('status' => 3, 'message' => 'This task is not connected to any template');
                }
            } else {

                $result_array = array('status' => 3, 'message' => 'This item is not mentioned in template');
            }
        } else {
            $result_array = array('status' => 4, 'message' => 'Warehouse site linking is not done');
        }
        echo json_encode($result_array);
    }

    public function actionApprove_consumption_item()
    {
        $id = $_POST['entry_id'];

        $model = DailyWorkProgress::model()->findByPk($id);
        $model->consumed_item_status = 1;
        if ($model->save()) {
            $return_data = array('status' => 1, 'message' => 'Approved successfully');
        } else {
            $return_data = array('status' => 0, 'message' => 'Something went wrong');
        }
        echo json_encode($return_data);
    }
    public function actiongetWarehouseItems()
    {

        $html['html'] = '';
        if (Tasks::model()->accountPermission() == 1 && (in_array('/dailyWorkProgress/itemConsumption', Yii::app()->session['menuauthlist']))) {
            $project_id = $_POST['project_id'];
            $record = Clientsite::model()->findByAttributes(array('pid' => $project_id));
            $warehouse_id = $record->account_warehouse_id;
            $data = array();
            $final = array();
            if ($warehouse_id != "") {
                $spec_sql = "SELECT id, specification, brand_id, cat_id "
                    . " FROM jp_specification INNER JOIN jp_warehousestock ON jp_warehousestock.warehousestock_itemid=jp_specification.id where warehousestock_warehouseid=" . $warehouse_id . " GROUP BY id";
                $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();
                foreach ($specification as $key => $value) {
                    $brand = '';
                    if ($value['brand_id'] != NULL) {
                        $brand_sql = "SELECT brand_name "
                            . " FROM jp_brand "
                            . " WHERE id=" . $value['brand_id'] . "";
                        $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                        $brand = '-' . ucwords($brand_details['brand_name']);
                    }

                    if ($value['cat_id'] != "") {
                        $result = $this->GetParent($value['cat_id']);
                    }
                    $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
                    $final[$key]['id'] = $value['id'];
                }
            }


            if (!empty($final)) {

                $html['html'] .= '<option value="" >Select Item</option>';
                foreach ($final as $key => $value) {
                    $html['html'] .= '<option value="' . $value['id'] . '" >' . $value['data'] . '</option>';
                }
            }
        }
        echo (json_encode($html));
    }
    public function actiongetItemRate()
    {
        $item_id = $_POST['consumed_item_id'];
        $project_id = $_POST['project_id'];
        $record = Clientsite::model()->findByAttributes(array('pid' => $project_id));
        $warehouse_id = $record->account_warehouse_id;
        $html['html'] = '';
        $query = "SELECT warehousestock_id,rate "
            . " FROM jp_warehousestock where warehousestock_itemid=" . $item_id . " and warehousestock_warehouseid=" . $warehouse_id;
        $data = Yii::app()->db->createCommand($query)->queryAll();

        $html['html'] .= '<option value="" >Select Rate</option>';
        if ($warehouse_id) {
            foreach ($data as $value) {
                $html['html'] .= '<option value="' . $value['warehousestock_id'] . '" >' . $value['rate'] . '</option>';
            }
        }

        echo (json_encode($html));
    }
    public function wprItemConsumed($task_id, $item_id, $item_count, $rate_id, $temp_id)
    {
        $sql = "SELECT SUM(consumed_item_count) as sum FROM pms_daily_work_progress "
            . "WHERE taskid ='" . $task_id . "' "
            . "AND consumed_item_id = '" . $item_id . "' ";
        $tot_qty = Yii::app()->db->createCommand($sql)->queryRow();

        $item_used_count = $tot_qty['sum'] + $item_count;
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->condition = 'find_in_set(' . $item_id . ',purchase_item) and temp_id=' . $$temp_id;
        $data = TemplateItems::model()->find($criteria);

        $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $task_id, 'item_id' => $data->id, 'template_id' => $temp_id));

        $update_item->attributes = array('item_quantity_used' => $item_used_count);

        $update_item->save();

        return $data->id;
    }
    public function UpdateItemConsumed($task_id, $item_id, $item_count, $rate_id, $wpr_id)
    {
        $task = Tasks::model()->findByPK($task_id);

        $clientsite_id = $task->clientsite_id;

        $client_site = Clientsite::model()->findByPk($clientsite_id);

        $warehouse_id = $client_site->account_warehouse_id;
        $added_by = yii::app()->user->id;

        $warehouse_item_rate = Yii::app()->db->createCommand()
            ->select('rate')
            ->from('jp_warehousestock')
            ->where('warehousestock_id=:id', array(':id' => $rate_id))
            ->queryScalar();

        $sql = "INSERT INTO 
        `pms_acc_wpr_item_consumed`
        (`id`, `warehouse_id`, `item_id`, `item_rate`,
         `item_qty`, `item_added_by`,`wpr_id`)
          VALUES (null,'$warehouse_id','$item_id','$warehouse_item_rate','$item_count','$added_by','$wpr_id')";

        Yii::app()->db->createCommand($sql)->query();
    }
    public function actionNew()
    {
        /* $dataProvider=new CActiveDataProvider('DailyWorkProgress');
          $this->render('index',array(
          'dataProvider'=>$dataProvider,
          )); */
        $from_date = '';
        $to_date = '';
        unset(Yii::app()->request->cookies['from_date']);  // first unset cookie for dates
        unset(Yii::app()->request->cookies['to_date']);
        $interval = (isset($_GET['interval']) ? $_GET['interval'] : 0);
        $selected_status = (isset($_GET['type']) ? $_GET['type'] : 'All');
        $model = new DailyWorkProgress('search');
        $model->unsetAttributes();  // clear any default values
        if (!empty($_REQUEST)) {
            if (isset($_POST['from_date'])) {
                Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date', $_POST['from_date']); // define cookie for from_date
                $model->from_date = $_REQUEST['from_date'];
            }

            if (isset($_POST['to_date'])) {
                Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date', $_POST['to_date']);
                $model->to_date = $_REQUEST['to_date'];
            }
        }
        if (isset($_REQUEST['DailyWorkProgress'])) {
            $model->attributes = $_REQUEST['DailyWorkProgress'];
        }
        $account_items = [];
        $resource_utilised = [];
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
            $resource_utilised = Resources::model()->findAll(
                array("order" => "resource_name ASC")
            );
        }

        $this->render('newindex', array(
            'model' => $model,
            'interval' => $interval,
            'selected_status' => $selected_status,
            'account_items' => $account_items,
            'resource_utilised' => $resource_utilised
        )
        );
    }
    public function actioncreate_daily_wpr()
    {

        $model = new DailyWorkProgress;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {


            $taskid = $_POST['DailyWorkProgress']['taskid'];
            $created_date = date("Y-m-d H:i:s");
            $itemdetails = $_POST['newdetails'];


            $projectid = $_POST['DailyWorkProgress']['project_id'];

            $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
            $item_id = $qry['tskid'];
            $tot_qty = $qry['quantity'];

            $data = Yii::app()->db->createCommand(
                "select sum(qty) as qty from pms_daily_work_progress 
			where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' GROUP BY project_id, taskid"
            )->queryRow();
            $existqty = $data['qty'];
            $newqty = $_POST['DailyWorkProgress']['qty'];
            $qty = $newqty + $existqty;
            $percentage = ($newqty / $tot_qty) * 100;
            $max = $tot_qty - $existqty;


            $model->attributes = $_POST['DailyWorkProgress'];
            $model->taskid = $_POST['DailyWorkProgress']['taskid'];
            //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
            $model->work_type = $_POST['DailyWorkProgress']['work_type'];
            $model->daily_work_progress = round($percentage, 2);
            $model->current_status = $_POST['DailyWorkProgress']['current_status'];
            $model->date = date('Y-m-d', strtotime($_POST['DailyWorkProgress']['date']));
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d H:i:s");
            $model->latitude = $_POST['DailyWorkProgress']['latitude'];
            $model->longitude = $_POST['DailyWorkProgress']['longitude'];
            $model->site_id = $_POST['DailyWorkProgress']['site_id'];
            $model->site_name = $_POST['DailyWorkProgress']['site_name'];
            $created_date_model = $model->created_date;
            if (isset($_POST['DailyWorkProgress']['consumed_item_id'])) {

                $model->consumed_item_status = 1;

                if ($_POST['DailyWorkProgress']['item_count_status'] != "") {

                    $model->consumed_item_status = 0;
                }
            }

            if (isset($_POST['DailyWorkProgress']['incident'])) {
                $model->incident = $_POST['DailyWorkProgress']['incident'];
            }
            if (isset($_POST['DailyWorkProgress']['inspection'])) {
                $model->inspection = $_POST['DailyWorkProgress']['inspection'];
            }
            if (isset($_POST['DailyWorkProgress']['visitor'])) {
                $model->visitor_name = $_POST['DailyWorkProgress']['visitor'];
            }
            if (isset($_POST['DailyWorkProgress']['company'])) {
                $model->company_name = $_POST['DailyWorkProgress']['company'];
            }
            if (isset($_POST['DailyWorkProgress']['designation'])) {
                $model->designation = $_POST['DailyWorkProgress']['designation'];
            }
            if (isset($_POST['DailyWorkProgress']['purpose'])) {
                $model->purpose = $_POST['DailyWorkProgress']['purpose'];
            }
            $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
            if ($qry2['assigned_to']) {
                $assigned_to = $qry2['assigned_to'];
            } else {
                $assigned_to = Yii::app()->user->id;
            }

            if ($qty > $tot_qty) {
                Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                $this->render('index', array(
                    'model' => $model,
                    'itemdetails' => $itemdetails,
                    'interval' => 0,
                )
                );
            } else {
                if ($model->save() && $model->validate()) {
                    try {
                        if (!empty($_POST['DailyWorkProgress']['consumed_item_id'])) {
                            $task_details = Tasks::model()->findByPk($_POST['DailyWorkProgress']['taskid']);
                            $project = Projects::model()->findByPK($task_details->project_id);
                            $temp_id = $project->template_id;
                            $this->consumedWprItems($model->id, $model->taskid, $temp_id, $_POST['DailyWorkProgress']['consumed_item_id'], $_POST['DailyWorkProgress']['consumed_item_count'], $_POST['DailyWorkProgress']['consumed_item_rate']);
                        }


                        if (!empty($_POST['DailyWorkProgress']['resources_used'] && $_POST['DailyWorkProgress']['utilised_qty'])) {
                            $this->consumedResources($model->id, $model->taskid, $_POST['DailyWorkProgress']['resources_used'], $_POST['DailyWorkProgress']['utilised_qty'], $_POST['DailyWorkProgress']['unit_id']);
                        }


                        $images = CUploadedFile::getInstancesByName('image');
                        $images_array = array();
                        $i = 0;
                        $label = [];
                        if (isset($_POST['DailyWorkProgress']['image_label'])) {
                            $label = $_POST['DailyWorkProgress']['image_label'];
                        }

                        if (count($images) > 0) {
                            foreach ($images as $image => $pic) {

                                $label_name = $label[$image];


                                $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                                $newfilename = rand(1000, 9999) . time();
                                $newfilename = md5($newfilename); //optional
                                $file_name = $newfilename . '.' . $pic->getExtensionName();

                                $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                                if (!file_exists($file))

                                    $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                                array_push($images_array, $file_name);


                                $create_sql = "INSERT INTO pms_wpr_images (wpr_id,image_path,image_label,image_status,created_by) VALUES (:id,:image_path,:image_label,:image_status,:created_by)";

                                $parameters = array(
                                    ":id" => $model->id,
                                    ":image_path" => $file_name,
                                    ":image_label" => $label_name,
                                    ":image_status" => 0,
                                    ":created_by" => Yii::app()->user->id
                                );

                                Yii::app()->db->createCommand($create_sql)->execute($parameters);

                                $i++;
                            }
                        }



                        $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                        $dependancy_data = $this->DependancyData($qry2, $model->date);
                        $final_result = $schedule_data . '<br>' . $dependancy_data;
                        // Yii::app()->user->setFlash('error', $final_result);
                        // Yii::app()->user->setFlash('success', "Successfully created..");
                    } catch (Exception $e) {

                        Log::trace("Error : " . $e);
                        throw new Exception("Error : " . $e);
                        // print_r($e);exit;
                    }



                    if (isset($_POST['DailyWorkProgress']['mytask_wpr'])) {
                        Yii::app()->user->setFlash('error', $final_result);
                        Yii::app()->user->setFlash('success', "Successfully created..");
                    } else {
                        $return_result = array('status' => 0, 'message' => $final_result);
                        echo json_encode($return_result);
                        exit;
                    }


                    Yii::app()->user->setFlash('success', "Successfully created..");
                    $model->unsetAttributes();
                    $this->redirect(array('new'));
                }
            }
        }

        $this->render('newindex', array(
            'model' => $model,
        )
        );
    }

    public function consumedWprItems($id, $task_id, $temp_id, $itemid, $itemcount, $itemrate)
    {



        $created_by = Yii::app()->user->id;
        $updated_by = Yii::app()->user->id;



        foreach ($itemid as $key => $item) {
            if ($item != "" && $itemrate[$key] != "" && $itemcount[$key] != "") {
                $item_rate = $itemrate[$key];
                $item_count = $itemcount[$key];


                $sql = "SELECT SUM(item_count) as sum FROM pms_wpr_item_used "
                    . "WHERE task_id ='" . $task_id . "' "
                    // . "AND item_rate_id = '" . $item_rate . "' "
                    . "AND item_id = '" . $item . "' ";
                $tot_qty = Yii::app()->db->createCommand($sql)->queryRow();

                $item_used_count = $tot_qty['sum'] + $item_count;
                $criteria = new CDbCriteria();
                $criteria->select = 'id';
                $criteria->condition = 'find_in_set(' . $item . ',purchase_item) and temp_id=' . $temp_id;
                $data = TemplateItems::model()->find($criteria);




                $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $task_id, 'item_id' => $data->id, 'template_id' => $temp_id));

                $update_item->attributes = array('item_quantity_used' => $item_used_count);

                $update_item->save();



                $template_item_id = $data->id;

                $sql = "INSERT INTO pms_wpr_item_used (wpr_id,task_id,item_id,item_rate_id,item_count,template_item_id,created_by,updated_by) VALUES (:id,:task_id,:item_id,:item_rate_id,:itemcount,:template_item_id,:created_by,:updated_by)";
                $parameters = array(":id" => $id, ":task_id" => $task_id, ":item_id" => $item, ":item_rate_id" => $item_rate, ":itemcount" => $item_count, ":template_item_id" => $template_item_id, ":created_by" => $created_by, ":updated_by" => $updated_by);
                Yii::app()->db->createCommand($sql)->execute($parameters);

                $this->UpdateItemConsumed($task_id, $item, $item_count, $item_rate, $id);
            }
        }
    }

    public function actionUpdatewpr($id)
    {

        $model = $this->loadModel($id);
        $already_exist_item_id = $model->consumed_item_id;
        $already_exist_item_count = $model->consumed_item_count;
        $task_details = Tasks::model()->findByPk($model->taskid);
        $project = Projects::model()->findByPK($task_details->project_id);
        $temp_id = $project->template_id;

        if (($model->created_by == Yii::app()->user->id) || (Yii::app()->user->role == 1)) {
            $total_qty = '';
            $balance_qty = '';
            $unit = '';
            $task_model = Tasks::model()->findByPk($model->taskid);
            if (!empty($task_model)) {
                $tblpx = Yii::app()->db->tablePrefix;
                $total_qty = $task_model->quantity;
                $boq_qty_sum = Yii::app()->db->createCommand("select sum(qty) as qty_total FROM {$tblpx}daily_work_progress WHERE approve_status != 2 AND taskid='" . $model->taskid . "'")->queryRow();
                if (!empty($boq_qty_sum['qty_total'])) {
                    if ($total_qty >= $boq_qty_sum['qty_total']) {
                        $balance_qty = $total_qty - $boq_qty_sum['qty_total'];
                    } else {
                        $balance_qty = 0;
                    }
                } else {
                    $balance_qty = $total_qty;
                }
                $units_data = Unit::model()->findByPk($task_model->unit);
                if (!empty($units_data))
                    $unit = $units_data->unit_code;
            }
            $this->performAjaxValidation($model);
            if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {






                if (isset($_POST['DailyWorkProgress']['consumed_item_id'])) {

                    if ($_POST['DailyWorkProgress']['item_count_status'] != "") {

                        $model->consumed_item_status = 0;
                    }

                    $task_details = Tasks::model()->findByPk($_POST['DailyWorkProgress']['taskid']);
                    $project = Projects::model()->findByPK($task_details->project_id);
                    $temp_id = $project->template_id;

                    $this->removeExistData($model->id, $model->taskid, $temp_id);

                    $this->consumedWprItems($model->id, $model->taskid, $temp_id, $_POST['DailyWorkProgress']['consumed_item_id'], $_POST['DailyWorkProgress']['consumed_item_count'], $_POST['DailyWorkProgress']['consumed_item_rate']);
                }

                if (isset($_POST['DailyWorkProgress']['resources_used'])) {
                    if (!empty($_POST['DailyWorkProgress']['resources_used'] && $_POST['DailyWorkProgress']['utilised_qty'])) {
                        $this->removeResourceUsed($model->id);

                        $this->consumedResources($model->id, $model->taskid, $_POST['DailyWorkProgress']['resources_used'], $_POST['DailyWorkProgress']['utilised_qty'], $_POST['DailyWorkProgress']['unit_id']);
                    }
                }




                if (isset($_POST['DailyWorkProgress']['incident'])) {
                    $model->incident = $_POST['DailyWorkProgress']['incident'];
                }
                if (isset($_POST['DailyWorkProgress']['inspection'])) {
                    $model->inspection = $_POST['DailyWorkProgress']['inspection'];
                }
                if (isset($_POST['DailyWorkProgress']['visitor_name'])) {
                    $model->visitor_name = $_POST['DailyWorkProgress']['visitor_name'];
                }
                if (isset($_POST['DailyWorkProgress']['company_name'])) {
                    $model->company_name = $_POST['DailyWorkProgress']['company_name'];
                }
                if (isset($_POST['DailyWorkProgress']['designation'])) {
                    $model->designation = $_POST['DailyWorkProgress']['designation'];
                }
                if (isset($_POST['DailyWorkProgress']['purpose'])) {
                    $model->purpose = $_POST['DailyWorkProgress']['purpose'];
                }

                $taskid = $_POST['DailyWorkProgress']['taskid'];
                $created_date = date("Y-m-d H:i:s");
                $itemdetails = "";
                if (isset($_POST['newdetails'])) {
                    $itemdetails = $_POST['newdetails'];
                }

                $projectid = $_POST['DailyWorkProgress']['project_id'];
                $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
                $item_id = $qry['tskid'];
                $tot_qty = $qry['quantity'];

                $data = Yii::app()->db->createCommand(
                    "select sum(qty) as qty from pms_daily_work_progress 
				where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' AND id != " . $id . " GROUP BY project_id, taskid"
                )->queryRow();
                $existqty = $data['qty'];
                $newqty = $_POST['DailyWorkProgress']['qty'];
                $qty = $newqty + $existqty;
                $percentage = ($newqty / $tot_qty) * 100;

                $max = $tot_qty - $existqty;


                $model->attributes = $_POST['DailyWorkProgress'];

                //$model->item_id= $item_id;
                $model->taskid = $_POST['DailyWorkProgress']['taskid'];
                $model->work_type = $_POST['DailyWorkProgress']['work_type'];
                //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
                $model->daily_work_progress = round($percentage, 2);
                $model->current_status = $_POST['DailyWorkProgress']['current_status'];
                $model->date = date('Y-m-d', strtotime($_POST['DailyWorkProgress']['date']));
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d H:i:s");
                $model->approve_status = 0;
                $created_date_model = $model->created_date;
                $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
                if ($qry2['assigned_to']) {
                    $assigned_to = $qry2['assigned_to'];
                } else {
                    $assigned_to = Yii::app()->user->id;
                }






                if ($qty > $tot_qty) {
                    Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                    $this->render('index', array(
                        'model' => $model,
                        'itemdetails' => $itemdetails,
                        'interval' => 0,
                    )
                    );
                } else {

                    // $images = CUploadedFile::getInstancesByName('image');
                    // $images_array = array();
                    // $i = 0;
                    // foreach ($images as $image => $pic) {
                    //     $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                    //     $newfilename = rand(1000, 9999) . time();
                    //     $newfilename = md5($newfilename); //optional
                    //     $file_name = $newfilename . '.' . $pic->getExtensionName();
                    //     echo $file_name;
                    //     // echo '<pre>';print_r($pic->getExtensionName());exit;
                    //     $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                    //     if (!file_exists($file))
                    //         $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                    //     array_push($images_array, $file_name);
                    //     $i++;
                    // }
                    // if (!empty($images_array)) {
                    //     $model->img_paths = json_encode($images_array);
                    // } else {
                    //     $model->img_paths = NULL;
                    // }

                    $images = CUploadedFile::getInstancesByName('image');
                    $images_array = array();
                    $i = 0;
                    $label = [];
                    if (isset($_POST['DailyWorkProgress']['image_label'])) {
                        $label = $_POST['DailyWorkProgress']['image_label'];
                    }

                    if (count($images) > 0) {
                        foreach ($images as $image => $pic) {

                            $label_name = $label[$image];

                            $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                            $newfilename = rand(1000, 9999) . time();
                            $newfilename = md5($newfilename); //optional
                            $file_name = $newfilename . '.' . $pic->getExtensionName();

                            $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                            if (!file_exists($file))
                                $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                            array_push($images_array, $file_name);


                            $create_sql = "INSERT INTO pms_wpr_images (wpr_id,image_path,image_label,image_status,created_by) VALUES (:id,:image_path,:image_label,:image_status,:created_by)";

                            $parameters = array(
                                ":id" => $model->id,
                                ":image_path" => $file_name,
                                ":image_label" => $label_name,
                                ":image_status" => 0,
                                ":created_by" => Yii::app()->user->id
                            );

                            Yii::app()->db->createCommand($create_sql)->execute($parameters);

                            $i++;
                        }
                    }


                    if ($model->save() && $model->validate()) {
                        Yii::app()->user->setFlash('success', "Successfully created..");
                        try {
                            // $command = Yii::app()->db->createCommand(
                            // "CALL afterdayworkprogress(:first,:second)");					
                            // $command->bindParam(":first", $taskid, PDO::PARAM_INT);
                            // $command->bindParam(":second", $assigned_to, PDO::PARAM_INT);
                            // $command->execute();


                            $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                            $dependancy_data = $this->DependancyData($qry2, $model->date);
                            $final_result = $schedule_data . '<br>' . $dependancy_data;
                            Yii::app()->user->setFlash('error', $final_result);
                        } catch (Exception $e) {

                            Log::trace("Error : " . $e);
                            throw new Exception("Error : " . $e);
                        }

                        // Yii::app()->user->setFlash('success', "Successfully created..");
                        $this->sendEditMail($model);
                        $model->unsetAttributes();

                        $this->redirect(array('new'));
                    }
                }
            }
            $account_items = [];
            if (Tasks::model()->accountPermission() == 1) {
                $account_items = $this->getAccountItems();
            }

            $sql_query = "SELECT  id,item_id,item_count,item_rate_id "
                . " FROM pms_wpr_item_used where wpr_id=" . $id;

            $consumed_data = Yii::app()->db->createCommand($sql_query)->queryAll();
            $consumed_data_array = [];
            foreach ($consumed_data as $datas) {

                $item_id = $datas['item_id'];
                $item_rate_id = $datas['item_rate_id'];

                $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                    . " FROM jp_specification "
                    . " WHERE id=" . $item_id . "";
                $specification = Yii::app()->db->createCommand($specsql)->queryRow();
                $cat_sql = "SELECT * FROM jp_purchase_category "
                    . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_sql = "SELECT brand_name "
                        . " FROM jp_brand "
                        . " WHERE id=" . $specification['brand_id'] . "";
                    $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
                $item_count = $datas['item_count'];

                $rate_sql = "SELECT rate FROM jp_warehousestock where warehousestock_id=" . $item_rate_id;
                $item_rate = Yii::app()->db->createCommand($rate_sql)->queryRow();

                $consumed_data_array[] = array(
                    'item_id' => $item_id,
                    'item_name' => $item_name,
                    'item_rate_id' => $item_rate_id,
                    'item_rate' => $item_rate['rate'],
                    'item_count' => $item_count,
                    'id' => $datas['id']
                );
            }

            $account_permission_status = Tasks::model()->accountPermission();

            $task_quantity = $task_model->quantity;




            $utilised_resource_sql_query = "SELECT  pms_wpr_resource_used.*,pms_unit.unit_title,pms_resources.resource_name "
                . " FROM pms_wpr_resource_used INNER JOIN pms_unit ON pms_unit.id = pms_wpr_resource_used.resource_unit
        INNER JOIN pms_resources ON pms_resources.id = pms_wpr_resource_used.resource_id 
         where wpr_id=" . $id;

            $utilised_resource = Yii::app()->db->createCommand($utilised_resource_sql_query)
                ->queryAll();


            //echo '<pre>',print_r( $utilised_resource);exit;

            $resource_utilised = [];
            if (Tasks::model()->accountPermission() == 1) {
                $account_items = $this->getAccountItems();
                $resource_utilised = Resources::model()->findAll(
                    array("order" => "resource_name ASC")
                );
            }



            $this->render('update_wpr', array(
                'model' => $model,
                'id' => $id,
                'quantity' => $total_qty,
                'bal_qty' => $balance_qty,
                'unit' => $unit,
                'account_items' => $account_items,
                'consumed_data_array' => $consumed_data_array,
                'account_permission_status' => $account_permission_status,
                'task_quantity' => $task_quantity,
                'utilised_resource' => $utilised_resource,
                'resource_utilised' => $resource_utilised
            )
            );
        } else {
            $this->redirect(array('index'));
        }
    }
    public function removeExistData($id, $task_id, $temp_id)
    {
        $sql_query = "SELECT  item_id,item_count,item_rate_id "
            . " FROM pms_wpr_item_used where wpr_id=" . $id;

        $alraedy_consumed_data = Yii::app()->db->createCommand($sql_query)->queryAll();
        if ($alraedy_consumed_data) {
            foreach ($alraedy_consumed_data as $data) {
                $consumed_item_id = $data['item_id'];
                $consumed_item_count = $data['item_count'];
                $consumed_rate_id = $data['item_rate_id'];

                $Criteria = new CDbCriteria();
                $Criteria->select = 'id';
                $Criteria->condition = 'find_in_set(' . $consumed_item_id . ',purchase_item) and temp_id=' . $temp_id;
                $data = TemplateItems::model()->find($Criteria);

                $already_exist_item_criteria = new CDbCriteria();
                $already_exist_item_criteria->condition = 'item_id=' . $data->id . ' AND task_id=' . $task_id . ' AND template_id=' . $temp_id;
                $already_exist_item_det = TaskItemEstimation::model()->find($already_exist_item_criteria);

                if ($already_exist_item_det) {

                    $count = $already_exist_item_det->item_quantity_used - $consumed_item_count;

                    $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $task_id, 'item_id' => $data->id, 'template_id' => $temp_id));

                    $update_item->attributes = array('item_quantity_used' => $count);

                    $update_item->save();

                    $delete_query = "delete from pms_wpr_item_used
            where wpr_id = '$id' ";
                    Yii::app()->db->createCommand($delete_query)->execute();

                    $delete_sql = "delete from pms_acc_wpr_item_consumed where wpr_id=" . $id;

                    Yii::app()->db->createCommand($delete_sql)->execute();
                }
            }
        }
    }
    public function actiondeleteItemConsumed()
    {
        $id = $_POST['id'];

        $sql_query = "SELECT  id,item_id,item_count,item_rate_id,task_id,wpr_id "
            . " FROM pms_wpr_item_used where id=" . $id;

        $data = Yii::app()->db->createCommand($sql_query)->queryRow();

        $task_id = $data['task_id'];

        $wpr_id = $data['wpr_id'];


        $task_details = Tasks::model()->findByPk($task_id);
        $project = Projects::model()->findByPK($task_details->project_id);
        $temp_id = $project->template_id;

        $remove = $this->removeItemConsumed($id, $task_id, $temp_id, $wpr_id);

        if ($remove == 1) {
            echo '1';
        } else {
            echo '2';
        }
    }
    public function removeItemConsumed($id, $task_id, $temp_id, $wpr_id)
    {
        $sql_query = "SELECT  item_id,item_count,item_rate_id "
            . " FROM pms_wpr_item_used where id=" . $id;

        $alraedy_consumed_data = Yii::app()->db->createCommand($sql_query)->queryRow();
        if ($alraedy_consumed_data) {

            $consumed_item_id = $alraedy_consumed_data['item_id'];
            $consumed_item_count = $alraedy_consumed_data['item_count'];
            $consumed_rate_id = $alraedy_consumed_data['item_rate_id'];

            $Criteria = new CDbCriteria();
            $Criteria->select = 'id';
            $Criteria->condition = 'find_in_set(' . $consumed_item_id . ',purchase_item) and temp_id=' . $temp_id;
            $data = TemplateItems::model()->find($Criteria);

            $already_exist_item_criteria = new CDbCriteria();
            $already_exist_item_criteria->condition = 'item_id=' . $data->id . ' AND task_id=' . $task_id . ' AND template_id=' . $temp_id;
            $already_exist_item_det = TaskItemEstimation::model()->find($already_exist_item_criteria);

            if ($already_exist_item_det) {

                $count = $already_exist_item_det->item_quantity_used - $consumed_item_count;

                $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $task_id, 'item_id' => $data->id, 'template_id' => $temp_id));

                $update_item->attributes = array('item_quantity_used' => $count);

                $update_item->save();

                $delete_query = "delete from pms_wpr_item_used
            where id = '$id' ";
                Yii::app()->db->createCommand($delete_query)->execute();

                $item_rate = Yii::app()->db->createCommand()
                    ->select('rate')
                    ->from('jp_warehousestock')
                    ->where('warehousestock_id=:id', array(':id' => $consumed_rate_id))
                    ->queryScalar();

                $delete_sql = "delete from pms_acc_wpr_item_consumed where wpr_id=" . $wpr_id . "  and item_id=" . $consumed_item_id . "  and item_rate LIKE " . $item_rate;

                Yii::app()->db->createCommand($delete_sql)->execute();

                return 1;
            }
        }
    }
    public function actionview_wpr()
    {
        $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        $task_model = Tasks::model()->findByPk($model->taskid);
        if (!empty($task_model->unit)) {
            $unit_name = $task_model->unit0->unit_title;
        } else {
            $unit_name = '';
        }
        $sql = "SELECT id,image_path,image_label,image_status FROM pms_wpr_images where wpr_id=" . $id;
        $images = Yii::app()->db->createCommand($sql)->queryAll();

        return ($this->renderPartial('view_wpr', array(
            'model' => $model,
            'unit_name' => $unit_name,
            'images' => $images
        )
        ));
    }
    public function actionapprove_wpr_entry()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['entry_id'])) {
            $id = $_REQUEST['entry_id'];
            $time_entry_model = $this->loadModel($id);


            if (isset($_REQUEST['imagearray'])) {
                $image_array = $_REQUEST['imagearray'];
                if (count($image_array) > 0) {
                    foreach ($image_array as $image) {
                        $updatesql = "UPDATE `pms_wpr_images` SET `image_status`=1 where `id`= $image";
                        Yii::app()->db->createCommand($updatesql)->query();
                    }
                }
            }
            if (!empty($time_entry_model->current_status) && $time_entry_model->current_status != 0) {
                $task_model = Tasks::model()->findByPk($time_entry_model->taskid);
                if (!empty($task_model)) {
                    $task_model->status = $time_entry_model->current_status;
                    $task_model->save();
                }
            }
            $task_model = Tasks::model()->findByPk($time_entry_model->taskid);
            $balance = $task_model['quantity'];
            $sql = "select sum(qty) as qty "
                . "FROM {$tblpx}daily_work_progress "
                . "WHERE approve_status = 1 "
                . "AND taskid =" . $time_entry_model->taskid;

            $sql_response3 = Yii::app()->db->createCommand($sql)->queryRow();
            if (!empty($sql_response3)) {
                $approved_qty_sum = $sql_response3['qty'];
                if ($task_model['quantity'] > $sql_response3['qty']) {

                    $balance = $task_model['quantity'] - $sql_response3['qty'];
                    $balance = number_format($balance, 2, '.', '');
                }
            }

            if ($time_entry_model->qty <= $balance && $task_model['quantity'] != $approved_qty_sum) {
                $time_entry_model->approve_status = 1;
                if (isset($_REQUEST['reason'])) {
                    $time_entry_model->approve_reject_data = $_REQUEST['reason'];
                }
                $time_entry_model->approved_date = date("Y-m-d H:i:s");
                if ($time_entry_model->save()) {
                    $task_model = Tasks::model()->findByPk($time_entry_model->taskid);
                    if ($time_entry_model['daily_work_progress'] >= 100) {
                        $task_model->status = 7;
                        $task_model->save();
                        $this->taskCompletionMail($time_entry_model->taskid);
                    }
                    $schedule_data = $this->CheckprogressPercentags($task_model, $time_entry_model->date, $time_entry_model->daily_work_progress, $time_entry_model->taskid, 2); // 2 => after approve
                    $dependancy_data = $this->DependancyData($task_model, $time_entry_model->date);
                    $return_data = array('status' => 1, 'schedule' => $schedule_data, 'dependant' => $dependancy_data);
                    echo json_encode($return_data);
                } else {
                    $return_data = array('status' => 2);
                    echo json_encode($return_data);
                }
            } else {
                $return_data = array('status' => 2, 'message' => 'Exceeded available quantity');
                echo json_encode($return_data);
            }
        }
    }
    public function actionRejectentry_wpr()
    {
        if (isset($_REQUEST['entry_id'])) {
            $id = $_REQUEST['entry_id'];
            $time_entry_model = $this->loadModel($id);
            $time_entry_model->approve_status = 2;
            if (isset($_REQUEST['reason'])) {
                $time_entry_model->approve_reject_data = $_REQUEST['reason'];
            }

            if (isset($_REQUEST['imagearray'])) {
                $image_array = $_REQUEST['imagearray'];
                if (count($image_array) > 0) {
                    foreach ($image_array as $image) {
                        $updatesql = "UPDATE `pms_wpr_images` SET `image_status`=2 where `id`= $image";
                        Yii::app()->db->createCommand($updatesql)->query();
                    }
                }
            }

            $time_entry_model->approved_date = date("Y-m-d H:i:s");
            if ($time_entry_model->save()) {
                $messge = 'Successfully Rejected';
                $return_data = array('status' => 1, 'message' => $messge);
                echo json_encode($return_data);
            } else {
                $return_data = array('status' => 2, 'message' => 'An error occured');
                echo json_encode($return_data);
            }
        }
    }
    public function actionworkProgress()
    {
        if (isset($_POST['task_id'])) {
            $task_id = $_POST['task_id'];
            $model = new DailyWorkProgress;

            $task = Tasks::model()->findByPk($task_id);

            $project = Projects::model()->findByPk($task->project_id);

            $work_type = WorkType::model()->findByPk($task->work_type_id);

            $unit = Unit::model()->findByPk($task->unit);

            $result = $this->renderPartial('create_work_progress', array(
                'model' => $model,
                'task' => $task,
                'project' => $project,
                'work_type' => $work_type,
                'unit' => $unit,
                'status' => 1
            ), true);
            echo json_encode($result);
        }
    }
    public function actioncreatewpr()
    {

        $model = new DailyWorkProgress;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {


            $taskid = $_POST['DailyWorkProgress']['taskid'];
            $created_date = date("Y-m-d H:i:s");
            $itemdetails = $_POST['newdetails'];


            $projectid = $_POST['DailyWorkProgress']['project_id'];

            $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
            $item_id = $qry['tskid'];
            $tot_qty = $qry['quantity'];

            $data = Yii::app()->db->createCommand(
                "select sum(qty) as qty from pms_daily_work_progress 
			where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' GROUP BY project_id, taskid"
            )->queryRow();
            $existqty = $data['qty'];
            $newqty = $_POST['DailyWorkProgress']['qty'];
            $qty = $newqty + $existqty;
            $percentage = ($newqty / $tot_qty) * 100;
            $max = $tot_qty - $existqty;


            $model->attributes = $_POST['DailyWorkProgress'];
            $model->taskid = $_POST['DailyWorkProgress']['taskid'];
            //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
            $model->work_type = $_POST['DailyWorkProgress']['work_type'];
            $model->daily_work_progress = round($percentage, 2);
            $model->current_status = $_POST['DailyWorkProgress']['current_status'];
            $model->date = date('Y-m-d', strtotime($_POST['DailyWorkProgress']['date']));
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d H:i:s");
            $model->latitude = $_POST['DailyWorkProgress']['latitude'];
            $model->longitude = $_POST['DailyWorkProgress']['longitude'];
            $model->site_id = $_POST['DailyWorkProgress']['site_id'];
            $model->site_name = $_POST['DailyWorkProgress']['site_name'];
            $created_date_model = $model->created_date;
            if (isset($_POST['DailyWorkProgress']['consumed_item_id'])) {

                $model->consumed_item_status = 1;

                if ($_POST['DailyWorkProgress']['item_count_status'] != "") {

                    $model->consumed_item_status = 0;
                }
            }


            if (isset($_POST['DailyWorkProgress']['incident'])) {
                $model->incident = $_POST['DailyWorkProgress']['incident'];
            }
            if (isset($_POST['DailyWorkProgress']['inspection'])) {
                $model->inspection = $_POST['DailyWorkProgress']['inspection'];
            }
            if (isset($_POST['DailyWorkProgress']['visitor'])) {
                $model->visitor_name = $_POST['DailyWorkProgress']['visitor'];
            }
            if (isset($_POST['DailyWorkProgress']['company'])) {
                $model->company_name = $_POST['DailyWorkProgress']['company'];
            }
            if (isset($_POST['DailyWorkProgress']['designation'])) {
                $model->designation = $_POST['DailyWorkProgress']['designation'];
            }
            if (isset($_POST['DailyWorkProgress']['purpose'])) {
                $model->purpose = $_POST['DailyWorkProgress']['purpose'];
            }




            $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
            if ($qry2['assigned_to']) {
                $assigned_to = $qry2['assigned_to'];
            } else {
                $assigned_to = Yii::app()->user->id;
            }

            if ($qty > $tot_qty) {
                Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                $this->render('index', array(
                    'model' => $model,
                    'itemdetails' => $itemdetails,
                    'interval' => 0,
                )
                );
            } else {




                if ($model->save() && $model->validate()) {
                    try {
                        if (!empty($_POST['DailyWorkProgress']['consumed_item_id'])) {


                            $task_details = Tasks::model()->findByPk($_POST['DailyWorkProgress']['taskid']);
                            $project = Projects::model()->findByPK($task_details->project_id);
                            $temp_id = $project->template_id;

                            $this->consumedWprItems($model->id, $model->taskid, $temp_id, $_POST['DailyWorkProgress']['consumed_item_id'], $_POST['DailyWorkProgress']['consumed_item_count'], $_POST['DailyWorkProgress']['consumed_item_rate']);
                        }

                        $images = CUploadedFile::getInstancesByName('image');
                        $images_array = array();
                        $i = 0;
                        $label = [];
                        if (isset($_POST['DailyWorkProgress']['image_label'])) {
                            $label = $_POST['DailyWorkProgress']['image_label'];
                        }









                        if (count($images) > 0) {
                            foreach ($images as $image => $pic) {

                                $label_name = $label[$image];


                                $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                                $newfilename = rand(1000, 9999) . time();
                                $newfilename = md5($newfilename); //optional
                                $file_name = $newfilename . '.' . $pic->getExtensionName();

                                $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                                if (!file_exists($file))

                                    $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                                array_push($images_array, $file_name);


                                $create_sql = "INSERT INTO pms_wpr_images (wpr_id,image_path,image_label,image_status,created_by) VALUES (:id,:image_path,:image_label,:image_status,:created_by)";

                                $parameters = array(
                                    ":id" => $model->id,
                                    ":image_path" => $file_name,
                                    ":image_label" => $label_name,
                                    ":image_status" => 0,
                                    ":created_by" => Yii::app()->user->id
                                );

                                Yii::app()->db->createCommand($create_sql)->execute($parameters);

                                $i++;
                            }
                        }



                        $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                        $dependancy_data = $this->DependancyData($qry2, $model->date);
                        $final_result = $schedule_data . '<br>' . $dependancy_data;
                        // Yii::app()->user->setFlash('error', $final_result);
                        // Yii::app()->user->setFlash('success', "Successfully created..");
                    } catch (Exception $e) {

                        Log::trace("Error : " . $e);
                        throw new Exception("Error : " . $e);
                    }



                    if (isset($_POST['DailyWorkProgress']['projem'])) {

                        Yii::app()->user->setFlash('error', $final_result);
                        Yii::app()->user->setFlash('success', "Successfully created..");
                    } else {
                        $return_result = array('status' => 0, 'message' => $final_result);
                        echo json_encode($return_result);
                        exit;
                    }


                    Yii::app()->user->setFlash('success', "Successfully created..");
                    $model->unsetAttributes();

                    if (isset($_POST['DailyWorkProgress']['monthytask'])) {
                        $this->redirect(array('Tasks/monthlyTask'));
                    } else {
                        $this->redirect(array('Reports/weeklyReport', 'project_id' => $_POST['DailyWorkProgress']['project_id']));
                    }

                    //
                }
            }
        }

        // $this->render('newindex', array(
        //     'model' => $model,
        // ));
    }
    public function actionmonthtaskworkProgress()
    {
        if (isset($_POST['task_id'])) {
            $task_id = $_POST['task_id'];
            $model = new DailyWorkProgress;

            $task = Tasks::model()->findByPk($task_id);

            $project = Projects::model()->findByPk($task->project_id);

            $work_type = WorkType::model()->findByPk($task->work_type_id);

            $unit = Unit::model()->findByPk($task->unit);

            $result = $this->renderPartial('monthly_task_work_progress', array(
                'model' => $model,
                'task' => $task,
                'project' => $project,
                'work_type' => $work_type,
                'unit' => $unit,
                'status' => 1
            ), true);
            echo json_encode($result);
        }
    }

    public function actiongetBalanceQuantity()
    {
        $task_id = $_POST['task_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $sql_response2 = Tasks::model()->findByPk($task_id);
        $balance_qty = 0;
        if (!empty($sql_response2)) {
            $boq_qty = Yii::app()->db->createCommand("select sum(qty) as qty FROM {$tblpx}daily_work_progress WHERE approve_status != 2 AND taskid='" . $task_id . "'")->queryRow();
            if (!empty($boq_qty)) {
                $boq_qty_val = $boq_qty['qty'];
                $balance_qty = $sql_response2['quantity'] - $boq_qty_val;
            } else {
                $balance_qty = $sql_response2['quantity'];
            }
        }
        echo json_encode(array('status' => 1, 'balance_qty' => $balance_qty));
    }

    public function actionItemEstimatiomMsg()
    {
        $wpr_id = $_POST['id'];

        $sql = "SELECT * FROM pms_wpr_item_used WHERE wpr_id=" . $wpr_id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $datas) {
            $template_item_id = $datas['template_item_id'];

            $item_count_used = $datas['item_count'];

            $item_estimation_query = "SELECT  * FROM pms_task_item_estimation WHERE item_id=" . $template_item_id;

            $item_estimation = Yii::app()->db->createCommand($item_estimation_query)->queryRow();

            if ($item_estimation['item_quantity_required'] < $item_count_used) {
                $data_array[] = array("  Required Qty is " . $item_estimation['item_quantity_required'] . " and used Quantity is " . $item_count_used);
            }
        }

        echo json_encode($data_array);
    }
    public function actiongetResourceUnit()
    {
        $resources_used = $_POST['resources_used'];
        $model = Resources::model()->findByPk($resources_used);
        $criteria = new CDbCriteria();

        $criteria->join = ' INNER JOIN `pms_resources` `resources` ON resources.resource_unit=t.id';


        $criteria->addCondition('resources.id = ' . $resources_used . '');

        $data = Unit::model()->find($criteria);
        $unit = $data->unit_title;
        echo json_encode(array('unit' => $unit, 'unit_id' => $data->id));
    }
    public function consumedResources($wpr_id, $taskid, $resources_used, $utilised_qty, $unit_id)
    {
        $created_by = Yii::app()->user->id;



        foreach ($resources_used as $key => $resource) {

            $used_qty = $utilised_qty[$key];
            $unit = $unit_id[$key];
            if ($resource != "" && $used_qty != "" && $unit != "") {
                $model = Resources::model()->findByPk($resource);
                $rate = $model->resource_rate;
                $total_amount = $used_qty * $rate;

                $sql = "INSERT INTO pms_wpr_resource_used (wpr_id,task_id,resource_id,resource_qty,resource_unit,amount,created_by,created_date) VALUES (:wpr_id,:task_id,:resource_id,:resource_qty,:resource_unit,:amount,:created_by,:created_date)";


                $parameters = array(
                    ":wpr_id" => $wpr_id,
                    ":task_id" => $taskid,
                    ":resource_id" => $resource,
                    ":resource_qty" => $used_qty,
                    ":resource_unit" => $unit,
                    ":amount" => $total_amount,
                    ":created_by" => $created_by,
                    ":created_date" => date('Y-m-d')
                );
                Yii::app()->db->createCommand($sql)->execute($parameters);
            }
        }
    }
    public function actiondeleteResourceUtilised()
    {
        $id = $_POST['id'];
        $delete_query = "delete from pms_wpr_resource_used
            where id = '$id' ";
        $delete = Yii::app()->db->createCommand($delete_query)->execute();

        if ($delete == 1) {
            echo '1';
        } else {
            echo '2';
        }
    }
    public function removeResourceUsed($wpr_id)
    {
        $delete_query = "delete from pms_wpr_resource_used
    where wpr_id = '$wpr_id' ";
        $delete = Yii::app()->db->createCommand($delete_query)->execute();
    }

    public function actiongetexpiretaskdetails()
    {
        $task_id = $_POST['task_id'];
        $model = new DailyWorkProgress;

        $task = Tasks::model()->findByPk($task_id);

        $project = Projects::model()->findByPk($task->project_id);

        $work_type = WorkType::model()->findByPk($task->work_type_id);

        $unit = Unit::model()->findByPk($task->unit);

        $resource_utilised = [];
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
            $resource_utilised = Resources::model()->findAll(
                array("order" => "resource_name ASC")
            );
        }

        $result = $this->renderPartial('create_expire_daily_work_progress', array(
            'model' => $model,
            'task' => $task,
            'project' => $project,
            'work_type' => $work_type,
            'unit' => $unit,
            'resource_utilised' => $resource_utilised
        ), true);
        echo json_encode($result);
    }
    public function actioncreate_expire_daily_wpr()
    {

        $model = new DailyWorkProgress;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {


            $taskid = $_POST['DailyWorkProgress']['taskid'];
            $created_date = date("Y-m-d H:i:s");
            $itemdetails = $_POST['newdetails'];


            $projectid = $_POST['DailyWorkProgress']['project_id'];

            $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
            $item_id = $qry['tskid'];
            $tot_qty = $qry['quantity'];

            $data = Yii::app()->db->createCommand(
                "select sum(qty) as qty from pms_daily_work_progress 
			where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' GROUP BY project_id, taskid"
            )->queryRow();
            $existqty = $data['qty'];
            $newqty = $_POST['DailyWorkProgress']['qty'];
            $qty = $newqty + $existqty;
            $percentage = ($newqty / $tot_qty) * 100;
            $max = $tot_qty - $existqty;


            $model->attributes = $_POST['DailyWorkProgress'];
            $model->taskid = $_POST['DailyWorkProgress']['taskid'];
            //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
            $model->work_type = $_POST['DailyWorkProgress']['work_type'];
            $model->daily_work_progress = round($percentage, 2);
            $model->current_status = $_POST['DailyWorkProgress']['current_status'];
            $model->date = date('Y-m-d');
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d H:i:s");
            $model->latitude = $_POST['DailyWorkProgress']['latitude'];
            $model->longitude = $_POST['DailyWorkProgress']['longitude'];
            $model->site_id = $_POST['DailyWorkProgress']['site_id'];
            $model->site_name = $_POST['DailyWorkProgress']['site_name'];
            $created_date_model = $model->created_date;
            if (isset($_POST['DailyWorkProgress']['consumed_item_id'])) {

                $model->consumed_item_status = 1;

                if ($_POST['DailyWorkProgress']['item_count_status'] != "") {

                    $model->consumed_item_status = 0;
                }
            }

            if (isset($_POST['DailyWorkProgress']['incident'])) {
                $model->incident = $_POST['DailyWorkProgress']['incident'];
            }
            if (isset($_POST['DailyWorkProgress']['inspection'])) {
                $model->inspection = $_POST['DailyWorkProgress']['inspection'];
            }
            if (isset($_POST['DailyWorkProgress']['visitor'])) {
                $model->visitor_name = $_POST['DailyWorkProgress']['visitor'];
            }
            if (isset($_POST['DailyWorkProgress']['company'])) {
                $model->company_name = $_POST['DailyWorkProgress']['company'];
            }
            if (isset($_POST['DailyWorkProgress']['designation'])) {
                $model->designation = $_POST['DailyWorkProgress']['designation'];
            }
            if (isset($_POST['DailyWorkProgress']['purpose'])) {
                $model->purpose = $_POST['DailyWorkProgress']['purpose'];
            }

            if (isset($_POST['DailyWorkProgress']['work_progress_type'])) {
                $model->work_progress_type = $_POST['DailyWorkProgress']['work_progress_type'];
            }




            $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
            if ($qry2['assigned_to']) {
                $assigned_to = $qry2['assigned_to'];
            } else {
                $assigned_to = Yii::app()->user->id;
            }

            if ($qty > $tot_qty) {
                Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                $this->render('index', array(
                    'model' => $model,
                    'itemdetails' => $itemdetails,
                    'interval' => 0,
                )
                );
            } else {




                if ($model->save() && $model->validate()) {
                    try {
                        if (!empty($_POST['DailyWorkProgress']['consumed_item_id'])) {


                            $task_details = Tasks::model()->findByPk($_POST['DailyWorkProgress']['taskid']);
                            $project = Projects::model()->findByPK($task_details->project_id);
                            $temp_id = $project->template_id;

                            $this->consumedWprItems($model->id, $model->taskid, $temp_id, $_POST['DailyWorkProgress']['consumed_item_id'], $_POST['DailyWorkProgress']['consumed_item_count'], $_POST['DailyWorkProgress']['consumed_item_rate']);
                        }


                        if (!empty($_POST['DailyWorkProgress']['resources_used'] && $_POST['DailyWorkProgress']['utilised_qty'])) {
                            $this->consumedResources($model->id, $model->taskid, $_POST['DailyWorkProgress']['resources_used'], $_POST['DailyWorkProgress']['utilised_qty'], $_POST['DailyWorkProgress']['unit_id']);
                        }


                        $images = CUploadedFile::getInstancesByName('image');
                        $images_array = array();
                        $i = 0;
                        $label = [];
                        if (isset($_POST['DailyWorkProgress']['image_label'])) {
                            $label = $_POST['DailyWorkProgress']['image_label'];
                        }

                        if (count($images) > 0) {
                            foreach ($images as $image => $pic) {

                                $label_name = $label[$image];


                                $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                                $newfilename = rand(1000, 9999) . time();
                                $newfilename = md5($newfilename); //optional
                                $file_name = $newfilename . '.' . $pic->getExtensionName();

                                $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                                if (!file_exists($file))

                                    $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                                array_push($images_array, $file_name);


                                $create_sql = "INSERT INTO pms_wpr_images (wpr_id,image_path,image_label,image_status,created_by) VALUES (:id,:image_path,:image_label,:image_status,:created_by)";

                                $parameters = array(
                                    ":id" => $model->id,
                                    ":image_path" => $file_name,
                                    ":image_label" => $label_name,
                                    ":image_status" => 0,
                                    ":created_by" => Yii::app()->user->id
                                );

                                Yii::app()->db->createCommand($create_sql)->execute($parameters);

                                $i++;
                            }
                        }



                        $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                        $dependancy_data = $this->DependancyData($qry2, $model->date);
                        $final_result = $schedule_data . '<br>' . $dependancy_data;
                        // Yii::app()->user->setFlash('error', $final_result);
                        // Yii::app()->user->setFlash('success', "Successfully created..");
                    } catch (Exception $e) {

                        Log::trace("Error : " . $e);
                        throw new Exception("Error : " . $e);
                        // print_r($e);exit;
                    }



                    if (isset($_POST['DailyWorkProgress']['mytask_wpr'])) {
                        Yii::app()->user->setFlash('error', $final_result);
                        Yii::app()->user->setFlash('success', "Successfully created..");
                    } else {
                        $return_result = array('status' => 0, 'message' => $final_result);
                        echo json_encode($return_result);
                        exit;
                    }


                    Yii::app()->user->setFlash('success', "Successfully created..");
                    $model->unsetAttributes();
                    //$this->redirect(array('new'));
                    $this->redirect(array('tasks/view', 'id' => $_POST['DailyWorkProgress']['taskid']));
                }
            }
        }
    }


    public function actionupdateexpiredwpr($id)
    {

        $model = $this->loadModel($id);
        $already_exist_item_id = $model->consumed_item_id;
        $already_exist_item_count = $model->consumed_item_count;
        $task_details = Tasks::model()->findByPk($model->taskid);
        $project = Projects::model()->findByPK($task_details->project_id);
        $temp_id = $project->template_id;

        if (($model->created_by == Yii::app()->user->id) || (Yii::app()->user->role == 1)) {
            $total_qty = '';
            $balance_qty = '';
            $unit = '';
            $task_model = Tasks::model()->findByPk($model->taskid);
            if (!empty($task_model)) {
                $tblpx = Yii::app()->db->tablePrefix;
                $total_qty = $task_model->quantity;
                $boq_qty_sum = Yii::app()->db->createCommand("select sum(qty) as qty_total FROM {$tblpx}daily_work_progress WHERE approve_status != 2 AND taskid='" . $model->taskid . "'")->queryRow();
                if (!empty($boq_qty_sum['qty_total'])) {
                    if ($total_qty >= $boq_qty_sum['qty_total']) {
                        $balance_qty = $total_qty - $boq_qty_sum['qty_total'];
                    } else {
                        $balance_qty = 0;
                    }
                } else {
                    $balance_qty = $total_qty;
                }
                $units_data = Unit::model()->findByPk($task_model->unit);
                if (!empty($units_data))
                    $unit = $units_data->unit_code;
            }
            $this->performAjaxValidation($model);
            if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {






                if (isset($_POST['DailyWorkProgress']['consumed_item_id'])) {

                    if ($_POST['DailyWorkProgress']['item_count_status'] != "") {

                        $model->consumed_item_status = 0;
                    }

                    $task_details = Tasks::model()->findByPk($_POST['DailyWorkProgress']['taskid']);
                    $project = Projects::model()->findByPK($task_details->project_id);
                    $temp_id = $project->template_id;

                    $this->removeExistData($model->id, $model->taskid, $temp_id);

                    $this->consumedWprItems($model->id, $model->taskid, $temp_id, $_POST['DailyWorkProgress']['consumed_item_id'], $_POST['DailyWorkProgress']['consumed_item_count'], $_POST['DailyWorkProgress']['consumed_item_rate']);
                }

                if (isset($_POST['DailyWorkProgress']['resources_used'])) {
                    if (!empty($_POST['DailyWorkProgress']['resources_used'] && $_POST['DailyWorkProgress']['utilised_qty'])) {
                        $this->removeResourceUsed($model->id);

                        $this->consumedResources($model->id, $model->taskid, $_POST['DailyWorkProgress']['resources_used'], $_POST['DailyWorkProgress']['utilised_qty'], $_POST['DailyWorkProgress']['unit_id']);
                    }
                }




                if (isset($_POST['DailyWorkProgress']['incident'])) {
                    $model->incident = $_POST['DailyWorkProgress']['incident'];
                }
                if (isset($_POST['DailyWorkProgress']['inspection'])) {
                    $model->inspection = $_POST['DailyWorkProgress']['inspection'];
                }
                if (isset($_POST['DailyWorkProgress']['visitor_name'])) {
                    $model->visitor_name = $_POST['DailyWorkProgress']['visitor_name'];
                }
                if (isset($_POST['DailyWorkProgress']['company_name'])) {
                    $model->company_name = $_POST['DailyWorkProgress']['company_name'];
                }
                if (isset($_POST['DailyWorkProgress']['designation'])) {
                    $model->designation = $_POST['DailyWorkProgress']['designation'];
                }
                if (isset($_POST['DailyWorkProgress']['purpose'])) {
                    $model->purpose = $_POST['DailyWorkProgress']['purpose'];
                }

                $taskid = $_POST['DailyWorkProgress']['taskid'];
                $created_date = date("Y-m-d H:i:s");
                $itemdetails = "";
                if (isset($_POST['newdetails'])) {
                    $itemdetails = $_POST['newdetails'];
                }

                $projectid = $_POST['DailyWorkProgress']['project_id'];
                $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
                $item_id = $qry['tskid'];
                $tot_qty = $qry['quantity'];

                $data = Yii::app()->db->createCommand(
                    "select sum(qty) as qty from pms_daily_work_progress 
				where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' AND id != " . $id . " GROUP BY project_id, taskid"
                )->queryRow();
                $existqty = $data['qty'];
                $newqty = $_POST['DailyWorkProgress']['qty'];
                $qty = $newqty + $existqty;
                $percentage = ($newqty / $tot_qty) * 100;

                $max = $tot_qty - $existqty;


                $model->attributes = $_POST['DailyWorkProgress'];

                //$model->item_id= $item_id;
                $model->taskid = $_POST['DailyWorkProgress']['taskid'];
                $model->work_type = $_POST['DailyWorkProgress']['work_type'];
                //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
                $model->daily_work_progress = round($percentage, 2);
                $model->current_status = $_POST['DailyWorkProgress']['current_status'];
                $model->date = date('Y-m-d', strtotime($_POST['DailyWorkProgress']['date']));
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d H:i:s");
                $model->approve_status = 0;
                $created_date_model = $model->created_date;
                $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
                if ($qry2['assigned_to']) {
                    $assigned_to = $qry2['assigned_to'];
                } else {
                    $assigned_to = Yii::app()->user->id;
                }






                if ($qty > $tot_qty) {
                    Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                    // $this->render('index', array(
                    //   'model' => $model, 'itemdetails' => $itemdetails, 'interval' => 0,
                    // ));
                    $this->redirect(array('tasks/view', 'id' => $_POST['DailyWorkProgress']['taskid']));
                } else {



                    $images = CUploadedFile::getInstancesByName('image');
                    $images_array = array();
                    $i = 0;
                    $label = [];
                    if (isset($_POST['DailyWorkProgress']['image_label'])) {
                        $label = $_POST['DailyWorkProgress']['image_label'];
                    }

                    if (count($images) > 0) {
                        foreach ($images as $image => $pic) {

                            $label_name = $label[$image];

                            $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/dailywork_progress");
                            $newfilename = rand(1000, 9999) . time();
                            $newfilename = md5($newfilename); //optional
                            $file_name = $newfilename . '.' . $pic->getExtensionName();

                            $file = Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name;
                            if (!file_exists($file))
                                $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                            array_push($images_array, $file_name);


                            $create_sql = "INSERT INTO pms_wpr_images (wpr_id,image_path,image_label,image_status,created_by) VALUES (:id,:image_path,:image_label,:image_status,:created_by)";

                            $parameters = array(
                                ":id" => $model->id,
                                ":image_path" => $file_name,
                                ":image_label" => $label_name,
                                ":image_status" => 0,
                                ":created_by" => Yii::app()->user->id
                            );

                            Yii::app()->db->createCommand($create_sql)->execute($parameters);

                            $i++;
                        }
                    }


                    if ($model->save() && $model->validate()) {
                        Yii::app()->user->setFlash('success', "Successfully created..");
                        try {


                            $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                            $dependancy_data = $this->DependancyData($qry2, $model->date);
                            $final_result = $schedule_data . '<br>' . $dependancy_data;
                            Yii::app()->user->setFlash('error', $final_result);
                        } catch (Exception $e) {

                            Log::trace("Error : " . $e);
                            throw new Exception("Error : " . $e);
                        }

                        // Yii::app()->user->setFlash('success', "Successfully created..");
                        $this->sendEditMail($model);
                        $model->unsetAttributes();
                        $this->redirect(array('tasks/view', 'id' => $_POST['DailyWorkProgress']['taskid']));
                        //$this->redirect(array('new'));
                    }
                }
            }
            $account_items = [];
            if (Tasks::model()->accountPermission() == 1) {
                $account_items = $this->getAccountItems();
            }

            $sql_query = "SELECT  id,item_id,item_count,item_rate_id "
                . " FROM pms_wpr_item_used where wpr_id=" . $id;

            $consumed_data = Yii::app()->db->createCommand($sql_query)->queryAll();
            $consumed_data_array = [];
            foreach ($consumed_data as $datas) {

                $item_id = $datas['item_id'];
                $item_rate_id = $datas['item_rate_id'];

                $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                    . " FROM jp_specification "
                    . " WHERE id=" . $item_id . "";
                $specification = Yii::app()->db->createCommand($specsql)->queryRow();
                $cat_sql = "SELECT * FROM jp_purchase_category "
                    . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_sql = "SELECT brand_name "
                        . " FROM jp_brand "
                        . " WHERE id=" . $specification['brand_id'] . "";
                    $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
                $item_count = $datas['item_count'];

                $rate_sql = "SELECT rate FROM jp_warehousestock where warehousestock_id=" . $item_rate_id;
                $item_rate = Yii::app()->db->createCommand($rate_sql)->queryRow();

                $consumed_data_array[] = array(
                    'item_id' => $item_id,
                    'item_name' => $item_name,
                    'item_rate_id' => $item_rate_id,
                    'item_rate' => $item_rate['rate'],
                    'item_count' => $item_count,
                    'id' => $datas['id']
                );
            }

            $account_permission_status = Tasks::model()->accountPermission();

            $task_quantity = $task_model->quantity;




            $utilised_resource_sql_query = "SELECT  pms_wpr_resource_used.*,pms_unit.unit_title,pms_resources.resource_name "
                . " FROM pms_wpr_resource_used INNER JOIN pms_unit ON pms_unit.id = pms_wpr_resource_used.resource_unit
        INNER JOIN pms_resources ON pms_resources.id = pms_wpr_resource_used.resource_id 
         where wpr_id=" . $id;

            $utilised_resource = Yii::app()->db->createCommand($utilised_resource_sql_query)
                ->queryAll();


            //echo '<pre>',print_r( $utilised_resource);exit;

            $resource_utilised = [];
            if (Tasks::model()->accountPermission() == 1) {
                $account_items = $this->getAccountItems();
                $resource_utilised = Resources::model()->findAll(
                    array("order" => "resource_name ASC")
                );
            }

            $task = Tasks::model()->findByPk($model->taskid);

            $project = Projects::model()->findByPk($task->project_id);

            $work_type = WorkType::model()->findByPk($task->work_type_id);





            $this->render('update_expired_wpr', array(
                'model' => $model,
                'id' => $id,
                'quantity' => $total_qty,
                'bal_qty' => $balance_qty,
                'unit' => $unit,
                'account_items' => $account_items,
                'consumed_data_array' => $consumed_data_array,
                'account_permission_status' => $account_permission_status,
                'task_quantity' => $task_quantity,
                'utilised_resource' => $utilised_resource,
                'resource_utilised' => $resource_utilised,
                'project' => $project,
                'task' => $task,
                'work_type' => $work_type
            )
            );
        } else {
            //$this->redirect(array('index'));
            $this->redirect(array('tasks/view', 'id' => $_POST['DailyWorkProgress']['taskid']));
        }
    }

    public function actionNewwprform()
    {
        /* $dataProvider=new CActiveDataProvider('DailyWorkProgress');
          $this->render('index',array(
          'dataProvider'=>$dataProvider,
          )); */
        $from_date = '';
        $to_date = '';
        unset(Yii::app()->request->cookies['from_date']);  // first unset cookie for dates
        unset(Yii::app()->request->cookies['to_date']);
        $interval = (isset($_GET['interval']) ? $_GET['interval'] : 0);
        $selected_status = (isset($_GET['type']) ? $_GET['type'] : 'All');
        $model = new DailyWorkProgress('search');
        $model->unsetAttributes();  // clear any default values
        if (!empty($_REQUEST)) {
            if (isset($_POST['from_date'])) {
                Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date', $_POST['from_date']); // define cookie for from_date
                $model->from_date = $_REQUEST['from_date'];
            }

            if (isset($_POST['to_date'])) {
                Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date', $_POST['to_date']);
                $model->to_date = $_REQUEST['to_date'];
            }
        }
        if (isset($_REQUEST['DailyWorkProgress'])) {
            $model->attributes = $_REQUEST['DailyWorkProgress'];
        }
        $account_items = [];
        $resource_utilised = [];
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
            $resource_utilised = Resources::model()->findAll(
                array("order" => "resource_name ASC")
            );
        }

        $this->render('new_index_wpr', array(
            'model' => $model,
            'interval' => $interval,
            'selected_status' => $selected_status,
            'account_items' => $account_items,
            'resource_utilised' => $resource_utilised
        )
        );
    }

    public function actionWpr()
    {
        $from_date = '';
        $to_date = '';
        unset(Yii::app()->request->cookies['from_date']);  // first unset cookie for dates
        unset(Yii::app()->request->cookies['to_date']);
        $interval = (isset($_GET['interval']) ? $_GET['interval'] : 0);
        $selected_status = (isset($_GET['type']) ? $_GET['type'] : 'All');
        $model = new DailyWorkProgress('search');
        $model->unsetAttributes();  // clear any default values
        if (!empty($_REQUEST)) {
            if (isset($_POST['from_date'])) {
                Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date', $_POST['from_date']); // define cookie for from_date
                $model->from_date = $_REQUEST['from_date'];
            }

            if (isset($_POST['to_date'])) {
                Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date', $_POST['to_date']);
                $model->to_date = $_REQUEST['to_date'];
            }
        }
        if (isset($_REQUEST['DailyWorkProgress'])) {
            $model->attributes = $_REQUEST['DailyWorkProgress'];
        }
        $account_items = [];
        $resource_utilised = [];
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
            $resource_utilised = Resources::model()->findAll(
                array("order" => "resource_name ASC")
            );
        }

        $this->performAjaxValidation($model);

        $this->render('work_progress_index', array(
            'model' => $model,
            'interval' => $interval,
            'selected_status' => $selected_status,
            'account_items' => $account_items,
            'resource_utilised' => $resource_utilised,
            'task_total_quantity' => ''
        )
        );
    }

    public function actionsaveWorkProgress()
    {

        $id = $_POST['DailyWorkProgress']['id'];
        if ($id != "") {
            $model = $this->loadModel($id);
        } else {
            $model = new DailyWorkProgress;
        }

        if (isset($_POST['DailyWorkProgress']) && !empty($_POST['DailyWorkProgress']['taskid'])) {

            $transaction = Yii::app()->db->beginTransaction();
            try {
                $taskid = $_POST['DailyWorkProgress']['taskid'];
                $created_date = date("Y-m-d H:i:s");
                $itemdetails = $_POST['newdetails'];


                $projectid = $_POST['DailyWorkProgress']['project_id'];

                $qry = Tasks::model()->find(array("condition" => "project_id=" . $projectid . " AND tskid =" . $taskid . ""));
                $item_id = $qry['tskid'];
                $tot_qty = $qry['quantity'];

                $data = Yii::app()->db->createCommand(
                    "select sum(qty) as qty from pms_daily_work_progress 
        where approve_status = 1 AND project_id=" . $projectid . " AND taskid='" . $taskid . "' GROUP BY project_id, taskid"
                )->queryRow();
                $existqty = $data['qty'];
                $newqty = $_POST['DailyWorkProgress']['qty'];
                $qty = $newqty + $existqty;
                $percentage = ($newqty / $tot_qty) * 100;
                $max = $tot_qty - $existqty;


                $model->attributes = $_POST['DailyWorkProgress'];
                $model->taskid = $_POST['DailyWorkProgress']['taskid'];
                //$model->image_label = $_POST['DailyWorkProgress']['image_label'];
                $model->work_type = $_POST['DailyWorkProgress']['work_type'];
                $model->daily_work_progress = round($percentage, 2);
                $model->current_status = $_POST['DailyWorkProgress']['current_status'];
                $model->date = date('Y-m-d', strtotime($_POST['DailyWorkProgress']['date']));
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d H:i:s");
                $model->latitude = $_POST['DailyWorkProgress']['latitude'];
                $model->longitude = $_POST['DailyWorkProgress']['longitude'];
                $model->site_id = $_POST['DailyWorkProgress']['site_id'];
                $model->site_name = $_POST['DailyWorkProgress']['site_name'];
                $created_date_model = $model->created_date;

                if (isset($_POST['DailyWorkProgress']['incident'])) {
                    $model->incident = $_POST['DailyWorkProgress']['incident'];
                }
                if (isset($_POST['DailyWorkProgress']['inspection'])) {
                    $model->inspection = $_POST['DailyWorkProgress']['inspection'];
                }
                if (isset($_POST['DailyWorkProgress']['visitor'])) {
                    $model->visitor_name = $_POST['DailyWorkProgress']['visitor'];
                }
                if (isset($_POST['DailyWorkProgress']['company'])) {
                    $model->company_name = $_POST['DailyWorkProgress']['company'];
                }
                if (isset($_POST['DailyWorkProgress']['designation'])) {
                    $model->designation = $_POST['DailyWorkProgress']['designation'];
                }
                if (isset($_POST['DailyWorkProgress']['purpose'])) {
                    $model->purpose = $_POST['DailyWorkProgress']['purpose'];
                }
                $qry2 = Tasks::model()->find(array("condition" => "tskid = '$taskid' AND project_id=" . $projectid));
                if ($qry2['assigned_to']) {
                    $assigned_to = $qry2['assigned_to'];
                } else {
                    $assigned_to = Yii::app()->user->id;
                }
                if ($qty > $tot_qty) {
                    Yii::app()->user->setFlash('error', "Please enter a less quantity, maximum is " . $max);
                    $this->render('index', array(
                        'model' => $model,
                        'itemdetails' => $itemdetails,
                        'interval' => 0,
                    )
                    );
                } else {
                    if ($model->save() && $model->validate()) {
                        Yii::app()->user->setState('wpr_id', $model->id);
                        $consumedItemModel = new WprItemUsed('search');

                        if (Yii::app()->user->wpr_id != "") {
                            $consumedItemModel->wpr_id = Yii::app()->user->wpr_id;
                        }
                        $images = CUploadedFile::getInstancesByName('image');
                        $images_array = array();
                        $i = 0;
                        $label = [];
                        if (isset($_POST['DailyWorkProgress']['image_label'])) {
                            $label = $_POST['DailyWorkProgress']['image_label'];
                        }
                        if (count($images) > 0) {
                            if ($id != "") {
                                Yii::app()->db->createCommand("DELETE FROM pms_wpr_images where wpr_id=" . $id)->execute();
                            }
                            $file_path = Yii::app()->basePath . "/../uploads/dailywork_progress";
                            foreach ($images as $image => $pic) {
                                $label_name = $label[$image];
                                $files = CFileHelper::findFiles($file_path);
                                $newfilename = rand(1000, 9999) . time();
                                $newfilename = md5($newfilename); //optional
                                $file_name = $newfilename . '.' . $pic->getExtensionName();
                                $file = $file_path . $file_name;
                                if (!file_exists($file))
                                    $pic->saveAs(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $file_name);
                                array_push($images_array, $file_name);

                                $create_sql = "INSERT INTO pms_wpr_images (wpr_id,image_path,image_label,image_status,created_by) VALUES (:id,:image_path,:image_label,:image_status,:created_by)";

                                $parameters = array(
                                    ":id" => $model->id,
                                    ":image_path" => $file_name,
                                    ":image_label" => $label_name,
                                    ":image_status" => 0,
                                    ":created_by" => Yii::app()->user->id
                                );

                                Yii::app()->db->createCommand($create_sql)->execute($parameters);


                                $i++;
                            }
                        }
                        $schedule_data = $this->CheckprogressPercentags($qry2, $model->date, $model->daily_work_progress, $taskid, 1); // 1 => for before approve				
                        $dependancy_data = $this->DependancyData($qry2, $model->date);
                        $final_result = $schedule_data . '<br>' . $dependancy_data;


                        $project_name = isset($model->project->name) ? $model->project->name : "";

                        $wpr_date = isset($model->date) ? date("d-M-y", strtotime($model->date)) : "";

                        $taskname = isset($model->tasks) ? $model->tasks->title : "";

                        $quantity = isset($model->qty) ? $model->qty : "";


                        $worktype = isset($model->worktype) ? $model->worktype->work_type : "";

                        $wpr_status = "";
                        if (!empty($model->current_status)) {
                            $wpr_status = isset($model->status0->caption) ? $model->status0->caption : "";
                        } else {
                            if (isset($model->taskid)) {
                                $wpr_status = $model->getStatus($model->taskid);
                            }
                        }

                        $progress = isset($model->daily_work_progress) ? round($model->daily_work_progress, 2) . " %" : "";
                        $totqty = 0;
                        if (isset($model->taskid)) {
                            $task = Tasks::model()->findByPk($model->taskid);
                            if ($task) {
                                $totqty = isset($task->quantity) ? $task->quantity : "";
                            }
                        }
                        $remarks = isset($model->description) ? $model->description : "";

                        $transaction->commit();

                        $return_result = array(
                            'status' => 1,
                            'message' => $final_result,
                            'wpr_model' => $model,
                            'wpr_id' => $model->id,
                            'project_name' => $project_name,
                            'wpr_date' => $wpr_date,
                            'taskname' => $taskname,
                            'quantity' => $quantity,
                            'worktype' => $worktype,
                            'wpr_status' => $wpr_status,
                            'progress' => $progress,
                            'totqty' => $totqty,
                            'remarks' => $remarks


                        );
                    } else {
                        throw new Exception("Failed to save data.");
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollback();
                $error = $e->getMessage();
                $return_result = array('status' => 0, 'message' => $error);
            }
            echo json_encode($return_result);
        } // issett($_POST['DailyWorkProgress'])
    }

    public function actionsaveConsumedItems()
    {
        $task_id = $_POST['task_id'];
        $item_id = array($_POST['item_id']);
        $rate_id = array($_POST['rate_id']);
        $item_quantity = array($_POST['item_quantity']);
        $wpr_id = $_POST['wpr_id'];
        $type = $_POST['type'];

        $task_details = Tasks::model()->findByPk($task_id);
        $project = Projects::model()->findByPK($task_details->project_id);
        $temp_id = $project->template_id;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $this->consumedWprItems($wpr_id, $task_id, $temp_id, $item_id, $item_quantity, $rate_id);

            if ($type == 1) {
                $return_result = array('status' => 1, 'message' => 'created succesfully...', 'next_stage' => 0);
            } else {
                $return_result = array('status' => 1, 'message' => 'created succesfully...', 'next_stage' => 1);
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            $error = $e->getMessage();
            $return_result = array('status' => 0, 'message' => $error);
        }

        echo json_encode($return_result);
    }

    public function actionfetchConsumedItem()
    {

        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = WprItemUsed::model()->findByPk($id);
            if (!empty($model)) {
                $model_array = array('stat' => 1, 'id' => $model->id, 'item_id' => $model->item_id, 'rate_id' => $model->item_rate_id, 'item_count' => $model->item_count);
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo json_encode($model_array);
        exit;
    }
    public function actionupdateConsumedItems()
    {
        $task_id = $_POST['task_id'];
        $item_id = array($_POST['item_id']);
        $rate_id = array($_POST['rate_id']);
        $item_quantity = array($_POST['item_quantity']);
        $wpr_id = $_POST['wpr_id'];
        $type = $_POST['type'];
        $task_details = Tasks::model()->findByPk($task_id);
        $project = Projects::model()->findByPK($task_details->project_id);
        $temp_id = $project->template_id;
        $item_already_exist = $_POST['item_already_exist'];
        $rate_already_exist = $_POST['rate_already_exist'];
        $transaction = Yii::app()->db->beginTransaction();
        try {



            $Criteria = new CDbCriteria();
            $Criteria->select = 'id';
            $Criteria->condition = 'find_in_set(' . $_POST['item_already_exist'] . ',purchase_item) and temp_id=' . $temp_id;
            $data = TemplateItems::model()->find($Criteria);



            $already_exist_item_criteria = new CDbCriteria();
            $already_exist_item_criteria->condition = 'item_id=' . $data->id . ' AND task_id=' . $task_id . ' AND template_id=' . $temp_id;
            $already_exist_item_det = TaskItemEstimation::model()->find($already_exist_item_criteria);

            if ($already_exist_item_det) {

                $count = $already_exist_item_det->item_quantity_used - $_POST['count_already_exist'];



                $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $task_id, 'item_id' => $data->id, 'template_id' => $temp_id));

                $update_item->attributes = array('item_quantity_used' => $count);

                // $update_item->save();

                if (!$update_item->save()) {
                    throw new Exception("Failed to update data.");
                }

                $delete_query = "delete from pms_wpr_item_used
            where wpr_id = '$wpr_id' and  task_id= '$task_id' and item_id='$item_already_exist' and item_rate_id='$rate_already_exist' ";
                Yii::app()->db->createCommand($delete_query)->execute();

                $delete_sql = "delete from pms_acc_wpr_item_consumed where wpr_id= '$wpr_id' and item_id= '$item_already_exist' ";

                Yii::app()->db->createCommand($delete_sql)->execute();

                $this->consumedWprItems($wpr_id, $task_id, $temp_id, $item_id, $item_quantity, $rate_id);

                if ($type == 1) {
                    $return_result = array('status' => 1, 'message' => 'updated succesfully...', 'next_stage' => 0);
                } else {
                    $return_result = array('status' => 1, 'message' => 'updated succesfully...', 'next_stage' => 1);
                }
            }


            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            $error = $e->getMessage();
            $return_result = array('status' => 0, 'message' => $error);
        }

        echo json_encode($return_result);
    }
    public function actiondeleteConsumedItem()
    {
        $item_id = $_POST['item_id'];
        $wpr_id = $_POST['wpr_id'];
        $id = $_POST['id'];
        $project_id = $_POST['project_id'];
        $task_id = $_POST['task_id'];
        $project = Projects::model()->findByPK($project_id);
        $temp_id = $project->template_id;
        $transaction = Yii::app()->db->beginTransaction();
        try {

            $item_consumed = WprItemUsed::model()->findByPK($id);



            $Criteria = new CDbCriteria();
            $Criteria->select = 'id';
            $Criteria->condition = 'find_in_set(' . $item_id . ',purchase_item) and temp_id=' . $temp_id;
            $data = TemplateItems::model()->find($Criteria);

            $already_exist_item_criteria = new CDbCriteria();
            $already_exist_item_criteria->condition = 'item_id=' . $data->id . ' AND task_id=' . $task_id . ' AND template_id=' . $temp_id;
            $already_exist_item_det = TaskItemEstimation::model()->find($already_exist_item_criteria);

            if ($already_exist_item_det) {

                $count = $already_exist_item_det->item_quantity_used - $item_consumed->item_count;

                $update_item = TaskItemEstimation::model()->findByAttributes(array('task_id' => $task_id, 'item_id' => $data->id, 'template_id' => $temp_id));

                $update_item->attributes = array('item_quantity_used' => $count);

                if (!$update_item->save()) {
                    throw new Exception("Failed to update data.");
                }

                $delete_query = "delete from pms_wpr_item_used
            where  id='$id' ";
                Yii::app()->db->createCommand($delete_query)->execute();

                $delete_sql = "delete from pms_acc_wpr_item_consumed where wpr_id= '$wpr_id' and item_id= '$item_id' ";

                Yii::app()->db->createCommand($delete_sql)->execute();

                $return_result = array('status' => 1, 'message' => 'deleted succesfully');
            } else {
                $return_result = array('status' => 0, 'message' => 'invalid item');
            }



            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            $error = $e->getMessage();
            $return_result = array('status' => 0, 'message' => $error);
        }
        echo json_encode($return_result);
    }

    public function actionsaveResource()
    {
        $wpr_id = $_POST['wpr_id'];
        $task_id = $_POST['task_id'];
        $resources_used = $_POST['resources_used'];
        $unit_id = $_POST['unit_id'];
        $resource_quantity = $_POST['resource_quantity'];
        $created_by = Yii::app()->user->id;
        $type = $_POST['type'];
        $resource_action_id = $_POST['resource_action_id'];
        if ($resource_action_id == "create") {
            $model = new WprResourceUsed;
            $model->created_by = $created_by;
            $model->created_date = date('Y-m-d');
            $message = "created succesfully...";
        } else {
            $resourceId = $_POST['resourceId'];
            // $model = new WprResourceUsed($resourceId);
            $model = WprResourceUsed::model()->findByPk($resourceId);
            $model->updated_date = date("y-m-d");
            $model->updated_by = $created_by;
            $message = "updated succesfully...";
        }
        $transaction = Yii::app()->db->beginTransaction();
        try {

            $model->wpr_id = $wpr_id;
            $model->task_id = $task_id;
            $model->resource_id = $resources_used;
            $model->resource_qty = $resource_quantity;
            $model->resource_unit = $unit_id;
            $resource_model = Resources::model()->findByPk($resources_used);
            $rate = $resource_model->resource_rate;
            $total_amount = $resource_quantity * $rate;
            $model->amount = $total_amount;

            if (!$model->save()) {

                throw new Exception("Failed to save data.");
            }
            if ($type == 1) {
                $return_result = array('status' => 1, 'message' => $message, 'next_stage' => 0);
            } else {
                $return_result = array('status' => 1, 'message' => 'added succesfully', 'next_stage' => 1);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            $error = $e->getMessage();
            $return_result = array('status' => 0, 'message' => $error);
        }

        echo json_encode($return_result);
    }
    public function actionfetchResource()
    {

        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = WprResourceUsed::model()->findByPk($id);
            if (!empty($model)) {
                $units_data = Unit::model()->findByPk($model->resource_unit);

                $model_array = array('stat' => 1, 'id' => $model->id, 'resource_id' => $model->resource_id, 'resource_qty' => $model->resource_qty, 'resource_unit' => $model->resource_unit, 'unit_name' => $units_data->unit_title);
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo json_encode($model_array);
    }
    public function actiondeleteResource()
    {
        $id = $_POST['id'];
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = WprResourceUsed::model()->findByPk($id);
            if (!$model->delete()) {
                throw new Exception("Failed to save data.");
            }
            $return_result = array('status' => 1, 'message' => 'deleted succesfully');
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            $error = $e->getMessage();
            $return_result = array('status' => 0, 'message' => $error);
        }
        echo json_encode($return_result);
    }

    public function actionwprUpdate($id)
    {
        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;
        $account_items = [];
        $resource_utilised = [];
        $total_wpr_qty = 0;
        $task_model = Tasks::model()->findByPk($model->taskid);
        $total_qty = $task_model->quantity;
        $interval = (isset($_GET['interval']) ? $_GET['interval'] : 0);
        $selected_status = (isset($_GET['type']) ? $_GET['type'] : 'All');
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
            $resource_utilised = Resources::model()->findAll(
                array("order" => "resource_name ASC")
            );
        }
        $sql = "select SUM(qty) as qty FROM {$tblpx}daily_work_progress "
            . "WHERE approve_status != 2 AND taskid='" . $model->taskid . "'";

        $achieved_qty = Yii::app()->db->createCommand($sql)->queryRow();

        $parent_task = $task_model->parent_tskid;

        if ($parent_task != "") {
            // sub task 

            $sql_query = "select * FROM {$tblpx}daily_work_progress "
                . "WHERE approve_status != 2 AND taskid IN ('" . $parent_task . "')";
            $data = Yii::app()->db->createCommand($sql_query)->queryAll();

            if ($data != null) {
                foreach ($data as $key => $val) {
                    foreach ($val as $key1 => $values) {
                        if ($key1 == 'qty') {
                            $total_wpr_qty += $values;
                        }
                    }
                }
            }
        }

        $get_sub_tasks = Tasks::model()->findAll(array('condition' => 'parent_tskid =' . $model->taskid));
        $sub_task_array = array();
        foreach ($get_sub_tasks as $sub_tasks) {
            $sub_task_array[] = $sub_tasks->tskid;
        }

        if (count($sub_task_array) > 0) {
            $task_ids = implode(',', $sub_task_array);
            $sql_query = "select * FROM {$tblpx}daily_work_progress "
                . "WHERE approve_status != 2 AND taskid IN ('" . $task_ids . "')";
            $data = Yii::app()->db->createCommand($sql_query)->queryAll();
            if ($data != null) {
                foreach ($data as $key => $val) {
                    foreach ($val as $key1 => $values) {
                        if ($key1 == 'qty') {
                            $total_wpr_qty += $values;
                        }
                    }
                }
            }
        }



        $balanceQuantity = $total_qty - ($achieved_qty['qty'] + $total_wpr_qty);

        if ($balanceQuantity <= 0) {
            $balanceQuantity = 0;
        }

        $this->performAjaxValidation($model);
        $this->render('update_daily_work_progress', array(
            'model' => $model,
            'id' => $id,
            'account_items' => $account_items,
            'resource_utilised' => $resource_utilised,
            'interval' => $interval,
            'selected_status' => $selected_status,
            'quantity' => $total_qty,
            'balanceQuantity' => $balanceQuantity
        )
        );
    }
    public function actionremoveSession()
    {
        if ($_POST['id'] != "") {
            Yii::app()->user->setState('wpr_id', null);
            $project_id = $_POST['project_id'];
            $return_result = array(
                'status' => 1,
                'message' => 'updated succesfully',
                'type_other' => Yii::app()->createUrl('DailyWorkProgress/wpr'),
                'type_three' => Yii::app()->createUrl('Reports/weeklyReport', array('project_id' => $project_id))
            );
            echo json_encode($return_result);
        }
    }
    public function actioncheckItemExist()
    {
        $wpr_id = $_POST['wpr_id'];

        $type = $_POST['type'];

        //$type=1->item; $type=2->resource

        if ($type == 1) {
            $item_id = $_POST['itm_id'];
            $Criteria = new CDbCriteria();
            $Criteria->select = 'id';
            $Criteria->condition = 'wpr_id=' . $wpr_id . ' and item_id=' . $item_id;
            $model = WprItemUsed::model()->find($Criteria);
        } else {
            $resource_id = $_POST['resources_used'];
            $Criteria = new CDbCriteria();
            $Criteria->select = 'id';
            $Criteria->condition = 'wpr_id=' . $wpr_id . ' and resource_id	=' . $resource_id;
            $model = WprResourceUsed::model()->find($Criteria);
        }


        if (count($model) > 0) {
            $return_result = array('status' => 1, 'message' => 'Item already exist');
        } else {
            $return_result = array('status' => 0, 'message' => '');
        }
        echo json_encode($return_result);
    }
    public function actiondailyProgress($type)
    {
        $from_date = '';
        $to_date = '';
        unset(Yii::app()->request->cookies['from_date']);  // first unset cookie for dates
        unset(Yii::app()->request->cookies['to_date']);
        $interval = (isset($_GET['interval']) ? $_GET['interval'] : 0);
        $selected_status = (isset($_GET['type']) ? $_GET['type'] : 'All');
        $taskid = (isset($_GET['taskid']) ? $_GET['taskid'] : '');
        $model = new DailyWorkProgress('search');
        $model->unsetAttributes();  // clear any default values
        if (!empty($_REQUEST)) {
            if (isset($_POST['from_date'])) {
                Yii::app()->request->cookies['from_date'] = new CHttpCookie('from_date', $_POST['from_date']); // define cookie for from_date
                $model->from_date = $_REQUEST['from_date'];
            }

            if (isset($_POST['to_date'])) {
                Yii::app()->request->cookies['to_date'] = new CHttpCookie('to_date', $_POST['to_date']);
                $model->to_date = $_REQUEST['to_date'];
            }
        }
        if (isset($_REQUEST['DailyWorkProgress'])) {
            $model->attributes = $_REQUEST['DailyWorkProgress'];
        }
        $account_items = [];
        $resource_utilised = [];
        if (Tasks::model()->accountPermission() == 1) {
            $account_items = $this->getAccountItems();
            $resource_utilised = Resources::model()->findAll(
                array("order" => "resource_name ASC")
            );
        }

        $this->performAjaxValidation($model);

        $this->render('daily_work_progress', array(
            'model' => $model,
            'interval' => $interval,
            'selected_status' => $selected_status,
            'account_items' => $account_items,
            'resource_utilised' => $resource_utilised,
            'task_total_quantity' => '',
            // 'task_id' =>$taskid,
        )
        );
    }
    public function actiondeleteWPR()
    {
        $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {

            $delete_query = "delete from pms_wpr_item_used
                where wpr_id = '$id' ";
            Yii::app()->db->createCommand($delete_query)->execute();

            $delete_sql = "delete from pms_acc_wpr_item_consumed where wpr_id=" . $id;
            Yii::app()->db->createCommand($delete_sql)->execute();

            $delete_image = "delete from pms_wpr_images where wpr_id=" . $id;

            Yii::app()->db->createCommand($delete_image)->execute();

            $delete_resource = "delete from pms_wpr_resource_used where wpr_id=" . $id;

            Yii::app()->db->createCommand($delete_resource)->execute();

            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }

            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {

            if ($success_status == 1) {

                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else if ($success_status == 2) {
                echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
            } else {

                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }
}
