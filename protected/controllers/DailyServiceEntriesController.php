<?php

class DailyServiceEntriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	 public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                

                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$controller];
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );

	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DailyServiceEntries;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DailyServiceEntries']))
		{
			$model->attributes=$_POST['DailyServiceEntries'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->entry_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DailyServiceEntries']))
		{
			$model->attributes=$_POST['DailyServiceEntries'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->entry_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id = 0)
                
	{
            
            if ($id == 0) {
            $model2 = new DailyServiceEntries;
           // $msg = 'Successfully updated.';
        } else {
           
            $model2 = $this->loadModel($id);
           // $msg = 'Successfully added new device.';
        }
      //  print_r($_POST['DailyServiceEntries']);die;
            if(isset($_POST['DailyServiceEntries']))
		{	
			$this->performAjaxValidation($model2);
			
			$model2->attributes=$_POST['DailyServiceEntries'];
			$model2->date= date('Y-m-d',strtotime($_POST['DailyServiceEntries']['date']));
			 $model2->created_by= Yii::app()->user->id;
			if($model2->save())
                        {
				$this->redirect(array('index'));
                        }
                       
		}
        
        
        
                $model=new DailyServiceEntries('search');
                
		$model->unsetAttributes();  // clear any default values
                //print_r($_GET);die;
		if(isset($_GET['DailyServiceEntries']))
			$model->attributes=$_GET['DailyServiceEntries'];

                
                
		
		$this->render('index',array(
			'model'=>$model,
                        'model2'=>$model2,
                    
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DailyServiceEntries('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DailyServiceEntries']))
			$model->attributes=$_GET['DailyServiceEntries'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DailyServiceEntries::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='daily-service-entries-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
