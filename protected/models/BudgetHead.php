<?php

/**
 * This is the model class for table "{{budget_head}}".
 *
 * The followings are the available columns in table '{{budget_head}}':
 * @property integer $budget_head_id
 * @property string $budget_head_title
 * @property integer $status
 * @property integer $project_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $ranking
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class BudgetHead extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BudgetHead the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{budget_head}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('budget_head_title, status, project_id, created_by, created_date, updated_by', 'required'),
            array('status, project_id, ranking, created_by, updated_by', 'numerical', 'integerOnly' => true),
            array('budget_head_title', 'length', 'max' => 100),
            array('start_date, end_date, updated_date', 'safe'),
            array('start_date,end_date', 'datevalidation'),
            array('ranking', 'check_exist', 'on' => 'create'),

            array('budget_head_title', 'check_name_exist', 'on' => 'create',),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('budget_head_id, budget_head_title, status, project_id, start_date, end_date, ranking, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'budget_head_id' => 'Budget Head',
            'budget_head_title' => 'Budget Head Title',
            'status' => 'Status',
            'project_id' => 'Project',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'ranking' => 'Ranking',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('budget_head_id', $this->budget_head_id);
        $criteria->compare('budget_head_title', $this->budget_head_title, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('project_id', $this->project_id);
        // $criteria->compare('start_date',$this->start_date,true);
        // $criteria->compare('end_date',$this->end_date,true);
        // $criteria->compare('ranking',$this->ranking);
        // $criteria->compare('created_by',$this->created_by);
        // $criteria->compare('created_date',$this->created_date,true);
        // $criteria->compare('updated_by',$this->updated_by);
        // $criteria->compare('updated_date',$this->updated_date,true);
        if ($this->project_id) {

            $criteria->compare('project_id', $this->project_id);
        } else {

            $criteria->compare('project_id', 0);
        }



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function datevalidation($attribute, $params)
    {
        if ($this->start_date != "" && $this->end_date != "") {
            if (strtotime($this->start_date) > strtotime($this->end_date)) {
                $this->addError('start_date', "start date should be less than end date");
            }
            if (strtotime($this->end_date) < strtotime($this->start_date)) {
                $this->addError('start_date', "end date should be greater than start date");
            }
        }
        if ($this->project_id != "") {
            $sql = "SELECT `start_date`,`end_date` FROM pms_projects WHERE pid=" . intval($this->project_id);
            $dates = Yii::app()->db->createCommand($sql)->queryRow();
            if ($this->start_date != "") {

                if (strtotime($this->start_date) < strtotime($dates['start_date'])) {
                    $this->addError('start_date', 'Project start date is ' . date('d-M-y', strtotime($dates['start_date'])));
                }
            }

            if ($this->end_date != "") {
                if (!empty($dates['end_date'])) {
                    if (strtotime($this->end_date) > strtotime($dates['end_date'])) {
                        $this->addError('end_date', 'Project end date is ' . date('d-M-y', strtotime($dates['end_date'])));
                    }
                } else {
                    $this->addError('end_date', 'Project End date missing');
                }
            }

            if ($this->budget_head_id != "") {
               
                $milestones = Milestone::model()->findAll(['condition' => 'budget_id = ' . $this->budget_head_id]);

                $milestone_start_date = "";
                $milestone_end_date = "";

                foreach($milestones as $milestone){

                    $milestone_start_date = strtotime($milestone->start_date);
                    $milestone_end_date = strtotime($milestone->end_date);

                    if ($this->start_date != "" && !empty($milestone_start_date)) {
                        if (strtotime($this->start_date) > $milestone_start_date) {
                            $this->addError('start_date', 'milestone start date is ' . date('d-M-y', $milestone_start_date));
                        }
                    }

                    if ($this->end_date != "" && !empty($milestone_end_date)) {
                        if (strtotime($this->end_date) < $milestone_end_date) {
                            $this->addError('end_date', 'milestone end date is ' . date('d-M-y', $milestone_end_date));
                        }
                    }

                }
              
            }
            
        }
    }

    public function check_exist($attribute, $params)
    {

        if ($this->$attribute != '') {

            $criteria = new CDbCriteria();
            $criteria->compare('project_id', $this->project_id);
            $criteria->compare('ranking', $this->ranking);
            $exist_data = BudgetHead::model()->find($criteria);
            if (!empty($exist_data['budget_head_id'])) {
                $this->addError($attribute, 'Ranking already exist');
            }
        }
    }



    public function check_name_exist($attribute, $params)
    {

        if ($this->$attribute != '') {
            $name = trim($this->budget_head_title);

            $q = new CDbCriteria();
            $q->compare('project_id', $this->project_id);
            $q->compare('budget_head_title', $name);
            $exist_data = BudgetHead::model()->find($q);
            if (!empty($exist_data['budget_head_id'])) {
                $this->addError($attribute, 'Name already exist');
            }
        }
    }
}
