<?php

/**
 * This is the model class for table "{{punch_settings}}".
 *
 * The followings are the available columns in table '{{punch_settings}}':
 * @property integer $setting_id
 * @property string $absent
 * @property string $halfday
 * @property string $late_halfday
 * @property string $late_fullday
 * @property double $minimum_in_time_half_day
 * @property double $minimum_in_time_full_day
 */
class PunchSettings extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PunchSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{punch_settings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('minimum_in_time_half_day, minimum_in_time_full_day,halfday', 'required'),
			array('minimum_in_time_half_day, minimum_in_time_full_day', 'numerical'),
			array('late_halfday, late_fullday', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('setting_id, absent, halfday, late_halfday, late_fullday, minimum_in_time_half_day, minimum_in_time_full_day', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'setting_id' => 'Setting',
			'absent' => 'Punch Before Avoid Half Day ',
			'halfday' => 'Halfday',
			'late_halfday' => 'Late Halfday',
			'late_fullday' => 'Late Fullday',
			'minimum_in_time_half_day' => 'Minimum In Time For Half Day',
			'minimum_in_time_full_day' => 'Minimum In Time For Full Day',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('setting_id',$this->setting_id);
		$criteria->compare('absent',$this->absent,true);
		$criteria->compare('halfday',$this->halfday,true);
		$criteria->compare('late_halfday',$this->late_halfday,true);
		$criteria->compare('late_fullday',$this->late_fullday,true);
		$criteria->compare('minimum_in_time_half_day',$this->minimum_in_time_half_day);
		$criteria->compare('minimum_in_time_full_day',$this->minimum_in_time_full_day);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}