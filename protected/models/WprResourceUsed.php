<?php

/**
 * This is the model class for table "{{wpr_resource_used}}".
 *
 * The followings are the available columns in table '{{wpr_resource_used}}':
 * @property integer $id
 * @property integer $wpr_id
 * @property integer $task_id
 * @property integer $resource_id
 * @property integer $resource_qty
 * @property integer $resource_unit
 * @property double $amount
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Resources $resource
 * @property DailyWorkProgress $wpr
 * @property Unit $resourceUnit
 * @property Tasks $task
 */
class WprResourceUsed extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WprResourceUsed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wpr_resource_used}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wpr_id, task_id, resource_id, resource_qty, resource_unit, amount, created_date, created_by', 'required'),
			array('wpr_id, task_id, resource_id, resource_qty, resource_unit, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, wpr_id, task_id, resource_id, resource_qty, resource_unit, amount, created_date, created_by, updated_date, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'resource' => array(self::BELONGS_TO, 'Resources', 'resource_id'),
			'wpr' => array(self::BELONGS_TO, 'DailyWorkProgress', 'wpr_id'),
			'resourceUnit' => array(self::BELONGS_TO, 'Unit', 'resource_unit'),
			'task' => array(self::BELONGS_TO, 'Tasks', 'task_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'wpr_id' => 'Wpr',
			'task_id' => 'Task',
			'resource_id' => 'Resource',
			'resource_qty' => 'Resource Quantity',
			'resource_unit' => 'Resource Unit',
			'amount' => 'Amount',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('wpr_id',$this->wpr_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('resource_id',$this->resource_id);
		$criteria->compare('resource_qty',$this->resource_qty);
		$criteria->compare('resource_unit',$this->resource_unit);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		if ($this->wpr_id) {

            $criteria->compare('wpr_id', $this->wpr_id);
         }
		else {

            $criteria->compare('wpr_id', 0);
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getResourceName($id)
	{
		$resource_model = Resources::model()->findByPk($id);
		
		return $resource_model->resource_name;
	}
	public function getResourceUnit($id)
	{
	 $units_data = Unit::model()->findByPk($id);
	 return  $units_data->unit_title;
	}
}