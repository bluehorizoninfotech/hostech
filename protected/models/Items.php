<?php

/**
 * This is the model class for table "{{items}}".
 *
 * The followings are the available columns in table '{{items}}':
 * @property integer $id
 * @property string $sl_no
 * @property integer $projectid
 * @property integer $taskid
 * @property string $description
 * @property string $qty
 * @property string $unit
 * @property integer $rate
 * @property string $amount
 * @property string $parentid
 *
 * The followings are the available model relations:
 * @property Projects $project
 */
class Items extends CActiveRecord
{
	 public $taskid;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sl_no', 'required'),
			//array('sl_no', 'unique'),
			array('projectid, rate', 'numerical'),
                        array('amount', 'filter', 'filter'=>array($this, 'customtrim')),
			array('sl_no', 'length', 'max'=>50),
			array('description, qty, unit,rate', 'required', 'on' => 'update','message' => 'Please Enter {attribute}'),
			array('description', 'length', 'max'=>250),
			array('qty, unit, amount', 'length', 'max'=>20),
			array('parentid,sl_no,taskid', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sl_no, projectid,taskid, description, qty, unit, rate, amount, parentid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sl_no' => 'S.No',
			'projectid' => 'Projectid',
			'taskid'  => 'Taskid',
			'description' => 'Description',
			'qty' => 'Quantity',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'amount' => 'Amount',
			'parentid' => 'Parent',
		);
	}
        
        public function customtrim($item){
           return str_replace(",","",$item);
        }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($projectid)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->addCondition("projectid ='$projectid'");
		
		$criteria->compare('id',$this->id);
		$criteria->compare('sl_no',$this->sl_no,true);
		$criteria->compare('projectid',$this->projectid);
		$criteria->compare('taskid',$this->taskid);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('qty',$this->qty,true);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('parentid',$this->parentid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,  
		));
	}
}