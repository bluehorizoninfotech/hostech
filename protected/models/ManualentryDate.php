<?php

/**
 * This is the model class for table "{{manualentry_date}}".
 *
 * The followings are the available columns in table '{{manualentry_date}}':
 * @property integer $id
 * @property integer $site_id
 * @property string $date
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Clientsite $site
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class ManualentryDate extends CActiveRecord
{
	
	public $fromdate;
	public $todate;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ManualentryDate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{manualentry_date}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date,site_id', 'required'),
			array('site_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
                        array('updated_by,created_by', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, site_id, date,fromdate,todate, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'site' => array(self::BELONGS_TO, 'Clientsite', 'site_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'site_id' => 'Site',
			'date' => 'Date',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('site_id',$this->site_id);
		
		
		if(!empty($this->fromdate) && empty($this->todate))
		{
			//$criteria->addCondition("date >= '".date('Y-m-d',strtotime($this->fromdate))."'"); 
			$criteria->addCondition("date >= '".date('Y-m-d',strtotime($this->fromdate))."'");  // date is database date column field
		}elseif(!empty($this->todate) && empty($this->fromdate))
		{
			$criteria->addCondition("date <= '".date('Y-m-d',strtotime($this->todate))."'");
		}elseif(!empty($this->todate) && !empty($this->fromdate))
		{
			$criteria->addCondition("date between '".date('Y-m-d',strtotime($this->fromdate))."' and  '".date('Y-m-d',strtotime($this->todate))."'");
		}
		
		
		//$criteria->compare('date',$this->date,true);
                if (Yii::app()->user->role > 1) {
                $criteria->condition = 't.site_id IN (select site_id from pms_clientsite_assigned where user_id ='.Yii::app()->user->id.' )';     
                }
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			 'sort'=>array(
				'defaultOrder'=>'id DESC',
			),
		));
	}
}
