<?php

/**
 * This is the model class for table "{{projects}}".
 *
 * The followings are the available columns in table '{{projects}}':
 * @property integer $pid
 * @property integer $client_id
 * @property string $name
 * @property integer $billable
 * @property integer $status
 * @property double $budget
 * @property string $start_date
 * @property integer $end_date
 * @property string $description
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Clients $client
 * @property Status $status0
 * @property Tasks[] $tasks
 */
class Projects extends CActiveRecord
{

    public $billable = 3; //Default radio button selection 
    public $status = 1;  //default radio button selection on create.
    public $analyser;
    // public $site_name;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Projects the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{projects}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, status,  created_date, created_by, updated_date, updated_by, start_date,assigned_to', 'required'),
            array('client_id,billable, status, created_by, updated_by', 'numerical', 'integerOnly' => true),
            array('name', 'unique'),
            array('project_no', 'unique'),
            array('budget', 'numerical'),
            array('total_square_feet', 'numerical'),
            array('name', 'length', 'max' => 100),
            array('description,analyser,total_square_feet', 'safe'),
            array('start_date,end_date', 'datevalidation'),
            array('end_date', 'updatedatevalidation'),

            array('name', 'checkname','on' => 'create',),
            array('project_no', 'checkproject_no','on' => 'insert',),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('pid, name,analyser, billable, status, budget,project_no,  description, created_date, created_by, updated_date, updated_by,client_id, start_date, end_date, assigned_to, project_no', 'safe', 'on' => 'search'),
        );
    }
    public function checkname($attribute, $params)
    {
        $name=trim($this->name);
        
        $project=Projects::model()->findByAttributes(array('name'=>$name));
        if($project)
        {
            $this->addError('name', 'Project name "' .$name.'"  has already been taken.');
        }
    }
    public function checkproject_no($attribute, $params)
    {
        
        $project_no=trim($this->project_no);

        $q = new CDbCriteria();
        $q->addSearchCondition('project_no', $project_no);

        $project = Projects::model()->findAll( $q );
        if($project)
        {
            
            $this->addError('project_no', 'Project No "' .$project_no.'"  has already been taken.');
        }
    }
    public function datevalidation($attribute, $params)
    {


        if ($this->start_date != "") {
            if ($this->end_date != "") {
                if (strtotime($this->start_date) > strtotime($this->end_date)) {
                    $this->addError('start_date', 'Start date should not exceed  end date');
                } elseif (strtotime($this->end_date) < strtotime($this->start_date)) {
                    $this->addError('end_date', 'End date should not preceed start date');
                }
            }
        } else {
            $this->addError('start_date', 'Enter' . $attribute);
        }
        // if($this->end_date != ""){
        // 	if(strtotime($this->end_date) < strtotime($this->start_date)){
        // 		$this->addError('end_date', 'End date should not preceed start date');
        // 	}
        // }else{
        // 	$this->addError('end_date', 'Enter'.$attribute);
        // }

    }
    public function updatedatevalidation($attribute, $params)
    {
        if ($this->pid != "") {
            if ($this->start_date != "") {
                if ($this->end_date != "") {
                    $tblpx = Yii::app()->db->tablePrefix;
                    $startdate = date('Y-m-d', strtotime($this->start_date));
                    $enddate = date('Y-m-d', strtotime($this->end_date));
                    $project_id = $this->pid;
                    if ($enddate) {
                        $sel_budget_head = "SELECT MAX(end_date) as max_date FROM `{$tblpx}budget_head`  WHERE  `project_id`='" . $project_id . "'";
                        $budget_head_exist = Yii::app()->db->createCommand($sel_budget_head)->queryRow();
                        if ($budget_head_exist['max_date'] && $enddate < $budget_head_exist['max_date']) {
                            $this->addError('end_date', 'Budget head end date is ' . date('d-M-Y', strtotime($budget_head_exist['max_date'])));
                        }
                        $sel_milestone = "SELECT MAX(end_date) as max_date FROM `{$tblpx}milestone`  WHERE  `project_id`='" . $project_id . "'";
                        $mile_stone_exist = Yii::app()->db->createCommand($sel_milestone)->queryRow();
                        if ($mile_stone_exist['max_date'] && $enddate < $mile_stone_exist['max_date']) {
                            $this->addError('end_date', 'Milestone end date is ' . date('d-M-Y', strtotime($mile_stone_exist['max_date'])));
                        }
                        $sel_tasks = "SELECT MAX(due_date) as max_date FROM `{$tblpx}tasks`  WHERE  `project_id`='" . $project_id . "'";
                        $tasks_exist = Yii::app()->db->createCommand($sel_tasks)->queryRow();
                        if ($tasks_exist['max_date'] && $enddate < $tasks_exist['max_date']) {
                            $this->addError('end_date', 'Task  end date is ' . date('d-M-Y', strtotime($tasks_exist['max_date'])));
                        }
                    }
                    if ($startdate) {
                        $sel_budget_head = "SELECT MIN(start_date) as min_start_date FROM `{$tblpx}budget_head`  WHERE `project_id`='" . $project_id . "'";
                        $budget_head_exist = Yii::app()->db->createCommand($sel_budget_head)->queryRow();
                        if ($budget_head_exist['min_start_date'] && $startdate > $budget_head_exist['min_start_date']) {
                            $this->addError('start_date', 'Budget head start date is ' . date('d-M-Y', strtotime($budget_head_exist['min_start_date'])));
                        }
                        $sel_milestone = "SELECT MIN(start_date) as min_start_date FROM `{$tblpx}milestone`  WHERE `project_id`='" . $project_id . "'";
                        $mile_stone_exist = Yii::app()->db->createCommand($sel_milestone)->queryRow();
                        if ($mile_stone_exist['min_start_date'] && $startdate > $mile_stone_exist['min_start_date']) {
                            $this->addError('start_date', 'milestone start date is ' . date('d-M-Y', strtotime($mile_stone_exist['min_start_date'])));
                        }
                        $sel_tasks = "SELECT MIN(start_date) as min_start_date FROM `{$tblpx}tasks`  WHERE `project_id`='" . $project_id . "'";
                        $tasks_exist = Yii::app()->db->createCommand($sel_tasks)->queryRow();
                        if ($tasks_exist['min_start_date'] && $startdate > $tasks_exist['min_start_date']) {
                            $this->addError('start_date', 'Task start date is ' . date('d-M-Y', strtotime($tasks_exist['min_start_date'])));
                        }
                    }
                }
            }
        }
    }



    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'billable0' => array(self::BELONGS_TO, 'Status', 'billable'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            'client' => array(self::BELONGS_TO, 'Clients', 'client_id'),
            'status0' => array(self::BELONGS_TO, 'Status', 'status'),
            'tasks' => array(self::HAS_MANY, 'Tasks', 'project_id'),
            'assignedTo' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
            // 'Clientsite0' => array(self::BELONGS_TO, 'Clientsite', 'site_name'),    
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'pid' => 'Pid',
            'client_id' => 'Client',
            'name' => 'Project name',
            'analyser' => 'Analyser',
            'billable' => 'Billable',
            'status' => 'Status',
            'budget' => 'Budget',
            'description' => 'Description',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'assigned_to' => 'Assigned Project Managers',
            'project_no' => 'Project ID',
            'img_path' => 'Image',
            'report_image' => 'Report Image',
            'total_square_feet' => 'Total Square Feet',

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('pid', $this->pid);

        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('status', $this->status);
        $criteria->compare('budget', $this->budget);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('project_no', $this->project_no);
        $criteria->compare('start_date',$this->start_date);
        $criteria->compare('end_date',$this->end_date);
        
        // $criteria->compare('img_path', $this->img_path);
        if (Yii::app()->user->role != 1) {
            $criteria_proj = new CDbCriteria;
            $criteria_proj->select = 'project_id';
            $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
            $criteria_proj->group = 'project_id';
            $project_ids = Tasks::model()->findAll($criteria_proj);
            $project_id_array = array();
            foreach ($project_ids as $projid) {
                array_push($project_id_array, $projid['project_id']);
            }
            if (!empty($project_id_array)) {
                $project_id_array = implode(',', $project_id_array);
                $proj_condition = "OR pid IN (" . $project_id_array . ") AND status =1";
                $criteria->addCondition('pid IN (' . $project_id_array . ')', 'OR');
            }
        }
        
        if (Yii::app()->user->role == 1) {

           // $criteria->compare('assigned_to', $this->assigned_to, true);
            // $criteria->compare('assignedTo.first_name', $this->assignedTo->first_name, true);

            if (!empty($this->assigned_to)) {
                $criteria->addCondition('find_in_set(' . $this->assigned_to . ',assigned_to)');
            }
            

        } else {
            if (!empty($this->assigned_to)) {
                $criteria->addCondition('find_in_set(' . $this->assigned_to . ',assigned_to)');
            } else {
                $criteria->addCondition('find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'OR');
                // $criteria->addCondition('assigned_to = "'.Yii::app()->user->id.'"','OR');
                $criteria->addCondition('created_by = "' . Yii::app()->user->id . '"', 'OR');
            }
        }

       
        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'pid desc',
            ),
        ));
    }

    public function checkdelete($id)
    {

        $tbl = Yii::app()->db->tablePrefix;
        $get_tasks = Yii::app()->db->createCommand()
            ->select('*')
            ->from("{$tbl}tasks")
            ->where(
                'project_id=:id',
                array(':id' => $id)
            )
            ->queryAll();

        $get_sites = Yii::app()->db->createCommand()
            ->select('*')
            ->from("{$tbl}clientsite")
            ->where(
                'pid=:id',
                array(':id' => $id)
            )
            ->queryAll();

        if (count($get_tasks) == 0 && count($get_sites) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getclientsite($pid)
    {
        if (!empty($pid)) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('pid = ' . $pid);
            $site_models = Clientsite::model()->findAll($criteria);
            // echo '<pre>' ;print_r($site_models);exit;
            $result_array = array();
            foreach ($site_models as $site) {
                if (!empty($site)) {
                    $clients_in_site = ClientsiteAssign::model()->findAll(array('condition' => 'site_id = ' . $site->id));
                    if (!empty($clients_in_site)) {
                        $client_array = array();
                        $users_array = array();
                        foreach ($clients_in_site as $client) {
                            array_push($client_array, $client->users->first_name);
                            array_push($users_array, $client->users->first_name);
                        }
                        $result_array[$site->site_name] = $client_array;
                    }
                }
            }
            $disp_data  = '';
            if (isset($users_array)) {
                if (count($users_array) > 2) {
                    $disp_data  = $users_array[0] . ',' . $users_array[1] . '........';
                } elseif (count($users_array) != 0) {
                    $disp_data  = $users_array[0];
                }
            }

            $data = '<div class = \'clearfix\' style=\'white-space:nowrap\'>';
            foreach ($result_array as $key => $value) {
                $data .= '<div style= \'float:left;margin:15px 15px 0px 0px;\' ><span style=\'text-decoration:underline;margin-bottom:5px\'>' . $key . '</span><ul>';
                foreach ($value as $val) {
                    $data .= '<li style=\'list-style:none\'>' . $val . '</li>';
                }
                $data .= '</ul></div>';
            }
            $data .= '</div>';
            $final = '<span style="display:block;width:auto;" rel="tooltip" data-original-title="' . $data . '" data-placement="right">' . $disp_data . '</span>';
            return $final;
        }
    }
    public function getassignees($assignees, $model)
    {
        $data = "";
        if (!empty($assignees)) {
            $assignee_array = explode(',', $assignees);
            $result_data = array();
            // echo '<pre>';print_r($model);exit;
            foreach ($assignee_array as $assign) {
                $user_model = Users::model()->findByPk($assign);
                if (!empty($user_model)) {
                    array_push($result_data, $user_model->first_name);
                }
            }
            $data = implode(',', $result_data);
            return $data;
        }
        return $data;
    }
    public function project_mapping()
    { {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria->join = "LEFT OUTER JOIN pms_acc_project_mapping d ON (d.pms_project_id=t.pid)";

            $criteria->addCondition('d.pms_project_id IS NULL');
            $criteria->compare('client_id', $this->client_id);
            $criteria->compare('name', $this->name, true);
            $criteria->compare('project_no', $this->project_no);

            if (Yii::app()->user->role != 1) {
                $criteria_proj = new CDbCriteria;
                $criteria_proj->select = 'project_id';
                $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria_proj->group = 'project_id';
                $project_ids = Tasks::model()->findAll($criteria_proj);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                }
                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $proj_condition = "OR pid IN (" . $project_id_array . ") AND status =1";
                    $criteria->addCondition('pid IN (' . $project_id_array . ')', 'OR');
                }
            }

            if (Yii::app()->user->role == 1) {

                $criteria->compare('assigned_to', $this->assigned_to, true);
            } else {
                if (!empty($this->assigned_to)) {
                    $criteria->compare('assigned_to', $this->assigned_to, true);
                } else {
                    $criteria->addCondition('find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'OR');

                    $criteria->addCondition('t.created_by = "' . Yii::app()->user->id . '"', 'OR');
                }
            }


            return new CActiveDataProvider($this, array(
                'pagination' => array('pageSize' => 25),
                'criteria' => $criteria, 'sort' => array(
                    'defaultOrder' => 'pid desc',
                ),
            ));
        }
    }

    public function getAcoountProject()
    {
        $sql = "SELECT* FROM `jp_projects` `t` LEFT OUTER JOIN pms_acc_project_mapping pc ON (pc.acc_project_id	=t.pid) WHERE pc.acc_project_id IS NULL";
        $project_id = '';
        $options = array();
        foreach (Yii::app()->db->createCommand($sql)->queryAll() as  $key => $row) {
            $options[$row['pid']] = $row['name'];
        }

        if(count($options)>0)
        {
            return CHtml::dropDownList("acc_project_{$key}", $project_id, $options, array('empty' => '', 'style' => 'width:200px', 'class' => "acc_project acc_project_validation"));
        }
      
    }
}
