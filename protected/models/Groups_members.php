<?php

/**
 * This is the model class for table "{{groups_members}}".
 *
 * The followings are the available columns in table '{{groups_members}}':
 * @property integer $id
 * @property integer $group_id
 * @property integer $group_members
 * @property string $created_date
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property  */
class Groups_members extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Groups_members the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{groups_members}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id, group_members, created_by', 'numerical', 'integerOnly'=>true),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, group_id, group_members, created_date, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'group' => array(self::BELONGS_TO, 'Groups', 'group_id'),
			'users' => array(self::HAS_MANY, 'Users', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'group_members' => 'Group Members',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('group_members',$this->group_members);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function grp_memberlistsearch() {
         // echo 'test';
          
        $gpid = $_SESSION["gpid"];
        $gpmemebr = array();
        $gplead = Groups::model()->find(array(
                    'select' => 'group_lead',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpld=$gplead['group_lead'];
        $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpmemebr[]     =   $gpld;
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }  
        
        $criteria = new CDbCriteria;
        
        $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('designation', $this->designation, true);
        $criteria->addCondition('status = 0');
        $criteria->addCondition('user_type > 1');
        $criteria->addInCondition('userid',$gpmemebr);

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 10),
                                                     'sort'=>array(
    'defaultOrder'=>'user_type ASC',
  ),
                    'criteria' => $criteria,
                ));
    }
}
