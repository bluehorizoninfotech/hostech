<?php

/**
 * This is the model class for table "{{task_dependancy}}".
 *
 * The followings are the available columns in table '{{task_dependancy}}':
 * @property integer $id
 * @property integer $task_id
 * @property integer $dependant_task_id
 * @property double $dependency_percentage
 */
class TaskDependancy extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TaskDependancy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{task_dependancy}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('task_id, dependant_task_id', 'numerical', 'integerOnly'=>true),
			array('dependency_percentage', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, task_id, dependant_task_id, dependency_percentage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'task' => array(self::BELONGS_TO, 'Tasks', 'task_id'),
            'dependantTask' => array(self::BELONGS_TO, 'Tasks', 'dependant_task_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'task_id' => 'Task',
			'dependant_task_id' => 'Dependant Task',
			'dependency_percentage' => 'Dependency Percentage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('dependant_task_id',$this->dependant_task_id);
		$criteria->compare('dependency_percentage',$this->dependency_percentage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}