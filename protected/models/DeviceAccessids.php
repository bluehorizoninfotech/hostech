<?php


/**
 * This is the model class for table "{{device_accessids}}".
 *
 * The followings are the available columns in table '{{device_accessids}}':
 * @property integer $id
 * @property integer $userid
 * @property integer $deviceid
 * @property integer $accesscard_id
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class DeviceAccessids extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DeviceAccessids the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{device_accessids}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deviceid', 'required', 'message' => 'Please Choose {attribute}.'),
			array('accesscard_id', 'required', 'message' => 'Please Enter {attribute}.'),
			array('userid, deviceid, accesscard_id', 'numerical', 'integerOnly'=>true),
			array('accesscard_id', 'checkexists'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userid, deviceid, accesscard_id', 'safe', 'on'=>'search'),
		);
	}
	
	
	public function checkexists($attribute, $params)
        {
			 $exist = DeviceAccessids::model()->findAll(array('condition' => 'deviceid = "' .$this->deviceid. '" AND accesscard_id = "' .$this->accesscard_id. '" '));	
             
             
                        if ($exist != null) {
				  $this->addError('accesscard_id', 'Already Exists');
			 }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			// 'devices' => array(self::BELONGS_TO, 'PunchingDevices', 'deviceid'),
            'devices0' => array(self::BELONGS_TO, 'PunchingDevices', 'deviceid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userid' => 'Userid',
			'deviceid' => 'Device',
			'accesscard_id' => 'Accesscard Id',
		);
	}



	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($userid)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('deviceid',$this->deviceid);
		$criteria->compare('accesscard_id',$this->accesscard_id);
		$criteria->addCondition('userid='.$userid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function devicename($deviceid){
            
            $textvalue= PunchingDevices::model()->findByAttributes(array('device_id' => $deviceid));
            return isset($textvalue) ? $textvalue->device_name : "";
        }
}
