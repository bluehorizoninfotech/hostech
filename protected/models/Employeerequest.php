<?php

/**
 * This is the model class for table "{{employeerequest}}".
 *
 * The followings are the available columns in table '{{employeerequest}}':
 * @property integer $id
 * @property string $title
 * @property string $request_date
 * @property string $message
 * @property integer $mail_send_status
 * @property integer $acknoledge_status
 * @property integer $created_by
 * @property string $created_date
 */
class Employeerequest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Employeerequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{employeerequest}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, request_date, message,ack_message', 'required'),
			array('mail_send_status, acknoledge_status, created_by,ack_by', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>50),
			array('created_date,ack_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, request_date, message, mail_send_status, acknoledge_status, created_by, created_date,ack_message, ack_by,ack_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                     'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
                     'ackBy' => array(self::BELONGS_TO, 'Users', 'ack_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'request_date' => 'Request Date',
			'message' => 'Message',
			'mail_send_status' => 'Mail Status',
			'acknoledge_status' => 'Approve/Reject Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
                        'ack_message' => 'Approved/Rejected Message',
                        'ack_by' => 'Approved/Rejected By',
                        'ack_date' => 'Approved/Rejected Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('id',$this->id);
                
		$criteria->compare('title',$this->title,true);
		$criteria->compare('request_date',$this->request_date,true);
		$criteria->compare('message',$this->message,true);
		
		$criteria->compare('acknoledge_status',$this->acknoledge_status);
                
                if(Yii::app()->user->role != 1 && Yii::app()->user->role != '9' ){
		$criteria->compare('created_by',Yii::app()->user->id);
                }
		$criteria->compare('created_date',$this->created_date,true);

                $uid = Yii::app()->user->id;
                $ack_status = '';
                if($this->checkAuthstatus($uid,$ack_status)){                    
                $criteria->addCondition('mail_send_status = 1');    
                }else{
                $criteria->compare('mail_send_status',$this->mail_send_status);    
                }
                
                $criteria->compare('ack_message',$this->ack_message,true);
                $criteria->compare('ack_by',$this->ack_by);
                $criteria->compare('ack_date',$this->ack_date,true);
                
		return new CActiveDataProvider($this, array(
                'sort'=>array(
                'defaultOrder'=>'id DESC',
                ),
		'criteria'=>$criteria,                                            
		));
	}
        public function checkMailstatus($status){
            if(empty($status)){
                return "Mail Not Sent";
            }else{
                return "Mail Sent";
            }
        }                
        public function checkAckstatus($status){
            if(empty($status)){
                return "Mail Not Sent ";
            }else if($status == 1){
                return "Waiting for approval";
            }else if($status == 2){
                return "Approved";
            }else{
                return "Rejected"; 
            }
        }
         public function checkAuthstatus($uid,$ack_status){
            $model = MailSettings::model()->find(array("select" => array("status"),'condition' => "user_id = $uid AND status = 1"));            
            //$model2 = Events::model()->find(array("select" => array("clientid,sales_manager"), "condition" => "event_id = $id"));           
            if(is_null($model)){               
            if(Yii::app()->user->role == '1' || Yii::app()->user->role == '9'){                                      
            $model2 = Employeerequest::model()->find(array("select" => array("id"),'condition'=>'acknoledge_status IN (3,2,1)'));            
            if(is_null($model2)){                 
            return false;    
            }else{                  
            if($ack_status != 0){
            if($ack_status == 1){
            return true;        
            }else{
            return false;        
            }            
            }else{                          
            return true;    
            }
            }    
            //return true;    
            }else{    
            return false;
            }
            }else{
            $model3 = Employeerequest::model()->find(array("select" => array("*"),'condition'=>'acknoledge_status IS NULL'));
            if(is_null($model3)){                
            if(Yii::app()->user->role == '1' || Yii::app()->user->role == '9'){                                      
            $model2 = Employeerequest::model()->find(array("select" => array("id"),'condition'=>'acknoledge_status IN (3,2,1)'));            
            if(is_null($model2)){                 
            return false;    
            }else{                  
            if($ack_status != 0){
            if($ack_status == 1){
            return true;        
            }else{
            return false;        
            }            
            }else{                          
            return true;    
            }
            }    
            //return true;    
            }else{    
            return false;
            }
            }else{    
            return false;    
            }
            }
            
        }
        public function checkUserstatus($uid,$reqid){
        $model = Employeerequest::model()->find(array("select" => array("*"),'condition' => "id = $reqid AND created_by = $uid"));    
        if(is_null($model)){
        return false;    
        }else{
        return true;    
        }
        }
        public static function getAckstatus()
        {
        if(Yii::app()->user->role == '1' || Yii::app()->user->role == '9'){
        $arr = array(                       
            array('id'=>'1', 'title'=>'Waiting for approval'),
            array('id'=>'2', 'title'=>'Approved'),
            array('id'=>'3', 'title'=>'Rejected'),
        );        
        }else{
            $arr = array(           
            array('id'=>'0', 'title'=>'Mail Not Sent'),
            array('id'=>'1', 'title'=>'Waiting for approval'),
            array('id'=>'2', 'title'=>'Approved'),
            array('id'=>'3', 'title'=>'Rejected'),
        );    
        }            
        return $arr ;
        }
        public static function getUsers(){        
        //$model2 = Users::model()->find(array("select" => array("userid,first_name"),'condition' => "user_type IN (1,9,10) "));    
        $criteria = new CDbCriteria();
        $criteria->addInCondition('user_type', array(1,9));
        $result = Users::model()->findAll($criteria);
        return $result;
        }
}