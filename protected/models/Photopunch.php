<?php

/**
 * This is the model class for table "{{photopunch}}".
 *
 * The followings are the available columns in table '{{photopunch}}':
 * @property double $aid
 * @property double $logid
 * @property integer $sqllogid
 * @property integer $empid
 * @property string $log_time
 * @property integer $status
 * @property integer $device_id
 * @property integer $manual_entry_status
 * @property string $image
 * @property string $latitude
 * @property string $longitude
 * @property integer $punch_type
 * @property integer $work_site_id
 * @property string $work_site_name
 * @property string $approved_status
 *
 * The followings are the available model relations:
 * @property Manualentry $manualEntryStatus
 */
class Photopunch extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Photopunch the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{photopunch}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('empid, log_time, device_id', 'required'),
            array('sqllogid, empid, status, device_id, manual_entry_status, punch_type, work_site_id', 'numerical', 'integerOnly' => true),
            array('logid', 'numerical'),
            array('image, latitude, longitude, work_site_name, approved_status', 'length', 'max' => 100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('aid, logid, sqllogid, empid, log_time, status, device_id, manual_entry_status, image, latitude, longitude, punch_type, work_site_id, work_site_name, approved_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manualEntryStatus' => array(self::BELONGS_TO, 'Manualentry', 'manual_entry_status'),
            'usrId' => array(self::BELONGS_TO, 'Users', 'empid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'aid' => 'Aid',
            'logid' => 'Logid',
            'sqllogid' => 'Sqllogid',
            'empid' => 'Empid',
            'log_time' => 'Log Time',
            'status' => 'Status',
            'device_id' => 'Device',
            'manual_entry_status' => 'Manual Entry Status',
            'image' => 'Image',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'punch_type' => 'Punch Type',
            'work_site_id' => 'Work Site',
            'work_site_name' => 'Work Site Name',
            'approved_status' => 'Approved Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($status_type="All",$date = "", $owndata=0) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
if(isset($_GET['sdate']) || isset($_GET['edate']))
{
        $date=date("Y-m-d",strtotime("-45 days"));
      
		$date='"'.$date.'"';
		$criteria=new CDbCriteria;
		$criteria->together = true;
        $criteria->with = array('usrId');
        $criteria->compare('aid',$this->aid);
		$criteria->compare('logid',$this->logid);
		$criteria->compare('sqllogid',$this->sqllogid);
		$criteria->compare('concat(first_name," ",last_name)',$this->empid);
		$criteria->compare('log_time',$this->log_time,true);
		// $criteria->compare('status',$this->status);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('manual_entry_status',$this->manual_entry_status);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('punch_type',$this->punch_type);
		$criteria->compare('work_site_id',$this->work_site_id);
		$criteria->compare('work_site_name',$this->work_site_name,true);
        $criteria->compare('approved_status',$this->approved_status,true);        
        $criteria->addCondition('Date(log_time) >= '.$date);
        if ($status_type !== 'All') {
            $criteria->addCondition('t.approved_status = ' . intval($status_type));
        }
        if ($owndata != 0) {
            $criteria->addCondition('empid = '.Yii::app()->user->id);
        }        
        if (!empty($_GET['sdate']) && empty($_GET['edate'])) { 
            
            $criteria->addCondition(
                "DATE_FORMAT(log_time, '%Y-%m-%d') ='" . date('Y-m-d', strtotime($_GET['sdate'])) . "'"); 
            }
         elseif (!empty($_GET['edate']) && empty($_GET['sdate'])) {           
            $criteria->addCondition(
                "DATE_FORMAT(log_time, '%Y-%m-%d') ='" . date('Y-m-d', strtotime($_GET['edate'])) . "'"); 
        } 
        elseif (!empty($_GET['edate']) && !empty($_GET['sdate'])) {
        $criteria->addCondition("DATE_FORMAT(log_time,'%Y-%m-%d') between '" .date('Y-m-d', strtotime($_GET['sdate'])) . "' and '" .  date('Y-m-d', strtotime($_GET['edate'])) ."' ");
         }

        }
        else
        {

        $date=date("Y-m-d",strtotime("-45 days"));
      
		$date='"'.$date.'"';
		$criteria=new CDbCriteria;
		$criteria->together = true;
        $criteria->with = array('usrId');
        $criteria->compare('aid',$this->aid);
		$criteria->compare('logid',$this->logid);
		$criteria->compare('sqllogid',$this->sqllogid);
		$criteria->compare('concat(first_name," ",last_name)',$this->empid);
		$criteria->compare('log_time',$this->log_time,true);
		// $criteria->compare('status',$this->status);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('manual_entry_status',$this->manual_entry_status);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('punch_type',$this->punch_type);
		$criteria->compare('work_site_id',$this->work_site_id);
		$criteria->compare('work_site_name',$this->work_site_name,true);
        $criteria->compare('approved_status',$this->approved_status,true);        
        $criteria->addCondition('Date(log_time) >= '.$date);
        if ($status_type !== 'All') {
            $criteria->addCondition('t.approved_status = ' . intval($status_type));
        }
        if ($owndata != 0) {
            $criteria->addCondition('empid = '.Yii::app()->user->id);
        }        
        }


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort'=>array(
				'defaultOrder'=>'aid desc',
			),
	
        ));
    }

    public function findphotoimage($path) {
        $result = $filename = basename($path);
        $imagefolder = '/uploads/photopunch/';
        $fileabspath = Yii::app()->basePath . "/..{$imagefolder}" . $filename;
        
        if(!file_exists($fileabspath)){
            return CHtml::image(Yii::app()->request->baseUrl . "/images/no-image-strip.jpg", 'No Image',array("align"=>'center'));
        }
        
        $imagepath = $this->checkImageResize($fileabspath, $filename);


        return CHtml::image(Yii::app()->request->baseUrl . "{$imagefolder}" . $imagepath, '', array('width' => '100', 'height' => '100'));


        if (file_exists($fileabspath)) {
            //Check Resized
            $imagepath = $this->checkImageResize($fileabspath, $filename);
//            die;

            return CHtml::image(Yii::app()->request->baseUrl . "{$imagefolder}" . $imagepath, '', array('width' => '100', 'height' => '100'));
        } else {
            return "No Image";
        }
    }

    public function checkImageResize($fileabspath, $filename) {
        if (filesize($fileabspath) / (1024 * 1024) > 1) {
            // echo dirname($fileabspath) . "/" . $filename;
        }




        if (stripos($filename, 'resized') === false) {

            if (!file_exists(dirname($fileabspath) . "/resized" . $filename)) {
                try {
                    $image = new EasyImage($fileabspath);
                    $image->resize(200, 200);
//                echo "<br /><b>".$filename."</b>";
//                echo var_dump(strpos('resized', $filename));
//                echo $filename;

                    $image->save(dirname($fileabspath) . "/resized" . $filename);
                    return "resized" . $filename;
                } catch (Exception $e) {
                    return $filename;
                }
            } else {


                if (!file_exists(dirname($fileabspath) . $filename)) {
                    if (file_exists(dirname($fileabspath) . trim($filename, "resized"))) {
                        try {
                            $image = new EasyImage($fileabspath);
                            $image->resize(200, 200);
//                echo "<br /><b>".$filename."</b>";
//                echo var_dump(strpos('resized', $filename));
//                echo $filename;

                            $image->save(dirname($fileabspath) . "/resized" . $filename);
                            return "resized" . $filename;
                        } catch (Exception $e) {
                            return $filename;
                        }
                    }
                } else {
                    
                }
                return $filename;
            }
        } else {
            if (!file_exists(dirname($fileabspath) .'/'. $filename)) {
                $filepath = dirname($fileabspath) . '/' . trim($filename, "resized");

                if (file_exists(dirname($fileabspath) . '/' . trim($filename, "resized"))) {
                    try {
                        $filefullpath = dirname(Yii::app()->basePath) . "/uploads/photopunch/" . $filename;
                        $image = new EasyImage(str_replace('resized', '', $filefullpath));
                        $image->resize(200, 200);

                        if ($image->save($filefullpath)) {
//                            echo 'success';
                            if (!file_exists($filefullpath)) {
                                return trim($filename, "resized");
                            }
                        }
                        return $filename;
                    } catch (Exception $e) {
                        // echo $filefullpath;
                        print_r($e->getMessage());
//                        die;
                        return $filename;
                    }
                }
            }
            return $filename;
        }
    }

}
