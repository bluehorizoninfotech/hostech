<?php

/**
 * This is the model class for table "manual_entry_report_view".
 *
 * The followings are the available columns in table 'manual_entry_report_view':
 * @property integer $entry_id
 * @property integer $emp_id
 * @property string $date
 * @property string $comment
 * @property integer $status
 * @property string $type
 * @property integer $created_by
 * @property integer $decision_by
 */
class ManualEntryReportView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manual_entry_report_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entry_id, emp_id, status, created_by, decision_by', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>1),
			array('date, comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('entry_id, emp_id, date, comment, status, type, created_by, decision_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'entry_id' => 'Entry',
			'emp_id' => 'Emp',
			'date' => 'Date',
			'comment' => 'Comment',
			'status' => 'Status',
			'type' => 'Type',
			'created_by' => 'Created By',
			'decision_by' => 'Decision By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('entry_id',$this->entry_id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('decision_by',$this->decision_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ManualEntryReportView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
