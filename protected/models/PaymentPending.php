<?php

/**
 * This is the model class for table "{{payment_pending}}".
 *
 * The followings are the available columns in table '{{payment_pending}}':
 * @property integer $id
 * @property integer $weekly_report_id
 * @property string $budget_head
 * @property string $description
 * @property string $vendor
 * @property string $bill_date
 * @property string $bill_ref
 * @property string $bill_amount
 * @property string $remarks
 * @property string $updated_date
 * @property integer $updated_by
 * @property string $created_date
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property ProjectReport $weeklyReport
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class PaymentPending extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaymentPending the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payment_pending}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('weekly_report_id, budget_head, description, vendor, bill_date, bill_ref, bill_amount, remarks', 'required'),
			array('weekly_report_id, updated_by, created_by', 'numerical', 'integerOnly'=>true),
			array('budget_head, vendor, bill_ref, bill_amount', 'length', 'max'=>200),
			array('updated_date, created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, weekly_report_id, budget_head, description, vendor, bill_date, bill_ref, bill_amount, remarks, updated_date, updated_by, created_date, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'weeklyReport' => array(self::BELONGS_TO, 'ProjectReport', 'weekly_report_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'weekly_report_id' => 'Weekly Report',
			'budget_head' => 'Budget Head',
			'description' => 'Description',
			'vendor' => 'Vendor',
			'bill_date' => 'Bill Date',
			'bill_ref' => 'Bill Ref',
			'bill_amount' => 'Bill Amount',
			'remarks' => 'Remarks',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('weekly_report_id',$this->weekly_report_id);
		$criteria->compare('budget_head',$this->budget_head,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('vendor',$this->vendor,true);
		$criteria->compare('bill_date',$this->bill_date,true);
		$criteria->compare('bill_ref',$this->bill_ref,true);
		$criteria->compare('bill_amount',$this->bill_amount,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}