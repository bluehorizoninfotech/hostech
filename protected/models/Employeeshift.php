<?php

/**
 * This is the model class for table "{{employee_shift}}".
 *
 * The followings are the available columns in table '{{employee_shift}}':
 * @property integer $shift_id
 * @property string $shift_name
 * @property string $short_name
 * @property string $shift_starts
 * @property string $shift_ends
 * @property double $grace_period
 * @property integer $created_by
 * @property string $created_date
 * @property string $updated_date
 * @property integer $updated_by
 * @property string $color_code
 * @property integer $type
 * @property double $grace_period_before
 * @property double $grace_period_after
 * @property double $pt_p
 * @property double $pt_ul
 * @property double $pt_uhl
 * @property double $pt_hp
 * @property integer $is_continuous_shift
 * @property string $min_ot_value
 * @property string $max_ot_value
 * @property integer $fIxed_ot
 * @property integer $full_ot
 * @property integer $no_ot
 * @property integer $twoP
 * @property integer $sunday_enable
 * @property string $cs_ot_description
 * @property string $continuous_shift_end
 * @property integer $ot_offset
 * @property integer $apply_ot_offset
 * @property double $min_ot_intime
 */
class Employeeshift extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Employeeshift the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{employee_shift}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shift_name, min_ot_value, max_ot_value, fIxed_ot, cs_ot_description', 'required'),
			array('created_by, updated_by, type, is_continuous_shift, fIxed_ot, full_ot, no_ot, twoP, sunday_enable, ot_offset, apply_ot_offset', 'numerical', 'integerOnly'=>true),
			array('grace_period, grace_period_before, grace_period_after, pt_p, pt_ul, pt_uhl, pt_hp, min_ot_intime', 'numerical'),
			array('shift_name', 'length', 'max'=>40),
			array('short_name', 'length', 'max'=>20),
			array('shift_starts, shift_ends, continuous_shift_end', 'length', 'max'=>11),
			array('color_code', 'length', 'max'=>50),
			array('min_ot_value, max_ot_value', 'length', 'max'=>10),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('shift_id, shift_name, short_name, shift_starts, shift_ends, grace_period, created_by, created_date, updated_date, updated_by, color_code, type, grace_period_before, grace_period_after, pt_p, pt_ul, pt_uhl, pt_hp, is_continuous_shift, min_ot_value, max_ot_value, fIxed_ot, full_ot, no_ot, twoP, sunday_enable, cs_ot_description, continuous_shift_end, ot_offset, apply_ot_offset, min_ot_intime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'shift_id' => 'Shift',
			'shift_name' => 'Shift Name',
			'short_name' => 'Short Name',
			'shift_starts' => 'Shift Starts',
			'shift_ends' => 'Shift Ends',
			'grace_period' => 'Grace Period',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
			'color_code' => 'Color Code',
			'type' => 'Type',
			'grace_period_before' => 'Grace Period Before',
			'grace_period_after' => 'Grace Period After',
			'pt_p' => 'Pt P',
			'pt_ul' => 'Pt Ul',
			'pt_uhl' => 'Pt Uhl',
			'pt_hp' => 'Pt Hp',
			'is_continuous_shift' => 'Is Continuous Shift',
			'min_ot_value' => 'Min Ot Value',
			'max_ot_value' => 'Max Ot Value',
			'fIxed_ot' => 'F Ixed Ot',
			'full_ot' => 'Full Ot',
			'no_ot' => 'No Ot',
			'twoP' => 'Two P',
			'sunday_enable' => 'Sunday Enable',
			'cs_ot_description' => 'Cs Ot Description',
			'continuous_shift_end' => 'Continuous Shift End',
			'ot_offset' => 'Ot Offset',
			'apply_ot_offset' => 'Apply Ot Offset',
			'min_ot_intime' => 'Min Ot Intime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('shift_name',$this->shift_name,true);
		$criteria->compare('short_name',$this->short_name,true);
		$criteria->compare('shift_starts',$this->shift_starts,true);
		$criteria->compare('shift_ends',$this->shift_ends,true);
		$criteria->compare('grace_period',$this->grace_period);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('color_code',$this->color_code,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('grace_period_before',$this->grace_period_before);
		$criteria->compare('grace_period_after',$this->grace_period_after);
		$criteria->compare('pt_p',$this->pt_p);
		$criteria->compare('pt_ul',$this->pt_ul);
		$criteria->compare('pt_uhl',$this->pt_uhl);
		$criteria->compare('pt_hp',$this->pt_hp);
		$criteria->compare('is_continuous_shift',$this->is_continuous_shift);
		$criteria->compare('min_ot_value',$this->min_ot_value,true);
		$criteria->compare('max_ot_value',$this->max_ot_value,true);
		$criteria->compare('fIxed_ot',$this->fIxed_ot);
		$criteria->compare('full_ot',$this->full_ot);
		$criteria->compare('no_ot',$this->no_ot);
		$criteria->compare('twoP',$this->twoP);
		$criteria->compare('sunday_enable',$this->sunday_enable);
		$criteria->compare('cs_ot_description',$this->cs_ot_description,true);
		$criteria->compare('continuous_shift_end',$this->continuous_shift_end,true);
		$criteria->compare('ot_offset',$this->ot_offset);
		$criteria->compare('apply_ot_offset',$this->apply_ot_offset);
		$criteria->compare('min_ot_intime',$this->min_ot_intime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}