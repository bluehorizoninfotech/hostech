<?php

/**
 * This is the model class for table "{{mail_log}}".
 *
 * The followings are the available columns in table '{{mail_log}}':
 * @property integer $log_id
 * @property integer $send_to
 * @property integer $send_by
 * @property string $send_date
 * @property string $description
 * @property string $created_date
 * @property integer $created_by
 */
class MailLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	
	public $sendemail_status;
	public $mail_type;
	public function tableName()
	{
		return '{{mail_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_date', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('send_date, description, message, sendemail, mail_type,send_to, send_by,sendemail_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('log_id, send_to, send_by, send_date, description, created_date, created_by,sendemail_status, message, sendemail, mail_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'sendBy' => array(self::BELONGS_TO, 'Employee', 'send_by'),
			'sendTo' => array(self::BELONGS_TO, 'Employee', 'send_to'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_id' => 'Log',
			'send_to' => 'Send To',
			'send_by' => 'Send By',
			'send_date' => 'Send Date',
			'sendemail_status'=> 'Mail Send Status',
			'message' => 'Message',
			'description' => 'Description',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'mail_type' => 'Mail Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria; 

		$criteria->compare('log_id',$this->log_id);
		$criteria->compare('send_to',$this->send_to);
		$criteria->compare('send_by',$this->send_by);
		$criteria->compare('sendemail_status',$this->sendemail_status);
		$criteria->compare('send_date',$this->send_date,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('message',$this->message);
		//$criteria->compare('sendemail',$this->sendemail);
		$criteria->compare('mail_type',$this->mail_type);

    $criteria->order = 'send_date DESC';


    return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MailLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	 public function checkMailstatus($status){
            if($status == 1){
				return "Mail Sent";                
            }else{
				return "Mail Not Sent";
            }
		}  
		
	public function getemailnotifications($interval=0) {
		// $model=GeneralSettings::model()->find(['condition'=>'id = 1']);
		// if(!empty($model['email_notification_page_size_home'])){
		// 	$email_page_count =$model['email_notification_page_size_home'];
		// }else{
		// 	$email_page_count = 5;
		// }		
		$criteria = new CDbCriteria;
		
		$today = date('Y-m-d'); 
        $criteria->compare('log_id',$this->log_id);
		$criteria->compare('send_to',$this->send_to);
		$criteria->compare('send_by',$this->send_by);
		$criteria->compare('sendemail_status',$this->sendemail_status);
		$criteria->compare('send_date',$this->send_date,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('message',$this->message);
		$criteria->compare('mail_type',$this->mail_type);
		//$criteria->compare('created_date',$today);
		if(!empty($interval)){
			$criteria->compare('created_date',date('Y-m-d', strtotime($interval)));	
			}else{
				$criteria->compare('created_date',date('Y-m-d'));	
			}
			$criteria->order = 'send_date DESC';
			$dataProvider = new CActiveDataProvider($this, array('pagination'=>array(
			'pageSize'=>20,
    	),
        'criteria' => $criteria, ));
		$dataProvider->setTotalItemCount(count($this->findAll($criteria)));
	   return $dataProvider;
	}
	public function send_toarrayjoin($send_to){
          if(!empty($send_to)){
			  if(json_decode($send_to, true)){
				$email_sent_to=json_decode($send_to, true);
				$email_sent_to_join=join(",",$email_sent_to);
				return $email_sent_to_join;
			  
		  }else{
			return $send_to;
		 }
		}
    }


}