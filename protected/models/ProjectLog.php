<?php

/**
 * This is the model class for table "project_log".
 *
 * The followings are the available columns in table 'project_log':
 * @property integer $id
 * @property integer $pid
 * @property integer $client_id
 * @property string $project_no
 * @property string $name
 * @property integer $billable
 * @property integer $status
 * @property double $budget
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property integer $assigned_to
 * @property string $img_path
 * @property integer $created_by
 * @property string $created_date
 */
class ProjectLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pid, client_id, billable, status, created_by', 'numerical', 'integerOnly'=>true),
			array('budget', 'numerical'),
			array('project_no', 'length', 'max'=>255),
			array('name', 'length', 'max'=>100),
			array('description, start_date, end_date, img_path, created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pid, client_id, project_no, name, billable, status, budget, description, start_date, end_date, assigned_to, img_path, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'pid'),
			'billable0' => array(self::BELONGS_TO, 'Status', 'billable'),
			'billable0Old' => array(self::BELONGS_TO, 'Status', 'billable_old'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'updatedByOld' => array(self::BELONGS_TO, 'Users', 'updated_by_old'),
			 'client' => array(self::BELONGS_TO, 'Clients', 'client_id'),
			 'clientOld' => array(self::BELONGS_TO, 'Clients', 'client_id_old'),
			'status0' => array(self::BELONGS_TO, 'Status', 'status'),
			'status0Old' => array(self::BELONGS_TO, 'Status', 'status_old'),
            'tasks' => array(self::HAS_MANY, 'Tasks', 'project_id'),
            'assignedTo' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pid' => 'Pid',
			'client_id' => 'Client',
			'project_no' => 'Project No',
			'name' => 'Name',
			'billable' => 'Billable',
			'status' => 'Status',
			'budget' => 'Budget',
			'description' => 'Description',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'assigned_to' => 'Assigned To',
			'img_path' => 'Img Path',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pid',$this->pid);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('project_no',$this->project_no,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('billable',$this->billable);
		$criteria->compare('status',$this->status);
		$criteria->compare('budget',$this->budget);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('assigned_to',$this->assigned_to);
		$criteria->compare('img_path',$this->img_path,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		if(Yii::app()->user->project_id !=""){
            $criteria->addCondition('pid = '.Yii::app()->user->project_id.'');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,'sort' => array(
				'defaultOrder' => 'id desc',),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
