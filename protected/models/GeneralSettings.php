<?php

/**
 * This is the model class for table "{{general_settings}}".
 *
 * The followings are the available columns in table '{{general_settings}}':
 * @property integer $id
 * @property string $from_name
 * @property string $from_email
 * @property string $smtp_host
 * @property string $smtp_auth
 * @property string $smtp_username
 * @property string $smtp_password
 * @property string $smtp_secure
 * @property string $smtp_email_from
 * @property string $smtp_mailfrom_name
 * @property string $smtp_port
 * @property integer $updated_by
 * @property string $updated_date
 * @property string $created_date
 * @property string $behind_task_mail_recipients
 * @property string $dropbox_token
 * @property integer $add_time_entry_approval

 */
class GeneralSettings extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GeneralSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{general_settings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('behind_task_mail_recipients,smtp_host, smtp_auth, smtp_username, smtp_password, smtp_secure,
			 smtp_email_from, updated_by,dropbox_token, backlog_days', 'required'),
			array('updated_by,experience_year,project_count,customers_count,engineers_count,ongoing_project_count', 'numerical', 'integerOnly'=>true),
			array('from_name, from_email, smtp_host, smtp_auth, smtp_username, smtp_password, smtp_secure, 
			smtp_email_from, smtp_mailfrom_name', 'length', 'max'=>50),
			array('smtp_port', 'length', 'max'=>20),
			array('email_notification_page_size_home,escalated_tasks_size_home', 'safe'),
			array('email_notification_page_size_home,escalated_tasks_size_home', 'numerical', 'integerOnly'=>true),
			array('email_notification_page_size_home,escalated_tasks_size_home', 'numerical', 'min'=>1),
			// array('behind_task_mail_recipients', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			
			array('id, from_name, from_email, smtp_host, smtp_auth, smtp_username, smtp_password, smtp_secure,
			 smtp_email_from, smtp_mailfrom_name, smtp_port, updated_by, updated_date, created_date,
			  behind_task_mail_recipients, backlog_days,experience_year,experience_title,project_count,project_title,customers_count,customers_title,engineers_count,engineers_title,ongoing_project_count,ongoing_project_title', 'safe', 'on'=>'search'),
			array('add_time_entry_approval', 'in', 'range' => array(1, 2)),

			);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_name' => 'From Name',
			'from_email' => 'From Email',
			'smtp_host' => 'Smtp Host',
			'smtp_auth' => 'Smtp Auth',
			'smtp_username' => 'Smtp Username',
			'smtp_password' => 'Smtp Password',
			'smtp_secure' => 'Smtp Secure',
			'smtp_email_from' => 'Smtp Email From',
			'smtp_mailfrom_name' => 'Smtp Mail From Name',
			'smtp_port' => 'Smtp Port',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'created_date' => 'Created Date',
			'behind_task_mail_recipients' => 'Behind Schedule Task Mail Recipients',
			'dropbox_token' => 'Dropbox Token',
			'slider_images'=>'Slider Images',
			'email_notification_page_size_home'=> 'Email Notification Limit',
            'escalated_tasks_size_home' =>'Escalated Task Limit',
			'backlog_days' => 'Backlog Days',
			'experience_year' => 'Count',
			'experience_title' => 'Label',
			'project_count' => 'Count',
			'project_title' => 'Label',
			'customers_count' => 'Count',
			'customers_title' => 'Label',
			'engineers_count' => 'Count',
			'engineers_title' => 'Label',
			'ongoing_project_count' => 'Count',
			'ongoing_project_title' => 'Label',
			'add_time_entry_approval'=>'Add Time Entry Approval'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('from_name',$this->from_name,true);
		$criteria->compare('from_email',$this->from_email,true);
		$criteria->compare('smtp_host',$this->smtp_host,true);
		$criteria->compare('smtp_auth',$this->smtp_auth,true);
		$criteria->compare('smtp_username',$this->smtp_username,true);
		$criteria->compare('smtp_password',$this->smtp_password,true);
		$criteria->compare('smtp_secure',$this->smtp_secure,true);
		$criteria->compare('smtp_email_from',$this->smtp_email_from,true);
		$criteria->compare('smtp_mailfrom_name',$this->smtp_mailfrom_name,true);
		$criteria->compare('smtp_port',$this->smtp_port,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('behind_task_mail_recipients',$this->behind_task_mail_recipients,true);
		$criteria->compare('backlog_days',$this->backlog_days,true);
		
		$criteria->compare('experience_year',$this->experience_year,true);
		$criteria->compare('experience_title',$this->experience_title,true);
		$criteria->compare('project_count',$this->project_count,true);
		$criteria->compare('project_title',$this->project_title,true);
		$criteria->compare('customers_count',$this->customers_count,true);
		$criteria->compare('customers_title',$this->customers_title,true);
		$criteria->compare('engineers_count',$this->engineers_count,true);
		$criteria->compare('engineers_title',$this->engineers_title,true);
		$criteria->compare('ongoing_project_count',$this->ongoing_project_count,true);
		$criteria->compare('ongoing_project_title',$this->ongoing_project_title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}