<?php

/**
 * This is the model class for table "task_log".
 *
 * The followings are the available columns in table 'task_log':
 * @property string $action
 * @property integer $revision
 * @property string $dt_datetime
 * @property integer $tskid
 * @property integer $project_id
 * @property integer $clientsite_id
 * @property string $title
 * @property string $start_date
 * @property string $due_date
 * @property integer $status
 * @property integer $priority
 * @property integer $assigned_to
 * @property integer $report_to
 * @property integer $coordinator
 * @property integer $billable
 * @property double $total_hrs
 * @property double $hourly_rate
 * @property double $min_rate
 * @property string $description
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 * @property integer $trash
 * @property integer $parent_tskid
 * @property double $progress_percent
 * @property integer $acknowledge_by
 * @property double $quantity
 * @property integer $unit
 * @property double $rate
 * @property double $amount
 * @property integer $milestone_id
 * @property integer $contractor_id
 * @property integer $task_duration
 * @property double $daily_target
 * @property integer $work_type_id
 * @property double $allowed_workers
 * @property integer $required_workers
 * @property string $task_type
 * @property string $email
 */
class TaskLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TaskLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'task_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tskid, title, start_date, status, priority, assigned_to, report_to, billable, created_date, created_by, updated_date, updated_by, quantity, unit, rate, amount, task_duration, daily_target', 'required'),
			array('tskid, project_id, clientsite_id, status, priority, assigned_to, report_to, coordinator, billable, created_by, updated_by, trash, parent_tskid, acknowledge_by, unit, milestone_id, contractor_id, task_duration, work_type_id, required_workers', 'numerical', 'integerOnly'=>true),
			array('total_hrs, hourly_rate, min_rate, progress_percent, quantity, rate, amount, daily_target, allowed_workers', 'numerical'),
			array('action', 'length', 'max'=>8),
			array('title', 'length', 'max'=>200),
			array('task_type', 'length', 'max'=>1),
			array('dt_datetime, due_date, description, email', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('action, revision, dt_datetime, tskid, project_id, clientsite_id, title, start_date, due_date, status, priority, assigned_to, report_to, coordinator, billable, total_hrs, hourly_rate, min_rate, description, created_date, created_by, updated_date, updated_by, trash, parent_tskid, progress_percent, acknowledge_by, quantity, unit, rate, amount, milestone_id, contractor_id, task_duration, daily_target, work_type_id, allowed_workers, required_workers, task_type, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'billable0' => array(self::BELONGS_TO, 'Status', 'billable'),
			'assignedTo' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
			'assignedToold' => array(self::BELONGS_TO, 'Users', 'assigned_to_old'),
			'reportTo' => array(self::BELONGS_TO, 'Users', 'report_to'),
			'reportToold' => array(self::BELONGS_TO, 'Users', 'report_to_old'),
			'coordinator0' => array(self::BELONGS_TO, 'Users', 'coordinator'),
			'coordinator0old' => array(self::BELONGS_TO, 'Users', 'coordinator_old'),
            'priority0' => array(self::BELONGS_TO, 'Status', 'priority'),
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'status0' => array(self::BELONGS_TO, 'Status', 'status'),
			'status0old' => array(self::BELONGS_TO, 'Status', 'status_old'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'createdByold' => array(self::BELONGS_TO, 'Users', 'created_by_old'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'updatedByold' => array(self::BELONGS_TO, 'Users', 'updated_by_old'),
            'timeEntries' => array(self::HAS_MANY, 'TimeEntry', 'tskid', ),
            'clientsite' => array(self::HAS_MANY, 'Clientsite', 'clientsite_id'),
            'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
            'milestone' => array(self::BELONGS_TO, 'Milestone', 'milestone_id'),
			'worktype' => array(self::BELONGS_TO, 'WorkType', 'work_type_id'),
			'worktypeold' => array(self::BELONGS_TO, 'WorkType', 'work_type_id_old'),
			'contractor' => array(self::BELONGS_TO, 'Contractors', 'contractor_id'),
			'contractorold' => array(self::BELONGS_TO, 'Contractors', 'contractor_id_old'),
			'project_old' => array(self::BELONGS_TO, 'Projects', 'project_id_old'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'action' => 'Action',
			'revision' => 'Revision',
			'dt_datetime' => 'Dt Datetime',
			'tskid' => 'Tskid',
			'project_id' => 'Project',
			'clientsite_id' => 'Clientsite',
			'title' => 'Title',
			'start_date' => 'Start Date',
			'due_date' => 'Due Date',
			'status' => 'Status',
			'priority' => 'Priority',
			'assigned_to' => 'Assigned To',
			'report_to' => 'Report To',
			'coordinator' => 'Coordinator',
			'billable' => 'Billable',
			'total_hrs' => 'Total Hrs',
			'hourly_rate' => 'Hourly Rate',
			'min_rate' => 'Min Rate',
			'description' => 'Description',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
			'trash' => 'Trash',
			'parent_tskid' => 'Parent Tskid',
			'progress_percent' => 'Progress Percent',
			'acknowledge_by' => 'Acknowledge By',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'amount' => 'Amount',
			'milestone_id' => 'Milestone',
			'contractor_id' => 'Contractor',
			'task_duration' => 'Task Duration',
			'daily_target' => 'Daily Target',
			'work_type_id' => 'Work Type',
			'allowed_workers' => 'Allowed Workers',
			'required_workers' => 'Required Workers',
			'task_type' => 'Task Type',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->join = 'JOIN `pms_tasks` `tasks` '
		. 'ON (`tasks`.`tskid`=`t`.`tskid`)';

		$criteria->compare('t.action',$this->action,true);
		$criteria->compare('t.revision',$this->revision);
		$criteria->compare('t.dt_datetime',$this->dt_datetime,true);
		$criteria->compare('t.tskid',$this->tskid);
		$criteria->compare('t.project_id',$this->project_id);
		$criteria->compare('t.clientsite_id',$this->clientsite_id);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.start_date',$this->start_date,true);
		$criteria->compare('t.due_date',$this->due_date,true);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.priority',$this->priority);
		$criteria->compare('t.assigned_to',$this->assigned_to);
		$criteria->compare('t.report_to',$this->report_to);
		$criteria->compare('t.coordinator',$this->coordinator);
		$criteria->compare('t.billable',$this->billable);
		$criteria->compare('t.total_hrs',$this->total_hrs);
		$criteria->compare('t.hourly_rate',$this->hourly_rate);
		$criteria->compare('t.min_rate',$this->min_rate);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.created_date',$this->created_date,true);
		$criteria->compare('t.created_by',$this->created_by);
		$criteria->compare('t.updated_date',$this->updated_date,true);
		$criteria->compare('t.updated_by',$this->updated_by);
		$criteria->compare('t.trash',$this->trash);
		$criteria->compare('t.parent_tskid',$this->parent_tskid);
		$criteria->compare('t.progress_percent',$this->progress_percent);
		$criteria->compare('t.acknowledge_by',$this->acknowledge_by);
		$criteria->compare('t.quantity',$this->quantity);
		$criteria->compare('t.unit',$this->unit);
		$criteria->compare('t.rate',$this->rate);
		$criteria->compare('t.amount',$this->amount);
		$criteria->compare('t.milestone_id',$this->milestone_id);
		$criteria->compare('t.contractor_id',$this->contractor_id);
		$criteria->compare('t.task_duration',$this->task_duration);
		$criteria->compare('t.daily_target',$this->daily_target,true);
		$criteria->compare('t.work_type_id',$this->work_type_id);
		$criteria->compare('t.allowed_workers',$this->allowed_workers);
		$criteria->compare('t.required_workers',$this->required_workers);
		//$criteria->compare('task_type',$this->task_type,true);
		$criteria->compare('t.email',$this->email,true);

		if($this->task_type != "")
		{
			$criteria->compare('tasks.task_type',$this->task_type,true);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,'sort' => array(
				'defaultOrder' => 'dt_datetime desc',),
		));
	}
	public function getcompleted_percent($id)
    {
        // $task = Tasks::model()->findByPk($id);
        // if($task->task_type == 1){
        //     $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='".$id."'")->queryRow();
        //     return isset($sql_response2['work_progress']) ? number_format($sql_response2['work_progress'],2).'%' : '0%';
        // }else{
        //     $query = "SELECT `progress_percent` FROM pms_tasks WHERE pms_tasks.tskid = '$id'  AND  trash=1 ";
        //     $percentage = Yii::app()->db->createCommand($query)->queryRow();
        //     return isset($percentage['progress_percent']) ? $percentage['progress_percent'].'%' : '0%';
        // }
        $task = Tasks::model()->findByPk($id);
        if($task->task_type == 1){
            $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(daily_work_progress) as work_progress FROM pms_daily_work_progress WHERE taskid ='".$id."' AND approve_status =1")->queryRow();
            return isset($sql_response2['work_progress']) ? number_format($sql_response2['work_progress'],2) : '0';
        }else{
            $sql_response2 = Yii::app()->db->createCommand("SELECT SUM(completed_percent) as work_progress FROM pms_time_entry WHERE tskid ='".$id."' AND approve_status =1")->queryRow();
            return isset($sql_response2['work_progress']) ? number_format($sql_response2['work_progress'],2) : '0';
        }
    }
	public function onHoldReport($project_id)
	{
		$task_array=array();
		if($project_id != "")
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 'project_id='.$project_id.' and status IN(5,73,74)';
			$criteria->order ='dt_datetime';
			$tasks = TaskLog::model()->findAll($criteria);

			foreach($tasks as $task)
			{
				$task_id=$task->tskid;
				$date=$task->dt_datetime;
				$dateTime = new DateTime($task->dt_datetime);
                $date = $dateTime->format('Y-m-d');
				$data  = Yii::app()->db->createCommand("SELECT * FROM task_log where tskid=" . $task_id . " and status NOT IN(5,73,74) and DATE(dt_datetime) >'$date'")->queryRow();
			$active_status_date=date('Y-m-d');
			if($data)
			{
				$status_date=new DateTime($data['dt_datetime']);
				$active_status_date=$status_date->format('Y-m-d');
			}
			$task_model = Tasks::model()->findByPk($task->tskid);
			$task_array[] =array(
				'task_id'=>$task->tskid,'task_name'=>$task->title,
				'status'=>$task->status,'hold_date'=>$date,'active_date'=>$active_status_date,
				'quantity'=>$task->quantity,
				'task_type'=>$task_model->task_type
			);
			
			}

			return $task_array;
		}
		
	}
	public function getTaskType($task_id)
	{
		$task = Tasks::model()->findByPk($task_id);
		if($task->task_type==1)
		{
			return "External";
		}
		else
		{
			return "Internal";
		}
	}
}