<?php

/**
 * This is the model class for table "{{design_deliverables}}".
 *
 * The followings are the available columns in table '{{design_deliverables}}':
 * @property integer $id
 * @property integer $weekly_report_id
 * @property integer $project_id
 * @property string $drawing_description
 * @property string $target_date
 * @property string $actual_date
 * @property string $remarks
 * @property integer $drawing_status
 * @property integer $created_by
 * @property string $created_date
 */
class DesignDeliverables extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DesignDeliverables the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{design_deliverables}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('weekly_report_id, project_id, drawing_description, target_date, actual_date, remarks, created_by', 'required'),
			array('weekly_report_id, project_id, drawing_status, created_by', 'numerical', 'integerOnly'=>true),
			array('drawing_description, remarks', 'length', 'max'=>200),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, weekly_report_id, project_id, drawing_description, target_date, actual_date, remarks, drawing_status, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'weekly_report_id' => 'Weekly Report',
			'project_id' => 'Project',
			'drawing_description' => 'Drawing Description',
			'target_date' => 'Target Date',
			'actual_date' => 'Actual Date',
			'remarks' => 'Remarks',
			'drawing_status' => 'Drawing Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('weekly_report_id',$this->weekly_report_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('drawing_description',$this->drawing_description,true);
		$criteria->compare('target_date',$this->target_date,true);
		$criteria->compare('actual_date',$this->actual_date,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('drawing_status',$this->drawing_status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}