<?php

/**
 * This is the model class for table "{{role_settings}}".
 *
 * The followings are the available columns in table '{{role_settings}}':
 * @property integer $rset_id
 * @property integer $role_id
 * @property string $short_name
 * @property integer $ot_formula
 * @property integer $min_ot
 * @property integer $max_ot_yes
 * @property integer $max_ot
 * @property integer $last_inpunch
 * @property integer $late_coming_gt
 * @property integer $early_going_gt
 * @property integer $weekly_off1_yes
 * @property string $weekly_off1
 * @property integer $weekly_off2_yes
 * @property string $weekly_off2
 * @property string $weekly_off2_count
 * @property integer $early_coming_punch
 * @property integer $late_going_punch
 * @property integer $first_last_punch
 * @property integer $deduct_breakhours
 * @property integer $halfday_by_duration
 * @property integer $halfday_duration_mins
 * @property integer $absent_by_duration
 * @property integer $absent_duration_mins
 * @property integer $partial_halfday_by_duration
 * @property integer $partial_halfday_duration_mins
 * @property integer $partial_absent_by_duration
 * @property integer $partial_absent_duration_mins
 * @property integer $absent_for_prefix
 * @property integer $absent_for_suffix
 * @property integer $absent_for_both
 * @property integer $absent_for_condition
 * @property string $absent_type
 * @property integer $absent_late
 * @property integer $halfday_by_late
 * @property integer $halfday_late_mins
 * @property integer $halfday_by_earlygoing
 * @property integer $halfday_earlygoing_mins
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property UserRoles $role
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class RoleSettings extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RoleSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{role_settings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                    array('role_id, created_by, created_date', 'required'),
                    array('role_id, ot_formula, min_ot, max_ot_yes, max_ot, last_inpunch, late_coming_gt, early_going_gt, weekly_off1_yes, weekly_off2_yes, early_coming_punch, late_going_punch, first_last_punch, deduct_breakhours, halfday_by_duration, halfday_duration_mins, absent_by_duration, absent_duration_mins, partial_halfday_by_duration, partial_halfday_duration_mins, partial_absent_by_duration, partial_absent_duration_mins, absent_for_prefix, absent_for_suffix, absent_for_both, absent_for_condition, absent_late, halfday_by_late, halfday_late_mins, halfday_by_earlygoing, halfday_earlygoing_mins, created_by, updated_by', 'numerical', 'integerOnly'=>true),
                    array('short_name, weekly_off1, weekly_off2, weekly_off2_count, absent_type', 'length', 'max'=>20),
                    array('updated_date', 'safe'),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('rset_id, role_id, short_name, ot_formula, min_ot, max_ot_yes, max_ot, last_inpunch, late_coming_gt, early_going_gt, weekly_off1_yes, weekly_off1, weekly_off2_yes, weekly_off2, weekly_off2_count, early_coming_punch, late_going_punch, first_last_punch, deduct_breakhours, halfday_by_duration, halfday_duration_mins, absent_by_duration, absent_duration_mins, partial_halfday_by_duration, partial_halfday_duration_mins, partial_absent_by_duration, partial_absent_duration_mins, absent_for_prefix, absent_for_suffix, absent_for_both, absent_for_condition, absent_type, absent_late, halfday_by_late, halfday_late_mins, halfday_by_earlygoing, halfday_earlygoing_mins, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
                );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'role' => array(self::BELONGS_TO, 'UserRoles', 'role_id'),
                        'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
                        'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rset_id' => 'Rset',
			'role_id' => 'Role Name',
			'short_name' => 'Short Name',
			'ot_formula' => 'OT Formula',
			'min_ot' => 'Min OT',
                        'max_ot_yes' => 'Max Ot Yes',
			'max_ot' => 'Max OT',
			'last_inpunch' => 'Neglect Last InPunch (For missed OutPunch)',
			'late_coming_gt' => 'Grace Time for Late Coming',
			'early_going_gt' => 'Grace Time for Early Going',
			'weekly_off1_yes' => 'Weekly Off 1',
			'weekly_off1' => 'Weekly Off1',
			'weekly_off2_yes' => 'Weekly Off 2',
			'weekly_off2' => 'Weekly Off2',
			'weekly_off2_count' => 'Weekly Off2 Count',
			'early_coming_punch' => 'Consider Early Coming Punch',
			'late_going_punch' => 'Consider Late Going Punch',
			'first_last_punch' => 'Calculate only First and Last Punch in Att Calculation',
                        'deduct_breakhours' => 'Deduct Break hours from Work duration',
			'halfday_by_duration' => 'Calculate Half Day if work duration is less than',
			'halfday_duration_mins' => 'Halfday Duration Mins',
			'absent_by_duration' => 'Calculate Absent if work duration is less than',
			'absent_duration_mins' => 'Absent Duration Mins',
			'partial_halfday_by_duration' => 'On Partial day, Calculate Half Day if work duration is less than',
			'partial_halfday_duration_mins' => 'Partial Halfday Duration Mins',
			'partial_absent_by_duration' => 'On Partial day, Calculate Absent if work duration is less than',
			'partial_absent_duration_mins' => 'Partial Absent Duration Mins',
			'absent_for_prefix' => 'Mark Weekly off and Holiday as Absent for Prefix Day is Absent',
			'absent_for_suffix' => 'Mark Weekly off and Holiday as Absent for Suffix Day is Absent',
			'absent_for_both' => 'Mark Weekly off and Holiday as Absent if both Suffix Day and Prefix Day is Absent',
			'absent_for_condition' => 'Mark',
			'absent_type' => 'Absent Type',
			'absent_late' => 'Absent when Late for ',
			'halfday_by_late' => 'Mark Half Day if late by',
			'halfday_late_mins' => 'Halfday Late Mins',
			'halfday_by_earlygoing' => 'Mark Half Day if Early going by',
			'halfday_earlygoing_mins' => 'Halfday Earlygoing Mins',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('rset_id',$this->rset_id);
        $criteria->compare('role_id',$this->role_id);
        $criteria->compare('short_name',$this->short_name,true);
        $criteria->compare('ot_formula',$this->ot_formula);
        $criteria->compare('min_ot',$this->min_ot);
        $criteria->compare('max_ot_yes',$this->max_ot_yes);
        $criteria->compare('max_ot',$this->max_ot);
        $criteria->compare('last_inpunch',$this->last_inpunch);
        $criteria->compare('late_coming_gt',$this->late_coming_gt);
        $criteria->compare('early_going_gt',$this->early_going_gt);
        $criteria->compare('weekly_off1_yes',$this->weekly_off1_yes);
        $criteria->compare('weekly_off1',$this->weekly_off1,true);
        $criteria->compare('weekly_off2_yes',$this->weekly_off2_yes);
        $criteria->compare('weekly_off2',$this->weekly_off2,true);
        $criteria->compare('weekly_off2_count',$this->weekly_off2_count,true);
        $criteria->compare('early_coming_punch',$this->early_coming_punch);
        $criteria->compare('late_going_punch',$this->late_going_punch);
        $criteria->compare('first_last_punch',$this->first_last_punch);
        $criteria->compare('deduct_breakhours',$this->deduct_breakhours);
        $criteria->compare('halfday_by_duration',$this->halfday_by_duration);
        $criteria->compare('halfday_duration_mins',$this->halfday_duration_mins);
        $criteria->compare('absent_by_duration',$this->absent_by_duration);
        $criteria->compare('absent_duration_mins',$this->absent_duration_mins);
        $criteria->compare('partial_halfday_by_duration',$this->partial_halfday_by_duration);
        $criteria->compare('partial_halfday_duration_mins',$this->partial_halfday_duration_mins);
        $criteria->compare('partial_absent_by_duration',$this->partial_absent_by_duration);
        $criteria->compare('partial_absent_duration_mins',$this->partial_absent_duration_mins);
        $criteria->compare('absent_for_prefix',$this->absent_for_prefix);
        $criteria->compare('absent_for_suffix',$this->absent_for_suffix);
        $criteria->compare('absent_for_both',$this->absent_for_both);
        $criteria->compare('absent_for_condition',$this->absent_for_condition);
        $criteria->compare('absent_type',$this->absent_type,true);
        $criteria->compare('absent_late',$this->absent_late);
        $criteria->compare('halfday_by_late',$this->halfday_by_late);
        $criteria->compare('halfday_late_mins',$this->halfday_late_mins);
        $criteria->compare('halfday_by_earlygoing',$this->halfday_by_earlygoing);
        $criteria->compare('halfday_earlygoing_mins',$this->halfday_earlygoing_mins);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_date',$this->updated_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
