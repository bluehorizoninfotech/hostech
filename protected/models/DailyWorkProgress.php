<?php

/**
 * This is the model class for table "{{daily_work_progress}}".
 *
 * The followings are the available columns in table '{{daily_work_progress}}':
 * @property integer $id
 * @property integer $project_id
 * @property integer $item_id
 * @property integer $qty
 * @property integer $no_of_workers
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Projects $project
 */
class DailyWorkProgress extends CActiveRecord
{

    public $fromdate;
    public $taskid;
    public $created_by;
    public $current_status;
    public $work_type;
    public $todate;
    public $daily_work_progress;
    public $total_quantity;
    public $from_date;
    public $to_date;
    public $approve_reject_data;
    public $image_path;
    public $image_id;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return DailyWorkProgress the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{daily_work_progress}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs. 
        return array(
            array('date,work_type,taskid,description,qty', 'required', 'message' => 'Please Enter {attribute}'),
            array('project_id', 'required', 'message' => 'Please Select {attribute}'),
            //array('qty', 'safe'),
            array('project_id', 'numerical', 'integerOnly' => true),
            array('created_by,description,taskid,work_type,approve_reject_data,'
                . 'daily_work_progress,created_date,task_id,'
                . 'total_quantity,current_status', 'safe'),
            array('description', 'validate_data'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, project_id,taskid,work_type,date,daily_work_progress,'
                . 'approve_reject_data, item_id,task_id, qty,'
                . 'created_date,total_quantity, no_of_workers, fromdate,'
                . 'todate,created_by,description,img_paths,current_status,'
                . 'latitude,longitude,approve_status,from_date,to_date', 'safe', 'on' => 'search'),

            array('qty', 'validation_msg'),
        );
    }

    public function ignoreifMainTasks($attribute, $params)
    {
        //       var_dump( $this->parent_tskid );
        //        die;
        if ($this->parent_tskid !== '') {
            if ($this->$attribute == '') {
                $this->addError($attribute, 'Enter ' . $this->getAttributeLabel($attribute));
            }
        }
    }

    public function validation_msg($attribute, $params)
    {
        if ($this->$attribute == '') {
            $this->addError($attribute, 'Enter ' . $attribute);
        }
    }

    public function validate_data($attribute, $params)
    {
        if ($this->$attribute == '') {
           
            $this->addError($attribute, 'Enter ' . $attribute);
        }
    }

    /* public function checkQty($attribute, $params) {


      if($this->project_id != null && $this->item_id != null )
      {

      $newqty = $this->qty;
      $firstVal = strtok($this->item_id, ",");
      $sql = Yii::app()->db->createCommand("select id,qty from wpr_items
      where projectid=".$this->project_id." AND sl_no='".$firstVal."'"
      )->queryRow();
      $itemid = $sql['id'];
      $totqty = $sql['qty'];

      $data = Yii::app()->db->createCommand("select sum(qty) as qty from wpr_daily_work_progress
      where project_id=".$this->project_id." AND item_id='".$itemid."'"
      )->queryRow();
      $existqty = $data['qty'];



      $qty = $newqty + $existqty;
      $max = $totqty - $existqty;
      //$str  = json_encode(var_dump($max));
      $this->addError("qty", ($this->item_id));

      if($qty > $totqty)
      {
      $this->addError("qty", "Please enter a less quantity.maximum-".$max."-".$qty."-".$totqty);
      }

      }

      } */

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
            'tasks' => array(self::BELONGS_TO, 'Tasks', 'taskid'),
            'worktype' => array(self::BELONGS_TO, 'WorkType', 'work_type'),
            'items' => array(self::BELONGS_TO, 'Items', 'item_id'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'status0' => array(self::BELONGS_TO, 'Status', 'current_status'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'project_id' => 'Project',
            'taskid' => 'Task: ',
            'task_id' => 'Task ID: ',
            'total_quantity' => 'Total Quantity',
            'item_description' => 'Item Description: ',
            'daily_work_progress' => 'Daily Progress %',
            'work_type' => 'Work Type',
            'item_id' => 'Item',
            'qty' => 'Quantity',
            'no_of_workers' => 'No Of Workers',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'img_paths' => 'images',
            'approve_reject_data' => 'Reason for Rejection',
            'latitude' => 'latitude',
            'longitude' => 'longitude',
            'description'=>'Remarks'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($interval = 0, $status_type = 0)
    {
//echo "<pre>",print_r($this);
$worktype = isset($_GET['DailyWorkProgress']['work_type'])?$_GET['DailyWorkProgress']['work_type']:"";
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        // echo $this->project_id;exit;
        // $this->project_id = '';
        $work_type_condition = '';
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        if (!empty($this->from_date) && empty($this->to_date)) {
            $criteria->condition = "date >= '$this->from_date'";  // date is database date column field
        } elseif (!empty($this->to_date) && empty($this->from_date)) {
            $criteria->condition = "date <= '$this->to_date'";
        } elseif (!empty($this->to_date) && !empty($this->from_date)) {
            $criteria->condition = "date  >= '$this->from_date' and date <= '$this->to_date'";
        }

        if ($status_type !== 'All') {
            $criteria->addCondition('t.approve_status = ' . intval($status_type));
        }

        if (isset($this->date) && !empty($this->date)) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
        $criteria->with = array('tasks');
        $criteria->addSearchCondition('tasks.title', $this->taskid);
        if (isset($this->current_status) && !empty($this->current_status)) {
            $criteria->addSearchCondition('tasks.status', $this->current_status);
        }

        // if ($this->work_type) {
           
        //     $work_type_tbl = WorkType::model()->findByAttributes(array('work_type' => $this->work_type));
        //     if ($work_type_tbl) {
        //         $work_type_condition = $work_type_tbl->wtid;
        //     } else {
        //         $work_type_condition = 0;
        //     }
        // }

        if($worktype )
        {
            $work_type_tbl = WorkType::model()->findByAttributes(array('work_type' => $worktype));
            if ($work_type_tbl) {
                $work_type_condition = $work_type_tbl->wtid;
            } else {
                $work_type_condition = 0;
            }
        }
       
        $criteria->compare('work_type', $work_type_condition);


        if (!empty($this->fromdate) && empty($this->todate)) {
            $criteria->addCondition("date = '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
        } elseif (!empty($this->todate) && empty($this->fromdate)) {
            $criteria->addCondition("date = '" . date('Y-m-d', strtotime($this->todate)) . "'");
        } elseif (!empty($this->todate) && !empty($this->fromdate)) {
            $criteria->addCondition("date between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'");
        }

        $criteria->compare('daily_work_progress', $this->daily_work_progress);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('qty', $this->qty);
        $criteria->compare('t.description', $this->description);
        $criteria->compare('t.approve_reject_data', $this->approve_reject_data);
       // $criteria->compare('t.created_date', $this->created_date, true);

        $criteria->compare('no_of_workers', $this->no_of_workers);


        $criteria->compare('t.created_by', $this->created_by, true);

        if ($this->current_status) {
            $criteria->compare('tasks.status', $this->current_status);
        }
        $criteria->compare('approve_status', $this->approve_status);

        if (isset($this->approve_reject_data) && !empty($this->approve_reject_data)) {
            $criteria->addCondition("t.approve_status=2");
            $criteria->addCondition("t.approve_reject_data like '%" . $this->approve_reject_data . "%'");
        }
        if ($this->project_id != "" && $this->project_id != 0) {
            $criteria->addCondition('t.project_id = ' . $this->project_id);
        } elseif ($this->project_id == 0) {
            $this->project_id = '';
            if (Yii::app()->user->role != 1) {
                $criteria_proj = new CDbCriteria;
                $criteria_proj->select = 'tskid';
                $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria_proj->group = 'tskid';
                $project_ids = Tasks::model()->findAll($criteria_proj);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['tskid']);
                }

                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $criteria->addCondition('t.taskid IN (' . $project_id_array . ')');
                } else {

                    $criteria->addCondition('t.taskid = NULL');
                }
            } else {
                $criteria->compare('t.project_id', $this->project_id, true);
            }
        } elseif (Yii::app()->user->project_id != "") {
            $criteria->addCondition('t.project_id = ' . Yii::app()->user->project_id . '');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.created_date DESC',
            ),
        ));
    }

    public function newsearch($project_id = "", $item_id = "")
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('taskid', $this->taskid);
        $criteria->compare('work_type', $this->work_type);
        $criteria->compare('daily_work_progress', $this->daily_work_progress);
        $criteria->compare('created_date', $this->created_date, true);
        //$criteria->compare('date',$this->date);
        //$criteria->compare('project_id',$this->project_id);
        //$criteria->compare('item_id',$this->item_id);
        $criteria->compare('qty', $this->qty);
        $criteria->compare('description', $this->description);
        $criteria->compare('no_of_workers', $this->no_of_workers);
        $criteria->compare('created_by', $this->created_by);

        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 9) {
            $criteria->compare('created_by', $this->created_by);
        } else {

            $criteria->addCondition('created_by = "' . Yii::app()->user->id . '"');
        }

        if (!empty($project_id)) {
            $criteria->compare('project_id', ($project_id == '' ? $this->projectid : $project_id));
        } else {
            $criteria->compare('project_id', $this->project_id);
        }
        if (!empty($item_id)) {
            $criteria->compare('item_id', ($item_id == '' ? $this->item_id : $item_id));
        } else {
            $criteria->compare('item_id', $this->item_id);
        }


        if (!empty($this->fromdate) && empty($this->todate)) {
            $criteria->addCondition("date = '" . date('Y-m-d', strtotime($this->fromdate)) . "'");  // date is database date column field
        } elseif (!empty($this->todate) && empty($this->fromdate)) {
            $criteria->addCondition("date = '" . date('Y-m-d', strtotime($this->todate)) . "'");
        } elseif (!empty($this->todate) && !empty($this->fromdate)) {
            $criteria->addCondition("date between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'");
        }
        // if (Yii::app()->user->project_id != "") {
        //     $criteria->addCondition('project_id = ' . Yii::app()->user->project_id . '');
        // }

        $criteria->addCondition('approve_status = 1');
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
        ));
    }

    public function getnoofworkers($items_id, $date)
    {
        $noofworkers = 0;
        if (!empty($items_id)) {
            $no_of_workers = Yii::app()->db->createCommand()
                ->select(['pms_daily_work.skilled_workers'])
                ->from('pms_daily_work')
                ->where('pms_daily_work.taskid=' . $items_id . ' AND pms_daily_work.date="' . $date . '"')
                ->queryAll();
            if (count($no_of_workers) == 0) {
                return $noofworkers;
            } else {
                foreach ($no_of_workers as $mainvalue) {
                    foreach ($mainvalue as $value) {
                        $noofworkers += $value;
                    }
                }
                return $noofworkers;
            }
        } else {
            return $noofworkers;
        }
    }

    public function statusLabel($model)
    {
        if ($model->current_status == 0) {
            $task_status = $model->tasks->status;
            if (!empty($task_status)) {
                $criteria = new CDbCriteria;
                $criteria->select = 'caption';
                $criteria->condition = 'sid = ' . $task_status;
                $status_model = Status::model()->find($criteria);
                return $status_model['caption'];
            } else {
                return '';
            }
        } else {
            return $model->status0->caption;
        }
    }

    public function approveStatus($model)
    {
        if ($model->approve_status == 0) {
            return "<span style='color:blue'>Pending for approval</span>";
        } elseif ($model->approve_status == 1) {
            return '<span style="color:green">Approved</span>';
        } elseif ($model->approve_status == 2) {
            return "<span style='color:red'>Rejected</span>";
        } else {
            return '';
        }
    }

    public function getApproveStatuses()
    {
        return array(
            array('id' => '0', 'title' => 'Not Approved'),
            array('id' => '1', 'title' => 'Approved'),
            array('id' => '2', 'title' => 'Rejected'),
        );
    }

    public function pendingRequests($interval = 0, $status_type = 0)
    {


        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        // if(!empty($interval)){			
        // $criteria->compare('date',date('Y-m-d', strtotime($interval)));	
        // }
        if ($status_type !== 'All') {
            $criteria->addCondition('t.approve_status = ' . intval($status_type));
        }
        // elseif($interval == 0){			
        // 	$criteria->compare('date',date('Y-m-d'));	
        // }
        if (isset($this->date) && !empty($this->date)) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
        // $criteria->compare('date',date('Y-m-d', strtotime($this->date)));
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('taskid', $this->taskid);
        $criteria->compare('work_type', $this->work_type);
        $criteria->compare('daily_work_progress', $this->daily_work_progress);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('qty', $this->qty);
        $criteria->compare('description', $this->description);
        $criteria->compare('created_date', $this->created_date, true);
        //$criteria->compare('created_by',$this->created_by);
        $criteria->compare('no_of_workers', $this->no_of_workers);
        // $criteria->addCondition('date=CURDATE() - INTERVAL '.$interval.' day');
        // echo '<pre>';print_r($criteria);exit;		
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('approve_status', $this->approve_status);
        // if(!empty($interval)){
        // $criteria->addCondition('approve_status = 0');	
        // }
        // $criteria->addCondition('created_by = "'.Yii::app()->user->id.'"');

        if (Yii::app()->user->project_id != "") {

            $criteria->addCondition('project_id = ' . Yii::app()->user->project_id . '');
        } else {
            if (Yii::app()->user->role != 1) {
                $criteria_proj = new CDbCriteria;
                $criteria_proj->select = 'project_id';
                $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria_proj->group = 'project_id';
                $project_ids = Tasks::model()->findAll($criteria_proj);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                }

                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $criteria->addCondition('project_id IN (' . $project_id_array . ')');
                } else {

                    $criteria->addCondition('project_id = NULL');
                }
            }
        }
        // echo '<pre>';print_r($criteria);exit;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'created_date DESC',
            ),
        ));
    }

    public function getStatus($task_id)
    {
        $task_model = Tasks::model()->findByPk($task_id);
        if (!empty($task_model)) {
            $task_status = $task_model->status0->caption;
            return $task_status;
        } else {
            return '';
        }
    }
    public function getBalanceQty($task_id, $id)
    {
        $task_model = Tasks::model()->findByPk($task_id);
        $quantity = $task_model->quantity;

        $daily_work_progress_tbl = DailyWorkProgress::model()->tableSchema->rawName;
        $sql = 'select sum(qty) as qty from ' . $daily_work_progress_tbl
            . ' where taskid=:taskid and id<=:id and approve_status != 2';
        $sql_response2 = DailyWorkProgress::model()->findBySql($sql, array(':taskid' => $task_id, ':id' => $id));

        return $quantity - $sql_response2['qty'];
    }

    public function task_search($status_type = 0)
    {
       
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('taskid', $this->taskid);
         if (!empty($this->date)) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
        $criteria->compare('work_type', $this->work_type);
        $criteria->addSearchCondition('daily_work_progress', $this->daily_work_progress);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('qty', $this->qty);
        $criteria->addSearchCondition('description', $this->description);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('no_of_workers', $this->no_of_workers);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'created_date DESC',
            ),
        ));
    }
    public function gallery()
    {
        // $criteria = new CDbCriteria;
        // $criteria->select = 't.*,a.id as area_id,m.id as milestone_id';
        // $criteria->join      = 'LEFT JOIN {{report_images}} i ON t.id=i.wpr_id ';
        // $criteria->join     .= 'INNER JOIN {{milestone}} m ON t.project_id=m.project_id ';
        // $criteria->join .= 'INNER JOIN {{area}} a ON t.project_id=a.project_id';
        // $criteria->addCondition('img_paths IS NOT NULL');

        $criteria = new CDbCriteria;
        $criteria->select = 't.*,a.id as area_id,m.id as milestone_id,w.image_path,w.id as image_id';
        $criteria->join      = 'LEFT JOIN {{report_images}} i ON t.id=i.wpr_id ';

        $criteria->join     .= 'INNER JOIN {{wpr_images}} w ON t.id=w.wpr_id ';

        $criteria->join     .= 'INNER JOIN {{milestone}} m ON t.project_id=m.project_id ';
        $criteria->join .= 'INNER JOIN {{area}} a ON t.project_id=a.project_id';
        $criteria->addCondition('image_status = 1');
        $criteria->group = 'w.id';

        if (isset($_GET['DailyWorkProgress'])) {
            if (isset($this->project_id)) {
                $criteria->compare('t.project_id', $this->project_id);
            }

            if (isset($this->taskid)) {
                $criteria->compare('t.taskid', $this->taskid);
            }
            if (isset($this->work_type)) {
                $criteria->compare('t.work_type', $this->work_type);
            }
            if (isset($_GET['DailyWorkProgress']['area'])) {
                $criteria->compare('a.id', $_GET['DailyWorkProgress']['area']);
            }
            if (isset($_GET['DailyWorkProgress']['milestone'])) {
                $criteria->compare('m.id', $_GET['DailyWorkProgress']['milestone']);
            }
            if (isset($_GET['DailyWorkProgress']['fromdate']) && $_GET['DailyWorkProgress']['todate']) {
                $startdate = $_GET['DailyWorkProgress']['fromdate'];
                $enddate = $_GET['DailyWorkProgress']['todate'];
                if ($startdate != '' && $enddate != '') {
                    $from_date = date('Y-m-d', strtotime($startdate));
                    $to_date = date('Y-m-d', strtotime($enddate));
                    $criteria->addCondition("date >='$from_date' and date <= '$to_date'");
                }
            }

            if (isset($_GET['DailyWorkProgress']['fromdate'])) {

                $startdate = $_GET['DailyWorkProgress']['fromdate'];
                $enddate = $_GET['DailyWorkProgress']['todate'];
                if ($startdate != '' && $enddate == '') {
                    $from_date = date('Y-m-d', strtotime($startdate));
                    $criteria->addCondition("date >='$from_date'");
                }
            }
            if ($_GET['DailyWorkProgress']['todate']) {
                $startdate = $_GET['DailyWorkProgress']['fromdate'];
                $enddate = $_GET['DailyWorkProgress']['todate'];
                if ($enddate != '' && $startdate == '') {
                    $to_date = date('Y-m-d', strtotime($enddate));
                    $criteria->addCondition("date <= '$to_date'");
                }
            }
        }



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.created_date DESC',
            ),
        ));
    }

    public function getDetails($project_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        // $qry = "SELECT d.*,p.name,i.sl_no,i.rate,i.qty as itemqty,t.title
        $qry = "SELECT d.*,p.name,t.title
			   FROM " . $tblpx . "daily_work_progress as d			  
			   left join " . $tblpx . "projects as p on p.pid=d.project_id
               left join " . $tblpx . "tasks as t on t.tskid=d.taskid
	           where d.project_id='" . $project_id . "'";

        $data = Yii::app()->db->createCommand($qry)->queryRow();

        return $data['name'] . " (" . $data['title'] . ")";
    }


    public function getreportStatus($id)
    {
        $get_report = Yii::app()->db->createCommand()
            ->select('*')
            ->from("pms_report_images")
            ->where(
                'wpr_image_id=:wpr_image_id',
                array(':wpr_image_id' => $id)
            )
            ->queryAll();

        if (count($get_report) > 0) {
            return "Included in report";
        }
    }
    public function getItemName($id)
    {
        $specsql = "SELECT id, cat_id,brand_id, specification, unit "
            . " FROM jp_specification "
            . " WHERE id=" . $id . "";
        $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
        $cat_sql = "SELECT * FROM jp_purchase_category "
            . " WHERE id='" . $specification['cat_id'] . "'";
        $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

        if ($specification['brand_id'] != NULL) {
            $brand_sql = "SELECT brand_name "
                . " FROM jp_brand "
                . " WHERE id=" . $specification['brand_id'] . "";
            $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
            $brand = '-' . $brand_details['brand_name'];
        } else {
            $brand = '';
        }
        $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
        return  $item_name;
    }
    public function warehouseApproval($id)
    {

        $account_permission = Tasks::model()->accountPermission();
        if ($account_permission == 1) {
            $status = Yii::app()->db->createCommand()
                ->select('status')
                ->from('pms_acc_wpr_item_consumed')
                ->where('wpr_id=' . $id)
                ->queryScalar();

                if($status != ""){
                    return $status;
                }
                else{
                    return 1;  
                }
           
        } else {
            return 1;
        }
    }
    public function getItemDet($id,$value)
    {

        $Query="SHOW TABLES LIKE 'pms_wpr_item_used'";

        $data=array();
        $checktTableExists = Yii::app()->db->createCommand($Query)->execute();

        if ($checktTableExists) {
            $sql = "SELECT  item_id,item_count "
            . " FROM pms_wpr_item_used where wpr_id=" . $id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($data as $item) {
            $item_id = $item['item_id'];
            $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                . " FROM jp_specification "
                . " WHERE id=" . $item_id . "";
            $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
            $cat_sql = "SELECT * FROM jp_purchase_category "
                . " WHERE id='" . $specification['cat_id'] . "'";
            $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

            if ($specification['brand_id'] != NULL) {
                $brand_sql = "SELECT brand_name "
                    . " FROM jp_brand "
                    . " WHERE id=" . $specification['brand_id'] . "";
                $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                $brand = '-' . $brand_details['brand_name'];
            } else {
                $brand = '';
            }
            $item_name[] = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
            $item_count[]=$item['item_count'];
        }
        }
        else
        {
            $item_name=[] ;
            $item_count=[];
        }

        

       // 
       if($value==0)
       {
        if(count($data)>0)
        {
            return implode(',', $item_name);
        }
        else
        {
           
        }
        
       }
       else
       {
        if(count($data)>0)
        {return implode(',',$item_count);
        }
        else
        {
            return "";
        }
        
       }
       
    }
    public function weeklyreport()
    {
        if (isset($_REQUEST['DailyWorkProgress'])) {
        }
        else
        {
            echo 456;
        }
        $where = '';
        $fields = " te.teid as id, p.pid AS project_id, p.name AS project, 
                tsk.tskid AS task_id, tsk.title AS task,
		te.description, wt.work_type, DATE_FORMAT(entry_date, '%d-%b-%y') as entry_date, te.hours, 
                if(te.billable=3,'Yes','No') as billable, te.user_id, 
                u.first_name as username, site.site_name,te.approve_status";
        $query = " FROM `pms_time_entry` AS te
		 INNER JOIN pms_tasks AS tsk ON tsk.`tskid` = te.tskid
		 LEFT JOIN pms_projects AS p ON p.pid = tsk.project_id
		 INNER JOIN pms_users as u on u.userid=te.user_id
		 INNER JOIN pms_work_type AS wt ON wt.wtid = te.work_type 
                 LEFT JOIN pms_clientsite site ON site.id=tsk.clientsite_id  "
            . $where;

            $count = Yii::app()->db->createCommand("SELECT count(*) " . $query)->queryScalar();
        
        return $dataProvider = new CSqlDataProvider("SELECT $fields " . $query, array(
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tsk.start_date DESC',
                'attributes' => array(
                    'project', 'task', 'entry_date', 'billable', 'site_name' // csv of sortable column names
                )
            ), 'totalItemCount' => $count
        ));
        
    }

    public function reportSearch($id)
    {
        
        $where = " where tsk.project_id=0";
        $condition = array();
        
        if(isset($_REQUEST['DailyWorkProgress']))
        {
            
            if (isset($_REQUEST['DailyWorkProgress']['project_id']) and ($_REQUEST['DailyWorkProgress']['project_id']) != '')
            {
                
                $condition[] = 'tsk.project_id=' . $_REQUEST['DailyWorkProgress']['project_id'];
                $where = " WHERE " . implode(' AND ', $condition);
            }
        }
        if($id !="")
        {
            $condition[] = 'tsk.project_id=' . $id ;
            $where = " WHERE " . implode(' AND ', $condition);
        }

        $query =" SELECT SUM(qty) as total_qty,title,pj.name,pj.start_date,pj.end_date,MIN(`date`) min_date,MAX(`date`) AS max_date,quantity,qty,tsk.unit,tsk.work_type_id,tsk.description,tsk.tskid,dw.project_id,dw.id,task_type,tskid,tsk.start_date as task_start_date,tsk.due_date as task_due_date FROM pms_tasks tsk
        LEFT JOIN pms_daily_work_progress AS dw ON dw.taskid = tsk.tskid 
        INNER JOIN pms_projects AS pj ON pj.pid=tsk.project_id"
        .$where. " GROUP BY tsk.tskid";

        $data=Yii::app()->db->createCommand($query)->queryAll();

        return $data;

       // $count = Yii::app()->db->createCommand("SELECT count(*) " . $query)->queryScalar();

        // return $dataProvider = new CSqlDataProvider("SELECT $fields " . $query, array(
        //     'pagination' => array('pageSize' => 20),
        //     'sort' => array(
        //         'defaultOrder' => 'tsk.tskid DESC',
        //         'attributes' => array(
        //             'project', 'task', 'entry_date', 'billable', 'site_name' // csv of sortable column names
        //         )
        //     ), 'totalItemCount' => $count
        // ));
    }

   
}
