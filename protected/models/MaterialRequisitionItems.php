<?php

/**
 * This is the model class for table "{{material_requisition_items}}".
 *
 * The followings are the available columns in table '{{material_requisition_items}}':
 * @property integer $id
 * @property integer $mr_id
 * @property integer $item_id
 * @property string $item_name
 * @property string $item_unit
 * @property integer $item_quantity
 * @property string $item_req_date
 * @property string $item_remarks
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_at
 */
class MaterialRequisitionItems extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MaterialRequisitionItems the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{material_requisition_items}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('item_req_date', 'required'),
            array('mr_id, item_id, item_quantity, created_by, updated_by', 'numerical', 'integerOnly' => true),
            array('item_name, item_unit, item_remarks', 'length', 'max' => 200),
            array('created_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, mr_id, item_id, item_name, item_unit, item_quantity, item_req_date, item_remarks, created_by, created_date, updated_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'mr_id' => 'Mr',
            'item_id' => 'Item',
            'item_name' => 'Item Name',
            'item_unit' => 'Item Unit',
            'item_quantity' => 'Item Quantity',
            'item_req_date' => 'Item Req Date',
            'item_remarks' => 'Item Remarks',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            //'updated_at' => 'Updated At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($id)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('mr_id', $this->mr_id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('item_name', $this->item_name, true);
        $criteria->compare('item_unit', $this->item_unit, true);
        $criteria->compare('item_quantity', $this->item_quantity);
        $criteria->compare('item_req_date', $this->item_req_date, true);
        $criteria->compare('item_remarks', $this->item_remarks, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        //$criteria->compare('updated_at',$this->updated_at,true);
        $criteria->addCondition('mr_id = ' . $id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    public function getItemName($item_id, $id)
    {
        //$data = MaterialRequisitionItems::model->findByPK($item_id);
        $data = MaterialRequisitionItems::model()->findByPK($id);
        if ($item_id == 0) {
            $item_name = $data->item_name;
        } else {

            $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                . " FROM jp_specification "
                . " WHERE id=" . $item_id . "";
            $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
            $cat_sql = "SELECT * FROM jp_purchase_category "
                . " WHERE id='" . $specification['cat_id'] . "'";
            $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

            if ($specification['brand_id'] != NULL) {
                $brand_sql = "SELECT brand_name "
                    . " FROM jp_brand "
                    . " WHERE id=" . $specification['brand_id'] . "";
                $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                $brand = '-' . $brand_details['brand_name'];
            } else {
                $brand = '';
            }
            $item_name = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
        }

        return $item_name;
    }
}
