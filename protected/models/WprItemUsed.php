<?php

/**
 * This is the model class for table "{{wpr_item_used}}".
 *
 * The followings are the available columns in table '{{wpr_item_used}}':
 * @property integer $id
 * @property integer $wpr_id
 * @property integer $task_id
 * @property integer $item_id
 * @property integer $item_rate_id
 * @property integer $item_count
 * @property integer $template_item_id
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_rate
 */
class WprItemUsed extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WprItemUsed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wpr_item_used}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_by, updated_by', 'required'),
			array('wpr_id, task_id, item_id, item_rate_id, item_count, template_item_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('created_date, updated_rate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, wpr_id, task_id, item_id, item_rate_id, item_count, template_item_id, created_by, created_date, updated_by, updated_rate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'wpr_id' => 'Wpr',
			'task_id' => 'Task',
			'item_id' => 'Item',
			'item_rate_id' => 'Item Rate',
			'item_count' => 'Item Count',
			'template_item_id' => 'Template Item',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_rate' => 'Updated Rate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('wpr_id',$this->wpr_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('item_rate_id',$this->item_rate_id);
		$criteria->compare('item_count',$this->item_count);
		$criteria->compare('template_item_id',$this->template_item_id);
		$criteria->compare('created_by',$this->created_by);
		//$criteria->compare('created_date',$this->created_date,true);
		// $criteria->compare('updated_by',$this->updated_by);
		// $criteria->compare('updated_rate',$this->updated_rate,true);

		//$criteria->addCondition('wpr_id ='.$wpr_id);
		if ($this->wpr_id) {

            $criteria->compare('wpr_id', $this->wpr_id);
         }
		else {

            $criteria->compare('wpr_id', 0);
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getItemDet($id,$value)
    {

        $Query="SHOW TABLES LIKE 'pms_wpr_item_used'";

        $data=array();
        $checktTableExists = Yii::app()->db->createCommand($Query)->execute();

        if ($checktTableExists) {
            $sql = "SELECT  item_id,item_count "
            . " FROM pms_wpr_item_used where item_id=" . $id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();

        
            $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                . " FROM jp_specification "
                . " WHERE id=" . $id . "";
            $specification  = Yii::app()->db->createCommand($specsql)->queryRow();
            $cat_sql = "SELECT * FROM jp_purchase_category "
                . " WHERE id='" . $specification['cat_id'] . "'";
            $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

            if ($specification['brand_id'] != NULL) {
                $brand_sql = "SELECT brand_name "
                    . " FROM jp_brand "
                    . " WHERE id=" . $specification['brand_id'] . "";
                $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                $brand = '-' . $brand_details['brand_name'];
            } else {
                $brand = '';
            }
            $item_name[] = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
           
       
        }
        else
        {
            $item_name=[] ;
            $item_count=[];
        }
		if($value==0)
       {
        if(count($data)>0)
        {
            return implode(',', $item_name);
        }
        else
        {
           return "";
        }
        
       }
       else
       {
        if(count($data)>0)
        {
			return implode(',',$item_count);
        }
        
        
       }
       
    }
	public function getItemRate($rate_id)
	{
		$sql = "SELECT rate from jp_warehousestock where warehousestock_id=" . $rate_id ;
        $data = Yii::app()->db->createCommand($sql)->queryRow();
		$rate="";
		if($data)
		{
			$rate= $data['rate'];
		}
		return $rate;
	}
}