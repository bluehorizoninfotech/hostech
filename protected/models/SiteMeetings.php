<?php

/**
 * This is the model class for table "{{site_meetings}}".
 *
 * The followings are the available columns in table '{{site_meetings}}':
 * @property integer $id
 * @property integer $project_id
 * @property integer $site_id
 * @property string $date
 * @property string $venue
 * @property string $objective
 * @property string $next_meeting_date
 * @property string $time
 * @property string $location
 * @property string $note
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property string $distribution
 * @property integer $approved_status

 * The followings are the available model relations:
 * @property MeetingMinutes[] $meetingMinutes
 * @property MeetingParticipants[] $meetingParticipants
 * @property Projects $project
 * @property Clientsite $site
 */
class SiteMeetings extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SiteMeetings the static model class
	 */

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{site_meetings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id,objective,date,venue,meeting_time,meeting_number', 'required', 'on' => 'meeting_start'),
			array('objective,date,venue', 'required', 'on' => 'meeting_next'),
			// array('note,', 'required', 'on' => 'meeting_final'),
			array('project_id, site_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('venue, location', 'length', 'max' => 200),
			array('date', 'validateCustomDate'),
			array('date, next_meeting_date, time,next_meeting_time, note, created_date, updated_date,meeting_time,meeting_number,approved_status', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_id, site_id, date, venue, objective, next_meeting_date, time, location, note, created_by, created_date, updated_by, updated_date,next_meeting_time', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'meetingMinutes' => array(self::HAS_MANY, 'MeetingMinutes', 'meeting_id'),
			'meetingParticipants' => array(self::HAS_MANY, 'MeetingParticipants', 'meeting_id'),
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'site' => array(self::BELONGS_TO, 'Clientsite', 'site_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'approve_decision_by'),
			'created' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'site_id' => 'Site',
			'date' => 'Date',
			'venue' => 'Venue',
			'objective' => 'Objective',
			'next_meeting_date' => 'Next Meeting Date',
			'next_meeting_time' => 'Next Meeting Time',
			'location' => 'Location',
			'note' => 'Note',
			'meeting_time' => 'Time',
			'meeting_number' => 'Meeting Number',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'distribution' => 'Distribution'

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('site_id', $this->site_id, true);
		$criteria->compare('approved_status', $this->approved_status);
		if (isset($this->date) && !empty($this->date)) {
			$criteria->compare('date', date('Y-m-d', strtotime($this->date)));
		}
		if (isset($this->created_date) && !empty($this->created_date)) {
			$criteria->compare('created_date', date('Y-m-d', strtotime($this->created_date)));
		}
		$criteria->compare('venue', $this->venue, true);
		$criteria->compare('objective', $this->objective, true);
		$criteria->compare('next_meeting_date', $this->next_meeting_date, true);
		$criteria->compare('next_meeting_time', $this->next_meeting_time, true);
		$criteria->compare('location', $this->location, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$project_ids = Yii::app()->myClass->project_ids();
		if (Yii::app()->user->role != 1) {
			// if (!empty($project_ids))
			// 	$criteria->addCondition('project_id IN (' . $project_ids . ')', 'AND');

			$condition = 'FIND_IN_SET(' . Yii::app()->user->id . ',approved_by) OR created_by=' . Yii::app()->user->id;
			$criteria->addCondition($condition);
		}
		return new CActiveDataProvider(
			$this,
			array(
				'criteria' => $criteria,
			)
		);
	}
	public function getCheckbox($id)
	{
		$condition = 'FIND_IN_SET(' . Yii::app()->user->id . ',approved_by)  AND id = ' . $id;
		$Criteria = new CDbCriteria();
		$Criteria->condition = $condition;
		$meeting = SiteMeetings::model()->findAll($Criteria);

		if (count($meeting) > 0) {
			return 1;
		} else {
			return 0;
		}
	}
	public function validateCustomDate($attribute, $params)
	{
		$value = $this->$attribute;
		if (!strtotime($value)) {
			$this->addError($attribute, 'The date must be in a valid format.');
		}
	}
}
