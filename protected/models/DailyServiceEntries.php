<?php

/**
 * This is the model class for table "{{daily_sevice_entries}}".
 *
 * The followings are the available columns in table '{{daily_sevice_entries}}':
 * @property integer $entry_id
 * @property integer $project_id
 * @property string $date
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Projects $project
 */
class DailyServiceEntries extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DailyServiceEntries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{daily_sevice_entries}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, description,project_id', 'required'),
			array('project_id', 'numerical', 'integerOnly'=>true),
                        array('created_by', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('entry_id, project_id, date, description,created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
                        'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'entry_id' => 'Entry',
			'project_id' => 'Project',
			'date' => 'Date',
			'description' => 'Description',
                        'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('entry_id',$this->entry_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('description',$this->description,true);
               	if(Yii::app()->user->role == 1 || Yii::app()->user->role == 9){
            $criteria->compare('created_by',$this->created_by);
        }else{
			
			$criteria->addCondition('created_by = "'.Yii::app()->user->id.'"');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}