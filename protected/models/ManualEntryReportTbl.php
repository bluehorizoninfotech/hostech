<?php

/**
 * This is the model class for table "manual_entry_report_tbl".
 *
 * The followings are the available columns in table 'manual_entry_report_tbl':
 * @property integer $entry_id
 * @property integer $emp_id
 * @property string $date
 * @property string $comment
 * @property string $status
 * @property string $type
 * @property integer $created_by
 * @property integer $decision_by
 */
class ManualEntryReportTbl extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{manual_entry_report_view}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id,employee_name, type', 'required'),
            array('emp_id, created_by, decision_by', 'numerical', 'integerOnly' => true),
            array('date', 'length', 'max' => 20),
            array('status, type', 'length', 'max' => 10),
            array('comment,entry_id_with_type,decision_by_name,created_by_name, type_char, status_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('entry_id,decision_by_name,created_by_name,employee_name, '
                . 'emp_id, date, comment, status, type, created_by, '
                . 'decision_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Emplid' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'entry_id' => 'Entry',
            'emp_id' => 'Emp',
            'employee_name' => 'Employee Name',
            'date' => 'Date',
            'comment' => 'Comment',
            'status' => 'Status',
            'type' => 'Type',
            'created_by' => 'Created By',
            'decision_by' => 'Decision By',
        );
    }

    public static function getManualReportActionlink($status, $type, $entry_id) {
        if ($type == 'Manual Punch') {
            return CHtml::link('Approve/Reject', array("/manualEntry/view", "id" => $entry_id));
        } else if ($type = "Ignore Punch") {
            return CHtml::link('Approve/Reject', array("/attendance/IgnorePunches/view", "id" => $entry_id));
        } else if ($type = "Shift Punch") {
            return CHtml::link('Approve/Reject', array("/shiftEntry/view", "id" => $entry_id));
        } else {
            return '-';
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($status_type = 'All') {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('entry_id', $this->entry_id);
        $criteria->compare('emp_id', $this->emp_id);
        $criteria->compare('employee_name', $this->employee_name, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('decision_by', $this->decision_by);
        $criteria->compare('created_by_name', $this->created_by_name, true);
        $criteria->compare('decision_by_name', $this->decision_by_name, true);
        // $criteria->order = 'entry_id desc';
        
        if($this->date!=''){
            $criteria->addCondition('date_format(date, "%Y-%m-%d") = "'.$this->date.'"');            
        }
        
        //Project site filter
        if (isset(Yii::app()->user->site_userids) and Yii::app()->user->site_userids != '') {
            $criteria->addCondition('emp_id in ( ' . Yii::app()->user->site_userids . ' )');
        }

        //Skill filter
        $skill_filter_id = Yii::app()->user->getState('skill_filter_id', "");
        if ($skill_filter_id != 0 and isset(Yii::app()->user->skill_emp_ids)) {
            $criteria->addCondition('emp_id in ( ' . Yii::app()->user->skill_emp_ids . ' )');
        }
        
        //Designation filter
        $desig_filter_id = Yii::app()->user->getState('desig_filter_id', "");
        
        if ($desig_filter_id != 0 and isset(Yii::app()->user->desig_emp_ids)) {
            $criteria->addCondition('emp_id in ( ' . Yii::app()->user->desig_emp_ids . ' )');
        }

        if ($status_type != 'All') {
            $criteria->addCondition('status ="' . $status_type . '"');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->user->getState('reportpagesize', 20),)
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ManualEntryReportTbl the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
