<?php

/**
 * This is the model class for table "{{project_participants}}".
 *
 * The followings are the available columns in table '{{project_participants}}':
 * @property integer $id
 * @property integer $project_id
 * @property integer $contractor_id
 * @property string $participants
 * @property string $participant_initial
 * @property string $designation
 * @property string $organization_name
 * @property string $organization_initial
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class ProjectParticipants extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectParticipants the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_participants}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, contractor_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('participant_initial, organization_name, organization_initial', 'length', 'max'=>50),
			array('participants, designation, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_id, contractor_id, participants, participant_initial, designation, organization_name, organization_initial, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'contractor_id' => 'Contractor',
			'participants' => 'Participants',
			'participant_initial' => 'Participant Initial',
			'designation' => 'Designation',
			'organization_name' => 'Organization Name',
			'organization_initial' => 'Organization Initial',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('contractor_id',$this->contractor_id);
		$criteria->compare('participants',$this->participants,true);
		$criteria->compare('participant_initial',$this->participant_initial,true);
		$criteria->compare('designation',$this->designation,true);
		$criteria->compare('organization_name',$this->organization_name,true);
		$criteria->compare('organization_initial',$this->organization_initial,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}