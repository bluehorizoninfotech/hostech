<?php

/**
 * This is the model class for table "{{leave_log}}".
 *
 * The followings are the available columns in table '{{leave_log}}':
 * @property integer $id
 * @property integer $userid
 * @property double $cl_credited
 * @property double $er_credited
 * @property double $cu_cl
 * @property double $cu_er
 * @property string $created_date
 * @property integer $created_by
 * @property string $comment
 */
class LeaveLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LeaveLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{leave_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, cl_credited, er_credited, cu_cl, cu_er, created_date, comment', 'required'),
			array('userid, created_by', 'numerical', 'integerOnly'=>true),
			// array('cl_credited, er_credited, cu_cl, cu_er', 'numerical'),
			array('comment', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userid, cl_credited, er_credited, cu_cl, cu_er, created_date, created_by, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userid' => 'Userid',
			'cl_credited' => 'Cl Credited',
			'er_credited' => 'Er Credited',
			'cu_cl' => 'Cu Cl',
			'cu_er' => 'Cu Er',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('cl_credited',$this->cl_credited);
		$criteria->compare('er_credited',$this->er_credited);
		$criteria->compare('cu_cl',$this->cu_cl);
		$criteria->compare('cu_er',$this->cu_er);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
