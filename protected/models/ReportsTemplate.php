<?php

/**
 * This is the model class for table "{{reports_template}}".
 *
 * The followings are the available columns in table '{{reports_template}}':
 * @property integer $template_id
 * @property string $template_name
 * @property integer $template_type
 * @property string $status
 * @property string $description
 * @property string $template_format
 * @property integer $created_by
 * @property string $updated_date
 * @property string $created_date
 */
class ReportsTemplate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReportsTemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{reports_template}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('template_name, template_type, description, created_by', 'required'),
			array('template_type, created_by', 'numerical', 'integerOnly'=>true),
			array('template_name, description', 'length', 'max'=>100),
			array('status', 'length', 'max'=>1),
			array('template_format, updated_date, created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('template_id, template_name, template_type, status, description, template_format, created_by, updated_date, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'template_id' => 'Template',
			'template_name' => 'Template Name',
			'template_type' => 'Template Type',
			'status' => 'Status',
			'description' => 'Description',
			'template_format' => 'Template Format',
			'created_by' => 'Created By',
			'updated_date' => 'Updated Date',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('template_id',$this->template_id);
		$criteria->compare('template_name',$this->template_name,true);
		$criteria->compare('template_type',$this->template_type);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('template_format',$this->template_format,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}