<?php

/**
 * This is the model class for table "{{punch_request}}".
 *
 * The followings are the available columns in table '{{punch_request}}':
 * @property double $aid
 * @property double $logid
 * @property integer $sqllogid
 * @property integer $empid
 * @property string $log_time
 * @property integer $status
 * @property integer $device_id
 * @property integer $manual_entry_status
 * @property string $image
 * @property string $latitude
 * @property string $longitude
 * @property integer $punch_type
 * @property integer $work_site_id
 * @property string $work_site_name
 */
class PunchRequest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PunchRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{punch_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('empid, log_time, device_id', 'required'),
			array('sqllogid, empid, status, device_id, manual_entry_status, punch_type, work_site_id', 'numerical', 'integerOnly'=>true),
			array('logid', 'numerical'),
			array('image, latitude, longitude, work_site_name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('aid, logid, sqllogid, empid, log_time, status, device_id, manual_entry_status, image, latitude, longitude, punch_type, work_site_id, work_site_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'aid' => 'Aid',
			'logid' => 'Logid',
			'sqllogid' => 'Sqllogid',
			'empid' => 'Empid',
			'log_time' => 'Log Time',
			'status' => 'Status',
			'device_id' => 'Device',
			'manual_entry_status' => 'Manual Entry Status',
			'image' => 'Image',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'punch_type' => 'Punch Type',
			'work_site_id' => 'Work Site',
			'work_site_name' => 'Work Site Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('aid',$this->aid);
		$criteria->compare('logid',$this->logid);
		$criteria->compare('sqllogid',$this->sqllogid);
		$criteria->compare('empid',$this->empid);
		$criteria->compare('log_time',$this->log_time,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('manual_entry_status',$this->manual_entry_status);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('punch_type',$this->punch_type);
		$criteria->compare('work_site_id',$this->work_site_id);
		$criteria->compare('work_site_name',$this->work_site_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}