<?php

/**
 * This is the model class for table "{{project_report}}".
 *
 * The followings are the available columns in table '{{project_report}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $from_date
 * @property string $to_date
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Projects $project
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class ProjectReport extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectReport the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_report}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, from_date, report_type,to_date,created_by, updated_by,created_date, updated_date', 'required'),
			//array('project_id, from_date, report_type,to_date', 'checkExist'),
			array('project_id, status, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('from_date, to_date, created_date, updated_date', 'safe'),
			array('from_date', 'dateValidation'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_id, from_date, to_date, status, created_by, created_date, updated_by, updated_date,report_type', 'safe', 'on' => 'search'),
		);
	}

	public function checkExist($attribute, $params)
	{
		if (!empty($this->project_id) && !empty($this->from_date) && !empty($this->to_date) && !empty($this->report_type)) {
			$model = ProjectReport::model()->findAll(array('condition' => 'project_id = ' . $this->project_id . ' AND report_type = ' . $this->report_type . ' AND from_date = "' . date('Y-m-d', strtotime($this->from_date)) . '" AND to_date = "' . date('Y-m-d', strtotime($this->to_date)) . '"'));
			if (!empty($model)) {
				$this->addError('project_id', 'Report Already exist');
			}
		}
	}
	public function dateValidation($attribute, $params)
	{
		if (!empty($this->from_date)) {

			if ($this->from_date > date('Y-m-d')) {
				$this->addError('from_date', 'From date should not exceed todays date');
			}
		}
		if (!empty($this->to_date)) {
			if ($this->to_date > date('Y-m-d')) {
				$this->addError('to_date', 'To date should not exceed  todays date');
			}
		}
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'from_date' => 'From Date',
			'to_date' => 'To Date',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.		
		$criteria = new CDbCriteria;
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('from_date', $this->from_date, true);
		$criteria->compare('to_date', $this->to_date, true);
		if ($this->report_type) {
			$criteria->compare('report_type', $this->report_type);
		}
		$criteria->addCondition('to_date != :date');
		$criteria->params[':date'] = '1970-01-01';
		$criteria->addCondition('report_type IN(1,2,3)');
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'created_date DESC',
			)
		));
	}
	public function daily_progress_report()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('from_date', $this->from_date, true);
		$criteria->compare('to_date', $this->to_date, true);
		if ($this->report_type) {
			$criteria->compare('report_type', $this->report_type);
		}
		$criteria->addCondition('to_date != :date');
		$criteria->params[':date'] = '1970-01-01';
		$criteria->addCondition('report_type IN(4)');
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'created_date DESC',
			)
		));
	}
	public function weekly_report()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('from_date', $this->from_date, true);
		$criteria->compare('to_date', $this->to_date, true);
		if ($this->report_type) {
			$criteria->compare('report_type', $this->report_type);
		}
		$criteria->addCondition('to_date != :date');
		$criteria->params[':date'] = '1970-01-01';
		$criteria->addCondition('report_type IN(5)');
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'created_date DESC',
			)
		));
	}
}
