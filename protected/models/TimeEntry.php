<?php

/**
 * This is the model class for table "{{time_entry}}".
 *
 * The followings are the available columns in table '{{time_entry}}':
 * @property integer $teid
 * @property integer $user_id
 * @property integer $tskid
 * @property integer $work_type
 * @property integer $entry_date
 * @property double $hours
 * @property integer $billable
 * @property string $description
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property WorkType $workType
 * @property Tasks $tsk
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Users $user
 */
class TimeEntry extends CActiveRecord {

    //Default selection
    public $billable = 3; //checkbox checked
    public $tentry_date;
    public $ddate;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TimeEntry the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{time_entry}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
           // array('user_id, tskid, work_type, entry_date, hours, billable, description,completed_percent', 'required'),
            array('user_id, tskid, entry_date,description,completed_percent', 'required'),
            array('start_time', 'validatestarttime'),
            array('end_time', 'validateendtime'),
            array('user_id, tskid, work_type, billable, created_by, updated_by,current_status', 'numerical', 'integerOnly' => true),
            // /array('entry_date', 'date', 'format' => array('yyyy-MM-dd', 'yyyy-MM-d')),
            // array('hours', 'numerical'),
            array('hours,billable,current_status', 'safe'),
           // array('hours', 'validatehour'),
            array('entry_date', 'validatedate'),
            array('entry_date', 'datevalidation'),
			array('completed_percent','numerical',
				'integerOnly'=>false,
				'min'=>1,
				'max'=>100,
				'tooSmall'=>'Enter greater than 1',
				'tooBig'=>'Enter <= 100'),
            //array('description', 'length', 'min' => 10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('teid,current_status, user_id, tskid, entry_date, hours, billable, description, created_date, created_by, updated_date, updated_by,completed_percent', 'safe', 'on' => 'search'),
        );
    }

    public function validatehour($attribute, $params) {
        $hours = floatval($this->$attribute);
        if ($hours == 0 || $hours > 24)
            $this->addError($attribute, 'Enter valid time' . $hours);
    }

    public function validatedate($attribute, $params) {        
        $edate = $this->$attribute;
        $selected_date = date('Y-m-d',strtotime($edate));
        $ddiff = (strtotime($edate) - strtotime(date('Y-m-d'))) / (3600 * 24);
        if($this->tskid != ""){       
            if (Yii::app()->user->role != 1 ) {
                if (($ddiff > 1 and !isset(Yii::app()->user->mainuser_id)))
                    $this->addError($attribute, 'Enter valid Date. (Entry date can\'t be in future days)');
                else if (($ddiff < -7 and !isset(Yii::app()->user->mainuser_id)))
                    $this->addError($attribute, 'Enter valid Date. (Entry date must be within today and past 7 days)');
                else {
                    $sql = "SELECT count(*) as date_range FROM pms_tasks WHERE ( '$edate' BETWEEN `start_date` AND `due_date`) AND tskid=" . intval($this->tskid);
                    $count_tsk = Yii::app()->db->createCommand($sql)->queryScalar();                 

                    if ($count_tsk == 0) {

                        $sql = "SELECT concat_ws(' & ',`start_date`,`due_date`) as date_range FROM pms_tasks WHERE ( '$selected_date' BETWEEN `start_date` AND `due_date`) AND tskid=" . intval($this->tskid);
                    
                        $date_range = Yii::app()->db->createCommand($sql)->queryScalar();
                       
                    if(empty($date_range) && $this->scenario!='expiretimeentry'){
                        $sql = "SELECT `start_date`,`due_date` FROM pms_tasks WHERE tskid=" . intval($this->tskid);
                        $dates = Yii::app()->db->createCommand($sql)->queryRow();
                        // echo '<pre>';print_r($dates);exit;
                        $this->addError($attribute, 'Invalid date1. <br />Selected task\'s Start & Due date: <b>' . date('d-M-y',strtotime($dates['start_date'])).' & '.date('d-M-y',strtotime($dates['due_date'])).'</b>');
                    }
                    
                    }
                }
            }
        }
    }


    public function validatestarttime($attribute, $params) {        
        $start_time = $this->start_time;
        
        if (in_array('/timeEntry/timevalidation', Yii::app()->session['menuauthlist'])){
            if($start_time==''){
                $this->addError($attribute, 'Please enter start time');
                return false;
            }else{
                return true;
            }
            
        }
    }

    public function validateendtime($attribute, $params) {        
        $end_time = $this->end_time;
        
        if (in_array('/timeEntry/timevalidation', Yii::app()->session['menuauthlist'])){
            if($end_time==''){
                $this->addError($attribute, 'Please enter end time');
                return false;
            }else{
                return true;
            }
            
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'workType' => array(self::BELONGS_TO, 'WorkType', 'work_type'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'tsk' => array(self::BELONGS_TO, 'Tasks', 'tskid'),
            'status' => array(self::BELONGS_TO, 'Status', 'current_status'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'teid' => 'Teid',
            'user_id' => 'User',
            'tskid' => 'Select task',
            'entry_date' => 'Entry Date',
            'work_type' => 'Work Type',
            'hours' => 'Hours',
            'billable' => 'Billable',
            'description' => 'Description',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By',
            'completed_percent' => 'Progress %',
            'current_status'=> 'Current Status'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('teid', $this->teid);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('tskid', $this->tskid);
        $criteria->compare('work_type', $this->work_type);
        $criteria->compare('entry_date', $this->entry_date);
        $criteria->compare('hours', $this->hours);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25,),
            'criteria' => $criteria,
        ));
    }

    public function pendingrequests() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('teid', $this->teid);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('tskid', $this->tskid);
        $criteria->compare('work_type', $this->work_type);
        // $criteria->compare('entry_date', $this->entry_date);
        $criteria->compare('hours', $this->hours);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->addCondition('approve_status=0');
        if(isset($this->entry_date) && !empty($this->entry_date)){                     
			$criteria->compare('entry_date', date('Y-m-d', strtotime($this->entry_date)));
		}

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 10,),
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'teid desc',
            )
        ));
    }
function datevalidation($attribute)
{
    $todays_date=date('j-F-Y');
    if ($this->$attribute != "") {
        $date = strtotime($this->$attribute);
        $date_from = date('Y-m-d ', $date);
        
    if(strtotime($this->$attribute) > strtotime($todays_date))	
    {
        $msg= date('j-F-Y',strtotime($todays_date));
        $this->addError('entry_date', 'Date should be less than or equal to '. $msg);
    }
        
    }
}
}