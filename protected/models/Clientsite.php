<?php

/**
 * This is the model class for table "{{clientsite}}".
 *
 * The followings are the available columns in table '{{clientsite}}':
 * @property integer $id
 * @property string $site_name
 *
 * The followings are the available model relations:
 * @property PunchingDevices[] $punchingDevices
 * @property Usersite[] $usersites
 */
class Clientsite extends CActiveRecord
{

	public $other_users;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Clientsite the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{clientsite}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('site_name,latitude,longitude,distance,pid', 'required', 'on' => 'project_site'),
			array('site_name,latitude,longitude,distance,', 'required', 'on' => 'general_site'),
			array('site_name,latitude,longitude,created_by,created_date', 'required'),
			array('site_name', 'length', 'max' => 50),
			array('site_name', 'unique'),
			array('latitude', 'numerical', 'integerOnly' => false),
			array('longitude', 'numerical', 'integerOnly' => false),
			array('distance', 'numerical', 'integerOnly' => false),

			array('site_name', 'checkexit'),
			array('id, site_name, pid, latitude, longitude,distance', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, site_name, pid, latitude, longitude', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'p' => array(self::BELONGS_TO, 'Projects', 'pid'),
			'punchingDevices' => array(self::HAS_MANY, 'PunchingDevices', 'site_id'),
			'usersites' => array(self::HAS_MANY, 'Usersite', 'site_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'site_name' => 'Site Name',
			'pid' => 'Project Name',
			'other_users' => 'Other Project Users',
			'distance' => 'Punching Radius(meter)'

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('site_name', $this->site_name, true);
		// $criteria->compare('pid', $this->pid);
		if($this->pid)
        {
            
            $criteria->compare('pid', $this->pid);            
        }
        else
        {
            $criteria->compare('pid', 0);  
        }
		$criteria->compare('latitude', $this->latitude);
		$criteria->compare('longitude', $this->longitude);
		$criteria->compare('distance', $this->distance);

		if (Yii::app()->controller->action->id == "generalSite") {

			$criteria->addCondition('pid is null');
		} else {
			$criteria->addCondition('pid is not null');
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 20,
			),
		));
	}

	public function searchClientSite()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('site_name', $this->site_name, true);
	    $criteria->compare('pid', $this->pid);
		
		$criteria->compare('latitude', $this->latitude);
		$criteria->compare('longitude', $this->longitude);
		$criteria->compare('distance', $this->distance);

		if (Yii::app()->controller->action->id == "generalSite") {

			$criteria->addCondition('pid is null');
		} else {
			$criteria->addCondition('pid is not null');
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 20,
			),
		));
	}

	public function getassignedusers($pid = 0)
	{
		$assgnuser = array();
		$query = "SELECT DISTINCT CONCAT_WS(' ', `first_name`, `last_name`) AS `whole_name`,userid FROM `pms_users` inner join pms_clientsite_assigned on pms_clientsite_assigned.user_id=pms_users.userid inner join pms_clientsite on pms_clientsite.id=pms_clientsite_assigned.site_id WHERE pms_users.status=0  AND pms_clientsite.id='" . $pid . "' ORDER BY whole_name";
		$command = Yii::app()->db->createCommand($query);
		$command->execute();
		$users = $command->queryAll();

		foreach ($users as $user) {
			$assgnuser[] = $user['whole_name'];
		}
		$assig = implode(",", $assgnuser);



		return $assig;
	}


	public function checkexit($attribute, $params)
	{
		if (Yii::app()->controller->action->id != "generalSite") {

			if (!$this->hasErrors()) {

				if ($this->id == "") {

					$tblpx = Yii::app()->db->tablePrefix;
					$data  = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}clientsite WHERE site_name='" . $this->site_name . "' AND pid=" . $this->pid . "")->queryRow();
					if (!empty($data)) {
						// die('sdsdsd');
						$this->addError("site_name", ("" . $this->site_name . " already exists in this project."));
					}
				} else {

					$tblpx = Yii::app()->db->tablePrefix;
					$data  = Yii::app()->db->createCommand("SELECT *  FROM {$tblpx}clientsite WHERE site_name='" . $this->site_name . "' AND pid=" . $this->pid . " AND id !=" . $this->id . "")->queryRow();
					if (!empty($data)) {
						$this->addError("site_name", ("" . $this->site_name . " already exists in this project."));
					}
				}
			}
		}
	}
}
