<?php

/**
 * This is the model class for table "{{meeting_minutes}}".
 *
 * The followings are the available columns in table '{{meeting_minutes}}':
 * @property integer $id
 * @property integer $meeting_id
 * @property string $points_discussed 
 * @property string $submission_date
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Contractors $actionBy
 * @property SiteMeetings $meeting
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class MeetingMinutes extends CActiveRecord
{
	public $users;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MeetingMinutes the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{meeting_minutes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('points_discussed', 'required'),
			array('meeting_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('points_discussed, submission_date, created_date, updated_date,create_task_status', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, meeting_id, points_discussed, submission_date, status, created_by, created_date, updated_by, updated_date,create_task_status', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'meeting' => array(self::BELONGS_TO, 'SiteMeetings', 'meeting_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'milestone' => array(self::BELONGS_TO, 'Milestone', 'milestone_id'),
			'tasks' => array(self::BELONGS_TO, 'Tasks', 'task_id'),
            'meetingMinutesUsers' => array(self::HAS_ONE, 'MeetingMinutesUsers', 'meeting_minutes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'meeting_id' => 'Meeting',
			'points_discussed' => 'Points Discussed',
			'submission_date' => 'Submission Date',
			'status' => 'Status',
			'users' => 'Action By',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'create_task_status' => 'Create Task',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		if ($this->meeting_id) {
			$criteria->compare('meeting_id', $this->meeting_id);
		} else {
			$criteria->compare('meeting_id', 0);
		}
		$criteria->compare('points_discussed', $this->points_discussed, true);
		$criteria->compare('submission_date', $this->submission_date, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
