<?php

/**
 * This is the model class for table "{{tasks}}".
 *
 * The followings are the available columns in table '{{tasks}}':
 * @property integer $tskid
 * @property integer $project_id
 * @property string $title
 * @property string $start_date
 * @property string $due_date
 * @property integer $status
 * @property integer $priority
 * @property integer $assigned_to
 * @property integer $report_to
 * @property integer $billable
 * @property double $total_hrs
 * @property string $description
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 * @property float $min_rate
 * The followings are the available model relations:
 * @property Users $assignedTo
 * @property Users $reportTo
 * @property Status $priority0
 * @property Projects $project
 * @property Status $status0
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property double $quantity
 * @property integer $unit
 * @property double $rate
 * @property double $amount
 * @property integer $milestone_id
 * @property integer $task_duration
 * @property integer $daily_target
 * @property integer $area
 * @property TimeEntry[] $timeEntries
 */
class Tasks extends CActiveRecord
{

    public $status = 6;
    public $priority = 10;
    public $billable = 3; //Default radio button selection
    public $progress_percent;
    public $parent_tskid1;
    public $quantity;
    public $progress_quantity;
    public $task_wpr_id;


    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Tasks the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /*
      // commented out for removal of additional AND condition
      public function defaultScope() {
      if (isset(yii::app()->user->role) AND yii::app()->user->role == 4) {
      return array(
      'condition' => '(coordinator=' . Yii::app()->user->id." OR assigned_to=".Yii::app()->user->id.') AND trash=1',
      );
      }
      else
      return array(
      'condition' => 'trash=1',
      );
      }
     */

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{tasks}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'title , start_date, created_date, created_by, updated_date, '
                    . 'updated_by,work_type_id, task_type,contractor_id,'
                    . 'milestone_id,unit,area', 'required',
                'message' => 'Enter {attribute}', 'on' => 'create'
            ),
            array(
                'status, priority, assigned_to, report_to, project_id',
                'required', 'message' => 'Choose {attribute}', 'on' => 'create'
            ),
            array('title', 'check_exist', 'on' => 'create', 'except' => 'mainTasks'),
            array(
                'title,milestone_id,start_date,due_date,ranking,status',
                'required', 'on' => 'mainTasks'
            ),
            array('ranking', 'rank_validation', 'on' => 'mainTasks'),
            //for external task_type following attributes required
            //            array('quantity,rate,amount,daily_target,allowed_workers,project_id', 'type_validation', 'except' => 'mainTasks'),
            array('quantity,rate,amount,daily_target,allowed_workers,project_id', 'ignoreifMainTasks'),
            array('quantity', 'length_validation', 'except' => 'mainTasks'),
            // array('title', 'unique',),
            //start and end date validations
            array('start_date,due_date', 'date_validation'),
            array('start_date,due_date', 'boq_validation'),
            array('assigned_to', 'assignee_validation'),
            array('work_type_id', 'work_type_validation', 'on' => 'create'),
            //maximum allowed workers count based on throughput
            // array('allowed_workers', 'default', 'value' => '2222'),
            array('project_id, status, priority, assigned_to, report_to, '
                . 'coordinator, billable, created_by, updated_by, '
                . 'clientsite_id,milestone_id,task_duration,unit,'
                . 'contractor_id,ranking', 'numerical', 'integerOnly' => true),
            array('total_hrs, hourly_rate, min_rate,quantity, rate, '
                . 'amount,daily_target, allowed_workers', 'numerical'),
            array('title', 'length', 'max' => 100),
            array('project_id, hourly_rate, progress_percent,unit', 'safe'),
            array('required_workers,due_date, description,billable,hourly_rate,'
                . 'total_hrs,min_rate,parent_tskid,parent_tskid1,'
                . 'acknowledge_by, progress_quantity,area,work_type_id', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('tskid, project_id,hourly_rate, title, start_date, due_date, '
                . 'status, priority, assigned_to, report_to, coordinator, '
                . 'billable, total_hrs, description, created_date, '
                . 'created_by, updated_date, updated_by, trash, progress_percent, '
                . 'clientsite_id, parent_tskid, parent_tskid1,acknowledge_by, '
                . 'quantity, unit, rate, amount,milestone_id, work_type_id, '
                . 'allowed_workers, task_type, email,contractor_id,'
                . 'daily_target', 'safe', 'on' => 'search'),
        );
    }

    public function check_exist($attribute, $params)
    {
        if ($this->$attribute != '') {
            $project_condtion = ' and project_id is null';
            if ($this->project_id != null) {
                $project_condtion = ' and project_id = ' . $this->project_id;
            }
            $task_list_count = Tasks::model()->find()->count('title=:title ' . $project_condtion, array(':title' => $this->title));
            if ($task_list_count) {
                $this->addError($attribute, 'Title is already exist.');
            }
        }
    }

    public function type_validation($attribute, $params)
    {
        if ($this->task_type == 1) {
            if ($this->$attribute == '') {
                $this->addError($attribute, 'Enter ' . $this->getAttributeLabel($attribute));
            }
        }
    }

    public function ignoreifMainTasks($attribute, $params)
    {
        if ($this->parent_tskid != '') {
            if ($this->$attribute == '') {
                $this->addError($attribute, 'Enter ' . $this->getAttributeLabel($attribute));
            }
        }
    }

    public function length_validation($attribute, $params)
    {
        if ($this->parent_tskid != '' and $this->task_type == 1) {
            if ($this->quantity <= 0)
                $this->addError($attribute, 'Quantity has to be greater than 0');
        }
    }

    public function date_validation($attribute, $params)
    {
        if ($this->$attribute != "") {
            if ($this->start_date != "" && $this->due_date != "") {
                if (strtotime($this->start_date) > strtotime($this->due_date)) {
                    $this->addError('start_date', "start date should be less than due date");
                }
                if (strtotime($this->due_date) < strtotime($this->start_date)) {
                    $this->addError('start_date', "due date should be greater than start date");
                }
            }
            if ($this->parent_tskid != "") {
                $parent_task_model = Tasks::model()->findByPk($this->parent_tskid);
                if ($this->start_date != "") {
                    if (strtotime($this->start_date) < strtotime($parent_task_model['start_date'])) {
                        $this->addError('start_date', "Preceeded Main Task's start date");
                    }
                }

                //                echo '<pre>';
                //                echo $this->due_date;
                //                echo '--';
                //                echo $this->parent_tskid;
                //                die;
                if ($this->due_date != "") {
                    if (strtotime($this->due_date) > strtotime($parent_task_model['due_date'])) {
                        $this->addError('due_date', "Exceeded Main Task's due/end date");
                    }
                }
            }
            if ($this->project_id != "") {
                $projct_model = Projects::model()->findByPk($this->project_id);
                if ($this->due_date != "") {
                    if (empty($projct_model['end_date'])) {
                        $this->addError('due_date', "Project End date missing");
                    }
                }
            }
            if ($this->milestone_id != "") {
                $milestone_model = Milestone::model()->findByPk($this->milestone_id);
                if ($this->start_date != "") {
                    if (strtotime($this->start_date) < strtotime($milestone_model['start_date'])) {
                        $this->addError('start_date', "Preceeded milestone's start date");
                    }
                }
                if ($this->due_date != "") {
                    if (strtotime($this->due_date) > strtotime($milestone_model['end_date'])) {
                        $this->addError('due_date', "Exceeded milestone's end date");
                    }
                }
            }
        } else {
            $this->addError($attribute, 'Enter ' . $attribute);
        }
    }

    public function boq_validation($attribute, $params)
    {
        if ($this->$attribute != "") {
            if ($this->due_date != "") {
                $criteria = new CDbCriteria;
                $criteria->condition = "date >:due_date AND taskid =:task_id";
                $criteria->params = array(':due_date' => date('Y-m-d', strtotime($this->due_date)), ':task_id' => $this->tskid);
                $boq_entries = DailyWorkProgress::model()->findAll($criteria);
                if (!empty($boq_entries)) {
                    $boq_count = count($boq_entries);
                    $this->addError('due_date', $boq_count . ' BOQ entries fall outside this end date');
                }
            }
            if ($this->start_date != "") {
                $criteria = new CDbCriteria;
                $criteria->condition = "date <:start_date AND taskid =:task_id AND approve_status != 2";
                $criteria->params = array(':start_date' => date('Y-m-d', strtotime($this->start_date)), ':task_id' => $this->tskid);
                $boq_entries = DailyWorkProgress::model()->findAll($criteria);
                if (!empty($boq_entries)) {
                    $boq_count = count($boq_entries);
                    $this->addError('start_date', $boq_count . ' BOQ entries fall outside this start date');
                }
            }
        }
    }

    public function assignee_validation($attribute, $params)
    {
        if ($this->assigned_to != "") {
            if ($this->clientsite_id != "") {
                $criteria = new CDbCriteria;
                $criteria->select = 'user_id';
                $criteria->condition = 'site_id=:site_id';
                $criteria->params = array(':site_id' => $this->clientsite_id);
                $users = ClientsiteAssign::model()->findAll($criteria);
                // $users = array_column($users, 'user_id');
                foreach ($users as $each_user) {
                    $user_list[] = $each_user->user_id;
                }
                if (!in_array($this->assigned_to, $user_list)) {
                    $this->addError('assigned_to', 'User(s) not specified in work site');
                }
            }
        }
    }

    public function rank_validation($attribute, $params)
    {
        if ($this->ranking != '' && $this->milestone_id != '') {
            $q = new CDbCriteria();
            $q->addSearchCondition('milestone_id', $this->milestone_id);
            $q->addSearchCondition('project_id', $this->project_id);
            $q->addSearchCondition('ranking', $this->ranking);
            if ($this->tskid != '') {
                $q->addCondition('tskid !=' . $this->tskid);
            }

            $tasks = Tasks::model()->find($q);

            if (!empty($tasks['tskid'])) {
                $this->addError($attribute, 'Rank already exist');
            }
        }
    }

    public function work_type_validation($attribute)
    {

        if ($this->work_type_id != '' && $this->project_id != '') {
            $linked_work_types = ProjectWorkType::model()->getWorktypes($this->project_id);
            if (!in_array($this->work_type_id, $linked_work_types)) {
                $this->addError($attribute, 'Work Type not defined in project');
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'billable0' => array(self::BELONGS_TO, 'Status', 'billable'),
            'assignedTo' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
            'reportTo' => array(self::BELONGS_TO, 'Users', 'report_to'),
            'coordinator0' => array(self::BELONGS_TO, 'Users', 'coordinator'),
            'priority0' => array(self::BELONGS_TO, 'Status', 'priority'),
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
            'status0' => array(self::BELONGS_TO, 'Status', 'status'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            'timeEntries' => array(self::HAS_MANY, 'TimeEntry', 'tskid',),
            'clientsite' => array(self::HAS_MANY, 'Clientsite', 'clientsite_id'),
            'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
            'milestone' => array(self::BELONGS_TO, 'Milestone', 'milestone_id'),
            'worktype' => array(self::BELONGS_TO, 'WorkType', 'work_type_id'),
            'contractor' => array(self::BELONGS_TO, 'Contractors', 'contractor_id'),
            'area0' => array(self::BELONGS_TO, 'Area', 'area'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            //          'serial_no' => 'SNO.',
            'tskid' => 'Task ID',
            'project_id' => 'Project',
            'title' => 'Title',
            'start_date' => 'Start Date',
            'due_date' => 'End Date',
            'hourly_rate' => 'Hourly rate',
            'status' => 'Current Progress',
            //'status' => 'Status',
            'priority' => 'Priority',
            'assigned_to' => 'Assigned To',
            'report_to' => 'Task owner',
            'coordinator' => 'Coordinator',
            'billable' => 'Billable',
            'total_hrs' => 'Estimated Hrs',
            'min_rate' => 'Minimum Rate',
            'description' => 'Description',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By',
            'trash' => 'Trash',
            'progress_percent' => 'Progress %',
            'clientsite_id' => 'Location',
            'parent_tskid' => 'Main Task',
            'parent_tskid1' => 'Main Task',
            'quantity' => 'Quantity',
            'unit' => 'Unit',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'work_type_id' => 'Work Type',
            'allowed_workers' => 'Maximum Workers Allowed',
            'task_type' => 'Task Type',
            'email' => 'Email',
            'contractor_id' => 'Contractor',
            'progress_quantity' => 'Progress Quantity',
            'milestone_id' => 'Milestone'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($type = '', $type_closed = '', $search_size = true)
    {
        //$fut_date = date("Y-m-d", strtotime("+3 day"));

        $expmodel = TaskExpiry::model()->findByPk(1);
        $fut_date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));

        $criteria = new CDbCriteria;
        $criteria->select = "t.work_type_id,t.quantity,t.required_workers,"
            . "t.daily_target,t.task_duration,t.report_to,t.parent_tskid,"
            . "t.tskid,t.project_id,t.assigned_to,t.coordinator,"
            . "t.start_date,t.due_date,t.status,t.title,"
            . "timeEntries.completed_percent,t.task_type,t.contractor_id,"
            . "t.email,t.unit,t.template_id,"
            . "IF(due_date < '" . date('Y-m-d') . "',3, "
            . "IF(due_date between '" . date('Y-m-d') . "' "
            . "and '" . $fut_date . "' ,1,2) )  as newsort ";

        $criteria->join = 'LEFT OUTER JOIN `pms_time_entry` `timeEntries` '
            . 'ON (`timeEntries`.`tskid`=`t`.`tskid`)';

        $criteria->compare('t.tskid', $this->tskid);

        $gpmemebr = array();
        if (!empty($this->start_date) && empty($this->due_date)) {
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
            $criteria->condition = "t.start_date >= '$this->start_date'";
        } elseif (!empty($this->due_date) && empty($this->start_date)) {
            $this->due_date = date('Y-m-d', strtotime($this->due_date));
            $criteria->condition = "t.due_date <= '" . $this->due_date . "'";
        } elseif (!empty($this->due_date) && !empty($this->start_date)) {
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
            $this->due_date = date('Y-m-d', strtotime($this->due_date));
            $criteria->condition = "start_date  >= '$this->start_date' and due_date <= '$this->due_date'";
        }

        if (isset($this->quantity) && !empty($this->quantity)) {
            $criteria->addCondition("t.quantity like '%" . $this->quantity . "%'");
        }
        if (isset($this->task_duration) && !empty($this->task_duration)) {
            $criteria->addCondition("t.task_duration like '%" . $this->task_duration . "%'");
        }
        if (isset($this->daily_target) && !empty($this->daily_target)) {
            $criteria->addCondition("t.daily_target like '%" . $this->daily_target . "%'");
        }
        if (isset($this->required_workers) && !empty($this->required_workers)) {
            $criteria->addCondition("t.required_workers like '%" . $this->required_workers . "%'");
        }

        if (isset(Yii::app()->user->group_id)) {
            $groupid = intval(Yii::app()->user->group_id);
            $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members,id',
                    'condition' => 'group_id=' . $groupid,
                )
            );
            foreach ($groupsmemeberlist as $groupsmemeber) {
                $gpmemebr[] = $groupsmemeber['group_members'];
                //$criteria->compare('assigned_to', $gpmemebr, true, 'OR');
            }
            $gpmem = implode(',', $gpmemebr);

            if (!empty($gpmemebr)) {
                $condition = '(assigned_to in(' . $gpmem . ') '
                    . 'OR assigned_to=' . Yii::app()->user->id . ')';
                $criteria->addCondition($condition);
            }
        }
        $gpmem = array();
        if (Yii::app()->user->role > 1) {
            $members = Groups::model()->findAll(
                array(
                    'select' => 'group_id',
                    'condition' => 'group_lead=' . Yii::app()->user->id,
                )
            );
            foreach ($members as $data) {
                $list = Groups_members::model()->findAll(
                    array(
                        'select' => 'group_id,group_members,id',
                        'condition' => 'group_id=' . $data['group_id'],
                    )
                );
                foreach ($list as $groupsmem) {
                    $gpmem[] = $groupsmem['group_members'];
                }
            }
            $gpmem = implode(',', $gpmem);
            if (!empty($gpmem)) {
                $criteria->addCondition('(assigned_to in(' . $gpmem . ') OR assigned_to=' . Yii::app()->user->id . ')');
            }
        }

        if ($type == 'wop' && Yii::app()->user->role == 1)
            $criteria->addCondition('project_id is Null');
        if ($type == 'wop' && Yii::app()->user->role != 1)
            $criteria->addCondition('project_id is Null AND coordinator=' . yii::app()->user->id);

        if ($type == 'odt')
            $criteria->addCondition('due_date<now() AND status!=7 AND coordinator=' . yii::app()->user->id);

        if ($type == 'expired') {
            $criteria->addCondition('t.status NOT IN (7)');
            $criteria->addCondition("t.due_date < '" . date('Y-m-d') . "' ");
        }
        if ($type == 'all_tasks') {
            $criteria->addCondition('t.status NOT IN (7)');
            $criteria->addCondition("t.due_date >= '" . date('Y-m-d') . "' ");
        }

        if ($type == 'main_tasks') {
            $criteria->addCondition('t.parent_tskid IS NULL');
        }
        if ($type == 'sub_tasks') {
            $criteria->addCondition('t.parent_tskid IS NOT NULL');
        }
        if ($type == 'expired_tasks') {
            $criteria->addCondition('t.status NOT IN (7)');
            $criteria->addCondition("t.due_date < '" . date('Y-m-d') . "' ");
        }

        if ($type == "" && $type_closed == "") {
            $criteria->compare('status', $this->status);
            $criteria->addCondition('t.status NOT IN (7)');
        }

        if ($type_closed == "closed") {
            $criteria->compare('status', '7');
        } else {
            $criteria->compare('status', $this->status);
            $criteria->addCondition('t.status NOT IN (7)');
        }

        $criteria->addCondition('trash=1');  //added new field for trash 1 - not trashed  and 0 mean trashed
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.due_date', $this->due_date, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('clientsite_id', $this->clientsite_id);
        $criteria->compare('priority', $this->priority);
        $criteria->compare('assigned_to', $this->assigned_to);
        $criteria->compare('report_to', $this->report_to);
        $criteria->compare('coordinator', $this->coordinator);
        $criteria->compare('t.billable', $this->billable);
        $criteria->compare('total_hrs', $this->total_hrs);
        $criteria->compare('hourly_rate', $this->hourly_rate);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('acknowledge_by', $this->acknowledge_by);
        $criteria->compare('task_type', $this->task_type);
        $criteria->compare('contractor_id', $this->contractor_id);

        if ($this->work_type_id) {
            $work_type_tbl = WorkType::model()->findByAttributes(array('work_type' => $this->work_type_id));
            if ($work_type_tbl) {
                $work_type_condition = $work_type_tbl->wtid;
            } else {
                $work_type_condition = 0;
            }
            $criteria->compare('work_type_id', $work_type_condition);
        }


        $criteria->compare('allowed_workers', $this->allowed_workers);
        $criteria->compare('email', $this->email);

        // $criteria->compare('timeEntries.completed_percent', $this->progress_percent, true);
        //$criteria->compare('timeEntries.completed_percent', $this->progress_percent, true);
        $criteria->compare('parent_tskid', $this->parent_tskid);
        // $criteria->compare('parent_tskid1', $this->parent_tskid1,true);

        if (Yii::app()->user->role != 1) {
            $condition = 't.created_by=' . Yii::app()->user->id
                . ' OR assigned_to=' . Yii::app()->user->id
                . ' OR coordinator=' . Yii::app()->user->id
                . ' OR report_to = ' . Yii::app()->user->id;
            $criteria->addCondition($condition);
        }

        if ($this->parent_tskid1 == 1) {
            $criteria->addCondition('parent_tskid is NULL');
        } else if ($this->parent_tskid1 == 2) {
            $criteria->addCondition('parent_tskid is not NULL');
        }

        $sort = new CSort;
        if ($this->progress_percent != '') {

            $criteria->addCondition('t.progress_percent >=' . $this->progress_percent);
            $criteria->order = 't.progress_percent ASC';
        }

        if ($this->project_id != "" && $this->project_id != 0) {

            $criteria->addCondition('t.project_id = ' . $this->project_id);
        } elseif ($this->project_id == 0) {
            $this->project_id = '';
            $criteria->compare('project_id', $this->project_id, true);
        } elseif (Yii::app()->user->project_id != "") {
            $criteria->addCondition('t.project_id = ' . Yii::app()->user->project_id . '');
        }

        if ($type == 'missed') {
            $missed_tasks = $this->getMissedTasksData($this->project_id);
            if (!empty($missed_tasks['missed_tasks'])) {
                $criteria->addCondition('t.tskid  IN (' . $missed_tasks['missed_tasks'] . ')');
            } else {
                $criteria->addCondition('t.tskid  IN (0)');
            }
        }

        $criteria->group = 't.tskid';
        $sort = new CSort;
        //$sort->attributes=array(
        //  '*',
        //  'progress_percent' => array( "asc"=>'timeEntries.completed_percent', "desc" => 'timeEntries.completed_percent desc'),
        // );

        $_SESSION['taskobject'] = new CActiveDataProvider($this, array(
            'pagination' => FALSE,
            'criteria' => $criteria,
            // 'sort' => array('defaultOrder' => 't.tskid ASC', ),
            'sort' => $sort
        ));

        $dataProvider = new CActiveDataProvider($this, array(
            'pagination' => (!isset($_GET['exportgrid']) ? array('pageSize' => 25,) : false),
            'criteria' => $criteria,
            //'sort'=>$sort,
            'sort' => array(
                'defaultOrder' => 'newsort ASC, due_date ASC',
            )
        ));
        $dataProvider->setTotalItemCount(count($this->findAll($criteria)));
        return $dataProvider;
    }

    public function assigned_task($type = '')
    {

        $expmodel = TaskExpiry::model()->findByPk(1);
        $fut_date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));
        $criteria = new CDbCriteria;
        $criteria->select = "t.required_workers,t.quantity,t.daily_target,"
            . "t.acknowledge_by,t.report_to,t.parent_tskid,"
            . "t.tskid,t.project_id,t.assigned_to,t.coordinator,"
            . "t.start_date,t.due_date,t.status,t.title,"
            . "timeEntries.completed_percent,t.unit,"
            . "IF(due_date < '" . date('Y-m-d') . "',3, "
            . "IF(due_date between '" . date('Y-m-d') . "' "
            . "and '" . $fut_date . "' ,1,2) ) as newsort,"
            . "t.task_duration,t.task_type,t.contractor_id";

        $criteria->join = 'LEFT OUTER JOIN `pms_time_entry` `timeEntries` '
            . 'ON (`timeEntries`.`tskid`=`t`.`tskid`)';
        $criteria->compare('t.tskid', $this->tskid);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.start_date', $this->start_date, true);
        $criteria->compare('priority', $this->priority);
        $criteria->compare('report_to', $this->report_to);
        $criteria->compare('assigned_to', $this->assigned_to);
        $criteria->compare('coordinator', $this->coordinator);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('total_hrs', $this->total_hrs);
        $criteria->compare('hourly_rate', $this->hourly_rate);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('acknowledge_by', $this->acknowledge_by);
        $criteria->compare('contractor_id', $this->contractor_id);
        $criteria->compare('work_type_id', $this->work_type_id);
        $criteria->compare('allowed_workers', $this->allowed_workers);
        // $criteria->compare('task_type', $this->task_type);
        $criteria->compare('milestone_id', $this->milestone_id);
        $criteria->compare('t.daily_target', $this->daily_target, true);
        $criteria->compare('email', $this->email);
        $criteria->compare('t.quantity', $this->quantity, true);
        $criteria->compare('t.required_workers', $this->required_workers, true);
        if (isset($this->task_duration) && !empty($this->task_duration)) {
            $criteria->addCondition("t.task_duration = " . $this->task_duration);
        }
        if (isset($_GET['project_filter'])) {
            $criteria->addCondition('t.project_id = ' . $_GET['project_filter']);
        }
        if (isset($_GET['assigned_filter'])) {
            $criteria->compare('assigned_to', $_GET['assigned_filter']);
        }
        if (isset($_GET['coordinator_filter'])) {
            $criteria->compare('coordinator', $_GET['coordinator_filter']);
        }
        if (isset($_GET['milestone_filter'])) {
            $criteria->compare('milestone_id', $_GET['milestone_filter']);
        }
        if ($type == '') {
            $criteria->compare('status', $this->status);
            $criteria->addCondition('t.status NOT IN (7)');
            $criteria->compare('due_date', $this->due_date, true);
            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        } elseif ($type == 'closed') {
            $criteria->compare('status', $this->status);
            $criteria->compare('status', 7);

            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        } elseif ($type == 'pending') {
            $criteria->addCondition('t.status NOT IN (7,9)');
            $criteria->addCondition("t.due_date > '" . date('Y-m-d') . "' ");

            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ') '
                . 'or t.report_to in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        } else {
            $criteria->addCondition('t.status NOT IN (7,9)');

            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        }


        $criteria->addCondition('trash=1');
        if ($this->project_id != "" && $this->project_id != 0) {

            $criteria->addCondition('t.project_id = ' . $this->project_id);
        } elseif ($this->project_id == 0) {
            $this->project_id = '';
            $criteria->compare('project_id', $this->project_id, true);
        } elseif (Yii::app()->user->project_id != "") {
            $criteria->addCondition('t.project_id = ' . Yii::app()->user->project_id . '');
        }

        $sort = new CSort;
        if ($this->progress_percent != '') {
            $criteria->addCondition('t.progress_percent >=' . $this->progress_percent);
            $criteria->order = 't.progress_percent ASC';
        }

        $criteria->group = 't.tskid';


        $dataProvider = new CActiveDataProvider($this, array(
            'pagination' => (!isset($_GET['exportgrid']) ? array('pageSize' => 25,) : false),
            'criteria' => $criteria,
            //'sort'=>$sort
            'sort' => array(
                'defaultOrder' => 'start_date ASC',
            )
        ));

        $dataProvider->setTotalItemCount(count($this->findAll($criteria)));
        return $dataProvider;
    }

    /* public function getcompleted_percent($id)
      {
      $query = "SELECT `completed_percent` FROM pms_time_entry,pms_tasks WHERE pms_tasks.tskid=pms_time_entry.tskid  AND pms_tasks.tskid = '$id'  AND  trash=1 ORDER BY pms_time_entry.created_date DESC LIMIT 1";
      $percentage = Yii::app()->db->createCommand($query)->queryRow();
      return isset($percentage['completed_percent']) ? $percentage['completed_percent'].'%' : '0%';
      } */

    public function getcompleted_percent($id)
    {

        $task = Tasks::model()->findByPk($id);
        if ($task->task_type == 1) {
            $percentage = 0;
            $sql = "SELECT SUM(qty) as qty "
                . "FROM pms_daily_work_progress "
                . "WHERE taskid ='" . $id . "' AND approve_status =1";
            $sql_response2 = Yii::app()->db->createCommand($sql)->queryRow();

            if ($task['quantity'] != '') {

                $percentage = ($sql_response2['qty'] / $task['quantity']) * 100;
            }
            if ($percentage > 100) {
                $percentage = 100;
            }
            $percentage = number_format($percentage, 2, '.', '');

            return isset($sql_response2['qty']) ? $percentage : '0';
        } else {
            $sql = "SELECT SUM(completed_percent) as work_progress "
                . "FROM pms_time_entry "
                . "WHERE tskid ='" . $id . "' AND approve_status =1";
            $sql_response2 = Yii::app()->db->createCommand($sql)->queryRow();
            return isset($sql_response2['work_progress']) ? number_format($sql_response2['work_progress'], 2) : '0';
        }
    }

    public function getTimeentrydescription($id)
    {
        $query = "SELECT pms_tasks.title,pms_time_entry.description,acknowledge_by "
            . "FROM pms_tasks LEFT JOIN pms_time_entry "
            . "ON pms_tasks.tskid=pms_time_entry.tskid "
            . "WHERE pms_tasks.tskid='$id'  AND trash=1 "
            . "ORDER BY pms_tasks.created_date DESC LIMIT 1";
        $percentage = Yii::app()->db->createCommand($query)->queryRow();
        $ack = '';
        if ($percentage['acknowledge_by'] != '') {
            $ack = '<i class="fa fa-check" style="margin-left:5px;color:green"></i>';
        }
        return CHtml::link($percentage['title'] . $ack, array("Tasks/view", "id" => $id), array('title' => $percentage['description']));
    }

    public function reportSearch()
    {
        $where = '';
        $condition = array();
        if (isset($_REQUEST['Tasks'])) {
            if (isset($_REQUEST['Tasks']['clientsite_id']) and ($_REQUEST['Tasks']['clientsite_id']) != '')
                $condition[] = 'tsk.clientsite_id like "%' . $_REQUEST['Tasks']['clientsite_id'] . '%"';

            if (isset($_REQUEST['Tasks']['project_id']) and ($_REQUEST['Tasks']['project_id']) != '')
                $condition[] = 'tsk.project_id like "%' . $_REQUEST['Tasks']['project_id'] . '%"';

            if (isset($_REQUEST['Tasks']['date_from']) and ($_REQUEST['Tasks']['date_from']) != '')

                $start_date = strtotime($_REQUEST['Tasks']['date_from']);
            $date_from = date('Y-m-d ', $start_date);
            $condition[] = 'te.entry_date >= "' . $date_from . '"';

            if (isset($_REQUEST['Tasks']['date_till']) and ($_REQUEST['Tasks']['date_till']) != '')
                $end_date = strtotime($_REQUEST['Tasks']['date_till']);
            $date_till = date('Y-m-d ', $end_date);
            $condition[] = 'te.entry_date <= "' . $date_till . '"';
        }

        if (!isset($_REQUEST['Tasks']['date_from']) or ($_REQUEST['Tasks']['date_from']) == '')
            $condition[] = 'te.entry_date >= "' . date("Y-m-") . "1" . '"';
        if (!isset($_REQUEST['Tasks']['date_till']) or ($_REQUEST['Tasks']['date_till']) == '')
            $condition[] = 'te.entry_date <= "' . date("Y-m-d") . '"';
        if (Yii::app()->user->role == 3) {
            $condition[] = 'te.user_id ="' . Yii::app()->user->id . '"';
            $condition[] = 'wt.work_type = "Service Report"';
        }
        $condition[] = 'te.approve_status = 1';
        if (Yii::app()->user->project_id != "") {
            $condition[] = 'tsk.project_id = ' . Yii::app()->user->project_id . '';
        }
        if (count($condition))
            $where = " WHERE " . implode(' AND ', $condition);


        $fields = " te.teid as id, p.pid AS project_id, p.name AS project, 
                tsk.tskid AS task_id, tsk.title AS task,
		te.description, wt.work_type, DATE_FORMAT(entry_date, '%d-%b-%y') as entry_date, te.hours, 
                if(te.billable=3,'Yes','No') as billable, te.user_id, 
                u.first_name as username, site.site_name,te.approve_status";
        $query = " FROM `pms_time_entry` AS te
		 INNER JOIN pms_tasks AS tsk ON tsk.`tskid` = te.tskid
		 LEFT JOIN pms_projects AS p ON p.pid = tsk.project_id
		 INNER JOIN pms_users as u on u.userid=te.user_id
		 INNER JOIN pms_work_type AS wt ON wt.wtid = te.work_type 
                 LEFT JOIN pms_clientsite site ON site.id=tsk.clientsite_id  "
            . $where;
        $count = Yii::app()->db->createCommand("SELECT count(*) " . $query)->queryScalar();

        return $dataProvider = new CSqlDataProvider("SELECT $fields " . $query, array(
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tsk.start_date DESC',
                'attributes' => array(
                    'project', 'task', 'entry_date', 'billable', 'site_name' // csv of sortable column names
                )
            ), 'totalItemCount' => $count
        ));
    }

    public function gettask($tskid)
    {

        $model = Tasks::model()->findByPk($tskid);
        return $model->title;
    }

    public function checktask($tskid)
    {
        if (!empty($tskid)) {
            $tsks = Tasks::model()->findByPk($tskid);
            $chk = Tasks::model()->findAll('parent_tskid=' . $tskid);
            // && empty($chk)
            if ($tsks->parent_tskid != NULL) {
                return "NO";
            } else {
                return "YES";
            }
        }
    }

    public function childtask($tskid)
    {
        $tsks = Tasks::model()->findAll('parent_tskid=' . $tskid);
        if (count($tsks) != 0) {
            foreach ($tsks as $key => $data) {
                $status = isset($data->status) ? $this->status0->caption : "";
                $projct = isset($data->project) ? $this->project->name : "not set";
                $link_main = Yii::app()->createUrl("Tasks/view", array("id" => $data->parent_tskid));
                $link_sub = Yii::app()->createUrl("Tasks/view", array("id" => $data->tskid));

                echo '<tr><td><a href="' . $link_main . '">' . $data->parent_tskid . '</a></td>'
                    . '<td><a href="' . $link_sub . '">' . $data->tskid . '</a></td>'
                    . '<td>' . $data->title . '</td>'
                    . '<td>' . $projct . '</td>'
                    . '<td>' . $data->start_date . '</td>'
                    . '<td>' . date('d-M-y', strtotime($data->due_date)) . '</td>'
                    . '<td>' . $this->getcompleted_percent($data->tskid) . '</td>'
                    . '<td>' . $status . '</td>'
                    . '</tr>';
                // $this->childtask($data['tskid']);
            }
        }
    }

    public function reportSearchnew()
    {
        $where = '';
        $condition = array();
        if (isset($_REQUEST['TimeReport'])) {
            // print_r($_REQUEST['TimeReport']);exit;
            if (isset($_REQUEST['TimeReport']['description']) and ($_REQUEST['TimeReport']['description']) != '')
                $condition[] = 'te.description like "%' . $_REQUEST['TimeReport']['description'] . '%"';
            if (isset($_REQUEST['TimeReport']['title']) and ($_REQUEST['TimeReport']['task']) != '')
                $condition[] = 'title like "%' . $_REQUEST['TimeReport']['task'] . '%"';

            if (isset($_REQUEST['TimeReport']['project_id']) and ($_REQUEST['TimeReport']['project_id']) != '')
                $condition[] = 'te.project_id like "%' . $_REQUEST['TimeReport']['project_id'] . '%"';

            if (isset($_REQUEST['TimeReport']['date_from']) and ($_REQUEST['TimeReport']['date_from']) != '')
                $condition[] = 'te.start_date >= "' . $_REQUEST['TimeReport']['date_from'] . '"';

            if (isset($_REQUEST['TimeReport']['date_till']) and ($_REQUEST['TimeReport']['date_till']) != '')
                $condition[] = 'te.due_date <= "' . $_REQUEST['TimeReport']['date_till'] . '"';
        }

        if (!isset($_REQUEST['TimeReport']['date_from']) or ($_REQUEST['TimeReport']['date_from']) == '')
            $condition[] = 'te.start_date >= "' . date("Y-m-") . "1" . '"';
        if (!isset($_REQUEST['TimeReport']['date_till']) or ($_REQUEST['TimeReport']['date_till']) == '')
            $condition[] = 'te.due_date <= "' . date("Y-m-d") . '"';

        //if (Yii::app()->user->role == 1)
        // $condition[] = 'u.user_type =1';
        if (Yii::app()->user->role == 3) {
            $condition[] = 'te.assigned_to ="' . Yii::app()->user->id . '"';
        } else if (isset($_REQUEST['TimeReport']['username']) and ($_REQUEST['TimeReport']['username'])) {
            $condition[] = 'te.assigned_to ="' . $_REQUEST['TimeReport']['username'] . '"';
        }

        if (count($condition))
            $where = " WHERE " . implode(' AND ', $condition);

        //print_r($_REQUEST['TimeReport']['user_id']);exit;


        $fields = "te.tskid as id, p.pid AS project_id, p.name AS project, "
            . "te.tskid AS task_id, te.title AS task, te.description,"
            . "te.start_date, te.due_date, if(te.billable=3,'Yes','No') as billable, "
            . "te.assigned_to as user_id, u.first_name as username, site.site_name";
        $query = "FROM `pms_tasks` AS te LEFT JOIN pms_projects AS p "
            . "ON p.pid = te.project_id INNER JOIN pms_users as u "
            . "on u.userid=te.assigned_to  LEFT JOIN pms_clientsite site "
            . "ON site.id=te.clientsite_id" . $where;

        $count = Yii::app()->db->createCommand("SELECT count(*) " . $query)->queryScalar();

        return $dataProvider = new CSqlDataProvider("SELECT $fields " . $query, array(
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'te.start_date DESC',
                'attributes' => array(
                    'project', 'task', 'entry_date', 'billable', 'site_name' // csv of sortable column names
                )
            ), 'totalItemCount' => $count
        ));
    }

    public function assigned_tasknew($type = '')
    {

        $expmodel = TaskExpiry::model()->findByPk(1);
        $fut_date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));

        $criteria = new CDbCriteria;
        $criteria->select = "t.report_to,t.parent_tskid,t.tskid,t.project_id,"
            . "t.assigned_to,t.coordinator,t.start_date,t.due_date,"
            . "t.status,t.title,timeEntries.completed_percent,"
            . "IF(due_date < '" . date('Y-m-d') . "',3, "
            . "IF(due_date between '" . date('Y-m-d') . "' "
            . "and '" . $fut_date . "' ,1,2) )  as newsort ";

        $criteria->join = 'LEFT OUTER JOIN `pms_time_entry` `timeEntries` '
            . 'ON (`timeEntries`.`tskid`=`t`.`tskid`)';
        $criteria->compare('t.tskid', $this->tskid);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.start_date', $this->start_date, true);
        $criteria->compare('priority', $this->priority);
        $criteria->compare('report_to', $this->report_to);
        $criteria->compare('assigned_to', $this->assigned_to);
        $criteria->compare('coordinator', $this->coordinator);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('total_hrs', $this->total_hrs);
        $criteria->compare('hourly_rate', $this->hourly_rate);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('acknowledge_by', $this->acknowledge_by);
        $criteria->compare('contractor_id', $this->contractor_id);
        $criteria->compare('work_type_id', $this->work_type_id);
        $criteria->compare('allowed_workers', $this->allowed_workers);
        $criteria->compare('task_type', $this->task_type);
        $criteria->compare('email', $this->email);
        if ($type == '') {
            $criteria->compare('status', $this->status);
            $criteria->compare('due_date', $this->due_date, true);
            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        } else {
            $criteria->addCondition('t.status NOT IN (7,9)');
            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ') '
                . 'or t.report_to in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        }
        $criteria->addCondition('trash=1');
        $sort = new CSort;
        if ($this->progress_percent != '') {

            $criteria->addCondition('t.progress_percent >=' . $this->progress_percent);
            $criteria->order = 't.progress_percent ASC';
        }
        $criteria->group = 't.tskid';
        // echo '<pre>';
        // print_r($criteria);
        // exit;
        $dataProvider = new CActiveDataProvider($this, array(
            'pagination' => false,
            'criteria' => $criteria,
            //'sort'=>$sort
            'sort' => array(
                'defaultOrder' => 'newsort ASC, due_date DESC',
            )
        ));

        $dataProvider->setTotalItemCount(count($this->findAll($criteria)));
        return $dataProvider;
    }

    public function displayEscalated()
    {

        $model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        if (!empty($model['escalated_tasks_size_home'])) {
            $tasks_page_count = $model['escalated_tasks_size_home'];
        } else {
            $tasks_page_count = 5;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('tskid', $this->tskid);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->order = 'updated_date DESC';
        $criteria->compare('status', 72);
        if (Yii::app()->user->project_id != "") {
            $criteria->addCondition('project_id = ' . Yii::app()->user->project_id . '');
        }
        if (!in_array('/site/internalescalated', Yii::app()->session['menuauthlist'])) {
            $criteria->addCondition('task_type = 1');
        }
        if (yii::app()->user->role != 1) {
            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ') '
                . 'or t.report_to in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        }

        $dataProvider = new CActiveDataProvider($this, array(
            'pagination' => array(
                'pageSize' => $tasks_page_count,
            ),
            'criteria' => $criteria,
        ));
        $dataProvider->setTotalItemCount(count($this->findAll($criteria)));
        return $dataProvider;
    }

    public function getlatestdescription($task_id, $type)
    {
        $latestdescription = '';
        if (!empty($task_id)) {
            if ($type == 1) {
                $query = "SELECT `tskid`,`description`, `updated_date` "
                    . "FROM `pms_tasks` WHERE `tskid`=" . $task_id . " UNION "
                    . "SELECT `taskid`,`description`,`created_date` "
                    . "FROM `pms_daily_work_progress` "
                    . "WHERE `taskid`=" . $task_id
                    . " ORDER BY `updated_date` DESC";
            } else {
                $query = "SELECT `tskid`,`description`, `updated_date` "
                    . "FROM `pms_tasks` WHERE `tskid`=" . $task_id
                    . " UNION SELECT `tskid`,`description`,`created_date` "
                    . "FROM `pms_time_entry` WHERE `tskid`=" . $task_id
                    . " ORDER BY `updated_date` DESC";
            }

            $latestdescription = Yii::app()->db->createCommand($query)->queryALl();
            if (count($latestdescription) == 0) {
                return $latestdescription;
            } else {
                return $latestdescription[0]['description'];
            }
        } else {
            return $latestdescription;
        }
    }

    public function assignedtask($type)
    {
        $expmodel = TaskExpiry::model()->findByPk(1);
        $fut_date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));
        $criteria = new CDbCriteria;
        $criteria->select = "t.acknowledge_by,t.report_to,"
            . "t.parent_tskid,t.tskid,t.project_id,t.assigned_to,"
            . "t.coordinator,t.contractor_id,t.start_date,"
            . "t.due_date,t.status,t.title,t.quantity,t.trash,t.unit,"
            . "IF(due_date < '" . date('Y-m-d') . "',3, "
            . "IF(due_date between '" . date('Y-m-d') . "'"
            . " and '" . $fut_date . "' ,1,2) )  as newsort ";

        $criteria->compare('t.tskid', $this->tskid);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.start_date', $this->start_date, true);
        //$criteria->compare('priority', $this->priority);
        $criteria->compare('report_to', $this->report_to);
        $criteria->compare('assigned_to', $this->assigned_to);
        $criteria->compare('coordinator', $this->coordinator);
        //$criteria->compare('billable', $this->billable);
        $criteria->compare('total_hrs', $this->total_hrs);
        $criteria->compare('hourly_rate', $this->hourly_rate);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('acknowledge_by', $this->acknowledge_by);
        $criteria->compare('contractor_id', $this->contractor_id);
        $criteria->compare('work_type_id', $this->work_type_id);
        $criteria->compare('allowed_workers', $this->allowed_workers);
        $criteria->compare('t.quantity', $this->quantity);
        $criteria->compare('email', $this->email);
        $criteria->compare('task_type', 1);

        //$criteria->compare('progress_percent', $this->progress_percent, true);
        // $criteria->addCondition('timeEntries.approve_status IS NULL OR timeEntries.approve_status ="1"');
        // $criteria->addCondition('boqentries.approve_status IS NULL OR boqentries.approve_status ="1"');

        if (Yii::app()->user->project_id != "") {
            $criteria->addCondition('t.project_id = ' . Yii::app()->user->project_id . '');
        }
        $criteria->addCondition('t.status != 72');
        // if($type == ''){
        if (yii::app()->user->role != 1) {
            $criteria->compare('due_date', $this->due_date, true);
            $criteria->addCondition('t.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.report_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ')');
        }
        //    }else{
        // 	$criteria->addCondition('t.status NOT IN (7,9)');
        // 	//$criteria->addCondition("t.due_date < '".date('Y-m-d')."' ");
        // 	//$criteria->addCondition('t.assigned_to in (' . Yii::app()->user->id . ')');
        //     $criteria->addCondition('t.assigned_to in (' . Yii::app()->user->id . ') or t.coordinator in (' . Yii::app()->user->id . ') or t.created_by in (' . Yii::app()->user->id . ')');
        //     }
        //echo $this->progress_percent;exit;
        $criteria->addCondition('trash=1');  //added new field for trash 1 - not trashed  and 0 mean trashed
        //$criteria->with=array('timeEntries');
        //$criteria->together=true;
        // $criteria->compare('timeEntries.completed_percent', $this->progress_percent, true);
        $sort = new CSort;
        // if($this->progress_percent!=''){
        //     $criteria->addCondition('t.progress_percent >='.$this->progress_percent);
        //     $criteria->order = 't.progress_percent ASC';
        // }
        $criteria->group = 't.tskid';
        /* $sort->attributes=array(
          '*',
          'progress_percent' => array( "asc"=>'timeEntries.completed_percent', "desc" => 'timeEntries.completed_percent desc'),
          ); */
        $dataProvider = new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 5,),
            'criteria' => $criteria,
            //'sort'=>$sort
            'sort' => array(
                'defaultOrder' => 'newsort ASC, due_date ASC',
            )
        ));

        $dataProvider->setTotalItemCount(count($this->findAll($criteria)));

        return $dataProvider;
    }

    public static function statuspercentage($id)
    {
        $task = Tasks::model()->findByPk($id);
        if ($task->task_type == 1) {
            $percentage = 0;
            $daily_work_progress_tbl = DailyWorkProgress::model()->tableSchema->rawName;
            $sql = 'select sum(qty) as qty from ' . $daily_work_progress_tbl
                . ' where taskid=:id and approve_status=1';
            $sql_response2 = DailyWorkProgress::model()->findBySql($sql, array(':id' => $id));

            if ($task['quantity'] != '') {

                $percentage = ($sql_response2['qty'] / $task['quantity']) * 100;
            }
            if ($percentage > 100) {
                $percentage = 100;
            }
            $percentage = number_format($percentage, 2, '.', '');

            return isset($sql_response2['qty']) ? $percentage : '0';
        } else {

            $time_entry_tbl = TimeEntry::model()->tableSchema->rawName;
            $sql = 'select SUM(completed_percent) as completed_percent from ' . $time_entry_tbl
                . ' where tskid=:id and approve_status=1';
            $sql_response2 = TimeEntry::model()->findBySql($sql, array(':id' => $id));

            return isset($sql_response2['completed_percent']) ? number_format($sql_response2['completed_percent'], 2) : '0';
        }
    }

    public function milestones()
    {
        $criteria = new CDbCriteria;
        if (Yii::app()->user->project_id != "") {
            $criteria->addCondition('t.project_id = ' . Yii::app()->user->project_id);
        }
        if (Yii::app()->user->role != 1) {
            $condition = 't.assigned_to in (' . Yii::app()->user->id . ') '
                . 'or t.report_to in (' . Yii::app()->user->id . ') '
                . 'or t.coordinator in (' . Yii::app()->user->id . ') '
                . 'or t.created_by in (' . Yii::app()->user->id . ')';
            $criteria->addCondition($condition);
        }

        $criteria->group = 't.milestone_id';
        $criteria->addCondition('trash=1');
        $criteria->addCondition('task_type=1');
        // echo $criteria;    
        $data = Tasks::model()->findAll($criteria);
        return $data;
    }

    public function percentagecalculation($milestone_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "select tskid FROM {$tblpx}tasks "
            . "WHERE milestone_id = " . $milestone_id
            . "  AND task_type = 1 ";

        if (Yii::app()->user->role != 1) {
            $sql .= "AND (assigned_to IN (" . Yii::app()->user->id . ") "
                . "OR report_to = (" . Yii::app()->user->id . ")"
                . "OR coordinator IN (" . Yii::app()->user->id . ") "
                . "OR created_by IN (" . Yii::app()->user->id . "))";
        }
        $tasks_external = Yii::app()->db->createCommand($sql)->queryAll();
        $tasks_external_array = array();
        foreach ($tasks_external as $task) {
            array_push($tasks_external_array, $task['tskid']);
        }
        $external_task_count = count($tasks_external_array);
        $tasks_external_array = implode(", ", $tasks_external_array);

        if (!empty($tasks_external_array)) {
            $sql = "SELECT SUM(daily_work_progress) as work_progress "
                . "FROM pms_daily_work_progress "
                . "WHERE taskid IN (" . $tasks_external_array . ") "
                . "AND approve_status = 1";
            $exteranl_progress_perc = Yii::app()->db->createCommand($sql)->queryRow();
            $exteranl_progress_perc = isset($exteranl_progress_perc['work_progress']) ? $exteranl_progress_perc['work_progress'] / $external_task_count : '';
        } else {
            $exteranl_progress_perc = '';
        }
        $sql = "select tskid FROM {$tblpx}tasks "
            . "WHERE milestone_id = " . $milestone_id
            . "  AND task_type = 2 "
            . "AND (assigned_to IN (" . Yii::app()->user->id . ") "
            . "OR coordinator IN (" . Yii::app()->user->id . ") "
            . "OR created_by IN (" . Yii::app()->user->id . "))";
        $tasks_internal = Yii::app()->db->createCommand($sql)->queryAll();

        $tasks_internal_array = array();
        foreach ($tasks_internal as $task) {
            array_push($tasks_internal_array, $task['tskid']);
        }
        $internal_task_count = count($tasks_internal_array);
        $tasks_internal_array = implode(", ", $tasks_internal_array);
        if (!empty($tasks_internal_array)) {
            $sql = "SELECT SUM(completed_percent) as work_progress "
                . "FROM pms_time_entry "
                . "WHERE tskid IN (" . $tasks_internal_array . ") "
                . "AND approve_status =1";
            $internal_progress_perc = Yii::app()->db->createCommand($sql)->queryRow();
            $internal_progress_perc = isset($internal_progress_perc['work_progress']) ? $internal_progress_perc['work_progress'] / $internal_task_count : '';
        } else {
            $internal_progress_perc = '';
        }

        $total_percentage = floatval($internal_progress_perc) + floatval($exteranl_progress_perc);
        return number_format($total_percentage, 2) . '%';
    }

    public function checkTaskType($model)
    {
        $status = 1; //1=>can change task_type; 2=>can't change task type
        if ($model->task_type == 1) {
            $boq_check = DailyWorkProgress::model()->findAll(array('condition' => 'taskid =' . $model->tskid));
            if (!empty($boq_check)) {
                $status = 2;
            }
        } elseif ($model->task_type == 2) {
            $time_entry = TimeEntry::model()->findAll(array('condition' => 'tskid =' . $model->tskid));
            if (!empty($time_entry)) {
                $status = 2;
            }
        }
        return $status;
    }

    public function getProgressQuantity($id)
    {
        $unit = '';
        $task = Tasks::model()->findByPk($id);
        if (!empty($task->unit)) {
            $unit = $task->unit0->unit_title;
            if (!empty($unit)) {
                $unit = ' (' . $unit . ')';
            }
        }

        if ($task->task_type == 1) {
            $sql = "SELECT SUM(qty) as tot_qty "
                . "FROM pms_daily_work_progress "
                . "WHERE taskid ='" . $id . "' AND approve_status =1";
            $sql_response2 = Yii::app()->db->createCommand($sql)->queryRow();
            return isset($sql_response2['tot_qty']) ? $sql_response2['tot_qty'] . $unit : '0';
        } else {
            return '';
        }
    }

    public function getAssigneedeatils($id)
    {
        $user_model = Users::model()->findByPk($id);
        return $user_model['first_name'] . ' ' . $user_model['last_name'];
    }

    public function getWorkTypePercentage($work_type_id, $milestone_id, $area_id, $project_id, $others = false)
    {
        $condition = 'work_type_id = ' . $work_type_id
            . ' AND milestone_id = ' . $milestone_id
            . ' AND area = ' . $area_id
            . ' AND project_id = ' . $project_id;
        $tasks_datas = Tasks::model()->findAll(array('condition' => $condition));
        $percentage_total = 0;
        foreach ($tasks_datas as $task_data) {
            $percentage_total += $task_data->getcompleted_percent($task_data->tskid);
        }
        if ($percentage_total != 0) {
            if (!empty($others)) {
                $result = array('status' => 1, 'value' => $percentage_total / count($tasks_datas));
                return $result;
            } else {
                if (is_numeric($percentage_total)) {
                    $percentage_value = sprintf("%.2f", $percentage_total / count($tasks_datas));
                    return $percentage_value . '%';
                } else {
                    return '0%';
                }
            }
        } else {
            return '0%';
        }
    }

    public function contractorProgressPercentage($project_id, $contractor_id)
    {
        $condition = 'project_id = ' . $project_id
            . ' AND contractor_id = ' . $contractor_id;
        $tasks_datas = Tasks::model()->findAll(array('condition' => $condition));
        $percentage_total = 0;
        $status = 'Completed';
        foreach ($tasks_datas as $task_data) {
            if ($task_data['status'] != 7) {
                $status = 'In progress';
            }
            $percentage_total += $task_data->getcompleted_percent($task_data->tskid);
        }
        if (is_numeric($percentage_total)) {
            $percentage_value = sprintf("%.2f", $percentage_total / count($tasks_datas));
            $percentage_ = $percentage_value . '%';
        } else {
            $percentage_ = '0%';
        }
        return array('percentage' => $percentage_, 'status' => $status);
    }

    public function getOthersPercentage($project_id, $milestone_id, $area_id)
    {
        $percentage_value = 0;
        $ids = array();
        $project_work_types = ProjectWorkType::model()->findAll(array('condition' => 'project_id = ' . $project_id));
        $work_types = array();
        foreach ($project_work_types as $project_work_type) {
            if (!array_key_exists($project_work_type->ranking, $work_types)) {
                $work_types[$project_work_type->ranking] = array(
                    'title' => $project_work_type->workType->work_type,
                    'id' => $project_work_type->work_type_id
                );
            } else {
                if (isset($work_types[$project_work_type->ranking]['id'])) {
                    array_push($ids, $work_types[$project_work_type->ranking]['id']);
                }
                unset($work_types[$project_work_type->ranking]);
                array_push($ids, $project_work_type->work_type_id);
                $work_types['0'] = array('title' => 'others', 'ids' => $ids);
            }
        }
        $count = 0;
        foreach ($ids as $id) {
            $progress_percentage = $this->getWorkTypePercentage($id, $milestone_id, $area_id, $project_id, 1);
            if (isset($progress_percentage['status'])) {
                $count++;
            }
            if (!empty($progress_percentage['value']))
                $percentage_value += $progress_percentage['value'];
        }
        if (is_numeric($percentage_value)) {
            $percentage_value = $percentage_value / $count;
            $percentage_value = sprintf("%.2f", $percentage_value);
            return $percentage_value . '%';
        } else {
            return '0%';
        }
    }

    public function mainTaskSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.tskid', $this->tskid);
        if ($this->project_id) {
            $criteria->compare('project_id', $this->project_id);
        } else {
            $criteria->compare('project_id', 0);
        }
        $criteria->compare('title', $this->title, true);
        // $criteria->compare('t.start_date', $this->start_date, true);
        $criteria->compare('priority', $this->priority);
        $criteria->compare('report_to', $this->report_to);
        $criteria->compare('assigned_to', $this->assigned_to);
        $criteria->compare('coordinator', $this->coordinator);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('total_hrs', $this->total_hrs);
        $criteria->compare('hourly_rate', $this->hourly_rate);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('acknowledge_by', $this->acknowledge_by);
        $criteria->compare('contractor_id', $this->contractor_id);
        $criteria->compare('work_type_id', $this->work_type_id);
        $criteria->compare('allowed_workers', $this->allowed_workers);
        $criteria->compare('task_type', $this->task_type);
        $criteria->compare('email', $this->email);
        //  $criteria->addCondition('status = 75');
        $criteria->addCondition('parent_tskid IS NULL');

        $dataProvider = new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 50,),
            'criteria' => $criteria,
            //'sort'=>$sort
            'sort' => array(
                'defaultOrder' => 'due_date ASC',
            )
        ));
        return $dataProvider;
    }

    public function getMissedTasksData($project_id = '')
    {
        $missed_task_count = 0;
        $missed_tasks_ids = '';
        $workers_count = '';
        $missed_tasks = array();
        $query_condition = 'status  IN (6,9) AND ';

        if (empty($project_id) && Yii::app()->user->project_id != "") {
            $project_id = Yii::app()->user->project_id;
        }
        if (!empty($project_id)) {
            $query_condition .= 'project_id =' . $project_id
                . ' AND `start_date` <= "' . date('Y-m-d') . '" '
                . 'AND `due_date` >= "' . date('Y-m-d') . '" ';
        } else {
            $query_condition .= '`start_date` <= "' . date('Y-m-d')
                . '"  AND `due_date` >= "' . date('Y-m-d') . '" ';
        }
        if (isset(Yii::app()->user->role) && Yii::app()->user->role !== 1) {
            $query_condition .= 'AND (assigned_to = ' . Yii::app()->user->id
                . ' OR report_to = ' . Yii::app()->user->id
                . ' OR coordinator = ' . Yii::app()->user->id
                . ' OR created_by = ' . Yii::app()->user->id . ')';
        }
        $tasks = Tasks::model()->findAll(array('condition' => $query_condition));
        foreach ($tasks as $task) {
            $days_gone = 0;
            $sql = "SELECT SUM(qty) as qty "
                . "FROM pms_daily_work_progress "
                . "WHERE taskid ='" . $task['tskid'] . "' "
                . "AND approve_status = 1";
            $boq_qty_total = Yii::app()->db->createCommand($sql)->queryRow();
            $daily_target = $task->daily_target;
            $days_gone = Yii::app()->Controller->getDaysCount($task->start_date, $task->due_date);
            $required_quantity = $days_gone * $daily_target;
            $balance_qty = $task->quantity - $boq_qty_total['qty'];
            $remaining_days = $task->task_duration - $days_gone;

            if (($boq_qty_total['qty'] < $required_quantity)) {
                $new_daily_target = $balance_qty / ($remaining_days == 0 ? 1 : $remaining_days);
                if (isset($task->worktype->daily_throughput)) {
                    $workers_count = $new_daily_target / $task->worktype->daily_throughput;
                }
                if ($workers_count > $task->allowed_workers) {
                    $missed_task_count++;
                    array_push($missed_tasks, $task->tskid);
                }
            }
        }
        $missed_tasks = implode(',', $missed_tasks);

        return array(
            'missed_task_count' => $missed_task_count,
            'missed_tasks' => $missed_tasks
        );
    }

    public function getCompletedQuantityByPeriod($id, $from_date, $to_date)
    {
        $percentage = 0;
        $sql = "SELECT SUM(qty) as qty FROM pms_daily_work_progress "
            . "WHERE taskid ='" . $id . "' AND approve_status =1 "
            . "AND (date >= '" . $from_date . "' "
            . "AND date <= '" . $to_date . "')";
        $sql_response2 = Yii::app()->db->createCommand($sql)->queryRow();
        return isset($sql_response2['qty']) ? $sql_response2['qty'] : '0';
    }

    public function getCompletedPercentageByPeriod($id, $from_date, $to_date)
    {
        $task = Tasks::model()->findByPk($id);
        $percentage = 0;
        $sql = "SELECT SUM(qty) as qty FROM pms_daily_work_progress "
            . "WHERE taskid ='" . $id . "' AND approve_status =1 "
            . "AND (date >= '" . $from_date . "' "
            . "AND date <= '" . $to_date . "')";
        $sql_response2 = Yii::app()->db->createCommand($sql)->queryRow();
        if ($task['quantity'] != '') {
            $percentage = ($sql_response2['qty'] / $task['quantity']) * 100;
        }
        if ($percentage > 100) {
            $percentage = 100;
        }
        $percentage = number_format($percentage, 2, '.', '');
        return isset($sql_response2['qty']) ? $percentage : '0';
    }

    public function wpr_task()
    {
        $date = date('Y-m-d');
        $criteria = new CDbCriteria;
        $criteria->select = "d.id as task_wpr_id,t.*";
        $criteria->join = "LEFT OUTER JOIN pms_daily_work_progress d ON (d.taskid=t.tskid) AND date='$date'";
        $criteria->addCondition('trash=1');
        $condition = "task_type = 1 AND status IN (6,9) "
            . "AND (assigned_to = " . Yii::app()->user->id . ") "

            . " AND start_date <= '$date' "
            . "AND due_date >= '$date'  ";
        $criteria->addCondition($condition);

        $criteria->group= 'tskid';

        $sort = new CSort;

        $dataProvider = new CActiveDataProvider($this, array(
            'pagination' => (!isset($_GET['exportgrid']) ? array('pageSize' => 5,) : false),
            'criteria' => $criteria,
            //'sort'=>$sort
            'sort' => array(
                'defaultOrder' => 'task_wpr_id ASC',
            )
        ));

        $dataProvider->setTotalItemCount(count($this->findAll($criteria)));

        return $dataProvider;
    }
    public function checkMRVisibility($project_id)
    {
        $account_permission = $this->accountPermission();

        if ($account_permission == 1) {
            $mapping = Yii::app()->db->createCommand()
                ->select('pms_project_id')
                ->from('pms_acc_project_mapping')
                ->where('pms_project_id=:pms_project_id', array(':pms_project_id' => $project_id))
                ->queryScalar();
            if ($mapping) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    public function checkTemplateAvailability($project_id)
    {
        $project = Projects::model()->findByPK($project_id);
        $template_id = $project->template_id;

        if ($template_id) {
            return 1;
        } else {
            return 0;
        }
    }
    public function accountPermission()
    {
        $result = Yii::app()->db->createCommand()
            ->from('pms_account_permission')
            ->where("status=1")
            ->queryRow();
        if ($result) {
            return 1;
        } else {
            return 2;
        }
    }
    public function projects()
    {
        $criteria = new CDbCriteria;
        if (Yii::app()->user->project_id != "") {
            $criteria->addCondition('t.pid = ' . Yii::app()->user->project_id);
        }
        if (Yii::app()->user->role != 1) {
            $criteria_proj = new CDbCriteria;
            $criteria_proj->select = 'project_id';
            $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
            $criteria_proj->group = 'project_id';
            $project_ids = Tasks::model()->findAll($criteria_proj);
            $project_id_array = array();
            foreach ($project_ids as $projid) {
                array_push($project_id_array, $projid['project_id']);
            }
            if (!empty($project_id_array)) {
                $project_id_array = implode(',', $project_id_array);
                $proj_condition = "OR pid IN (" . $project_id_array . ") AND status =1";
                $criteria->addCondition('pid IN (' . $project_id_array . ')', 'OR');
            }
        }

          
        $data = Projects::model()->findAll($criteria);
        return $data;
    }
     public function projectpercentagecalculation($project_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "select tskid FROM {$tblpx}tasks "
            . "WHERE project_id = " . $project_id
            . "  AND task_type = 1 ";

        if (Yii::app()->user->role != 1) {
            $sql .= "AND (assigned_to IN (" . Yii::app()->user->id . ") "
                . "OR report_to = (" . Yii::app()->user->id . ")"
                . "OR coordinator IN (" . Yii::app()->user->id . ") "
                . "OR created_by IN (" . Yii::app()->user->id . "))";
        }
        $tasks_external = Yii::app()->db->createCommand($sql)->queryAll();
        $tasks_external_array = array();
        foreach ($tasks_external as $task) {
            array_push($tasks_external_array, $task['tskid']);
        }
        $external_task_count = count($tasks_external_array);
        $tasks_external_array = implode(", ", $tasks_external_array);

        if (!empty($tasks_external_array)) {
            $sql = "SELECT SUM(daily_work_progress) as work_progress "
                . "FROM pms_daily_work_progress "
                . "WHERE taskid IN (" . $tasks_external_array . ") "
                . "AND approve_status = 1";
            $exteranl_progress_perc = Yii::app()->db->createCommand($sql)->queryRow();
            $exteranl_progress_perc = isset($exteranl_progress_perc['work_progress']) ? $exteranl_progress_perc['work_progress'] / $external_task_count : '';
        } else {
            $exteranl_progress_perc = '';
        }
        $sql = "select tskid FROM {$tblpx}tasks "
            . "WHERE project_id = " . $project_id
            . "  AND task_type = 2 "
            . "AND (assigned_to IN (" . Yii::app()->user->id . ") "
            . "OR coordinator IN (" . Yii::app()->user->id . ") "
            . "OR created_by IN (" . Yii::app()->user->id . "))";
        $tasks_internal = Yii::app()->db->createCommand($sql)->queryAll();

        $tasks_internal_array = array();
        foreach ($tasks_internal as $task) {
            array_push($tasks_internal_array, $task['tskid']);
        }
        $internal_task_count = count($tasks_internal_array);
        $tasks_internal_array = implode(", ", $tasks_internal_array);
        if (!empty($tasks_internal_array)) {
            $sql = "SELECT SUM(completed_percent) as work_progress "
                . "FROM pms_time_entry "
                . "WHERE tskid IN (" . $tasks_internal_array . ") "
                . "AND approve_status =1";
            $internal_progress_perc = Yii::app()->db->createCommand($sql)->queryRow();
            $internal_progress_perc = isset($internal_progress_perc['work_progress']) ? $internal_progress_perc['work_progress'] / $internal_task_count : '';
        } else {
            $internal_progress_perc = '';
        }

        $total_percentage = floatval($internal_progress_perc) + floatval($exteranl_progress_perc);
        return number_format($total_percentage, 2) . '%';
    }

    public function adminworkload()
    {
        $user = Users::model()->findAll(array("condition" => "user_type
        = 4"));
        $project_array=[];
                   foreach ($user as $users) {
                       $user_id = $users->userid;
                       $sql = "SELECT status,name  FROM pms_projects WHERE FIND_IN_SET(" . $user_id . ",assigned_to)   ";
                       $data    = Yii::app()->db->createCommand($sql)->queryAll();
                     
                       foreach ($data as $data) {
                       
                           if ($data['status'] == 1) {
                               $status = 'Active';
                           } else {
                               $status = 'Inactive';
                           }
       
                           $project_array[] = array(
                               'user' => $users->first_name . " " . $users->last_name,
                               'project' => $data['name'],
                               'status' => $status
       
                           );
                       }
                   }
                   
        return  $project_array;
    }
    public function todaystask()
    {
        $date=date('Y-m-d');
        $criteria=new CDbCriteria;
        $criteria= ' assigned_to ='.Yii::app()->user->id;
        $criteria.=" AND start_date  <= '$date' and due_date >= '$date'" ;
        $criteria.=" AND trash=1";
        $tasks = Tasks::model()->findAll($criteria);
        
        return $tasks;
    }

}
