<?php

/**
 * This is the model class for table "{{project_calender}}".
 *
 * The followings are the available columns in table '{{project_calender}}':
 * @property integer $id
 * @property integer $sunday
 * @property integer $monday
 * @property integer $tuesday
 * @property integer $wednesday
 * @property integer $thursday
 * @property integer $friday
 * @property integer $saturday
 * @property integer $second_saturday
 * @property integer $fourth_saturday
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class ProjectCalender extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectCalender the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_calender}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('calender_name,sunday, monday, tuesday, wednesday, thursday, friday, saturday, second_saturday, fourth_saturday, created_by, created_date', 'required'),
			array('sunday, monday, tuesday, wednesday, thursday, friday, saturday, second_saturday, fourth_saturday, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('updated_date', 'safe'),
			array('calender_name', 'checkname','on' => 'insert'),
			array('calender_name', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, calender_name,sunday, monday, tuesday, wednesday, thursday, friday, saturday, second_saturday, fourth_saturday, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
			
		);
	}

	public function checkname($attribute, $params)
    {
		
        $name=trim($this->calender_name);

		
        
        $project=ProjectCalender::model()->findByAttributes(array('calender_name'=>$name));
        if($project)
        {
            $this->addError('calender_name', 'Name "' .$name.'"  has already been taken.');
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'calender_name'=>'Name',
			'sunday' => 'Sunday',
			'monday' => 'Monday',
			'tuesday' => 'Tuesday',
			'wednesday' => 'Wednesday',
			'thursday' => 'Thursday',
			'friday' => 'Friday',
			'saturday' => 'Saturday',
			'second_saturday' => 'Second Saturday',
			'fourth_saturday' => 'Fourth Saturday',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('calender_name',$this->calender_name);
		$criteria->compare('sunday',$this->sunday);
		$criteria->compare('monday',$this->monday);
		$criteria->compare('tuesday',$this->tuesday);
		$criteria->compare('wednesday',$this->wednesday);
		$criteria->compare('thursday',$this->thursday);
		$criteria->compare('friday',$this->friday);
		$criteria->compare('saturday',$this->saturday);
		$criteria->compare('second_saturday',$this->second_saturday);
		$criteria->compare('fourth_saturday',$this->fourth_saturday);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}