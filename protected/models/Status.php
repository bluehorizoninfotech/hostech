<?php

/**
 * This is the model class for table "{{status}}".
 *
 * The followings are the available columns in table '{{status}}':
 * @property integer $sid
 * @property string $caption
 * @property string $status_type
 *
 * The followings are the available model relations:
 * @property Clients[] $clients
 * @property Projects[] $projects
 * @property Tasks[] $tasks
 * @property Tasks[] $tasks1
 */
class Status extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Status the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{status}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('caption, status_type', 'required'),
			array('caption', 'length', 'max'=>30),
			array('status_type', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('sid, caption, status_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clients' => array(self::HAS_MANY, 'Clients', 'status'),
			'projects' => array(self::HAS_MANY, 'Projects', 'status'),
			'tasks' => array(self::HAS_MANY, 'Tasks', 'priority'),
			'tasks1' => array(self::HAS_MANY, 'Tasks', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sid' => 'Sid',
			'caption' => 'Caption',
			'status_type' => 'Status Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sid',$this->sid);
		$criteria->compare('caption',$this->caption,true);
		$criteria->compare('status_type',$this->status_type,true);

		return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25,),
			'criteria'=>$criteria,
		));
	}
}