<?php

/**
 * This is the model class for table "{{project_comments}}".
 *
 * The followings are the available columns in table '{{project_comments}}':
 * @property integer $comment_id
 * @property integer $section_id
 * @property string $comment
 * @property string $created_date
 * @property integer $user_id
 * @property integer $type
 */
class ProjectComments extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_comments}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('section_id, comment, user_id, type', 'required'),
			array('section_id, user_id, type', 'numerical', 'integerOnly'=>true),
			array('created_date, status_change_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('comment_id, section_id,status_change_description, comment, created_date, user_id, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comment_id' => 'Comment',
			'section_id' => 'Section',
			'comment' => 'Comment',
			'created_date' => 'Created Date',
			'user_id' => 'User',
                        'status_change_description'=>'status change description',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('comment_id',$this->comment_id);
		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('comment',$this->comment,true);
                $criteria->compare('status_change_description',$this->status_change_description,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}