<?php

/**
 * This is the model class for table "{{meeting_detailed_reports}}".
 *
 * The followings are the available columns in table '{{meeting_detailed_reports}}':
 * @property integer $id
 * @property integer $meeting_id
 * @property integer $report_id
 * @property string $description
 *
 * The followings are the available model relations:
 * @property SiteMeetings $meeting
 * @property ProjectReport $report
 */
class MeetingDetailedReports extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MeetingDetailedReports the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{meeting_detailed_reports}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('description', 'required'),
			array('meeting_id, report_id', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, meeting_id, report_id, description', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'meeting' => array(self::BELONGS_TO, 'SiteMeetings', 'meeting_id'),
			'report' => array(self::BELONGS_TO, 'ProjectReport', 'report_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'meeting_id' => 'Meeting',
			'report_id' => 'Report',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($meeting_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		if ($meeting_id != 0) {
			$criteria->compare('meeting_id', $meeting_id);
		} else if ($meeting_id == 0) {
			$criteria->compare('meeting_id', 0);
		} else if ($this->meeting_id != '') {
			$criteria->compare('meeting_id', $this->meeting_id);
		}
		$criteria->compare('meeting_id', $this->meeting_id);
		$criteria->compare('report_id', $this->report_id);
		$criteria->compare('description', $this->description, true);
		$criteria->group = 'report_id';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	public function getReportName($report_id)
	{
		$project_report_model = ProjectReport::model()->findByPk($report_id);
		$start_date = strtotime($project_report_model->from_date);
		$to_date = strtotime($project_report_model->to_date);
		return "Detailed Report(" . date("d-m-Y", $start_date) . "to" . date("d-m-Y", $to_date) . ")";
	}
}
