<?php

/**
 * This is the model class for table "{{attendance_requests}}".
 *
 * The followings are the available columns in table '{{attendance_requests}}':
 * @property integer $att_id
 * @property integer $user_id
 * @property string $att_date
 * @property string $comments
 * @property integer $att_entry
 * @property integer $created_by
 * @property string $created_date
 * @property integer $type
 * @property integer $status
 * @property integer $decision_by
 * @property string $decision_date
 */
class AttendanceRequests extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{attendance_requests}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, comments, att_entry, created_by, created_date, att_date', 'required'),
            array('user_id, att_entry, created_by, type, status, decision_by', 'numerical', 'integerOnly' => true),
            array('att_date, decision_date, reason', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('att_id, user_id, att_date, comments, att_entry, created_by, created_date, type, status, decision_by, decision_date, reason', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usrId' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'decisionBy' => array(self::BELONGS_TO, 'Users', 'decision_by'),
            'attEntry' => array(self::BELONGS_TO, 'Legends', 'att_entry'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'att_id' => 'Att',
            'user_id' => 'User',
            'att_date' => 'Date',
            'comments' => 'Comments',
            'att_entry' => 'Entry',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'type' => 'Type',
            'status' => 'Status',
            'decision_by' => 'Decision By',
            'decision_date' => 'Decision Date',
            'reason' => 'Reason'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($status_type = "All") {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array('usrId');
        $criteria->compare('att_id', $this->att_id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('att_date', $this->att_date, true);
        $criteria->compare('comments', $this->comments, true);
        $criteria->compare('att_entry', $this->att_entry);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('decision_by', $this->decision_by);
        $criteria->compare('decision_date', $this->decision_date, true);

        $criteria->order = 'att_id desc';
        $criteria->order = 't.status asc';

        if ($status_type !== 'All') {
            $criteria->addCondition('t.status = ' . intval($status_type));
        }

        //Project site filter
        if (isset(Yii::app()->user->site_userids) and Yii::app()->user->site_userids != '') {
            $criteria->addCondition('user_id in ( ' . Yii::app()->user->site_userids . ' )');
        }

        //Skill filter
        $skill_filter_id = Yii::app()->user->getState('skill_filter_id', "");
        if ($skill_filter_id != 0 and isset(Yii::app()->user->skill_emp_ids)) {
            $criteria->addCondition('user_id in ( ' . Yii::app()->user->skill_emp_ids . ' )');
        }

        //Designation filter
        $desig_filter_id = Yii::app()->user->getState('desig_filter_id', "");

        if ($desig_filter_id != 0 and isset(Yii::app()->user->desig_emp_ids)) {
            $criteria->addCondition('user_id in ( ' . Yii::app()->user->desig_emp_ids . ' )');
        }
        
         $employee_group = Yii::app()->user->getState('employee_group', "");

        if ($employee_group != 0 and isset(Yii::app()->user->employee_group)) {
            $criteria->addCondition(' main_grp_id ="'.Yii::app()->user->employee_group.'"');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->user->getState('reportpagesize', 20),)
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AttendanceRequests the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function shift_status($sts) {

        if ($sts == 0) {
            $status = "<span class='pending'><b>Pending</b></span>";
        } else if ($sts == 1) {
            $status = "<span class='approved'><b>Approved</b></span>";
        } else {
            $status = "<span class='rejected'><b>Rejected</b></span>";
        }

        return $status;
    }

}
