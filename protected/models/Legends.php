<?php

/**
 * This is the model class for table "{{legends}}".
 *
 * The followings are the available columns in table '{{legends}}':
 * @property integer $leg_id
 * @property string $short_note
 * @property string $description
 * @property string $color_code
 * @property string $text_color
 * @property string $class_note
 */
class Legends extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Legends the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{legends}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('short_note, description, color_code, text_color', 'required'),
			array('short_note, text_color', 'length', 'max'=>10),
			array('description', 'length', 'max'=>255),
			array('color_code', 'length', 'max'=>100),
			array('class_note', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('leg_id, short_note, description, color_code, text_color, class_note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'leg_id' => 'Leg',
			'short_note' => 'Short Note',
			'description' => 'Description',
			'color_code' => 'Color Code',
			'text_color' => 'Text Color',
			'class_note' => 'Class Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('leg_id',$this->leg_id);
		$criteria->compare('short_note',$this->short_note,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('color_code',$this->color_code,true);
		$criteria->compare('text_color',$this->text_color,true);
		$criteria->compare('class_note',$this->class_note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}