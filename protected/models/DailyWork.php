<?php

/**
 * This is the model class for table "{{daily_work}}".
 *
 * The followings are the available columns in table '{{daily_work}}':
 * @property integer $id
 * @property integer $project_id
 * @property double $qty
 * @property integer $skilled_workers
 * @property string $date
 * @property string $description
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Projects $project
 */
class DailyWork extends CActiveRecord
{


	public $item_id;
	public $taskid;
	public $work_type;
	public $assigned_to;
	public $item_description;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DailyWork the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{daily_work}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date,qty,skilled_workers, description,work_type,taskid', 'required','message' => 'Please Enter {attribute}.'),
			array('project_id', 'required','message' => 'Please Choose {attribute}.'),
			array('project_id, skilled_workers, unskilled_workers, created_by', 'numerical', 'integerOnly'=>true),
			array('qty', 'numerical'),
			array('date', 'datevalidation'),
			array('created_by,work_type,taskid,assigned_to,item_id,unskilled_workers', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_id, qty, skilled_workers, date, description, created_by,work_type,taskid,assigned_to,item_id, unskilled_workers', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'assignedto' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
			'worktype' => array(self::BELONGS_TO, 'WorkType', 'work_type'),
			'tasks' => array(self::BELONGS_TO, 'Tasks', 'taskid'),
			'items' => array(self::BELONGS_TO, 'Items', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'item_id' => 'Item',
			'qty' => 'Qty',
			'taskid' => 'Task',
			'item_description' => 'Item Description: ',
			'work_type'=> 'Work Type',
			'skilled_workers' => 'Skilled Workers',
			'date' => 'Date',
			'description' => 'Description',
			'created_by' => 'Created By',
            'assigned_to' => 'Assigned To',
            'unskilled_workers' =>'Un-Skilled Workers'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		
		$criteria->compare('qty',$this->qty);
		$criteria->compare('taskid',$this->taskid);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('assigned_to',$this->assigned_to);
		$criteria->compare('work_type',$this->work_type);
        $criteria->compare('skilled_workers',$this->skilled_workers);
        $criteria->compare('unskilled_workers',$this->unskilled_workers);
        if($this->date !=""){
            $criteria->compare('date',date('Y-m-d',strtotime($this->date)),true);
        }else{
            $criteria->compare('date',$this->date,true);
        }
		$criteria->compare('description',$this->description,true);
		//$criteria->compare('created_by',$this->created_by);
		
		if(Yii::app()->user->role == 1 || Yii::app()->user->role == 9){
            $criteria->compare('created_by',$this->created_by);
        }else{
			
			$criteria->addCondition('created_by = "'.Yii::app()->user->id.'"');
		}

		if($this->project_id != "" && $this->project_id != 0){
         
            $criteria->addCondition('t.project_id = '.$this->project_id);
        }
        elseif($this->project_id == 0){
            $this->project_id = '';
            $criteria->compare('project_id',$this->project_id,true);
        }
        elseif(Yii::app()->user->project_id !=""){           
                $criteria->addCondition('t.project_id = '.Yii::app()->user->project_id.'');                       
        }
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=> 'id DESC',
			)
		));
	}
	public function getWorkers() {
		$today = date('Y-m-d'); 
		$criteria=new CDbCriteria;
		$criteria->compare('date',$today);
		if(Yii::app()->user->project_id !=""){
			$criteria->addCondition('project_id = '.Yii::app()->user->project_id.'');
		}else{
			$criteria_tasks = new CDbCriteria;
			$criteria_tasks->select = 'project_id'; 
			$criteria_tasks->condition = 'assigned_to = '.Yii::app()->user->id.' OR report_to = '.Yii::app()->user->id.' OR coordinator = '.Yii::app()->user->id;
			$criteria_tasks->group = 'project_id';                
			$project_ids = Tasks::model()->findAll($criteria_tasks);
			$project_id_array= array();
			foreach($project_ids as $projid){
				array_push($project_id_array,$projid['project_id']);
			} 
			if(!empty($project_id_array)){
				// $project_id_array = implode(',',$project_id_array); 
				// $criteria->addCondition('project_id IN ('.$project_id_array.')');
				$criteria->addInCondition('project_id', $project_id_array);
			}else{
				$criteria->addCondition('project_id = NULL');
			}
		}
		$criteria->select ='*,sum(skilled_workers) as skilled_workers,sum(unskilled_workers) as unskilled_workers';				
		$criteria->group = 'project_id,taskid,work_type';
        $dataProvider = new CActiveDataProvider($this, array('pagination'=>array(
        'pageSize'=>5,
    	),
        'criteria' => $criteria)); 
	   $dataProvider->setTotalItemCount(count($this->findAll($criteria)));	   
	   return $dataProvider;
	}
	public function getsitename($site_id){
          if(!empty($site_id)){
			  $sitename = Yii::app()->db->createCommand()
               ->select(['pms_clientsite.site_name'])
                ->from('pms_clientsite')
                ->where('pms_clientsite.id='.$site_id)
			   ->queryAll();
			   
			    if(count($sitename)==0){
					return "";
			    }else{
				    return $sitename['0']['site_name'];
			    }
             
          }else{
			  return "";
		  }
		}
	public function getnoofworkers($items_id,$type=false){
		if(empty($type))
			$type = 0;
		$noofworkers=0;
        if(!empty($items_id)){
			if($type == 2){
				$skilled_workers = Yii::app()->db->createCommand()
				->select(['pms_daily_work.unskilled_workers'])
				 ->from('pms_daily_work')
				 ->where('pms_daily_work.taskid='.$items_id)
				->queryAll();
			}else{
				$skilled_workers = Yii::app()->db->createCommand()
				->select(['pms_daily_work.skilled_workers'])
				 ->from('pms_daily_work')
				 ->where('pms_daily_work.taskid='.$items_id)
				->queryAll();
			}
			
			if(count($skilled_workers)==0){
					return $noofworkers;
			}else{
				   foreach($skilled_workers as $mainvalue ){
					  	foreach($mainvalue as $value ){
							$noofworkers+=$value;
				   		} 
				   }
				   return $noofworkers;
			}
			   
        }else{
			  return $noofworkers;
		}
    }
	public function datevalidation($attribute)
	{
		$todays_date=date('j-F-Y');
		if ($this->date != "") {
			$date = strtotime($this->date);
			$date_from = date('Y-m-d ', $date);
		if(strtotime($this->date) > strtotime($todays_date))	
		{
			$msg= date('j-F-Y',strtotime($todays_date));
			$this->addError('date', 'Date should be less than or equal to '. $msg);
		}
			
		}
	}
}