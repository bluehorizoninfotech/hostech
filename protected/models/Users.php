<?php

/**
 * This is the model class for table "tc_users".
 */
class Users extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    //for forgot password
    public $myemail_or_username;
    public $ruser_id;
    public $retype_pwd;
    public $new_pwd;
    public $old_password;
    public $rules_array = array();
    public $action_name;
    public $status = 0;
    public $full_name;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{users}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $this->action_name = trim(strtolower(Yii::app()->controller->action->id));

        if ($this->action_name === 'pwdrecovery') {
            $this->rules_array = array(
                array('myemail_or_username', 'required', 'message' => 'Entera your username or email id'),
                //array('myemail_or_username', 'match', 'pattern' => '/^[A-Za-z0-9@.-\s,]+$/u', 'message' => "Enter Valid username or email id"),
                // password needs to be authenticated
                array('myemail_or_username', 'checkexists'),
            );
        } elseif ($this->action_name === 'pwdreset') {
            $this->rules_array = array(
                array('new_pwd, retype_pwd', 'required', 'message' => 'Entera {attribute}'),
                array('new_pwd', 'length', 'max' => 32, 'min' => 4, 'message' => "Incorrect password (minimal length 4 characters)."),
                array('retype_pwd', 'compare', 'compareAttribute' => 'new_pwd', 'message' => "Password mismatch"),
            );
        } elseif ($this->action_name === 'passchange') {
            $this->rules_array = array(
                array('old_password, password ,retype_pwd', 'required', 'message' => 'Enter {attribute}'),
                array('old_password', 'validate_oldpass'),
                array('password', 'length', 'min' => '5'),
                array('retype_pwd', 'compare', 'compareAttribute' => 'password', 'message' => "Password mismatch"),
            );
        } elseif ($this->action_name === 'editprofile') {
            $this->rules_array = array(
                array('first_name ,email', 'required', 'message' => 'Enter {attribute}'),
                array('email', 'email'),
                array('first_name ,last_name, email, accesscard_id', 'safe'),
            );
        } elseif ($this->action_name === 'update') {
            $this->rules_array = array(
                //  array('first_name, username, email, status, user_type, designation, employee_id', 'required', 'message' => 'Enter {attribute}'),
                array('first_name, email, status, user_type, designation, employee_id', 'required', 'message' => 'Enter {attribute}'),
                //  array('username,password', 'validateusername'),
                array('department_id', 'required', 'message' => 'Choose your {attribute}'),
                array('password', 'length', 'min' => 5, 'max' => 32),
                array('email', 'email'),
                array('employee_id, username, email', 'unique'),
                array('accesscard_id', 'numerical', 'integerOnly' => true),
                array('accesscard_id','unique'),   
                array('company,contact_number,type', 'type_validation'),             
                array('first_name,accesscard_id, username, email, status, user_type, last_name,reporting_person', 'safe'),
            );
        } elseif ($this->action_name === 'create') {
            $this->rules_array = array(
                // array('first_name, username, password, email,department_id, designation, employee_id', 'required', 'message' => 'Enter your {attribute}'),
                array('first_name, email,department_id, designation, employee_id', 'required', 'message' => 'Enter your {attribute}'),
                array('username,password', 'validateusername'),
                array('user_type', 'required', 'message' => 'Select your registration type'),
                array('user_type', 'numerical', 'integerOnly' => true),
                array('password', 'length', 'min' => 5, 'max' => 32),
                array('first_name, last_name', 'length', 'max' => 30),
                array('username', 'length', 'max' => 20),
                array('employee_id, username, email', 'unique'),
                array('email', 'length', 'max' => 70),
                array('email', 'email'),
                array('first_name,accesscard_id,pms_access,	tms_access,wpr_access, last_name, username, password, email,reporting_person', 'safe'),
            );
        } else {
            $this->rules_array = array(
                array('first_name, email, designation,user_type', 'required', 'message' => 'Enter your {attribute}'),
                array('employee_id,department_id,user_type', 'internal_validation'),
                array('username,password', 'validateusername'),
                array('username', 'checkusername'),
                // array('user_type', 'required', 'message' => 'Select your registration type'),
                array('user_type,contact_number,type', 'numerical', 'integerOnly' => true),
                array('company,contact_number,type', 'type_validation'),

                array('password', 'length', 'min' => 5, 'max' => 32),
                array('first_name, last_name', 'length', 'max' => 30),
                array('username', 'length', 'max' => 20),
                array('email', 'length', 'max' => 70),
                array('email', 'email'),
                array('reg_ip', 'length', 'max' => 15),
                array('employee_id, email', 'unique'),
                array('accesscard_id', 'numerical', 'integerOnly' => true),
                array('accesscard_id','unique'),

                array('activation_key', 'length', 'max' => 32),
                array('reg_date, reporting_person,accesscard_id,contact_number,type', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                //array('userid, user_type, first_name, last_name, username, password, email, last_modified, reg_date, reg_ip, activation_key, email_activation, status', 'safe', 'on' => 'search'),
                array('full_name, user_type, first_name,pms_access,tms_access,wpr_access,last_name,username,email,status,accesscard_id,department_id,type', 'safe', 'on' => 'search'),

            );
        }

        return $this->rules_array;
    }

    //Custom validation functions
    public function checkexists($attribute, $params)
    {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            if (strpos($this->myemail_or_username, "@")) {
                $user = Users::model()->findByAttributes(array('email' => $this->myemail_or_username, 'status' => 0, 'email_activation' => 1));
                if ($user)
                    $this->ruser_id = $user->userid;
            } else {
                $user = Users::model()->findByAttributes(array('username' => $this->myemail_or_username, 'status' => 0, 'email_activation' => 1));
                if ($user)
                    $this->ruser_id = $user->userid;
            }

            if ($user === null)
                if (strpos($this->myemail_or_username, "@")) {
                    $this->addError("myemail_or_username", ("This email address was not found in our records"));
                } else {
                    $this->addError("myemail_or_username", ("This username was not found in our records"));
                }
        }
    }


    public function Validateusername($attribute, $params)
    {
        if ($this->user_type != 11 && $this->type == 1) {
            if ($this->username == '') {
                $this->addError("username", ("Enter your Username"));
            }
            if ($this->password == '') {
                $this->addError("password", ("Enter your Password"));
            }
            if (strrpos($this->password, ' ') !== false)
            {
                $this->addError($attribute, 'avoid space from password'); 
            }
        }
    }

    public function checkusername($attribute, $params)
    {
        if ($this->user_type != 11 && $this->type == 1) {
            if ($this->username == '') {
                $this->addError("username", ("Enter your Username"));
            } else {
                $q = new CDbCriteria();
                $q->addSearchCondition('username', $this->username);
                $users = Users::model()->find($q);
                if (!empty($users['userid'])) {
                    $this->addError($attribute, 'username already exist');
                }
            }
            if (strrpos($this->username, ' ') !== false)
            {
                $this->addError($attribute, 'avoid space from username'); 
            }
        }
        
    }


    public function validate_oldpass($attribute, $params)
    {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            $oldpass = md5(base64_encode($this->old_password));
            //$user = Users::model()->findByAttributes(array('password' => $oldpass, 'userid' => Yii::app()->user->id, 'status' => 0, 'email_activation' => 1));
            $user = Users::model()->findByAttributes(array('password' => $oldpass, 'userid' => Yii::app()->user->id, 'status' => 0));
            if (!$user) {
                $this->addError("old_password", ("Invalid old password"));
            }
        }
    }

    public function type_validation($attribute, $params)
    {

        if ($this->type == 2) {
            if ($this->$attribute == '') {
                $this->addError($attribute, 'Enter ' . $attribute);
            }
        }
    }

    public function internal_validation($attribute, $params)
    {

        if ($this->type == 1) {
            if ($this->$attribute == '') {
                $this->addError($attribute, 'Enter ' . $attribute);
            }
        }
    }


    //End of custome validation functions
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'reportingPerson' => array(self::BELONGS_TO, 'Users', 'reporting_person'),
            'users' => array(self::HAS_MANY, 'Users', 'reporting_person'),
            'userType' => array(self::BELONGS_TO, 'UserRoles', 'user_type'),
            'Department' => array(self::BELONGS_TO, 'Department', 'department_id'),


        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'userid' => 'Userid',
            'user_type' => 'Profile type',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'full_name' => 'Full Name',
            'password' => 'Password',
            'email' => 'Email',
            'reporting_person' => 'Reporting Person',
            'last_modified' => 'Last Modified',
            'reg_date' => 'Reg Date',
            'reg_ip' => 'Reg Ip',
            'activation_key' => 'Activation Key',
            'email_activation' => 'Email Activation',
            'status' => 'Status',
            'myemail_or_username' => 'Username or Email id',
            'retype_pwd' => 'Retype password',
            'new_pwd' => 'New password',
            'old_password' => 'Old password',
            'reg_type' => '',
            'accesscard_id' => 'Punching Access ID',
            'pms_access' => 'pms',
            'tms_access' => 'tms',
            'wpr_access' => 'wpr',
            'department_id' => 'Department',
            'designation' => 'Designation',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        // $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        //$criteria->compare('first_name', $this->first_name, true);
        // $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('CONCAT_WS(" ",first_name,last_name)', $this->full_name, true);

        $criteria->compare('username', $this->username, true);
        //$criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('accesscard_id', $this->accesscard_id, true);
        $criteria->compare('reporting_person', $this->reporting_person, true);
        //$criteria->compare('last_modified', $this->last_modified, true);
        //$criteria->compare('reg_date', $this->reg_date, true);
        //$criteria->compare('reg_ip', $this->reg_ip, true);
        //  $criteria->compare('activation_key', $this->activation_key, true);
        //  $criteria->compare('email_activation', $this->email_activation);
        $criteria->compare('status', $this->status);
        $criteria->compare('designation', $this->designation, true);
        $criteria->compare('department_id', $this->department_id, true);
        $criteria->compare('employee_id', $this->employee_id, true);
        $criteria->compare('type', $this->type, true);

        $url = Yii::app()->controller->getAction()->getId();
        if ($url == 'Userlog') {
            $tblpx = Yii::app()->db->tablePrefix;
            $query = "SELECT created_by FROM {$tblpx}time_entry WHERE date(`created_date`) = '" . date('Y-m-d') . "' ";
            $sql = Yii::app()->db->createCommand($query)->queryALl();

            $query1 = "SELECT created_by FROM {$tblpx}tasks WHERE date(`created_date`) = '" . date('Y-m-d') . "' ";
            $sql1 = Yii::app()->db->createCommand($query1)->queryALl();
            $merges = array_merge($sql, $sql1);
            $d = '';
            foreach ($merges as $merge) {
                $d .= '"' . $merge['created_by'] . '",';
            }
            if ($d) {
                $re = rtrim($d, ',');
                $criteria->addCondition("userid not in(" . $re . ") ");
            }
            $re = rtrim($d, ',');
            $criteria->addCondition('status = "0" ');
            //$criteria->addCondition("date(`last_modified`) != '".date('Y-m-d')."' ");
            $criteria->addCondition("user_type not in(5,11) ");

            $criteria->order = ' date(`last_modified`) ASC';
        }




        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25),
            'sort' => array(
                'defaultOrder' => 'status,user_type ASC',
            ),
            'criteria' => $criteria,
        ));
    }

    public function getapplication($id)
    {

        $a = array();
        $query = "SELECT `pms_access`,`tms_access`,`wpr_access` FROM pms_users where userid=" . $id;
        $sql = Yii::app()->db->createCommand($query)->queryRow();
        if ($sql['pms_access'] == 1) {
            array_push($a, "pms");
        }
        if ($sql['tms_access'] == 1) {
            array_push($a, "tms");
        }
        if ($sql['wpr_access'] == 1) {
            array_push($a, "wpr");
        }

        $app = implode(', ', $a);
        return $app;
    }


    public function getLogdays($date)
    {

        $now = time();
        $your_date = strtotime($date);
        $datediff = $now - $your_date;

        return floor($datediff / (60 * 60 * 24)) . ' ' . 'Days';
    }
    /*-------------mod by Rajisha 15-1-2018-------------*/

    public function getGroupname($id)
    {
        $tbl = Yii::app()->db->tablePrefix;

        $users = Yii::app()->db->createCommand()
            ->select(["{$tbl}groups.group_name"])
            ->from("{$tbl}groups")
            ->leftJoin("{$tbl}groups_members", "{$tbl}groups_members.group_id = {$tbl}groups.group_id")
            ->where("{$tbl}groups_members.group_members=:group_members", array(':group_members' => $id))
            ->queryAll();
        $groupname = array();
        $gplead = Groups::model()->findAll(array(
            'select' => 'group_name',
            'condition' => 'group_lead=' . $id,
        ));
        if (isset($gplead) && ($gplead != NULL)) {
            foreach ($gplead as $lead) {
                $groupname[] = $lead['group_name'] . " ( Group Lead)";
            }
        }

        foreach ($users as $user) {
            $groupname[] = $user['group_name'];
        }
        $gpname = "";
        if ($groupname != NULL) {
            $gpname = implode(', ', $groupname);
        }
        return $gpname;
    }

    public function memberlistsearch()
    {


        $gpid = $_SESSION["gpid"];
        $gpmemebr = array();
        $gplead = Groups::model()->find(array(
            'select' => 'group_lead',
            'condition' => 'group_id=' . $gpid,
        ));
        $gpld = $gplead['group_lead'];

        $groupsmemeberlist = Groups_members::model()->findAll(
            array(
                'select' => 'group_id,group_members',
                'condition' => 'group_id=' . $gpid,
            )
        );
        $gpmemebr[]     =   $gpld;
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }



        $criteria = new CDbCriteria;

        $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        //$criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('CONCAT_WS(" ",first_name,last_name)', $this->first_name, true);
        $criteria->compare('designation', $this->designation, true);
        $criteria->compare('department_id', $this->department_id, true);
        $criteria->addCondition('status = 0');
        $criteria->addCondition('user_type >= 1');
        $criteria->addNotInCondition('userid', $gpmemebr);

        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 10),
            'sort' => array(
                'defaultOrder' => 'user_type ASC',
            ),
            'criteria' => $criteria,
        ));
    }

    public function grp_memberlistsearch()
    {

        $gpid = $_SESSION["gpid"];
        $gpmemebr = array();
        $gplead = Groups::model()->find(array(
            'select' => 'group_lead',
            'condition' => 'group_id=' . $gpid,
        ));
        $gpld = $gplead['group_lead'];
        $groupsmemeberlist = Groups_members::model()->findAll(
            array(
                'select' => 'group_id,group_members,id',
                'condition' => 'group_id=' . $gpid,
            )
        );
        $gpmemebr[]     =   $gpld;
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }


        $criteria = new CDbCriteria;

        $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        // $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('CONCAT_WS(" ",first_name,last_name)', $this->full_name, true);
        $criteria->compare('designation', $this->designation, true);
        $criteria->compare('department_id', $this->department_id, true);
        $criteria->addCondition('status = 0');
        $criteria->addCondition('user_type >= 1');
        $criteria->addInCondition('userid', $gpmemebr);

        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 10),
            'sort' => array(
                'defaultOrder' => 'user_type ASC',
            ),
            'criteria' => $criteria,
        ));
    }




    public function getGrouplead($userid)
    {
        $gpid =  $_SESSION["gpid"];
        $gplead = Groups::model()->find(array(
            'select' => 'group_lead',
            'condition' => 'group_id=' . $gpid,
        ));
        $gpld = $gplead['group_lead'];
        if ($userid == $gpld) {
            return "Group Lead";
        } else {
            return "";
        }
    }

    public function getDeleteaction($userid)
    {
        $gpid =  $_SESSION["gpid"];
        $gplead = Groups::model()->find(array(
            'select' => 'group_lead',
            'condition' => 'group_id=' . $gpid,
        ));
        $gpld = $gplead['group_lead'];
        if ($userid == $gpld) {
            return 0;
        } else {
            return 1;
        }
    }
    public function getDepartment($userid)
    {
        $tbl = Yii::app()->db->tablePrefix;
        // echo "SELECT department_name FROM {tbl}department  LEFT JOIN {tbl}users ON {tbl}users.department_id = department_id";exit;
        $users = Yii::app()->db->createCommand()
            ->select(["{$tbl}department.department_name"])
            ->from("{$tbl}department")
            ->leftJoin("{$tbl}users", "{$tbl}department.department_id = {$tbl}users.department_id")
            ->where("{$tbl}users.userid=:userid", array(':userid' => $userid))
            ->queryRow();

        if (isset($users) && ($users != NULL)) {
            return $users['department_name'];
        }
    }
}
