<?php

/**
 * This is the model class for table "{{material_requisition}}".
 *
 * The followings are the available columns in table '{{material_requisition}}':
 * @property integer $requisition_id
 * @property integer $requisition_project_id
 * @property integer $requisition_task_id
 * @property string $requisition_no
 
 * @property string $requisition_unit
 * @property integer $requisition_quantity
 * @property string $requisition_date
 * @property integer $requisition_status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Projects $requisitionProject
 * @property Tasks $requisitionTask
 */
class MaterialRequisition extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MaterialRequisition the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{material_requisition}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('requisition_project_id, requisition_task_id, requisition_no,  created_by, updated_by', 'required'),
            // array('requisition_project_id, requisition_task_id, requisition_quantity, requisition_status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
            array('requisition_no', 'length', 'max' => 200),
            array('created_date, updated_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('requisition_id, requisition_project_id, requisition_task_id, requisition_no,requisition_status, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'requisitionProject' => array(self::BELONGS_TO, 'Projects', 'requisition_project_id'),
            'requisitionTask' => array(self::BELONGS_TO, 'Tasks', 'requisition_task_id'),
            'requisitionCreated' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'requisition_id' => 'Requisition',
            'requisition_project_id' => 'Project',
            'requisition_task_id' => 'Task',
            'requisition_no' => 'Requisition No',
            'requisition_status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('requisition_id', $this->requisition_id);
        $criteria->compare('requisition_project_id', $this->requisition_project_id);
        $criteria->compare('requisition_task_id', $this->requisition_task_id);
        $criteria->compare('requisition_no', $this->requisition_no, true);
        $criteria->compare('requisition_status', $this->requisition_status);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);

        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25),
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'requisition_id desc',
            ),
        )
        );
    }
    public function mrStatus($mr_id)
    {


        $material_req = MaterialRequisition::model()->findByPk($mr_id);


        if ($material_req->requisition_status == 0) {
            $status = "Not Approved";
        } elseif ($material_req->requisition_status == 1) {
            if ($material_req->purchase_id == "") {
                $status = "Approved";
            } else {
                $sql = "SELECT purchase_billing_status FROM jp_purchase "
                    . " WHERE p_id=" . $material_req->purchase_id;
                $po_bill_status_id = Yii::app()->db->createCommand($sql)->queryScalar();
                if ($po_bill_status_id) {
                    $status_sql = "SELECT caption FROM jp_status where sid=" . $po_bill_status_id;
                    $status = Yii::app()->db->createCommand($status_sql)->queryScalar();
                } else {
                    $status = "Not Approved";
                }
            }
        } elseif ($material_req->requisition_status == 2) {

            $status = "Rejected";
        }



        return $status;
    }
}
