<?php

/**
 * This is the model class for table "{{time_report}}".
 *
 * The followings are the available columns in table '{{time_report}}':
 * @property integer $teid
 * @property integer $project_id
 * @property string $project
 * @property integer $task_id
 * @property string $task
 * @property string $description
 * @property string $work_type
 * @property string $entry_date
 * @property double $hours
 * @property integer $billable
 * @property integer $user_id
 */
class TimeReport extends CActiveRecord {

    public $date_from, $date_till,$project1id;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TimeReport the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{time_report}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('task, description, entry_date, hours, billable, user_id, date_from, username, date_till', 'required'),
            array('teid, project_id, task_id, billable, user_id', 'numerical', 'integerOnly' => true),
            array('hours', 'numerical'),
            array('project', 'length', 'max' => 100),
            array('task', 'length', 'max' => 200),
            array('work_type', 'length', 'max' => 30),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('teid, project_id, project, task_id, task,username, description, work_type, entry_date, hours, billable, user_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'teid' => 'Teid',
            'project_id' => 'Project',
            'project' => 'Project',
            'task_id' => 'Task',
            'task' => 'Task',
            'description' => 'Description',
            'work_type' => 'Work Type',
            'date_from' => 'Date from',
            'date_till' => 'Date Till',
            'hours' => 'Hours',
            'billable' => 'Billable',
            'user_id' => 'User',
            'username' => 'Username'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('teid', $this->teid);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('project', $this->project, true);
        $criteria->compare('task_id', $this->task_id);
        $criteria->compare('task', $this->task, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('work_type', $this->work_type, true);
        $criteria->compare('entry_date', $this->entry_date, true);
        $criteria->compare('hours', $this->hours);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('username', $this->username);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function reportSearch() {
        $where = '';
        $condition = array();
        if(Yii::app()->user->role > 1){
         $members = Groups::model()->findAll(
                     array(
                         'select' => 'group_id',
                         'condition' => 'group_lead=' . Yii::app()->user->id,
                ));
                foreach ($members as $data){
                    $list = Groups_members::model()->findAll(
                         array(
                             'select' => 'group_id,group_members,id',
                             'condition' => 'group_id=' . $data['group_id'],
                    ));
                    foreach($list as $groupsmem) {
                        $gpmem[] = $groupsmem['group_members'];
                    }
                }
               $gpmem[] =  Yii::app()->user->id;
            $gpmem = implode(',', $gpmem); 
            $condition[] = 'user_id IN ('.$gpmem.')';
        }
        if (isset($_REQUEST['TimeReport'])) {
            if (isset($_REQUEST['TimeReport']['description']) and ($_REQUEST['TimeReport']['description']) != '')
                $condition[] = 'te.description like "%' . $_REQUEST['TimeReport']['description'] . '%"';
            if (isset($_REQUEST['TimeReport']['project']) and ($_REQUEST['TimeReport']['project']) != '')
                $condition[] = 'p.name like "%' . $_REQUEST['TimeReport']['project'] . '%"';
            
            if (isset($_REQUEST['TimeReport']['projectid']) and ($_REQUEST['TimeReport']['projectid']) != '')
                $condition[] = 'p.pid = "' . $_REQUEST['TimeReport']['projectid'] . '"';            
            
            if (isset($_REQUEST['TimeReport']['title']) and ($_REQUEST['TimeReport']['task']) != '')
                $condition[] = 'title like "%' . $_REQUEST['TimeReport']['task'] . '%"';
            if (isset($_REQUEST['TimeReport']['billable']) and ($_REQUEST['TimeReport']['billable']) != '')
                //$condition[] = ' te.`billable` = '.($_REQUEST['TimeReport']['billable']=='Yes'?3:4);
                $condition[] = ' te.`billable` = '.($_REQUEST['TimeReport']['billable']);
            if (isset($_REQUEST['TimeReport']['work_type']) and ($_REQUEST['TimeReport']['work_type']) != '')
                $condition[] = 'te.work_type = "' . $_REQUEST['TimeReport']['work_type'] . '"';

            if (isset($_REQUEST['TimeReport']['date_from']) and ($_REQUEST['TimeReport']['date_from']) != '')
                $condition[] = 'te.entry_date >= "' . $_REQUEST['TimeReport']['date_from'] . '"';

            if (isset($_REQUEST['TimeReport']['date_till']) and ($_REQUEST['TimeReport']['date_till']) != '')
                $condition[] = 'te.entry_date <= "' . $_REQUEST['TimeReport']['date_till'] . '"';
        }

        if (!isset($_REQUEST['TimeReport']['date_from']) or ($_REQUEST['TimeReport']['date_from']) == '')
            $condition[] = 'te.entry_date >= "' . date("Y-m-") . "1" . '"';
        if (!isset($_REQUEST['TimeReport']['date_till']) or ($_REQUEST['TimeReport']['date_till']) == '')
            $condition[] = 'te.entry_date <= "' . date("Y-m-d") . '"';

        if (Yii::app()->user->role == 3)
            $condition[] = 'te.user_id ="' . Yii::app()->user->id . '"';
     else if (isset($_REQUEST['TimeReport']['username']) and ($_REQUEST['TimeReport']['username']))
            $condition[] = 'te.user_id ="' . $_REQUEST['TimeReport']['username'] . '"';          
        else if (isset($_REQUEST['TimeReport']['user_id']) and ($_REQUEST['TimeReport']['user_id']))
            $condition[] = 'te.user_id ="' . $_REQUEST['TimeReport']['user_id'] . '"';      
        

        if (count($condition))
            $where = " WHERE " . implode(' AND ', $condition);


        $fields = " te.teid as id, p.pid AS project_id, p.name AS project, tsk.tskid AS task_id, tsk.title AS task, 
            te.description, wt.work_type, te.entry_date, te.hours, if(te.billable=3,'Yes','No') as billable, te.user_id, u.first_name as username";
        $query = " FROM `pms_time_entry` AS te
INNER JOIN pms_tasks AS tsk ON tsk.`tskid` = te.tskid
LEFT JOIN pms_projects AS p ON p.pid = tsk.project_id 
INNER JOIN pms_users as u on u.userid=te.user_id
INNER JOIN pms_work_type AS wt ON wt.wtid = te.work_type " . $where;

       

        $count = Yii::app()->db->createCommand("SELECT count(*) " . $query)->queryScalar();

        return $dataProvider = new CSqlDataProvider("SELECT $fields " . $query, array('pagination' => array('pageSize' => $count),
            'sort' => array(
                 'defaultOrder' => 'entry_date DESC',
                'attributes' => array(
                    'project', 'task', 'entry_date', 'billable' // csv of sortable column names
                )
            ), 'totalItemCount' => $count));
    }

    public function getTotals($ids) {
        $where = '';
        $condition = array();
        if (isset($_REQUEST['TimeReport'])) {
            if (isset($_REQUEST['TimeReport']['description']) and ($_REQUEST['TimeReport']['description']) != '')
                $condition[] = 'te.description like "%' . $_REQUEST['TimeReport']['description'] . '%"';
            if (isset($_REQUEST['TimeReport']['project']) and ($_REQUEST['TimeReport']['project']) != '')
                $condition[] = 'p.name like "%' . $_REQUEST['TimeReport']['project'] . '%"';
            
            if (isset($_REQUEST['TimeReport']['projectid']) and ($_REQUEST['TimeReport']['projectid']) != '')
                $condition[] = 'p.pid = "' . $_REQUEST['TimeReport']['projectid'] . '"';  
            
            if (isset($_REQUEST['TimeReport']['title']) and ($_REQUEST['TimeReport']['task']) != '')
                $condition[] = 'title like "%' . $_REQUEST['TimeReport']['task'] . '%"';
            if (isset($_REQUEST['TimeReport']['billable']) and ($_REQUEST['TimeReport']['billable']) != '')
                $condition[] = ' te.`billable` = '.($_REQUEST['TimeReport']['billable']=='Yes'?3:4);

            if (isset($_REQUEST['TimeReport']['work_type']) and ($_REQUEST['TimeReport']['work_type']) != '')
                $condition[] = 'te.work_type = "' . $_REQUEST['TimeReport']['work_type'] . '"';

            if (isset($_REQUEST['TimeReport']['date_from']) and ($_REQUEST['TimeReport']['date_from']) != '')
                $condition[] = 'te.entry_date >= "' . $_REQUEST['TimeReport']['date_from'] . '"';


            if (isset($_REQUEST['TimeReport']['date_till']) and ($_REQUEST['TimeReport']['date_till']) != '')
                $condition[] = 'te.entry_date <= "' . $_REQUEST['TimeReport']['date_till'] . '"';
        }

        if (!isset($_REQUEST['TimeReport']['date_from']) or ($_REQUEST['TimeReport']['date_from']) == '')
            $condition[] = 'te.entry_date >= "' . date("Y-m-") . "1" . '"';
        if (!isset($_REQUEST['TimeReport']['date_till']) or ($_REQUEST['TimeReport']['date_till']) == '')
            $condition[] = 'te.entry_date <= "' . date("Y-m-d") . '"';

       if (Yii::app()->user->role == 3)
            $condition[] = 'te.user_id ="' . Yii::app()->user->id . '"';
        else if (isset($_REQUEST['TimeReport']['user_id']) and ($_REQUEST['TimeReport']['user_id']))
            $condition[] = 'te.user_id ="' . $_REQUEST['TimeReport']['user_id'] . '"';
        else if (isset($_REQUEST['TimeReport']['username']) and ($_REQUEST['TimeReport']['username']))
            $condition[] = 'te.user_id ="' . $_REQUEST['TimeReport']['username'] . '"';          

        if (count($condition))
            $where = " WHERE " . implode(' AND ', $condition);

        $query = "SELECT sum(hours) as hours FROM `pms_time_entry` AS te
INNER JOIN pms_tasks AS tsk ON tsk.`tskid` = te.tskid
LEFT JOIN pms_projects AS p ON p.pid = tsk.project_id INNER JOIN pms_users as u on u.userid=te.user_id
INNER JOIN pms_work_type AS wt ON wt.wtid = te.work_type " . $where;

        $sum_hrs = Yii::app()->db->createCommand($query)->queryScalar();
        return "<b>" . $sum_hrs . "</b>";
    }
}