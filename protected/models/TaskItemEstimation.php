<?php

/**
 * This is the model class for table "{{task_item_estimation}}".
 *
 * The followings are the available columns in table '{{task_item_estimation}}':
 * @property integer $estimation_id
 * @property integer $task_id
 * @property integer $template_id
 * @property integer $item_id
 * @property integer $item_quantity_required
 * @property integer $item_quantity_used
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Tasks $task
 * @property Template $template
 * @property TemplateItems $item
 */
class TaskItemEstimation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TaskItemEstimation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{task_item_estimation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('task_id, template_id, item_id,  created_by', 'required'),
			array('task_id, template_id, item_id, item_quantity_required, item_quantity_used, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('estimation_id, task_id, template_id, item_id, item_quantity_required, item_quantity_used, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'task' => array(self::BELONGS_TO, 'Tasks', 'task_id'),
			'template' => array(self::BELONGS_TO, 'Template', 'template_id'),
			'item' => array(self::BELONGS_TO, 'TemplateItems', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'estimation_id' => 'Estimation',
			'task_id' => 'Task',
			'template_id' => 'Template',
			'item_id' => 'Item',
			'item_quantity_required' => 'Item Quantity Required',
			'item_quantity_used' => 'Item Quantity Used',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('estimation_id',$this->estimation_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('template_id',$this->template_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('item_quantity_required',$this->item_quantity_required);
		$criteria->compare('item_quantity_used',$this->item_quantity_used);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}