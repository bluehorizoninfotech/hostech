<?php
/*
 * For top menu module 
 * 
 */
Yii::import('zii.widgets.CWidget');

class MasqueradeWidget extends CWidget {

    public function run() {
        $tblpx = yii::app()->db->tablePrefix;
        $masquerade = "";
         //Change user login from Direct admin user
         if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->mainuser_role) && (Yii::app()->user->mainuser_role == 1)) {
            $masquerade = '<div class="switch-user-custom"><span>Switch user: </span>';
            $sql = "SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name, `{$tblpx}user_roles`.`role` FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type  WHERE status=0 ORDER BY user_type,full_name ASC";
             $result = Yii::app()->db->createCommand($sql)->queryAll();
             $listdata = CHtml::listData($result, 'userid', 'full_name', 'role');
             $masquerade .= CHtml::dropDownList('shift_userid', '', $listdata, array('options' => array(Yii::app()->user->id => array('selected' => true)),
               'style' =>'color:black', 
                                            'ajax' => array(
                                                'type' => 'POST', //request type
                                                'url' => Yii::app()->controller->createUrl('/users/usershift'), //url to call.
                                                'success' => 'js:function(data){
                                        location.reload();
                                    }',
                                                'data' => array('shift_userid' => 'js:this.value'),
                                        )));
              if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->id) && Yii::app()->user->mainuser_id != Yii::app()->user->id) {  
                $masquerade .= " <span class='mobile-block'>(Warning: You on other user's session)</span>";
               }                               
           $masquerade .= '</div>';                          

             
          }
           $this->render('masquerade', array('masquerade'=> $masquerade));
     }
 
}

?>
