<?php

/*
 * For top menu module 
 *  
 */
Yii::import('zii.widgets.CPortlet');

class LatestTask extends CPortlet {
    protected function renderContent() {
        // $this->render("DisplayEscalated");
        $model = new Tasks('assignedtask');
        $this->render('LatestTask', array(
            'model' => $model,
        ));
    }

}

?>