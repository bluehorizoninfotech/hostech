<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    .summary {
        float: left;
    }

    .parent {
        max-height: 250px;
    }

    @media(min-width: 767px) {
        .parent {
            max-height: 300px;
        }
    }
</style>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'escalated-task-grid',
    'itemsCssClass' => 'table table-bordered',
    'template' => '<div class="parent">{items}</div>{summary}{pager}',
    'dataProvider' => $model->displayEscalated(),
    'afterAjaxUpdate' => 'function() { $j(".table").tableHeadFixer({"head" : true}); }',
    'columns' => array(
        array(
            'header' => 'DATE',
            'value' => 'date("d-M-y", strtotime($data->updated_date))',
        ),
        array(
            'name' => 'project_id',
            'value' => '$data->project->name',
        ),
        array(
            'name' => 'task_type',
            'value' => function ($model) {
                if ($model->task_type == 1) {
                    return "External";
                } else {
                    return "Internal";
                }
            },
            'visible' => in_array('/site/internalescalated', Yii::app()->session['menuauthlist']) ? true : false,
        ),
        array(
            'name' => '# TASK',
            'value' => ' ($data->tskid!=NULL)? 
            CHtml::link($data->tskid, array("Tasks/view","id"=>$data->tskid),array("target"=>"_blank")) : " -- " ',
            'type' => 'raw',

        ),
        array(
            'name' => 'REASON',
            'value' => '$data->getlatestdescription($data->tskid,$data->task_type)',
        ),



    ),
));

?>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
        $j(".table").tableHeadFixer({
            'head': true
        });
    });
</script>