<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    /* .summary {
    float: left;
} */
    .parent {
        max-height: 250px;
    }

    @media(min-width: 767px) {
        .parent {
            max-height: 300px;
        }
    }
</style>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'today-workers-grid',
    'itemsCssClass' => 'table table-borderd',
    'template' => '<div class="table-responsive">{items}</div>{summary}{pager}',
    'dataProvider' => $model->getWorkers(),
    'afterAjaxUpdate' => 'function() { $j(".table").tableHeadFixer({"head" : true}); }',
    'columns' => array(
        array(
            'header' => 'PROJECT',
            'value' => '$data->project->name',
        ),
        array(
            'header' => 'SITE',
            'value' => '($data->tasks->clientsite_id)?$data->getsitename($data->tasks->clientsite_id):"--"',
        ),
        array(
            'header' => 'WORK TYPE',
            'value' => '$data->worktype->work_type',
        ),
        array(
            'header' => '# Skilled Workers',
            'value' => '$data->skilled_workers',
        ),
        array(
            'header' => '# UnSkilled Workers',
            'value' => '$data->unskilled_workers',
        ),



    ),
));
?>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
        $j(".table").tableHeadFixer({
            'head': true
        });
    });
</script>