<style>
         .bg_green {
            background-color: #679e6a8f !important;
        }
    </style>
<?php
$model=new Tasks();

 $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'daily-work-progress-task-grid',
    'htmlOptions' => array('class' => 'grid-view'),
    'itemsCssClass' => 'table table-bordered progress-table',
    'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',

    'pager' => array(
        'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->wpr_task(),
    'ajaxUpdate' => false,
    //'filter' => $model,
    //'selectableRows' => 2,
    'rowCssClassExpression' => '$data->task_wpr_id > 0 ? "bg_green":""',
    'columns' => array(

        array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('style' => 'width:20px;'),),

        array(
            'name' => 'title',
            'value' => '($data->acknowledge_by=="")?$data->title:$data->title."<i class=\"fa fa-check\" style=\"margin-left:5px;color:green\"></i>" ',
            'type' => 'raw',
            
        ),

        array(
            'name' => 'project_id',
            'value' => '(empty($data->project_id)?"test":CHtml::link($data->project->name, array("Projects/view","id"=>$data->project_id)))',
            'type' => 'raw',
           
        ),
        array(
            'name' => 'coordinator',
            'value' => '!empty($data->coordinator)?$data->coordinator0->first_name." ".$data->coordinator0->last_name:""',
            'type' => 'raw',
            
        ),
        array(
            'name' => 'quantity',
            'value' => '$data->quantity'
        ),
        array(
            'name' => 'start_date',
            'value' => 'date("d-M-y",strtotime($data->start_date))',
            'type' => 'html',
            'htmlOptions' => array('class' => 'date_start')
        ),
        array(
            'name' => 'due_date',
            'value' => 'date("d-M-y",strtotime($data->due_date))',
            'type' => 'html',
            'htmlOptions' => array('class' => 'date_end'),
            'cssClassExpression' => '(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "text-danger1" : ""',
        ),
        array(
            'name' => 'status',
            'header' => 'Status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
         ),
       
        array(
            'htmlOptions' => array('style' => 'white-space:nowrap;'),
            'class' => 'ButtonColumn',
            'template' => '{create_wpr}',
            'evaluateID' => true,

            'buttons' => array(
               
                'create_wpr' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'fa fa-plus add_work_progress', 'title' => 'Create WPR', 'id' => '$data->tskid'),
                ),
                

              
               
              
            )

        ),


     
       
    ),
));
?>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        $j("#fixTable").tableHeadFixer({
            'head': true
        });
    });

    $('.add_work_progress').click(function() {
        var task_id = this.id;
        alert(task_id);

        $.ajax({
            "type": "POST",

            "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/getwprtaskdetails'); ?>",
            "dataType": "json",
            "data": {
                task_id: task_id
            },
            "success": function(data) {
                $('#wpr_form_div').html(data);
            }
        });

    });
   

</script>