<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style type="text/css">
    .parent {
        max-height: 250px;
    }

    @media(min-width: 767px) {
        .parent {
            max-height: 300px;
        }
    }
</style>
<?php
$model = new Tasks();
$data = $model->adminworkload();
?>
<div class="parent">
    <table class="table" id="fixTable">
        <thead>
            <tr>
                <th>Supervisor</th>
                <th>Project</th>
                <th>Status</th>

            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($data)) {
                foreach ($data as $data) {
                    if ($data['project'] != '') {


            ?>
                        <tr class="odd">
                            <td style="vertical-align:middle">
                                <?php
                                echo $data['user']

                                ?>
                            </td>
                            <td style="vertical-align:middle;white-space:nowrap;">
                                <?= $data['project'] ?>
                            </td>
                            <td style="vertical-align:middle;white-space:nowrap;">
                                <?= $data['status'] ?>
                            </td>

                        </tr>

            <?php

                    }
                }
            }

            ?>
        </tbody>
    </table>
</div>