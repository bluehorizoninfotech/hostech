<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style type="text/css">
    .parent {
        max-height: 250px;
    }
    @media(min-width: 767px) {
        .parent {
            max-height: 300px;
        }
    }
</style>
<?php
Yii::app()->getModule('masters');
$model = new Tasks();
$data = $model->milestones();
$milestones = new Milestone();
$milestones_count = $milestones->milestonesCount();
?>
<div class="parent custom-parent">
    <table class="table table-bordered" id="fixTable">
        <thead>
            <tr>
                <th>Title</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Progress %</th>
                <!-- <th>Quantity</th> -->
                <th>Contractor</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $mod) {
                    if ($mod->milestone_id != 0 && $mod->project_id != '') {
                        $criteria = new CDbCriteria();
                        $criteria->condition = 'project_id=' . $mod->project_id
                                . ' AND milestone_id = ' . $mod->milestone_id;
                        $criteria->group = 'contractor_id';
                        $contractor_milestone_model = Tasks::model()->findAll($criteria);
                        $rowspan = 'rowspan="' . (count($contractor_milestone_model) + 1) . '"';
                        $percentage_value = $model->percentagecalculation($mod->milestone_id);
                        ?>
                        <tr class="odd">
                            <td <?php echo $rowspan; ?> style="vertical-align:middle">
                                <?php
                                echo $mod->milestone->milestone_title
                                . "<small class='ref-rext'>Ref #"
                                . $mod->milestone_id."</small>";
                                ?>
                            </td>
                            <td <?php echo $rowspan ?> style="vertical-align:middle;white-space:nowrap;">
                                <?php echo date('d-M-Y', strtotime($mod->milestone->start_date)) ?>
                            </td>
                            <td <?php echo $rowspan ?> style="vertical-align:middle;white-space:nowrap;">
                                <?php echo date('d-M-Y', strtotime($mod->milestone->end_date)) ?>
                            </td>
                            <td <?php echo $rowspan ?> style="vertical-align:middle;white-space:nowrap;">
                                <?php echo $percentage_value ?>
                            </td>
                        </tr>
                        <?php foreach ($contractor_milestone_model as $contrac) { ?>
                            <tr class="odd">
                                <?php
                                $sql = "SELECT SUM(quantity) as qty FROM pms_tasks "
                                        . "WHERE project_id ='" . $mod->project_id . "' "
                                        . "AND milestone_id = '" . $mod->milestone_id . "' "
                                        . "AND contractor_id = '" . $contrac->contractor_id . "' "
                                        . "AND task_type = 1";
                                $tot_qty = Yii::app()->db->createCommand($sql)->queryRow();
                                ?>
                                <!-- <td><?php echo $tot_qty['qty'] ?></td> -->
                                <td><?php echo isset($contrac->contractor->contractor_title) ? $contrac->contractor->contractor_title : '<i>No Name </i>' ?></td>
                            </tr>
                            <?php
                        }
                    }
                }
            } else {
                if ($milestones_count != 0) {
                    echo '<tr><td colspan="6">' . $milestones_count . ' milestones yet to start</td></tr>';
                }
            }
            ?>
        </tbody>
    </table>
</div>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        $j("#fixTable").tableHeadFixer({
            'head': true
        });
    });
</script>