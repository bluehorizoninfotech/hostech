<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    /* .summary {
    float: left;
} */
    .parent {
        max-height: 250px !important;
    }

</style>
<?php
Yii::app()->getModule('masters');
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'task-grid',
    'itemsCssClass' => 'table table-bordered',
    'template' => '<div class="parent custom-parent">{items}</div>{summary}{pager}',
    'pager' => array(
        'header' => '',
        'nextPageLabel' => 'Next',
        'prevPageLabel' => 'Previous',
        'selectedPageCssClass' => 'active',
        'maxButtonCount' => '5'
    ),
    'dataProvider' => $model->assignedtask($type = ''),
    'afterAjaxUpdate' => 'function() { $j(".table").tableHeadFixer({"head" : true}); }',
    'columns' => array(
        array(
            'header' => 'Title',
            'type' => 'raw',
            'value' => 'CHtml::link($data->title,array(\'Tasks/view\',\'id\'=>$data->tskid), array(\'target\'=>\'_blank\'))'
            . '."<small class=\'ref-rext\'>#".$data->tskid."</small>"',
        ),
        array(
            'header' => 'Start Date',
            'value' => 'date("d-M-Y", strtotime($data->start_date))',
            'htmlOptions' => array('style' => 'white-space:nowrap'),
            'headerHtmlOptions' => array('style' => 'white-space:nowrap')
        ),
        array(
            'header' => 'End Date',
            'value' => 'date("d-M-Y", strtotime($data->due_date))',
            'htmlOptions' => array('style' => 'white-space:nowrap'),
            'headerHtmlOptions' => array('style' => 'white-space:nowrap')
        ),
        array(
            'name' => 'Present Status %',
            'headerHtmlOptions' => array('style' => 'white-space:nowrap'),
            'value' => function ($data) {
                return Tasks::statuspercentage($data->tskid);
            }
        ),
        array(
            'name' => 'quantity',
            'headerHtmlOptions' => array('style' => 'white-space:nowrap'),
            'value' => function ($data) {
                return !empty($data->unit) ? $data->quantity . ' (' . $data->unit0->unit_code . ')' : $data->quantity;
            }
        ),
        // 'quantity',
        array(
            'header' => 'Contractor',
            'value' => 'isset($data->contractor->contractor_title)?$data->contractor->contractor_title:""',
        ),
    ),
));
?>

<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        $j(".table").tableHeadFixer({
            'head': true
        });
    });
    $j(document).ajaxComplete(function () {
        $j(".table").tableHeadFixer({
            'head': true
        });
    });
</script>