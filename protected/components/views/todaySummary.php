<?php
$timeEntry = $this->getSummary();
$tdate = '';
echo "<div style='overflow:auto;height: 345px;'>";
 array_multisort(array_column($timeEntry, "id"), SORT_DESC, $timeEntry);

  //array_multisort($timeEntry, array('id'=>SORT_DESC));

foreach ($timeEntry as $time_entry) {
    echo "<div class='dash_work_overview'>";
    if ($tdate != $time_entry['tentry_date']) {
        $tdate = $time_entry['tentry_date'];
        echo "<div class='date_qk_smy'>" . $time_entry['tentry_date'] . "</div>";
    }
    if ($time_entry['approve_status'] == 0) {
        $status = "Pending For Approval";
    } elseif ($time_entry['approve_status'] == 1) {
        $status = "Approved";
    } elseif ($time_entry['approve_status'] == 2) {
        $status = "Rejected";
    }
    //echo "<div class='hrs_box'>".$time_entry['hours']."</div>";
    echo "<div class='hrs_box'>" . $time_entry['completed_percent'] . "%</div>";

    echo "<div class='qk_desc'><b>" . $time_entry['first_name'] . "</b><br /><span class='task_name'>" . $time_entry['tsk_name'] . "</span></div>";
    echo "<div class='qk_desc'><span class='task_name'>" . $status . "</span></div>";
    echo "</div>";
}

echo "<br clear='all' /></div>";
