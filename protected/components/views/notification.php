<?php
/*
if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) {
echo '<li id="header_notification_bar"><a title="'.$exptskcount.' expired tasks!" href="'.Yii::app()->createUrl('tasks/index2', array('type' => 'expired')).'" ><i class="icon-bell"></i><span class="badge badge-default">'.$exptskcount.'</span></a></li>';
}
if (isset(Yii::app()->user->id)) {
echo '<li id="header_task_bar"><a title="You have '.$tskcount.' pending tasks!" href="'.Yii::app()->createUrl('tasks/mytask', array('type' => 'pending')).'" ><i class="icon-calendar"></i><span class="badge badge-default">'.$tskcount.'</span></a></li>';
}

if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) {
echo '<li id="header_task_bar"><a title="'.$userlist.' in-active users!" href="'.Yii::app()->createUrl('Users/Userlog').'" ><i class="icon-user-unfollow"></i><span class="badge badge-default">'.$userlist.'</span></a></li>';
} 
*/
?>
<li class="dropdown dropdown-user dropdown-dark dropdown-extended dropdown-notification" id="header_notification_bar">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true" aria-expanded="false">
        <i class="icon-bell" style="font-size:20px;vertical-align:middle;"></i>      
        <!-- <span class="badge badge-default notify-number" > <?php echo $notifycount; ?> </span> -->
        <span class="badge" style="background-color: #f45245;margin-top: -30px;"> <?php echo  $notifycount;?>  </span>
    </a>
    <ul class="dropdown-menu dropdown-menu-default">

        <li>
            <a title="<?php echo $exptskcount; ?> expired tasks!" href="<?php echo Yii::app()->createUrl('tasks/index2', array('type' => 'expired')); ?>">Expired tasks <span class="badge badge-warning"><?php echo $exptskcount; ?></span></a>
        </li>
        <li>
            <a title="<?php echo $unassigned_task_count; ?> Unassigned tasks!" href="<?php echo Yii::app()->createUrl('tasks/index2', array('type' => 'unassigned')); ?>">Unassigned tasks <span class="badge badge-warning"><?php echo $unassigned_task_count; ?></span></a>
        </li>

        <?php if (isset(Yii::app()->user->id)) { ?>
            <li>
                <a title="You have <?php echo $tskcount; ?> pending tasks!" href="<?php echo Yii::app()->createUrl('tasks/mytask', array('type' => 'pending')); ?>">Pending tasks <span class="badge badge-danger"><?php echo $tskcount; ?></span></a>
            </li>
        <?php } ?>
        <?php if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) { ?>
            <li>
                <a title="<?php echo $userlist; ?> active users!" href="<?php echo Yii::app()->createUrl('Users/Userlog'); ?>">Active users <span class="badge badge-primary"><?php echo $userlist; ?></span></a>
            </li>
        <?php } ?>

        <li>
            <a title="<?php echo $escalted; ?>escalated tasks!" href="<?php echo Yii::app()->createUrl('site/index2#escalated_div'); ?>">Escalated Tasks <span class="badge badge-primary"><?php echo $escalted; ?></span></a>
        </li>

        <li>
            <a title="<?php echo $missed_task_count; ?> task missing deadlines!" href="<?php echo Yii::app()->createUrl('tasks/index2', array('type' => 'missed')); ?>">Task missing deadlines <span class="badge badge-primary"><?php echo $missed_task_count; ?></span></a>
        </li>

    </ul>
</li>