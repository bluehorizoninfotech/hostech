<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style type="text/css">
    .parent {
        max-height: 250px;
    }
    @media(min-width: 767px) {
        .parent {
            max-height: 300px;
        }
    }
</style>
<?php
Yii::app()->getModule('masters');
$model = new Tasks();
$data = $model->projects();

?>
<div class="parent">
    <table class="table" id="fixTable">
        <thead>
            <tr>
                <th>Title</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Progress %</th>
               
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $mod) {
                    if ($mod->pid != '') {
                       
                        $percentage_value = $model->projectpercentagecalculation($mod->pid);
                        ?>
                        <tr class="odd">
                            <td  style="vertical-align:middle">
                                <?php
                                echo $mod->name
                               
                                ?>
                            </td>
                            <td  style="vertical-align:middle;white-space:nowrap;">
                                <?php echo date('d-M-Y', strtotime($mod->start_date)) ?>
                            </td>
                            <td  style="vertical-align:middle;white-space:nowrap;">
                                <?php echo date('d-M-Y', strtotime($mod->end_date)) ?>
                            </td>
                            <td  style="vertical-align:middle;white-space:nowrap;">
                                <?php echo $percentage_value ?>
                            </td>
                        </tr>
                       
                            <?php
                        
                    }
                }
            } 
            ?>
        </tbody>
    </table>
</div>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        $j("#fixTable").tableHeadFixer({
            'head': true
        });
    });
</script>