<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'email-notification-grid',
    'itemsCssClass' => 'table table-bordered',
    'template' => '{items}{summary}{pager}', 
	'dataProvider'=>$model->getemailnotifications(),
	'columns'=>array(
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'htmlOptions' => array( "style"=>"width:120px;text-align: center;"),
            'buttons'=>array(
                'view' => array(
                    'label'=>'',
                    'imageUrl' =>false,
                    'url'=>'Yii::app()->createUrl("MailSettings/maillogview", array("id"=>$data->log_id))',  
                    
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View',),
                ),
            ),

        ),
        array(
            'header' => 'DATE',
            'value' =>'date("d-M-y", strtotime($data->send_date))',                
        ),
        array(
            'header' => 'RECIPIENT',
            'value' =>'$data->send_toarrayjoin($data->send_to)',                
        ),
        array(
            'header' => 'TYPE',
            'value' =>'$data->mail_type',                
        ),
        array(
            'header' => 'STATUS',
            'value' =>'(($data->sendemail_status==1)?"Sent":"Not Sent")',                
        ),
        array(
            'header' => 'Remarks',
            'value' =>'(($data->notsent_reason)?$data->notsent_reason:"")',                
        ),
        array(
            'header' => 'Time',
            'value' =>function($model){
                return date('d-M-y H:i:s',strtotime($model->send_date));
            },                
        ),
        
		
	),
));
?>
<style>
/* .summary {
    float: left;
} */
</style>