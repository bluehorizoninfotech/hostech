<?php
/*--------------------Privileges--------------------*/
define('ADD', 1);
define('EDIT', 2);
define('DELETE', 3);
define('VIEW', 4);
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{

    public $logo = '';
    public $favicon = '';

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->getLogoAndFavicon();
    }

    public function getLogoAndFavicon()
    {
        $app_root = YiiBase::getPathOfAlias('webroot');
        $theme_asset_url = Yii::app()->baseUrl . "/themes/assets/";
        $this->logo = $theme_asset_url . 'default-logo.png';
        if (file_exists($app_root . "/themes/assets/logo.png")) {
            $this->logo = $theme_asset_url . 'logo.png';
        }
        $this->favicon = $theme_asset_url . 'default-favicon.ico';
        if (file_exists($app_root . "/themes/assets/favicon.ico")) {
            $this->favicon = $theme_asset_url . 'favicon.ico';
        }

        return array('logo' => $this->logo, 'favicon' => $this->favicon);
    }

    public static function datetime($format = 'Y-m-d H:i:s', $date_str = false)
    {
        if ($date_str !== false)
            return date($format, strtotime($date_str));

        return date($format);
    }

    public function taskChange($date, $site_id)
    {

        if (!empty($date)) {

            if ($site_id == 1) {

                $Criteria = new CDbCriteria();
                $Criteria->condition = "start_date <= '" . $date . "' AND due_date >= '" . $date . "' AND (clientsite_id = 1 OR clientsite_id IS NULL) AND status != 2";
                $tasks = Tasks::model()->findAll($Criteria);
            } else {
                $Criteria = new CDbCriteria();
                $Criteria->condition = "start_date <= '" . $date . "' AND due_date >= '" . $date . "' AND clientsite_id = " . $site_id . " AND status != 2";
                $tasks = Tasks::model()->findAll($Criteria);
            }

            foreach ($tasks as $task) {
                $duration = 0;
                $Criteria1 = new CDbCriteria();
                if ($task->clientsite_id == 1 || $task->clientsite_id == NULL) {
                    $Criteria1->addCondition('status = 0');
                } else {
                    $Criteria1->addCondition('status = 1');
                }
                $Criteria1->addBetweenCondition("event_date", $task->start_date, $task->due_date, 'AND');
                $Criteria1->addCondition('site_id = ' . $site_id);
                $CalenderEvents = CalenderEvents::model()->findAll($Criteria1);
                $event_array = array();
                foreach ($CalenderEvents as $events) {
                    array_push($event_array, $events->event_date);
                }
                $start_date = new DateTime($task->start_date);
                $due_date = new DateTime($task->due_date);
                $due_date->modify('+1 day');
                $period = new DatePeriod(
                    $start_date,
                    new DateInterval('P1D'),
                    $due_date
                );
                foreach ($period as $key => $value) {
                    if (!in_array($value->format('Y-m-d'), $event_array)) {
                        $duration++;
                    }
                }
                $target = round($task->quantity / $duration);
                $task->daily_target = $target;
                $task->task_duration = $duration;
                $task->updated_by = yii::app()->user->id;
                $task->updated_date = date('Y-m-d');
                $task->save();
            }
        }
    }
    public function customAssets($file_location, $localFilePath = null)
    {
        if (defined('ASSETS_SOURCES') and ASSETS_SOURCES === 'offline') {
            return Yii::app()->theme->baseUrl . $localFilePath;
        } else {
            return $file_location;
        }
    }


    // 	public function smtpMailer($data = array(), $htmlmail = true)
    // 	{
    // 		$mail = new JPhpMailer();
    // 		$mail->IsSMTP();
    // 		$mail->Host = SMTP_HOST;
    // 		$mail->SMTPSecure = SMTP_SECURE;
    // 		$mail->SMTPAuth = SMTP_AUTH;
    // 		$mail->Username = SMTP_USERNAME;
    // 		$mail->Password = SMTP_PASSWORD;
    // 		$mail->Hostname = SMTP_HOST;
    // 		$mail->Port = SMTP_PORT;

    // 		extract($data);

    // 		if (count($mailfrom) == 2 and isset($mailfrom[0])) {
    // 			$mail->setFrom($mailfrom[0], ($mailfrom[1] == '' ? 'Hostech' : $mailfrom[1]));
    // 		} else {
    // 			$mail->setFrom($mailfrom['email'], $mailfrom['name']);
    // 		}
    // 		if (is_array($mailto)) {
    // 			foreach ($mailto as $toemail => $mtoname) {
    // 				$mail->addAddress($toemail, $mtoname);
    // 			}
    // 		} else {
    // 			$mail->addAddress($mailto);
    // 		}

    // 		if (isset($cc)) {
    // 			if (is_array($cc)) {
    // 				foreach ($cc as $ccmail => $ccname) {
    // 					$mail->AddCC($ccmail, $ccname);
    // 				}
    // 			} else {
    // 				$mail->addAddress($cc);
    // 			}
    // 		}

    // 		if (isset($replyto)) {
    // 			if (is_array($replyto)) {
    // 				foreach ($replyto as $rtmail => $rtname) {
    // 					$mail->AddReplyTo($rtmail, $rtname);
    // 				}
    // 			} else {
    // 				$mail->AddReplyTo($replyto);
    // 			}
    // 		}


    // 		$mail->isHTML($htmlmail);
    // 		$mail->Subject = $subject;
    // 		$mail->Body = $body;

    // 		if ($mail->send()) {
    // 			return true;
    // 		} else {

    // 			$logsql = "insert into hrms_email_sendlog values (null, '" . $mail->ErrorInfo . "-->" . json_encode($data) . "', '" . json_encode($_REQUEST) . "', '" . date('Y-m-d H:i:s') . "'," . yii::app()->user->id . ")";
    // 			Yii::app()->db->createCommand($logsql)->execute();

    // 			return $mail->ErrorInfo;
    // 		}


    // 		/*  //Maill call script
    // 		  $data = array();
    // 		  $data['htmlmail'] = true;   //true or false
    // 		  $data['replyto'] = array('email' => 'testing@bluehorizoninfotech.com', 'name' => 'Tester Name'); //optional
    // 		  $data['from'] = array('email' => 'tony.xa@bluehorizoninfotech.com', 'name' => 'Tony tester');
    // 		  $data['mailto'] = array('bala@bluehorizoninfotech.com', 'infobala@gmail.com'); //mandatory "can be string or array for multiple"
    // 		  $data['cc'] =  array('bala@bluehorizoninfotech.com'=>'hhhh', 'infobala@gmail.com'); //optional "can be string or array for multiple"
    // 		  $data['subject'] = 'Send SMTP mail from Yii1 Controller';

    // */
    // 	}

    public function paramsval($params)
    {
        if (isset(Yii::app()->params[$params])) {
            return Yii::app()->params[$params];
        } else {
            return '';
        }
    }


    public function addtoLeaveLog($userid, $data)
    {

        extract($data);

        $lv_log = Yii::app()->db->createCommand("SELECT * FROM `pms_leave_log` WHERE `userid` = " . $userid . " ORDER BY `pms_leave_log`.`id` DESC limit 1")->queryRow();

        $cu_co = 0;
        if ($lv_log['cu_co'] != '') {
            $cu_co = $lv_log['cu_co'];
        }
        $tot_lop = 0;
        if ($lv_log['tot_lop'] != '') {
            $tot_lop = $lv_log['tot_lop'];
        }

        Yii::app()->db->createCommand(" INSERT INTO `pms_leave_log`(`id`, `userid`, `cl_credited`, `er_credited`, `rh_credited`, `co_credited`, `ml_credited`, `rh_cu`, `cu_cl`, `cu_er`, `cu_co`, `cu_ml`, `tot_lop`, `created_date`, `created_by`, `comment`, `leave_id`, `comp_id`,`pl_creadeited`,`cu_pl`) VALUES (null, " . $userid . " ," . $cl . ", " . $el . "," . $rh . ",0," . $ml . "," . ($lv_log['rh_cu'] + ($rh)) . "," . ($lv_log['cu_cl'] + ($cl)) . "," . ($lv_log['cu_er'] + ($el)) . "," . $cu_co . "," . ($lv_log['cu_ml'] + ($ml)) . "," . $tot_lop . ",'" . date('Y-m-d H:i:s') . "'," . Yii::app()->user->id . ",'" . $coment . "',NULL,NULL," . $el . "," . ($lv_log['cu_pl'] + ($el)) . ")")->query();

        return;
    }

    function getDaysCount($start_date, $end_date)
    {
        $days_gone = 0;
        if (!empty($start_date)) {
            $start_date_data = new DateTime($start_date);
            if (strtotime(date('Y-m-d')) > strtotime($end_date)) {
                $todays_date = new DateTime($end_date);
                $todays_date->modify('+1 day');
            } else {
                $todays_date = new DateTime(date('Y-m-d'));
                $todays_date->modify('+1 day');
            }

            $period = new DatePeriod(
                $start_date_data,
                new DateInterval('P1D'),
                $todays_date
            );


            foreach ($period as $key => $value) {
                $days_gone++;
            }
        }
        return $days_gone;
    }

    function getProjectCondition()
    {
        $condition = 'status = 1 ';
        if (isset(Yii::app()->user->role) && Yii::app()->user->role !== '1') {
            $condition .= 'AND (FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to) ';
            $criteria = new CDbCriteria;
            $criteria->select = 'project_id,tskid';
            $criteria->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
            $criteria->group = 'project_id';
            $project_ids = Tasks::model()->findAll($criteria);
            $project_id_array = array();
            foreach ($project_ids as $projid) {
                array_push($project_id_array, $projid['project_id']);
            }
            if (!empty($project_id_array)) {
                $project_id_array = implode(',', $project_id_array);
                $condition .= 'OR pid IN (' . $project_id_array . '))';
            } else {
                $condition .= ')';
            }
        }
        return $condition;
    }

    function getWeeksCount($month, $year)
    {
        $month = intval($month);                //force month to single integer if '0x'
        $suff = array('st', 'nd', 'rd', 'th', 'th', 'th');         //week suffixes
        $end = date('t', mktime(0, 0, 0, $month, 1, $year));         //last date day of month: 28 - 31
        $start = date('w', mktime(0, 0, 0, $month, 1, $year));     //1st day of month: 0 - 6 (Sun - Sat)
        $last = 7 - $start;                     //get last day date (Sat) of first week
        $noweeks = ceil((($end - ($last + 1)) / 7) + 1);        //total no. weeks in month
        $output = "";                        //initialize string		
        $monthlabel = str_pad($month, 2, '0', STR_PAD_LEFT);
        $output = array();
        for ($x = 1; $x < $noweeks + 1; $x++) {
            if ($x == 1) {
                $startdate = "$year-$monthlabel-01";
                $day = $last - 6;
            } else {
                $day = $last + 1 + (($x - 2) * 7);
                $day = str_pad($day, 2, '0', STR_PAD_LEFT);
                $startdate = "$year-$monthlabel-$day";
            }
            if ($x == $noweeks) {
                $enddate = "$year-$monthlabel-$end";
            } else {
                $dayend = $day + 6;
                $dayend = str_pad($dayend, 2, '0', STR_PAD_LEFT);
                $enddate = "$year-$monthlabel-$dayend";
            }
            $output[] = array('start_date' => $startdate, 'end_date' => $enddate);
        }
        return $output;
    }

    function getMonthLastDate($month, $year)
    {
        $dateString = $year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT);
        $lastDayOfMonth = date('t', strtotime($dateString));
        return $lastDayOfMonth;
    }
    function getMonthDates($month, $year)
    {
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for ($day = 1; $day <= $daysInMonth; $day++) {

            $date = date("$year-$month-$day");

            $output[] = $date;
        }
        return $output;
    }

    function getStartAndEndDate($week, $year)
    {
        $dateTime = new DateTime();
        $dateTime->setISODate($year, $week);
        $result['start_date'] = $dateTime->format('d-M-Y');
        $dateTime->modify('+6 days');
        $result['end_date'] = $dateTime->format('d-M-Y');
        return $result;
    }
    function getRomanNumerals($decimalInteger)
    {
        $n = intval($decimalInteger);
        $res = '';

        $roman_numerals = array(
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1
        );

        foreach ($roman_numerals as $roman => $numeral) {
            $matches = intval($n / $numeral);
            $res .= str_repeat($roman, $matches);
            $n = $n % $numeral;
        }

        return $res;
    }
    public function smtpMailerold($data = array(), $htmlmail = true)
    {
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        try {


            $mail = new JPhpMailer(true);
            $mail->IsSMTP();

            $mail->Host = $mail_model['smtp_host'];
            // $mail->SMTPSecure = $mail_model['smtp_secure']; 
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->Hostname = $mail_model['smtp_host'];
            $mail->Port = $mail_model['smtp_port'];
            extract($data);

            if (count($mailfrom) == 2 and isset($mailfrom[0])) {
                $mail->setFrom($mailfrom[0], ($mailfrom[1] == '' ? Yii::app()->name : $mailfrom[1]));
            } else {
                $mail->setFrom($mailfrom['email'], $mailfrom['name']);
            }

            if (is_array($mailto)) {
                foreach ($mailto as $toemail => $mtoname) {
                    $mail->addAddress($toemail, $mtoname);
                }
            } else {
                $mail->addAddress($mailto);
            }
            if (isset($cc)) {
                if (is_array($cc)) {
                    foreach ($cc as $ccmail => $ccname) {
                        $mail->AddCC($ccmail, $ccname);
                    }
                } else {
                    $mail->addAddress($cc);
                }
            }
            // if (isset($replyto)) 
            // {
            // 	if (is_array($replyto)) 
            // 	{
            // 		foreach ($replyto as $rtmail => $rtname) 
            // 		{
            // 			$mail->AddReplyTo($rtmail, $rtname);
            // 		}
            // 	} 
            // 	else 
            // 	{
            // 		$mail->AddReplyTo($replyto);
            // 	}
            // }
            $mail->isHTML($htmlmail);
            $mail->Subject = $subject;
            $mail->Body = $body;

            if ($mail->send() or (defined('SEND_EMAIL') and SEND_EMAIL == false)) {
                return true;
            } else {

                return $mail->ErrorInfo;
            }
        } catch (phpmailerException $e) {


            // $exceptionError = trim(strip_tags($e->errorMessage()));
            // if($exceptionError=="SMTP Error: Could not connect to SMTP host." or (defined('SEND_EMAIL') and SEND_EMAIL==false))
            // {
            // 	return true;
            // }
            if ($e->errorMessage()) {
                echo $e->errorMessage();
                echo "fail";
            } else {

                //echo $e->errorMessage();
                return true;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    public function smtpMailer($data = array(), $htmlmail = true)
    {
        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $mail = new JPhpMailer(true);

        try {

            $mail->isSMTP();
            $mail->Host = $mail_model['smtp_host'];
            $mail->SMTPAuth = $mail_model['smtp_auth'];
            $mail->Username = $mail_model['smtp_username'];
            $mail->Password = $mail_model['smtp_password'];
            $mail->SMTPSecure = $mail_model['smtp_secure'];
            $mail->Port = $mail_model['smtp_port'];

            extract($data);

            $mail->setFrom($mailfrom['email'], $mailfrom['name']);
            if (is_array($mailto)) {
                foreach ($mailto as $toemail => $mtoname) {

                    $mail->addAddress($toemail, $mtoname);
                }
            } else {
                $mail->addAddress($mailto);
            }

            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $body;

            $mail->send();
            echo "Mail has been sent successfully!";
        } catch (Exception $e) {

            $exceptionError = trim(strip_tags($e->errorMessage()));
            if ($exceptionError == "SMTP Error: Could not connect to SMTP host." or (defined('SEND_EMAIL') and SEND_EMAIL == false)) {
                //return true;

                echo $mail->ErrorInfo;
            } else {
                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            }
        }
    }
    public function getMilestones($project_tasks, $project_id)
    {
        $milstone_data = array();
        foreach ($project_tasks as $task) {

            if (!array_key_exists($task->milestone_id, $milstone_data)) {
                if (isset($task->contractor_id)) {
                    $contractor_title = $task->contractor->contractor_title;
                } else {
                    $contractor_title = '';
                }
                $milstone_data[$task->milestone_id] = array('id' => $task->milestone_id, 'title' => $task->milestone->milestone_title, 'contractor' => $contractor_title, 'project_id' => $project_id, 'ranking' => $task->milestone->ranking);
                if (!empty($task->area)) {
                    $area['title'] = $task->area0->area_title;
                    $area['id'] = $task->area;
                    $milstone_data[$task->milestone_id]['areas'][] = $area;
                } else {
                    $milstone_data[$task->milestone_id]['areas'] = array();
                }
                $milstone_data[$task->milestone_id]['work_type'] = array();
                $milstone_data[$task->milestone_id]['work_type'] = $this->setProjectWorkTypes($milstone_data[$task->milestone_id]['work_type'], $project_id, $task->work_type_id, $task->milestone_id);
                if (isset($milstone_data[$task->milestone_id]['progress_data'])) {
                    $milstone_data[$task->milestone_id]['progress_data'] = $this->getProgressPercentage($milstone_data[$task->milestone_id]['progress_data'], $task);
                }
            } else {
                if (isset($task->contractor_id)) {
                    $contractor_title = $task->contractor->contractor_title;
                } else {
                    $contractor_title = '';
                }
                $milstone_data[$task->milestone_id]['contractor'] .= $this->updateContractorTitle($milstone_data[$task->milestone_id]['contractor'], $contractor_title);
                if (isset($milstone_data[$task->milestone_id]['work_type'])) {
                    $milstone_data[$task->milestone_id]['work_type'] = $this->setProjectWorkTypes($milstone_data[$task->milestone_id]['work_type'], $project_id, $task->work_type_id, $task->milestone_id);
                }
                if (isset($milstone_data[$task->milestone_id]['progress_data'])) {
                    $milstone_data[$task->milestone_id]['progress_data'] = $this->getProgressPercentage($milstone_data[$task->milestone_id]['progress_data'], $task);

                    array_push($milstone_data[$task->milestone_id]['task_datas'], $task->attributes);
                } else {
                    $milstone_data[$task->milestone_id]['work_type'] = $this->setProjectWorkTypes($milstone_data[$task->milestone_id]['work_type'], $project_id, $task->work_type_id, $task->milestone_id);
                }
                if (!empty($task->area)) {
                    $area_key = array_search($task->area, array_column($milstone_data[$task->milestone_id]['areas'], 'id'));
                    if ($area_key === false) {
                        $area['title'] = $task->area0->area_title;
                        $area['id'] = $task->area;
                        array_push($milstone_data[$task->milestone_id]['areas'], $area);
                    }
                }
            }
        }
        $ranking = array_column($milstone_data, 'ranking');
        array_multisort($ranking, SORT_ASC, $milstone_data);
        return $milstone_data;
    }
    public function setProjectWorkTypes($milestone_data, $project_id, $work_type_id, $milestone_id)
    {

        if (empty($milestone_data)) {
            $milestone_data = array();
        }



        if (!isset($work_type_id)) {
            $project_work_types = ProjectWorkType::model()->findAll(array('condition' => 'project_id = ' . $project_id . ' AND work_type_id IS NULL'));
        } else {
            $project_work_types = ProjectWorkType::model()->findAll(array('condition' => 'project_id = ' . $project_id . ' AND work_type_id = ' . $work_type_id));
        }

        foreach ($project_work_types as $project_work_type) {


            $key = array_search($project_work_type->ranking, array_column($milestone_data, 'ranking'));
            $id_check = array_search($work_type_id, array_column($milestone_data, 'id'));
            if ($key === false) {
                $data = array('ranking' => $project_work_type->ranking, 'title' => $project_work_type->workType->work_type, 'id' => $project_work_type->work_type_id, 'status' => '1');
                array_push($milestone_data, $data);
            } else {
                foreach ($milestone_data as $key_val => $dat) {
                    if ($id_check === false) {
                        if ($dat['ranking'] == $project_work_type->ranking) {
                            $milestone_data[$key_val]['status'] = '0';
                        }
                        $others_key = array_search('0', array_column($milestone_data, 'ranking'));
                        if ($others_key === false && count($milestone_data) != 1) {
                            $data = array('ranking' => '0', 'title' => 'others', 'id' => $work_type_id, 'status' => 1);
                            array_push($milestone_data, $data);
                        }
                    }
                }
            }
        }

        return $milestone_data;
    }
    public function updateContractorTitle($contractor_list, $contractor_title)
    {
        $contractor_list = explode(',', $contractor_list);
        if (!in_array($contractor_title, $contractor_list)) {
            return ',' . $contractor_title;
        } else {
            return '';
        }
    }
    protected function milestonedatechange($task_model)
    {
        Yii::app()->getModule('masters');
        if (!empty($task_model->milestone_id) || $task_model->milestone_id != 0) {
            $result = Yii::app()->db->createCommand()
                ->select('MIN(start_date) as start_date,MAX(due_date) as end_date')
                ->from('pms_tasks')
                ->where('milestone_id =' . $task_model->milestone_id . ' AND project_id=' . $task_model->project_id)
                ->queryRow();
            $milestone_model = Milestone::model()->find(array('condition' => 'id = ' . $task_model->milestone_id . ' AND project_id = ' . $task_model->project_id));
            if (!empty($milestone_model)) {
                if (!empty($result['start_date'])) {
                    $milestone_model->start_date = $result['start_date'];
                }
                if (!empty($result['end_date'])) {
                    $milestone_model->end_date = $result['end_date'];
                }
                $milestone_model->save();
            }
        }
    }

    protected function meetingdependancyBasedDate($task_ids, $percentage_values, $depndant_on_types, $task_model)
    {
        if (!empty($task_ids) && !empty($percentage_values)) {
            foreach ($task_ids as $key => $value) {
                if (!empty($value)) {
                    $model = new TaskDependancy;
                    $model->task_id = $task_model->tskid;
                    $model->dependant_task_id = $value;
                    $model->dependency_percentage = $percentage_values[$key];
                    $model->dependant_on = $depndant_on_types[$key];
                    $model->save();
                }
            }
        }
    }
    public function getMaterialNames($materialids)
    {

        if ($materialids) {

            $materialids = explode(',', $materialids);
            foreach ($materialids as $item_id) {
                $specsql = "SELECT id, cat_id,brand_id, specification, unit "
                    . " FROM jp_specification "
                    . " WHERE id=" . $item_id . "";
                $specification = Yii::app()->db->createCommand($specsql)->queryRow();
                $cat_sql = "SELECT * FROM jp_purchase_category "
                    . " WHERE id='" . $specification['cat_id'] . "'";
                $parent_category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                if ($specification['brand_id'] != NULL) {
                    $brand_sql = "SELECT brand_name "
                        . " FROM jp_brand "
                        . " WHERE id=" . $specification['brand_id'] . "";
                    $brand_details = Yii::app()->db->createCommand($brand_sql)->queryRow();
                    $brand = '-' . $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $item_name[] = $parent_category['category_name'] . $brand . '-' . $specification['specification'];
            }
        }

        return implode(",<br>", $item_name);
    }


    public static function meetingStatus($key = false)
    {
        $items = array("1" => "Info", "2" => "Action", "3" => "Decision");

        if ($key === false) {
            return $items;
        } else {
            return '<small style="color:red;">' . $items[$key] . '</small>';
        }
    }

    public function getParentTask($milestone_id)
    {


        $Criteria = new CDbCriteria();
        $Criteria->condition = "milestone_id = $milestone_id and parent_tskid IS NULL";
        if (isset($_GET['Tasks']['report_to'])) {
            $Criteria->compare('t.report_to', $_GET['Tasks']['report_to']);
        }
        if (isset($_GET['Tasks']['coordinator'])) {
            $Criteria->compare('t.coordinator', $_GET['Tasks']['coordinator']);
        }
        if (isset($_GET['Tasks']['priority'])) {
            $Criteria->compare('t.priority', $_GET['Tasks']['priority']);
        }



        if (isset(Yii::app()->session['monthly_task_owner'])) {

            $Criteria->compare('t.report_to', Yii::app()->session['monthly_task_owner']);
        }

        if (isset(Yii::app()->session['monthly_task_coordinator'])) {

            $Criteria->compare('t.coordinator', Yii::app()->session['monthly_task_coordinator']);
        }

        if (isset(Yii::app()->session['monthly_task_priority'])) {

            $Criteria->compare('t.priority', Yii::app()->session['monthly_task_priority']);
        }

        $parent_task = Tasks::model()->findAll($Criteria);

        return $parent_task;
    }

    public function getParentTaskDet($milestone_id, $task_id_array)
    {


        $Criteria = new CDbCriteria();
        $Criteria->condition = "milestone_id = $milestone_id and parent_tskid IS NULL";
        $Criteria->addInCondition('tskid', $task_id_array);
        if (isset($_GET['Tasks']['report_to'])) {
            $Criteria->compare('t.report_to', $_GET['Tasks']['report_to']);
        }
        if (isset($_GET['Tasks']['coordinator'])) {
            $Criteria->compare('t.coordinator', $_GET['Tasks']['coordinator']);
        }
        if (isset($_GET['Tasks']['priority'])) {
            $Criteria->compare('t.priority', $_GET['Tasks']['priority']);
        }


        if (isset(Yii::app()->session['monthly_task_owner'])) {

            $Criteria->compare('t.report_to', Yii::app()->session['monthly_task_owner']);
        }

        if (isset(Yii::app()->session['monthly_task_coordinator'])) {

            $Criteria->compare('t.coordinator', Yii::app()->session['monthly_task_coordinator']);
        }

        if (isset(Yii::app()->session['monthly_task_priority'])) {

            $Criteria->compare('t.priority', Yii::app()->session['monthly_task_priority']);
        }

        $parent_task = Tasks::model()->findAll($Criteria);

        return $parent_task;
    }
    public function getSubTask($parent_tskid)
    {


        $Criteria = new CDbCriteria();
        $Criteria->condition = "parent_tskid = $parent_tskid";
        if (isset($_GET['Tasks']['report_to'])) {
            $Criteria->compare('t.report_to', $_GET['Tasks']['report_to']);
        }
        if (isset($_GET['Tasks']['coordinator'])) {
            $Criteria->compare('t.coordinator', $_GET['Tasks']['coordinator']);
        }
        if (isset($_GET['Tasks']['priority'])) {
            $Criteria->compare('t.priority', $_GET['Tasks']['priority']);
        }

        if (isset(Yii::app()->session['monthly_task_owner'])) {

            $Criteria->compare('t.report_to', Yii::app()->session['monthly_task_owner']);
        }

        if (isset(Yii::app()->session['monthly_task_coordinator'])) {

            $Criteria->compare('t.coordinator', Yii::app()->session['monthly_task_coordinator']);
        }

        if (isset(Yii::app()->session['monthly_task_priority'])) {

            $Criteria->compare('t.priority', Yii::app()->session['monthly_task_priority']);
        }
        $parent_task = Tasks::model()->findAll($Criteria);

        return $parent_task;
    }


    public function getSubTaskRecursive($parent_tskid, $spacing = '')
    {
        $subtasks = array();

        $Criteria = new CDbCriteria();
        $Criteria->condition = "parent_tskid = $parent_tskid";
        if (isset($_GET['Tasks']['report_to'])) {
            $Criteria->compare('t.report_to', $_GET['Tasks']['report_to']);
        }
        if (isset($_GET['Tasks']['coordinator'])) {
            $Criteria->compare('t.coordinator', $_GET['Tasks']['coordinator']);
        }
        if (isset($_GET['Tasks']['priority'])) {
            $Criteria->compare('t.priority', $_GET['Tasks']['priority']);
        }

        if (isset(Yii::app()->session['monthly_task_owner'])) {

            $Criteria->compare('t.report_to', Yii::app()->session['monthly_task_owner']);
        }

        if (isset(Yii::app()->session['monthly_task_coordinator'])) {

            $Criteria->compare('t.coordinator', Yii::app()->session['monthly_task_coordinator']);
        }

        if (isset(Yii::app()->session['monthly_task_priority'])) {

            $Criteria->compare('t.priority', Yii::app()->session['monthly_task_priority']);
        }


        $parent_tasks = Tasks::model()->findAll($Criteria);



        if (!empty($parent_tasks)) {
            foreach ($parent_tasks as $parent_task) {
                // Add the current parent task to the list of subtasks
                $parent_task->title = $spacing . " " . $parent_task->title;
                $subtasks[] = $parent_task;

                // Recursively get subtasks for the current parent task
                $subtasks = array_merge($subtasks, $this->getSubTaskRecursive($parent_task->tskid, $spacing . '&nbsp;&nbsp; -'));
            }
        }



        return $subtasks;
    }


    public function getTimeDuration($start_date, $end_date)
    {
        $date1 = new DateTime($start_date);
        $date2 = new DateTime($end_date);
        $interval = $date1->diff($date2);

        return $interval->days;
    }
    public function getName($id)
    {
        $name = "";
        if ($id != "") {
            $user = Users::model()->findbyPk($id);
            $name = $user->first_name . " " . $user->last_name;
        }
        return $name;
    }
    public function getStatus($id)
    {
        $name = "";
        if ($id != "") {
            $status = Status::model()->findbyPk($id);
            $name = $status->caption;
        }
        return $name;
    }
    public function getWrDetails($task_id, $quantity, $type)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $wpr = Yii::app()->db->createCommand("select  SUM(qty) as qty,MIN(date) as min_date,MAX(date) as max_date, max(description) as description,max(current_status) as status from {$tblpx}daily_work_progress where taskid=" . $task_id . " and approve_status=1")->queryRow();

        $actual_sart_date = "";
        $actual_end_date = "";
        $progress_percentage = "";
        $status_name = "";
        $wpr_qty = "";
        $progress_upto_date = "";
        $status_class = "";

        if ($type == 1) {

            if ($quantity != "") {
                $wpr_qty = $wpr['qty'];
                if ($wpr['min_date'] != "") {
                    $actual_sart_date = date("d-M-y", strtotime($wpr['min_date']));
                }


                if ($wpr['qty'] != "") {
                    $progress_percentage = ($wpr['qty'] / $quantity) * 100;
                }
                if ($quantity == $wpr_qty) {

                    $actual_end_date = date("d-M-y", strtotime($wpr['max_date']));
                }
            }


            $task = Tasks::model()->findbyPk($task_id);


            $status = Status::model()->findbyPk($task->status);

            $status_class = $this->getStatusClass($task->status);
            $status_name = $status->caption;

            if ($wpr['status'] != "") {
                // $status = Status::model()->findbyPk($wpr['status']);
                // $status_name = $status->caption;
            }
            if ($wpr['max_date'] != "") {
                $progress_upto_date = date("d-M-y", strtotime($wpr['max_date']));
            }


            $result_array = array($actual_sart_date, $actual_end_date, $progress_percentage, $status_name, $wpr['description'], $wpr_qty, $progress_upto_date, $status_class);
        } else {

            $time_entry = Yii::app()->db->createCommand("select MIN(entry_date) as min_date,MAX(entry_date) as max_date, max(description) as description,max(current_status) as status from {$tblpx}time_entry where tskid=" . $task_id . " and approve_status=1")->queryRow();

            $time_entry_progress = Yii::app()->db->createCommand("select completed_percent from {$tblpx}time_entry where tskid=" . $task_id . " and approve_status=1 order by teid desc limit 1")->queryRow();

            if ($time_entry['min_date'] != "") {
                $actual_sart_date = date("d-M-y", strtotime($time_entry['min_date']));
            }

            $progress_percentage = $time_entry_progress['completed_percent'];

            if ($progress_percentage == 100) {

                $actual_end_date = date("d-M-y", strtotime($time_entry['max_date']));
            }

            $task = Tasks::model()->findbyPk($task_id);


            $status = Status::model()->findbyPk($task->status);
            $status_class = $this->getStatusClass($task->status);


            $status_name = $status->caption;

            // if ($time_entry['status'] != "") {
            // 	$status = Status::model()->findbyPk($time_entry['status']);
            //     $status_name = $status->caption;
            // }
            if ($time_entry['max_date'] != "") {
                $progress_upto_date = date("d-M-y", strtotime($time_entry['max_date']));
            }


            $result_array = array($actual_sart_date, $actual_end_date, $progress_percentage, $status_name, $time_entry['description'], $progress_percentage, $progress_upto_date, $status_class);
        }


        return $result_array;
    }
    public function checkTaskAssigned($task_id)
    {

        $assigned = Tasks::model()->findByAttributes(array('tskid' => $task_id, 'assigned_to' => Yii::app()->user->id));

        if ($assigned) {
            $status = 1;
        } else {
            $status = 0;
        }

        return $status;
    }
    public function checkTimeEntryAssigned($task_id)
    {
        $userid = Yii::app()->user->id;
        $tasks = Tasks::model()->findAll(array("condition" => " CURDATE() between start_date and due_date And status IN (6,9) And assigned_to=" . $userid . " And tskid=" . $task_id, 'order' => 'title ASC'));

        if (count($tasks) > 0) {
            $status = 1;
        } else {
            $status = 0;
        }
        return $status;
    }
    public function getWorkProgressDetails($task_id, $task_type)
    {
        $result_array = [];
        if ($task_type == 1) {
            $data = DailyWorkProgress::model()->findAllByAttributes([
                'taskid' => $task_id,
                'approve_status' => 1,
            ]);
            if (count($data) > 0) {
                foreach ($data as $datas) {
                    $result_array[] = array(
                        'progress_date' => $datas->date,
                        'progress_percentage' => $datas->daily_work_progress,
                        'progress_comments' => $datas->description,
                        'type' => 'DW',
                        'task_id' => $task_id
                    );
                }
            }
        } else {

            $data = TimeEntry::model()->findAllByAttributes([
                'tskid' => $task_id,
                'approve_status' => 1,
            ]);

            if (count($data) > 0) {
                foreach ($data as $datas) {
                    $result_array[] = array(
                        'progress_date' => $datas->entry_date,
                        'progress_percentage' => $datas->completed_percent,
                        'progress_comments' => $datas->description,
                        'type' => 'TE',
                        'task_id' => $task_id
                    );
                }
            }
        }
        return $result_array;
    }

    public function sendEmailToRecipients($toemail, $subject, $mail_body)
    {

        if ($toemail != "") {

            try {
                $model2 = new MailLog;
                $mail_data = $mail_body;
                $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
                $mail = new JPhpMailer(true);
                $today = date('Y-m-d H:i:s');
                $bodyContent = $mail_data;
                $mail->IsSMTP();
                $mail->Port = $mail_model['smtp_port'];
                $mail->Host = $mail_model['smtp_host'];
                $mail->SMTPSecure = $mail_model['smtp_secure'];
                $mail->SMTPAuth = $mail_model['smtp_auth'];
                $mail->Username = $mail_model['smtp_username'];
                $mail->Password = $mail_model['smtp_password'];
                $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
                $mail->Subject = $subject;


                $recepients_mailids = explode(',', $toemail);

                if (!empty($recepients_mailids)) {
                    $to_mailids = implode(',', $recepients_mailids);

                    foreach ($recepients_mailids as $mailid) {
                        if (!empty($mailid)) {
                            $mail->addAddress($mailid); // Add a recipient
                        }
                    }
                }



                $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
                $mail_send_byJSON = json_encode($mail_send_by);
                $subject = $subject;
                $model2->send_to = $toemail;
                $model2->send_by = $mail_send_byJSON;
                $model2->send_date = $today;
                $model2->message = htmlentities($bodyContent);
                $model2->description = $mail->Subject;
                $model2->created_date = $today;
                $model2->created_by = Yii::app()->user->getId();
                $model2->mail_type = $subject;
                $mail->isHTML(true);
                $mail->MsgHTML($bodyContent);
                $mail->Body = $bodyContent;
                $model2->sendemail_status = 0;
                if ($mail->send()) {
                    $model2->sendemail_status = 1;
                    $response_msg = "Mail sent Successfully";
                    $msg = 'success';
                }

                if (!$model2->save()) {
                    $response_msg = $model2->getErrors();
                    $msg = 'error';
                    $model2->notsent_reason = 'Technical Error';
                }
            } catch (phpmailerException $e) {
                $response_msg = $e->getMessage();
                $msg = 'error';
                if (!$model2->save()) {
                    $response_msg = $model2->getErrors();
                    $msg = 'error';
                }
            }
        }
    }
    public static function getAllowedApps($id)
    {
        if (!Yii::app()->user->isGuest && (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '')) {
            $query = "SELECT system_access_types FROM "
                . "`global_users` WHERE  global_user_id = '" . $id . "'";
            $command = Yii::app()->db->createCommand($query);
            $result = $command->queryRow();
            $apps = strtolower($result['system_access_types']);
            if ($apps != '' and $apps !== null) {
                $allowed_apps = explode(',', $apps);
            } else {
                return false;
            }
            return $allowed_apps;
        }
        return false;
    }
    public function getTaskDuration($start_date, $due_date, $project_id, $site_id)
    {



        $project = Projects::model()->findByPK($project_id);
        $site_calender = $project->site_calendar_id;

        if ($site_calender != "") {

            $get_wworking_days = $this->getWorkingDays($start_date, $due_date, $site_calender);

            return $get_wworking_days;
        } else {


            $duration = 0;
            $event_array = array();
            //if ($site_id != "") {
            if (empty($site_id) || $site_id == 1) {
                $Criteria = new CDbCriteria();
                $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                $Criteria->addCondition('site_id = 1');
                $Criteria->addCondition('status = 0');
                $events = CalenderEvents::model()->findAll($Criteria);

                foreach ($events as $event) {
                    array_push($event_array, $event->event_date);
                }
            } else {
                $Criteria = new CDbCriteria();
                $Criteria->addBetweenCondition("event_date", $start_date, $due_date, 'AND');
                $Criteria->addCondition('site_id = ' . $site_id);
                $Criteria->addCondition('status = 1');
                $events = CalenderEvents::model()->findAll($Criteria);

                foreach ($events as $event) {
                    array_push($event_array, $event->event_date);
                }
            }
            //}
            $start_date_data = new DateTime($start_date);
            $due_date_data = new DateTime($due_date);
            $due_date_data->modify('+1 day');
            $period = new DatePeriod(
                $start_date_data,
                new DateInterval('P1D'),
                $due_date_data
            );

            foreach ($period as $key => $value) {
                if (!in_array($value->format('Y-m-d'), $event_array)) {
                    $duration++;
                }
            }
            return $duration;
        } //site calendar not assigned to project

    }

    public function getWorkingDays($start_date, $due_date, $site_calender)
    {



        $begin = new DateTime($start_date);
        $end = new DateTime($due_date);
        $start_date = $start_date;
        $end_date = $due_date;

        $calender = ProjectCalender::model()->findByPK($site_calender);
        $duration = 0;
        $sat_count = 0;
        $date_array = [];
        $saturday_array = [];

        for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
            $day = $i->format("l");
            $date = $i->format("Y-m-d");

            if ($day == "Sunday" && $calender->sunday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Monday" && $calender->monday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Tuesday" && $calender->tuesday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Wednesday" && $calender->wednesday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Thursday" && $calender->thursday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Friday" && $calender->friday == 1) {
                $duration++;
                array_push($date_array, $date);
            } else if ($day == "Saturday" && $calender->saturday == 1) {



                $duration++;

                array_push($date_array, $date);
            }

            $first_sat_s_date = "01";
            $first_sat_e_date = "7";
            $second_sat_s_date = "08";
            $second_sat_e_date = "14";
            $third_sat_s_date = "15";
            $third_sat_e_date = "21";
            $fourth_sat_s_date = "22";
            $fourth_sat_e_date = "28";

            if ($day == "Saturday" && $calender->second_saturday == 0 && strtotime($date) >= strtotime($i->format("Y-m-" . $second_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $second_sat_e_date))) {
                $sat_count++;

                array_push($saturday_array, $date);
            }
            if ($day == "Saturday" && $calender->fourth_saturday == 0 && strtotime($date) >= strtotime($i->format("Y-m-" . $fourth_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $fourth_sat_e_date))) {
                $sat_count++;
                array_push($saturday_array, $date);
            }


            /////////////////////////////////////////////////

            if ($day == "Saturday" && $calender->second_saturday == 1 && strtotime($date) >= strtotime($i->format("Y-m-" . $second_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $second_sat_e_date))) {
                array_push($date_array, $date);
            }

            if ($day == "Saturday" && $calender->fourth_saturday == 1 && strtotime($date) >= strtotime($i->format("Y-m-" . $fourth_sat_s_date)) && strtotime($date) <= strtotime($i->format("Y-m-" . $fourth_sat_e_date))) {
                array_push($date_array, $date);
            }



            ///////////////////////////////////////////////////





        }
        //echo $duration-$sat_count;



        $available_date_arr = array_diff($date_array, $saturday_array);
        $criteria = new CDbCriteria;
        $criteria->condition = "date >= '$start_date' AND date <= '$end_date' AND project_calendar_id=" . $site_calender;
        $holidays = CalendarHolidays::model()->findAll($criteria);
        $holiday_array = [];
        foreach ($holidays as $holiday) {
            $holiday_array[] = $holiday->date;
        }



        $total_working_days = array_diff($available_date_arr, $holiday_array);


        return count($total_working_days);
    }
    public function getPriorityColor($id)
    {
        return ($id == 10 ? 'mid' : ($id == '11' ? 'high' : 'low'));
    }
    public function getStatusClass($id)
    {
        if ($id == 5) {
            $class = 'on-hold-contractor';
        } else if ($id == 6) {
            $class = 'open-task';
        } else if ($id == 7) {
            $class = 'completed';
        } else if ($id == 8) {
            $class = 'review';
        } else if ($id == 9) {
            $class = 'in-progress';
        } else if ($id == 72) {
            $class = 'escalated';
        } else if ($id == 73) {
            $class = 'on-hold-client';
        } else if ($id == 74) {
            $class = 'on-hold';
        } else {
            $class = 'unassigned';
        }
        return $class;
    }
    function cleanHtmlContent($html)
    {

        $html = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', '', $html);
        $html = preg_replace('/\bon[a-z]+\s*=\s*"[^"]*"/i', '', $html);
        $html = preg_replace('/<a\b[^>]*\bhref=["\']javascript:(.*?)["\'][^>]*>(.*?)<\/a>/is', '', $html);

        return $html;
    }
    public static function money_format_inr($input = 0, $decimals = 0, $opt1 = "", $opt2 = "")
    {
        //$input = round($input);

        if ($input == '' || $input == NULL) {
            $input = 0;
        }

        $input = $input;
        $decimals = intval($decimals);

        $dec = "";
        $negative_sign = "";

        $negative_pos = strpos($input, "-");

        if ($negative_pos === FALSE) {
            $negative_sign = "";
        } else {
            $negative_sign = "-";
            $input = str_replace("-", "", $input);
        }

        $pos = strpos($input, ".");
        if ($pos === FALSE) {
            //no decimals
            $dec = "00";
        } else {
            //decimals
            $dec = substr(round(substr($input, $pos), 2), 1);
            //$dec   = substr(substr($input, $pos), 2, 1);
            $input = substr($input, 0, $pos);
        }
        $num = substr($input, -3);    // get the last 3 digits
        $input = substr($input, 0, -3); // omit the last 3 digits already stored in $num
        // loop the process - further get digits 2 by 2
        while (strlen($input) > 0) {
            $num = substr($input, -2) . "," . $num;
            $input = substr($input, 0, -2);
        }
        // return $num.str_replace("0.",".",number_format($dec,2));

        $dec = floatval($dec);

        if ($opt1 == 1) {
            return $num . ltrim(number_format($dec, $decimals, ".", ""), "0");
        } else {
            return $negative_sign . $num . ltrim(number_format($dec, $decimals, ".", ""), "0");
        }
    }
    public function setHtmlContent($template_data, $sample_template_data)
    {
        $sample_data = json_decode($sample_template_data, true);
        $tables = [
            'participant_data' => 'PARTICIPANTTEMPLATE',
            'meeting_minutes_data' => 'MEETINGMINUTESTEMPLATE',
        ];
        $sample_data['address'] = str_replace("\n", "<br><br>", $sample_data['address']);
        return $this->setLayoutContent($sample_data, $template_data, $tables);
    }
    public function setWeeklyReportContent($template_data, $sample_template_data)
    {
        $sample_data = json_decode($sample_template_data, true);

        $list_data = ['present_status', 'key_achievements', 'key_actions', 'key_risks'];
        $tables = [
            'drawing_delivered' => 'DRAWINGDELIVEREDTEMPLATE',
            'drawing_pending' => 'DRAWINGPENDINGTEMPLATE',
            'payment_advice' => 'PAYMENTADVICETEMPLATE',
            'payment_pending' => 'PAYMENTPENDINGTEMPLATE',
            'site_visit' => 'SITEVISITTEMPLATE',
            'weekly_images' => 'WEEKLYIMAGETEMPLATE',
            'wpr_images' => 'WPRIMAGETEMPLATE',
        ];
        $nested_tables = [
            'target' => ['TARGETFIRSTTEMPLATE', 'TARGETSECONDTEMPLATE'],
        ];
        return $this->setLayoutContent($sample_data, $template_data, $tables, $nested_tables, $list_data);
    }
    public function setLayoutContent($sample_data, $template_data, $tables = array(), $nested_tables = array(), $list_data = array())
    {
        $template_variables = '';
        if (!empty($tables)) {
            $template_variables = implode('|', $tables);
            foreach ($tables as $key => $template_variable) {
                $sample_data[$key] = isset($sample_data[$key]) ? $this->replaceTableData($template_data, $sample_data[$key], $template_variable) : '';
            }
        }
        if (!empty($nested_tables)) {
            foreach ($nested_tables as $key => $template_variable) {
                $sample_data[$key] = isset($sample_data[$key]) ? $this->replaceNestedTableData($template_data, $sample_data[$key], $template_variable) : '';
                $template_variables .= $template_variables ? '|' . implode('|', $template_variable) : implode('|', $template_variable);
            }
        }
        if (!empty($list_data)) {
            foreach ($list_data as $key) {
                $list ='';
                foreach($sample_data[$key] as $item){
                    if($item !=null){
                        $list .="<li>{$item}</li>";
                    }
                }
                $sample_data[$key] = $list ? $list : 'No Items Found';
            }
        }
        $modified_template = preg_replace_callback(
            '/\[\[(' . $template_variables . ')(.*?)END\1\]\]/s',
            function ($matches) use ($template_data) {
                return '';
            },
            $template_data
        );
        return $this->replacePlaceholders($modified_template, $sample_data);
    }
    public function replaceNestedTableData($template_data, $table_data, $template_variables)
    {
        $extractedContent = array();
        foreach ($template_variables as $template_variable) {
            $pattern = '/\[\[' . $template_variable . '(.*?)END' . $template_variable . '\]\]/s';
            preg_match($pattern, $template_data, $matches);
            $extractedContent[] = isset($matches[1]) ? $matches[1] : '';
        }

        $table_rows = "";
        foreach ($table_data as $data) {
            foreach ($extractedContent as $index => $content) {
                foreach ($data[$index] as $element) {
                    if (is_array($element)) {
                        $replacements = $element;
                    } else {
                        $replacements = $data[$index];
                    }
                    $table_rows .= $this->replacePlaceholders($content, $replacements);
                }
            }
        }
        return $table_rows;
    }
    public function replaceTableData($template_data, $table_data, $template_variable)
    {
        $pattern = '/\[\[' . $template_variable . '(.*?)END' . $template_variable . '\]\]/s';
        preg_match($pattern, $template_data, $matches);
        $extractedContent = isset($matches[1]) ? $matches[1] : '';
        $table_rows = "";
        if (!empty($table_data)) {
            foreach ($table_data as $value) {
                $table_rows .= $this->replacePlaceholders($extractedContent, $value);
            }
        } else {
            $td_count = (substr_count($extractedContent, 'td'))/2;
            $table_rows = "<tr><td class='text-cenetr empty' colspan={$td_count}>N/A</td>";
        }
        return $table_rows;
    }
    public function replacePlaceholders($template_data, $sample_data)
    {
        return str_replace(array_map(function ($key) {
            return '{' . $key . '}';
        }, array_keys($sample_data)), array_values($sample_data), $template_data);
    }
    public function replaceItemlistWithCondition($extractedContent, $replacements)
    {
        $grouppattern = '/\[\((.*?)\)\]/';
        preg_match_all($grouppattern, $extractedContent, $groupmatches);
        foreach ($groupmatches[0] as $match) {
            //  $replaceString = $match;
            $input = str_replace(array("[(", ")]"), '', $match);
            // Check if "length," "width," and "size" are all present
            $variables = preg_match_all('/\{([^}]+)\}/', $match, $matches);
            // if (count(array_intersect($requiredVariables, $matches[0])) == count($requiredVariables)) {
            $i = 0;
            foreach ($matches[0] as $variable) {
                if (!empty($replacements[$variable])) {
                    $i++;
                    $replaceString = str_replace($variable, $replacements[$variable], $input);

                } else {
                    // If any variable is missing a value, replace it with an empty string
                    $replaceString = str_replace($variable, '', $match);
                }
            }
            if (count($matches[0]) != $i) {
                // Any required variable is missing, replace with an empty string
                $replaceString = '';
            }
            $extractedContent = str_replace($match, $replaceString, $extractedContent);
            //$extractedContent = preg_replace('/\[|\]|\(|\)|\s+/', '', $extractedContent);
        }//echo $extractedContent;die;

        return $extractedContent;
    }
}
