<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class AutologinUser extends CBaseUserIdentity
{
	private $_id;
/**
	 * @var string username
	 */
	public $username;
        public $mainuser;
	
	/**
	 * Constructor.
	 * @param string $username username
	 * @param string $password password
	 */
	public function __construct($username,$mainuser)
	{
		$this->username=$username;
                $this->mainuser=$mainuser;
	}

	
	public function getName()
	{
		return $this->username;
	}
        public function authenticate() {
        $record = Users::model()->findByAttributes(array('username' => $this->username, 'status' => 0));   
        $recordmain = Users::model()->findByAttributes(array('username' => $this->mainuser, 'status' => 0));   
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else {
            $this->_id = $record->userid;
            Yii::app()->user->setState('role',$record->user_type);
            //echo Yii::app()->user->getState('role').Yii::app()->user->role;exit;
            //echo $this->setState('role', $record->user_type);exit;
            Yii::app()->user->setState('firstname', $record->first_name);
            Yii::app()->user->setState('logged_time', date("Y-m-d H:i:s"));
			
			$usertype = UserRoles::model()->findByPk($record->user_type);
			Yii::app()->user->setState('rolename', $usertype->role);
            Yii::app()->user->setState('mainuser_id', $recordmain->userid);
            Yii::app()->user->setState('mainuser_role', $recordmain->user_type);
            Yii::app()->user->setState('mainuser_username', $recordmain->first_name); 
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    
   

    public function getId() {
        return $this->_id;
    }
}