<?php
/*
 * For top menu module 
 *  
 */
Yii::import('zii.widgets.CPortlet');

class EmailNotification extends CPortlet {
   
     protected function renderContent() {
       // $this->render("DisplayEscalated");
       $model = new MailLog;
        $this->render('EmailNotification',array(
			'model'=>$model,
			
		));
    }
}

?>