<?php

class GlobalApiCall
{
    public static function callApi($slug, $method, $request = array())
    {
        $ch = curl_init();
        $headers = array();
        $result = array();
        $data = $request;
        $token = APP_AUTH_KEY;
        $headers[] = "accept: application/json";
        $headers[] = 'APP_KEY:' . $token;
        curl_setopt($ch, CURLOPT_URL, GLOBAL_API_DOMAIN.$slug);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);
        if (!isset($response['success'])) {
            $response['success'] = false;
        }
        return $response;
        exit;
    }
}