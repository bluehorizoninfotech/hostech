
<?php
class MyClass extends CApplicationComponent {
 
 public function project_ids() { 
     $user_id = Yii::app()->user->id;
     $user_role = Yii::app()->user->role;
     $project_id_array= array();
     if($user_role != 1){
        $criteria_task = new CDbCriteria;
        $criteria_task->select = 'project_id'; 
        $criteria_task->condition = 'assigned_to = '.Yii::app()->user->id.' OR report_to = '.Yii::app()->user->id.' OR coordinator = '.Yii::app()->user->id;
        $criteria_task->group = 'project_id';                
        $tasks = Tasks::model()->findAll($criteria_task);       
        foreach($tasks as $task){
            array_push($project_id_array,$task['project_id']);
        }
        $criteria_proj = new CDbCriteria();        
        $criteria_proj->select = 'pid,name'; 
        $criteria_proj->condition = 'find_in_set('.$user_id.',assigned_to) OR created_by ='.$user_id;  
              
        $projects = Projects::model()->findAll($criteria_proj);      
        foreach($projects as $project){
            if(!in_array($project['pid'],$project_id_array)){
                array_push($project_id_array,$project['pid']);
            }            
        }
        if(!empty($project_id_array))       {
            $project_id_array = implode(',',$project_id_array);
        }
       
     }
     return $project_id_array;
 }
 
}
?>