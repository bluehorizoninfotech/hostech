<?php
/*
 * For top menu module 
 * 
 */
Yii::import('zii.widgets.CWidget');

class NotificationWidget extends CWidget
{

  public function run()
  {

    $tbl_px = Yii::app()->db->tablePrefix;
    $notifycount = $exptskcount =  $tskcount =   $userlist =  $escalted_count = $unassigned_task_count = $missed_task_count = 0;

    if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) {

      if (Yii::app()->user->project_id != "") {
        $allexpired = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7) AND due_date<:tod AND project_id=:projectid', array(':tod' => date('Y-m-d'), ':projectid' => Yii::app()->user->project_id))
          ->queryAll();
        $unassigned_tasks = Tasks::model()->findAll(['condition' => 'project_id = ' . Yii::app()->user->project_id . ' AND status = 75']);
      } else {

        $allexpired = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7) AND due_date<:tod', array(':tod' => date('Y-m-d')))
          ->queryAll();
        $unassigned_tasks = Tasks::model()->findAll(['condition' => 'status = 75']);
      }


      $exptskcount = count($allexpired);
      $unassigned_task_count = count($unassigned_tasks);
    } else {

      if (Yii::app()->user->project_id != "") {
        $allexpired = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7) AND due_date<:tod AND  (assigned_to = :userid OR report_to = :userid OR coordinator =:userid OR created_by =:userid ) AND project_id=:projectid', array(':tod' => date('Y-m-d'), ':projectid' => Yii::app()->user->project_id, ':userid' => Yii::app()->user->id,))
          ->queryAll();
        $unassigned_tasks = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7) AND status IN (75) AND due_date<:tod AND  (assigned_to = :userid OR report_to = :userid OR coordinator =:userid OR created_by =:userid ) AND project_id=:projectid', array(':tod' => date('Y-m-d'), ':projectid' => Yii::app()->user->project_id, ':userid' => Yii::app()->user->id,))
          ->queryAll();
      } else {
        $allexpired = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7)  AND (assigned_to = :userid OR report_to = :userid OR coordinator =:userid OR created_by =:userid ) AND due_date<:tod', array(':tod' => date('Y-m-d'), ':userid' => Yii::app()->user->id,))
          ->queryAll();
        $unassigned_tasks = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7) AND status IN (75) AND (assigned_to = :userid OR report_to = :userid OR coordinator =:userid OR created_by =:userid ) AND due_date<:tod', array(':tod' => date('Y-m-d'), ':userid' => Yii::app()->user->id,))
          ->queryAll();
      }
      $exptskcount = count($allexpired);
      $unassigned_task_count = count($unassigned_tasks);
    }
    if (isset(Yii::app()->user->id)) {
      if (Yii::app()->user->project_id != "") {
        $chkentry = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7,9)  AND (assigned_to = :userid OR report_to = :userid OR coordinator =:userid OR created_by =:userid )    AND due_date>:tod AND project_id=:projectid', array(':userid' => Yii::app()->user->id, ':tod' => date('Y-m-d'), ':projectid' => Yii::app()->user->project_id))
          ->queryAll();
      } else {
        $chkentry = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7,9)  AND (assigned_to = :userid OR report_to = :userid OR coordinator =:userid OR created_by =:userid )    AND due_date>:tod', array(':userid' => Yii::app()->user->id, ':tod' => date('Y-m-d')))
          ->queryAll();
      }


      $tskcount = count($chkentry);
    }
    if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) {

      $query = "SELECT created_by FROM {$tbl_px}time_entry WHERE date(`created_date`) = '" . date('Y-m-d') . "' ";
      $sql = Yii::app()->db->createCommand($query)->queryALl();

      $query1 = "SELECT created_by FROM {$tbl_px}tasks WHERE date(`created_date`) = '" . date('Y-m-d') . "' ";
      $sql1 = Yii::app()->db->createCommand($query1)->queryAll();
      $merges = array_merge($sql, $sql1);
      $d = '';
      foreach ($merges as $merge) {
        $d .= '"' . $merge['created_by'] . '",';
      }
      $where = '';
      if ($d) {
        $re = rtrim($d, ',');
        $where = "and userid not in(" . $re . ")";
      }
      $chkuser = Yii::app()->db->createCommand()
        ->select('*')
        ->from("{$tbl_px}users")
        ->where("status = '0' and user_type not in(5)")
        ->queryAll();

      $userlist = count($chkuser);
    }
    /* Escalated Tasks */
    if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) {
      /*for admin starts */
      if (Yii::app()->user->project_id != "") {
        $escalatedtry = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status IN (72)  AND project_id=:projectid', array(':projectid' => Yii::app()->user->project_id))
          ->queryAll();
      } else {
        $escalatedtry = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status  IN (72) ', array())
          ->queryAll();
      }
      $escalted_count = count($escalatedtry);
      /*for admin ends */
    } else {
      if (Yii::app()->user->project_id != "") {
        $escalatedtry = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status IN (72)  AND (assigned_to = :userid OR report_to = :userid OR coordinator =:userid OR created_by =:userid ) AND project_id=:projectid', array(':userid' => Yii::app()->user->id, ':projectid' => Yii::app()->user->project_id))
          ->queryAll();
      } else {
        $escalatedtry = Yii::app()->db->createCommand()
          ->select('*')
          ->from("{$tbl_px}tasks")
          ->where('trash=1 AND status NOT IN (7,9)  AND assigned_to = :userid     AND due_date>:tod', array(':userid' => Yii::app()->user->id, ':tod' => date('Y-m-d')))
          ->queryAll();
      }
      $escalted_count = count($escalatedtry);
    }

    $task_model = new Tasks();
    $data =  $task_model->getMissedTasksData();
    $missed_task_count = 0;
    if (!empty($data['missed_task_count'])) {
      $missed_task_count = $data['missed_task_count'];
    }
    $notifycount = $exptskcount + $tskcount + $userlist + $escalted_count + $unassigned_task_count + $missed_task_count;

    $this->render('notification', array('exptskcount' => $exptskcount, 'tskcount' => $tskcount, 'userlist' => $userlist, 'notifycount' => $notifycount, 'escalted' => $escalted_count, 'unassigned_task_count' => $unassigned_task_count, 'missed_task_count' => $data['missed_task_count']));
  }
}
