<?php

if (isset($_GET['exportgrid'])) {

//Yii::import('zii.widgets.grid.CGridColumn');
    Yii::import('application.core.grid.CGridColumn1');

//require("../core/grid/CGridColumn1.php");

    class IndexColumn extends CGridColumn1 {

        public $sortable = false;

        public function init() {
            parent::init();
        }

        protected function renderDataCellContent($row, $data) {
            $pagination = $this->grid->dataProvider->getPagination();
            $index = $pagination->pageSize * $pagination->currentPage + $row + 1;
            echo '';
        }

    }

} else {
    Yii::import('zii.widgets.grid.CGridColumn');

//    Yii::import('application.core.grid.CGridColumn1');
//require("../core/grid/CGridColumn1.php");

    class IndexColumn extends CGridColumn {

        public $sortable = false;

        public function init() {
            parent::init();
        }

        protected function renderDataCellContent($row, $data) {
            $pagination = $this->grid->dataProvider->getPagination();
            $index = $pagination->pageSize * $pagination->currentPage + $row + 1;
            echo $index;
        }

    }

}
?>
