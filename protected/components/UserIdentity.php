<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    private $_id;
    public $global_user_id;
    public function __construct($username, $password, $global_user_id)
    {
        parent::__construct($username, $password);
        $this->global_user_id = $global_user_id;
    }
    public function authenticate()
    {        
        $error = 0;
        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
            if (isset($this->global_user_id) and $this->global_user_id != '') {
                $sql = "SELECT * FROM global_users WHERE global_user_id =:global_user_id "
                    . "AND system_access_types LIKE :systemAccessTypes";
                $command = Yii::app()->db->createCommand($sql);
                $command->bindValue(':global_user_id', $this->global_user_id);
                $command->bindValue(':systemAccessTypes', '%PMS%');
                $record = $command->queryRow();                
                if ($record === null) {
                    $this->errorCode = self::ERROR_USERNAME_INVALID;
                    $error = 1;
                }
            } else {

                $sql = "SELECT * FROM global_users WHERE username =:username "
                    . "AND passwd =:password "
                    . "AND system_access_types LIKE :systemAccessTypes";
                $command = Yii::app()->db->createCommand($sql);
                $command->bindValue(':username', $this->username);
                $command->bindValue(':password', md5(base64_encode($this->password)));
                $command->bindValue(':systemAccessTypes', '%PMS%');
                $record = $command->queryRow();
                // $record = GlobalUsers::model()->findByAttributes(array('username' => $this->username, 'passwd' => md5(base64_encode($this->password)), 'status' => 0));
                if ($record === null) {
                    $this->errorCode = self::ERROR_USERNAME_INVALID;
                    $error = 1;
                } else if ($record['passwd'] !== $this->password && $record['passwd'] !== md5(base64_encode($this->password)) and $this->password != (defined('GLOBAL_LOGIN_PASS') ? GLOBAL_LOGIN_PASS : md5(base64_encode($this->password)))) {
                    $this->errorCode = self::ERROR_PASSWORD_INVALID;
                    $error = 1;
                }
            }
        }
        
        $record = Users::model()->findByAttributes(array('username' => $this->username, 'status' => 0));
        
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $error = 1;
        } else if ((!defined('LOGIN_USER_TABLE')) && $record->password !== md5(base64_encode($this->password)) and $this->password != (defined('GLOBAL_LOGIN_PASS') ? GLOBAL_LOGIN_PASS : md5(base64_encode($this->password)))) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            $error = 1;
        }
        if ($error == 0) {
            $this->_id = $record->userid;
            $this->setState('role', $record->user_type);
            $this->setState('firstname', $record->first_name . " " . $record->last_name);
            $this->setState('logged_time', date("Y-m-d H:i:s"));

            $usertype = UserRoles::model()->findByPk($record->user_type);
            $this->setState('rolename', $usertype->role);
            $this->setState('project_id', "");

            // $assigned_tasks = Tasks::model()->find(array('condition' => 'assigned_to ='.$record->userid.' AND trash =1','order'=>'tskid DESC'));
            // if($assigned_tasks != null){
            //     $this->setState('project_id', $assigned_tasks->project_id);
            // }else{
            //     $this->setState('project_id', "");
            // }
            $this->setState('trigger_status', 1);

            if ($record->user_type == 1) {
                $this->setState('mainuser_id', $record->userid);
                $this->setState('mainuser_role', $record->user_type);
                $this->setState('mainuser_username', $record->first_name);
            }
            $this->errorCode = self::ERROR_NONE;

            /* new update login time */
            $command = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'users set last_modified = "' . date('Y-m-d H:i:s') . '"  WHERE userid = ' . $record->userid);
            $num = $command->execute();


            // 	$this->errorCode = self::ERROR_UNKNOWN_IDENTITY;	
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}
