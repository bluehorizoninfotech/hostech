<?php
/*
 * For top menu module 
 * 
 */
Yii::import('zii.widgets.CPortlet');

class TodaySummary extends CPortlet
{
    public function getSummary()
    {
        $tbl_px = Yii::app()->db->tablePrefix;

        $query = "";
        if (Yii::app()->user->role != 1) {
            if (Yii::app()->user->project_id != "") {
                // $criteria->addCondition('t.project_id = '.Yii::app()->user->project_id.'');
                $query .= " AND pro.pid = " . Yii::app()->user->project_id . "";
                $query .= " AND (tsk.assigned_to in (" . Yii::app()->user->id . ") or tsk.report_to in (" . Yii::app()->user->id . ") or tsk.coordinator in (" . Yii::app()->user->id . ") or tsk.created_by in (" . Yii::app()->user->id . ")) ";
            } else {
                $query .= " AND (tsk.assigned_to in (" . Yii::app()->user->id . ") or tsk.report_to in (" . Yii::app()->user->id . ") or tsk.coordinator in (" . Yii::app()->user->id . ") or tsk.created_by in (" . Yii::app()->user->id . ") )";
            }
        } else {
            if (Yii::app()->user->project_id != "") {
                // $criteria->addCondition('t.project_id = '.Yii::app()->user->project_id.'');
                $query .= " AND pro.pid = " . Yii::app()->user->project_id . "";
            }
        }

        // if(yii::app()->user->role !=1){
        //     $query .=" AND tsk.assigned_to in (" . Yii::app()->user->id . ") or tsk.report_to in (" . Yii::app()->user->id . ") or tsk.coordinator in (" . Yii::app()->user->id . ") or tsk.created_by in (" . Yii::app()->user->id . ") ";
        // }
        // echo $query;exit;
        $sql = "select date_format(`entry_date`, \"%d-%b-%y\") as tentry_date,teid,t.tskid,t.approve_status,  
            title as tsk_name ,hours,completed_percent, t.billable, work_type, t.description,  concat_ws(' ',`usr`.`first_name`, `usr`.`last_name`) as first_name";
        $sql .= " from " . $tbl_px . "time_entry as t 
            INNER JOIN " . $tbl_px . 'tasks tsk ON tsk.tskid = t.tskid
            LEFT JOIN ' . $tbl_px . 'projects as pro on tsk.project_id=pro.pid 
            INNER JOIN ' . $tbl_px . 'users as usr on usr.userid=tsk.assigned_to   
                WHERE (t.approve_status = 0 OR t.approve_status = 1) AND entry_date BETWEEN (CURDATE() - INTERVAL 7 DAY) AND CURDATE() and userid!=43 AND tsk.trash=1 ' . $query . '
                ORDER BY  t.entry_date DESC';
        // echo $sql;
        // exit;
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        $work_details = $command->queryAll();

        $sql_boq = "select date_format(`date`, \"%d-%b-%y\") as tentry_date,id,t.taskid,t.approve_status,t.daily_work_progress as  completed_percent,
                title as tsk_name , work_type, t.description,  concat_ws(' ',`usr`.`first_name`, `usr`.`last_name`) as first_name";
        $sql_boq .= " from " . $tbl_px . "daily_work_progress as t 
                INNER JOIN " . $tbl_px . 'tasks tsk ON tsk.tskid = t.taskid
                LEFT JOIN ' . $tbl_px . 'projects as pro on tsk.project_id=pro.pid 
                INNER JOIN ' . $tbl_px . 'users as usr on usr.userid=tsk.assigned_to   
                    WHERE  date BETWEEN (CURDATE() - INTERVAL 7 DAY) AND CURDATE() and userid!=43 AND tsk.trash=1 ' . $query . '
                    ORDER BY  t.date DESC';
        // echo $sql_boq;
        // exit;
        $command = Yii::app()->db->createCommand($sql_boq);
        $command->execute();
        $boq_details = $command->queryAll();
        $result = array_merge($boq_details, $work_details);
        return $result;
    }

    protected function renderContent()
    {
        $this->render("todaySummary");
    }
}
