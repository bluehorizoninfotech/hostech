<?php
/* @var $this ProjectCalenderController */
/* @var $model ProjectCalender */

$this->breadcrumbs=array(
	'Project Calenders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProjectCalender', 'url'=>array('index')),
	array('label'=>'Manage ProjectCalender', 'url'=>array('admin')),
);
?>

<h1>Create Site Calender</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>