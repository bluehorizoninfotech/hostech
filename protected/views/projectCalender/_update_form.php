<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this ProjectCalenderController */
/* @var $model ProjectCalender */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'project-calender-form',
        'enableAjaxValidation' => false,
    )); ?>


    <div class="site-calendar-sec">
        <div class="col-lg-12 newform">
            <div class="row">

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'calender_name'); ?>
                    <?php echo $form->textField($model, 'calender_name', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'calender_name'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'sunday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'sunday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'sunday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'monday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'monday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'monday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'tuesday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'tuesday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'tuesday'); ?>
                </div>

            </div>
            <div class="row">

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'wednesday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'wednesday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'wednesday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'thursday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'thursday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'thursday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'friday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'friday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'friday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'saturday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'saturday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'saturday'); ?>
                </div>

            </div>

            <div class="row">

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'second_saturday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'second_saturday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'second_saturday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'fourth_saturday'); ?>

                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'fourth_saturday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'fourth_saturday'); ?>
                </div>


               

                </div>

                <?php
                foreach($holiday as $holidays)
                {
                ?>

                <div class="row">
            <div class="col-md-3">
                        <label>Date</label>
                        <input type="text" id="holiday_date" class="form-control datepicker" name="holiday_date[]" value="<?php echo date("d-M-y",strtotime($holidays->date)) ?>">
                        <div class="errorMessage display-none" id="date_em_"></div>

                    </div>
                    <div class="col-md-3">
                        <label>Remarks</label>
                        <input type="text" id="holiday_description" class="form-control" name="holiday_remarks[]" value=<?php echo $holidays->remarks?>>
                        <div class="errorMessage display-none" id="description_em_"></div>

                    </div>







                    </div>

 

  <?php } ?>

  <div id="container"></div> 
            <br>
            <input type="button" id="add_new" value="Add"  class="btn btn-secondary" onClick="addInput();">
            <br>

            <div class="row buttons text-center">
                    <?php echo CHtml::Submitbutton($model->isNewRecord ? 'Create' : 'Update', array('class' => 'btn blue save_btn')); ?>


                </div>

                </div>


               

           
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->


<script>

var count = 0;
    var val = 0;
   function addInput() {
    $('#container').append(
        ` 
        <div class="row remove-div_` + count + `">
            <div class="col-md-3">
                        <label>Date</label>
                        <input type="text" id="holiday_date_`+count+`" class="form-control" name="holiday_date[]">
                        <div class="errorMessage display-none" id="date_em_` + count + `"></div>

                    </div>
                    <div class="col-md-3">
                        <label>Remarks</label>
                        <input type="text" id="holiday_remarks_`+count+`" class="form-control" name="holiday_remarks[]">
                        <div class="errorMessage display-none" id="remarks_em_` + count + `"></div>

                    </div>

                    <!-- remove button -->
                        <div class=" col-md-2  remove-btn-padding">
                       
                        <input type="button" id="` + count + `" value="Remove" class="remove-btn btn btn-secondary">
                        </div>
                        <!-- end remove button -->

                    </div>
                   
        `
    ); 
    
    

        $('#holiday_date_' + count).datepicker({
        dateFormat: 'dd-M-y',
    });
        count += 1;
   }


   $(document).on('click', '.remove-btn', function() {
            id = this.id;
            $(".remove-div_" + id).remove();
        });

$(document).ready(function(){
 $('.datepicker').datepicker({
        dateFormat: 'dd-M-y',
    });
 });


 
    
</script>    
