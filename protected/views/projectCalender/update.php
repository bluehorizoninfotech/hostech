<?php
/* @var $this ProjectCalenderController */
/* @var $model ProjectCalender */

$this->breadcrumbs=array(
	'Project Calenders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectCalender', 'url'=>array('index')),
	array('label'=>'Create ProjectCalender', 'url'=>array('create')),
	array('label'=>'View ProjectCalender', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectCalender', 'url'=>array('admin')),
);
?>

<h1>Update  <?php echo $model->calender_name; ?></h1>

<?php echo $this->renderPartial('_update_form', array('model'=>$model,'holiday'=>$holiday)); ?>