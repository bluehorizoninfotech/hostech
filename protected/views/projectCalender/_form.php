<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this ProjectCalenderController */
/* @var $model ProjectCalender */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'project-calender-form',
        'enableAjaxValidation' => false,
    )); ?>

<!-- start response message -->

<div class="alert alert-success display-none" role="alert">
</div>
<div class="alert alert-danger display-none" role="alert">
</div>
<div class="alert alert-warning display-none" role="alert">

</div>

<!-- end response message -->
    <div class="site-calendar-sec">
        <div class="col-lg-12 newform">
            <div class="row">

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'calender_name'); ?>
                    <?php echo $form->textField($model, 'calender_name', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'calender_name'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'sunday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'sunday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'sunday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'monday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'monday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'monday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'tuesday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'tuesday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'tuesday'); ?>
                </div>

            </div>
            <div class="row">

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'wednesday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'wednesday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'wednesday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'thursday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'thursday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'thursday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'friday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'friday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'friday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'saturday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'saturday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'saturday'); ?>
                </div>

            </div>

            <div class="row">

                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'second_saturday'); ?>
                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'second_saturday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'second_saturday'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'fourth_saturday'); ?>

                    <?php
                    echo CHtml::activeDropDownList(
                        $model,
                        'fourth_saturday',
                        array('0' => 'Holiday', '1' => 'Working day'),
                        array('empty' => 'Select Option', 'class' => 'form-control')
                    ); ?>
                    <?php echo $form->error($model, 'fourth_saturday'); ?>
                </div>
            </div>

            <div class="row">
            <div class="col-md-3">
                        <label>Date</label>
                        <input type="text" id="holiday_date" class="form-control" name="holiday_date[]">
                        <div class="errorMessage display-none" id="date_em_"></div>

                    </div>
                    <div class="col-md-3">
                        <label>Remarks</label>
                        <input type="text" id="holiday_description" class="form-control" name="holiday_remarks[]">
                        <div class="errorMessage display-none" id="description_em_"></div>

                    </div>
                    </div>

                    <div id="container"></div> 
            <br>
            <input type="button" id="add_new" value="Add"  class="btn btn-secondary" onClick="addInput();">
            <br>
        
                </div>

            <div class="row buttons text-center">
                    <?php echo CHtml::Submitbutton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue save_btn')); ?>


                </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->



<div class="row">
    <div class="col-md-12">
    <?php



$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'project-calender-grid',
    'dataProvider' => $model->search(),
   
    'itemsCssClass' => 'table table-bordered',
    'template' => '<div class="table-responsive">{items}</div>',
    'pager' => array(
        'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.'),

        array(
            'name' => 'calender_name',
            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
        ),
        array(
            'name' => 'sunday',
            'value' => function ($data) {
                if ($data->sunday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
            
        ),
        array(
            'name' => 'monday',
            'value' => function ($data) {
                if ($data->monday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
           
           
        ),
        array(
            'name' => 'tuesday',
            'value' => function ($data) {
                if ($data->tuesday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
        ),
        array(
            'name' => 'wednesday',
            'value' => function ($data) {
                if ($data->wednesday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
           
        ),
        array(
            'name' => 'thursday',
            'value' => function ($data) {
                if ($data->thursday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
        ),
        array(
            'name' => 'friday',
            'value' => function ($data) {
                if ($data->friday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
        ),
        array(
            'name' => 'saturday',
            'value' => function ($data) {
                if ($data->saturday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
        ),
        array(
            'name' => 'second_saturday',
            'value' => function ($data) {
                if ($data->second_saturday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
        ),
        array(
            'name' => 'fourth_saturday',
            'value' => function ($data) {
                if ($data->fourth_saturday == 0) {
                    $day = 'Holiday';
                } else {
                    $day = 'Working day';
                }
                return $day;
            },
            
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'visible'=>"isset(Yii::app()->user->role) && (in_array('/projectCalender/update', Yii::app()->session['menuauthlist']))",
                    'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Edit'),
                ),

                'delete' => array(
                    'label' => '',
                    'imageUrl' => false,
                   
                    'visible' => "isset(Yii::app()->user->role) && (in_array('/projectCalender/delete', Yii::app()->session['menuauthlist']))",
                    
                    'click' => 'function(e){e.preventDefault();deletsitecalendar($(this).attr("href"));}',
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                ),
            ),
        ),
    ),
));

?>
               
</div>

<script>
    function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
        }
   function deletsitecalendar(href)
   {
    var id = getURLParameter(href, 'id');
    var answer = confirm("Are you sure you want to delete?");
    var url = "<?php echo $this->createUrl('/projectCalender/delete&id=') ?>" + id;
    if(answer)
    {
        $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                success: function(response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                    } else if (response.response == "warning") {
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                    }
                    $("#project-calender-grid").load(location.href + " #project-calender-grid");

                }
            });
    }

   }

   var count = 0;
    var val = 0;
   function addInput() {
    $('#container').append(
        ` 
        <div class="row remove-div_` + count + `">
            <div class="col-md-3">
                        <label>Date</label>
                        <input type="text" id="holiday_date_`+count+`" class="form-control" name="holiday_date[]">
                        <div class="errorMessage display-none" id="date_em_` + count + `"></div>

                    </div>
                    <div class="col-md-3">
                        <label>Remarks</label>
                        <input type="text" id="holiday_remarks_`+count+`" class="form-control" name="holiday_remarks[]">
                        <div class="errorMessage display-none" id="remarks_em_` + count + `"></div>

                    </div>

                    <!-- remove button -->
                        <div class=" col-md-2  remove-btn-padding">
                       
                        <input type="button" id="` + count + `" value="Remove" class="remove-btn btn btn-secondary">
                        </div>
                        <!-- end remove button -->

                    </div>
                   
        `
    ); 
    
    

        $('#holiday_date_' + count).datepicker({
        dateFormat: 'dd-M-y',
    });
        count += 1;
   }


   $(document).on('click', '.remove-btn', function() {
            id = this.id;
            $(".remove-div_" + id).remove();
        });

        $(document).ready(function(){
 $('#holiday_date').datepicker({
        dateFormat: 'dd-M-y',
    });
 });    
   
    </script>