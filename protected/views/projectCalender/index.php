<?php
/* @var $this ProjectCalenderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Project Calenders',
);

$this->menu=array(
	array('label'=>'Create ProjectCalender', 'url'=>array('create')),
	array('label'=>'Manage ProjectCalender', 'url'=>array('admin')),
);
?>

<h1>Project Calenders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
