<?php
/* @var $this ProjectCalenderController */
/* @var $model ProjectCalender */

$this->breadcrumbs=array(
	'Project Calenders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectCalender', 'url'=>array('index')),
	array('label'=>'Create ProjectCalender', 'url'=>array('create')),
	array('label'=>'Update ProjectCalender', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectCalender', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectCalender', 'url'=>array('admin')),
);
?>

<h1>View ProjectCalender #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sunday',
		'monday',
		'tuesday',
		'wednesday',
		'thursday',
		'friday',
		'saturday',
		'second_saturday',
		'fourth_saturday',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
