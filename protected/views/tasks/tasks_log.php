
<?php
Yii::app()->getModule('masters');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
Yii::app()->clientScript->registerCss('mycss', '
    tr.higlight_backgrnd td:first-child {
    background: #880000;
    color: #fff;
');

Yii::app()->clientScript->registerCss('mycss', '

ul.yiiPager .first, ul.yiiPager .last{
    display:inline !important;
}
ul.yiiPager .hidden {
    display: none !important;
}
ul.yiiPager li.first, ul.yiiPager li.next {
    margin-right: 3px;
}
');

  */
?>
<?php
/* @var $this TasksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Tasks',
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        //array('label' => 'Manage Tasks', 'url' => array('admin')),
        //array('label' => 'Create Tasks', 'url' => array('create')),
    );
}

if (yii::app()->user->role == 6 or yii::app()->user->role == 4) {
    $this->menu = array(
        //array('label' => 'Create Tasks', 'url' => array('create')),
    );
}
?>


<div class="tasks-log">
<div class="clearfix">
    <div class="pull-right">

    </div>

    <div class="pull-right margin-right-10 margin-top-5">



    </div>




    <h1> Task Log</h1>


</div>
<?php
if (Yii::app()->user->role == 1) {
?>

    <?php
    Yii::app()->clientScript->registerScript('checkBoxSelect', "
function getSelectedTasks(){
var taskids = [];
 $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
            taskids[i]= $(this).val();
          });
       return \"&checkd_tsk=\"+taskids;
   }
   $(document).ready(function(){
   if($('#formtogle').css('display') == 'none'){
            $('.imptogle').val('Import Tasks +');
        }else{
            $('.imptogle').val('Import Tasks -');
        }
    $('.imptogle').click(function(){
        if($('#formtogle').css('display') != 'none'){
            $('.imptogle').val('Import Tasks +');
        }else{
            $('.imptogle').val('Import Tasks -');
        }
        $('#formtogle').toggle();
    });
});
");
    ?>

<?php
}
?>
<?php
if (Yii::app()->user->role == 1) {
?>

    <div class="form">
        <div class="clearfix">
            <div class="">

            </div>

        </div>
        <?php foreach (Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
        }
        ?>




        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'service-form',
            'enableAjaxValidation' => false,
            'method' => 'post',
            'action' => Yii::app()->createUrl('/tasks/import'),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data'
            )
        )); ?>

        <fieldset id="formtogle" style="<?php echo (empty(Yii::app()->user->getFlashes()) && Yii::app()->controller->action->id !== 'import') ? 'display:none' : 'display:block'; ?>">

            <div class="control-group">
                <div class="span4">
                    <div class="row">
                        <div class="subrow col-md-3">
                            <?php echo $form->labelEx($pmodel, 'file'); ?>
                            <?php echo $form->fileField($pmodel, 'file'); ?>
                        </div>
                        <div class="subrow col-md-2">
                            <label>&nbsp;</lable>
                                <?php echo CHtml::submitButton('Upload', array('buttonType' => 'submit', 'name' => 'upload', 'type' => 'primary', 'icon' => 'ok white', 'label' => 'UPLOAD', 'class' => 'btn btn-sm green margin-top-10')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="subrow col-md-12">
                            <?php echo $form->error($pmodel, 'file'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="subrow col-md-12">
                        <?= CHtml::link('<i class="fa fa-download" title="Download"></i>Download csv sample', $url = $this->createAbsoluteUrl('/tasks/downloadCSVsample')) ?>
                    </div>
                </div>
            </div>
        </fieldset>



    </div><!-- form -->
    <?php $this->endWidget(); ?>

<?php
}
?>

<?php

$fut_date=strtotime(date('Y-m-d'));
$expmodel = TaskExpiry::model()->findByPk(1);
if($expmodel)
{
    $date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));
    $fut_date = strtotime($date);
}

// $expmodel = TaskExpiry::model()->findByPk(1);
// $date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));
// $fut_date = strtotime($date);

$this->widget('zii.widgets.grid.CGridView', array(

    'id' => 'tasks-grid',
    'rowCssClassExpression' => '"myclass_{$data->Id}"',
    'itemsCssClass' => 'table table-bordered newcolor',
    'dataProvider' => $model->search($type),
    'ajaxUpdate' => false,
    'template' => "<div class='clearfix'><div class='pull-left pageination'>{pager}</div>  <div class='pull-right'>{summary}</div><div class='pull-right'>
   
</div></div>\n<div class='table-responsive'>{items}</div>\n{pager}",
    'filter' => $model,
    // 'ajaxUpdate' =>  'tasks-grid',
    // 'ajaxUrl'=> Yii::app()->request->getUrl(),
    'htmlOptions' => array("class" => "width-100-percentage"),
    // 'rowCssClassExpression'=>'(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "higlight_backgrnd" : ""',
    'pager' => array(
        'id' => 'dataTables-example_paginate',  'header' => '', 'firstPageLabel' => 'First ', 'lastPageLabel' => 'Last ', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next ', 'maxButtonCount' => 5,
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers pageination-margin-top',

    'rowCssClassExpression' => 'strtotime($data->due_date) < strtotime(date("Y-m-d")) ? "bg_red": ( strtotime($data->due_date) <=  ' . $fut_date . ' && strtotime($data->due_date) >= strtotime(date("Y-m-d")) ? "bg_orange" : "bg_green")',


    // 'rowCssClassExpression' =>'(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "bg_danger" : ""',

    'columns' => array(

        'tskid',
        array(
            'name' => 'title',
            'value' => function ($model) {
                if ($model->title_old != $model->title) {
                    return $model->title . '<span style="color:red"> (' . $model->title_old . ')</span>';
                } else {
                    return $model->title;
                }
            },
            'type' => 'raw',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Tasks[title]',
                'source' => $this->createUrl('Tasks/autocomplete'),
                'value' => isset($model->title) ? $model->title : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) {
                       $("#Tasks_title").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Tasks_title").val(ui.item.value); }'

                ),
            ), true),
        ),
        array(
            'name' => 'project_id',

            'value' => function ($model) {
                if ($model->project_id != $model->project_id_old) {
                    return $model->project->name . '<span style="color:red"> (' . $model->project_old->name . ')</span>';
                } else {
                    return $model->project->name;
                }
            },

            'type' => 'raw',
            'filter' => CHtml::listData(Projects::model()->findAll(
                array(
                    'select' => array('pid,name'),
                    'order' => 'name',
                    'distinct' => true
                )
            ), "pid", "name")

        ),
        array(
            'name' => 'assigned_to',
            'value' => function ($model) {
                if (($model->assigned_to != $model->assigned_to_old) && !empty($model->assigned_to)) {
                    return isset($model->assignedTo->first_name)?$model->assignedTo->first_name:""  . '<span style="color:red"> (' . $model->assignedToold->first_name . ')</span>';
                } elseif (!empty($model->assigned_to)) {
                   // return $model->assignedTo->first_name;
                } else {
                    return '';
                }
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                    'condition' => 'status=0',
                    'order' => 'first_name',
                    'distinct' => true
                )
            ), "userid", "first_name")
        ),
        array(
            'name' => 'coordinator',
            'value' => function ($model) {
                if ($model->coordinator != $model->coordinator_old) {
                    return isset($model->coordinator0->first_name)?$model->coordinator0->first_name:"" . '<span style="color:red"> (' . $model->coordinator0old->first_name . ')</span>';
                } elseif (!empty($model->coordinator)) {
                    return $model->coordinator0->first_name;
                } else {
                    return '';
                }
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                    'condition' => 'status=0',
                    'order' => 'first_name',
                    'distinct' => true
                )
            ), "userid", "first_name"),
            //'htmlOptions' => array('style' => 'border-right:1px solid #c2c2c2;')
        ),
        array(
            'name' => 'quantity',
            'value' => function ($model) {
                if ($model->quantity != $model->quantity_old) {
                    return $model->quantity . '<span style="color:red"> (' . $model->quantity_old . ')</span>';
                } else {
                    return $model->quantity;
                }
            },
            'type' => 'html',

        ),
        array(
            'name' => 'task_duration',
            'value' => function ($model) {
                if ($model->task_duration != $model->task_duration_old) {
                    return $model->task_duration . '<span style="color:red"> (' . $model->task_duration_old . ')</span>';
                } else {
                    return $model->task_duration;
                }
            },
            'type' => 'html',
            //'htmlOptions' => array( 'style' => 'width:20px;border-right:1px solid #c2c2c2;'),

        ),
        array(
            'name' => 'daily_target',
            'value' => function ($model) {
                if ($model->daily_target != $model->daily_target_old) {
                    return $model->daily_target . '<span style="color:red"> (' . $model->daily_target_old . ')</span>';
                } else {
                    return $model->daily_target;
                }
            },
            'type' => 'html',
            //'htmlOptions' => array( 'style' => 'width:20px;border-right:1px solid #c2c2c2;'),

        ),
        array(
            'name' => 'required_workers',
            'value' => function ($model) {
                if ($model->required_workers != $model->required_workers_old) {
                    return $model->required_workers . '<span style="color:red"> (' . $model->required_workers_old . ')</span>';
                } else {
                    return $model->required_workers;
                }
            },
            'type' => 'html',
            //'htmlOptions' => array( 'style' => 'width:20px;border-right:1px solid #c2c2c2;'),

        ),
        array(
            'name' => 'start_date',
            'value' => function ($model) {
                if (strtotime($model->start_date) != strtotime($model->start_date_old)) {
                    return date("d-M-y", strtotime($model->start_date)) . '<span style="color:red"> (' . date("d-M-y", strtotime($model->start_date_old)) . ')</span>';
                } else {
                    return date("d-M-y", strtotime($model->start_date));
                }
            },
            'type' => 'html',
            //'htmlOptions' => array( 'style' => 'width:20px;border-right:1px solid #c2c2c2;'),

        ),
        array(
            'name' => 'due_date',
            'value' => function ($model) {
                if (strtotime($model->due_date) != strtotime($model->due_date_old)) {
                    return date("d-M-y", strtotime($model->due_date)) . '<span style="color:red"> (' . date("d-M-y", strtotime($model->due_date_old)) . ')</span>';
                } else {
                    return date("d-M-y", strtotime($model->due_date));
                }
            },
            'type' => 'html',
            //'htmlOptions' => array( 'style' => 'border-right:1px solid #c2c2c2;'),
            'cssClassExpression' => '(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "text-danger1" : ""',
        ),
        array(
            'name' => 'status',
            'value' => function ($model) {
                if ($model->status != $model->status_old) {
                    return $model->status0->caption . '<span style="color:red"> (' . $model->status0old->caption . ')</span>';
                } else {
                    return $model->status0->caption;
                }
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type="task_status"',
                    'order' => 'status_type',
                    'distinct' => true
                )
            ), "sid", "caption")
        ),


        array(
            'name' => 'task_type',
            'value' => function ($model) {

                echo $model->getTaskType($model->tskid);
                // if ($model->task_type != $model->task_type_old) {
                //     return $model->task_type == 1 ? "External" : "Internal" . '<span style="color:red"> (' . $model->task_type_old == 1 ? "External" : "Internal" . ')</span>';
                // } else {
                //     return $model->task_type == 1 ? "External" : "Internal";
                // }
            },
            'filter' => array(1 => 'External', 2 => 'Internal'),
            'type' => 'raw',
        ),
        array(
            'name' => 'report_to',
            'value' => function ($model) {
                if (!empty($model->report_to)) {
                    if ($model->report_to != $model->report_to_old) {
                        return isset($model->reportTo->first_name)?$model->reportTo->first_name:"" . " " . isset($model->reportTo->last_name)?$model->reportTo->last_name:"" . '<span style="color:red"> (' . $model->reportToold->first_name . " " . $model->reportToold->last_name . ')</span>';
                    } else {
                        return $model->reportTo->first_name . " " . $model->reportTo->last_name;
                    }
                }
            },

            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                    'order' => 'first_name',
                    'distinct' => true
                )
            ), "userid", "first_name")
        ),
        array(
            'name' => 'work_type_id',
            'value' => function ($model) {
                if (!empty($model->work_type_id)) {
                    if ($model->work_type_id != $model->work_type_id_old) {
                        return isset($model->worktype->work_type)?$model->worktype->work_type:"" . '<span style="color:red"> (' . $model->worktypeold->work_type . ')</span>';
                    } else {
                        return  $model->worktype->work_type;
                    }
                }
            },
            'type' => 'raw',
            //'filter' => array(1 => 'External', 2 => 'Internal'),
            'filter' => CHtml::listData(WorkType::model()->findAll(
                array(
                    'select' => array('wtid,work_type'),
                    'order' => 'work_type',
                    'distinct' => true
                )
            ), "wtid", "work_type")
            //'type'=>'raw',
        ),
        array(
            'name' => 'contractor_id',
            'value' => function ($model) {
                if (!empty($model->contractor_id)) {
                    if ($model->contractor_id != $model->contractor_id_old) {
                        return $model->contractor->contractor_title . '<span style="color:red"> (' . $model->contractorold->contractor_title . ')</span>';
                    } else {
                        return $model->contractor->contractor_title;
                    }
                }
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Contractors::model()->findAll(
                array(
                    'select' => array('id,contractor_title'),
                    'condition' => 'status=1',
                    'order' => 'contractor_title',
                    'distinct' => true
                )
            ), "id", "contractor_title")
        ),
        array(
            'name' => 'created_by',
            'value' => function ($model) {
                if ($model->created_by != $model->created_by_old) {
                    return $model->createdBy->first_name . " " . $model->createdBy->last_name . '<span style="color:red"> (' . $model->createdByold->first_name . " " . $data->createdByold->last_name . ')</span>';
                } else {
                    return $model->createdBy->first_name . " " . $model->createdBy->last_name;
                }
            },

            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                    'condition' => 'status=0',
                    'order' => 'first_name',
                    'distinct' => true
                )
            ), "userid", "first_name")
        ),
        array(
            'name' => 'updated_by',
            'value' => function ($model) {
                if ($model->updated_by != $model->updated_by_old) {
                    return $model->updatedBy->first_name . " " . $model->updatedBy->last_name . '<span style="color:red"> (' . $model->updatedByold->first_name . " " . $data->updatedByold->last_name . ')</span>';
                } else {
                    return $model->updatedBy->first_name . " " . $model->updatedBy->last_name;
                }
            },

            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                    'condition' => 'status=0',
                    'order' => 'first_name',
                    'distinct' => true
                )
            ), "userid", "first_name")
        ),
        array(
            'name' => 'updated_date',
            'value' => 'date("d-M-y H:i:s",strtotime($data->dt_datetime))',
            'type' => 'html',
        ),





    ),
));
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<?php
$alltasks = Tasks::model()->findAll();
$newarr = array();
foreach ($alltasks as $data) {
    $newarr[] = $data['title'];
}
$alltasklist =   json_encode($newarr);


?>


<div id="id_view"></div>
</div>
<?php
Yii::app()->clientScript->registerScript('myjavascript', '

    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });


  } );

');


?>