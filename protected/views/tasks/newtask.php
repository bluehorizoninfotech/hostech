<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<?php
/* @var $this TasksController */
/* @var $model Tasks */
/* @var $form CActiveForm */
?>





<div class="new-task-sec">
<div class="form margin-top-5">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tasks-form',

        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>

    <h2>Add Task</h2>
    <?php
    $condition = '';

    if (Yii::app()->user->role != 1) {


        /* $tblpx = Yii::app()->db->tablePrefix;
    
    $sqlp = Yii::app()->db->createCommand("SELECT GROUP_CONCAT(CONCAT('''', cs.pid, '''' )) as pid FROM {$tblpx}clientsite cs JOIN {$tblpx}clientsite_assigned ca ON ca.site_id = cs.id WHERE ca.user_id = '".Yii::app()->user->id."'")->queryRow();

    if($sqlp['pid']){
       $condition = 'pid IN ('.$sqlp['pid'].')';
    }else{
        $condition = '1!=1';
    }*/

        $proarr = array();
        $proarr1 = 0;
        $allprojects = Tasks::model()->findAll('assigned_to = ' . Yii::app()->user->id);

        if (!empty($allprojects)) {
            foreach ($allprojects as $key => $value) {
                if ($value['project_id'] != '') {
                    $proarr[] = $value['project_id'];
                }
            }
        }

        if (!empty($proarr)) {
            $proarr1 = implode(',', $proarr);
        } else {
            $proarr1 = 0;
        }

        $condition = ' pid in (' . $proarr1 . ')';
    }

    ?>
    <div class="row">
        <div class="subrow subrowlong">
            <?php //$project = Projects::model()->findByPk(306); 
            ?>
            <!--<label style="margin-left: 13px;">Project : &nbsp;&nbsp;<?php //$project->name; 
                                                                        ?></label>-->
            <?php echo $form->labelEx($model, 'project_id'); ?>
            <?php

            if ($model->isNewRecord) {
                if (Yii::app()->user->project_id != "") {
                    $model->project_id = Yii::app()->user->project_id;
                } else {
                    $model->project_id = "";
                }
            }

            echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC', 'condition' => $condition)), 'pid', 'name'), array('empty' => 'Choose a project','class' => 'form-control width-466', 'ajax' => array(
                'type' => 'POST',
                'url' => CController::createUrl('Tasks/getclientlocationbyid'), //or $this->createUrl('loadcities') if '$this' extends CController
                'update' => '#Tasks_clientsite_id', //or 'success' => 'function(data){...handle the data in the way you want...}',
                'data' => array('pid' => 'js:this.value'),
            ))); ?>
            <?php echo $form->error($model, 'project_id'); ?>
        </div>
    </div>


    <div class="row">
        <div class="subrow subrowlong">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>
    </div>
    <div class="row">

        <?php
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 3)
            $display = 'block';
        else
            $display = "none";
        ?>
        <?php
        ?>




        <div class="subrow">


            <?php // echo $form->dropDownList($model,'clientsite_id',array('class'=>'form-control input-medium',)); 
            ?>
            <?php echo $form->labelEx($model, 'clientsite_id'); ?>

            <?php
            if (empty($location) && $location == 'create') {
            ?>
            <?php
                echo $form->dropDownList(
                    $model,
                    'clientsite_id',
                    CHtml::listData(Clientsite::model()->findAll(array('order' => 'site_name ASC')), 'id', 'site_name'),
                    array(
                        'empty' => 'Choose a client site',
                        'class' => 'form-control input-medium',
                    )
                );
            } else {
            ?>

                <?php
                echo $form->dropDownList(
                    $model,
                    'clientsite_id',
                    CHtml::listData($location, 'id', 'site_name'),
                    array(
                        'empty' => 'Choose a client site',
                        'class' => 'form-control input-medium',
                        'ajax' => array(
                            'type' => 'GET',
                            'url' => CController::createUrl('Tasks/getassignedperson'), //or $this->createUrl('loadcities') if '$this' extends CController
                            'update' => '#Tasks_assigned_to', //or 'success' => 'function(data){...handle the data in the way you want...}',
                            'data' => array('id' => 'js:this.value'),
                        )

                    )
                );
                ?>

            <?php } ?>


            <?php echo $form->error($model, 'clientsite_id'); ?>
        </div>


        <div class="subrow">
            <?php echo $form->labelEx($model, 'priority'); ?>
            <?php
            echo $form->dropDownList($model, 'priority', CHtml::listData(Status::model()->findAll(array(
                'select' => array('sid,caption'),
                'condition' => 'status_type="priority"',
                'order' => 'caption',
                'distinct' => true
            )), 'sid', 'caption'), array('empty' => 'Choose a priority', 'class' => 'form-control input-medium'));
            ?>
            <?php echo $form->error($model, 'priority'); ?>
        </div>
    </div>


    <div class="row">
        <div class="subrow">
            <?php
            //Default value for start and due date

            if (!isset($model->start_date))
                $model->start_date = date('d-M-y');

            if (!isset($model->due_date)) {
                $cd = strtotime(date('d-M-y'));
                $model->due_date = date('d-M-y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + 7, date('Y', $cd)));
            }
            $model->start_date = date('d-M-y', strtotime($model->start_date));
            $model->due_date = date('d-M-y', strtotime($model->due_date));
            ?>


            <?php echo $form->labelEx($model, 'start_date'); ?>
            <?php echo CHtml::activeTextField($model, 'start_date', array('class' => 'form-control input-small start_date', "id" => "Tasks_start_date")); ?>
            <?php
            // $this->widget('application.extensions.calendar.SCalendar', array(
            //     'inputField' => 'Tasks_start_date',
            //     'ifFormat' => '%Y-M-%d',
            // ));
            ?>
            <?php echo $form->error($model, 'start_date'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'due_date'); ?>
            <?php echo CHtml::activeTextField($model, 'due_date', array('class' => 'form-control input-small due_date', "id" => "due_date", 'size' => 10)); ?>
            <?php
            // $this->widget('application.extensions.calendar.SCalendar', array(
            //     'inputField' => 'due_date',
            //     'ifFormat' => '%Y-%m-%d',
            // ));
            ?>
            <?php echo $form->error($model, 'due_date'); ?>
        </div>

    </div>


    <div class="row">

        <?php
        $co_toggle = '';
        $rep_toggle = '';
        if (Yii::app()->user->role == 6) {
            if (!isset($model->coordinator)) {
                $model->coordinator = Yii::app()->user->id;
                $co_toggle = 'toggle-field';
            }
        } else {
            if (!isset($model->report_to)) {
                $model->report_to = Yii::app()->user->id;
                $rep_toggle = 'toggle-field';
            }
        }
        ?>


    </div>
    <div class="row">



        <div class="subrow">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php
            echo $form->dropDownList($model, 'status', CHtml::listData(Status::model()->findAll(array(
                'select' => array('sid,caption'),
                'condition' => 'status_type="task_status"',
                'order' => 'caption',
                'distinct' => true
            )), 'sid', 'caption'), array('class' => 'form-control input-medium', 'empty' => 'Choose a status'));
            ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>



    </div>


    <!--div class="row">
         <div class="subrow">
            <?php //echo $form->labelEx($model, 'hourly_rate'); 
            ?>
            <?php //echo $form->textField($model, 'hourly_rate', array('class'=>'form-control input-small')); 
            ?>
            <?php //echo $form->error($model, 'hourly_rate'); 
            ?>
        </div>
        
        <div class="subrow">
            <?php //echo $form->labelEx($model, 'billable'); 
            ?>
            <div class="radio_btn">
                <?php
                /* echo $form->radioButtonList($model, 'billable', CHtml::listData(Status::model()->findAll(
                                        array(
                                            'select' => array('sid,caption'),
                                            'condition' => 'status_type="yesno"',
                                            'order' => 'caption',
                                            'distinct' => true
                                )), 'sid', 'caption'), array( 'labelOptions'=>array('style'=>'display:inline'),'separator' => '')); */
                ?>
             </div>
            <?php //echo $form->error($model, 'billable'); 
            ?>
      </div>
      </div-->
    <!--div class="row">
        <div class="subrow">
            <?php //echo $form->labelEx($model, 'total_hrs'); 
            ?>
            <?php //echo $form->textField($model, 'total_hrs', array('class'=>'form-control input-small','size'=>10)); 
            ?>
            <?php //echo $form->error($model, 'total_hrs'); 
            ?>
        </div>

        <div class="subrow">
            <?php //echo $form->labelEx($model, 'min_rate'); 
            ?>
            <?php //echo $form->textField($model, 'min_rate', array('class'=>'form-control input-small','size'=>10)); 
            ?>
            <?php //echo $form->error($model, 'min_rate'); 
            ?>
        </div>
   
    </div-->
    <div class="row">
        <div class="subrow subrowlong">

            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('rows' => 3, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->
                            </div>
<script type="text/javascript">
    $(document).ready(function() {


        $('.due_date').datepicker({
            dateFormat: 'd-M-y',
        });
        $('.start_date').datepicker({
            dateFormat: 'd-M-y',

        });
    });

    $(document).ready(function() {

        var data = $('#main_task').val();

        $("#main_task").change(function() {

            var content = $("#main_task option:selected").text();
            if (content == '--') {
                alert();
            }


            //$("#show_dropdown_content").text("You have selected: "+content);
        });
    });
</script>

<script>
    /*$('.project').change(function(){
var val =$(this).val();
    alert(val);
    $.ajax({
        url: '<?php echo Yii::app()->createUrl('tasks/getlocation'); ?>',
        data:{project_id:val},
        dataType:'json',
        method: "GET",
        success: function(result) {
             $('.clientsite').append(result);
        }
        
    })
});*/
</script>