<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */
/* @var $form CActiveForm */
?>
<div class="search-report-section">
<div id="msg"></div>

<div class="form custom-form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="col-md-3 col-sm-6 date">


            <?php
            if (!isset($_GET['Tasks']['date_from']) || $_GET['Tasks']['date_from'] == '') {
                $datefrom = date("Y-m-") . "01";
            } else {
                $datefrom = $_GET['Tasks']['date_from'];
            }
            ?>
            <?php echo CHtml::label('Date From', ''); ?>
            <?php echo CHtml::textField('Tasks[date_from]', $datefrom, array("id" => "date_from", 'class' => 'form-control height-28')); ?>
            <?php echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button3", "class" => "pointer"));
            ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'date_from',
                'button' => 'c_button3',
                'ifFormat' => '%d-%b-%y',
                // 'ifFormat' => '%Y-%m-%d',
            ));
            ?>
        </div>
        <div class="col-md-3 col-sm-6 date">
            <?php
            if (!isset($_GET['Tasks']['date_till']) || $_GET['Tasks']['date_till'] == '') {
                $datetill = date("Y-m-d");
            } else {
                $datetill = $_GET['Tasks']['date_till'];
            }
            ?>

            <?php echo CHtml::label('Date Upto', ''); ?>
            <?php echo CHtml::textField('Tasks[date_till]', $datetill, array("id" => "date_till", 'class' => 'form-control height-28')); ?>
            <?php echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button2", "class" => "pointer"));
            ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'date_till',
                'button' => 'c_button2',
                //'ifFormat' => '%Y-%m-%d',
                'ifFormat' => '%d-%b-%y',
            ));
            ?>
        </div>
        <!--div class="col-sm-3" style="width:150px">
            <?php //echo CHtml::label('Only billable', 'billable'); 
            ?>
            <?php //echo CHtml::checkBox('TimeReport[billable]', '', array("value" => 'Yes', "id" => "obill", 'style' => 'margin-top:12px;')); 
            ?>
        </div-->

        <div class="col-md-2">
            <?php
            /* echo CHtml::dropDownList('Tasks[projectid]', '', CHtml::listData(Projects::model()->findAll(
                                    array(
                                        'select' => array('pid,name'),
                                        'order' => 'name',
                                        'distinct' => true
                            )), "pid", "name"), array('class'=>'form-control project','empty' => 'Select a project'));
                            * 
                            * */
            ?>
            <?php
            if ($model->project_id == "") {
                if (Yii::app()->user->project_id != "") {
                    $model->project_id = Yii::app()->user->project_id;
                } else {
                    $model->project_id = "";
                }
            }
            echo $form->dropDownList(
                $model,
                'project_id',
                CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
                array(
                    'empty' => 'Choose a project',
                    'class' => 'form-control input-medium select_drop project change_project height-28',
                    /*'ajax' => array
                        (
                        'type'=>'GET', 
                        'url'=>CController::createUrl('Tasks/test'), //or $this->createUrl('loadcities') if '$this' extends CController
                        'update'=>'#clientsite_id', //or 'success' => 'function(data){...handle the data in the way you want...}',
                        'data'=>array('pid'=>'js:this.value'),
                        )*/
                )
            );
            ?>
        </div>


        <div class="col-md-2">
            <?php
            /* echo CHtml::dropDownList('Tasks[clientsite_id]', '', CHtml::listData(Clientsite::model()->findAll(
                                        array('order' => 'site_name',
                                            'distinct' => true
                                )), "id", "site_name"), array('class'=>'form-control','empty' => 'Select a location'));
                                * */
            ?>
            <?php // echo CHtml::dropDownList('clientsite_id','', array(), array('prompt'=>'Select City'));  
            ?>

            <!-- <select name="Tasks[clientsite_id]" id="Tasks_clientsite_id" class="form-control input-medium select_drop clientsite">
                 <option value="">select location</option>
                 </select> -->
            <?php
            echo CHtml::dropDownList('Tasks[clientsite_id]', '', CHtml::listData(Clientsite::model()->findAll(
                array(
                    'order' => 'site_name',
                    'distinct' => true
                )
            ), "id", "site_name"), array('class' => 'form-control height-28', 'empty' => 'Select a location'));

            ?>
        </div>
        <div class="col-md-2">
            <?php echo CHtml::Button('Search', array('class' => 'btn btn-sm blue test')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div>
        </div>
<!-- search-form -->
<script>
    jQuery(function($) {
        $('.project').change(function() {
            var val = $(this).val();
            if (val != '') {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('tasks/getlocation'); ?>',
                    data: {
                        project_id: val
                    },
                    dataType: 'json',
                    method: "GET",
                    success: function(result) {
                        $('.clientsite').html(result);
                    }

                })
            }
        });

        $('.test').click(function() {
            var date_from = $('#date_from').val();
            var date_till = $('#date_till').val();
            if (date_from < date_till || date_till > date_from) {
                $('#yw0').submit();
                $('#msg').html('');
                $('.grid-view').show()
            } else {
                $('.grid-view').hide()
                $('#msg').html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Please enter valid date range.</div>');
            }

            //
        });
    });
</script>