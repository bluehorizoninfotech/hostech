<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	$model->title=>array('view','id'=>$model->title),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Tasks', 'url'=>array('index')),
//	array('label'=>'Create Tasks', 'url'=>array('create')),
//	array('label'=>'View Tasks', 'url'=>array('view', 'id'=>$model->tskid)),
//	array('label'=>'Manage Tasks', 'url'=>array('admin')),
);
?>

<h2>Update Task: <span><?php echo $model->title; ?></span></h2>

<?php echo $this->renderPartial('_task_form', array('model'=>$model,'location'=>$location, 'assigned_to' => $assigned_to,'template'=>$template,'task_project_id'=>"")); ?>
