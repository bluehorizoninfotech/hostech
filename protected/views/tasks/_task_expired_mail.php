<html><head><title>Expired Tasks</title></head>
<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi <?php echo $owner['first_name'].' '.$owner['last_name']?>,</span></h3>
    <body><p><h2>Expired Task Notification</h2></p>
   
    <table border='1' style='border-collapse:collapse;width: 100%;'><tr>
            <th style='color: #000;padding:6px 8px;'>Sl no</th>
            <th style='color: #000;padding:6px 8px;'>Task Id</th>
            <th style='color: #000;padding:6px 8px;'>Task Name</th>
            <th style='color: #000;padding:6px 8px;'>Project Name</th>						
            <th style='color: #000;padding:6px 8px;'>Assignee</th>
            <th style='color: #000;padding:6px 8px;'>Start Date</th>
            <th style='color: #000;padding:6px 8px;'>End Date</th>						
        </tr>
        <?php
        $k = 1;

        foreach ($data as $task) {

            if (!empty($task)) {
                ?>
                <tr>
                    <td style='padding:5px 8px;'><?= $k ?></td>
                    <td style='padding:5px 8px;'><?= $task->tskid ?></td>
                    <td style='padding:5px 8px;'><?= $task['title'] ?></td>
                    <td style='padding:5px 8px;'><?= $task->project->name ?></td>


                    <td style='padding:5px 8px;'><?= $task->getAssigneedeatils($task['assigned_to']) ?></td>
                    <td style='padding:5px 8px;'><?= Yii::app()->dateFormatter->format("d MMM y", strtotime($task['start_date'])) ?></td>
                    <td style='padding:5px 8px;'><?= Yii::app()->dateFormatter->format("d MMM y", strtotime($task['due_date'])) ?></td>							
                </tr>
                <?php
                $k++;
            }
        }
        ?>



    </table>
    <p>Sincerely,  <br />
<?php echo Yii::app()->name; ?></p>
    </body></html>