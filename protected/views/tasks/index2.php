<?php
if (!isset($_GET['exportgrid'])) {
?>

    <?php
    Yii::app()->getModule('masters');

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    /*
      Yii::app()->clientScript->registerCss('mycss', '
      tr.higlight_backgrnd td:first-child {
      background: #880000;
      color: #fff;
      ');

      Yii::app()->clientScript->registerCss('mycss', '

      ul.yiiPager .first, ul.yiiPager .last{
      display:inline !important;
      }
      ul.yiiPager .hidden {
      display: none !important;
      }
      ul.yiiPager li.first, ul.yiiPager li.next {
      margin-right: 3px;
      }
      ');

     */
    ?>
    <?php
    /* @var $this TasksController */
    /* @var $dataProvider CActiveDataProvider */

    $this->breadcrumbs = array(
        'Tasks',
    );

    if (yii::app()->user->role <= 2) {
        $this->menu = array(
            //array('label' => 'Manage Tasks', 'url' => array('admin')),
            //array('label' => 'Create Tasks', 'url' => array('create')),
        );
    }

    if (yii::app()->user->role == 6 or yii::app()->user->role == 4) {
        $this->menu = array(
            //array('label' => 'Create Tasks', 'url' => array('create')),
        );
    }
    ?>
    <!-- start response message -->

    <div class="alert alert-success " role="alert" style="display: none;">
    </div>
    <div class="alert alert-danger " role="alert" style="display: none;">
    </div>
    <div class="alert alert-warning " role="alert" style="display: none;">

    </div>

    <!-- end response message -->
    <div class="index-two-tasksec">
        <div class="clearfix">
            <div class="pull-right responsive-screen task-btns">

                <!-- <input class="form-control margin-right-4"> -->
                <button class="btn blue margin-right-4" id="search_by_date">Search</button>





                <?php
                if ($unassigned_tasks_count == 0) {
                    $unassigned_tasks_count = '';
                }
                echo CHtml::link('Unassigned Tasks<span class="badge" style="background-color: #f45245;margin-top: -30px;">' . $unassigned_tasks_count . '</span>', array("Tasks/index2", 'type' => 'unassigned'), array('class' => 'btn blue button-space'));

                // if(Controller::allowPermission('1') && !empty(Yii::app()->user->getState("permission"))){
                $createUrl = $this->createUrl('addtask', array("asDialog" => 1, "gridId" => 'tasks-grid'));
                // echo CHtml::link('Add Task', '', array('class' => 'btn blue margin-left-4 button-space', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
                // }


                echo CHtml::link('Add Task', array('createTask'), array('class' => 'btn blue margin-left-4 button-space margin-right-4'));


                ?>
                <?php
                /* echo CHtml::link('Download <img src="' . Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/pdf.png" border="0"
              style="height:30px; margin-bottom: -12px;"/>', array('/tasks/SaveToPdf'), array('class' => 'savepdf')); */
                echo CHtml::link('Download <i class="fa fa-file-excel-o"></i>', Yii::app()->request->requestUri . "&exportgrid=1", array('class' => 'btn savepdf button-space', 'target' => '_blank'));
                ?>

                <!-- <?php
                        if (in_array("/tasks/itemEstimation", Yii::app()->session["menuauthlist"]) && Tasks::model()->accountPermission() == 1) {
                            echo CHtml::link('Import Task Estimate', array('tasks/import_task_estimate'), array('class' => 'btn blue'));
                        }

                        ?> -->

                <?php
                if (Yii::app()->user->role == 1) {
                ?>
                    <?php //echo CHtml::submitButton('Import Tasks +', array('name' => 'button1','class' => 'imptogle btn blue btn-primary')); 
                    ?>
                <?php } ?>
            </div>



            <div class="pull-right margin-right-10 margin-top-5">

                <?php /*
              echo CHtml::dropDownList('selectgroup', '', CHtml::listData(Groups::model()->findAll(
              array(
              'select' => array('group_id,group_name'),
              'order' => 'group_name',
              'distinct' => true,
              'condition' => (Yii::app()->user->role > 1) ? 'group_lead = '.Yii::app()->user->id : ''
              )), "group_id", "group_name"),
              array('empty' => 'All Groups','class' => 'form-control','options' => array((isset(Yii::app()->user->group_id) ? Yii::app()->user->group_id : 0) => array('selected' => true)),
              'ajax' => array(
              'type' => 'POST', //request type
              'url' => Yii::app()->request->getBaseUrl(true) . '/index.php?r=tasks/filterbygroup', //url to call.
              'success' => 'js:function(data){
              location.reload();
              }',
              'data' => array('group_id' => 'js:this.value'),
              )));

             */ ?>

            </div>



            <?php
            if ($type == 'expired') {
            ?>

                <h1 class="assigned-task-header">Expired Tasks</h1>

            <?php
            } else {
            ?>
                <h1 class="assigned-task-header"> Tasks</h1>
            <?php }
            ?>
        </div>
        <!-- here -->
        <div class="view" id="view_search">


            <div class="form-group custom-search-form">
                <div class="task-date-search">
                    <div>
                        <input type="text" class="form-control date_pick height-28" id="Tasks_start_date" name="Tasks[filter_start_date]">
                    </div>
                    <div>


                        <input type="text" class="form-control date_pick height-28" id="Tasks_due_date" name="Tasks[filter_due_date]">
                    </div>
                    <div>
                        <div class="pull-right">

                            <?php echo CHtml::Button('GO', array('id' => 'serachbtn', 'class' => 'btn btn-sm btn-primary')); ?>

                            <?php echo CHtml::Button('Cancel', array('class' => 'btn-default', 'id' => 'cancel-btn')); ?>
                        </div>
                    </div>
                </div>


            </div>

        </div>
        <?php
        if (Yii::app()->user->role == 1) {
        ?>

            <?php
            Yii::app()->clientScript->registerScript('checkBoxSelect', "
function getSelectedTasks(){
var taskids = [];
 $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
            taskids[i]= $(this).val();
          });
       return \"&checkd_tsk=\"+taskids;
   }
   $(document).ready(function(){
   if($('#formtogle').css('display') == 'none'){
            $('.imptogle').val('Import Tasks +');
        }else{
            $('.imptogle').val('Import Tasks -');
        }
    $('.imptogle').click(function(){
        if($('#formtogle').css('display') != 'none'){
            $('.imptogle').val('Import Tasks +');
        }else{
            $('.imptogle').val('Import Tasks -');
        }
        $('#formtogle').toggle();
    });
});
");
            ?>

        <?php
        }
        ?>
        <?php
        if (Yii::app()->user->role == 1) {
        ?>

            <div class="form task-responsive-form">
                <div class="clearfix">
                    <div class="">

                    </div>

                </div>
                <?php if (Yii::app()->user->hasFlash('success')) : ?>
                    <div class="alert alert-success alert-dismissable ">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'service-form',
                    'enableAjaxValidation' => false,
                    'method' => 'post',
                    'action' => Yii::app()->createUrl('/tasks/import'),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data'
                    )
                ));
                ?>

                <fieldset id="formtogle" style="<?php echo (empty(Yii::app()->user->getFlashes()) && Yii::app()->controller->action->id !== 'import') ? 'display:none' : 'display:block'; ?>">

                    <div class="control-group">
                        <div class="span4">
                            <div class="row">
                                <div class="subrow col-md-3">
                                    <?php echo $form->labelEx($pmodel, 'file'); ?>
                                    <?php echo $form->fileField($pmodel, 'file'); ?>
                                </div>
                                <div class="subrow col-md-2">
                                    <label>&nbsp;</lable>
                                        <?php echo CHtml::submitButton('Upload', array('buttonType' => 'submit', 'name' => 'upload', 'type' => 'primary', 'icon' => 'ok white', 'label' => 'UPLOAD', 'class' => 'btn btn-sm green margin-top-10')); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="subrow col-md-12">
                                    <?php echo $form->error($pmodel, 'file'); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="subrow col-md-12">
                                <?= CHtml::link('<i class="fa fa-download" title="Download"></i>Download csv sample', $url = $this->createAbsoluteUrl('/tasks/downloadCSVsample')) ?>
                            </div>
                        </div>
                    </div>
                </fieldset>



            </div><!-- form -->
            <?php $this->endWidget(); ?>

        <?php
        }
        ?>
        <div class="clearfix btn-holder">
            <div class="checkbox-sec pull-left">
                <label class="checkbox-inline">
                    <input type="checkbox" class="change" <?= (isset($_GET['type']) and $_GET['type'] == 'all_tasks') ? 'checked' : '' ?> id="list_change" data-toggle="toggle">
                </label> Hide Expired Tasks
                <label class="checkbox-inline">
                    <input type="checkbox" class="change" <?= (isset($_GET['type_closed']) and $_GET['type_closed'] == 'closed') ? 'checked' : '' ?> id="list_change_closed" data-toggle="toggle">
                </label> Completed Tasks
                <?php
                if (in_array('/tasks/miantasks', Yii::app()->session['menuauthlist'])) {
                ?>
                    <label class="checkbox-inline">
                        <input type="checkbox" class="change" <?= (isset($_GET['type']) and $_GET['type'] == 'main_tasks') ? 'checked' : '' ?> id="list_change_main_tasks" data-toggle="toggle">
                    </label> Main Tasks
                <?php
                }
                ?>

                <label class="checkbox-inline">
                    <input type="checkbox" class="change" <?= (isset($_GET['type']) and $_GET['type'] == 'sub_tasks') ? 'checked' : '' ?> id="list_change_sub_tasks" data-toggle="toggle">
                </label> Sub Tasks


                <label class="checkbox-inline">
                    <input type="checkbox" class="change" <?= (isset($_GET['type']) and $_GET['type'] == 'expired_tasks') ? 'checked' : '' ?> id="list_change_expired_tasks" data-toggle="toggle">
                </label> Expired Tasks

            </div>
            <div class="tab-content">
                <p id="success_message" class="font-15 green-color font-weight-bold">Successfully Updated..</p>
            </div>
            <div class="selection-btns margin-top-30">
                <div class="pull-left filter-form status_change_btn margin-left-16">
                    <button type="button" class="btn btn-primary popover-test" data-toggle="popover" data-placement="bottom" type="button" data-html="true">
                        Change Status
                    </button>
                    <div class="popover-content display-none">

                        <ul class="box_sec">
                            <?php
                            $lists = CHtml::listData(Status::model()->findAll(
                                array(
                                    'select' => array('sid,caption'),
                                    'condition' => 'status_type="task_status"',
                                    'order' => 'status_type',
                                    'distinct' => true
                                )
                            ), "sid", "caption");

                            foreach ($lists as $key => $value) {
                                echo "<li><input type='radio' name='$value' value='$key'>" . $value . "</li>";
                            }
                            ?>
                        </ul>
                        <label>Description</label>
                        <textarea id="status_description" name="status_description"></textarea>
                        <div class="text-center status_submit">
                            <button type="button" class="submit_btn change_status btn btn-primary btn-sm margin-top-5">Submit</button>
                        </div>
                    </div>
                </div>

                <div class="pull-left filter-form margin-left-16">
                    <button class="btn btn-primary popover-test" data-toggle="popover" data-placement="bottom" type="button" data-html="true">Date Extension</button>
                    <div class="popover-content display-none">
                        <div class="list-holder">
                            <div class="btn-group change_type" role="group" aria-label="Change Type">
                                <button type="button" class="btn active" data-id="1"> Prepone By</button>
                                <button type="button" class="btn" data-id="2">Postpone By</button>
                            </div>
                            <div class="btn-group date_type" role="group" aria-label="Date Type">
                                <button type="button" class="btn active" data-id="1"> Start Date</button>
                                <button type="button" class="btn" data-id="2">End Date</button>
                            </div>

                            <div class="change_value text-center">
                                <label>#Number of Days:</label>
                                <input type="number" min="1" name="change_value" id="change_value" class="form-control width-50 display-inline-block">
                            </div>

                        </div>

                        <div class="text-center">
                            <input type="button" name="submit" value="Submit" class="submit_btn apply btn btn-primary btn-sm">
                        </div>
                    </div>
                </div>
                <div class="pull-left filter-form assignee-form margin-left-16">
                    <button class="btn btn-primary popover-test" data-toggle="popover" data-placement="bottom" type="button" data-html="true" id="change_assign">Change Assignee</button>
                    <div class="popover-content display-none">
                        <div class="assignee_change_form">
                            <select name="assigne_change" id="assigne_change" class="form-control js-example-basic-multiple ">
                                <option value="">Select Assignee</option>
                                <?php
                                $condition = array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0 ', 'order' => 'first_name ASC');
                                $users = Users::model()->findAll($condition);

                                foreach ($users as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['userid']; ?>"><?php echo $value['full_name'] . ' (' . $value->userType->role . ')'; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <div class="text-center">
                                <input type="button" name="submit" value="Submit" class="submit_btn change_assignee btn btn-primary btn-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>
        <!-- <div style="display:flex;" class="status_change_form">
    <?php
    $list = CHtml::listData(Status::model()->findAll(
        array(
            'select' => array('sid,caption'),
            'condition' => 'status_type="task_status"',
            'order' => 'status_type',
            'distinct' => true
        )
    ), "sid", "caption");

    echo CHtml::dropDownList(
        'status_change',
        'ss',
        $list,
        array('empty' => 'Select Status', 'class' => 'form-control input-small')
    );
    ?>
        <input type="button" name="submit" value="Submit" class="change_status btn btn-primary btn-sm">
    </div> -->

        <!-- <div style="display:flex;" class="assignee_change_form">
        
        <select name="assigne_change" id="assigne_change" class="form-control js-example-basic-single ">
            <option value="">Select Assignee</option>
    <?php
    $condition = array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0 ', 'order' => 'first_name ASC');
    $users = Users::model()->findAll($condition);

    foreach ($users as $key => $value) {
    ?>
                                                        <option value="<?php echo $value['userid']; ?>"><?php echo $value['full_name'] . ' (' . $value->userType->role . ')'; ?></option>
        <?php
    }
        ?>
        </select>
        <input type="button" name="submit" value="Submit" class="change_assignee btn btn-primary btn-sm">
    </div> -->


        <!-- Task status bulk change ends-->
        <div class="tab-content">
            <p id="success_message_id" class=" font-15  green-color font-weight-bold">Successfully Updated..</p>
        </div>
    <?php
    $proj_condition = Yii::app()->Controller->getProjectCondition();
    $project_dropdown_1 = CHtml::listData(Projects::model()->findAll(
        array(
            'select' => array('pid,name'),
            'order' => 'name',
            'distinct' => true,
            'condition' => $proj_condition
        )
    ), "pid", "name");
    $null_dropdown = array('0' => 'All');
    $projects_list = $project_dropdown_1 + $null_dropdown;
    ksort($projects_list);
    $expmodel = TaskExpiry::model()->findByPk(1);
    $fut_date = strtotime(date('Y-m-d'));
    if ($expmodel) {
        $date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));
        $fut_date = strtotime($date);
    }



    //////////// assigned to///
    $assigned_to_dropdown = CHtml::listData(Users::model()->findAll(
        array(
            'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
            'condition' => 'status=0',
            'order' => 'first_name',
            'distinct' => true
        )
    ), "userid", "first_name");
    $null_assigned_to = array('' => 'All');
    $assigned_to_list = $assigned_to_dropdown + $null_assigned_to;
    ksort($assigned_to_list);

    //////////// milestone ////////////

    $milestone_dropdown = CHtml::listData(Milestone::model()->findAll(
        array(
            'select' => array('id, milestone_title'),
            'condition' => 'status=1 and project_id != ""',
            'order' => 'milestone_title',
            
        )
    ), "id", "milestone_title");

    $null_milestone = array('' => 'All');

    $milestone_list = $milestone_dropdown + $null_milestone;
    ksort($milestone_list);

    ////////// coordinator ///////////
    $co_ordinator_dropdown =  CHtml::listData(Users::model()->findAll(
        array(
            'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
            'condition' => 'status=0',
            'order' => 'first_name',
            'distinct' => true
        )
    ), "userid", "first_name");
    $null_co_ordinator = array('' => 'All');
    $co_ordinator_list = $co_ordinator_dropdown + $null_co_ordinator;
    ksort($co_ordinator_list);

    ////////////// status //////////////
    $status_dropdown = CHtml::listData(Status::model()->findAll(
        array(
            'select' => array('sid,caption'),
            'condition' => 'status_type="task_status"',
            'order' => 'status_type',
            'distinct' => true
        )
    ), "sid", "caption");
    $null_status = array('' => 'All');
    $status_list = $status_dropdown + $null_status;
    ksort($status_list);

    /////////// Task owner /////////
    $task_owner_dropdown =  CHtml::listData(Users::model()->findAll(
        array(
            'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
            'order' => 'first_name',
            'distinct' => true
        )
    ), "userid", "first_name");
    $null_task_owner = array('' => 'All');
    $task_owner_list = $task_owner_dropdown + $null_task_owner;
    ksort($task_owner_list);

    ////////// contractor ////////////////
    $contractor_dropdown = CHtml::listData(Contractors::model()->findAll(
        array(
            'select' => array('id,contractor_title'),
            'condition' => 'status=1',
            'order' => 'contractor_title',
            'distinct' => true
        )
    ), "id", "contractor_title");
    $null_contractor = array('' => 'All');
    $contractor_list = $contractor_dropdown + $null_contractor;
}


$widget = 'zii.widgets.grid.CGridView';
$search_size = true;
$showFilter = $model;
if (isset($_GET['exportgrid'])) {
    $widget = 'application.core.CGridView';
    $search_size = false;
    $showFilter = false;
}



$grid_params_array = array(
    'id' => 'tasks-grid',
    'rowCssClassExpression' => '"myclass_{$data->Id}"',
    'itemsCssClass' => 'table table-bordered newcolor',
    'dataProvider' => $model->search($type, $type_closed, $search_size),
    'ajaxUpdate' => false,
    'template' => "<div class='clearfix'><div class='pull-left'>{pager}</div>  <div class='pull-right'>{summary}</div><div class='pull-right'>
    <i class=\"fa fa-check\" style=\"color:green\"></i><span class='cont_pal'>Acknowledged&nbsp;&nbsp;</span>
    <span class='clr_pal bg_orange'></span><span class='cont_pal'>Expiring tasks&nbsp;&nbsp;</span>
    <span class='clr_pal bg_green'></span><span class='cont_pal'>New tasks&nbsp;&nbsp;</span>
    <span class='clr_pal bg_red'></span><span class='cont_pal'>Expired tasks&nbsp;&nbsp;</span>
    <span class='clr_pal bg_beige'></span><span class='cont_pal'>OnHold tasks&nbsp;&nbsp;</span>
</div></div>\n<div class='table-responsive'>{items}</div>\n{pager}",
    'filter' => $showFilter,
    //    'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',
    // 'htmlOptions' => array("style" => "width:100%"),
    'htmlOptions' => array("class" => "width-100-percentage"),
    'pager' => array(
        'id' => 'dataTables-example_paginate', 'header' => '', 'firstPageLabel' => 'First ', 'lastPageLabel' => 'Last ', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next ', 'maxButtonCount' => 5,
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'rowCssClassExpression' => '($data->status == 5 || $data->status == 73 || $data->status == 74)?"bg_beige":((strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "bg_red": ( strtotime($data->due_date) <=  ' . $fut_date . ' && strtotime($data->due_date) >= strtotime(date("Y-m-d")) ? "bg_orange" : "bg_green"))',
    'columns' => array(
        array(
            'id' => 'selected_tasks',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        array(
            'class' => 'ButtonColumn',
            //'template' => '{view}{update}{subtask}{delete}',
            'template' => '{view}{update}{validation}{MR}{item_estimation}{delete}',
            'evaluateID' => true,
            'htmlOptions' => array("class" => "min-width-125"),
            'buttons' => array(
                'view' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View',),
                ),
                'update' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Edit'),
                    //  'visible'=>' Yii::app()->user->role==1',
                ),
                'subtask' => array(
                    'label' => '',
                    //'imageUrl' => Yii::app()->request->baseUrl . '/images/subtask.png',
                    'options' => array('class' => 'icon-docs icon-comn', 'title' => 'sub task',),
                    'url' => 'Yii::app()->createUrl("Tasks/subtask", array("tskid"=>$data->tskid))',
                ),
                'validation' => array(
                    'label' => '',
                    'options' => array(
                        'class' => 'fa fa-exclamation-triangle validation_msg hide red', 'title' => '',
                        'id' => '$data->tskid',
                    ),
                ),
                'MR' => array(
                    'label' => '',
                    'options' => array('class' => 'icon-docs icon-comn', 'title' => 'MR'),
                    'visible' => 'in_array("/materialRequisition/index", Yii::app()->session["menuauthlist"]) && $data->checkMRVisibility($data->project_id)==1',
                    'url' => 'Yii::app()->createUrl("MaterialRequisition/create", array("id"=>$data->tskid,"type"=>1))',
                ),
                'item_estimation' => array(
                    'label' => '',

                    'options' => array('class' => 'icon-book-open icon-comn', 'title' => 'Item Estimation'),
                    'visible' => 'in_array("/tasks/itemEstimation", Yii::app()->session["menuauthlist"]) && $data->checkTemplateAvailability($data->project_id)==1 && $data->accountPermission()==1',
                    'url' => 'Yii::app()->createUrl("tasks/itemEstimation", array("id"=>$data->tskid,"project_id"=>$data->project_id))',
                ),


                'delete' => array(
                    'label' => '',
                    'imageUrl' => false,

                    'visible' => "isset(Yii::app()->user->role) && (in_array('/tasks/delete', Yii::app()->session['menuauthlist']))",

                    'click' => 'function(e){e.preventDefault();deletetask($(this).attr("href"));}',
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                ),
            ),
        ),
        array(
            'name' => 'title',
            'value' => '$data->getTimeentrydescription($data->tskid)',
            'type' => 'raw',
            // 'cssClassExpression' => '$data->reportTo->user_type ==1 ? "text-danger1" : ""',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Tasks[title]',
                'source' => $this->createUrl('Tasks/autocomplete'),
                'value' => isset($model->title) ? $model->title : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) {
                       $("#Tasks_title").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Tasks_title").val(ui.item.value); }'
                ),
            ), true),
        ),
        array(
            'name' => 'project_id',
            'value' => '(empty($data->project_id)?"test":CHtml::link($data->project->name, array("Projects/view","id"=>$data->project_id)))',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($model, 'project_id', $projects_list)
        ),
        array(
            'name' => 'milestone_id',
            
            'value'=>'!empty($data->milestone_id)?$data->milestone->milestone_title:""',

            'filter' => CHtml::activeDropDownList($model, 'milestone_id', $milestone_list)
        ),
        array(
            'name' => 'assigned_to',
            'value' => '!empty($data->assigned_to)?$data->assignedTo->first_name." ".$data->assignedTo->last_name:""',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($model, 'assigned_to', $assigned_to_list)
        ),
        array(
            'name' => 'coordinator',
            'value' => '!empty($data->coordinator)?$data->coordinator0->first_name." ".$data->coordinator0->last_name:""',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($model, 'coordinator', $co_ordinator_list)
        ),
        array(
            'name' => 'quantity',
            'value' => function ($model) {
                if (!empty($model->unit)) {
                    return $model->quantity . ' (' . $model->unit0->unit_title . ')';
                } else {
                    return $model->quantity;
                }
            }
        ),
        array(
            'name' => 'progress_percent',
            'value' => '$data->getcompleted_percent($data->tskid)',
            'type' => 'html',
            // 'htmlOptions' => array( 'style' => 'width:15px;border-right:1px solid #c2c2c2;'),
            'headerHtmlOptions' => array('class' => 'wrap'),
        ),
        array(
            'name' => 'task_duration',
            // 'value' => '!empty($data->task_duration)?$data->task_duration . " days":NULL',
            'value' => function ($model) {

                return $this->getTaskDuration($model->start_date, $model->due_date, $model->project_id, $model->clientsite_id);
            },
            'type' => 'html',
            'htmlOptions' => array('class' => 'date_duration'),
        ),
        'daily_target',
        array(
            'name' => 'work_type_id',
            'value' => '!empty($data->work_type_id)?$data->worktype->work_type:$data->work_type_id',
            'type' => 'raw',
        ),
        'required_workers',
        //'start_date',
        array(
            'name' => 'start_date',
            'value' => 'date("d-M-y",strtotime($data->start_date))',
            'type' => 'html',
            'htmlOptions' => array('class' => 'date_start')
        ),
        array(
            'name' => 'due_date',
            'value' => 'date("d-M-y",strtotime($data->due_date))',
            'type' => 'html',
            'htmlOptions' => array('class' => 'date_end'),
            'cssClassExpression' => '(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "text-danger1" : ""',
        ),
        array(
            'name' => 'status',
            'header' => 'Status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($model, 'status', $status_list)
        ),
        array(
            'name' => 'task_type',
            'value' => '($data->task_type ==1)?"External":"Internal"',
            'filter' => array('' => 'All', 1 => 'External', 2 => 'Internal'),
            //'type'=>'raw',
        ),
        array(
            'name' => 'report_to',
            'value' => '!empty($data->report_to)?$data->reportTo->first_name." ".$data->reportTo->last_name:""',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($model, 'report_to', $task_owner_list),
        ),
        array(
            'name' => 'contractor_id',
            'value' => '!empty($data->contractor_id)?$data->contractor->contractor_title:""',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($model, 'contractor_id', $contractor_list),
        ),

       
    ),
);


$gridwidget = 'zii.widgets.grid.CGridView';
if (isset($_GET['exportgrid'])) {
    $gridwidget = 'application.core.grid.CGridView';
    // unset($gridcolumns[0]);   //Serial number column remove for export
    $export_grid_column_label = array('S.NO.', "TITLE", "PROJECT", "ASSIGNED TO", "COORDINATOR", "QUANTITY", "PROGRESS %", "TASK DURATION", "DAILY TARGET", "WORK TYPE", "REQUIRED WORKERS", "START DATE", "END DATE", "STATUS", "TASK TYPE", "TASK OWNER", "CONTRACTOR");

    $grid_params_array['export_grid_column_label'] = $export_grid_column_label;
    $grid_params_array['export_file_name'] = 'Assigned task report_' . date('d_M_Y-his') . ".csv";
}


$this->widget($gridwidget, $grid_params_array);
    ?>
    <?php
    if (!isset($_GET['exportgrid'])) {
    ?>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
        <script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>



        <?php
        $alltasks = Tasks::model()->findAll();
        $newarr = array();
        foreach ($alltasks as $data) {
            $newarr[] = $data['title'];
        }
        $alltasklist = json_encode($newarr);
        ?>


        <?php
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id' => 'cru-dialog',
            'options' => array(
                'title' => 'Add Task',
                'autoOpen' => false,
                'modal' => false,
                'width' => "590",
                'height' => "auto",
            ),
        ));
        ?>
        <iframe id="cru-frame" width="550" height="550" class="min-height-500"></iframe>

        <?php
        $this->endWidget();
        ?>
        <div id="id_view"></div>

        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
  

    $( function() {       
         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });


  } );

');

        ?>
    </div>
    <script>
        $(document).ready(function() {
            // $('.js-example-basic-single').select2();
        });
        $(document).delegate('#tasks-grid input[type=checkbox]', 'click', function() {
            if ($("#tasks-grid").find("input:checked").length > 0) {
                $(".filter-form").show();
            } else {
                $(".filter-form").hide();
            }

        });
        $(document).ready(function() {
            $(document).on('click', 'ul.box_sec li', function() {
                console.log('checked');
                $(this).children('div').children('span').toggleClass('checked');
                $(this).siblings().children('div').children('span').removeClass('checked');

            });

            $(document).on('click', 'ul.change_type li', function() {
                console.log('checked');
                $(this).children('div').children('span').toggleClass('checked');
                $(this).siblings().children('div').children('span').removeClass('checked');

            });

            $(document).on('click', 'ul.date_type li', function() {
                console.log('checked');
                $(this).children('div').children('span').toggleClass('checked');
                $(this).siblings().children('div').children('span').removeClass('checked');

            });

            $(".popover-test").popover({
                html: true,
                content: function() {
                    return $(this).next('.popover-content').html();
                }
            });
            //$('.status_change_form').hide();
            $('.filter-form').hide();
            // $('.assignee_change_form').hide();
            $('#list_change').on('change.bootstrap', function(e) {
                var status = e.target.checked;
                if (this.checked) {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: "all_tasks"
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: ""
                        },
                    });
                }
            });
            $('#list_change_closed').on('change.bootstrapSwitch', function(e) {
                var status = e.target.checked;
                if (this.checked) {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type_closed: "closed"
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type_closed: ""
                        },
                    });
                }
            });
            $('#list_change_main_tasks').on('change.bootstrapSwitch', function(e) {
                var status = e.target.checked;
                if (this.checked) {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: "main_tasks"
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: ""
                        },
                    });
                }
            });

            $('#list_change_sub_tasks').on('change.bootstrapSwitch', function(e) {
                var status = e.target.checked;
                if (this.checked) {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: "sub_tasks"
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: ""
                        },
                    });
                }
            });

            $('#list_change_expired_tasks').on('change.bootstrapSwitch', function(e) {
                var status = e.target.checked;
                if (this.checked) {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: "expired_tasks"
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: ""
                        },
                    });
                }
            });


            $('#serachbtn').on('click.bootstrapSwitch', function(e) {

                var filter_start_date = $('#Tasks_start_date').val();
                var filter_due_date = $('#Tasks_due_date').val();

                if (filter_start_date != "" && filter_due_date != "") {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            filter_start_date: filter_start_date,
                            filter_due_date: filter_due_date
                        },
                    });
                }
            });

            $('#cancel-btn').on('click.bootstrapSwitch', function(e) {

                $.fn.yiiGridView.update('tasks-grid', {
                    data: {
                        filter_start_date: "",
                        filter_due_date: ""
                    },
                });

            });




            $('#success_message').hide();
            $('#success_message_id').hide();

        });


        $('#selected_tasks_all').click(function() {
            var checkboxes = document.getElementsByTagName('input');
            if (this.checked) {
                //$('.status_change_form').show();
                $('.filter-form').show();
                //$('.assignee_change_form').show();
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                        $('#tasks-grid div.checker span').addClass('checked');
                    }
                }
            } else {
                $('.filter-form').hide();
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                        // $('div.checker span').removeClass('checked');
                        $('#tasks-grid div.checker span').removeClass('checked');
                    }
                }
            }
        });
        $('input[name="selected_tasks[]"]').click(function() {
            if (this.checked) {
                // $('.status_change_form').show();
                // $('.filter-form').slideDown(); 
                // $('.assignee_change_form').show();
                $('.filter-form').show();

            } else {
                var all = [];
                $('.filter-form').hide();
                $("#selected_tasks_all").prop('checked', false);
                $('div#uniform-selected_tasks_all span').removeClass('checked');

            }

        });

        $(document).on('click', '.apply', function() {

            var change_type = $(this).parent().siblings().children('div.change_type').find('button.active').attr('data-id');
            var changed_value = $(this).parent().siblings().children('div.change_value').children('input').val();
            var date_type = $(this).parent().siblings().children('div.date_type').find('button.active').attr('data-id');
            var all = [];

            $('input[name="selected_tasks[]"]:checked').each(function() {
                all.push(this.value);
            });
            if (all != '') {
                if (changed_value == '') {
                    alert("Please add details");
                } else {
                    if (!confirm('Are you sure, want to change the date?') === false) {
                        $('.loaderdiv').show();
                        $.ajax({
                            method: "post",
                            dataType: "json",
                            data: {
                                id: all,
                                change_type: change_type,
                                changed_value: changed_value,
                                date_type: date_type
                            },
                            url: '<?php echo Yii::app()->createUrl("/tasks/changedate") ?>',
                            success: function(ret) {
                                $('.loaderdiv').hide();
                                $.fn.yiiGridView.update('tasks-grid');
                                // if(ret['change_type'] == 2){
                                $('.validation_msg').each(function() {
                                    var task_id = this.id;
                                    if (typeof(ret[task_id]) != "undefined" && ret[task_id] !== null) {
                                        if (ret[task_id]['task_id'] == task_id) {
                                            if (ret[task_id]['task_id'] == task_id) {
                                                if (ret[task_id]['start_date'] != "") {
                                                    $(this).closest("tr").find("td.date_start").text(ret[task_id]['start_date']);
                                                }
                                                if (ret[task_id]['duration'] != "" && Math.floor(ret[task_id]['duration']) == ret[task_id]['duration']) {
                                                    $(this).closest("tr").find("td.date_duration").text(ret[task_id]['duration'] + ' days');
                                                }
                                                if (ret[task_id]['due_date'] != "") {
                                                    $(this).closest("tr").find("td.date_end").text(ret[task_id]['due_date']);
                                                }
                                                if (ret[task_id]['msg'] != "" && ret[task_id]['msg'] != undefined) {
                                                    $(this).closest("tr").find("td .validation_msg").removeClass('hide');
                                                    $(this).closest("tr").find("td .validation_msg").attr('title', ret[task_id]['msg']);
                                                    if (ret[task_id]['msg'] != '') {
                                                        alert(ret[task_id]['msg']);
                                                    }
                                                } else {
                                                    $(this).closest("tr").find("td .validation_msg").addClass('hide');
                                                }
                                            }
                                        }
                                    }

                                });
                            },
                            error: function() {
                                alert('Error');
                                $('.loaderdiv').hide();
                            }
                        });
                    } else {
                        $('.loaderdiv').hide();
                        $('.apply').attr('disabled', false);
                    }
                }
            } else {

                alert('Please select Item');
                $('.apply').attr('disabled', false);

            }
        });
        $(document).on('click', '.change_status', function() {
            // var status_val = $('#status_change').val();
            var status_val = $(this).parent().siblings('ul').find('span.checked').children('input').val();
            if (status_val === "" || status_val == undefined) {
                alert('Please Select status');
            } else {
                var selected_taskids = [];
                $("input[name='selected_tasks[]']:checked").each(function(i) {
                    selected_taskids[i] = $(this).val();
                });
                var description = $('textarea#status_description').val();
                if (selected_taskids.length === 0) {
                    alert('Please select a Task');
                } else {
                    $.ajax({
                        method: "post",
                        dataType: "json",
                        data: {
                            status_id: status_val,
                            task_ids: selected_taskids,
                            description: description
                        },
                        url: '<?php echo Yii::app()->createUrl("/tasks/changestatus") ?>',
                        success: function(result) {
                            if (result.status == 1) {
                                $("#success_message").fadeIn().delay(1000).fadeOut();
                                $.fn.yiiGridView.update('tasks-grid');
                                $('.success_data').removeClass('hide');
                            }
                        }
                    });
                }
            }
        });
        $(document).on('click', '.change_assignee', function() {
            //$('.change_assignee').click(function() {
            var assignee_id = $('#assigne_change').val();
            if (assignee_id === "") {
                alert('Please Choose an assignee');
            } else {
                var selected_taskids = [];
                $("input[name='selected_tasks[]']:checked").each(function(i) {
                    selected_taskids[i] = $(this).val();
                });
                if (selected_taskids.length === 0) {
                    alert('Please select a Task');
                } else {
                    $('.loaderdiv').show();
                    $.ajax({
                        method: "post",
                        dataType: "json",
                        data: {
                            assignee_id: assignee_id,
                            task_ids: selected_taskids
                        },
                        url: '<?php echo Yii::app()->createUrl("/tasks/changeAssignee") ?>',
                        success: function(ret) {

                            $('.loaderdiv').hide();
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            var show_alert = 0;
                            $('.validation_msg').each(function() {
                                var task_id = this.id;
                                if (typeof(ret[task_id]) != "undefined" && ret[task_id] !== null) {

                                    if (ret[task_id]['task_id'] == task_id) {
                                        if (ret[task_id]['task_id'] == task_id) {
                                            if (ret[task_id]['assigned_to'] != "") {
                                                $(this).closest("tr").find("td:eq(3)").text(ret[task_id]['assigned_to']);
                                            }

                                            if (ret[task_id]['msg'] != "" && ret[task_id]['msg'] != undefined) {
                                                $(this).closest("tr").find("td .validation_msg").removeClass('hide');
                                                $(this).closest("tr").find("td .validation_msg").attr('title', ret[task_id]['msg']);
                                                if (show_alert == 0 && ret[task_id]['msg'] != '') {
                                                    if (show_alert == 0) {
                                                        alert(ret[task_id]['msg']);
                                                        show_alert++;
                                                    }
                                                }
                                            } else {
                                                $(this).closest("tr").find("td .validation_msg").addClass('hide');
                                            }
                                        }
                                    }
                                }

                            });

                            setTimeout(function() {
                                window.location.reload();
                            }, 1000);
                        }
                    });
                }
            }
        });

        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

        $(document).on('click', '.btn-group button', function() {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
        })

        // Concept: Render select2 fields after all javascript has finished loading 

        var initSelect2 = function() {
            // function that will initialize the select2 plugin, to be triggered later 
            var renderSelect = function() {

                $('#change_assign').click(function() {
                    $("#assigne_change").select2({
                        width: "100%"
                    });
                });

            };
            var style = document.createElement('link');
            var script = document.createElement('script');
            style.rel = 'stylesheet';
            style.href = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css';
            script.type = 'text/javascript';
            script.src = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.full.min.js';

            script.onload = renderSelect;
            // render the style and script tags into the DOM 
            document.getElementsByTagName('head')[0].appendChild(style);
            document.getElementsByTagName('head')[0].appendChild(script);
        };
        initSelect2();
    </script>
<?php
    }
?>
<script>
    function deletetask(href) {
        var id = getURLParameter(href, 'id');

        var answer = confirm("Are you sure you want to delete?");
        var url = "<?php echo $this->createUrl('tasks/delete&id=') ?>" + id;
        if (answer) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                success: function(response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                        $(".alert-success").html(response.msg).css("display","").delay(3000).fadeOut();
                    } else if (response.response == "warning") {
                        $(".alert-warning").html(response.msg).css("display","").delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").html(response.msg).css("display","").delay(3000).fadeOut();
                    }
                    $("#tasks-grid").load(location.href + " #tasks-grid");


                }
            });
        }


    }

    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
    }

    $('#view_search').hide();
    $(document).on('click', '#search_by_date', function() {
        $('#view_search').show();
    })

    $(function() {
        $("#Tasks_start_date").attr("autocomplete", "off");
        $("#Tasks_due_date").attr("autocomplete", "off");
        $("#Tasks_start_date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd-M-y',


        });
        $("#Tasks_due_date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd-M-y',

        });
    });

    var filter_start_date = "<?php echo isset($_GET['filter_start_date']) ? $_GET['filter_start_date'] : "" ?>";


    var filter_due_date = "<?php echo isset($_GET['filter_due_date']) ? $_GET['filter_due_date'] : "" ?>";

    if (filter_start_date != "" && filter_due_date != "") {
        $('#view_search').show();
        $("#Tasks_start_date").val(filter_start_date);
        $("#Tasks_due_date").val(filter_due_date);
    }
</script>