<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css');
Yii::app()->clientScript->registerScript('myscript', "
$(function() {
$('.select_drop').select2();
});
");
?>
<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs = array(
    'Time Entries' => array('index'),
    'Manage',
);
?>

<h1>Manage Service Report</h1>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('timereport-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div><?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?></div>
<div class="search-form">
<?php $this->renderPartial('_searchreport',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'timereport-grid',
    'dataProvider'=> $model->reportsearch(),
   // 'filter'=>$model,
      
     'itemsCssClass' => 'table table-bordered',
     'template' => '<div class="table-responsive margin-top-10">{items}</div>',
    
    'columns'=>array(
       array('class' => 'IndexColumn', 'header' => 'S.No.',),
		/*
         array(
                'header'=>'Sl.No.',
                'value'=>'$this->grid->dataProvider->pagination->offset + $row+1',       //  row is zero based
        ),
      
        array(
            'header' => 'Sl No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ), */
        array('name'=>'project','header'=>'Project'),           
        array('name'=>'task','header'=>'Task'),        
        array('name'=>'work_type','header'=>'Work Type','filter' => CHtml::listData(WorkType::model()->findAll(
                            array(
                                'select' => array('wtid,work_type'),
                                'order' => 'work_type',
                                'distinct' => true
                    )), "wtid", "work_type"), 'visible'=>Yii::app()->user->role!=3),
        array('name'=>'description','header'=>'Description'),
        array('name'=>'site_name','header'=>'Location'),
        'entry_date',
       
        /* array(
        'name'=>'entry_date',
        'header'=>'Entry Date',
        'value' => 'date("d-M-y",strtotime($data->entry_date))',
        ), */
       // array('name'=>'entry_date','header'=>'Entry Date','filter'=>'<h4 style=\'margin-bottom:0px;background-color:#E5B02B;\'><b>Total:</b></h4>','footer'=>'<h4 style=\'margin-bottom:0px;background-color:#E5B02B;\'><b>Total:</b></h4>'),
       // array('name'=>'hours','header'=>'Hours', 'type'=>'text','filter'=>"<h4 style='margin-bottom:0px;background-color:#E5B02B;'>".$model->getTotals($model->search()->getKeys())."</h4>", 'footer'=>"<h4 style='margin-bottom:0px;background-color:#E5B02B;'>".$model->getTotals($model->search()->getKeys())."</h4>"),
       /* array('name'=>'billable', 'type'=>'html',          
            'value' => '$data["billable"]=="Yes"?"<span style=\"font-size:25px;color:blue;\">&#x2714;</span>":"&#x2716;"',
            'htmlOptions'=>array('style' => 'text-align: center;font-weight:bold;font-size:20px;'),
            'header'=>'Billable','filter'=>array('Yes'=>'Billable','No'=>'Non-Billable')), */
        array('name'=>'username', 'type'=>'raw','header'=>'Resource','filter'=>CHtml::listData(Users::model()->findAll(
                                array(
                                    'select' => array('userid,first_name'),
                                    'condition'=>'status=0',
                                    'order' => 'first_name',
                                    'distinct' => true
                        )), "userid", "first_name"),'visible'=>Yii::app()->user->role!=3)
        ),
));
?>

</div>
</div>
