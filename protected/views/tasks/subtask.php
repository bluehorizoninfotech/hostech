<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
/* @var $this TasksController */
/* @var $model Tasks */
/* @var $form CActiveForm */
?>




<?php

?>
<div class="subtask-sec">
<div class="form margin-top-5">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tasks-form',

        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>

    <?php //echo $form->errorSummary($model);?>


    <div class="row">



            <div class="subrow subrowlong">
              <?php echo $form->labelEx($model, 'Main Task'); ?>
               <?php
               $condition = '';

if(Yii::app()->user->role != 1){


   /* $tblpx = Yii::app()->db->tablePrefix;

    $sqlp = Yii::app()->db->createCommand("SELECT GROUP_CONCAT(CONCAT('''', cs.pid, '''' )) as pid FROM {$tblpx}clientsite cs JOIN {$tblpx}clientsite_assigned ca ON ca.site_id = cs.id WHERE ca.user_id = '".Yii::app()->user->id."'")->queryRow();

    if($sqlp['pid']){
       $condition = 'pid IN ('.$sqlp['pid'].')';
    }else{
        $condition = '1!=1';
    }*/

    $proarr = array();
            $proarr1 = 0;
            $allprojects = Tasks::model()->findAll('assigned_to = '.Yii::app()->user->id);

            if(!empty($allprojects)){
               foreach ($allprojects as $key => $value) {
                    if($value['project_id']!=''){
                       $proarr[]=$value['project_id'];
                   }
              }
            }

            if(!empty($proarr)){
               $proarr1 = implode(',', $proarr);
           }else{
             $proarr1 = 0;
           }

           $condition = ' project_id in ('.$proarr1.') AND assigned_to ='. Yii::app()->user->id.' or coordinator='.Yii::app()->user->id .' or created_by='.Yii::app()->user->id;
}


          //echo $condition;exit;

//               if(yii::app()->user->role != 1){
//                 $condition= 'assigned_to ='. Yii::app()->user->id .' or coordinator='.Yii::app()->user->id .' or created_by='.Yii::app()->user->id;
//                   }else{
//                     $condition = "";
//                   }
                if(!empty($_GET['tskid'])){
                    $model->parent_tskid = $_GET['tskid'];
                }
                echo $form->dropDownList($model, 'parent_tskid', CHtml::listData(Tasks::model()->findAll(
                    array('condition' =>$condition, 'order' => 'title ASC' )
                    ), 'tskid', 'title'), array('empty' => '--', 'id'=>'main_task', 'style' => 'width:100%;','ajax' => array
                        (
                        'type'=>'POST',
                        'url'=>CController::createUrl('Tasks/getprojectbytaskid'), //or $this->createUrl('loadcities') if '$this' extends CController
                        'update'=>'#Tasks_project_id', //or 'success' => 'function(data){...handle the data in the way you want...}',
                        'data'=>array('pid'=>'js:this.value'),
                        )));
               ?>
               <?php echo $form->error($model, 'parent_tskid'); ?>
            </div>
    </div>

    <div class="row">
        <div class="subrow subrowlong">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title', array('class'=>'form-control')); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>
    </div>
 <div class="row">

	<?php
	if(Yii::app()->user->role==1 || Yii::app()->user->role==3)
		$display='block';
	else
		$display="none";
	?>
		<?php
		?>

        <div class="subrow" style="display:<?php echo $display;?>">


            <?php  echo $form->labelEx($model, 'project_id'); ?>
            <?php //  echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'), array('empty' => 'Choose a project','class'=>'form-control input-medium project')); ?>
           <?php echo $form->hiddenField($model, 'project_id', array('class'=>'form-control')); ?>


        <?php
		echo $form->dropDownList($model,'project_id',CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
                    array(
                        'empty' => 'Choose a project',
                        'class'=>'form-control input-medium project',
                        'ajax' => array
                        (
                        'type'=>'POST',
                        'url'=>CController::createUrl('Tasks/getclientlocation'), //or $this->createUrl('loadcities') if '$this' extends CController
                        'update'=>'#Tasks_clientsite_id', //or 'success' => 'function(data){...handle the data in the way you want...}',
                        'data'=>array('pid'=>'js:this.value'),
                        )));
                ?>
             <?php  echo $form->error($model, 'project_id'); ?>
        </div>


        <div class="subrow" style="display:<?php echo $display;?>">


		 <?php // echo $form->dropDownList($model,'clientsite_id',array('class'=>'form-control input-medium',)); ?>
		            <?php  echo $form->labelEx($model, 'clientsite_id'); ?>

            <?php
            if(empty($location) && $location == 'create'){
            ?>
             <?php
		echo $form->dropDownList($model,'clientsite_id',CHtml::listData(Clientsite::model()->findAll(array('order' => 'site_name ASC')), 'id', 'site_name'),
                    array(
                        'empty' => 'Choose a client site',
                        'class'=>'form-control input-medium',));

					} else {
                ?>

                <?php
                echo $form->dropDownList($model,'clientsite_id',CHtml::listData($location, 'id', 'site_name'),
                    array(
                        'empty' => 'Choose a client site',
                        'class'=>'form-control input-medium',
                        'ajax' => array
                        (
                        'type'=>'GET',
                        'url'=>CController::createUrl('Tasks/getassignedperson'), //or $this->createUrl('loadcities') if '$this' extends CController
                        'update'=>'#Tasks_assigned_to', //or 'success' => 'function(data){...handle the data in the way you want...}',
                        'data'=>array('id'=>'js:this.value'),
                        )

                        ));
                ?>

                <?php } ?>


             <?php  echo $form->error($model, 'clientsite_id'); ?>
        </div>


        <div class="subrow">
            <?php echo $form->labelEx($model, 'priority' ); ?>
            <?php
            echo $form->dropDownList($model, 'priority', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="priority"',
                                'order' => 'caption',
                                'distinct' => true
                            )), 'sid', 'caption'), array('empty' => 'Choose a priority','class'=>'form-control input-medium'));
            ?>
            <?php echo $form->error($model, 'priority'); ?>
        </div>
    </div>


		<div class="row">
                    <div class="subrow">
		    <?php
//Default value for start and due date

    if (!isset($model->start_date))
        $model->start_date = date('d-M-y');

    if (!isset($model->due_date)) {
        $cd = strtotime(date('d-M-y'));
        $model->due_date = date('d-M-y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + 7, date('Y', $cd)));
    }
    $model->start_date = date('d-M-y',strtotime($model->start_date));
    $model->due_date = date('d-M-y',strtotime($model->due_date));
    ?>


            <?php echo $form->labelEx($model, 'start_date'); ?>
            <?php echo CHtml::activeTextField($model, 'start_date', array('class'=>'form-control input-small start_date',"id" => "Tasks_start_date" )); ?>
            <?php
            // $this->widget('application.extensions.calendar.SCalendar', array(
            //     'inputField' => 'Tasks_start_date',
            //     'ifFormat' => '%Y-%m-%d',
            // ));
            ?>
            <?php echo $form->error($model, 'start_date'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'due_date'); ?>
            <?php echo CHtml::activeTextField($model, 'due_date', array('class'=>'form-control input-small due_date',"id" => "due_date",'size'=>10)); ?>
            <?php
            // $this->widget('application.extensions.calendar.SCalendar', array(
            //     'inputField' => 'due_date',
            //     'ifFormat' => '%Y-%m-%d',
            // ));
            ?>
            <?php echo $form->error($model, 'due_date'); ?>
        </div>

    </div>


    <div class="row">

            <?php
            $co_toggle = '';
            $rep_toggle = '';
            if (Yii::app()->user->role == 6) {
                if (!isset($model->coordinator)) {
                    $model->coordinator = Yii::app()->user->id;
                    $co_toggle = 'toggle-field';
                }
            } else {
                if (!isset($model->report_to)) {
                    $model->report_to = Yii::app()->user->id;
                    $rep_toggle = 'toggle-field';
                }
            }
            ?>
        <div class="subrow">
            <?php echo $form->labelEx($model, 'assigned_to'); ?>
            <?php
				if(Yii::app()->user->role==1) {
            ?>

            <?php

            if ($model->isNewRecord)
            {
				$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => "status=0",'order' => 'first_name ASC');
			}else {
				$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'order' => 'first_name ASC');
			}





			 if (empty($assigned_to)) {?>

                 <span class="report_assign">
               <?php  echo $form->dropDownList($model,'assigned_to',CHtml::listData(Users::model()->findAll($condition), 'userid', 'full_name'),
				array(
					'empty' => 'Choose a resource',
					'class'=>'form-control input-medium',
					));

                        ?>
                        </span>
                       <!-- <select class="form-control input-medium" name="Tasks[assigned_to]" id="Tasks_assigned_to">
						<option value="">Choose a resource</option>
						</select> -->
                        <?php


             } else {     ?>
               <span class="report_assign">

				<?php	echo $form->dropDownList($model,'assigned_to',CHtml::listData($assigned_to, 'userid', 'whole_name'),
                    array(
                        'empty' => 'Choose a resource',
                        'class'=>'form-control input-medium',
                        ));

		    }


            ?>
            </span>
			<?php } else { ?>

            <?php
            $gpmem = array();
            $allprojects = Tasks::model()->findAll('created_by = '.Yii::app()->user->id.' OR assigned_to = '.Yii::app()->user->id.' OR coordinator = '.Yii::app()->user->id.' OR report_to = '.Yii::app()->user->id);

            if(!empty($allprojects)){
               foreach ($allprojects as $key => $value) {
                    if($value['assigned_to']!=''){
                       $gpmem[]=$value['assigned_to'];
                   }
              }
            }






            $con = '1=1';
            if(Yii::app()->user->role == 2 || Yii::app()->user->role == 4){
            $members = Groups::model()->findAll(
                     array(
                         'select' => 'group_id',
                         'condition' => 'group_lead=' . Yii::app()->user->id,
                ));
                foreach ($members as $data){
                    $list = Groups_members::model()->findAll(
                         array(
                             'select' => 'group_id,group_members,id',
                             'condition' => 'group_id=' . $data['group_id'],
                    ));
                    foreach($list as $groupsmem) {
                        $gpmem[] = $groupsmem['group_members'];
                    }
                }
                $gpmem[] = Yii::app()->user->id;
            $gpmem = implode(',', $gpmem);
            $con = "userid in(".$gpmem. ")";
            }else{
                $con = "userid = ".Yii::app()->user->id;
            }
            if ($model->isNewRecord)
					$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => "status=0 AND $con",'order' => 'first_name ASC');
					else
					$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => "$con",'order' => 'first_name ASC'); ?>
            <span class="report_assign">

			<?php echo $form->dropDownList($model, 'assigned_to', CHtml::listData(Users::model()->findAll($condition), 'userid', 'full_name'), array('empty' => '-Choose a resource-','class'=>'form-control input-medium')); ?>

			<?php } ?>
            </span>
            <?php  echo $form->error($model, 'assigned_to'); ?>
        </div>

		        <div class="subrow<?php echo $co_toggle; ?>">

            <?php echo $form->labelEx($model, 'coordinator'); ?>
            <span class="report_coordinator">
            <?php echo $form->dropDownList($model, 'coordinator', CHtml::listData(Users::model()->findAll(array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0','order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => '-Choose co-ordinator-','class'=>'form-control input-medium width-466')); ?>
            </span>
            <?php echo $form->error($model, 'coordinator'); ?>

        </div>
    </div>

    <div class="row">
		 <div class="subrow<?php //echo $rep_toggle; ?>">
            <?php echo  $form->labelEx($model, 'report_to'); ?>
            <span class="report_owner">
            <?php


             echo $form->dropDownList($model, 'report_to', CHtml::listData(Users::model()->findAll(array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0','order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => '-Choose a reporting person-','class'=>'form-control input-medium width-466'));
            ?>
            </span>
            <?php echo $form->error($model, 'report_to'); ?>
        </div>


		 <div class="subrow">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php
            $consts = true;
           if ($model->coordinator == Yii::app()->user->id || Yii::app()->user->role == 1) {
               $consts = false;
           }
            echo $form->dropDownList($model, 'status', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="task_status"',
                                'order' => 'caption',
                                'distinct' => true
                            )), 'sid', 'caption'), array('class'=>'form-control input-medium','empty' => 'Choose a status','disabled' => $consts));
            ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>



    </div>


        <!--div class="row">
         <div class="subrow">
            <?php //echo $form->labelEx($model, 'hourly_rate'); ?>
            <?php //echo $form->textField($model, 'hourly_rate', array('class'=>'form-control input-small')); ?>
            <?php //echo $form->error($model, 'hourly_rate'); ?>
        </div>

        <div class="subrow">
            <?php //echo $form->labelEx($model, 'billable'); ?>
            <div class="radio_btn">
                <?php
                /* echo $form->radioButtonList($model, 'billable', CHtml::listData(Status::model()->findAll(
                                        array(
                                            'select' => array('sid,caption'),
                                            'condition' => 'status_type="yesno"',
                                            'order' => 'caption',
                                            'distinct' => true
                                )), 'sid', 'caption'), array( 'labelOptions'=>array('style'=>'display:inline'),'separator' => '')); */
                ?>
             </div>
            <?php //echo $form->error($model, 'billable'); ?>
      </div>
      </div-->
    <!--div class="row">
        <div class="subrow">
            <?php //echo $form->labelEx($model, 'total_hrs'); ?>
            <?php //echo $form->textField($model, 'total_hrs', array('class'=>'form-control input-small','size'=>10)); ?>
            <?php //echo $form->error($model, 'total_hrs'); ?>
        </div>

        <div class="subrow">
            <?php //echo $form->labelEx($model, 'min_rate'); ?>
            <?php //echo $form->textField($model, 'min_rate', array('class'=>'form-control input-small','size'=>10)); ?>
            <?php //echo $form->error($model, 'min_rate'); ?>
        </div>

    </div-->
    <div class="row">
    <div class="subrow subrowlong">

        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 3, 'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>
    </div>

    <div class="form-group">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn blue')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
    </div>

    <span id="change_user_report" class="display-none">
     <?php
     $condition11 = '1=1';
              $gpmem = array();
        if(Yii::app()->user->role > 1 && Yii::app()->user->role <= 4){
            $members = Groups::model()->findAll(
                     array(
                         'select' => 'group_id',
                         'condition' => 'group_lead=' . Yii::app()->user->id,
                ));
                foreach ($members as $data){
                    $list = Groups_members::model()->findAll(
                         array(
                             'select' => 'group_id,group_members,id',
                             'condition' => 'group_id=' . $data['group_id'],
                    ));
                    foreach($list as $groupsmem) {
                        $gpmem[] = $groupsmem['group_members'];
                    }
                }
            $gpmem = implode(',', $gpmem);
            if(!empty($gpmem)){
              $condition11 = '(userid in('.$gpmem.') OR userid='.Yii::app()->user->id.')';
            }
        }elseif(Yii::app()->user->role > 4){
            $condition11 = 'userid='.Yii::app()->user->id;
        }
     $model->report_to = yii::app()->user->id;
     echo $form->dropDownList($model, 'report_to', CHtml::listData(Users::model()->findAll(array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => "status=0 AND userid = ".Yii::app()->user->id,'order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => '-Choose a person-','class'=>'form-control input-medium width-466',  ));
     ?>


    </span>

    <span id="change_user_assign" class="display-none">
    <?php

    $model->assigned_to = yii::app()->user->id;
     echo $form->dropDownList($model, 'assigned_to', CHtml::listData(Users::model()->findAll(array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => "status=0 AND $condition11",'order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => '-Choose a person-','class'=>'form-control input-medium width-466')); ?>


    </span>


    <span id="change_user_coordinator" class="display-none">
    <?php


    $model->coordinator = yii::app()->user->id;
     echo $form->dropDownList($model, 'coordinator', CHtml::listData(Users::model()->findAll(array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0 and userid = '. yii::app()->user->id ,'order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => '-Choose a person-','class'=>'form-control input-medium width-466')); ?>


    </span>
    <input type="hidden" name="role1" id="role1" value="<?= yii::app()->user->role; ?>">


    <?php $this->endWidget(); ?>

</div><!-- form -->
    </div>
<script type="text/javascript">
   $(document).ready(function(){
    $('.due_date').datepicker({
        dateFormat: 'd-M-y',
    });
    $('.start_date').datepicker({
        dateFormat: 'd-M-y',
    });

       var role =  $('#role1').val();
       var data =  $("#main_task").val();
       var report =  $('.report_owner').html();
       var assign = $('.report_assign').html();
       var coordinator = $('.report_coordinator').html();
       var change_report = $("#change_user_report").html();
       var change_assign = $("#change_user_assign").html();
       var change_coordinator = $("#change_user_coordinator").html();

        if(data == ''){


                $('.report_owner').html('');
                $('.report_assign').html('');
                $('.report_coordinator').html('');

                $('.report_owner').html(change_report);
                $('.report_assign').html(change_assign);
                $('.report_coordinator').html(change_coordinator);

        }

                $("#change_user_report").html('');
                $("#change_user_assign").html('');
                $("#change_user_coordinator").html('');

        $("#main_task").change(function() {

         var content = $("#main_task option:selected").text();
         if(content == '--'){

            if(role!= 1){

                $('.report_owner').html('');
                $('.report_assign').html('');
                $('.report_coordinator').html('');

                $('.report_owner').html(change_report);
                $('.report_assign').html(change_assign);
                $('.report_coordinator').html(change_coordinator);

            }


        }else{

                $('.report_owner').html(report);
                $('.report_assign').html(assign);
                $('.report_coordinator').html(coordinator);

        }

                $("#change_user_report").html('');
                $("#change_user_assign").html('');
                $("#change_user_coordinator").html('');



      });
   });
</script>

<script>



/*$('.project').change(function(){
var val =$(this).val();
	alert(val);
	$.ajax({
		url: '<?php echo Yii::app()->createUrl('tasks/getlocation'); ?>',
		data:{project_id:val},
		dataType:'json',
		method: "GET",
		success: function(result) {
			 $('.clientsite').append(result);
		}

	})
});*/
</script>
