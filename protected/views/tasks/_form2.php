 <?php
    /* @var $this TasksController */
    /* @var $model Tasks */
    /* @var $form CActiveForm */
    ?>
<div class="form-two-sec">
 <div class="form margin-top-minus-20">

     <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'tasks-form',

            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,
            ),
        ));
        ?>

     <?php //echo $form->errorSummary($model); 
        ?>

     <div class="form-group">
         <?php echo $form->labelEx($model, 'title'); ?>
         <?php echo $form->textField($model, 'title', array('class' => 'form-control', 'size' => 67, 'maxlength' => 72,'class' => 'form-control width-466')); ?>
         <?php echo $form->error($model, 'title'); ?>
     </div>

     <div class="form-group">
         <?php
            if (Yii::app()->user->role == 1)
                $display = 'block';
            else
                $display = "none";
            ?>


         <div class="form-group" style="display:<?php echo $display; ?>">


             <?php echo $form->labelEx($model, 'project_id'); ?>
             <?php echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'), array('empty' => 'Choose a project','class' => 'form-control width-466')); ?>
             <?php echo $form->error($model, 'project_id'); ?>
         </div>

         <div class="form-group">
             <?php echo $form->labelEx($model, 'priority'); ?>
             <?php
                echo $form->dropDownList($model, 'priority', CHtml::listData(Status::model()->findAll(array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type="priority"',
                    'order' => 'caption',
                    'distinct' => true
                )), 'sid', 'caption'), array('empty' => 'Choose a priority','class' => 'form-control width-466'));
                ?>
             <?php echo $form->error($model, 'priority'); ?>
         </div>

         <div class="form-group">
             <?php
                //Default value for start and due date

                if (!isset($model->start_date))
                    $model->start_date = date('Y-m-d');

                if (!isset($model->due_date)) {
                    $cd = strtotime(date('Y-m-d'));
                    $model->due_date = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + 7, date('Y', $cd)));
                }
                ?>


             <?php echo $form->labelEx($model, 'start_date'); ?>
             <?php echo CHtml::activeTextField($model, 'start_date', array('class' => 'form-control', "id" => "start_date")); ?>
             <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'start_date',
                    'ifFormat' => '%Y-%m-%d',
                ));
                ?>
             <?php echo $form->error($model, 'start_date'); ?>
         </div>

         <div class="form-group">
             <?php echo $form->labelEx($model, 'due_date'); ?>
             <?php echo CHtml::activeTextField($model, 'due_date', array('class' => 'form-control', "id" => "due_date", 'size' => 10)); ?>
             <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'due_date',
                    'ifFormat' => '%Y-%m-%d',
                ));
                ?>
             <?php echo $form->error($model, 'due_date'); ?>
         </div>

     </div>

     <div class="form-group">

         <div class="form-group">
             <?php
                $co_toggle = '';
                $rep_toggle = '';
                if (Yii::app()->user->role == 6) {
                    if (!isset($model->coordinator)) {
                        $model->coordinator = Yii::app()->user->id;
                        $co_toggle = 'toggle-field';
                    }
                } else {
                    if (!isset($model->report_to)) {
                        $model->report_to = Yii::app()->user->id;
                        $rep_toggle = 'toggle-field';
                    }
                }
                ?>
             <?php echo $form->labelEx($model, 'assigned_to'); ?>


             <?php if ($model->isNewRecord)
                    $condition = array('condition' => 'status=0', 'order' => 'first_name ASC');
                else
                    $condition = array('order' => 'first_name ASC');

                echo $form->dropDownList($model, 'assigned_to', CHtml::listData(Users::model()->findAll($condition), 'userid', 'first_name'), array('empty' => '-Choose a resource-','class' => 'form-control width-466')); ?>
             <?php echo $form->error($model, 'assigned_to'); ?>
         </div>

         <div class="form-group<?php echo $co_toggle; ?>">

             <?php echo $form->labelEx($model, 'coordinator'); ?>
             <?php echo $form->dropDownList($model, 'coordinator', CHtml::listData(Users::model()->findAll(array('condition' => 'status=0', 'order' => 'first_name ASC')), 'userid', 'first_name'), array('empty' => '-Choose co-ordinator-','class' => 'form-control width-466')); ?>
             <?php echo $form->error($model, 'coordinator'); ?>

         </div>

         <div class="form-group <?php echo $rep_toggle; ?>">
             <?php echo $form->labelEx($model, 'report_to'); ?>
             <?php echo $form->dropDownList($model, 'report_to', CHtml::listData(Users::model()->findAll(array('condition' => 'status=0', 'order' => 'first_name ASC')), 'userid', 'first_name'), array('empty' => '-Choose a reporting person-','class' => 'form-control width-466')); ?>
             <?php echo $form->error($model, 'report_to'); ?>
         </div>

         <div class="form-group">
             <?php echo $form->labelEx($model, 'hourly_rate'); ?>
             <?php echo $form->textField($model, 'hourly_rate', array('class' => 'form-control')); ?>
             <?php echo $form->error($model, 'hourly_rate'); ?>
         </div>
     </div>

     <div class="form-group">
         <div class="form-group">
             <?php echo $form->labelEx($model, 'status'); ?>
             <?php
                echo $form->dropDownList($model, 'status', CHtml::listData(Status::model()->findAll(array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type="task_status"',
                    'order' => 'caption',
                    'distinct' => true
                )), 'sid', 'caption'), array('class' => 'form-control width-466', 'empty' => 'Choose a status'));
                ?>
             <?php echo $form->error($model, 'status'); ?>
         </div>

         <div class="form-group">
             <?php echo $form->labelEx($model, 'billable'); ?>
             <div class="radio_btn">
                 <?php
                    echo $form->radioButtonList($model, 'billable', CHtml::listData(Status::model()->findAll(
                        array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="yesno"',
                            'order' => 'caption',
                            'distinct' => true
                        )
                    ), 'sid', 'caption'), array('separator' => ''));
                    ?>
             </div>
             <?php echo $form->error($model, 'billable'); ?>
         </div>

         <div class="form-group">
             <?php echo $form->labelEx($model, 'total_hrs'); ?>
             <?php echo $form->textField($model, 'total_hrs', array('class' => 'form-control', 'size' => 10)); ?>
             <?php echo $form->error($model, 'total_hrs'); ?>
         </div>

         <div class="form-group">
             <?php echo $form->labelEx($model, 'min_rate'); ?>
             <?php echo $form->textField($model, 'min_rate', array('class' => 'form-control', 'size' => 10)); ?>
             <?php echo $form->error($model, 'min_rate'); ?>
         </div>
     </div>

     <div class="form-group">
         <?php echo $form->labelEx($model, 'description'); ?>
         <?php echo $form->textArea($model, 'description', array('rows' => 3, 'class' => 'form-control')); ?>
         <?php echo $form->error($model, 'description'); ?>
     </div>


     <div class="form-group">
         <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
         <?php echo CHtml::resetButton('Reset', array('class' => 'btn btn-default')); ?>
     </div>


     <?php $this->endWidget(); ?>

 </div><!-- form -->
                </div>
