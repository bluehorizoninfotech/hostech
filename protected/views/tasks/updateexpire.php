<?php
$cs = Yii::app()->clientScript;
$cs->scriptMap = array(
    'jquery.js' => false,
    'jquery.min.js' => false,
);

?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'time-entry-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'action' => Yii::app()->createUrl('timeEntry/createexpiretimeentry',array('id'=>$model->teid)),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
            ));
    ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php //echo $form->errorSummary($model); ?>
<div class="panel-body">
 <div class="row">    
        <?php
       
        if (Yii::app()->user->role <= 2) {
            // if (!isset($model->user_id))
            //     $model->user_id = Yii::app()->user->id;
            ?>

            <?php
            if(Yii::app()->user->role == 1) {  
              
            ?>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'user_id'); ?>
                <?php echo $form->dropDownList($model, 'user_id', CHtml::listData(Users::model()->findAll(array('select' => array('userid,CONCAT_WS(" ", first_name, last_name) as first_name'),'order' => 'first_name ASC')), 'userid', 'first_name'), 
                array('class'=>'form-control','disabled'=>true,
                 'empty' => '--',
                 'ajax' => array
                            (
                            'type'=>'POST', 
                            'url'=>CController::createUrl('TimeEntry/getTask'), //or $this->createUrl('loadcities') if '$this' extends CController
                            'update'=>'#TimeEntry_tskid', //or 'success' => 'function(data){...handle the data in the way you want...}',
                            'data'=>array('userid'=>'js:this.value'),
                            ),
                 )); ?>        
                <?php echo $form->error($model, 'user_id'); ?>
            </div>

            <?php } else { 
                $members = Groups::model()->findAll(
                         array(
                             'select' => 'group_id',
                             'condition' => 'group_lead=' . Yii::app()->user->id,
                    ));
                    foreach ($members as $data){
                        $list = Groups_members::model()->findAll(
                             array(
                                 'select' => 'group_id,group_members,id',
                                 'condition' => 'group_id=' . $data['group_id'],
                        ));
                        foreach($list as $groupsmem) {
                            $gpmem[] = $groupsmem['group_members'];
                        }
                    }
                    $gpmem[] = Yii::app()->user->id;
                $gpmem = implode(',', $gpmem);
                ?>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'user_id'); ?>
                <?php echo $form->dropDownList($model, 'user_id', CHtml::listData(Users::model()->findAll(array('condition' => 'userid in('.$gpmem.')','order' => 'first_name ASC')), 'userid', 'first_name'), array('class'=>'form-control','disabled'=>'disabled', 'empty' => '--')); ?>        
                <?php echo $form->error($model, 'user_id'); ?>
            </div>
            
		<?php } ?>
        <?php
    }
    ?>
    <?php
    if($model->isNewRecord){ ?>
        <input type="hidden" name="TimeEntry[user_id]" class="user_id_" value="">
   <?php }else{ ?>
        <input type="hidden" name="TimeEntry[user_id]" class="user_id_" value="<?= $model->user_id?>">
    <?php }

    ?>
   
   <!-- Task section starts-->
        <div class="col-md-3"> 
            <?php echo $form->labelEx($model, 'tskid'); ?>	
                <?php 
                echo $form->dropDownList($model, 'tskid', CHtml::listData(Tasks::model()->findAll(array("condition"=>"task_type = 2 AND tskid=".$model->tskid."",'order' => 'title ASC')), 'tskid', 'title'), array('class'=>'form-control')); 
            ?>
            <?php echo $form->error($model, 'tskid'); ?>
            <span  class="red-color" id="task_span_id"></span>
        </div>
          <!-- Task section ends-->
          <!-- <div class="col-md-3">
            <?php echo $form->labelEx($model, 'work_type'); ?>
            <?php
            if(Yii::app()->user->role == 3) {
            ?>
             <?php echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll(array("condition"=>"work_type='Service Report'")), 'wtid', 'work_type'), array('class'=>'form-control','empty' => '---')); ?>

            <?php } else { ?>
               <?php echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll(array('order' => 'work_type ASC')), 'wtid', 'work_type'), array('class'=>'form-control','empty' => '---')); ?>
                            <?php } ?>
            <?php echo $form->error($model, 'work_type'); ?>
        </div>        -->
    
        

        <?php	
        if (!isset($model->entry_date))
            $model->entry_date = (isset(Yii::app()->session['start_date']) ? (Yii::app()->session['start_date']) : date('Y-m-d'));
            $model->entry_date = date('d-M-y',strtotime($model->entry_date));
        ?>

        <div class="col-md-3">
            <?php echo $form->labelEx($model, 'entry_date'); ?>
            <?php echo CHtml::activeTextField($model, 'entry_date', array("id" => "TimeEntry_entry_date", "size" => "15", 'class'=>'form-control','readonly'=>'readonly')); ?>
            <?php echo $form->error($model, 'entry_date'); ?>
            <span  class="red-color" id="span_id"></span>
        </div>          
 
     <div class="col-md-3">
        <?php echo $form->labelEx($model, 'completed_percent'); ?>
        <?php echo $form->textField($model,'completed_percent',array('class' => 'form-control img_comp_class','autocomplete'=>'off',
			'name'=>'TimeEntry[completed_percent]',
			// 'ajax' => array(
			// 			'url'=>array('TimeEntry/getprogress'),
            // 			'type'=>'POST',

			// 			'data'=>array('progress'=>'js:this.value',
			// 						  'task_id'=>'js: $("#TimeEntry_tskid").val()',
			// 							),
            //                             'dataType' => 'json',
			// 			'success' => 'function(data){
			// 				if(data.progress==1){
            //                     alert("progress  percentage should be less than or equal to " +data.balance_progress+ ""); 
			// 					$("#TimeEntry_completed_percent").val("");
			// 				}				
							
			// 			}',
                        
            //             )
			
			)); ?>
        <?php // echo $form->textField($model, 'completed_percent', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'completed_percent'); ?>
    </div>
    <div class="col-md-3">
            <?php echo $form->labelEx($model, 'current_status'); ?>           
             <?php echo $form->dropDownList($model, 'current_status', CHtml::listData(Status::model()->findAll(array("condition"=>"status_type='task_status' AND sid IN (5,7,8,9,72) ORDER BY caption ASC")), 'sid', 'caption'), array('class'=>'form-control','empty' => '---')); ?>           
            <?php echo $form->error($model, 'current_status'); ?>
            <span class="errorMessage" id="current_status"></span>
        </div> 
        <!-- <div class="col-md-3">
            <?php echo $form->labelEx($model, 'hours'); ?>
            <?php
            if(!$model->isNewRecord){
            //    $time =  gmdate('H:i', floor($model->hours * 3600));
            $time=$model->hours;
            }
            ?>
            <input type="time" class="form-control" value="<?= !$model->isNewRecord?$time:''?>" name="TimeEntry[hours]" id="TimeEntry_hours">
            <?php //echo $form->textField($model,'hours',array('class'=>'form-control input-medium','placeholder'=>'hh:mm')); ?>
            <?php echo $form->error($model, 'hours'); ?>
        </div>       -->

        <div class="col-md-3 width-80">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'start_time'); ?>
                                <?php echo $form->textField($model, 'start_time', array("autocomplete" => 'off',  'class' => 'form-control width-50')); ?>
                                <?php echo $form->error($model, 'start_time'); ?>
                            </div>
                        </div>
                        <div class="col-md-3 width-80">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'end_time'); ?>
                                <?php echo $form->textField($model, 'end_time', array("autocomplete" => 'off', 'class' => 'form-control width-50')); ?>
                                <?php echo $form->error($model, 'end_time'); ?>
                            </div>
                        </div>


        <div class="col-md-3">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('class'=>'form-control','rows' => 3)); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>

<!-- <div class="col-md-3">
<input type="checkbox" id="read" name="gantt_chart_permission"  value="1" > 
			Show on GanttChart </label> 
        </div> -->

     </div>
    <div class="row buttons text-center">
        <?php echo CHtml::Submitbutton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn blue save_btn')); ?>
       
       <?php
        if($type ==0){
            echo CHtml::resetButton('Close', array('onclick'=>"closeform('statusform')",'class'=>'btn margin-x-10')); 
            echo CHtml::resetButton('Reset', array('class' => 'btn default'));
        }
       ?>
        <?php // echo CHtml::resetButton('Close', array('onclick'=>"closeform('statusform')",'class'=>'btn'));  ?>
    </div>

    <?php $this->endWidget(); ?>

</div>
<?php
       Yii::app()->clientScript->registerScript('myjavascript', '
            function closeform(formname){
                   $("#"+formname).slideUp();
                   $("html,body").animate({
                       scrollTop: $("body").offset().top
                   },"slow");
           }

           $("#btnSubmit").on("click",function () {

            var form_data = $("#time-entry-form").serialize();

             $.ajax({
                type: "POST",
                data: form_data + "&ajaxcall=1",
                dataType: "json",
                 url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/Addtime").'",

                success: function (response)    
                { 

                    
                }

                });

           });

        //    $("#TimeEntry_tskid").on("change",function () {

        //     var tskid = $(this).val();

        //      $.ajax({
        //         type: "GET",
        //         data: {tskid:tskid},
        //         dataType: "json",
        //          url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/getworytype").'",

        //         success: function (response)    
        //         { 
        //             if(response.id != null){
        //                 $("#TimeEntry_work_type").html(response.option);
        //                 $("#TimeEntry_work_type").val(response.id);
        //                 //$("#TimeEntry_work_type").prop("disabled", true);
        //             }else{
        //                 $("#TimeEntry_work_type").html(response.option);
        //             }
                    
        //         }

        //         });

        //    });
           

           
               ');
 ?>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
<link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/bootstrap-datetimepicker.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
<script type="text/javascript">
    var start=$('#TimeEntry_start_time').val("");
    var end=$('#TimeEntry_end_time').val("");
    $('#TimeEntry_start_time, #TimeEntry_end_time').datetimepicker({
            //language:  'fr',
            pickDate: false,
            weekStart: 1,
            format: "hh:ii",
            todayBtn: false,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            maxView: 1,
            forceParse: 0,
            showMeridian: 1
        });
    var starting_time='<?php echo $model->start_time;?>';
    var ending_time='<?php echo $model->end_time;?>';
    $('#TimeEntry_start_time').val(starting_time);
    $('#TimeEntry_end_time').val(ending_time);

    $(document).on("change","#TimeEntry_completed_percent",function() {
            var status = $('#TimeEntry_current_status').val();
            var progress = $('#TimeEntry_completed_percent').val();
            if(progress==100 && status!=7 && status!=''){
                $('#TimeEntry_current_status').val('');
                $('#current_status').html('Current status should be completed.');
                $('#current_status').fadeIn().delay(10000).fadeOut();
            }else if(progress!=100 && progress!='' && status==7){
                $('#TimeEntry_current_status').val('');
                $('#current_status').html('Completed status is only for 100% progress');
                $('#current_status').fadeIn().delay(10000).fadeOut();
            }
    });

    $(document).on("change","#TimeEntry_current_status",function() {
            var progress = $('#TimeEntry_completed_percent').val();
            var status = $('#TimeEntry_current_status').val();
            if(progress==100 && progress!='' && status!=7){
                $('#TimeEntry_current_status').val('');
                $('#current_status').html('Current status should be completed.');
                $('#current_status').fadeIn().delay(10000).fadeOut();
            }else if(progress!=100 && progress!='' && status==7){
                $('#TimeEntry_current_status').val('');
                $('#current_status').html('Completed status is only for 100% progress');
                $('#current_status').fadeIn().delay(10000).fadeOut();
            } 
    });
</script>


