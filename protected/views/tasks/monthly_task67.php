<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/onedrive.js" type="text/javascript">
</script>



<!-- <div class="view-project-sec"> -->

<div class="clearfix project-filer">
    <div class="advanced-search">
        <?php
        $milestone_model = new Milestone();
        $this->renderPartial('_monthly_task_search', array(
            'model' => $model

        ));
        ?>
    </div>
</div>


<div id="create_wpr_form_div">

</div>

<div id="int_task_form" class="panel panel-primary">

</div>

<div class="row">

    <div class="col-md-12">
        <!-- <div style="height:500px; overflow-y: scroll;"> -->
        <div class="table-page-scroll-bar">
            <table class="table table-striped bordering-table weekly-report-table border-collapsed-separate">
                <thead>
                    <tr>
                        <!-- <th colspan="14" align="center"></th> -->
                    </tr>
                    <tr>
                        <th rowspan="2" class="first-th">Task id</th>
                        <th rowspan="2" colspan="3" class="first-th">Description of work</th>
                        <th rowspan="2" class="first-th">Owner</th>
                        <th rowspan="2" class="first-th">Co-ordinator</th>
                        <th rowspan="2" class="first-th">Priority</th>
                        <th rowspan="2" class="first-th">Task duration</th>
                        <th colspan="2" class="first-th">scheduled</th>
                        <th colspan="2" class="first-th">Actual</th>

                        <th rowspan="2" class="first-th"> Progress update (date)</th>
                        <th rowspan="2" class="first-th">Progress % </th>
                        <th rowspan="2" class="first-th">Remarks</th>
                        <th rowspan="2" class="first-th">Status</th>

                        <th rowspan="2" class="first-th">Action</th>



                    </tr>

                    <tr>



                        <th class="second-th">Start date</th>
                        <th class="second-th">End date</th>


                        <th class="second-th">Start date</th>
                        <th class="second-th">End date</th>


                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = 0;
                    foreach ($data_array as $key => $project) {
                    ?>
                        <tr class="add-button-row task-main">
                            <!-- <td colspan="13" class="soft-blue-bg"><?php echo $key ?> -->

                            <td colspan="17" class="soft-blue-bg1 project_name full-border">

                                <div class="monthly-table-left"><?php echo $key ?></div>

                                <div class="monthly-table-right">
                                    <?php
                                                                    // $createUrl = $this->createUrl('tasks/addweeklytask', array("asDialog" => 1, "gridId" => 'tasks-grid', 'project' => $key));
                                                                    // echo CHtml::link('Add Task', '', array('class' => 'btn btn-sm blue margin-left-80 tasks-project', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));

                                                                    ?>

<?php echo CHtml::link('Add Task', array('createTask','project' => $key), array('class' => 'btn btn-sm blue margin-left-80 tasks-project'));?>
                                                                    
                                                                </div>





                            </td>
                        </tr><!-- project tr close -->


                        <?php
                        // print_r($project);die;
                        foreach ($project as $key1 => $milestone) {
                        ?>
                            <tr class="add-button-row task-main">
                                <td class="full-border"></td>
                                <td colspan="16" class="pink-bg full-border">
                                    <div class="monthly-table-left"><?php echo $key1  ?></div>


                                    <div class="monthly-table-right"><?php



                                                                        // $createUrl = $this->createUrl('tasks/addweeklytask', array("asDialog" => 1, "gridId" => 'tasks-grid', 'project' => $key, 'budget_head' => $key1));
                                                                        // echo CHtml::link('Add Task Milestone ', '', array('class' => 'btn btn-sm blue margin-left-80 tasks-budgethead', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));


                                                                        ?>
                                                                        <?php echo CHtml::link('Add Task', array('createTask','project' => $key,'budget_head' => $key1), array('class' => 'btn btn-sm blue margin-left-80 tasks-project'));?>
                                                                        </div>

                                </td>
                            </tr>

                            <?php
                            foreach ($milestone as  $key2 => $milestones) {
                                $milestone_id = $milestones['milestone_id'];
                                $parent_task = $this->getParentTaskDet($milestone_id, $task_id_array);
                                $milestone_duration = $this->getTimeDuration($milestones['start_date'], $milestones['end_date']);
                            ?>
                                <tr class="add-button-row task-main">

                                    <td class="full-border"></td>
                                    <td class="full-border" colspan="3">
                                        <div class="monthly-table-left"><?php echo $key2  ?></div>
                                        <div class="monthly-table-right"> <?php



                                                                            // $createUrl = $this->createUrl('tasks/addweeklytask', array("asDialog" => 1, "gridId" => 'tasks-grid', 'project' => $key, 'milestone_id' => $milestone_id));
                                                                            // echo CHtml::link('Add Task Milestone ', '', array('class' => 'btn btn-sm blue margin-left-80 tasks-milestone', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));


                                                                            ?>
                                                                            <?php echo CHtml::link('Add Task ', array('createTask','project' => $key,'milestone_id' => $milestone_id), array('class' => 'btn btn-sm blue margin-left-80 tasks-project'));?>
                                                                            
                                                                        </div>
                                    </td>

                                    <td class="full-border"></td>
                                    <td class="full-border"></td>
                                    <td class="full-border"></td>
                                    <td class="full-border"><?php echo  $milestone_duration ?></td>
                                    <td class="full-border"><?php echo date("d-M-y", strtotime($milestones['start_date'])) ?></td>
                                    <td class="full-border"><?php echo date("d-M-y", strtotime($milestones['end_date'])) ?></td>
                                    <td class="full-border"></td>
                                    <td class="full-border"></td>
                                    <td class="full-border"></td>
                                    <td class="full-border"></td>

                                    <td class="full-border"></td>
                                    <td class="full-border"></td>
                                    <td class="full-border"></td>

                                </tr> <!-- milestone tr -->

                                <?php
                                if (count($parent_task) > 0) {
                                    foreach ($parent_task as $parent) {
                                        $parent_id = $parent->tskid;
                                        // $sub_task = $this->getSubTask($parent_id);
                                        $sub_task = $this->getSubTaskRecursive($parent_id);
                                        $parent_task_owner = $this->getName($parent->assigned_to);
                                        $parent_task_coordinator = $this->getName($parent->coordinator);
                                        $parent_task_priority = $this->getStatus($parent->priority);
                                        $parent_task_duration = $this->getTimeDuration($parent->start_date, $parent->due_date);
                                        $parent_wpr_det = $this->getWrDetails($parent_id, $parent->quantity, $parent->task_type);
                                        $owner = "";



                                ?>
                                        <tr class="add-button-row task-main">
                                            <td class="full-border"></td>
                                            <td></td>
                                            <td class="full-border" colspan="2">
                                                <div class="monthly-table-left task-open"><?php echo $parent->title ?></div>

                                                <div class="monthly-table-right"> <?php
                                                                                    // $createUrl = $this->createUrl('tasks/addweeklytask', array("asDialog" => 1, "gridId" => 'tasks-grid', 'project' => $key, 'parent_task_id' => $parent_id));
                                                                                    // echo CHtml::link('Add Task  Pare', '', array('class' => 'btn btn-sm blue margin-left-80', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
                                                                                    ?>

<?php echo CHtml::link('Add Task ', array('createTask','project' => $key,'parent_task_id' => $parent_id), array('class' => 'btn btn-sm blue margin-left-80 tasks-project'));?>
                                                                                    
                                                                                </div>
                                            </td>

                                            <td class="full-border"><?php echo $parent_task_owner ?></td>
                                            <td class="full-border"><?php echo $parent_task_coordinator ?></td>
                                            <td class="full-border"><?php echo $parent_task_priority ?></td>
                                            <td class="full-border"><?php echo $parent_task_duration ?></td>
                                            <td class="full-border"><?php echo date("d-M-y", strtotime($parent->start_date)) ?></td>
                                            <td class="full-border"><?php echo date("d-M-y", strtotime($parent->due_date)) ?></td>
                                            <td class="full-border"><?php echo $parent_wpr_det[0] ?></td>
                                            <td class="full-border"><?php echo $parent_wpr_det[1] ?></td>
                                            <td class="full-border"><?php echo $parent_wpr_det[6] ?></td>
                                            <td class="full-border"><?php echo round($parent_wpr_det[2], 2) . " %" ?></td>
                                            <td class="full-border"><?php echo  $parent_wpr_det[4] ?></td>
                                            <td class="full-border"><?php echo $parent_wpr_det[3] ?></td>

                                            <!--  wpr/time entry section starts-->
                                            <?php

                                            $checkTaskAssigned = $this->checkTaskAssigned($parent->tskid);

                                            if ($parent['task_type'] == 1) {
                                                if ($checkTaskAssigned == 1 && $parent->work_type_id != "") {
                                            ?>


                                                    <td class="white-space-nowrap full-border"><i class="fa fa-plus add-work-progress" id=<?= $parent->tskid ?>></i>
                                                    </td>

                                                <?php
                                                } else {
                                                ?>
                                                    <td class="full-border"></td>
                                                <?php
                                                }
                                            } // task type==1 close

                                            else {
                                                $timeEntryAssigned = $this->checkTimeEntryAssigned($parent->tskid);

                                                if ($timeEntryAssigned == 1) {
                                                ?>

                                                    <td class="white-space-nowrap"><i class="fa fa-plus add-time-entry" id=<?php echo $parent->tskid ?>></i> </td>

                                                <?php } else {
                                                ?>

                                                    <td class="full-border"></td>


                                            <?php
                                                }
                                            } // else close

                                            ?>




                                            <!-- wpr/time entry section end  -->

                                        </tr><!-- parent task tr  -->

                                        <?php

                                        $getWorkProgressDetails = $this->getWorkProgressDetails($parent->tskid, $parent->task_type);
                                        // echo "<pre>",print_r($getWorkProgressDetails);
                                        if (count($getWorkProgressDetails) > 0) {
                                            foreach ($getWorkProgressDetails as $progress) {


                                        ?>

                                                <tr class="task-progress">
                                                    <td class="full-border"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="left-side-border"></td>
                                                    <td class="left-side-border"></td>
                                                    <td class="left-side-border"></td>
                                                    <td class="left-side-border"></td>
                                                    <td class="left-side-border"></td>
                                                    <td class="left-side-border"></td>
                                                    <td class="left-side-border"></td>
                                                    <td class="full-border"></td>
                                                    <td class="full-border"></td>
                                                    <td class="full-border"><?php echo date("d-M-y", strtotime($progress['progress_date'])) ?> </td>
                                                    <td class="full-border"><?php echo $progress['progress_percentage'] . " %" ?></td>
                                                    <td class="full-border"><?php echo $progress['progress_comments'] ?></td>
                                                    <td class="full-border"></td>
                                                    <td class="full-border"></td>

                                                </tr>

                                        <?php
                                            } // $getWorkProgressDetails foreach
                                        } // count($getWorkProgressDetails)
                                        ?>

                                        <?php
                                        if (count($sub_task) > 0) {
                                            foreach ($sub_task as $sub) {
                                                $task_id = $sub->tskid;

                                                $sub_task_owner = $this->getName($sub->assigned_to);
                                                $sub_task_coordinator = $this->getName($sub->coordinator);
                                                $sub_task_priority = $this->getStatus($sub->priority);
                                                $sub_task_duration = $this->getTimeDuration($sub->start_date, $sub->due_date);
                                                $sub_wpr_det = $this->getWrDetails($task_id, $sub->quantity, $sub->task_type);
                                                $sub_task_status = $this->getStatus($sub->status);
                                                $sub_status = $sub->status;
                                                $due_date = $sub->due_date;
                                                $expired_condition = $due_date < date('Y-m-d');

                                                if (!$expired_condition && $sub_status != 7  ||  !$expired_condition &&  $sub_status == 7 || $expired_condition && $sub_status != 7) {


                                        ?>

                                                    <tr class="add-button-row task-main">
                                                        <td class="full-border"></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="full-border">
                                                            <div class="monthly-table-left task-open"><?php echo $sub->title  ?></div>



                                                            <div class="monthly-table-right"><?php
                                                                                                // $createUrl = $this->createUrl('tasks/addweeklytask', array("asDialog" => 1, "gridId" => 'tasks-grid', 'project' => $key, 'parent_task_id' => $task_id, 'sub_task_id' => $task_id));
                                                                                                // echo CHtml::link('Add Task Sub ', '', array('class' => 'btn btn-sm blue margin-left-80', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
                                                                                                ?>
                                                                                                <?php echo CHtml::link('Add Task ', array('createTask','project' => $key,'sub_task_id' => $task_id), array('class' => 'btn btn-sm blue margin-left-80 tasks-project'));?>
                                                                                                </div>

                                                        </td>

                                                        <td class="full-border"><?php echo $sub_task_owner ?></td>
                                                        <td class="full-border"><?php echo $sub_task_coordinator ?></td>
                                                        <td class="full-border"><?php echo $sub_task_priority ?></td>
                                                        <td class="full-border"><?php echo $sub_task_duration ?></td>
                                                        <td class="full-border"><?php echo date("d-M-y", strtotime($sub->start_date)) ?></td>
                                                        <td class="full-border"><?php echo date("d-M-y", strtotime($sub->due_date)) ?></td>
                                                        <td class="full-border"><?php echo $sub_wpr_det[0] ?></td>
                                                        <td class="full-border"><?php echo $sub_wpr_det[1] ?></td>
                                                        <td class="full-border"><?php echo $sub_wpr_det[6] ?></td>
                                                        <td class="full-border"><?php echo round($sub_wpr_det[2], 2) . " %" ?></td>
                                                        <td class="full-border"><?php echo $sub_wpr_det[4] ?></td>
                                                        <td class="full-border"><?php echo $sub_wpr_det[3] ?></td>

                                                        <!--  wpr/time entry section starts-->
                                                        <?php

                                                        $checkTaskAssigned = $this->checkTaskAssigned($sub->tskid);

                                                        if ($sub['task_type'] == 1) {
                                                            if ($checkTaskAssigned == 1 && $sub->work_type_id != "") {
                                                        ?>


                                                                <td class="white-space-nowrap full-border"><i class="fa fa-plus add-work-progress" id=<?= $sub->tskid ?>></i>
                                                                </td>

                                                            <?php
                                                            } else {
                                                            ?>
                                                                <td class="full-border"></td>
                                                            <?php
                                                            }
                                                        } // task type==1 close

                                                        else {
                                                            $timeEntryAssigned = $this->checkTimeEntryAssigned($sub->tskid);
                                                            if ($timeEntryAssigned == 1) {
                                                            ?>

                                                                <td class="white-space-nowrap full-border"><i class="fa fa-plus add-time-entry" id=<?php echo $sub->tskid ?>></i> </td>

                                                            <?php } else {
                                                            ?>

                                                                <td class="full-border"></td>


                                                        <?php
                                                            }
                                                        } // else close

                                                        ?>
                                                        <!-- wpr/time entry section end  -->
                                                    </tr> <!--  subtask tr -->
                                                    <?php
                                                    $getSubTaskWorkProgressDetails = $this->getWorkProgressDetails($sub->tskid, $sub->task_type);
                                                    ?>

                                                    <?php
                                                    if (count($getSubTaskWorkProgressDetails) > 0) {
                                                        foreach ($getSubTaskWorkProgressDetails as $subprogress) {
                                                    ?>

                                                            <tr class="task-progress">
                                                                <td class="full-border"></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="left-side-border"></td>
                                                                <td class="left-side-border"></td>
                                                                <td class="left-side-border"></td>
                                                                <td class="left-side-border"></td>
                                                                <td class="left-side-border"></td>
                                                                <td class="left-side-border"></td>
                                                                <td class="left-side-border"></td>
                                                                <td class="full-border"></td>
                                                                <td class="full-border"></td>
                                                                <td class="full-border"><?php echo date("d-M-y", strtotime($subprogress['progress_date'])) ?></td>
                                                                <td class="full-border"><?php echo $subprogress['progress_percentage'] . " %" ?></td>
                                                                <td class="full-border"><?php echo $subprogress['progress_comments'] ?></td>
                                                                <td class="full-border"></td>
                                                                <td class="full-border"></td>
                                                            </tr>

                                                    <?php
                                                        }
                                                    }
                                                    ?>

                                        <?php
                                                }  //if(! $expired_condition  && $status!=7 )

                                            } // $sub_task foreach close
                                        } // count($sub_task) close
                                        ?>

                                <?php
                                    } //$parent_task foreach close
                                } // count($parent_task) close
                                ?>

                    <?php
                            } // $milestone foreach close
                        } // $project foreach close
                    } // $data_array foreach close
                    ?>
                </tbody>
        </div>
    </div>
</div>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Task',
        'autoOpen' => false,
        'modal' => false,
        'width' => "590",
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="550" class="min-height-500"></iframe>

<?php
$this->endWidget();
?>

<script>
    $('.add-work-progress').click(function() {
        var task_id = this.id;

        $.ajax({
            "type": "POST",

            "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/monthtaskworkProgress'); ?>",
            "dataType": "json",
            "data": {
                task_id: task_id
            },
            "success": function(data) {

                $('#create_wpr_form_div').html(data);
            }
        });

    });

    $(".add-time-entry").click(function() {

        var task_id = this.id;

        $.ajax({
            type: "GET",

            url: "<?php echo Yii::app()->createAbsoluteUrl("TimeEntry/addtimeentry&layout=1") ?>",
            data: {
                taskid: task_id
            },
            success: function(response) {
                $('#create_wpr_form_div').hide();
                $("#int_task_form").html(response).slideDown();

            }
        });

    });

    // $(function() {
    //     $(".weekly-report-table", "body").on({
    //             'click': function(event) {
    //                 event.preventDefault();
    //                 $(this).closest("tr.task-main").nextUntil("tr.task-main").toggle("fast");
    //             }
    //         },
    //         "tr.task-main", null);
    // });



    $(".tasks-project").click(function() {
        $.ajax({
            type: "GET",

            url: "<?php echo Yii::app()->createAbsoluteUrl("Tasks/unsetTaskSession") ?>",

            success: function(response) {}
        });

    });
    $(".tasks-budgethead").click(function() {
        $.ajax({
            type: "GET",

            url: "<?php echo Yii::app()->createAbsoluteUrl("Tasks/unsetTaskSession") ?>",

            success: function(response) {}
        });
    });


    $(".tasks-milestone").click(function() {
        $.ajax({
            type: "GET",

            url: "<?php echo Yii::app()->createAbsoluteUrl("Tasks/unsetTaskSession") ?>",

            success: function(response) {}
        });
    });
    $('#int_task_form').hide();
</script>