<?php
/* @var $this TasksController */
/* @var $model Tasks */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'post',
));
if (Yii::app()->user->role == 1) {
    $condition = array('select' => array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
} else {
    $criteria = new CDbCriteria;
    $criteria->select = 'project_id';
    $criteria->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
    $criteria->group = 'project_id';
    $project_ids = Tasks::model()->findAll($criteria);
    $project_id_array = array();
    foreach ($project_ids as $projid) {
        array_push($project_id_array, $projid['project_id']);
    }
    if (!empty($project_id_array)) {
        $project_id_array = implode(',', $project_id_array);
        $condition = array('select' => array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
    } else {
        $condition = array('select' => array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'order' => 'name ASC');
    }
}
if (!empty(Yii::app()->user->project_id)) {
    $milestone_condition = 'project_id = ' . Yii::app()->user->project_id;
} else {
    // $milestone_condition = '';
    if(isset($_GET['project_filter']))
    {
        $prj=$_GET['project_filter'];
        $milestone_condition = 'project_id = ' . $prj;
    }
    else
    {
        $milestone_condition = ''; 
    }
    
}
// if(Yii::app()->user->project_id !=""){
// 	$model->project_id = Yii::app()->user->project_id;
// 	$project_id = Yii::app()->user->project_id;
// }	
$project = Projects::model()->findAll($condition);
?>

<div class="form search-filter">
    <div class="row max-width-800">
        <div class="form-group col-md-2 min-width-160 padding-left-0">
            <?php echo $form->label($model, 'project_id'); ?>
            <?php
            echo CHtml::activeDropDownList(
                $model,
                'project_id',
                CHtml::listData(
                    Projects::model()->findAll($condition),
                    'pid',
                    'name'
                ),
                array('empty' => 'Select a Project', 'class' => 'form-control project_class')
            );
            $form->textField($model, 'project_id');
            ?>
        </div>


        <div class="form-group col-md-2 min-width-160 padding-left-0">
            <?php echo $form->label($model, 'assigned_to'); ?>
            <?php
            echo CHtml::activeDropDownList(
                $model,
                'assigned_to',
                CHtml::listData(Users::model()->findAll(
                    array(
                        'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                        'order' => 'first_name',
                        'distinct' => true
                    )
                ), "userid", "first_name"),
                array('empty' => 'Select Assigned To', 'class' => 'form-control assigned_filter')
            );
            $form->textField($model, 'assigned_to');
            ?>
        </div>

        <div class="form-group col-md-2 min-width-160 padding-left-0">
            <?php echo $form->label($model, 'coordinator'); ?>

            <?php
            echo CHtml::activeDropDownList(
                $model,
                'coordinator',
                CHtml::listData(Users::model()->findAll(
                    array(
                        'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                        'order' => 'first_name',
                        'distinct' => true
                    )
                ), "userid", "first_name"),
            
            
            
                array('empty' => 'Select Coordinator', 'class' => 'form-control coordinator_filter')
            );


            ?>
        </div>
        <div class="form-group col-md-2 min-width-160 padding-left-0">
            <?php echo $form->label($model, 'milestone_id'); ?>

            <?php
            echo CHtml::activeDropDownList(
                $model,
                'milestone_id',
                CHtml::listData(Milestone::model()->findAll(
                    array(
                        'select' => array('id,milestone_title'),
                        'condition' => $milestone_condition,
                        'order' => 'milestone_title',
                        'distinct' => true
                    )
                ), "id", "milestone_title"),
                array('empty' => 'Select Milestone', 'class' => 'form-control milestone_filter')
            );
            ?>
            <input type="hidden" id="milestone_val" value="<?= $model->milestone_id ?>">
        </div>    

        <div class="form-group col-md-2 search-btn margin-top-12">
            <div>
                <!-- <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary btn-sm')); ?> -->
                <?php echo CHtml::resetButton('Clear', array('class' => 'btn blue btn-sm', 'onclick' => 'javascript:location.href="' . $this->createUrl('mytask') . '"')); ?>
            </div>
        </div>
    </div>

</div>
<?php $this->endWidget(); ?>

<script>
        $(document).ready(function() {
            $('.project_class').on('change.bootstrapSwitch', function(e) {
               var project= $(".project_class").val();                              
                if (project!='') {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            project_filter: project
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            project_filter: ""
                        },
                    });
                }
            });
            var queryString = window.location.search;
            var urlParams = new URLSearchParams(queryString);
            var project = urlParams.get('project_filter')             
            $(".project_class").val(project);
            $('.mytask-table').wrap('<div class="table-responsive"></div>')

    $('.assigned_filter').on('change.bootstrapSwitch', function(e) {
               var assigned_to= $(".assigned_filter").val();            
               if (assigned_to!='') {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            assigned_filter: assigned_to
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            assigned_filter: ""
                        },
                    });
                }
            });
            var queryString = window.location.search;
            var urlParams = new URLSearchParams(queryString);
            var assigned = urlParams.get('assigned_filter')             
            $(".assigned_filter").val(assigned);
            $('.mytask-table').wrap('<div class="table-responsive"></div>')

            $('.coordinator_filter').on('change.bootstrapSwitch', function(e) {
               var coordinator_filter= $(".coordinator_filter").val();               
                if (coordinator_filter!='') {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            coordinator_filter: coordinator_filter
                        },
                    });
                    
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            coordinator_filter: ""
                        },
                    });
                }
            }); 
           var queryString = window.location.search;
           var urlParams = new URLSearchParams(queryString);          
           var co_ordinator = urlParams.get('coordinator_filter')             
           $(".coordinator_filter").val(co_ordinator);
           $('.mytask-table').wrap('<div class="table-responsive"></div>')  
           
           
           $('.milestone_filter').on('change.bootstrapSwitch', function(e) {
               var milestone_filter= $(".milestone_filter").val();                          
                if (milestone_filter!='') {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            milestone_filter: milestone_filter
                        },
                    });
                    
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            milestone_filter: ""
                        },
                    });
                }
            }); 
           var queryString = window.location.search;
           var urlParams = new URLSearchParams(queryString);
           var milestone = urlParams.get('milestone_filter')             
           $(".milestone_filter").val(milestone);
           $('.mytask-table').wrap('<div class="table-responsive"></div>') 
        });
    </script>