<html>

<head>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
    type="text/css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" rel="stylesheet"
    type="text/css" />
</head>

<body style="font-family: sans-serif">
<div class="container" align="center">
    <table style="border-collapse: unset;min-width: 600px;font-size: 22px;padding: 5px;border-radius: 5px;margin-top: 10px;margin-bottom: 10px;border: 1px solid #c9c9c9;background: #f3f3f3;">

      <tr style="display: flex; column-gap: 10px; align-items: baseline; font-size: 22px;">
        <td style="text-align: left; padding: 5px 8.5px; vertical-align: middle !important">My Tasks -</td>
        <td style="text-align: left;padding: 5px 8.5px;vertical-align: middle !important;font-size: 22px;font-weight: 700;color: #c7006d;">
          <?= count($mytasks) ?>
        </td>
      </tr>
      <tr style="display: flex; column-gap: 10px; align-items: baseline; font-size: 22px;">
        <td style="text-align: left; padding: 5px 8.5px; vertical-align: middle !important">My Milestones -</td>
        <td style="text-align: left;padding: 5px 8.5px;vertical-align: middle !important;font-size: 22px;font-weight: 700;color: #4b8df8;">
          <?= count($milestones) ?>
        </td>
      </tr>
      <tr style="display: flex; column-gap: 10px; align-items: baseline; font-size: 22px;">
        <td style="text-align: left; padding: 5px 8.5px; vertical-align: middle !important">Delayed Tasks -</td>
        <td style="text-align: left;padding: 5px 8.5px;vertical-align: middle !important;font-size: 22px;font-weight: 700;color: #f47777;">
          <?= count($delayed_tasks) ?>
        </td>
      </tr>
    </table>
    <table style="border-collapse: unset;min-width: 600px;font-size: 22px;padding: 5px ;border-radius: 5px;margin-top: 10px;margin-bottom: 10px;border: 1px solid #c9c9c9;background: #f3f3f3;">
      <tr>
        <td colspan="2" style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important">
          <h2 style="font-size: 23px;margin: 0px;margin-bottom:5px;">My Tasks</h2>
          <hr style="border-color: #2E2E2E; margin: 0px;" />
        </td>
      </tr>
      <?php if (!empty($mytasks)) {
        foreach ($mytasks as $key => $tasks) {
          ?>
          <tr>
            <td style=" max-width:600px;text-align: left;padding: 5px 8.5px;vertical-align: middle !important;display: flex;column-gap: 10px;justify-content: space-between;align-items: center;font-size: 22px;">
              <p style="color: #004399; font-size: 22px; margin: 0px;">
                <?= isset($tasks['title']) ? $tasks['title'] : ''; ?>
              </p>

              <?php
              if ($tasks['quantity'] != '') {
                $delaystatus = $this->getDelayedTasksStatus($tasks['tskid'], $tasks['daily_target'], $tasks['quantity'], $tasks['project_id'], $tasks['clientsite_id'], $tasks['start_date']);
                ?>
                <p style="color: #ffb71d; font-weight: 700; margin: 0px 0px 0px auto;">
                  <?= $delaystatus ?>
                </p>

              <?php } else {
                if ($tasks['sid'] == 5) {
                  $color = '#aa8b00';
                } else if ($tasks['sid'] == 6) {
                  $color = '#74cb80';
                } else if ($tasks['sid'] == 7) {
                  $color = '#9900ff';
                } else if ($tasks['sid'] == 8) {
                  $color = '#005222';
                } else if ($tasks['sid'] == 9) {
                  $color = '#08aeea';
                } else if ($tasks['sid'] == 72) {
                  $color = '#c7006d';
                } else if ($tasks['sid'] == 73) {
                  $color = '#614a00';
                } else if ($tasks['sid'] == 74) {
                  $color = '#cf9f00';
                } else {
                  $color = '#f6a96d';
                }
                ?>
                <p style="color:<?= $color ?>; font-weight: 700; margin: 0px 0px 0px auto;">
                  <?= isset($tasks['caption']) ? $tasks['caption'] : ''; ?>
                </p>
              <?php } ?>
            </td>
            <td style=" max-width:600px;text-align: left;padding: 5px 8.5px;vertical-align: middle !important;display: flex;column-gap: 10px;font-size: 22px;align-items: center;">
              <p style="margin: 0px;">
                <?= (isset($tasks['project_name']) ? $tasks['project_name'] : '').'&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Due Date &nbsp; - &nbsp;'.( isset($tasks['due_date']) ? date("d M Y", strtotime($tasks['due_date'])) : ''); ?>
              </p>
            </td>
          </tr>
          <?php if ($key !== (count($mytasks) - 1)) { ?>

            <tr>
              <td colspan="2" style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important;">
                <hr style="border-color: #2E2E2E; margin: 0px;border-width:1px !important;" />
              </td>
            </tr>
          <?php } ?>

        <?php }
      } else { ?>
        <tr>
          <td  style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important;">
            <p style="color: #004399; font-size: 22px; margin: 0px">No results found.</p>
          </td>
        </tr>
      <?php } ?>
    </table>
    <table style="border-collapse: unset;min-width: 600px;;font-size: 22px;padding: 5px ;border-radius: 5px;margin-top: 10px;margin-bottom: 10px;border: 1px solid #c9c9c9;background: #f3f3f3;">
      <tr>
        <td colspan="2" style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important;">
          <h2 style="font-size: 23px;margin:0px;margin-bottom:5px;">My Milestones</h2>
          <hr style="border-color: #2E2E2E; margin: 0px" />
        </td>
      </tr>
      <?php if (!empty($milestones)) {
        foreach ($milestones as $key => $milestone) { ?>
          <tr>
            <td style=" max-width:600px;text-align: left;padding: 5px 8.5px;vertical-align: middle !important;display: flex;column-gap: 10px;justify-content: space-between;align-items: center;font-size: 22px;">
              <p style="color: #004399; font-size: 22px; margin: 0px">
                <?= isset($milestone['milestone_title']) ? $milestone['milestone_title'] : ''; ?>
              </p>
              <p style="color: <?= $milestone['status'] == 1 ? '#69bd6f' : 'red'; ?>; font-weight: 700; margin: 0px 0px 0px auto">
                <?= $milestone['status'] == 1 ? 'Enable' : 'Disable'; ?>
              </p>
            </td>
            <td style=" max-width:600px;text-align: left;padding: 5px 8.5px;vertical-align: middle !important;display: flex;column-gap: 10px;font-size: 22px;align-items: center;">
              <p style="margin: 0px;">
                <?= (isset($milestone['project_name']) ? $milestone['project_name'] : '').'&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Due Date &nbsp; - &nbsp;'. (isset($milestone['end_date']) ? date("d M Y", strtotime($milestone['end_date'])) : ''); ?>
              </p>
            </td>
          </tr>
          <?php if ($key !== (count($milestones) - 1)) { ?>
            <tr>
              <td colspan="2" style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important">
                <hr style="border-color: #2E2E2E; margin: 0px" />
              </td>
            </tr>
          <?php } ?>
        <?php }
      } else { ?>
        <tr>
          <td style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important">
            <p style="color: #004399; font-size: 22px; margin: 0px">No results found.</p>
          </td>
        </tr>
      <?php } ?>
    </table>
    <table style="border-collapse: unset;min-width: 600px;font-size: 22px;padding: 5px ;border-radius: 5px;margin-top: 10px;margin-bottom: 10px;border: 1px solid #c9c9c9;background: #f3f3f3;">
      <tr>
        <td colspan="2" style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important">
          <h2 style="font-size: 23px;margin:0px;margin-bottom:5px;">My Upcoming Tasks (upto 5 days)</h2>
          <hr style="border-color: #2E2E2E; margin: 0px" />
        </td>
      </tr>
      <?php if (!empty($upcoming_tasks)) {
        foreach ($upcoming_tasks as $key => $tasks) { ?>
          <tr>
          <td style=" max-width:600px;text-align: left;padding: 5px 8.5px;vertical-align: middle !important;display: flex;column-gap: 10px;justify-content: space-between;align-items: center;font-size: 22px;">
              <p style="color: #004399; font-size: 22px; margin: 0px">
                <?= isset($tasks['title']) ? $tasks['title'] : ''; ?>
              </p>
            </td>
            <td style=" max-width:600px;text-align: left;padding: 5px 8.5px;vertical-align: middle !important;display: flex;column-gap: 10px;font-size: 22px;align-items: center;">
             
            <p style="margin: 0px;">
                <?= (isset($tasks['project_name']) ? $milestone['project_name'] : '').'&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'. (isset($tasks['start_date']) ? 'From &nbsp; - &nbsp;' . date("d M Y", strtotime($tasks['start_date'])) : '').'    '.(isset($tasks['end_date']) ? 'To &nbsp; - &nbsp;' . date("d M Y", strtotime($tasks['end_date'])) : ''); ?>              </p>
            </td>
          </tr>
          <?php if ($key !== (count($upcoming_tasks) - 1)) { ?>

            <tr>
              <td colspan="2" style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important">
                <hr style="border-color: #2E2E2E; margin: 0px;" />
              </td>
            </tr>
          <?php } ?>
        <?php }
      } else { ?>
        <tr>
          <td  colspan="2" style=" max-width:600px;text-align: left; padding: 5px 8.5px; vertical-align: middle !important">
            <p style="color: #004399; font-size: 22px; margin: 0px;">No results found.</p>
          </td>
        </tr>
      <?php } ?>
    </table>

  </div>
  <div style="margin-top: 22px; margin-bottom: 10px; text-align: center; color: #555; width: 100%;">
  <p style="margin: 0;">
    Powered by <a href="https://www.bluehorizoninfotech.com" style="color: #007bff; text-decoration: none;" target="_blank">bluehorizoninfotech.com</a>
  </p>
  <p style="margin: 0;">&copy; 2023 by Blue Horizon Infotech, Cochin. All Rights Reserved.</p>
</div>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>