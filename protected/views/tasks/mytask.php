<?php
if (!isset($_GET['exportgrid'])) {
?>
    <script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
    <?php
    Yii::app()->getModule('masters');
    /* @var $this TasksController */
    /* @var $dataProvider CActiveDataProvider */

    $this->breadcrumbs = array(
        'Tasks',
    );
    /*
      Yii::app()->clientScript->registerCss('mycss', '
      tr.higlight_backgrnd td:first-child {
      background: #880000;
      color: #fff;
      ');
     */
    ?>
    <div class="display-flex justify-content-between">
        <div>
    <?php
    if (!isset($_GET['exportgrid'])) {

        if ($type == 'pending') {
    ?>

            <h1>My Pending Tasks</h1>

        <?php
        } else {
        ?>

            <h1>My Tasks</h1>
        </div>
<?php
        }
    }
}
?>
</div>
<div class="addlink">
<?php
            $url = array('Tasks/export');

            echo CHtml::link('<i class="fa fa-download" title="Export"></i>', Yii::app()->request->requestUri . "&exportgrid=1", array(
                //                'submit' => $url,
                //                'params' => array('model' => json_encode($newarr)),
                'class' => 'btn blue',
            ));
            ?>
</div>
</div>
<?php
$provider = $model->assigned_tasknew($type);
$data = $provider->getData();
$newarr = array();
foreach ($data as $value) {
    $newarr[] = array(
        'project_id' => $value->project_id,
        'tskid' => $value->tskid,
        'report_to' => $value->report_to,
        'assigned_to' => $value->assigned_to,
        'coordinator' => $value->coordinator,
        'start_date' => $value->start_date,
        'title' => $value->title,
        'due_date' => $value->due_date,
    );
}

//echo '<pre>';print_r($newarr);exit;
if (!isset($_GET['exportgrid'])) {
?>
<div class="mytask-section">
    <div class="clearfix project-filer">
        <div class="pull-left advanced-search">
            <?php
            $this->renderPartial('_search', array(
                'model' => $model,
            ));
            ?>
        </div>
        <div class="addlink">
            <label class="checkbox-inline padding-left-0">
                <input type="checkbox" class="change" <?= (isset($_GET['type']) and $_GET['type']) == 'closed' ? 'checked' : '' ?> id="list_change" data-toggle="toggle">
            </label> Completed Tasks
            <?php
            // echo CHtml::button('Add Task', array('submit' => array('Tasks/AddnewTask') ,'class'=>'btn blue' , 'style' =>""));
            ?>
        </div>
    </div>
<?php
}
$condition = '';
if (Yii::app()->user->role == 10) {
    $tblpx = Yii::app()->db->tablePrefix;
    //echo "SELECT cs.pid FROM {$tblpx}clientsite cs JOIN {$tblpx}clientsite_assigned ca ON ca.site_id = cs.id WHERE ca.user_id = '".Yii::app()->user->id."'";exit;
    $sqlp = Yii::app()->db->createCommand("SELECT GROUP_CONCAT(CONCAT('''', cs.pid, '''' )) as pid FROM {$tblpx}clientsite cs JOIN {$tblpx}clientsite_assigned ca ON ca.site_id = cs.id WHERE ca.user_id = '" . Yii::app()->user->id . "'")->queryRow();
    if ($sqlp['pid']) {
        $condition = 'pid IN (' . $sqlp['pid'] . ')';
    } else {
        $condition = '1!=1';
    }
}
?>
<?php
$alltasks = Tasks::model()->findAll('assigned_to= ' . Yii::app()->user->id);
$newarr = array();
foreach ($alltasks as $data) {
    $newarr[] = $data['title'];
}
// $alltasklist =   implode(',',$newarr);
$alltasklist = json_encode($newarr);

//echo '<pre>';print_r($alltasklist);exit;
?>
<?php
$expmodel = TaskExpiry::model()->findByPk(1);
$fut_date=strtotime(date('Y-m-d'));
if($expmodel)
{
    $date = date("Y-m-d", strtotime("+" . $expmodel->day . " day"));
    $fut_date = strtotime($date);
}
//echo $fut_date;


$status_dropdown= CHtml::listData(Status::model()->findAll(
    array(
         'select' => array('sid,caption'),
         'condition' => 'status_type="task_status"',
         'order' => 'status_type',
         'distinct' => true
     )
), "sid", "caption");

$null_status=array(''=>'All');
$status_list=$status_dropdown+$null_status;
ksort($status_list); 

$task_owner_dropdown=  CHtml::listData(Users::model()->findAll(
    array(
        'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
        'order' => 'first_name',
        'distinct' => true
    )
), "userid", "first_name");
$null_task_owner=array(''=>'All');
$task_owner_list=$task_owner_dropdown+$null_task_owner;
ksort($task_owner_list); 

 $contractor_dropdown=CHtml::listData(Contractors::model()->findAll(
                            array(
                                'select' => array('id,contractor_title'),
                                'condition' => 'status=1',
                                'order' => 'contractor_title',
                                'distinct' => true
                            )
                    ), "id", "contractor_title");
    $null_contractor=array(''=>'All');
    $contractor_list=$contractor_dropdown+$null_contractor;
    ksort($contractor_list);  

$grid_params_array = array(
    'id' => 'tasks-grid',
    'dataProvider' => $model->assigned_task($type),
    'ajaxUpdate' => false,
    'template' => "<div class='clearfix'>
        <div class='pull-left'>{pager}</div>
        <div class='pull-right'>{summary}</div><div class='pull-right'>
        <i class=\"fa fa-check\" style=\"color:green\"></i>
            <span class='cont_pal'>Acknowledged&nbsp;&nbsp;</span>
            <span class='clr_pal bg_orange'></span><span class='cont_pal'>Expiring tasks&nbsp;&nbsp;</span>
            <span class='clr_pal bg_green'></span><span class='cont_pal'>New tasks&nbsp;&nbsp;</span>
            <span class='clr_pal bg_red'></span><span class='cont_pal'>Expired tasks&nbsp;&nbsp;</span>
            </div></div>\n{items}\n{pager}",
    'itemsCssClass' => 'table table-striped table-bordered table-hover mytask-table newcolor',
    'pager' => array(
        'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'filter' => $model,
    'rowCssClassExpression' => 'strtotime($data->due_date) < strtotime(date("Y-m-d")) ? "bg_red": ( strtotime($data->due_date) <=  ' . $fut_date . ' && strtotime($data->due_date) >= strtotime(date("Y-m-d")) ? "bg_orange" : "bg_green")',
    'afterAjaxUpdate' => 'function() { afterAjaxUpdate(); $("#Tasks_title").addClass("ui-autocomplete-input"); }',
    //  'rowCssClassExpression'=>'(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "higlight_backgrnd" : ""',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        // array('name' => 'tskid'),
        array(
            'class' => 'CButtonColumn',
            'template' => !isset($_GET['exportgrid']) ? (Yii::app()->user->role == 1 || Yii::app()->user->role == 9) ? '{view}{update}' : '{view}{update}' : '',
            'htmlOptions' => array('width' => '140px'),
            'buttons' => array(
                'addtime' => array(
                    'label' => '',
                    //  'url' => 'Yii::app()->createUrl("timeEntry/addtasktime", array("tskid"=>$data->tskid))',
                    'url' => '$data->tskid',
                    'options' => array('class' => 'icon-note icon-comn addstatus', 'title' => 'Add Status',),
                    'visible' => '$data->status != 7 && $data->status != 9 && $data->status != 5 && $data->due_date  >=  "' . date("Y-m-d") . '"',
                ),
                'view' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View'),
                ),
                'update' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Edit',),
                    //'visible'=>' Yii::app()->user->role==1',
                ),
                'subtask' => array(
                    'label' => '',
                    //'imageUrl' => Yii::app()->request->baseUrl . '/images/subtask.png',
                    'options' => array('class' => 'icon-docs icon-comn', 'title' => 'Sub task',),
                    'url' => 'Yii::app()->createUrl("Tasks/subtask", array("tskid"=>$data->tskid))',
                ),
            ),
        ),
        array(
            'name' => 'title',
            'value' => '($data->acknowledge_by=="")?$data->title:$data->title."<i class=\"fa fa-check\" style=\"margin-left:5px;color:green\"></i>" ',
            'type' => 'raw',
            // 'cssClassExpression' => '$data->reportTo->user_type ==1 ? "text-danger1" : ""',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Tasks[title]',
                'source' => $this->createUrl('Tasks/autocompletetask'),
                'value' => isset($model->title) ? $model->title : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) {
                           $("#Tasks_title").val(ui.item.value);
                        }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Tasks_title").val(ui.item.value); }'
                ),
            ), true),
        ),
        array(
            'name' => 'quantity',
            'header' => 'Target Quantity',
            // 'value'=>'$data->quantity',
            'value' => function ($model) {
                if (!empty($model->unit)) {
                    return $model->quantity . ' (' . $model->unit0->unit_title . ')';
                } else {
                    return $model->quantity;
                }
            }
        ),
        array(
            'name' => 'progress_quantity',
            'value' => '$data->getProgressQuantity($data->tskid)',
            'filter'=>false,
        ),
        array(
            'name' => 'task_duration',
            // 'value' => function ($model) {
            //     return $model->task_duration . ' days';
            // },
            'value' => function ($model) {
            
                return $this->getTaskDuration($model->start_date, $model->due_date, $model->project_id, $model->clientsite_id);
            },
            'type' => 'html',
            //'htmlOptions' => array( 'style' => 'width:20px;border-right:1px solid #c2c2c2;'),
        ),
        array(
            'name' => 'progress_percent',
            'value' => '$data->getcompleted_percent($data->tskid)."%"',
            'type' => 'html',
            'htmlOptions' => array('class' => 'width-20 light-gray-border-right')
        ),
        'daily_target',
        'required_workers',
        array(
            'name' => 'start_date',
            'value' => '($data->start_date!="")?date("d-M-Y",strtotime($data->start_date)):""',
            'filter' => false,
            'htmlOptions' => array('class' => 'wrap'),
            //'htmlOptions' => array('width' => '90px', 'style' => 'text-align:center')
        ),
        array(
            'name' => 'due_date',
            'value' => 'date("d-M-y",strtotime($data->due_date))',
            'filter' => false,
            'htmlOptions' => array('class' => 'wrap'),
            //'cssClassExpression' => '$data->status0->caption == "Open"? "text-danger" : ""',
            'cssClassExpression' => '(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "text-danger1" : ""'
        ),
        array(
            'name' => 'status',
            'header' => 'Status',
            'value' => '$data->status0->caption',
            'type' => 'raw',           
            'filter' => CHtml::activeDropDownList($model, 'status', $status_list)
        ),

        array(
            'name' => 'task_type',
            'value' => '($data->task_type ==1)?"External":"Internal"',
            //'filter' => array(1 => 'External', 2 => 'Internal'),
            'filter' => array('00000' => 'All', 1 => 'External', 2 => 'Internal'),
            //'type'=>'raw',
        ),
        array(
            'name' => 'report_to',
            'value' => '!empty($data->report_to)?$data->reportTo->first_name." ".$data->reportTo->last_name:""',
            'type' => 'raw',           
            'filter'=>CHtml::activeDropDownList($model,'report_to',$task_owner_list),
        ),
        array(
            'name' => 'contractor_id',
            'value' => '!empty($data->contractor_id)?$data->contractor->contractor_title:""',
            'type' => 'raw',
            // 'filter' => CHtml::listData(Contractors::model()->findAll(
            //     array(
            //         'select' => array('id,contractor_title'),
            //         'condition' => 'status=1',
            //         'order' => 'contractor_title',
            //         'distinct' => true
            //     )
            // ), "id", "contractor_title")
            'filter'=>CHtml::activeDropDownList($model,'contractor_id',$contractor_list),
        ),
       
    ),
);
$gridwidget = 'zii.widgets.grid.CGridView';
if (isset($_GET['exportgrid'])) {
    $gridwidget = 'application.core.grid.CGridView';
    // unset($gridcolumns[0]);   //Serial number column remove for export
    $export_grid_column_label = array(
        'S.NO.', 'TITLE', 'TARGET QUANTITY',
        'PROGRESS QUANTITY', 'TASK DURATION', 'PROGRESS %',
        'DAILY TARGET', 'REQUIRED WORKERS', 'START DATE', 'END DATE', 'STATUS',
        'TASK TYPE', 'TASK OWNER', 'CONTRACTOR'
    );

    $grid_params_array['export_grid_column_label'] = $export_grid_column_label;
    $grid_params_array['export_file_name'] = 'My Task report_' . date('d_M_Y-his') . ".csv";
}



//echo '<pre>';
//print_r($gridcolumns);
//die;

$this->widget($gridwidget, $grid_params_array);
//            echo '<pre>';
//            print_r($result_data);
if (!isset($_GET['exportgrid'])) {
?>

    <div class="popup_view display-none" id="view_content"></div>

    <?php
    //--------------------- begin new code --------------------------
    // add the (closed) dialog for the iframe
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'create-user-dialog',
        'options' => array(
            'title' => 'Add status',
            'autoOpen' => false,
            'modal' => false,
            'width' => 590,
            'height' => "auto",
        ),
    ));
    ?>
    <iframe id="create-user-frame" width="550" height="auto" frameborder="0" class="min-height-400"></iframe>

    <?php
    $this->endWidget();
    ?>
    <div id="id_view"></div>
    <?php
    Yii::app()->clientScript->registerScript('myjavascript22', '
            $(document).ready(function () {

                $("body").on("keyup",".filters > td > input", function() {
                $("#tasks-grid12").yiiGridView("update", {
                    data: $(this).serialize()
                });
                return false;
                });

           });
           $(document).delegate(".addstatus","click",function(event){
            event.preventDefault();
            var id = $(this).attr("href");
            $(this).attr("id",id);
                $.ajax({
                    method: "POST",
                    url:"' . Yii::app()->createUrl("timeEntry/addtasktime&layout=1&tskid=") . '" + id  ,
                }).done(function(ret){
                    //$("html, body").animate({scrollTop: 0}, 1000);
                    $("#view_content").html(ret).show();
                });
        });
');
    ?>




    <?php
    Yii::app()->clientScript->registerScript(
        'script',
        '
     
    //$(\'table tr:nth-child(1) td:nth-child(2)\').text();
    $(document).on("click", ".editicon", function (e) {
    e.preventDefault();

        var url = $(this).attr(\'href\');
        var src = url +\'&asDialog=1&gridId=address-grid\';
        $(\'#create-user-frame\').attr(\'src\', src);
        $(\'#create-user-dialog\').dialog(\'open\');


    });


	afterAjaxUpdate();
',
        CClientScript::POS_READY
    );
    ?>
</div>
    <script type="text/javascript">
        function afterAjaxUpdate() {
            var i = 0;
            $('table.items > tbody  > tr').each(function() {
                var bcol = $(this).find('td:eq(5)');
                var ecolumn = $(this).find('td:eq(8)');
                var bhrs = parseInt(bcol.html());
                var estihrs = parseInt(ecolumn.html());

                if (isNaN(bhrs) == false && isNaN(estihrs) == false) {
                    if (bhrs > estihrs)
                        ecolumn.css("background-color", "red");
                    else if (bhrs < estihrs)
                        ecolumn.css("background-color", "#FF9900");
                    else if (bhrs == estihrs)
                        ecolumn.css("background-color", "#46B525");
                }
            });
        }
    </script>

    <?php
    Yii::app()->clientScript->registerScript('myjavascript', '

    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });


  } );

');
    ?>
    <script>
        $(document).ready(function() {
            $('#list_change').on('change.bootstrapSwitch', function(e) {
                var status = e.target.checked;
                if (this.checked) {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: "closed"
                        },
                    });
                } else {
                    $.fn.yiiGridView.update('tasks-grid', {
                        data: {
                            type: ""
                        },
                    });
                }
            });
            $('.mytask-table').wrap('<div class="table-page-scroll-bar"></div>')
        });
    </script>
<?php
}
?>
<script>
   var url = new URLSearchParams(window.location.search);
   const urlParams = new URLSearchParams(url);
   const taskType = urlParams.get("Tasks[task_type]");
    
    if(taskType != "")
    {
        $('select[name="Tasks[task_type]"]').val(taskType);
    }
</script>