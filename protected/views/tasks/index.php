<?php
/* @var $this TasksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Tasks',
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        //array('label' => 'Manage Tasks', 'url' => array('admin')),
       //array('label' => 'Create Tasks', 'url' => array('create')),
    );
}

if (yii::app()->user->role == 6 or yii::app()->user->role == 4) {
    $this->menu = array(
        //array('label' => 'Create Tasks', 'url' => array('create')),
    );
}
?>
 
<div class="pull-left">
<h2>Tasks (<?php echo ($type=='wop'?'Without project':($type=='odt'?'Overdues':'All')) ;?>)</h2></div>

	


 
<br clear="all" />

<?php if(Yii::app()->user->role == 1)
//{
?>
 

<?php
//Yii::app()->clientScript->registerScript('checkBoxSelect', "
//function getSelectedTasks(){
//var taskids = [];
// $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
//            taskids[i]= $(this).val();
//          });
//       return \"&checkd_tsk=\"+taskids;
//   }
//");
//?>
    </div>
	<?php
//	}
	?>
<div class="green-color opacity-1 display-none font-weight-bold pull-leftclear:right;" id="ajax_project_assign_result">
&nbsp;
</div>
      <div class="row">
                <div class="col-lg-16"><!--
-->                    <div class="panel panel-default"><!--
-->                        <div class="panel-heading">
    Showing Tasks
    <div class="pull-right">
     <?php echo CHtml::link('ADD TASK',array('Tasks/create')); ?></div>
 </div>
    <br/>
 <!--
                         /.panel-heading 
-->                        <div class="panel-body"> 

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    
    'id' => 'tasks-grid',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'dataProvider' => $model->search($type),
    'filter' => $model,
     'htmlOptions' => array( "class"=>"width-100-percentage",'width'=>'100%','id'=>'dataTables-example_paginate'),
     'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
   'nextPageLabel'=>'Next ' ),
     
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
    'columns' => array(
        array(
            'id' => 'selected_tasks',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
            
        ),
        
        array('name' => 'tskid',  'htmlOptions' => array('class'=>'odd gradeX font-weight-bold text-align-center','width' => '20px')),
        array(
            'name' => 'project_id',
            'value' => '(isset($data->project->name)?$data->project->name:"")',
            'type' => 'raw',
           'htmlOptions' => array('class'=>'even gradeC'),
            'filter' => CHtml::listData(Projects::model()->findAll(
                            array(
                                'select' => array('pid,name'),
                                'order' => 'name',
                                'distinct' => true
                    )), "pid", "name")
        ),
        /*   array(
          'name' => 'work_type',
          'value' => '$data->worktype0->work_type',
          'type' => 'raw',
          'filter' => CHtml::listData(WorkType::model()->findAll(
          array(
          'select' => array('wtid, work_type'),
          'order' => 'work_type',
          'distinct' => true
          )), "wtid", "work_type")
          ), */
        'title',
        array(
            'name' => 'assigned_to',
            'value' => '$data->assignedTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
				'condition'=>'status=0',
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
        ),
    /*    array(
            'name' => 'report_to',
            'value' => '$data->reportTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
        ),*/
        array(
            'name' => 'coordinator',
            'value' => '$data->coordinator0->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
				'condition'=>'status=0',
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name"),
            'htmlOptions' => array('class' => 'light-gray-border-right')
        ),
        array(
            'header' => 'Billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"3\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
        ),
        array(
            'header' => 'Non-billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"4\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
        ),
        array(
            'header' => 'Total Hours',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
        ),
        array('name' => 'total_hrs',
            'header' => 'Estimated hrs',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
        ),
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="task_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
         array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
        ),
		 
         
    ),
));
?>
    <?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Task',
        'autoOpen' => false,
        'modal' => false,
        'width' => 700,
        'height' => 500,
    ),
));
?>
<iframe id="cru-frame" width="680" class="min-height-430"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>

<div/><!--
--><div/><!--
--><div/><!--
--><div/>

 
 <!-- /.row -->
 <div id="wrapper">
    <div class="row">
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Task
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Button trigger modal -->
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                 ADD TASk
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">TASK</h4>
                                        </div>
                                        <div class="modal-body">
                                            
    
                                              
                                               <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
 
                                             
                                        </div>
                                         
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
        <!-- DataTables JavaScript -->
     