<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs = array(
    'Tasks' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Tasks', 'url' => array('index')),
    array('label' => 'Create Tasks', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tasks-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tasks</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tasks-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('name'=>'tskid', 'htmlOptions' => array('width' => '40px','class'=>'font-weight-bold text-align:center')),
        array(
            'name' => 'project_id',
            'value' => '(isset($data->project->name)?$data->project->name:"")',
            'type' => 'raw',
            'filter' => CHtml::listData(Projects::model()->findAll(
                            array(
                                'select' => array('pid,name'),
                                'order' => 'name',
                                'distinct' => true
                    )), "pid", "name")
        ),
        'title',
 array(
            'name' => 'assigned_to',
            'value' => '$data->assignedTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
				'condition'=>'status=0',
                                'distinct' => true
                    )), "userid", "first_name")
        ),
        
        array(
            'name' => 'report_to',
            'value' => '$data->reportTo->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
				'condition'=>'status=0',
                                'distinct' => true
                    )), "userid", "first_name")
        ),
        

        array(
            'name' => 'coordinator',
            'value' => '$data->coordinator0->first_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name"),
            'htmlOptions' => array( 'class' => 'light-gray-border-right')
        ),        
//        'start_date',
//        'due_date',
 array(
            'header' => 'Billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"3\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
        ),
        array(
            'header' => 'Non-billable hrs',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "billable=\"4\" AND tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
        ),
        array(
            'header' => 'Total Hours',
            'value' => 'TimeEntry::model()->find(
                            array(
                                "select" => array("sum(hours) as hours"),
                                "condition" => "tskid=$data->tskid"
                    ))->hours',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
        ),
        array('name' => 'total_hrs',
               'header' => 'Estimated hrs',
            'htmlOptions' => array('width' => '40px', 'class' => 'light-gray-border font-weight-bold text-align-center')
            ),        
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="task_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        /*
          'priority',


          'billable',
          'total_hrs',
          'description',
          'created_date',
          'created_by',
          'updated_date',
          'updated_by',
         */
        array(
            'class' => 'CButtonColumn',
            'buttons' => array('view' =>
                array(
                    'url' => 'Yii::app()->createUrl("tasks/view", array("id"=>$data->tskid,"asDialog"=>1))',
                    'options' => array(
                        'ajax' => array(
                            'type' => 'POST',
                            // ajax post will use 'url' specified above 
                            'url' => "js:$(this).attr('href')",
                            'update' => '#id_view',
                        ),
                    ),
                ),
                'update' =>
                array(
                    'url' => '$this->grid->controller->createUrl("update", array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                    'click' => 'function(){$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                ),
            ),
        ),
    ),
));
?>

<?php
//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Update task',
        'autoOpen' => false,
        'modal' => false,
        'width' => 650,
        'height' => 570,
    ),
));
?>
<iframe id="cru-frame" width="674" height="500" scrolling="auto" class="overflow-x-hidden"></iframe>
<?php
$this->endWidget();
//--------------------- end new code --------------------------
//the dialog
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'dlg-tasks-view',
    'options' => array(
        'title' => 'Detail view',
        'autoOpen' => false, //important!
        'modal' => true,
        'width' => 850,
        'height' => 480,
    ),
));
?>
<div id="id_view"></div>

<?php
$this->endWidget();
//--------------------- end new code --------------------------
?>
