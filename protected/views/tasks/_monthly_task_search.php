<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>
<!--<h5>Filter By :</h5>-->
<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )
); ?>
<div class="monthly-task-search">
    <div class="row">
        <div class="col-sm-2">
            <?php echo $form->label($model, 'project_id', array('class' => 'font-bold')); ?>
            <?php
            if (Yii::app()->user->role == 1) {
                $condition = array('select' => array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
            } else {
                $criteria = new CDbCriteria;
                $criteria->select = 'project_id';
                $criteria->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria->group = 'project_id';
                $project_ids = Tasks::model()->findAll($criteria);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                }
                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $condition = array('select' => array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
                } else {
                    $condition = array('select' => array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'order' => 'name ASC');
                }
            }
            if (Yii::app()->user->project_id != "") {
                $model->project_id = Yii::app()->user->project_id;
                $project_id = Yii::app()->user->project_id;
            } else {
                $model->project_id = "";
                $project_id = "";
            }

            echo $form->dropDownList(
                $model,
                'project_id',
                CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'),
                array(
                    'empty' => 'Select Project',
                    'class' => 'form-control change_project',
                    'id' => 'project_id',
                )
            );

            ?>
        </div>
        <div class="col-sm-2">
            <?php echo $form->labelEx($model, 'report_to', array('class' => 'font-bold')); ?>
            <?php echo $form->dropDownList($model, 'report_to', CHtml::listData(Users::model()->findAll(array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'), 'condition' => 'status=0', 'order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => 'Select Task Owner', 'class' => 'form-control form-control-sm')); ?>

        </div>
        <div class="col-sm-2">

            <?php echo $form->labelEx($model, 'coordinator', array('class' => 'font-bold')); ?>
            <?php echo $form->dropDownList($model, 'coordinator', CHtml::listData(Users::model()->findAll(array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'), 'condition' => 'status=0', 'order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => 'Select Coordinator', 'class' => 'form-control form-control-sm')); ?>

        </div>
        <div class="col-sm-2">
            <?php echo $form->labelEx($model, 'priority', array('class' => 'font-bold')); ?>
            <?php echo $form->dropDownList($model, 'priority', CHtml::listData(Status::model()->findAll(array('select' => array('sid,caption'), 'condition' => 'status_type="priority"', 'order' => 'sid', )), 'sid', 'caption'), array('empty' => 'Select Priority', 'class' => 'form-control form-control-sm')); ?>

        </div>
        <div class="col-sm-2 search-buttons">
            <label></label>
            <?php echo CHtml::submitButton('Go', array('class' => 'btn btn-sm btn-blue search-form')); ?>
            <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-default btn-sm reset-button', 'onclick' => 'javascript:location.href="' . $this->createUrl('monthlyTask') . '"')); ?>
        </div>
    </div>
</div>




<?php $this->endWidget();


$project_id = "";
$owner = "";
$coordinator = "";
$priority = "";
if (isset($_GET['Tasks']['project_id'])) {
    $project_id = $_GET['Tasks']['project_id'];
}
if (isset($_GET['Tasks']['report_to'])) {
    $owner = $_GET['Tasks']['report_to'];
}
if (isset($_GET['Tasks']['coordinator'])) {
    $coordinator = $_GET['Tasks']['coordinator'];
}
if (isset($_GET['Tasks']['priority'])) {
    $priority = $_GET['Tasks']['priority'];
}

?>

<script>
    $(document).ready(function () {



        var url = new URLSearchParams(window.location.search);
        const urlParams = new URLSearchParams(url);

        var project_id = urlParams.get("Tasks[project_id]");

        var owner = urlParams.get("Tasks[report_to]");
        var coordinator = urlParams.get("Tasks[coordinator]");

        var priority = urlParams.get("Tasks[priority]");
        $("#project_id").val(project_id);
        $('#Tasks_report_to').val(owner);
        $('#Tasks_coordinator').val(coordinator);
        $('#Tasks_priority').val(priority);

    });


</script>