<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs = array(
    'Tasks' => array('index'),
    $model->title,
);

if (Yii::app()->user->role == 3) {
    // $this->menu = array(
    // array('label' => 'My Tasks', 'url' => array('mytask')),);
} else {

    $this->menu = array(
        //        array('label' => 'List Tasks', 'url' => array('index')),
        //        array('label' => 'Create Tasks', 'url' => array('create')),
        //        array('label' => 'Update Tasks', 'url' => array('update', 'id' => $model->tskid)),
        //        array('label' => 'Delete Tasks', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->tskid), 'confirm' => 'Are you sure you want to delete this item?')),
        //        array('label' => 'Manage Tasks', 'url' => array('admin')),
    );
}
?>


<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger alert-dismissable ">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
   <?php endif; ?>

<div class="view-section">
<div class="d-flex space-between w-100">

<h2>Task: <span><?php echo $model->title; ?></span></h2>
<div class="d-flex space-between">
    <div>
        <?php 
        $expired_condition = isset($model->due_date) && !empty($model->due_date) && strtotime($model->due_date) < strtotime(date("Y-m-d"));
            if($expired_condition){
                if($model->task_type==2 && $model->assigned_to == Yii::app()->user->id ){
                    echo CHtml::link('Add Time','' , array('class' => 'btn btn-primary addstatus'));
                }
                else if($model->task_type==1 && $model->assigned_to == Yii::app()->user->id)
                {
                    echo CHtml::link('Add Daily Work Progress',array('DailyWorkProgress/dailyProgress', 'type' => 4,'taskid'=>$model->tskid), array('class' => 'btn btn-primary'));
                }
            }
        ?>
    </div>
    <div class="margin-left-10">
        <?php
        if ($model->acknowledge_by == '') {
            echo CHtml::link('Acknowledge', array('Tasks/updateacknowledge&id=' . $model->tskid), array('class' => 'btn btn-primary '));
        } else {
            echo "<div class ='acknow'>Acknowledged</div>";
        }
        ?>
    </div>
</div>

</div>

<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<?php
        Yii::app()->clientScript->registerScript('myjavascript', '
            var selected_val = "'.Yii::app()->user->id.'";
            var taskid = "'.$model->tskid.'";
            selected_val = $.trim(selected_val);
            $(document).ready(function () {
              
                $(".addstatus").click(function () {                                   
                   $.ajax({
                       type: "POST",
                       data: {taskid:taskid},
                       url:"'.Yii::app()->createAbsoluteUrl("tasks/createexpire").'",
                       success: function (response)
                       {
                           $("#statusform").html(response).slideDown();
                           var selected_resource = selected_val;
                           $("#TimeEntry_user_id option:selected").removeAttr("selected");    
                           $("#TimeEntry_user_id").find("option[value=" + selected_resource + "]").attr("selected","selected");                          
                           $(".user_id_").val(selected_resource);
                           gettasks(selected_resource);
                        }
                   });
               });
               
               function gettasks(selected_val){
                $.ajax({
                    type: "POST",
                    data: {userid:selected_val,taskid:taskid},
                    dataType: "json",
                     url:"'.Yii::app()->createAbsoluteUrl("Tasks/gettasks").'",
                    success: function (response)
                    {                           
                        $("#TimeEntry_tskid").html(response.option);                           
                     }
                });
                }
                $(".edittime").click(function () {
                    var id = $(this).attr("data-id");
                    $.ajax({
                        type: "GET",
                         url:"'.Yii::app()->createAbsoluteUrl("Tasks/UpdateExpire&asDialog=1&id=" ).'" + id,
                        success: function (response)
                        {
                            $("#statusform").html(response).slideDown();
                            $("html, body").animate({scrollTop:$(".page-container").position().top}, "slow");
                            
                        }
                    });
                });      
                
                });
                ');


                ?>


<div id="ajaxloading"></div>

<div id="statusform" class="panel panel-primary"></div>
<div id="wpr_form_div" class="page-scroll-bar" style="display:none">

</div>
<hr />
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(

        array(
            'name' => 'parent_tskid',
            'type' => 'raw',
            'value' => (isset($model->parent_tskid) ? $model->gettask($model->parent_tskid) : '<span class="null">Not set</span>'),
        ),

        'title',
        array(
            'name' => 'project_id',
            'type' => 'raw',
            'value' => (isset($model->project) ? $model->project->name : '<span class="null">Not set</span>'),
        ),
        array(
            'name' => 'start_date',
            'type' => 'raw',
            'value' => isset($model->start_date) ? date('d-M-y', strtotime($model->start_date)) : "",
        ),
        array(
            'name' => 'due_date',
            'type' => 'raw',
            'value' => isset($model->due_date) ? date('d-M-y', strtotime($model->due_date)) : "",
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => isset($model->status0) ? $model->status0->caption : "",
        ),
        array(
            'name' => 'priority',
            'type' => 'raw',
            'value' => isset($model->priority0) ? $model->priority0->caption : "",
        ),
        // 'hourly_rate',
        array(
            'name' => 'assigned_to',
            'type' => 'raw',
            'value' => isset($model->assignedTo) ?  $model->assignedTo->first_name : "",
        ),
        array(
            'name' => 'report_to',
            'type' => 'raw',
            'value' => isset($model->reportTo) ?  $model->reportTo->first_name : "",
        ),
        array(
            'name' => 'coordinator',
            'type' => 'raw',
            'value' => isset($model->coordinator0) ?  $model->coordinator0->first_name : "",
        ),
        /*  array(
            'name' => 'billable',
            'type' => 'raw',
            'value' => $model->billable0->caption,
        ), */
        // 'total_hrs',
        array('name' => 'description', 'value' => nl2br($model->description), 'type' => 'raw'),
        array(
            'name' => 'created_date',
            'type' => 'raw',
            'value' => isset($model->created_date) ? date('d-M-y', strtotime($model->created_date)) : "",
        ),
        array(
            'name' => 'created_by',
            'type' => 'raw',
            'value' => isset($model->createdBy) ?   $model->createdBy->first_name : "",
        )
    ),
));


$criteria = new CDbCriteria();
$criteria->select = 'teid,tskid, entry_date as tentry_date , hours, billable, work_type, description,approve_status,current_status,reason_rejection';
$criteria->condition = 'tskid = :tskid';
$criteria->order = 'entry_date ASC';
$criteria->params = array('tskid' => $model->tskid);
if($expired_condition){
    $criteria->addCondition('timeentry_type=2');
}else{
    $criteria->addCondition('timeentry_type=1');
}
$weekDetailEntries = TimeEntry::model()->findAll($criteria);
Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".flash-success").animate({opacity: 1.0}, 2000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<hr />
<?php if($model->task_type==2){ ?>
<h2><span>Status Entries</span></h2>
<div class="table-responsive">
    <table cellpadding="3" cellspacing="0" class="table table-bordered timehours">
        <thead>
            <tr class="tr_title">
            <?php if($expired_condition && $model->task_type==2){?>
                <th width="600">Action</th>
                <?php } ?>
                <th width="300">Date</th>
                <th width="300">Current Status</th>
                <th width="600">Work description</th>
                <th width="200">Progress %</th>
                <th width="200">Reason For Rejection</th>
                <th width="600">Approval Status</th>
                
            </tr>
        </thead>
        <?php
        if (count($weekDetailEntries) == 0) {
        ?>
            <tr class="even">
                <td colspan="9">This week's timesheet does not have any time added.
                    Click add time to start adding time.</td>
            </tr>
        <?php
        }
        ?>
        <?php
        $total_hours = 0;
        $k = 1;
        $billable = 0;
        $non_billable = 0;

        foreach ($weekDetailEntries as $tdentry) {

            $total_hours += $tdentry->hours;
            //Edit link for each row
            if ($tdentry->billable == 3)
                $billable += $tdentry->hours;
            else
                $non_billable += $tdentry->hours;
            ?>
            <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
            <?php if($expired_condition && $model->task_type==2 ){
                   $editlink = CHtml::link('<i class="icon-pencil icon-comn black-color"></i>','', array('class' => 'edittime','title'=>'Edit',
                   'onclick' => "", "data-id"=> $tdentry->teid));
                    ?>
                    <td><?php if($tdentry->approve_status==0 && (($tdentry->created_by==Yii::app()->user->id) || ($model->assigned_to == Yii::app()->user->id))) { echo $editlink; }?></td>
                <?php } ?>

                <td><?php echo date('d-M-y', strtotime($tdentry->tentry_date)); ?></td>
                <td><?php if (!empty($tdentry->current_status)){
                    echo $tdentry->status->caption;
                } ?></td>
                <td><?php echo $tdentry->description; ?></td>
                <td><?php echo TimeEntry::model()->findByPk($tdentry->teid)->completed_percent; ?></td>
                <td><?php echo isset($tdentry->reason_rejection)?$tdentry->reason_rejection:''; ?></td>
                <td><?php if (!empty($tdentry->approve_status) &&  $tdentry->approve_status==1){
                    echo 'Approved';
                } else if(!empty($tdentry->approve_status) &&  $tdentry->approve_status==2){
                    echo 'Rejected';
                }else{
                    echo 'Pending';
                } ?>
                </td>
                
            </tr>

        <?php
            $k++;
        }
        ?>

    </table>
</div>
<?php } ?>

<h2><span>Sub Tasks</span></h2>
<div class="table-responsive ">
    <table cellpadding="3" cellspacing="0" class="table table-bordered timehours">
        <thead>
            <tr class="tr_title">
                <th width="100">Main Task id</th>
                <th width="100">Task id</th>
                <th width="300">Title</th>
                <th width="300">Project</th>
                <th width="100">Start Date</th>
                <th width="100">Due Date</th>
                <th width="100">Progress %</th>
                <th width="100">Current Progress</th>
            </tr>
        </thead>
        <?php
            $model->childtask($model->tskid);
        ?>
    </table>
</div>
<hr />
<?php
if($model->task_type==1){
    echo $this->renderPartial('_boq_list', array('boq_model' => $boq_model)); 
}
?>
</div>

<script>

$(".add-daily-work-progress").click(function () {  
    var taskid = '<?php echo $model->tskid ?>';
    $.ajax({
      "type": "POST",

      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/getexpiretaskdetails'); ?>",
      "dataType": "json",
      "data": {
        task_id: taskid
      },
      "success": function(data) {
        //$.fn.MultiFile();
        $('#wpr_form_div').show();
        $('#wpr_form_div').html(data);
      }
    });
});
$(document).ready(function(e) {
$('#statusform').hide();
});
</script>
    