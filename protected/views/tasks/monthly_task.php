<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script
    src="<?= $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?= $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?= Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?= Yii::app()->baseUrl; ?>/js/select2.js"></script>
<script src="<?= Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/onedrive.js" type="text/javascript">
</script>

<link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
      rel="stylesheet"
      type="text/css"/>
<link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css"
    />
  
<?php 
  
  $session = Yii::app()->session;
  $hideClass=$session['monthly_task_table_class']?$session['monthly_task_table_class']:[];
  $expandedTargets=isset($_GET['monthly_task_page'])?($session['expanded_targets']?$session['expanded_targets']:[]):[];
  unset($session['current_url']);
  unset($session['expanded_targets']);
?>
<!-- <div class="view-project-sec"> -->
<div class="container main-block ">

    <div class="clearfix project-filer">
            <?php
            $milestone_model = new Milestone();
            $this->renderPartial(
              '_monthly_task_search',
              array(
                'model' => $model
              )
            );
            ?>
    </div>


    <div id="create_wpr_form_div">

    </div>

    <!-- <div id="int_task_form" class="panel panel-primary"> -->

    <!-- </div> -->
      <div class="legend-show-hide">
        <!-- new line -->
        <div class="legends">
          <div class="tr1">Project</div>
          <div class="tr2">Budget Head</div>
          <div class="tr3">Milestone</div>
          <div class="tr4">Main Task</div>
          <div class="tr5">Subtask</div>
          <div><i class="fa-solid fa-clock clock-icon-wpr-entry"></i> WPR Entry</div>
          <div><i class="fa-solid fa-clock clock-icon-time-entry"></i> Time Entry</div>
        </div>
        <!-- new div starts-->
        <div class="button-group show-hide">
          <button type="button" class="btn-blue btn-sm dropdown-toggle" data-toggle="dropdown" id="dropdown-table-headers">
            <span class="anchor">Show/Hide Columns</span>
          </button>
          <ul class="dropdown-menu" id="dropdown-menu-list">
          
          </ul>
        </div>
        <!-- new div ends -->
      </div>
      <div class="table-responsive monthly-table-responsive">
        <table class="table mb-0">
          <thead>
            <tr class="table-top">
              <td rowspan="2" class="c1"></td>
              <td rowspan="2" class="c2" disabled><i class="fa-solid fa-angles-up"></i></td>
              <td rowspan="2" class="c3">DESCRIPTION OF <br />WORK</td>
              <td rowspan="2" class="c4">OWNER</td>
              <td rowspan="2" class="c5 white-space-nowrap ">CO-ORDINATOR</td>
              <td rowspan="2" class="c6">PRIORITY</td>
              <td rowspan="2" class="c7">TASK <br />DURATION</td>
              <td colspan="2" class="c8 text-center">SCHEDULED</td>
              <td colspan="2" class="c10 text-center">ACTUAL</td>
              <td rowspan="2" class="c12">
                PROGRESS <br />UPDATE<br />
                (DATE)
              </td>
              <td rowspan="2" class="c13 white-space-nowrap ">PROGRESS %</td>
              <td rowspan="2" class="c14">REMARKS</td>
              <td rowspan="2" class="c15">STATUS</td>
            </tr>
            <tr class="table-down">
              <td class="date-border c8">START DATE</td>
              <td class="date-border c9 end-date">END DATE</td>
              <td class="date-border c10">START DATE</td>
              <td class="date-border c11 end-date">END DATE</td>
            </tr>
          </thead>
          <tbody>
          <?php 
          foreach ($data_array as $key => $project) {
            $firstLevel = md5($key);
            if (count($project) > 0) { ?>
            <tr data-toggle="collapse" data-target="#<?= $firstLevel ?>" class="accordion-toggle level-1" >
            <td class="c1"></td>
            <td class="c2 arrow">
            <i class="fa-solid fa-arrow-up"></i>
            </td>
            <?php } else { ?>
            <tr class="level-1">
            <td class="c1"></td>
            <td class="c2 arrow"></td>
      <?php } ?>     
      <td class="c3" title="<?= $key ?>">
      <span class="work-description"><?= $key ?></span><?= CHtml::link('Add Task', array('createTask', 'project' => $key), array('class' => 'btn btn-sm add-task-btn tasks-project')); ?>
      </td>
      <td colspan="12"></td>
      </tr>
      <tr>
      <td colspan="15" class="hidden-row">
      <div class="accordian-body collapse" id="<?= $firstLevel ?>">
    <table class="table mb-0">
    <tbody>
    <?php
     $count = 1;
     foreach ($project as $key1 => $milestone) {
      $secondLevel = md5($key . $key1 .  $count);
      if (count($milestone) > 0) { ?>
            <tr data-toggle="collapse" class="accordion-toggle level-2" data-target="#<?= $secondLevel; ?>">
            <td class="c1"></td>
            <td class="c2 arrow">
            <i class="fa-solid fa-arrow-up"></i>
            </td>
        <?php } else { ?>
            <tr class="level-2">
            <td class="c1"></td>
            <td class="c2 arrow"></td>
        <?php } $count++;?>
        <td class="c3" title="<?= $key1 ?>">
        <span class="work-description"><?= $key1 ?></span>
        <?= CHtml::link('Add Task', array('createTask', 'project' => $key, 'budget_head' => $key1), array('class' => 'btn btn-sm add-task-btn tasks-project')); ?>
        </td>
        <td colspan="12" width="100%"></td>
        </tr>
        <tr>
        <td colspan="15" class="hidden-row">
        <div class="accordian-body collapse" id="<?= $secondLevel ?>">
        <table class="table mb-0">
        <tbody>
        <?php
        foreach ($milestone as $key2 => $milestones) {
          $milestone_id = $milestones['milestone_id'];
          $parent_task = $this->getParentTaskDet($milestone_id, $task_id_array);
          $milestone_duration = $this->getTimeDuration($milestones['start_date'], $milestones['end_date']);
          if (count($parent_task) > 0) { ?>
                <tr data-toggle="collapse" class="accordion-toggle level-3" data-target="#<?= 'milestone_'.$milestone_id ?>">
                <td class="c1"></td>
                <td class="c2 arrow">
                <i class="fa-solid fa-arrow-up"></i>
                </td>
            <?php } else { ?>
                <tr class="level-3">
                <td class="c1"></td>
                <td class="c2 arrow"></td>
            <?php } ?>      
            <td class="c3" title="<?= $key2 ?>">
            <span class="work-description"><?= $key2 ?></span>
            <?= CHtml::link('Add Task', array('createTask', 'project' => $key, 'milestone_id' => $milestone_id), array('class' => 'btn btn-sm add-task-btn tasks-project')); ?></td>
            <td class="c4"></td>
            <td class="c5"></td>
            <td class="c6"><i class="fa-solid fa-circle-exclamation priority-mid"></i>Medium</td>
            <td class="c7"><?= $milestone_duration ?></td>
            <td class="c8"><?= date("d-M-y", strtotime($milestones['start_date'])) ?></td>
            <td class="c9"><?= date("d-M-y", strtotime($milestones['end_date'])) ?></td>
            <td class="c10"></td>
            <td class="c11"></td>
            <td class="c12"></td>
            <td class="c13"></td>
            <td class="c14"></td>
            <td class="c15"></td>
            </tr>
            <tr>
            <td colspan="15" class="hidden-row">
            <div class="accordian-body collapse" id="<?='milestone_'.$milestone_id ?>">
            <table class="table mb-0">
            <tbody>
            <?php
            if (count($parent_task) > 0) {
              foreach ($parent_task as $parent) {
                $parent_id = $parent->tskid;
                $sub_task = $this->getSubTaskRecursive($parent_id);
                $parent_task_owner = $this->getName($parent->assigned_to);
                $parent_task_coordinator = $this->getName($parent->coordinator);
                $parent_task_priority = $this->getStatus($parent->priority);
                $parent_task_color = $this->getPriorityColor($parent->priority);
                $parent_task_duration = $this->getTimeDuration($parent->start_date, $parent->due_date);
                $parent_wpr_det = $this->getWrDetails($parent_id, $parent->quantity, $parent->task_type);
                $owner = "";
                $getWorkProgressDetails = $this->getWorkProgressDetails($parent->tskid, $parent->task_type);
                $checkTaskAssigned = $this->checkTaskAssigned($parent->tskid);
                $taskModalClass='';
                if ($parent['task_type'] == 1) {
                    $taskModalClass=($checkTaskAssigned == 1 && $parent->work_type_id != "")?'add-work-progress':'';
                } else {
                    $timeEntryAssigned = $this->checkTimeEntryAssigned($parent->tskid);
                    $taskModalClass= ($timeEntryAssigned == 1)?'add-time-entry':'';
                } 
                if (count($sub_task) > 0) { ?>
                        <tr data-toggle="collapse"class="accordion-toggle level-4"data-target="#<?= 'parent_task_'.$parent_id ?>">
                        <td class="c1"></td>
                        <td class="c2 arrow">
                        <i class="fa-solid fa-arrow-up"></i></td>
                    <?php } else { ?>
                        <tr class="level-4">
                        <td class="c1"></td>
                        <td class="c2 arrow"></td>
                    <?php } ?>  
                    <td class="c3" >
                    <?php if($taskModalClass=='add-time-entry'){ ?>
                      <i class="fa-solid fa-clock clock-icon-time-entry <?= $taskModalClass ?>"
                      data-toggle="modal"
                      data-target="#entryModal" id=<?= $parent->tskid ?> title="Add Time Entry"></i>
                      <?php }else if($taskModalClass=='add-work-progress' && $parent->status !=7){
                        echo CHtml::link(
                          CHtml::tag('i', array('class' => 'fa-solid fa-clock clock-icon-wpr-entry','title'=>'Add Work Progress'), ''),
                          array('DailyWorkProgress/dailyProgress', 'type' => 1,'monthly_task'=>1,'taskid'=>$parent->tskid),
                          array('class' => 'add-work-progress')
                        );
                      } ?>
                    <span class="work-description"><?= $parent->title ?></span>
                    <?= CHtml::link('Add Task', array('createTask', 'project' => $key, 'parent_task_id' => $parent_id), array('class' => 'btn btn-sm add-task-btn tasks-project')); ?>                                          </td>
                    <td class="c4"><?= $parent_task_owner ?></td>
                    <td class="c5"><?= $parent_task_coordinator ?></td>
                    <td class="c6">
                    <i class="fa-solid fa-circle-exclamation priority-<?= $parent_task_color ?>"></i><?= $parent_task_priority ?>
                    </td>
                    <td class="c7"><?= $parent_task_duration ?></td>
                    <td class="c8"><?= date("d-M-y", strtotime($parent->start_date)) ?></td>
                    <td class="c9"><?= date("d-M-y", strtotime($parent->due_date)) ?></td>
                    <td class="c10 <?= strtotime($parent_wpr_det[0])>strtotime($parent->start_date)?'past-date':'';?>"><?= $parent_wpr_det[0] ?></td>
                    <td class="c11 <?= strtotime($parent_wpr_det[1])>strtotime($parent->due_date)?'past-date':'';?>"><?= $parent_wpr_det[1] ?></td>
                    <td class="c12"><?= $parent_wpr_det[6] ?></td>
                    <td class="c13">
                    <?php $parent_progress = round($parent_wpr_det[2], 2); ?>
                    <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $parent_progress ?>"
                    aria-valuemin="0"
                    aria-valuemax="100"
                    style="width: <?= $parent_progress ?>%">
                    <?= $parent_progress . '%'; ?>
                    </div>
                    </div>
                    </td>
                    <td class="c14"><?= $parent_wpr_det[4] ?></td>
                    <td class="c15"><span class="<?= $parent_wpr_det[7] ?>"><?= $parent_wpr_det[3] ?></span></td>
                    </tr>
                    <tr>
                    <td colspan="15" class="hidden-row">
                    <div class="accordian-body collapse" id="<?= 'parent_task_'.$parent_id ?>">
                    <table class="table mb-0">
                    <tbody>
                    <?php if (count($getWorkProgressDetails) > 0) {
                      foreach ($getWorkProgressDetails as $progress) { ?>
                            <tr class="level-5 task-progress">
                            <td class="c1"></td>
                            <td class="c2"></td>
                            <td class="c3"></td>
                            <td class="c4"></td>
                            <td class="c5"></td>
                            <td class="c6"></td>
                            <td class="c7"></td>
                            <td class="c8"></td>
                            <td class="c9"></td>
                            <td class="c10"></td>
                            <td class="c11"></td>
                            <td class="c12"><?= date("d-M-y", strtotime($progress['progress_date'])) ?></td>
                            <td class="c13">
                            <div class="progress">
                            <div class="progress-bar progress-bar-success"
                            role="progressbar"
                            aria-valuenow="<?= $progress['progress_percentage']; ?>"
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style="width:<?= $progress['progress_percentage'] ?>%" >
                            <?= $progress['progress_percentage'] . " %" ?>
                            </div>
                            </div>
                            </td>
                            <td class="c14"><?= $progress['progress_comments'] ?></td>
                            <td class="c15"></td>
                            </tr>
                        <?php }
                    }
                    if (count($sub_task) > 0) {
                      foreach ($sub_task as $sub) {
                        $task_id = $sub->tskid;
                        $sub_task_owner = $this->getName($sub->assigned_to);
                        $sub_task_coordinator = $this->getName($sub->coordinator);
                        $sub_task_priority = $this->getStatus($sub->priority);
                        $sub_task_color = $this->getPriorityColor($sub->priority);
                        $sub_task_duration = $this->getTimeDuration($sub->start_date, $sub->due_date);
                        $sub_wpr_det = $this->getWrDetails($task_id, $sub->quantity, $sub->task_type);
                        $sub_task_status = $this->getStatus($sub->status);
                        $sub_status = $sub->status;
                        $due_date = $sub->due_date;
                        $expired_condition = $due_date < date('Y-m-d');
                        if (!$expired_condition && $sub_status != 7 || !$expired_condition && $sub_status == 7 || $expired_condition && $sub_status != 7) { ?>
                <?php
                $checkTaskAssigned = $this->checkTaskAssigned($sub->tskid);
                $modalClass='';
                if ($sub['task_type'] == 1) {
                  $modalClass=($checkTaskAssigned == 1 && $sub->work_type_id != "")?'add-work-progress':'';
                } else {
                  $timeEntryAssigned = $this->checkTimeEntryAssigned($sub->tskid);
                  $modalClass=($timeEntryAssigned == 1)?'add-time-entry':'';
                } ?>
                                <tr class="level-5">
                                <td class="c1"></td>
                                <td class="c2 arrow"></td>
                                <td class="c3" >
                                
                                <?php if($modalClass=='add-time-entry'){ ?>
                                  <i class="fa-solid fa-clock clock-icon-time-entry <?= $modalClass ?>"
                                  data-toggle="modal"
                                  data-target="#entryModal" id=<?= $sub->tskid ?> title="Add Time Entry"></i>
                                  <?php }else if($modalClass=='add-work-progress' && $sub->status !=7){
                                    echo CHtml::link(
                                      CHtml::tag('i', array('class' => 'fa-solid fa-clock clock-icon-wpr-entry','title'=>'Add Work Progress'), ''),
                                      array('DailyWorkProgress/dailyProgress', 'type' => 1,'monthly_task'=>1,'taskid'=>$sub->tskid),
                                      array('class' => 'add-work-progress')
                                    );
                                  } ?>
                                <span class="work-description"><?= $sub->title ?></span>
                                <?= CHtml::link('Add Task', array('createTask', 'project' => $key, 'sub_task_id' => $task_id), array('class' => 'btn btn-sm add-task-btn tasks-project')); ?>                                                                                                                                                                                                  
                                </td>
                                <td class="c4"><?= $sub_task_owner ?></td>
                                <td class="c5"><?= $sub_task_coordinator ?></td>
                                <td class="c6">
                                <i class="fa-solid fa-circle-exclamation priority-<?= $sub_task_color ?>"></i><?= $sub_task_priority ?>
                                </td>
                                <td class="c7"><?= $sub_task_duration ?></td>
                                <td class="c8"><?= date("d-M-y", strtotime($sub->start_date)) ?></td>
                                <td class="c9"><?= date("d-M-y", strtotime($sub->due_date)) ?></td>
                                <td class="c10 <?= strtotime($sub_wpr_det[0])>strtotime($sub->start_date)?'past-date':'';?>"><?= $sub_wpr_det[0] ?></td>
                                <td class="c11 <?= strtotime($sub_wpr_det[1])>strtotime($sub->due_date)?'past-date':'';?>"><?= $sub_wpr_det[1] ?></td>
                                <td class="c12"><?= $sub_wpr_det[6] ?></td>
                                <td class="c13">
                                <?php $subtask_progress = round($sub_wpr_det[2], 2)  ?>
                                <div class="progress">
                                <div class="progress-bar progress-bar-success"
                                role="progressbar"
                                aria-valuenow="<?= $subtask_progress ?>"
                                aria-valuemin="0"
                                aria-valuemax="100"
                                style="width: <?= $subtask_progress ?>%">
                                <?= $subtask_progress . ' %'; ?>
                                </div>
                                </div>
                                </td>
                                <td class="c14"><?= $sub_wpr_det[4] ?></td>
                                <td class="c15"><span class="<?= $sub_wpr_det[7] ?>"><?= $sub_wpr_det[3] ?></span></td>
                                </tr>
                                <?php
                                $getSubTaskWorkProgressDetails = $this->getWorkProgressDetails($sub->tskid, $sub->task_type);
                                if (count($getSubTaskWorkProgressDetails) > 0) {
                                  foreach ($getSubTaskWorkProgressDetails as $subprogress) { ?>
                                        <tr class="level-5 task-progress">
                                        <td class="c1"></td>
                                        <td class="c2"></td>
                                        <td class="c3"></td>
                                        <td class="c4"></td>
                                        <td class="c5"></td>
                                        <td class="c6"></td>
                                        <td class="c7"></td>
                                        <td class="c8"></td>
                                        <td class="c9"></td>
                                        <td class="c10"></td>
                                        <td class="c11"></td>
                                        <td class="c12"><?= date("d-M-y", strtotime($subprogress['progress_date'])) ?></td>
                                        <td class="c13">
                                        <div class="progress">
                                        <div class="progress-bar progress-bar-success"
                                        role="progressbar"
                                        aria-valuenow="<?= $subprogress['progress_percentage']; ?>"
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style="width:<?= $subprogress['progress_percentage'] ?>%" >
                                        <?= $subprogress['progress_percentage'] . " %" ?>
                                        </div>
                                        </div>
                                        </td>
                                        <td class="c14"><?= $progress['progress_comments'] ?></td>
                                        <td class="c15"></td>
                                        </tr> 
                                    <?php }
                                }
                        }
                      }
                    } ?>
                    </tbody>
                    </table>
                    </div>
                    </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
            </table>
            </div>
            </td>
            </tr>
        <?php } ?>
        </tbody>
        </table>
        </div>
        </td>
        </tr>
    <?php } ?>       
    </tbody>
    </table>
    </div>
    </td>
    </tr>
<?php } ?>       
</tbody>
        </table>
      </div>
        <!-- Modal -->
        <div
          class="modal modal-entry fade"
          id="entryModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="entryModalLabel"
        >
          <div class="modal-dialog" role="document">
              <div id="int_task_form" class="panel panel-primary"></div>
          </div>
        </div>   
</div>

<?php
$this->beginWidget(
  'zii.widgets.jui.CJuiDialog',
  array(
    'id' => 'cru-dialog',
    'options' => array(
      'title' => 'Add Task',
      'autoOpen' => false,
      'modal' => false,
      'width' => "590",
      'height' => "auto",
    ),
  )
);
?>
<iframe id="cru-frame" width="550" height="550" class="min-height-500"></iframe>

<?php
$this->endWidget();
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<script>
  $(document).ready(function() {

    // $('.add-work-progress').click(function () {
    //     var task_id = this.id;

    //     $.ajax({
    //         "type": "POST",

    //         "url": "<?php //echo Yii::app()->createUrl('dailyWorkProgress/monthtaskworkProgress'); ?>",
    //         "dataType": "json",
    //         "data": {
    //             task_id: task_id
    //         },
    //         "success": function (data) {

    //             $('#create_wpr_form_div').html(data);
    //         }
    //     });

    // });

 $(".add-time-entry").click(function () {
      pageData();
     var task_id = this.id;

        $.ajax({
            type: "GET",

            url: "<?= Yii::app()->createAbsoluteUrl("TimeEntry/addtimeentry&layout=1") ?>",
            data: {
                taskid: task_id
            },
            success: function (response) {
                $('#create_wpr_form_div').hide();
                $("#int_task_form").html(response).slideDown();

            }
        });

    });

    // $(function() {
    //     $(".weekly-report-table", "body").on({
    //             'click': function(event) {
    //                 event.preventDefault();
    //                 $(this).closest("tr.task-main").nextUntil("tr.task-main").toggle("fast");
    //             }
    //         },
    //         "tr.task-main", null);
    // });

 $(".tasks-project").click(function () {
        $.ajax({
            type: "GET",

            url: "<?= Yii::app()->createAbsoluteUrl("Tasks/unsetTaskSession") ?>",

            success: function (response) {
              pageData();
             }
        });

    });
    $(".tasks-budgethead").click(function () {
        $.ajax({
            type: "GET",

            url: "<?= Yii::app()->createAbsoluteUrl("Tasks/unsetTaskSession") ?>",

            success: function (response) { }
        });
    });


    $(".tasks-milestone").click(function () {
        $.ajax({
            type: "GET",

            url: "<?= Yii::app()->createAbsoluteUrl("Tasks/unsetTaskSession") ?>",

            success: function (response) { }
        });
    });
    $('#int_task_form').hide();
      $(".level-1 .c1").addClass("tr1");
      $(".level-2 .c1").addClass("tr2");
      $(".level-3 .c1").addClass("tr3");
      $(".level-4 .c1").addClass("tr4");
      $(".level-5 .c1").addClass("tr5");
      let expandedTargets = <?= json_encode($expandedTargets); ?>;
      expandedTargets.forEach(function(value){
        var expandButton = document.querySelector('[data-target="' + value + '"]');
        if(expandButton){
          expandButton.click();
          $(expandButton).children(".arrow").toggleClass("rotate");
        }
       });
      
      $(".level-1,.level-2,.level-3,.level-4,.level-5").click(function () {
        $(this).children(".arrow").toggleClass("rotate");

        var dataTarget = $(this).data('target');
        var isExpanded = $(this).attr('aria-expanded');
        if(!isExpanded){
          expandedTargets.push(dataTarget);
        }else{
          if ((pos = expandedTargets.indexOf(dataTarget)) !== -1) {
            expandedTargets.splice(pos, 1);
          }
        }        
      });
      
      var hiddenClasses = <?= json_encode($hideClass); ?>; // Ensure $hideClass is an array
      var options = hiddenClasses;

      if( hiddenClasses.length>0){
        hiddenClasses.forEach(function(element) {
        $("." + element).hide();
        element == 'c8' ? $(".c9").hide() : (element == 'c10' ? $(".c11").hide() : null);
        });
       }
  $("#dropdown-table-headers").on("click", function(event) {
   
    var tableHeaders = $(".table-top td");
    var dropdownMenu = $("#dropdown-menu-list");
    dropdownMenu.empty(); // Clear existing content before appending new items

    tableHeaders.each(function(index, element) {
      var headerText = $(element).text().trim();
      var classNames = $(element).attr("class").split(' ')[0];
      var checked = true;
      if (options.includes(classNames)) {
        var checked = false;
      }
      if (headerText !== '') {
        var li = $("<li>");
        var checkbox = $("<input>")
          .addClass("column-hide")
          .attr("data-value", classNames)
          .attr("type", "checkbox").prop("checked", checked);
        var span = $("<span>")
          .addClass("small")
          .append(checkbox, headerText);
        li.append(span);
        dropdownMenu.append(li);
      }
    });
  });

  $(document).on("click", "#dropdown-menu-list", function(event) {
    event.stopPropagation();
  });
  $(document).on("click", ".column-hide", function(event) {
    event.stopPropagation();
    var $target = $(event.currentTarget);
    var val = $target.attr("data-value");
    var idx;

    if ($target.prop("checked")) {
      if ((idx = options.indexOf(val)) !== -1) {
        options.splice(idx, 1);
      }
      val == 'c8' ? $(".c9").show() : (val == 'c10' ? $(".c11").show() : '');
      $("." + val).show();
    } else {
      val == 'c8' ? $(".c9").hide() : (val == 'c10' ? $(".c11").hide() : '');
      $("." + val).hide();
      options.push(val);
    }
    hiddenClasses = options;
 
  $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createAbsoluteUrl("Tasks/createColumnSession") ?>",
            data: { options: options },  // Pass the array as data
            success: function(response) {
                console.log('Session Created');
                // Handle success, if needed
            },
            error: function(error) {
                console.error('Error creating session:', error);
                // Handle error, if needed
            }
  });

  // return false;
});/*  */
function pageData(){
  var currentUrl=window.location.href;
  var verticalScrollPosition = $('.monthly-table-responsive').scrollTop();
  // Store the scroll position in localStorage
  localStorage.setItem('scrollPosition', verticalScrollPosition);
  $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createAbsoluteUrl("Tasks/createExpandSession") ?>",
            data: {expandedTargets: expandedTargets,currentUrl:currentUrl} , // Pass the array as data
            success: function(response) {
                console.log('Session Created');
                // Handle success, if needed
            },
            error: function(error) {
                console.error('Error creating session:', error);
                // Handle error, if needed
            }
  });
  return true;
}
$(document).on("click", ".add-work-progress", function(e) {
  e.preventDefault();
  var targetUrl = $(this).attr('href');
  var currentUrl=window.location.href;
  var verticalScrollPosition = $('.monthly-table-responsive').scrollTop();
  // Store the scroll position in localStorage
  localStorage.setItem('scrollPosition', verticalScrollPosition);
  $.ajax({
            type: 'POST',
            url: "<?= Yii::app()->createAbsoluteUrl("Tasks/createExpandSession") ?>",
            data: {expandedTargets: expandedTargets,currentUrl:currentUrl} , // Pass the array as data
            success: function(response) {
                console.log('Session Created');
                // Handle success, if needed
                window.location.href = targetUrl;

            },
            error: function(error) {
                console.error('Error creating session:', error);
                // Handle error, if needed
            }
  });
  return true;
});
var storedScrollPosition = localStorage.getItem('scrollPosition');
// Set the scroll position if it is available
if (storedScrollPosition !== null) {
  $('.monthly-table-responsive').scrollTop(parseInt(storedScrollPosition));
}
localStorage.removeItem('scrollPosition');

});

 
</script>