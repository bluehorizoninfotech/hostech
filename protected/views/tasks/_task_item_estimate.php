<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<div class="task-item-estimate-sec">
<h2>Item Estimate: <span><?= $task->title ?></span></h2>
<p id="create_success_message" class="display-none font-15 green-color font-weight-bold">Successfully created..</p>
<div class="panel panel-default ScrollStyle">
    <div class="panel-heading clearfix">


        <?php
        foreach ($template_items as $key => $item) {
            $item_estimate = $this->getItemEstimate($task->tskid, $item->id);
        ?>

            <div class="row">
                <div class="col-md-3">
                    <input type="hidden" value="<?= $task->tskid ?>" id="task_id">
                    <input type="hidden" value="<?= $project->template_id ?>" id="template_id">
                    <input type="text" class="form-control" value="<?= $item->item_name ?>" readonly id="item_id_<?= $key ?>">
                </div>

                <div class="col-md-3">
                    <input type="hidden" value="<?= $item_estimate['estimation_id'] ?>" id="item_estimation_id">
                    <input type="text" class="form-control item_qty" value="<?= $item_estimate['item_quantity_required'] ?>" name="<?= $item->id ?>" id="<?= $item_estimate['estimation_id'] ?>">

                </div>

            </div>
            <br>

        <?php } ?>

        <?php
        echo CHtml::link('Create', array('Tasks/index2'), array('class' => 'btn blue'));
        ?>
    </div>
</div>
        </div>
<script>
    $(".item_qty").change(function() {
        var estimate_id = this.id;
        var item_id = this.name;
        var $t = $(this);
        var item_estimate = $t.val();
        var task_id = $('#task_id').val();
        var template_id = $('#template_id').val();
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl("tasks/addItemEstimate") ?>',
            method: "POST",
            data: {
                estimate_id: estimate_id,
                item_id: item_id,
                item_estimate: item_estimate,
                task_id: task_id,
                template_id: template_id


            },
            "dataType": "json",
            success: function(result) {
                if (result.status == 1) {
                    $("#create_success_message").fadeIn().delay(1000).fadeOut();
                    setTimeout(location.reload.bind(location), 1000);
                } else {
                    alert('something went wrong');
                }

            }

        });
    });
</script>