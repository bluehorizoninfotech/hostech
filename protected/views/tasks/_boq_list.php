<?php

?>
<h2><span>Daily Work Progress Entries</span></h2>
<div class="table-responsive">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'daily-work-progress-grid',
        'htmlOptions' => array('class' => 'grid-view'),
        'itemsCssClass' => 'table table-bordered progress-table',
        'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',

        'pager' => array(
            'id' => 'dataTables-example_paginate',
            'header' => '',
            'prevPageLabel' => 'Previous ',
            'nextPageLabel' => 'Next '
        ),
        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
        'dataProvider' => $boq_model->task_search(),
        'ajaxUpdate' => false,
        'filter' => $boq_model,
        'selectableRows' => 2,
        'columns' => array(

            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'), ),
            array(
                'htmlOptions' => array('class' => 'white-space-nowrap'),
                'class' => 'ButtonColumn',
                'template' => '{Update}',
                'evaluateID' => true,
                'buttons' => array(
                    'Update' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'url' => 'Yii::app()->createUrl("/dailyWorkProgress/updateexpiredwpr", array("id"=>$data->id))',
                        'visible' => '(in_array("/dailyWorkProgress/update", Yii::app()->session["menuauthlist"]) && ($data->created_by == ' . Yii::app()->user->id . ' || Yii::app()->user->role == 1) && $data->work_progress_type==2)',
                        'options' => array('class' => 'icon-pencil  icon-comn margin_5 edit', 'title' => 'Edit', 'id' => '$data->id'),
                    ),
                )
            ),
            array(
                'name' => 'date',
                'value' => 'date("d-M-y", strtotime($data->date))',
                'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $boq_model,
                    'attribute' => 'date',
                    'options' => array('dateFormat' => 'd-M-y'),
                    'htmlOptions' => array('class' => 'width-100', 'autocomplete' => 'off'),
                ), true),

            ),
            array(
                'name' => 'qty',
                'value' => function ($data) {
                    if (!empty ($data->tasks->unit)) {
                        $unit = Unit::model()->findByPk($data->tasks->unit);
                        echo $data->qty . ' (' . $unit->unit_code . ')';
                    } else {
                        echo $data->qty;
                    }
                },
            ),

            array(
                'name' => 'work_type',
                'value' => '(isset($data->worktype)?$data->worktype->work_type:"")',
                'filter' => CHtml::listData(WorkType::model()->findAll(
                    array(
                        'select' => array('wtid,work_type'),
                        'order' => 'work_type',
                        'distinct' => true
                    )
                ), "wtid", "work_type")
            ),
            array(
                'name' => 'daily_work_progress',
                'value' => function ($data) {
                    echo round($data->daily_work_progress, 2);
                },
            ),

            array(
                'name' => 'approve_reject_data',
                'value' => function ($data) {
                    if ($data->approve_status != 1) {
                        echo $data->approve_reject_data;
                    }
                },
            ),
            'description',
            array(
                'name' => 'created_by',
                'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
                'visible' => Yii::app()->user->role == 1 || Yii::app()->user->role == 9,
                'filter' => CHtml::listData(Users::model()->findAll(
                    array(
                        'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                        'order' => 'first_name',
                        'distinct' => true
                    )
                ), "userid", "first_name")
            ),
            array(
                'name' => 'approve_status',
                'value' => function ($data) {
                    if ($data->approve_status == 0) {
                        return "Not Approved";
                    } elseif ($data->approve_status == 1) {
                        return "Approved";
                    } elseif ($data->approve_status == 2) {
                        return "Rejected";
                    } else {
                        return "";
                    }
                },
                'filter' => false
                // 'filter' => CHtml::listData(DailyWorkProgress::getApproveStatuses(), 'id', 'title'),
            ),

            array(
                'header' => 'Status',
                'value' => function ($data) {
                    if ($data->work_progress_type == 1) {
                        return "Active";
                    } else {
                        return "Expired";
                    }
                },
            ),



        ),
    )
    );
    ?>
</div>