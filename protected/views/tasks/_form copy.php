<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/query-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','js/jquery-ui.js');?>"></script>
<?php
if(!$model->isNewRecord && $model->parent_tskid ==""){
    $tasks = Tasks::model()->findAll(array('condition'=>'parent_tskid='.$model->tskid));
    $start_dates = array();
    $end_dates = array();
    foreach($tasks as $key=> $value){
        array_push($start_dates,$value->start_date);
        array_push($end_dates,$value->due_date);
    }
    $min = min($start_dates);
    $max = max($end_dates);
    $new_record = 2;
}else{
    $new_record = 1;
}

Yii::app()->getModule('masters');
/* @var $this TasksController */
/* @var $model Tasks */
/* @var $form CActiveForm */

$entry_date ='';
if( Yii::app()->controller->action->id == 'update'){
    $timentry = Yii::app()->db->createCommand("SELECT max(entry_date) as entry_date,count(*) as count  FROM `pms_time_entry` WHERE `tskid`=".$model->tskid)->queryRow();

    $entry_date = $timentry['entry_date'];
    if( $timentry['count']==0 && Yii::app()->user->role ==1 ){
       
        $readonly = false;
    }else{
       
        $readonly = true;
    }

}else{
   
    $readonly = false;
}
if($model->isNewRecord){
    $disable=false;
}else{
    $disable = true;
}
?>

<div class="form-copy-sec">  
<div class="form margin-top-5">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tasks-form',

        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>

    <?php //echo $form->errorSummary($model);?>


    <div class="row">
    <div class="subrow subrowlong">


<?php  echo $form->labelEx($model, 'project_id'); ?>
<?php
    if(isset(Yii::app()->user->role) && (in_array('/tasks/addprojectmaster', Yii::app()->session['menuauthlist']))){
?>
<a href="#" class="new_project">Add new project</a>
    <?php } ?>
<?php
  

if($model->isNewRecord){
    if(Yii::app()->user->project_id !=""){
        $model->project_id = Yii::app()->user->project_id;
        $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
        $model->start_date = $projects->start_date;
        $model->due_date = $projects->end_date;
    }else{
        $model->project_id = "";
    }
}

 echo $form->dropDownList($model,'project_id',CHtml::listData(Projects::model()->findAll(array( 'condition' => 'status="1"','order' => 'name ASC ')), 'pid', 'name'),
        array(
            'empty' => 'Choose a project',
            'class'=>'form-control project change_project','disabled' => $readonly,
            'ajax' => array
            (
            'type'=>'POST',
            'url'=> (Yii::app()->user->role == 10) ? CController::createUrl('Tasks/getclientlocationbyid') : CController::createUrl('Tasks/getclientlocation'), //or $this->createUrl('loadcities') if '$this' extends CController
            'update'=>'#Tasks_clientsite_id', //or 'success' => 'function(data){...handle the data in the way you want...}',
            'data'=>array('pid'=>'js:this.value'),
            )));
    ?>
<?php  echo $form->error($model, 'project_id'); ?>
</div>
</div>

<!-- Project section -->
<div class="row">
    <div class="subrow subrowlong">
        <div class="project_section display-none">
                <div class="form">
                <div class="clearfix">
                <a class="margin-top-10 black-color cursor-pointer close-panel  pull-right">X</a>
                <h4 class="subhead">Project</h4>
                </div>
                
                
                    <div class="row">
                        <div class="subrow">
                        <label for="Projects_name" class="required">Project name <span class="required">*</span></label>        <input class="form-control display-none" name="Projects[name]" id="Projects_name" type="text" maxlength="100">        <div class="errorMessage" id="Projects_name_em_"></div>    </div>
                        <div class="subrow">
                            <input class="btn blue project_save btn-sm" type="button" name="yt0" value="Create">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

<div class="row">
        <div class="subrow ">
            <?php echo $form->labelEx($model, 'Main Task'); ?>
            <?php
               $condition1 = '';

if(Yii::app()->user->role != 1){
    $proarr = array();
            $proarr1 = 0;
            if(Yii::app()->user->project_id !=""){
                $allprojects = Tasks::model()->findAll('project_id = '.Yii::app()->user->project_id);
            }else{
                $allprojects = Tasks::model()->findAll('assigned_to = '.Yii::app()->user->id);
            }

            if(!empty($allprojects)){
               foreach ($allprojects as $key => $value) {
                    if($value['project_id']!=''){
                       $proarr[]=$value['project_id'];
                   }
              }
            }

            if(!empty($proarr)){
               $proarr1 = implode(',', $proarr);
           }else{
             $proarr1 = 0;
           }

           $condition1 = 'project_id in ('.$proarr1.') AND assigned_to ='. Yii::app()->user->id.' or coordinator='.Yii::app()->user->id .' or created_by='.Yii::app()->user->id;
           if(!$model->isNewRecord){
                $condition1 = 'project_id = '.$model->project_id.' AND assigned_to ='. Yii::app()->user->id.' or coordinator='.Yii::app()->user->id .' or created_by='.Yii::app()->user->id ;           
            }
        }else{
            if(!$model->isNewRecord){
                $condition1 = 'project_id = '.$model->project_id;           
            }else{
                if(Yii::app()->user->project_id !=""){
                    $condition1 = 'project_id = '.Yii::app()->user->project_id;         
                }
            } 
        }
    
                 echo $form->dropDownList($model, 'parent_tskid', CHtml::listData(Tasks::model()->findAll(
                    array('condition' =>$condition1, 'order' => 'title ASC' )
                    ), 'tskid', 'title'), array('empty' => '--', 'id'=>'main_task','class'=>'form-control input-medium', 'class' => 'width-100-percentage'));

               //echo $form->dropDownList($model, 'parent_tskid', CHtml::listData(Tasks::model()->findAll(array('order' => 'title ASC')), 'tskid', 'title'), array('empty' => '--', 'style' => 'width:100%;'));
               ?>
            <?php echo $form->error($model, 'parent_tskid'); ?>
        </div>
        
        <div class="subrow ">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title', array('class'=>'form-control input-medium','readonly' => $readonly)); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>
    </div>
    <!-- *********************Task dependancy Section Starts ***********************************-->
  <input type="hidden" id="start_based_on_dependancy" val="<?= !$model->isNewRecord?$model->start_date:''?>">
  <?php  
  if(!$model->isNewRecord){
      $dependant_datas = TaskDependancy::model()->findAll(['condition'=>'task_id ='.$model->tskid]);
    $i=1;
    ?>
     <div class="dependant_section">
  </div>
    <div class="dependency_1 display-none">
                <div class="subrow subsmall">
                <label for="Tasks_Main_Task">Dependant Task</label> 
                <?php   
                 $options = CHtml::listData(Tasks::model()->findAll(['condition'=>'project_id = '.$model->project_id]), 'tskid', 'title');
                 echo CHtml::dropDownList('dependant_taskid[]','', $options, array('empty' => '--', 'class'=>'form-control input-medium dependant_taskid width-100-percentage'));
                ?>                                                     
                </div>
                <div class="subrow pos-rel subsmall">               
                    <label for="Tasks_dependancy_percenatge">Dependancy percentage</label>            <input class="form-control input-medium dependancy_percenatge" name="dependancy_percenatge[]" id="Tasks_dependancy_percenatge" type="text">                    
                </div> 
                <div class="subrow pos-rel subsmall">  
                <label for="Tasks_Main_Task">Dependant On</label>                                   
                    <select class="form-control input-medium dependancy_on" name="dependant_on[]" id="dependancy_type">
                        <option value="1">Start Date</option>
                        <option value="2">End Date</option>
                    </select>
                    <span class="close-dependency">X</span>
                </div> 
               
               
            
            </div>
            <?php
      foreach($dependant_datas as $key=> $data){
        ?>


        <div class="row dependant_data_div div_<?php echo $i; ?>" id="<?= $i;?>">
           
                <div class="subrow subsmall">
                <label for="Tasks_Main_Task">Dependant Task</label> 
                <?php   
                $depend_task_id = $data->dependant_task_id;
                 $options = CHtml::listData(Tasks::model()->findAll(['condition'=>'project_id = '.$model->project_id]), 'tskid', 'title');
                 echo CHtml::dropDownList('dependant_taskid[]',$depend_task_id, $options, array('empty' => '--', 'class'=>'form-control input-medium dependant_taskid width-100-percentage'));
                ?>                                                     
                </div>
                <div class="subrow pos-rel subsmall">               
                    <label for="Tasks_dependancy_percenatge">Dependancy percentage</label>            <input class="form-control input-medium dependancy_percenatge" name="dependancy_percenatge[]" id="Tasks_dependancy_percenatge" type="text" value="<?= $data->dependency_percentage?>"> 
                   
                </div>  
                <div class="subrow pos-rel subsmall">  
                <label for="Tasks_Main_Task">Dependant On</label>                                   
                    <select class="form-control input-medium dependancy_on" name="dependant_on[]" id="dependancy_type">
                        <option value="1" <?= $data->dependant_on == 1?'selected="selected"':''?>>Start Date</option>
                        <option value="2"  <?= $data->dependant_on == 2?'selected="selected"':''?>>Due Date</option>
                    </select>
                    <span class="close-dependency">X</span>
                </div> 
               
            
           
        </div>
        <?php
        $i++;
    }
  }else{ ?>
      <div class="dependency_1 display-none">
                <div class="subrow subsmall">
                <label for="Tasks_Main_Task">Dependant Task</label> 
                <?php 
                if(Yii::app()->user->project_id !=""){
                    $options = CHtml::listData(Tasks::model()->findAll(['condition'=>'project_id = '.$model->project_id]), 'tskid', 'title');
                }else{
                    $options = CHtml::listData(Tasks::model()->findAll(), 'tskid', 'title');
                }  
                 echo CHtml::dropDownList('dependant_taskid[]','', $options, array('empty' => '--', 'class'=>'form-control input-medium dependant_taskid width-100-percentage'));
                ?>                                                     
                </div>
                <div class="subrow pos-rel subsmall">               
                    <label for="Tasks_dependancy_percenatge">Dependancy percentage</label>            <input class="form-control input-medium dependancy_percenatge" name="dependancy_percenatge[]" id="Tasks_dependancy_percenatge" type="text"> 
                  
                </div>  
                <div class="subrow pos-rel subsmall">  
                <label for="Tasks_Main_Task">Dependant On</label>                                   
                    <select class="form-control input-medium dependancy_on" name="dependant_on[]" id="dependancy_type">
                        <option value="1">Start Date</option>
                        <option value="2">Due Date</option>
                    </select>
                    <span class="close-dependency">X</span>
                </div> 
                              
            
            </div>
  <div class="dependant_section">
  </div>
  <div class="row dependant_data_div div_1" id="1">
            <div class="dependency_1">
                <div class="subrow subsmall">
                <label for="Tasks_Main_Task">Dependant Task</label> 
                <?php   
                 if(Yii::app()->user->project_id !=""){
                    $options = CHtml::listData(Tasks::model()->findAll(['condition'=>'project_id = '.$model->project_id]), 'tskid', 'title');
                }else{
                    $options = CHtml::listData(Tasks::model()->findAll(), 'tskid', 'title');
                }  
                 echo CHtml::dropDownList('dependant_taskid[]','', $options, array('empty' => '--', 'class'=>'form-control input-medium dependant_taskid width-100-percentage'));
                ?>                                                     
                </div>
                <div class="subrow pos-rel subsmall">               
                    <label for="Tasks_dependancy_percenatge">Dependancy percentage</label>            <input class="form-control input-medium dependancy_percenatge" name="dependancy_percenatge[]" id="Tasks_dependancy_percenatge" type="text"> 
                   
                </div>
                <div class="subrow pos-rel subsmall">  
                <label for="Tasks_Main_Task">Dependant On</label>   
                    <select class="form-control input-medium dependancy_on" name="dependant_on[]" id="dependancy_type">
                        <option value="1">Start Date</option>
                        <option value="2">Due Date</option>
                    </select>
                    <span class="close-dependency">X</span>
                </div> 
                
               
            
            </div>
        </div>

  <?php

  }
  
  ?>

    <a  id="add_dependancy_div">ADD </a>






    <!-- *********************Task dependancy Section Ends ***********************************-->

    <div class="row">

        <?php



        $condition = '';

        if( Yii::app()->user->role !=1 ){


            $proarr = array();
            $proarr1 = 0;
            $allprojects = Tasks::model()->findAll('assigned_to = '.Yii::app()->user->id);

            if(!empty($allprojects)){
               foreach ($allprojects as $key => $value) {

                if($value['project_id']!=''){

                  $proarr[]=$value['project_id'];
                }
              }
            }

            if(!empty($proarr)){
               $proarr1 = implode(',', $proarr);
           }else{
             $proarr1 = 0;
           }

           $condition = ' pid in ('.$proarr1.')';


       }

        ?>

       


        <div class="subrow">


            <?php // echo $form->dropDownList($model,'clientsite_id',array('class'=>'form-control input-medium',)); ?>
            <?php  echo $form->labelEx($model, 'clientsite_id'); ?>

            <?php
            if(empty($location) && $location == 'create'){
               
            ?>
            <?php
		echo $form->dropDownList($model,'clientsite_id',CHtml::listData(Clientsite::model()->findAll(array('order' => 'site_name ASC')), 'id', 'site_name'),
                    array(
                        'empty' => 'Choose a client site','disabled' => $readonly,
                        'class'=>'form-control input-medium',));

					} else {
                        if(empty($model->clientsite_id)){
                            if(Yii::app()->user->project_id !=""){
                                $location = Clientsite::model()->findAll(array("condition"=>"pid = ".Yii::app()->user->project_id));
                            }else{
                                $location = Clientsite::model()->findAll();
                            }
                        }                      
                ?>

            <?php
                echo $form->dropDownList($model,'clientsite_id',CHtml::listData($location, 'id', 'site_name'),
                    array(
                        'empty' => 'Choose a client site','disabled' => $readonly,
                        'class'=>'form-control input-medium',
                        'ajax' => array
                        (
                        'type'=>'GET',
                        'url'=>CController::createUrl('Tasks/getassignedperson'), //or $this->createUrl('loadcities') if '$this' extends CController
                        'update'=>'#Tasks_assigned_to', //or 'success' => 'function(data){...handle the data in the way you want...}',
                        'data'=>array('id'=>'js:this.value'),
                        )

                        ));
                ?>

            <?php } ?>


            <?php  echo $form->error($model, 'clientsite_id'); ?>
        </div>
        <div class="subrow">
            <?php echo $form->labelEx($model, 'priority' ); ?>
            <?php
            echo $form->dropDownList($model, 'priority', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="priority"',
                                'order' => 'caption',
                                'distinct' => true
                            )), 'sid', 'caption'), array('empty' => 'Choose a priority','readonly' => $readonly,'class'=>'form-control input-medium'));
            ?>
            <?php echo $form->error($model, 'priority'); ?>
        </div>



    </div>




    <div class="row">
        <div class="subrow">
            <?php
//Default value for start and due date

    if (!isset($model->start_date))
        $model->start_date = date('d-M-y');

    if (!isset($model->due_date)) {
        $cd = strtotime(date('d-M-y'));
        $model->due_date = date('d-M-y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + 7, date('Y', $cd)));
    }
    $model->start_date = date('d-M-y',strtotime($model->start_date));
    $model->due_date = date('d-M-y',strtotime($model->due_date));
    ?>


            <?php echo $form->labelEx($model, 'start_date'); ?>
            <?php

            if($model->tskid!=''){
               echo CHtml::activeTextField($model, 'start_date', array('class'=>'form-control input-medium start_date',"id" => "Tasks_start_date" ,'autocomplete'=>"off"));
            }else{

                echo CHtml::activeTextField($model, 'start_date', array('class'=>'form-control input-medium start_date',"id" => "Tasks_start_date" ,'autocomplete'=>"off"));
            }
            ?>
            <?php
            // $this->widget('application.extensions.calendar.SCalendar', array(
            //     'inputField' => 'Tasks_start_date',
            //     'ifFormat' => '%Y-%m-%d',
            // ));
            ?>
            <?php echo $form->error($model, 'start_date'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'due_date'); ?>
            <?php
             if($model->tskid!=''){
                echo CHtml::activeTextField($model, 'due_date', array('class'=>'form-control input-medium due_date','readonly' => false,"id" => "due_date",'size'=>10,'autocomplete'=>"off"));
             }else{
                echo CHtml::activeTextField($model, 'due_date', array('class'=>'form-control input-medium due_date','readonly' => false,"id" => "due_date",'size'=>10,'autocomplete'=>"off"));
             }

            ?>
            <?php
            // $this->widget('application.extensions.calendar.SCalendar', array(
            //     'inputField' => 'due_date',

            //     'ifFormat' => '%Y-%m-%d',
            // ));
            ?>
            <?php echo $form->error($model, 'due_date'); ?>
        </div>

    </div>
    <div class="row">
<div class="date_section" id="test_sample"></div>
<div class="date_section_1" id="test_sample"></div>
</div>

    <div class="row">

        <?php
            $co_toggle = '';
            $rep_toggle = '';
            if (Yii::app()->user->role == 6) {
                if (!isset($model->coordinator)) {
                    $model->coordinator = Yii::app()->user->id;
                    $co_toggle = 'toggle-field';
                }
            } else {
                if (!isset($model->report_to)) {
                    $model->report_to = Yii::app()->user->id;
                    $rep_toggle = 'toggle-field';
                }
            }
            $members = Groups::model()->findAll(
                     array(
                         'select' => 'group_id',
                         'condition' => 'group_lead=' . Yii::app()->user->id,
                ));
                foreach ($members as $data){
                    $list = Groups_members::model()->findAll(
                         array(
                             'select' => 'group_id,group_members,id',
                             'condition' => 'group_id=' . $data['group_id'],
                    ));
                    foreach($list as $groupsmem) {
                        $gpmem[] = $groupsmem['group_members'];
                    }
                }
                $gpmem[] = Yii::app()->user->id;
            $gpmem = implode(',', $gpmem);
            ?>
        <div class="subrow">
            <?php echo $form->labelEx($model, 'assigned_to'); ?>
            <?php
                   $disstatus = false;
                   if(Yii::app()->user->role==1) {
                       if($model->isNewRecord){
                    				$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0','order' => 'first_name ASC');
                      			}else {
                      				$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'order' => 'first_name ASC');
                      			}

                       if(empty($assigned_to)){

                            echo $form->dropDownList($model,'assigned_to',CHtml::listData(Users::model()->findAll($condition), 'userid', 'full_name'),
                  				array(
                  					'empty' => 'Choose a resource','disabled' => $readonly,
                  					'class'=>'form-control input-medium',
                  					));

                        } else {

                					echo $form->dropDownList($model,'assigned_to',CHtml::listData($assigned_to, 'userid', 'whole_name'),
                                    array(
                                        'empty' => 'Choose a resource','disabled' => $readonly,
                                        'class'=>'form-control input-medium',
                                        ));

      		            }  } else {
                        $gpmem = array();
                         $allprojects = Tasks::model()->findAll('created_by = '.Yii::app()->user->id.' OR assigned_to = '.Yii::app()->user->id.' OR coordinator = '.Yii::app()->user->id.' OR report_to = '.Yii::app()->user->id);

                        if(!empty($allprojects)){
                           foreach ($allprojects as $key => $value) {
                                if($value['assigned_to']!=''){
                                   $gpmem[]=$value['assigned_to'];
                               }
                          }
                        }
                        $con = '1=1';
                        if(Yii::app()->user->role == 2 || Yii::app()->user->role == 4){
                        $members = Groups::model()->findAll(
                                 array(
                                     'select' => 'group_id',
                                     'condition' => 'group_lead=' . Yii::app()->user->id,
                            ));
                            foreach ($members as $data){
                                $list = Groups_members::model()->findAll(
                                     array(
                                         'select' => 'group_id,group_members,id',
                                         'condition' => 'group_id=' . $data['group_id'],
                                ));
                                foreach($list as $groupsmem) {
                                    $gpmem[] = $groupsmem['group_members'];
                                }
                            }
                            $gpmem[] = Yii::app()->user->id;
                        $gpmem = implode(',', $gpmem);
                        $con = "userid in(".$gpmem. ")";
                        }else{
                            $con = "userid = ".Yii::app()->user->id;
                            $disstatus  = true;
                        }
                        if($model->isNewRecord) {
                					$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => "status=0 AND $con",'order' => 'first_name ASC');
                        }else{
                					$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => "$con",'order' => 'first_name ASC');
                        }
                			 echo $form->dropDownList($model, 'assigned_to', CHtml::listData(Users::model()->findAll($condition), 'userid', 'full_name'), array('empty' => '-Choose a resource-','class'=>'form-control input-medium','disabled'=>  $disstatus)); ?>

            <?php } ?>
            <?php  echo $form->error($model, 'assigned_to'); ?>


        </div>

        <div class="subrow<?php echo $co_toggle; ?>">

            <?php echo $form->labelEx($model, 'coordinator'); ?>
            <?php echo $form->dropDownList($model, 'coordinator', CHtml::listData(Users::model()->findAll(array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0','order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => '-Choose co-ordinator-','disabled' => $readonly,'class'=>'form-control input-medium width-466')); ?>
            <?php echo $form->error($model, 'coordinator'); ?>

        </div>
    </div>
    <div class="row">
        <div class="subrow<?php //echo $rep_toggle; ?>">
            <?php echo $form->labelEx($model, 'report_to'); ?>
            <?php echo $form->dropDownList($model, 'report_to', CHtml::listData(Users::model()->findAll(array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0','order' => 'first_name ASC')), 'userid', 'full_name'), array('empty' => '-Choose a reporting person-','disabled' => $readonly,'class'=>'form-control input-medium width-466')); ?>
            <?php echo $form->error($model, 'report_to'); ?>
        </div>


        <div class="subrow">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php

            $consts = true;
           if ($model->coordinator == Yii::app()->user->id || Yii::app()->user->role == 1) {
               $consts = false;
           }

            if(Yii::app()->user->role <= 4){
                $condition_status = '1=1';
            }else{
                $condition_status = 'sid != 7';
            }
            echo $form->dropDownList($model, 'status', CHtml::listData(Status::model()->findAll(array(
                                'select' => array('sid,caption'),
                                'condition' => "status_type='task_status' AND $condition_status" ,
                                'order' => 'caption',
                                'distinct' => true
                            )), 'sid', 'caption'), array('class'=>'form-control input-medium','empty' => 'Choose a status','disabled' => $consts));
            ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>



    </div>
    <div class="row">
    <div class="subrow">
        <input type="hidden" name="sub_task_total_qty" id="sub_task_total_qty" value="0">
        <input type="hidden" name="parent_task_qty" id="parent_task_qty" value="0">
        <?php  if($model->isNewRecord){
            echo '<input type="hidden" name="task_primery_key" id="task_primery_key" value="0">';
        }else{
            echo '<input type="hidden" name="task_primery_key" id="task_primery_key" value='.$model->tskid.'>';
        }

        ?>
        <?php  
        if(!empty($model->parent_tskid) && $model->parent_tskid != ""){
            $remaining_qty =  Yii::app()->db->createCommand("select SUM(quantity) FROM pms_tasks WHERE parent_tskid = ".$model->parent_tskid)->queryScalar(); 
            $main_task_qunatity = Yii::app()->db->createCommand("select quantity FROM pms_tasks WHERE tskid = ".$model->parent_tskid)->queryScalar(); 
            $available = $main_task_qunatity - $remaining_qty;
           
        }
        ?>
        <label for="Tasks_unit">Quantity</label>
        <span id="remain_qty" class="red-color"><?php echo '(Available qty: '.$available.')' ?></span>
        <?php //echo $form->labelEx($model,'quantity'.' <span id="remain_qty"></span>'); ?>
        <?php echo $form->textField($model,'quantity',array('class'=>'form-control input-medium')); ?>
        <?php echo $form->error($model,'quantity'); ?>
    </div>
    <div class="subrow">
        <?php echo $form->labelEx($model, 'unit'); ?>
        <?php
            if(isset(Yii::app()->user->role) && (in_array('/tasks/addunitmaster', Yii::app()->session['menuauthlist']))){
        ?>
        <a href="#" class="new_unit">Add new unit</a>
        <?php } ?>
        <?php
        echo $form->dropDownList($model, 'unit', CHtml::listData(Unit::model()->findAll(array(
                            'select' => array('id,unit_title'),
                            'condition' => "status = 1" ,
                            'order' => 'unit_title',
                            'distinct' => true
                        )), 'id', 'unit_title'), array('class'=>'form-control input-medium','empty' => 'Choose a Unit'));
        ?>
        <?php echo $form->error($model, 'unit'); ?>
        
    </div>
    </div>

    <!-- unit section -->
    
    <div class="row">
        <div class="subrow subrowlong">
            <div class="unit_section display-none">
                <div class="form">
                    <div class="clearfix">
                    <a class="margin-top-10 black-color cursor-pointer close-panel2  pull-right">X</a>
                    <h4 class="subhead">Unit</h4>
                    </div>
                    <div class="row">
                    <div class="subrow subrowsmall">
                        <label for="Unit_unit_title" class="required">Unit Title <span class="required">*</span></label>            <input class="form-control input-medium" size="60" maxlength="100" name="Unit[unit_title]" id="Unit_unit_title" type="text"> 
                    <div class="errorMessage display-none" id="Unit_unit_title_em_"></div> 
                </div>
            <div class="subrow subrowsmall">
                <label for="Unit_unit_code" class="required">Unit Code <span class="required">*</span></label>        <input class="form-control input-medium" size="50" maxlength="50" name="Unit[unit_code]" id="Unit_unit_code" type="text">
                <div class="errorMessage display-none" id="Unit_unit_code_em_"></div>
            </div>
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input class="btn blue unit_save btn-sm" type="button" name="yt0" value="Create">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <div class="row">
    <div class="subrow">
        <?php echo $form->labelEx($model,'rate'); ?>
        <?php echo $form->textField($model,'rate',array('class'=>'form-control input-medium')); ?>
        <?php echo $form->error($model,'rate'); ?>
    </div>  
    <div class="subrow">
        <?php echo $form->labelEx($model,'amount'); ?>
        <?php echo $form->textField($model,'amount',array('class'=>'form-control input-medium','readonly'=>true)); ?>
        <?php echo $form->error($model,'amount'); ?>
    </div>                 
    </div>
    <div class="row">
        <div class="subrow">
        <?php 
        
        if($model->parent_tskid != NULL){          
            $parent_model = Tasks::model()->findByPk($model->parent_tskid);
            $model->milestone_id = !empty($parent_model)?$parent_model->milestone_id:'';

        }?>
            <?php echo $form->labelEx($model,'milestone_id'); ?>
            <?php
                if(isset(Yii::app()->user->role) && (in_array('/tasks/addmilestonemaster', Yii::app()->session['menuauthlist']))){
            ?>
            <a class="new_milestone" href="#">Add new Milestone</a>
           <?php } ?>
           <?php if(!empty($model->project_id)){
               $condition_ = "status = 1 AND project_id = ".$model->project_id;
           }else{
               $condition_ = "status = 1 "; 
           }
           ?>
            <?php echo $form->dropDownList($model, 'milestone_id', CHtml::listData(Milestone::model()->findAll(array(
                                'select' => array('id,milestone_title'),
                                'condition' => $condition_ ,
                                'order' => 'milestone_title',
                                'distinct' => true
                            )), 'id', 'milestone_title'), array('class'=>'form-control input-medium','empty' => 'Choose a Milestone','disabled'=>$model->parent_tskid != NULL?true:false));
            
            
            $form->textField($model,'milestone_id',array('class'=>'form-control input-medium')); ?>
            <?php echo $form->error($model,'milestone_id'); ?>
        </div>
        
        <div class="subrow">
        <?php echo $form->labelEx($model,'task_duration'); ?>
        <?php echo $form->textField($model,'task_duration',array('class'=>'form-control input-medium','readonly'=>true)); ?>
        <?php echo $form->error($model,'task_duration'); ?>
    </div>
    </div>

            <!-- milestone  --->
           
        <div class="row">
            <div class="subrow subrowlong">
            <div class="milestone_section display-none">
                <div class="form">
                    <div class="clearfix">
                        <a class="margin-top-10 black-color cursor-pointer close-panel3 pull-right">X</a>
                        <h4 class="subhead">Milestone</h4>
                    </div>
                    <div class="row">
                        <div class="subrow">
                            <label for="Milestone_milestone_title" class="required">Milestone Title <span class="required">*</span></label>		
                            	<input class="form-control input-medium display-none" size="60" maxlength="100" name="Milestone[milestone_title]" id="Milestone_milestone_title" type="text">	<div class="errorMessage" id="Milestone_milestone_title_em_"></div>
                        </div>
                        <div class="subrow">
			<label for="Milestone_project_id" class="required">Project <span class="required">*</span></label>
             <select class="form-control  input-small" name="Milestone[project_id]" id="Milestone_project_id">
            <?php
             if(Yii::app()->user->project_id !=""){
                $proj_id = Yii::app()->user->project_id;
                $projects = Projects::model()->findAll(array('condition'=>'pid ='.$proj_id));
             }else{
                $projects = Projects::model()->findAll(array('condition' => 'status="1"','order' => 'name ASC '));
                echo '<option value="">Choose a project</option>';
             }
               
                foreach($projects as $proj){
                    echo '<option value="'.$proj['pid'].'">'.$proj['name'].'</option>';
                }

            ?>
        </select>		
	<div class="errorMessage display-none" id="Milestone_project_id_em_"></div>           
        </div>
                        <div class="subrow">
                        <input class="btn blue add_milestone btn-sm" type="button" name="yt0" value="Create">
                    </div>
                </div>	    
            </div>
        </div>
            
            </div>
        </div>

    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($model,'daily_target'); ?>
            <?php echo $form->textField($model,'daily_target',array('class'=>'form-control input-medium')); ?>
            <?php echo $form->error($model,'daily_target'); ?>
        </div>
        <div class="subrow">
            <?php echo $form->labelEx($model,'work_type_id'); ?>
            <?php echo $form->dropDownList($model, 'work_type_id', CHtml::listData(WorkType::model()->findAll(array(
                                'select' => array('wtid,work_type'),
                                'order' => 'work_type ASC',
                                'distinct' => true
                            )), 'wtid', 'work_type'), array('class'=>'form-control input-medium','empty' => 'Choose a Work'));

                            ?>
            <?php echo $form->error($model,'work_type_id'); ?>
        </div>
    </div>
    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($model,'contractor_id'); ?>
            <?php echo $form->dropDownList($model, 'contractor_id', CHtml::listData(Contractors::model()->findAll(array(
                                'select' => array('id,contractor_title'),
                                'order' => 'contractor_title ASC',
                                'distinct' => true
                            )), 'id', 'contractor_title'), array('class'=>'form-control input-medium','empty' => 'Choose a Contractor'));

                            ?>
            <?php echo $form->error($model,'contractor_id'); ?>
        </div>
         <div class="subrow">
            <?php echo $form->labelEx($model,'required_workers'); ?>
            <?php echo $form->textField($model,'required_workers',array('class'=>'form-control input-medium','readonly'=>true)); ?>
            <?php echo $form->error($model,'required_workers'); ?>
        </div>    
      
    </div>
    <div class="row">
    <div class="subrow">
            <?php echo $form->labelEx($model,'allowed_workers'); ?>
            <?php echo $form->textField($model,'allowed_workers',array('class'=>'form-control input-medium')); ?>
            <?php echo $form->error($model,'allowed_workers'); ?>
        </div>
        <div class="subrow">
            <?php echo $form->labelEx($model,'task_type'); ?>
            <?php
            $accountStatus = array('1'=>'External', '2'=>'Internal');
            echo $form->radioButtonList($model,'task_type',$accountStatus,array('separator'=>' '));
            ?>
            <?php // echo $form->textField($model,'task_type',array('class'=>'form-control input-medium')); ?>
            <?php echo $form->error($model,'task_type'); ?>
        </div>
    </div>
    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email',array('class'=>'form-control input-medium')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
        <div class="subrow email_text">
           <small>An email will be sent to this id when the task reaches 100% and status changed to closed.</small>
        </div>
    </div>
    <div class="row">
        <div class="subrow subrowlong">

            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('rows' => 3, 'class'=>'form-control','readonly' => $readonly)); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
    </div>   

    <div class="form-group">
        <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn blue save_btn')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->
                        </div>
<script>
$(document).ready(function() {

    var date1 = '<?= $entry_date; ?>';
    var start_date_ = $('#Tasks_start_date').val();
    var dateToday =new  Date(start_date_); 
    $('.due_date').datepicker({
        dateFormat: 'd-M-y',
        minDate: dateToday,
        //minDate: date1,
      
    });
    $('.start_date').datepicker({
        dateFormat: 'd-M-y',
       

    });

});
</script>


<script>
$(document).ready(function(){
    
    daysduration();
    $('#Tasks_quantity').keyup(calculate);
    $('#Tasks_quantity').focusout(checkQuantity);
    $('#Tasks_quantity').keyup(dailytarget);
    $('#Tasks_rate').keyup(calculate);
     $('#Tasks_daily_target').focusout(checktarget);
    $('#Tasks_start_date').on('change', function (e) {
        daysduration();
        validatedate();
    });
    
    $('#due_date').on('change', function (e) {
        daysduration();
        validatedate();
    });
    $('#Tasks_allowed_workers').on('change', function (e) {
        allowedDatacheck() ;
    });
   

    $(document).on('change','.dependant_taskid',function(e){
        var element = $(this);
        var dependancy_percenatge =  $(this).closest(".dependant_data_div").find(".dependancy_percenatge").val();    
        if(dependancy_percenatge == ""){
            getdaterange(element);
        }
        dependantdatafetch();   
    });

    $(document).on('change','.dependancy_on',function(e){
        var element = $(this);
        var dependancy_percenatge =  $(this).closest(".dependant_data_div").find(".dependancy_percenatge").val();    
        if(dependancy_percenatge == ""){    
            getdaterange(element);
        }
       
    });

    function getdaterange(element){
        var task_id    =  element.closest(".dependant_data_div").find(".dependant_taskid").val();  
        var dependancy =  element.closest(".dependant_data_div").find(".dependancy_on").val();  
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('tasks/getdependancydate'); ?>',
            data:{task_id:task_id,dependancy:dependancy},            
            method: "GET",
            dataType:"json",
            success: function(result) {
                if(dependancy == 1){
                    $("#Tasks_start_date").val(result.start_date);
                    daysduration();
                    validatedate();
                }else{
                    $("#due_date").val(result.end_date);
                    daysduration();
                    validatedate();
                }
            }
        })
    }
    function allowedDatacheck(){
        var required_worker_count = $('#Tasks_required_workers').val();
       var allowed_count = $('#Tasks_allowed_workers').val();
       if(required_worker_count != "" && (allowed_count != "")){
            if(allowed_count < required_worker_count || allowed_count == 0){
                     $('.allowed_span').remove();
                    $(".save_btn").prop('disabled', true);
                    $("<span class='allowed_span'>value should be greater than required workers</span>").insertAfter("#Tasks_allowed_workers_em_");  
            }else{
                    $('.allowed_span').remove();
                    $(".save_btn").prop('disabled', false);
                }
       }    
    }

    // $('#due_date').change(daysduration);
    $('#Tasks_clientsite_id').on('change', function (e) {
        daysduration();
    });
    $('#Tasks_project_id').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;       
	    $.ajax({
            url: '<?php echo Yii::app()->createUrl('tasks/getMainTasks'); ?>',
            data:{project_id:valueSelected},            
            method: "GET",
            dataType:"json",
            success: function(result) {
                $('#main_task').html(result.option);
                $('.dependant_taskid').html(result.option);
                if(result.start_date !=""){
                    $("#Tasks_start_date").val(result.start_date);
                }
                if(result.end_date !=""){
                    $("#due_date").val(result.end_date);
                    
                    daysduration();
                }
            }
            	    })
    });
    $('#main_task').change(function(){               
        checkQuantity(); 
        getdate();       
    });
    $(document).on('change', ".dependancy_percenatge", function() {         
            dependantdatafetch();        
    });
    $(document).on('change', ".dependancy_on", function() {
        var dependancy_percenatge =  $(this).closest(".dependant_data_div").find(".dependancy_percenatge").val();           
        if(dependancy_percenatge != ""){
            dependantdatafetch();
        }        
    });
    $('#Tasks_work_type_id').on('change', function (e) {
        requiredworkers();
    }); 
    $('#Tasks_allowed_workers').on('change', function (e) {
        requiredworkers();
    });   
    $('#Tasks_start_date').on('change', function (e) {       
        if((new Date(this.value) <= new Date($('#dependnacy_genearted_date').val()) && ($("#Tasks_is_dependant").is(':checked'))))
        {         
           $("<span class='target_red '>Invalid Date</span>").insertAfter("#Tasks_start_date_em_");
           $(".save_btn").prop('disabled', true);
        }else{
            $('.target_red').remove();
            $(".save_btn").prop('disabled', false);
        }
    });

    /* mutliple dependency section */   
    $("#add_dependancy_div").click(function () {
        var data = $('.dependency_1').html();
        var div_no = $('.dependant_data_div').length;
        var last_div = div_no;
        div_no++;
        if(last_div == 0){
            $(".dependant_section").after('<div class="row dependant_data_div div_1" id="1">'+data+'</div>');
        }else{
            var div_class = 'div_'+div_no;
            $(".div_"+last_div).after('<div class="row dependant_data_div '+div_class+'" id="'+div_no+'">'+data+'</div>');
        }
        
    });
    $(document).on('click', ".close-dependency", function() { 
        var div_id = $(this).closest(".dependant_data_div").attr("id");  
        $('#'+div_id).remove();
        $('.start_based_on_dependancy').val('');
        dependantdatafetch();
    });

     /* mutliple dependency section */
    /* To calculate amount ie:- rate*Quantity */
    function calculate(e)
    {
        
        $('#Tasks_amount').val($('#Tasks_quantity').val() * $('#Tasks_rate').val());
    }

    /* Days between start and due date of task, here saturday and sundays are excluded */
    
    function daysduration(){
       
        dependancyDateCheck();
       var start_date =  $('#Tasks_start_date').val();
       var due_date =  $('#due_date').val();
       var site_id = $('#Tasks_clientsite_id').val();  
       var project_id=$('#Tasks_project_id').val();    
        // start_date = new Date(start_date);
        // due_date = new Date(due_date);
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('tasks/durationcalculation'); ?>',
            data:{due_date:due_date,start_date:start_date,site_id:site_id,project_id:project_id},            
            method: "GET",
            success: function(result) {
                $('#Tasks_task_duration').val(result); 
                dailytarget();   
            }
	    })       
       
    }

    
    function dailytarget(){
        if($('#Tasks_quantity').val() !== '')        {
            var daily_target = $('#Tasks_quantity').val() / $('#Tasks_task_duration').val();     
        }else{
            var daily_target = 0;
        }       
        $('#Tasks_daily_target').val( daily_target.toFixed(2)); 
        requiredworkers();
        allowedDatacheck();
    }

    function requiredworkers(){
        var selected_work_type = $('#Tasks_work_type_id').find(":selected").val();
        var workers_max_count = $('#Tasks_allowed_workers').val();       
        if(selected_work_type != ''){
            var daily_target =  $('#Tasks_daily_target').val();
            $.ajax({
            url: '<?php echo Yii::app()->createUrl('tasks/workerslimit'); ?>',
            data:{daily_target:daily_target,selected_work_type:selected_work_type,workers_max_count:workers_max_count},            
            method: "POST",
            dataType:"json",
            success: function(result) {
             if(result.status == 1){
                //  $('#Tasks_allowed_workers').val(result.allowed);
                 if(result.required != ''){
                    $('#Tasks_required_workers').val(result.required);
                 }  
                 allowedDatacheck()             ;
             }
            }
	    })
        }

    }
    function checktarget(e){
        var field_value = $('#Tasks_daily_target').val();
        var daily_target =$('#Tasks_quantity').val() / $('#Tasks_task_duration').val(); 
        if(field_value < daily_target){  
            $('.target_red').remove();
            $(".save_btn").prop('disabled', true);
            $("<span class='target_red'>Daily Target should not less than "+daily_target.toFixed(2)+"</span>").insertAfter("#Tasks_daily_target_em_");  
            e.preventDefault();                 
        }else{
            $('.target_red').remove();
            $(".save_btn").prop('disabled', false);
        }

    }
    
    
    function checkQuantity(e){
        var result_values;
        var valueSelectedMainTask = $('#main_task').find(":selected").val();
        var task_id = $('#task_primery_key').val();       
        $.ajax({
            async: false,
            dataType: "json",
            
            url: '<?php echo Yii::app()->createUrl('tasks/checktaskquantity'); ?>',
            data:{main_task_id:valueSelectedMainTask,task_id:task_id},            
            method: "GET",            
            success: function(result) {
                 result_values = jQuery.parseJSON(JSON.stringify(result));
                 var remain_qty = result_values.parent_quantity-result_values.sub_task_total_qty;
                
                $('#parent_task_qty').val(result_values.parent_quantity);
                $('#sub_task_total_qty').val(result_values.sub_task_total_qty);
                // if(remain_qty != ""){
                //     $('#remain_qty').text(' (Available qty: '+remain_qty+')');
                // }
               
                
               
            }
	    });
        var quantity = $('#Tasks_quantity').val();
        var parent_qty = $('#parent_task_qty').val();
        var sub_task_total_qty= $('#sub_task_total_qty').val();
       if(sub_task_total_qty !== ''){
        var present_qty= (parseFloat(sub_task_total_qty)+parseFloat(quantity));
       }else if(quantity !== ''){
        var present_qty= parseFloat(quantity);
       }else{
        var present_qty= 0;
       }             
        if(valueSelectedMainTask === ''){               
            if (parseFloat(quantity) < parseFloat(result_values.sub_task_total_qty)){
                $(".save_btn").prop('disabled', true);
                $('.target_red').remove();
                $("<span class='target_red '>Eneterd Quantity less than sum of sub task</span>").insertAfter("#Tasks_quantity_em_");
                e.preventDefault();
            } else{
                $(".save_btn").prop('disabled', false);
                $('.target_red').remove();
            }                     
        }
        else if (parseFloat(present_qty.toFixed(2)) > parseFloat(parent_qty)){
            $(".save_btn").prop('disabled', true);
            $('.target_red').remove();
            $("<span class='target_red '>Quantity exceeded main task </span>").insertAfter("#Tasks_quantity_em_");
            e.preventDefault();
        }
        else{
            $(".save_btn").prop('disabled', false);
            $('.target_red').remove();
        }
    }
  
    function dependantdatafetch(){ 
        var parent_task_id =  $('#main_task').find(":selected").val();
        var project_id =  $('#Tasks_project_id').find(":selected").val();    
        var task_ids = $("select[name='dependant_taskid[]']")
              .map(function(){return $(this).val();}).get();
        var depndant_on_types = $("select[name='dependant_on[]']")
              .map(function(){return $(this).val();}).get();
        var percentage_values = $("input[name='dependancy_percenatge[]']")
              .map(function(){return $(this).val();}).get();                    
        $.ajax({
            async: false,                      
            url: '<?php echo Yii::app()->createUrl('tasks/dependantdata'); ?>',
            data:{task_ids:task_ids,percentage_values:percentage_values,project_id:project_id,depndant_on_types:depndant_on_types,parent_task_id:parent_task_id},  
            dataType:'json',         
            method: "POST",            
            success: function(result) { 
                if(result.start_date != ""){
                    $('#Tasks_start_date').val(result.start_date); 
                    $('#start_based_on_dependancy').val(result.start_date);
                }
                if(result.end_date != ""){
                    $('#due_date').val(result.end_date); 
                    
                }
                
                    daysduration();    
              if(result.error != undefined){     
                                             
                    $(".date_section_1").html('<span style="color:red;padding-left: 12px;">'+result.error+'</span>');
                }
               
            }
	    });

    }
    function getdate(e){
        var result_values;
        var maintask_id = $('#main_task').find(":selected").val();
        var project_id = $('#Tasks_project_id').val();       
        $.ajax({
            async: false,
            dataType: "json",
            
            url: '<?php echo Yii::app()->createUrl('tasks/getmaintaskdate'); ?>',
            data:{maintask_id:maintask_id,project_id:project_id},            
            method: "GET",            
            success: function(result) {
                if(result.start_date !=""){
                    $("#Tasks_start_date").val(result.start_date);
                }
                if(result.end_date !=""){
                    $("#due_date").val(result.end_date);
                }
                //  result_values = jQuery.parseJSON(JSON.stringify(result));
                // $('#parent_task_qty').val(result_values.parent_quantity);
                // $('#sub_task_total_qty').val(result_values.sub_task_total_qty);
               
            }
	    });
    }

    function dependancyDateCheck(){        
        var dependancy_calculated_start_date = $('#start_based_on_dependancy').val();
        var dependancy_calculated_start_date = new Date(dependancy_calculated_start_date);
        var current_date_ = $('#Tasks_start_date').val();
        var current_date = new Date($('#Tasks_start_date').val());  
        
        if (current_date < dependancy_calculated_start_date){       
      var task_ids = $("select[name='dependant_taskid[]']")
              .map(function(){return $(this).val();}).get();
        var percentage_values = $("input[name='dependancy_percenatge[]']")
              .map(function(){return $(this).val();}).get();                  
        $.ajax({
            async: false,                      
            url: '<?php echo Yii::app()->createUrl('tasks/dateChangeValidation'); ?>',
            data:{task_ids:task_ids,percentage_values:percentage_values,current_date:current_date_},  
            // dataType:'json',         
            method: "POST",            
            success: function(result) {                 
                $(".date_section_1").html('<span style="color:red;padding-left: 12px;">'+result+'</span>');
            }
	    });
    }else{
        $(".date_section_1").html(''); 
    }     
    }
});
$(".new_project").click(function(){
    $('.project_section').show();
});
$(".project_save").click(function(){
    var Projects_name = $("#Projects_name").val();
    if(Projects_name !=''){
        $("#Projects_name_em_").hide();
        $("#Projects_name_em_").html('');
        $.ajax({
            'url':'<?php echo Yii::app()->createAbsoluteUrl('tasks/createproject'); ?>',
            data:{Projects_name:Projects_name},
            dataType:'Json',
            type:'GET',
            success:function(result){
                if(result.status == 1){
                    $("#Tasks_project_id").html(result.option);
                    $("#Tasks_project_id").val(result.lastid);
                    $('.project_section').hide();
                }
            }
        })

    }else{
        if(Projects_name == ''){
            $("#Projects_name_em_").show();
            $("#Projects_name_em_").html('Project name cannot be blank.');
        }
    }
})

$(".new_unit").click(function(){
    $('.unit_section').show();
});

$(".unit_save").click(function(){
    var title = $("#Unit_unit_title").val();
    var code  = $("#Unit_unit_code").val();
    if(title !='' && code !=''){
        $("#Unit_unit_title_em_").hide();
            $("#Unit_unit_title_em_").html('');
            $("#Unit_unit_code_em_").hide();
            $("#Unit_unit_code_em_").html('');
        $.ajax({
            'url':'<?php echo Yii::app()->createAbsoluteUrl('tasks/createunit'); ?>',
            data:{title:title,code:code},
            dataType:'Json',
            type:'GET',
            success:function(result){
                if(result.status == 1){
                    $("#Tasks_unit").html(result.option);
                    $("#Tasks_unit").val(result.lastid);
                    $('.unit_section').hide();
                }
            }
        })

    }else{
        if(title == ''){
            $("#Unit_unit_title_em_").show();
            $("#Unit_unit_title_em_").html('Unit Title cannot be blank.');
            $("#Unit_unit_code_em_").show();
            $("#Unit_unit_code_em_").html('Unit Code cannot be blank.');
        }
    }
})

$(".new_milestone").click(function(){
    $(".milestone_section").show();
}) 

$(".add_milestone").click(function(){
    var title = $("#Milestone_milestone_title").val();
    var project_id = $("#Milestone_project_id").val();
    if(title !=''){
        $("#Milestone_milestone_title_em_").hide();
            $("#Milestone_milestone_title_em_").html('');
        $.ajax({
            'url':'<?php echo Yii::app()->createAbsoluteUrl('tasks/createmilestone'); ?>',
            data:{title:title,project_id:project_id},
            dataType:'Json',
            type:'GET',
            success:function(result){
                if(result.status == 1){
                    $("#Tasks_milestone_id").html(result.option);
                    $("#Tasks_milestone_id").val(result.lastid);
                    $('.milestone_section').hide();
                }
            }
        })

    }else{
        if(title == ''){
            $("#Milestone_milestone_title_em_").show();
            $("#Milestone_milestone_title_em_").html('Milestone Title cannot be blank.');
        }
    }
})
$(".close-panel").click(function(){
    $(".project_section").hide();
})
$(".close-panel2").click(function(){
    $(".unit_section").hide();
})
$(".close-panel3").click(function(){
    $(".milestone_section").hide();
})

function validatedate(){
    var project_id = $("#Tasks_project_id").val();
    var start_date = $("#Tasks_start_date").val();
    var end_date = $("#due_date").val();
    var main_task = $("#main_task").val();
    if(project_id !=''){
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('tasks/taskdatevalidation'); ?>',
                data:{project_id:project_id,start_date:start_date,end_date:end_date,main_task:main_task},            
                method: "GET",
                dataType:"json",
                success: function(result) {
                    if(result.flag == 1){
                        $(".date_section").html('<span style="color:red;padding-left: 12px;">'+result.msg+'</span>');
                        $(".save_btn").prop('disabled', true);
                    }else{
                      
                        $(".date_section").html('');
                        $(".save_btn").prop('disabled', false);
                    }
                }
        })
    }
}

$(".save_btn").click(function(){
    var text = $(".date_section").text();
    var text_1 = $(".date_section_1").text();
   if(text == "" && text_1 == ""){
        $("#tasks-form").submit();
   }
})
</script>