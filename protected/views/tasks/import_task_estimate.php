<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css" />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Task Estimate:
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-3 display-flex grid-col-gap-1">
                        <select class='form-control  height-34' id="project_id">
                            <option value="">Choose Project</option>
                            <?php
                            foreach ($project as $projects) { ?>

                                <option value="<?= $projects->pid ?>"><?= $projects->name ?></option>

                            <?php }
                            ?>
                        </select>



                        <p id="create_success_message" class="font-15 green-color font-weight-bold">Successfully created..</p>

                        <div>
                            <p id="validation_message_div" class="red-color font-weight-bold"></p>
                        </div>

                        <div>
                            <p id="validation" class="red-color font-weight-bold"></p>
                        </div>



                        <!-- <button id="generate_handson_tbl" class="btn btn-primary">Go</button> -->
                    </div>
                </div>

                <br>

                <!--  start handson tbl-->
                <div class="row">
                    <div class="col-lg-12">
                        <div id="example" class="width-100-percentage"></div>
                        <br>
                        <div class="controls">

                            <button id="save" class="btn btn-primary">Save Data</button>
                        </div>
                    </div>
                </div>

                <!-- end handson tble -->
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('change', '#project_id', function() {
        var project_id = $('#project_id').val();
        const autosave = false;
        const hot = [];

        const container = document.getElementById('example');

        $.ajax({
            method: "POST",
            data: {
                project_id: project_id
            },
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('tasks/checkProject'); ?>',
            success: function(data) {

                if (data.status == 2) {
                    $('#project_id').val('');
                    
                    $('#validation').hide();
                    $('#validation_message_div').show();
                    $('#validation_message_div').text("Template not defined");
                } else {
                    $('#validation_message_div').hide();

                    $.ajax({
                        method: "POST",
                        data: {
                            project_id: project_id
                        },
                        "dataType": "json",
                        url: '<?php echo Yii::app()->createUrl('tasks/getHandsontbl'); ?>',
                        success: function(data) {
                            if (data.status == 1) {
                                $('#generate_handson_tbl').hide();
                                $('#save').show();
                                $('#validation').hide();
                                $('#example').empty();
                                const hot = new Handsontable(container, {

                                    data: Handsontable.helper.createSpreadsheetData(),
                                    data: data.task_array,
                                    colHeaders: data.data,
                                    customBorders: true,
                                    stretchH: 'all',
                                    licenseKey: 'non-commercial-and-evaluation',
                                    afterChange: function(change, source) {
                                        if (source === 'loadData') {
                                            return; //don't save this change
                                        }

                                        if (!autosave.checked) {
                                            return;
                                        }
                                    }
                                });

                                // disable column

                                hot.updateSettings({
                                    cells(row, col) {
                                        const cellProperties = {};

                                        if (col === 0) {
                                            cellProperties.readOnly = true;
                                        }
                                        if (col === 1) {
                                            cellProperties.readOnly = true;
                                        }

                                        return cellProperties;
                                    }
                                });

                                //    save data

                                Handsontable.dom.addEvent(save, 'click', () => {

                                    var records = hot.getData();
                                    var head = hot.getColHeader();
                                    var template_id = data.template_id;

                                    $.ajax({
                                        method: "POST",
                                        data: {
                                            records: records,
                                            head: head,
                                            template_id: template_id
                                        },
                                        "dataType": "json",
                                        url: '<?php echo Yii::app()->createUrl('tasks/CreateHandsontable'); ?>',
                                        success: function(data) {

                                            if (data.status == 1) {
                                                $("#create_success_message").fadeIn().delay(1000).fadeOut();
                                                setTimeout(location.reload.bind(location), 1000);
                                            }
                                        }
                                    });
                                });
                            } else {
                                $('#validation').show();
                                $('#validation').text("There is no task in this project");
                            }
                        }
                    });
                }
            }
        });
    });


    $('#save').hide();
    $('#create_success_message').hide();
</script>