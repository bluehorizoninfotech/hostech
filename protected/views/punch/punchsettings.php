<?php
/* @var $this HolidaysController */
/* @var $model Holidays */


$this->menu=array(
	array('label'=>'List Holidays', 'url'=>array('index')),
	array('label'=>'Create Holidays', 'url'=>array('create')),

	array('label'=>'Manage Holidays', 'url'=>array('admin')),
);
?>
<div class="row">
<div class="col-md-6 col-md-offset-2">
<h1>Punch Settings</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'punch-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="late_halfday">Fields with <span class="required">*</span> are required.</p>

	

	<div class="row">
		<?php echo $form->labelEx($model,'halfday'); ?>
		<?php echo $form->textField($model,'halfday',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'halfday'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'minimum_in_time_half_day'); ?>
		<?php echo $form->textField($model,'minimum_in_time_half_day',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'minimum_in_time_half_day'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'minimum_in_time_full_day'); ?>
		<?php echo $form->textField($model,'minimum_in_time_full_day',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'minimum_in_time_full_day'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>