<?php
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/punching_attendance.css');
?>
<div class="index-punch-sec">

<div class='pagecontainer'>


    <div class="mainpanel">

        <div class="titlearea">
            <h2>Daily Punch Report</h2>
        </div>

        <div class='cust_pagenation'>
            <?php
            echo CHtml::link('Next', array('punchresult/generateresult', 'days' => (isset($_GET['days']) ? intval($_GET['days']) + 1 : 1)));
            echo CHtml::link('Today', array('punchresult/generateresult', 'days' => 0));
            echo CHtml::link('Previous', array('punchresult/generateresult', 'days' => (isset($_GET['days']) ? intval($_GET['days']) - 1 : -1)));
            ?>
        </div>
        <div class="datearea">
            <?php echo CHtml::beginForm($this->createUrl('punchresult/generateresult'), 'get', array('class' => 'form form-inline')); ?>
            <?php echo CHtml::label('Date', '', array('class' => 'control-label')); ?>
            <?php echo CHtml::textField('logdate', $this->pdate, array("id" => "c_button3", 'class' => 'width-100 line-height-20 font-14 light-yellow-bg padding-5', 'autocomplete' => 'off', 'class' => 'form-control')); ?>

            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'c_button3',
                'button' => 'c_button3',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>
            <?php echo CHtml::submitButton('Submit', array('name' => 'logsearch', 'class' => 'searchtask btn btn-default')); ?>
            <?php echo CHtml::endForm(); ?>
        </div>
        <table cellpadding='0' cellspacing='0' id='punchresulttable'>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Emp Code</th>
                    <th>Name</th>
                    <th >Status</th>
                    <th>First Punch</th>
                    <th>Last Punch</th>
                    <th>IN time</th>
                    <th>OUT time</th>
                    <th>Tot Time</th>
                    <th>Tot Punch</th>
                    <th>Tot Break Time</th>
                    <th>Outside Time</th>
                    <th>Shift</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                $pdateindex = date('Ymd', strtotime($this->pdate));
               
                //echo '<pre>';print_r($this->punchresult);exit;

                foreach ($this->punchresult as $item) {

                    
                    $punchres = $item['punches'][$pdateindex];
                    extract($item['empdetails']);
                    extract($punchres['presult']);

                    // echo '<pre>';print_r($punchres['shift_punch']);exit;

                    $shift= (isset($punchres['shift_details']['shift']))? $punchres['shift_details']['shift']:'';
                    $shift_punches = (isset($punchres['shift_punch'] ))? $punchres['shift_punch']:'';

                    $newarr = array();
               
                    if(!empty($shift_punches)){
                        foreach ($shift_punches as $val) {
                            $newarr[strtotime($val)] = json_encode($val);
                        }

                    }
                   
                    if($totpunch <= 10) {
                        $pix = 10;
                        $color = 'green';
                    } else {
                        $pix = $totpunch;
                        $color = '#960000';
                    }

                    $firstpunchsec = $this->timetosec($firstpunch);
                    $lastpunchsec = $this->timetosec($lastpunch);
                    $before6pm = '';

                    if ($lastpunchsec < 64800 and $totpunch > 0) {
                        $before6pm = " class='before6pm'";
                    }

                    // $att = '';
                    // $attclass = '';
                    // $after10am = '';
                    // if ($totpunch == 0) {
                    //     $att = 'L';
                    //     $attclass = 'leave';
                    // } elseif (($firstpunchsec > 36000 and $user_type > 2) or ( $firstpunchsec > 41400 and $user_type <= 2)) {
                    //     $att = 'LHL';
                    //     $attclass = 'leave';
                    //     $after10am = 'class="after10am"';
                    // } else {
                    //     $att = 'P';
                    //     $attclass = 'present';
                    // }

                    $tottime = $intime + $outtime;

                    $less9hrs = '';
                    if ($totpunch > 0 and $tottime < 32400) {
                        $less9hrs = 'class="less9hrs"';
                    }
                    ?>
                    <tr class="userpunchrow">
                        <td><?php echo $i; ?></td>
                        <td><?php echo ($empcode == '') ? '' : $empcode ?></td>
                        <td><?php echo $fullname; ?></td>
                        <td><?php echo $status ?></td>
                        <td><?php echo $firstpunch ?></td>
                        <td><?php echo $lastpunch ?></td>
                        <td><?php echo ($intime > 0 ? gmdate("H:i:s", $intime) : '-') ?></td>
                        <td><?php echo ($outtime > 0 ? gmdate("H:i:s", $outtime) : '-') ?></td>
                        <td><?php echo ($intime > 0 ? gmdate("H:i:s", $tottime) : '-') ?></td>
                        <td class='totpunch'><?php echo ($totpunch > 0 ? $totpunch : '-') ?>
                        <span class='totpunchnew'><?php  
                            if(!empty($newarr)){
                                echo "[" . (implode(",", $newarr)) . "]";
                            }else{
                                echo '[]';
                            }
                        ?></span>
                        </td>
                        <td><?= (isset($break_time ) && $break_time!='' )? gmdate("H:i:s", $break_time) :' - ' ?></td>
                        <td><?= (isset($outside_punch) && $outside_punch != '') ? gmdate("H:i:s", $outside_punch) : ' - ' ?></td>
                        <td><?php echo $shift; ?></td>
                    </tr>
                    <?php $i++; } ?>
                </tbody>

            <tfoot>

            </tfoot>

        </table>
    </div>
        <div class="rightpanel">
            Hover on Punch count to display Punches
        </div>
    </div>
                        </div>
<script>

    $(document).ready(function(){

       //  $('.userpunchrow').hover(function(){

        $('.userpunchrow').on('click', function(){

            var punchdata = $.parseJSON($(this).find('.totpunch span').text());
            var punchlist = '<h4>' + $(this).find('td:nth-child(3)').html() + '</h4>';
            if(punchdata.length > 0){

                $.each(punchdata, function (i, item) {

                    punchlist += "<li>" + item + " <b>" + (i % 2 == 0 ? 'IN' : 'OUT')  + "</b></li>";

                });
            }else{
                punchlist += "<li><b>No Punches</b></li>";
            }
            $('.rightpanel').html("<ol>" + punchlist + "</ol>");
        });

    });

    function timetodate(unixtimestamp) {

       

        // Months array
        var months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        // Convert timestamp to milliseconds
        var date = new Date(unixtimestamp * 1000);

        // Year
        var year = date.getFullYear();

        // Month
        var month = months_arr[date.getMonth()];

        // Day
        var day = date.getDate();

        // Hours
        var hours = date.getHours();

        // Minutes
        var minutes = "0" + date.getMinutes();

        // Seconds
        var seconds = "0" + date.getSeconds();

        // Display date time in MM-dd-yyyy h:m:s format
        var convdataTime = day + '-' + month + '-' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

        return convdataTime;

    }

</script>
