
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php 

if(isset(Yii::app()->user->attendance_type))
{
$astatus=Yii::app()->user->attendance_type;
}
else
$astatus=3;

?>

<div class='row'></div>

<!--<div class="circle punchout" style="margin:0px auto; float:left;">--</div>-->
<!--<br clear='all'/>-->
<div class="width-100-percentage">

  

<div class="punchreportexport-sec">
            <h1>Punching log</h1>

                
                    

              

                <td colspan="4"  class="text-align-right">

                    <?php
                    // url change

                    $user = Yii::app()->db->createCommand("SELECT * FROM `pms_users` where userid=" . Yii::app()->user->id)->queryRow();

                    

                   
                     

                     if(isset(Yii::app()->user->skill_id_filter)){
                        $my_sql="SELECT * FROM `pms_employee_skill` where skill_id=" . Yii::app()->user->skill_id_filter;
        
                        $designation_name = Yii::app()->db->createCommand($my_sql)->queryRow();
                    }
                    else{
                        $designation_name['skill_name']="Not Selected";
                    }

?>
 <p>Skill : <?= $designation_name['skill_name']?></p>
                </td>

              

               </tr>
                <tr><td class="none-border"><h5 class="strong-blue">Punch Date : <?php echo date('d-m-Y',strtotime($this->pdate))?></h5></td></tr>

         
</div>

<div class="pull-left width-100-percentage" id="parent">

    <table cellpadding="10" cellspacing="0" border="1" class="logtable " id="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Emp Code</th>
                <th>Name</th>
                <th >Status</th>
                <th>First Punch</th>
                <th>Last Punch</th>
                <th>IN time</th>
                <th>OUT time</th>
                <th>Tot Time</th>
                <th>Tot Punch</th>
                <th>Tot Break Time</th>
                <th>Over Time</th>
                <th>Shift</th>
            </tr>
        </thead>
        <tbody class="newlog">
            <?php
            $i = 1;
            $l=1;
            $pdateindex = date('Ymd', strtotime($this->pdate));
            $user_punches = '';

    // echo '<pre>';print_r($this->punchresult);exit;

            if (!empty($this->punchresult)) {

                foreach ($this->punchresult as $item) {
                    $punchres = $item['punches'][$pdateindex];
                    extract($item['empdetails']);
                    extract($punchres['presult']);
                if ($firstpunch != '-'&&$astatus==3)
                    {
                        $l++;
                    }
                    $k=$l;
                }
                foreach ($this->punchresult as $item) {


                    $date1 = date('Y-m-d',strtotime($pdateindex));
                    // $userid = $item['empdetails']['userid'];
                   

                    $punchres = $item['punches'][$pdateindex];
                    extract($item['empdetails']);
                    extract($punchres['presult']);

                    $shift = (isset($punchres['shift_details']['shift'])) ? $punchres['shift_details']['shift'] : 'No shift';
                    $color_code = (isset($punchres['shift_details']['color_code'])) ? $punchres['shift_details']['color_code'] : '';
                    $shift_punches = (isset($punchres['shift_punch'])) ? $punchres['shift_punch'] : '';
                    $outside_punches = (isset($punchres['outside_punch'])) ? $punchres['outside_punch'] : '';
                    $standby_punches = (isset($punchres['standby_punches'])) ? $punchres['standby_punches'] : '';
                    $ignored_punches = (isset($punchres['ignored_punches'])) ? $punchres['ignored_punches'] : '';
                    $manual_entry = (isset($punchres['manual_entry'])) ? $punchres['manual_entry'] : '';
                    $shift_temp_no_ot = (isset($punchres['shift_details']['no_ot']) && $punchres['shift_details']['no_ot']==1) ? 1 : 0;


                    // manual ot

                    $manual_ot = Yii::app()->db->createCommand("SELECT manual_ot  FROM `pms_daily_overtime` WHERE userid = " . $userid . " and date='" . date('Y-m-d', strtotime($pdateindex)) . "' ")->queryRow();


                    if (!isset($punchres['shift_details']['shift'])) {

                        $res = Yii::app()->db->createCommand("SELECT pms_shift_assign.shift_id,shift_name, user_id, att_date, shift_starts, shift_ends,grace_period_before,grace_period_after,color_code FROM `pms_shift_assign`left join pms_employee_shift on pms_employee_shift.shift_id = pms_shift_assign. shift_id WHERE `user_id` = " . $userid . " and `att_date` = '" . date('Y-m-d', strtotime($pdateindex)) . "' ")->queryRow();
                        if (!empty($res)) {
                            $shift = $res['shift_name'];
                        }
                    }

                    $newarr = array();
                    if (!empty($shift_punches)) {
                        foreach ($shift_punches as $val) {
                            $newarr[strtotime($val['date'])] = json_encode($val);
                        }
                    }

                    if (empty($shift_punches) && isset($manual_entry) && !empty($manual_entry)) {
                        $dat = '';
                        foreach ($manual_entry as $val) {
                            $dat = array('date' => $val, 'device_id' => 1, 'dname' => 'Manual', 'ignore' => 'green');
                            $newarr[strtotime($val)] = json_encode($dat);
                        }
                    }


                    //  if(!empty($standby_punches)) {
                    if ($standby_id != 0) {

                        $newarr = array();
                        foreach ($standby_punches as $val) {
                            $newarr[strtotime($val['date'])] = json_encode($val);
                        }
                    }

                    $outsidenewarr = array();
                    if (!empty($outside_punches)) {
                        foreach ($outside_punches as $dat) {
                            $outsidenewarr[strtotime($dat)] = json_encode($dat);
                        }
                    }

                    if ($totpunch % 2 == 0) {
                        $st_color = '#FF8080';
                    } else {
                        $st_color = '#8AE62E';
                    }

                    if (isset($break_time) && $break_time != '') {
                        $break_color = '#ff8080c7';
                    } else {
                        $break_color = '#bcde94';
                    }


                    $firstpunchsec = $this->timetosec($firstpunch);
                    $lastpunchsec = $this->timetosec($lastpunch);
                    $tottime = $intime + $outtime;
                    ?>

                    <?php
                    
                    if ($firstpunch == '-') {
                        if($astatus==1 ||$astatus==3){
                        $user_punches .= '<tr class="userpunchrow">';
                        $user_punches .= '<td>' .$k++ . '</td>';
                        $user_punches .= '<td>' . $empcode . '</td>';
                        $user_punches .= '<td style="font-size:14px">' .  $fullname. '<br/>'
                                . '<small>' . $company_name . '</small>'
                                . '<span style="float:right;color:#c2c2c2;"></span><br/>';
                        if (isset($this->interchange[$userid])) {
                            $user_punches .= '<small style="color:#0025f1;">Shift Interchange By - ' . $this->interchange[$userid] . '</small>';
                        }
                        if ($standby_id != 0) {
                            $standby = Users::model()->findByPk($standby_id);
                            $user_punches .= '<small style="color:red;">Stand By - ' . $standby['firstname'] . " " . $standby['lastname'] . '</small>';
                        }



                        $user_punches .= '</td><td colspan="8" class="totpunch"><span class="totpunchnew">nopunch</span></td> ';
                        $user_punches .= '<td  class="daypunch"><span class="totpunchnew"></span></td> ';

                        $user_punches .= '<td style="background-color: #d8eef3;color: #555;">' . $shift . '</td> ';

                        $user_punches .= '</tr>';
                    }
                    } else {

                        if($astatus==2 ||$astatus==3){
                        ?>

                        <tr class="userpunchrow">
                            <td><?php echo $i; ?></td>
                            <td><?php echo ($empcode == '') ? '' : $empcode ?></td>
                            <td><?php echo $fullname ;?><br/><span style="color:green"><small><?= $company_name; ?></small></span>
                                <span class="pull-right gray-color"></span><br/>
                                <?php
                                if ($standby_id != 0) {
                                    $standby = Users::model()->findByPk($standby_id);
                                    ?>
                                    <br/><small style="color:red;">Stand By - <?= $standby['firstname'] . ' ' . $standby['lastname']; ?></small>
                                <?php } ?>
                                <?php if (isset($this->interchange[$userid])) { ?>
                                    <br/><small style="color:#0025f1;">Shift Interchange By - <?= $this->interchange[$userid]; ?></small>
                                <?php } ?>


                            </td>
                            <td style="background-color:<?= $st_color; ?>"><?php echo $status ?></td>
                            <td><?php echo date('h:i:s', strtotime($firstpunch)) ?></td>
                            <td><?php echo date('h:i:s', strtotime($lastpunch)) ?></td>
                            <td><?php echo ($intime > 0 ? gmdate("H:i:s", $intime) : '-') ?></td>
                            <td><?php echo ($outtime > 0 ? gmdate("H:i:s", $outtime) : '-') ?></td>
                            <td><?php echo ($intime > 0 ? gmdate("H:i:s", $tottime) : '-') ?></td>
                            <td class='totpunch'><?php echo ($totpunch > 0 ? $totpunch : '-') ?>
                                <span class='totpunchnew'><?php
                                    if (!empty($newarr)) {
                                        // echo "[" . (implode(",", $newarr)) . "]";
                                    } else {
                                        // echo '[]';
                                    }
                                    ?></span>
                            </td>
                            <td style="background-color:<?= $break_color; ?>"><?= (isset($break_time) && $break_time != '') ? gmdate("H:i:s", $break_time) : ' - ' ?></td>
                            <td class="outdaypunch" style='background-color:<?= (isset($outside_punch) && $outside_punch != '') ? '#e6121259' : ''; ?>' <?php if (isset($manual_ot['manual_ot']) && isset($manual_ot['manual_ot']) != '') { ?>title="Manual OT"<?php } ?>>
                                <?php
                                if($shift_temp_no_ot ==0){
                                      if (isset($manual_ot['manual_ot']) && isset($manual_ot['manual_ot']) != '') {
                                          echo (($manual_ot['manual_ot'] != 0) ? gmdate("H:i:s", $manual_ot['manual_ot']) : 0) . " (M)";
                                      } else {
                                           //echo (isset($outside_punch) && $outside_punch != '') ? gmdate("H:i:s", $outside_punch) : ' - ';

                                           echo (isset($outside_punch) && $outside_punch != '') ?$outside_punch : ' - ';

                                      }
                                 }else{
                                    echo ' - ';
                                 }
                                ?>

                             
                            </td>
                            <td class="daypunch" style='background-color: <?= ($color_code != '') ? $color_code : '#ff80804d'; ?>'><?php echo $shift; ?>
                                <span class='totpunchnew'><?php


                                    if (isset($this->punchresult[$userid]['punches'][date('Ymd', strtotime($this->pdate))]['day_punch'])) {
                                        // echo "[" . (implode(",", $this->punchresult[$userid]['punches'][date('Ymd', strtotime($this->pdate))]['day_punch'])) . "]";
                                    } else {
                                        // echo '[]';
                                    }
                                    ?></span>
                            </td>
                        </tr>
                    <?php

                $i++;
                
                } } ?>
                

                    <?php
                    
                }
                ?>

                <?php echo $user_punches; ?>
            <?php } else { ?>
                <tr><td></td><td colspan="12">No Result</td></tr>
            <?php } ?>


        </tbody>
    </table>
</div>
            </div>

<script>

$(document).ready(function(){

    var addSerialNumber = function () {
            var i = 0;
            $('.newlog tr').each(function (index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };
        addSerialNumber();
})

</script>
