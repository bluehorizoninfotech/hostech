<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs=array(
	'Time Entries'=>array('index'),
	$model->teid=>array('view','id'=>$model->teid),
	'Update',
);

$this->menu=array(
	array('label'=>'List TimeEntry', 'url'=>array('index')),
	array('label'=>'Create TimeEntry', 'url'=>array('create')),
	array('label'=>'View TimeEntry', 'url'=>array('view', 'id'=>$model->teid)),
	array('label'=>'Manage TimeEntry', 'url'=>array('admin')),
);
?>

<!--<h1>Update TimeEntry <?php echo $model->teid; ?></h1>-->

<div class="panel-heading form-head">
<a class="closebtn pull-right closeform" onclick='closeform("statusform")'>X</a>
 <h3 class="panel-title">Update Status</h3>
   </div>

<?php echo $this->renderPartial('update_form', array('model'=>$model,'type'=>$type)); ?>




		




