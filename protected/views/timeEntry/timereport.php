<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs = array(
    'Time Entries' => array('index'),
    'Manage',
);
?>

<h1>Manage Status Report</h1>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
 $('.search-form form').submit(function(){
   
	$.fn.yiiGridView.update('timereport-grid', {
		data: $(this).serialize()
    });

    $.fn.yiiGridView.update('timereport-grid1', {
		data: $(this).serialize()
    });
   
	return false;
}); 
");
?>

<div><?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?></div>
<div class="search-form">
    <?php $this->renderPartial('_searchreport',array(
            'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'timereport-grid',
    'dataProvider'=> $model->reportsearch(),
   // 'filter'=>$model,
      
     'itemsCssClass' => 'table table-bordered',
    
    'columns'=>array(
       array('class' => 'IndexColumn', 'header' => 'S.No.',),
		/*
         array(
                'header'=>'Sl.No.',
                'value'=>'$this->grid->dataProvider->pagination->offset + $row+1',       //  row is zero based
        ),
      
        array(
            'header' => 'Sl No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ), */
        array('name'=>'project','header'=>'Project'),           
        array('name'=>'task','header'=>'Task'),        
        array('name'=>'work_type','header'=>'Work Type','filter' => CHtml::listData(WorkType::model()->findAll(
                            array(
                                'select' => array('wtid,work_type'),
                                'order' => 'work_type',
                                'distinct' => true
                    )), "wtid", "work_type")),
        array('name'=>'description','header'=>'Description'),
        array(
        'name'=>'entry_date',
        'header'=>'Entry Date',
        'type' => 'raw',
        'value'=>'isset($data["entry_date"]) ? date("d-m-Y",strtotime($data["entry_date"])) : ""',    
        ),
       // array('name'=>'entry_date','header'=>'Entry Date','filter'=>'<h4 style=\'margin-bottom:0px;background-color:#E5B02B;\'><b>Total:</b></h4>','footer'=>'<h4 style=\'margin-bottom:0px;background-color:#E5B02B;\'><b>Total:</b></h4>'),
       // array('name'=>'hours','header'=>'Hours', 'type'=>'text','filter'=>"<h4 style='margin-bottom:0px;background-color:#E5B02B;'>".$model->getTotals($model->search()->getKeys())."</h4>", 'footer'=>"<h4 style='margin-bottom:0px;background-color:#E5B02B;'>".$model->getTotals($model->search()->getKeys())."</h4>"),
       /* array('name'=>'billable', 'type'=>'html',          
            'value' => '$data["billable"]=="Yes"?"<span style=\"font-size:25px;color:blue;\">&#x2714;</span>":"&#x2716;"',
            'htmlOptions'=>array('style' => 'text-align: center;font-weight:bold;font-size:20px;'),
            'header'=>'Billable','filter'=>array('Yes'=>'Billable','No'=>'Non-Billable')), */
        array('name'=>'username', 'type'=>'raw','header'=>'Resource','filter'=>CHtml::listData(Users::model()->findAll(
                                array(
                                    'select' => array('userid,first_name'),
                                    'condition'=>'status=0',
                                    'order' => 'first_name',
                                    'distinct' => true
                        )), "userid", "first_name"),'visible'=>Yii::app()->user->role!=3)
        ),
));
?>

</div>


</div>

<h2>All Tasks </h2>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'timereport-grid1',
    'dataProvider'=> $taskmodel->reportSearchnew(),
   // 'filter'=>$model,
      
     'itemsCssClass' => 'table table-bordered',
    
    'columns'=>array(
       array('class' => 'IndexColumn', 'header' => 'S.No.',),
		
        array('name'=>'project','header'=>'Project'),           
        array('name'=>'task','header'=>'Task'),        
        array('name'=>'description','header'=>'Description'),
        array(
        'name'=>'start_date',
        'header'=>'Date from',
        'type' => 'raw',
        'value'=>'isset($data["start_date"]) ? date("d-m-Y",strtotime($data["start_date"])) : ""',    
        ),
        array(
        'name'=>'due_date',
        'header'=>'Date to',
        'type' => 'raw',
        'value'=>'isset($data["due_date"]) ? date("d-m-Y",strtotime($data["due_date"])) : ""',    
        ),
   
        array('name'=>'username', 'type'=>'raw','header'=>'Resource','filter'=>CHtml::listData(Users::model()->findAll(
                                array(
                                    'select' => array('userid,first_name'),
                                    'condition'=>'status=0',
                                    'order' => 'first_name',
                                    'distinct' => true
                        )), "userid", "first_name"),'visible'=>Yii::app()->user->role!=3)
        ),
));
?>

