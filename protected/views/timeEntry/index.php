
<?php
/* @var $this TimeEntryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Time Entries',
);

$this->menu = array(
    //array('label' => 'Create TimeEntry', 'url' => array('create')),
    //array('label' => 'Manage TimeEntry', 'url' => array('admin')),
);

?>
<div class="clearfix">
    <div class="addlink pull-right">
        <?php
        $createUrl = $this->createUrl('addtime', array("asDialog" => 1, "gridId" => 'address-grid'));
        //echo CHtml::link('Add status', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        echo CHtml::link('Add Time', '', array('class' => 'btn blue addstatus'));
        ?>
    </div>
    

<div id="ajaxloading"></div>

<?php
if (Yii::app()->user->role <= 2) {
    ?>
    <div class="pull-right select-drop margin-top-0">
        <?php
        
        $tblpx=yii::app()->db->tablePrefix;
                $members = Groups::model()->findAll(
                     array(
                         'select' => 'group_id',
                         'condition' => 'group_lead=' . Yii::app()->user->id,
                ));
                foreach ($members as $data){
                    $list = Groups_members::model()->findAll(
                         array(
                             'select' => 'group_id,group_members,id',
                             'condition' => 'group_id=' . $data['group_id'],
                    ));
                    foreach($list as $groupsmem) {
                        $gpmem[] = $groupsmem['group_members'];
                    }
                }
               $gpmem[] =  Yii::app()->user->id;
                $where = '';
            $gpmem = implode(',', $gpmem);    
            if(Yii::app()->user->role != 1){
                $where = 'AND t.userid IN  ('.$gpmem.')';
            }
		$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
				. "`{$tblpx}user_roles`.`role` "
				. "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
				. "WHERE status=0 AND user_type!=11 $where ORDER BY user_type,full_name ASC";
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		$listdata = CHtml::listData($result, 'userid', 'full_name','role');
       /* echo CHtml::dropDownList('resource_sheets', Yii::app()->user->id, CHtml::listData(Users::model()->findAll(array(
						//'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
						'condition'=>"status=0",
						'order' => 'first_name ASC'
						)), 'userid', 'first_name'), 
						array(
								'ajax' => array(
								'type' => 'POST', //request type
								'url' => CController::createUrl('/timeEntry/entry'), //url to call.
								'update' => '#time_area',
								'beforeSend' => "function( request )
									 {
									 $('#ajaxloading').addClass('loading');
									 }",
								'complete' => "function( request )
								  {
									$('#ajaxloading').removeClass('loading');
								  }",
								'data' => array('resource_id' => 'js:this.value'),
								'success' => 'function(data) {        
								$("#time_area").html(data);
								}',
								)

						   ,  'class' => 'form-control'

							)); */
							
							
						echo CHtml::dropDownList('resource_sheets', Yii::app()->user->id, $listdata,
						array(
								'ajax' => array(
								'type' => 'POST', //request type
								'url' => CController::createUrl('/timeEntry/entry'), //url to call.
								'update' => '#time_area',
								'beforeSend' => "function( request )
									 {
									 $('#ajaxloading').addClass('loading');
									 }",
								'complete' => "function( request )
								  {
									$('#ajaxloading').removeClass('loading');
								  }",
								'data' => array('resource_id' => 'js:this.value'),
								'success' => 'function(data) {        
								$("#time_area").html(data);
								}',
								)

						   ,  'class' => 'form-control height-34'

							)); 
        ?>
    </div>
        <?php
    }
    ?>
<h1>Time Sheet</h1>
</div>

<div id="statusform" class="panel panel-primary"></div>

<!--Time Entry area-->
<div id="time_area">
<?php
//Initial values with current week
list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-d"));

echo $this->renderPartial('entry', array(
    'start_date' => $start_date,
    'end_date' => $end_date,
    'week_title' => $week_title,
    'resource_id' => '0',
));
?>
</div>

<?php
//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add status',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0" class="min-height-400" ></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>
<?php
        Yii::app()->clientScript->registerScript('myjavascript', '
            
            $(document).ready(function () {
                $("#resource_sheets").on("change",function () {
                    var selected_val = this.value; 
                    $("#TimeEntry_user_id option:selected").removeAttr("selected");    
                    $("#TimeEntry_user_id").find("option[value=" + selected_val + "]").attr("selected","selected");                                      
                    $(".user_id_").val(selected_val);
                    gettasks(selected_val);
                   
                });
               $(".addstatus").click(function () {                                   
                   $.ajax({
                       type: "GET",
                        url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/create&layout=1").'",
                       success: function (response)
                       {
                           $("#statusform").html(response).slideDown();
                           var selected_resource = $("#resource_sheets").find(":selected").val();
                           $("#TimeEntry_user_id option:selected").removeAttr("selected");    
                           $("#TimeEntry_user_id").find("option[value=" + selected_resource + "]").attr("selected","selected");                          
                           $(".user_id_").val(selected_resource);
                           gettasks(selected_resource);
                        }
                   });
               });
               
               function gettasks(selected_val){
                $.ajax({
                    type: "POST",
                    data: {userid:selected_val},
                    dataType: "json",
                     url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/gettasks").'",
                    success: function (response)
                    {                           
                        $("#TimeEntry_tskid").html(response.option);                           
                     }
                });
                }
                $(".edittime").click(function () {
                     
                    var id = $(this).attr("data-id");
                    $.ajax({
                        type: "GET",
                         url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/update&asDialog=1&id=" ).'" + id,
                        success: function (response)
                        {
                            $("#statusform").html(response).slideDown();
                        }
                    });
                });      
                
           });

           $("#statusform").hide();
                ');


                ?>

                
