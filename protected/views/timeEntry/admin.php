<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs=array(
	'Time Entries'=>array('index'),
	'Manage',
);
?>

<h1>Manage Time Entries</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'time-entry-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
	    array('header'=>'Resource','name'=>'user_id','value'=>'$data->user->first_name', 
                'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                'condition'=>"status=0",
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")),
            array('header'=>'Task','name'=>'tskid','type'=>'raw','value'=>'"<b>(#".$data->tskid.")</b>--".$data->tsk->title'
//               
                ),
            array('name'=>'work_type','value'=>'$data->workType->work_type','filter' => CHtml::listData(WorkType::model()->findAll(
                            array(
                                'select' => array('wtid,work_type'),
                                'order' => 'work_type',
                                'distinct' => true
                    )), "wtid", "work_type")),
		'entry_date',
		'hours',
		array('name'=>'billable','value'=>'($data->billable==3?"Yes":"No")','filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="yesno"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")),
            
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
