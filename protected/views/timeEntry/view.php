<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs=array(
	'Time Entries'=>array('index'),
	$model->teid,
);

$this->menu=array(
	array('label'=>'List TimeEntry', 'url'=>array('index')),
	array('label'=>'Create TimeEntry', 'url'=>array('create')),
	array('label'=>'Update TimeEntry', 'url'=>array('update', 'id'=>$model->teid)),
	array('label'=>'Delete TimeEntry', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->teid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TimeEntry', 'url'=>array('admin')),
);
?>

<h1>View TimeEntry #<?php echo $model->teid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'teid',
		'user_id',
		'tskid',
                'work_type',
		'entry_date',
		'hours',
		'billable',
		'description',
		'created_date',
		'created_by',
		'updated_date',
		'updated_by',
	),
)); ?>
