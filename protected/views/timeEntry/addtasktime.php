<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */
/* @var $form CActiveForm */
?>
<?php
Yii::app()->clientscript->scriptMap['jquery.min.js'] = false; Yii::app()->clientscript->scriptMap['jquery.js'] = false;?>
<div class="panel panel-primary">
    <div class="panel-heading clearfix">
        <h3 class="panel-title pull-left">Status</h3>
        <div class="pull-right">
            <a class="closebtn " onclick="closeaction('popup_view')">X</a>
        </div>
    </div>
  <div class="panel-body">
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'time-entry-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>


    <div class="row">



        <?php echo $form->hiddenField($model, 'user_id', array('class'=>'form-control','value' => Yii::app()->user->id)); ?>


		<div class="col-md-6">
        <?php echo $form->labelEx($model, 'tskid'); ?>
        <?php echo $form->dropDownList($model, 'tskid', CHtml::listData(Tasks::model()->findAll(array(
								'order' => 'title ASC',
								'condition' => 'tskid ='.$tskid,
								)), 'tskid', 'title'), array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'tskid'); ?>
		</div>
        <div class="col-md-6">
        <?php echo $form->labelEx($model, 'work_type'); ?>
        <?php // echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll(array('order' => 'work_type ASC')), 'wtid', 'work_type'), array('class'=>'form-control','empty' => '---')); ?>
            <?php
        if(Yii::app()->user->role == 3) {
        ?>
         <?php echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll(array("condition"=>"work_type='Service Report'")), 'wtid', 'work_type'), array('class'=>'form-control','empty' => '---')); ?>

        <?php } else { ?>
	   <?php echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll(array('order' => 'work_type ASC')), 'wtid', 'work_type'), array('class'=>'form-control','empty' => '---')); ?>
			<?php } ?>
        <?php echo $form->error($model, 'work_type'); ?>
		</div>

    </div>


 <div class="row">
     <div class="col-md-6">
        <?php echo $form->labelEx($model, 'completed_percent'); ?>
        <?php echo $form->textField($model, 'completed_percent', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'completed_percent'); ?>
    </div>
    <?php
		if (!isset($model->entry_date))
            $model->entry_date = (isset(Yii::app()->session['start_date']) ? (Yii::app()->session['start_date']) : date('Y-m-d'));
            $model->entry_date = date('d-M-Y',strtotime($model->entry_date));
		?>
		<div class="col-md-6">
        <?php echo $form->labelEx($model, 'entry_date'); ?>
        <?php echo CHtml::activeTextField($model, 'entry_date', array("id" => "TimeEntry_entry_date", 'class'=>'form-control')); ?>
        <?php
        /*$this->widget('application.extensions.calendar.SCalendar', array(
            'inputField' => 'TimeEntry_entry_date',
            'ifFormat' => '%Y-%m-%d',
        ));*/
        ?>
        <?php echo $form->error($model, 'entry_date'); ?>
		</div>
 </div>
     <div class="row">
    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>
     </div>
    <div class="row buttons text-center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn green')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>
<script>
function closeaction(formname){
			 $('.' +formname ).hide();
	 };
   $(document).ready(function () {
           $("#TimeEntry_entry_date").datepicker({dateFormat: 'd-M-y'}).datepicker("setDate", new Date());
   });
</script>
