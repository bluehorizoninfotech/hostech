<?php
$cs = Yii::app()->clientScript;
$cs->scriptMap = array(
    'jquery.js' => false,
    'jquery.min.js' => false,
);

?>
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- <link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen"> -->
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'time-entry-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
            ));
    ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php //echo $form->errorSummary($model); ?>
<div class="panel-body">
 <div class="row">    
       
       
   	<div class="col-md-3"> 

                <?php echo $form->labelEx($model, 'tskid'); ?>	
                   
                     <?php

                       echo $form->dropDownList($model, 'tskid', CHtml::listData(Tasks::model()->findAll(array("condition"=>"tskid = ".$taskid ,'order' => 'title ASC')), 'tskid', 'title'), array('class'=>'form-control'));	
                    
                    
                            ?>
                <?php echo $form->error($model, 'tskid'); ?>
                <span  class="red-color" id="task_span_id"></span>
        </div>
          <!-- Task section ends-->
          <div class="col-md-3">
            <?php echo $form->labelEx($model, 'work_type'); ?>
            <?php
            if(Yii::app()->user->role == 3) {
            ?>
             <?php echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll(array("condition"=>"work_type='Service Report'")), 'wtid', 'work_type'), array('class'=>'form-control','empty' => '---')); ?>

            <?php } else { ?>
               <?php echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll(array('order' => 'work_type ASC')), 'wtid', 'work_type'), array('class'=>'form-control','empty' => '---')); ?>
                            <?php } ?>
            <?php echo $form->error($model, 'work_type'); ?>
        </div>       
    
        

        <?php	
        if (!isset($model->entry_date))
            $model->entry_date = (isset(Yii::app()->session['start_date']) ? (Yii::app()->session['start_date']) : date('Y-m-d'));
            $model->entry_date = date('d-M-y',strtotime($model->entry_date));
        ?>

        <div class="col-md-3">
            <?php echo $form->labelEx($model, 'entry_date'); ?>
            <?php echo CHtml::activeTextField($model, 'entry_date', array("id" => "TimeEntry_entry_date", "size" => "15", 'class'=>'form-control')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'TimeEntry_entry_date',
                'ifFormat' => '%d-%b-%y',
            ));
            ?>
            <?php echo $form->error($model, 'entry_date'); ?>
            <span  class="red-color" id="span_id"></span>
        </div>          
 
     <div class="col-md-3">
        <?php echo $form->labelEx($model, 'completed_percent'); ?>
        <?php echo $form->textField($model,'completed_percent',array('class' => 'form-control img_comp_class','autocomplete'=>'off',
			'name'=>'TimeEntry[completed_percent]',
			'ajax' => array(
						'url'=>array('TimeEntry/getprogress'),
            			'type'=>'POST',

						'data'=>array('progress'=>'js:this.value',
									  'task_id'=>'js: $("#TimeEntry_tskid").val()',
										),
                                        'dataType' => 'json',
						'success' => 'function(data){
							if(data.progress==1){
                                alert("progress  percentage should be less than or equal to " +data.balance_progress+ ""); 
								$("#TimeEntry_completed_percent").val("");
							}				
							
						}',
                        
                        )
			
			)); ?>
        <?php // echo $form->textField($model, 'completed_percent', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'completed_percent'); ?>
    </div>
    <div class="col-md-3">
            <?php echo $form->labelEx($model, 'current_status'); ?>           
             <?php echo $form->dropDownList($model, 'current_status', CHtml::listData(Status::model()->findAll(array("condition"=>"status_type='task_status' AND sid IN (5,8,9,72) ORDER BY caption ASC")), 'sid', 'caption'), array('class'=>'form-control','empty' => '---')); ?>           
            <?php echo $form->error($model, 'current_status'); ?>
        </div> 
        <!-- <div class="col-md-3">
            <?php echo $form->labelEx($model, 'hours'); ?>
            <?php
            if(!$model->isNewRecord){
            //    $time =  gmdate('H:i', floor($model->hours * 3600));
            $time=$model->hours;
            }
            ?>
            <input type="time" class="form-control" value="<?= !$model->isNewRecord?$time:''?>" name="TimeEntry[hours]" id="TimeEntry_hours">
            <?php //echo $form->textField($model,'hours',array('class'=>'form-control input-medium','placeholder'=>'hh:mm')); ?>
            <?php echo $form->error($model, 'hours'); ?>
        </div>       -->

        <div class="col-md-3 width-80">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'start_time'); ?>
                                <?php echo $form->textField($model, 'start_time', array("autocomplete" => 'off',  'class' => 'form-control width-50')); ?>
                                <?php echo $form->error($model, 'start_time'); ?>
                            </div>
                        </div>
                        <div class="col-md-3 width-80">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'end_time'); ?>
                                <?php echo $form->textField($model, 'end_time', array("autocomplete" => 'off', 'class' => 'form-control width-50')); ?>
                                <?php echo $form->error($model, 'end_time'); ?>
                            </div>
                        </div>


        <div class="col-md-3">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('class'=>'form-control','rows' => 3)); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>

<!-- <div class="col-md-3">
<input type="checkbox" id="read" name="gantt_chart_permission"  value="1" > 
			Show on GanttChart </label> 
        </div> -->

     </div>
    <div class="row buttons text-center">
        <?php echo CHtml::Submitbutton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn blue save_btn')); ?>
       
       <?php
        if($type ==0){
            echo CHtml::resetButton('Close', array('onclick'=>"closeform('statusform')",'class'=>'btn margin-x-10')); 
            echo CHtml::resetButton('Reset', array('class' => 'btn default'));
        }
       ?>
        <?php // echo CHtml::resetButton('Close', array('onclick'=>"closeform('statusform')",'class'=>'btn'));  ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<!--<style>
.row{margin-left: -15px;
  margin-right: -15px;
  width: 500px;
  }
  .subrow{
        width: 50%;
        float:left;
    }
    form .subrow {
  padding: 0 12px !important;
    }
    div.form input, div.form textarea, div.form select {
  margin: 0.2em 0 0.5em 0;
}
.subrowlong{
        width: 400px;
        float:left;
        
        
    }
    .blue.btn {
  
  margin-left: 30px;
}

</style>-->

<?php
       Yii::app()->clientScript->registerScript('myjavascript', '
            function closeform(formname){
                   $("#"+formname).slideUp();
                   $("html,body").animate({
                       scrollTop: $("body").offset().top
                   },"slow");
           }

           $("#btnSubmit").on("click",function () {

            var form_data = $("#time-entry-form").serialize();

             $.ajax({
                type: "POST",
                data: form_data + "&ajaxcall=1",
                dataType: "json",
                 url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/Addtime").'",

                success: function (response)    
                { 

                    
                }

                });

           });

        //    $("#TimeEntry_tskid").on("change",function () {

        //     var tskid = $(this).val();

        //      $.ajax({
        //         type: "GET",
        //         data: {tskid:tskid},
        //         dataType: "json",
        //          url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/getworytype").'",

        //         success: function (response)    
        //         { 
        //             if(response.id != null){
        //                 $("#TimeEntry_work_type").html(response.option);
        //                 $("#TimeEntry_work_type").val(response.id);
        //                 //$("#TimeEntry_work_type").prop("disabled", true);
        //             }else{
        //                 $("#TimeEntry_work_type").html(response.option);
        //             }
                    
        //         }

        //         });

        //    });
           

           
               ');
 ?>
<script>
    // $('#TimeEntry_entry_date').datepicker({
    //     dateFormat: 'd-M-y',

    // });
</script>

<script>
     $(document).ready(function (e) {
        $("#TimeEntry_entry_date").blur(function(){           
            var date=$('#TimeEntry_entry_date').val();
            var taskid=$("#TimeEntry_tskid").val();
            if(taskid=="")
            {
                document.getElementById("task_span_id").innerHTML = 'Select task cannot be blank.';  
            }
            else
            {
            $.ajax({
        type: 'POST',
        url: '<?php echo Yii::app()->createAbsoluteUrl("TimeEntry/datevalidation")?>',
        dataType: "json",
        async:false,
        data: {
            date: date,
            taskid:taskid

        },
        success: function(data) {

            if(data.status==1){
                
                var startdate=data.start_date;
                var enddate=data.end_date;                  
                var msg="Selected task Start & Due date: "+startdate+" & "+enddate;             
                $("#span_id").show().html(msg);                
                $(".save_btn").prop('disabled', true);
            }
            else
            {
              $("#span_id").hide();
              $(".save_btn").prop('disabled', false); 
                               
            }
        }

    });
}       
        });

    });
   
</script>


