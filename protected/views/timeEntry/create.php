<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs=array(
	'Time Entries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TimeEntry', 'url'=>array('index')),
	array('label'=>'Manage TimeEntry', 'url'=>array('admin')),
);
?>

<!--<h1>Add time</h1>-->

<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>

   <div class="panel-heading form-head">
   <?php
   if($type == 1){
       ?>
<a class="closebtn pull-right closeform" onclick='closeform("statusform")'>X</a>
       <?php
   }
   ?>
       <h3 class="panel-title">Add Time</h3>
   </div>
   <?php if($type == 1){
 echo $this->renderPartial('_form', array('model'=>$model,'type'=>$type,'project_id'=>$project_id,'task_id'=>$task_id));
   }else{
    echo $this->renderPartial('_form', array('model'=>$model,'type'=>$type));
   }
       ?>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>

<link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/bootstrap-datetimepicker.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">


<script type="text/javascript">
    $(document).ready(function() {
        $('#TimeEntry_start_time, #TimeEntry_end_time').datetimepicker({
            //language:  'fr',
            pickDate: false,
            weekStart: 1,
            format: "hh:ii",
            todayBtn: false,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            maxView: 1,
            forceParse: 0,
            showMeridian: 1
        });

    });
        </script>