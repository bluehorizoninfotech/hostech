<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs=array(
	'Time Entries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TimeEntry', 'url'=>array('index')),
	array('label'=>'Manage TimeEntry', 'url'=>array('admin')),
);
?>

<!--<h1>Add time</h1>-->

<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>

   <div class="modal-header">
   <?php
  // if($type == 1){
       ?>
<!-- <a class="closebtn pull-right close-entry-form">X</a> -->
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
       <?php
   //}
   ?>
       <h3 class="modal-title">Add Time</h3>
   </div>
</div>
   <?php if($type == 1){
 echo $this->renderPartial('_add_time_entry_form', array('model'=>$model,'type'=>$type,'project_id'=>$project_id,'task_id'=>$task_id,'taskid'=>$taskid));
   }else{
    echo $this->renderPartial('_add_time_entry_form', array('model'=>$model,'type'=>$type,'taskid'=>$taskid));
   }
       ?>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>

<link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/bootstrap-datetimepicker.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">


<script type="text/javascript">
        $('#TimeEntry_start_time, #TimeEntry_end_time').datetimepicker({
            //language:  'fr',
            pickDate: false,
            weekStart: 1,
            format: "hh:ii",
            todayBtn: false,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            maxView: 1,
            forceParse: 0,
            showMeridian: 1
        });


        $(document).on('click', '.close-entry-form', function() {
            location.reload();
        
    });



        </script>