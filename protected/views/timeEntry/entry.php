<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this TimeEntryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Time Entries',
);

$this->menu = array(
    //array('label' => 'Add time', 'url' => array('create')),
    //array('label' => 'Advanced time manager', 'url' => array('admin'),'visible'=>(Yii::app()->user->role == 1))
);
// Yii::app()->clientScript->registerScript('myjquery', ' 

//     $(document).on("click",".deletetime",function(e){
//             e.preventDefault();

//             var datid=$(this).attr("data-id")  ;
//             if(confirm("Are you sure you want to delete this time entry?")){
//              $(this).parent().parent().parent("tr").hide();
//               $.ajax({
//                     method: "POST",
//                     dataType: "html",
//                     url:"' . Yii::app()->createUrl('timeEntry/CDelete') . '&id="+datid,
//                     success: function (data) {

//                         $("#success_message").fadeIn().delay(1000).fadeOut();
//                                 $(".refreshhidelink").trigger("click");

//                             }
//                 });

//           }
//         });



// ');
?>

<?php
//For resource name, if admin chooses the other users, then it will display selected resource report in time sheet
$resource_name = (intval($resource_id) > 0 ? Users::model()->findByPk($resource_id)->first_name : Yii::app()->user->firstname)

    ?>
<br /><br />
<div class="entry-report">
    <div class="page-title">
        <h3 class="margin-bottom-minus-20 responsive-page-title">Time Sheet - <span class="sort-link">
                <?php echo $resource_name; ?>
            </span>, week of
            <?= $week_title; ?>
        </h3>
    </div>


    <!--Ajax navigation for weeks-->

    <div class="'nav navbar-nav pull-right">
        <?php
        echo CHtml::ajaxLink(
            '<img src="' . Yii::app()->request->baseUrl . '/images/previous.gif" />' . " Previous week",
            Yii::app()->createUrl('timeEntry/entry'),
            array(// ajaxOptions
                'type' => 'POST',
                'update' => '#time_area',
                'beforeSend' => "function( request )
                     {
                     $('#ajaxloading').addClass('loading');
                     }",
                'complete' => "function( request )
                  {
                    $('#ajaxloading').removeClass('loading');
                  }",
                'data' => array('start_date' => "$(\"#start_date\").val()", 'end_date' => "$(\"#end_date\").val()", 'moveto' => 'prev'),
            ),
            array(//htmlOptions
                'href' => Yii::app()->createUrl('timeEntry/entry'),
                'class' => 'nav navbar-nav pull-left'
            )
        );
        ?>
        <?php
        echo CHtml::ajaxLink(
            "",
            Yii::app()->createUrl('timeEntry/entry'),
            array(// ajaxOptions
                'type' => 'POST',
                'update' => '#time_area',
                'beforeSend' => "function( request )
                     {
                     $('#ajaxloading').addClass('loading');
                     }",
                'complete' => "function( request )
                  {
                    $('#ajaxloading').removeClass('loading');
                  }",
                'data' => array('start_date' => "$(\"#start_date\").val()", 'end_date' => "$(\"#end_date\").val()", 'moveto' => 'current'),
            ),
            array(//htmlOptions
                'href' => Yii::app()->createUrl('timeEntry/entry'),
                'class' => 'nav navbar-nav pull-right refreshhidelink',
            )
        );
        ?>
        <?php
        echo CHtml::ajaxLink(
            "&nbsp;&nbsp;Current week",
            Yii::app()->createUrl('timeEntry/entry'),
            array(// ajaxOptions
                'type' => 'POST',
                'update' => '#time_area',
                'beforeSend' => "function( request )
                     {
                     $('#ajaxloading').addClass('loading');
                     }",
                'complete' => "function( request ) 
                  {
                    $('#ajaxloading').removeClass('loading');
                  }",
                'data' => array('start_date' => "$(\"#start_date\").val()", 'end_date' => "$(\"#end_date\").val()"),
            ),
            array(//htmlOptions
                'href' => Yii::app()->createUrl('timeEntry/entry'),
                'class' => 'nav navbar-nav pull-left'
            )
        );
        ?>
        <?php
        echo CHtml::ajaxLink(
            "&nbsp; &nbsp;Next week" . ' <img src="' . Yii::app()->request->baseUrl . '/images/next.gif" />',
            Yii::app()->createUrl('timeEntry/entry'),
            array(// ajaxOptions
                'type' => 'POST',
                'update' => '#time_area',
                'beforeSend' => "function( request )
                     {
                     $('#ajaxloading').addClass('loading');
                     }",
                'complete' => "function( request )
                  {
                    $('#ajaxloading').removeClass('loading');
                  }",
                'data' => array('start_date' => "$(\"#start_date\").val()", 'end_date' => "$(\"#end_date\").val()", 'moveto' => 'next'),
            ),
            array(//htmlOptions
                'href' => Yii::app()->createUrl('timeEntry/entry'),
                'class' => 'nav navbar-nav pull-right'
            )
        );
        ?>

    </div>
    <!--<div class="nav navbar-nav pull-right">
    <?php
    //    echo CHtml::ajaxLink("Submit for Approval" . ' <img src="' . Yii::app()->request->baseUrl . '/images/next.gif" />', Yii::app()->createUrl('timeEntry/submitsheet'), array(// ajaxOptions
//        'type' => 'POST',
//        'update' => '#time_area',
//        'beforeSend' => "function( request )
//                     {
//                         $('#ajaxloading').addClass('loading');
//                     }",
//        'complete' => "function( request )
//                  {
//                     alert(request.start_date);
//                    $('#ajaxloading').removeClass('loading');
//                  }",
//        'data' => array('start_date' => "$(\"#start_date\").val()", 'end_date' => "$(\"#end_date\").val()", 'moveto' => 'next'),
//            ), array(//htmlOptions
//        'href' => Yii::app()->createUrl('timeEntry/submitsheet'),
//        'class' => 'nav links',
//        'style'=>'background-color: #0066a4;float:right;color:white;border:0px;line-height: 25px;padding:0px 20px;text-decoration:none;',
//            )
//    );
    
    ?>
    
 </div>-->

    <br /><br />
    <!--End of ajax week navigation-->
    <!--table cellpadding="3" cellspacing="0" class="table table-bordered" >
    <thead>
        <tr>
            <th  class="sort-link" colspan="2">Tasks</th><th  class="sort-link" width='80'>Billable</th>
            <?php
            $week_day = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            $week_dates = array();
            $week_date = '';

            for ($i = 0; $i < 7; $i++) {
                $week_date = $this->add_date($start_date, $i);
                echo "<th width='80'>" . $week_day[$i] . " " . $week_date . "</th>";
                $week_dates[] = $week_date;
            }
            ?>
            <th  class="sort-link" width="80">Total</th>
        </tr>
    </thead>
    <?php
    $weekEntries = $this->getConsolidEntries($start_date, $end_date);
    // echo $weekEntries;exit;
    if (count($weekEntries) == 0) {
        ?>
        <tr>
            <td colspan="3">This week's timesheet does not have any time added.
                Click add time to start adding time.</td>
            <?php
            for ($i = 0; $i < 7; $i++)
                echo "<td width='80'>&nbsp;</td>";
            ?>
            <td width="80" class="tbl_title"></td>
        </tr>    
        <?php
    }
    $tot_hours = array();
    $tot_bill_hours = array();
    $tot_nbill_hours = array();

    for ($o = 0; $o < 7; $o++) {
        $tot_hours[$o] = 0;
        $tot_bill_hours[$o] = 0;
        $tot_nbill_hours[$o] = 0;
    }

    $j = 1;
    $taskid_flag = 0;
    $billable_flag = 0;
    $m = 0;
    $i = 0;
    $record_flag = 1;
    $row_total = 0;

    //$condition = '';
    $count_records = count($weekEntries);

    //records list
//    foreach ($weekEntries as $te) {
//        echo "<br />" . $te->tskid . "====" . $te->tentry_date . "=====" . $te->billable . "=====" . $te->hours;
//    }
    
    foreach ($weekEntries as $tentry) {
        //$condition .="Task iD: ".$taskid_flag ."---->".$tentry->tskid."####Billable: ".$billable_flag ."----->". $tentry->billable."---M--->$m<br />";
        if ($taskid_flag != $tentry->tskid || $billable_flag != $tentry->billable) {
            if ($m != 0 && $m <= 7) {
                for ($n = $m; $n < 7; $n++)
                    echo "<td>&nbsp;</td>";
                echo "<th class='totaltr'>" . number_format($row_total, 2) . "</th></tr>";
                $j++;
                $i = 0;
            }

            $row_total = 0;
            $m = 0;
            //$test = $this->gettaskbyid($tentry->tskid);
            $tblpx = Yii::app()->db->tablePrefix;
            $data = Yii::app()->db->createCommand("select tskid, title FROM {$tblpx}tasks WHERE tskid=" . $tentry->tskid . "")->queryRow();

            $ttask = Tasks::model()->findByPk($tentry->tskid);
            $tbstatus = Status::model()->findByPk($tentry->billable);
            $client_project = 'No Client / No Project';
            //Project and client name
            if (isset($ttask->project_id)) {
                $project_name = Projects::model()->findByPk($ttask->project_id)->name;
                // $client_id = Projects::model()->findByPk($ttask->project_id)->client_id;
                // $client_name = Clients::model()->findByPk($client_id)->name;
                //$client_project = "$client_name / $project_name";
                $client_project = "$project_name";
            }
            //echo $test;
            // print_r($ttask);
    
            //exit();
    
            echo "<tr class='" . ($j % 2 == 1 ? 'odd' : 'even') . "'>
                    <td colspan=\"2\"><b>#" . $tentry->tskid . " " . $data['title'] . "</b><br /> <span class='client_project_name'>$client_project</span></td>
                    <td>" . $tbstatus->caption . "</td>";
        }

        if ($m != 0)
            $i += 1;

        $m = $i;
        for ($i = $m; $i < 7; $i++) {
            if ($week_dates[$i] === $tentry->tentry_date) {
                echo "<td width='80'>";
                echo $tentry->hours;
                $row_total += floatval($tentry->hours);
                echo "</td>";
                $m = $i + 1;

                $tot_hours[$i] = (float) $tot_hours[$i] + $tentry->hours;
                if ($tentry->billable == 3)
                    $tot_bill_hours[$i] = (float) $tot_bill_hours[$i] + $tentry->hours;
                else
                    $tot_nbill_hours[$i] = (float) $tot_nbill_hours[$i] + $tentry->hours;

                //If match is got. then need to break this loop.
                break;
            } else {
                echo "<td width='80'>&nbsp;</td>";
            }
        }

        //For last record. So it will teminate with colled colum tag with row
        if ($record_flag == $count_records) {
            for ($n = $m; $n < 7; $n++)
                echo "<td>&nbsp;</td>";
            echo "<th class='totaltr'>" . number_format($row_total, 2) . "</th></tr>";
        }
        //End with last record
        $record_flag++;

        $taskid_flag = $tentry->tskid;
        $billable_flag = $tentry->billable;
    }
    ?>
    <tr class="totaltr">
        <th colspan="3">Totals</th>
        <?php
        for ($i = 0; $i < 7; $i++)
            echo "<th width='80'>" . number_format($tot_hours[$i], 2) . "</th>";
        ?>
        <th width="80"><?php echo number_format(array_sum($tot_hours), 2) ?></th>
    </tr>    

    <tr class="totaltr">
        <th colspan="3">Billable</th>
        <?php
        for ($i = 0; $i < 7; $i++)
            echo "<th width='80'>" . number_format($tot_bill_hours[$i], 2) . "</th>";
        ?>
        <th width="80"><?php echo number_format(array_sum($tot_bill_hours), 2) ?></th>
    </tr>  

    <tr class="totaltr">
        <th colspan="3">Unbillable</th>
        <?php
        for ($i = 0; $i < 7; $i++)
            echo "<th width='80'>" . number_format($tot_nbill_hours[$i], 2) . "</th>";
        ?>
        <th width="80"><?php echo number_format(array_sum($tot_nbill_hours), 2) ?></th>
    </tr>
</table-->


    <?php
    //echo $condition;
    ?>


    <!--h3>Detail of time entries for <span class="username"><?php //echo $resource_name;  ?></span>, week of <? //= $week_title;  ?></h3-->

    <?php
    $weekDetailEntries = $this->weekDetailEntries($start_date, $end_date);
    ?>
    <p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully updated..</p>
    <div class="table-responsive">
        <table cellpadding="3" cellspacing="0" class="table table-bordered" id="status_table">
            <thead>
                <tr class="odd">
                    <th>&nbsp;</th>
                    <?php echo Yii::app()->user->role == 1 ? '<th class="sort-link" colspan="3">Assignee</th>' : '' ?>
                    <th class="sort-link" colspan="3">Task</th>
                    <th class="sort-link" width="100">Project</th>
                    <th class="sort-link" width="100">Progress %</th>
                    <th class="sort-link" width="350">Description</th>
                    <th class="sort-link" width="350">Hours</th>
                    <th class="sort-link" width="70">Date</th>
                    <th class="sort-link" width="70">Current Status</th>
                    <th class="sort-link" width="70">Status</th>
                </tr>
            </thead>
            <?php
            if (count($weekDetailEntries) == 0) {
                ?>
                <tr class="even">
                    <td colspan="12">This week's timesheet does not have any time added.
                        Click add time to start adding time.</td>
                </tr>
                <?php
            }
            ?>
            <?php
            $total_hours = 0;
            $k = 1;

            foreach ($weekDetailEntries as $tdentry) {
                $total_hours += strtotime($tdentry->hours);
                //Edit link for each row
            
                $tblpx = Yii::app()->db->tablePrefix;
                $data = Yii::app()->db->createCommand("select tskid, title, clientsite_id FROM {$tblpx}tasks WHERE tskid=" . $tdentry->tskid . "")->queryRow();

                $createUrl = $this->createUrl('update', array("id" => $tdentry->teid, "asDialog" => 1, "gridId" => 'address-grid'));
                $editlink = CHtml::link('<i class="icon-pencil icon-comn black-color"></i>', '', array(
                    'class' => 'edittime',
                    'title' => 'Edit',
                    'onclick' => "",
                    "data-id" => $tdentry->teid
                )
                );
                $deletelink = CHtml::link('<i class="icon-trash icon-comn black-color"></i>', '', array('class' => 'deletetime', 'title' => 'Delete', 'data-id' => $tdentry->teid));
                if (in_array('/timeEntry/approveentry', Yii::app()->session['menuauthlist'])) {

                    $approvelink = CHtml::link($tdentry->approve_status == 1 ? '<i class="fa fa-check" style="color: #008000d1;" ></i>' : '<i class="fa fa-thumbs-up" style="color: #ff0000c7;"></i>', '', array('class' => $tdentry->approve_status == 1 ? 'approve_entry checked green' : 'approve_entry', 'data-id' => $tdentry->teid, 'title' => 'Approve', 'id' => $tdentry->teid));
                } else {
                    $approvelink = '';
                }
                ?>
                <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
                    <td class="action_buttons">
                        <?php
                        if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->mainuser_role) && (Yii::app()->user->mainuser_role == 1)) {
                            if (isset(Yii::app()->user->mainuser_id) && $tdentry->timeentry_type == 1) {
                                echo $editlink;
                            }
                            if (isset(Yii::app()->user->mainuser_id)) {
                                echo $deletelink;
                            }
                        } else {
                            if ($tdentry->ddate && $tdentry->timeentry_type == 1) {
                                echo $editlink;
                            }

                            if ($tdentry->ddate) {
                                echo $deletelink;
                            }
                        } ?>
                    </td>

                    <?php
                    $task_model = Tasks::model()->findByPk($tdentry->tskid);

                    echo Yii::app()->user->role == 1 ? ' <td colspan="3">' . $task_model->assignedTo->first_name . ' ' . $task_model->assignedTo->last_name . '</td>' : '' ?>

                    <td colspan="3" class="width-16-percentage">
                        <?php
                        $tblpx = Yii::app()->db->tablePrefix;
                        $data = Yii::app()->db->createCommand("select tskid, title, clientsite_id FROM {$tblpx}tasks WHERE tskid=" . $tdentry->tskid . "")->queryRow();
                        echo $data['title'];
                        ?>
                    </td>
                    <?php

                    $project_tbl = Projects::model()->tableSchema->name;
                    $tasks_tbl = Tasks::model()->tableSchema->name;

                    $sql = "SELECT * FROM $project_tbl p 
                INNER JOIN $tasks_tbl a ON a.project_id=p.pid WHERE a.tskid =$tdentry->tskid";
                    $project = Projects::model()->findBySql($sql);
                    ?>
                    <td>
                        <?php echo $project['name']; ?>
                    </td>
                    <td>
                        <?php echo TimeEntry::model()->findByPk($tdentry->teid)->completed_percent; ?>
                    </td>
                    <td>
                        <?php echo $tdentry->description; ?>
                    </td>
                    <td class="width-16-percentage">
                        <?php
                        // echo gmdate('H:i', floor($tdentry->hours * 3600));	
                        echo $tdentry->hours;
                        ?>
                    </td>
                    <td class="width-7-percentage">
                        <?php echo date('d-M-Y', strtotime($tdentry->tentry_date)); ?>
                    </td>
                    <td>
                        <?php

                        if (!empty($tdentry->current_status)) {
                            echo $tdentry->status->caption;
                        }
                        ?>
                    </td>
                    <td>
                        <?php

                        if (!empty($tdentry->timeentry_type)) {
                            if ($tdentry->timeentry_type == 1) {
                                echo 'Active';
                            } else {
                                echo 'Expired';
                            }
                        }
                        ?>
                    </td>

                </tr>

                <?php
                $k++;
            }
            ?>
            <!--tr class="odd">
            <td colspan="6" class="tbl_title">Totals</td>
            <th><?php echo number_format($total_hours, 2); ?></th><td>&nbsp;</td><td>&nbsp;</td>
        </tr-->
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(document).on('click', ".approve_entry", function () {
            if (!confirm("Do you want to Approve ?")) {
            } else {
                var entry_id = $(this).attr('data-id');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('TimeEntry/approveentry'); ?>',
                    data: { entry_id: entry_id },
                    method: "GET",
                    success: function (result) {
                        if (result === '1') {
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            $("#status_table").load(location.href + " #status_table>*", "");
                        }

                    }
                })
            }


        });
    });
</script>
<script>
    $(document).ready(function (e) {

        $(document).on('click', ".deletetime", function () {
            if (!confirm("Are you sure you want to delete this time entry ?")) {
            }
            else {
                var datid = $(this).attr("data-id");
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('TimeEntry/CDelete'); ?>',

                    data: { datid: datid },
                    method: "POST",
                    success: function (result) {
                        if (result === '1') {
                            $("#success_message").fadeIn().delay(1000).fadeOut();

                            $("#status_table").load(location.href + " #status_table>*", "");
                        }

                    }
                })

            }
        });


    });
</script>