<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>



<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs=array(
	'Time Entries'=>array('index'),
	'Manage',
);
?>
<div class="alert alert-success" role="alert">
</div>
<div class="alert alert-danger" role="alert">
</div>
<div class="pending-requests-section">
<h1>Pending Requests</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'time-entry-grid',
    'afterAjaxUpdate' => 'reinstallDatePicker',
	'itemsCssClass' => 'table table-bordered',
        'template'=>"<div class='table-responsive'>{items}</div><div class='pull-left'>{pager}</div>",
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
            'nextPageLabel'=>'Next ' ),    
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 	
	    'dataProvider'=>$model->pendingrequests(),
	'filter'=>$model,
	'columns'=>array(
            array('class' => 'IndexColumn', 'header' => 'Sl.No.',),array(
                'htmlOptions' => array('class'=>'width-80'),
                'class' => 'ButtonColumn',
                  'template' => '{approve_btn}{approved}{reject_btn}{rejected}',
                  'evaluateID'=>true,
                  'buttons'=>array(                
                       'approve_btn' => array(
                        'label'=>'',
                        'imageUrl' =>false,
                        'visible'=>'($data->approve_status=="0") && in_array("/timeEntry/approveentry", Yii::app()->session["menuauthlist"])',
                        'options' => array('class' => 'fa fa-thumbs-up approve','title'=>'Approve','id'=>'$data->teid'),
                       ),
                       'approved' => array(
                        'label'=>'',
                        'imageUrl' =>false,
                        'visible'=>'$data->approve_status=="1" ',
                        'options' => array('class' => 'fa fa-check approved','title'=>'Approved','id'=>'$data->teid'),
                       ),
                       'reject_btn' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '($data->approve_status=="0" ) && in_array("/timeEntry/rejectentry", Yii::app()->session["menuauthlist"])',
                        // 'url' => 'Yii::app()->createUrl("TimeEntry/approverejectdata", array("id"=>$data->teid,"asDialog"=>1,"gridId"=>"time-entry-grid","stat"=>2))',
                        // 'click' => 'function(e){e.preventDefault();$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                        'options' => array('class' => 'fa fa-thumbs-down margin_5 reject', 'title' => 'Reject','id'=>'$data->teid'),
                    ),
                    'rejected' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '($data->approve_status=="2")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                        'options' => array('class' => 'fa fa-ban rejected', 'title' => 'Rejected', 'id' => '$data->teid'),
                    ),
                    )    
            ),

            array('header'=>'Assignee',
                    'name'=>'user_id','value'=>'$data->user->first_name', 
                    // 'filter' => CHtml::listData(Users::model()->findAll(
                    //         array(
                    //             'select' => array('userid,first_name'),
                    //             'condition'=>"status=0",
                    //             'order' => 'first_name',
                    //             'distinct' => true
                    // )), "userid", "first_name")
                    'filter'=>false
                ),
            array('header'=>'Task',
                'name'=>'tskid',
                'type'=>'raw',
                'value'=>'$data->tsk->title',
                'filter'=>false
                ),
            // array('name'=>'work_type',
            //     'value'=>'($data->work_type)?$data->workType->work_type:""',
            //     'filter'=>false
            // ),
            array('header'=>'Progress %',
                'name'=>'completed_percent',
            // 'value'=>'$data->workType->work_type',
            'filter'=>false
            ), 
            array(
                    'name'=>'description',                
                'filter'=>false
            ),
            array(
                'name'=>'hours',          
                'filter'=>false
                ),	
            array(
                'header'=>'Date',
                'name'=>'entry_date',
                'type'=>'raw',
                'value'=>'date("d-M-y", strtotime($data->entry_date))',
                'filter' => $this->widget( 'zii.widgets.jui.CJuiDatePicker', array(
                    'model'          => $model,
                    'attribute'      => 'entry_date',
                    'options' => array('dateFormat' => 'd-M-y'),
                    'htmlOptions' => array('class' => 'width-100'),
                ), true ),
            ),
            array(
                'header'=>'Current Status',
                'name'=>'current_status', 
                'value'=> 'isset($data->current_status)?$data->status->caption:""',         
                'filter'=>false
            ),
            array(
                'header'=>'Status',
                'name'=>'timeentry_type', 
                'value'=>'($data->timeentry_type==1)?"Active":"Expired"',         
                'filter'=>false
            ),
	        
	),
)); 
Yii::app()->clientScript->registerScript('re-install-date-picker', "
    function reinstallDatePicker(id, data) {
        $('#TimeEntry_entry_date').datepicker();
    }
");
?>
</div>
<div id='popup1'>
    <div class="popup-header clearfix">
        <h5 class="text-center margin-bottom-10">Time Entry Reject &nbsp;</h5>
        <span class="close-icon">&times;</span>
        <div class="approve-reject-data-sec pending-req">
            <div class="form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'daily-task-progress-form',
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,),
                ));   
                ?>
                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model,'reason_rejection'); ?>
                        <?php echo $form->textArea($model,'reason_rejection',array('class' => 'form-control')); ?>
                        <?php echo $form->error($model,'reason_rejection'); ?>           
                    </div>
                    <?php echo $form->hiddenField($model, 'teid'); ?>
                </div>	
                <div class="row">
                    <?php echo CHtml::submitButton('Reject', array('class' => 'btn btn-sm blue','id'=>'rejectentry')); ?>
                    <?php echo CHtml::resetButton('Cancel', array('class' => 'btn default cancel')); ?>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(document).on('click', ".approve", function(){       
        if (!confirm("Do you want to Approve ?")){
        }else{
            var entry_id = this.id;    
            
            $.ajax({
            url: '<?php echo Yii::app()->createUrl('TimeEntry/approveentry'); ?>',
            data:{entry_id:entry_id},            
            method: "GET",
            success: function(result) {
                
                $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (result== 1) {
                        $(".alert-success").show().html("Succesfully Updated").delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").show().html("Something went wrong").delay(3000).fadeOut();
                    }
                    $("#time-entry-grid").load(location.href + " #time-entry-grid");
                
            }
	    })            
        }
      });
      $('.alert-success').hide();
      $('.alert-danger').hide();
    });
    $(document).on('click', ".reject", function(){ 
        var entry_id = this.id; 
        $('#TimeEntry_teid').val(entry_id);     
        $("#popup1").show();
    });
    $(document).on('click', ".cancel", function(){ 
        $("#popup1").hide();
    });
    $('#popup1 .close-icon').click(function() {
        $('#popup1').hide();
    });

    $(document).on('click', "#rejectentry", function(){       
        var reason = $('#TimeEntry_reason_rejection').val();
        var entry_id = $('#TimeEntry_teid').val();
        if(reason){
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('TimeEntry/approverejectdata'); ?>',
                data:{entry_id:entry_id,reason:reason},            
                method: "POST",
                success: function(result) {
                    if(result ==1){  
                        alert('Rejected Successfully');                  
                        location.reload();
                    }else{
                        alert('Something went wrong in reject');
                    }
                }
            }) 
        }else{
            alert('Please enter reason before reject');
        }
    });
</script>
