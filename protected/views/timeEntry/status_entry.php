<div class="panel-heading form-head">
   <?php
   if($type == 1){
       ?>
<a class="closebtn pull-right closeform" onclick='closeform("statusform")'>X</a>
       <?php
   }
   ?>
       <h3 class="panel-title">Add Status</h3>
   </div>

   <div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'time-entry-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php //echo $form->errorSummary($model); ?>
<div class="panel-body">
 <div class="row">    
        <?php
      
            if (!isset($model->user_id))
                $model->user_id = Yii::app()->user->id;
                $user_data = Users::model()->find(['condition'=>'userid = '.Yii::app()->user->id]);
                $user_name = $user_data['first_name'].' '.$user_data['last_name'];
            if(isset($task_id)) {
                $task_data = Tasks::model()->find(['condition'=>'tskid ='.$task_id]);
                if(!empty($task_data)){
                    $task_name =  $task_data['title'];
                    $model->work_type = $task_data['work_type_id'];
                    $work_type_title = $task_data->worktype->work_type;
                }
                $model->tskid = $task_id;
            }
            ?>
            
          
           
            <div class="col-md-4">
                <?php echo $form->labelEx($model, 'user_id'); ?>              
                <?php echo CHtml::textField('user_id',$user_name,array('class'=>'form-control','readonly' => 'true')); ?>
                <?php echo $form->hiddenField($model,'user_id', array('readonly' => 'true','class'=>'form-control')); ?>
                <?php echo $form->error($model, 'user_id'); ?>
            </div>
            <div class="col-md-4">
            <?php echo $form->labelEx($model, 'tskid'); ?>
            <?php echo CHtml::textField('task_name',$task_name,array('class'=>'form-control','readonly' => 'true')); ?>
            <?php echo $form->hiddenField($model,'tskid', array('readonly' => 'true','class'=>'form-control')); ?>	              
                <?php echo $form->error($model, 'tskid'); ?>
            </div>

      
   
	
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php echo $form->labelEx($model, 'work_type'); ?>
            <?php echo CHtml::textField('work_type_name',$work_type_title,array('class'=>'form-control','readonly' => 'true')); ?>
            <?php echo $form->hiddenField($model,'work_type', array('readonly' => 'true','class'=>'form-control')); ?>	       
          
            <?php echo $form->error($model, 'work_type'); ?>
        </div>        

        <?php	
        if (!isset($model->entry_date))
            $model->entry_date = (isset(Yii::app()->session['start_date']) ? (Yii::app()->session['start_date']) : date('Y-m-d'));
        ?>

        <div class="col-md-4">
            <?php echo $form->labelEx($model, 'entry_date'); ?>
            <?php echo CHtml::activeTextField($model, 'entry_date', array("id" => "TimeEntry_entry_date", "size" => "15", 'class'=>'form-control')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'TimeEntry_entry_date',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>
            <?php echo $form->error($model, 'entry_date'); ?>
        </div>
    

 
     <div class="col-md-4">
        <?php echo $form->labelEx($model, 'completed_percent'); ?>
        <?php echo $form->textField($model, 'completed_percent', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'completed_percent'); ?>
    </div>
</div>

     <div class="row">
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('class'=>'form-control','rows' => 3)); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
     </div>
    <div class="row buttons text-center">
        <?php echo CHtml::Submitbutton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn blue')); ?>
       
       <?php
        if($type ==0){
            echo CHtml::resetButton('Close', array('onclick'=>"closeform('statusform')",'class'=>'btn')); 
            echo CHtml::resetButton('Reset', array('class' => 'btn default'));
        }
       ?>
        
    </div>
</div>
    <?php $this->endWidget(); ?>

</div><!-- form -->


