<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */
/* @var $form CActiveForm */
?>
<div class="search-report-section">
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
      <div class="col-md-3 col-sm-6 date">
          

            <?php
            if (!isset($_GET['TimeReport']['date_from']) || $_GET['TimeReport']['date_from'] == '') {
                $datefrom = date("Y-m-") . "1";
            } else {
                $datefrom = $_GET['TimeReport']['date_from'];
            }
            ?>
            <?php echo CHtml::label('Date From', ''); ?>
            <?php echo CHtml::textField('TimeReport[date_from]', $datefrom, array("id" => "date_from","class"=>"form-control")); ?>
            <?php echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button3", "class" => "pointer"));
            ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'date_from',
                'button' => 'c_button3',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>
        </div>
          <div class="col-md-3 col-sm-6 date">
            <?php
            if (!isset($_GET['TimeReport']['date_till']) || $_GET['TimeReport']['date_till'] == '') {
                $datetill = date("Y-m-d");
            } else {
                $datetill = $_GET['TimeReport']['date_till'];
            }
            ?>

            <?php echo CHtml::label('Date Upto', ''); ?>
            <?php echo CHtml::textField('TimeReport[date_till]', $datetill, array("id" => "date_till","class"=>"form-control")); ?>
            <?php echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button2", "class" => "pointer"));
            ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'date_till',
                'button' => 'c_button2',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>
        </div>
        <!--div class="col-sm-3" style="width:150px">
            <?php //echo CHtml::label('Only billable', 'billable'); ?>
            <?php //echo CHtml::checkBox('TimeReport[billable]', '', array("value" => 'Yes', "id" => "obill", 'style' => 'margin-top:12px;')); ?>
        </div-->
        <?php
        if (Yii::app()->user->role != 3) {
            ?>
            <div class="col-md-2">
                <?php
                $members = Groups::model()->findAll(
                     array(
                         'select' => 'group_id',
                         'condition' => 'group_lead=' . Yii::app()->user->id,
                ));
                foreach ($members as $data){
                    $list = Groups_members::model()->findAll(
                         array(
                             'select' => 'group_id,group_members,id',
                             'condition' => 'group_id=' . $data['group_id'],
                    ));
                    foreach($list as $groupsmem) {
                        $gpmem[] = $groupsmem['group_members'];
                    }
                }
               $gpmem[] =  Yii::app()->user->id;
                $where = '';
            $gpmem = implode(',', $gpmem); 
                echo CHtml::dropDownList('TimeReport[username]', '', CHtml::listData(Users::model()->findAll(
                                        array(
                                            'select' => array('userid,CONCAT_WS(" ",first_name,last_name) as first_name'),
                                           // 'condition' => 'status=0',
                                            //'condition' => 'status=0 AND user_type != 11',
                                            'condition' => (Yii::app()->user->role > 1) ? 'userid IN ('.$gpmem.') ' : '',
                                            'order' => 'first_name',
                                            'distinct' => true
                                )), "userid", "first_name"), array('class'=>'form-control','empty' => 'Select a Resource'));
                ?>
            </div>
              
            
            <?php
        }
        ?>
                
               
        <div class="col-md-2">
            <?php
            echo CHtml::dropDownList('TimeReport[projectid]', '', CHtml::listData(Projects::model()->findAll(
                                    array(
                                        'select' => array('pid,name'),
                                        'order' => 'name',
                                        'distinct' => true
                            )), "pid", "name"), array('class'=>'form-control','empty' => 'Select a project'));
            ?>
        </div>
            <div class="col-md-2">
                <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-sm blue')); ?>
            </div>
    
    <?php $this->endWidget(); ?>


                </div>
        </div>
                        </div>
<!-- search-form -->

