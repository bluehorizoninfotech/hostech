<?php
/* @var $this MailTemplateController */
/* @var $data MailTemplate */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->temp_id), array('view', 'id'=>$data->temp_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_name')); ?>:</b>
	<?php echo CHtml::encode($data->temp_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_content')); ?>:</b>
	<?php echo CHtml::encode($data->temp_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>