<?php
/* @var $this TaskExpiryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Task Expiries',
);

$this->menu=array(
	array('label'=>'Create TaskExpiry', 'url'=>array('create')),
	array('label'=>'Manage TaskExpiry', 'url'=>array('admin')),
);
?>

<h1>Task Expiries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
