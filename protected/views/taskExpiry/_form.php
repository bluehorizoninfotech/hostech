
<h1>Task Expiry Settings</h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-expiry-form',
	'enableAjaxValidation'=>false,
)); ?>





	<div class="row">
		<?php echo $form->labelEx($model,'day'); ?>
		<?php echo $form->textField($model,'day'); ?>
		<?php echo $form->error($model,'day'); ?>
	</div>



	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
