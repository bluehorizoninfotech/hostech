<?php
/* @var $this TaskExpiryController */
/* @var $model TaskExpiry */

$this->breadcrumbs=array(
	'Task Expiries'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TaskExpiry', 'url'=>array('index')),
	array('label'=>'Create TaskExpiry', 'url'=>array('create')),
	array('label'=>'View TaskExpiry', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TaskExpiry', 'url'=>array('admin')),
);
?>

<h1>Update TaskExpiry <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>