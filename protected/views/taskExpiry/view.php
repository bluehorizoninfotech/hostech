<?php
/* @var $this TaskExpiryController */
/* @var $model TaskExpiry */

$this->breadcrumbs=array(
	'Task Expiries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TaskExpiry', 'url'=>array('index')),
	array('label'=>'Create TaskExpiry', 'url'=>array('create')),
	array('label'=>'Update TaskExpiry', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TaskExpiry', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TaskExpiry', 'url'=>array('admin')),
);
?>

<h1>View TaskExpiry #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'day',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
