<?php
/* @var $this TaskExpiryController */
/* @var $model TaskExpiry */

$this->breadcrumbs=array(
	'Task Expiries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TaskExpiry', 'url'=>array('index')),
	array('label'=>'Manage TaskExpiry', 'url'=>array('admin')),
);
?>

<h1>Create TaskExpiry</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>