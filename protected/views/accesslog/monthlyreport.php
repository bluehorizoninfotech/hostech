<?php
 $baseUrl = Yii::app()->baseUrl; 
Yii::app()->clientScript->registerScript('myjquery', " 
    $(document).ready(function () {
        $('#dp1').datepicker();
        $('#dp2').datepicker();
        setTimeout('$(\"html\").removeClass(\"js\")', 1000);
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();

    });
");

Yii::app()->clientScript->registerCssFile($baseUrl.'/css/datepicker.css');

?>
<div class="main_content"> 
    
    <div class="row">
        
        <div class="col-md-4">
            <h2 class="heading">Monthly Status Report</h2>
            <p><?php echo date("F d Y", strtotime($start_date)).' to '.date("F d Y", strtotime($end_date)) ?></p>
            
        </div>
        
        <div class="col-md-8 clearfix sel-date">
            <div class="col-md-12">
                <?php
                    $this->renderPartial('_searchtimereport', array(
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                    ));
                ?>
            </div> 
                                 
<!--            <div class="col-md-3"> 
                <label>Date from </label>                     
                <input type="text" class="" id="dp1">                            
            </div>
            <div class="col-md-3">
                <label>to </label>
                <input type="text" class="" id="dp2">
                <a class="btn btn-default" id="submitbtn" style="margin-bottom:9px;">Go</a>
            </div>-->



        </div>

        <div class="col-md-12 table_hold">
            
            <?php
//            echo '<pre>';
//            print_r($results);die();
            
            $departments = array();  $deptnames = array();
            foreach($results as $r=>$res){
                
                if(!empty($res['department_id']) && ($res['department_id']!=0)){
                    if(!in_array($res['department_id'], $departments)){
                        array_push($departments, $res['department_id']);
                        $deptid= $res['department_id'];
                        $deptnames[$deptid] = $res['department_name'];
                     

                    }
                }
            }//print_r($deptnames);die;
                 
            ?>
            <?php 
            if(!empty($departments)){
                foreach($departments as $k => $dept){
            ?>        
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2" class="fixed_th">Department</th>
                            <th colspan="2" class="fixed_th"><?php echo $deptnames[$dept]; ?></th> 
                            <th>Total</th> 
                            <th colspan="<?php echo count($devices); ?>"></th>
                            <th>Total</th> 
                            <th colspan="<?php echo count($devices); ?>"></th>

                        </tr>
                        
                        <tr>
                            <th>Sl No.</th>
                            <th>Employee Code</th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>P</th>
                            <?php
                                foreach($devices as $dev){ 
                            ?>
                                <th><?php echo $dev['device_name']; ?> P</th>    
                            <?php    
                                }
                            ?>
                            <th>OT</th>
                            <?php
                                foreach($devices as $dev){ 
                            ?>
                                <th><?php echo $dev['device_name']; ?> OT</th>    
                            <?php    
                                }
                            ?>



                        </tr>

                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                            echo "<pre>";


                            
                            foreach($results as $res){
                               
                                if($res['department_id'] == $dept){
                                    $i++;
                        ?>            
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $res['employee_id']; ?></td>
                            <td><?php echo $res['name']; ?></td>
                            <td><?php echo $res['designation']; ?></td>
                            <?php 
                            $totalpunch=0;
                            foreach($devices as $dev){ 
                            $totalpunch += isset($res['totalpunch_'.$dev['id']])?$res['totalpunch_'.$dev['id']]:'0';  
                            
                            }?>
                            <td><?php echo $totalpunch; ?></td>
                            <?php
                               foreach($devices as $dev){ 
                            ?>
                                <td><?php echo isset($res['totalpunch_'.$dev['id']])?$res['totalpunch_'.$dev['id']]:'0'; 
                                ?></td>    
                            <?php    
                                } 
                            ?>

                            <td><?php echo isset($res['totalot'])?$res['totalot']:"0"; ?></td>
                            <?php
                                foreach($devices as $dev){ 
                            ?>
                                <td><?php echo isset($res['total_ot_'.$dev['id']])?$res['total_ot_'.$dev['id']]:'0:0:0'; ?><?php //echo $res['total_ot']; ?></td>    
                            <?php    
                                }
                            ?>

                        </tr>           
                                    
                        <?php        
                                }     
                            }
                            if($i == 0){
                        ?>
                        <tr>
                            <th colspan="12">No users found.</th>
                        </tr> 
                        <?php } ?>
                        

                    </tbody>
                </table>
            <?php        
                }
            }
            
            ?>
            
           
        </div>
         
    </div>
    
</div>