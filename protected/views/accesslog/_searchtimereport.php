<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript('myjquery', " 
    $(document).ready(function () {
        $('#date_from').datepicker({dateFormat: 'yy-mm-dd',autoclose: true});
        $('#date_till').datepicker({autoclose: true, dateFormat: 'yy-mm-dd'});
    });   


");
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl('accesslog/monthlyreportnew'),
        'method' => 'get',
    ));
    ?>

    <div class="row">
        <div class="col-md-12">

            <?php
            if (!isset($_GET['TimeEntry']['date_from']) || $_GET['TimeEntry']['date_from'] == '') {
                $datefrom = $start_date;
            } else {
                $datefrom = $_GET['TimeEntry']['date_from'];
            }
            ?>
            <?php //echo CHtml::label('Date From', '', array('class' => '')); ?>

            Date From:<?php echo CHtml::textField('TimeEntry[date_from]', $datefrom, array("id" => "date_from")); ?>

            <?php
            if (!isset($_GET['TimeEntry']['date_till']) || $_GET['TimeEntry']['date_till'] == '') {
                $datetill = $end_date;
            } else {
                $datetill = $_GET['TimeEntry']['date_till'];
            }
            ?>

            Date To:<?php //echo CHtml::label('Date To', '', array('class' => ''));  ?>
            <?php echo CHtml::textField('TimeEntry[date_till]', $datetill, array("id" => "date_till")); ?>
            <?php //echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button2", "class" => "pointer dateimg", "style" => "cursor:pointer;width:25px;"));
            ?>


            <?php echo CHtml::submitButton('GO', array('id' => 'serachbtn', 'class' => 'btn btn-sm btn-info')); ?>

        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- search-form -->
