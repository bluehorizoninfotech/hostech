<?php

$sites = CHtml::listData(Clientsite::model()->findAll(), 'id', 'site_name');

if (!isset($_GET['ajaxcall'])) {
    Yii::app()->clientScript->registerScript('myjquery', '
        
        $("#totpre").html($("#total_pre").html());
        $("#totot").html($("#total_ot").html());

        $(\'#dprtment\').val(0);
        $(\'#btn_submit_for_report\').on(\'click\', function(){
        var startdate = $(\'#startdate\').val();
        var tilldate = $(\'#tilldate\').val();
        var deptment = $(\'#dprtment\').val();
        var site = $(\'#site\').val();
        $("#loadingdiv").html("Loading...");
        $.ajax({
        url:"' . Yii::app()->createUrl('accesslog/attreport') . '",
         data:{sdate:startdate, tdate:tilldate,ajaxcall:1,deptment:deptment,site:site}
        }).done(function(msg){
            //alert(msg);
            $("#result").html(msg);
            $("#totpre").html($("#total_pre").html());
            $("#totot").html($("#total_ot").html());
        }).alwasys(function(){
            $("#loadingdiv").html("");
        });
        });');
    ?>
    <div class="main_content">
        <div class="row">
            <div class="col-md-4">
                <h1 class="heading">Attendance Report</h1>
            </div>
            <div class="col-md-8 attsearch">
                <?php echo CHtml::textField('startdate', date('Y-m-01'), array("id" => "startdate", 'size' => 10, 'Placeholder' => 'Start date','class'=>'form-control')); ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'startdate',
                    'ifFormat' => '%Y-%m-%d',
                ));
                //echo ' | ';
                echo CHtml::textField('tilldate', date('Y-m-t'), array("id" => "tilldate", 'size' => 10, 'Placeholder' => 'Till date','class'=>'form-control'));
                ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'tilldate',
                    'ifFormat' => '%Y-%m-%d',
                ));

                echo CHtml::dropDownList('department', '0', CHtml::listData(Department::model()->findAll(), 'department_id', 'department_name'), array('empty' => array('0'=>'All Department'), 'id' => 'dprtment','class'=>'form-control'));
                echo CHtml::dropDownList('sites', '0', $sites, array('empty' => array('0'=>'All Sites'), 'id' => 'site','class'=>'form-control'))
                ?>
                <?php echo CHtml::button('Submit', array('id' => 'btn_submit_for_report','class'=>'btn btn-sm blue margin-left-10')); ?>
        `   </div>
        <?php
            }
        ?>
    </div>
    <?php
    if (!isset($_GET['ajaxcall'])) {
        ?>
        <div class="row">
            <div class="col-md-12 table_hold" id='result'>
                <?php
            }
            ?>
                <div class="row">  
                    <div class="col-md-3" id='duration'><p><?php echo date("F d Y", strtotime($start_date)) . ' to ' . date("F d Y", strtotime($end_date)) ?></p></div>
                    <div class="col-md-3 red-color" id="loadingdiv">&nbsp;</div>
                    <div class="col-md-3"><h3 class="cyan-blue">[Total] Presents: <span id='totpre' class="green-color">--</span> | OT: <span id='totot'class="green-color">--</span></h3></div>
                    <div class="col-md-3"><p class="text-align-right">Total Records: <?php echo count($users) ?></p></div>
                </div>  
            <table class="table table-bordered">
                <thead>                                                
                    <tr>
                        <th rowspan="2">S.No.</th>
                        <th rowspan="2">Employee Code</th>
                        <th rowspan="2">Employee Name</th>
                        <th rowspan="2">Department</th>
                        <th rowspan="2">Designation</th>                            
                        <?php
                        $i = 1;
                        $site_header = '';
                        $sites['-1'] = 'Total';
                        //foreach ($sites as $site) {
                            ?>
                            <th rowspan="2" class="text-align-center gray-border">Site Name</th> 
                            <th colspan="2" class="text-align-center gray-border">Total</th>
                            <?php
                          //  $site_header .= '<th>Presence</th><th>OT</th>';
                            //$i++;
                        //}
                        ?>
                    </tr> 
                    <tr>
                        <th>Presence</th><th>OT</th>                       
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    
                    $stotpr = array();   //site based total present
                    $sitetotot = array();  //site based total OT
                    
                    $m = 1;
                    if (!empty($results)) {
//                         echo '<pre>';
//                            print_r($results);
//                            echo '</pre>';
//                            die;
                        $grandpresent = 0;
                        $grandot=0;
                        $ots=array();                        
                        
                        foreach ($results as $puserid => $res) {
                            ?>
                            <tr>                            
                                <td><?php echo $m++; ?></td>                                               
                                <td><?php echo $res['empid']; ?></td>
                                <td><?php echo $res['fullname']; ?></td>
                                <td><?php echo $res['dept']; ?></td>                        
                                <td><?php echo $res['designation']; ?></td>
                                <?php
                                $j = 1;
                                $totpresent = 0;
                                $tot_ot = 0;
                                foreach ($sites as $siteid => $site_name) {
                                    $tpresent = 0;
                                    if (isset($res['otarray'][$siteid])) {
                                        $tpresent = count($res['otarray'][$siteid]['daywise']);
                                    }
                                                                        
                                    $site_ot = ((isset($res['otarray'][$siteid]) and isset($res['otarray'][$siteid]['ot'])) ? array_sum($res['otarray'][$siteid]['ot']) : 0);
                                    
                                    $tot_ot += $site_ot;
                                    
                                    $ots[$res['userid']][]=(isset($res['otarray']['totot']) ? $res['otarray']['totot'] : 0);
                                    
                                     
                                    $grandot+=(isset($res['otarray'][$siteid])?array_sum($res['otarray'][$siteid]['ot']):0); 
                                    
                                    $presentcount = ((count($res['otarray']) > 0 && isset($res['otarray'][$siteid]) && isset($res['otarray'][$siteid]['daywise'])) ? count($res['otarray'][$siteid]['daywise']) : 0);
                                    $grandpresent+=$presentcount; 
                                    $totpresent+=$presentcount;
                                    
                                    $ottotl = $site_ot;
                                    if ($siteid != -1) {
                                        
                                        if(!isset($stotpr[$siteid])){
                                            $stotpr[$siteid]=0;
                                        }
                                        $stotpr[$siteid]+=$presentcount;
                                        
                                        if(!isset($sitetotot[$siteid])){
                                            $sitetotot[$siteid]=0;
                                        }
                                        $sitetotot[$siteid]+=$site_ot;

                                        if(!isset($res['otarray'][$siteid]) or count($res['otarray'][$siteid]['daywise'])==0){
                                            continue;
                                        }
                                        
                                        ?>
                                        <td <?php echo ($presentcount != 0 ? 'style="background-color:#b7ddc7"' : '') ?>><?php echo $site_name ?></td>
                                        <!--<td <?php //echo (($ottotl != 0 or strlen($ottotl) > 1) ? 'style="background-color:#b7ddc7"' : '') ?>>
                                            <?php //echo ($site_ot==0?"0":gmdate('H:i:s',$site_ot)) ?></td>-->
                                        <?php
                                    } else {   
                                        if($totpresent==0){
                                            echo '<td></td>';
                                        }
                                        ?>
                                        <td <?php echo ($totpresent != 0 ? 'style="background-color:#b7ddc7"' : '') ?>><?php echo $totpresent; ?></td>
                                        <td <?php echo (($tot_ot != 0 or strlen($tot_ot) > 1) ? 'style="background-color:#b7ddc7"' : '') ?>><?php echo gmdate('H:i:s', $tot_ot); ?> </td>
                                        <?php
                                    }
                                    $j++;
                                }
                                ?>
                            </tr>                                               
                            <?php
                        }
                        ?>
                            <tr><th colspan="6" class="text-align-right">Total</th>
                                
                                <th id='total_pre'><?php echo $grandpresent;?></th>
                                <th id="total_ot"><?php echo gmdate('H:i:s',$grandot);?></th>
                            </tr>
                        <?php
                        
                        
                        
                        
                    }
                    if (count($results) == 0) {
                        ?>
                        <tr>
                            <th colspan="<?php echo (count($sites)*2)+5 ?>">No users found.</th>
                        </tr> 
                    <?php } 
                    ?>

                </tbody>
            </table> 
            <?php
//                                
//                    echo '<pre>';
//                    print_r($stotpr);
//                    print_r($sitetotot);
//                    echo '</pre>';
//            
            ?>
            <div class="col-md-12"><p>Total Records: <?php echo count($users) ?></p></div>
            <?php
            if (!isset($_GET['ajaxcall'])) {
                ?>
            </div>
        </div>
    </div>
    <?php
}
?>