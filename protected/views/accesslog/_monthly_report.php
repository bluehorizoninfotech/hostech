<?php
Yii::app()->clientScript->registerCss('style_css',"
.rotate {

/* Safari */
-webkit-transform: rotate(-90deg);

/* Firefox */
-moz-transform: rotate(-90deg);

/* IE */
-ms-transform: rotate(-90deg);

/* Opera */
-o-transform: rotate(-90deg);

/* Internet Explorer */
filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

}

table {
  overflow: hidden;
}

td, th {
  padding: 10px;
  position: relative;
  outline: 0;
}

body:not(.nohover) tbody tr:hover {
  /*background-color: #ffa;*/
  background-color: #3B434C;
  color: #fff;
}

td:hover::after,
thead th:not(:empty):hover::after,
td:focus::after,
thead th:not(:empty):focus::after { 
  content: '';  
  height: 10000px;
  left: 0;
  position: absolute;  
  top: -5000px;
  width: 100%;
  z-index: -1;
}

td:hover::after,
th:hover::after {
  background-color: #ffa;
}

td:focus::after,
th:focus::after {
  background-color: lightblue;
}

/* Focus stuff for mobile */
td:focus::before,
tbody th:focus::before {
  background-color: lightblue;
  content: '';  
  height: 100%;
  top: 0;
  left: -5000px;
  position: absolute;  
  width: 10000px;
  z-index: -1;
}
 
"
);
Yii::app()->clientScript->registerScript('rol_col_js','
// whatever kind of mobile test you wanna do.
if (screen.width < 500) {
      $(\'body\').addClass(\'nohover\');
  $(\'td, th\')
    .attr(\'tabindex\', \'1\')
    .on(\'touchstart\', function() {
      $(this).focus();
    });
  
}
'
);
 
if (!isset($_GET['ajaxcall'])) {
?>	
<div class="float-right">
<button class="download btn blue" id="saveasexcel" href="<?php  //echo $this->createAbsoluteUrl('accesslog/exportExcel', array("start_date" => $start_date,"end_date" => $end_date)) ?>">Save as Excel</button>
</div>
<?php
}
?>


<?php
if (!isset($_GET['ajaxcall'])) {



    Yii::app()->clientScript->registerScript('myjquery', '
    
		
        $("#totpre").html($("#total_pre").html());
        $("#totot").html($("#total_ot").html());
        
        $(\'#dprtment\').val(0);
        $(\'#btn_submit_for_report\').on(\'click\', function(){
        var startdate = $(\'#startdate\').val();
        var tilldate = $(\'#tilldate\').val();
        var deptment = $(\'#dprtment\').val();
        $("#loadingdiv").html("Loading...");
        $.ajax({
        url:"' . Yii::app()->createUrl('accesslog/getmonthreport') . '",
         data:{sdate:startdate, tdate:tilldate,ajaxcall:1,deptment:deptment}
        }).done(function(msg){
            //alert(msg);
            $("#result").html(msg);
            $("#totpre").html($("#total_pre").html());
            $("#totot").html($("#total_ot").html());
        }).alwasys(function(){
            $("#loadingdiv").html("");
        });
        });
        
        
        $(\'#saveasexcel\').on(\'click\', function(){
       
			 var startdate = $(\'#startdate\').val();
			 var tilldate = $(\'#tilldate\').val();
			 var deptment = $(\'#dprtment\').val();
			 
			 var date1 = new Date(startdate);
			 var date2 = new Date(tilldate);
			 var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			 var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
			
			 var action = "' . Yii::app()->createUrl('accesslog/exportExcel&startdate=') . '"  + startdate + "&tilldate=" + tilldate + "&deptment=" + deptment;
			 
			 if(diffDays < 32){
				window.location.href = action;
			 }else{
				alert("Date range must be month period ! ");
			 }
			
			 
		 });	 
        ');
    ?>
    <div class="main_content">
        <div class="col-md-4">
            <h1 class="heading">Present & OT Report</h1>
        </div>
        <div class="col-md-6">
            <?php echo CHtml::textField('startdate', date('Y-m-01'), array("id" => "startdate", 'size' => 10, 'class' => 'grey-border height-28 border-radius-5', 'Placeholder' => 'Start date')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'startdate',
                'ifFormat' => '%Y-%m-%d',
            ));
            echo ' | ';
            echo CHtml::textField('tilldate', date('Y-m-t'), array("id" => "tilldate", 'size' => 10, 'class' => 'grey-border height-28 border-radius-5', 'Placeholder' => 'Till date'));
            ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'tilldate',
                'ifFormat' => '%Y-%m-%d',
            ));

            echo CHtml::dropDownList('department', '0', CHtml::listData(Department::model()->findAll(), 'department_id', 'department_name'), array('empty' => '', 'id' => 'dprtment','class'=>'form-control height-28 width-auto display-inline-block','empty'=>'Select Department'))
            ?>
            <?php echo CHtml::button('Submit', array('id' => 'btn_submit_for_report','class'=>'btn btn-sm blue')); ?>
        </div>
        <?php
    }
    ?>
    <?php
    if (!isset($_GET['ajaxcall'])) {
        ?>
        <div class="row">
            <div class="col-md-12 table_hold" id='result'>
                <?php
            }
            ?>
            <div class="col-md-3" id='duration'><p><?php echo date("F d Y", strtotime($start_date)) . ' to ' . date("F d Y", strtotime($end_date)) ?></p></div>
            <div class="col-md-3 red-color" id="loadingdiv">&nbsp;</div>
            <div class="col-md-3"><h3 class="cyan-blue">[Total] Presents: <span id='totpre' class="green-color">--</span> | OT: <span id='totot' class="green-color">--</span></h3></div>
            <!--div class="col-md-3"><p style="text-align: right;">Total Records: <?php //echo count($users)   ?></p></div-->
            <div class="col-xs-12 table-responsive">
            <table class="table table-bordered">
                <thead>                                                
                    <tr>
                        <th rowspan="2" class="col">Sl No.</th>
                        <th rowspan="2" class="col">Employee Code</th>
                        <th rowspan="2" class="col">Employee Name</th>
                        <th rowspan="2" class="col">Department</th>
                        <th rowspan="2" class="col">Designation</th>                            
                        <?php
                        $i = 1;
                        $sites = CHtml::listData(Clientsite::model()->findAll(), 'id', 'site_name');
                        $site_header = '';
                        $sites['-1'] = 'Total';
                        $grandpresent = 0;
                        $grandot = 0;
                        
                        foreach ($sites as $site) {
                            ?>
                            <th colspan="2" valign="middle" class="text-align-center gray-border" ><div class="text-align-center padding-top-26 padding-bottom-26"><?php echo $site; ?></div></th>                       
                            <?php
                            $site_header .= '<th  class="col">P</th><th  class="col">OT</th>';
                            $i++;
                        }
                        ?>
                    </tr> 
                    <tr> <?php
                        echo $site_header;
                        ?>                        
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    $stotpr = array();   //site based total present
                    $sitetotot = array();  //site based total OT

                    $m = 1;
                    if (!empty($results)) {
//                         echo '<pre>';
//                            print_r($results);
//                            echo '</pre>';
//                            die;

                        $ots = array();

                        foreach ($results as $puserid => $res) {
                            ?>
                            <tr>                            
                                <td  class="row" ><?php echo $m++; ?></td>                                               
                                <td  ><?php echo $res['empid']; ?></td>
                                <td  ><?php echo $res['fullname']; ?></td>
                                <td  ><?php echo $res['dept']; ?></td>                        
                                <td  ><?php echo $res['designation']; ?></td>
                                <?php
                                $j = 1;
                                $totpresent = 0;
                                $tot_ot = 0;
                                foreach ($sites as $siteid => $site_name) {
                                    $tpresent = 0;
                                    if (isset($res['otarray'][$siteid])) {
                                        $tpresent = count($res['otarray'][$siteid]['daywise']);
                                    }

                                    $site_ot = ((isset($res['otarray'][$siteid]) and isset($res['otarray'][$siteid]['ot'])) ? array_sum($res['otarray'][$siteid]['ot']) : 0);

                                    $tot_ot += $site_ot;
                                    $ots[$res['userid']][] = (isset($res['otarray']['totot']) ? $res['otarray']['totot'] : 0);

                                    

                                    $presentcount = ((count($res['otarray']) > 0 && isset($res['otarray'][$siteid]) && isset($res['otarray'][$siteid]['daywise'])) ? count($res['otarray'][$siteid]['daywise']) : 0);
                                    $grandpresent += $presentcount;
                                    $totpresent += $presentcount;

                                    $ottotl = $site_ot;
                                    if ($siteid != -1) {

                                        if (!isset($stotpr[$siteid])) {
                                            $stotpr[$siteid] = 0;
                                        }
                                        $stotpr[$siteid] += $presentcount;

                                        if (!isset($sitetotot[$siteid])) {
                                            $sitetotot[$siteid] = 0;
                                        }
                                        $sitetotot[$siteid] += $site_ot;
                                        
//                                        if($tot_ot>0){
//                                        echo $tot_ot;
//                                        die;
//                                    }
                                    $grandot += $site_ot;
                                        
                                        
                                        ?>
                                        <td <?php echo ($presentcount != 0 ? 'style="background-color:#b7ddc7"' : '') ?>><?php echo $presentcount ?></td>
                                        <td <?php echo (($ottotl != 0 or strlen($ottotl) > 1) ? 'style="background-color:#b7ddc7"' : '') ?>><?php echo ($site_ot == 0 ? "0" : gmdate('H:i', $site_ot)) ?></td>
                                        <?php
                                    } else {
                                        ?>
                                        <td <?php echo ($totpresent != 0 ? 'style="background-color:#b7ddc7"' : '') ?>><?php echo $totpresent; ?></td>
                                        <td <?php echo (($tot_ot != 0 or strlen($tot_ot) > 1) ? 'style="background-color:#b7ddc7"' : '') ?>><?php echo gmdate('H:i', $tot_ot); ?> </td>
                                        <?php
                                    }
                                    $j++;
                                }
                                ?>
                            </tr>                                               
                            <?php
                        }
                        ?>
                        <tr><td colspan="5" class="text-align-right">Total</td>
                            <?php
                            $grandpresent = 0;
                        $grandot = 0;
                            
//                        echo '<pre>';
//                        
//                        echo $grandottot = gmdate('H:i',array_sum($sitetotot));
////                       echo $grandrestot = 
//                        print_r($sitetotot);
//                        echo '</pre>';
//                     //   die;
                            foreach ($stotpr as $key => $present) {
                                
                                $grandot +=$sitetotot[$key];
                                $grandpresent+=$present;
                                
                                ?>

                                <td><?php echo $present; ?></td>
                                <td><?php echo secondsToTime($sitetotot[$key]); ?></td>

                                <?php
                            }
                            ?>

                            <td id="total_pre"><?php echo $grandpresent; ?></td>
                            <td id="total_ot"><?php echo secondsToTime($grandot); ?></td>
                        </tr>
                        <?php
                    }
                    if (count($results) == 0) {
                        ?>
                        <tr>
                            <td colspan="<?php echo (count($sites) * 2) + 5 ?>">No users found.</td>
                        </tr> 
                    <?php }
                    ?>

                </tbody>
            </table> 
            </div>
            <?php
//                                
//                    echo '<pre>';
//                    print_r($stotpr);
//                    print_r($sitetotot);
//                    echo '</pre>';
//            
            ?>
            <div class="col-md-12"><p>Total Records: <?php echo $m - 1; //echo count($users)  ?></p></div>
            <?php
            if (!isset($_GET['ajaxcall'])) {
                ?>
            </div>
        </div>
    </div>
    <?php
}


function secondsToTime($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    
    $days = $dtF->diff($dtT)->format('%a');
    $hrs = $dtF->diff($dtT)->format('%H')+($days*24);
    $mins = $dtF->diff($dtT)->format('%i');
    
    return $hrs.":".$mins;
}
?>