<?php
$baseUrl = Yii::app()->baseUrl; 
Yii::app()->clientScript->registerScript('myjquery', " 
    $(document).ready(function () {
        $('#dp1').datepicker();
        $('#dp2').datepicker();
        setTimeout('$(\"html\").removeClass(\"js\")', 1000);
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
    });       
");

Yii::app()->clientScript->registerCssFile($baseUrl.'/css/datepicker.css');

?>
<div class="main_content"> 
    
    <div class="row">
        
        <div class="col-md-4">
            <h1 class="heading">Attendance Report</h1>
            <p><?php echo date("F d Y", strtotime($start_date)).' to '.date("F d Y", strtotime($end_date)) ?></p>
            
        </div>
        
        <div class="col-md-2 padding-top-10">
            <!--<h3 class="heading">Site</h3>-->
             <?php
   
   if(Yii::app()->user->role==1){
    //print_r($accessuserdata);
    echo CHtml::dropDownList('code', '', CHtml::listData($sitedata, 'id', 'site_name'), array('class' => 'gray-border font-15', 'options' => array($code => array('selected' => true)), 
    'empty' => '-- select site --',
    'onchange' => 'document.location.href = "index.php?r=accesslog/attendancereport&code=" + this.value+""',

    ));
   }
    ?>
             </div>
            
        
        
        
        <div class="col-md-6 clearfix sel-date">
            <div class="col-md-12">
               
                
                <?php
                    $this->renderPartial('_searchattnreport', array(
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                    ));
                ?>
            </div> 
                                 
<!--            <div class="col-md-3"> 
                <label>Date from </label>                     
                <input type="text" class="" id="dp1">                            
            </div>
            <div class="col-md-3">
                <label>to </label>
                <input type="text" class="" id="dp2">
                <a class="btn btn-default" id="submitbtn" style="margin-bottom:9px;">Go</a>
            </div>-->



        </div>

        <div class="col-md-12 table_hold">
            
            <?php
//            echo '<pre>';
//            print_r($results);die();            
          
                 
            ?>
               
                <table class="table table-bordered">
                    <thead>
                      
                        <tr>
                            <th>Sl No.</th>
                            <th>Employee Name</th>
                            <th>Employee Code</th>                            
                            <th>Designation</th>
                            <th>Site Name</th>
                            <th>Attendance</th>                
                            <th>OT</th>                           
                        </tr>

                    </thead>
                    <tbody>  
                        
                        <?php
                        $j=1;
                        if(count($results)> 0 ){
                        for($i=0;$i<count($results);$i++){ ?>
                        <tr>
                            <td><?php echo $j; ?></td>
                            <td><?php echo $results[$i]['name']; ?></td>
                            <td><?php echo $results[$i]['employee_id']; ?></td>                            
                            <td><?php echo $results[$i]['designation']; ?></td>
                            <td><?php echo $results[$i]['site_name']; ?></td>
                            <td><?php echo $results[$i]['totalpunch1']; ?></td>   
                            <td><?php if($results[$i]['total_ot1'] != '0:0:0'){ 
                                echo $results[$i]['total_ot1']; 
                                }else{
                                echo "0";    
                                } ?></td>                               
                        </tr>  
                        
                        <?php 
                        $j++;
                        }
                        ?>
                         <tr>
                             <td colspan="5" class="text-align-right">Total Hours</td>
                            
                            <td ><?php echo $totalhrs; ?></td>
                            
                        </tr>
                        <?php
                        }else{ ?>                                               
                        
                        <tr>
                            <th colspan="12">No users found.</th>
                        </tr> 
                        <?php } ?>
                       
                    </tbody>
                </table>
            <?php        
           
            ?>
            
           
        </div>
         
    </div>
    
</div>