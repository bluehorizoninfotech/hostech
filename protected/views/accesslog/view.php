
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

<?php
$action = Yii::app()->controller->action->id;
if($_SERVER['REMOTE_ADDR']=='192.168.1.97'){
	echo $_SERVER['REMOTE_ADDR'];
	?>
<div class="assesslog-view-sec">    
<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch Demo Modal</a>
    
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to save changes you made to document before closing?</p>
                    <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>     


    <script type="text/javascript">

    $(document).ready(function(){

        $(".btn").click(function(){

            $("#myModal").modal('show');

        });

    });

    </script>


	<?php
}

?>
<h2>Log Last Updated: <?php echo $lastsync; ?></h2>
<table cellpadding="10" cellspacing="0" border="1" class="logtable red-border width-1000">
    <tr>
        <td colspan="9" class="font-18 padding-10">
            <div class="datetime float-left min-width-300 light-blue">
                <?php
                if ($days == 0) {
                    echo date('d-m-Y <\s\p\a\n>h:i:s A</\s\p\a\n> l');
                } else {
                    echo date('d-m-Y l', strtotime($precent_date));
                }
                ?>
            </div>
            <div class="float-left width-300">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'method' => 'get',
                    'id' => 'access-log',
                    'action' => Yii::app()->createUrl('accesslog/'.$action),
                ));
                ?>
                
                <?php echo CHtml::activeTextField($model, 'log_time', array("id" => "log_time", 'size' => 10,'placeholder'=>'Choose date')); ?> &nbsp;&nbsp; <?php echo CHtml::submitButton('Submit'); ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'log_time',
                    'ifFormat' => '%Y-%m-%d',
                ));
                ?> 
                <?php $this->endWidget(); ?>
                
            </div>

            <div class="min-width-200 float-right">
                <a href="<?php echo Yii::app()->createUrl('accesslog/'.$action, array('days' => $days - 1)) ?>">Previous</a>  |
                <?php
                if ($days >= 0) {
                    echo 'Today | Next';
                } else {
                    ?>
                    <a href="<?php echo Yii::app()->createUrl('accesslog/'.$action, array('days' => 0)) ?>">Today</a>  | 
                    <a href="<?php echo Yii::app()->createUrl('accesslog/'.$action, array('days' => $days + 1)) ?>">Next</a>
                    <?php
                }
                ?>
            </div></td></tr>
    <tr class="tablehead"><th width="25">Sno#</th><th>Access Card Id</th><th>Resource Name</th><th><?=($days==0?'Current':'Last')?> status</th><th>First Punch time</th>
        <th>Last Punch time</th><th>Total Out Time</th><th>Total IN Time</th>
        <th>Total Hrs</th></tr>
    <?php
    $i = 0;
    $k = 1;
    foreach ($logemp as $item) {
        $cardid = $item['accesscard_id'];
        $dbtime = $item['dbtime'];
        //$dbtime = $item['cdate'] . ' 18:30:00';

        if ($days < 0) {
            $dbtime = $item['lpunch'];
        } else if ($days == 0 && strtotime($item['dbtime']) > strtotime($item['cdate'] . ' 19:30:00')) {
            //$dbtime = $item['cdate'] . ' 18:30:00';
            $dbtime = $item['lpunch'];
        } else {
          // die();
        }

        $totl_intime = (isset($inout[$cardid]) ? $inout[$cardid]['IN'] + ($item['count_punch'] % 2 == 1 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');
        $total_outtime = (isset($inout[$cardid]) ? $inout[$cardid]['OUT'] + ($item['count_punch'] % 2 == 0 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');
        echo '<tr class="' . ($i % 2 == 1 ? 'odd' : 'even') . '"><td>' . $k++ . '</td><td width="50" style="text-align:center" bgcolor="#ADD6FF">' . $cardid . '</td>'
        . '<td width="200"><b>' . $item['username'] . '</b></td>';

        if ($item['ipunch'] == '' && $item['lpunch'] == '') {
            echo '<td width="150" style="color:red;" colspan="6">No Punch</td>';
        } else {
            echo '<td width="150" bgcolor="' . ($item['count_punch'] % 2 == 1 ? '#8AE62E' : '#FF8080') . '">'. ($item['count_punch'] % 2 == 1 ? 'Punched IN' : 'Punched OUT') . '</td>';

            echo '<td width="105">' . date('h:i:s A', strtotime($item['ipunch'])) . '</td>'
            . '<td width="105">' . date('h:i:s A', strtotime($item['lpunch'])). '<!--(' . $item['count_punch'] . ')--></td>'
            . '<td width="80">' . $this->calculate_time_span($total_outtime) . '</td>'
            . '<td width="80" bgcolor="#ADD6FF">' . $this->calculate_time_span($totl_intime) . '</td>';

            echo '<td width="60" bgcolor="#ADD6FF">' . ($this->calculate_time_span($totl_intime + $total_outtime)) . '</td>';
        }
        echo "</tr>";
        $i++;
    }
    ?>
</table>
</div>
<?php
/*
function datetotime($date) {
    return date('h:i:s A', strtotime($date));
}*/
?>