<?php
//echo "<pre>";
//print_r($results);
//die;
$baseUrl = Yii::app()->baseUrl;
Yii::app()->clientScript->registerScript('myjquery', " 
    $(document).ready(function () {
        $('#dp1').datepicker();
        $('#dp2').datepicker();
        setTimeout('$(\"html\").removeClass(\"js\")', 1000);
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
    });
");
Yii::app()->clientScript->registerCssFile($baseUrl . '/css/datepicker.css');
?>

<div class="main_content"> 

    <div class="row">

        <div class="col-md-4">
            <h2 class="heading">Monthly Report</h2>
        </div>
        <div class="col-md-3">
            <?php echo CHtml::textField('startdate', date('Y-m-01'), array("id" => "startdate", 'size' => 10, 'Placeholder' => 'Start date')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'startdate',
                'ifFormat' => '%Y-%m-%d',
            ));
            echo ' | ';
            echo CHtml::textField('tilldate', date('Y-m-t'), array("id" => "tilldate", 'size' => 10, 'Placeholder' => 'Till date'));
            ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'tilldate',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>   

            <?php echo CHtml::button('Submit', array('id' => 'btn_submit_for_report')); ?>
        </div>


        <div class="col-md-12 table_hold" id='result'>
            <div class="col-md-12 padding-left-0" id='duration'><p><?php echo date("F d Y", strtotime($start_date)) . ' to ' . date("F d Y", strtotime($end_date)) ?></p></div>
            <?php
//            echo '<pre>';
//            print_r($results);die();
            //$departments = array();  $deptnames = array();
//            foreach($results as $r=>$res){
//                
//                if(!empty($res['department_id']) && ($res['department_id']!=0)){
//                    if(!in_array($res['department_id'], $departments)){
//                        array_push($departments, $res['department_id']);
//                        $deptid= $res['department_id'];
//                        $deptnames[$deptid] = $res['department_name'];
//                     
//
//                    }
//                }
//            }
//print_r($deptnames);die;
            ?>
            <?php
//                    print_r($res);
//                    die;                    
            ?>        
            <table class="table table-bordered">
                <thead>                                                
                    <tr>
                        <th rowspan="2">Sl No.</th>
                        <th rowspan="2">Employee Code</th>
                        <th rowspan="2">Employee Name</th>
                        <th rowspan="2">Department</th>
                        <th rowspan="2">Designation</th>                            
                        <?php
                        $i = 1;
                        foreach ($total_sites as $site) {
                            ?>
                            <th colspan="2" class="text-align-center gray-border"><?php echo $site['site_name']; //$dev['device_name'];      ?>                                                                                                                                                                                              
                            </th>                       
                            <?php
                            $i++;
                        }
                        ?>                                                          
                    </tr> 
                    <tr>
                        <?php
                        $i = 1;
                        foreach ($total_sites as $site) {
                            ?>
                            <th>Presence </th>  
                            <th>OT</th>                                                         
                            <?php
                            $i++;
                        }
                        ?>  
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    $m = 1;
                    if (!empty($users)) {
                        foreach ($users as $res) {
                            ?>
                            <tr>                            
                                <td><?php echo $m; ?></td>                                               
                                <td><?php echo $res['employee_id']; ?></td>
                                <td><?php echo $res['fullname']; ?></td>
                                <td><?php echo $res['department_name']; ?></td>                        
                                <td><?php echo $res['designation']; ?></td>                         
                                <?php
                                $j = 1;
                                foreach ($total_sites as $site) {
                                    ?>
                                    <td>Presence <?php echo $j; ?></td>
                                    <td>OT <?php echo $j; ?> </td>
                                    <?php
                                    $j++;
                                }
                                ?>
                            </tr>                                               
                            <?php
                            $m++;
                        }
                    }
                    if ($i == 0) {
                        ?>
                        <tr>
                            <th colspan="12">No users found.</th>
                        </tr> 
                    <?php } ?>                        
                </tbody>
            </table>                                   
        </div>

    </div>

</div>

<?php
Yii::app()->clientScript->registerScript('myjquery', '
        $(\'#btn_submit_for_report\').on(\'click\', function(){
var startdate = $(\'#startdate\').val();
var tilldate = $(\'#tilldate\').val();

$.ajax({
url:"' . Yii::app()->createUrl('accesslog/getmonthreport') . '",
 data:{sdate:startdate, tdate:tilldate}
}).done(function(msg){
    //alert(msg);
    $("#result").html(msg);
});
});');
?>