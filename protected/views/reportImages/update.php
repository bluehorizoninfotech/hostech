<?php
/* @var $this ReportImagesController */
/* @var $model ReportImages */

$this->breadcrumbs=array(
	'Report Images'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ReportImages', 'url'=>array('index')),
	array('label'=>'Create ReportImages', 'url'=>array('create')),
	array('label'=>'View ReportImages', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ReportImages', 'url'=>array('admin')),
);
?>

<h1>Update ReportImages <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>