<?php
/* @var $this ReportImagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Report Images',
);

$this->menu=array(
	array('label'=>'Create ReportImages', 'url'=>array('create')),
	array('label'=>'Manage ReportImages', 'url'=>array('admin')),
);
?>

<h1>Report Images</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
