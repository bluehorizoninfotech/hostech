<?php
/* @var $this ReportImagesController */
/* @var $model ReportImages */

$this->breadcrumbs=array(
	'Report Images'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ReportImages', 'url'=>array('index')),
	array('label'=>'Manage ReportImages', 'url'=>array('admin')),
);
?>

<h1>Create ReportImages</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>