<?php
/* @var $this ReportImagesController */
/* @var $model ReportImages */

$this->breadcrumbs=array(
	'Report Images'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ReportImages', 'url'=>array('index')),
	array('label'=>'Create ReportImages', 'url'=>array('create')),
	array('label'=>'Update ReportImages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ReportImages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ReportImages', 'url'=>array('admin')),
);
?>

<h1>View ReportImages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'wpr_id',
		'created_by',
		'created_date',
	),
)); ?>
