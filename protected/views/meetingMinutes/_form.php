<?php
/* @var $this MeetingMinutesController */
/* @var $model MeetingMinutes */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'meeting-minutes-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_id'); ?>
		<?php echo $form->textField($model,'meeting_id'); ?>
		<?php echo $form->error($model,'meeting_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'points_discussed'); ?>
		<?php echo $form->textArea($model,'points_discussed',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'points_discussed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action_by'); ?>
		<?php echo $form->textField($model,'action_by'); ?>
		<?php echo $form->error($model,'action_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'submission_date'); ?>
		<?php echo $form->textField($model,'submission_date'); ?>
		<?php echo $form->error($model,'submission_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
		<?php echo $form->error($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->