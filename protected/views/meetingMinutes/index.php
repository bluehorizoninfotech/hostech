<?php
/* @var $this MeetingMinutesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Meeting Minutes',
);

$this->menu=array(
	array('label'=>'Create MeetingMinutes', 'url'=>array('create')),
	array('label'=>'Manage MeetingMinutes', 'url'=>array('admin')),
);
?>

<h1>Meeting Minutes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
