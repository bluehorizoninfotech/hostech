<?php
/* @var $this MeetingMinutesController */
/* @var $model MeetingMinutes */

$this->breadcrumbs=array(
	'Meeting Minutes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MeetingMinutes', 'url'=>array('index')),
	array('label'=>'Create MeetingMinutes', 'url'=>array('create')),
	array('label'=>'View MeetingMinutes', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MeetingMinutes', 'url'=>array('admin')),
);
?>

<h1>Update MeetingMinutes <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>