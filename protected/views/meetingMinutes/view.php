<?php
/* @var $this MeetingMinutesController */
/* @var $model MeetingMinutes */

$this->breadcrumbs=array(
	'Meeting Minutes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MeetingMinutes', 'url'=>array('index')),
	array('label'=>'Create MeetingMinutes', 'url'=>array('create')),
	array('label'=>'Update MeetingMinutes', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MeetingMinutes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MeetingMinutes', 'url'=>array('admin')),
);
?>

<h1>View MeetingMinutes #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'meeting_id',
		'points_discussed',
		'action_by',
		'submission_date',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
