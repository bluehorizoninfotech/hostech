<?php
/* @var $this MeetingMinutesController */
/* @var $model MeetingMinutes */

$this->breadcrumbs=array(
	'Meeting Minutes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MeetingMinutes', 'url'=>array('index')),
	array('label'=>'Manage MeetingMinutes', 'url'=>array('admin')),
);
?>

<h1>Create MeetingMinutes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>