<?php
/* @var $this MeetingMinutesController */
/* @var $data MeetingMinutes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_id')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('points_discussed')); ?>:</b>
	<?php echo CHtml::encode($data->points_discussed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action_by')); ?>:</b>
	<?php echo CHtml::encode($data->action_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('submission_date')); ?>:</b>
	<?php echo CHtml::encode($data->submission_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>