<?php
/* @var $this ManualentryDateController */
/* @var $model ManualentryDate */

$this->breadcrumbs=array(
	'Manualentry Dates'=>array('index'),
	'Manage',
);

?>

<h1>Manage Manual Entries</h1>

<?php if(Yii::app()->user->hasFlash('error')):?>
<div class="alert alert-danger">
   <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>

<div class="form gray-bg">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'manualentry-date-form',
         'enableAjaxValidation'=>false,  
         'stateful'=>true,
    ));
    ?>

    <div class="row">
        <div class="col-md-3">
        <?php echo $form->labelEx($model2, 'site_id'); ?>
            
        <?php
         if (Yii::app()->user->role == 1) {
        ?>     
        <?php echo $form->dropDownList($model2, 'site_id', CHtml::listData(Clientsite::model()->findAll(
								array('order' => 'site_name ASC')),
								 'id', 'site_name'), array('class'=>'form-control','empty' => '-Choose a site-')); ?>
        
        <?php } else{ ?>    
        <?php echo $form->dropDownList($model2, 'site_id', CHtml::listData(Clientsite::model()->findAll(
								array(
                                                                 'order' => 'site_name ASC',
                                                                 'condition'=>'id IN (select site_id from pms_clientsite_assigned where user_id ='.Yii::app()->user->id.' )', 
                                                                  )),'id', 'site_name'), array('class'=>'form-control','empty' => '-Choose a Site-')); ?>
        <?php } ?>
        <?php echo $form->error($model2, 'site_id'); ?>
        </div>
        <div class="col-md-3">
		<?php echo $form->labelEx($model2,'date'); ?>
		<?php echo CHtml::activeTextField($model2, 'date', array('class'=>'form-control' ,"value" => (($model2->isNewRecord) ? date('Y-m-d') : date('Y-m-d', strtotime($model->date))),)); ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'ManualentryDate_date',
                    'ifFormat' => '%Y-%m-%d',
                ));
                ?>
		<?php echo $form->error($model2,'date'); ?>
	</div>
    
    <div class="col-md-3">
        <label>&nbsp;</label>
        <?php echo CHtml::submitButton($model2->isNewRecord ? 'Add' : 'Save', array('class'=>'btn btn-sm blue')); ?>
    </div>
   </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->




<div class="form">		

<!--<h5>Filter By :</h5>-->
	<?php $form=$this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
	)); ?>
	<div class=" filter">
		<div class="row ">
			
			<div class="col-md-2 col-sm-5 col-xs-5">	
			 <?php echo $form->label($model, 'site_id'); ?>
			 <?php echo $form->dropDownList($model, 'site_id', CHtml::listData(Clientsite::model()->findAll(array(
								'order' => 'site_name ASC'
								)), 'id', 'site_name'), array('empty' => '-Choose a site-','class' => 'form-control')); ?>
			</div>
			<?php //if(Yii::app()->user->role == 1) { 
				
			$tblpx=yii::app()->db->tablePrefix;
								
			$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
					. "`{$tblpx}user_roles`.`role` "
					. "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
					. "WHERE  user_type!=11  ORDER BY user_type,full_name ASC";
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$listdata = CHtml::listData($result, 'userid', 'full_name','role');?>
			
			<div class="col-md-2 col-sm-5 col-xs-5">	
			 <?php echo $form->label($model, 'created_by'); ?>
			 <?php echo $form->dropDownList($model, 'created_by', $listdata , array('empty' => '-Choose a user-','class' => 'form-control')); ?>
			</div>
			<?php //} ?>
			<div class="col-md-3 col-sm-5 col-xs-5 date">						
						<?php echo $form->label($model, 'date'); ?>
						<?php echo CHtml::activeTextField($model, 'fromdate', array('class'=>'form-control display-inline-block','size'=>10,'placeholder' => 'From date','value'=>isset($_REQUEST['ManualentryDate']['fromdate']) ? $_REQUEST['ManualentryDate']['fromdate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'ManualentryDate_fromdate',
							'ifFormat' => '%Y-%m-%d',
						));
						?> 
						<?php echo CHtml::activeTextField($model, 'todate', array('class'=>'form-control display-inline-block','size'=>10,'placeholder' => 'To date','value'=>isset($_REQUEST['ManualentryDate']['todate']) ? $_REQUEST['ManualentryDate']['todate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'ManualentryDate_todate',
							'ifFormat' => '%Y-%m-%d',
						));
						?>
						
					</div>
      
			<div class="col-md-2 col-sm-3 col-xs-3">	
				<label>&nbsp;</label>
				<div class= "text-left">
				<?php echo CHtml::submitButton('Go', array('class'=>'btn btn-sm blue')); ?>
				<?php echo CHtml::resetButton('Clear', array('class'=>'btn btn-sm btn-default','onclick' => 'javascript:location.href="'. $this->createUrl('index').'"')); ?>
				</div>
			</div>
	</div>

</div>
	<?php $this->endWidget(); ?>			
</div>				
				
				

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manualentry-date-grid',
	'dataProvider'=>$model->search(),
         'itemsCssClass' => 'table table-bordered',
	'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
        'nextPageLabel'=>'Next ' ),    
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	//'filter'=>$model,
	'columns'=>array(
                 array('class' => 'IndexColumn', 'header' => 'S.No.'),
		//'id',
		//'site_id',
                array(
                    'name' => 'site_id',
                    'value' => '(isset($data->site) ? $data->site->site_name : "")',  
                    'filter' =>false
                ),
		//'date',
		array(
			'name' => 'date',
			'filter' =>false,
		),
		//'created_by',
                array(
                    'name' => 'created_by',
                    'value' => '(isset($data->createdBy) ? $data->createdBy->first_name." ".$data->createdBy->last_name : "")', 
                    'filter' =>false 
                ),
		//'created_date',
		//'updated_by',
		/*
		'updated_date',
		*/
		// array(
		// 	'class'=>'CButtonColumn',
  //                       'template'=>'{update}',
		// ),
    array(
        'class'=>'CButtonColumn',
        'template'=>'{mobileupdate}',
        'buttons'=>array
        (
            'update' => array
            (
                'imageUrl'=>Yii::app()->request->baseUrl.'/images/update.png',
                'url'=>'Yii::app()->createUrl("manualentryDate/update", array("id"=>$data->id))',
            ),
            'mobileupdate' => array
            (
                'imageUrl'=>Yii::app()->request->baseUrl.'/images/mobile_edit.png',
                'url'=>'Yii::app()->createUrl("manualentryDate/mobileupdate", array("id"=>$data->id))',
            ),
        ),
    ),
	),
)); ?>