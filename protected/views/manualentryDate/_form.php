<?php Yii::app()->clientScript->registerScriptFile("https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js", CClientScript::POS_END); ?>
<?php
Yii::app()->clientScript->registerScript("checktime", "
$(function(){    
$('input[class=\"input_time\"]').inputmask(\"hh:mm\", {
        placeholder: \"HH:00\", 
        showMaskOnHover: false,
      }
   );
});    


$(document).on('change', '.table input[name=\"last_punch[]\"]', function(){
    var last_time = this.value;
    var first_time = $(this).closest('tr').find('input[name=\"first_punch[]\"]').val();

    if(last_time < first_time){
    alert('Last-Punch must be greater than First-punch');
        $('input[type=submit]').attr('disabled','disabled');
    }else{
        $('input[type=submit]').removeAttr('disabled');
    }

});

$(document).on('change', '.table input[name=\"total_out_time[]\"]', function(e){
    var out_time = this.value;
    var first_time = $(this).closest('tr').find('input[name=\"first_punch[]\"]').val();
    var last_time = $(this).closest('tr').find('input[name=\"last_punch[]\"]').val();
    var time = last_time-first_time;
   // alert(time);

    start = first_time.split(':');
    end = last_time.split(':');
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    var diff = (hours < 9 ? '0' : '') + hours + ':' + (minutes < 9 ? '0' : '') + minutes;

    if(out_time > diff){
    alert('Invalid Out Time');
        $('input[type=submit]').attr('disabled','disabled');
    }else{
        $('input[type=submit]').removeAttr('disabled');
    }
    

});


"
, CClientScript::POS_END);
?>

<form  method="POST"  id="form" name="form" action="<?php echo $this->createAbsoluteUrl('manualentryDate/update&id='.$id); ?>" >

<table class="table table-bordered">
<thead>
    <tr>
        <th>Sl NO</th>
        <th>User Name</th>
        <th>First Punch</th>
        <th>Last Punch</th>
        <th>Total Out Time</th>
    </tr>
    
</<thead>
<tbody>
<?php $i =1;
foreach($users as $u)
{ 
$tbl = Yii::app()->db->tablePrefix;  
$newarr = Yii::app()->db->createCommand()
->select("{$tbl}manualentry.*")
->from("{$tbl}manualentry")
->where("user_id=:uid AND site_id =:site  AND added_date =:date",
        array(':uid'=>$u['user_id'],':site'=>$u['site_id'],':date'=>$u['date']))    
->queryRow();
    
          
?>
<tr> 
  
<td>
    <?php echo $i;?>
</td>
<td>
    <?php echo $u['first_name'].' '.$u['last_name'];?>
    <input type='hidden' name="user_id[]" value="<?php echo $u['user_id'];?>">
    <input type='hidden' name="entry_id[]" value="<?php echo (isset($newarr['id']) ? $newarr['id'] : "") ?>"">
</td>
<td>
    <input type='text' class="input_time" name="first_punch[]" placeholder="eg: 13:00 for 1 PM" value="<?php echo (isset($newarr['first_punch']) ? $newarr['first_punch'] : "") ?>"  required><?php //echo gmdate("H:i:s", $newarr['first_punch']);?>
</td>
<td>  
    <input type='text' class="input_time" name="last_punch[]" placeholder="eg: 13:00 for 1 PM" value="<?php echo (isset($newarr['last_punch']) ? $newarr['last_punch'] : "") ?>"   required>
</td>
<td>
    <input type='text' class="input_time" name="total_out_time[]" placeholder="eg: 1:00 for 1 hour" value="<?php echo (isset($newarr['total_out_time']) ? $newarr['total_out_time'] : "") ?>" required>
</td>

</tr>

<?php $i++;} ?>

<tr>
    <td colspan="6" class="text-center">
    <input type="submit" name="ManualentryDate" value="submit" class="btn btn-info"  id="save_btn">
    </td>
</tr>

</tbody>
</table>
</form>



<br>
<h1>Previous History</h1>
<table class="table table-bordered">
<thead>
    <tr>
        <th>Sl NO</th>
        <th>User Name</th>
        <th>Site</th>
        <th>First Punch</th>
        <th>Last Punch</th>
        <th>Total Out Time</th>
    </tr>
    
</<thead>
<tbody>
<?php $j =1;
foreach($entries as $entry)
{ 
       
?>
<tr> 
  
<td>
    <?php echo $j;?>
</td>
<td>
    <?php echo $entry['first_name'].' '.$entry['last_name'];?>
</td>
<td>
    <?php echo $entry['site_name'];?>
</td>
<td>
    <?php echo $entry['first_punch'];?>
</td>
<td>  
    <?php echo $entry['last_punch'];?>
</td>
<td>
    <?php echo $entry['total_out_time'];?>
</td>

</tr>

<?php $j++;} ?>


</tbody>
</table>