<?php
/* @var $this ManualentryDateController */
/* @var $model ManualentryDate */

$this->breadcrumbs=array(
	'Manualentry Dates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ManualentryDate', 'url'=>array('index')),
	array('label'=>'Manage ManualentryDate', 'url'=>array('admin')),
);
?>

<h1>Create ManualentryDate</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>