<?php
/* @var $this ManualentryDateController */
/* @var $model ManualentryDate */

$this->breadcrumbs=array(
	'Manualentry Dates'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ManualentryDate', 'url'=>array('index')),
	array('label'=>'Create ManualentryDate', 'url'=>array('create')),
	array('label'=>'Update ManualentryDate', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ManualentryDate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ManualentryDate', 'url'=>array('admin')),
);
?>

<h1>View ManualentryDate #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'site_id',
		'date',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
