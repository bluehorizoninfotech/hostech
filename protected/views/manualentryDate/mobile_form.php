<?php $id = $_GET['id'];?>
<link type="text/css" rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<!--<link type="text/css" rel="stylesheet" href="https://weareoutman.github.io/clockpicker/dist/bootstrap-clockpicker.css">
<link type="text/css" rel="stylesheet" href="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.css">-->



<?php $url =  Yii::app()->createAbsoluteUrl('manualentryDate/saveindividualdata&id='.$id);

$delurl =  Yii::app()->createAbsoluteUrl('manualentryDate/deleteindividualdata&id='.$id);
?>
<div class="mobile-form-sec">
<div id="punch_reportUpdate">
<h2>Manual Entries / Site : <?php echo $model->site->site_name; ?> / Date:   <?php echo $model->date; ?> </h2>
<h4><i>Note:Click on user's name to add an entry.</i></h4>
<?php Yii::app()->clientScript->registerCssFile("https://weareoutman.github.io/clockpicker/dist/bootstrap-clockpicker.css"); ?>
<?php Yii::app()->clientScript->registerCssFile("https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.css"); ?>
<?php Yii::app()->clientScript->registerScriptFile("https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js", CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile("https://weareoutman.github.io/clockpicker/dist/bootstrap-clockpicker.js", CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile("https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.js", CClientScript::POS_END); ?>
    <?php
Yii::app()->clientScript->registerScript("checktime", "


$(document).ready(function(){   
$('input[class=\"form-control\"]').inputmask(\"hh:mm\", {
        placeholder: \"HH:00\", 
        showMaskOnHover: false,
      }
   );
});    
$('.clockpicker').clockpicker();




$(document).on('change', '.table input[name=\"last_punch[]\"]', function(){
    var last_time = this.value;
    var first_time = $(this).closest('tr').find('input[name=\"first_punch[]\"]').val();

    if(last_time < first_time){
    alert('Last-Punch must be greater than First-punch');
        $('input[type=submit]').attr('disabled','disabled');
    }else{
        $('input[type=submit]').removeAttr('disabled');
    }

});

$(document).on('change', '.table input[name=\"total_out_time[]\"]', function(e){
    var out_time = this.value;
    var first_time = $(this).closest('tr').find('input[name=\"first_punch[]\"]').val();
    var last_time = $(this).closest('tr').find('input[name=\"last_punch[]\"]').val();
    var time = last_time-first_time;
   // alert(time);

    start = first_time.split(':');
    end = last_time.split(':');
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);
    var diff = (hours < 9 ? '0' : '') + hours + ':' + (minutes < 9 ? '0' : '') + minutes;

    if(out_time > diff){
    alert('Invalid Out Time');
        $('input[type=submit]').attr('disabled','disabled');
    }else{
        $('input[type=submit]').removeAttr('disabled');
    }
    

});

$(document).ready(function(){
    $('.button-save').click(function(){
        var element = this;
        var userId = $(this).closest('.manual_report').find('#u_id').val();
        var entryId = $(this).closest('.manual_report').find('#en_id').val();
        var firstPunch = $(this).closest('.manual_report').find('#firstpunch').val();
        var lastPunch = $(this).closest('.manual_report').find('#lastpunch').val();
        var totalOut = $(this).closest('.manual_report').find('#totalout').val();
        
        
        var time = firstPunch-lastPunch;
		start = firstPunch.split(':');
		end = lastPunch.split(':');
		var startDate = new Date(0, 0, 0, start[0], start[1], 0);
		var endDate = new Date(0, 0, 0, end[0], end[1], 0);
		var diff = endDate.getTime() - startDate.getTime();
		var hours = Math.floor(diff / 1000 / 60 / 60);
		diff -= hours * 1000 * 60 * 60;
		var minutes = Math.floor(diff / 1000 / 60);
		var diff = (hours < 9 ? '0' : '') + hours + ':' + (minutes < 9 ? '0' : '') + minutes;

		
        if(firstPunch == '' || lastPunch == '' || totalOut == '' ){
        alert('Please Enter all Fields');		
        }
        else if(lastPunch < firstPunch){
		alert('Last-Punch must be greater than First-punch');
		}
        else if(totalOut >= diff){
		alert('Invalid Out Time');
		}
		
        else{
        
        $(element).closest('.manual_report').find('input[type=submit]').attr('disabled','disabled');
        
        $.ajax({
            url: '".$url."',
            type: 'POST',
            dataType: 'json',
            data: {
                'userId' :userId, 'entryId' :entryId, 'first' :firstPunch, 'last' :lastPunch, 'total' :totalOut
            },
            success: function(data){
           // alert(JSON.stringify(data));
                $(element).closest('.manual_report').find('#firstData').html(data.f_punch);
                $(element).closest('.manual_report').find('#lastData').html(data.l_punch);
                $(element).closest('.manual_report').find('#outData').html(data.t_out);
                if((data.f_punch!= '') && (data.l_punch!= '') && (data.t_out!= '')){
					//$(element).closest('.manual_report').find('input[type=submit]').attr('disabled','disabled');
                    $(element).closest('.manual_report').find('h4.user_name').addClass('status');
                    $('.update_block').hide();
                    $('.view_block').removeClass('hide');
                }
            }
        });
        }
       
    });
    $('.button-delete').click(function(){
        var element = this;
        var userId = $(this).closest('.manual_report').find('#u_id').val();
        var entryId = $(this).closest('.manual_report').find('#en_id').val();
         
        if(confirm('Are you sure to delete this time entry?')){	
        //$(element).closest('.manual_report').find('input[type=submit]').attr('disabled','disabled');
        
        $.ajax({
            url: '".$delurl."',
            type: 'POST',
            dataType: 'json',
            data: {
                'userId' :userId, 'entryId' :entryId
            },
            success: function(data){
             //alert(JSON.stringify(data));
             $(element).hide();
             $(element).closest('.manual_report').find('#en_id').val('');
             $(element).closest('.manual_report').find('#firstpunch').val('');
                $(element).closest('.manual_report').find('#lastpunch').val('');
                $(element).closest('.manual_report').find('#totalout').val('');

                $(element).closest('.manual_report').find('#firstData').html('');
                $(element).closest('.manual_report').find('#lastData').html('');
                $(element).closest('.manual_report').find('#outData').html('');
		   // $(element).closest('.manual_report').find('input[type=submit]').attr('disabled','disabled');
                    $(element).closest('.manual_report').find('h4.user_name').removeClass('status');
                    $('.update_block').hide();
                    $('.view_block').removeClass('hide');
             }
        });
        }
       
    });
});

    $(document).ready(function(){
        $('.user_name').click(function(){
            $(this).next('.update_block').slideToggle('slow');
            $(this).next('.update_block').next('.view_block').toggleClass('hide');
        })
    });

"
, CClientScript::POS_END);
?>


<div class="list_hold">
<?php $i =1;
$countinside = 0;
	
	
	if(isset($users) && count($users) > 0){
	$count  = count($users);
	
    foreach($users as $u)
    { 
	$tbl = Yii::app()->db->tablePrefix; 
	$checkentry =   Yii::app()->db->createCommand()
	->select("*")
	->from("{$tbl}device_accessids")
	->where('userid = '.$u['user_id'])    
	->queryAll();
    $flag =0;
    foreach($checkentry as $ch){
	   
	   $check =   Yii::app()->db->createCommand()
		->select("count(*)")
		->from("{$tbl}accesslog")
		->where('empid = '.$ch['accesscard_id'].' AND device_id = '.$ch['deviceid'].' AND DATE(log_time) = " '.$u['date'].'" AND manual_entry_status IS NULL' )    
		->queryScalar();
		
		if($check > 0){
			$flag =1;
		}
	   
	}
	
	if($flag == 0){
	
	$countinside++;
	

    $newarr = Yii::app()->db->createCommand()
    ->select("{$tbl}manualentry.*")
    ->from("{$tbl}manualentry")
    ->where("user_id=:uid AND site_id =:site  AND added_date =:date",
            array(':uid'=>$u['user_id'],':site'=>$u['site_id'],':date'=>$u['date']))    
    ->queryRow();
?>

    <div class="manual_report">
    <!-- <form  method="POST"  id="form" name="form" action="<?php //echo $this->createAbsoluteUrl('manualentryDate/save_individualdata&id='.$id); ?>" > -->
        <?php if(isset($newarr['id'])) { ?>
        <h4 class="user_name status"><?php echo $u['first_name'].' '.$u['last_name'];?>
        <?php } else {?>
         <h4 class="user_name"><?php echo $u['first_name'].' '.$u['last_name'];?>
        <?php } ?>
        <input type='hidden' id="u_id" name="user_id[]" value="<?php echo $u['user_id'];?>">
        <input type='hidden' id="en_id" name="entry_id[]" value="<?php echo (isset($newarr['id']) ? $newarr['id'] : "") ?>">
        </h4>
        <div class="update_block display-none">
        
            <ul class="list-inline">
                
                <li class="form-group">
                    <span>First: </span>
                    <span class="input-group clockpicker"   data-align="top" data-autoclose="true">
                        <input type='text' id="firstpunch" class="form-control" name="first_punch[]" placeholder="HH:00" value="<?php echo (isset($newarr['first_punch']) ? date( "H:i", strtotime( $newarr['first_punch'] ) ) : "") ?>"  required><?php //echo gmdate("H:i:s", $newarr['first_punch']);?>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </span>
                </li>
                <li class="form-group" >
                    <span>Last: </span>
                    <span class="input-group clockpicker"   data-autoclose="true">
                    <input type='text' id="lastpunch" class="form-control" name="last_punch[]" placeholder="HH:00" value="<?php echo (isset($newarr['last_punch']) ? date( "H:i", strtotime( $newarr['last_punch'] ) ) : "") ?>"   required>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </span>
                </li>
                <li class="form-group">
                    <span>Out: </span>
                    <span class="input-group clockpicker"  data-autoclose="true">
                    <input type='text' id="totalout" class="form-control" name="total_out_time[]" placeholder="HH:00" value="<?php echo (isset($newarr['total_out_time']) ?  date( "H:i", strtotime( $newarr['total_out_time'] ) ) : "") ?>" required> 
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </span>
                </li>
                
                
<!--                <li class="form-group">
                    <span>First: </span>
                    <input type='text' id="firstpunch" class="form-control" name="first_punch[]" placeholder="HH:00" value="<?php echo (isset($newarr['first_punch']) ? date( "H:i", strtotime( $newarr['first_punch'] ) ) : "") ?>"  required><?php //echo gmdate("H:i:s", $newarr['first_punch']);?>
                </li>
                <li class="form-group">
                    <span>Last: </span>
                    <input type='text' id="lastpunch" class="form-control" name="last_punch[]" placeholder="HH:00" value="<?php echo (isset($newarr['last_punch']) ? date( "H:i", strtotime( $newarr['last_punch'] ) ) : "") ?>"   required>
                </li>
                <li class="form-group"> 
                    <span>Out: </span>
                    <input type='text' id="totalout" class="form-control" name="total_out_time[]" placeholder="HH:00" value="<?php echo (isset($newarr['total_out_time']) ?  date( "H:i", strtotime( $newarr['total_out_time'] ) ) : "") ?>" required> 
                </li>-->
                 <li class="form-group"> 
                     <span>&nbsp;</span><span class="input-group">
                   <input type="submit" class="btn btn-sm btn-success button-save" value="Save" /> 
                     </span>
                 </li> 
                 <?php if((Yii::app()->user->role==1 || Yii::app()->user->role==2 || Yii::app()->user->role==6) && isset($newarr['first_punch']) && isset($newarr['last_punch']) && isset($newarr['total_out_time'])) { ?>
                 <li class="form-group"> 
                     <span>&nbsp;</span><span class="input-group">
                   <input type="submit" class="btn btn-sm btn-danger button-delete" value="Delete" /> 
                     </span>
                 </li> 
            <?php } ?>
            </ul>
        
        </div>
        <!-- </form> -->
        <ul class="list-inline view_block">
            <li class="form-group">
                <span>First: </span>
                <span id="firstData"><?php echo (isset($newarr['first_punch']) ? date( "H:i", strtotime( $newarr['first_punch'] ) ): "") ?></span>
            </li>
            <li class="form-group">
                <span>Last: </span> 
                <span id="lastData"><?php echo (isset($newarr['last_punch']) ? date( "H:i", strtotime( $newarr['last_punch'] ) ) : "") ?></span>
            </li>
            <li class="form-group"> 
                <span>Out: </span>
                <span id="outData"><?php echo (isset($newarr['total_out_time']) ? date( "H:i", strtotime( $newarr['total_out_time'] ) ) : "") ?></span>
            </li>
        </ul>
        
    </div>

	
<?php
}
 $i++; } 
 
 if($countinside == 0){
	?>
<h3><i>No Records Found.</i></h3>
<?php 
 }


 
 }
 
 
  ?>
</div>


</div>
</div>
