<script src="<?php echo $this->customAssets('//code.jquery.com/jquery-1.11.0.min.js', '/js/jquery-1.11.0.min.js'); ?>"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="https://cdn.anychart.com/releases/8.8.0/js/anychart-base.min.js"></script>
<script src="https://cdn.anychart.com/releases/8.8.0/js/anychart-data-adapter.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" defer></script>

<div class="loaderdiv margin-top-0 margin-bottom-0 margin-left-auto margin-right-auto width-64">
    <img id="loader" src="<?php echo Yii::app()->request->baseUrl ?>/images/loading.gif" width="50%" />
</div>
<div class="custom-dashboard">

    <div class="index-two-site-sec">

        <div class="row psuedo-none padding-top-10 display-flex flex-flow-wrap justify-content-between align-items-center">
            <div class="padding-x-15 padding-bottom-30">
                <div class="display-flex align-items-center">
                    <label class="text-black font-weight-bold font-20 margin-right-10" for="page-date-filter">Dashboard</label>
                    <select class="font-16 text-black custom-day-span margin-right-10" name="day-span-filter" id="day-span-filter">

                        <option value="30" sselected>30</option>
                        <option value="60">60</option>
                        <option value="90">90</option>
                    </select>
                    <span class="font-16 text-light-black font-italic">Days</span>
                </div>
            </div>
            <div class="padding-x-15 padding-bottom-30">
                <div class="display-flex align-items-center">
                    <label class="text-black font-weight-bold font-18 margin-right-10" for="page-date-filter">Date</label>
                    <div class="custom-form-group display-flex align-items-center">
                        <input class="form-control text-light-black custom-date-range" type="text" id="page-date-filter" name="daterange" value="01-01-2022 - 01-02-2022" readonly>
                        <i class="fa fa-calendar font-18 text-light-black"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row page-logo-img display-lg-flex margin-bottom-40">
            <div class="col-md-6 col-lg-3 display-flex">
                <div class="card mom-request-card dashboard-card-style fade-grey-shadow">
                    <div class="display-flex align-items-center">
                        <img src="themes/assets/dashboard-images/mom-request-ic.png" alt="">
                    </div>
                    <div class="display-flex flex-direction-col align-items-end justify-content-between">
                        <span class="font-52 font-weight-bold text-white">
                            <?= $pending_meeting_minutes ?>
                        </span>
                        <h4 class="card-text">MOM Request
                        </h4>
                        <a class="font-14 text-white" href="<?php echo Yii::app()->createUrl('Reports/mom'); ?>">View More <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 display-flex">
                <div class="card photo-punch-card dashboard-card-style fade-grey-shadow">
                    <div class="display-flex align-items-center">
                        <img src="themes/assets/dashboard-images/photo-punch-request-ic.png" alt="">
                    </div>
                    <div class="display-flex flex-direction-col align-items-end justify-content-between">
                        <span class="font-52 font-weight-bold text-white">
                            <?= $pending_photo_punch ?>
                        </span>
                        <h4 class="card-text">Photo Punch
                            Request</h4>
                        <a class="font-14 text-white" href="<?php echo Yii::app()->createUrl('Reports/photoPunchReport'); ?>">View More <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 display-flex">
                <div class="card wpr-request-card dashboard-card-style fade-grey-shadow">
                    <div class="display-flex align-items-center">
                        <img src="themes/assets/dashboard-images/wpr-request-ic.png" alt="">
                    </div>
                    <div class="display-flex flex-direction-col align-items-end justify-content-between">
                        <span class="font-52 font-weight-bold text-white">
                            <?= $wpr_pending_count ?>
                        </span>
                        <h4 class="card-text">WPR Request
                        </h4>
                        <a class="font-14 text-white" href="<?php echo Yii::app()->createUrl('DailyWorkProgress/new'); ?>">View More <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 display-flex">
                <div class="card project-completed-card dashboard-card-style fade-grey-shadow">
                    <div class="display-flex align-items-center">
                        <img src="themes/assets/dashboard-images/project-completed-ic.png" alt="">
                    </div>
                    <div class="display-flex flex-direction-col align-items-end justify-content-between">
                        <span class="font-52 font-weight-bold text-white">
                            <?= $completed_project ?>
                        </span>
                        <h4 class="card-text">Project
                            Completed</h4>
                        <a class="font-14 text-white" href="<?php echo Yii::app()->createUrl('Projects/index'); ?>">View More <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="row display-lg-flex margin-bottom-60">
            <div class="col-md-6 col-lg-4">
                <div class="min-height-lg-full padding-y-40 rounded-15 fade-grey-shadow display-flex flex-direction-col">
                    <h5 class="font-18 padding-x-40 margin-bottom-10">Project Progress</h5>
                    <div class="project-progress-section padding-x-40 padding-top-24 custom-scroll">


                        <?php
                        if (count($project_data) > 0) {
                            foreach ($project_data as $project) {
                                $model = new Tasks();
                                $percentage_value = $model->projectpercentagecalculation($project->pid);
                                $percentage = round($percentage_value);
                                $style = "width:" .  $percentage . "%";
                        ?>

                                <div class="project-progress-holder">
                                    <h6 class="font-18 font-weight-bold text-light-black"><?php echo $project->name ?></h6>
                                    <div class="project-progress display-flex">
                                        <div class="progress-bar" style="<?php echo  $style ?>">
                                            <div class="progress-counter text-black fade-grey-shadow text-black padding-10 rounded-5">
                                                <?php echo $percentage_value; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="display-flex justify-content-between text-light-black">
                                        <span>0%</span>
                                        <span>100%</span>
                                    </div>
                                </div>
                            <?php }
                        } else {
                            ?>
                            <div class="project-progress-holder">
                                <h6 class="font-18 font-weight-bold text-light-black text-align-center">No data found </h6>
                            </div>
                        <?php } ?>



                    </div>
                </div>
            </div>
            <?php if (in_array('/site/todaySummary', Yii::app()->session['menuauthlist'])) { ?>
                <div class="col-md-6 col-lg-4">
                    <div class="min-height-lg-full display-flex flex-direction-col justify-content-between rounded-15 padding-top-40 fade-grey-shadow">
                        <h5 class="font-18 padding-x-40"> Task Status</h5>
                        <div>
                            <div id="task-status-chart">
                                <div class="total-task">
                                    <span>Total Tasks<br></span>
                                    <?php echo $total_task_count ?>
                                </div>
                            </div>
                        </div>
                        <div class="label-section">
                            <div class="display-flex justify-content-center align-items-center margin-bottom-10">
                                <div class="box-30-new completed-label rounded-5">
                                    <span class="task-status"><?php echo $open_task_count ?><span>

                                </div>
                                <span class="font-14 margin-left-10">Not Started</span>
                            </div>
                            <div class="display-flex justify-content-center align-items-center margin-bottom-10">
                                <div class="box-30-new progress-label rounded-5">
                                    <span class="task-status"><?php echo $completed_task_count ?><span>

                                </div>
                                <span class="font-14 margin-left-10">Completed</span>
                            </div>
                            <div class="display-flex justify-content-center align-items-center margin-bottom-10">
                                <div class="box-30-new not-started-label rounded-5">
                                    <span class="task-status"><?php echo $inprogress_task_count ?><span>

                                </div>
                                <span class="font-14 margin-left-10">In Progress</span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            if (in_array('/site/latesttask', Yii::app()->session['menuauthlist'])) { ?>
                <div class="col-md-6 col-lg-4 margin-bottom-5">
                    <div class="min-height-lg-full rounded-15 padding-top-40 fade-grey-shadow">
                        <h5 class="font-20 padding-x-40 margin-bottom-25">Work Load</h5>
                        <div class="work-load-section padding-x-40 custom-scroll">


                            <?php
                            foreach ($workload_array as $workload) {
                                $total_percentage = 0;
                                if ($workload['all_task'] != 0) {
                                    $total_percentage = round(($workload['total'] / $workload['all_task']) * 100);
                                }

                                $total_width = "width: " . $total_percentage . "%";
                                $completed_percentage = 0;
                                $completed_width = "width: " . $completed_percentage . "%";
                                $inprogress_percentage = 0;
                                $inprogress_width = "width: " . $inprogress_percentage . "%";
                                if ($workload['total'] != 0) {

                                    $completed_percentage = round(($workload['completed'] / $workload['total']) * 100);
                                    $completed_width = "width: " . $completed_percentage . "%";
                                    $inprogress_percentage = round(($workload['inprogress'] / $workload['total']) * 100);
                                    $inprogress_width = "width: " . $inprogress_percentage . "%";
                                }


                            ?>


                                <div class="work-load-holder">
                                    <h6 class="workload-name"><?php echo $workload['user_name'] ?></h6>
                                    <!-- <div class="work-load-progress display-flex rounded-5">
                                  
                                    <div class="progress-bar work-time-bar" style="<?php echo $total_width ?>"><?php echo $total_percentage . "%" ?></div>
                                    <div class="progress-bar work-progress-bar" style="<?php echo $completed_width ?>"><?php echo $completed_percentage ?></div>
                                    <div class="progress-bar work-series-bar" style="<?php echo $inprogress_width ?>"><?php echo $inprogress_percentage ?></div>
                                </div> -->
                                    <div class="display-flex justify-content-around">
                                        <div class="column-gap-15 d-flex">
                                            <div class="progress-barr work-time-bar"></div>
                                            <div class="workload-label"><?php echo $total_percentage . "%" ?></div>
                                        </div>

                                        <div class="column-gap-15 d-flex">
                                            <div class="progress-barr work-progress-bar"></div>
                                            <div class="workload-label"><?php echo $completed_percentage . "%" ?></div>
                                        </div>
                                        <div class="column-gap-15 d-flex">
                                            <div class="progress-barr work-series-bar">
                                            </div>
                                            <div class="workload-label"><?php echo $inprogress_percentage . "%" ?></div>

                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                         </div>
                        <div class="label-section">
                            <div class="display-flex justify-content-center align-items-center">
                                <div class="box-20 work-time-bar rounded-5"></div>
                                <span class="font-14 margin-left-10">Total</span>
                            </div>
                            <div class="display-flex justify-content-center align-items-center">
                                <div class="box-20 work-progress-bar rounded-5"></div>
                                <span class="font-14 margin-left-10">Completed</span>
                            </div>
                            <div class="display-flex justify-content-center align-items-center">
                                <div class="box-20 work-series-bar rounded-5"></div>
                                <span class="font-14 margin-left-10">InProgress</span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            ?>
        </div>



        <!-- Recently Updated -->
        <div class="row margin-bottom-40 overflow-x-hidden">
            <div class="recently-updated-section">
                <h5 class="font-20 margin-0">Recently Updated</h5>
                <div class="display-flex">
                    <span id="prev-update" class="margin-right-20 "><i class="fa fa-chevron-left font-26 text-pink"></i></span>
                    <span id="next-update"><i class="fa fa-chevron-right font-26 text-pink"></i></span>
                </div>
            </div>
            <div class="display-flex update-card-slider">
                <?php
                if (count($recently_updated_task) > 0) {
                    foreach ($recently_updated_task as $recently) {
                        if ($recently['quantity'] != "") {
                            if ($recently['quantity'] == $recently['total_quantity']) {
                                $status_msg = "Completed";
                                $status = "recent-completed-tag";
                            }
                            if ($recently['total_quantity'] == "") {
                                $status_msg = "Pending";
                                $status = "recent-pending-tag";
                            }
                            if ($recently['quantity']  != $recently['total_quantity'] && $recently['total_quantity'] != "") {
                                $status_msg = "In progress";
                                $status = "recent-progress-tag";
                            }
                        } else {
                            $status_msg = "Pending";
                            $status = "recent-pending-tag";
                        }

                ?>

                        <div class="col-card-md-6 col-card-lg-4 col-card-xl-3 col-card-xxl-5 update-card margin-bottom-20">
                            <div class="fade-grey-shadow recently-upadted-card">
                                <div class="display-flex justify-content-between align-items-center margin-bottom-10">
                                    <span></span>
                                    <div class="<?php echo $status ?> display-flex justify-content-center align-items-center">
                                        <?php echo $status_msg ?></div>
                                </div>
                                <h6 class="font-18"><?php echo $recently['title'] . " - " . $recently['name'] ?></h6>
                                <div>
                                    <div class="box-30">
                                        <img src="themes/assets/dashboard-images/recent-profile-1.png" alt="">
                                    </div>
                                    <span class="font-14 font-weight-bold"><?php echo $recently['first_name'] . " " . $recently['last_name'] ?></span>
                                </div>
                                <div class="display-flex">
                                    <div class="box-30 margin-bottom-10 display-flex align-items-start">
                                        <i class="icon-calendar icons font-18"></i>
                                    </div>
                                    <div>
                                        <span class="font-14"><?php echo date('d-M-Y', strtotime($recently['start_date'])) . " to " . date('d-M-Y', strtotime($recently['due_date'])) ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                    }
                } else {
                    ?>
                    <div class="col-card-md-6 col-card-lg-4 col-card-xl-3 col-card-xxl-5 update-card margin-bottom-20">
                        <div class="font-18 display-flex flex-direction-col min-height-full fade-grey-shadow padding-x-20 padding-y-30 rounded-10 text-align-center">
                            No data found
                        </div>
                    </div>
                <?php
                }
                ?>

            </div>
        </div>
        <!-- End of Recently Updated -->

        <!-- project schedule start -->
        <div class="row margin-x-0 margin-bottom-40">
            <div class="rounded-15 overflow-hidden fade-grey-shadow">
                <div class="display-flex flex-flow-wrap justify-content-between align-items-center  padding-y-20">
                    <h5 class="font-18 margin-bottom-0">Project Schedule</h5>
                    <div class="display-flex align-items-center">
                        <!-- <div class="display-flex align-items-center margin-right-30">
                            <label class="text-black font-weight-bold font-18 margin-right-10" for="page-date-filter">Date</label>
                            <div class="custom-form-group display-flex align-items-center">
                                <input class="form-control text-light-black custom-date-range" type="text" id="page-date-filter" name="daterange" value="01-01-2022 - 01-02-2022">
                                <i class="fa fa-calendar font-18 text-light-black"></i>
                            </div>
                        </div> -->
                        <!-- <div class="circle-50 bg-light-grey display-flex justify-content-center align-items-center margin-right-10">
                            <img src="themes/assets/dashboard-images/menu-block-ic.png" alt="">
                        </div>
                        <div class="circle-50 bg-light-grey display-flex justify-content-center align-items-center">
                            <img src="themes/assets/dashboard-images/filter-ic.png" alt="">
                        </div> -->
                    </div>
                </div>
                <div class="table-responsive project-schedule-gantt custom-scroll">
                    <table class="table attnd_table margin-bottom-10" id="fixTable">

                        <thead>
                            <tr>
                                <th colspan="4" class="fixed_th sticky-left">
                                    <div></div>
                                </th>
                                <?php foreach ($date_output as $month) { ?>
                                    <th class="class='month-period text-align-center white-space-nowrap"><?php echo $month ?></th>
                                <?php } ?>
                            </tr>



                        </thead>
                        <tbody>

                            <?php
                            if (count($projects_details) > 0) {
                                foreach ($projects_details as $projects) { ?>
                                    <tr id="user" class="position-relative">
                                        <td colspan="4" class="sticky-left">
                                            <div class="pull-left text-elipsis max-width-275">
                                                <?php echo $projects['project_name']  ?>
                                            </div>
                                        </td>



                                        <?php foreach ($date_output as $date) {

                                            $month_date_f = date('Y-m-01', strtotime($date));
                                            $month_date_e = date('Y-m-t', strtotime($date));

                                            $starting_date = date("Y-m-d", strtotime($projects['s_date']));

                                            $month_date = date('Y-m-d', strtotime($date));
                                            $ending_date = date("Y-m-d", strtotime($projects['e_date']));

                                            $class = 'project-inactive';
                                            $class1 = '';
                                            $value = '';

                                            if (($starting_date >=  $month_date_f) && ($starting_date <= $month_date_e)) {
                                                $class = 'project-active left-curve';
                                                $value = '<span class="project-period font-18 text-white font-weight-bold">' . date("d-M-Y", strtotime($starting_date)) . " to " . date("d-M-Y", strtotime($ending_date)) . '</span>';
                                            }


                                            if (($ending_date >=  $month_date_f) && ($ending_date <= $month_date_e)) {
                                                $class = 'project-active right-curve';
                                            }




                                            if ($month_date >= $starting_date && $month_date <= $ending_date) {
                                                $class1 = ' project-active';
                                            } else {
                                            }

                                        ?>
                                            <td class="project-status">
                                                <div class="<?php echo $class .  $class1 ?>"><?= $value ?></div>
                                            </td>

                                        <?php } ?>

                                    </tr>
                                <?php }
                            } else { ?>
                                <tr>
                                    <td class="shadow-none border-0 text-align-center font-18 font-weight-bold">No data found</td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>




                <!-- project schedule pagination start -->
                <div class="display-flex justify-content-end padding-x-40">
                    <nav class="custom-pagination padding-bottom-30">
                        <div class="pagnt_clnt">

                            <?php
                            $this->widget('CLinkPager', array(
                                'pages' => $pages,
                                'firstPageLabel' => '',
                                'lastPageLabel' => '',
                                'nextPageLabel' => '<i class="fa fa-chevron-right font-18 text-pink"></i>',
                                'prevPageLabel' => '<i class="fa fa-chevron-left font-18 text-pink"></i>',
                                'header' => '',
                                'htmlOptions' => array('class' => 'yiiPager pagination mb-0'),

                                'internalPageCssClass' => 'page-item',
                            ));
                            ?>
                        </div>


                    </nav>
                </div>
                <!-- project schedule end -->
            </div>
        </div>
        <!-- project schedule end -->



        <!-- Current Projects -->
        <div class="row overflow-x-hidden">
            <div class="display-flex justify-content-between align-items-center padding-x-15 margin-bottom-20">
                <h5 class="font-18 margin-bottom-0">Current Projects</h5>
                <div class="display-flex">
                    <span id="prev-project" class="margin-right-20"><i class="fa fa-chevron-left font-26 text-pink"></i></span>
                    <span id="next-project"><i class="fa fa-chevron-right font-26 text-pink"></i></span>
                </div>
            </div>
            <div class="display-flex project-card-slider">
                <!-- card-1 -->
                <?php
                if (count($project_data) > 0) {
                    foreach ($project_data as $datas) {
                        $pending_task = $this->taskStatus($datas->pid, $status = 75);
                        $completed_task = $this->taskStatus($datas->pid, $status = 7);
                        $in_progress_task = $this->taskStatus($datas->pid, $status = 9);
                ?>
                        <div class="col-card-md-6 col-card-xl-4 col-card-xxl-3 project-card margin-bottom-20">
                            <div class="display-flex flex-direction-col min-height-full fade-grey-shadow padding-top-30 rounded-10 overflow-hidden">
                                <h6 class="font-18 padding-x-20"><?php echo $datas->name; ?></h6>
                                <span class="margin-bottom-30 padding-x-20"></span>
                                <div class="display-flex justify-content-between flex-flow-wrap padding-x-20">
                                    <div class="display-flex align-items-center padding-bottom-16">
                                        <div class="completed-project-circle display-flex align-items-center
                                    justify-content-center font-weight-bold text-white font-14">
                                            <?php echo count($completed_task) ?>
                                        </div>
                                        <span class="font-14 margin-left-5">Completed</span>
                                    </div>
                                    <div class="display-flex align-items-center padding-bottom-16">
                                        <div class="progress-project-circle display-flex align-items-center
                                    justify-content-center font-weight-bold text-white font-14">
                                            <?php echo count($in_progress_task); ?>
                                        </div>
                                        <span class="font-14 margin-left-5">In Progress</span>
                                    </div>
                                    <div class="display-flex align-items-center padding-bottom-16">
                                        <div class="pending-project-circle display-flex align-items-center
                                    justify-content-center font-weight-bold text-white font-14">
                                            <?php echo count($pending_task) ?>
                                        </div>
                                        <span class="font-14 margin-left-5">Pending</span>
                                    </div>
                                </div>
                                <div class="current-progress display-flex">
                                    <div class="progress-bar current-completed-project width-65-percentage"></div>
                                    <div class="progress-bar current-progress-project width-20-percentage"></div>
                                    <div class="progress-bar current-pending-project width-15-percentage"></div>
                                </div>
                            </div>
                        </div>


                    <?php }
                } else {
                    ?>
                    <div class="col-card-md-6 col-card-xl-4 col-card-xxl-3 project-card margin-bottom-20">
                        <div class="display-flex flex-direction-col min-height-full fade-grey-shadow padding-y-30 rounded-10 overflow-hidden">
                            <h6 class="font-18 padding-x-20 text-align-center">No data found</h6>
                        </div>
                    </div>
                <?php
                }
                ?>

            </div>
        </div>
        <!-- End of Current Projects -->
    </div>

</div>



<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

<script>
    // recently updated section
    var updateCard = $('.update-card');
    var updateCardWidth = $(updateCard).innerWidth();
    var counter = 0;
    var updateCardCount = $('.update-card').length;

    if ($(window).width() > 1400) {
        var updateEnd = $('.update-card').length - 5;
    } else if ($(window).width() < 1400) {
        var updateEnd = $('.update-card').length - 4;
    } else if ($(window).width() < 1200) {
        var updateEnd = $('.update-card').length - 3;
    } else if ($(window).width() < 992) {
        var updateEnd = $('.update-card').length - 2;
    } else if ($(window).width() < 768) {
        var updateEnd = $('.update-card').length - 1;
    }

    $(window).resize(function() {
        // storyHolder = $('.story-holder');
        $('.update-card-slider').css('transform', 'translate(' + (0) + 'px' + ')');
        counter = 0;
        updateCardWidth = $(updateCard).innerWidth();

        if ($(window).width() > 1400) {
            updateEnd = $('.update-card').length - 5;
        } else if ($(window).width() < 1400) {
            updateEnd = $('.update-card').length - 4;
        } else if ($(window).width() < 1200) {
            updateEnd = $('.update-card').length - 3;
        } else if ($(window).width() < 992) {
            updateEnd = $('.update-card').length - 2;
        } else if ($(window).width() < 768) {
            updateEnd = $('.update-card').length - 1;
        }
    });

    $('#prev-update').on('click', function() {
        if (counter != 0) {
            counter--;
        }
    });
    $('#next-update').on('click', function() {
        if (counter != updateEnd) {
            counter++;
        }
    });

    $('#prev-update, #next-update').on('click', function() {
        console.log(updateEnd);
        // if (counter = 1) {
        //     counter = updateEnd;
        // }
        $('.update-card-slider').css('transform', 'translate(' + (-updateCardWidth * counter) + 'px' + ')');
        $('.update-card-slider').css('transition', 'transform 0.4s ease-in-out');

    });


    if ($(window).width() < 768) {
        if (updateCardCount <= 1) {
            $('#prev-update, #next-update').addClass('display-none')
        }
    } else if ($(window).width() < 992) {
        if (updateCardCount <= 2) {
            $('#prev-update, #next-update').addClass('display-none')
        }
    } else if ($(window).width() < 1200) {
        if (updateCardCount <= 3) {
            $('#prev-update, #next-update').addClass('display-none')
        }
    } else if ($(window).width() < 1400) {
        if (updateCardCount <= 4) {
            $('#prev-update, #next-update').addClass('display-none')
        }
    } else if ($(window).width() >= 1400) {
        if (updateCardCount <= 5) {
            $('#prev-update, #next-update').addClass('display-none')
        }
    }
    // end of recently updated section

    // current project section
    var projectCard = $('.project-card');
    var projectCardWidth = $(projectCard).innerWidth();
    var counter = 0;
    var projectCardCount = $('.project-card').length;

    if ($(window).width() > 1400) {
        var projectEnd = $('.project-card').length - 4;
    } else if ($(window).width() > 1200) {
        var projectEnd = $('.project-card').length - 3;
    } else if ($(window).width() > 768) {
        var projectEnd = $('.project-card').length - 2;
    } else if ($(window).width() > 0) {
        var projectEnd = $('.project-card').length - 1;
    }
    $(window).resize(function() {
        // storyHolder = $('.story-holder');
        $('.project-card-slider').css('transform', 'translate(' + (0) + 'px' + ')');
        counter = 0;
        projectCardWidth = $(projectCard).innerWidth();

        if ($(window).width() > 1400) {
            projectEnd = $('.project-card').length - 4;
        } else if ($(window).width() > 1200) {
            projectEnd = $('.project-card').length - 3;
        } else if ($(window).width() > 768) {
            projectEnd = $('.project-card').length - 2;
        } else if ($(window).width() > 0) {
            projectEnd = $('.project-card').length - 1;
        }
    });

    $('#prev-project').on('click', function() {
        if (counter != 0) {
            counter--;
        }
    });
    $('#next-project').on('click', function() {
        if (counter != projectEnd) {
            counter++;
        }
    });

    $('#prev-project, #next-project').on('click', function() {
        $('.project-card-slider').css('transform', 'translate(' + (-projectCardWidth * counter) + 'px' + ')');
        $('.project-card-slider').css('transition', 'transform 0.4s ease-in-out');

    });

    if ($(window).width() < 768) {
        if (projectCardCount <= 1) {
            $('#prev-project, #next-project').addClass('display-none')
        }
    } else if ($(window).width() < 1200) {
        if (projectCardCount <= 2) {
            $('#prev-project, #next-project').addClass('display-none')
        }
    } else if ($(window).width() < 1400) {
        if (projectCardCount <= 3) {
            $('#prev-project, #next-project').addClass('display-none')
        }
    } else if ($(window).width() >= 1400) {
        if (projectCardCount <= 4) {
            $('#prev-project, #next-project').addClass('display-none')
        }
    }
    // end of current project section
</script>

<!-- Task Status Chart -->
<script>
    var root = am5.Root.new("task-status-chart");

    // Set themes
    root.setThemes([am5themes_Animated.new(root)]);

    // Create chart
    // start and end angle must be set both for chart and series
    var chart = root.container.children.push(
        am5percent.PieChart.new(root, {
            startAngle: 180,
            endAngle: 360,
            layout: root.verticalLayout,
            innerRadius: am5.percent(70),
        })
    );

    // Create series
    // start and end angle must be set both for chart and series
    var series = chart.series.push(
        am5percent.PieSeries.new(root, {
            startAngle: 180,
            endAngle: 360,
            valueField: "value",
            categoryField: "category",
            alignLabels: false,
        })
    );

    series.states.create("hidden", {
        startAngle: 180,
        endAngle: 180,
    });

    // colors of the chart
    series.get("colors").set("colors", [
        am5.color(0x07ad16),
        am5.color(0xfbac1e),
        am5.color(0xea0808)
    ]);

    series.ticks.template.setAll({
        forceHidden: true,
    });









    // Set data
    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data

    var pie_chart_array = '<?php echo $pie_chart_array ?>';

    //alert(pie_chart_array);

    // series.data.setAll([{
    //         value: 70,
    //         category: "Completed"
    //     },
    //     {
    //         value: 20,
    //         category: "In Progress"
    //     },
    //     {
    //         value: 78,
    //         category: "Pending"
    //     },
    // ]);



    var url = '<?php echo Yii::app()->createUrl('site/taskPieChart'); ?>';

    anychart.onDocumentReady(function() {
        anychart.data.loadJsonFile(url, function(data) {

            series.data.setAll(
                data
            );
        });
    });







    series.labels.template.set("forceHidden", true);


    series.appear(1000, 100);
</script>
<!-- End of Task Status Chart -->

<script>
    $(document).ready(function() {
        $('.project-schedule-gantt tr').each(function(index, element) {
            var colorR = Math.floor((Math.random() * 256));
            var colorG = Math.floor((Math.random() * 256));
            var colorB = Math.floor((Math.random() * 256));
            $(this).find('.project-active').css("background-color", "rgb(" + colorR + "," + colorG + "," + colorB + ")");
        });
    });
</script>

<script>
    // $(function() {
    //     $('input[name="daterange"]').daterangepicker({
    //             opens: "left",
    //             locale: {
    //                 format: 'DD/MM/YYYY'
    //             },
    //         },
    //     );
    // });



    $("#day-span-filter").change(function() {

        var days = $("#day-span-filter").val();


        $('.loaderdiv').show();
        $.ajax({
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('site/setDateRange'); ?>',
            data: {
                days: days

            },
            method: "POST",
            success: function(result) {
                $('.loaderdiv').hide();
                window.location.reload();

            }
        })

    });

    $(document).ready(function() {
        var from_date = '<?php echo date('d/m/Y', strtotime(Yii::app()->session['session_start_date'])) ?>';
        var to_date = '<?php echo date('d/m/Y', strtotime(Yii::app()->session['session_end_date'])) ?>';
        var days = '<?php echo Yii::app()->session['days']; ?>';



        if (days != "") {
            $("#day-span-filter").val(days);
            $('#page-date-filter').val(from_date + " - " + to_date);

        }



    });
</script>