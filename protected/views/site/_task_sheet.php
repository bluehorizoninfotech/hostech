<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css" />
<?php

$statusResult = array();
$status = array(1 => 'Open', 'In-progress', 'On Hold Contractor', 'Escalated', 'Review', 'Closed');

foreach ($status as $key => $value) {
    $result = Status::model()->find(array('condition' => 'status_type="task_status" AND caption ="' . $value . '" '));
    if (!empty($result)) {
        $statusResult[$key]['sid'] = $result->sid;
        $statusResult[$key]['caption'] = $result->caption;
    }
}

?>
<div class="clearfix">
    <div class="task-board">
        <h2>Kanban Board</h2>

        <?php

        foreach ($statusResult as $statusRow) {
            if (!empty($id)) {
                if (isset(Yii::app()->user->role) && Yii::app()->user->role == 1) {
                    $taskResult = Yii::app()->db->createCommand("SELECT * FROM pms_tasks LEFT JOIN pms_users 
                    ON pms_tasks.assigned_to=pms_users.userid where pms_tasks.status=" . $statusRow["sid"] . " 
                    AND pms_tasks.trash=1 AND pms_tasks.project_id=" . $id . " AND task_type = 1 ")->queryAll();
                } else {
                    $taskResult = Yii::app()->db->createCommand("SELECT * FROM pms_tasks LEFT JOIN pms_users 
                    ON pms_tasks.assigned_to=pms_users.userid where pms_tasks.status=" . $statusRow["sid"] . " 
                    AND pms_tasks.trash=1 AND pms_tasks.project_id=" . $id . " AND task_type = 1 AND (assigned_to = " . Yii::app()->user->id . " OR pms_tasks.report_to = " . Yii::app()->user->id . "  OR coordinator = " . Yii::app()->user->id . ")")->queryAll();
                }

                // echo '<pre>';
                // print_r();
                // exit;
            }

            $div_id = "";
            $class  = '';
            if ($statusRow["caption"] == 'Open') {
                $div_id = 'open_block';
                $class_status = 'status_open';
            } else if ($statusRow["caption"] == 'In-progress') {
                $div_id = 'progress_block';
                $class_status = 'status_progress';
            } else if ($statusRow["caption"] == 'On Hold Contractor') {
                $div_id = 'onhold_block';
                $class_status = 'status_hold';
            } else if ($statusRow["caption"] == 'Review') {
                $div_id = 'review_block';
                $class_status = 'status_review';
            } else if ($statusRow["caption"] == 'Closed') {
                $div_id = 'closed_block';
                $class_status = 'status_closed';
                if (Yii::app()->user->role == 1) {
                    $class  = '';
                } else {
                    $class  = 'hide_block';
                }
            } else if ($statusRow["caption"] == 'Escalated') {
                $div_id = 'escalated_block';
                $class_status = 'status_escalated';
            }
        ?>
            <div class="status-card <?php echo $class; ?>" id="<?php echo $div_id; ?>">
                <div class="card-header clearfix">
                    <div class="card-header-text pull-left"><span class=""><?php echo $statusRow["caption"]; ?></span></div>
                    <?php if ($statusRow["caption"] == 'Open') { ?>


                    <?php } ?>
                    <span class="dot <?= $class_status ?>"></span>
                </div>
                <ul class="sortable ui-sortable" id="sort<?php echo $statusRow["sid"]; ?>" data-status-id="<?php echo $statusRow["sid"]; ?>">
                    <?php
                    if (!empty($taskResult)) {
                        foreach ($taskResult as $taskRow) {
                            if ($taskRow['priority'] == 10) {
                                $leftbor = 'style="border-left:5px solid #FF6D00"';
                            } else if ($taskRow['priority'] == 11) {
                                $leftbor = 'style="border-left:5px solid #D50000"';
                            } else if ($taskRow['priority'] == 12) {
                                $leftbor = 'style="border-left:5px solid  #FFAB00"';
                            }
                    ?>

                            <li class="text-row ui-sortable-handle" <?php echo $leftbor; ?> data-task-id="<?php echo $taskRow["tskid"]; ?>" style="padding-top: 0px;">
                                <div class="clearfix showdesc">
                                    <h4 class="pull-left" style="border-bottom: none;" title="<?= $taskRow['first_name'] . ' ' . $taskRow['last_name'] ?>"><?php echo $taskRow['title']; ?></h4>
                                    <?php if ($taskRow['description'] != "") { ?>
                                        <p class="pull-right"><span class="icon-options"></span></p>
                                    <?php } ?>
                                </div>
                                <p class="desctext" style="display:none;"><?php echo $taskRow['description']; ?></p>
                                <?php if ($taskRow['due_date'] != "") { ?>
                                    <div class="clearfix">
                                        <div class="pull-left">

                                            <span class="icon-eye detail_block pull-left " id="<?php echo $taskRow["tskid"]; ?>"></span>
                                            <?php
                                            if (in_array('/dailyWorkProgress/create', Yii::app()->session['menuauthlist']) || Yii::app()->user->role == 1) { ?>
                                                <span class="fa fa-clock-o  addtask pull-right pos_plus" id="<?= $taskRow["tskid"]; ?>" atr_project="<?= $taskRow['project_id'] ?>" title="Add Status"></span>
                                            <?php }

                                            ?>


                                        </div>

                                        <div class="pull-right duedate">Due: <?php echo date('d-M-y', strtotime($taskRow['due_date'])); ?></div>
                                    </div>
                                <?php } ?>




                            </li>
                    <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        <?php
        }
        ?>
    </div>
    <div id="statusform" class="slide_box panel panel-primary" style="display:none;"></div>
    <div id="task_details" class="slide_box" style="display:none;">

    </div>
</div>
<script>
    function closeform(formname) {
        $('#statusform').slideUp();
        location.reload();
        _task_sheet

    }

    $(document).on('click', '.closeform', function() {
        $('#task_details').slideUp();
    });
    $(document).on('click', '.closebtn', function() {
        $('#statusform').slideUp();
    });

    var source = null;
    $(function() {


        $('ul[id^="sort"]').sortable({

            connectWith: ".sortable",
            /*revert:200,*/
            start: function(e, ui) {
                source = $(ui.item).parent(".sortable").attr("id").trim();
            },
            receive: function(e, ui) {



                var target = $(ui.item).parent(".sortable").attr("id").trim();
                var status_id = $(ui.item).parent(".sortable").data("status-id");
                var task_id = $(ui.item).data("task-id");
                /*if((source == 'sort6' && (target== 'sort5' || target == 'sort7' || target == 'sort76')) || (source == 'sort77' && target=='sort7') || (source == 'sort7' && target=='sort77') || (source == 'sort76' && target=='sort5') || (source == 'sort5' && target=='sort76') || (source == 'sort76' && target=='sort7') || (source == 'sort7' && target=='sort76') || (source == 'sort76' && target=='sort6') || (source == 'sort5' && target=='sort6') || (source == 'sort7' && target=='sort6')){
                    $(ui.sender).sortable('cancel');
                    return false;
                }*/
                statuschangedescription(task_id, status_id);
                var id = $.urlParam('id');

                //    if(id !== '' && id != null){
                //     $("#data_new").load('<?php echo  Yii::app()->createAbsoluteUrl('gantt/chart/chartreload/id/'); ?>/'+id).delay(5000);               
                //    }else{
                //     $("#data_new").load('<?php echo  Yii::app()->createAbsoluteUrl('gantt/chart/chartreload'); ?>').delay(5000);              
                //    }
                //   console.log($.urlParam('id'));   
                //    $("#data_new").load('<?php echo  Yii::app()->createAbsoluteUrl('gantt/chart/chartreload'); ?>').delay(5000);              
            },
            /*update:function (e, ui) {
            var target = $(ui.item).parent(".sortable").attr("id").trim();
             if((source == 'sort6' && (target== 'sort5' || target == 'sort7' || target == 'sort76')) || (source == 'sort77' && target=='sort7') || (source == 'sort7' && target=='sort77') || (source == 'sort76' && target=='sort5') || (source == 'sort5' && target=='sort76') || (source == 'sort76' && target=='sort7') || (source == 'sort7' && target=='sort76') || (source == 'sort76' && target=='sort6') || (source == 'sort5' && target=='sort6') || (source == 'sort7' && target=='sort6')){
                 $(ui.sender).sortable('cancel');
                 return false;
                  }
             }*/

        }).disableSelection();
    });

    function reloadcalender() {
        $('#loader').hide();
        // var startdate = $('#start_date').val();
        // var enddate   = $('#end_date').val();
        var pid = $('#project_id').val();


        $.ajax({
            method: "get",
            url: "<?php echo $this->createUrl('projects/calandersheet') ?>",
            data: {
                pid: pid
            },
        }).done(function(msg) {
            $("#calendertable").html(msg);
        });
        kanbanchange(pid);

    }

    function kanbanchange(pid) {
        $.ajax({
            method: "get",
            url: "<?php echo $this->createUrl('site/tasksheet') ?>",
            data: {
                pid: pid
            },
        }).done(function(msg) {
            $("#task_data").html(msg);
        });
    }

    function statuschangedescription(task_id, status_id) {
        $('.loading').show();
        $('#task_details').slideUp();
        $.ajax({
            type: "GET",
            url: "<?php echo $this->createUrl('DailyWorkProgress/statuschange') ?>",
            data: {
                layout: 1,
                status_id: status_id,
                task_id: task_id
            },
            success: function(response) {
                $('.loading').hide();
                // if(status_id == 72 || status_id == 5){
                $('#statusform').html(response).animate({
                    height: 'show'
                }, 290);
                $("input:first").focus();
                // }
            }

        })
    }
    $.urlParam = function(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        return decodeURI(results[1]) || 0;
    }
    $(document).ready(function() {


        $(".showdesc").click(function() {
            $(this).parents().siblings().find('.desctext').hide();
            $(this).closest('.text-row').find('.desctext').toggle();
        });
        //        $(".detail_block").click(function(){
        //            $("#task_details").slideDown();
        //        });
        $(".show_hide").click(function(ev) {
            $(this).parents().find(".commentslist").toggle();
            $(this).html(($('.show_hide').text() == 'Show') ? 'Hide' : 'Show');
        })


        $(".addtask").click(function(e) {
            var id = this.id;
            var project_id = $(this).attr("atr_project");
            statuschangedescription(id, '');

        });

        function statusdatapopup(id, project_id) {
            $('.loading').show();
            $('#task_details').slideUp();

            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('TimeEntry/statusentry') ?>",
                data: {
                    layout: 1,
                    id: id,
                    type: 1,
                    project_id: project_id
                },
                success: function(response) {

                    $('.loading').hide();

                    $('#statusform').html(response).animate({
                        height: 'show'
                    }, 290);
                    $("input:first").focus();
                },
            })
        }

    });

    $(document).on('click', '.show_hide', function() {
        $(this).parents().find(".commentslist").toggle();
        $(this).html(($('.show_hide').text() == 'Show') ? 'Hide' : 'Show');
    })



    $(".detail_block").click(function() {
        $('#statusform').slideUp();
        var id = $(this).attr('id');
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('tasks/gettaskdetails'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                task_id: id
            },
            success: function(response) {
                $("#task_details").html(response.html);
                $("#task_details").slideDown();
            }
        });
    });
    jQuery.browser = {};
    (function() {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();
</script>

<style>
    #content {
        padding-left: 0px;
        padding-right: 0px;
    }

    .task-board {
        display: inline-block;
        white-space: nowrap;
        overflow-x: auto;
        min-height: 300px;
    }

    .status-card {
        width: 240px;
        margin-right: 3px;
        background: #e2e4e6;
        border-radius: 3px;
        display: inline-block;
        vertical-align: top;
        font-size: 0.9em;
    }

    .pos_plus {
        margin-left: 20px !important;
        margin-top: 7px !important;
        ;
    }

    .status-card:last-child {
        margin-right: 0px;
    }

    .card-header {
        width: 100%;
        padding: 5px 5px;
        box-sizing: border-box;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        border-bottom: 1px solid #c2c2c2;
        display: block;
        font-weight: bold;
        background-color: #dedede;
        /* margin-bottom: 10px; */
    }

    .card-header-text {
        display: block;
        font-size: 16px;
        color: #000;
        font-weight: 500;
    }

    ul.sortable {
        padding-bottom: 10px;
        min-height: 300px;
        max-height: 300px;
        overflow-y: auto;
    }

    ul.sortable li:last-child {
        margin-bottom: 0px;
    }

    ul {
        list-style: none;
        margin: 0;
        padding: 0px;
    }

    .text-row {
        padding: 10px;
        color: #000;
        margin: 3px 5px 8px 5px;
        background: #fff;
        box-sizing: border-box;
        border-radius: 3px;
        border-bottom: 1px solid #ccc;
        cursor: pointer;
        font-size: 13px;
        white-space: normal;
        line-height: 20px;
    }

    .ui-sortable-placeholder {
        visibility: inherit !important;
        background: transparent;
        /*   border: #666 2px dashed;*/
    }

    .text-row h4 {
        margin: 0px;
        font-size: 15px;
        padding-bottom: 5px;
        color: #000;
        border-bottom: 1px solid #ccc;
        white-space: initial;
    }

    .text-row p {
        font-size: 13px;
        margin-top: 2px;
        margin-bottom: 0px;
    }

    .detail_block.icon-eye:before {
        position: absolute;
        margin-top: 4px;
    }

    table.viewtable tr td:last-child {
        color: #000;
    }

    table.viewtable tr td:first-child {
        width: 35%;
    }

    td {
        padding: 6px;
        border-bottom: 1px solid #ececec;
        font-size: 13px;
    }

    .commentslist li {
        margin: 0px -15px;
        background: #fafafa;
        padding: 6px 15px;
        border-bottom: 3px solid #fff;
    }

    .showdesc {
        border-bottom: 1px solid #ccc;
        white-space: nowrap;
    }

    .duedate {
        font-size: 10px;
        color: #000;
        padding-top: 2px;
    }

    .hide_block {
        display: none;
    }

    .show_hide {
        margin-top: 2px;
    }

    .panel-body {
        position: relative;
    }

    .viewtask {
        position: absolute;
        top: 15px;
        right: 21px;
    }

    .comments_sec {
        border-bottom: none;
    }

    .apply_gap {
        margin-top: 10px;
        margin-bottom: 5px;
    }

    /*.commentslist{background-color: #fafafa;}*/
    .comments_footer {
        font-style: oblique;
        color: #555;
        font-size: 12px;
    }

    .addtask {
        margin-top: 4px;
        cursor: pointer;
        color: #555;
        margin-right: 5px;
        font-size: 14px;
    }

    .addtask:hover {
        color: #000;
    }

    .innerpage {
        padding-top: 50px;
    }

    .slide_box {
        position: fixed;
        right: 30px;
        z-index: 12;
        width: 500px;
        bottom: 10px;
    }

    @media(min-width: 1300px) {
        .status-card {
            width: 190px;
        }

        .text-row h4 {
            font-size: 14px;
        }
    }

    @media(min-width: 1440px) {
        .status-card {
            width: 210px;
        }

        #content {
            padding: 20px;
        }

        .status-card {
            margin-right: 5px;
        }
    }

    .table-responsive {

        overflow-x: inherit;
    }
</style>