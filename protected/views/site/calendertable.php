<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js" type="text/javascript"></script>
<div class="calender-table-sec">
<div class="col-md-12 table_hold" >

    <form name="emp_att_grid" id="emp_att_grid">
        <div id="parent" >
            <table class="table attnd_table table-bordered" id="fixTable">
                <?php
                $date1 = new DateTime($start_date);
                $date2 = new DateTime($end_date);
                $diff = $date2->diff($date1)->format("%a");
                $totdays = $diff + 1;
                ?>
                <thead>        
                    <tr>                      
                        <th rowspan="2" colspan="4" class="fixed_th">                                                      
                        </th>
                        <?php
                        for ($i = 0; $i < $totdays; $i++) {
                            $week_date = $this->add_date($start_date, $i);
                            $week_datenext = $this->add_date($start_date, $i + 1);
                            $week_day = explode('/', $week_date);
                            $dat_next = explode('/', $week_datenext);
                            $fromday = $this->add_date($start_date, 0);
                            if ($fromday == $week_date) {
                                $flag = 1;
                            }
                            $countmonth = 0;
                            if ($week_day[1] != $dat_next[1]) {

                                $endmonthday = $week_day[0];
                                $startmothday = explode('-', $start_date);
                                $monthdaysdiff = ($endmonthday - $dat_next[0]) + 1;
                                if ($flag == 1) {
                                    $monthdaysdiff = ($endmonthday - $startmothday[2]) + 1;
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                    $countmonth++;
                                    $flag = 0;
                                } else {
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                }
                            }
                            $week_dates[] = $week_date;
                        }
                        $endmonth = explode('-', $end_date);

                        $startdatelast = $endmonth[0] . '-' . $endmonth[1] . '-01';
                        $monthdayarr[$endmonth[1]] = $this->getcountBetween2Dates($startdatelast, $end_date);
                        ?>
                        <?php
                        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
                        foreach ($monthdayarr as $month => $diffdays) {
                            if (count($monthdayarr) == 1) {
                                $diffdays = $totdays;
                            }

                            echo "<th colspan='" . $diffdays . "' class='month-period'>" . $monthsarr[$month] . "</th>";
                        }
                        ?>
                        <!-- <th colspan="1" class="days-period">Total Summary(<span class="monthtot"><?= $diffdays; ?></span>) </th> -->

                    </tr>
                    <tr>

                        <?php
                        $days = $this->getDatesBetween2Dates($start_date, $end_date);
                        $global_assign = '';
                        foreach ($days as $key => $value) {

                            $strtime = strtotime($value);
                            $prev_date = date('Y-m-d', strtotime('-2 day'));

                            $global_assign .= '<th class="global_row" id="i_' . $strtime . '"></th>' . "\n";
                            echo '<th>' . substr(date('D', strtotime($value)), 0, 2) . '<br>' . date('d', strtotime($value)) . '</th>';
                        }
                        ?>
 

                    </tr>

                    <tr>
                        <th class="fixed_th">

                       
                        <th class="fixed_th hdrborder" colspan="3">
                            Task Name
                        </th>
                        
                        <!-- <th class="fixed_th"> </th> -->
                        <?php
                        echo $global_assign;
                        ?>

                    </tr>


                </thead>
                <tbody>
                    <?php
                    $count = 0;
                    
                    foreach ($tasks as $task) {
                        // echo '<pre>';print_r($task);exit;
                        if($task['status'] == 1){
                        $status = 'in_progress';
                        }elseif($task['status'] == 3){
                            $status = 'delay';
                        }
                        $count++;
                        ?>
                        <tr id="user ?>">
                            <th  width="20"></th>
                           
                            <th colspan="3" width="300" class="bg_grey">
                                <div class="min-width-182 pull-left">
                                <?= $task['title'] ?>
                                </div> 
                                <span class="useridspan">#<?php ?></span></th>
                            </div>
                            </th>
                            <?php
                            $task_start =  strtotime($task['start_date']);
                            $task_end = strtotime($task['due_date']);
                           
                            
                            for ($p = 0; $p < $totdays; $p++) {
                                $var = $week_dates[$p];
                                $todd = date('d/m/Y');
                                $datecur = str_replace('/', '-', $var);
                                $datecur_ = str_replace('/', '-', $todd);
                                $id = strtotime($datecur);
                                $id_ = strtotime($datecur_);
                              
                                ?>
                               
                                <td width="20" data-toggle="modal"  id="c_<?= $id ?>" 
                                data-backdrop="static" data-target="#entry" 
                                class="coldate <?= ($id >= $task_start) && ($id <= $task_end)?$status:'' ?>
                                <?=  (($id >= $task_start) && ($id <= $task_end)) &&  ($id > $id_)?'balance':''?>"
                                data-value="" 
                                data-coldate="">
                                </td>
                            <?php } ?>
                            <!-- <td id="ot_user_" 
                            style='font-weight:bold;font-size:12px;'></td> -->
                           
                        </tr>
                        <?php
                            if(!empty($task['children'])){
                               $tasks = $task['children']; 
                               foreach($tasks as $task){
                                   ?>
                                <tr id="user ?>">
                                    <th  width="20"></th>                           
                                    <th colspan="3" width="300" class="">
                                    <div class="min-width-100 pull-left padding-left-15">
                                    <?= $task['title'] ?>
                                    </div> 
                                    <span class="useridspan">#<?php ?></span></th>                            
                                    </th>
                                <?php
                                    $task_start =  strtotime($task['start_date']);
                                    $task_end = strtotime($task['due_date']);                                                            
                                    for ($p = 0; $p < $totdays; $p++) {
                                        $var = $week_dates[$p];
                                        $todd = date('d/m/Y');
                                        $datecur = str_replace('/', '-', $var);
                                        $datecur_ = str_replace('/', '-', $todd);
                                        $id = strtotime($datecur);
                                        $id_ = strtotime($datecur_);                              
                                        ?>                                
                                        <td width="20" data-toggle="modal"  id="c_<?= $id ?>" 
                                        data-backdrop="static" data-target="#entry" 
                                        class="coldate  <?= ($id >= $task_start) && ($id <= $task_end)?$status:'' ?>
                                <?=  (($id >= $task_start) && ($id <= $task_end)) &&  ($id > $id_)?'balance':''?>"
                                        data-value="" 
                                        data-coldate="">
                                        </td>
                                    <?php } ?>
                                    <!-- <td id="ot_user_" style='font-weight:bold;font-size:12px;'></td> -->
                           
                                 </tr>
                                   <?php
                               }
                                //   echo '<pre>';print_r($task);exit;
                                ?>
                        <?php }?>                       
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </form>
</div>
                            </div>




<script type="text/javascript">
    var $ = jQuery;
    $(document).ready(function () {
        // reload();
         $("#fixTable").tableHeadFixer({'left': 2, 'foot': false, 'head': true});
    });

  

    
    $(document).ready(function () {

       



    });



</script>


