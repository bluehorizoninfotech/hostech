<!-- <script src="https://www.jqueryscript.net/demo/jQuery-Plugin-For-Fixed-Table-Header-Footer-Columns-TableHeadFixer/assets/jquery-2.1.3.js" ></script> -->
<script src="<?php echo $this->customAssets('//code.jquery.com/jquery-1.11.0.min.js','/js/jquery-1.11.0.min.js');?>"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
$settings = GeneralSettings::model()->findByPk(1);
?>

<!-- <ul class="cb-slideshow">
            <li><span></span></li>
            <li><span></span></li>
        </ul> -->
<div class="index-two-site-sec">
<div class="pull-left">
    <h1 class="text-align-left margin-left-20"></i></h1>
</div>


<!-- <br clear="all" /> -->


<div id="dashboard" class="display-none">
    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/timesheet.png', '', array('class' => 'dasimage')) . '<span class="dblink">Time sheets</span>
  </div>', array('/timeEntry/index')); ?>



    <?php if (Yii::app()->user->role < 3 || Yii::app()->user->role == 4) { ?>

        <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/task.png', '', array('class' => 'dasimage')) . '<span class="dblink">Tasks</span>
  </div>', array('/tasks/index2')); ?>

        <?php
        if (Yii::app()->user->role <= 2) {
        ?>
            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/users.png', '', array('class' => 'dasimage')) . '<span class="dblink">Users</span>
  </div>', array('/users/index')); ?>





    <?php }
    }

    ?>



    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/logout.png', '', array('class' => 'dasimage')) . '<span class="dblink">Logout</span>
  </div>', array('site/logout')); ?>
    <br clear="all">
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page-logo-img">
            <?php
            if (isset($projects['img_path'])) {
                if ($projects['img_path'] != '') {
                    $path = realpath(Yii::app()->basePath . '/../uploads/project/thumbnail/' . $projects['img_path']);
                    if (file_exists($path)) {
            ?>
                        <img height="45" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/thumbnail/" . $projects['img_path']; ?>" alt="..." class="max-width-100-percentage">
                    <?php } else { ?>
                        <img height="45" src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/no_img.png" alt="..." class="max-width-100-percentage">
                    <?php }
                } else { ?>
                    <img height="45" src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/no_img.png" alt="..." class="max-width-100-percentage">
                <?php } ?>
            <?php } else {
            ?>
                <img height="45" src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/no_img.png" alt="..." class="max-width-100-percentage">
            <?php
            } ?>
            <h2 class="margin-bottom-0"><?= isset($projects['name']) ? $projects['name'] : 'No name' ?>
            </h2>

            <?php

            if (isset($client_projects_list) && !empty($client_projects_list)) {
                $client_projects_count = count($client_projects_list, true);
            ?>
                <select class="client_projects change_project width-18"
   ">
                    <option></option>
                    <?php

                    foreach ($client_projects_list as $list) {
                        echo '<option value="' . $list['pid'] . '">' . $list['name'] . '</option>';
                    }
                    ?>
                </select>
                <!-- <a class="change_project"><i class="fa fa-arrow-right"></i></a> -->
            <?php
                // if (count($client_projects_count) != 1) {
                //     echo CHtml::link('<i class="fa fa-arrow-right"></i>', array(), array('class' => 'change_project'));
                // }
            }
            ?>

        </div>
    </div>
    <?php if (in_array('/site/todaySummary', Yii::app()->session['menuauthlist'])) { ?>
        <div class="col-md-9">
            <div class="email_nottification">
                <!-- accordion starts -->
                <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'milestones')" id="defaultOpen">Milestone</button>
                    <button class="tablinks" onclick="openCity(event, 'tasks')">Task</button>
                    <div class="tab_inner_div">
                        <?php 
                        if($tasks_to_start>0)
                        {?>
                     <span class="pull-right"><b>Tasks yet to start:</b> <?= $tasks_to_start ?> </span>
                        <?php
                        }
                        ?>
                      
                        <span class="pull-right"><b>Total Tasks:</b> <?= $task_count ?></span>
                        <span class="pull-right"><b>Total Unassigned Tasks:</b> <?= $unassigned_task_count ?></span>
                    </div>
                </div>
                <div id="milestones" class="tabcontent">
                    <?php $this->widget('Milestones'); ?>
                </div>
                <div id="tasks" class="tabcontent">
                    <?php $this->widget('LatestTask'); ?>
                </div>


                <!-- accordion ends -->
                <!-- <div class="week_sum_heading"> TASK</div> -->
                <?php // $this->widget('EmailNotification'); 
                ?>


            </div>
        </div>
    <?php }
    if (in_array('/site/latesttask', Yii::app()->session['menuauthlist'])) { ?>
        <div class="col-md-3">
            <div class="today_summary  border-none">
                <div class="week_sum_heading grey-border-bottom"> Last 7 days work list</div>
                <!--        <div class="portlet" id="yw0">
            <div class="portlet-content">-->
                <?php $this->widget('TodaySummary'); ?>

            </div>
        </div>
        
    <?php }
    if (in_array('/site/todayworkers', Yii::app()->session['menuauthlist'])) { ?>
        <div class=" col-md-6">
            <div class="today_workers">
                <div class="week_sum_heading"> WORKERS ON
                    <?php echo date("d-M-y"); ?>
                </div>
                <?php $this->widget('TodayWorkers'); ?>

            </div>
        </div>
    <?php }
    if (in_array('/site/escalatedtasks', Yii::app()->session['menuauthlist'])) { ?>
        <div class="col-md-6" id="escalated_div">
            <div class="escalated_tasks ">
                <div class="week_sum_heading"> ESCALATED TASKS</div>
                <?php $this->widget('DisplayEscalated'); ?>

            </div>
        </div>

    <?php } ?>
</div>
<?php
if (Yii::app()->user->role < 7 and 0) {
?>
    <iframe src="https://www.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=bluehorizoninfotech.com_o64ui5j1ti02jqpbae13p2ct80%40group.calendar.google.com&amp;color=%23875509&amp;ctz=Asia%2FCalcutta" class="border-width-0" width="500" height="400" frameborder="0" scrolling="no"></iframe>

    <?php
    if (0) {
    ?>
        <br /><br />
        <div class="text-align-center very-light-gray-border">
            <iframe width='95%' height='400' frameborder='0' src='https://docs.google.com/a/bluehorizoninfotech.com/spreadsheet/pub?key=0AiPYyXg6QgmUdEhqaFZpVVdQbElxc3RrZ2s4THQ1a2c&output=html' class="margin-10 pull-lefts"></iframe>
            <br clear="all" />
        </div>

        <br /><br />
        <br clear="all" />
        <div class="light-gray-border margin-10 text-align-center padding-10 margin-top-0 margin-bottom-0 margin-right-auto margin-left-auto width-1000"><iframe class="margin-top-0 margin-bottom-0 margin-left-auto margin-right-auto border-width-0"  src="https://docs.google.com/a/bluehorizoninfotech.com/document/d/1IsoVH_DU3Y5o7odNRLidHYZADBrhHU4u0rO3oqzKxcw/pub?embedded=true" width="1000" height="400" frameborder="1" scrolling="auto"></iframe></div>
    <?php
    }
    ?>
<?php
}
?>

<?php
/*if(Yii::app()->user->role==1)
{
?>
<iframe width='45%' height='400' frameborder='0'
    src='https://docs.google.com/a/bluehorizoninfotech.com/spreadsheet/pub?key=0AiPYyXg6QgmUdGlGR3g0dnlQaXBuUWRuRXhHdEYtYVE&output=html'></iframe>
<?php
}
else
{

?>
<iframe width='45%' height='400' frameborder='0'
    src='https://docs.google.com/a/bluehorizoninfotech.com/spreadsheet/pub?key=0AiPYyXg6QgmUdEdWUDBhTmZtZU1PY1F1ZUlMWlNPWGc&single=true&gid=1&output=html&widget=true'></iframe>
<?php
}*/
?>
<!-- <div class="row">
    <?php
    if ($settings->experience_year != "" && $settings->experience_title != "") {
    ?>
    <div class="col-sm-4 col-md-3">
        <div class="counter_section">
            <span class="count"><?php echo $settings->experience_year; ?></span>
            <span class="head"><?php echo $settings->experience_title; ?></span>
        </div>
    </div>
    <?php } ?>
    <?php
    if ($settings->project_count != "" && $settings->project_title != "") {
    ?>
    <div class="col-sm-4 col-md-3">
        <div class="counter_section">
            <span class="count"><?php echo $settings->project_count; ?></span>
            <span class="head"><?php echo $settings->project_title; ?></span>
        </div>
    </div>
    <?php } ?>
    <?php
    if ($settings->customers_count != "" && $settings->customers_title != "") {
    ?>
    <div class="col-sm-4 col-md-3">
        <div class="counter_section">
            <span class="count"><?php echo $settings->customers_count; ?></span>
            <span class="head"><?php echo $settings->customers_title; ?></span>
        </div>
    </div>
    <?php } ?>
    <?php
    if ($settings->engineers_count != "" && $settings->engineers_title != "") {
    ?>
    <div class="col-sm-4 col-md-3">
        <div class="counter_section">
            <span class="count"><?php echo $settings->engineers_count; ?></span>
            <span class="head"><?php echo $settings->engineers_title; ?></span>
        </div>
    </div>
    <?php } ?>
    <?php
    if ($settings->ongoing_project_count != "" && $settings->ongoing_project_title != "") {
    ?>
    <div class="col-sm-4 col-md-3">
        <div class="counter_section">
            <span class="count"><?php echo $settings->ongoing_project_count; ?></span>
            <span class="head"><?php echo $settings->ongoing_project_title; ?></span>
        </div>
    </div>
    <?php } ?>
</div> -->



<div class="block-elements">
    <?php
    if($settings->dashboard_status==1)
    {
    if ($settings->experience_year != "" && $settings->experience_title != "") {
    ?>
        <div class="single-block">
            <div class="counter_section text-center">
                <div class="count left-section"><?php echo $settings->experience_year; ?></div>
                <div class="head right-section"><?php echo $settings->experience_title; ?></div>
            </div>
        </div>
    <?php } ?>
    <?php
    if ($settings->project_count != "" && $settings->project_title != "") {
    ?>
        <div class="single-block">
            <div class="counter_section text-center">
                <div class="count left-section margin-top-15"><?php //echo $settings->project_count;   
                                                                            ?>
                    <?php if ($settings->project_count > 1000000) {
                        echo round(($settings->project_count / 1000000), 2) . ' <span class="million-text"> million</span>';
                    } else echo  $settings->project_count;  ?>
                </div>
                <div class="head right-section"><?php echo $settings->project_title; ?></div>
            </div>
        </div>
    <?php } ?>
    <?php
    if ($settings->customers_count != "" && $settings->customers_title != "") {
    ?>
        <div class="single-block">
            <div class="counter_section text-center">
                <div class="count left-section margin-top-15"><?php echo $settings->customers_count; ?></div>
                <div class="head right-section"><?php echo $settings->customers_title; ?></div>
            </div>
        </div>
    <?php } ?>
    <?php
    if ($settings->engineers_count != "" && $settings->engineers_title != "") {
    ?>
        <div class="single-block">
            <div class="counter_section text-center">
                <div class="count left-section"><?php echo $settings->engineers_count; ?></div>
                <div class="head right-section"><?php echo $settings->engineers_title; ?></div>
            </div>
        </div>
    <?php } ?>
    <?php
    if ($settings->ongoing_project_count != "" && $settings->ongoing_project_title != "") {
    ?>
        <div class="single-block">
            <div class="counter_section text-center">
                <div class="count left-section"><?php echo $settings->ongoing_project_count; ?></div>
                <div class="head right-section"><?php echo $settings->ongoing_project_title; ?></div>
            </div>
        </div>
    <?php } } ?>
</div>

    </div>

<script>
    document.getElementById("defaultOpen").click();

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<script>
    $(document).ready(function() {
        //$("#fixTable").tableHeadFixer({'head' : true});
        // var content_height=$(".content_sec") .height();
        // $(".blur_bg").css('height',content_height+100)    
        let i = 1;
        var url;
        $.ajax({
            type: 'POST',
            async: false,
            url: "<?php echo Yii::app()->createUrl('mailSettings/getImages'); ?>",
            data: {},
            success: function(data) {
                url = JSON.parse(data);
            }
        });
        change = () => {

            // $('.page-content').fadeTo(800, 0.8, function()
            // {
            // $(this).css("background-image", "url(" + url[i] + ")");
            // i = (i + 1) % url.length;
            // }).fadeTo(1000, 1);  

            $(".page-content-image").css("background-image", "url(" + url[i] + ")")
            i = (i + 1) % url.length;
        }
        $(".page-content-image").css("background-image", "url(" + url[0] + ")")
        setInterval(change, 4000);
    });
    $('.change_project').change(function() {
        var project_id = $('.client_projects').val();
        $.ajax({
            method: "POST",
            url: '<?php echo Yii::app()->createAbsoluteUrl('/site/changeproject') ?>',
            data: {
                project_id: project_id
            },
            success: function(data) {
                console.log(data);
                if (data === '1') {
                    location.reload();
                }
            }
        });
    })
</script>
