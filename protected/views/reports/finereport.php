<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<div class="fine-report-sec">
<h1>Punching Report</h1>

<div>

    <?php echo CHtml::beginForm($this->createUrl('reports/finereport'), 'get'); ?>

    <div class="search">



        <?php

        foreach (Yii::app()->user->getFlashes() as $key => $message) {

            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";

        }

        ?>

        <div class="flash-error" style="display: none;"></div>



         <div>

            <label>Start Date</label>

            <?php echo CHtml::textField('sdate', (isset($date_from) ? date('d-m-Y', strtotime($date_from)) : ''), array("id" => "sdate", 'class' => 'width-100', 'autocomplete' => 'off', 'readonly' => false)); ?>

        </div>
        <div>

            <label>End Date</label>

            <?php echo CHtml::textField('edate', (isset($datetill) ? date('d-m-Y', strtotime($datetill)) : ''), array("id" => "edate", 'class' => 'width-100', 'autocomplete' => 'off', 'readonly' => false)); ?>

        </div>
        <?php $this->widget('application.extensions.calendar.SCalendar', array(  'inputField' => 'sdate',  'button' => 'sdate',  'ifFormat' => '%d-%m-%Y',  ));?>

        <?php  $this->widget('application.extensions.calendar.SCalendar', array('inputField' => 'edate','button' => 'edate',
               'ifFormat' => '%d-%m-%Y', ));  ?>

      

        <div>
        <?php

           
            $acessusersql = "SELECT label,pm.userid, concat_ws(' ',first_name,last_name) AS full_name FROM pms_users as pm left join pms_device_accessids as de on de.userid = pm.userid
             left join `pms_employee_default_data` as ur on ur.default_data_id=pm.user_type 
            ORDER BY TRIM(full_name)";

            $accessuserdetails = Yii::app()->db->createCommand($acessusersql);
            $accessuserdata = $accessuserdetails->queryAll();


            $selected_values = explode(",", $code);

        ?>
        <label>User</label>
        <select class="multiselect form-control width-95-percentage" name="code[]" multiple="multiple">

            <?php foreach ($accessuserdata as $data) { ?>
            <option value="<?php echo $data['userid'] ?>" <?php echo ((in_array($data['userid'], $selected_values)) ? " selected='selected'" : "") ?>>
              <?php echo $data['full_name'] ?></option>
            <?php } ?>
        </select>

      </div>

        <div class="margin-right-10">

            <?php echo CHtml::submitButton('Search', array('name' => 'search', 'id' => 'search', 'class' => 'btn btn-primary btn-sm finereport-serach-button')); ?>

        </div>

        <div >

        <button  type="submit" class='btn btn-sm btn-info finereport-pdf-export-button' name='exportpdf'>Export <img src="images/pdf.png" class="height-15 margin-right-8"/></button>
         <button  type="submit" class='btn btn-sm btn-info finereport-excel-export-button' name='exportexcel' >Export <img src="images/xl.png" class="height-15 margin-right-8"/></button>
       </div>

    </div>

    <?php echo CHtml::endForm(); ?>



    <br clear="all"/>

</div>

<br>

<div id="parent" class="fine-report-table-scroll">

    <table class="tg greytable" id="fixtable">


        <thead>
            <tr class="toplevel">

                <th class="rname"><div class="width-200">Resource Name</div></th>

                    <?php

                    for ($i = 0; $i < $interval->days + 1; $i++) {

                        $datehead = new DateTime($date_from);

                        $datehead->add(new DateInterval('P' . $i . 'D'));

                        $labelhead = (string)$datehead->format('d-m-Y');

                        ?>
                        <th class="rtime"><?php echo $labelhead ?></th>
                        <?php

                    }

                    ?>

            </tr>
        </thead>

        <tbody>

            <?php

            if (count($final_result) == 0) {

                echo '<tr><td colspan="2">No Entry found</td></tr>';

            }



            ?>





            <?php

            // $final_result = array();

            $k1 = 0;

            foreach ($final_result as $userid => $result) {



                foreach ($result as $pudate => $fitem) {

                    $m = 0;

                    //extract($fitem);



                    if ($m == 0) {

                        ?>

                        <tr class="rowbg_<?php echo $k1 % 2 ?>">

                            <th width="200" colspan="<?php echo ($interval->days) + 2; ?>">

                                <?php echo $fitem['name']; ?>

                            </th>



                        </tr>

                        <?php

                    }

                    $first = '<tr><td width="150" class="left_fix">First Punch</td>';

                    $last = '<tr><td class="left_fix">Last Punch</td>';

                    $in = '<tr><td class="left_fix">IN Time</td>';

                    $out = '<tr><td class="left_fix">Out Time</td>';

                    $tot = '<tr><td class="left_fix">Total Time</td>';

                    $more = '<tr><td class="left_fix">Total Punch:<br/> Missed Punches: </td>';



                    $k1++;

                    $resdate = array_keys($result);

                    for ($i = 0; $i <= $interval->days; $i++) {



                        $date = new DateTime($date_from);

                        $date->add(new DateInterval('P' . $i . 'D'));

                        $caldate = (string)$date->format('d-m-Y');



//                        echo in_array($caldate,$resdate);

//                        die;

                        //echo $caldate.'=='.$pudate."<br/>";

                        $pundate = (string)$pudate;



//                        echo '<pre>';

//                        $d = '10-07-2017';

//                        print_r($result[$d]);

//                        die;



                        if (in_array($caldate, $holiday) == 1 && in_array($caldate, $resdate) != 1) {

                            $first .= '<td align="center">H</td>';

                            $last .= '<td align="center">H</td>';

                            $in .= '<td align="center">H</td>';

                            $out .= '<td align="center">H</td>';

                            $tot .= '<td align="center">H</td>';

                            $more .= '<td align="center">H</td>';

                        } else {

                            $nopunch = '<small>No Punch</small>';

                            $first .= '<td>' . ( (in_array($caldate, $resdate) == 1)?$result[$caldate]['first'] : $nopunch) . '</td>';

                            $last .= '<td>' . (in_array($caldate, $resdate) == 1 ? $result[$caldate]['last'] : $nopunch) . '</td>';

                            $in .= '<td>' . (in_array($caldate, $resdate) == 1 ? gmdate('H:i:s',$result[$caldate]['in']): $nopunch) . '</td>';

                            $out .= '<td>' . (in_array($caldate, $resdate) == 1 ? gmdate('H:i:s',$result[$caldate]['out']): $nopunch) . '</td>';

                            $tot .= '<td>' . (in_array($caldate, $resdate) == 1 ? gmdate('H:i:s',$result[$caldate]['tot']) : $nopunch) . '</td>';

                            $more .= '<td>' . (in_array($caldate, $resdate) == 1 ? intval($result[$caldate]['absol_punch']) . ($result[$caldate]['missed_punch'] == 1 ? '<br />1' : '<br/>&nbsp;') : $nopunch) . '</td>';

                        }

                        if ($i == $interval->days) {

                            echo $first .= '</tr>' . "\n";

                            echo $last .= '</tr>' . "\n";

                            echo $in .= '</tr>' . "\n";

                            echo $out .= '</tr>' . "\n";

                            echo $tot .= '</tr>' . "\n";

                            echo $more .= '</tr>' . "\n";

                        }

                        $m++;

                    }



                    break;

                }

            }

            ?>

        </tbody>

    </table>

</div>
        </div>
<script>
    $(document).ready(function($) {

        //  $("#fixtable").tableHeadFixer({ 'head' : true,'left':1});
        
        $(".multiselect").select2({
            placeholder: "Select User",
            allowClear: true
        });
   });
</script>

