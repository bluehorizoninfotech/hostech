<head>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <script
    src="<?php echo $this->customAssets('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', '/js/jquery.min.js'); ?>"></script>
  <!-- handson table -->
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css" />
</head>

<?php
$this->breadcrumbs = array(
  'Projects',
);
if (empty($project_id)) {
  $project_model = Projects::model()->find(['condition' => 'status = 1 ORDER BY updated_date DESC']);
  if (Yii::app()->user->project_id != "") {
    $project_id = Yii::app()->user->project_id;
  }
}
// echo $project_id;
// exit;
if (empty($from_date)) {
  $from_date = date('01-M-Y');
}
if (empty($to_date)) {
  $to_date = date('t-M-Y');
}
?>
<div class="generateweeklyreport-sec">
  <div class="clearfix">
    <h1>Weekly Reports</h1>
  </div>
  <!-- response message -->
  <div style="display:none;" class="alert alert-success" id="success-alert123">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
    Report Successfully generated.
  </div>
  <div style="display:none;" class="alert alert-danger" id="danger-alert123">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
    Report already exist.
  </div>
  <!-- end message -->
  <div class="view">
    <?php
    $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'id' => 'generate-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'), // Add enctype for file uploads
    
      )
    );
    ?>
    <div class="form-group  weekly-report">
      <div class="row custom-search padding-none">
        <div class="col-md-3">
          <label>Project</label>
          <?=
            $form->dropDownList(
              $model,
              'project_id',
              CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
              array(
                'empty' => 'Choose a project',
                'class' => 'form-control change_project dropdown_style',
                'id' => 'project_id',
                'required' => true
              )
            );
          ?>
          <?php echo $form->error($model, 'project_id'); ?>
        </div>

        <div class="col-md-2">
          <?php
          if ($model->isNewRecord) {
            $model->from_date = '';
            $model->to_date = '';
          } else {
            $model->from_date = date('d-M-y', strtotime($model->from_date));
          }
          ?>
          <label>Date</label>
          <?php echo $form->textField($model, 'from_date', array('class' => 'form-control date_pick', 'autocomplete' => false, 'placeholder' => 'From Date', 'required' => true)); ?>
          <?php echo $form->error($model, 'from_date'); ?>
        </div>
        <div class="col-md-2">
          <label>&nbsp;</label><br>
          <?php echo CHtml::submitButton('Generate', array('class' => 'save_btn btn btn-blue btn-sm btn-bold generate', 'id' => 'generate_id3')); ?>
        </div>
      </div>
      <?php
      if (isset($_GET['ProjectReport']['project_id']) && isset($_GET['ProjectReport']['from_date']) || isset($type)) {
        ?>
        <div class="row">
          <div class="col-md-4 margin-top-10">
            <?php
            $present_status_value = empty($present_status) ? '' : implode("\n", $present_status) . "\n";
            $key_achievements = empty($completed_task_array) ? '' : implode("\n", $completed_task_array) . "\n";
            ?>
            <?= $form->labelEx($design_model, 'Present Status'); ?>
            <textarea class="form-control margin-left-0" rows="6" cols="50" id="present_status">
                                                  <?= $present_status_value; ?>
                                                </textarea>
          </div>
          <div class="col-md-4 margin-top-10">
            <?php echo $form->labelEx($design_model, 'Key Achievements'); ?>
            <textarea class="form-control" rows="6" cols="50" id="key_achievements">
                                                  <?php echo $key_achievements; ?> 
                                                </textarea>
                                                <input type="hidden" name="weekly_report_update" id="weekly_report_update" value="<?= isset($weekly_report_update) ? true : ''; ?>">
          </div>
        </div>
        <!-- design deliverables start -->
        <div class="row margin-top-40">
          <div class="col-md-12">
            <h2>Design Deliverables </h2>
          </div>
          <div id="design_deliverables_id" class="design_deliverables padding-top-10 deliverables" style="display:none1;">
            <?php if (isset($existing_design) && !empty($existing_design)) {
              foreach ($existing_design as $key => $ex_design) { ?>
                <div class="row deliverable-remove-div_<?= ($key + 1) ?>">
                  <div class="col-md-2">
                    <label for="DesignDeliverables_drawing_description">Drawing Description</label>
                    <input class="form-control drawing-description" name="DesignDeliverables[drawing_description][]"
                      id="DesignDeliverables_drawing_description_<?= ($key + 1) ?>" type="text"
                      value="<?= $ex_design->drawing_description ?>">
                    <div class="errorMessage" id="DesignDeliverables_drawing_description_em_" style="display:none"></div>
                  </div>
                  <div class="col-md-2">
                    <label>Status</label>
                    <select class="form-control" name="status">
                      <option value="">Select status</option>
                      <option value="1" <?= ($ex_design->drawing_status == 1) ? 'selected' : ''; ?>>Delivered</option>
                      <option value="2" <?= ($ex_design->drawing_status == 2) ? 'selected' : ''; ?>>Pending</option>
                    </select>
                  </div>

                  <div class="col-md-2 ">
                    <label for="DesignDeliverables_target_date">Target Date</label>
                    <input class="form-control datepicker" autocomplete="off" name="DesignDeliverables[target_date][]"
                      id="DesignDeliverables_target_date_<?= ($key + 1) ?>" type="text"
                      value="<?= $ex_design->target_date ?>">
                    <div class="errorMessage" id="DesignDeliverables_target_date_em_" style="display:none"></div>
                  </div>

                  <div class="col-md-2 validating">
                    <label for="DesignDeliverables_actual_date">Actual Date</label>
                    <input class="form-control datepicker" autocomplete="off" name="DesignDeliverables[actual_date][]"
                      id="DesignDeliverables_actual_date_<?= ($key + 1) ?>" type="text"
                      value="<?= $ex_design->actual_date ?>">
                    <div class="errorMessage" id="DesignDeliverables_actual_date_em_" style="display:none"></div>
                  </div>
                  <div class="col-md-3 validating">
                    <label for="DesignDeliverables_remarks">Remarks</label>
                    <input class="form-control" name="DesignDeliverables[remarks][]"
                      id="DesignDeliverables_remarks_<?= ($key + 1) ?>" type="text" value="<?= $ex_design->remarks ?>">
                    <div class="errorMessage" id="DesignDeliverables_remarks_em_" style="display:none"></div>
                  </div>
                  <input type="hidden" class="deliverable_id" name="DesignDeliverables[deliverable_id][]"
                    value="<?= $ex_design->id; ?>">
                  <div class=" col-md-1  ">
                    <label for="">&nbsp;</label><br>
                    <button type="button" data-id="<?= ($key + 1) ?>" value="Remove"
                      class="btn btn-danger btn-sm deliverable-remove-btn">
                      <i class="glyphicon glyphicon-minus"></i></button>
                  </div>
                </div>
              <?php }
            } ?>
            <div class="row">
              <div class="col-md-2">
                <?php echo $form->labelEx($design_model, 'drawing_description'); ?>
                <?php echo $form->textField($design_model, 'drawing_description[]', array('class' => 'form-control drawing-description')); ?>
                <?php echo $form->error($design_model, 'drawing_description'); ?>
              </div>

              <div class="col-md-2">
                <label>Status</label>
                <select class="form-control" name="status">
                  <option value="">Select status</option>
                  <option value="1">Delivered</option>
                  <option value="2">Pending</option>
                </select>
              </div>

              <div class="col-md-2">
                <?php echo $form->labelEx($design_model, 'target_date'); ?>
                <?php echo $form->textField($design_model, 'target_date[]', array('class' => 'form-control datepicker', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($design_model, 'target_date'); ?>
              </div>

              <div class="col-md-2">
                <?php echo $form->labelEx($design_model, 'actual_date'); ?>
                <?php echo $form->textField($design_model, 'actual_date[]', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($design_model, 'actual_date'); ?>
              </div>

              <div class="col-md-3">
                <?php echo $form->labelEx($design_model, 'remarks'); ?>
                <?php echo $form->textField($design_model, 'remarks[]', array('class' => 'form-control')); ?>
                <?php echo $form->error($design_model, 'remarks'); ?>
              </div>
              <div class="col-md-1">
                <label for="">&nbsp;</label><br>
                <button type="button" id="add_new" value="Add" class="btn blue btn-sm" onClick="addInput();">
                  <i class="glyphicon glyphicon-plus"></i></button>
              </div>
            </div>

            <input type="hidden" id="report_id" value="<?php echo $model->id ?>">
          </div>
          <div id="container"></div>
        </div>
        <div class="row margin-top-40 weekly-report-images">
          <div class="col-md-12">
            <h2>Report images </h2>
          </div>
          <?php if (isset($existing_weekly_report_images) && !empty($existing_weekly_report_images)) {
            foreach ($existing_weekly_report_images as $key => $weekly_report_image) { ?>
              <div class="row remove-div_<?= ($key + 1) ?>">
                <div class="col-md-3">
                  <label for="WeeklyReportImages_image_label">Image Label</label>
                  <input class="form-control image_label" name="WeeklyReportImages[image_label][<?= $key ?>]"
                    id="WeeklyReportImages_image_label<?= ($key + 1) ?>" type="text" required
                    value="<?= $weekly_report_image->image_label ?>">
                  <div class="errorMessage" id="WeeklyReportImages_image_label_em_" style="display:none"></div>
                </div>
                <div class="col-md-3">
                  <label for="WeeklyReportImages_image_path">Image</label>
                  <input type="file" name="WeeklyReportImages[image_path][<?= $key ?>]"
                    id="weekly_report_images_image_path<?= ($key + 1) ?>" class="form-control image_path"
                    accept=".jpg, .jpeg, .png" value="<?= $weekly_report_image->image_path ?>">
                  <div class="errorMessage" id="WeeklyReportImages_image_path_em_" style="display:none"></div>
                </div>
                <input type="hidden" class="weekly_image_id" name="WeeklyReportImages[weekly_image_id][<?= $key ?>]"
                  value="<?= $weekly_report_image->id ?>">
                <div class=" col-md-2  ">
                  <label for="">&nbsp;</label><br>
                  <button type="button" data-id="<?= ($key + 1) ?>" value="Add" class="btn btn-danger btn-sm remove-btn">
                    <i class="glyphicon glyphicon-minus"></i></button>
                </div>
              </div>
            <?php }
          }else{
            $key = -1;
          } ?>
          <div class="row">
            <div class="col-md-3">
              <?php echo $form->labelEx($weekly_report_images, 'image_label'); ?>
              <?php echo $form->textField($weekly_report_images, 'image_label[' . ($key + 1) . ']', array('class' => 'form-control image_label', 'required' => true)); ?>
              <?php echo $form->error($weekly_report_images, 'image_label'); ?>
            </div>
            <div class="col-md-3">
              <?php echo $form->labelEx($weekly_report_images, 'image_path'); ?>
              <?php //echo $form->fileField($weekly_report_images, 'image_path[]', array('class' => 'form-control image_path', 'accept' => '.jpg, .jpeg, .png', 'required' => true)); ?>
              <input type="file" name="WeeklyReportImages[image_path][<?= ($key + 1) ?>]"
                id="weekly_report_images_image_path0" class="form-control image_path" accept=".jpg, .jpeg, .png" required>
              <?php //echo $form->error($weekly_report_images, 'image_path'); ?>
            </div>
            <div class="col-md-1">
              <label for="">&nbsp;</label><br>
              <button type="button" id="add_new_image" value="Add" class="btn blue btn-sm" onClick="addImage();">
                <i class="glyphicon glyphicon-plus"></i></button>
            </div>
          </div>
          <div id="image_container"></div>

        </div>
        <div class="row margin-top-40">
          <div class="col-md-12">
            <h2>Payment Advice Details</h2>
          </div>
          <div class="col-md-12">
            <label class="btn-bold">Payment Advice Issued This Week</label>
            <!-- handson div -->
            <div id="payment-advice-issued-table" class="width-100-percentage"></div>
            <br>
          </div>
          <div class="col-md-12">
            <label class="btn-bold">Pending Payments</label>
            <!-- handson div -->
            <div id="payment-pending-table" class="width-100-percentage"></div>
            <br>
          </div>
        </div>
        <div class="row margin-top-20 margin-bottom-20">
          <div class="col-md-12">
            <?php echo CHtml::Button('Create', array('class' => 'save_btn btn btn-blue create_report float-right', 'id' => 'create_report_id')); ?>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
  <?php $this->endWidget(); ?>
</div>

<!-- grid -->
<?php
$this->widget(
  'zii.widgets.grid.CGridView',
  array(
    'id' => 'weekly-report-grid',
    'dataProvider' => $model->weekly_report(),
    'filter' => $model,
    'itemsCssClass' => 'table custom-table',
    'htmlOptions' => array('class' => 'grid-view table-responsive custom-table-responsive'), // Add custom class to the grid
    'pager' => array(
      'id' => 'dataTables-example_paginate',
      'header' => '',
      'prevPageLabel' => 'Previous ',
      'nextPageLabel' => 'Next '
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
      array(
        'header' => 'S.No.',
        'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        'htmlOptions' => array('class' => 'width-10'),
      ),
      array(
        'class' => 'CButtonColumn',
        'template' => '<div class="d-flex">{view}{update}{pdf}</div>',
        'buttons' => array(
          'view' => array(
            'label' => '',
            'imageUrl' => false,
            'url' => "CHtml::normalizeUrl(array('weekly_report_view', 'project_id'=>\$data->project_id,'date'=>\$data->from_date,'id'=>\$data->id))",
            'options' => array('class' => 'fa-solid icon-eye ', 'title' => 'View', 'target' => '_blank'),
          ),
          'update' => array(
            'label' => '',
            'imageUrl' => false,
            'url' => "CHtml::normalizeUrl(array('update_weekly_report', 'project_id'=>\$data->project_id,'date'=>\$data->from_date,'id'=>\$data->id))",
            'options' => array('class' => 'fa-solid icon-pencil ', 'title' => 'Edit'),
          ),
          'pdf' => array(
            'label' => '',
            'imageUrl' => false,
            'url' => "CHtml::normalizeUrl(array('weekly_report_pdf', 'project_id'=>\$data->project_id,'date'=>\$data->from_date,'id'=>\$data->id))",
            'options' => array('class' => 'fa-solid fa fa-file-pdf-o ', 'title' => 'View'),
          ),
        )
      ),
      //'department_id',
      array(
        'name' => 'project_id',
        'value' => '$data->project->name',
        'type' => 'raw',
        'filter' => CHtml::listData(Projects::model()->findAll(
          array(
            'select' => array('pid,name'),
            'order' => 'name',
            'distinct' => true
          )
        ), "pid", "name"),
      ),

      array(
        'name' => 'from_date',
        'value' => function ($data) {
          if ($data->from_date != '1970-01-01')
            echo date("d-M-y", strtotime($data->from_date));
        },
        'type' => 'html',
        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id'),
        'filter' => false,
      ),


    ),
  )
);
?>
</div>
<!-- end grid -->
<script>
  $(function () {

    $("#ProjectReport_from_date").attr("autocomplete", "off");
    $("#ProjectReport_to_date").attr("autocomplete", "off");
    $("#ProjectReport_from_date").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'd-M-y',
      maxDate: new Date(),
      onSelect: function (selectedDate) {
        $('#ProjectReport_to_date').datepicker('option', 'minDate', selectedDate);
      }
    });

  });

  $(function () {
    $("#ProjectReport_from_date").datepicker({
      maxDate: new Date()
    });
  });


  $(document).on("click", ".generate1", function () {


    var project_id = $('#project_id').val();
    var date = $("#ProjectReport_from_date").val();

    if (project_id == "") {

      $("#ProjectReport_project_id_em_").css("display", "");
      $("#ProjectReport_project_id_em_").text('Select Project');
    }
    if (date == "") {

      $("#ProjectReport_from_date_em_").css("display", "");
      $("#ProjectReport_from_date_em_").text('Select Date');
    } else {
      $("#ProjectReport_project_id_em_").hide();
      $("#ProjectReport_from_date_em_").hide();
      $('#generate_id').hide();
      $('#design_deliverables_id').show();
      $('#add_new').show();
      $('#create_report_id').show();
    }



  });


  $(function () {
    $("#DesignDeliverables_target_date").attr("autocomplete", "off");
    $("#DesignDeliverables_target_date").attr("autocomplete", "off");
    $("#DesignDeliverables_target_date").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'd-M-y',
      onSelect: function (selectedDate) {
        $('#DesignDeliverables_target_date').datepicker('option', 'minDate', selectedDate);
      }
    });
    $("#DesignDeliverables_target_date").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'd-M-y',
    });

    $("#DesignDeliverables_actual_date").attr("autocomplete", "off");
    $("#DesignDeliverables_actual_date").attr("autocomplete", "off");
    $("#DesignDeliverables_actual_date").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'd-M-y',
      onSelect: function (selectedDate) {
        $('#DesignDeliverables_actual_date').datepicker('option', 'minDate', selectedDate);
      }
    });
    $("#DesignDeliverables_actual_date").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'd-M-y',
    });
  });

  // var count = 0;
  var count = $('.design_deliverables .row').length;
  $(function () {
    function initializeDatepicker(selector, count) {
      $(selector + count).attr("autocomplete", "off").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'd-M-y',
        onSelect: function (selectedDate) {
          $(selector + count).datepicker('option', 'minDate', selectedDate);
        }
      });
    }

    for (var i = 1; i < count; i++) {
      initializeDatepicker("#DesignDeliverables_target_date_", i);
      initializeDatepicker("#DesignDeliverables_actual_date_", i);
    }
  });
  // console.log(count);
  function addInput() {

    count += 1;
    $('#container').append(
      `
    <div class="row deliverable-remove-div_` + count + `">

        <div class="col-md-2">
          <label for="DesignDeliverables_drawing_description">Drawing Description</label>     
          <input class="form-control drawing-description" name="DesignDeliverables[drawing_description][]" id="DesignDeliverables_drawing_description_` + count + `" type="text">          <div class="errorMessage" id="DesignDeliverables_drawing_description_em_" style="display:none"></div>        </div>
          <div class="col-md-2">
          <label>Status</label>
          <select class="form-control" name="status">
            <option value="">Select status</option>
            <option value="1">Delivered</option>
            <option value="2">Pending</option>
          </select>
        </div>

        <div class="col-md-2 ">
          <label for="DesignDeliverables_target_date">Target Date</label>    
          <input class="form-control datepicker" autocomplete="off" name="DesignDeliverables[target_date][]" id="DesignDeliverables_target_date_` + count + `" type="text">
          <div class="errorMessage" id="DesignDeliverables_target_date_em_" style="display:none"></div>        </div>

          <div class="col-md-2 validating">
          <label for="DesignDeliverables_actual_date">Actual Date</label>  
          <input class="form-control " autocomplete="off" name="DesignDeliverables[actual_date][]" id="DesignDeliverables_actual_date_` + count + `" type="text">
          <div class="errorMessage" id="DesignDeliverables_actual_date_em_" style="display:none"></div>        </div>

          <div class="col-md-3 validating">
          <label for="DesignDeliverables_remarks">Remarks</label> 
          <input class="form-control" name="DesignDeliverables[remarks][]" id="DesignDeliverables_remarks_` + count + `" type="text">
          <div class="errorMessage" id="DesignDeliverables_remarks_em_" style="display:none"></div>        </div>

          <div class=" col-md-1  ">
          <label for="">&nbsp;</label><br>
          <button type="button"  data-id="`+ count + `" value="Remove" class="btn btn-danger btn-sm deliverable-remove-btn">
              <i class="glyphicon glyphicon-minus"></i></button>
      </div>
      </div>
        
        `
    );


    // datepicker

    $(function () {
      function initializeDatepicker(selector, count) {
        $(selector + count).attr("autocomplete", "off").datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'd-M-y',
          onSelect: function (selectedDate) {
            $(selector + count).datepicker('option', 'minDate', selectedDate);
          }
        });
      }

      initializeDatepicker("#DesignDeliverables_target_date_", count);
      initializeDatepicker("#DesignDeliverables_actual_date_", count);

    });

    // end datepicker

  }

  $(document).on('click', '.deliverable-remove-btn', function () {
    var id = $(this).data('id');
    $(".deliverable-remove-div_" + id).remove();
  });
  var image_count = $('.weekly-report-images .row').length;
  function addImage() {
    $('#image_container').append(`
    <div class="row remove-div_` + image_count + `">
      <div class="col-md-3">
          <label for="WeeklyReportImages_image_label">Image Label</label>
          <input class="form-control image_label" name="WeeklyReportImages[image_label][`+ image_count + `]" id="WeeklyReportImages_image_label` + image_count + `" type="text" required>
          <div class="errorMessage" id="WeeklyReportImages_image_label_em_" style="display:none"></div> 
      </div>
      <div class="col-md-3">
          <label for="WeeklyReportImages_image_path">Image</label>
          <input type="file" name="WeeklyReportImages[image_path][`+ image_count + `]" id="weekly_report_images_image_path`+ image_count + `" class="form-control image_path" accept=".jpg, .jpeg, .png" required>
          <div class="errorMessage" id="WeeklyReportImages_image_path_em_" style="display:none"></div> 
      </div>
      <div class=" col-md-2  ">
          <label for="">&nbsp;</label><br>
          <button type="button"  data-id="`+ image_count + `" value="Add" class="btn btn-danger btn-sm remove-btn">
              <i class="glyphicon glyphicon-minus"></i></button>
      </div>
    </div>      
    `
    );
    image_count += 1;
  }
  $(document).on('click', '.remove-btn', function () {
    id = $(this).data('id');
    $(".remove-div_" + id).remove();
  });
  $(document).on("click", "#create_report_id", function (e) {
    e.preventDefault();
    // var formData = $('#generate-form')[0];
    var formData = new FormData($('#uploadForm')[0]);
    formData.append('project_id', $('#project_id').val());
    formData.append('weekly_report_update', $('#weekly_report_update').val());
    formData.append('date', $("#ProjectReport_from_date").val());
    formData.append('report_id', $('#report_id').val());

    // Extract drawing description, status, target_date, actual_date, remarks, image_path, and image_label
    var drawing_description = $("input[name='DesignDeliverables[drawing_description][]']")
      .map(function () { return $(this).val(); }).get();
    var status = $("select[name=status]").map(function () { return $(this).val(); }).get();
    var deliverable_id = $("input[name='DesignDeliverables[deliverable_id][]']").map(function () { return $(this).val(); }).get();
    // var weekly_image_id = $("input[name='DesignDeliverables[weekly_image_id][]']").map(function () { return $(this).val(); }).get();
    var target_date = $("input[name='DesignDeliverables[target_date][]']")
      .map(function () { return $(this).val(); }).get();
    var actual_date = $("input[name='DesignDeliverables[actual_date][]']")
      .map(function () { return $(this).val(); }).get();
    var remarks = $("input[name='DesignDeliverables[remarks][]']")
      .map(function () { return $(this).val(); }).get();
    // var image_label = $("input[name='WeeklyReportImages[image_label][]']")
    //   .map(function () { return $(this).val(); }).get();
    for (var i = 0; i < drawing_description.length; i++) {
      formData.append('drawing_description[]', drawing_description[i]);
    }
    for (var i = 0; i < status.length; i++) {
      formData.append('status[]', status[i]);
    }
    for (var i = 0; i < deliverable_id.length; i++) {
      formData.append('deliverable_id[]', deliverable_id[i]);
    }
    for (var i = 0; i < target_date.length; i++) {
      formData.append('target_date[]', target_date[i]);
    }
    for (var i = 0; i < actual_date.length; i++) {
      formData.append('actual_date[]', actual_date[i]);
    }
    for (var i = 0; i < remarks.length; i++) {
      formData.append('remarks[]', remarks[i]);
    }
    // for (var i = 0; i < image_label.length; i++) {
    //   formData.append('image_label[]', image_label[i]);
    // }
    // for (var i = 0; i < weekly_image_id.length; i++) {
    //   formData.append('weekly_image_id[]', weekly_image_id[i]);
    // }

    var payment_advice_data = JSON.stringify(advice_hot.getData());
    var payment_pending_data = JSON.stringify(pending_hot.getData());
    formData.append('payment_advice_data', payment_advice_data);
    formData.append('payment_pending_data', payment_pending_data);
    formData.append('key_achievements', $('#key_achievements').val());
    formData.append('present_status', $('#present_status').val());
    // console.log($("input[name='WeeklyReportImages[image_path][]']").val());
    // return false;
    var label = '';
    var file;
    var file_name;
    var weekly_image_id;
    for (var i = 0; i < image_count; i++) {
      label = $("input[name='WeeklyReportImages[image_label][" + i + "]']").val();
      weekly_image_id = $("input[name='WeeklyReportImages[weekly_image_id][" + i + "]']").val();
      if (label) {
        if ($("input[name='WeeklyReportImages[image_path][" + i + "]']").val()) {
          file = $("input[name='WeeklyReportImages[image_path][" + i + "]']")[0].files[0];
          file_name = file.name;
          formData.append('image_files[]', file);
          console.log($("input[name='WeeklyReportImages[image_path][" + i + "]']")[0].files[0]);
        } else {
          file_name = '';
        }
        formData.append('weekly_images[]', JSON.stringify({ label: label, file_name: file_name ,weekly_image_id: weekly_image_id }));
      }
    }
    // return false;
    // $("input[name='WeeklyReportImages[image_path][]']").each(function () {
    //   var files = $(this)[0].files;
    //   for (var j = 0; j < files.length; j++) {
    //     formData.append('image_files[]', files[j]);
    //   }
    // });
    $.ajax({
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('reports/generateweeklyreport'); ?>',
      data: formData,
      method: "POST",
      contentType: false,
      processData: false,
      success: function (result) {
        if (result.status == 1) {
          $('#success-alert123').show();
          // $.fn.yiiGridView.update('weekly-report-grid');
        } else if (result.status == 0) {
          $('#danger-alert123').show();
        } else {
          alert('Something went wrong')
        }
        window.location.href = result.redirect;
      }
    })
    return false;
  });
  const advice_header_data = ['Budget Head', 'Description', 'Vendor / Contractor Name', 'Issued Date', 'PA Reference', 'PA value', 'Status (Paid / Unpaid)'];
  const autosave = false;
  var advice_body_array = '<?php echo $advice_payment_data; ?>';
  var advice_body_array = JSON.parse(advice_body_array);
  const advice_container = document.getElementById('payment-advice-issued-table');
  advice_container.classList.add('custom-handson-table');
  const advice_hot = new Handsontable(advice_container, {
    data: Handsontable.helper.createSpreadsheetData(),
    data: advice_body_array,
    startRows: 5,
    startCols: 12,
    hiddenColumns: {
      columns: [7] // Specify the index of the column you want to hide (0-based index)
    },
    rowHeaders: true,
    colHeaders: advice_header_data,
    width: 'auto',
    height: 'auto',
    minSpareRows: 3,
    minSpareColumns: 3,
    customBorders: true,
    stretchH: 'all',
    licenseKey: 'non-commercial-and-evaluation',
    contextMenu: {
      items: {
        'remove_row': {
          name: 'Remove row'
        }
      }
    },
    afterChange: function (change, source) {
      if (source === 'loadData') {
        return; //don't save this change
      }
      if (!autosave.checked) {
        return;
      }
    },
    columns: [
      {}, // Column 1
      {}, // Column 2
      {}, // Column 3
      { type: 'date', dateFormat: 'YYYY-MM-DD' }, // Column 4 as a date column
      {}, // Column 5
      {}, // Column 6
      {},  // Column 7
      {}
    ]
  });

  const pending_header_data = ['Budget Head', 'Description', 'Vendor / Contractor Name', 'Bill Date', 'Bill Ref No', 'Bill amount', 'Remarks'];
  // const autosave = false;
  var pending_body_array = '<?php echo $pending_payment_data; ?>';
  pending_body_array = JSON.parse(pending_body_array);
  const pending_container = document.getElementById('payment-pending-table');
  pending_container.classList.add('custom-handson-table');
  const pending_hot = new Handsontable(pending_container, {
    data: Handsontable.helper.createSpreadsheetData(),
    data: pending_body_array,
    hiddenColumns: {
      columns: [7] // Specify the index of the column you want to hide (0-based index)
    },
    startRows: 5,
    startCols: 12,
    rowHeaders: true,
    colHeaders: pending_header_data,
    width: 'auto',
    height: 'auto',
    minSpareRows: 3,
    minSpareColumns: 3,
    customBorders: true,
    stretchH: 'all',
    licenseKey: 'non-commercial-and-evaluation',
    contextMenu: {
      items: {
        'remove_row': {
          name: 'Remove row'
        }
      }
    },
    afterChange: function (change, source) {
      if (source === 'loadData') {
        return; //don't save this change
      }
      if (!autosave.checked) {
        return;
      }
    },
    columns: [
      {}, // Column 1
      {}, // Column 2
      {}, // Column 3
      { type: 'date', dateFormat: 'YYYY-MM-DD' }, // Column 4 as a date column
      {}, // Column 5
      {}, // Column 6
      {},  // Column 7
      {}
    ]
  });
</script>