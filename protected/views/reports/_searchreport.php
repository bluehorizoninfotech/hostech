<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */
/* @var $form CActiveForm */
?>
<div class="search-report-section">
<div id="msg"></div>

<div class="form custom-form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
      
      
        <!--div class="col-sm-3" style="width:150px">
            <?php //echo CHtml::label('Only billable', 'billable'); 
            ?>
            <?php //echo CHtml::checkBox('TimeReport[billable]', '', array("value" => 'Yes', "id" => "obill", 'style' => 'margin-top:12px;')); 
            ?>
        </div-->

        <div class="col-md-2">
            <?php
            /* echo CHtml::dropDownList('Tasks[projectid]', '', CHtml::listData(Projects::model()->findAll(
                                    array(
                                        'select' => array('pid,name'),
                                        'order' => 'name',
                                        'distinct' => true
                            )), "pid", "name"), array('class'=>'form-control project','empty' => 'Select a project'));
                            * 
                            * */
            ?>
            <?php
            if ($model->project_id == "") {
                if (Yii::app()->user->project_id != "") {
                    $model->project_id = Yii::app()->user->project_id;
                } else {
                    $model->project_id = "";
                }
            }
            echo $form->dropDownList(
                $model,
                'project_id',
                CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
                array(
                    'empty' => 'Choose a project',
                    'class' => 'form-control input-medium select_drop project change_project',
                   
                )
            );
            ?>
        </div>


        
        <div class="col-md-2">
            <?php echo CHtml::Button('Generate', array('class' => 'btn btn-sm blue submit')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div>
        </div>
<!-- search-form -->
<script>
    jQuery(function($) {
        $('.project').change(function() {
            var val = $(this).val();
            if (val != '') {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('tasks/getlocation'); ?>',
                    data: {
                        project_id: val
                    },
                    dataType: 'json',
                    method: "GET",
                    success: function(result) {
                        $('.clientsite').html(result);
                    }

                })
            }
        });

        $('.submit').click(function() {
            var project_id=$('.project').val();
           if(project_id=="")
           {
            $('.grid-view').hide()
                 $('#msg').html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Please choose a project.</div>');
           }
           else
           {
            $('#yw0').submit();
           }
           

            

          
        });
    });
</script>