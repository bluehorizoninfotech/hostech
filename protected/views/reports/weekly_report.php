<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css');
Yii::app()->clientScript->registerScript('myscript', "
$(function() {
$('.select_drop').select2();
});
");
?>
<div>
  <?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));   ?>
</div>
<div class="search-form">
  <?php $this->renderPartial(
    '_searchreport',
    array(
      'model' => $model,
    )
  ); ?>
</div><!-- search-form -->

<?php
/* @var $this TimeEntryController */
/* @var $model TimeEntry */

$this->breadcrumbs = array(
  'Time Entries' => array('index'),
  'Manage',
);
?>

<h1>Weekly Report</h1>

<div id="wpr_form_div">

</div>

<div id="inttaskform" class="panel panel-primary"></div>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('timereport-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
if (isset($_GET['project_id'])) {

  $data = $model->reportSearch($_GET['project_id']);
  $count = count($data);


} else {
  $data = $model->reportSearch($id = "");
  $count = count($data);
}



?>
<div class="scrollit">
  <table class="table table-striped table-bordered weekly-report-table">
    <thead>
      <tr>
        <th rowspan="2">SL NO</th>
        <th rowspan="2">Action</th>
        <th rowspan="2">Task</th>
        <th rowspan="2">Work head</th>
        <th rowspan="2">Work description</th>
        <th colspan="2">scheduled</th>
        <th colspan="2">Actual</th>
        <th rowspan="2">unit</th>
        <th colspan="2">Quantity</th>
        <th rowspan="2">% completed</th>
        <th rowspan="2">Remarks</th>
      </tr>

      <tr>


        <th>Start date</th>
        <th>End date</th>
        <th>Start date</th>
        <th>End date</th>

        <th>Estimated</th>
        <th>Achieved</th>


      </tr>

    </thead>

    <tbody id="myTable">

      <?php
      if ($count == 0) { ?>

        <td rowspan="12">No data found</td>

      <?php } else {

        foreach ($data as $key => $value) {
          $unit_name = "";
          $actual_end_date = "";
          $worktype = "";
          $progress_percent = "";
          $actual_start_date = "";
          if (!empty($value['unit'])) {
            $unit = Unit::model()->findByPk($value['unit']);
            $unit_name = $unit->unit_code;
          }


          $wpr_timeentry_det = $this->getWrDetails($value['tskid'], $value['quantity'], $value['task_type']);

          // if($value['quantity']!="")
          // {
          //   $progress_percent = ($value['total_qty']/$value['quantity'])*100;
          // }
      
          $work_type = WorkType::model()->findByPk($value['work_type_id']);
          if ($work_type) {
            $worktype = $work_type->work_type;
          }

          // if($progress_percent>=100)
          // {
          //   $actual_end_date=date("d-m-Y",strtotime($value['max_date']));
          // }
      
          // if($value['min_date'])
          // {
          //   $actual_start_date= date("d-m-Y",strtotime($value['min_date']));
          // }
      


          ?>
          <tr>
            <td>
              <? $key + 1; ?>
            </td>
            <?php

            $checkTaskAssigned = $this->checkTaskAssigned($value['tskid']);
            if ($value['task_type'] == 1) {
              if ($checkTaskAssigned == 1 && $worktype != "") {
                ?>
                <!-- <td class="white-space-nowrap"><i class="fa fa-plus addwpr123" id=<?= $value['tskid'] ?>></i> </td> -->


                <td class="white-space-nowrap">
                  <?php echo CHtml::link('<i class="fa fa-plus addwpr123"></i>', array('DailyWorkProgress/dailyProgress', 'type' => 3, 'taskid' => $value['tskid']), array('class' => 'href-colour')) ?>
                </td>
                <?php
              } else {
                ?>
                <td></td>
                <?php
              }
            } else {
              $timeEntryAssigned = $this->checkTimeEntryAssigned($value['tskid']);
              if ($timeEntryAssigned == 1) {
                ?>
                <td class="white-space-nowrap"><i class="fa fa-plus addtimeentry" id=<?= $value['tskid'] ?>></i> </td>
                <?php
              } else {
                ?>
                <td></td>
              <?php } ?>
            <?php } ?>
            <td>
              <?= $value['title'] ?>
            </td>
            <td>
              <?= $worktype; ?>
            </td>
            <td>
              <?= $value['description']; ?>
            </td>
            <td>
              <?= date("d-m-Y", strtotime($value['task_start_date'])); ?>
            </td>
            <td>
              <?= date("d-m-Y", strtotime($value['task_due_date'])); ?>
            </td>
            <td>
              <?php echo $wpr_timeentry_det[0]; ?>
            </td>
            <td>
              <?php echo $wpr_timeentry_det[1]; ?>
            </td>
            <td>
              <?= $unit_name ?>
            </td>
            <td>
              <?= $value['quantity'] ?>
            </td>
            <td>
              <?php echo $wpr_timeentry_det[5]; ?>
            </td>
            <td>
              <?php echo round($wpr_timeentry_det[2], 2) ?>
            </td>
            <td>
              <?php echo $wpr_timeentry_det[4] ?>
            </td>


          </tr>
        <?php }
      } ?>




    </tbody>
  </table>
</div>


</div>
</div>

<script>
  $('.addwpr123').click(function () {
    var task_id = this.id;


    $.ajax({
      "type": "POST",

      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/workProgress'); ?>",
      "dataType": "json",
      "data": {
        task_id: task_id
      },
      "success": function (data) {
        $("#inttaskform").hide();
        $('#wpr_form_div').html(data);
      }
    });

  });


  $(".addtimeentry").click(function () {

    var task_id = this.id;

    $.ajax({
      type: "GET",
      // url:"'.Yii::app()->createAbsoluteUrl("TimeEntry/create&layout=1").'",
      url: "<?php echo Yii::app()->createAbsoluteUrl("TimeEntry/createtimeentry&layout=1") ?>",
      data:
      {
        taskid: task_id
      },
      success: function (response) {
        $('#wpr_form_div').hide();
        $("#inttaskform").html(response).slideDown();

      }
    });

  });

  $(document).ready(function () {
    $('#inttaskform').hide();
  });

</script>