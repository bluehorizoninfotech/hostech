<style>
    .caption {
        /* Make the caption a block so it occupies its own line. */
        display: block;
    }

    body,
    #content {
        background: url('images/pattern.png');
    }

    h2 {
        font-size: 2.5rem;
    }

    .inner_table tr td {
        border: 1px solid black;
        border-collapse: collapse;
        font-size: 14px;
        text-align: center;
    }

    .wrapper {
        max-width: 1000px;
        margin: 0 auto;
    }

    .blue-txt {
        color: rgb(18, 0, 128);
        text-align: center;
    }

    .ted-txt {
        color: rgb(255, 0, 0);
    }

    .white-txt {
        color: rgb(255, 255, 255);
        ;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    table td,
    table th {
        padding: 8px;
    }

    .border-table td,
    .border-table th {
        border: 1px solid #111;
    }

    .work-list>div {
        line-height: 2;
    }

    /*table*/
    .bg_green {
        background: #c6efce;
    }

    .bg_blue {
        background: #5b9bd5;
    }

    .bg_red {
        background: #ffc7ce;
    }

    .bg_yellow {
        background: #ffeb9c;
    }


    .ongoing td {
        background: url('images/pattern.png');
        line-height: 2;
        color: #092863;
        font-weight: bold;
        clear: both;

    }

    .green_txt {
        color: #11704f;
    }

    .red_txt {
        color: #a5104e;
    }

    .yellow_txt {
        color: #b27104
    }

    .px-0 {
        padding-left: 0px;
        padding-right: 0px;
    }

    .text-center {
        text-align: center;
    }

    .banner_sec {
        text-align: center
    }

    ul.on-list li {
        line-height: 2;
        color: #092863;
        font-weight: bold;
        clear: both;
    }

    ul.on-list li span {
        float: right;
        padding-right: 4rem;
    }

    .brown_txt {
        color: #4e3b30;
    }

    h2.brown_txt {
        text-decoration: underline;
        border-top: 1px solid #efa12e;
        padding-top: 1rem;
    }

    div.work-holder {
        padding: 0px 1rem;
    }

    .row {
        display: -ms-flexbox;
        /* IE 10 */
        display: flex;
        -ms-flex-wrap: wrap;
        /* IE 10 */
        flex-wrap: wrap;
        padding: 0 4px;
        margin-bottom: 4rem;
    }

    /* Create two equal columns that sits next to each other */
    .column {
        -ms-flex: 50%;
        /* IE 10 */
        flex: 50%;
        padding: 0 4px;
        text-align: center;
    }

    .column img {
        margin-top: 8px;
        vertical-align: middle;
        width: 200px;
        height: 200px;
    }

    .mb-0 {
        margin-bottom: 0px;
    }

    .mt-0 {
        margin-top: 0px;
    }

    .banner_sec h2,
    h5 {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    ul.bulletin {
        list-style-type: square;

    }
</style>
<style>
    .newspaper {
        column-count: 5;
        column-gap: 40px;
        /* column-rule: 4px double #ff00ff; */
    }

    table,
    th,
    td {
        border: 1px solid black;
    }
</style>
<style>
    .backround1 {
        width: 300px;
        height: 300px;
        border: solid 2px red;
    }

    td,
    tr {
        border: 1px solid;
    }
</style>


<style>
    figure {
        display: inline-block;
        border: 1px dotted gray;
        margin: 20px;
        /* adjust as needed */
    }

    figure img {
        vertical-align: top;
        padding: 5px;
        width: 450px;
    }

    figure figcaption {
        border: 1px dotted blue;
        text-align: center;
    }
</style>
</head>

<body>

    <?php

    if ($model->report_type == 3) { ?>
        <div class="banner_sec">
            <h2>
                <span class="ted-txt" style="color: #7c1316;"><?php echo $model->project->name; ?></span>
                <br>
                <span class="blue-txt" style="color: #e65100;">PROJECT PROGRESS REPORT</span>
            </h2>
            <div class="text-center">
                <h5><span class="blue-txt" style="font-size: 1.5rem;">As on <b><?= date('d-M-Y', strtotime($model->created_date)) ?></b></span></h5>
            </div>
            <div style="max-width:100%;overflow:hidden;">
                <table border="0" width="100%" cellspacing="0" cellpadding="0" style="background: transparent;">
                    <tr>
                        <?php
                        if ($model->project->report_image != '') {
                        ?>
                            <td style="height:800px;overflow:hidden;text-align:center;" valign="middle">
                                <?php
                                if ($model->project->report_image != '') {
                                ?>
                                    <?php
                                    $path = realpath(Yii::app()->basePath . '/../uploads/project/report_imgs/' . $model->project->report_image);
                                    if ($pdf == 1) {

                                    ?>

                                        <img class="img_click" src="<?php echo $path; ?>" alt="Project Image" width="250" height="300" height="700">
                                    <?php
                                    } else { ?>
                                        <img class="" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $model->project->report_image; ?>" alt="Project Image">
                                <?php
                                    }
                                }
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                </table>

            </div>

        </div>
        <div class="banner_sec">
            <h2><span class="blue-txt" style="color: #e65100;"><u>ONGOING WORK</u></span></h2>
        </div>

        <div class="newspaper">
            <ul class="bulletin">
                <?php
                foreach ($project_tasks as $tasks) {
                    if ($tasks['parent_id'] == NULL) { ?>
                        <li>
                            <h3><?php echo $tasks['title'] . " - " . $tasks['contractor_name'] ?></h3>
                        </li>
                <?php
                    }
                }
                ?>
            </ul>
        </div>

        <div class="banner_sec">

            <?php foreach ($project_tasks as $tasks) {
                $area_details = $this->getAreaDetails($tasks['id']);

                $work_type = $this->getworkType($tasks['id']);

                if ($tasks['parent_id'] == NULL && count($area_details) > 0 && count($work_type) > 0) {
                    //
            ?>

                    <h2><span class="blue-txt" style="color: #e65100;"><u>Working progress-<?php echo $tasks['title'] ?></u></span></h2>

                    <table width="100%" cellspacing="0" cellpadding="0" style="background: transparent;">
                        <thead>
                            <tr align="center">
                                <th style="padding:2.5px; width: 10px;" rowspan="2">Area</th>

                                <th style="padding:2.5px; text-align:center;" colspan="<?php echo count($work_type) ?>">Nature of work</th>

                            </tr>
                            <tr>
                                <?php
                                foreach ($work_type as $key => $type) {
                                ?>
                                    <th><?php echo $type['work_type'] ?></th>
                                <?php
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($area_details as $area) {
                            ?>
                                <tr>
                                    <td>
                                        <?php echo $area['area_title']; ?>
                                    </td>
                                   
                                    <?php
                                    for ($j = 0; $j < count($work_type); $j++) {
                                        foreach ($work_type as $type1) {

                                            $getTotalWorkProgress = $this->getTotalWorkProgress($area['tskid'], $type1['wtid'], $area['id'], $tasks['id']);
                                    ?>

                                            <td><?php echo $getTotalWorkProgress . "%" ?></td>
                                <?php }
                                        break;
                                    }
                                }
                                ?>
                                </tr>
                                <?

                                ?>
                        </tbody>
                    </table>

                    <div class="banner_sec">
                        <?php
                        $getImages = $this->getImages($tasks['id']);
                        if (count($getImages) > 0) { ?>

                            <h2><span class="blue-txt" style="color: #e65100;"><u><?php echo $tasks['title'] . " work in progress pictures from site" ?></u></span></h2>
                            <?php
                            foreach ($getImages as $images) {
                                // $res = str_replace(array(
                                //     '\'', '"',
                                //     ',', '[', ']', '>'
                                // ), '', $images['img_paths']);
                                $res=$images['image_path'];
                                $path = realpath(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $res);
                                if ($pdf == 1) {
                            ?>

                                    <figure>
                                        <img src="<?php echo $path ?>" alt='' class="img-class" />
                                        <figcaption><?= isset($images['image_label']) ? $images['image_label'] : '' ?>
                                        </figcaption>
                                    </figure>

                                <?php } else {
                                ?>
                                    <figure>
                                        <img src="<?php echo Yii::app()->request->baseUrl . "/uploads/dailywork_progress/" . $res; ?>" alt='' />
                                        <figcaption><?= isset($images['image_label']) ? $images['image_label'] : '' ?>
                                        </figcaption>
                                    </figure>
                            <?php
                                }
                            }
                            ?>
                    </div>
        </div>

    <?php
                        }
    ?>
<?php
                }
?>







<?php  }
?>
</div>

<?php } //closing of  if($model->report_type==3) 
?>