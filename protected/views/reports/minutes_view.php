<div class="minutes-view-reports-sec">
<div class="wrapper">
    <?php
    if ($pdf == 1) {
        $project_image = realpath(Yii::app()->basePath . '/../uploads/project/report_imgs/' . $project->report_image);
        $logo = realpath(Yii::app()->basePath . '/../images/logo.png');
    ?>
        <div>
            <tr>
                <?php if ($project->report_image != '') { ?>
                    <td>
                        <img class="pull-left" src="<?php echo $project_image ?>" alt=" " height="50px">
                    </td>
                <?php } ?>
                <td>
                    <img class="pull-right" src="<?php echo $logo ?>" alt="" height="50px">
                </td>

            </tr>
        </div>

    <?php } else {

        $project_image = Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $project->report_image;
        $logo = Yii::app()->request->baseUrl . "/images/logo.png";
    ?>
        <div>
            <tr>
                <?php if ($project->report_image != '') { ?>
                    <td>
                        <img class="pull-left" src="<?php echo $project_image ?>" alt=" " height="50px">
                    </td>
                <?php } ?>
                <td>
                    <img class="pull-right" src="<?php echo $logo ?>" alt="" height="50px">
                </td>

            </tr>
        </div>
        <br>
    <?php
    }
    ?>



    <tr>
        <th colspan="5">
            <u>
                <h2 class="blue-txt">MINUTES OF MEETING</h2>
            </u>
        </th>
    </tr>


    <table class="border-table">
        <tr>
            <td colspan="6" bgcolor="#801100" class="white-txt text-align-center">
                <b>SITE MEETING-<?= $model->meeting_number ?></b>
            </td>
        </tr>
        <tbody>
            <tr>
                <td colspan="5">
                    Project Name
                </td>
                <td><b><?= $project->name ?></b></td>
            </tr>
            <tr>
                <td colspan="5">
                    Architect
                </td>
                <td><b><?= isset($project->architect) ? $project->architect : '' ?></b></td>
            </tr>
            <tr>
                <td colspan="4">
                    PMC
                </td>
                <td><b>Ashly PMS</b></td>
                </td>
                <td class="pale-blue-bg"><b><?= $model->meeting_number ?></b></td>
            </tr>
            <tr>
                <td>Date</td>
                <td><?= date("d/m/Y", strtotime($model->date)); ?></td>
                <td>Time</td>
                <td><?= date("g:i A", strtotime($model->meeting_time)); ?></td>
                <td>Venue</td>
                <td><?= $model->venue ?></td>

            </tr>

            <tr>
                <td colspan="3">Date of commencement</td>
                <td><?= date("d/m/Y", strtotime($project->start_date)); ?></td>
                <td>Doc No</td>
                <td><b><?= $project->project_no . "/MOM-" . $model->meeting_number ?></b></td>

            </tr>
            <tr>
                <td colspan="3">Date of completion(as per
                    agreement)</td>
                <td><?= date("d/m/Y", strtotime($project->end_date)); ?></td>
                <td><b>Project Days
                        Till date</b></td>
                <?php
                $startTimeStamp = strtotime($project->start_date);
                $endTimeStamp = strtotime(date('Y-m-d'));

                $timeDiff = abs($endTimeStamp - $startTimeStamp);

                $numberDays = $timeDiff / 86400;

                $numberDays = intval($numberDays);
                ?>
                <td><b><?= $numberDays . " days" ?></b></td>


            </tr>
            <tr>
                <td colspan="3" class="pink-bg"><b>Sl.No</b></td>
                <td class="pink-bg"><b>Name</b></td>
                <td class="pink-bg"><b>Designation</b></td>
                <td class="pink-bg"><b>Firm</b></td>

            </tr>
            <?php
            $i = 1;
            foreach ($participants as $participant) {
            ?>
                <tr>
                    <td colspan="3"><?= $i ?></td>
                    <td><?= $participant->participants ?></td>
                    <td><?= $participant->designation ?></td>
                   
                    <td><?= isset($participant->contractor_id)?$participant->contractor->contractor_title:null; ?></td>
                </tr>
            <?php
                $i++;
            } ?>
        </tbody>

    </table>
    <br><br>
    <!-- <tr>
            <td colspan="2">
                <b>Date and Venue of <br />
                    meeting:</b>
            </td>
            <td colspan="3"><i><?= isset($model->date) ? date('F d, Y', strtotime($model->date)) : ''  ?> at <?= isset($model->site->site_name) ? $model->site->site_name : '' ?> Site</i></td>
        </tr>
        <tr>
            <td colspan="5">
                <hr />
            </td>
        </tr> -->
    <!-- <tr>
            <td colspan="2" valign="top"><b>present:</b></td>
            <td colspan="3">
                <?php
                foreach ($participants as $participant) {
                    $contractor_title=isset($participant->contractor_id)?$participant->contractor->contractor_title:null;
                    echo  '<br />';
                    echo $participant->participants . ' (' . $contractor_title . ')';
                }
                ?>
            </td>
        </tr> -->
    <!-- <tr>
            <td colspan="5">
                <hr />
            </td>
        </tr> -->
    <table class="border-table">
        <tr>
            <td colspan="5" bgcolor="#801100" class="white-txt">
                <b>1. Objective of Meeting</b>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <?= $model->objective ?>
            </td>
        </tr>
    </table>
    <br /><br />
    <!-- <tr>
            <td colspan="5">
                <hr />
            </td>
        </tr> -->
    <table class="border-table">
        <tr>
            <td colspan="5" bgcolor="#801100" class="white-txt">
                <b>2. Meeting Notes, Decisions, Issues</b>
            </td>
        </tr>
        <tr>
            <td colspan="5">Following points were discussed:</td>
        </tr>
        <tr>
            <th>Sl <br />No</th>
            <th>Points discussed</th>
            <th>Decisions/ Status/Remarks</th>
            <th>Action By</th>

        </tr>
        <?php
        $i = 1;
        foreach ($minutes_points as $minutes_point) {
        ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $minutes_point->title ?></td>
                <td><?= $minutes_point->points_discussed ?></td>
                <td align="center">
                    <?php $this->getActionBy($minutes_point->id) ?>
                </td>


            </tr>
        <?php
            $i++;
        }
        ?>

    </table>


    <br /><br />
    <table class="border-table">
        <tr>
            <td colspan="6" bgcolor="#801100" class="white-txt">
                <b>5. Next Meeting – proposed</b>
            </td>
        </tr>
        <tr>
            <td><i>Date:</i></td>
            <td><i>
                    <?= isset($model->next_meeting_date) ? date('d/m/Y', strtotime($model->next_meeting_date)) : ''  ?> </i></td>
            <td><i>Time:</i></td>
            <td><i><?= isset($model->next_meeting_time) ? date("g:i A", strtotime($model->next_meeting_time)) : ''; ?></i></td>
            <td><i>Location:</i></td>
            <td><i><?= $model->location ?></i></td>
        </tr>
    </table>

    <br />

    <table class="border-table">
        <tr>
            <td colspan="6" bgcolor="#801100" class="white-txt">
                <b>6. Conclusion</b>
            </td>
        </tr>
    </table>
    <!-- <tr>
        <td colspan="5"> -->
    The Following points were discussed
    <?= $model->note ?>
    <br>
    MOM Approved By
    <br>
    <?= isset($model->approve_decision_by) ?    $model->createdBy->first_name . " " . $model->createdBy->last_name : ''; ?>
    <br><br>
    <?php
    if ($pdf == 1) {

    ?>
        <div>
            <span><?= $project->name ?></span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>Ashly Group of Companies</span>
        </div>

    <?php } else {
    ?>
        <span class="pull-left"><?= $project->name ?></span>
        <span class="pull-right">Ashly Group of Companies</span>

    <?php
    }
    ?>
    </div>
</div>