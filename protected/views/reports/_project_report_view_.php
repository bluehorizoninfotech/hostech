<style>
    body{
        background: url('images/pattern.png');
    }
    </style>
</head>
<body>

    <?php
    if (!isset($filepart) or ($filepart == 1)) {
    ?>
        <div class="text-center">
            <img src="<?php echo Yii::app()->request->baseUrl . "/images/logo.jpg" ?>" alt=" hostech" height="50px">
        </div>
        <div>

            <div class="banner_sec">
                <h2>
                    <span class="ted-txt dark-red-color"><?php echo $model->project->name; ?></span>
                    <br>
                    <span class="blue-txt orange-color">PROGRESS REPORT</span>
                </h2>
                <div class="text-center">
                    <h5><span class="blue-txt font-one-point-five-rem">As on <b><?= date('d-M-Y', strtotime($model->created_date)) ?></b></span></h5>
                </div>
                <div class="max-width-100-percentage overflow-hidden">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="transprant-bg">
                        <tr>
                            <td class="height-700 overflow-hidden text-align-center" valign="middle">
                                <?php
                                if ($model->project->report_image != '') {
                                    $path = realpath(Yii::app()->basePath . '/../uploads/project/report_imgs/' . $model->project->report_image);
                                    if (file_exists($path)) {
                                ?>
                                        <img class="img_click" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $model->project->report_image; ?>" alt="Project Image">
                                <?php
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                    </table>

                    <!-- <img class="img_click" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/" . $model->project->img_path; ?>" alt="..." style=" border:3px solid #efa12e;height:768px;"> -->

                </div>

            </div>

        <?php
    }

    if (!isset($filepart) or ($filepart == 2)) {
        ?>


            <div>
                <h2 class="brown_txt text-center">ONGOING WORKS</h2>
                <!-- <ul class="on-list"> -->
                <div class="work-holder">
                    <table>
                        <?php
                        foreach ($milestone_datas as $milestone_data) {
                        ?>
                            <tr class="ongoing">
                                <td class="ongoing_list font-one-point-two-rem line-height-2 font-weight-bold"><?= $milestone_data['title'] ?></td>
                                <td class="ongoing_list font-one-rem line-height-2 font-weight-bold"><?= '-' ?></td>
                                <td class="ongoing_list text-align-right font-one-rem line-height-2 font-weight-bold"><?= $milestone_data['contractor'] ?></td>
                            </tr>
                            <!-- <li style="line-height: 2;font-weight:bold;"><?= $milestone_data['title'] ?><span>-<?= $milestone_data['contractor'] ?>.</span></li> -->
                        <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- </ul> -->
            </div>
        <?php
    }

    if (!isset($filepart) or ($filepart == 3)) {
        ?>

            <?php
            $flg = 0;
            foreach ($milestone_datas as $key => $milestone_data) {
                if (isset($milestone_data['work_type'])) {
                    $flg = 1;
                    $ranking = array_column($milestone_data['work_type'], 'ranking');
                    array_multisort($ranking, SORT_ASC, $milestone_data['work_type']);
                    // echo '<pre>';
                    // print_r($milestone_data['work_type']);
                    if (isset($milestone_data['work_type'][0])) {
                        $others = $milestone_data['work_type'][0];
                    } else {
                        $others = '';
                    }

                    $work_type_count = 0;
                    foreach ($milestone_data['work_type'] as $wrk_type) {
                        if ($wrk_type['status'] == 1) {
                            $work_type_count++;
                        }
                    }


                    if (isset($milestone_data['work_type']) && !empty($milestone_data['areas'][0])) {
            ?>
                        <div class="work-holder">
                            <h2 class="text-center brown_txt"><?= $milestone_data['title'] ?></h2>

                            <table class="inner_table">
                                <tr>
                                    <td rowspan="2" class="bg_blue white-txt">AREA</td>
                                    <td colspan="<?= $work_type_count ?>" class="bg_blue white-txt">NATURE OF WORK</td>

                                </tr>
                                <tr>
                                    <?php
                                    foreach ($milestone_data['work_type'] as $project_work_type) {

                                        if ($project_work_type['status'] == 1 && $project_work_type['ranking'] != 0) {
                                            echo '<td class="bg_blue white-txt">' . $project_work_type['title'] . '</td>';
                                        }
                                    }
                                    if (isset($others['ranking']) && isset($others['title'])) {
                                        if ($others['ranking'] == 0)
                                            echo '<td class="bg_blue white-txt">' . $others['title'] . '</td>';
                                    }

                                    ?>

                                </tr>
                                <?php
                                $bg_count = 0;

                                foreach ($milestone_data['areas'] as $project_area) {
                                    // if (!empty($project_area)) {
                                    $bg_count++;
                                    if ($bg_count == 1) {
                                        $bg_class = 'bg_green green_txt';
                                    } elseif ($bg_count == 2) {
                                        $bg_class = 'bg_yellow yellow_txt';
                                    } elseif ($bg_count == 3) {
                                        $bg_class = 'bg_red red_txt';
                                    } else {
                                        $bg_class = 'bg_green green_txt';
                                    }
                                ?>
                                    <tr>
                                        <td class="<?= $bg_class ?>"><?= $project_area['title'] ?></td>
                                        <?php
                                        foreach ($milestone_data['work_type'] as $project_work_type) {

                                            if ($project_work_type['status'] == 1) {

                                                if ($project_work_type['ranking'] != 0) {

                                                    echo '<td class="' . $bg_class . '">' . $task_model->getWorkTypePercentage($project_work_type['id'], $milestone_data['id'], $project_area['id'], $milestone_data['project_id']) . '</td>';
                                                }
                                            } else {
                                                if ($others['ranking'] == 0) {
                                                    echo '<td class="' . $bg_class . '">' . $task_model->getOthersPercentage($milestone_data['project_id'], $milestone_data['id'], $project_area['id']) . '</td>';
                                                }
                                            }
                                        }
                                        ?>
                                    </tr>
                                <?php
                                    if ($bg_count == 3)
                                        $bg_count = 0;
                                }
                                // }
                                ?>
                            </table>
                        </div>
                        <!-- <div>
                            <h2 class="text-center brown_txt">TILE WORK IN PROGRESS PICTURES FROM SITE</h2>
                            <div class="row">
                                <div class="column">
                                    <img src="images/site1.jpg">
                                    <img src="images/site2.jpg">
                                    <img src="images/site3.jpg">
                                    <img src="images/site4.jpg">
                                    <img src="images/site5.jpg">
                                    <img src="images/site6.jpg">

                                </div>
                            </div>
                        </div> -->
        <?php
                    }
                }
            }
            if ($flg == 0 and isset($filepart)) {
                echo 'No milestone worktype data';
            }
        }
        ?>