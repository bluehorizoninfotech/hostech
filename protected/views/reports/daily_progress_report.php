<script
    src="<?php echo $this->customAssets('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', '/js/jquery.min.js'); ?>"></script>

<?php
$this->breadcrumbs = array(
    'Projects',
);
if (empty($project_id)) {
    $project_model = Projects::model()->find(['condition' => 'status = 1 ORDER BY updated_date DESC']);
    if (Yii::app()->user->project_id != "") {
        $project_id = Yii::app()->user->project_id;
    }
}
// echo $project_id;
// exit;
if (empty($from_date)) {
    $from_date = date('01-M-Y');
}
if (empty($to_date)) {
    $to_date = date('t-M-Y');
}
?>
<div class="daily-progress-report-sec">
    <div class="clearfix">
        <div class="pull-right">

        </div>
        <h1>Daily Progress Reports</h1>
    </div>



    <!-- response message -->



    <div style="display:none;" class="alert alert-success" id="success-alert123">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
        Report Successfully generated.
    </div>

    <div style="display:none;" class="alert alert-danger" id="danger-alert123">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
        Report already exist.
    </div>

    <!-- end message -->


    <div class="view">
        <?php $form = $this->beginWidget(
            'CActiveForm',
            array(
                'id' => 'project-report-_form-form',
                // 'action' => Yii::app()->createUrl('/reports/generateDailyProgressReport'),
                'enableAjaxValidation' => true,

            )
        ); ?>
        <div class="form-group custom-search-form">
            <div class="row">
                <div class="col-md-3">
                    <?=
                        $form->dropDownList(
                            $model,
                            'project_id',
                            CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
                            array(
                                'empty' => 'Choose a project',
                                'class' => 'form-control change_project dropdown_style height-28',
                                'id' => 'project_id'
                            )
                        );
                    ?>
                    <?php echo $form->error($model, 'project_id'); ?>
                </div>

                <div class="col-md-2 fileds-on-daily-progress-report">
                    <?php
                    if ($model->isNewRecord) {
                        $model->from_date = '';
                        $model->to_date = '';
                    }
                    ?>
                    <?php echo $form->textField($model, 'from_date', array('class' => 'form-control date_pick height-28', 'autocomplete' => false, 'placeholder' => 'From Date')); ?>
                    <?php echo $form->error($model, 'from_date'); ?>
                </div>

                <div class="col-md-2 fileds-on-daily-progress-report">
                    <div>
                        <?php echo CHtml::Button('Generate', array('class' => 'save_btn btn blue btn-sm generate')); ?>
                    </div>
                </div>
            </div>


        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div>

    </div>
    <!-- grid -->
    <?php
    $this->widget(
        'zii.widgets.grid.CGridView',
        array(
            'id' => 'daily-progress-report-grid',
            'dataProvider' => $model->daily_progress_report(),
            'filter' => $model,
            'itemsCssClass' => 'table table-bordered',
            'template' => '<div class="table-responsive">{items}</div>',
            'pager' => array(
                'id' => 'dataTables-example_paginate',
                'header' => '',
                'prevPageLabel' => 'Previous ',
                'nextPageLabel' => 'Next '
            ),
            'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
            'columns' => array(
                array(
                    'header' => 'S.No.',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                ),
                array(
                    'htmlOptions' => array('class' => 'width-80'),
                    'class' => 'CButtonColumn',
                    'template' => '{view}{pdf}',
                    'buttons' => array(
                        'view' => array(
                            'label' => '',
                            'imageUrl' => false,
                            'url' => "CHtml::normalizeUrl(array('view_daily_progress_report', 'id'=>\$data->project_id,'date'=>\$data->from_date))",
                            'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View', 'target' => '_blank'),
                        ),
                        'pdf' => array(
                            'label' => '',
                            'imageUrl' => false,
                            'url' => "CHtml::normalizeUrl(array('daily_progress_report_pdf', 'id'=>\$data->project_id,'date'=>\$data->from_date))",

                            'options' => array('class' => 'fa fa-file-pdf-o icon-comn', 'title' => 'View'),
                        ),
                    )
                ),
                //'department_id',
                array(
                    'name' => 'project_id',
                    'value' => '$data->project->name',
                    'type' => 'raw',
                    'filter' => CHtml::listData(Projects::model()->findAll(
                        array(
                            'select' => array('pid,name'),
                            'order' => 'name',
                            'distinct' => true
                        )
                    ), "pid", "name"),
                ),

                array(
                    'name' => 'from_date',
                    'value' => function ($data) {
                        if ($data->from_date != '1970-01-01')
                            echo date("d-M-y", strtotime($data->from_date));
                    },
                    'type' => 'html',
                    'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id'),
                    'filter' => false,
                ),


            ),
        )
    );
    ?>
    <!-- end grid -->
    <script>
        $(function () {
            $("#ProjectReport_from_date").attr("autocomplete", "off");
            $("#ProjectReport_to_date").attr("autocomplete", "off");
            $("#ProjectReport_from_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'd-M-y',
                onSelect: function (selectedDate) {
                    $('#ProjectReport_to_date').datepicker('option', 'minDate', selectedDate);
                }
            });
            $("#ProjectReport_to_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'd-M-y',
            });
        });



        $(document).on("click", ".generate", function () {
            var project_id = $('#project_id').val();
            var date = $("#ProjectReport_from_date").val();

            $.ajax({
                "dataType": "json",
                url: '<?php echo Yii::app()->createUrl('reports/generateDailyProgressReport'); ?>',
                data: {
                    project_id: project_id,
                    date: date

                },
                method: "POST",
                success: function (result) {
                    if (result.status == 1) {
                        $('#success-alert123').show();
                        $.fn.yiiGridView.update('daily-progress-report-grid');
                    }
                    else if (result.status == 0) {
                        $('#danger-alert123').show();
                    }
                    else {
                        alert('Something went wrong')
                    }
                }
            })

        });


    </script>