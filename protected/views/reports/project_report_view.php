<style>
    body,
    #content {
        background: url('images/pattern.png');
    }

    h2 {
        font-size: 2.5rem;
    }

    .inner_table tr td {
        border: 1px solid black;
        border-collapse: collapse;
        font-size: 14px;
        text-align: center;
    }

    .wrapper {
        max-width: 1000px;
        margin: 0 auto;
    }

    .blue-txt {
        color: rgb(18, 0, 128);
        text-align: center;
    }

    .ted-txt {
        color: rgb(255, 0, 0);
    }

    .white-txt {
        color: rgb(255, 255, 255);
        ;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    table td,
    table th {
        padding: 8px;
    }

    .border-table td,
    .border-table th {
        border: 1px solid #111;
    }

    .work-list>div {
        line-height: 2;
    }

    /*table*/
    .bg_green {
        background: #c6efce;
    }

    .bg_blue {
        background: #5b9bd5;
    }

    .bg_red {
        background: #ffc7ce;
    }

    .bg_yellow {
        background: #ffeb9c;
    }


    .ongoing td {
        background: url('images/pattern.png');
        line-height: 2;
        color: #092863;
        font-weight: bold;
        clear: both;

    }

    .green_txt {
        color: #11704f;
    }

    .red_txt {
        color: #a5104e;
    }

    .yellow_txt {
        color: #b27104
    }

    .px-0 {
        padding-left: 0px;
        padding-right: 0px;
    }

    .text-center {
        text-align: center;
    }

    .banner_sec {
        text-align: center
    }

    ul.on-list li {
        line-height: 2;
        color: #092863;
        font-weight: bold;
        clear: both;
    }

    ul.on-list li span {
        float: right;
        padding-right: 4rem;
    }

    .brown_txt {
        color: #4e3b30;
    }

    h2.brown_txt {
        text-decoration: underline;
        border-top: 1px solid #efa12e;
        padding-top: 1rem;
    }

    div.work-holder {
        padding: 0px 1rem;
    }

    .row {
        display: -ms-flexbox;
        /* IE 10 */
        display: flex;
        -ms-flex-wrap: wrap;
        /* IE 10 */
        flex-wrap: wrap;
        padding: 0 4px;
        margin-bottom: 4rem;
    }

    /* Create two equal columns that sits next to each other */
    .column {
        -ms-flex: 50%;
        /* IE 10 */
        flex: 50%;
        padding: 0 4px;
        text-align: center;
    }

    .column img {
        margin-top: 8px;
        vertical-align: middle;
        width: 200px;
        height: 200px;
    }

    .mb-0 {
        margin-bottom: 0px;
    }

    .mt-0 {
        margin-top: 0px;
    }

    .banner_sec h2,
    h5 {
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>
</head>

<body>

    <?php
    if (!isset($filepart) or ($filepart == 1)) {
    ?>       
        <div>
            <div class="banner_sec">
                <h2>
                    <span class="ted-txt" style="color: #7c1316;"><?php echo $model->project->name; ?></span>
                    <br>
                    <?php
                    if($model->report_type==1)
                    {
                        $type="CONSOLIDATED REPORT";
                    }
                    else
                    {
                        $type="DETAILED REPORT";
                    }
                    ?>
                    <span class="blue-txt" style="color: #e65100;"><?php echo $type?></span>
                </h2>
                <div class="text-center">
                    <h5><span class="blue-txt" style="font-size: 1.5rem;">As on <b><?= date('d-M-Y', strtotime($model->project->end_date)) ?></b></span></h5>
                </div>
                <div style="max-width:100%;overflow:hidden;">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" style="background: transparent;">
                     
                    <tr>
                        <?php
                        if ($model->project->report_image != '') {
                        ?>
                            <td style="height:800px;overflow:hidden;text-align:center;" valign="middle">
                                <?php
                                if ($model->project->report_image != '') {
                                ?>
                                    <?php
                                    $path = realpath(Yii::app()->basePath . '/../uploads/project/report_imgs/' . $model->project->report_image);
                                    if ($pdf == 1) {

                                    ?>

                                        <img class="img_click" src="<?php echo $path; ?>" alt="Project Image" width="250" height="300" height="700">
                                    <?php
                                    } else { ?>
                                        <img class="" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $model->project->report_image; ?>" alt="Project Image">
                                <?php
                                    }
                                }
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                        
                    </table>

                    <!-- <img class="img_click" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/" . $model->project->img_path; ?>" alt="..." style=" border:3px solid #efa12e;height:768px;"> -->

                </div>

            </div>

        <?php
    }

    if (!isset($filepart) or ($filepart == 2)) {
        ?>
            <div>
                <h2 class="brown_txt text-center">ONGOING WORKS</h2>
                <!-- <ul class="on-list"> -->
                <div class="work-holder">
                    <table>
                        <?php
                        foreach ($milestone_datas as $milestone_data) {
                        ?>
                            <tr class="ongoing">
                                <td class="ongoing_list" style="font-size: 1.2rem;line-height: 2;font-weight:bold;"><?= $milestone_data['title'] ?></td>
                                <td class="ongoing_list" style="font-size: 1rem;line-height: 2;font-weight:bold;"><?= '-' ?></td>
                                <td class="ongoing_list" style="text-align: right;font-size: 1rem;line-height: 2;font-weight:bold;"><?= $milestone_data['contractor'] ?></td>
                            </tr>
                            <!-- <li style="line-height: 2;font-weight:bold;"><?= $milestone_data['title'] ?><span>-<?= $milestone_data['contractor'] ?>.</span></li> -->
                        <?php
                        }
                        ?>
                    </table>
                </div>
                <!-- </ul> -->
            </div>
        <?php
    }
    $flg = 0;
    
   
    if (!isset($filepart) or ($filepart == 3)) {
        $flg = 0;
        ?>
            <?php
            if ($model->report_type == 1) {
                $flg = 1;

            ?>
                <div class="work-holder">


                    <table class="inner_table">
                        <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Milestone</th>
                                <th>Contractor</th>
                                <th>Progress %</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $report_datas = !empty($model->report_data) ? json_decode($model->report_data, true) : '';
                            $i = 1;
                            foreach ($report_datas as $report_data) {
                            ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $report_data['milestone_title'] ?></td>
                                    <td><?= isset($report_data['contractor_title'])?$report_data['contractor_title']:'' ?></td>
                                    <td><?= isset($report_data['progress_perc'])?$report_data['progress_perc']:'' ?></td>
                                    <td><?= isset($report_data['status'])?$report_data['status']:'' ?></td>
                                </tr>
                            <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <?php
            } elseif ($model->report_type == 2) {
            ?>
                <div class="work-holder">


                    <table class="inner_table">
                        <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Milestone</th>
                                <th>Contractor</th>
                                <th>Main Task</th>
                                <th>SubTask</th>
                                <th>Area</th>
                                <th>WorkType</th>
                                <th>Total Quantity</th>
                                <th>Targeted Quantity</th>
                                <th>Achieved Quantity</th>
                                <th>Balance Quantity</th>
                                <th>Progress</th>
                                <th>Status</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $report_datas = !empty($model->report_data) ? json_decode($model->report_data, true) : '';
                            $i = 1;
                            if(count($report_datas)>0)
                            {
                            foreach ($report_datas as $report_data) {
                            ?>
                                <tr>
                                    <td><?= $i ?></td>                                    
                                    <td><?= isset($report_data['milestone_title'])? $report_data['milestone_title']:''?></td>       
                                    <td><?= isset($report_data['contractor_title'])? $report_data['contractor_title']:''?></td>
                                    <td><?= $report_data['task_title'] ?></td>
                                    <td><?= isset($report_data['sub_task']) ? $report_data['sub_task'] : '' ?></td>                                   
                                    <td><?= isset($report_data['area'])?$report_data['area']:'' ?></td>                                   
                                    <td><?= isset($report_data['work_type'])?$report_data['work_type']:'' ?></td>
                                    <td><?= $report_data['total_qty'] ?></td>
                                    <td><?= $report_data['targeted_qty'] ?></td>
                                    <td><?= $report_data['achieved_qty'] ?></td>
                                    <td><?= $report_data['balance'] ?></td>
                                    <td><?= $report_data['progres_percentage'] ?></td>
                                    <td><?= $report_data['status'] ?></td>
                                    <td><?= $report_data['description'] ?></td>
                                </tr>
                            <?php
                                $i++;
                            }
                        }
                            ?>
                        </tbody>
                    </table>
                </div>
        <?php

            }
        }
        if ($flg == 0 and isset($filepart)) {
            $report_datas = !empty($model->report_data) ? json_decode($model->report_data, true) : '';
            if(count($report_datas)>0){
                if ($model->report_type == 2) {
            ?>
            <!-- echo 'No milestone worktype data'; -->

           <div class="work-holder">


                    <table class="inner_table">
                        <thead>
                            <tr>
                                <th>SL No111</th>
                                <th>Milestone</th>
                                <th>Contractor</th>
                                <th>Main Task</th>
                                <th>SubTask</th>
                                <th>Area</th>
                                <th>WorkType</th>
                                <th>Total Quantity</th>
                                <th>Targeted Quantity</th>
                                <th>Achieved Quantity</th>
                                <th>Balance Quantity</th>
                                <th>Progress</th>
                                <th>Status</th>
                                <th>Description</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            
                            $i = 1;
                            foreach ($report_datas as $report_data) {
                            ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= isset($report_data['milestone_title'])?$report_data['milestone_title']:'' ?></td>
                                    <td><?= isset($report_data['contractor_title'])?$report_data['contractor_title']:'' ?></td>                                   
                                    <td><?=isset($report_data['task_title'])?$report_data['task_title']:''?></td>
                                    <td><?= isset($report_data['sub_task']) ? $report_data['sub_task'] : '' ?></td>
                                    <td><?= isset($report_data['area'])?$report_data['area']:'' ?></td>
                                    <td><?= isset($report_data['work_type'])?$report_data['work_type']:'' ?></td>
                                    <td><?= isset($report_data['total_qty'])?$report_data['total_qty']:'' ?></td>
                                    <td><?= isset($report_data['targeted_qty'])?$report_data['targeted_qty']:'' ?></td>
                                    <td><?= isset($report_data['achieved_qty'])?$report_data['achieved_qty']:'' ?></td>

                                    <td><?= isset($report_data['balance'])?$report_data['balance']:'' ?></td>
                                    <td><?= isset($report_data['progres_percentage'])?$report_data['progres_percentage']:'' ?></td>
                                    <td><?= isset($report_data['status'])?$report_data['status']:'' ?></td>
                                    <td><?= isset($report_data['description'])?$report_data['description']:'' ?></td>
                                </tr>
                            <?php
                                $i++;
                            }
                }
                            ?>
                        </tbody>
                    </table>
                </div>
           
           
       
       <?php
       if ($model->report_type == 1) {?>
       
        <div class="work-holder">


        <table class="inner_table">
            <thead>
                <tr>
                    <th>SL No</th>
                    <th>Milestone</th>
                    <th>Contractor</th>
                    <th>Progress %</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $report_datas = !empty($model->report_data) ? json_decode($model->report_data, true) : '';
                $i = 1;
                foreach ($report_datas as $report_data) {
                ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $report_data['milestone_title'] ?></td>
                        <td><?= isset($report_data['contractor_title'])?$report_data['contractor_title']:'' ?></td>
                        <td><?= isset($report_data['progress_perc'])?$report_data['progress_perc']:'' ?></td>
                        <td><?= isset($report_data['status'])?$report_data['status']:'' ?></td>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
     <?php  }
         } }
        ?>