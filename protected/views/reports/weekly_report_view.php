<style>
  .wrapper {
    max-width: 1000px;
    margin: 0 auto;
  }

  .blue-txt {
    color: #120080;
    text-align: center;
  }

  .ted-txt {
    color: red;
  }

  .white-txt {
    color: #fff;
  }

  .grey-bg {
    background-color: #ddd;
  }


  table {
    width: 100%;
    border-collapse: collapse;
  }

  table td,
  table th {
    padding: 8px;
  }

  .table custom-table td,
  /* .table custom-table th {
    border: 1px solid #111;
  } */

  .head-border th {
    border: 1px solid #111;
  }


  .text-center {
    text-align: center;
  }

  .text-left {
    text-align: left;
  }

  .text-right {
    text-align: right;
  }

  .blue-bg {
    background: #44709C;
    color: white;
    border: 1px solid #111;
  }

  .table-top {
    border: 1px solid white !important;
    background: white !important;
  }
</style>
<div class="minutes-view-sec">
  <div class="wrapper">
    <div>
      <table>
        <?php if ($project->report_image != "" && file_exists(YiiBase::getPathOfAlias('webroot') . "/uploads/project/report_imgs/" . $project->report_image)) {
          $project_image = Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $project->report_image;
          ?>
          <tr>
            <td rowspan="7">
              <img class="pull-left" src="<?= $project_image ?>" alt=" " height="250px">
            </td>
          <?php } ?>

          <td>
            <img class="pull-right" src="<?php echo $this->logo ?>" alt="" height="50px">
          </td>
        </tr>
        <tr>
          <td>
            <h3>WEEKLY ROJECT STATUS REPORT - WEEK
              <?= $weekOfMonth . " (" . $month . ")" ?>
            </h3>
          </td>
        </tr>
        <tr>
          <td>
            <h4>Client Name:
              <?= isset($data->client) ? strtoupper($data->client->name) : "N/A" ?>
            </h4>
          </td>
        </tr>
        <tr>
          <td>
            <h4>Location:</h4>
          </td>
        </tr>
        <tr>
          <td>
            <h4>Project Code:
              <?= strtoupper($project->project_no) ?>
            </h4>
          </td>
        </tr>
      </table>
    </div>
    <table class="table custom-table">
      <?php
      if ($weekly_report_data) {
        $jsonData = json_decode($weekly_report_data->report_data, true);
      }
      ?>

      <thead>
        <tr>
          <th colspan="13" class="text-left blue-bg">PRESENT STATUS</th>
        </tr>
        <thead>


        <tbody>
          <?php
          if (isset($jsonData['1'])) {
            $value1 = $jsonData['1'];


            if ($value1 != "") {

              $lines = explode("\n", $value1);
              $bulletPointedLines = array_map(function ($line) {
                return " " . trim($line);
              }, $lines);
              // Join the lines back together
              $value = implode("\n", $bulletPointedLines);

              $nonEmptyLines = array_filter($bulletPointedLines, function ($line) {
                return !empty (trim($line, ' '));
              });


              ?>
              <tr>
                <td colspan="13">
                  <?php echo nl2br(htmlspecialchars(implode("\n", $nonEmptyLines))); ?>
                </td>
              </tr>
              <?php
            } else {
              ?>
              <tr>
                <td class="empty" colspan="13"><i>No data found.</i></td>
              </tr>
              <?php
            }
            ?>




            <?php
          } else { ?>
            <tr>
              <td class="empty" colspan="13"><i>No data found.</i></td>
            </tr>

          <?php } ?>

        </tbody>

    </table>
    <!--  -->
    <table class="table custom-table">

      <thead>
        <tr>
          <th colspan="13" class="table-top"></th>
        </tr>

        <tr>
          <th colspan="13" class="text-left blue-bg"> KEY ACHIEVEMENTS IN THE LAST WEEK</th>
        </tr>
        <thead>



        <tbody>
          <?php
          if (isset($jsonData['2'])) {
            $value2 = $jsonData['2'];

            ?>


            <?php
            if ($value2 != "") {

              $lines = explode("\n", $value2);
              $bulletPointedLines = array_map(function ($line) {
                return " " . trim($line);
              }, $lines);
              // Join the lines back together
              $value = implode("\n", $bulletPointedLines);

              $nonEmptyLines = array_filter($bulletPointedLines, function ($line) {
                return !empty (trim($line, ' '));
              });

              ?>
              <tr>
                <td colspan="13">
                  <?php echo nl2br(htmlspecialchars(implode("\n", $nonEmptyLines))); ?>
                </td>
              </tr>
              <?php
            } else {
              ?>
              <tr>
                <td class="empty" colspan="13"><i>No data found.</i></td>
              </tr>
              <?php
            }
            ?>




            <?php
          } else {
            ?>
            <tr>
              <td class="empty" colspan="13"><i>No data found.</i></td>
            </tr>
            <?php
          }
          ?>


        </tbody>

    </table>
    <table class=" custom-table">
      <thead>
        <tr>
          <th colspan="13" class="table-top"></th>
        </tr>
        <tr>
          <th colspan="13" class="text-left blue-bg">KEY ACTIONS PLANNED FOR THE NEXT TWO WEEKS </th>
        </tr>
        <thead>
        <tbody>
          <tr>
            <?php if (count($task_for_nxt_wk) > 0) { ?>
              <td colspan="13">
                <ul>
                  <?php foreach ($task_for_nxt_wk as $next) { ?>
                    <li>
                      <?= $next->title ?>
                    </li>
                    <?php
                  } ?>
                </ul>
              </td>
            <?php } else { ?>
              <td class="empty" colspan="13"><i>No data found.</i></td>
            <?php } ?>
          <tr>
        </tbody>
    </table>
    <table class=" custom-table ">
      <thead>
        <tr>
          <th colspan="13" class="table-top"></th>
        </tr>
        <tr>
          <th colspan="13" class="text-left blue-bg">KEY RISKS / DESIGN ISSUES / PENDING TARGETS</th>
        </tr>
        <thead>
        <tbody>
          <tr>
            <?php
            if (count($expired_pending_task) > 0) { ?>
              <td colspan="13">
                <ul>
                  <?php foreach ($expired_pending_task as $expired) {
                    ?>
                    <li>
                      <?= $expired['title'] ?>
                    </li>
                  <?php } ?>
                </ul>
              </td>
            <?php } else { ?>
              <td class="empty" colspan="13">
                <i class="empty">No data found.</i>
              </td>
            <?php }
            ?>
          </tr>
        </tbody>
    </table>

    <table class="table custom-table">
      <thead>
        <tr>
          <th colspan="13" class="table-top"></th>
        </tr>

        <tr>
          <th colspan="13" class="text-left blue-bg">DESIGN DELIVERABLES</th>
        </tr>
        <tr>
          <th colspan="13" class="text-left blue-bg">DRAWING DELIVERED THIS WEEK</th>
        </tr>
        <tr class="head-border">
          <th>SL NO </th>
          <th>DRAWING DESCRIPTION</th>
          <th>TARGETED DATE</th>
          <th> ACTUAL DATE</th>
          <th> REMARKS</th>
        </tr>
        <thead>
        <tbody>
          <?php if (count($delivered_deliverables) > 0) {
            foreach ($delivered_deliverables as $delivered) {
              $count = 1;
              ?>
              <tr>
                <td>
                  <?php echo $count ?>
                </td>
                <td>
                  <?php echo $delivered->drawing_description ?>
                </td>
                <td>
                  <?php echo date("d-m-Y", strtotime($delivered->target_date)) ?>
                </td>
                <td>
                  <?php echo date("d-m-Y", strtotime($delivered->actual_date)) ?>
                </td>
                <td>
                  <?php echo $delivered->remarks ?>
                </td>
              </tr>
              <?php $count++;
            }
          } else { ?>
            <tr>
              <td class="empty" colspan="8"><i>No data found.</i></td>
            </tr>
          <?php } ?>
        </tbody>
    </table>
    <!--  -->
    <table class="table custom-table">
      <thead>
        <tr>
          <th colspan="13" class="table-top"></th>
        </tr>

        <tr>
          <th colspan="13" class="text-left blue-bg">PENDING DRAWINGS</th>
        </tr>
        <tr class="head-border">
          <th>SL NO </th>
          <th>DRAWING DESCRIPTION</th>
          <th>TARGETED DATE</th>
          <th> ACTUAL DATE</th>
          <th> REMARKS</th>
        </tr>
        <thead>
        <tbody>
          <?php if (count($pending_deliverables) > 0) {
            foreach ($pending_deliverables as $delivered) {
              $count = 1;
              ?>
              <tr>
                <td>
                  <?php echo $count ?>
                </td>
                <td>
                  <?php echo $delivered->drawing_description ?>
                </td>
                <td>
                  <?php echo date("d-m-Y", strtotime($delivered->target_date)) ?>
                </td>
                <td>
                  <?php echo date("d-m-Y", strtotime($delivered->actual_date)) ?>
                </td>
                <td>
                  <?php echo $delivered->remarks ?>
                </td>
              <tr>
                <?php $count++;
            }
          } else { ?>
            <tr>
              <td class="empty" colspan="8"><i>No data found</i></td>
            </tr>
          <?php } ?>
        </tbody>
    </table>
    <!--  -->
    <div class="table-responsive">
      <table class="table custom-table">
        <thead>
          <tr>
            <th colspan="13" class="table-top"></th>
          </tr>

          <tr>
            <th colspan="13" class="text-left blue-bg">WORK SCHEDULE</th>
          </tr>
          <tr class="head-border">

            <th rowspan="2">SL NO</th>
            <th rowspan="2">ACTIVITY DESCRIPTION</th>

            <th colspan="2" class="text-center">SCHEDULED</th>
            <th colspan="2" class="text-center">ACTUAL</th>
            <th rowspan="2">% COMPLETED</th>
            <th rowspan="2">REMARKS</th>
          </tr>
          <tr>
            <th>Start date</th>
            <th>End date</th>
            <th>Start date</th>
            <th>End date</th>
          </tr>

          <thead>
          <tbody>
            <?php
            if (count($task_array) > 0) {
              foreach ($task_array as $key => $data) {
                ?>
                <tr>
                  <td></td>
                  <td>
                    <?= $key ?>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>

                </tr>
                <?php
                $count = 1;
                foreach ($data as $value) {
                  $actual_start_date = "";
                  $progress_percent = "";
                  $actual_end_date = "";
                  if ($value['min_date']) {
                    $actual_start_date = date("d-m-Y", strtotime($value['min_date']));
                  }
                  if ($value['quantity'] != "") {
                    $progress_percent = ($value['total_quantity'] / $value['quantity']) * 100;
                  }

                  if ($progress_percent >= 100) {
                    $actual_end_date = date("d-m-Y", strtotime($value['max_date']));
                  }
                  ?>
                  <tr>
                    <td>
                      <?= $count ?>
                    </td>
                    <td>
                      <?= $value['task'] ?>
                    </td>
                    <td>
                      <?= date("d-m-Y", strtotime($value['start_date'])) ?>
                    </td>
                    <td>
                      <?= date("d-m-Y", strtotime($value['end_date'])) ?>
                    </td>
                    <td>
                      <?= $actual_start_date ?>
                    </td>
                    <td>
                      <?= $actual_end_date ?>
                    </td>
                    <td>
                      <?= $progress_percent ?>
                    </td>
                    <td>
                      <?= $value['description'] ?>
                    </td>

                  </tr>
                  <?php $count++;
                }
              }
            } else { ?>
              <tr>
                <td class="empty" colspan="8"><i>No data found</i></td>
              </tr>
            <?php } ?>
          </tbody>

      </table>
    </div>
    <!--  -->
    <div class="table-responsive">
      <table class="table custom-table">
        <thead>
          <tr>
            <th colspan="13" class="table-top"></th>
          </tr>

          <tr>
            <th colspan="13" class="text-left blue-bg">DETAILS OF SITE VISIT</th>
          </tr>
          <tr>
            <th colspan="13" class="text-left blue-bg">DETAILS OF VISIT</th>
          </tr>
          <tr class="head-border">
            <th>SL NO </th>
            <th>NAME OF EMPLOYEE </th>
            <th>DESIGNATION </th>
            <th> SITE VISIT DATE</th>
            <th> PURPOSE</th>
          </tr>
          <thead>
          <tbody>
            <?php if (count($site_visit) > 0) {
              foreach ($site_visit as $key => $visit) {
                ?>
                <tr>
                  <td>
                    <?= $key + 1 ?>
                  </td>
                  <td>
                    <?= $visit->visitor_name ?>
                  </td>
                  <td>
                    <?= $visit->designation ?>
                  </td>
                  <td>
                    <?= date("d-m-Y", strtotime($visit->date)) ?>
                  </td>
                  <td>
                    <?= $visit->purpose ?>
                  </td>
                </tr>
              <?php }
            } else { ?>
              <tr>
                <td class="empty" colspan="5"><i>No data found</i></td>

              </tr>
            <?php } ?>
          </tbody>
      </table>

    </div>

    <div class="table-responsive">
      <table class="table custom-table">
        <thead>
          <tr>
            <th colspan="13" class="table-top"></th>
          </tr>
          <tr>
            <th colspan="13" class="text-left blue-bg">Payment Advice Issued</th>
          </tr>
          <tr class="head-border">
            <th>SL NO </th>
            <th>Budget Head</th>
            <th>Description</th>
            <th>Vendor / Contractor Name</th>
            <th>Issued Date</th>
            <th>PA Reference</th>
            <th>PA value</th>
            <th>Status (Paid / Unpaid)</th>
          </tr>
          <thead>
          <tbody>
            <?php if (count($payment_advice_data) > 0) {
              foreach ($payment_advice_data as $key => $advice) {
                ?>
                <tr>
                  <td>
                    <?= $key + 1 ?>
                  </td>
                  <td>
                    <?= isset($advice->budget_head) ? $advice->budget_head : '' ?>
                  </td>
                  <td>
                    <?= isset($advice->description) ? $advice->description : '' ?>
                  </td>
                  <td>
                    <?= isset($advice->vendor) ? $advice->vendor : '' ?>
                  </td>
                  <td>
                    <?= isset($advice->budget_head) ? date("d-m-Y", strtotime($advice->issued_date)) : '' ?>
                  </td>
                  <td>
                    <?= isset($advice->pa_ref) ? $advice->pa_ref : '' ?>
                  </td>
                  <td>
                    <?= isset($advice->pa_value) ? $advice->pa_value : '' ?>
                  </td>
                  <td>
                    <?= isset($advice->status) ? $advice->status : '' ?>
                  </td>
                </tr>
              <?php }
            } else { ?>
              <tr>
                <td class="empty" colspan="8"><i>No data found</i></td>
              </tr>
            <?php } ?>
          </tbody>
      </table>
    </div>

    <div class="table-responsive">
      <table class="table custom-table">
        <thead>
          <tr>
            <th colspan="13" class="table-top"></th>
          </tr>

          <tr>
            <th colspan="13" class="text-left blue-bg">Pending Payment Details</th>
          </tr>
          <tr class="head-border">
            <th>SL NO </th>
            <th>Budget Head</th>
            <th>Description</th>
            <th>Vendor / Contractor Name</th>
            <th>Bill Date</th>
            <th>Bill Re</th>
            <th>Bill Amount</th>
            <th>Remarks</th>
          </tr>
          <thead>
          <tbody>
            <?php if (count($payment_pending_data) > 0) {
              foreach ($payment_pending_data as $key => $pending) {
                ?>
                <tr>
                  <td>
                    <?= $key + 1 ?>
                  </td>
                  <td>
                    <?= isset($pending->budget_head) ? $pending->budget_head : '' ?>
                  </td>
                  <td>
                    <?= isset($pending->description) ? $pending->description : '' ?>
                  </td>
                  <td>
                    <?= isset($pending->vendor) ? $pending->vendor : '' ?>
                  </td>
                  <td>
                    <?= isset($pending->bill_date) ? date("d-m-Y", strtotime($pending->bill_date)) : '' ?>
                  </td>
                  <td>
                    <?= isset($pending->bill_ref) ? $pending->bill_ref : '' ?>
                  </td>
                  <td>
                    <?= isset($pending->bill_amount) ? $pending->bill_amount : '' ?>
                  </td>
                  <td>
                    <?= isset($pending->remarks) ? $pending->remarks : '' ?>
                  </td>
                </tr>
              <?php }
            } else { ?>
              <tr>
                <td class="empty" colspan="8"><i>No data found</i></td>
              </tr>
            <?php } ?>
          </tbody>
      </table>
    </div>
    <?php
    if (count($weekly_report_images) > 0) {
      $counter = 0;
      ?>
      <table class="table custom-table">
        <thead>
          <tr>
            <th colspan="13" class="text-left ">WEEKLY REPORT PHOTOS</th>
          </tr>
        </thead>
      </table>
      <div class="banner_sec">
        <?php
        foreach ($weekly_report_images as $images) {
          if ($counter % 3 === 0) {
            // Start a new row
            echo '<div class="image-row">';
          }
          $res = $images['image_path'];
          $path = Yii::app()->request->baseUrl . "/uploads/weekly_report/" . $res;
          ?>
          <figure>
            <img src="<?php echo $path ?>" alt='' width="100" height="100" />
            <figcaption>
              <?= isset($images['image_label']) ? $images['image_label'] : '' ?>
            </figcaption>
          </figure>

          <?php
          $counter++;
          if ($counter % 3 === 0) {
            echo '</div>';
          }
        }
        if ($counter % 3 !== 0) {
          echo '</div>';
        }
    }
    ?>
      <!-- report images -->
      <?php
      if (count($report_images) > 0) {
        $counter = 0;
        ?>
        <table class="table custom-table">
          <thead>
            <tr>
              <th colspan="13" class="text-left blue-bg">WORK PHOTOS</th>
            </tr>
          </thead>
        </table>
        <div class="banner_sec">
          <?php
          foreach ($report_images as $images) {
            if ($counter % 3 === 0) {
              // Start a new row
              echo '<div class="image-row">';
            }
            $path = Yii::app()->request->baseUrl . "/uploads/dailywork_progress/" . $images['image_path'];
            ?>
            <figure>
              <img src="<?php echo $path ?>" alt='' width="100" height="100" />
              <figcaption>
                <?= isset($images['image_label']) ? $images['image_label'] : '' ?>
              </figcaption>
            </figure>
            <?php
            $counter++;
            if ($counter % 3 === 0) {
              echo '</div>';
            }
          }
          if ($counter % 3 !== 0) {
            echo '</div>';
          }
      }
      ?>
        <!-- end report images -->
      </div>
    </div>
  </div>