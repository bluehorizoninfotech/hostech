<?php
/* @var $this SiteMeetingsController */
/* @var $model SiteMeetings */
/* @var $form CActiveForm */
?>

<div class="wide form custom-search-form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
	)); ?>

	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'id'); ?>
				<?php echo $form->textField($model, 'id', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'project_id'); ?>
				<?php echo $form->textField($model, 'project_id', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'site_id'); ?>
				<?php echo $form->textField($model, 'site_id', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'date'); ?>
				<?php echo $form->textField($model, 'date', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'venue'); ?>
				<?php echo $form->textField($model, 'venue', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
			</div>
		</div>

		

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'next_meeting_date'); ?>
				<?php echo $form->textField($model, 'next_meeting_date', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'next_meeting_time'); ?>
				<?php echo $form->textField($model, 'next_meeting_time', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'location'); ?>
				<?php echo $form->textField($model, 'location', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
			</div>
		</div>

		

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'created_by'); ?>
				<?php echo $form->textField($model, 'created_by', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'created_date'); ?>
				<?php echo $form->textField($model, 'created_date', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'updated_by'); ?>
				<?php echo $form->textField($model, 'updated_by', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'updated_date'); ?>
				<?php echo $form->textField($model, 'updated_date', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'objective'); ?>
				<?php echo $form->textArea($model, 'objective', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->label($model, 'note'); ?>
				<?php echo $form->textArea($model, 'note', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-md-12 buttons">
			<div class="pull-right">
				<?php echo CHtml::submitButton('Search'); ?>
			</div>
		</div>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- search-form -->