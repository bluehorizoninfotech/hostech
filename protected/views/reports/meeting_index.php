<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-3.4.1.min.js', '/js/jquery-3.4.1.min.js'); ?>">
</script>
<?php
/* @var $this SiteMeetingsController */
/* @var $model SiteMeetings */

$this->breadcrumbs = array(
	'Site Meetings' => array('index'),
	'Manage',
);

$this->menu = array(
	array('label' => 'List SiteMeetings', 'url' => array('index')),
	array('label' => 'Create SiteMeetings', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('site-meetings-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="meeting-index-sec">
<div class="add link pull-right">
	<?php

	echo CHtml::link('New Meeting', array('projects/addmeeting'), array('class' => 'btn blue'));
	?>
</div>
<h1 class="margin-top-20">Manage Site Meetings</h1>

<!-- <?php 
echo CHtml::link('Advanced Search', '#', array('class' => 'search-button'));
 ?> -->
<div class="search-form display-none">
	<?php
	$this->renderPartial('_search', array(
		'model' => $model,
	));
	?>
</div>
<!-- search-form -->
<!-- approve form -->
<div class="approve_form">
	<div>
		<?php //if (in_array('/approvals/attendanceprivileges', Yii::app()->session['pmsmenuauthlist'])) { 
		?>
		<textarea name="reason" class="reason"></textarea>
		<input type="button" name='approve' class='requestaction btn btn-success' value='Approve' /> &nbsp;
		<input type="button" class='requestaction btn btn-danger' name='reject' value='Reject' />
		<img id="loadernew" src="/images/loading.gif" class="width-30 margin-left-159 display-none" />
		<?php
		echo CHtml::image(
			Yii::app()->request->baseUrl . '/images/loading.gif',
			'Loading....',
			array('class' => 'width-30 margin-left-159 display-none')
		);
		?>
		<?php //} 
		?>
	</div>


</div>
<div id="approve-div" class="approve_div"></div>
<p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully updated..</p>
<p id="error_message" class="display-none font-15 red-color font-weight-bold">An error occured...</p>
<!-- end approve form -->
<?php
if (yii::app()->user->role == 1) {
	$project_condition = 'status = 1';
	$site_condition = '';
} else {
	$site_condition = '';
	$project_ids = Yii::app()->myClass->project_ids();
	if (!empty($project_ids)) {
		$site_condition = 'pid IN (' . $project_ids . ')';
		$project_condition = 'FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to) OR created_by = ' . Yii::app()->user->id . ' OR pid IN (' . $project_ids . ') AND status = 1';
	} else {
		$project_condition = 'FIND_IN_SET(' . Yii::app()->user->id . ',assigned_to) OR created_by = ' . Yii::app()->user->id . ' AND status = 1';
	}
}
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	// 'id' => 'photo-punch',
	'id' => 'site-meetings-grid',
	'dataProvider' => $model->search(),
	'ajaxUpdate' => false,
	'itemsCssClass' => 'table table-bordered photo-punch-table',
	'template' => '<div class="table-responsive">{items}</div>',
	'filter' => $model,
	'pager' => array(
		'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
		'nextPageLabel' => 'Next '
	),
	'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
			'selectableRows' => 2,
			'value' => '$data->id',


			'checkBoxHtmlOptions' => array(
				'name' => 'ids[]',
				'class' => 'checkbox',
			),
			'cssClassExpression' => '( $data->approved_status == 0 && $data->getCheckbox($data->id)==1)? "" : "hiden" ',

		),
		array(
			'header' => 'S.No.',
			'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),

		array(
			'name' => 'project_id',
			'value' => '$data->project->name',
			'type' => 'raw',
			'filter' => CHtml::listData(Projects::model()->findAll(
				array(
					'select' => array('pid,name'),
					'condition' => $project_condition,
					'order' => 'name',
					'distinct' => true
				)
			), "pid", "name"),

		),

		array(
			'name' => 'site_id',
			'value' => 'isset($data->site_id)?$data->site->site_name:""',
			'type' => 'raw',
			'filter' => CHtml::listData(Clientsite::model()->findAll(
				array(
					'select' => array('id,site_name'),
					'condition' => $site_condition,
					'order' => 'site_name',
					'distinct' => true
				)
			), "id", "site_name"),
		),

		array(
			'name' => 'date',
			'value' => 'date("d-M-y",strtotime($data->date))',
			'type' => 'html',
			'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
		),
		'venue',
		array(
			'name' => 'approved_status',
			'header' => 'Status',
			'value' => '"<div id=\'approved_status_".$data->id."\'>".(($data->approved_status==0) ? "<span style=color:orange>Pending</span>": (($data->approved_status ==1)? "<span style=color:green>Approved </span>" : "<span style=color:red> Rejected </span>"))."</div>"',
			'type' => 'raw',
			'filter' => array('0' => 'Pending', '1' => 'Approved', '2' => 'Rejected'),
		),
		array(
			'name' => 'created_date',
			'value' => 'date("d-M-y",strtotime($data->created_date))',
			'type' => 'html',
		),

		array(
			'htmlOptions' => array('class' => 'width-80'),
			'class' => 'CButtonColumn',
			'template' => '{view}{update}{pdf}',

			'buttons' => array(
				'view' => array(
					'label' => '',
					'imageUrl' => false,
					'url' => "CHtml::normalizeUrl(array('minutesview', 'id'=>\$data->id))",
					'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View'),
				),
				'update' => array(
					'label' => '',
					'imageUrl' => false,
					'url' => "CHtml::normalizeUrl(array('/projects/minutesupdate', 'id'=>\$data->id))",
					'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Update'),
				),
				'pdf' => array(
					'label' => '',
					'imageUrl' => false,
					'url' => "CHtml::normalizeUrl(array('meetingpdf', 'id'=>\$data->id))",

					'options' => array('class' => 'fa fa-file-pdf-o icon-comn', 'title' => 'View'),
				),


			)

		),
	),
));
?>
</div>
</div>
<script>
	$(document).ready(function() {
		var parenttr = $(this).closest("tr");
		var id = parenttr.find("#idd").val();
		alert

		$('.hiden').children().addClass('hidden_checkbox');
		$('.hidden_checkbox').remove();
		$(document).on('click', '#site-meetings-grid_c0_all', function() {
			var checked = this.checked;
			$("input[name='ids[]']").each(function() {
				if (checked == true) {
					$(this).find("input[name='ids']").prop('checked', true);
					$(this).parent('span').addClass('checked');
				} else {
					$(this).find("input[name='ids']").prop('checked', false);
					$(this).parent('span').removeClass('checked');
				}

			});
		});
	});
	$(function() {
		$('.approve_form').hide();
		$("table input[type=checkbox]").click(function() {
			var count = $("table tbody input[type=checkbox]").length;
			var countchecked = $("table tbody input[type=checkbox]:checked").length;
			if (countchecked >= 1) {
				$('.approve_form').slideDown();
			} else if (countchecked == 0) {
				$('.approve_form').slideUp();
			}
			if (countchecked == count) {
				$('table thead tr th:first-child').find('span').addClass('checked');
			} else {
				$('table thead tr th:first-child').find('span').removeClass('checked');
			}
		});

		$("table thead  input[type=checkbox]").click(function() {
			$(this).parent('span').toggleClass('checked');
			if ($(this).parent('span').hasClass('checked')) {
				$('.approve_form').slideDown();
			} else {
				$('.approve_form').slideUp();
			}
		})
	})

	$(".requestaction").click(function() {

		$('.requestaction').attr('disabled', true);
		var req = $(this).val();
		var reason = $('.reason').val();
		var all = [];
		var data_type = [];
		$('input[name="ids[]"]:checked').each(function() {
			$id_type = (this.value).split('#');
			all.push($id_type[0]);
			data_type.push($id_type[1])
		});
		if (all != '') {
			if (reason == '') {
				alert("please add comment");
				$('.requestaction').attr('disabled', false);
			} else {
				if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {
					$('#loadernew').show();
					$.ajax({
						method: "post",
						dataType: "json",
						data: {
							id: all,
							req: req,
							reason: reason,
							data_type: data_type
						},
						url: '<?php echo Yii::app()->createUrl("/SiteMeetings/approveMeeting") ?>',

						success: function(ret) {

							$('#loadernew').hide();
							$('.requestaction').attr('disabled', false);

							if (ret.status == 1) {
								$("#success_message").fadeIn().delay(1000).fadeOut();
								window.setTimeout(function() {
									location.reload()
								}, 1000)
							} else {
								$("#error_message").fadeIn().delay(1000).fadeOut();
								window.setTimeout(function() {
									location.reload()
								}, 1000)

							}
						}

					});
				} else {
					$('#loadernew').hide();
					$('.requestaction').attr('disabled', false);
				}
			}
		} else {
			alert('Please select entry');
			$('.requestaction').attr('disabled', false);
		}

	});

	$(document).on('click', '#site-meetings-grid_c0_all', function() {
		var checked = this.checked;
		$("input[name='ids[]']").each(function() {
			if (checked == true) {
				$(this).find("input[name='ids']").prop('checked', true);
				$(this).parent('span').addClass('checked');
			} else {
				$(this).find("input[name='ids']").prop('checked', false);
				$(this).parent('span').removeClass('checked');
			}

		});
	});
</script>