<h1>Punching Report</h1>

<div id="parent">
   <table border="1">
         <thead>
            <tr class="toplevel">
                <th class="rname"><div style="width:200px;">Resource Name</div></th>
                    <?php

                    for ($i = 0; $i < $interval->days + 1; $i++) {
                        $datehead = new DateTime($date_from);
                        $datehead->add(new DateInterval('P' . $i . 'D'));
                        $labelhead = (string)$datehead->format('d-m-Y');

                    ?>
                     <th class="rtime"><?php echo $labelhead ?></th>
                    <?php } ?>
             </tr>
        </thead>
        <tbody>

            <?php
            if (count($final_result) == 0) {
              echo '<tr><td colspan="2">No Entry found</td></tr>';

            } ?>
            <?php


            $k1 = 0;
            foreach ($final_result as $userid => $result) {
                foreach ($result as $pudate => $fitem) {
                     $m = 0;
                     if ($m == 0) {   ?>

                        <tr class="rowbg_<?php echo $k1 % 2 ?>">
                              <th align="left" width="200" colspan="<?php echo ($interval->days) + 2; ?>">
                                <?php echo $fitem['name']; ?></th>
                        </tr>

                        <?php }

                    $first = '<tr><td width="150" class="left_fix">First Punch</td>';

                    $last = '<tr><td class="left_fix">Last Punch</td>';

                    $in = '<tr><td class="left_fix">IN Time</td>';

                    $out = '<tr><td class="left_fix">Out Time</td>';

                    $tot = '<tr><td class="left_fix">Total Time</td>';

                    $more = '<tr><td class="left_fix">Total Punch:<br/> Missed Punches: </td>';



                    $k1++;

                    $resdate = array_keys($result);

                    for ($i = 0; $i <= $interval->days; $i++) {
                        $date = new DateTime($date_from);
                        $date->add(new DateInterval('P' . $i . 'D'));
                        $caldate = (string)$date->format('d-m-Y');

                        $pundate = (string)$pudate;

                        if (in_array($caldate, $holiday) == 1 && in_array($caldate, $resdate) != 1) {

                            $first .= '<td align="center">H</td>';

                            $last .= '<td align="center">H</td>';

                            $in .= '<td align="center">H</td>';

                            $out .= '<td align="center">H</td>';

                            $tot .= '<td align="center">H</td>';

                            $more .= '<td align="center">H</td>';

                        } else {

                            $nopunch = '<small>No Punch</small>';

                            $first .= '<td>' . ( (in_array($caldate, $resdate) == 1)?$result[$caldate]['first'] : $nopunch) . '</td>';

                            $last .= '<td>' . (in_array($caldate, $resdate) == 1 ? $result[$caldate]['last'] : $nopunch) . '</td>';

                            $in .= '<td>' . (in_array($caldate, $resdate) == 1 ? gmdate('H:i:s',$result[$caldate]['in']): $nopunch) . '</td>';

                            $out .= '<td>' . (in_array($caldate, $resdate) == 1 ? gmdate('H:i:s',$result[$caldate]['out']): $nopunch) . '</td>';

                            $tot .= '<td>' . (in_array($caldate, $resdate) == 1 ? gmdate('H:i:s',$result[$caldate]['tot']) : $nopunch) . '</td>';

                            $more .= '<td>' . (in_array($caldate, $resdate) == 1 ? intval($result[$caldate]['absol_punch']) . ($result[$caldate]['missed_punch'] == 1 ? '<br />1' : '<br/>&nbsp;') : $nopunch) . '</td>';

                        }

                        if ($i == $interval->days) {

                            echo $first .= '</tr>' . "\n";
                            echo $last .= '</tr>' . "\n";
                            echo $in .= '</tr>' . "\n";
                            echo $out .= '</tr>' . "\n";
                            echo $tot .= '</tr>' . "\n";
                            echo $more .= '</tr>' . "\n";

                        }

                         $m++;
                      }
                      break;
                  }
              }

            ?>

        </tbody>

    </table>

</div>

<style>
    #parent{max-height: 500px;}
    table{margin-bottom:0px;border-collapse: collapse;}
    .greytable, .greytable th, .greytable td {
        border-top: 0px;
    }
    .rname{z-index:3 !important}
    .rtime{z-index:2;white-space:nowrap;}
    .left_fix{background-color: #fafafa;}
</style>
