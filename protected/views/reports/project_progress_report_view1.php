
<style>
    body{
        background: url('images/pattern.png');
    }
    </style>

</head>

<body>

    <?php
   
   if($model->report_type==3) 
   {?>
   <div class="project-progree-report-view-one">
     <div class="banner_sec">
                <h2>
                    <span class="ted-txt" style="color: #7c1316;"><?php echo $model->project->name; ?></span>
                    <br>
                    <span class="blue-txt" style="color: #e65100;">PROJECT PROGRESS REPORT</span>
                </h2>
                <div class="text-center">
                    <h5><span class="blue-txt" style="font-size: 1.5rem;">As on <b><?= date('d-M-Y', strtotime($model->created_date)) ?></b></span></h5>
                </div>
                <div style="max-width:100%;overflow:hidden;">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" style="background: transparent;">
                        <tr>
                            <?php
                            if ($model->project->report_image != '') {
                            ?>
                            <td style="height:800px;overflow:hidden;text-align:center;" valign="middle">
                                <?php
                                if ($model->project->report_image != '') {
                                    ?>
                                 
     
                                    <?php
                                    $path = realpath(Yii::app()->basePath . '/../uploads/project/report_imgs/' . $model->project->report_image);
                                    if ($pdf==1) {
                                        
                                ?>
                                      
                                        <img class="img_click" src="<?php echo $path; ?>" alt="Project Image"  width="250" height="300" 
     height="700">
                                <?php
                                    }else{?>
                                        <img class="" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $model->project->report_image; ?>" alt="Project Image">
                      <?php
                      }
                    }
                                ?>
                            </td>
                            <?php }?>
                        </tr>
                    </table>

                    <!-- <img class="img_click" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/" . $model->project->img_path; ?>" alt="..." style=" border:3px solid #efa12e;height:768px;"> -->

                </div>

            </div>
            <div class="banner_sec">
            <h2><span class="blue-txt" style="color: #e65100;"><u>ONGOING WORK</u></span></h2>
</div>

<div class="newspaper">
<ul class="bulletin">
 <?php  
 foreach($project_tasks as $tasks)
 {
     if($tasks['parent_id']==NULL){?>
 <li><h3><?php echo $tasks['title']." - ".$tasks['contractor_name']?></h3></li>
 <?php
     }
 }
 ?>
</ul>
</div>

   <div class="banner_sec">
       
   <?php foreach($project_tasks as $tasks)
   {
    $area_details= $this->getAreaDetails($tasks['id']);
   
    $work_type= $this->getworkType($tasks['id']);
  
     if($tasks['parent_id']==NULL && count($area_details)>0 && count($work_type)>0){
         //
         ?>
       
        <h2><span class="blue-txt" style="color: #e65100;"><u>Working progress-<?php echo $tasks['title']?></u></span></h2>
       
       <!-- <table style="width: 100%; margin-top: 10px; font-size: 0.8em;" border="1" cellspacing="0"> -->
       <table  width="100%" cellspacing="0" cellpadding="0" style="background: transparent;">
       <thead>
    <tr align="center" >
        <th style="padding:2.5px; width: 10px;" rowspan="2">Area</th>     
      
        <th style="padding:2.5px; text-align:center;" colspan="<?php echo count($work_type)?>">Nature of work</th>
        
    </tr>
    <tr>
        <?php     
        foreach($work_type as $key=>$type)
        {           
            ?>
       <th ><?php echo $type['work_type']?></th>
       <?php
        }
         
        ?>       
        
       
    </tr>
       </thead>
       <tbody>
          
               <?php 
               
               foreach($area_details as $area){
                   ?>
                <tr>
                   <td>
             <?php echo $area['area_title'];?>
               </td>
               <?php              
            ?>              
            <?php
            for($j=0;$j<count($work_type);$j++)
            {
            foreach($work_type as $type1)
            {
            
                $getTotalWorkProgress=$this->getTotalWorkProgress($area['tskid'],$type1['wtid'],$area['id'],$tasks['id']);
                ?>
               
          <td><?php echo $getTotalWorkProgress."%"?></td>
            <?}
            break;
            }
        }  
            ?>
            </tr>
               <?
               
               ?>
               </tbody>
</table>

<div class="banner_sec">
   <?php
   $getImages=$this->getImages($tasks['id']);   
   if(count($getImages)>0)
   {?>
 
 <h2><span class="blue-txt" style="color: #e65100;"><u><?php echo $tasks['title']. " work in progress pictures from site"?></u></span></h2>
 <?php 
 foreach($getImages as $images)
 { 
     $res = str_replace( array( '\'', '"',
    ',' , '[', ']', '>' ), '', $images['img_paths']);
    $path = realpath(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $res);
    if($pdf==1)
    {
       ?>
       <td style="padding-right: 200px">
      <img class="img_click" src="<?php echo $path; ?>" alt="Project Image"  width="100" 
     height="100">
     </td>
   <?php }else{
     ?>
      <td style="padding-right: 200px">
      <img class="img_click" src="<?php echo Yii::app()->request->baseUrl . "/uploads/dailywork_progress/" .$res; ?>" alt="Project Image"  width="250" 
     height="250">
     
     <span class="caption" style="font-weight:bold"><?= isset($images['image_label'])?$images['image_label']:'' ?></span>
     </td> 
   
    
<?php
   }
 }
 ?>
 </div>
    <?php
   }
   ?> 
  <?php
 }
 ?>
 </div>
  
  
 
 

 
  
 <?php  }
  ?>
  </div>
  
   <?php }//closing of  if($model->report_type==3) 
   ?>