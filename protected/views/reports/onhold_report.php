<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>


<div id="msg"></div>

<h1>OnHold Report</h1>

<div class="form custom-form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="col-md-2">
            <?php
            if ($model->project_id == "") {
                if (Yii::app()->user->project_id != "") {
                    $model->project_id = Yii::app()->user->project_id;
                } else {
                    $model->project_id = "";
                }
            }
            echo $form->dropDownList(
                $model,
                'project_id',
                CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
                array(
                    'empty' => 'Choose a project',
                    'class' => 'form-control input-medium select_drop project change_project',

                )
            );
            ?>
        </div>



        <div class="col-md-2">
            <?php echo CHtml::Button('Generate', array('class' => 'btn btn-sm blue submit')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div>
</div>
<?php

if (isset($_GET['TaskLog']['project_id'])) {

    $data = $model->onHoldReport($_GET['TaskLog']['project_id']);
    $count = count($data);
} else {
    $data = $model->onHoldReport($id = "");
    $count = count($data);
}

?>

<br>

<table class="table table-striped table-bordered weekly-report-table">
    <thead>
        <th>SL NO</th>
        <th>TASK</th>
        <th>TASK TYPE</th>
        <th>BALANCE QUANTITY</th>
        <th>REASON FOR HOLD</th>
        <th>FROM</th>
        <th>To</th>
        <th>NO.OF DAYS</th>
    </thead>
    <?php
    if ($count == 0) {
    ?>
        <td rowspan="7">No results found</td>
        <?php
    } else {
        $i = 1;
        foreach ($data as $task) {
            $used_qty = $this->getTaskBalanceQty($task['task_id'],$task['task_type']);
            if ($used_qty == "") {
                $used_qty = 0;
            }

            $quantity = $task['quantity'];

            $balance_quantity = "";

            

            if($task['task_type']==1)
            {
                if ($quantity != "") {
                    $balance_quantity = $quantity - $used_qty;
                }
                $type="Internal";
            }
            else
            {
                
                if ($quantity != "") {
                    $balance_quantity = 100 - $used_qty ." %";
                } 
                $type = "External";
            }

            if ($task['status'] == 73) {
                $status = "On Hold Client";
            } else if ($task['status'] == 74) {
                $status = "On Hold Other";
            } else {
                $status = "On Hold Contractor";
            }

            $date1 = new DateTime($task['hold_date']);
            $date2 = new DateTime($task['active_date']);
            $interval = $date1->diff($date2);
             ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $task['task_name']?></td>
                <td><?php echo $type ?></td>
                <td><?php echo  $balance_quantity ?></td>
                <td><?php echo $status ?></td>
                <td><?php echo date("d-M-Y", strtotime($task['hold_date'])) ?></td>
                <td><?php echo date("d-M-Y", strtotime($task['active_date'])) ?></td>
                <td><?php echo $interval->d ?></td>
            </tr>
    <?php
            $i++;
        }
    }
    ?>
</table>





<script>
    jQuery(function($) {
        $('.project').change(function() {
            var val = $(this).val();
            if (val != '') {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('tasks/getlocation'); ?>',
                    data: {
                        project_id: val
                    },
                    dataType: 'json',
                    method: "GET",
                    success: function(result) {
                        $('.clientsite').html(result);
                    }

                })
            }
        });

        $('.submit').click(function() {
            var project_id = $('.project').val();
            if (project_id == "") {
                $('.grid-view').hide()
                $('#msg').html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Please choose a project.</div>');
            } else {
                $('#yw0').submit();
            }





        });
    });
</script>