<?php
/* @var $this SiteMeetingsController */
/* @var $model SiteMeetings */
Yii::app()->getModule('masters');
$this->breadcrumbs=array(
	'Site Meetings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SiteMeetings', 'url'=>array('index')),
	array('label'=>'Create SiteMeetings', 'url'=>array('create')),
	array('label'=>'Update SiteMeetings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SiteMeetings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SiteMeetings', 'url'=>array('admin')),
);
?>

<h1>Minutes of meeting – Site Meeting-<?php echo $model->id; ?></h1>
<div clss="row">
	<p>Date and Venue of
	meeting: <?php echo date('F d, Y',strtotime($model->date)).' at '. $model->venue; ?><p>
	<p>Present: </p>
	<?php
	if(!empty($participant_model)){
		echo '<ul>';
		foreach($participant_model as $participants){
			if(!empty($participants->participants)){
				echo '<li>'.$participants->participants.'('.$participants->contractor->contractor_title.')</li>';
			}else{
				echo '<li> Absent ('.$participants->contractor->contractor_title.')</li>';
			}
			
		}
		echo '</ul>';
	}
	?>
	<p>1. Objective of Meeting: </p>
	<div>
		<?php echo $model->objective?>
	</div>
	<p>2. Meeting Notes, Decisions, Issues</p>
	<div>
		<table>
			<thead>
				<tr class="">
					<th width="150">Sl no</th>
					<th width="550">Points Discussed</th>
					<th width="200">Action By</th>
					<th width="200">Date of completion/submission</th>
					<th width="400">Status/Remarks</th>           
				</tr>				
			</thead>
			<?php
			if(!empty($minutes_data)){
				$i = 1;
				foreach($minutes_data as $data){ ?>
					<tr>
					<td width="150"><?= $i ?></td>
					<td width="550"><?= $data->points_discussed ?></td>
					<td width="200"><?= $data->actionBy->contractor_title ?></td>
					<td width="200"><?= $data->submission_date ?></td>
					<td width="400"><?= $data->status ?></td> 
					</tr>

				<?php 
				$i++;
				}
			}
			
			?>
		</table>		
	</div>
</div>
