<div class="consolidated-report">
<div class="row">
    <h3>Project consolidated Report</h3>
    <table class="table-bordered">
        <thead>
            <tr>
                <th>SL No</th>
                <th>Milestone</th>
                <th>Contractor</th>
                <th>Progress %</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $report_datas = !empty($model->report_data) ? json_decode($model->report_data, true) : '';
            $i = 1;
            foreach ($report_datas as $report_data) {
            ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $report_data['milestone_title'] ?></td>
                    <td><?= $report_data['contractor_title'] ?></td>
                    <td><?= $report_data['progress_perc'] ?></td>
                    <td><?= $report_data['status'] ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>
        </div>
