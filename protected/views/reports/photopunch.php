<!-- <script
 
src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous">
</script> -->
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-3.4.1.min.js','/js/jquery-3.4.1.min.js');?>">
    </script>
<?php
/* @var $this ClientsController */
/* @var $model Clients */


$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contact-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>

<?php $this->endWidget(); ?>
<div class="photo-punch-sec">
<div class="clearfix">
    <div class="pull-left">
        <h1>Photo Punch Report</h1>
</div>
<div class="pull-right">
        <?php
        $date=date("Y-m-d",strtotime("-45 days"));
      
		$date='"'.$date.'"';
    $sql = 'select `approved_status`, '
                . 'count(approved_status) as itemcount '
                . 'from pms_photopunch '
                .'where Date(log_time) >= '.$date
                . 'group by `approved_status`';
        $status_count = CHtml::listData(Yii::app()->db->createCommand($sql)->queryAll(), 'approved_status', 'itemcount');
        $filterarray = array('All' => 'All', 'Approved' => 'Approved', 'Pending' => 'Pending', 'Rejected' => 'Rejected');
        $statusarray = array('All' => 'All', 1 => 'Approved', 0 => 'Pending', 2 => 'Rejected');

        echo '<div class="nav-div h-100"><ul class="nav1 nav">';
        $activeitem = ((!isset($_GET['type']) or $_GET['type'] == 'All') ? 'All' : intval($_GET['type']));

        $status_count['All'] = array_sum($status_count);

       
        $statusbgcolor = array('Red'=>'ALL','green' => 1, 'blue' => 0, '#715d13' => 2);
        foreach ($statusarray as $k => $status) {
            echo '<li class="nav-item">';
            echo CHtml::link($status
                    . '&nbsp;&nbsp;<span class="badge" style="background-color: ' . array_search($k, $statusbgcolor) . '">'
                    . (isset($status_count[$k]) ? $status_count[$k] : 0)
                    . '</span>', array('/reports/photoPunchReport', 'type' => $k),
                    array('class' => "nav-link border-radius-5" . ($k === $activeitem ? ' active ' : ''), 'aria-disabled' => "true"));
            echo '</li>';
        }
        echo '</ul></div>';
        ?>
    </div>
</div> 
 
<div class="approve_form">
    <div>
        <?php //if (in_array('/approvals/attendanceprivileges', Yii::app()->session['pmsmenuauthlist'])) { ?>
            <textarea name="reason" class="reason"></textarea> 
            <input type="button" name='approve' class='requestaction btn btn-success' value='Approve' /> &nbsp;
            <input type="button" class='requestaction btn btn-danger' name='reject' value='Reject' />
            <img id="loadernew" src="/images/loading.gif" class="width-30 margin-left-159 display-none" />
            <?php
            echo CHtml::image(Yii::app()->request->baseUrl . '/images/loading.gif',
                    'Loading....', array('class' => 'width-30 margin-left-159 display-none'));
            ?>
        <?php //} ?>
    </div>


    </div>
    <div id="approve-div" class="approve_div"></div> 

    <br>
<!-- ////////// date filter //////// -->
<div class="clearfix table-filter">

<div class="prev_curr_next pull-left sec_h">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'page-form',
            'method' => 'GET',
            'enableAjaxValidation' => true,
            'htmlOptions' => array('class' => 'form-inline margin-bottom-8')
        )); ?>
        <div class="form-group datepick">						
      

<label>Start Date</label>

<?php echo CHtml::textField('sdate', (isset($date_from) ? date('d-m-Y', strtotime($date_from)) : ''), array("id" => "sdate", 'class' => 'width-100 height-28', 'autocomplete' => 'off', 'readonly' => false)); ?>


<label>End Date</label>

<?php echo CHtml::textField('edate', (isset($datetill) ? date('d-m-Y', strtotime($datetill)) : ''), array("id" => "edate", 'class' => 'width-100 height-28', 'autocomplete' => 'off', 'readonly' => false)); ?>


<?php $this->widget('application.extensions.calendar.SCalendar', array(  'inputField' => 'sdate',  'button' => 'sdate',  'ifFormat' => '%d-%m-%Y',  ));?>

<?php  $this->widget('application.extensions.calendar.SCalendar', array('inputField' => 'edate','button' => 'edate',
   'ifFormat' => '%d-%m-%Y', ));  ?>
						
					</div>
      
        <div class="form-group min-btn">
            <?php echo CHtml::submitButton('Go', array('class' => 'btn blue btn-sm')); ?>
           
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<!-- /////////////////// -->    
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'photo-punch',
    'dataProvider' => $model->search($status_type),
    'ajaxUpdate' => false,
    'itemsCssClass' => 'table table-bordered photo-punch-table',
    'filter' => $model,
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),  
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
            'value' => '$data->aid',
           //  'htmlOptions'=>array('class'=>'test'.$data->aid),
         
            'checkBoxHtmlOptions' => array(
                'name' => 'ids[]',
                'class'=> 'checkbox',
            ),
          'cssClassExpression' => '( $data->approved_status == 0 && $data->work_site_id!="")? "" : "hiden" ',

        ),
        array(
            'header' => 'S.No.',           
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{approve_reject}',
            'buttons' => array(
                'approve_reject' => array(
                    'label' => 'Approve/Reject',
                    'visible' => '$data->status!=0 ',
                    'options'=>array("class"=>'status_button'),
                ),
            ),
        ),
        array(
            'value' => '$data->aid',
            'headerHtmlOptions' => array('class' => 'display-none'),
            'filterHtmlOptions' => array('class' => 'display-none'),
            'htmlOptions' => array('class'=>'hidden_aid display-none'),
        ),       
        //    array('name'=>'cid', 'htmlOptions' => array('width' => '40px','style'=>'font-weight: bold;text-align:center')), 
        array(
            'name' => 'empid',
            'header' => 'Employee Name',
            'value' => '$data->usrId->first_name." " .$data->usrId->last_name ',
            'type' => 'raw',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid', 'concat(first_name," ",last_name) as first_name'),
                    'order' => 'first_name',
                   
                    'distinct' => true
                )
        ), "first_name", "first_name")
        ),
        array(
            'name' => 'log_time',
            'header' => 'Punch Time',
            'value' => 'date("d-m-y h:i:s a",strtotime($data->log_time))',
            'type' => 'raw',   
        ),
        array(
            'name' => 'latitude',
            'value' => '$data->latitude',
            'type' => 'raw',
        ),
        array(
            'name' => 'longitude',
            'value' => '$data->longitude',
            'type' => 'raw',
        ),
        array(
            'name' => 'work_site_name',
            'header' => 'Site',
            'value' => '($data->work_site_name!="")?$data->work_site_name:"No site"',
            'type' => 'raw',
        ),
        array(
            'name' => 'warning_message',
            'header' => 'Warning',
            'value' => '$data->warning_message',
            'type' => 'raw',
        ),
        array(
            'name' => 'punch_type',
            'header' => 'Punch Type',
            'value' => '($data->punch_type==1)?"In":"Out"',
            'type' => 'raw',
        ),
        array(
            'name' => 'approved_status',
            'header' => 'Status',
            'value' => '"<div id=\'approved_status_".$data->aid."\'>".(($data->approved_status==0) ? "<span style=color:orange>Pending</span>": (($data->approved_status ==1)? "<span style=color:green>Approved </span>" : "<span style=color:red> Rejected </span>"))."</div>"',
            'type' => 'raw',
            'filter'=>array("Pending", "Approved", "Rejected"),
            'htmlOptions'=>array('class'=>'photo_punch_status')
        ),
        array(
            'name' => 'image',
            'type' => 'raw',
            'value'=> '$data->findphotoimage($data->image)',            
            
            //	'value'=>'(file_exists(dirname(Yii::app()->basePath).'/'.$data->image))?CHtml::image($data->image,"",array("style"=>"width:100px;height:100px;")):"no image found"',             
//            'value' => '"/uploads/photopunch/".basename($data->image)."<br />".(file_exists(Yii::app()->basePath '
//            . '. \'/../\'."/uploads/photopunch/".basename($data->image))?CHtml::image($data->image,"",array("style"=>"width:100px;height:100px;")):"no image found")',
        ),
        
    /*
      'description',
      'created_date',
      'created_by',
      'updated_date',
      'updated_by',
     */
    ),
));
?>


</div>
</div>
<script>

$(document).ready(function(){

   
        $("input[name='Photopunch[log_time]']").datepicker({dateFormat: 'yy-mm-dd'});
        $('.hasDatepicker').attr('autocomplete', 'off');
    
$('.hiden').children().addClass('hidden_checkbox');
$('.hidden_checkbox').remove();
    
})


$(".requestaction").click(function () {

$('.requestaction').attr('disabled', true);

var req = $(this).val();
var reason = $('.reason').val();
var all = [];
var data_type = [];

$('input[name="ids[]"]:checked').each(function () {

    $id_type = (this.value).split('#');
//            alert($id_type[1]);
//            die;
    all.push($id_type[0]);
    data_type.push($id_type[1])
});

if (all != '') {
    if (reason == '') {
        alert("please add comment");
        $('.requestaction').attr('disabled', false);
    } else {

        if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

            $('#loadernew').show();

            $.ajax({
                method: "post",
                dataType: "json",
                data: {id: all, req: req, reason: reason, data_type: data_type},
                url: '<?php echo Yii::app()->createUrl("/reports/photoPunchReportAction") ?>',

                //}).done(function(ret){
                success: function (ret) {

                    $('#loadernew').hide();
                    $('.requestaction').attr('disabled', false);

                    if (ret.error != '') {
                        $('#actionmsg').addClass('alert alert-danger');
                        $('#actionmsg').html(ret.error);
                        window.setTimeout(function () {
                            location.reload()
                        }, 1000)
                    } else {
                        $('#actionmsg').addClass('alert alert-success');
                        $('#actionmsg').html(ret.msg);
                        window.setTimeout(function () {
                            location.reload()
                        }, 1000)

                    }
                }

            });
        } else {
            $('#loadernew').hide();
            $('.requestaction').attr('disabled', false);
        }
    }
} else {
    alert('Please select entry');
    $('.requestaction').attr('disabled', false);
}
});

$(document).on('click','#photo-punch_c0_all',function() {
	var checked=this.checked;
	 $("input[name='ids[]']").each(function() {
         if(checked==true)
         {
          $(this).find("input[name='ids']").prop('checked', true);
          $(this).parent('span').addClass('checked');
         }
         else{
            $(this).find("input[name='ids']").prop('checked', false);
          $(this).parent('span').removeClass('checked');
         }
         
    });
});

$(".status_button").click(function(e){
    e.preventDefault();
    // $('html,body').animate({
    //     scrollTop: $(".approve_div").offset().top},
    //     'slow');
   
   var id = $(this).closest('tr').find('.hidden_aid').text();
   if(id !=''){
    $.ajax({
        type:'POST',
            url:'<?php echo Yii::app()->createUrl('/PhotoPunch/default/view'); ?>',
            data:{id:id},
            success:function(data) {
                $(".approve_div").html(data);
                $('.punch_details').animate({height: 'show'}, 290);
            }
    })
   }
   
})
function closeformnoti(formname){
    $('.punch_details').animate({height: 'hide'}, 290);
    }

    $('.photo-punch-table').wrap('<div class="table-responsive"></div>')
</script>
<script>
$(function(){
    $('.approve_form').hide();    
    $("table input[type=checkbox]").click(function(){ 
        var count= $("table tbody input[type=checkbox]").length;
    var countchecked = $("table tbody input[type=checkbox]:checked").length;        
        if(countchecked >=1){
            $('.approve_form').slideDown();
        }
        else if(countchecked ==0){
            $('.approve_form').slideUp();
        }
        if(countchecked==count){          
            $('table thead tr th:first-child').find('span').addClass('checked');
        }else{
            $('table thead tr th:first-child').find('span').removeClass('checked');
        }
    });

    $("table thead  input[type=checkbox]").click(function(){
        $(this).parent('span').toggleClass('checked');
        if ($(this).parent('span').hasClass('checked')) {
            $('.approve_form').slideDown();
        }
        else {
            $('.approve_form').slideUp();
        }
    })   
})
</script>







