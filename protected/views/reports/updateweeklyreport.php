<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<?php
/* @var $this ProjectsController */
/* @var $model Projects */

//$this->breadcrumbs = array(
//     'Projects' => array('index'),
//     $model->name => array('view', 'id' => $model->pid),
//     'Update',
// );
?>

<h2>Update Weekly Report : <span>
        <?php //echo $model->name; ?>
    </span></h2>
<?php

?>
<?php echo $this->renderPartial(
    'generateweeklyreport',
    array(
        'model' => $model,
        'type' => true,
        'design_model' => $design_model,
        'existing_design' => $existing_design,
        'weekly_report_images' => $weekly_report_images,
        'existing_weekly_report_images' => $existing_weekly_report_images,
        'present_status' => $present_status,
        'completed_task_array' => $key_achievements,
        'pending_payment_data' => $pending_payment_data,
        'advice_payment_data' => $advice_payment_data,
        'weekly_report_update' => true
    )
); ?>
<script>
    // $(document).ready(function () {
        // Handle the file input change
        $('input[type="file"].image_path').on('change', function () {
            var fileInput = $(this);
            var existingImagePath = fileInput.siblings('input[type="hidden"]').val();

            if (fileInput[0].files.length === 0) {
                // If no new file is selected, retain the existing image path
                fileInput.val(existingImagePath);
            } else {
                // Remove the hidden input value if a new file is selected
                fileInput.siblings('input[type="hidden"]').val('');
            }
        });
    // });

</script>