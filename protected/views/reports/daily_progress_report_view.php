<style>
  body {
    text-align: center;
    background: white;
    font-size: 14px;
  }

  .font-10 {
    font-size: 10px;
  }

  .font-12 {
    font-size: 12px;
  }

  table,
  th,
  td,
  tr {
    border-collapse: collapse;
    border: 1px solid black;
  }

  th {
    background-color: #055C9D;
    color: white;
  }

  .table-div {
    float: left;
    width: 48%;
    margin-bottom: 10px;
    border: 1px solid black;
    padding-top: 10px;
    padding-bottom: 10px;
    height: 210px;
  }

  .w-100 {
    width: 100%;
  }

  .width-10 {
    width: 10px;
  }

  .qrcode-width {
    width: 80px;
  }

  .border-bottom-none {
    border-bottom: none;
  }

  .border-bottom-solid {
    border-bottom: 1px solid;
  }

  .border-top-none {
    border-top: none;
  }

  .border-top-solid {
    border-top: 1px solid;
  }

  .border-left-none {
    border-left: none;
  }

  .border-left-solid {
    border-left: 1px solid;
  }

  .border-right-none {
    border-right: none;
  }

  .border-right-solid {
    border-right: 1px solid;
  }

  .text-center {
    text-align: center;
  }

  .whitespace-nowrap {
    white-space: nowrap;
  }

  .wpr_images {
    width: 90%;
    display: block;
    margin: 20px auto;
  }
</style>

<div class="scrollit">
  <table class="table table-striped table-bordered e">
    <thead>
      <tr>
        <th colspan="13" align="center">KEY ACTIVITIES & PROGRESS OF WORK (2 WEEKS)</th>
      </tr>
      <tr>
        <th rowspan="2">SL NO</th>
        <th rowspan="2">Task</th>
        <th rowspan="2">Work head</th>
        <th rowspan="2">Work description</th>
        <th colspan="2">scheduled</th>
        <th colspan="2">Actual</th>
        <th rowspan="2">unit</th>
        <th colspan="2">Quantity</th>
        <th rowspan="2">% completed</th>
        <th rowspan="2">Remarks</th>
      </tr>
      <tr>
        <th>Start date</th>
        <th>End date</th>
        <th>Start date</th>
        <th>End date</th>
        <th>Estimated</th>
        <th>Achieved</th>
      </tr>
    </thead>
    <tbody id="myTable">
      <?php
      if (count($data) == 0) { ?>
        <tr>
          <td colspan="13">No data found</td>
        </tr>
      <?php } else {
        foreach ($data as $key => $value) {
          $unit_name = "";
          $actual_end_date = "";
          $worktype = "";
          $progress_percent = "";
          $actual_start_date = "";
          if (!empty($value['unit'])) {
            $unit = Unit::model()->findByPk($value['unit']);
            $unit_name = $unit->unit_code;
          }

          if ($value['quantity'] != "") {
            $progress_percent = ($value['total_quantity'] / $value['quantity']) * 100;
          }

          $work_type = WorkType::model()->findByPk($value['work_type_id']);
          if ($work_type) {
            $worktype = $work_type->work_type;
          }

          if ($progress_percent >= 100) {
            $actual_end_date = date("d-m-Y", strtotime($value['max_date']));
          }

          if ($value['min_date']) {
            $actual_start_date = date("d-m-Y", strtotime($value['min_date']));
          }
          ?>
          <tr>
            <td>
              <?= $key + 1; ?>
            </td>
            <td>
              <?= $value['title'] ?>
            </td>
            <td>
              <?= $worktype; ?>
            </td>
            <td>
              <?= $value['description']; ?>
            </td>
            <td>
              <?= date("d-m-Y", strtotime($value['start_date'])); ?>
            </td>
            <td>
              <?= date("d-m-Y", strtotime($value['end_date'])); ?>
            </td>
            <td>
              <?= $actual_start_date ?>
            </td>
            <td>
              <?= $actual_end_date ?>
            </td>
            <td>
              <?= $unit_name ?>
            </td>
            <td>
              <?= $value['quantity'] ?>
            </td>
            <td>
              <?= $value['total_quantity'] ?>
            </td>
            <td>
              <?= round($progress_percent, 2); ?>
            </td>
            <td></td>
          </tr>

        <?php }
      } ?>
    </tbody>

    <tbody>
      <tr>
        <th colspan="13" align="center">VISITORS TO SITE</th>
      </tr>

      <?php
      if (count($visitors) == 0) { ?>
        <tr>
          <td colspan="13">No data found</td>
        </tr>
      <?php } else {
        foreach ($visitors as $visitor) {
          ?>
          <tr>
            <td colspan="13">
              <?= $visitor->visitor_name . " - " . (isset($visitor->tasks) ? $visitor->tasks->title : "") ?>
         </tr>
      <?php }
      }
      if (count($test_inspection) == 0) { ?>
        <tr>
          <td colspan="13">No data found</td>
        </tr>
      <?php } else {
        foreach ($test_inspection as $inspection) {
          ?>
          <tr>
            <td colspan="13">
              <?= $inspection->inspection . " - " . (isset($visitor->tasks) ? $visitor->tasks->title : "") ?>
            </td>
          </tr>
        <?php }
      } ?>
    </tbody>

    <tbody>
      <tr>
        <th colspan="13" align="center">ACCIDENTS, INJURIES, OR UNUSUAL EVENTS</th>
      </tr>
      <?php
      if (count($incident) == 0) { ?>
        <tr>
          <td colspan="13">No data found</td>
        </tr>

      <?php } else {
        foreach ($incident as $incident) {
          if ($incident->incident == 1) {
            $incident = "Accidents";
          } else if ($incident->incident == 2) {
            $incident = "Injuries";
          } else if ($incident->incident == 3) {
            $incident = "Unusual Events";
          }
          ?>
          <tr>
            <td colspan="13">
              <?= $incident . " - " . (isset($visitor->tasks) ? $visitor->tasks->title : "") ?>
            </td>
          </tr>
        <?php }
      } ?>
    </tbody>
    <tbody>
      <tr>
        <th colspan="13" align="center">PHOTOGRAPHS</th>
      </tr>
      <?php
      if (count($images) == 0) { ?>
        <tr>
          <td colspan="13">No data found</td>
        </tr>
      <?php } else {
        foreach ($images as $value) {
          $file_name = $value['image_path'];
          $path = ('uploads/dailywork_progress/' . $file_name);
          ?>
          <tr>
              <td colspan="13" style="text-align: center;"><img src="<?= 'uploads/dailywork_progress/' . $file_name ?>" class="wpr_images" title="<?= $value['image_label'] ?>" /></td>
          </tr>
        <?php }
      } ?>
    </tbody>

  </table>
</div>