<head>
    <style>
        
        .blue-txt {
            text-align: center;
            text-decoration: underline;
        }

        .pull-rights {
            float: right;
            margin-bottom: -45px;
        }

        table {
            font-family: times, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 16px;
        }

        td,
        th {
            border: 1px solid #000000;
            text-align: left;
            padding: 0px !important;
            font-size: 17px !important;
        }

        .second-table td,
        .second-table th {
            padding: 8px !important;

        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left !important;
        }

        .text-bold {
            font-weight: 700;
        }

        .pl-8 {
            padding-left: 8px !important;
        }

        .underline {
            text-decoration: underline;
        }

        .w-90 {
            width: 90%;
        }

        .w-10 {
            width: 10%;
        }

        .w-35 {
            width: 35%;
        }

        .w-15 {
            width: 15%;
        }

        .left-border-none {
            border-left: none;
        }

        .right-border-none {
            border-right: none;
        }

        .bottom-border-none {
            border-bottom: none;
        }

        .top-border-none {
            border-top: none;
        }

        .left-border-only {
            border-top: none;
            border-bottom: none;
            border-right: none;
        }

        .right-border-only {
            border-left: none;
            border-top: none;
            border-bottom: none;
        }

        .bottom-border-only {
            border-top: none;
            border-left: none;
            border-right: none;
        }

        .top-border-only {
            border-left: none;
            border-right: none;
            border-bottom: none;
        }

        .no-border {
            border: none;
        }

        .x-padding {
            padding: 8px 0px;
        }

        .no-padding {
            padding: 0px !important;
        }

        .second-table tr,
        .second-table tr td {
            background: #c4daf2;
        }

        .third-table tr td,
        .fourth-table tr td {
            text-align: center;
        }

        .third-table .header,
        .third-table .header td,
        .fourth-table .header,
        .fourth-table .header td {
            background: #dadada !important;
            font-weight: bold !important;
            color: black;
        }

        .add-task-button {
            background-color: #17a2b8;
            border: none;
            color: white;
            padding: 1px 10px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            /* font-weight: bold; */
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 6px;
        }
    </style>
</head>
<div class="minutes-view-sec">
    <div class="wrapper">
        <?php
        $condition = 'status = 1 ';
        if (!empty($model->project_id)) {
            $condition .= 'AND project_id = ' . $model->project_id;
        }
        $project_image = Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $project->report_image;
        $logo = Yii::app()->request->baseUrl . "/images/logo.png";
        ?>
        <div>
            <tr>
                <?php if ($project->report_image != '') { ?>
                    <td>
                        <img class="pull-left" src="<?php echo $project_image ?>" alt=" " height="50px">
                    </td>
                <?php } ?>
                <td>
                    <img class="pull-rights" src="<?php echo $this->logo ?>" alt="" height="50px">
                </td>

            </tr>
        </div>
        <br>
        <tr>
            <th colspan="5">
                <u>
                    <h2 class="blue-txt">MINUTES OF MEETING</h2>
                </u>
            </th>
        </tr>
        <table>

            <tr>
                <td class="no-padding">
                    <table class="second-table">
                        <tr>
                            <td class="w-15 ">Project Name:</td>
                            <td class="w-35">
                                <?= $project->name ?>
                            </td>
                            <td class="w-15">Client Name:</td>
                            <td class="w-35">
                                <?= isset($project->client) ? $project->client->name : "N/A" ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="w-15">Location:</td>
                            <td class="w-35">
                                <?= isset($model->site) ? $model->site->site_name : "N/A" ?>
                            </td>
                            <td class="w-15">Meeting ref:</td>
                            <td class="w-35">
                                <?= isset($model->meeting_ref) ? $model->meeting_ref : "N/A" ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="w-15">Project Code:</td>
                            <td class="w-35">
                                <?= isset($project->project_no) ? $project->project_no : "N/A" ?>
                            </td>
                            <td class="w-15">Purpose:</td>
                            <td class="w-35">
                                <?= isset($model->meeting_title) ? $model->meeting_title : "N/A" ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="w-15">Date, time&venue:</td>
                            <td class="w-35">
                                <?= date("d-m-Y", strtotime($model->date)) . "," . date("g:i A", strtotime($model->meeting_time)) . "," . $model->venue ?>
                            </td>
                            <td class="w-15">MOM no:</td>
                            <td class="w-35">
                                <?= isset($model->meeting_number) ? $model->meeting_number : "N/A" ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="x-padding"></td>
            </tr>
            <tr>
                <td class="no-padding">
                    <table class="third-table">
                        <tr class="header">
                            <td>SL NO</td>
                            <td>NAME</td>
                            <td>INITIALS</td>
                            <td>DESIGNATION</td>
                            <td>COMPANY/<br />ORGANIZATION</td>
                            <td>INITIAL<br />COMPANY</td>
                        </tr>
                        <?php foreach ($participants as $key => $participant) { ?>
                            <tr>
                                <td>
                                    <?= $key + 1 ?>
                                </td>
                                <td>
                                    <?= $participant->participants ?>
                                </td>
                                <td>
                                    <?= $participant->participant_initial ?>
                                </td>
                                <td>
                                    <?= $participant->designation ?>
                                </td>
                                <td>
                                    <?= $participant->organization_name ?>
                                </td>
                                <td>
                                    <?= $participant->organization_initial ?>
                                </td>
                            </tr>
                        <?php } ?>

                    </table>
                </td>
            </tr>
            <tr>
                <td class="x-padding"></td>
            </tr>

            <tr>
                <td class="no-padding">
                    <table class="fourth-table">
                        <tr class="header">
                            <td>SL NO</td>
                            <td>POINTS DISCUSSED</td>
                            <td>
                                STATUS (INFO/ <br />
                                DECISION<br />
                                ACTION)
                            </td>
                            <td>OWNER</td>
                            <td>DUE DATE</td>
                            <td>TASKS</td>

                        </tr>
                        <?php foreach ($minutes_points as $key => $minutes_point) { ?>
                            <tr>
                                <td>
                                    <?= $key + 1 ?>
                                </td>
                                <td class="text-left pl-8">
                                    <?= $minutes_point->points_discussed ?>
                                </td>

                                <?php
                                $sts = $minutes_point->meeting_minutes_status;
                                if ($sts == 1) {
                                    $status = "Info";
                                } else if ($sts == 2) {
                                    $status = "Action";
                                } else if ($sts == 3) {
                                    $status = "Decision";
                                } else {
                                    $status = "";
                                }
                                ?>
                                <td>
                                    <?= $status ?>
                                </td>
                                <td>
                                    <?= isset($minutes_point->meetingMinutesUsers->user) ? $minutes_point->meetingMinutesUsers->user->first_name . ' ' . $minutes_point->meetingMinutesUsers->user->last_name : ''; ?>

                                </td>
                                <td>
                                    <?= date("d-m-Y", strtotime($minutes_point->end_date)) ?>
                                </td>
                                <td>
                                    <?php if ($minutes_point->task_id && isset($minutes_point->tasks->title)) {
                                        echo $minutes_point->tasks->title;
                                    } else { ?>
                                        <a href="#" class="add_task_modal add-task-button" type="button" data-toggle="modal"
                                            data-target="#addTask" data-id="<?= ($minutes_point->id) ?>">Add
                                            Task</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
            </tr>

        </table>
        <table>
            <tr>
                <td class="no-border text-bold underline">Minutes Recorded By</td>
            </tr>
            <tr>
                <td class="no-border">
                    <?= $model->created->first_name . " " . $model->created->last_name ?>
                </td>
            </tr>
            <tr>
                <td class="no-border text-bold underline">Distribution</td>
            </tr>
            <tr>
                <td class="no-border">
                    <?= $model->distribution?$model->distribution:"N/A"; ?>
                </td>
            </tr>
            <tr>
                <td class="no-border text-bold underline">Date of next meeting</td>
            </tr>
            <tr>
                <td class="no-border">
                    <?= $model->next_meeting_date? date("d-m-Y", strtotime($model->next_meeting_date)):"N/A"; ?>
                </td>
            </tr>
            <tr>
                <td class="no-border text-bold">
                NOTE: All the facts and data presented in these minutes are as per the decisions taken in the meeting. In case of any errors or changes, the owners of the above mentioned minutes or the recipients of the minutes are to respond to the Minutes Recorder within 3 days of getting the minutes.
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="add_task_modal_div"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $(".add_task_modal").click(function () {
            var meeting_minutes_id = $(this).data('id');
            $.ajax({
                type: "GET",

                "url": "<?php echo Yii::app()->createUrl('projects/getaddTaskModal'); ?>",
                data: {
                    meeting_minutes_id: meeting_minutes_id
                },
                success: function (response) {
                    $("#add_task_modal_div").html(response);
                    $("#addTask").modal('show');
                }
            });

        });


    });
</script>