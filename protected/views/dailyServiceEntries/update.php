<?php
/* @var $this DailyServiceEntriesController */
/* @var $model DailyServiceEntries */

$this->breadcrumbs=array(
	'Daily Service Entries'=>array('index'),
	$model->entry_id=>array('view','id'=>$model->entry_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DailyServiceEntries', 'url'=>array('index')),
	array('label'=>'Create DailyServiceEntries', 'url'=>array('create')),
	array('label'=>'View DailyServiceEntries', 'url'=>array('view', 'id'=>$model->entry_id)),
	array('label'=>'Manage DailyServiceEntries', 'url'=>array('admin')),
);
?>

<h1>Update DailyServiceEntries <?php echo $model->entry_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>