

<h1>Daily Service Entries</h1>



<?php

	if(Yii::app()->user->role == 1 || Yii::app()->user->role == 3){ 
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'daily-service-entries-form',
	//'action'=>Yii::app()->createUrl('//dailyServiceEntries/create'),
	'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(           
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
)); ?>

	
	<div class="row">
		
        <div class="subrow col-md-3 col-sm-6">
           <?php echo $form->labelEx($model2,'project_id'); ?>
			
			<?php
                       
			echo $form->dropDownList($model2, 'project_id', CHtml::listData(Projects::model()->findAll(), 'project_id', 'project_name'), array('empty' => '-Choose a Project-','class'=>'form-control')); 
			?>
			<?php echo $form->error($model2,'project_id'); ?>
        </div>
        <div class="subrow col-md-2 col-sm-6">           
		<?php echo $form->labelEx($model2, 'date'); ?>
		<?php echo CHtml::activeTextField($model2, 'date',array('class' => 'form-control')); ?>
		<?php
		$this->widget('application.extensions.calendar.SCalendar', array( 
			'inputField' => 'DailyServiceEntries_date',
			'ifFormat' => '%d-%m-%Y',
		));
		?>
		<?php echo $form->error($model2, 'date'); ?>
        </div>
        

	</div>
	
	<div class="row">
    <div class="subrow col-md-2 col-sm-6">
			<?php echo $form->labelEx($model2,'description'); ?>
			<?php echo $form->textArea($model2,'description',array('class' => 'form-control')); ?>
			<?php echo $form->error($model2,'description'); ?>
    </div> 
	
	

	<div class="buttons col-md-3 col-sm-6">
		<label>&nbsp;</label>
		<?php echo CHtml::submitButton($model2->isNewRecord ? 'Add' : 'Save',array('class'=>' btn blue btn-sm')); ?>
	</div>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
}
?>

<div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'daily-service-entries-grid',
	'itemsCssClass' => 'table table-bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		
		array('class' => 'IndexColumn', 'header' => 'S.No.','htmlOptions'=>array('class'=>'width-20'),),
		//'id',
		//'project_id',
		array(
		'name' =>'project_id',
		'value' =>'$data->project->project_name',
                    'type' => 'raw',
                    'htmlOptions' => array('width' => '120px'),
                    'filter' => CHtml::listData(Projects::model()->findAll(
                                    array(
                                        'select' => array('project_id,project_name'),
                                        'order' => 'project_name',
                                        'distinct' => true
                            )), "project_id", "project_name")
		),
		'description',
		
		'date',
		array(
		'name' =>'created_by',
                  'type' => 'raw',   
		'value' =>'(isset($data->createdBy) ? $data->createdBy->first_name.$data->createdBy->last_name :"")',
		'visible' => Yii::app()->user->role == 1,
                     'type' => 'raw',
                    'htmlOptions' => array('width' => '120px'),
                    'filter' => CHtml::listData(Users::model()->findAll(
                                    array(
                                        'select' => array('userid,concat(first_name, " ", last_name) as first_name'),
                                        'order' => 'first_name',
                                       
                                        'distinct' => true
                            )), "userid", "first_name")
		
		),
		

		 array(
			'template' =>'{update}',
			'class'=>'CButtonColumn',
			'visible' => Yii::app()->user->role == 1,	
			'buttons' => array(
                    'update' => array
                        (
                        'label'=>'',
                        'imageUrl'=>false,
                        'options'=>array('class'=>'icon-pencil icon-comn', 'title'=>'Edit'),
                        'url' => 'Yii::app()->createUrl("DailyServiceEntries/index", array("id"=>$data->entry_id))',
                    ),
                ),
		), 
	),
)); ?>
</div>