<?php
/* @var $this DailyServiceEntriesController */
/* @var $model DailyServiceEntries */

$this->breadcrumbs=array(
	'Daily Service Entries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DailyServiceEntries', 'url'=>array('index')),
	array('label'=>'Manage DailyServiceEntries', 'url'=>array('admin')),
);
?>

<h1>Create DailyServiceEntries</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>