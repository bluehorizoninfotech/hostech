<?php
/* @var $this DailyServiceEntriesController */
/* @var $model DailyServiceEntries */

$this->breadcrumbs=array(
	'Daily Service Entries'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DailyServiceEntries', 'url'=>array('index')),
	array('label'=>'Create DailyServiceEntries', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('daily-service-entries-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Daily Service Entries</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form display-none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'daily-service-entries-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'entry_id',
		'project_id',
		'date',
		'description',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
