<?php
/* @var $this DailyServiceEntriesController */
/* @var $model DailyServiceEntries */

$this->breadcrumbs=array(
	'Daily Service Entries'=>array('index'),
	$model->entry_id,
);

$this->menu=array(
	array('label'=>'List DailyServiceEntries', 'url'=>array('index')),
	array('label'=>'Create DailyServiceEntries', 'url'=>array('create')),
	array('label'=>'Update DailyServiceEntries', 'url'=>array('update', 'id'=>$model->entry_id)),
	array('label'=>'Delete DailyServiceEntries', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->entry_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DailyServiceEntries', 'url'=>array('admin')),
);
?>

<h1>View DailyServiceEntries #<?php echo $model->entry_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'entry_id',
		'project_id',
		'date',
		'description',
	),
)); ?>
