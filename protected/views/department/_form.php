<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'department-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>


    <div class="row">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'department_name'); ?>
            <?php echo $form->textField($model, 'department_name', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'department_name'); ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->