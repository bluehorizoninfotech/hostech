<?php
/* @var $this ManualEntryReportTblController */
/* @var $model ManualEntryReportTbl */

$this->breadcrumbs=array(
	'Manual Entry Report Tbls'=>array('index'),
	$model->entry_id,
);

$this->menu=array(
	array('label'=>'List ManualEntryReportTbl', 'url'=>array('index')),
	array('label'=>'Create ManualEntryReportTbl', 'url'=>array('create')),
	array('label'=>'Update ManualEntryReportTbl', 'url'=>array('update', 'id'=>$model->entry_id)),
	array('label'=>'Delete ManualEntryReportTbl', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->entry_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ManualEntryReportTbl', 'url'=>array('admin')),
);
?>

<h1>View ManualEntryReportTbl #<?php echo $model->entry_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'entry_id',
		'emp_id',
		'date',
		'comment',
		'status',
		'type',
		'created_by',
		'decision_by',
	),
)); ?>
