<?php
/* @var $this ManualEntryReportTblController */
/* @var $model ManualEntryReportTbl */

$this->breadcrumbs=array(
	'Manual Entry Report Tbls'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ManualEntryReportTbl', 'url'=>array('index')),
	array('label'=>'Manage ManualEntryReportTbl', 'url'=>array('admin')),
);
?>

<h1>Create ManualEntryReportTbl</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>