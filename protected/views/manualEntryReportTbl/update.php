<?php
/* @var $this ManualEntryReportTblController */
/* @var $model ManualEntryReportTbl */

$this->breadcrumbs=array(
	'Manual Entry Report Tbls'=>array('index'),
	$model->entry_id=>array('view','id'=>$model->entry_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ManualEntryReportTbl', 'url'=>array('index')),
	array('label'=>'Create ManualEntryReportTbl', 'url'=>array('create')),
	array('label'=>'View ManualEntryReportTbl', 'url'=>array('view', 'id'=>$model->entry_id)),
	array('label'=>'Manage ManualEntryReportTbl', 'url'=>array('admin')),
);
?>

<h1>Update ManualEntryReportTbl <?php echo $model->entry_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>