<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/manual_entry_report.css');
?>
<div class="manualentry-index-sec">
<div class="row">
    <div class="col-md-3">
        <h2>Manage Manual / Ignored Punches</h2>
    </div>
    <div class="col-md-2">
        
    </div>
  
    </div>

    
</div>

<div class="row">
    <div class="col-md-4 display-flex" >
       
            <textarea name="reason" class="reason margin-0 height-37 w-50"></textarea> 
            <input type="button" name='approve' class='attendanceaction btn btn-success margin-left-10' value='Approve' /> &nbsp;
            <input type="button" class='attendanceaction btn btn-danger' name='reject' value='Reject' />
            <img id="loadernew" src="/images/loading.gif" class="width-30 margin-left-159 display-none" />
            <?php
            echo CHtml::image(Yii::app()->request->baseUrl . '/images/loading.gif',
                    'Loading....', array('class' => 'width-30 margin-left-159 display-none'));
            ?>
       
    </div>
    <div class="col-sm-6">
        <div class="wide form custom-form">

            <?php
            $include_site_condition = '';
            if (isset(Yii::app()->user->site_userids) and Yii::app()->user->site_userids != '') {
                $include_site_condition = ' where userid in ( ' . Yii::app()->user->site_userids . ' ) ';
            }

            $sql = 'select `status`, '
                    . 'count(status) as itemcount '
                    . 'from pms_manual_entry_report_view '
                    . $include_site_condition . ' group by `status`';
            $status_count = CHtml::listData(Yii::app()->db->createCommand($sql)->queryAll(), 'status', 'itemcount');
            $filterarray = array('All' => 'All', 'Approved' => 'Approved', 'Pending' => 'Pending', 'Rejected' => 'Rejected');
            echo '<ul class="nav1 nav">';
            $activeitem = ((isset($_GET['type']) and isset($filterarray[$_GET['type']])) ? $_GET['type'] : 'All');
            $status_count['All'] = array_sum($status_count);
            $statusbgcolor = array('#f44336' => 'Pending', 'green' => 'Approved', 'blue' => 'All', '#715d13' => 'Rejected');
            foreach ($filterarray as $k => $status) {
                echo '<li class="nav-item">';
                echo CHtml::link($status
                        . '&nbsp;&nbsp;<span class="badge" style="background-color: ' . array_search($k, $statusbgcolor) . '">'
                        . (isset($status_count[$status]) ? $status_count[$status] : 0)
                        . '</span>', array('manualEntryReportTbl/index', 'type' => $k),
                        array('class' => "nav-link choice-btn manualentryreport-page-status" . ($activeitem == $k ? ' active' : ''), 'aria-disabled' => "true"));
                echo '</li>';
            }
            echo '</ul>';
            ?>
        </div>
    </div>


    <div class="col-md-2 col-sm-6">
        <div class="goto_pager pull-right">Records Per Page: <?php
            $pageSize = Yii::app()->user->getState('reportpagesize', 20);
            echo CHtml::dropDownList('reportpagesize', $pageSize,
                    array(10 => 10, 20 => 20, 50 => 50, 100 => 100, 200 => 200, 500 => 500), array('class'=>'form-control width-55',
                'onchange' =>
                "$.fn.yiiGridView.update('manual-entry-report-tbl-grid',{ data:{reportpagesize: $(this).val() }})",
            ));
            ?>
        </div>
    </div>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'manual-entry-report-tbl-grid',
    'itemsCssClass' => 'table greytable',
    'template' => '<div class="table-responsive">{items}</div>',
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->search($activeitem),
    'filter' => $model,
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'htmlOptions' => array('data-value' => '$data->type_id'),
            'selectableRows' => 2,
            'value' => '$data->entry_id_with_type',
         
            'checkBoxHtmlOptions' => array(
                'name' => 'ids[]',
            ),
        //  'cssClassExpression' => '( $data->status == 0 )? "" : "hiden" ',
        ),
        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('class' => 'width-60'),),
        array(
            'header' => 'Action',
            'type' => 'raw',
            'value' => function($data) {
                return ManualEntryReportTbl::getManualReportActionlink($data->status, $data->type, $data->entry_id);
            },
        ),
        array(
            'name' => 'employee_name',
            'value' => '$data->employee_name',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid', 'concat(first_name," ",last_name) as first_name'),
                                'order' => 'first_name',
                               
                                'distinct' => true
                            )
                    ), "first_name", "first_name")
        ),
//        'entry_id',
        // array('name'=>'emp_id', 'header'=>'Employee Name', 'value'=>'$data->employee_name'),
        array('name' => 'date', 'value' => '$data->date'),
        'comment',
        array('name' => 'status', 'type' => 'raw',
            'value' => '$data->status=="Rejected"?"<span style=\"color:red\">Rejected</span>":'
            . '($data->status=="Approved"?"<span style=\"color:green\">Approved</span>":'
            . '"<span style=\"color:#f44336\">Pending</span>")',
            'filter' => array('Pending' => 'Pending', 'Approved' => 'Approved', 'Rejected' => 'Rejected')
        ),
        array('name' => 'type', 'type' => 'raw',
            'value' => '$data->type',
            'filter' => array('Manual Punch' => 'Manual Punch', 'Ignore Punch' => 'Ignore Punch', 'Shift Punch' => 'Shift Punch')
        ),
//        'created_by',
//        'decision_by',
        array(
            'name' => 'created_by_name',
            'value' => '$data->created_by_name',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid', 'concat(first_name," ",last_name) as first_name'),
                                'order' => 'first_name',
                             
                                'distinct' => true
                            )
                    ), "first_name", "first_name")
        ),
        array(
            'name' => 'decision_by_name',
            'value' => '$data->decision_by_name',
            'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid', 'concat(first_name," ",last_name) as first_name'),
                                'order' => 'first_name',
                            
                                'distinct' => true
                            )
                    ), "first_name", "first_name")
        ),
        
    ),
));
?>
</div>
<script type="text/javascript">
    
    $(function() {
    $('body').bind('ajaxComplete', function() {
        $(".grid-view input[name='ManualEntryReportTbl[date]']").datepicker({dateFormat: 'yy-mm-dd'});
        $('.hasDatepicker').attr('autocomplete', 'off');
    });
    });

    $(document).ajaxStart(function () {
    console.log("ajax start");
    $("#loading").show();
});

    $(document).ready(function () {
        $(".grid-view input[name='ManualEntryReportTbl[date]']").datepicker({dateFormat: 'yy-mm-dd'});
        $(".reason").keyup(function () {
            var val = $(this).val()
            $(this).val(val.toUpperCase())
        })

        $('.hasDatepicker').attr('autocomplete', 'off');
    })

    $(".all").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(".attendanceaction").click(function () {

        $('.attendanceaction').attr('disabled', true);

        var req = $(this).val();
        var reason = $('.reason').val();
        var all = [];
        var data_type = [];

        $('input[name="ids[]"]:checked').each(function () {

            $id_type = (this.value).split('#');
//            alert($id_type[1]);
//            die;
            all.push($id_type[0]);
            data_type.push($id_type[1])
        });

        if (all != '') {
            if (reason == '') {
                alert("please add comment");
                $('.attendanceaction').attr('disabled', false);
            } else {

                if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

                    $('#loadernew').show();

                    $.ajax({
                        method: "post",
                        dataType: "json",
                        data: {id: all, req: req, reason: reason, data_type: data_type},
                        url: '<?php echo Yii::app()->createUrl("/manualEntry/ManualEntryAction") ?>',

                        //}).done(function(ret){
                        success: function (ret) {

                            $('#loadernew').hide();
                            $('.attendanceaction').attr('disabled', false);

                            if (ret.error != '') {
                                $('#actionmsg').addClass('alert alert-danger');
                                $('#actionmsg').html(ret.error);
                                window.setTimeout(function () {
                                    location.reload()
                                }, 1000)
                            } else {
                                $('#actionmsg').addClass('alert alert-success');
                                $('#actionmsg').html(ret.msg);
                                window.setTimeout(function () {
                                    location.reload()
                                }, 1000)

                            }
                        }

                    });
                } else {
                    $('#loadernew').hide();
                    $('.attendanceaction').attr('disabled', false);
                }
            }
        } else {
            alert('Please select Item');
            $('.attendanceaction').attr('disabled', false);
        }
    });

    $(document).on('click','#manual-entry-report-tbl-grid_c0_all',function() {
	var checked=this.checked;
	 $("input[name='ids[]']").each(function() {
         if(checked==true)
         {
          $(this).find("input[name='ids']").prop('checked', true);
          $(this).parent('span').addClass('checked');
         }
         else{
            $(this).find("input[name='ids']").prop('checked', false);
          $(this).parent('span').removeClass('checked');
         }
         
    });
});
</script>


