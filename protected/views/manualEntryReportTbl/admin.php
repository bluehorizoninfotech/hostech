
<h1>Manage Manual Entry Report</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'manual-entry-report-tbl-grid',
    'itemsCssClass' => 'table greytable',
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
            'checkBoxHtmlOptions' => array(
                'name' => 'ids[]',
                'value' => '$data->entry_id',
            ),
        //  'cssClassExpression' => '( $data->status == 0 )? "" : "hiden" ',
        ),
        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('class' => 'width-60'),),
        array(
            'name' => 'employee_name',
            'value' => '$data->employee_name',
            'filter' => CHtml::listData(Employee::model()->findAll(
                            array(
                                'select' => array('emp_id', 'concat(firstname," ",lastname) as firstname'),
                                'order' => 'firstname',
                                'condition' => 'system_user!=1 and status=28',
                                'distinct' => true
                            )
                    ), "firstname", "firstname")
        ),
//        'entry_id',
        // array('name'=>'emp_id', 'header'=>'Employee Name', 'value'=>'$data->employee_name'),
        'date',
        'comment',
        
        array('name'=>'status','type'=>'raw' ,
            'value'=>'$data->status=="Rejected"?"<span style=\"color:red\">Rejected</span>":'
            . '($data->status=="Approved"?"<span style=\"color:green\">Approved</span>":'
            . '"<span style=\"color:#f44336\">Pending</span>")',
            'filter'=>array('Pending'=>'Pending','Approved'=>'Approved','Rejected'=> 'Rejected')
            ),
        
        array('name'=>'type','type'=>'raw' ,
            'value'=>'$data->type',
            'filter'=>array('Manual Punch','Ignore Punch','Shift Punch')
            ),
//        'created_by',
//        'decision_by',
        array(
            'name' => 'created_by_name',
            'value' => '$data->created_by_name',
            'filter' => CHtml::listData(Employee::model()->findAll(
                            array(
                                'select' => array('emp_id', 'concat(firstname," ",lastname) as firstname'),
                                'order' => 'firstname',
                                'condition' => 'system_user!=1 and status=28',
                                'distinct' => true
                            )
                    ), "firstname", "firstname")
        ),
        array(
            'name' => 'decision_by_name',
            'value' => '$data->decision_by_name',
            'filter' => CHtml::listData(Employee::model()->findAll(
                            array(
                                'select' => array('emp_id', 'concat(firstname," ",lastname) as firstname'),
                                'order' => 'firstname',
                                'condition' => 'system_user!=1 and status=28',
                                'distinct' => true
                            )
                    ), "firstname", "firstname")
        ),
    ),
));
?>
