<?php
/* @var $this TempItemsController */
/* @var $model TempItems */

$this->breadcrumbs=array(
	'Temp Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'projects', 'url'=>array('projects/index')),
	
);
Yii::app()->clientScript->registerScript('itemscripts', '
 if($("div.newdiv").text().length>1){

$("#hide").show(); 
$("#clear").show(); 

$("#clear").click(function(){
    $("div.newdiv").empty();
    $("#hide").hide(); 
    $("#clear").hide(); 
 });

$("#hide").click(function(){
    $("div.newdiv").hide();
    $("#show").show();
    $("#hide").hide();
});

$("#show").click(function(){
    $("div.newdiv").show();
    $("#hide").show();
    $("#show").hide();
});
}
');
?>
<div class="clearfix">
    <?php echo CHtml::link('Back To Projects',array('projects/index'),array('class' => 'btn blue pull-right')); ?>
    <h1>Manage Items</h1>
</div>
<div class="form">
    <?php
 // echo $projectid; 
  
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'service-form',
    'action'=>Yii::app()->createUrl('/items/importcsv&projectid='.$projectid),
    'enableAjaxValidation'=>false,
    'method'=>'post',
    
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'
    )
)); ?>
    <fieldset>
        <?php /*?><?php echo $form->errorSummary($model, 'Opps!!!', null, array('class'=>'alert alert-error span12')); ?><?php */?>
        <div class="control-group">
            <div class="span4">
                <div class="control-group <?php  if ($newmodel->hasErrors('postcode')) echo "error"; ?>">
                    <?php  echo $form->labelEx($newmodel,'file'); ?> <?php   echo $form->fileField($newmodel,'file'); ?>
                    <?php  echo $form->error($newmodel,'file'); ?> </div>
            </div>
        </div>
        <div class="form-actions">
            <?php echo CHtml::submitButton( 'Upload',array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'UPLOAD','class'=>'btn btn-success')); ?>

        </div>

        <?= CHtml::link( 'Download Sample CSV', $url = $this->createAbsoluteUrl('items/downloadsample')) ?> |
        <?= CHtml::link( 'View Documentation', $url = Yii::app()->getBaseUrl(true)."/uploads/Item_upload_csv_documentation.pdf", array ("target" =>"_blank" )) ?>
    </fieldset>
    <?php $this->endWidget(); ?>
</div>
<div id="hide" class="font-weight-bold display-none">Hide</div>
<div id="clear" class="font-weight-bold display-none">Clear</div>
<div id="show" class="font-weight-bold display-none">Show</div>
<div class="newdiv"><?php
if(isset($newarr))
{
	foreach($newarr as $arr){
?>

    <div class="info red-color">
        <?php echo $arr; ?>
    </div>
    <?php		
}
	
}
?></div>
<div class="table-responsive">
    <?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'temp-items-grid',
	'itemsCssClass' => 'table table-bordered',
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
            'nextPageLabel'=>'Next ' ),    
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	'dataProvider'=>$model->search($projectid),
	'filter'=>$model,
	'columns'=>array(
		//array('class' => 'IndexColumn', 'header' => 'Sl.No.','htmlOptions'=>array('style'=>'width:20px;'),),
		/*array(
                'name' => 'check',
                'id' => 'selectedIds',
                'value' => '$data->id',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => '100',

            ),*/
		//'id',
        'sl_no',
        'taskid',
        'description',
		'qty',
		'unit',
		'rate',
		//'amount',
	
		'amount',
        'parentid',
        
		
	
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}',//{deleteitem}
			'htmlOptions' => array('width' => '60px'),
			'buttons' => array(
               
                'update' => array(
                    'label'=>'Edit',
                    'options' => array('class' => 'edititems'),
                    'url' => 'Yii::app()->createUrl("items/update",array("id"=>$data->id))',
                    'visible'=>'$data->parentid != "" & $data->qty != "" & $data->unit != "" & $data->rate != "" ',
                ),
                            /*
                'deleteitem' => array(
                    'label' => 'Delete',
                    'options' => array('class' => 'deleteicon'),
                    'url' => 'Yii::app()->createUrl("items/delete",array("id"=>$data->id))',
                    'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                ),    */
                 
            ),
			
		),
	),
)); ?>
</div>


<!-- Add tools Popup -->
<div id="edit" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery','

$(document).ready(function(){
		
            
		$(".edititems").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
            url = url + "&projectid='.$projectid . '",
           //alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#edit").html(response);
                        $("#edit").css({"display":"block"})
                    }

                });
            
        });
        
        
        $(".deleteicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		url = url + "&projectid='.$projectid . '";
		
		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){
					//alert("gsdf");
					//$(".newdiv").hide();
					window.location.reload();
					
				}
				else
				{
				
				}  
				
						   }
					   });
					   
			}
				  
       });
		

		

	});
');