<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
		'id'=>'items-form',
		'action'=>Yii::app()->createUrl('//items/update&id='.$model->id.'&projectid='.$projectid),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>
    <div class="clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'sl_no'); ?>
                    <?php echo $form->textField($model,'sl_no',array('class'=>'form-control','readonly' => true)); ?>
                    <?php echo $form->error($model,'sl_no'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'taskid'); ?>
                    <?php echo $form->textField($model,'taskid',array('class'=>'form-control','readonly' => true)); ?>
                    <?php echo $form->error($model,'taskid'); ?>
                </div>



            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'description'); ?>
                    <?php echo $form->textField($model,'description',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'description'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'qty'); ?>
                    <?php echo $form->textField($model,'qty',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'qty'); ?>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'unit'); ?>
                    <?php echo $form->textField($model,'unit',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'unit'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'rate'); ?>
                    <?php echo $form->textField($model,'rate',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'rate'); ?>
                </div>

            </div>
            <div class="row">

                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'amount'); ?>
                    <?php echo $form->textField($model,'amount',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'amount'); ?>
                </div>
            </div>


            <div class="modal-footer save-btnHold">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn green')); ?>
                <button data-dismiss="modal" class="btn default"
                    onclick="javascript:window.location.reload()">Close</button>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div><!-- model body -->