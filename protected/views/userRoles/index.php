<?php
/* @var $this UserRolesController */
/* @var $model UserRoles */
$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	'Manage',
);
$this->menu=array(
	//array('label'=>'List UserRoles', 'url'=>array('index')),
	//array('label'=>'Create UserRoles', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-roles-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="userrole-index-sec">
<div class="half-table">
    <div class="clearfix">
    <div class="add link pull-right">
        <?php
          if(isset(Yii::app()->user->role) && (in_array('/userRoles/create', Yii::app()->session['menuauthlist']))){
            $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
            echo CHtml::link('Add User Role', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        }
       
        
        ?>
    </div>  
    <h1>Manage User Roles</h1>
</div>
<!-- start response message -->

<div class="alert alert-success display-none" role="alert">
</div>
<div class="alert alert-danger display-none" role="alert">
</div>
<div class="alert alert-warning display-none" role="alert">

</div>

<!-- end response message -->
<?php if(Yii::app()->user->hasFlash('success')):?>
<div class="info">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif; ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-roles-grid',
	'dataProvider'=>$model->search(),
	 'filter'=>$model,
        'itemsCssClass' => 'table table-bordered',
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ','nextPageLabel'=>'Next ' ),
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	'columns'=>array(
		 array(
            'header' => 'S.No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
        array(
            'class' => 'CButtonColumn',
            // 'visible'=> in_array('/userRoles/update', Yii::app()->session['menuauthlist'])?true:false,
            // 'template' => isset(Yii::app()->user->role) && (in_array('/userRoles/update', Yii::app()->session['menuauthlist']))?'{update}':'',
            'template' =>'{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'imageUrl'=>false,
                    //'url' => 'Yii::app()->createUrl("department/update", array("id"=>$data->department_id))',
                    'visible'=> 'in_array("/userRoles/update", Yii::app()->session["menuauthlist"])?true:false',
                    'url' => 'Yii::app()->createUrl("userRoles/update", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid"))',
                    'click' => 'function(e){e.preventDefault();$("#cru-frame-edit").attr("src",$(this).attr("href")); $("#cru-dialog-edit").dialog("open");  return false;}',
                    'options' => array('class' => 'actionitem updateicon icon-pencil icon-comn' ,'title'=>'Edit'),
                ),

                'delete' => array(
                    'label' => '',
                    'imageUrl' => false,
                   
                    'visible' => "isset(Yii::app()->user->role) && (in_array('/userRoles/delete', Yii::app()->session['menuauthlist']))",
                    
                    'click' => 'function(e){e.preventDefault();deleteuserrole($(this).attr("href"));}',
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                ),
            ),
        ),

		'role',
        
	),
)); ?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add User Role',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog-edit',
    'options' => array(
        'title' => 'Edit User Role',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame-edit" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
 <script type="text/javascript"> 
      $(document).ready( function() {
        $('.info').delay(2000).fadeOut();
      });

      function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
        }

      function deleteuserrole(href) {
            var id = getURLParameter(href, 'id');
            var answer = confirm("Are you sure you want to delete?");
           
            var url = "<?php echo $this->createUrl('userRoles/delete&id=') ?>" + id;
            if(answer)
            {
                $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                success: function(response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                    } else if (response.response == "warning") {
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                    }
                    $("#user-roles-grid").load(location.href + " #user-roles-grid");

                }
            });
            }
           
            
        }     
    </script>