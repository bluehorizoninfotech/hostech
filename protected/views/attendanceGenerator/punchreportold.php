
<?php

// print_r(  $this->punchresult[128]['punches'] ); 

if (isset(Yii::app()->user->attendance_type)) {
    $astatus = Yii::app()->user->attendance_type;
} else {
    $astatus = 3;
}

?>

<?php Yii::app()->getModule('groups');?>
<div class="punchreporttold-attendancegenerator-sec">
<div class='row filter'>
    <div class="col-md-2">
<h1>Punching log</h1>
</div>
<div class="col-md-1 filter-items">
<?php
$user_sql = "SELECT * FROM `pms_users` where userid=" . Yii::app()->user->id;

$user = Yii::app()->db->createCommand($user_sql)->queryRow();

?>
</div>
</div>
<div class="width-70-percentage">

    <table  cellspacing="0" border="1" class="logtable margin-bottom-10">
        <thead>
            <tr class="headerinfo font-15 gray-bg">

                <td colspan="2" >
                    <?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contact-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
));
?>

                    <?php
$punchdate = $this->pdate;
$selected_date = date('d-m-Y', strtotime($punchdate));

echo CHtml::textField('reportdate', $selected_date, array(
    "id" => "reportdate",
    'class' => 'reportdate width-100 cursor-pointer',
    'readonly' => true,
));

$this->widget('application.extensions.calendar.SCalendar', array(
    'inputField' => 'reportdate',
    'ifFormat' => '%d-%m-%Y',
));
?>&nbsp;&nbsp;<?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>



            </td>


                <td >
                <select class="attendance_type form-control width-165" name="attendance_type">

<option value="3" <?php if ($astatus == 3) {?> selected <?php }?>>Punch status</option>
<option value="2" <?php if ($astatus == 2) {?> selected <?php }?> >Punched</option>
<option value="1" <?php if ($astatus == 1) {?> selected <?php }?>>Not Punched</option>

</select>

                </td>
                <td>

                <?php echo CHtml::link('Export <img src="images/pdf.png" class="height-15 margin-right-8"/>', array('AttendanceGenerator/generateresult',
    'pdate' => $this->pdate, 'numdays' => 0, 'export' => 1),
    array('class' => 'btn btn-sm btn-info pull-right margin-3')

); ?>



                </td>
                <td>
                <?php echo CHtml::link('Export <img src="images/xl.png" style="height: 15px;margin-right:8px;"/>', array('AttendanceGenerator/generateresult',
    'pdate' => $this->pdate, 'numdays' => 0, 'export' => 0, 'excel' => 1),
    array('class' => 'btn btn-sm btn-info pull-right margin-3')

); ?>
                </td>



                <?php $this->endWidget();?>

                <td colspan="2" class='text-align-right border-0 font-weight-normal'>

                <?php

if (in_array('/Generateresult/monthlylog', Yii::app()->session['pmsmenuauthlist'])) {

    echo CHtml::link('My monthly log &raquo;', array('/Generateresult/monthlylog'), array('style' => 'color:red;font-size:15px;text-decoration:underline;'));
}

?>



                </td></tr>


                </tr>

              </thead>
    </table>

</div>

<div class="row">
                    <div class="col-md-6">
                    <h5 class="strong-blue">Punch Date : <?php echo date('d-m-Y', strtotime($this->pdate)) ?></h5>
                    </div>
                    <div class="col-md-6 links">
                    <?php
if (isset($_GET['days'])) {
    $days = $_GET['days'];
} else {
    $days = 0;
}
$action = 'generateresult';
?>

                    &laquo;   <?php echo CHtml::link('Previous', array('AttendanceGenerator/' . $action, 'days' => $days - 1)) ?> |

                    <?php
if ($days >= 0) {
    echo 'Today | Next &raquo;';
} else {
    ?>
                        <?php echo CHtml::link('Today', array('AttendanceGenerator/' . $action, 'days' => 0)) ?> |
                        <?php echo CHtml::link('Next ', array('AttendanceGenerator/' . $action, 'days' => $days + 1)) ?>&raquo;
                        <?php
}
?>

                    </div>
    </div>

<div class="pull-left width-70-percentage" id="parent">


    <table cellpadding="10" cellspacing="0" border="1" class="logtable " id="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Emp Code</th>
                <th>Name</th>
                <th >Status</th>
                <th>First Punch</th>
                <th>Last Punch</th>
                <th>IN time</th>
                <th>OUT time</th>
                <th>Tot Time</th>
                <th>Tot Punch</th>
                <th>Tot Break Time</th>
                <th>Over Time</th>
                <th>Shift</th>
            </tr>
        </thead>
        <tbody class="newlog">
            <?php
$i = 1;
$pdateindex = date('Ymd', strtotime($this->pdate));
$user_punches = '';
 echo '<pre>';print_r($this->punchresult);exit;

if (!empty($this->punchresult)) {

    foreach ($this->punchresult as $item) {

        $date1 = date('Y-m-d', strtotime($pdateindex));
        // $userid =144;
        // $userid = $item['empdetails']['userid'];

        $punchres = $item['punches'][$pdateindex];
        extract($item['empdetails']);
        extract($punchres['presult']);

        $shift = (isset($punchres['shift_details']['shift'])) ? $punchres['shift_details']['shift'] : 'No shift';
        $color_code = (isset($punchres['shift_details']['color_code'])) ? $punchres['shift_details']['color_code'] : '';
        $shift_punches = (isset($punchres['shift_punch'])) ? $punchres['shift_punch'] : '';
        $outside_punches = (isset($punchres['outside_punch'])) ? $punchres['outside_punch'] : '';
        $standby_punches = (isset($punchres['standby_punches'])) ? $punchres['standby_punches'] : '';
        $ignored_punches = (isset($punchres['ignored_punches'])) ? $punchres['ignored_punches'] : '';
        $manual_entry = (isset($punchres['manual_entry'])) ? $punchres['manual_entry'] : '';
        $shift_temp_no_ot = (isset($punchres['shift_details']['no_ot']) && $punchres['shift_details']['no_ot'] == 1) ? 1 : 0;

        // manual ot

        $manual_ot = Yii::app()->db->createCommand("SELECT manual_ot  FROM `pms_daily_overtime` WHERE userid = " . $userid . " and date='" . date('Y-m-d', strtotime($pdateindex)) . "' ")->queryRow();

        if (!isset($punchres['shift_details']['shift'])) {

            $res = Yii::app()->db->createCommand("SELECT pms_shift_assign.shift_id,shift_name, user_id, att_date, shift_starts, shift_ends,grace_period_before,grace_period_after,color_code FROM `pms_shift_assign`left join pms_employee_shift on pms_employee_shift.shift_id = pms_shift_assign. shift_id WHERE `user_id` = " . $userid . " and `att_date` = '" . date('Y-m-d', strtotime($pdateindex)) . "' ")->queryRow();
            if (!empty($res)) {
                $shift = $res['shift_name'];
            }
        }

        if (empty($shift_punches) && isset($manual_entry) && !empty($manual_entry)) {
            $dat = '';
            
            foreach ($manual_entry as $val) {
                $dat = array('date' => $val, 'device_id' => 1, 'dname' => 'Manual', 'ignore' => 'green');
                $newarr[strtotime($val)] = json_encode($dat);
                // print_r($newarr); exit;
            }
        }

        //  if(!empty($standby_punches)) {
        if ($standby_id != 0) {

            $newarr = array();
            foreach ($standby_punches as $val) {
                $newarr[strtotime($val['date'])] = json_encode($val);
            }
        }

        $outsidenewarr = array();
        if (!empty($outside_punches)) {
            foreach ($outside_punches as $dat) {
                $outsidenewarr[strtotime($dat)] = json_encode($dat);
            }
        }

        if ($totpunch % 2 == 0) {
            $st_color = '#FF8080';
        } else {
            $st_color = '#8AE62E';
        }

        if (isset($break_time) && $break_time != '') {
            $break_color = '#ff8080c7';
        } else {
            $break_color = '#bcde94';
        }

        $firstpunchsec = $this->timetosec($firstpunch);
        $lastpunchsec = $this->timetosec($lastpunch);
        $tottime = $intime + $outtime;
        ?>

                    <?php

        if ($firstpunch == '-') {
            if ($astatus == 1 || $astatus == 3) {
                $user_punches .= '<tr class="userpunchrow">';
                $user_punches .= '<td>' . $i . '</td>';
                $user_punches .= '<td>' . $empcode . '</td>';
                $user_punches .= '<td>' . CHtml::link($fullname, array('/employee/default/view', 'id' => $userid), array('target' => '_blank')) . '<br/>'
                    . '<span style="color:green">' . $company_name . '</span>'
                    . '<span style="float:right;color:#c2c2c2;"><small>#' . $userid . '</small></span><br/>';
                if (isset($this->interchange[$userid])) {
                    $user_punches .= '<small style="color:#0025f1;">Shift Interchange By - ' . $this->interchange[$userid] . '</small>';
                }
                if ($standby_id != 0) {
                    $standby = Users::model()->findByPk($standby_id);
                    $user_punches .= '<small style="color:red;">Stand By - ' . $standby['firstname'] . " " . $standby['lastname'] . '</small>';
                }

                $user_punches .= '</td><td colspan="8" class="totpunch"><span class="totpunchnew">[]</span></td> ';
                $user_punches .= '<td  class="daypunch"><span class="totpunchnew">nopunch</span></td> ';

                $user_punches .= '<td style="background-color: #d8eef3;color: #555;">' . $shift . '</td> ';

                $user_punches .= '</tr>';
            }
        } else {
          
            if ($astatus == 2 || $astatus == 3) {
                ?>

                        <tr class="userpunchrow">
                            <td><?php echo $i; ?></td>
                            <td><?php echo ($empcode == '') ? '' : $empcode ?></td>
                            <td><?php echo CHtml::link($fullname, array('/employee/default/view', 'id' => $userid), array('target' => '_blank')) ?><br/><span style="color:green"><?=$company_name;?></span>
                                <span class="pull-right gray-color"><small>#'.<?php echo $userid ?>.'</small></span><br/>
                                <?php
if ($standby_id != 0) {
                    $standby = Users::model()->findByPk($standby_id);
                    ?>
                                    <br/><small class="red-color">Stand By - <?=$standby['firstname'] . ' ' . $standby['lastname'];?></small>
                                <?php }?>
                                <?php if (isset($this->interchange[$userid])) {?>
                                    <br/><small class="pure-blue-color">Shift Interchange By - <?=$this->interchange[$userid];?></small>
                                <?php }?>


                            </td>
                            <td style="background-color:<?=$st_color;?>"><?php echo $status ?></td>
                            <td><?php echo date('h:i:s', strtotime($firstpunch)) ?></td>
                            <td><?php echo date('h:i:s', strtotime($lastpunch)) ?></td>
                            <td><?php echo ($intime > 0 ? gmdate("H:i:s", $intime) : '-') ?></td>
                            <td><?php echo ($outtime > 0 ? gmdate("H:i:s", $outtime) : '-') ?></td>
                            <td><?php echo ($intime > 0 ? gmdate("H:i:s", $tottime) : '-') ?></td>
                            <td class='totpunch'><?php echo ($totpunch > 0 ? $totpunch : '-') ?>
                                <span class='totpunchnew'><?php
                if (!empty($newarr)) {
                    echo "[" . (implode(",", $newarr)) . "]";
                } else {
                    echo '[]';
                }
                ?></span>
                            </td>
                            <td style="background-color:<?=$break_color;?>"><?=(isset($break_time) && $break_time != '') ? gmdate("H:i:s", $break_time) : ' - '?></td>
                            <td class="outdaypunch" style='background-color:<?=(isset($outside_punch) && $outside_punch != '') ? '#e6121259' : '';?>' <?php if (isset($manual_ot['manual_ot']) && isset($manual_ot['manual_ot']) != '') {?>title="Manual OT"<?php }?>>
                                <?php
                    if ($shift_temp_no_ot == 0) {
                    if (isset($manual_ot['manual_ot']) && isset($manual_ot['manual_ot']) != '') {
                        echo (($manual_ot['manual_ot'] != 0) ? gmdate("H:i:s", $manual_ot['manual_ot']) : 0) . " (M)";
                    } else {
                        //echo (isset($outside_punch) && $outside_punch != '') ? gmdate("H:i:s", $outside_punch) : ' - ';

                        echo (isset($outside_punch) && $outside_punch != '') ? $outside_punch : ' - ';

                    }
                } else {
                    echo ' - ';
                }
                ?>

                                <span class='totpunchnew'><?php
if (!empty($outsidenewarr)) {
                    echo "[" . (implode(",", $outsidenewarr)) . "]";
                } else {
                    echo '[]';
                }
                ?></span>
                           </td>
                            <td class="daypunch" style='background-color: <?=($color_code != '') ? $color_code : '#ff80804d';?>'><?php echo $shift; ?>
                                <span class='totpunchnew'><?php

                if (isset($this->punchresult[$userid]['punches'][date('Ymd', strtotime($this->pdate))]['day_punch'])) {
                    echo "[" . (implode(",", $this->punchresult[$userid]['punches'][date('Ymd', strtotime($this->pdate))]['day_punch'])) . "]";
                } else {
                    echo '[]';
                }
                ?></span>
                            </td>
                        </tr>
                    <?php }?>

                    <?php
$i++;
        }
    }
    ?>

                <?php echo $user_punches; ?>
            <?php } else {?>
                <tr><td></td><td colspan="12">No Result</td></tr>
            <?php }?>


        </tbody>
    </table>
</div>

<div id='punchlogs' class="punchentries">
    Hover on entries to get Punch list.

</div>

            </div>
            
  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>



  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js"></script>




<script>

    $(document).ready(function () {

       
        var addSerialNumber = function () {
            var i = 0;
            $('.newlog tr').each(function (index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };
        addSerialNumber();

        $('.userpunchrow').hover(function () {


            var punchdata = $.parseJSON($(this).find('.totpunch span').text());
           
            var punchlist = '<h5>' + $(this).find('td:nth-child(3)').html() + '</h5> <br/>';

            punchlist += '<span class="pull-right span_style"></span><span class="green1"></span><br/><br/>';

           

             var shift_arr = [];
            if (punchdata != null&&punchdata.length > 0) {
                 $.each(punchdata, function (i, item) {
                      shift_arr.push(item.date);
                  });
              }

              

            //new day punch

            var test = $(this).find('.daypunch span').text();
            if(test == 'nopunch'){
                punchlist += "No punch";
            }else{


            var daypunch = $.parseJSON($(this).find('.daypunch span').text());
              
            if ( daypunch.length > 0) {

             
              var j = 0;
                $.each(daypunch, function (i, item) {

                 
                  punchstatus = '';
                  

                   if (item.ignore == '') {
                       punchstatus = (j % 2 == 0 ? 'IN' : 'OUT');
                       j++;
                   }

                    punchlist += "<li " + (shift_arr.includes(item.date) ? "style='background-color:#21c52136;color:#000'" :"") + " class='newitem pitem_" + punchstatus + "'>" + timetodate(item.logdate) + " <b> &nbsp;" + punchstatus  + "</b><small style='font-size: 10px;float:right;color:" + (item.dname == 'Production' ? "blue" : "blue") + "'></small></li>";


                });
            } else {
               punchlist += "";
            }
          }
            $('.punchentries').html("<ol>" + punchlist + "</ol>");


            //new out day punch

            var outdaypunch = $.parseJSON($(this).find('.outdaypunch span').text());
            if (outdaypunch!=null&&outdaypunch.length > 0) {
            
                punchlist += '<br/><h5><b>' + 'Out Day Punches' + '</b></h5> <br/>';
                $.each(outdaypunch, function (i, item) {
            
                    punchlist += "<li>" + item + "(<b>" + (i % 2 == 0 ? ' IN' : 'OUT  ') + "</b>)</li>";
                });
            } else {
                punchlist += "";
            }
            $('.punchentries').html("<ol>" + punchlist + "</ol>");

        });

        
    });

    function timetodate(unixtimestamp) {

     // Months array
     var months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
     // Convert timestamp to milliseconds
     var date = new Date(unixtimestamp * 1000);

     // Year
     var year = date.getFullYear();
     // Month
     var month = months_arr[date.getMonth()];
     // Day
     var day = date.getDate();
     // Hours
     var hours = date.getHours();
     // Minutes
     var minutes = "0" + date.getMinutes();
     // Seconds
     var seconds = "0" + date.getSeconds();
     // Display date time in MM-dd-yyyy h:m:s format
     var convdataTime = day + '-' + month + '-' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
     return convdataTime;
 }

    $(document).ready(function () {
        // $("#table").tableHeadFixer({'head': true});
    });


$(".mastergrp").change(function(){

var group= $('.mastergrp').val();

 $.ajax({
    method: "post",
    url: "<?php echo CController::createUrl('AttendanceGenerator/generateresult' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')) ?>",
    data: {group:group,ajax:1}
}).done(function (msg) {
     window.location=" <?php echo CController::createUrl('AttendanceGenerator/generateresult' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')) ?>";
});
})

$(".attendance_type").change(function(){

var attendance_type= $('.attendance_type').val();

 $.ajax({
    method: "post",
    url: "<?php echo CController::createUrl('AttendanceGenerator/generateresult' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')) ?>",
    data: {attendance_type:attendance_type,ajax:1}
}).done(function (msg) {
     window.location=" <?php echo CController::createUrl('AttendanceGenerator/generateresult' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')) ?>";
});
})


</script>



