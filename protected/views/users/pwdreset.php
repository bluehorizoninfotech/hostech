<?php
$this->breadcrumbs=array(
	'Reset password',
);
?>

<div class="form">
    <?php
   // $flash_msg = Yii::app()->user->getFlash('flash_msg');

//    if ($act_status == 0) {
//        echo "Verification key is not matched.Please try again";
//    } else {
        ?>    

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'users-pwdrest-form',
            'enableClientValidation'=>true,
		'enableAjaxValidation'=>true,
	          'clientOptions' => array(
                    'validateOnSubmit'=>true,
                    'validateOnChange'=>true,
                    'validateOnType'=>false,),
                ));
        ?>

        <p class="note">Enter your new password.</p>

    <?php echo $form->errorSummary($model); ?>

        <div class="row">
    <?php echo $form->labelEx($model, 'new_pwd'); ?>
            <?php echo $form->passwordField($model, 'new_pwd', array('size' => 32, 'maxlength' => 32)); ?>
        </div>
        <div class="row">
    <?php echo $form->labelEx($model, 'retype_pwd'); ?>
            <?php echo $form->passwordField($model, 'retype_pwd', array('size' => 32, 'maxlength' => 32)); ?>
        </div>        
        <div class="row buttons">
    <?php echo CHtml::submitButton('Submit'); ?>
        </div>

    <?php
    $this->endWidget();
//}
?>

</div><!-- form -->