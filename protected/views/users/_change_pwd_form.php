<?php $this->breadcrumbs = array(
    'My profile'=> array('users/myprofile'),
    'Change password'
);
?>

<h1>Change password</h1>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-form',
        'enableAjaxValidation' => true,
            ));
    ?>

<!--    <p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'old_password'); ?>
        <?php echo $form->passwordField($model, 'old_password', array('class'=>'form-control input-medium','size' => 30, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'old_password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'New password'); ?>
        <?php echo $form->passwordField($model, 'password', array('class'=>'form-control input-medium','size' => 30, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'retype_pwd'); ?>
        <?php echo $form->passwordField($model, 'retype_pwd', array('class'=>'form-control input-medium','size' => 30, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'retype_pwd'); ?>
    </div>        

    <div class="row buttons">
        <div class="parl_btn"><?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn blue')); ?>
        <?php echo CHtml::link('Cancel', array('users/myprofile'),  array('class' => 'btn default')); ?>
    <br clear='all'/>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
