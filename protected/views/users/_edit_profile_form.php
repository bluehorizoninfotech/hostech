<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
$readonly = (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') ? true : false;
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array('validateOnSubmit' => true,),
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'first_name'); ?>
        <?php echo $form->textField($model, 'first_name', array('class' => 'form-control input-medium', 'size' => 40, 'maxlength' => 40, 'readonly' => $readonly)); ?>
        <?php echo $form->error($model, 'first_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'last_name'); ?>
        <?php echo $form->textField($model, 'last_name', array('class' => 'form-control input-medium', 'size' => 40, 'maxlength' => 40, 'readonly' => $readonly)); ?>
        <?php echo $form->error($model, 'last_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('class' => 'form-control input-medium', 'size' => 40, 'maxlength' => 40)); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row buttons">
        <div class="parl_btn"><?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn green')); ?>
            <?php echo CHtml::link('Cancel', array('users/myprofile'), array('class' => 'btn default')); ?>
            <br clear='all' />
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->