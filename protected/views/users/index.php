<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Users',
);

$this->menu = array(
    //array('label' => 'Create User', 'url' => array('create')),
);
?>

<div class="clearfix">
    <div class="add link pull-right">
        <?php
        if (in_array("/users/adduser", Yii::app()->session["menuauthlist"]) && (!defined('LOGIN_USER_TABLE') || LOGIN_USER_TABLE == '')) {
            $createUrl = $this->createUrl('adduser', array("asDialog" => 1, "gridId" => 'address-grid'));
            echo CHtml::link('Add User', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        }
        ?>
    </div>
    <h1>Users List</h1>
</div>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'users-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate' => false,
    'rowCssClassExpression' => '($data->user_type == -1)?"bg_gray":""',
    'filter' => $model,
    'itemsCssClass' => 'table table-bordered',
    //'template' => '<div class="table-responsive">{items}</div>',
    'pager' => array(
        'id' => 'dataTables-example_paginate',  'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),

    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'visible' => 'in_array("/users/update", Yii::app()->session["menuauthlist"])',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Edit'),
                ),
                'delete' => array(
                    'label' => '',
                    // 'visible' => 'in_array("/users/delete", Yii::app()->session["menuauthlist"])',
                    'visible'=>'defined("DELETE_USER") && DELETE_USER && in_array("/users/delete", Yii::app()->session["menuauthlist"])', 
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete',),
                ),
            ),
        ),

        array(
            'name' => 'full_name',
            // 'value'=>'CHtml::link($data->status==0?"$data->first_name $data->last_name *":"$data->first_name $data->last_name" , 
            'value' => '($data->status==0?"<i class=\"fa fa-circle\" style=\"color:#008000;font-size: 10px;\"></i>":"<i class=\"fa fa-circle\" style=\"color:#aaa;font-size: 10px;\"></i>")." ".CHtml::link($data->first_name." ".$data->last_name , 
            
            array("Users/update","id"=>$data->userid))',
            'type' => 'raw',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Users[full_name]',
                'source' => $this->createUrl('Users/autocomplete'),
                'value' => isset($model->full_name) ? $model->full_name : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) { 
                       $("#Users_full_name").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Users_full_name").val(ui.item.value); }'

                ),
            ), true),


        ),

        array(

            'name' => 'username',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Users[username]',
                'source' => $this->createUrl('Users/autouser'),
                'value' => isset($model->username) ? $model->username : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) { 
                       $("#Users_username").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Users_username").val(ui.item.value); }'

                ),
            ), true),
        ),




        array(
            'name' => 'user_type',
            'value' => function ($model) {
                if (!empty($model->user_type)) {
                    return $model->userType->role;
                } else {
                    return '';
                }
            },
            'type' => 'raw',
            'filter' => CHtml::listData(UserRoles::model()->findAll(
                array(
                    'select' => array('id,role'),
                    'order' => 'role',
                    'distinct' => true
                )
            ), "id", "role")
        ),
        // 'email',
        array(
            'name' => 'email',
            'value' => '$data->email',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Users[email]',
                'source' => $this->createUrl('Users/autoemail'),
                'value' => isset($model->email) ? $model->email : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) { 
                       $("#Users_email").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Users_email").val(ui.item.value); }'

                ),
            ), true),
        ),
        array(
            'name' => 'reporting_person',
            'type' => 'raw',
            'value' => '(isset($data->reportingPerson) ? $data->reportingPerson->first_name . $data->reportingPerson->last_name: "")',
            'filter' => CHtml::listData(Users::model()->findAll(array(
                'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                'condition' => 'user_type in (1,2,8)',
                'order' => 'first_name',
                'distinct' => true
            )), 'userid', 'first_name')


        ),
        array(
            'name' => 'department_id',
            'type' => 'raw',
            'value' => '(isset($data->Department) ? $data->Department->department_name : "")',
            'filter' => CHtml::listData(Department::model()->findAll(
                array(
                    'select' => array('department_id,department_name'),
                    'order' => 'department_name',
                    'distinct' => true
                )
            ), "department_id", "department_name")

        ),

        array(

            'name' => 'designation',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Users[designation]',
                'source' => $this->createUrl('Users/autodesig'),
                'value' => isset($model->designation) ? $model->designation : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) { 
                       $("#Users_designation").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Users_designation").val(ui.item.value); }'

                ),
            ), true),
        ),
        array(

            'name' => 'employee_id',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Users[employee_id]',
                'source' => $this->createUrl('Users/autoemployee'),
                'value' => isset($model->employee_id) ? $model->employee_id : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) { 
                       $("#Users_employee_id").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Users_employee_id").val(ui.item.value); }'

                ),
            ), true),
        ),

        array(
            'name' => 'type',
            'value' => function ($model) {
                if ($model->type == 1) {
                    return 'Internal';
                } else {
                    return 'External';
                }
            },
            'filter' => array(1 => 'Internal', 2 => 'External'),
        ),
        array(
            'name' => 'accesscard_id',
            'value' => '(!isset($data->accesscard_id) ? "---" : $data->accesscard_id)',
            //'htmlOptions' => array('style' => 'width: 30px;'),
        ),

        /*
         array(
            'header' => 'Application',
			'value' =>'$data->getapplication($data->userid)',
			'type' => 'html',
			//'htmlOptions' => array('style' => 'width: 30px;'),
         ),
         
         */

        
    ),
));
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add User',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => 500,
    ),
));
?>
<iframe id="cru-frame" width="550" height="430" frameborder="0" class="min-height-325 custom-frame"></iframe>

<?php
$this->endWidget();

?>
<div id="id_view"></div>
<?php
Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   
  } );

');


?>