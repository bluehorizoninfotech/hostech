<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<style>
.div.form .row {
 width: 500px;
}
div.form .subrow {
	width: 50%;
	float: left;
}
div.form .subrow {
	padding: 0 12px !important;
}
div.form input,div.form textarea,div.form select {

	margin: 0.2em 0 0.5em 0;
}
.form-page-users-sec span#Users_type {
	display: flex;
}
.radio input[type=radio] {
	position: static;
	margin-left: 0px;
}
#Users_type{
    display:flex;
}
</style>
<div class="form">
    <?php
    $readonly=(defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '')?true:false;
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
        //        'htmlOptions' => array(
        //            'class' => ' table table-bordered'),
    ));
    ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($model, 'type'); ?>
            <div>
                <?php echo $form->radioButtonList($model, 'type', array('1' => 'Internal', '2' => 'External')); ?>
            </div>
            <?php echo $form->error($model, 'type'); ?>
        </div>
    </div>
    <div class="show" id="internal">
        <div class="row">
            <div class="subrow">
                <?php echo $form->labelEx($model, 'first_name'); ?>
                <?php echo $form->textField($model, 'first_name', array('class' => 'form-control', 'size' => 30, 'maxlength' => 30,'readonly'=>$readonly)); ?>
                <?php echo $form->error($model, 'first_name'); ?>
            </div>


            <div class="subrow">
                <?php echo $form->labelEx($model, 'last_name'); ?>
                <?php echo $form->textField($model, 'last_name', array('class' => 'form-control', 'size' => 30, 'maxlength' => 30,'readonly'=>$readonly)); ?>
                <?php echo $form->error($model, 'last_name'); ?>
            </div>




        <div class=" show internal">
            <div class="subrow">
                <?php echo $form->labelEx($model, 'username'); ?>
                <span class="required">*</span>
                <?php
                echo $form->textField($model, 'username', array('class' => 'form-control', 'size' => 30, 'maxlength' => 20, 'placeholder' => 'username', 'autocomplete' => '',));
                ?>
                <?php echo $form->error($model, 'username'); ?>
            </div>


            <div class="subrow">
                <?php
                $model->password = '';
                ?>
                <?php echo $form->labelEx($model, 'password'); ?>
                <span class="required">*</span>
                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'autocomplete' => 'off', 'size' => 30, 'maxlength' => 30)); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>
        </div>

            <div class="subrow">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'size' => 30, 'maxlength' => 70)); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
            <div class="subrow">
                <?php echo $form->labelEx($model, 'designation'); ?>
                <?php echo $form->textField($model, 'designation', array('class' => 'form-control', 'size' => 30, 'maxlength' => 70)); ?>
                <?php echo $form->error($model, 'designation'); ?>
            </div>


        <div class="show internal">
            <div class="subrow">
                <?php echo $form->labelEx($model, 'department_id'); ?>
                <span class="required">*</span>
                <?php echo $form->dropDownList($model, 'department_id', CHtml::listData(Department::model()->findAll(array('order' => 'department_name ASC')), 'department_id', 'department_name'), array('class' => 'form-control', 'empty' => '-Choose a Department-')); ?>
                <?php echo $form->error($model, 'department_id'); ?>
            </div>
            <div class="subrow">
                <?php echo $form->labelEx($model, 'reporting_person'); ?>

                <?php
                echo $form->dropDownList($model, 'reporting_person', CHtml::listData(Users::model()->findAll(array(
                    'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                    'condition' => 'user_type in (1,2,8)',
                    'order' => 'first_name',
                    'distinct' => true
                )), 'userid', 'first_name'), array('class' => 'form-control', 'empty' => '-----------'));
                ?>

                <?php echo $form->error($model, 'reporting_person'); ?>
            </div>

        </div>
        <div class=" hide" id="external">
            <div class="subrow">
                <label for="Users_company" class="required">Company <span class="required">*</span></label>
                <?php echo $form->textField($model, 'company', array('class' => 'form-control', 'size' => 30, 'maxlength' => 70)); ?>
                <?php echo $form->error($model, 'company'); ?>
            </div>
            <div class="subrow">
                <label for="Users_contact_number" class="required">Contact Number <span class="required">*</span></label>
                <?php echo $form->textField($model, 'contact_number', array('class' => 'form-control', 'size' => 30, 'maxlength' => 70)); ?>
                <?php echo $form->error($model, 'contact_number'); ?>
            </div>

        </div>
            <div class="subrow">
                <?php echo $form->labelEx($model, 'user_type'); ?>
                <?php echo $form->dropDownList($model, 'user_type', CHtml::listData(UserRoles::model()->findAll(array('order' => 'role ASC')), 'id', 'role'), array('class' => 'form-control', 'empty' => '-Choose a role-', 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'user_type'); ?>
            </div>

            <div class="subrow  show internal">
                <?php echo $form->labelEx($model, 'employee_id'); ?>
                <span class="required">*</span>
                <?php echo $form->textField($model, 'employee_id', array('class' => 'form-control', 'size' => 30, 'maxlength' => 70)); ?>
                <?php echo $form->error($model, 'employee_id'); ?>
            </div>


        <div class=" show internal">
            <div class="subrow">
                <?php echo $form->labelEx($model, 'accesscard_id'); ?>
                <?php echo $form->textField($model, 'accesscard_id', array('class' => 'form-control', 'size' => 30, 'maxlength' => 20)); ?>
                <?php echo $form->error($model, 'accesscard_id'); ?>
            </div>


            <div class="subrow">
                <?php

                if (!$model->isNewRecord) {
                    echo $form->labelEx($model, 'status');
                    echo '<div class="radio_btn">' .
                        $form->radioButtonList($model, 'status', array('Active', 'Inactive'), array(
                            'labelOptions' => array('class' => 'display-inline'),
                            'separator' => ''
                        )) . '</div>' . $form->error($model, 'status');
                } else {
                    $model->status = 0;
                }
                ?>
            </div>

        </div>

    </div>




    <div class="row">
        <div class="text-center">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
            </div>
</div>
</<!-- form -->
<script src="<?php echo $this->customAssets('//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js','/js/jquery-1.7.1-jquery.min.js');?>" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        var type = $('input[name="Users[type]"]:checked', '#users-form').val();
        if (type === '2') {
            $('#external').removeClass('hide');
            $('.internal').removeClass('show');
            $('.internal').addClass('hide');
        } else {
            $('#external').addClass('hide');
            $('.internal').addClass('show');
            $('.internal').removeClass('hide');
        }
        $('input[type=radio][name="Users[type]"]').change(function() {
            var type_value = this.value;
            if (type_value === '2') {
                $('#external').removeClass('hide');
                $('.internal').removeClass('show');
                $('.internal').addClass('hide');
            } else {
                $('#external').addClass('hide');
                $('.internal').addClass('show');
                $('.internal').removeClass('hide');
            }
        });
    });
</script>