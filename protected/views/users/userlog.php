

<div class="clearfix">
  
    <h1>In Active Users Log</h1>
</div>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'users-grid',
    'dataProvider' => $model->Search(),
    'filter' => $model,
    'itemsCssClass' => 'table table-bordered',
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
   'nextPageLabel'=>'Next ' ),
     
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
    'columns' => array(
	array('class' => 'IndexColumn', 'header' => 'S.No.',),
         
     
        
        array(
            'name' => 'full_name',
            
            'value'=>'CHtml::link($data->first_name." ".$data->last_name , array("Users/update","id"=>$data->userid)) ', 

            /*'value'=>'($data->status==0?"<i class=\"fa fa-circle\" style=\"color:#008000;font-size: 10px;\"></i>":"<i class=\"fa fa-circle\" style=\"color:#aaa;font-size: 10px;\"></i>")." ".CHtml::link($data->first_name." ".$data->last_name , array("Users/update","id"=>$data->userid))',*/

            'type'=>'raw',
            ),
        'username',
         
         
        array(
            'name' => 'user_type',
            'value' => '$data->userType->role',
            'type' => 'raw',
            'filter' => CHtml::listData(UserRoles::model()->findAll(
                            array(
                                'select' => array('id,role'),
                                'order' => 'role',
                                'distinct' => true
                    )), "id", "role")
        ),
       // 'email',
         array(
            'name' => 'email',
			'value' =>'$data->email',
			//'htmlOptions' => array('style' => 'width: 30px;'),
         ),

        array(

            'header' => 'Last Login',
            'value'  => '$data->getLogdays($data->last_modified)',
            'type'   => 'html',
            
        ),
         


         /*array(
            'name' => 'reporting_person',
            'type' => 'raw',
            'value' => '(isset($data->reportingPerson) ? $data->reportingPerson->first_name . $data->reportingPerson->last_name: "")',
            'filter' => CHtml::listData(Users::model()->findAll(array(
                                'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                                'condition' => 'user_type in (1,2,8)',
                                'order' => 'first_name',
                                'distinct' => true
                            )), 'userid', 'first_name')
            
					
         ),*/
        /* array(
			'name' => 'department_id',
			'type' => 'raw',
			'value' => '(isset($data->Department) ? $data->Department->department_name : "")', 
			'filter' => CHtml::listData(Department::model()->findAll(
                            array(
                                'select' => array('department_id,department_name'),
                                'order' => 'department_name',
                                'distinct' => true
                    )), "department_id", "department_name")
						
         ),*/
        // 'designation',
         /*array(
            'header' => 'Application',
			'value' =>'$data->getapplication($data->userid)',
			'type' => 'html',
			
         ),*/
         
         
        
       
    ),
));
?>
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add User',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => 500,
    ),
));
?>
<iframe id="cru-frame" width="550" height="430" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>
