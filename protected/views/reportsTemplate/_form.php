<?php

$app_root = YiiBase::getPathOfAlias('webroot');
$base_url = Yii::app()->getBaseUrl(true);
$theme_asset_url = $base_url . '/themes/assets/';
$default_logo_path = $theme_asset_url . 'default-logo.png';
$logo = file_exists($app_root . "/themes/assets/logo.png") ? $theme_asset_url . 'logo.png' : $default_logo_path;
$pdf_logo = file_exists($app_root . "/themes/assets/pdf_logo.png") ? $theme_asset_url . 'pdf_logo.png' : $logo;
$report_image = file_exists($app_root.'/images/projem1.png')?'<img src="'.$base_url.'/images/projem1.png'.'" class="size-auto  work-image"  />' : '';
$work_img_1 = file_exists($app_root.'/images/projem4.png')?'<img src="'.$base_url.'/images/projem4.png'.'" class="size-auto work-image" class="" />' : '';
$work_img_2 = file_exists($app_root.'/images/projem5.png')?'<img src="'.$base_url.'/images/projem5.png'.'" class="size-auto work-image" \ />' : '';

?>
<form id="template-form" action="" method="post">
    <!-- Add the plain HTML form here -->
    <input type="hidden" id="ReportsTemplate[template_id]" name="ReportsTemplate[template_id]"
        value="<?php echo $model->template_id; ?>">
    <div class="col-md-12">
        <div id="alertBox" class="alert" style="display: none;"></div>
    </div>
    <div class="col-md-12">
        <p>* Use following code in template :
            {project_name}, {client_name}, {meeting_location}, {meeting_ref}, {project_code}, {purpose},
            {date_time_venue}, {mom_no}, {recorded_by}, {distribution}, {next_meeting_date}, {logo_image_path}

            *</p>
    </div>

    <div class="col-md-12">
        <label for="ProjectTemplate_description">Description <span class="required"> * </span></label>
        <textarea name="ReportsTemplate[description]" id="ReportsTemplate[description]" rows="6" cols="50"
            class="form-control"><?php echo $model->description; ?></textarea>
        <br>
    </div>

    <div class="col-md-6">
        <?php echo CHtml::label('Template', 'template_format'); ?>
        <textarea name="ReportsTemplate[template_format]" class="form-control" id="editor"
            style="min-height: 400px !important; overflow: auto;"><?php echo $model['template_format']; ?></textarea>
    </div>
    <div class="col-md-6">
        <label for="sample_template_data">Sample Data</label>
        <?php
        if ($model->template_id == 1) {
            $sample_data = array(
                'project_name' => 'Project X',
                'client_name' => 'Client Y',
                'meeting_location' => 'Conference Room A',
                'meeting_ref' => 'REF-123',
                'project_code' => 'PRJ-001',
                'purpose' => 'Discuss project progress',
                'date_time_venue' => '2024-02-21, 10:00 AM, Room A',
                'mom_no' => 'MOM-001',
                'recorded_by' => 'John Doe',
                'distribution' => 'Team members',
                'next_meeting_date' => '2024-03-01',
                'logo_image_path' => $pdf_logo,
                'number' => '859054591',
                'email' => 'abc@gmail.com',
                'website' => 'www.google.com',
                'address' => '2nd floor, yeshoda arcade
            near azhakodi temple, puthiyaara po
            calciut 636363, kerala, india',
                'participant_data' => array(
                    array(
                        'si_no' => 1,
                        'name' => 'Adarsh',
                        'initial' => 'M',
                        'designation' => 'Tester',
                        'organization_name' => 'BHI',
                        'organization_initial' => 'B'
                    ),
                    array(
                        'si_no' => 1,
                        'name' => 'Ajith',
                        'initial' => 'K',
                        'designation' => 'PC',
                        'organization_name' => 'BHI',
                        'organization_initial' => 'B'
                    )
                ),
                'meeting_minutes_data' => array(
                    array(
                        'si_no' => 1,
                        'points_discussed' => 'Discuss project progress',
                        'meeting_minutes_status' => 'Pending',
                        'owner' => 'John Doe',
                        'due_date' => '2024-02-28'
                    ),
                    array(
                        'si_no' => 2,
                        'points_discussed' => 'Review budget allocation',
                        'meeting_minutes_status' => 'Completed',
                        'owner' => 'Jane Smith',
                        'due_date' => '2024-03-10'
                    )
                )
            );
        } else if ($model->template_id == 2) {
            $sample_data = array(
                'week_of_month' => '4 (December)',
                'project_name' => 'Project cX',
                'client_name' => 'Client Y',
                'location' => 'Conference Room A',
                'project_code' => 'PRJ-001',
                'project_image' => $report_image,
                'report_logo' => $pdf_logo,
                'footer_logo' => $logo,
                'website' => 'www.projemconsulting.com',
                'weekly_images' => array(
                    array('weekly_image_1' => $work_img_1, 'weekly_image_2' => $work_img_2, 'weekly_label_1' => 'Kochi', 'weekly_label_2' => 'Vypin'),
                    array('weekly_image_1' => $work_img_2, 'weekly_image_2' => $work_img_1, 'weekly_label_1' => 'Plastering', 'weekly_label_2' => 'Drawing'),
                ),
                'wpr_images' => array(
                    array('weekly_image_1' => $work_img_2, 'weekly_image_2' => $work_img_1, 'weekly_label_1' => 'Fixing', 'weekly_label_2' => 'Painting'),
                    array('weekly_image_1' => $work_img_1, 'weekly_image_2' => $work_img_2, 'weekly_label_1' => 'Plastering', 'weekly_label_2' => 'Drawing'),
                ),
                'present_status' => array('1 st stage Back filling work completed.','2 nd stage back filling up to plinth beam bottom is in progress.(@ phase 1)','SSM work along with PCC in progress.(30% completed)','Plinth beam reinforcement work started.'),
                'key_achievements' => array('SSM work up to plinth beam bottom and Copping concrete on top of SSM','Excavation work for remaining Footings @phase1(If clearance given from architect team for PROPOSED GATE ).'),
                'key_actions' => array('Ground flooor Plastering', 'Ground flooor Drawing', 'Ground flooor Painiting'),
                'key_risks' => array('Second flooor Pastering', 'Second flooor Roof work', 'Second flooor Painiting'),
                'drawing_delivered' => array(
                    array(
                        'si_no' => 1,
                        'drawing_description' => 'Final architectural plan. (If any changes done)	 ',
                        'target_date' => '30-04-2024',
                        'actual_date' => '30-04-2025',
                        'remarks' => 'Good',
                    ),
                    array(
                        'si_no' => 1,
                        'drawing_description' => 'Drawing sections and Elevations.	 ',
                        'target_date' => '30-04-2024',
                        'actual_date' => '30-04-2024',
                        'remarks' => 'Good',
                    )
                ),
                'drawing_pending' => array(
                    array(
                        'si_no' => 1,
                        'drawing_description' => 'Final architectural plan. (If any changes done)	',
                        'target_date' => '30-04-2024',
                        'actual_date' => '30-04-2024',
                        'remarks' => 'Good',
                    ),
                    array(
                        'si_no' => 1,
                        'drawing_description' => 'Back filling up to plinth beam bottom	 ',
                        'target_date' => '03-04-2024',
                        'actual_date' => '30-04-2052',
                        'remarks' => 'Good',
                    )
                ),
                'payment_advice' => array(
                    array(
                        'si_no' => 1,
                        'budget_head' => 'Ground Floor ',
                        'description' => 'Plastering',
                        'vendor' => 'BHI',
                        'issued_date' => '30-04-2024',
                        'pa_ref' => 'Bhi-23',
                        'pa_value' => '23000',
                        'status' => 'Paid',
                    ),
                    array(
                        'si_no' => 2,
                        'budget_head' => 'Second Floor ',
                        'description' => 'Painting',
                        'vendor' => 'BHI',
                        'issued_date' => '30-04-2024',
                        'pa_ref' => 'Bhi-34',
                        'pa_value' => '12000',
                        'status' => 'Unpaid',
                    ),
                ),
                'payment_pending' => array(
                    array(
                        'si_no' => 1,
                        'budget_head' => 'First Floor ',
                        'description' => 'Painting',
                        'vendor' => 'BHI',
                        'bill_date' => '30-04-2024',
                        'bill_ref' => 'Good',
                        'bill_amount' => 'Good',
                        'remarks' => 'Good',
                    ),
                    array(
                        'si_no' => 2,
                        'budget_head' => 'Second Floor ',
                        'description' => 'Painting',
                        'vendor' => 'BHI',
                        'bill_date' => '30-04-2024',
                        'bill_ref' => 'Good',
                        'bill_amount' => 'Good',
                        'remarks' => 'Good',
                    ),
                ),
                'site_visit' => array(
                    array(
                        'si_no' => 1,
                        'emplyee_name' => 'Mr.Nahas	 ',
                        'designation' => 'PROJECT ENGINEER	',
                        'visit_date' => '30-04-2024',
                        'purpose' => 'Review and Concrete checking',
                    ),
                    array(
                        'si_no' => 2,
                        'emplyee_name' => 'Mr. Jaise M Jose	',
                        'designation' => 'PMC - Head	',
                        'visit_date' => '30-04-2024',
                        'purpose' => 'Weekly meeting',
                    )
                ),
                'target' => array(
                    array(
                        array(
                            'milestone' => 'Ground floor	',
                        ),
                        array(
                            array(
                                'si_no' => 1,
                                'description' => 'Earth work excavation	',
                                'scheduled_start' => '30-04-2024',
                                'scheduled_finish' => '03-12-2024',
                                'actual_start' => '30-04-2024',
                                'actual_finish' => '',
                                'completed' => '90 %',
                                'remarks' => '',
                            ),
                            array(
                                'si_no' => 2,
                                'description' => 'PCC For footing',
                                'scheduled_start' => '30-04-2024',
                                'scheduled_finish' => '03-12-2024',
                                'actual_start' => '30-04-2024',
                                'actual_finish' => '',
                                'completed' => '90 %',
                                'remarks' => '',
                            ),
                        ),
                    ),
                    array(
                        array(
                            'milestone' => 'Second Floor',
                        ),
                        array(
                            array(
                                'si_no' => 1,
                                'description' => 'R.C.C Neck column	',
                                'scheduled_start' => '30-04-2024',
                                'scheduled_finish' => '03-12-2024',
                                'actual_start' => '30-04-2024',
                                'actual_finish' => '',
                                'completed' => '80 %',
                                'remarks' => '',
                            ),
                            array(
                                'si_no' => 2,
                                'description' => 'R.C.C Footing	',
                                'scheduled_start' => '30-04-2024',
                                'scheduled_finish' => '03-12-2024',
                                'actual_start' => '30-04-2024',
                                'actual_finish' => '',
                                'completed' => '60 %',
                                'remarks' => '',
                            ),
                        ),
                    ),

                )
            );
        }

        $jsonString = isset($sample_data) ? json_encode($sample_data, JSON_PRETTY_PRINT) : '';
        ?>
        <textarea name="ReportsTemplate[template_format_data]" class="form-control" id="sample_template_data"
            style="height: 400px !important; overflow: auto;">
                  <?php echo $jsonString; ?>
                </textarea>

    </div>
    <br>

    <?php if (!$model->isNewRecord) { ?>

        <div class="row  text-center">
            <div class="col-md-12 m-10" style="padding-top:20px;">
                <button type="button" class="btn" id="previewBtn">Preview</button>
                <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-info')); ?>
            </div>
        </div>
    <?php } ?>
</form>

<?php $template_preview = $this->createUrl('/ReportsTemplate/preview'); ?>
<script>

    $(document).ready(function () {
        $('#template-form').on('submit', function (e) {
            e.preventDefault();
            var formData = $(this).serialize();
            var id = document.getElementById('ReportsTemplate[template_id]').value;
            var alertBox = $('#alertBox');
            alertBox.hide();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('ReportsTemplate/SaveTemplateData&id=') ?>" + id,
                data: formData,
                success: function (response) {
                    var parsedResponse = JSON.parse(response);
                    if (parsedResponse.success) {
                        alertBox.removeClass('alert-danger').addClass('alert-success').text(parsedResponse.message).show();
                    } else {
                        alertBox.removeClass('alert-success').addClass('alert-danger').text('Error: ' + parsedResponse.errors).show();
                    }
                    var scrollTop = (alertBox.offset().top) - (($(window).height()) / 2);
                    // Scroll the window to bring the alert box into view
                    $('html, body').animate({
                        scrollTop: scrollTop
                    }, 'slow');

                }
            });
        });
        $('#previewBtn').on('click', function () {

            var params = {};
            params['template_data'] = $('#editor').val();
            params['sample_template_data'] = $('#sample_template_data').val();
            params['template_id'] = document.getElementById('ReportsTemplate[template_id]').value;


            $.ajax({
                type: "POST",
                url: '<?php echo $template_preview; ?>',
                data: params,
                success: function (output) {
                    //popup = window.open('', 'PDF Popup', params);
                    popup = window.open('', '', 'width=800,height=600');
                    // set the document size to A4 size
                    popup.document.body.style.width = '210mm';
                    popup.document.body.style.height = '297mm';
                    popup.document.body.style.margin = '0 auto';
                    popup.document.write(output);
                    popup.document.write("<div class='text-center' style='padding-bottom:20px'><button onclick='window.close()'>Close</button></div>");
                    popup.document.close();
                }
            });

        });
    });
</script>