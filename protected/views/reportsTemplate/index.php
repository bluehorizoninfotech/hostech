<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
// Yii::app()->clientScript->registerScriptFile('/path/to/parsedown.js');

?>

<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>

<div class="container container-pdf-report" id="project">
    <div class="clearfix">
        <h2 class="pull-left">PDF Layouts</h2>
    </div>

    <div class="panel panel-gray">
        <div class="panel-heading form-head">
            <h3 class="panel-title">Edit Template</h3>
        </div>
        <div class="panel-body">

            <div class="row addRow">
                <div class="col-md-6">
                    <?php

                    // Render the form
                    echo CHtml::label('Template Type', 'pdf_type');
                    echo CHtml::beginForm(Yii::app()->createUrl('reportsTemplate/index'), 'post', array('id' => 'templateSelectionForm')); // Using 'post' method and assigning an ID to the form
                    $templateListData = CHtml::listData($template_types, 'template_id', 'template_name');

                    echo CHtml::dropDownList(
                        'template_id', // Name of the attribute
                        $model->template_id, // Selected value
                        $templateListData, // Options
                        array(
                            'prompt' => 'Select Template', // Optional prompt text
                            'class' => 'form-control', // Optional CSS class
                            'onchange' => '$("#templateSelectionForm").submit();', // Submit form on change
                        )
                    );
                    echo CHtml::submitButton('Submit', array('style' => 'display:none;')); // Hide submit button
                    echo CHtml::endForm();
                    ?>

                    </br>
                </div>
                <?php
                echo $this->renderPartial(
                    '_form',
                    array(
                        'model' => $model,
                    )
                );
                ?>


            </div>
        </div>
    </div>
</div>

<style>
    .addRow label {
        display: inline-block;

    }

    input[type="radio"] {
        margin: 4px 4px 0;
    }

    label {
        /* font-weight: 10px; */
        font-size: 14px !important;
    }

    input[type="checkbox"][readonly] {
        pointer-events: none;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .container-pdf-report {
        max-width: 1200px;
        /* Adjust maximum width as needed */
        min-width: 800px;
        /* Adjust minimum width as needed */
        margin: 0 auto;
        /* Center the div horizontally */
        padding: 0 80px;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }

    input[type="checkbox"] {
        margin: 11px 3px 0;
    }
</style>