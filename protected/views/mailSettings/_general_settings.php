<div class="general-settings-sec">
    <div class="row">

        <div class="col-md-6">

            <div class="form">
                <h1>General Settings</h1>

                <?php $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'general-settings-form',
                        'enableAjaxValidation' => false,
                        'action' => Yii::app()->createUrl('mailSettings/GenearlSettings'),
                    )
                ); ?>

                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_host'); ?>
                        <?php echo $form->textField($model, 'smtp_host', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'smtp_host'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_auth'); ?>
                        <?php echo $form->textField($model, 'smtp_auth', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'smtp_auth'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_username'); ?>
                        <?php echo $form->textField($model, 'smtp_username', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'smtp_username'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_password'); ?>
                        <?php echo $form->passwordField($model, 'smtp_password', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'smtp_password'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_secure'); ?>
                        <?php echo $form->textField($model, 'smtp_secure', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'smtp_secure'); ?>
                    </div>

                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_email_from'); ?>
                        <?php echo $form->textField($model, 'smtp_email_from', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'smtp_email_from'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_mailfrom_name'); ?>
                        <?php echo $form->textField($model, 'smtp_mailfrom_name', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'smtp_mailfrom_name'); ?>
                    </div>

                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'smtp_port'); ?>
                        <?php echo $form->textField($model, 'smtp_port', array('class' => 'form-control input-medium', 'size' => 20, 'maxlength' => 20)); ?>
                        <?php echo $form->error($model, 'smtp_port'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="subrow width-100-percentage">
                        <?php echo $form->labelEx($model, 'behind_task_mail_recipients'); ?>
                        <i class="font-11">Seperate email id with comma</i>
                        <?php echo $form->textArea($model, 'behind_task_mail_recipients', ['class' => 'form-control ', 'rows' => 5, 'cols' => 20]); ?>
                        <?php echo $form->error($model, 'behind_task_mail_recipients'); ?>
                    </div>
                </div>

                <h1>Dropbox Access Details</h1>
                <div class="row">
                    <div class="subrow width-100-percentage">
                        <?php echo $form->labelEx($model, 'dropbox_token'); ?>
                        <?php echo $form->textArea($model, 'dropbox_token', ['class' => 'form-control ', 'rows' => 5, 'cols' => 20]); ?>
                        <?php echo $form->error($model, 'dropbox_token'); ?>
                    </div>
                </div>

                <h1>OneDrive Access Details</h1>
                <div class="row">
                    <div class="subrow width-100-percentage">
                        <?php echo $form->labelEx($model, 'onedrive_clientId'); ?>
                        <?php echo $form->textArea($model, 'onedrive_clientId', ['class' => 'form-control ', 'rows' => 5, 'cols' => 20]); ?>
                        <?php echo $form->error($model, 'onedrive_clientId'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="subrow">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary']); ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <!-- <div class="row margin-top-20 padding-bottom-20 form"> -->
                <h3>SMTP Configuration</h3>
                <?php $email_form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'email-settings-form',
                        'enableAjaxValidation' => false,

                        'action' => Yii::app()->createUrl('mailSettings/sendEmail'),

                    )
                );

                ?>

                <div class="row">
                    <div class="subrow">
                        <label>Email</label>
                        <input type="text" class="form-control" name="GeneralSettings[email]">

                        <?php echo CHtml::submitButton('Send Test Mail', ['class' => 'btn btn-primary']); ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <!-- </div> -->
                <h1>Task Notification Cron<a
                        href="<?= Yii::app()->request->baseUrl ?>/index.php?r=projects/projectprogresscron"
                        target="_blank" class="font-16 padding-left-10"> Run</a></h1>
            </div>
            <div class="row margin-top-20 padding-bottom-20 form">

                <?php
                if (isset(Yii::app()->user->role) && Yii::app()->user->role == '1') {
                    ?>
                    <div class="padding-top-20 col-md-6">
                        <label>

                            <input type="checkbox" id="read" name="permission[]" value="0" <?php echo ($account_permission['status'] == 1 ? 'checked' : ''); ?> />
                            Use Accounts </label>
                    </div>
                    <?php
                }

                ?>

                <div class="padding-top-20 col-md-6">
                    <?php echo $form->labelEx($model, 'dashboard_type'); ?>
                    <div class="radio_btn">
                        <input type="radio" id="dashboard_type_1" name="GeneralSettings[dashboard_type]" value="0" <?php echo ($model->dashboard_type == 0) ? "checked" : ""; ?>>Dashboard 1
                        <input type="radio" id="dashboard_type_2" name="GeneralSettings[dashboard_type]" value="1" <?php echo ($model->dashboard_type == 1) ? "checked" : ""; ?>>Dashboard 2
                    </div>
                    <?php echo $form->error($model, 'dashboard_type'); ?>
                </div>
            </div>
            <div class="form margin-top-15">
                <h1>Update Time Entry Approval</h1>
                <?php $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'general-settings-form',
                        'enableAjaxValidation' => false,
                        'action' => Yii::app()->createUrl('mailSettings/approvalTimeEntry'),
                    )
                ); ?>
                <div class="row">
                    <div class="subrow">
                        <div class="col-md-6">
                            <?php echo $form->labelEx($model, 'add_time_entry_approval'); ?>
                        </div>
                        <div class="col-md-6 ">
                            <?php echo $form->dropDownList(
                                $model,
                                'add_time_entry_approval',
                                array(
                                    1 => 'Required ',
                                    2 => 'Not Required',
                                ),
                                array('class' => 'form-control ', 'name' => 'add_time_entry')
                            ); ?>

                        </div>
                        <?php echo $form->error($model, 'add_time_entry_approval'); ?>

                    </div>
                    </div>

                        <div class="clearfix">
                            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary margin-left-15']); ?>
                        </div>
                    <?php $this->endWidget(); ?>

            </div>
        </div>
        <div class="col-md-6">

            <div class="form">
                <h1>Sliders</h1>
                <?php $form1 = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'general-settings-form',
                        'enableAjaxValidation' => false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                        'action' => Yii::app()->createUrl('mailSettings/addsliders'),

                    )
                );
                $m = '';
                ?>
                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'slider_images'); ?>
                        <span class="img_span"> (WidthxHeight)(2048x1536)</span>
                        <?php
                        $this->widget(
                            'CMultiFileUpload',
                            array(
                                'model' => $model,
                                'name' => 'image',
                                'attribute' => 'image',
                                'accept' => 'jpg|gif|png|jpeg',
                                'max' => 4,
                                'remove' => 'Remove Image   ',
                                'duplicate' => 'Already Selected',
                            )
                        );
                        ?>
                        <?php echo $form->error($model, 'slider_images'); ?>
                    </div>
                </div>
                <div class="clearfix">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary margin-left-15']); ?>
                </div>
                <div class="row">
                    <div id="slider_images">
                        <?php
                        $files = CFileHelper::findFiles(Yii::app()->basePath . "/../uploads/sliders");
                        foreach ($files as $key => $val) {
                            $file_name = basename($val);

                            $new_file_name = "";

                            if ($file_name != '.gitkeep') {
                                $new_file_name = basename($val);
                            }


                            ?>
                            <div class="img-holder">
                                <?php
                                if ($new_file_name != "") {
                                    echo CHtml::image(
                                        Yii::app()->request->baseUrl . '/uploads/sliders/' . $new_file_name . '?' . date('YmdHis'),
                                        '',
                                        array(
                                            'class' => 'width-100 height-100'
                                        )
                                    ) . '<i class="del fa fa-close" id="' . $new_file_name . '"></i>';
                                }

                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
            <div class="clearfix"></div>
            <div class="form  margin-top-15">
                <h1>Dashboard Settings</h1>
                <?php $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'general-settings-form',
                        'enableAjaxValidation' => false,
                        'action' => Yii::app()->createUrl('mailSettings/GenearlSettings'),
                    )
                ); ?>
                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'email_notification_page_size_home'); ?>
                        <?php echo $form->textField($model, 'email_notification_page_size_home', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'email_notification_page_size_home'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'escalated_tasks_size_home'); ?>
                        <?php echo $form->textField($model, 'escalated_tasks_size_home', array('class' => 'form-control input-medium', 'size' => 20, 'maxlength' => 20)); ?>
                        <?php echo $form->error($model, 'escalated_tasks_size_home'); ?>
                    </div>
                </div>
                <div class="row">
                    <h4 class="margin-left-10">Section 1</h4>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'experience_year'); ?>
                        <?php echo $form->textField($model, 'experience_year', array('class' => 'form-control input-medium', 'size' => 50, 'onkeypress' => "return isNumberKey(event)")); ?>
                        <?php echo $form->error($model, 'experience_year'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'experience_title'); ?>
                        <?php echo $form->textField($model, 'experience_title', array('class' => 'form-control input-medium', 'size' => 20)); ?>
                        <?php echo $form->error($model, 'experience_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <h4 class="margin-left-10">Section 2</h4>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'project_count'); ?>
                        <?php echo $form->textField($model, 'project_count', array('class' => 'form-control input-medium', 'size' => 50, 'onkeypress' => "return isNumberKey(event)")); ?>
                        <?php echo $form->error($model, 'project_count'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'project_title'); ?>
                        <?php echo $form->textField($model, 'project_title', array('class' => 'form-control input-medium', 'size' => 20)); ?>
                        <?php echo $form->error($model, 'project_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <h4 class="margin-left-10">Section 3</h4>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'customers_count'); ?>
                        <?php echo $form->textField($model, 'customers_count', array('class' => 'form-control input-medium', 'size' => 50, 'onkeypress' => "return isNumberKey(event)")); ?>
                        <?php echo $form->error($model, 'customers_count'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'customers_title'); ?>
                        <?php echo $form->textField($model, 'customers_title', array('class' => 'form-control input-medium', 'size' => 20)); ?>
                        <?php echo $form->error($model, 'customers_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <h4 class="margin-left-10">Section 4</h4>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'engineers_count'); ?>
                        <?php echo $form->textField($model, 'engineers_count', array('class' => 'form-control input-medium', 'size' => 50, 'onkeypress' => "return isNumberKey(event)")); ?>
                        <?php echo $form->error($model, 'engineers_count'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'engineers_title'); ?>
                        <?php echo $form->textField($model, 'engineers_title', array('class' => 'form-control input-medium', 'size' => 20)); ?>
                        <?php echo $form->error($model, 'engineers_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <h4 class="margin-left-10">Section 5</h4>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'ongoing_project_count'); ?>
                        <?php echo $form->textField($model, 'ongoing_project_count', array('class' => 'form-control input-medium', 'size' => 50, 'onkeypress' => "return isNumberKey(event)")); ?>
                        <?php echo $form->error($model, 'ongoing_project_count'); ?>
                    </div>
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'ongoing_project_title'); ?>
                        <?php echo $form->textField($model, 'ongoing_project_title', array('class' => 'form-control input-medium', 'size' => 20)); ?>
                        <?php echo $form->error($model, 'ongoing_project_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <label>

                        <input type="checkbox" id="read" name="footer_permission" value="1" <?php echo ($model->dashboard_status == 1 ? 'checked' : ''); ?> />
                        Show on Dashboard </label>
                </div>
                <div class="clearfix">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary margin-left-15']); ?>
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Clear', ['class' => 'btn btn-primary']); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
            <div class="form margin-top-15">
                <h1>Update Settings</h1>
                <?php $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'general-settings-form',
                        'enableAjaxValidation' => false,
                        'action' => Yii::app()->createUrl('mailSettings/backlogdays'),
                    )
                ); ?>
                <div class="row">
                    <div class="subrow">
                        <?php echo $form->labelEx($model, 'backlog_days'); ?>
                        <?php echo $form->textField($model, 'backlog_days', array('class' => 'form-control input-medium', 'size' => 50, 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'backlog_days'); ?>
                    </div>
                </div>
                <div class="clearfix">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary margin-left-15']); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(document).on('click', ".del", function () {
            // $(".del").on('click', function(){
            if (!confirm("Do you want to delete")) {
                return false;
            } else {
                var file_name = this.id;
                var div = $('#slider_images').html();
                $.ajax({
                    type: 'POST',
                    url: "<?php echo Yii::app()->createUrl('mailSettings/deleteSlider'); ?>",
                    data: { file_name: file_name },
                    success: function (data) {
                        if (data == 1) {
                            alert("Successfully Deleted");
                            $("#slider_images").empty();
                            $("#slider_images").load(location.href + " #slider_images>*", "");
                            // $( "#slider_images" ).load(window.location.href + " #slider_images" );
                        } else {
                            alert("An error Occured");
                        }
                        // $('#userlist').html(data);

                    },


                });
            }
        });
    });

    $("#read").click(function () {

        if ($("#read").val() == 0) {
            $("#read").val(1);
        }
        else {
            $("#read").val(0);
        }
        var id = $("#read").val();
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl("mailSettings/accountPermission") ?>',
            method: "POST",
            data: {
                id: id
            },
            dataType: "json",
            success: function (result) {

                if (result.status == 1) {
                    alert('updated successfully');
                }

            }

        });

    });



</script>
<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $('input[type=radio][name="GeneralSettings[dashboard_type]"]').change(function () {
        var val = $('input[name="GeneralSettings[dashboard_type]"]:checked').val();
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl("mailSettings/setDashboard") ?>',
            method: "POST",
            data: {
                val: val
            },
            dataType: "json",
            success: function (result) {

                if (result.status == 1) {
                    alert('updated successfully');
                }

            }

        });
    });
</script>