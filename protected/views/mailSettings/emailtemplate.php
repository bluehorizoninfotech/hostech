<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<?php
/* @var $this LeaveController */
/* @var $model Leave */
/* @var $form CActiveForm */
?>
<div class="emailtemplate-sec">
<p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully created..</p>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'tasks-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
));

?>


<div class="row">
    <div class="col-md-6">
        <div class="page_devide-removed">
            <div class="form template_form">

                <?php if (Yii::app()->user->hasFlash('success')) : ?>
                    <div class="info-flash margin-left-16 text-align-center font-16 solid-border">
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="MailTemplate[temp_id]" id="MailTemplate_temp_id">
                        <?php echo $form->labelEx($model, 'temp_name'); ?>
                        <?php echo $form->textField($model, 'temp_name', array('size' => 25, 'maxlength' => 25, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'temp_name'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo $form->labelEx($model, 'temp_content'); ?>
                        <?php echo $form->textArea($model, 'temp_content', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'temp_content'); ?>
                    </div>
                </div>

                <div class="row buttons">
                    <div class="col-md-6">
                        <?php echo CHtml::Button($model->isNewRecord ? 'Save' : 'Update', array('class' => 'btn btn-primary save_temp')); ?>
                    </div>
                </div>

            </div>


        </div>
        
    </div>

    <div class="col-md-6">
        <div class="page_devide-removed" id="dateslist">

            <div class="row table">
                
                <style type="text/css">
                    .leave_sum {
                        border-collapse: collapse;
                    }

                    .leave_sum td,
                    .leave_sum th {
                        border: 1px solid #c2c2c2;
                        padding: 5px;
                        width: 30%;
                        font-size: 14px;
                    }
                </style>

                <!-- <div class="col-md-12"><div class="addtable"> <p>Legend</p>
              </div></div>
                <br clear="all" /> -->
                <?php
                ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
 <div class="row">
                <div class="col-md-12">
                    <?php
                    if (!$model->isNewRecord || $model->isNewRecord) {
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'template-grid',
                            'dataProvider' => $model->search(),
                            'itemsCssClass' => 'table table-bordered',
                            'pager' => array(
                                'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                                'nextPageLabel' => 'Next '
                            ),
                            'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                            'columns' => array(
                                array('class' => 'IndexColumn', 'header' => 'S.No.'),
                                array(
                                    'class' => 'CButtonColumn',
                                    'template' => isset(Yii::app()->user->role) ? '{update}' : '',
                                    'buttons' => array(
                                        'update' => array(
                                            'label' => '',
                                            'imageUrl' => false,
                                            // 'url' => 'Yii::app()->createUrl("masters/milestone/update", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid"))',
                                            'click' => 'function(e){e.preventDefault();edittemplate($(this).attr("href"));}',
                                            'options' => array('class' => 'update_milestone actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                                        ),
                                    ),
                                ),
                                array(
                                    'name' => 'temp_name',
                                ),
                                array(
                                    'name' => 'temp_content',
                                ),
                                
                            ),
                        ));
                    }
                    ?>
                </div>
            </div>

                </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".save_temp").click(function() {

                    var temp_name = $('#MailTemplate_temp_name').val();
                    var temp_content = $('#MailTemplate_temp_content').val();
                    var temp_id= $('#MailTemplate_temp_id').val();                   
                    if (temp_name == '') {
                        $("#MailTemplate_temp_name_em_").show();
                        $("#MailTemplate_temp_name_em_").html('Template name cannot be blank.');
                    }
                    if (temp_content == '') {
                        $("#MailTemplate_temp_content_em_").show();
                        $("#MailTemplate_temp_content_em_").html('Template content cannot be blank.');
                    }
                    if (temp_name != '' && temp_content != '') {
                        $.ajax({
                            "type": 'post',
                            "url": "<?php echo Yii::app()->createUrl('MailTemplate/addTemplate'); ?>",
                            "data": {
                                temp_name: temp_name,
                                temp_content: temp_content,
                                temp_id:temp_id
                            },
                            "dataType": "json",
                            success: function(result) {
                                if (result.status == 1) {                                  
                                    $("#success_message").fadeIn().delay(3000).fadeOut();
                                    $('#MailTemplate_temp_name').val("");
                                    $('#MailTemplate_temp_content').val("");
                                    $("#template-grid").load(location.href + " #template-grid");
                                    
                                }


                            }
                        });
                    }

                })
            })
            function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
        }
            function edittemplate(href) {
            var id = getURLParameter(href, 'id');           
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('MailTemplate/getmodel'); ?>",
                "data": {
                    id: id
                },
                "dataType": "json",
                success: function(result) {
                    if (result.stat == 1) {
                         $('#MailTemplate_temp_name').val(result.name);
                         $('#MailTemplate_temp_content').val(result.content);
                         $('#MailTemplate_temp_id').val(result.id);
                       
                    }
                }
            });
        }
        </script>
        <style type="text/css">
            .info-flash {
                color: red;
            }

            .bottom_space {
                margin-bottom: 10px;
            }

            .form-inline .form-control {
                width: 22%;
                display: inline-block;
            }

            h4.title {
                margin-bottom: 8px;
            }

            span#hed {
                font-size: 10px;
            }

            .form-inline .form-control {
                width: 26%;
                display: inline-block;
                height: 25px;
            }

            input#submit2 {
                height: 26px;
            }

            .form-inline.bottom_space {
                margin-top: 10px;
                margin-left: 2px;
            }
        </style>