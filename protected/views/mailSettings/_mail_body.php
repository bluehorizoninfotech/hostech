<?php 
$coordinator='';
?>
<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
<h3 style="padding:0 0 6px 0;margin: 0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi <?php echo  $model->assignedTo->first_name ?> </span></h3>
<table style="margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
<tr><td colspan="2" style="background-color: #ffff;color: #ffff;"><div style="margin-left:20px;"><img src=" <?php echo  $logo ?> " width="50" height="50" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
</tr>
<tr><td colspan="2"><div style="text-align:center;"><h2>PMS - New Task created</h2></div></td>
</tr>
<tr><td colspan="2" style="color:blue;">This is a notification to inform that new task #<?php echo  $id ?>  has been created: </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Task Name: </td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->title ?></td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Project Name: </td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->project->name ?> </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Start date - End dates: </td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo date('d-M-y', strtotime($model->start_date)) .' - ' . date('d-M-y', strtotime($model->due_date)) ?></td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Priority:</td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->priority0->caption ?> </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Assigned To:</td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->assignedTo->first_name  ?></td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Coordinator:</td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo    $coordinator ?> </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Created by: </td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->createdBy->first_name ?> </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Billable:</td> <td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->billable0->caption ?> </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Quantity: </td><td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->quantity ?> '</td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Task status:</td> <td style="border:1px solid #f5f5f5;padding:20px"><?php echo  $model->status0->caption ?> </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;">Description:</td> <td style="border:1px solid #f5f5f5;padding:20px"><?php echo  nl2br($model->description) ?> </td></tr>
<tr><td style="width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold; padding:20px;" colspan="2"><?php  CHtml::link('Click here to see this task', $this->createAbsoluteUrl('tasks/view', array('id' => $id))) ?> </td></tr>
</table>

<p>Sincerely,  <br />
<?php echo  Yii::app()->name ?> </p>
</div>;