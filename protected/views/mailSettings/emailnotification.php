<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
/* @var $this MailSettingsController */
/* @var $model MailSettings */

$this->breadcrumbs=array(
	'Mail Settings'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List MailSettings', 'url'=>array('index')),
	//array('label'=>'Manage MailSettings', 'url'=>array('admin')),
);
?>

<h1>Email Notification</h1>

<ol class="breadcrumb">
  </ol>
  <?php $this->widget('EmailNotification'); ?>
<?php // echo $this->renderPartial('_form', array('model'=>$model,'users'=>$users)); ?>

<script>
  $( function() {
    $( "#datepicker" ).datepicker({
      
        onSelect: function(selectedDate) {
        var url = '<?php echo Yii::app()->createUrl('mailSettings/emailnotification&interval='); ?>'+selectedDate;      
        $(location).attr('href', url);
    }              
    });
  } );
</script>