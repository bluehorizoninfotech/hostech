<?php
/* @var $this MailSettingsController */
/* @var $model MailSettings */
/* @var $form CActiveForm */
?>


<?php

$url = Yii::app()->createAbsoluteUrl("mailSettings/getusers");
$saveurl = Yii::app()->createAbsoluteUrl("mailSettings/savepermissions");

Yii::app()->clientScript->registerScript('myjquery', "
  

    $('input[type=radio][name=type]').change(function() {
        var type = $(this).val();
        $.ajax({
            type: 'POST',
            url: '" . $url . "',
            data:{type:type},
            success:function(data){
                $('#userslist').html(data);
            },
            error: function(data) { // if error occured
                  alert('Error occured.please try again');                 
            },

        });

    });
    
     $('#savebtn').click(function(){
    
        var type = $('input[name=type]:checked').val();
        
        var userids = [];
        var profileids = [];
        if(type == 'individual'){

           
            $('#userslist :checked').each(function() {
			userids.push($(this).val());
			});
          
                $.ajax({
                    type: 'POST',
                    url: '" . $saveurl . "',
                    data:{type: type, userids:userids},
                    success:function(data){
                        alert('success');
                        location.reload();
                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },


                });

        }
        else if(type == 'profile'){

            $('#userslist :checked').each(function() {
			profileids.push($(this).val());
			});
            
           //alert(profileids);
                $.ajax({
                    type: 'POST',
                    url: '" . $saveurl . "',
                    data:{type: type, profileids:profileids},
                    success:function(data){
                        alert('success');
						location.reload();

                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },
                });
        }
       

    });





	$('#clearbtn').click(function(){
	//alert('hi');
		$('input:checkbox').removeAttr('checked');
		$('.userid div span').removeClass('checked');
	});




");
Yii::app()->clientScript->registerCss('mycss', '
    .gridviewlist{
        /*column-count:1;
        column-gap:10px;
        -moz-column-count:1;
        -moz-column-gap:10px;
        -webkit-column-count:1;
        -webkit-column-gap:10px;*/
        
    }

    .gridviewlist div{
        display: inline-block; 
        float: left;
        margin: 5px;
        padding: 5px;
//        width:21%;

    }
');
?>

<?php
$tbl = Yii::app()->db->tablePrefix;
?>

 <div class="col-md-6 mailset">
			
			<div class="typeselect">
				<label><input type="radio" name="type" value="individual" checked="checked">Individual</label>
				<label><input type="radio" name="type" value="profile">Profile</label>

			</div>
			<div id="userslist">
				<?php
				echo "<ul class='users-listview' style='list-style:none'>";
				foreach ($users as $user) {
					
					$userid = $user['userid'];
					$newqry = Yii::app()->db->createCommand("SELECT * FROM {$tbl}mail_settings WHERE user_id = $userid  AND status = 1")->queryRow();
					if($newqry != NULL){
					
					echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><input type='checkbox' value=" . $user['userid'] . " checked >" . $user['full_name'] . "</li>";
					}else{
					echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><input type='checkbox' value=" . $user['userid'] . " >" . $user['full_name'] . "</li>";		
					}
				}
				echo "</ul>";
				
				
				?>   
			</div>
</div>

 <div class="gridviewlist col-md-9">
        <button id="savebtn" class="btn btn-primary float-none">Save</button> 
        <button id="clearbtn" class="btn btn-default float-none">Clear</button> <br/>
</div>
