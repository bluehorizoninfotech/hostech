<?php
/* @var $this MailSettingsController */
/* @var $model MailSettings */

$this->breadcrumbs=array(
	'Mail Settings'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List MailSettings', 'url'=>array('index')),
	//array('label'=>'Manage MailSettings', 'url'=>array('admin')),
);
?>

<h1>Create MailSettings</h1>

<ol class="breadcrumb">
    <li><?php echo CHtml::link('Employee Request',array('employeerequest/admin')); ?> / </li>
    <li>Mail Settings</li>
          
  </ol>

<?php echo $this->renderPartial('_form', array('model'=>$model,'users'=>$users)); ?>
