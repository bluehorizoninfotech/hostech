<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */

$this->breadcrumbs=array(
	'Employeerequests'=>array('index'),
	$model->mail_type,
);


?>
<style>
        table.detail-view th, table.detail-view td{
                border:1px solid #555!important;
        }
        </style>
<div class="view-maillog-sec">
<h1> <?php echo $model->mail_type; ?></h1>

<?php 
$data=$model->message;
$data_array=json_decode($data, true);
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                'mail_type',
                array(
                        'label' => 'created_by',
                        'value' => date('d-M-y H:i:s',strtotime($model->send_date)),
                        'type' => 'raw',               
                        ),
               
                array(
                        'label' => 'Recipient',
                        'value' =>$model->send_toarrayjoin($model->send_to), 
                        'type' => 'raw',               
                ),
		array(
                'label' => 'message',
                'value' =>(($model->mail_type=="Task Progress Notification")?("<div class='mail_message_table'>".(isset($data)?html_entity_decode($data):"--")."</div>"):(isset($data)?html_entity_decode($data):"--")),
                'type' => 'raw',              
                ),
		
		array(
                'label' => 'sendemail_status',
                'value' =>$model->checkMailstatus($model->sendemail_status),
                'type' => 'raw',               
                ),	
             
		// array(
                // 'label' => 'created_by',
                // 'value' => $model->createdBy->first_name.' '.$model->createdBy->last_name,
                // 'type' => 'raw',               
                // ),
                array(
                        'label' => 'created_by',
                        'value' => date('d-M-y',strtotime($model->created_date)),
                        'type' => 'raw',               
                        ),
		
                
            
            
	),
)); ?>
</div>
