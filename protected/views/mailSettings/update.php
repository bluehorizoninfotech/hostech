<?php
/* @var $this MailSettingsController */
/* @var $model MailSettings */

$this->breadcrumbs=array(
	'Mail Settings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MailSettings', 'url'=>array('index')),
	array('label'=>'Create MailSettings', 'url'=>array('create')),
	array('label'=>'View MailSettings', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MailSettings', 'url'=>array('admin')),
);
?>

<h1>Update MailSettings <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>