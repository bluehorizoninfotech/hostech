<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
    'Clients' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Clients', 'url' => array('index')),
    array('label' => 'Create Clients', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('clients-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Clients</h1>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form display-none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
echo CHtml::link('Create Client', '#', array('onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'clients-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('name'=>'cid', 'htmlOptions' => array('width' => '40px','class'=>'font-weight-bold text-align-center')),  
        'name',
		'nick_name',
        array(
            'name' => 'project_type',
            'value' => '$data->projectType->project_type',
            'type' => 'raw',
            'filter' => CHtml::listData(ProjectType::model()->findAll(
                            array(
                                'select' => array('ptid,project_type'),
                                'order' => 'project_type',
                                'distinct' => true
                    )), "ptid", "project_type")
        ),
        'description',
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        'created_date',
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}{update}',
            'buttons' => array(
                /*'view' =>
                array(
                    'url' => 'Yii::app()->createUrl("clients/view", array("id"=>$data->cid,"asDialog"=>1))',
                    'options' => array(
                        'ajax' => array(
                            'type' => 'POST',
                            // ajax post will use 'url' specified above 
                            'url' => "js:$(this).attr('href')",
                            'update' => '#id_view',
                        ),
                    ),
                ),*/
             
                'update' =>
                array(
                    'url' => '$this->grid->controller->createUrl("update", array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                    'click' => 'function(){$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                ),
            ),
        ),
    ),
));
?>
<?php
//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Detail view',
        'autoOpen' => false,
        'modal' => false,
        'width' => 620,
        'height' => 560,
    ),
));
?>
<iframe id="cru-frame" width="600" height="500" scrolling="auto"></iframe>
<?php
$this->endWidget();
//--------------------- end new code --------------------------
//the dialog
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'dlg-clients-view',
    'options' => array(
        'title' => 'Detail view',
        'autoOpen' => false, //important!
        'modal' => true,
        'width' => 550,
        'height' => 470,
    ),
));
?>
<div id="id_view"></div>
<?php $this->endWidget(); ?>
