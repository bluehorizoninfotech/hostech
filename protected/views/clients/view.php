<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
    'Clients' => array('index'),
    $model->name,
);

/*$this->menu = array(
    array('label' => 'List Clients', 'url' => array('index')),
    array('label' => 'Create Clients', 'url' => array('create')),
    array('label' => 'Update Clients', 'url' => array('update', 'id' => $model->cid)),
    array('label' => 'Delete Clients', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->cid), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Clients', 'url' => array('admin')),
);*/
?>

<div class="client-view-sec">
    <h1>View Client</h1>

    <div class="row">
        <div class="col-md-3 d-flex">
            <div>
                <label for="">CID :</label>
            </div>
            <div>
                <label for=""><?php echo $model->cid ?></label>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div>
                <label for="">Name :</label>
            </div>
            <div>
                <label for=""><?php echo $model->name ?></label>
            </div>
        </div>
        <div class="col-md-3 d-flex">
            <div>
                <label for="">Nick Name :</label>
            </div>
            <div>
                <label for=""><?php echo $model->nick_name ?></label>
            </div>

        </div>

        <div class="col-md-3 d-flex">
            <div>
                <label for="">Project Type :</label>
            </div>
            <div>
                <label for=""><?php echo $model->projectType->project_type ?></label>
            </div>

        </div>

        <div class="col-md-3 d-flex">
            <div>
                <label for="">Description :</label>
            </div>
            <div>
                <label for=""><?php echo $model->description ?></label>
            </div>

        </div>

        <div class="col-md-3 d-flex">
            <div>
                <label for="">Status :</label>
            </div>
            <div>
                <label for=""><?php echo $model->status0->caption ?></label>
            </div>

        </div>

        <div class="col-md-3 d-flex">
            <div>
                <label for="">Created Date :</label>
            </div>
            <div>
                <label for=""><?php echo date('d-M-y', strtotime($model->created_date)) ?></label>
            </div>

        </div>

        <div class="col-md-3 d-flex">
            <div>
                <label for="">Created By :</label>
            </div>
            <div>
                <label for=""><?php echo $model->createdBy->first_name ?></label>
            </div>

        </div>



    </div>


    <br />
    <br />
    <div class="table-responsive">
        <div>
            <table>
                <thead class="head_list text-black">
                    <th>Billable Hrs </th>
                    <th>Grand Billable Amount</th>
                    <th>Total Invoiced</th>
                    <th>Received Total</th>
                    <th>Balance</th>
                    <th>Total to be Billed</th>
                </thead>
                <tbody>
                    <tr>
                        <td><?php
                            $get_val = Invoice::model()->Totgetbillhrs($model->cid);
                            echo $get_val['bilhre'];
                            ?>
                        </td>
                        <td>$<?php
                                //  echo Invoice::model()->Gettotlamnt($model->cid);
                                $get_val = Invoice::model()->Totgetbillhrs($model->cid);
                                echo $get_val['bilamnt'];
                                ?>

                        </td>

                        <td>$<?php
                                echo Invoice::model()->Gettotamnt($model->cid);
                                ?>
                        </td>
                        <td>$<?php
                                echo Invoice::model()->getInvoicepayamnt($model->cid);
                                ?>
                        </td>
                        <td><?php
                            $calamnts = (Invoice::model()->Gettotamnt($model->cid) - Invoice::model()->getInvoicepayamnt($model->cid));
                            if ($calamnts >= 0) {
                                $numvalues = number_format($calamnts, 2);
                                $numvalues = "<span style='color:red'>$" . $numvalues . "</span>";
                            } else {
                                $numvalues = number_format($calamnts, 2);
                                $numvalues = str_replace("-", "+$", $numvalues);
                                $numvalues = "<span style='color:green'>" . $numvalues . "</span>";
                            }
                            echo $numvalues;
                            ?>


                        </td>
                        <td>
                            <?php
                            $calamns = (Invoice::model()->Gettotlamnt($model->cid) - Invoice::model()->getInvoicepayamnt($model->cid));
                            if ($calamns >= 0) {
                                $numvalus = number_format($calamns, 2);
                                $numvalus = "<span style='color:red'>$" . $numvalus . "</span>";
                            } else {
                                $numvalus = number_format($calamns, 2);
                                $numvalus = str_replace("-", "+$", $numvalus);
                                $numvalus = "<span style='color:green'>" . $numvalus . "</span>";
                            }
                            echo $numvalus;
                            ?>

                            <?php
                            //                    $tobebilld = number_format(Invoice::model()->Gettotlamnt($model->cid) - Invoice::model()->getInvoicepayamnt($model->cid), 2);
                            //                    echo $tobebilld;
                            ?>
                        </td>


                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <?php
    $pageSize = Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']);

    /* echo CHtml::dropDownList('pageSize',$pageSize, array(10=>10,20=>20,50=>50,100=>100,500=>500,1000=>1000), array('empty' => 'Select', 'id' => 'clientid',
  'ajax' => array(
  'type' => 'POST',
  'dataType' => 'json',
  'url' => CController::createUrl('clients/view'),
  'data' => array('pagid' => 'js:this.value'),
  'success' => 'function(data) {
  alert("test");
  }'
  )));
 */

    //echo CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,500=>500,1000=>1000),array('onchange'=>"test()',{ data:{pageSize: $(this).val() }})",'empty'=>'-- Select Page Range --','style'=>'width:198px;'));
    ?>
    <h1>List of Projects </h1>
    <div class="table-responsive">
        <table cellpadding="3" cellspacing="0" class="timehours table table-bordered">
            <thead>
                <tr class="tr_title">
                    <th width="50">S.No</th>
                    <th width="550">Project Name</th>
                    <th width="350">Outstanding</th>
                    <th>Non Billable Hrs</th>
                    <th width="100">Billable(Hrs)</th>
                    <th>Invoiced Hours</th>

                    <th width="50">Billable Amount($)</th>
                    <th width="50">Invoiced Amount($)</th>
                    <th width="50">Amount Recieved</th>
                    <th width="50">Remaining/Balance Amount</th>
                </tr>
            </thead>
            <?php
            $Criteria = new CDbCriteria();
            $Criteria->select = "*";

            $Criteria->compare('clntid', $model->cid);
            $Criteria->group = 'pjctid';

            $count_deal = PaymentReport::model()->count($Criteria);


            $pages = new CPagination($count_deal);
            $pages->pageSize = 5;
            $pages->applyLimit($Criteria);
            $listproject = PaymentReport::model()->findAll($Criteria);
            if (!empty($getdata)) {
            ?>
                <?php
                $k = 1;
                foreach ($getdata as $pro) {
                ?>
                    <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
                        <td><?php echo $k; ?></td>
                        <td><?php
                            echo CHtml::link(Projects::model()->findByAttributes(array('pid' => $pro['pjctid']))->name, array(
                                'projects/view',
                                'id' => $pro['pjctid']
                            ));
                            ?></td>
                        <td>
                            <?php
                            $createUrl = $this->createUrl('invoice/page', array('id' => $pro['pjctid'], "asDialog" => 1, "gridId" => 'address-grid'));
                            echo CHtml::link('Outstanding', '#', array('onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
                            ?>

                        </td>
                        <td>
                            <?php
                            echo Invoice::model()->Getnonbil($pro['pjctid']);
                            ?>


                        </td>
                        <td><?php echo Invoice::model()->Getbillhrs($pro['pjctid']); ?></td>
                        <td><?php echo Invoice::model()->Getinvichr($pro['pjctid']); ?></td>
                        <td>$<?php echo Invoice::model()->Getbillamnt($pro['pjctid']); ?></td>

                        <td><?php echo "$" . Invoice::model()->getprjctamnt($pro['pjctid']); ?></td>
                        <th class="totaltr">$<?php echo Invoice::model()->Getamountpaid($pro['pjctid']); ?></th>
                        <td><?php
                            $calamnt = (Invoice::model()->getprjctamnt($pro['pjctid']) - Invoice::model()->Getamountpaid($pro['pjctid']));
                            if ($calamnt >= 0) {
                                $numvalue = number_format($calamnt, 2);
                                $numvalue = "<span style='color:red'>$" . $numvalue . "</span>";
                            } else {
                                $numvalue = number_format($calamnt, 2);
                                $numvalue = str_replace("-", "+$", $numvalue);
                                $numvalue = "<span style='color:green'>" . $numvalue . "</span>";
                            }
                            echo $numvalue;
                            ?></td>


                    </tr>

                <?php
                    $k++;
                }
                ?>


            <?php
            }
            ?>
        </table>
    </div>
    <div class="pagnt_clnt">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
        ));
        ?>
    </div>

    <br />



    <h1>List of Invoices </h1>
    <div class="table-responsive">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => Invoice::model()->getinvoicedata($model->cid),
            'columns' => array(
                array('class' => 'IndexColumn', 'header' => 'S.No.',),
                //        'invoice_id',
                array('name' => 'client_id', 'value' => '$data->client->name',),
                'invoice_title',
                'invoice_no',
                array(
                    'name' => 'amount',
                    'value' => '$data->gettotalamt($data->invoice_id)'
                ),
                array(
                    'name' => 'amnt_paid',
                    'value' => '$data->getrecvamt($data->invoice_id)'
                    //  'value' => '($data->paymntid) ? Paylist::Model()->FindByPk($data->paymntid)->amountpaid :"0" ',
                ),
                array(
                    'name' => 'balance',
                    'value' => '$data->getbalanc($data->invoice_id)',
                    'type' => 'html'
                    //  'value' => '(isset(Paylist::Model()->FindByPk($data->paymntid)->balance_amnt) !="") ? Paylist::Model()->FindByPk($data->paymntid)->balance_amnt :"0" ',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{Add}',
                    'buttons' => array(
                        'Add' =>
                        array(
                            'label' => 'Payment',
                            'url' => '$this->grid->controller->createUrl("invoice/reports_paymnt", array("id"=>$data->invoice_id,"asDialog"=>1,"gridId"=>$this->grid->id))',
                            'click' => 'function(){$("#cru-frames").attr("src",$(this).attr("href")); $("#cru-dialogs").dialog("open");  return false;}',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
    <?php
    //--------------------- begin new code --------------------------
    // add the (closed) dialog for the iframe
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'cru-dialog',
        'options' => array(
            'title' => 'Detailed Invoice',
            'autoOpen' => false,
            'modal' => false,
            'width' => "90%",
            'height' => 560,
        ),
    ));
    ?>
    <iframe id="cru-frame" height="500" scrolling="auto" style="border:1px solid #c2c2c2;width:100%"></iframe>
    <?php
    $this->endWidget();
    //--------------------- end new code -------------------------- 
    //the dialog
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'dlg-invoice-view',
        'options' => array(
            'title' => 'Detailed Invoice',
            'autoOpen' => false, //important!
            'modal' => true,
            'width' => "100%",
            'height' => 470,
        ),
    ));
    ?>
    <div id="id_view"></div>
    <?php $this->endWidget(); ?>


    <?php
    //--------------------- begin new code --------------------------
    // add the (closed) dialog for the iframe
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'cru-dialogs',
        'options' => array(
            'title' => 'Detailed Invoice',
            'autoOpen' => false,
            'modal' => false,
            'width' => "90%",
            'height' => 560,
        ),
    ));
    ?>
    <iframe id="cru-frames" height="500" scrolling="auto" class="light-gray-border width-100-percentage"></iframe>
    <div id="id_view"></div>
</div>
<?php $this->endWidget(); ?>