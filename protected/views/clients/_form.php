<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
?>
<style>
    .button-row {
        display: flex;
        justify-content: center;
        grid-column-gap: 10px;
    }
</style>
<div class="form custom-form">

    <?php
    $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'clients-form',
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,
            ),
        )
    );
    ?>


    <?php //echo $form->errorSummary($model);   ?>

    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'nick_name'); ?>
            <?php echo $form->textField($model, 'nick_name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'nick_name'); ?>
        </div>
    </div>
    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($model, 'project_type'); ?>
            <?php echo $form->dropDownList($model, 'project_type', CHtml::listData(ProjectType::model()->findAll(array('order' => 'project_type ASC')), 'ptid', 'project_type'), array('empty' => '--', 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'project_type'); ?>
        </div>

        <div class="subrow">
            <?php echo $form->labelEx($model, 'status'); ?>
            <div class="radio_btn">
                <?php

                $status = Status::model()->findAll(
                    array(
                        'select' => array('sid,caption'),
                        'condition' => 'status_type="active_status"',
                        'order' => 'caption',
                        'distinct' => true
                    )
                );
                ?>
                <?php foreach ($status as $sts) { ?>
                    <label class="display-inline margin-right-10 verticl-align-bottom">
                        <input type="radio" name="Clients[status]" value="<?= $sts->sid ?>" <?php echo ($model->status == $sts->sid) ? 'checked' : ''; ?>>
                        <?= $sts->caption; ?>
                    </label>
                <?php } ?>
            </div>
            <?php echo $form->error($model, 'status'); ?>
        </div>
    </div>

    <div class="row">
        <div class="subrow subrowlong">

            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('rows' => 6, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
    </div>


    <div class="row buttons">
        <div class="button-row">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Cancel', array('class' => 'btn default')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    #content {
        width: 500px;
        /*border: 1px solid #ddd;*/
    }

    div.form .row {
        /* width: 500px; */
    }

    .subrow {
        width: 50%;
        float: left;
    }

    form .subrow {
        padding: 0 12px !important;
    }

    div.form input,
    div.form textarea,
    div.form select {
        margin: 0.2em 0 0.5em 0;
    }

    .subrowlong {
        /*        width: 500px;
                float:left;*/
    }

    .radio_btn label {
        display: inline-block !important;
        margin-right: 15px;
    }

    .blue.btn {
        margin-left: 30px;
    }
</style>