<?php
/* @var $this AttendanceRequestsController */
/* @var $model AttendanceRequests */

$this->breadcrumbs=array(
	'Attendance Requests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AttendanceRequests', 'url'=>array('index')),
	array('label'=>'Manage AttendanceRequests', 'url'=>array('admin')),
);
?>

<h1>Create AttendanceRequests</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>