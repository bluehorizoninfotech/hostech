<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<?php
// Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/manual_entry_report.css');

$user_array = array();
$sql = "select `userid` from pms_users";
$user_list_query = Yii::app()->db->createCommand($sql)->queryAll();
foreach ($user_list_query as $user_list) {
    $user_array[] = $user_list['userid'];
}
if (isset(Yii::app()->user->project_site) && isset(Yii::app()->user->site_userids) && !empty(Yii::app()->user->site_userids)) {
    $user_array = Yii::app()->user->site_userids;
    $user_array = explode(',', $user_array);
}
if (isset(Yii::app()->user->project_site) && empty(Yii::app()->user->site_userids)) {
    $user_array = array();
}
?>

<?php
/* @var $this attendanceRequestController */
/* @var $model attendanceRequest */

$this->breadcrumbs = array(
    'Attendance Requests' => array('index'),
    'Manage',
);
?>
<div class="attendance-request-sec">
<div class="row">
    <div class="col-md-3">
        <h2>Manage Attendance Requests</h2>
    </div>
</div>

<div class="row tag-section">
    <div class="col-md-4 flex-direction display-flex">
        <?php //$this->renderPartial('_form',array('model'=>$model1));     
        ?>
        <div id='actionmsg' class="width-440 margin-top-10"></div>


        <div class="pull-left form decision_form display-flex">
            <?php if (in_array('/attendanceRequest/approve', Yii::app()->session['menuauthlist'])) { ?>
                <textarea name="reason" class="reason margin-0 height-37 w-50"></textarea>
                <input type="button" name='approve' class='attendanceaction btn btn-success margin-left-10' value='Approve' /> &nbsp;
                <input type="button" class='attendanceaction btn btn-danger' name='reject' value='Reject' />
                <img id="loadernew" src="/images/loading.gif" class="width-30 margin-left-159 display-none" />
            <?php } ?>
        </div>
    </div>

    <div class="col-md-6">

        <?php
        $include_site_condition = '';
        if (isset(Yii::app()->user->site_userids) and Yii::app()->user->site_userids != '') {
            $include_site_condition = ' where user_id in ( ' . Yii::app()->user->site_userids . ' ) ';
        }

        $sql = 'select `status`, '
            . 'count(status) as itemcount '
            . 'from pms_attendance_requests '
            . $include_site_condition . ' group by `status`';
        $status_count = CHtml::listData(Yii::app()->db->createCommand($sql)->queryAll(), 'status', 'itemcount');
        $filterarray = array('All' => 'All', 'Approved' => 'Approved', 'Pending' => 'Pending', 'Rejected' => 'Rejected');
        $statusarray = array('All' => 'All', 1 => 'Approved', 0 => 'Pending', 2 => 'Rejected');

        echo '<ul class="nav1 nav">';
        $activeitem = ((!isset($_GET['type']) or $_GET['type'] == 'All') ? 'All' : intval($_GET['type']));

        $status_count['All'] = array_sum($status_count);

        $statusbgcolor = array('#f44336' => '0', '#379258' => '1', '#337ab7' => 'All', '#ad8383' => '2');
        foreach ($statusarray as $k => $status) {

            echo '<li class="nav-item">';
            echo CHtml::link(
                $status
                    . '&nbsp;&nbsp;<span class="badge" style="background-color: ' . array_search($k, $statusbgcolor) . '">'
                    . (isset($status_count[$k]) ? $status_count[$k] : 0)
                    . '</span>',
                array('/attendanceRequest/index', 'type' => $k),
                array('class' => "nav-link choice-btn" . ($k === $activeitem ? ' active ' : ''), 'aria-disabled' => "true")
            );
            echo '</li>';
        }
        echo '</ul>';
        ?>


    </div>

    <div class="col-md-2 pull-right">
        <div class="goto_pager pull-right margin-bottom-12">Records Per Page: <?php
                                                                $pageSize = Yii::app()->user->getState('reportpagesize', 20);
                                                                echo CHtml::dropDownList(
                                                                    'reportpagesize',
                                                                    $pageSize,
                                                                    array(10 => 10, 20 => 20, 50 => 50, 100 => 100, 200 => 200, 500 => 500),
                                                                    array(
                                                                         'class' => 'form-control width-80 margin-top-8',
                                                                        'onchange' =>
                                                                        "$.fn.yiiGridView.update('attendance-requests-grid',{ data:{reportpagesize: $(this).val() }})",
                                                                    )
                                                                );
                                                                ?>
        </div>
    </div>
</div>

<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="row"><div class="flash-' . $key . '">' . $message . "</div></div>\n";
}

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'attendance-requests-grid',
    'dataProvider' => $model->search($activeitem),
    'itemsCssClass' => 'table table-bordered',
    'template' => '<div class="table-responsive">{items}</div>',
    'filter' => $model,
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
            'checkBoxHtmlOptions' => array(
                'name' => 'ids[]',
            ),
            'cssClassExpression' => '( $data->status == 0 )? "" : "hidden_check" ',
        ),
        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('class' => 'width-60'),),
        array(
            'class' => 'CButtonColumn',
            'template' => '{approve_reject}',
            'buttons' => array(
                'approve_reject' => array(
                    'label' => 'Approve/Reject',
                    'url' => 'Yii::app()->createUrl("/attendanceRequest/view", array("id"=>$data->att_id))',
                    'visible' => '$data->status!=3 && ( Yii::app()->user->role==1 or in_array("/attendanceRequest/approve", Yii::app()->session["menuauthlist"]) )',
                ),
                'view' => array(
                    'label' => '',
                    'url' => 'Yii::app()->createUrl("/attendanceRequest/view", array("id"=>$data->att_id))',
                    'visible' => '$data->status!=0 && (  Yii::app()->user->role==1 or in_array("/attendance/attendanceview", Yii::app()->session["menuauthlist"]) )',
                )
            ),
        ),
        array(
            'name' => 'user_id',
            'value' => '$data->usrId->first_name." ".$data->usrId->last_name',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid', 'concat(first_name," ",last_name) as first_name'),
                    'order' => 'first_name',

                    'distinct' => true
                )
            ), "userid", "first_name")
        ),
        array(
            'name' => 'att_entry',
            'value' => '$data->attEntry->short_note',
            'filter' => array(1 => "P", 2 => "PL", 3 => "HP", 4 => "LOP", 5 => "HO", 6 => "SUN", 7 => "2P", 8 => "SP"),
        ),
        array(
            'name' => 'att_date',
            'value' => 'Yii::app()->dateFormatter->format("d-MMM-y",strtotime($data->att_date))',
            //'filter' => CHtml::activeTextField($model, 'att_date', array("placeholder" => "yyyy-mm-dd"))
        ),
        'comments',
        array(
            'name' => 'created_by',
            'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
        ),
        array(
            'name' => 'decision_by',
            'header' => 'Approved/Rejected by',
            'value' => 'isset($data->decisionBy->first_name)?$data->decisionBy->first_name." ".$data->decisionBy->last_name:""',
        ),
        array(
            'name' => 'status',
            'value' => '$data->shift_status($data->status)',
            'type' => 'raw',
            'filter' => array(0 => "Pending", 1 => "Approved", 2 => "Rejected"),
        ),
        
    ),
));
?>
</div>
<script>
    /* approve action */
    $(document).ajaxComplete(function() {
        $(".grid-view input[name='attendanceRequest[att_date]']").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.hasDatepicker').attr('autocomplete', 'off');
    });

    $(document).ready(function() {
        $(".grid-view input[name='attendanceRequest[att_date]']").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.hiden').find('input[name="ids[]"]').remove();
    });

    $(".reason").keyup(function() {
        var val = $(this).val()
        $(this).val(val.toUpperCase())
    })

    $(".attendanceaction").click(function() {

        $('.attendanceaction').attr('disabled', true);

        var req = $(this).val();
        var reason = $('.reason').val();
        var all = [];

        $('input[name="ids[]"]:checked').each(function() {
            all.push(this.value);
        });

        if (all != '') {
            if (reason == '') {
                alert("please add comment");
                $('.attendanceaction').attr('disabled', false);
            } else {

                if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

                    $('#loadernew').show();

                    $.ajax({
                        method: "post",
                        dataType: "json",
                        data: {
                            id: all,
                            req: req,
                            reason: reason
                        },
                        url: '<?php echo Yii::app()->createUrl("/attendanceRequest/attendanceaction") ?>',

                        //}).done(function(ret){
                        success: function(ret) {

                            $('#loadernew').hide();
                            $('.attendanceaction').attr('disabled', false);

                            if (ret.error != '') {
                                $('#actionmsg').addClass('alert alert-danger');
                                $('#actionmsg').html(ret.error);
                                window.setTimeout(function() {
                                    location.reload()
                                }, 3000)
                            } else {
                                $('#actionmsg').addClass('alert alert-success');
                                $('#actionmsg').html(ret.msg);
                                window.setTimeout(function() {
                                    location.reload()
                                }, 3000)

                            }
                        }

                    });
                } else {

                    $('#loadernew').hide();
                    $('.attendanceaction').attr('disabled', false);


                }
            }
        } else {

            alert('Please select Item');
            $('.attendanceaction').attr('disabled', false);

        }
    });

    $(document).on('click', '#attendance-requests-grid_c0_all', function() {
        var checked = this.checked;
        $("input[name='ids[]']").each(function() {
            if (checked == true) {
                $(this).find("input[name='ids']").prop('checked', true);
                $(this).parent('span').addClass('checked');
            } else {
                $(this).find("input[name='ids']").prop('checked', false);
                $(this).parent('span').removeClass('checked');
            }

        });
    });
</script>
