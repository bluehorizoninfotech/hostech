<?php
/* @var $this AttendanceRequestsController */
/* @var $model AttendanceRequests */

$this->breadcrumbs=array(
	'Attendance Requests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AttendanceRequests', 'url'=>array('index')),
	array('label'=>'Create AttendanceRequests', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#attendance-requests-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Attendance Requests</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form display-none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'attendance-requests-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'att_id',
		'user_id',
		'att_date',
		'comments',
		'att_entry',
		'created_by',
		/*
		'created_date',
		'type',
		'status',
		'decision_by',
		'decision_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
