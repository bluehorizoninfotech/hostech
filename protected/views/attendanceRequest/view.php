<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<?php
/* @var $this attendanceRequestController */
/* @var $model attendanceRequest */

$this->breadcrumbs = array(
	'Attendance Requests' => array('index'),
	$model->att_id,
);

$this->menu = array(

	array('label' => 'Manage attendanceRequest', 'url' => array('index')),
);
?>
<div class="attendance-view-sec">
<?php if (in_array('/approvals/attendanceprivileges', Yii::app()->session['pmsmenuauthlist'])) { ?>
	<?php if ($model->status == 0) { ?>

		<div align="center" class="margin-top-25">

			&nbsp; &nbsp; &nbsp;
			<textarea name="reason" class="reason margin-zero height-37 width-192"></textarea>

			<input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approve' /> &nbsp; &nbsp;
			<input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Reject' /> &nbsp; &nbsp;

		</div>
	<?php } else if ($model->status == 1) { ?>
		<div align="center"class="margin-top-25">

			<textarea name="reason" class="reason margin-zero height-37 width-192"></textarea>

			<input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Reject' /> &nbsp; &nbsp;
		</div>
	<?php } else { ?>
		<div align="center" class="margin-top-25">

			<textarea name="reason" class="reason margin-zero margin-zero height-37 width-192"></textarea>

			<input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approve' /> &nbsp; &nbsp;
		</div>
<?php }
} ?>

<div id="actionmsg"> </div>

<h1>attendanceRequest Details</h1>



<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(

		array(
			'name' => 'user_id',
			'value' => $model->usrId->first_name . " " . $model->usrId->last_name,
		),
		array(
			'name' => 'att_entry',
			'value' => $model->attEntry->short_note,
		),
		'att_date',
		'comments',
		array(
			'name' => 'created_by',
			'value' => $model->createdBy->first_name . " " . $model->createdBy->last_name,
		),
		'created_date',

		array(
			'name' => 'decision_by',
			'value' => isset($model->decisionBy->first_name) ? $model->decisionBy->first_name . " " . $model->decisionBy->last_name : null,
		),
		'decision_date',
		'reason',
		array(
			'name' => 'status',
			'value' => $model->shift_status($model->status),
			'type' => 'raw'
		),
	),
)); ?>

<!-- include -->
<?php
Yii::app()->clientScript->registerScript("", "");
?>
<!-- end -->
</div>
<script type='text/javascript'>
	$(document).ready(function() {


		$(".reason").keyup(function() {
			var val = $(this).val()
			$(this).val(val.toUpperCase())
		})


		$('.shift_action').click(function() {
			var req = $(this).val();
			var id = <?php echo $model->att_id ?>;
			var reason = $('.reason').val();
			var type = 0;

			if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

				$('#load').show();
				$.ajax({
					method: "POST",
					dataType: "json",
					url: '<?php echo Yii::app()->createUrl('/attendanceRequest/ShiftAction') ?>',
					data: {
						id: id,
						req: req,
						type: type,
						reason: reason
					}
				}).done(function(ret) {

					$('#load').hide();
					if (ret.msg != '') {

						$('#actionmsg').addClass('successaction');
						$('#actionmsg').html(ret.msg);
						$('.shift_action').hide();
						$('.reason').hide();
					} else if (ret.error != '') {

						$('#actionmsg').addClass('erroraction');
						$('#actionmsg').html(ret.error);

					}


				});
			}
		});
	});
</script>
