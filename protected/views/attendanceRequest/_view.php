<?php
/* @var $this AttendanceRequestsController */
/* @var $data AttendanceRequests */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->att_id), array('view', 'id'=>$data->att_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_date')); ?>:</b>
	<?php echo CHtml::encode($data->att_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comments')); ?>:</b>
	<?php echo CHtml::encode($data->comments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_entry')); ?>:</b>
	<?php echo CHtml::encode($data->att_entry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decision_by')); ?>:</b>
	<?php echo CHtml::encode($data->decision_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decision_date')); ?>:</b>
	<?php echo CHtml::encode($data->decision_date); ?>
	<br />

	*/ ?>

</div>