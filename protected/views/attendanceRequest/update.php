<?php
/* @var $this AttendanceRequestsController */
/* @var $model AttendanceRequests */

$this->breadcrumbs=array(
	'Attendance Requests'=>array('index'),
	$model->att_id=>array('view','id'=>$model->att_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AttendanceRequests', 'url'=>array('index')),
	array('label'=>'Create AttendanceRequests', 'url'=>array('create')),
	array('label'=>'View AttendanceRequests', 'url'=>array('view', 'id'=>$model->att_id)),
	array('label'=>'Manage AttendanceRequests', 'url'=>array('admin')),
);
?>

<h1>Update AttendanceRequests <?php echo $model->att_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>