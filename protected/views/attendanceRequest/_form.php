<div class="form-attendance-request-sec">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'attendance-requests-form',
    //'enableAjaxValidation'=>true,
)); ?>


	<?php // echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'user_id'); ?>
			<?php
			$company = "";
		if (isset(Yii::app()->user->site_userids) && !empty(Yii::app()->user->site_userids)) {
			 $company = ' and emp_id in ('.Yii::app()->user->site_userids.')';

		}
		if(isset(Yii::app()->user->project_site) && $company==''){
				$company = ' and emp_id in (0)';

		}

			$userarraysql = "SELECT emp_id, concat_ws(' ',firstname,lastname) as firstname FROM `pms_employee` WHERE status = 28 and system_user!=1  $company order by firstname ";
			$userarray = Yii::app()->db->createCommand($userarraysql)->queryAll();
			$users =  CHtml::listData($userarray, 'emp_id', 'firstname');

			echo $form->dropDownList($model, 'user_id',$users,array('empty'=>'Select Employee','class' => 'form-control'));
			?>
			<?php echo $form->error($model,'user_id'); ?>
		</div>
		<div class="col-md-2">
			<?php echo $form->labelEx($model,'att_date'); ?>
			<?php echo $form->textField($model,'att_date', array('class' => 'form-control','id'=> 'datepic', 'placeholder' => 'Date','autocomplete'=>'off')); ?>
			<?php echo $form->error($model,'att_date'); ?>
		</div>
		<div class="col-md-2">
			<?php echo $form->labelEx($model, 'att_entry'); ?>
			<?php
				echo $form->dropDownList($model, 'att_entry', CHtml::listData(Legends::model()->findAll(array('order' => 'leg_id','condition'=>'leg_id!=0','condition'=>'leg_id!=9')), 'leg_id', 'short_note'), array('empty' => 'Select', 'class' => 'form-control'));

			?>
			<?php echo $form->error($model, 'att_entry'); ?>
		</div>
		<div class="col-md-4">
			<?php echo $form->labelEx($model, 'comments'); ?>
			<?php echo $form->textField($model, 'comments', array('class' => 'form-control','autocomplete'=>'off')); ?>
			<?php echo $form->error($model, 'comments'); ?>
		</div>
		<div class="col-md-2 buttons">
			<label>&nbsp;</label>
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
			<?php echo CHtml::Button('Clear', array('class' => 'btn btn-default reset')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->



<?php
Yii::app()->clientScript->registerScript('myjquery', "
	    $(document).ready(function () {
	        $('#datepic').datepicker({autoclose: true,dateFormat: 'yy-mm-dd'});


	    });

			$('input[type=text], textarea').keyup(function () {
					$(this).val($(this).val().toUpperCase());

			 });

			 $( '.reset' ).click(function() {
				location.href = '".Yii::app()->createAbsoluteUrl('attendanceRequests/index')."';
		 });



	");
?>

		</div>

