<?php
/* @var $this DeviceAccessidsController */
/* @var $model DeviceAccessids */

$this->breadcrumbs=array(
	'Device Accessids'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DeviceAccessids', 'url'=>array('index')),
	array('label'=>'Manage DeviceAccessids', 'url'=>array('admin')),
);
?>

<h1>Create DeviceAccessids</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>