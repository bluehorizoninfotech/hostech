<?php
/* @var $this DeviceAccessidsController */
/* @var $model DeviceAccessids */

$this->breadcrumbs=array(
	'Device Accessids'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DeviceAccessids', 'url'=>array('index')),
	array('label'=>'Create DeviceAccessids', 'url'=>array('create')),
	array('label'=>'View DeviceAccessids', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DeviceAccessids', 'url'=>array('admin')),
);
?>

<h1>Update DeviceAccessids <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>