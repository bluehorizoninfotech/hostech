<?php
/* @var $this DeviceAccessidsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Device Accessids',
);

$this->menu=array(
	//array('label'=>'Create DeviceAccessids', 'url'=>array('create')),
	//array('label'=>'Manage DeviceAccessids', 'url'=>array('admin')),
);
?>
<div class="index-device-accessids-sec">
<ul class="nav nav-tabs">
    <li><?php echo CHtml::link('Update User',array('users/update','id'=> $userid)); ?></li>
    <li  class="active"><?php echo CHtml::link('Manage Access IDs',array('deviceAccessids/index','userid'=> $userid)); ?></li>
</ul>

<br clear='all' />
<div class="row margin-0 custom-form">
	<div class="form pull-left light-gray-border-right-2 padding-right-20 col-md-3 col-sm-5 col-xs-12">
		<h1> <?php echo ($model2->isNewRecord ? 'Add New Access Id' : 'Update Access Id') ?></h1>
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id'=>'device-accessids-form',
			'enableAjaxValidation' => false,
		));
		?>

		<div class="row">
			<?php 		
			$devices = CHtml::listData(PunchingDevices::model()->findAll(array(
				'select' => array('device_id, device_name'),
				'order' => 'device_name',
							'condition' => 'nodevice_status IS NULL',
				'distinct' => true
			)), 'device_id', 'device_name');
			
		echo $form->labelEx($model2, 'deviceid'); ?>
		<?php echo $form->dropDownList($model2, 'deviceid', $devices, array('class' => 'form-control','empty' => 'Select Device')); ?>
		<?php //echo CHtml::dropDownList('device_name', $model->deviceid, $devices, array('empty' => 'Select', 'style' => 'width: 250px;')); ?> 
		<?php echo $form->error($model2,'deviceid'); ?>          
		</div>

		<div class="row">
			<?php echo $form->labelEx($model2, 'accesscard_id'); ?>
			<?php echo $form->textField($model2, 'accesscard_id', array('class' => 'form-control','size' => 29)); ?>
			<?php echo $form->error($model2, 'accesscard_id'); ?>
		</div>
		<?php echo $form->hiddenField($model,'userid',array('value'=>$userid)); ?>
		
		

		<div class="row buttons">
			<?php echo CHtml::submitButton($model2->isNewRecord ? 'Add' : 'Update', array('class' => 'btn green')); ?>
		</div>


		<?php $this->endWidget(); ?>

	</div><!-- form -->

	<div class="pull-left col-md-9 col-sm-7 col-xs-12 padding-left-20">
		<h1 class='pull-left'>Manage Access IDs</h1>
		<?php if (Yii::app()->user->hasFlash('success')) { ?>

		<div class="flash-success margin-left-20 padding-top-0 padding-bottom-0 padding-right-20 padding-left-20 green-color-border green-color font-15 pull-left">
				<?php echo Yii::app()->user->getFlash('success'); ?>
			</div>
			<?php
		}
		?>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'device-accessids-grid',
					'dataProvider'=>$model->search($userid),
					'itemsCssClass' => 'table table-bordered new-table',
					
					//'filter'=>$model,
					'columns'=>array(
						//'id',
						array(
						'header' => 'S No.',
						'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
					),
					/*
					// commented 
					array(
							'name' =>'deviceid',
							'value' => '(isset($data->devices0->device_name) ? $data->devices0->device_name : "" )',
						
						),
					*/	
										/*	array(
							'name' =>'User Name',
							'value' => '$data->user->first_name',
						
						), */
									// 'deviceid',
										array(
											'name' => "deviceid",
											'value' => '$data->devicename($data->deviceid)',
										),
										/*
						array(
							'name' =>'Device',
							'value' => '(isset($data->devices) ? $data->devices->device_name : "" )',
						
						),
						*/
						'accesscard_id',
						array(
							'class'=>'CButtonColumn',
							'template' =>'{update}{delete}',
							'buttons' => array(
								'update' => array
									(
									'label' =>'',	
									'url' => 'Yii::app()->createUrl("deviceAccessids/index", array("userid"=>$data->userid,"id"=>$data->id))',
									'imageUrl' => false,
									'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit',),

								),
								'delete' => array
									(
									'label' =>'',		
									'imageUrl' => false,
									'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete',),

								),
							),
							/*'buttons' => array(
								'deletedevice' => array(
									'label' => 'Delete',
									'url' => 'Yii::app()->createUrl("employmentDetails/deletedevice", array("id"=>$data->id))',
									'options' => array('class' => 'actionitem deleteicon'),
									'imageUrl' => Yii::app()->theme->baseUrl . '/images/icons.png',
								),
								'editicon' => array(
									'label' => 'Update',
									'url' => 'Yii::app()->createUrl("employmentDetails/update", array("id" => $data->userid,"devid"=>$data->id))',
									'options' => array('class' => 'actionitem editicon'),
									'imageUrl' => Yii::app()->theme->baseUrl . '/images/icons.png',
								),
								
							), */

							
						),
					),
				)); ?>

	</div>
</div>
			</div>

