<?php
/* @var $this DeviceAccessidsController */
/* @var $model DeviceAccessids */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'device-accessids-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
		<?php echo $form->error($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deviceid'); ?>
		<?php echo $form->textField($model,'deviceid'); ?>
		<?php echo $form->error($model,'deviceid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accesscard_id'); ?>
		<?php echo $form->textField($model,'accesscard_id'); ?>
		<?php echo $form->error($model,'accesscard_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->