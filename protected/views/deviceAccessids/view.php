<?php
/* @var $this DeviceAccessidsController */
/* @var $model DeviceAccessids */

$this->breadcrumbs=array(
	'Device Accessids'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DeviceAccessids', 'url'=>array('index')),
	array('label'=>'Create DeviceAccessids', 'url'=>array('create')),
	array('label'=>'Update DeviceAccessids', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DeviceAccessids', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DeviceAccessids', 'url'=>array('admin')),
);
?>

<h1>View DeviceAccessids #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userid',
		'deviceid',
		'accesscard_id',
	),
)); ?>
