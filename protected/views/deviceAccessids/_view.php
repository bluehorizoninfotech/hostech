<?php
/* @var $this DeviceAccessidsController */
/* @var $data DeviceAccessids */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deviceid')); ?>:</b>
	<?php echo CHtml::encode($data->deviceid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accesscard_id')); ?>:</b>
	<?php echo CHtml::encode($data->accesscard_id); ?>
	<br />


</div>