<?php
/* @var $this UsersiteController */
/* @var $model Usersite */

$this->breadcrumbs=array(
	'Usersites'=>array('index'),
	'Manage',
);




//echo "<pre>";
//print_r($ulist);
//die;
?>
<h1>Employee Allocation</h1>

<?php
/* @var $this UsersiteController */
/* @var $model Usersite */
/* @var $form CActiveForm */
?>
<form action="" method="post" id="myform" class="form-horizontal">
    
   
    <div id="usrMsg" class='alert alert-danger display-none'>Please select a user!...</div>
    <div id="usrMsg1" class='alert alert-danger display-none'>Please provide site and assigned_date !...</div>
<div id='userlist' class="display-none">
<?php
//echo '<pre>';
echo $res =  (json_encode($ulist));
//echo '</pre>';
?>
</div>  
    

    
    <div class="">
        <div class="row">
            
                <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
    
     $sitedata = '';
           foreach($clientsite as $clienkey => $clienrow){  
           $sitedata.='<option value="'.$clienrow["id"].'" >'.$clienrow['site_name'].'</option>';
           }           
           
           
    $userdata = '';
     foreach($users as $userkey => $userrow){  
           $userdata.='<option value="'.$userrow["userid"].'" >'.$userrow['first_name'].' '.$userrow['last_name'].'</option>';
           }  
    
?>
            <div class="col-md-12 allocation_form">
<!--                <p><strong>Filter by</strong></p>-->
                
                <div id="searchMsg" class='alert alert-danger display-none'>Please provide a search criteria!...</div>
              <div class="col-md-3">
               <label for="Assigned date">Site:</label>
              <select name="site[new1]" id="sitesearch" class="form-control">
                <option value="">Choose Site</option>    
              <?php echo $sitedata; ?>
            </select> 
          </div>
                 <div class="col-md-3">
               <label for="">Name :</label>
              <select name="site[new2]" id="usersearch" class="form-control">
                <option value="">Choose User</option>    
              <?php echo $userdata; ?>
            </select> 
          </div>
                
                 <div class="col-md-3">
               <label for="">( Designation, Employee Code ) :</label>
               <input type="text" name="search" id="search" value="" class="form-control" />
          </div>
                
                 <div class="col-md-3">     
                     <br>
               <input type="button" id="searchBtn" value="Search" class="btn btn-sm blue" />
          </div>
                
                </div> 
            <div class="col-md-12 alloc" id="allsave">
                <h4>Allocation</h4>
                <div class="col-md-3">
                    <label for="site">Site:</label>
                    <select name="site[new]" id="site" class="form-control">
                <option value="">Choose Site</option>    
              <?php echo $sitedata; ?>
            </select>
                </div>
                <div class="col-md-3">
                    <label for="Assigned date">Assigned Date:</label><br>
                    <?php /*
                     //$model->assigned_date = date("Y-m-d");
        echo CHtml::activeTextField($model, 'assigned_date[new]', array("id" => "TimeEntry_entry_date", "size" => "15", 'class'=>'form-control')); ?>                
        <?php
        $this->widget('application.extensions.calendar.SCalendar', array(             
            'inputField' => 'TimeEntry_entry_date',
            'ifFormat' => '%Y-%m-%d',
        ));
                    */ 
                    ?>
                    
                     <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'assigned_date[new]',
                'attribute' => 'assigned_date[new]',
                'model'=>$model,
                'options'=> array(
                  'dateFormat' =>'yy-mm-dd',                  
                  'altFormat' =>'yy-mm-dd',
                  'changeMonth' => true,
                  //'changeYear' => true,                  
                ),
               'htmlOptions'=>array('value'=>'','class'=>'form-control','id'=>'assigned_date')
              )); 
                ?>
                    
                </div>
                
                  <div class="col-md-6"> 
                    <br>
                    <input type="submit" name="submitusersite"  value="Save" class="btn btn-sm blue">
                </div>
                
            </div>
            
            <div class="col-md-12">
                
                </div>
            
        </div>
      
 <div class="row">
      <div class="col-md-12">
          
        
          
<table class="table table-bordered table-striped" id="mytable">
    <thead>
      <tr>
        <th width="2%">   <INPUT type="checkbox" id="select-all" name="chk[]" /></th>
        <th>User</th>
         <th>Employee Code</th>
          <th>Designation</th>
        <th>Site</th>
        <th>Assigned Date</th>
      </tr>
    </thead>
    <tbody id="tablecontent">        
    <?php          
    $i = 0;
   
    foreach ($users as $key => $value){    
    extract($value);
    ?>
    <tr>
    <td><input type="checkbox" name="users1[]" class="chkbox" class="form-control" onclick="check_me(this)" value="<?php echo $userid; ?>"></td>  
    <td><?php echo $first_name.' '.$last_name;?></td>
    <td id="employee_id_<?php echo $userid; ?>"></td>
    <td id="designation_<?php echo $userid; ?>"></td>    
    <td id="site_user_<?php echo $userid; ?>">
    <?php /*
    <select name="site[<?php echo $userid; ?>]" id="site_user_<?php echo $userid; ?>" class="form-control">
    <option value="">Choose Site</option>    
    <?php echo $sitedata; ?>
    </select> 
        */
    ?>
    </td>            
    
    <td id="assigned_date_<?php echo $userid; ?>">  
        
        <?php 
        //$model->assigned_date = date("Y-m-d");
       // echo CHtml::activeTextField($model, 'assigned_date['.$userid.']', array("id" => "TimeEntry_entry_date".$userid."", "size" => "15", 'class'=>'form-control')); ?>                
        <?php /*
        $this->widget('application.extensions.calendar.SCalendar', array(             
            'inputField' => 'TimeEntry_entry_date'.$userid.'',
            'ifFormat' => '%Y-%m-%d',             
        ));
         * 
         */
        ?>
                <?php //echo CHtml::activeTextField($model, 'assigned_date['.$userid.']', array("id" => "TimeEntry_entry_date".$userid."", "size" => "15", 'class'=>'form-control')); ?>                
                <?php /*
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'assigned_date['.$userid.']',
                'attribute' => 'assigned_date['.$userid.']',
                'model'=>$model,
                'options'=> array(
                  'dateFormat' =>'yy-mm-dd',                  
                  'altFormat' =>'yy-mm-dd',
                  'changeMonth' => true,
                  //'changeYear' => true,                  
                ),
               'htmlOptions'=>array('value'=>'','readonly'=>true)
              )); 
                 * 
                 */
                ?>
      </td>
      </tr>     
        <?php
        $i++;
        } ?>
    </tbody>
  </table>
 </div>
 </div>  
    </div>  
</form>


<?php 



Yii::app()->clientScript->registerScript('search', "

$(document).ready(function(){

$('.datepicker').datepicker();
$('#select-all').click(function(){
 //$('#allsave').show();
 var checkboxes = document.getElementsByTagName('input');
     if (this.checked) {
         for (var i = 0; i < checkboxes.length; i++) {                
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;                 
                 $('div.checker span').addClass('checked');
             }
         }
     } else {
        //$('#allsave').hide();
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
                 $('div.checker span').removeClass('checked');
             }
         }
     }
}); 

$('#myform').submit(function(){
  var checkboxes = document.getElementsByTagName('input');
  var site = $('#site').val();
  var assigned_date = $('#assigned_date').val();
  
var flag = 0;
 for (var i = 0; i < checkboxes.length; i++) {                
             if (checkboxes[i].checked) {
             if(site != '' && assigned_date != '' ){
              flag = 1;
              return true;
             }else{
             flag = 2;
             }
             
             }
         }
if(flag == 0 || flag == 2){   
if(flag == 2){
$('#usrMsg').hide();
$('#usrMsg1').show();
}else{
$('#usrMsg').show();
$('#usrMsg1').hide();
}

$('html,body').scrollTop(0);
return false;
} 
});


$('#sitesearch').change(function(){
var val = $(this).val();
$('#searchMsg').hide();
$('#usersearch').val('');
$('#search').val('')
 $.ajax({
      type: 'POST',
      async: false,
      data    : {siteid:val},
      url: '".Yii::app()->createUrl('usersite/showsitedata')."',                    
      success: function (data) {  
      if(data != 2){
      $('#tablecontent').html('');
      $('#tablecontent').html(data);        
      }else{
      location.reload();
      }    
      }
      });
});

$('#usersearch').change(function(){
$('#sitesearch').val('');
$('#searchMsg').hide();
$('#search').val('')
var val = $(this).val();
 $.ajax({
      type: 'POST',
      async: false,
      data    : {userid:val},
      url: '".Yii::app()->createUrl('usersite/showuserdata')."',                    
      success: function (data) {  
      if(data != 2){
      $('#tablecontent').html('');
      $('#tablecontent').html(data);        
      }else{
      location.reload();
      }    
      }
      });
});


var getuserdata = $('#userlist').html(); 
$.each($.parseJSON(getuserdata), function (key, data) { 
//console.log(data)
var uid = data['user_id'];
$('#designation_'+uid).html(data['designation']);
$('#employee_id_'+uid).html(data['employee_id']);
$('#site_user_'+uid ).html(data['site_name']);
$('#assigned_date_'+uid ).html(data['assigned_date']);
});

$('#searchBtn').click(function(){
var site = $('#sitesearch').val();
var user = $('#usersearch').val();
var searchval = $('#search').val();

if(searchval == '' && site == '' && user == ''){
$('#searchMsg').show();
}else{
$('#searchMsg').hide();
$.ajax({
method:'GET',
data:{site:site,user:user,searchval:searchval},
url:'".Yii::app()->createUrl('usersite/showsearchdata')."',
}).done(function(data){
if(data != 2){
$('#tablecontent').html('');
$('#tablecontent').html(data);
}else{
$('#tablecontent').html('<tr><td></td><td>No records found!...</td></tr>');
}
});
}



});

})
");
?>
<script type="text/javascript">
/*function check_me(val){    
if(val.checked){
$('#allsave').show();    
}    
}
*/
</script>