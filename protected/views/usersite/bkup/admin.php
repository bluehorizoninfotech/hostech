<?php
/* @var $this UsersiteController */
/* @var $model Usersite */

$this->breadcrumbs=array(
	'Usersites'=>array('index'),
	'Manage',
);




//echo "<pre>";
//print_r($ulist);
//die;
?>
<h1>Manage User Sites</h1>
<?php
/* @var $this UsersiteController */
/* @var $model Usersite */
/* @var $form CActiveForm */
?>
<form action="" method="post" id="myform">
    <div id="usrMsg" class='alert alert-danger display-none'>Please select a user!...</div>
<div id='userlist' class="display-none">
<?php
//echo '<pre>';
echo $res =  (json_encode($ulist));
//echo '</pre>';
?>
</div>  
    
    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>
    
<table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th width="2%">   <INPUT type="checkbox" id="select-all" name="chk[]" /></th>
        <th>User</th>
        <th>Site</th>
        <th>Assigned Date</th>
      </tr>
    </thead>
    <tbody>
        
       <?php
           $sitedata = '';
           foreach($clientsite as $clienkey => $clienrow){  
           $sitedata.='<option value="'.$clienrow["id"].'" >'.$clienrow['site_name'].'</option>';
           }           
        $i = 0;
 foreach ($users as $key => $value){    
     extract($value);
        ?>
      <tr>
        <td><input type="checkbox" name="users1[]" class="chkbox" class="form-control" value="<?php echo $userid; ?>"></td>  
        <td><?php echo $first_name.' '.$last_name;?></td>
        <td>
            <select name="site[<?php echo $userid; ?>]" id="site_user_<?php echo $userid; ?>" class="form-control">
                <option value="">Choose Site</option>    
              <?php echo $sitedata; ?>
            </select> </td>
            
            <td>                
        <?php 
        $model->assigned_date = date("Y-m-d");
        echo CHtml::activeTextField($model, 'assigned_date['.$userid.']', array("id" => "TimeEntry_entry_date".$userid."", "size" => "15", 'class'=>'form-control')); ?>                
        <?php
        $this->widget('application.extensions.calendar.SCalendar', array(             
            'inputField' => 'TimeEntry_entry_date'.$userid.'',
            'ifFormat' => '%Y-%m-%d',
        ));
        ?>
      </td>
      </tr>     
        <?php
        $i++;
        } ?>
    </tbody>
  </table>

<div>
    <input type="submit" name="submitusersite"  value="Save" class="btn btn-primary">
</div>
</form>
<?php 



Yii::app()->clientScript->registerScript('search', "

$(document).ready(function(){

$('.datepicker').datepicker();

$('#select-all').click(function(){
 var checkboxes = document.getElementsByTagName('input');
     if (this.checked) {
         for (var i = 0; i < checkboxes.length; i++) {                
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;                 
                 $('div.checker span').addClass('checked');
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
                 $('div.checker span').removeClass('checked');
             }
         }
     }
}); 

$('#myform').submit(function(){
  var checkboxes = document.getElementsByTagName('input');
var flag = 0;
 for (var i = 0; i < checkboxes.length; i++) {                
             if (checkboxes[i].checked) {
              flag = 1;
              return true;
             }
         }
if(flag == 0){   
$('#usrMsg').show();
$('html,body').scrollTop(0);
return false;
} 
});



var getuserdata = $('#userlist').html(); 
$.each($.parseJSON(getuserdata), function (key, data) { 
//console.log(data)
var uid = data['user_id'];
var sitid = data['site_id'];
var assigned_date = data['assigned_date'];
$('#site_user_'+uid ).val(sitid);
$('#TimeEntry_entry_date'+uid ).val(assigned_date);
});

})
");
?>
