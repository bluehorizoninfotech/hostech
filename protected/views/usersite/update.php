<?php
/* @var $this UsersiteController */
/* @var $model Usersite */

$this->breadcrumbs=array(
	'Usersites'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Usersite', 'url'=>array('index')),
	array('label'=>'Create Usersite', 'url'=>array('create')),
	array('label'=>'View Usersite', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Usersite', 'url'=>array('admin')),
);
?>

<h1>Update Usersite <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>