<?php
/* @var $this UsersiteController */
/* @var $model Usersite */

$this->breadcrumbs=array(
	'Usersites'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Usersite', 'url'=>array('index')),
	array('label'=>'Create Usersite', 'url'=>array('create')),
	array('label'=>'Update Usersite', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Usersite', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Usersite', 'url'=>array('admin')),
);
?>

<h1>View Usersite #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'site_id',
		'assigned_date',
	),
)); ?>
