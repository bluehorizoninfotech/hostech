<?php
/* @var $this UsersiteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Usersites',
);

$this->menu=array(
	array('label'=>'Create Usersite', 'url'=>array('create')),
	array('label'=>'Manage Usersite', 'url'=>array('admin')),
);
?>

<h1>Usersites</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
