<?php
/* @var $this UsersiteController */
/* @var $model Usersite */

$this->breadcrumbs=array(
	'Usersites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Usersite', 'url'=>array('index')),
	array('label'=>'Manage Usersite', 'url'=>array('admin')),
);
?>

<h1>Create Usersite</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>