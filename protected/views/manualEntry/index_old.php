<?php
/* @var $this ManualEntryController */
/* @var $model ManualEntry */

$this->breadcrumbs = array(
	'Manual Entries' => array('index'),
	'Manage',
);

// $this->menu = array(
// 	array('label' => 'List ManualEntry', 'url' => array('index')),
// 	array('label' => 'Create ManualEntry', 'url' => array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#manual-entry-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Manual Entries</h1>



<?php //  echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form display-none">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php 

   // echo '<pre>';print_r($dataprovider);exit;



$this->widget('zii.widgets.grid.CGridView', array(
	//'id' => 'manual-entry-grid',
	'dataProvider' => $dataprovider,
	//'filter' => $model,
	'itemsCssClass' => 'greytable',
	'columns' => array(
		//array('class' => 'IndexColumn', 'header' => 'Sl.No.', ),
		'entry_id',
	
		array(
			'name' => 'emp_id',
			'value' => 'isset($data->usrId->firstname)?$data->usrId->firstname." ".$data->usrId->lastname:null',
			'filter' => CHtml::listData(Users::model()->findAll(
				array(
					'select' => array('emp_id, concat_ws(" ",firstname,lastname) as firstname '),
					'order' => 'firstname',
                )
			), "emp_id", "firstname")
		),
		'date',
		'comment',
		// array(
		// 	'name' => 'status',
		// 	'value' => '$data->shift_status($data->status)',
		// 	'filter' => array(0 => "Pending", 1 => "Approved", 2 => "Rejected"),

		// ),
		
		// 'decision_by',
		/*
		'decision_date',
		'created_by',
		'created_date',
		 */
		array(
			'class' => 'CButtonColumn',
			'template' => '{approve_reject}{view}',
			'buttons' => array(
				'approve_reject' => array(
					'label' => 'Approve/Reject',
					'url' => 'Yii::app()->createUrl("/manualEntry/view", array("id"=>$data->entry_id))',
					'visible' => '$data->status==0',
				),

				'view' => array(
					'label' => 'view',
					'url' => 'Yii::app()->createUrl("/manualEntry/view", array("id"=>$data->entry_id))',
					'visible' => '$data->status!=0',
				)
			),
		),
	),
)); ?>

