<?php
/* @var $this ManualEntryController */
/* @var $model ManualEntry */

$this->breadcrumbs=array(
	'Manual Entries'=>array('index'),
	$model->entry_id=>array('view','id'=>$model->entry_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ManualEntry', 'url'=>array('index')),
	array('label'=>'Create ManualEntry', 'url'=>array('create')),
	array('label'=>'View ManualEntry', 'url'=>array('view', 'id'=>$model->entry_id)),
	array('label'=>'Manage ManualEntry', 'url'=>array('admin')),
);
?>

<h1>Update ManualEntry <?php echo $model->entry_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>