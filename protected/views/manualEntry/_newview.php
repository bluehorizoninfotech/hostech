<?php 

$tblpx = Yii::app()->db->tablePrefix;

if ($index == 0) {
?>
<thead>
    <tr>
	<th><input type="checkbox" class="all"></th>
			<th>Sl No.</th>
			<th>Employee</th>
			<th>Punch Time</th>
			<th>Comment</th>
			<th>Status</th>
			<th>Type</th>
			<th>Created By</th>
			<th>Approved/Rejected by</th>
			<th></th>
	  </tr>
  </thead>
<?php } ?>

<tbody>
	<tr>
		<td>
			<?php if($data['status']==0){?>
			<input value="<?php echo $data['entry_id']?>" data-value="<?php echo $data['type'] ?>" id="" type="checkbox" name="ids[]">
			<?php }?>
		</td>
	   <td><?php echo $widget->dataProvider->getPagination()->currentPage * $widget->dataProvider->getPagination()->pageSize + $index + 1; ?></td>
	   <td><?php 
			
			$user= Users::model()->findByPk($data['emp_id']);
			echo $user['firstname'].' '. $user['lastname'];
	   ?></td>
	   <td><?php echo $data['date']; ?></td>
	   <td><?php echo $data['comment']; ?></td>
	   <td><?php 

	   $status = '';
	   if($data['status']==2){
			$status = '<span style="color:red">Rejected</span>';
	   }else if($data['status'] == 1){
			$status = '<span style="color:green">Approved</span>';
	   }else{
			$status = '<span style="color:#f44336">Pending</span>';
	   }
	   echo $status;
	      
	   ?></td>
	   <td><?php
		 
		 
		if($data['type']=='m'){
			echo 'Manual Punch';
		}else if($data['type'] == 'i'){
			echo 'Ignore Punch';
		}else{
			echo 'Shift Punch';
		}

	   ?></td>
	   <td><?php 
			
			$user= Users::model()->findByPk($data['created_by']);
			echo $user['firstname'].' '. $user['lastname'];
	   ?></td>
	    <td><?php 
			
			$user= Users::model()->findByPk($data['decision_by']);
			echo $user['firstname'].' '. $user['lastname'];
	   ?></td>


		<td>
			<?php if($data['type']=='m'){  ?>
			<a class="view" title="View" href="<?php echo Yii::app()->createUrl("/manualEntry/view&amp;id=") ?><?= $data['entry_id']?>">
			<?php if($data['status']==0||$data['status']==1||$data['status']==2){ ?>Approve/Reject<?php }
			else{ ?><img src="<?= Yii::app()->request->baseUrl ?>/images/view.png" alt="View"><?php } ?></a>
			<?php }
			else if($data['type'] == 'i') { ?> 
				<a class="view" title="View" href="<?php echo Yii::app()->createUrl("/attendance/IgnorePunches/view&amp;id=")?><?= $data['entry_id'] ?>"><?php if ($data['status'] == 0||$data['status'] == 1||$data['status'] == 2) { ?>Approve/Reject<?php } else { ?><img src="<?= Yii::app()->request->baseUrl ?>/images/view.png" alt="View"><?php } ?></a>
			<?php } else{ ?>
				<a class="view" title="View" href="<?php echo Yii::app()->createUrl("/shiftEntry/view&amp;id=")?><?= $data['entry_id'] ?>"><?php if ($data['status'] == 0) { ?>Approve/Reject<?php } else { ?><img src="<?= Yii::app()->request->baseUrl ?>/images/view.png" alt="View"><?php } ?></a>

			<?php } ?>
	    </td>

		

	</tr>


</tbody>

 
