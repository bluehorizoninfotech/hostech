<?php
/* @var $this ManualEntryController */
/* @var $model ManualEntry */

$this->breadcrumbs=array(
	'Manual Entries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ManualEntry', 'url'=>array('index')),
	array('label'=>'Manage ManualEntry', 'url'=>array('admin')),
);
?>

<h1>Create ManualEntry</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>