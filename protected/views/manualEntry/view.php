<script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
<?php
$this->breadcrumbs = array(
    'Manual Entries' => array('index'),
    $model->entry_id,
);

$this->menu = array(
    array('label' => 'Manage Manual Entry Report', 'url' => array('/manualEntryReportTbl/index')),
);
?>

<div id='actionmsg'></div> <br/>

<img src="images/loading.gif" class="width-37 margin-left-562 display-none" id="load" >

<div class="text-center">
    <?php echo CHtml::link('View Punching Details', array('/attendance/IgnorePunches/ManageShiftPunches', 'id' => $model->emp_id, 'date' => date('Y-m-d', strtotime($model->date))), array('target' => '_blank')); ?>
    <?php if ($model->status == 0) { ?>

        <textarea name="reason" class="reason margin-0 margin-bottom-minus-15 height-37 width-192"></textarea> 

        <input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approve' /> &nbsp; &nbsp;
        <input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Reject' /> &nbsp; &nbsp; 

        <?php } elseif ($model->status == 1) {
        ?>

        <textarea name="reason" class="reason margin-0 margin-bottom-minus-15 height-37 width-192"></textarea> 

        <input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Reject' /> &nbsp; &nbsp; 

<?php } else {
    ?>
        <textarea name="reason" class="reason"  style="margin: 0px;     margin-bottom: -15px; height: 37px; width: 192px;"></textarea> 

        <input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approve' /> &nbsp; &nbsp;
<?php } ?>
</div>

<h1>ManualEntry Details</h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'entry_id',
        array(
            'name' => 'emp_id',
            'value' => isset($model->usrId->firstname) ? $model->usrId->firstname . " " . $model->usrId->lastname : null,
        ),
        'date',
        'comment',
        array(
            'name' => 'status',
            'value' => $model->shift_status($model->status),
            'filter' => array(0 => "Pending", 1 => "Approved", 2 => "Rejected"),
        ),
        array(
            'name' => 'decision_by',
            'value' => isset($model->decisionBy->firstname) ? $model->decisionBy->firstname . " " . $model->decisionBy->lastname : null,
        ),
        'decision_date',
        array(
            'name' => 'created_by',
            'value' => isset($model->createdBy->firstname) ? $model->createdBy->firstname . " " . $model->createdBy->lastname : null,
        ),
        'created_date',
        array(
            'name' => 'reason',
            'value' => isset($model->reason) ? $model->reason : null,
        ),
    ),
));
?>

<!-- include -->
<?php
Yii::app()->clientScript->registerScript("", "");
?>
<!-- end -->

<script type='text/javascript'>

    $(document).ready(function () {

        $(".reason").keyup(function () {
            var val = $(this).val()
            $(this).val(val.toUpperCase())
        })

        $('.shift_action').click(function () {
            var req = $(this).val();
            var id =<?php echo $model->entry_id ?>;
            var type=1;
            var reason = $('.reason').val();
            if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

                $('#load').show();
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('/manualEntry/ShiftAction') ?>',
                    data: {id: id, req: req, type: type, reason: reason}
                }).done(function (ret) {

                    $('#load').hide();
                    if (ret.msg != '') {

                        $('#actionmsg').addClass('successaction');
                        $('#actionmsg').html(ret.msg);
                        $('.shift_action').hide();
                    } else if (ret.error != '') {

                        $('#actionmsg').addClass('erroraction');
                        $('#actionmsg').html(ret.error);

                    }


                });
            }
        });
    });
</script>

<style type="text/css">
    .erroraction{color:red;border:1px solid red;padding:5px;}
    .successaction{color:green;border:1px solid green;padding:5px;}
</style>

