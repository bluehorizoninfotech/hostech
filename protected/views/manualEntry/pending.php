<?php
/* @var $this ManualEntryController */
/* @var $model ManualEntry */

$this->breadcrumbs = array(
	'Manual Entries' => array('index'),
	'Manage',
);

// $this->menu = array(
// 	array('label' => 'List ManualEntry', 'url' => array('index')),
// 	array('label' => 'Create ManualEntry', 'url' => array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#manual-entry-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="manualentry-pending-sec">
<div class="row">
<div class="col-md-6">
<h1 class="margin-top-20">Manage Manual/Ignore Punches </h1>
</div>

<div class="col-md-6 float-right">

<?php if(in_array('/settings/projectsite', Yii::app()->session['pmsmenuauthlist'])){ ?>
 <div class="wide form pull-right margin-top-20">
	<?php $this->widget('ProjectSite'); ?>
	<ul class="nav1 nav">
  <li class="nav-item">
    <a class="nav-link " href="<?=Yii::app()->createUrl("/manualEntry/index") ?>">All</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="<?=Yii::app()->createUrl("/manualEntry/approved") ?>">Approved</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="<?=Yii::app()->createUrl("/manualEntry/pending") ?>">Waiting for approval</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="<?=Yii::app()->createUrl("/manualEntry/rejected") ?>" aria-disabled="true">Rejected</a>
  </li>
</ul>
 </div>
<?php } ?>
</div>
</div>
<div class="row margin-bottom-70 margin-left-10">
<div class="pull-left form margin-bottom-5 position-absolute transprant-bg margin-top-10">
				 <?php  if (in_array('/approvals/attendanceprivileges', Yii::app()->session['pmsmenuauthlist'])) { ?>
					 
										 <textarea name="reason" class="reason margin-0 height-37 width-192"></textarea> 
										 <input type="button" name='approve' class='attendanceaction btn btn-success' value='Approve' /> &nbsp;
										 <input type="button" class='attendanceaction btn btn-danger' name='reject' value='Reject' />
										 <img id="loadernew" src="/images/loading.gif" class="width-30 margin-left-159 display-none" />
					 <?php  } ?>
	
		

			 </div>
</div>
				 



<?php //  echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form display-none">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->


<div class="nav-list">

</div>



<?php
	$this->widget('zii.widgets.CListView', array(
		'dataProvider' => $dataprovider,
		 //'viewData' => array(),
		//'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div id="parent2"><table class="greytable " id="fixTable2">{items}</table></div>',
		
		'itemView' => '_newview', 'template' => '<div>{summary}{sorter}<table class="greytable " id="fixTable2">{items}</table>{pager}</div><div id="parent2"></div>',
        
    ));
?>
</div>
<script>

$(document).ready(function() {
    $(".reason").keyup(function() {
        var val = $(this).val()
        $(this).val(val.toUpperCase())
    })
})



$(".all").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

$(".attendanceaction").click(function () {

$('.attendanceaction').attr('disabled',true);

	var req = $(this).val();
	var reason = $('.reason').val();
	var all = [];
	var data_type=[];

	$('input[name="ids[]"]:checked').each(function () {
			all.push(this.value);
			data_type.push($(this).attr('data-value'))

	});

	if (all != '') {
			if(reason == '') {
					alert("please add comment");
					$('.attendanceaction').attr('disabled',false);
			} else {

					if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

							$('#loadernew').show();

							$.ajax({
									method: "post",
									dataType: "json",
									data: {id: all, req: req, reason: reason,data_type:data_type},
									url: '<?php echo Yii::app()->createUrl("/manualEntry/ManualEntryAction") ?>',

									//}).done(function(ret){
									success: function (ret) {

											$('#loadernew').hide();
											$('.attendanceaction').attr('disabled',false);

											if (ret.error != '') {
													$('#actionmsg').addClass('alert alert-danger');
													$('#actionmsg').html(ret.error);
													window.setTimeout(function () {
															location.reload()
													}, 1000)
											} else {
													$('#actionmsg').addClass('alert alert-success');
													$('#actionmsg').html(ret.msg);
													window.setTimeout(function () {
															location.reload()
													}, 1000)

											}
									}

							});
					}else{
						$('#loadernew').hide();
						$('.attendanceaction').attr('disabled',false);
					}
			}
	} else {
			alert('Please select Item');
			$('.attendanceaction').attr('disabled',false);
	}
});

</script>
