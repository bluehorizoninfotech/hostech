<script src="<?php echo $this->customAssets('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js','/js/jquery.min.js');?>"></script>
<?php
if(isset($_GET['id'])){
    $project_id = $_REQUEST['id'];
}else{
    $project_model = Projects::model()->find(['condition'=>'status = 1 ORDER BY updated_date DESC']);
    if(Yii::app()->user->project_id !=""){
        $project_id = Yii::app()->user->project_id;
    }else{
        // $project_id = $project_model['pid'];
        $project_id='';
    }
}

?>
<div class="testchart-test-sec">
<div class="padding-bottom-30">
 <?= CHtml::dropDownList('project_id', $project_id, 
 CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'), 
 array('empty' => 'Choose a project','class'=>'form-control change_project pull-right width-25-percentage',
 'id'=>'project_id')); ?>
 </div>
<div class="main_content">
    <div class="clearfix page_head">    
      <h1></h1>     
        <div class="loaderdiv" style="margin: 0 auto;   width: 64px;">
            <img id="loader" src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" width="50%"/>
        </div>
    </div>  
    <?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger alert-dismissable ">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
   <?php endif; ?>

   
    
    <?=  CHtml::link('Refresh', '', array('class' => 'btn blue right refresh','id'=>'refresh_btn')); ?>
    <?= CHtml::link( 'PDF', $url = $this->createAbsoluteUrl('/projects/downloadpdf',array('id'=>'415')),array('class' => 'btn blue right refresh','id'=>'download_id')) ?>
    <?= CHtml::link( 'Excel', $url = $this->createAbsoluteUrl('/projects/downloadexcel',array('id'=>'')),array('class' => 'btn blue right refresh','id'=>'download_excel')) ?>
    <span class="pull-right span_style">
    On schedule</span>
    <div class="foo in_progress"></div>
    <span class="pull-right span_style">Behind schedule</span>
    <div class="foo delay"></div>
    
    
  
    <div class="row-fluid clearfix">
        <div id="calendertable"></div>
       
     </div>

     <div  id="task_data" class="margin_div clearfix">
    </div>
     
</div>
    </div>
<script>
    $('#project_id').change(function(){
        var project_id = $('#project_id').val();
        $('.loaderdiv').show();


        $.ajax({
                method: "get",
                url: "<?php echo $this->createUrl('site/tasksheet') ?>",
                data: {pid: project_id},
            }).done(function (msg) { 
                            
                $("#task_data").html(msg);
               
                
                
            });

        $.ajax({
            method: "POST",
            data: {
                project_id: project_id
            },
           // "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('test/calenderdata'); ?>',
            success: function(msg) {

              
                $("#calendertable").html(msg);
                $('.loaderdiv').hide();



// another ajax start
//$('.loaderdiv').show();
$.ajax({
            method: "POST",
            data: {
                project_id: project_id
            },
          //  "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('test/taskProgressDetails'); ?>',
            success: function(msg) {

              
              
            }
        });


// ajax end








            }
        });
    
});
    </script>
