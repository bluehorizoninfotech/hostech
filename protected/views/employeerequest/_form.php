<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employeerequest-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">				
            <?php echo $form->labelEx($model,'request_date'); ?>
            <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'request_date', //attribute name
        'language'=>'en-AU',
                'mode'=>'date', //use "time","date" or "datetime" (default)
        'options'=>array(
        'dateFormat'=>'yy-mm-dd') // jquery plugin options
    ));
?>  
            <?php echo $form->error($model,'request_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	
<?php echo $form->hiddenField($model, 'mail_send_status',array('value'=> $model->isNewRecord ? 0 : $model->mail_send_status )); ?>
<?php echo $form->hiddenField($model, 'acknoledge_status',array('value'=>$model->isNewRecord ? 0 : $model->acknoledge_status  )); ?>
        
<?php echo $form->hiddenField($model, 'created_by',array('value'=>Yii::app()->user->id)); ?>
<?php echo $form->hiddenField($model, 'created_date',array('value'=> $model->isNewRecord ? date('Y-m-d') : $model->created_date )); ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->