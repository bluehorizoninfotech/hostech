<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */

$this->breadcrumbs=array(
	'Employeerequests'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "

$('.approve').click(function(){
//var id = $(this).val();
//alert(id)
$('#myModal').modal('show');
var val =  $('#erid').val();
alert(val)
});
");
?>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Approve Employee Request</h4>
        </div>
        <div class="modal-body">
            <div class="row">
               <?php echo CHtml::beginForm(array('employeerequest/approve'),'post'); ?>
                    <input type="hidden" name="erid" id="erid" value="" />
                <div class="col-md-12">
                    <div class="col-md-6">
                        Comment : 
                    </div>
                     <div class="col-md-6">
                         <textarea name="comment" class="form-control" required></textarea>
                    </div>
                </div>
                    <div class="col-md-12">
                    <div class="col-md-6">
                         
                    </div>
                     <div class="col-md-6">
                         <br>
                         <input type="submit" name="approveBtn" class="btn btn-success" value="Approve">
                    </div>
                </div>
                  <?php echo CHtml::endForm(); ?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="clearfix">
    <?php
    if (Yii::app()->user->role == '10') {
        echo CHtml::link('Send New Request', array('employeerequest/create'), array('class' => 'btn blue pull-right'));
    }

    if (Yii::app()->user->role == '1') {
        ?>
        <div class="pull-right"><?php echo CHtml::link('Mail Settings', array('mailSettings/create'), array('class' => 'btn blue')); ?></div>
    <?php }
    ?>
    <h1>Manage Employee requests</h1>
</div>
<br><br>
    <?php if (Yii::app()->user->hasFlash('success')) { ?>

    <div class="alert alert-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
        <?php
    }
    ?>

<?php 
$mailarr[0] = 'Mail Sent'; 
$mailarr[1] = 'Mail Not Sent'; 
$row = 1;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employeerequest-grid',
        'itemsCssClass' => 'table table-bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		/*'id',*/
                 array('class' => 'IndexColumn', 'header' => 'S.No',),
		'title',
		'request_date',
		'message',	
                 array(
                'name' => 'mail_send_status',                 
                'value' => '$data->checkMailstatus($data->mail_send_status)',
                'type' => 'raw',               
                ),	
             array(
                'name' => 'acknoledge_status',
                'filter' => CHtml::listData(Employeerequest::getAckstatus(), 'id', 'title'),    
                'value' => '$data->checkAckstatus($data->acknoledge_status)',
                'type' => 'raw',               
                ),
		/*
		'created_by', */
		'created_date',
		 array(
                'name' => 'ack_by',
                'filter' => CHtml::listData(Employeerequest::getUsers(), 'userid','first_name'),    
                'value' => '(isset($data->ackBy->first_name) ? $data->ackBy->first_name." ".$data->ackBy->last_name : "" )',
                'type' => 'raw', 
                // 'visible' => '($data->ack_by != 0 )'    
                ),
             array(
                'name' => 'ack_date',
                'value' => '(isset($data->ack_date) ? $data->ack_date : "" )',
                'type' => 'raw', 
                // 'visible' => '($data->ack_by != 0 )'    
                ),
                 	array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('class'=>'width-130'),
			'template' => '{update}{mail}{view}{approve}{disapprove}',
                            
                        //'template' => '{update}{delete}',
			'buttons' => array(
               
                'update' => array(
                    'label'=>'',
                    'imageUrl'=>false,
                    //'options' => array('class' => 'editVendor'),
                    'options' => array('class' => 'btn btn-xs btn-default icon-pencil icon-comn','title'=>'Edit'),
                    //'url' => 'Yii::app()->createAbsoluteUrl("purchase/update",array("id"=>$data->id,))',
                    'visible' => '($data->mail_send_status != 1 && $data->checkUserstatus(Yii::app()->user->id,$data->id))',  
                ),
               'mail' => array(
                'label'=>'',
                //'options' => array('class' => 'editVendor'),
                'options' => array('class' => 'fa fa-send','title'=>'Send Mail','id'=>'mailsend','onclick'=>' if (confirm("Do you really want to send mail ? ") == true) {
                    return true;
                } else {
                    return false;
                }',),
                'url' => 'Yii::app()->createAbsoluteUrl("employeerequest/sendmail",array("id"=>$data->id,))',
                'visible' => '($data->	mail_send_status != 1 && $data->checkUserstatus(Yii::app()->user->id,$data->id))',  
                ),
                'view' => array(
                    'label'=>'',
                    'imageUrl'=>false,
                    //'options' => array('class' => 'editVendor'),
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                    //'url' => 'Yii::app()->createAbsoluteUrl("purchase/update",array("id"=>$data->id,))',
                    'visible' => '($data->mail_send_status == 1)',  
                ),            
               'approve' => array(
                    'label'=>'',
                    //'options' => array('class' => 'editVendor'),
                    'options' => array('class' => 'icon-like icon-comn','title'=>'Approve'),                    
                    'url' => 'Yii::app()->createAbsoluteUrl("employeerequest/approve",array("id"=>$data->id))',
                    'visible' => '$data->checkAuthstatus(Yii::app()->user->id,$data->acknoledge_status)',  
                ),
                'disapprove' => array(
                    'label'=>'',
                    //'options' => array('class' => 'editVendor'),
                    'options' => array('class' => 'icon-dislike icon-comn','title'=>'Reject'),
                    'url' => 'Yii::app()->createAbsoluteUrl("employeerequest/disapprove",array("id"=>$data->id,))',
                    'visible' => '$data->checkAuthstatus(Yii::app()->user->id,$data->acknoledge_status)',  
                ),                
                            
                            /* Inactive section end here */  	            
                /*'delete' => array(
                    'label'=>'Delete',
                    'visible'=>'$data->chkitems($data->id)',
                    
                ),*/
            ),
		),
	),
)); ?>

