<?php
/* @var $this EmployeerequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Employeerequests',
);

$this->menu=array(
	array('label'=>'Create Employeerequest', 'url'=>array('create')),
	array('label'=>'Manage Employeerequest', 'url'=>array('admin')),
);
?>

<h1>Employeerequests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
