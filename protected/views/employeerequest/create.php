<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */

$this->breadcrumbs=array(
	'Employeerequests'=>array('index'),
	'Create',
);


?>

<h1>Create Employee request</h1>

<ol class="breadcrumb">
    <li><?php echo CHtml::link('Employee Request',array('employeerequest/admin')); ?> / </li>
    <li>Create</li>
          
  </ol>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>