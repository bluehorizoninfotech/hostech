<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employeerequest-form',
	'enableAjaxValidation'=>false,
)); ?>
    <h3>Approve Request</h3>
	
	<div class="row">
                <?php $model->ack_message = ''; ?>
		<?php echo $form->labelEx($model,'ack_message'); ?>
		<?php echo $form->textArea($model,'ack_message',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'ack_message'); ?>
	</div>

        <?php echo $form->hiddenField($model, 'ack_by',array('value'=>Yii::app()->user->id)); ?>
        <?php echo $form->hiddenField($model, 'ack_date',array('value'=> date('Y-m-d'))); ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('onclick'=> "if (confirm('Do you want to approve this request ? ') == true) {
        return true;
    } else {
        return false;
    }")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->