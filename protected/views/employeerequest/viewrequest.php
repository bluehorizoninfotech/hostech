<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */

$this->breadcrumbs=array(
	'Employeerequests'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('employeerequest-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Employee requests</h1>


 <?php
   //$createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
   //echo CHtml::link('Add', '', array('class' => 'btn btn-primary', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
   
   ?>
<?php echo CHtml::link('Send New Request',array('employeerequest/create'),array('class' => 'btn btn-primary')); ?>

<div class="pull-right"><?php echo CHtml::link('Mail Settings',array('mailSettings/create'),array('class' => 'btn btn-success')); ?></div>

<br><br>
    <?php if (Yii::app()->user->hasFlash('success')) { ?>

    <div class="alert alert-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
        <?php
    }
    ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employeerequest-grid',
        'itemsCssClass' => 'table table-bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		/*'id',*/
		'title',
		'request_date',
		'message',	
                 array(
                'name' => 'mail_send_status',
                'value' => '$data->checkMailstatus($data->mail_send_status)',
                'type' => 'raw',               
                ),	
             array(
                'name' => 'acknoledge_status',
                'value' => '$data->checkAckstatus($data->acknoledge_status)',
                'type' => 'raw',               
                ),
		/*
		'created_by', */
		'created_date',
		
                 	array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:130px;'),
			'template' => '{update}{mail}{view}',
                        //'template' => '{update}{delete}',
			'buttons' => array(
               
                'update' => array(
                    'label'=>'Edit',
                    //'options' => array('class' => 'editVendor'),
                    'options' => array('class' => 'btn btn-xs btn-default '),
                    //'url' => 'Yii::app()->createAbsoluteUrl("purchase/update",array("id"=>$data->id,))',
                    'visible' => '($data->mail_send_status != 1)',  
                ),
               'mail' => array(
                'label'=>'Send Mail',
                //'options' => array('class' => 'editVendor'),
                'options' => array('class' => 'fa fa-send','id'=>'mailsend','onclick'=>' if (confirm("Do you really want to send mail ? ") == true) {
        return true;
    } else {
        return false;
    }',),
                'url' => 'Yii::app()->createAbsoluteUrl("employeerequest/sendmail",array("id"=>$data->id,))',
                'visible' => '($data->	mail_send_status != 1)',  
                ),
                'view' => array(
                    'label'=>'View',
                    //'options' => array('class' => 'editVendor'),
                    'options' => array('class' => 'btn btn-xs btn-default '),
                    //'url' => 'Yii::app()->createAbsoluteUrl("purchase/update",array("id"=>$data->id,))',
                    'visible' => '($data->mail_send_status == 1)',  
                ),            
               
                            
                            /* Inactive section end here */  	            
                /*'delete' => array(
                    'label'=>'Delete',
                    'visible'=>'$data->chkitems($data->id)',
                    
                ),*/
            ),
		),
	),
)); ?>
