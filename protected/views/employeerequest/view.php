<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */

$this->breadcrumbs=array(
	'Employeerequests'=>array('index'),
	$model->title,
);

//$this->menu=array(
//	array('label'=>'List Employeerequest', 'url'=>array('index')),
//	array('label'=>'Create Employeerequest', 'url'=>array('create')),
//	array('label'=>'Update Employeerequest', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Delete Employeerequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage Employeerequest', 'url'=>array('admin')),
//);
?>

<h1>View Employee request <?php //echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		/*'id',*/
		'title',
		'request_date',
		'message',
		 array(
                'label' => 'mail_send_status',
                'value' => $model->checkMailstatus($model->mail_send_status),
                'type' => 'raw',               
                ),	
             array(
                'label' => 'acknoledge_status',
                'value' => $model->checkAckstatus($model->acknoledge_status),
                'type' => 'raw',               
                ),
		array(
                'label' => 'created_by',
                'value' => $model->createdBy->first_name.' '.$model->createdBy->last_name,
                'type' => 'raw',               
                ),
		'created_date',
                array(
                'name' => 'ack_message',
                'value' => ($model->ack_message != '' && $model->ack_message != 0 ) ? "" : $model->ack_message ,
                'type' => 'raw',               
                ),
            array(
                //'label' => 'ack_by',
                'name'=> 'ack_by',
                'value' => (isset($model->ackBy->first_name) ? $model->ackBy->first_name." ".$model->ackBy->last_name : "" ),
                'type' => 'raw',               
                ),
            array(
                'name' => 'ack_date',
                'value' => (isset($model->ack_date) ? $model->ack_date : "") ,
                'type' => 'raw',               
                )
            
	),
)); ?>
