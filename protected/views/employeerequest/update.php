<?php
/* @var $this EmployeerequestController */
/* @var $model Employeerequest */

$this->breadcrumbs=array(
	'Employeerequests'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);


?>

<h1>Update Employee request </h1>

<ol class="breadcrumb">
    <li><?php echo CHtml::link('Employee Request',array('employeerequest/admin')); ?> / </li>
    <li>Update</li>
          
  </ol>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>