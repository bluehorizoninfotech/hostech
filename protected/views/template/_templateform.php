<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="row">
            <div class="col-md-6">



                <?php echo $form->labelEx($model, 'template_name'); ?>
                <?php echo $form->textField($model, 'template_name', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'template_name'); ?>

            </div>

        </div>



        <div class="row">
            <div id="" class="participants_data participants">

                <div class="col-md-3">

                    <label for="Item" class="required">Item <span class="required">*</span></label>
                    <input class="form-control item" name="Template[item_name][]" id="Template_item_name" type="text" required>
                </div>

                <div class="col-md-3">

                    <label for="Ref_column" class="required">Ref column <span class="required">*</span></label>
                    <input class="form-control ref-column uppercase" name="Template[reference_column][]" id="Template_reference_column0" type="text" required>
                    <div class="errorMessage display-none"></div>
                </div>


                <div class="col-md-4 padding-bottom-10">
                    <label for="Purchaseitem" class="required">Purchase Item <span class="required">*</span></label>

                    <select name="template_purchase_item[0][]" id="template_purchase_item" class="form-control js-example-basic-multiple purchase_items " multiple="multiple" required>
                        <?php

                        foreach ($account_items as $key => $value) {
                        ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                        <?php
                        }
                        ?>
                    </select>

                </div>
            </div>
            <div id="container"></div>
            <br>
            <input type="button" id="add_new" value="Add"  class="btn btn-secondary" onClick="addInput();">
            <br>
        </div>
        <div class="row text-right">
            <div class="col-sm-12">
                <?php echo CHtml::SubmitButton('Create', array('class' => 'btn blue save_btn create-template margin-left-30','id' => 'save_btn', 'name' => 'save_btn'), array()); ?>


            </div>
        </div>


        <!-- <div class="btn-holder">
    <p id="add_field"><a><span>&raquo; Add More</span></a></p>
    <p id="add_field_remove"><a><span>&raquo; Remove</span></a></p>
</div> -->

    </div>
</div>