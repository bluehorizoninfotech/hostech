<?php
/* @var $this TemplateController */
/* @var $model Template */

$this->breadcrumbs=array(
	'Templates'=>array('index'),
	$model->template_id,
);

$this->menu=array(
	array('label'=>'List Template', 'url'=>array('index')),
	array('label'=>'Create Template', 'url'=>array('create')),
	array('label'=>'Update Template', 'url'=>array('update', 'id'=>$model->template_id)),
	array('label'=>'Delete Template', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->template_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Template', 'url'=>array('admin')),
);
?>

<h1>View Template #<?php echo $model->template_name; ?></h1>
<div>
    <div class="media">

        <div class="media-body">
            <div class="row">
                <div class="col-md-4 col-sec">

                    <div><b>Name:</b><span><?php echo $model->template_name ?></span></div>
                    <div><b> Column Ref:</b><span><?php echo($model->status == 1 ? "Used" : "Active"); ?></span></div>
                   
                    

                </div>
                <div class="col-md-4 col-sec">
                   
                    <div><b> Created By:</b><span><?= $model->createdBy->first_name . " " . $model->createdBy->last_name ?></span></div>
                    <div><b> Created Date:</b><span><?php echo  date('d-M-y', strtotime($model->created_date)); ?></span></div>
                </div>

            </div>
        </div>
    </div>
</div>
<br><br>
<hr />
<div class="row">
    <div class="col-md-6">
        <table cellpadding="3" cellspacing="0" class="timehours table-bordered">
            <thead>
                <tr class="tr_title">
                    <th>Sl no</th>
                    <th>ITEM</th>
                    <th>Ref COLUMN</th>
                    <th>PURCHASE ITEM</th>
                   
                </tr>
            </thead>
            <?php
            foreach ($data as $key => $item) {
				$purchase_items = Controller::getMaterialNames($item->purchase_item);
            ?>
                <tr>
                    <td width="10%"><?= $key + 1?></td>
                    <td width="40%"><?= $item->item_name?></td>
                    <td><?= $item->ref_column ?></td>
                    <td width="40%">
						
						<?=  $purchase_items  ?>
					</td>
                    
		

                </tr>
            <?php
            }

            ?>

        </table>
    </div>

</div>


