
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>

<?php
/* @var $this TemplateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	//'Templates',
);

$this->menu=array(
	array('label'=>'Create Template', 'url'=>array('create')),
	array('label'=>'Manage Template', 'url'=>array('admin')),
);
?>

<div class="add link pull-right">
	<?php

	echo CHtml::link('New Template', array('template/create'), array('class' => 'btn blue'));
	?>
</div>
<h1>Templates</h1>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
	// 'id' => 'photo-punch',
	'id' => 'site-meetings-grid',
	'dataProvider' => $model->search(),
	'ajaxUpdate' => false,
	'itemsCssClass' => 'table table-bordered photo-punch-table',
	'filter' => $model,
	'pager' => array(
		'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
		'nextPageLabel' => 'Next '
	),
	'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
	'columns' => array(
		
		array(
			'header' => 'S.No.',
			'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		),

	
		array(
			'htmlOptions' => array('class' => 'width-80'),
			'class' => 'CButtonColumn',
			'template' => '{view}{update}',

			'buttons' => array(
				'view' => array(
					'label' => '',
					'imageUrl' => false,
					//'url' => "CHtml::normalizeUrl(array('minutesview', 'id'=>\$data->template_id))",
					'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View'),
				),
				'update' => array(
					'label' => '',
					'imageUrl' => false,
					//'url' => "CHtml::normalizeUrl(array('/projects/minutesupdate', 'id'=>\$data->template_id))",
					'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Update'),
				),

				// 'excel' => array(
				// 	'label' => '',
				// 	'imageUrl' => false,
				// 	'url' => "CHtml::normalizeUrl(array('template_excel', 'id'=>\$data->template_id))",

				// 	'options' => array('class' => 'fa fa-file-excel-o icon-comn', 'title' => 'View'),
				// ),


			)

		),
		array(
			'name' => 'template_name',
			'value' => '$data->template_name',
			'type' => 'raw',
		),

		array(
			'name' => 'created_by',
			'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
			'filter'=>false,
			'type' => 'raw',
		),
	

		array(
			'name' => 'created_date',
			'value' => 'date("d-M-y",strtotime($data->created_date))',
			'type' => 'html',
		),

	),
));
?>


<script>

$(function () {  
    $( "input[name='Template[created_date]']" ).datepicker({                 
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd'                     
    });
    
     
});
    </script>