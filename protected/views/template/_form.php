<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<?php
/* @var $this TemplateController */
/* @var $model Template */
/* @var $form CActiveForm */
?>
<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(

        'action' => Yii::app()->createAbsoluteUrl("template/create"),
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>

    <?php
    $this->renderPartial('_templateform', array('model' => $model, 'form' => $form, 'account_items' => $account_items));
    ?>

    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Select Material",
        });

    });

    var count = 0;
    var val = 0;

    function addInput() {

        count += 1;


        $('#container').append(

            `
           
            <div class="row padding-top-10 remove-div_` + count + `">
			<div class="col-md-3">
            <label for="Item" class="required">Item <span class="required">*</span></label>
			 <input class="form-control item" name="Template[item_name][]" id="" type="text" required>
        </div>

        <div class="col-md-3">
          
          <label for="Item" class="required">Ref column <span class="required">*</span></label>
           <input class="form-control ref-column uppercase" name="Template[reference_column][]" id="Template_reference_column` + count + `" type="text" required >
           
           <span id="duplicate` + count + `" style="color:red;font-weight:bold"></span>
           <div class="errorMessage"  style="display: none;"></div>
      </div>

      
        <div class="col-md-4">
        <label for="Purchaseitem" class="required">Purchase Item <span class="required">*</span></label>
			
			<select name="template_purchase_item[` + count + `][]" 
			id="template_purchase_item_` + count + `"
			 class="form-control js-example-basic-multiple purchase_items"  multiple = "multiple" required>
            <?php

            foreach ($account_items as $key => $value) {
            ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
            <?php
            }
            ?>
        </select>
        </div>



        <!-- remove button -->
                        <div class=" col-md-2  remove-btn-padding">
                       
                        <input type="button" id="` + count + `" value="Remove" class="remove-btn btn btn-secondary">
                        </div>
                        <!-- end remove button -->
                        </div>
			`

        );

        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Select Material",
        });


        $(document).on('click', '.remove-btn', function() {
            id = this.id;
            $(".remove-div_" + id).remove();
        });

    }

    $(document).on('change', '.ref-column', function() {

        var $column = $(this);
        var val = $column.val().toUpperCase();
        var z = 0;
        var template_id = $('#template_id').val();
        $(".ref-column").each(function() {

            var inputval = $(this).val();
            var ref_col = inputval.toUpperCase();

            if (val == ref_col) {
                z = z + 1;
            }

        });

        if (z > 1) {
            $column.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
            $column.val('');
            return false;
        }

    });


    $(document).ready(function() {
        $('.uppercase').on('keyup', function() {
            var inputval = $(this).val();
            $(this).val(inputval.toUpperCase());
        });
    });
</script>

</div><!-- form -->