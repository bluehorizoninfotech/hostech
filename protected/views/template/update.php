<?php
/* @var $this TemplateController */
/* @var $model Template */

$this->breadcrumbs=array(
	'Templates'=>array('index'),
	$model->template_id=>array('view','id'=>$model->template_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Template', 'url'=>array('index')),
	array('label'=>'Create Template', 'url'=>array('create')),
	array('label'=>'View Template', 'url'=>array('view', 'id'=>$model->template_id)),
	array('label'=>'Manage Template', 'url'=>array('admin')),
);
?>

<h1>Update Template : <?php echo $model->template_name; ?></h1>

<?php echo $this->renderPartial('_updateform', array('model'=>$model,'data'=>$data,'account_items'=>$account_items)); ?>