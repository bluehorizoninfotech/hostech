<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

<?php
$form = $this->beginWidget('CActiveForm', array(

    'action' => Yii::app()->createAbsoluteUrl("template/update", array('id' => $model->template_id)),
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
));
?>
<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <div class="row">

            <div class="col-md-6">

                <input type="hidden" name="Template[template_id]" value=<?= $model->template_id ?> id="template_id">
                <input type="hidden" name="Template[id]" id="id">

                <?php echo $form->labelEx($model, 'template_name'); ?>
                <?php echo $form->textField($model, 'template_name', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'template_name'); ?>

            </div>



        </div>

        <div class="row">

            <div id="" class="participants_data participants">


                <div class="col-md-3">

                    <label for="Item" class="required">Item <span class="required">*</span></label>
                    <input class="form-control item" name="Template[item_name][]" id="Template_item_name" type="text" required>
                </div>

                <div class="col-md-3">

                    <label for="Item" class="required">Ref column <span class="required">*</span></label>
                    <input class="form-control ref-column uppercase" name="Template[reference_column][]" id="Template_reference_column0" type="text" required>
                    <div class="errorMessage display-none"></div>
                </div>

                <div class="col-md-4 ">
                    <label for="Purchaseitem" class="required">Purchase Item <span class="required">*</span></label>

                    <select name="template_purchase_item[0][]" id="template_purchase_item" class="form-control js-example-basic-multiple purchase_items " multiple="multiple" required>
                        <?php

                        foreach ($account_items as $key => $value) {
                        ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                        <?php
                        }
                        ?>
                    </select>

                </div>
            <!-- </div> -->

        </div>

        <div id="container"></div>
        <br>
        <input type="button" id="add_new" value="Add"  class="btn btn-sm margin-top-4 btn-secondary"  onClick="addInput();">
        <br>
        <div class="row text-right">
            <div class="col-sm-12">
                <?php echo CHtml::SubmitButton('Update', array('class' => 'btn blue save_btn create-template margin-left-30','id' => 'save_btn', 'name' => 'save_btn'), array()); ?>


            </div>
        </div>
        <!-- panel div close -->
    </div>
</div>


<?php $this->endWidget(); ?>



<br>


<!-- table table -->
<div id="table-scroll" class="table-scroll">
    <div id="faux-table" class="faux-table" aria="hidden"></div>
    <div id="table-wrap" class="table-wrap">
        <div class="table-responsive">

            <!-- <table cellpadding="10" class="table"> -->
            <table cellpadding="3" cellspacing="0" class="table-bordered">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Action</th>
                        <th>Item</th>
                        <th>Purchase Item</th>
                        <th>Ref Column</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($data as $key => $item) {
                        $purchase_items = Controller::getMaterialNames($item->purchase_item);
                    ?>
                        <tr>
                            <td width="10%"><?= $key + 1 ?></td>
                            <td>
                                <?php
                                if ($model->status == 0) { ?>

                                    <a class="icon-pencil icon-comn editbtn" id="<?= $item->id ?>" title="View"></a>
                                <?php
                                }
                                ?>

                            </td>
                            <td width="40%"><?= $item->item_name ?></td>
                            <td width="40%">

                                <?= $purchase_items  ?>
                            </td>
                            <td><?= $item->ref_column ?></td>
                            
                        </tr>
                    <?php
                    }

                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end tble -->

<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Select Material",
        });

    });

    var count = 0;

    function addInput() {

        count += 1;


        $('#container').append(

            `
            <div class="row padding-top-10 remove-div_` + count + `">
			<div class="col-md-3">
            <label for="Item" class="required">Item <span class="required">*</span></label>
			 <input class="form-control item" name="Template[item_name][]" id="" type="text" required>
        </div>

        <div class="col-md-3">
          
          <label for="Item" class="required">Ref column <span class="required">*</span></label>
           <input class="form-control ref-column uppercase" name="Template[reference_column][]" id="Template_reference_column` + count + `" type="text" required >
           
          
           <div class="errorMessage display-none"></div>
      </div>

      
        <div class="col-md-4">
        <label for="Purchaseitem" class="required">Purchase Item <span class="required">*</span></label>
			
			<select name="template_purchase_item[` + count + `][]" 
			id="template_purchase_item_` + count + `"
			 class="form-control js-example-basic-multiple purchase_items"  multiple = "multiple" required>
            <?php

            foreach ($account_items as $key => $value) {
            ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
            <?php
            }
            ?>
        </select>
        </div>
        

        <!-- remove button -->
                        <div class=" col-md-2  remove-btn-padding">
                       
                        <input type="button" id="` + count + `" value="Remove" class="remove-btn btn btn-sm btn-secondary">
                        </div>
                        <!-- end remove button -->
                        </div>
                        </div>
			`

        );

        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Select Material",
        });




        $(document).on('click', '.remove-btn', function() {
            id = this.id;
            $(".remove-div_" + id).remove();
        });



    }


    $(document).on('change', '.ref-column', function() {

        var $column = $(this);
        var val = $column.val().toUpperCase();
        var z = 0;
        var template_id = $('#template_id').val();
         $(".ref-column").each(function() {

            var inputval = $(this).val();
            var ref_col = inputval.toUpperCase();

            if (val == ref_col) {
                z = z + 1;
            }

        });

        if (z > 1) {
            $column.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
            $column.val('');
            return false;
        }

        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl("template/checkduplicate") ?>',
            method: "POST",
            data: {
                ref_col: val,
                template_id: template_id

            },
            success: function(result) {

                if (result != 0) {
                    $column.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
                    $column.val('');

                }
            }

        });

    });

    $(document).on('click', '.editbtn', function(e) {
        $('#add_new').hide();
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        $.ajax({
            method: "POST",
            data: {
                item_id: item_id
            },
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('template/UpdateTemplateItem'); ?>',
            success: function(data) {
                if (data.status == 1) {
                    $('#Template_item_name').val(data.item_name);
                    var items = data.purchase_item.split(',');
                    $('#template_purchase_item').val(items);
                    $('#template_purchase_item').trigger('change');
                    $('#id').val(data.id);
                    $('.ref-column').val(data.ref_column);


                }
            }
        });

    });
    $(document).ready(function() {
        $('.uppercase').on('keyup', function() {
            var inputval = $(this).val();
            $(this).val(inputval.toUpperCase());
        });
    });
</script>