<?php
/* @var $this BudgetHeadController */
/* @var $model BudgetHead */

$this->breadcrumbs=array(
	'Budget Heads'=>array('index'),
	$model->budget_head_id=>array('view','id'=>$model->budget_head_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BudgetHead', 'url'=>array('index')),
	array('label'=>'Create BudgetHead', 'url'=>array('create')),
	array('label'=>'View BudgetHead', 'url'=>array('view', 'id'=>$model->budget_head_id)),
	array('label'=>'Manage BudgetHead', 'url'=>array('admin')),
);
?>

<h1>Update BudgetHead <?php echo $model->budget_head_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>