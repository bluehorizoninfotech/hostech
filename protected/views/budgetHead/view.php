<?php
/* @var $this BudgetHeadController */
/* @var $model BudgetHead */

$this->breadcrumbs=array(
	'Budget Heads'=>array('index'),
	$model->budget_head_id,
);

$this->menu=array(
	array('label'=>'List BudgetHead', 'url'=>array('index')),
	array('label'=>'Create BudgetHead', 'url'=>array('create')),
	array('label'=>'Update BudgetHead', 'url'=>array('update', 'id'=>$model->budget_head_id)),
	array('label'=>'Delete BudgetHead', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->budget_head_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BudgetHead', 'url'=>array('admin')),
);
?>

<h1>View BudgetHead #<?php echo $model->budget_head_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'budget_head_id',
		'budget_head_title',
		'status',
		'project_id',
		'start_date',
		'end_date',
		'ranking',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
