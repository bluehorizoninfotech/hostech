<?php
/* @var $this BudgetHeadController */
/* @var $model BudgetHead */

$this->breadcrumbs=array(
	'Budget Heads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BudgetHead', 'url'=>array('index')),
	array('label'=>'Manage BudgetHead', 'url'=>array('admin')),
);
?>

<h1>Create BudgetHead</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>