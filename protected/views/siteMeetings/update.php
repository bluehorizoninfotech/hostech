<?php
/* @var $this SiteMeetingsController */
/* @var $model SiteMeetings */

$this->breadcrumbs=array(
	'Site Meetings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SiteMeetings', 'url'=>array('index')),
	array('label'=>'Create SiteMeetings', 'url'=>array('create')),
	array('label'=>'View SiteMeetings', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SiteMeetings', 'url'=>array('admin')),
);
?>

<h1>Update SiteMeetings <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>