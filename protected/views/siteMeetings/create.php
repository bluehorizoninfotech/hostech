<?php
/* @var $this SiteMeetingsController */
/* @var $model SiteMeetings */

$this->breadcrumbs=array(
	'Site Meetings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SiteMeetings', 'url'=>array('index')),
	array('label'=>'Manage SiteMeetings', 'url'=>array('admin')),
);
?>

<h1>Create SiteMeetings</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>