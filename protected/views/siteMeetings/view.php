<?php
/* @var $this SiteMeetingsController */
/* @var $model SiteMeetings */

$this->breadcrumbs=array(
	'Site Meetings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SiteMeetings', 'url'=>array('index')),
	array('label'=>'Create SiteMeetings', 'url'=>array('create')),
	array('label'=>'Update SiteMeetings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SiteMeetings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SiteMeetings', 'url'=>array('admin')),
);
?>

<h1>View SiteMeetings #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'site_id',
		'date',
		'venue',
		'objective',
		'next_meeting_date',
		'time',
		'location',
		'note',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
