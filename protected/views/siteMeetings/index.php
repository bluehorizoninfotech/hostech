<?php
/* @var $this SiteMeetingsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Site Meetings',
);

$this->menu=array(
	array('label'=>'Create SiteMeetings', 'url'=>array('create')),
	array('label'=>'Manage SiteMeetings', 'url'=>array('admin')),
);
?>

<h1>Site Meetings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
