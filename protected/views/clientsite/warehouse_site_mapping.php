<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<h1> Site Warehouse Mapping</h1>
<div class="panel panel-default white-bg">
    <div class="panel-heading clearfix">
    <p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully created..</p>
        <div class="row display-flex align-items-start">
            <div class="col-md-3">
            <label>Site</label>
                 <select class="form-control" id="site_id">
                    <option></option>
                    <?php
                    foreach($client_site as $site)
                    {?>
                    <option value="<?= $site->id?>"><?= $site->site_name	?></option>

                    <?php
                    }
                    ?>
                </select>
                
                <span id="validation" class="red-color font-weight-bold"></span>
                <span id="error_site" class="red-color font-weight-bold"></span>

            </div>

            <div class="col-md-3">
            <label>Warehouse</label>
                 <select class="form-control" id="warehouse_id">
                    <option></option>
                   
                </select>
                
                <span id="error_warehouse" class="red-color font-weight-bold"></span>

            </div>
  <?php echo CHtml::SubmitButton('Save', array('class' => 'btn blue save_btn btn-sm site-warehouse-mapping margin-top-14',  'id' => 'save_btn', 'name' => 'save_btn'), array()); ?>
        </div>

    </div>
</div>
<script type="text/javascript">
  $(document).on('change', '#site_id', function() {
        var site_id = $('#site_id').val();
        $.ajax({
            method: "POST",
            data: {
                site_id: site_id
            },
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('clientsite/getWarehouse'); ?>',
            success: function(data) {

                if (data.status == 1) {
                    $("#warehouse_id").html(data.html.html);
                    $('#validation').hide()

                } else {
                   
                    $("#warehouse_id").html(data.html.html);
                    $('#validation').text("Project  not mapped");
                }


            }
        });

    });
  
    $(".site-warehouse-mapping").click(function() {
        
        var site_id=$('#site_id').val();
        var warehouse_id=$('#warehouse_id').val();

        if(site_id && warehouse_id != "")
        {
            $.ajax({
            "type": "POST",
            "url": "<?php echo Yii::app()->createUrl('clientsite/warehouse_site_mapping'); ?>",
            "dataType": "json",
            "data": {
                site_id:site_id,
                warehouse_id:warehouse_id

            },

            "success": function(data) {
                if (data.stat == 1) {
                    $("#success_message").fadeIn().delay(1000).fadeOut();
                    setTimeout(location.reload.bind(location), 1000);
                } else {
                   
                   alert('something went wrong')
                }

            }
        });
        }
        else
        {
            if(site_id=="")
            {
                $('#error_site').text("Select Site");
            }
            if(warehouse_id=="")
            {
                $('#error_warehouse').text("Select Warehouse");
            }
        }
      
        
       

    });
    </script>