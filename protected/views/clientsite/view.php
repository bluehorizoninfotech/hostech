<?php
/* @var $this ClientsiteController */
/* @var $model Clientsite */

$this->breadcrumbs=array(
	'Clientsites'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Clientsite', 'url'=>array('index')),
	array('label'=>'Create Clientsite', 'url'=>array('create')),
	array('label'=>'Update Clientsite', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Clientsite', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Clientsite', 'url'=>array('admin')),
);
?>

<h1>View Clientsite #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'site_name',
	),
)); ?>
