<?php
/* @var $this ClientsiteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Clientsites',
);

$this->menu=array(
	array('label'=>'Create Clientsite', 'url'=>array('create')),
	array('label'=>'Manage Clientsite', 'url'=>array('admin')),
);
?>

<h1>Clientsites</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
