<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<?php
// Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);
 Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css');



// Yii::app()->clientScript->registerScript('myscript', "
// $(function() {
// $('.project').select2();
// });
// ");
Yii::app()->clientScript->registerCss('mycss', '

.height{
    /* height: 29px !important; */
    
}
.select2-container .select2-selection--single{
        height: 24px;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 23px;
    }
    .select2-container--default .select2-selection--single{
        border: 1px solid #e5e5e5;
    }
    .select2-container{
          margin: 0.4em 0 0.2em 0;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow{
        top: -1px;
    }
    .select2 select2-container select2-container--default{
        width: 175px !important;
    }
    .add-form{
        background-color: rgba(128, 128, 128, 0.15);
        padding: 5px 0px;
    }
    .addelement{
        padding:0px 15px;
    }
    .addelement input{
        margin: 0.4em 0 0.2em 0;
    }  

');


/* @var $this ClientsiteController */
/* @var $model Clientsite */

$this->breadcrumbs = array(
    'Clientsites' => array('index'),
    'Manage',
);
//
//$this->menu=array(
//	array('label'=>'List Clientsite', 'url'=>array('index')),
//	array('label'=>'Create Clientsite', 'url'=>array('create')),
//);
//Yii::app()->clientScript->registerScript('search', "
//$('.search-button').click(function(){
//	$('.search-form').toggle();
//	return false;
//});
//$('.search-form form').submit(function(){
//	$.fn.yiiGridView.update('clientsite-grid', {
//		data: $(this).serialize()
//	});
//	return false;
//});
//");
?>

<h1>Manage Sites</h1>

<div>
    <div class="">
        <div class="col-md-6 add-form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'work-site-form',
                'enableAjaxValidation' => false,
                    /* 'enableAjaxValidation' => true,
                      'enableClientValidation'=>true,
                      'clientOptions' => array(
                      'validateOnSubmit' => true,
                      'validateOnChange' => true,
                      'validateOnType' => false,)
                     */
            ));
            ?>
            <div class="addelement">
                <div class="row">
                    <div class="col-md-4">
                        <?php echo $form->textField($actmodel, 'site_name', array('class' => 'height form-control pull-left width-180 height-30 margin-bottom-1 margin-right-10', 'placeholder' => $actmodel->getAttributeLabel('site_name'))); ?>
                        <?php echo $form->error($actmodel, 'site_name'); ?>
                    </div>
                    <div class="col-md-4">
                    <?php
                    if($actmodel->isNewRecord){
                        if(Yii::app()->user->project_id !=""){
                            $actmodel->pid = Yii::app()->user->project_id;
                        }else{
                            $actmodel->pid = "";
                        }
                    }
                    ?>
                        <?php echo $form->dropDownList($actmodel, 'pid', CHtml::listData($project, 'pid', 'name'), array('empty' => 'Please choose a project', 'id' => 'pid', 'class' => 'form-control input-medium project')); ?>
                        <?php echo $form->error($actmodel, 'pid'); ?>	
                    </div>
 
                <!-- </div>
                <div class="row"> -->
                    <div class="col-md-4">
                        <?php echo $form->textField($actmodel, 'latitude', array('class' => 'height form-control pull-left width-180 height-30 margin-bottom-1 margin-right-10', 'placeholder' => 'Latitude')); ?>
                        <?php echo $form->error($actmodel, 'latitude'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo $form->textField($actmodel, 'longitude', array('class' => 'height form-control pull-left width-180 height-30 margin-bottom-1 margin-right-10', 'placeholder' => 'Longitude')); ?>
                        <?php echo $form->error($actmodel, 'longitude'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo $form->textField($actmodel, 'distance', array('class' => 'height form-control pull-left width-180 height-30 margin-bottom-1 margin-right-10', 'placeholder' => 'Punching Radius')); ?>
                        <?php echo $form->error($actmodel, 'distance'); ?>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-4">
                        <?php echo CHtml::submitButton($actmodel->isNewRecord ? 'Create' : 'Save', array('class' => 'addbutton_form btn blue btn-sm')); ?>
                    </div>
                </div>
            </div>
                </div>
                
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="half-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'clientsite-grid',
            'dataProvider' => $model->searchClientSite(),
            'itemsCssClass' => 'table table-bordered',
            'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                'nextPageLabel' => 'Next '),
            'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
            'filter' => $model,
            'columns' => array(
                array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-60'),),
                array(
                    'class' => 'CButtonColumn',
                    'htmlOptions' => array('class' => 'width-60'),
                    //'template' => '{deletesite}{active}{inactive}',
                    'template' => '{assigned}{update}',
                    'buttons' => array(
                        'assigned' => array(
                            'label' => '',
                            'url' => '"#".Yii::app()->createUrl("clientsite/assigntouser",array("id"=>$data->id,"asDialog"=>1,"gridId"=>$this->grid->id))',
                            'click' => 'function(){$("#cru-frame").attr("src",($(this).attr("href")).substring(1)); $("#cru-dialog").dialog("open");return false;}',
//                            'imageUrl' => Yii::app()->baseUrl . '/images/users.png',
                            'imageUrl'=>false,
                            'options' => array('class' => 'actionitem viewicon icon-target icon-comn','title'=>'Assigned' ),
                            'visible'=>"in_array('/clientsite/assigntouser', Yii::app()->session['menuauthlist'])",
                        ),
                        'delete' => array(
                            'label' => 'Delete',
                            'url' => 'Yii::app()->createUrl("clientsite/delete", array("id"=>$data->id))',
                            //'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                            'options' => array('class' => 'deleteicon'),
                        ),
                        'update' => array(
                            'label' => '',
                            'imageUrl'=>false,
                            //'url' => 'Yii::app()->createUrl("tools/update", array("id"=>$data->id))',
                            'url' => 'Yii::app()->createAbsoluteUrl("clientsite/admin", array("id"=>$data->id))',
                            //'options' => array('class' => 'actionitem updatelink'),
                            'options' => array('class' => 'actionitem icon-pencil icon-comn'),
                        ),
                    // Inactive section end here     
                    // 'update' => array(
                    // 'label'=>'edit',
                    // 'options' => array('class' => 'editWorksite'),
                    //  'url' => 'Yii::app()->createUrl("workSite/update",array("id"=>$data->id,))',
                    //),
                    ),
                ),
                /* 'id', */
                'site_name',
                //'pid',
                
                array(
                    //'filter' => false,
                    'name' => 'pid',
                    'value' => '(isset($data->p->name) ? $data->p->name : "")',
                    'filter' => CHtml::listData(Projects::model()->findAll(
                           array(
                               'select' => array('pid,name'),
                               'order' => 'name',
                               'distinct' => true
                   )), "pid", "name")
                ),
                'latitude',
                'longitude',
                array(
                    'filter' => false,
                    'name' => 'assignuser',
                    'header' => 'Site Engineers/Supervisors',
                    'value' => '$data->getassignedusers($data->id)',
                    'type' => 'raw',
                    'visible' => ((Yii::app()->user->role == 1) ? true : false),
                ),
                
            ),
        ));
        ?>

    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('myjquery', ' 
		
$(document).ready(function () {
$(document).on("click","#work-site-grid table tbody tr .editable",function() {
//jQuery("#work-site-grid table tbody tr .editable").click(function(){
		   //alert("hi");
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		  // alert(id);
		   var label = $(this);
		   label.after("<input class=\'work_site_name singletextbox\' type = \'text\' style = \'display:none\' name=\'site_name\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn green btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage1\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();
		  jQuery(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("work-site-grid");
				location.reload();
				
			
			}); 
		  
		 
		  jQuery(".save").click(function(){
		  
				var value = $(".work_site_name").val();
				
				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('workSite/update') . '",
				data:{id:id,value: value},
				success:function(response){
							
							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage1").text(obj);
							}
						 },

			   });
			
			});   
		   
		   
         });
         
     $(".deleteicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		
		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){
					
					//alert("Work Site Deleted");
					location.reload();
				}
				else
				{
					alert("Work Site is already in use,Cannot delete! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
       
//active status script starts from here
$(document).on("click",".changestatusicon",function() {
//$(".changestatusicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		//alert(url);
		var answer = confirm ("Are you sure you want to change the status?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){					
					
					location.reload();
				}
				else
				{
					alert("Cannot change the status! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
//active status script end here
		

	

	});
      
   ');
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'User Form',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="555" height="620" frameborder="0" class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>

<script>
// $(document).ready(function() {
 $(".project").select2();
//});
</script>

