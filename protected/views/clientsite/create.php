<?php
/* @var $this ClientsiteController */
/* @var $model Clientsite */

$this->breadcrumbs=array(
	'Clientsites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Clientsite', 'url'=>array('index')),
	array('label'=>'Manage Clientsite', 'url'=>array('admin')),
);
?>

<h1>Create Clientsite</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>