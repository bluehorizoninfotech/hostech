<?php
/* @var $this ClientsiteController */
/* @var $model Clientsite */

$this->breadcrumbs=array(
	'Clientsites'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
//
//$this->menu=array(
//	array('label'=>'List Clientsite', 'url'=>array('index')),
//	array('label'=>'Create Clientsite', 'url'=>array('create')),
//	array('label'=>'View Clientsite', 'url'=>array('view', 'id'=>$model->id)),
//	array('label'=>'Manage Clientsite', 'url'=>array('admin')),
//);
?>

<h1>Update Clientsite <?php //echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>