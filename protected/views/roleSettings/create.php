<?php
/* @var $this RoleSettingsController */
/* @var $model RoleSettings */

$this->breadcrumbs=array(
	'Role Settings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RoleSettings', 'url'=>array('index')),
	array('label'=>'Manage RoleSettings', 'url'=>array('admin')),
);
?>
<div class="main_content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Create Role Settings</h1>

                <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
            </div>
        </div>
    </div>

</div>