
<?php
/* @var $this RoleSettingsController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
/* @var $this RoleSettingsController */
/* @var $model RoleSettings */
/* @var $form CActiveForm */


Yii::app()->clientScript->registerScript('myjquery', "
    $(document).ready(function () {
        setTimeout('$(\"html\").removeClass(\"js\")', 1000);

        $(document).on('change', '#roleid', function(){
            var roleid = $('#roleid').val();
            accountresults(roleid);

        });

        
        
    });
    
    function accountresults(roleid){
            $.ajax({
                method: 'GET',
                dataType: 'json',
                data: {roleid:roleid},
                url: 'index.php?r=roleSettings/getsettings',
                success: function (data) {
                    if(data){
                        $.each(data.data, function (key, value) { 
                            $('#RoleSettings_'+key).not('input[type=\'checkbox\']').val(value); 
                            
                            $('#RoleSettings_'+key).each(function(){
                                if($(this).attr('type') == 'checkbox'){ 
                                
                                    if(value == '1'){
                                        $(this).prop('checked', true);
                                    }else{
                                        $(this).prop('checked', false);
                                    }
                                }
                            });
                            

                            if(key == 'weekly_off2_count'){
                                var checkedvals = value.split(','); 
                                $.each(checkedvals, function(k,v){
                                    --v;
                                    $('#RoleSettings_'+key+'_'+v).prop('checked', true);
                                });
                            }
                                

                        });
                    }else{
                    $('#role-settings-form').find('input[type=\'text\'],input[type=\'checkbox\'], select, textarea').not('#roleid').val('');
                    $('#role-settings-form').find('input:checkbox').removeAttr('checked');
                        //$('form#role-settings-form').find('input').not('#roleid').val('');

                    }

                }
            });
        
    }



");
?>
<div class="role-settings-sec">
<div class="modal-header">
    <button type="button"  class="close font-36" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><b>Role Settings Details</b></h4>
</div>
<div class="modal-body">
    <div class="well">
        <div class="main_content">

            <div class="row">

                <div class="alert alert-success alert-dismissable display-none" id="successmsg2">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'role-settings-form',
                    'enableAjaxValidation' => true,
                    'action' => Yii::app()->createUrl('roleSettings/rolesettings'),
                    'htmlOptions' => array('class' => 'form form-inline')
                        ));
                ?>

                    <div class="row">
<!--                        <input type="hidden" id="RoleSettings_rset_id" name="RoleSettings[rset_id]" value="0" />-->
                        <?php echo $form->hiddenField($model,'rset_id') ?>
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'role_id'); ?>
                            <?php 
                            // array('condition' => "id!= 1")
                            echo $form->dropDownList($model, 'role_id', CHtml::listData(UserRoles::model()->findAll(), 'id', 'role'), array('class'=>'form-control','empty' => 'Please choose a category', 'id' => 'roleid')); ?>
                            <?php echo $form->error($model, 'role_id'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'short_name'); ?>
                            <?php echo $form->textField($model, 'short_name', array('size' => 20, 'maxlength' => 20,'class'=>'form-control',)); ?>
                            <?php echo $form->error($model, 'short_name'); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'ot_formula'); ?>
                            <?php echo $form->dropDownList($model, 'ot_formula', array('0' => 'OT not applicable', '1' => 'OT applicable'),array('class'=>"form-control")); ?>
                            <?php echo $form->error($model, 'ot_formula'); ?>
                            
                            <span>[ Min 9+ hours(including 1 hour break) per day needed for OT calculation ]</span>
                        </div>
                        
                        
                        <div class="form-group inline-div" id="message-box" >
                            <span>
                                <?php echo $form->labelEx($model, 'min_ot'); ?>in mins
                                <?php echo $form->textField($model, 'min_ot', array('class'=>'form-control width-60')); ?>
                                <?php //echo $form->error($model,'min_ot'); ?>
                            </span>
                            <span>
                                <?php echo $form->checkBox($model, 'max_ot_yes', array('class' => 'margin-top-25 margin-bottom-25 margin-right-10 margin-left-10')); ?>
                            </span>
                            <span>
                                <?php echo $form->labelEx($model, 'max_ot'); ?>in mins
                                <?php echo $form->textField($model, 'max_ot', array('class'=>'form-control width-60')); ?>
                                <?php //echo $form->error($model,'max_ot'); ?>
                            </span>
                            
                            
                           
                        </div>
                    </div>

<div id="message-box">
                    <div class="row">
                        <div class="form-group col-md-4 inline-input">
                            <?php //echo $form->hiddenField($model,'last_inpunch',array('value'=>'0')); ?>
                            <?php echo $form->checkBox($model, 'last_inpunch'); ?>
                            <?php echo $form->labelEx($model, 'last_inpunch'); ?>

                            <?php echo $form->error($model, 'last_inpunch'); ?>
                        </div>

                        <div class="form-group">
                            <div class="inline-input">
                                <?php echo $form->labelEx($model, 'late_coming_gt', array('class' => 'margin-10')); ?>
                                <?php echo $form->textField($model, 'late_coming_gt', array('class'=>'form-control width-60')); ?><label>mins</label>
                                <?php echo $form->error($model, 'late_coming_gt'); ?> 
                            </div>
                            <div class="inline-input">
                                <?php echo $form->labelEx($model, 'early_going_gt', array('class' => 'margin-10')); ?>
                                <?php echo $form->textField($model, 'early_going_gt', array('class'=>'form-control width-60')); ?><label>mins</label>
                                <?php echo $form->error($model, 'early_going_gt'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'weekly_off1_yes', array()); ?>
                            <?php echo $form->labelEx($model, 'weekly_off1_yes'); ?>
                        </div>
                        <div class="form-group">
                            <?php
                            $days = array('Sunday' => 'Sunday', 'Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday', 'Saturday' => 'Saturday');
                            echo $form->dropDownList($model, 'weekly_off1', $days,array('class'=>'form-control'));
                            ?>
                        </div>
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'weekly_off2_yes'); ?>
                            <?php echo $form->labelEx($model, 'weekly_off2_yes'); ?>
                        </div>
                        <div class="form-group inline-input">
                                <?php echo $form->dropDownList($model, 'weekly_off2', $days,array('class'=>'form-control')); ?>
                            <span class="inline-check">
                            <?php  $model->weekly_off2_count = explode(",", $model->weekly_off2_count); 
                            echo $form->checkBoxList($model, 'weekly_off2_count', array('1' => '1st', '2' => '2nd', '3' => '3rd', '4' => '4th', '5' => '5th'), array('separator' => "")); ?>
                            </span>

                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'early_coming_punch'); ?>
                            <?php echo $form->labelEx($model, 'early_coming_punch'); ?>	
                        </div>

                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'late_going_punch'); ?>
                            <?php echo $form->labelEx($model, 'late_going_punch'); ?>	
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'first_last_punch'); ?>
                            <?php echo $form->labelEx($model, 'first_last_punch'); ?>
                        </div>
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'deduct_breakhours'); ?>
                            <?php echo $form->labelEx($model, 'deduct_breakhours'); ?>
                        </div>
                    </div>
                    
                    
                </div>                    

        <div id="message-box">
    <!-- section needed to be grayed out starts here  -->
    <div class="row">
                        <div class="form-group inline-input">

                            <?php echo $form->checkBox($model, 'halfday_by_duration'); ?>
                            <?php echo $form->labelEx($model, 'halfday_by_duration'); ?>
                            <?php echo $form->textField($model, 'halfday_duration_mins',array('class'=>'form-control')); ?><label>mins</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'absent_by_duration'); ?>
                            <?php echo $form->labelEx($model, 'absent_by_duration'); ?>
                            <?php echo $form->textField($model, 'absent_duration_mins',array('class'=>'form-control')); ?><label>mins</label>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'partial_halfday_by_duration'); ?>
                            <?php echo $form->labelEx($model, 'partial_halfday_by_duration'); ?>
                            <?php echo $form->textField($model, 'partial_halfday_duration_mins',array('class'=>'form-control')); ?><label>mins</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'partial_absent_by_duration'); ?>
                            <?php echo $form->labelEx($model, 'partial_absent_by_duration'); ?>
                            <?php echo $form->textField($model, 'partial_absent_duration_mins',array('class'=>'form-control')); ?><label>mins</label>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'absent_for_prefix'); ?>
                            <?php echo $form->labelEx($model, 'absent_for_prefix'); ?>

                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'absent_for_suffix'); ?>
                            <?php echo $form->labelEx($model, 'absent_for_suffix'); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'absent_for_both'); ?>
                            <?php echo $form->labelEx($model, 'absent_for_both'); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'absent_for_condition'); ?>
                            <?php echo $form->labelEx($model, 'absent_for_condition'); ?>
                            <?php echo $form->dropDownList($model, 'absent_type', array('halfday' => 'Half Day', 'fullday' => 'Full Day'),array('class'=>"form-control")); ?>
                            <?php echo $form->labelEx($model, 'absent_late'); ?>
                            <?php echo $form->dropDownList($model, 'absent_late', array('1' => '1'),array('class'=>"form-control")); ?><label>Days</label>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'halfday_by_late'); ?>
                            <?php echo $form->labelEx($model, 'halfday_by_late'); ?>
                            <?php echo $form->textField($model, 'halfday_late_mins',array('class'=>'form-control')); ?><label>mins</label>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group inline-input">
                            <?php echo $form->checkBox($model, 'halfday_by_earlygoing'); ?>
                            <?php echo $form->labelEx($model, 'halfday_by_earlygoing'); ?>
                            <?php echo $form->textField($model, 'halfday_earlygoing_mins',array('class'=>'form-control')); ?><label>mins</label>
                        </div>
                    </div>

<!-- ends here -->
</div>
                    
                    <div class="row buttons">
<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');  ?>


                        <?php
                        echo CHtml::ajaxSubmitButton('Save', Yii::app()->request->url, array(
                            'dataType' => 'json', // If this option is not set, the returned data is a string not a JSON object
                            //'type' => 'post',
                            'success' =>
                            'js:function (data) {
                            if (data.status == "error") {
                                $("#successmsg2").removeClass("alert-success");
                                $("#successmsg2").addClass("alert-error");
                                $("#successmsg2").html(data.message).show();
                                $("#successmsg2").delay(6000).fadeOut("slow");
                                $(".errorMessage").hide();
                                $.each(data.errors, function(key, val){
                                    $("input#RoleSettings_"+key).after("<div class=\"errorMessage\">"+val+"</div>").show();
                                    $("span#RoleSettings_"+key).after("<div class=\"errorMessage\">"+val+"</div>").show();
                                    $("textarea#RoleSettings_"+key).after("<div class=\"errorMessage\">"+val+"</div>").show(); 
                                    $("select#RoleSettings_"+key).after("<div class=\"errorMessage\">"+val+"</div>").show(); 
                                    
                                });


                            } else {
                            $("#successmsg2").removeClass("alert-error");
                            $("#successmsg2").addClass("alert-success");
                            $("#successmsg2").html(data.message).show();
                            $("#successmsg2").delay(6000).fadeOut("slow");
                            
                            $(".errorMessage").hide();
                            //$.fn.yiiGridView.update("role-settings-grid");
                             
                            }
                        }',
                                ), array(
                            'id' => 'myform_submit_' . rand(1, 255), // Need a unique id or they start to conflict with more than one load.
                            'class' => 'btn btn-primary'
                        ));
                        ?>
                    </div>

                    <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
    </div>
</div>
                    </div>
                    
