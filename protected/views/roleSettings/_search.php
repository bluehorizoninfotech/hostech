<?php
/* @var $this RoleSettingsController */
/* @var $model RoleSettings */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'rset_id'); ?>
		<?php echo $form->textField($model,'rset_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'role_id'); ?>
		<?php echo $form->textField($model,'role_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'short_name'); ?>
		<?php echo $form->textField($model,'short_name',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ot_formula'); ?>
		<?php echo $form->textField($model,'ot_formula'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'min_ot'); ?>
		<?php echo $form->textField($model,'min_ot'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'max_ot'); ?>
		<?php echo $form->textField($model,'max_ot'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_inpunch'); ?>
		<?php echo $form->textField($model,'last_inpunch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'late_coming_gt'); ?>
		<?php echo $form->textField($model,'late_coming_gt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'early_going_gt'); ?>
		<?php echo $form->textField($model,'early_going_gt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'weekly_off1'); ?>
		<?php echo $form->textField($model,'weekly_off1',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'weekly_off2'); ?>
		<?php echo $form->textField($model,'weekly_off2',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'weekly_off2_count'); ?>
		<?php echo $form->textField($model,'weekly_off2_count',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'early_coming_punch'); ?>
		<?php echo $form->textField($model,'early_coming_punch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'late_going_punch'); ?>
		<?php echo $form->textField($model,'late_going_punch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_last_punch'); ?>
		<?php echo $form->textField($model,'first_last_punch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'halfday_by_duration'); ?>
		<?php echo $form->textField($model,'halfday_by_duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'halfday_duration_mins'); ?>
		<?php echo $form->textField($model,'halfday_duration_mins'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_by_duration'); ?>
		<?php echo $form->textField($model,'absent_by_duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_duration_mins'); ?>
		<?php echo $form->textField($model,'absent_duration_mins'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'partial_halfday_by_duration'); ?>
		<?php echo $form->textField($model,'partial_halfday_by_duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'partial_halfday_duration_mins'); ?>
		<?php echo $form->textField($model,'partial_halfday_duration_mins'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'partial_absent_by_duration'); ?>
		<?php echo $form->textField($model,'partial_absent_by_duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'partial_absent_duration_mins'); ?>
		<?php echo $form->textField($model,'partial_absent_duration_mins'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_for_prefix'); ?>
		<?php echo $form->textField($model,'absent_for_prefix'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_for_suffix'); ?>
		<?php echo $form->textField($model,'absent_for_suffix'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_for_both'); ?>
		<?php echo $form->textField($model,'absent_for_both'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_for_condition'); ?>
		<?php echo $form->textField($model,'absent_for_condition'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_type'); ?>
		<?php echo $form->textField($model,'absent_type',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'absent_late'); ?>
		<?php echo $form->textField($model,'absent_late'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'halfday_by_late'); ?>
		<?php echo $form->textField($model,'halfday_by_late'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'halfday_late_mins'); ?>
		<?php echo $form->textField($model,'halfday_late_mins'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'halfday_by_earlygoing'); ?>
		<?php echo $form->textField($model,'halfday_by_earlygoing'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'halfday_earlygoing_mins'); ?>
		<?php echo $form->textField($model,'halfday_earlygoing_mins'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->