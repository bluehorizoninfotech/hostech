<?php
/* @var $this RoleSettingsController */
/* @var $data RoleSettings */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('rset_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->rset_id), array('view', 'id'=>$data->rset_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('role_id')); ?>:</b>
	<?php echo CHtml::encode($data->role_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('short_name')); ?>:</b>
	<?php echo CHtml::encode($data->short_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ot_formula')); ?>:</b>
	<?php echo CHtml::encode($data->ot_formula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('min_ot')); ?>:</b>
	<?php echo CHtml::encode($data->min_ot); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_ot')); ?>:</b>
	<?php echo CHtml::encode($data->max_ot); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_inpunch')); ?>:</b>
	<?php echo CHtml::encode($data->last_inpunch); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('late_coming_gt')); ?>:</b>
	<?php echo CHtml::encode($data->late_coming_gt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('early_going_gt')); ?>:</b>
	<?php echo CHtml::encode($data->early_going_gt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weekly_off1')); ?>:</b>
	<?php echo CHtml::encode($data->weekly_off1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weekly_off2')); ?>:</b>
	<?php echo CHtml::encode($data->weekly_off2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weekly_off2_count')); ?>:</b>
	<?php echo CHtml::encode($data->weekly_off2_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('early_coming_punch')); ?>:</b>
	<?php echo CHtml::encode($data->early_coming_punch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('late_going_punch')); ?>:</b>
	<?php echo CHtml::encode($data->late_going_punch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_last_punch')); ?>:</b>
	<?php echo CHtml::encode($data->first_last_punch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('halfday_by_duration')); ?>:</b>
	<?php echo CHtml::encode($data->halfday_by_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('halfday_duration_mins')); ?>:</b>
	<?php echo CHtml::encode($data->halfday_duration_mins); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_by_duration')); ?>:</b>
	<?php echo CHtml::encode($data->absent_by_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_duration_mins')); ?>:</b>
	<?php echo CHtml::encode($data->absent_duration_mins); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('partial_halfday_by_duration')); ?>:</b>
	<?php echo CHtml::encode($data->partial_halfday_by_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('partial_halfday_duration_mins')); ?>:</b>
	<?php echo CHtml::encode($data->partial_halfday_duration_mins); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('partial_absent_by_duration')); ?>:</b>
	<?php echo CHtml::encode($data->partial_absent_by_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('partial_absent_duration_mins')); ?>:</b>
	<?php echo CHtml::encode($data->partial_absent_duration_mins); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_for_prefix')); ?>:</b>
	<?php echo CHtml::encode($data->absent_for_prefix); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_for_suffix')); ?>:</b>
	<?php echo CHtml::encode($data->absent_for_suffix); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_for_both')); ?>:</b>
	<?php echo CHtml::encode($data->absent_for_both); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_for_condition')); ?>:</b>
	<?php echo CHtml::encode($data->absent_for_condition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_type')); ?>:</b>
	<?php echo CHtml::encode($data->absent_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('absent_late')); ?>:</b>
	<?php echo CHtml::encode($data->absent_late); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('halfday_by_late')); ?>:</b>
	<?php echo CHtml::encode($data->halfday_by_late); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('halfday_late_mins')); ?>:</b>
	<?php echo CHtml::encode($data->halfday_late_mins); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('halfday_by_earlygoing')); ?>:</b>
	<?php echo CHtml::encode($data->halfday_by_earlygoing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('halfday_earlygoing_mins')); ?>:</b>
	<?php echo CHtml::encode($data->halfday_earlygoing_mins); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>