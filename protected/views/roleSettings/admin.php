<?php
/* @var $this RoleSettingsController */
/* @var $model RoleSettings */

$this->breadcrumbs=array(
	'Role Settings'=>array('index'),
	'Manage',
);

//$this->menu=array(
////	array('label'=>'List RoleSettings', 'url'=>array('index')),
////	array('label'=>'Create RoleSettings', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('role-settings-grid', {
		data: $(this).serialize()
	});
	return false;
});

    $('#rolesettings').click(function(){
        var modal = $(this).attr('data-target');
        $.ajax({
            method: 'POST',
            url: '".Yii::app()->createAbsoluteUrl('roleSettings/rolesettings')."',
        }).done(function (msg) {
            $(''+modal+'').html(msg);
         
            
        });
    
    });


");
?>
<div class="rolesettings-form-sec">
<div class="clearfix">
    <div class="add link pull-right">
        <?php echo CHtml::link('Add Role Settings', '', array('class' => 'btn blue', 'data-toggle' => "modal", 'data-backdrop' => "static", 'data-target' => "#roleSettings", 'id' => "rolesettings")); ?>
    <!--<input style="margin: 20px;" type="button" value="Role Settings" class="btn btn-default"  data-toggle="modal" data-backdrop="static" data-target="#roleSettings" id="rolesettings">-->
    </div>   
    <h1 class="role-settings-page-head">Manage Role Settings</h1>    
</div>    
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'role-settings-grid',
         'itemsCssClass' => 'table table-bordered',
         'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
         'nextPageLabel' => 'Next '),
         'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
	 'dataProvider'=>$model->search(),
	 'filter'=>$model,
	 'columns'=>array(
		array(
                    'header' => 'S.No.',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                ),array(
                    'class'=>'CButtonColumn',
                                'template' => '{view} {delete}',
                                'htmlOptions' => array('class' => 'width-80'),
                                'buttons' => array(
                                    'view' => array( // My custom button options
                                        'label' => '',
                                        'imageUrl'=>false,
                                        'url' => 'Yii::app()->createUrl("roleSettings/view", array("id"=>$data->rset_id,  "ajax"=>true));', // My ajax url
                                        //'imageUrl' => Yii::app()->baseUrl . '/images/items.png',
                                        'click' => 'function(e) {
                                                      $("#ajaxModal").remove();
                                                      e.preventDefault();
                                                      var $this = $(this)
                                                        , $remote = $this.data("remote") || $this.attr("href")
                                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                                      $("body").append($modal);
                                                      $modal.modal({backdrop: "static", keyboard: false});
                                                      $modal.load($remote);
                                                    }',
                                        'options' => array('data-toggle' => 'ajaxModal', 'class'=>'icon-eye icon-comn padding-4','title'=>'View'),
                                    ),
                                   
                                    'delete' => array(
                                        'label'=>'',
                                        'imageUrl' =>false,
                                        'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete',),
                                       ),
                                    
                                )
                ),

                array(
                    'name'=>'role_id',
                    'header'=>'Role',
                    'value'=>'$data->role->role'
                ),
		'short_name',
		/*
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		*/
		
	),
)); ?>

 

<div class="modal" id="roleSettings">
    <div class="modal-header">
        
        <button class="close" data-dismiss="modal">x</button>
        <h2 class="title">Category settings</h2>
    </div>
    
        <div class="modal-body">
       
    </div>
 
    
</div>
</div>


