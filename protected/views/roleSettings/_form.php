<?php
/* @var $this RoleSettingsController */
/* @var $model RoleSettings */
/* @var $form CActiveForm */


Yii::app()->clientScript->registerScript('myjquery', "
    $(document).ready(function () {
        setTimeout('$(\"html\").removeClass(\"js\")', 1000);

        $(document).on('change', '#roleid', function(){
            var roleid = $('#roleid').val();
            accountresults(roleid);

        });
        
        $(document).on('submit', '#payrollsettings', function (e) {
            e.preventDefault();
            var formdata = $('#payrollsettings').serialize();
            var url = $(this).attr('action');

            $.ajax({
                method: 'POST',
                url: url,
                data: formdata,
            }).done(function (msg) {

                $('#successmsg').show();
            });
        });
        
        
    });
    
    function accountresults(roleid){
            $.ajax({
                method: 'GET',
                dataType: 'json',
                data: {roleid:roleid},
                url: 'index.php?r=roleSettings/getsettings',
                success: function (data) {
                    if(data){
                        $.each(data.data, function (key, value) { 
                            $('#RoleSettings_'+key).val(value); 
                            
                            $('#RoleSettings_'+key).each(function(){
                                if($(this).attr('type') == 'checkbox'){ 
                                
                                    if($(this).attr('value') == '1'){
                                        $(this).prop('checked', true);
                                    }else{
                                        $(this).prop('checked', false);
                                    }
                                }
                            });
                            

                            if(key == 'weekly_off2_count'){
                                var checkedvals = value.split(','); 
                                for(var i=0; i< checkedvals.length; i++){
                                    $('#RoleSettings_'+key+'_'+i).prop('checked', true);
                                }
                            
                            }
                                

                        });
                    }else{
                        $('input,select').val('');
                    }

                }
            });
        
    }



");
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'role-settings-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=> array('class'=> 'form form-inline')
)); ?>

    <div class="row">
	<div class="form-group">
		<?php echo $form->labelEx($model,'role_id'); ?>
		<?php echo $form->dropDownList($model,'role_id', CHtml::listData(UserRoles::model()->findAll(array('condition' => "id!= 1")), 'id', 'role'), array('empty' => 'Please choose a category', 'id'=>'roleid')); ?>
		<?php echo $form->error($model,'role_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'short_name'); ?>
		<?php echo $form->textField($model,'short_name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'short_name'); ?>
	</div>
    </div>

    <div class="row">
        <div class="form-group">
		<?php echo $form->labelEx($model,'ot_formula'); ?>
		<?php echo $form->dropDownList($model,'ot_formula', array('0'=> 'OT not applicable', '1'=>'Early Coming + Late Going'), array('empty' => 'Please choose an option')); ?>
		<?php echo $form->error($model,'ot_formula'); ?>
	</div>

	<div class="form-group inline-div">
            <span>
		<?php echo $form->labelEx($model,'min_ot'); ?>
		<?php echo $form->textField($model,'min_ot', array( 'class' => 'width-60')); ?>
                <?php //echo $form->error($model,'min_ot'); ?>
            </span>
            <span>
                <?php echo $form->checkBox($model,'max_ot_yes', array('class' => 'margin-top-35 margin-bottom-35 margin-left-10 margin-right-10')); ?>
            </span>
            <span>
		<?php echo $form->labelEx($model,'max_ot'); ?>
		<?php echo $form->textField($model,'max_ot',array( 'class' => 'width-60')); ?>
		<?php //echo $form->error($model,'max_ot'); ?>
            </span>
	</div>
    </div>
	

    <div class="row">
        <div class="form-group col-md-4 inline-input">
                <?php echo $form->checkBox($model,'last_inpunch'); ?>
		<?php echo $form->labelEx($model,'last_inpunch'); ?>
		
		<?php echo $form->error($model,'last_inpunch'); ?>
	</div>

	<div class="form-group">
            <div class="inline-input">
		<?php echo $form->labelEx($model,'late_coming_gt', array('class'=>'margin-10')); ?>
		<?php echo $form->textField($model,'late_coming_gt',array('class' => 'width-60')); ?>
		<?php echo $form->error($model,'late_coming_gt'); ?> 
            </div>
            <div class="inline-input">
                <?php echo $form->labelEx($model,'early_going_gt', array('class'=>'margin-10')); ?>
		<?php echo $form->textField($model,'early_going_gt',array('class' => 'width-60')); ?>
		<?php echo $form->error($model,'early_going_gt'); ?>
            </div>
	</div>
    </div>

    <div class="row">
        <div class="form-group inline-input">
		<?php echo $form->checkBox($model,'weekly_off1_yes',array()); ?>
                <?php echo $form->labelEx($model,'weekly_off1_yes'); ?>
	</div>
        <div class="form-group">
		<?php 
                $days = array('Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday');
                echo $form->dropDownList($model,'weekly_off1',$days); ?>
	</div>
        <div class="form-group inline-input">
		<?php echo $form->checkBox($model,'weekly_off2_yes'); ?>
                <?php echo $form->labelEx($model,'weekly_off2_yes'); ?>
	</div>
        <div class="form-group inline-input">
		<?php echo $form->dropDownList($model,'weekly_off2',$days); ?>
            <span class="inline-check">
                    <?php echo $form->checkBoxList($model,'weekly_off2_count', array('1'=>'1st','2'=>'2nd','3'=>'3rd','4'=>'4th','5'=> '5th'), array('separator' => "")); ?>
                </span>
                    
	</div>
        
    </div>

    <div class="row">
	<div class="form-group inline-input">
            <?php echo $form->checkBox($model,'early_coming_punch'); ?>
            <?php echo $form->labelEx($model,'early_coming_punch'); ?>	
	</div>

	<div class="form-group inline-input">
            <?php echo $form->checkBox($model,'late_going_punch'); ?>
            <?php echo $form->labelEx($model,'late_going_punch'); ?>	
	</div>
    </div>

    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'first_last_punch'); ?>
            <?php echo $form->labelEx($model,'first_last_punch'); ?>
        </div>
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'deduct_breakhours'); ?>
            <?php echo $form->labelEx($model,'deduct_breakhours'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'halfday_by_duration'); ?>
            <?php echo $form->labelEx($model,'halfday_by_duration'); ?>
            <?php echo $form->textField($model,'halfday_duration_mins'); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'absent_by_duration'); ?>
            <?php echo $form->labelEx($model,'absent_by_duration'); ?>
            <?php echo $form->textField($model,'absent_duration_mins'); ?>
        </div>
    </div>


    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'partial_halfday_by_duration'); ?>
            <?php echo $form->labelEx($model,'partial_halfday_by_duration'); ?>
            <?php echo $form->textField($model,'partial_halfday_duration_mins'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group inline-input">
                <?php echo $form->checkBox($model,'partial_absent_by_duration'); ?>
		<?php echo $form->labelEx($model,'partial_absent_by_duration'); ?>
		<?php echo $form->textField($model,'partial_absent_duration_mins'); ?>
	</div>
    </div>


    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'absent_for_prefix'); ?>
            <?php echo $form->labelEx($model,'absent_for_prefix'); ?>
            
        </div>
    </div>

    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'absent_for_suffix'); ?>
            <?php echo $form->labelEx($model,'absent_for_suffix'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'absent_for_both'); ?>
            <?php echo $form->labelEx($model,'absent_for_both'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'absent_for_condition'); ?>
            <?php echo $form->labelEx($model,'absent_for_condition'); ?>
            <?php echo $form->dropDownList($model,'absent_type',array('halfday'=>'Half Day', 'fullday'=>'Full Day')); ?>
            <?php echo $form->labelEx($model,'absent_late'); ?>
            <?php echo $form->dropDownList($model,'absent_late', array('1'=>'1')); ?><label>Days</label>
        </div>
            
    </div>
    
    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'halfday_by_late'); ?>
            <?php echo $form->labelEx($model,'halfday_by_late'); ?>
            <?php echo $form->textField($model,'halfday_late_mins'); ?>
        </div>
    </div>


    <div class="row">
        <div class="form-group inline-input">
            <?php echo $form->checkBox($model,'halfday_by_earlygoing'); ?>
            <?php echo $form->labelEx($model,'halfday_by_earlygoing'); ?>
            <?php echo $form->textField($model,'halfday_earlygoing_mins'); ?>
        </div>
    </div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->