<?php
/* @var $this RoleSettingsController */
/* @var $model RoleSettings */

$this->breadcrumbs=array(
	'Role Settings'=>array('index'),
	$model->rset_id=>array('view','id'=>$model->rset_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RoleSettings', 'url'=>array('index')),
	array('label'=>'Create RoleSettings', 'url'=>array('create')),
	array('label'=>'View RoleSettings', 'url'=>array('view', 'id'=>$model->rset_id)),
	array('label'=>'Manage RoleSettings', 'url'=>array('admin')),
);
?>

<h1>Update RoleSettings <?php echo $model->rset_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>