<?php
/* @var $this RoleSettingsController */
/* @var $model RoleSettings */

$this->breadcrumbs=array(
	'Role Settings'=>array('index'),
	$model->rset_id,
);

$this->menu=array(
	array('label'=>'List RoleSettings', 'url'=>array('index')),
	array('label'=>'Create RoleSettings', 'url'=>array('create')),
	array('label'=>'Update RoleSettings', 'url'=>array('update', 'id'=>$model->rset_id)),
	array('label'=>'Delete RoleSettings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->rset_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RoleSettings', 'url'=>array('admin')),
);
?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Your Model header</h4>
        </div>
        <div class="modal-body">
            <div class="well">
                  <h1>View RoleSettings #<?php echo $model->rset_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'rset_id',
		'role_id',
		'short_name',
		'ot_formula',
		'min_ot',
		'max_ot',
		'last_inpunch',
		'late_coming_gt',
		'early_going_gt',
		'weekly_off1',
		'weekly_off2',
		'weekly_off2_count',
		'early_coming_punch',
		'late_going_punch',
		'first_last_punch',
		'halfday_by_duration',
		'halfday_duration_mins',
		'absent_by_duration',
		'absent_duration_mins',
		'partial_halfday_by_duration',
		'partial_halfday_duration_mins',
		'partial_absent_by_duration',
		'partial_absent_duration_mins',
		'absent_for_prefix',
		'absent_for_suffix',
		'absent_for_both',
		'absent_for_condition',
		'absent_type',
		'absent_late',
		'halfday_by_late',
		'halfday_late_mins',
		'halfday_by_earlygoing',
		'halfday_earlygoing_mins',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
        </div>
    </div>
</div>