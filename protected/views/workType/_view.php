<?php
/* @var $this WorkTypeController */
/* @var $data WorkType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->wtid), array('view', 'id'=>$data->wtid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_type')); ?>:</b>
	<?php echo CHtml::encode($data->work_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />


</div>