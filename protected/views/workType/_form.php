<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */
/* @var $form CActiveForm */
?>
<div class="form-worktypesec">
<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'work-type-form',
	'enableAjaxValidation'=>false,
	'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
));
Yii::app()->getModule('masters');
?>
    <div class="row">
            <div class="col-md-4 subrow">
                <?php echo $form->labelEx($model,'work_type'); ?>
                <?php echo $form->textField($model,'work_type',array('size'=>30,'maxlength'=>30,'class' => 'form-control input-medium','autocomplete'=>'off')); ?>
                <?php echo $form->error($model,'work_type'); ?>
            </div>
            <div class="col-md-4 subrow">
                <?php echo $form->labelEx($model,'daily_throughput'); ?>
                <?php echo $form->textField($model,'daily_throughput',array('size'=>30,'maxlength'=>30,'class' => 'form-control input-medium','autocomplete'=>'off')); ?>
                <?php echo $form->error($model,'daily_throughput'); ?>
            </div>
            <!-- <div class="subrow">
            <?php echo $form->labelEx($model,'rate'); ?>
            <?php echo $form->textField($model,'rate',array('class' => 'form-control input-medium','autocomplete'=>'off')); ?>
            <?php echo $form->error($model,'rate'); ?>
        </div> -->
        <div class="col-md-4 subrow">
            <?php echo $form->labelEx($model,'unit_id'); ?>
            <?php
            echo $form->dropDownList($model, 'unit_id', CHtml::listData(Unit::model()->findAll(array(
                                'select' => array('id,unit_title'),
                                'condition' => "status = 1" ,
                                'order' => 'unit_title',
                                'distinct' => true
                            )), 'id', 'unit_title'), array('class'=>'form-control input-medium','empty' => 'Choose a Unit'));
            ?>
            <?php echo $form->error($model,'unit_id'); ?>
        </div>
    </div>


    <div class="row buttons work-type-button">
        <div class="col-md-6 subrow">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
                        </div>
