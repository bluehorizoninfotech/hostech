<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */

$this->breadcrumbs=array(
	'Work Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List WorkType', 'url'=>array('index')),
	
);
?>

<h1>Create WorkType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>