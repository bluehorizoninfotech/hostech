<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */
Yii::app()->getModule('masters');
$this->breadcrumbs=array(
	'Work Types'=>array('index'),
	$model->wtid,
);

$this->menu=array(
	array('label'=>'List WorkType', 'url'=>array('index')),
	array('label'=>'Create WorkType', 'url'=>array('create')),
	array('label'=>'Update WorkType', 'url'=>array('update', 'id'=>$model->wtid)),
	array('label'=>'Delete WorkType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->wtid),'confirm'=>'Are you sure you want to delete this item?')),
	
);
?>

<h1>View WorkType #<?php echo $model->wtid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'wtid',
		'work_type',
		array(
		'name' => 'unit_id',
		'type' => 'raw',
		'value' => isset($model->unit->unit_title) ? $model->unit->unit_title : "",
	),
	),
)); ?>