<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */

$this->breadcrumbs=array(
	'Work Types'=>array('index'),
	$model->wtid=>array('view','id'=>$model->wtid),
	'Update',
);

$this->menu=array(
	array('label'=>'List WorkType', 'url'=>array('index')),
	array('label'=>'Create WorkType', 'url'=>array('create')),
	array('label'=>'View WorkType', 'url'=>array('view', 'id'=>$model->wtid)),
	
);
?>

<h1>Update WorkType <?php echo $model->wtid; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>