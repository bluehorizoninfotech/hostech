<?php
/* @var $this WorkTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Work Types',
);

$this->menu=array(
	array('label'=>'Create WorkType', 'url'=>array('create')),
	
);
?>

<?php if(Yii::app()->user->hasFlash('success')){?>
<div class="info">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php }elseif(Yii::app()->user->hasFlash('error')){ ?>
<div class="info">
    <?php echo Yii::app()->user->getFlash('error'); ?>

</div>
<?php } ?>
<div class="clearfix">
    <div class="add link pull-right">
        <?php
		
        $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
	   //echo $createUrl;die;
		echo CHtml::link('Add Work Type', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        ?>
    </div>
    <h1>Work Types</h1>
</div>
<div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$model->search(),
	'id'=>'work-type-form',
	'itemsCssClass' => 'table table-bordered',
	'filter'=>$model,
	'columns'=>array( 
		array('class' => 'IndexColumn', 'header' => 'S.No.',),
        array(
			'class'=>'CButtonColumn',
			'template' => '{view}{update}{delete1}',
			'htmlOptions' => array( "class"=>"width-80"),
            'buttons'=>array(
 
              'view' => array(
                    'label'=>'',
                    'imageUrl' =>false,
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View',),
                   ),

                'update' => array(
                    'label'=>'',
                    'imageUrl' =>false,
                    'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit'),
                  //  'visible'=>' Yii::app()->user->role==1',
                   ),

                 'delete1' => array(
                    'label'=>'',
					'imageUrl' =>false,
					'url' => 'Yii::app()->controller->createUrl("/WorkType/delete1", array("id"=>$data->wtid))',
					'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete',),
                    'visible'=>' Yii::app()->user->role==1',
                   ),
           ),
		),
        'work_type',
        'daily_throughput',
        // 'rate',  
        array(
            'name' => 'unit_id',
            'value' => '$data->unit->unit_title',
            'type' => 'raw',
            'filter' => CHtml::listData(Unit::model()->findAll(
                            array(
                                'select' => array('id,unit_title'),                            
                                'order' => 'id',
                                'distinct' => true
                    )), "id", "unit_title")
        ),
       
		
	),
)); 



Yii::app()->clientScript->registerScript(
			'myHideEffect',
			'$(".info").animate({opacity: 1.0}, 2000).fadeOut("slow");',
			CClientScript::POS_READY
);

	?>
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add User',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => 300,
    ),
));
?>
<iframe id="cru-frame" width="550" height="430" frameborder="0" class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>
<?php
    Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   
  } );

');
?>
</div>