<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<div class="dailyworkprogress-gallery-sec">
<div class="clearfix project-filer">
    <div class="pull-left advanced-search">
        <?php
        $milestone_model = new Milestone();
        $this->renderPartial('_gallerysearch', array(
            'model' => $model, 'milestone_model' => $milestone_model
        ));
        ?>
    </div>
</div>
<div id='actionmsg'></div>
<div class="report_form">
    <div>
        <input type="button" class='requestaction btn btn-danger' name='include_report' id='include_report' value='Include in Report' />

    </div>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'daily-work-progress-grid',
    'afterAjaxUpdate' => 'reinstallDatePicker',
    'itemsCssClass' => 'table table-bordered',
    // 'template' => "<div class='table-responsive'>{items}</div>",
    'pager' => array(
        'id' => 'dataTables-example_paginate',  'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->gallery(),
    'filter' => $model,
    'columns' => array(
        array(
            'id' => 'selected_tasks',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
            'value'=>'$data->image_id',

        ),
        array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'),),

        array(
            'name' => 'img_paths',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'image_handler'),
            'value' =>  function ($model) {
                // $res = str_replace(array(
                //     '\'', '"',
                //     ',', '[', ']', '>'
                // ), '', $model->img_paths);
                $res=$model->image_path;
                return CHtml::image(Yii::app()->request->baseUrl . '/uploads/dailywork_progress/' . $res);
            },
            'filter' => false,


        ),
        array(
            'name' => 'project_id',
            'value' => '$data->project->name',
            'filter' => false,
        ),

        array(
            'header' => 'Status',
            'value' =>
            function ($data) {
                echo    $data->getreportStatus($data->image_id);
            },
        ),


    ),
));

?>
</div>
<script>
    $('.report_form').hide();
    $('#selected_tasks_all').click(function() {
        var checkboxes = document.getElementsByTagName('input');
        $('.report_form').slideDown();
        if (this.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                    $('div.checker span').addClass('checked');
                }
            }

        } else {
            $('.report_form').slideUp();
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                    $('div.checker span').removeClass('checked');
                }
            }
        }

    });
    $('input[name="selected_tasks[]"]').click(function() {
       
        var countchecked = $('input[name="selected_tasks[]"]:checked').length;
        if (countchecked >= 1) {
            $('.report_form').slideDown();
        } else if (countchecked == 0) {

            $('.report_form').slideUp();
        }
        if (this.checked) {
            $('.report_form').slideDown();

        } else {
            var all = [];
            $("#selected_tasks_all").prop('checked', false);
            $('div#uniform-selected_tasks_all span').removeClass('checked');
        }
    });
    $('#include_report').click(function() {
        if (confirm('Are you sure want to include in reports?')) {
            var images = [];
            $('input[name="selected_tasks[]"]:checked').each(function() {
                images.push(this.value)
                $.ajax({
                    method: "post",
                    dataType: "json",
                    data: {
                        images: images,
                    },
                    url: '<?php echo Yii::app()->createUrl("/ReportImages/saveImages") ?>',
                    success: function() {
                        $('#actionmsg').addClass('alert alert-success');
                        $('#actionmsg').html('successfully included');
                        window.setTimeout(function() {
                            location.reload()
                        }, 2000)
                    }
                });
            });
        }

    });
</script>