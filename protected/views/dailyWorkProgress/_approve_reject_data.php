<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

?>
<div class="approve-reject-data-sec">
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
		'id'=>'daily-work-progress-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));   
    ?>


    <div class="row">
        <div class="subrow">
			<?php echo $form->labelEx($model,'approve_reject_data'); ?>
			<?php echo $form->textArea($model,'approve_reject_data',array('class' => 'form-control input-medium')); ?>
			<?php echo $form->error($model,'approve_reject_data'); ?>           
        </div>
    </div>	
  
    <div class="row">
      
            <?php echo CHtml::submitButton('Reject', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Cancel', array('class' => 'btn default cancel')); ?>
        
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('.cancel').click(function(){
        $("#cru-dialog").dialog("close");
    });
   
});

</script>