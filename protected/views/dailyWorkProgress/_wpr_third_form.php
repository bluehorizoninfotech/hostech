<div class="d-flex justify-content-between wpr-border-bottom">
                    <div class="progress-table-head">
                      <h2 class="font-14 margin-bottom-0">Resources Utillised</h2>
                    </div>
                    <div>
                      <button id="resource-new-btn" class="margin-left-14 btn work-progress-table-save-botton">New</button>
                    </div>
                  </div>
                  <div class="row margin-top-20">
                    <div class="col-md-4 col-xs-12">
                      <label>Resource utilised</label>
                      <select class="form-control" id="resources_used" name="DailyWorkProgress[resources_used][]">
                        <option value="">Choose a resource</option>
                        <?php
                        foreach ($resource_utilised as $resources) {
                        ?>
                          <option value="<?php echo $resources->id ?>"><?php echo $resources->resource_name ?></option>
                        <?php
                        }
                        ?>

                      </select>
                      <div class="errorMessage" id="DailyWorkProgress_consumed_resource_em_" style=""></div>
                    </div>
                    <div class="col-md-3 col-xs-4">

                      <label>Quantity</label>
                      <input class="form-control img_comp_class consumed-resource-qty" autocomplete="off" name="DailyWorkProgress[utilised_qty][]" id="resource_quantity" type="text">
                      
                    </div>
                    <div class="col-md-3 col-xs-4">
                      <label>Unit</label>
                      <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[uom][]" type="text" id="uom" readonly>
                    </div>
                    <div class="width-150 display-none1">
                      <input class="form-control " autocomplete="off" name="DailyWorkProgress[unit_id][]" type="hidden" id="unit_id" >
                      <input type="hidden" id="resourceId">
                    </div>

                    <div class="col-md-2 col-xs-4">
                      <input type="button" id="add_new_resource" value="Save" class="btn blue blue-button-style">
                      <input type="hidden" value="create" id="resource_action_id">
                    </div>
                  </div>
                  <!-- </div> -->
                  <br>
                  <div class="work-progress-table">
                 
                  <?php
                  $resourceModel = new WprResourceUsed('search');
                
                  if($WPRID != "")
                  {
                    $resourceModel->wpr_id = $WPRID;
                  }
                  else
                  {
                    if (Yii::app()->user->getState('wpr_id') != "") {
                      $resourceModel->wpr_id = Yii::app()->user->getState('wpr_id');
                   }
                  }
                  
                  




                  $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'resource-used-grid',
                    'dataProvider' => $resourceModel->search(),
                    'itemsCssClass' => 'table table-bordered',
                    'pager' => array(
                        'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                        'nextPageLabel' => 'Next '
                    ),
                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                    'columns' => array(
                        array('class' => 'IndexColumn', 'header' => 'S.No.'),
                        array(
                            'name' => 'resource_id',
                            'value' => function ($data) {
                                echo $data->getResourceName($data->resource_id);
                              },
                            
                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                        ),
                        array(
                            'name' => 'resource_unit',
                            'value' => function ($data) {
                                echo $data->getResourceUnit($data->resource_unit);
                              },
                            
                            
                        ),
                        array(
                            'name' => 'resource_qty',
                            //'value' => ''
                        ),

                        array(
                          'name' => 'resource_id',
                          'headerHtmlOptions' => array('style' => 'display:none'),
                          'htmlOptions' => array('style' => 'display:none', 'class' => 'rowId'),
                          'filterHtmlOptions' => array('style' => 'display:none'),
                          //'value' => ''
                      ),
                        array(
                            'class' => 'CButtonColumn',
                          

                            'template'=>'{update}{delete}',


                            'buttons' => array(
                                'update' => array(
                                    'label' => '',
                                    'imageUrl' => false,
                                     'click' => 'function(e){e.preventDefault();editResource($(this).attr("href"));}',
                                    'options' => array('class' => 'actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'
                                    
                                  ),
                                ),

                                'delete' => array(
                                    'label' => '',
                                    'imageUrl' => false,
                                   
                                    
                                    
                                    'click' => 'function(e){e.preventDefault();deleteResource($(this).attr("href"));}',
                                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                                ),
                            ),
                        ),
                    ),
                ));




                  ?>
                       
                  </div> <!-- cgrid end -->
                  <div class="d-flex justify-content-between">
                    <div>
                      <!-- <button class="btn wpr-back-button">Back</button> -->
                    </div>
                    <div>
                      <button id="save_and_proceed_resource" class="btn blue save-proceed-btn-style">Save & Proceed</button>
                    </div>
                  </div>