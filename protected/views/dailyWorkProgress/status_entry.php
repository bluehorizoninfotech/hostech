
<?php
// echo $quantity;exit;
?>
<div class="panel-heading form-head">
  
<a class="closebtn pull-right closeform" onclick='closeform("statusform")'>X</a>
      
      <h3>Add Work Progress</h3>
   </div>

   <div class="form">

   <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'daily-work-progress-form',   
	'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => true,),
)); ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php //echo $form->errorSummary($model); ?>
<div class="panel-body">
 <div class="row">    
       
            
          
           
            <div class="col-md-6">
            <?php
              $task = Tasks::model()->findByPk($task_id);
              $work_type = $task->work_type_id;
              $tsk_id =  $task->tskid;
              $quantity = $task->quantity;
            

            ?>
         
            <?php echo $form->labelEx($model,'qty'); ?>
            <?php echo $form->textField($model,'qty',array('class' => 'form-control img_comp_class ','data-qty'=>$quantity,'data-task'=>$tsk_id,'autocomplete'=>'off',
			'name'=>'DailyWorkProgress[qty]',
			'ajax' => array(
						'url'=>array('DailyWorkProgress/getprogress'),
            			'type'=>'POST',
						'data'=>array('qty'=>'js:this.value',
									  'item_total_qty'=>'js:$(this).attr("data-qty")',
									  'item_id'=>'js:$(this).attr("data-task")',									   
                                        ),
                        'dataType'=>'json',
						'success' => 'function(data){                       
							 if(data.progress==0){
							 	$("#DailyWorkProgress_qty").val("");
                                 $("#DailyWorkProgress_daily_work_progress").val("");                                
                                 $("#DailyWorkProgress_qty_em_").css("display", "");
                                $("#DailyWorkProgress_qty_em_").text(data.msg);
							 }else{                               
								$("#DailyWorkProgress_daily_work_progress").val(data.progress);
							 }					
							
						}',
                        
                        )
			
			)); ?>           
            <?php echo $form->error($model,'qty'); ?>

            </div>
            <div class="col-md-6">
            <?php echo $form->labelEx($model,'total_quantity'); ?>
            <?php echo $form->textField($model,'total_quantity',array('class' => 'form-control','autocomplete'=>'off','readonly'=>true,'value'=>$quantity)); ?>
           <span id="unit_value"><?php echo $unit; ?></span>
            <?php echo $form->error($model,'total_quantity'); ?>
        </div>
        </div>
        <div class="row">
            <div class="col-md-6">          
            <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textArea($model,'description',array('class' => 'form-control','autocomplete'=>'off')); ?>
        <?php echo $form->error($model,'description'); ?>
            </div>
          
            <div class="col-md-6">
        <?php echo $form->labelEx($model,'daily_work_progress'); ?>
        <?php echo $form->textField($model,'daily_work_progress',array('class' => 'form-control','autocomplete'=>'off','readonly'=>true)); ?>
        <?php echo $form->error($model,'daily_work_progress'); ?>
    </div>
            </div>
           
      
   
	
    </div>
   
    <div class="row buttons text-center">
        <?php echo CHtml::Submitbutton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn blue')); ?>
       
       <?php
        if($type ==0){
            echo CHtml::resetButton('Close', array('onclick'=>"closeform('statusform')",'class'=>'btn')); 
            echo CHtml::resetButton('Reset', array('class' => 'btn default'));
        }
       ?>
        
    </div>
</div>
    <?php $this->endWidget(); ?>

</div><!-- form -->


