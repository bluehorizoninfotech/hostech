<html><head><title>Task Notification</title></head>
			<body>
			<?php
			if(!empty($ahead)){
			?>
			<p><h2>Ahead Task Notification</h2></p>
			<table border='1' class='border-collapse width-100-percentage'><tr>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Sl no</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Task Id</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Task Name</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Project Name</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Ahead Percentage</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Required Percentage</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Assignee</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Start Date</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Due Date</th>
					   </tr>
					   <?php 
					   $k=1;
					   
					 foreach($ahead as $values)  {
                         if(!empty($values)){
							 $task_id = $values->tskid;
							 $task    = Tasks::model()->findByPk($task_id);							
						 ?>
						<tr>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $k?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['tskid']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['title']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task->project->name?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $values->ahead_percent.'%'?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $values->required_percent.'%'?></td>
                            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $this->getAssigneedeatils($task['assigned_to'])?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['start_date']))?></td>
                            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['due_date']))?></td>
							
						</tr>
						 <?php
						 $k++;
                     }
                    }
					   ?>
				
		</table>
				<?php } ?>
<br/>
				<?php
			if(!empty($behind)){
			?>
			<p><h2>Behind Task Notification</h2></p>
			<table border='1' class='border-collapse width-100-percentage'><tr>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Sl no</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Task Id</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Task Name</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Project Name</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Behind Percentage</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Required Percentage</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Assignee</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Start Date</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Due Date</th>
					   </tr>
					   <?php 
					   $k=1;
					   
					 foreach($behind as $values)  {
                         if(!empty($values)){
							 $task_id = $values->tskid;
							 $task    = Tasks::model()->findByPk($task_id);							
						 ?>
						<tr>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $k?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['tskid']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['title']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task->project->name?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $values->behind_percent.'%'?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $values->required_percent.'%'?></td>
                            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $this->getAssigneedeatils($task['assigned_to'])?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['start_date']))?></td>
                            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['due_date']))?></td>
							
						</tr>
						 <?php
						 $k++;
                     }
                    }
					   ?>
				
		</table>
				<?php } ?>

				<br/>
				<?php
			if(!empty($onschedule)){
			?>
			<p><h2>On-schedule Task Notification</h2></p>
			<table border='1' class='border-collapse width-100-percentage'><tr>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Sl no</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Task Id</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Task Name</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Project Name</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Assignee</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Start Date</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-left-8 padding-right-8'>Due Date</th>
					   </tr>
					   <?php 
					   $k=1;
					   
					 foreach($onschedule as $values)  {
                         if(!empty($values)){
							 $task_id = $values->tskid;
							 $task    = Tasks::model()->findByPk($task_id);							
						 ?>
						<tr>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $k?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['tskid']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['title']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task->project->name?></td>
				            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $this->getAssigneedeatils($task['assigned_to'])?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['start_date']))?></td>
                            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['due_date']))?></td>
							
						</tr>
						 <?php
						 $k++;
                     }
                    }
					   ?>
				
		</table>
				<?php } ?>
		</body>
		</html>