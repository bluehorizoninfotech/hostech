<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<?php
/* @var $this DailyWorkProgressController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Daily Work Progresses',
);

$settings = GeneralSettings::model()->find(['condition' => 'id = 1']);
?>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<div class="pending-request-sec">
<div class="clearfix daily-work-header">
    <h1 class="pull-left">Pending Requests</h1>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/themes/btheme2/assets/admin/layout3/scripts/autocomplete.js', CClientScript::POS_END); ?>

</div>
<!-- <div class="prev_curr_next clearfix">
        <div class="form-group">
            <div>
            <?php
            if (Yii::app()->user->role == 1) {
                $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
            } else {
                $criteria = new CDbCriteria;
                $criteria->select = 'project_id';
                $criteria->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                $criteria->group = 'project_id';
                $project_ids = Tasks::model()->findAll($criteria);
                $project_id_array = array();
                foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                }
                if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
                } else {
                    $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'order' => 'name ASC');
                }
            }
            $project_list = CHtml::listData(
                Projects::model()->findAll($condition),
                'pid',
                'name'
            );
            // echo '<pre>';print_r($project_list);exit;
            if (Yii::app()->user->project_id != "") {
                $selected = Yii::app()->user->project_id;
            } else {
                $selected = '';
            }
            ?>
            <div class="subrow col-md-2 col-sm-6 form-group">
                <label style="">Project: </label>
                <?php echo  CHtml::dropDownList(
                    'project',
                    $selected,
                    $project_list,
                    array('class' => 'form-control')
                );; ?>
            </div>

                
            </div>
        </div>
    </div> -->




<div class="alert <?= Yii::app()->user->hasFlash('error') ? "" : "hide" ?>">
    <?php

    if (Yii::app()->user->hasFlash('error')) {
        echo Yii::app()->user->getFlash('error');
    }
    ?>
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>

</div>
<div id="boq_entries">

    <?php
    $project_condition = '';
    if (Yii::app()->user->project_id != "") {
        $project_condition .= 'pid = ' . Yii::app()->user->project_id;
    } else {
        if (Yii::app()->user->role != 1) {
            $criteria_proj = new CDbCriteria;
            $criteria_proj->select = 'project_id';
            $criteria_proj->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
            $criteria_proj->group = 'project_id';
            $project_ids = Tasks::model()->findAll($criteria_proj);
            $project_id_array = array();
            foreach ($project_ids as $projid) {
                array_push($project_id_array, $projid['project_id']);
            }

            if (!empty($project_id_array)) {
                $project_id_array = implode(',', $project_id_array);
                $project_condition .= 'pid IN (' . $project_id_array . ')';
            } else {
                $project_condition .= 'pid = NULL';
            }
        }
    }
    ?>
<div id='actionmsg'></div>
    <div class="approve_form">
        <div>
            <textarea name="reason" class="reason"></textarea>

            <input type="button" class='requestaction btn btn-danger' name='reject' value='Reject' />

            <input type="button" class='requestaction btn btn-success margin-left-5' name='reject' value='Approve' />

            <?php
            echo CHtml::image(
                Yii::app()->request->baseUrl . '/images/loading.gif',
                'Loading....',
                array('class' => 'width-30 margin-left-159 display-none')
            );
            ?>
        </div>
    </div>
<div class="table-responsive">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'daily-work-progress-grid',
        'afterAjaxUpdate' => 'reinstallDatePicker',
        'itemsCssClass' => 'table table-bordered',
    //    'template' => "<div class='table-responsive'>{items}</div>",
        'pager' => array(
            'id' => 'dataTables-example_paginate',  'header' => '', 'prevPageLabel' => 'Previous ',
            'nextPageLabel' => 'Next '
        ),
        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
        'dataProvider' => $model->pendingRequests($interval),
        'filter' => $model,
        'columns' => array(
            array(
                'id' => 'selected_tasks',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => '50',

            ),
            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'),),
            //'id',
            array(
                'htmlOptions' => array('class' => 'width-80'),
                'class' => 'ButtonColumn',
                'template' => '{view}{approve_btn}{approved}{reject_btn}{rejected}{recall}',
                'evaluateID' => true,

                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View'),
                    ),
                    'approve_btn' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/approveentry", Yii::app()->session["menuauthlist"])',
                        'options' => array('class' => 'fa fa-thumbs-up approve', 'title' => 'Approve', 'id' => '$data->id'),
                    ),
                    'approved' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '$data->approve_status=="1" ',
                        'options' => array('class' => 'fa fa-check approved', 'title' => 'Approved', 'id' => '$data->id'),
                    ),
                    'reject_btn' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '($data->approve_status=="0" ) && ($data->tasks->status != "7")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                        // 'url' => 'Yii::app()->createUrl("dailyWorkProgress/approverejectdata", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid","stat"=>2))',
                        // 'click' => 'function(e){e.preventDefault();$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                        // 'options' => array('class' => 'fa fa-thumbs-down margin_5 reject reject-entry', 'title' => 'Reject','id'=>'$data->id'),

                        'options' => array('class' => 'fa fa-thumbs-down margin_5 reject reject-entry', 'title' => 'Reject','id'=>'$data->id'),
                    ),
                    'rejected' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '($data->approve_status=="2")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                        'options' => array('class' => 'fa fa-ban rejected', 'title' => 'Rejected', 'id' => '$data->id'),
                    ),
                    'recall' => array(
                        'label' => 'Recall',
                        'imageUrl' => false,
                        'visible' => '($data->approve_status=="2")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                        'options' => array('class' => 'margin_5 recall', 'title' => 'Recall', 'id' => '$data->id'),
                    ),
                )

            ),
            array(
                'name' => 'date',
                'value' => 'date("d-M-y", strtotime($data->date))',
                'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'          => $model,
                    'attribute'      => 'date',
                    'options' => array('dateFormat' => 'd-M-y'),
                    'htmlOptions' => array('class' => 'width-100'),
                ), true),
            ),
            array(
                'name' => 'project_id',
                'value' => '$data->project->name',
                'filter' => CHtml::listData(Projects::model()->findAll(
                    array(
                        'select' => array('pid,name'),
                        'condition' => $project_condition,
                        'order' => 'name',
                        'distinct' => true
                    )
                ), "pid", "name")
            ),
            array(
                'name' => 'taskid',
                'value' => '(isset($data->tasks)?$data->tasks->title:"")',
                'filter' => false,
            ),
            //'project_id',
            //'item_id',

            array(
                'name' => 'qty',
                'value' => function ($data) {
                    if (!empty($data->tasks->unit)) {
                        $unit = Unit::model()->findByPk($data->tasks->unit);
                        echo $data->qty . ' (' . $unit->unit_code . ')';
                    } else {
                        echo $data->qty;
                    }
                },
                'filter' => false,
            ),

            array(
                'name' => 'work_type',
                'value' => '(isset($data->worktype)?$data->worktype->work_type:"")',
                'filter' => false,
            ),
            // 'daily_work_progress',
            array(
                'name' => 'daily_work_progress',
                'value' => function ($data) {
                    echo round($data->daily_work_progress, 2);
                },
                'filter' => false,
            ),           
            array(
                'name' => 'current_status',
                'value' => function ($data) {
                    if (!empty($data->current_status)) {
                        echo $data->status0->caption;
                    } else {
                        echo $data->getStatus($data->taskid);
                    }
                },
                'filter' => false,
               
            ),
          
            array(
                'name' => 'approve_reject_data',
                'value' => function ($data) {
                    if ($data->approve_status != 1) {
                        echo $data->approve_reject_data;
                    }
                },
                'filter' => false,
            ),
            array(
                'name' => 'description',
                'filter' => false,
            ),
            array(
                'name' => 'created_by',
                'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
                'filter' => false,
                'visible' => Yii::app()->user->role == 1 || Yii::app()->user->role == 9,
            ),
            

            /*array(
			'class'=>'CButtonColumn',
			 'htmlOptions' => array('width' => '90px'),
			 'template' => '{delete}',
		),*/
        ),
    ));
    Yii::app()->clientScript->registerScript('re-install-date-picker', "
    function reinstallDatePicker(id, data) {
        $('#DailyWorkProgress_date').datepicker();
    }
");


    ?>
</div>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Reject BOQ Entry',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0" class="min-height-200"></iframe>
<?php
$this->endWidget();
?>
<div id="view_progress" class="view_progress"></div>
</div>

<div id='popup1'>
    <div class="popup-header clearfix">
        <h5 class="text-center margin-bottom-10">WPR Reject &nbsp;</h5>
        <span class="close-icon">&times;</span> <!-- Close icon (X symbol) -->
        <div class="approve-reject-data-sec pending-req">
            <div class="form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'daily-progress-form',
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,),
                ));   
                ?>
                <div class="row">
                    <div class="subrow">
                        <input type="hidden" id="wpr_id">
                        <?php echo $form->labelEx($model,'approve_reject_data'); ?>
                        <?php echo $form->textArea($model,'approve_reject_data',array('class' => 'form-control')); ?>
                        <?php echo $form->error($model,'approve_reject_data'); ?>           
                    </div>
                    <?php echo $form->hiddenField($model, 'id'); ?>
                </div>	
                <div class="row">
                    <?php echo CHtml::submitButton('Reject', array('class' => 'btn btn-sm blue','id'=>'rejectentry')); ?>
                    <?php echo CHtml::resetButton('Cancel', array('class' => 'btn btn-sm default cancel')); ?>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $('#daily-work-progress-form').on('beforeSubmit', function(e) {
        $('#submit_boq').css('disable', 'disable');

        return false;

    });


    $(function() {
        $("#datepicker").datepicker({

            onSelect: function(selectedDate) {
                var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/index&interval='); ?>' + selectedDate;
                $(location).attr('href', url);
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        var date1 = '<?php echo $settings->backlog_days; ?>';

        $('#DailyWorkProgress_date').datepicker({
            dateFormat: 'dd-M-y',
            minDate: -date1,

        });
        $(document).on('click', ".approve", function() {
            if (!confirm("Do you want to Approve ?")) {} else {
                $('.loaderdiv').show();
                var entry_id = this.id;
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approveentry'); ?>',
                    data: {
                        entry_id: entry_id
                    },
                    method: "GET",
                    dataType: 'json',
                    success: function(result) {
                        if (result.status == '1') {
                            $("#boq_entries").load(location.href + " #boq_entries>*", "");
                            $('.alert').removeClass('hide');
                            // $('<p>' + result.schedule + '</p>').appendTo('.alert');
                            // $('<p>' + result.dependant + '</p>').appendTo('.alert');
                            $('.alert').append(result.schedule);
                            $('.alert').append("<br/>");
                            $('.alert').append(result.dependant);
                            $('.loaderdiv').hide();
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        } else {
                            $('.alert').removeClass('hide');
                            $('.alert').css('display', 'block');
                            $('.alert').append(result.message);
                            $('.loaderdiv').hide();
                        }



                    }
                })
            }


        });

        $(document).on('click', ".recall", function() {
            if (!confirm("Do you want to Reject ?")) {} else {
                $('.loaderdiv').show();
                var entry_id = this.id;
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/recall'); ?>',
                    data: {
                        entry_id: entry_id
                    },
                    method: "GET",
                    dataType: 'json',
                    success: function(result) {
                        if (result.status == '1') {
                            $("#boq_entries").load(location.href + " #boq_entries>*", "");
                            // $('.alert').removeClass('hide');                 
                            // $('.alert').append(result.message);                   
                            $('.loaderdiv').hide();
                        }

                    }
                })
            }


        });
        $(".icon-eye").click(function(e) {
            e.preventDefault();
            var id = $(this).closest('tr').find('input[type="checkbox"]').val();
            if (id != '') {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->createUrl('/dailyWorkProgress/view'); ?>',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        $(".view_progress").html(data);
                        $('.boq_details_sec').animate({
                            height: 'show'
                        }, 500);
                    }
                })
            }
        })

        function entrydatevalidation(date) {
            var project_id = $('#DailyWorkProgress_project_id').find(":selected").val();
            if (project_id !== '') {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/checkdate'); ?>',
                    data: {
                        date: date,
                        project_id: project_id
                    },
                    method: "POST",
                    dataType: 'json',
                    success: function(result) {
                        //    console.log(result);
                        $('#DailyWorkProgress_taskid').html(result.html);
                        $('#DailyWorkProgress_taskid').val(result.tsk_id);
                        $('#DailyWorkProgress_work_type').html(result.work_type_html);
                        $('#DailyWorkProgress_work_type').val(result.work_type);
                        $('#task_id_span1').text(result.tsk_id);
                        $('#DailyWorkProgress_total_quantity').val(result.quantity);
                        $("#unit_value").html(result.unit);
                        $("#item_total_qty").val(result.quantity);
                        $("#item_main_id").val(result.tsk_id);
                    }
                });
            }

        }
    
        $('.approve_form').hide();
    });

    function pendingdatas() {
        var selected_project = $('#project').find(":selected").val();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/pendingrequests'); ?>',
            data: {
                project_id: selected_project
            },
            method: "POST",
            success: function(result) {
                $("#daily-work-progress-grid").load(location.href + " #daily-work-progress-grid");
                // jQuery('#daily-work-progress-grid').yiiGridView('update');
            }
        })
    }

    $("#DailyWorkProgress_project_id").change(function() {
        var val = $(this).val();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getAllTasks'); ?>',
            data: {
                project_id: val
            },
            method: "GET",
            success: function(result) {
                $('.taskid').html(result);
            }
        })
    })

    $("#DailyWorkProgress_taskid").change(function() {
        var val = $(this).val();
        if (val != '') {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/gettaskdetails'); ?>',
                data: {
                    taskid: val
                },
                method: "GET",
                dataType: "json",
                success: function(result) {
                    $("#task_id_span1").html("Task Id: <b>" + result.taskid + "</b>");
                    $("#DailyWorkProgress_total_quantity").val(result.total_qty);
                    $("#item_total_qty").val(result.total_qty);
                    $("#item_main_id").val(result.taskid);
                    $("#DailyWorkProgress_work_type").html(result.option);
                    $("#DailyWorkProgress_work_type").val(result.work_type);
                    $('#unit_value').html(result.unit)
                }
            })
        } else {
            $("#task_id_span1").html("");
            $("#DailyWorkProgress_total_quantity").html("");
            $("#item_total_qty").val("");
            $("#item_main_id").val("");
        }
    })
</script>
<script>
         $('#selected_tasks_all').click(function() {            
            var checkboxes = document.getElementsByTagName('input');          
            if (this.checked) {
                $('.approve_form').slideDown();
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                        $('div.checker span').addClass('checked');
                    }
                }
            } else {
                $('.approve_form').slideUp();
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                        $('div.checker span').removeClass('checked');
                    }
                }
            }
        });
        $('input[name="selected_tasks[]"]').click(function() {
            var countchecked = $('input[name="selected_tasks[]"]:checked').length;
            if (countchecked >= 1) {
                $('.approve_form').slideDown();
            } else if (countchecked == 0) {
                $('.approve_form').slideUp();
            }
            if (this.checked) {
                $('.approve_form').slideDown();

            } else {
                var all = [];
                $("#selected_tasks_all").prop('checked', false);
                $('div#uniform-selected_tasks_all span').removeClass('checked');
            }
        });
        $(".requestaction").click(function() {
            $('.requestaction').attr('disabled', true);
            var req = $(this).val();
            var reason = $('.reason').val();
            var boq_ids = [];
            $('input[name="selected_tasks[]"]:checked').each(function() {
                boq_ids.push(this.value)
            });
            if (boq_ids != '') {
                if (reason == "") {
                    alert("please enter reason")
                    flag = 1;
                } else {
                    if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

                        $('.loaderdiv').show();
                        $.ajax({
                            method: "post",
                            dataType: "json",
                            data: {
                                ids: boq_ids,
                                req: req,
                                reason: reason,
                            },
                            url: '<?php echo Yii::app()->createUrl("/dailyWorkProgress/bulkReject") ?>',
                            success: function(ret) {
                                $('.loaderdiv').hide();
                                $('.requestaction').attr('disabled', false);
                                if (ret.status == 2) {
                                    $('#actionmsg').addClass('alert alert-danger');
                                    $('#actionmsg').html(ret.message);
                                    window.setTimeout(function() {
                                        location.reload()
                                    }, 2000)
                                } else {
                                    $('#actionmsg').addClass('alert alert-success');
                                    $('#actionmsg').html(ret.message);
                                    window.setTimeout(function() {
                                        location.reload()
                                    }, 2000)
                                }
                            }
                        });
                    } else {
                        $('#loadernew').hide();
                        $('.requestaction').attr('disabled', false);
                    }
                }
            } else {
                alert('Please select entry');
                $('.requestaction').attr('disabled', false);
            }
            console.log(boq_ids);
        });


        $(document).on('click', ".reject-entry", function(){ 
         var id = this.id;  
         $('#wpr_id').val(id);   
         $("#popup1").show();
        
        });
        $('#popup1 .close-icon').click(function() {
        
        $('#popup1').hide();
        });
        $(document).on('click', "#rejectentry", function(){       
        var reason = $('#DailyWorkProgress_approve_reject_data').val();
        var entry_id = $('#wpr_id').val();
       
        if(reason){
            $('.loaderdiv').show();
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approverejectdata'); ?>',
                data:{entry_id:entry_id,reason:reason},            
                method: "POST",
                success: function(result) {
                    $('.loaderdiv').hide();
                    if(result ==1){  
                        alert('Rejected Successfully');                  
                        location.reload();
                    }else{
                        alert('Something went wrong in reject');
                    }
                }
            }) 
        }else{
            alert('Please enter reason before reject');
        }
    });
</script>