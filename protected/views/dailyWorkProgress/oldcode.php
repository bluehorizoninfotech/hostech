<?php
    if (Yii::app()->user->role != 9) {
    ?>
      <div class="panel panel-default work_progress_form page-scroll-bar">
        <div class="panel-heading clearfix">
          <a class="close_work_progress pull-right"><b>X</b></a>
          <h3>Add Work Progress</h3>
        </div>
        <div class="panel-body">
          <p>Site:<span id="site_text"></span></p>
          <div class="form">
            <?php $form = $this->beginWidget('CActiveForm', array(
              'id' => 'daily-work-progress-form',
              'htmlOptions' => array('enctype' => 'multipart/form-data'),
              //'action' => Yii::app()->createUrl('/dailyWorkProgress/create'),
              'enableAjaxValidation' => true,
              'enableClientValidation' => true,
              'clientOptions' => array(
                'validateOnChange' => true,
                'validateOnType' => true,
              ),
            )); ?>
            <div class="row">
              <input type="hidden" name="newdetails" id="newdetails">
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'project_id'); ?>
                <?php
                if (Yii::app()->user->role == 1) {
                  $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
                } else {
                  $criteria = new CDbCriteria;
                  $criteria->select = 'project_id';
                  $criteria->condition = ' parent_tskid is not null and (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ")";
                  $criteria->group = 'project_id';
                  $project_ids = Tasks::model()->findAll($criteria);
                  $project_id_array = array();
                  foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                  }
                  if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
                  } else {
                    $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'order' => 'name ASC');
                  }
                }

                if (Yii::app()->user->project_id != "") {
                  $project_id = Yii::app()->user->project_id;
                }
                $project = Projects::model()->findAll($condition);
                $data_array = array();
                if (count($project) == 1) {
                  foreach ($project as $key => $value) {
                    array_push($data_array, $value->pid);
                  }
                  $id = implode(" ", $data_array);
                  $p_data =  Projects::model()->findByPk($id);
                ?>
                  <select class="form-control" name="DailyWorkProgress[project_id]" id="DailyWorkProgress_project_id">
                    <option value="<?php echo $p_data['pid']; ?>" <?php echo ($project_id == $p_data['pid']) ? 'selected' : ""; ?>><?php echo $p_data['name']; ?></option>
                  </select>
                <?php
                } else {
                  echo $form->dropDownList(
                    $model,
                    'project_id',
                    CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'),
                    array('empty' => '-Choose a Project-', 'class' => 'form-control')
                  );
                }
                ?>
                <?php echo $form->error($model, 'project_id'); ?>
              </div>
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control date', 'autocomplete' => 'off',)); ?>
                <?php echo $form->error($model, 'date'); ?>
                <div class="date_section" id="test_sample"></div>
              </div>
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'taskid'); ?>
                <?php
                $tasks = array();
                $work_type = "";
                $tsk_id = "";
                $quantity = "";
                $unit = "";
                if (Yii::app()->user->project_id != "") {
                  if (Yii::app()->user->role != 1) {
                    $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND  parent_tskid is not null and  project_id=' . Yii::app()->user->project_id . ' AND (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')', 'order' => 'title'));
                  } else {
                    $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND  parent_tskid is not null and  project_id=' . Yii::app()->user->project_id, 'order' => 'title'));
                  }
                } else {
                  $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1  AND   parent_tskid is not null and  (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')', 'order' => 'title'));
                }
                $data_array = array();
                $tsk_id = "";
                $work_type = "";
                if (count($tasks) == 1) {
                  foreach ($tasks as $key => $value) {
                    array_push($data_array, $value->tskid);
                  }
                  $tsk_id = implode(" ", $data_array);
                  $task = Tasks::model()->findByPk($tsk_id);
                  $work_type = $task->work_type_id;
                  $tsk_id = $tsk_id;
                  $quantity = $task->quantity;
                  if ($task->unit != 0) {
                    $units_data = Unit::model()->findByPk($task->unit);
                    $unit = $units_data->unit_code;
                  }
                }
                ?>
                <span id="task_id_span1"><?php echo $tsk_id; ?></span>




                <select class="form-control taskid" name="DailyWorkProgress[taskid]" id="DailyWorkProgress_taskid">
                  <option value="">-Choose a Task-</option>
                  <?php
                  if (!empty($tasks)) {
                    foreach ($tasks as $key => $value) {
                  ?>
                      <option value="<?php echo $value['tskid']; ?>" <?php echo ($value['tskid'] == $tsk_id) ? "selected" : ""; ?>>
                        <?php echo ($value['parent_tskid'] != null ? ' - ' : '') ?><?php echo $value['title']; ?></option>
                  <?php }
                  } ?>
                </select>
                <span class="details_item"><?php echo ((isset($itemdetails)) ? $itemdetails : ''); ?></span>
                <?php echo $form->error($model, 'taskid'); ?>
              </div>
              <div class="col-md-2 col-sm-3 form-group">
                <?php echo $form->labelEx($model, 'qty'); ?>
                <?php echo $form->textField($model, 'qty', array(
                  'class' => 'form-control img_comp_class', 'autocomplete' => 'off',
                  'name' => 'DailyWorkProgress[qty]', 'id' => 'zero',
                  'ajax' => array(
                    'url' => array('DailyWorkProgress/getprogress'),
                    'type' => 'POST',
                    'data' => array(
                      'qty' => 'js:this.value',
                      'item_total_qty' => 'js: $("#item_total_qty").val()',
                      'item_id' => 'js: $("#item_main_id").val()',

                    ),
                    'dataType' => 'json',
                    'success' => 'function(data){
                            if(data.progress==0){
                            $("#DailyWorkProgress_qty").val("0");
                            $("#DailyWorkProgress_daily_work_progress").val("0");                                
                            $("#DailyWorkProgress_qty_em_").css("display", "");
                            $("#DailyWorkProgress_qty_em_").text(data.msg);
                             setTimeout(function(){ location.reload(); }, 800);
                        }else{
                            $("#DailyWorkProgress_daily_work_progress").val(data.progress);
                            }
                            if(data.balance_qty !== ""){
                               
                               $("#balance_qty").text("Balance quantity: "+data.balance_qty);
                               $("#balance_quantity_id").val(data.balance_qty);
                            }

                            var status=$("#DailyWorkProgress_current_status").val();
                            var balance_quantity=data.balance_qty;

                            if(status!="")
                            {
                              if(balance_quantity == 0 && status!=7)
                          {
                            $("#DailyWorkProgress_current_status").val("");
                            $("#DailyWorkProgress_current_status_em_").css("display", "");
                            $("#DailyWorkProgress_current_status_em_").text("If the balance quantity is 0 then the status should be completed");
                          }
                          if(balance_quantity!=0 && status==7)
                                  {
                                  
                                   $("#DailyWorkProgress_current_status").val("");
                                 $("#DailyWorkProgress_current_status_em_").css("display", "");
                                 $("#DailyWorkProgress_current_status_em_").text("The balance quantity should be 0 if the status is to be completed");
                                  }
                            }
       
                        }',
                  )
                )); ?>
                <span id="balance_qty"></span>
                <?php echo $form->error($model, 'qty'); ?>
                <input type="hidden" id="balance_quantity_id">
              </div>


              <div class="col-md-2 col-sm-3 pos-rel form-group">
                <?php echo $form->labelEx($model, 'total_quantity'); ?>
                <?php echo $form->textField($model, 'total_quantity', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'value' => $quantity)); ?>
                <span id="unit_value"><?php echo $unit; ?></span>
                <?php echo $form->error($model, 'total_quantity'); ?>
              </div>
            </div>
            <div class="row">
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'work_type'); ?>
                <?php
                $condition = array('select' =>  array('wtid,work_type'), 'order' => 'work_type ASC');
                echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll($condition), 'wtid', 'work_type'), array('options' => array($work_type => array('selected' => true)), 'empty' => '-Choose a Work Type-', 'class' => 'form-control'));
                ?>
                <?php echo $form->error($model, 'work_type'); ?>
              </div>
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'current_status'); ?>
                <?php
                $condition_status = array('select' =>  array('sid,caption'), 'condition' => "status_type='task_status' AND sid NOT IN(6)", 'order' => 'caption ASC');
                echo $form->dropDownList($model, 'current_status', CHtml::listData(Status::model()->findAll($condition_status), 'sid', 'caption'), array('empty' => '-Choose status-', 'class' => 'form-control'));
                ?>
                <?php echo $form->error($model, 'current_status'); ?>

              </div>
              <div class="col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'daily_work_progress'); ?>
                <?php echo $form->textField($model, 'daily_work_progress', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true)); ?>
                <?php echo $form->error($model, 'daily_work_progress'); ?>
              </div>
              <div class="col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'remarks'); ?>
                <?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($model, 'description'); ?>
              </div>
              <div class="col-md-2 col-sm-6 form-group">
                <span class="img_span"> (WidthxHeight)(2048x1536)</span>
                <?php
                $this->widget('CMultiFileUpload', array(
                  'model' => $model,
                  'name' => 'image',
                  'attribute' => 'image',
                  'accept' => 'jpg|gif|png|jpeg',
                  'max' => 3,

                  'duplicate' => 'Already Selected',
                  'options' => array(
                    'onFileSelect' => 'function(e, v, m){ addLabel() }',

                  ),
                ));
                ?>
              </div>




              <div id="label_container"></div>



              <!-- <div class="col-md-2 col-sm-6 form-group">
                                <?php echo $form->labelEx($model, 'image label'); ?>
                                <?php echo $form->textField($model, 'image_label', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                <?php echo $form->error($model, 'image_label'); ?>
                            </div> -->


              <div class="">
                <?php echo CHtml::hiddenField('item_description', '', array('id' => 'item_description')); ?>
                <?php echo CHtml::hiddenField('item_total_qty', '', array('id' => 'item_total_qty', 'value' => $quantity)); ?>
                <?php echo CHtml::hiddenField('item_main_id', '', array('id' => 'item_main_id', 'value' => $tsk_id)); ?>
              </div>
              <?php echo $form->hiddenField($model, 'latitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "latitude")); ?>
              <?php echo $form->hiddenField($model, 'longitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "longitude")); ?>
              <?php echo $form->hiddenField($model, 'site_id', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_id")); ?>
              <?php echo $form->hiddenField($model, 'site_name', array('class' => 'form-control site_name', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_name")); ?>


              <!--   Projem section -->

              <div class="row margin-top-120">
                <?php if (in_array('/dailyWorkProgress/incident', Yii::app()->session['menuauthlist'])) { ?>
                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label>Incidents</label>
                    <select class="form-control" name="DailyWorkProgress[incident]" id="incident_id">
                      <option value="">Choose Incident</option>
                      <option value="1">Accidents </option>
                      <option value="2">Injuries </option>
                      <option value="3">Unusual Events </option>
                    </select>
                  </div>
                <?php } ?>
                <?php if (in_array('/dailyWorkProgress/inspection', Yii::app()->session['menuauthlist'])) { ?>
                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Test/Inspection">Test / Inspection </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[inspection]">
                  </div>
                <?php } ?>
                <?php if (in_array('/dailyWorkProgress/siteVisit', Yii::app()->session['menuauthlist'])) { ?>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Visitor">Visitor Name </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[visitor]">
                  </div>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Company">Company </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[company]">
                  </div>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Designation">Designation </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[designation]">
                  </div>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Designation">Purpose </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[purpose]">
                  </div>
                <?php } ?>
              </div>


              <!-- end projem section -->


              <!-- item consumed start -->
              <?php
              // if (Tasks::model()->accountPermission() == 1) {
              if (Tasks::model()->accountPermission() == 1 && (in_array('/dailyWorkProgress/itemConsumption', Yii::app()->session['menuauthlist']))) {
              ?>
                <div class="row margin-top-25">
                  <hr size="30">
                  <input type="hidden" id="item_count_status" name="DailyWorkProgress[item_count_status]">


                  <div class=" col-md-2 col-sm-6 ">
                    <?php echo $form->labelEx($model, 'Item Consumed'); ?>

                    <select class="form-control" id="consumed_item_id" name="DailyWorkProgress[consumed_item_id][]">
                      <option></option>

                    </select>

                    <?php echo $form->error($model, 'consumed_item_id'); ?>
                  </div>



                  <div class=" col-md-2 col-sm-6 ">

                    <label>Rate</label>
                    <select class="form-control" id="consumed_item_rate" name="DailyWorkProgress[consumed_item_rate][]">
                      <option></option>

                    </select>


                  </div>



                  <div class=" col-md-2 col-sm-6 ">



                    <?php echo $form->labelEx($model, 'Item Consumed'); ?>
                    <?php echo $form->textField($model, 'consumed_item_count[]', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                    <?php echo $form->error($model, 'consumed_item_count'); ?>


                  </div>
                </div>


                <!-- add more -->
                <div id="container"></div>
                <br>
                <input type="button" id="add_new" value="Add" onClick="addInput();" class="margin-left-14">
                <br>
                <!-- end add more -->

              <?php
              }
              ?>


              <!-- end item consumed -->

              <!-- Resources utilized start -->
              <?php
              if (Tasks::model()->accountPermission() == 1) {
              ?>
                <div class="row margin-top-25">
                  <hr size="30">


                  <div class=" col-md-2 col-sm-6 ">

                    <label>Resource utilised</label>
                    <select class="form-control" id="resources_used" name="DailyWorkProgress[resources_used][]">
                      <option value="">Choose a resource</option>
                      <?php
                      foreach ($resource_utilised as $resources) {
                      ?>
                        <option value="<?php echo $resources->id ?>"><?php echo $resources->resource_name ?></option>
                      <?php
                      }
                      ?>

                    </select>


                  </div>




                  <div class=" col-md-2 col-sm-6 ">

                    <label>Quantity</label>
                    <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[utilised_qty][]" type="text">

                  </div>

                  <div class=" col-md-2 col-sm-6 ">



                    <label>UOM</label>
                    <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[uom][]" type="text" id="uom" readonly>





                  </div>

                  <div class=" col-md-2 col-sm-6 ">




                    <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[unit_id][]" type="hidden" id="unit_id" readonly>





                  </div>





                </div>


                <!-- add more -->
                <div id="resources_container"></div>
                <br>
                <input type="button" id="add_new" value="Add" onClick="addResources();" class="margin-left-14">
                <br>

              <?php
              }
              ?>
              <!-- end add more -->
              <!-- Resources utilized end -->




              <div class="buttons col-md-2 col-sm-12 form-group">
                <?php echo CHtml::Button($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'save_btn btn blue', 'id' => 'submit_boq')); ?>
              </div>
            </div>
            <?php $this->endWidget(); ?>
          </div><!-- form -->
        </div>
      </div>
    <?php
    }
    ?>