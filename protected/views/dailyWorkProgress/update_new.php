<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<?php
/* @var $this DailyWorkProgressController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Daily Work Progresses',
);

$settings = GeneralSettings::model()->find(['condition' => 'id = 1']);
?>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>


<div class="update-new-dailywork-progress">
<div class="clearfix daily-work-header">
    <h1 class="pull-left">Daily Work Progresses</h1>

    <?php

    if (Yii::app()->user->role != 9) {
    ?>

        <div class="pull-right"><button class="btn btn-primary btn-sm addworkprogressbtn">Add Daily Work Progress</button></div>
    <?php  } ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/themes/btheme2/assets/admin/layout3/scripts/autocomplete.js', CClientScript::POS_END); ?>

</div>




<?php

Yii::app()->clientScript->registerScript('myjquery', " 
		
$(document).ready(function () {



    $('.addworkprogressbtn').click(function(){
        $('.work_progress_form').slideDown();
    });

    $('.close_work_progress').click(function(){
        $('#daily-work-progress-form').reset();
        $('#task_id_span1').html('');
        $('red').text('');
        
        $('.work_progress_form').slideUp();
    });
      
    $('.info').fadeOut('3000');

	  $( '.img_comp_class').keyup(function() {

	 // $(function(){
	  var projectid = $('#DailyWorkProgress_project_id').val();
	  //alert(projectid);
	  if(projectid == '')
	  {
		//alert('Please Select Project');
		//$('#DailyWorkProgress_project_id_em_').css({display:block});
	  
	  }else
	  {
	//   $.ajax({
	// 		type: 'GET',
	// 		dataType: 'text',
	// 		url:'" . Yii::app()->createUrl("DailyWorkProgress/getitems") . "',
	// 		data:{projectid:projectid},
	// 		success:function(data){
	// 		//alert(data);
	// 		console.log(data); 
	// 		var arrResponse = JSON.parse(data); 
	// 		test(arrResponse);    
					
	// 				 },		
	// 	   });
		  

		//});
	   function test(data){
			var items = data;
			console.log(items);        
			console.log(typeof items); 
			
		  $('.img_comp_class').autocomplete({
		  source: items,   					
		  select: function(event,ui) {		 
		  $('#autocomplete_hid').val(ui.item.label);
		  $('#autocomplete_hid_dataval').val($(this).attr('data-val'));
		  }
		  });
		}
	 }
	});
	
		$('#DailyWorkProgress_item_id').change(function () {
			var details = $('#DailyWorkProgress_item_id').val();
			
			var arr = details.split(',');
			var fst = arr.splice(0,1).join('');
			var rest = arr.join(',');
			$('#DailyWorkProgress_item_id').val(fst);
			$('.details_item').html(rest);
			$('#newdetails').val(rest);
			//alert(fst);
		});
	
	
  });
     

 ");
?>


<!-- Add tools Popup -->
<div id="addproject" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>



<?php

if (Yii::app()->user->role != 9) {
?>

    <div class="panel panel-default work_progress_form">
        <div class="panel-heading clearfix">
            <a class="close_work_progress pull-right"><b>X</b></a>
            <h3>Add Work Progress</h3>
        </div>
        <div class="">
            <div class="form">

                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'daily-work-progress-form',
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    'action' => Yii::app()->createUrl('/dailyWorkProgress/update&id=' . $id),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        // 'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => true,
                    ),
                )); ?>


                <div class="row">
                    <input type="hidden" name="newdetails1" id="newdetails1">
                    <div class="subrow col-md-2 col-sm-6 form-group">
                        <?php echo $form->labelEx($model, 'project_id'); ?>

                        <?php

                        if (Yii::app()->user->role == 1) {
                            $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
                        } else {
                            $criteria = new CDbCriteria;
                            $criteria->select = 'project_id';
                            $criteria->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                            $criteria->group = 'project_id';
                            $project_ids = Tasks::model()->findAll($criteria);
                            $project_id_array = array();
                            foreach ($project_ids as $projid) {
                                array_push($project_id_array, $projid['project_id']);
                            }
                            if (!empty($project_id_array)) {
                                $project_id_array = implode(',', $project_id_array);
                                $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
                            } else {
                                $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'order' => 'name ASC');
                            }
                        }
                        if (empty($model->project_id)) {
                            $project_id =   Yii::app()->user->project_id;
                            $model->project_id = Yii::app()->user->project_id;
                        }


                        $project = Projects::model()->findAll($condition);
                        $data_array = array();
                        if (count($project) == 1) {
                            foreach ($project as $key => $value) {
                                array_push($data_array, $value->pid);
                            }
                            $id = implode(" ", $data_array);
                            $p_data =  Projects::model()->findByPk($id);
                        ?>
                            <select class="form-control" name="DailyWorkProgress[project_id]" id="DailyWorkProgress_project_id">
                                <option value="<?php echo $p_data['pid']; ?>" <?php echo (isset($project_id) == $p_data['pid']) ? 'selected' : ""; ?>><?php echo $p_data['name']; ?></option>
                            </select>
                        <?php
                        } else {
                            echo $form->dropDownList(
                                $model,
                                'project_id',
                                CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'),
                                array('empty' => '-Choose a Project-', 'class' => 'form-control')
                            );
                        }
                        ?>
                        <?php echo $form->error($model, 'project_id'); ?>
                    </div>

                    <div class="subrow col-md-2 col-sm-6 form-group">
                        <?php
                        // if($model->date ==''){
                        //     $model->date = date('d-M-y');
                        // }
                        ?>
                        <?php echo $form->labelEx($model, 'date'); ?>
                        <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php
                        // $this->widget('application.extensions.calendar.SCalendar', array( 
                        // 	'inputField' => 'DailyWorkProgress_date',
                        // 	'ifFormat' => '%d-%b-%Y',
                        // ));
                        ?>
                        <?php echo $form->error($model, 'date'); ?>
                        <div class="date_section" id="test_sample"></div>
                    </div>
                    <div class="subrow col-md-2 col-sm-6 form-group">


                        <?php echo $form->labelEx($model, 'taskid'); ?>

                        <?php
                        $tasks = array();
                        $work_type = "";
                        $tsk_id = "";

                        if (Yii::app()->user->project_id != "") {

                            if (Yii::app()->user->role != 1) {
                                $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND project_id=' . Yii::app()->user->project_id . ' AND (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')'));
                            } else {
                                $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND project_id=' . Yii::app()->user->project_id));
                            }
                        } else {
                            if (Yii::app()->user->role != 1) {
                                $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1  AND (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')'));
                            } else {
                                $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 '));
                            }
                        }
                        $data_array = array();
                        $tsk_id = "";
                        $work_type = "";
                        if (count($tasks) == 1) {
                            foreach ($tasks as $key => $value) {
                                array_push($data_array, $value->tskid);
                            }
                            $tsk_id = implode(" ", $data_array);
                            $task = Tasks::model()->findByPk($tsk_id);
                            $work_type = $task->work_type_id;
                            $tsk_id = $tsk_id;
                            $quantity = $task->quantity;
                            if ($task->unit != 0) {
                                $units_data = Unit::model()->findByPk($task->unit);
                                $unit = $units_data->unit_code;
                            }
                        }
                        if (!empty($model->taskid)) {
                            $tsk_id = $model->taskid;
                        }

                        ?>
                        <span id="task_id_span1"><?php echo $tsk_id; ?></span>
                        <select class="form-control taskid" name="DailyWorkProgress[taskid]" id="DailyWorkProgress_taskid">
                            <option value="">-Choose a Task-</option>
                            <?php
                            if (!empty($tasks)) {
                                foreach ($tasks as $key => $value) {
                                    if($value['parent_tskid']!=null)
                                    {
                                      $name= "- - ".$value['title'];
                                    }
                                    else
                                    {
                                      $name= "- ".$value['title'];
                                    }
                            ?>
                                    <option value="<?php echo $value['tskid']; ?>" <?php echo ($value['tskid'] == $tsk_id) ? "selected" : ""; ?>><?php echo $name; ?></option>
                            <?php }
                            } ?>
                        </select>

                        <span class="details_item"><?php echo ((isset($itemdetails)) ? $itemdetails : ''); ?></span>
                        <?php echo $form->error($model, 'taskid'); ?>
                    </div>
                    <!-- <div class="col-md-2 col-sm-6 form-group">
            <span class="img_span"> (WidthxHeight)(2048x1536)</span>
                <?php
                $this->widget('CMultiFileUpload', array(
                    'model' => $model,
                    'name' => 'image',
                    'attribute' => 'image',
                    'accept' => 'jpg|gif|png|jpeg',
                    'max' => 3,
                    'duplicate' => 'Already Selected',
                ));
                ?>
        </div> -->
                    <div class="col-md-2 col-sm-3 form-group">
                        <?php echo $form->labelEx($model, 'qty'); ?>
                        <?php echo $form->textField($model, 'qty', array(
                            'class' => 'form-control img_comp_class', 'autocomplete' => 'off',
                            'name' => 'DailyWorkProgress[qty]',
                            'ajax' => array(
                                'url' => array('DailyWorkProgress/getprogress'),
                                'type' => 'POST',
                                'data' => array(
                                    'qty' => 'js:this.value',
                                    'item_total_qty' => 'js: $("#item_total_qty").val()',
                                    'item_id' => 'js: $("#item_main_id").val()',

                                ),
                                'dataType' => 'json',
                                'success' => 'function(data){
							if(data.progress==0){
								$("#DailyWorkProgress_qty").val("0");
                                $("#DailyWorkProgress_daily_work_progress").val("0");                                
                                $("#DailyWorkProgress_qty_em_").css("display", "");
                                $("#DailyWorkProgress_qty_em_").text(data.msg);
                                setTimeout(function(){// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                               }, 500);
							}else{                               
								$("#DailyWorkProgress_daily_work_progress").val(data.progress);
                            }
                            if(data.balance_qty !== ""){
                               
                                $("#balance_qty").text("Balance quantity: "+data.balance_qty);
                             }					
							
						}',

                            )

                        )); ?>
                        <?php echo $form->error($model, 'qty'); ?>
                    </div>

                    <div class="col-md-2 col-sm-3 pos-rel form-group">
                        <?php echo $form->labelEx($model, 'total_quantity'); ?>
                        <?php echo $form->textField($model, 'total_quantity', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'value' => $quantity)); ?>
                        <span id="unit_value"><?php echo $unit; ?></span>
                        <span id="balance_qty">Balance Quantity: <?php echo $bal_qty; ?></span>
                        <?php echo $form->error($model, 'total_quantity'); ?>
                    </div>



                </div>
                <div class="row">
                    <div class="subrow col-md-2 col-sm-6 form-group">
                        <?php echo $form->labelEx($model, 'work_type'); ?>
                        <?php
                        $condition = array('select' =>  array('wtid,work_type'), 'order' => 'work_type ASC');
                        echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll($condition), 'wtid', 'work_type'), array('options' => array($work_type => array('selected' => true)), 'empty' => '-Choose a Work Type-', 'class' => 'form-control'));
                        ?>
                        <?php echo $form->error($model, 'work_type'); ?>
                    </div>
                    <div class="subrow col-md-2 col-sm-6 form-group">
                        <?php echo $form->labelEx($model, 'current_status'); ?>
                        <?php
                        $condition_status = array('select' =>  array('sid,caption'), 'condition' => "status_type='task_status' AND sid NOT IN(6,7)", 'order' => 'caption ASC');
                        echo $form->dropDownList($model, 'current_status', CHtml::listData(Status::model()->findAll($condition_status), 'sid', 'caption'), array('empty' => '-Choose status-', 'class' => 'form-control'));
                        ?>
                        <?php echo $form->error($model, 'current_status'); ?>

                    </div>
                    <div class="col-md-2 col-sm-6 form-group">
                        <?php echo $form->labelEx($model, 'daily_work_progress'); ?>
                        <?php echo $form->textField($model, 'daily_work_progress', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true)); ?>
                        <?php echo $form->error($model, 'daily_work_progress'); ?>
                    </div>
                    <div class="col-md-2 col-sm-6 form-group">
                        <?php echo $form->labelEx($model, 'description'); ?>
                        <?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </div>
                    <div class="col-md-2 col-sm-6 form-group">
                        <span class="img_span"> (WidthxHeight)(2048x1536)</span>
                        <?php
                        $this->widget('CMultiFileUpload', array(
                            'model' => $model,
                            'name' => 'image',
                            'attribute' => 'image',
                            'accept' => 'jpg|gif|png|jpeg',
                            'max' => 3,
                            'duplicate' => 'Already Selected',
                        ));
                        ?>
                    </div>
                    <div class="col-md-2 col-sm-6 form-group">
                        <?php echo $form->labelEx($model, 'image label'); ?>
                        <?php echo $form->textField($model, 'image_label', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'image_label'); ?>
                    </div>
                    <!-- <div class="subrow col-md-2 col-sm-6 form-group">

        <?php echo $form->labelEx($model, 'task_id'); ?>
        <span id="task_id_span1"></span>
    </div> -->

                        <!--   Projem section -->

                        <div class="row margin-top-120">
                            <?php if (in_array('/dailyWorkProgress/incident', Yii::app()->session['menuauthlist'])) { ?>
                                <div class="subrow col-md-2 col-sm-6 form-group">
                                    <label>Incidents</label>
                                    <select class="form-control" name="DailyWorkProgress[incident]" id="incident_id">
                                        <option value="">Choose Incident</option>

                                        <option value="1" <?= $model->incident == '1' ? ' selected="selected"' : ''; ?>>Accidents</option>
                                        <option value="2" <?= $model->incident == '2' ? ' selected="selected"' : ''; ?>>Injuries</option>
                                        <option value="3" <?= $model->incident == '3' ? ' selected="selected"' : ''; ?>>Unusual Events </option>
                                    </select>
                                </div>
                            <?php } ?>
                            <?php if (in_array('/dailyWorkProgress/inspection', Yii::app()->session['menuauthlist'])) { ?>
                                <div class="subrow col-md-2 col-sm-6 form-group">

                                    <?php echo $form->labelEx($model, 'inspection'); ?>
                                    <?php echo $form->textField($model, 'inspection', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                    <?php echo $form->error($model, 'inspection'); ?>
                                </div>
                            <?php } ?>
                            <?php if (in_array('/dailyWorkProgress/siteVisit', Yii::app()->session['menuauthlist'])) { ?>

                                <div class="subrow col-md-2 col-sm-6 form-group">

                                    <?php echo $form->labelEx($model, 'visitor_name'); ?>
                                    <?php echo $form->textField($model, 'visitor_name', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                    <?php echo $form->error($model, 'visitor_name'); ?>
                                </div>

                                <div class="subrow col-md-2 col-sm-6 form-group">

                                    <?php echo $form->labelEx($model, 'company_name'); ?>
                                    <?php echo $form->textField($model, 'company_name', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                    <?php echo $form->error($model, 'company_name'); ?>
                                </div>

                                <div class="subrow col-md-2 col-sm-6 form-group">

                                    <?php echo $form->labelEx($model, 'designation'); ?>
                                    <?php echo $form->textField($model, 'designation', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                    <?php echo $form->error($model, 'designation'); ?>
                                </div>

                                <div class="subrow col-md-2 col-sm-6 form-group">

                                    <?php echo $form->labelEx($model, 'purpose'); ?>
                                    <?php echo $form->textField($model, 'purpose', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                    <?php echo $form->error($model, 'purpose'); ?>
                                </div>
                            <?php } ?>
                        </div>


                        <!-- end projem section -->

                    <div class="">

                        <?php // echo $form->labelEx($model,'item_description'); 
                        ?>
                        <input id="item_description" type="hidden" value="" name="item_description">
                        <input id="item_total_qty" value="<?= $quantity ?>" type="hidden" name="item_total_qty"> <input id="item_main_id" value="<?= $tsk_id ?>" type="hidden" name="item_main_id">
                    </div>

                    <?php
                    // if(Tasks::model()->accountPermission()==1)
                    if(Tasks::model()->accountPermission()==1 && (in_array('/dailyWorkProgress/itemConsumption', Yii::app()->session['menuauthlist'])))
                    {
                    ?>

                    <div class="row margin-top-120">
                        <div class=" col-md-2 col-sm-6 ">
                            <?php echo $form->labelEx($model, 'Item Consumed'); ?>

                            <select class="form-control" id="consumed_item_id" name="DailyWorkProgress[consumed_item_id]">
                                
                            </select>

                            <?php echo $form->error($model, 'consumed_item_id'); ?>
                        </div>

                        <div class=" col-md-2 col-sm-6 ">
                           
<label>Rate</label>
                            <select class="form-control" id="consumed_item_rate" name="DailyWorkProgress[consumed_item_rate]">
                                <option></option>
                                
                            </select>

                            
                        </div>
                        <div class=" col-md-2 col-sm-6 ">

                            <input type="hidden" id="item_count_status" name="DailyWorkProgress[item_count_status]">

                            <?php echo $form->labelEx($model, 'Item Consumed'); ?>
                            <?php echo $form->textField($model, 'consumed_item_count', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                            <?php echo $form->error($model, 'consumed_item_count'); ?>

                        </div>
                    </div>

<?php } ?>


                    <div class="buttons col-md-2 col-sm-12 form-group">
                        <!-- <label>&nbsp;</label> -->
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'save_btn btn blue', 'id' => 'submit_boq', 'onClick' => 'this.disabled=true;this.form.submit();')); ?>
                    </div>

                </div>

                <?php $this->endWidget(); ?>

            </div><!-- form -->
        </div>
    </div>



<?php
}
?>

<!-- <div class="table-responsive"> -->
<?php
if (!empty($_GET['interval'])) {
    $date = $_GET['interval'];
} else {
    $date = date('m/d/Y');
}
?>







<div class="alert <?= Yii::app()->user->hasFlash('error') ? "" : "hide" ?>">
    <?php

    if (Yii::app()->user->hasFlash('error')) {
        echo Yii::app()->user->getFlash('error');
    }
    ?>
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>

</div>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Reject BOQ Entry',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0" class="min-height-200"></iframe>
<?php
$this->endWidget();
?>
</div>
<script>
    $('#daily-work-progress-form').on('beforeSubmit', function(e) {
        $('#submit_boq').css('disable', 'disable');

        return false;

    });


    $(function() {
        $("#datepicker").datepicker({

            onSelect: function(selectedDate) {
                var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/index&interval='); ?>' + selectedDate;
                $(location).attr('href', url);
            }
        });
    });
</script>
<script>
    $(document).ready(function() {

        var date1 = '<?php echo $settings->backlog_days; ?>';

        $('#DailyWorkProgress_date').datepicker({
            dateFormat: 'dd-M-y',
            minDate: -date1,
            onSelect: function(selectedDate) {
                var d = new Date(selectedDate);
                d.setDate(d.getDate() - date1);
                $('#DailyWorkProgress_date').datepicker('option', 'minDate', d);
                var date = this.value;
                entrydatevalidation(date);
            }
        });
        $(document).on('click', ".approve", function() {
            if (!confirm("Do you want to Approve ?")) {} else {
                $('.loaderdiv').show();
                var entry_id = this.id;
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approveentry'); ?>',
                    data: {
                        entry_id: entry_id
                    },
                    method: "GET",
                    dataType: 'json',
                    success: function(result) {
                        if (result.status == '1') {
                            $("#boq_entries").load(location.href + " #boq_entries>*", "");
                            $('.alert').removeClass('hide');
                            $('.alert').css('display', 'block');
                            // $('<p>' + result.schedule + '</p>').appendTo('.alert');
                            // $('<p>' + result.dependant + '</p>').appendTo('.alert');
                            $('.alert').append(result.schedule);
                            $('.alert').append("<br/>");
                            $('.alert').append(result.dependant);
                            $('.loaderdiv').hide();
                        } else {
                            $('.alert').removeClass('hide');
                            $('.alert').css('display', 'block');
                            $('.alert').append(result.message);
                            $('.loaderdiv').hide();
                        }

                    }
                })
            }


        });
       
        $(document).on('click', ".recall", function() {
            if (!confirm("Do you want to Recall ?")) {} else {
                $('.loaderdiv').show();
                var entry_id = this.id;
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/recall'); ?>',
                    data: {
                        entry_id: entry_id
                    },
                    method: "GET",
                    dataType: 'json',
                    success: function(result) {
                        if (result.status == '1') {
                            $("#boq_entries").load(location.href + " #boq_entries>*", "");
                            // $('.alert').removeClass('hide');                 
                            // $('.alert').append(result.message);                   
                            $('.loaderdiv').hide();
                        } else if (result.status == '2') {
                            $('.alert').removeClass('hide');
                            $('.alert').append(result.message);
                            $('.loaderdiv').hide();
                        }

                    }
                })
            }


        });

        function entrydatevalidation(date) {
            var project_id = $('#DailyWorkProgress_project_id').find(":selected").val();
            if (project_id !== '') {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/checkdate'); ?>',
                    data: {
                        date: date,
                        project_id: project_id
                    },
                    method: "POST",
                    dataType: 'json',
                    success: function(result) {
                        //    console.log(result);
                        $('#DailyWorkProgress_taskid').html(result.html);
                        $('#DailyWorkProgress_taskid').val(result.tsk_id);
                        $('#DailyWorkProgress_work_type').html(result.work_type_html);
                        $('#DailyWorkProgress_work_type').val(result.work_type);
                        $('#task_id_span1').text(result.tsk_id);
                        $('#DailyWorkProgress_total_quantity').val(result.quantity);
                        $("#unit_value").html(result.unit);
                        $("#item_total_qty").val(result.quantity);
                        $("#item_main_id").val(result.tsk_id);
                    }
                });
            }

        }
    });

    $("#DailyWorkProgress_project_id").change(function() {
        var val = $(this).val();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getAllTasks'); ?>',
            data: {
                project_id: val
            },
            method: "GET",
            success: function(result) {
                $('.taskid').html(result);
            }
        })
    })

    $("#DailyWorkProgress_taskid").change(function() {
        var val = $(this).val();
        if (val != '') {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/gettaskdetails'); ?>',
                data: {
                    taskid: val
                },
                method: "GET",
                dataType: "json",
                success: function(result) {
                    $("#task_id_span1").html("Task Id: <b>" + result.taskid + "</b>");
                    $("#DailyWorkProgress_total_quantity").val(result.total_qty);
                    $("#item_total_qty").val(result.total_qty);
                    $("#item_main_id").val(result.taskid);
                    $("#DailyWorkProgress_work_type").html(result.option);
                    $("#DailyWorkProgress_work_type").val(result.work_type);
                    $('#unit_value').html(result.unit)
                }
            })
        } else {
            $("#task_id_span1").html("");
            $("#DailyWorkProgress_total_quantity").html("");
            $("#item_total_qty").val("");
            $("#item_main_id").val("");
        }
    })
    $('#consumed_item_id').change(function() {
        var itm_id = $('#consumed_item_id').val();
        var project_id=$("#DailyWorkProgress_project_id").val();
        
        $.ajax({
            "type": "POST",

            "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemAvailability'); ?>",
            "dataType": "json",
            "data": {
                itm_id: itm_id,
                project_id:project_id
            },
            "success": function(data) {
                // if (data.status != 1) {
                //     $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
                //     $("#DailyWorkProgress_consumed_item_id_em_").text('This item is not mentioned in template');
                //     $('#consumed_item_id').val("");
                // } else {
                  
                //     $("#DailyWorkProgress_consumed_item_id_em_").hide();
                // }

                if(data.status == 1)
          {
            $("#DailyWorkProgress_consumed_item_id_em_").hide();
          }
          else if(data.status == 0)
          {
                $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
           $("#DailyWorkProgress_consumed_item_id_em_").text('This item is not mentioned in template');
           $('#consumed_item_id').val("");
          }
          else
          {
            $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
           $("#DailyWorkProgress_consumed_item_id_em_").text('No template is defined for this project');
           $('#consumed_item_id').val("");
          }

            }
        });
    })
    $("#DailyWorkProgress_consumed_item_count").change(function() {

var itm_id = $('#consumed_item_id').val();
var task_id = $('#DailyWorkProgress_taskid').val();
var item_count = $('#DailyWorkProgress_consumed_item_count').val();
var already_exist_count="<?php echo $model->consumed_item_count; ?>";
var item_rate=$('#consumed_item_rate').val();



$.ajax({
    "type": "POST",

    "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemCountAvailability'); ?>",
    "dataType": "json",
    "data": {
        itm_id: itm_id,
        task_id: task_id,
        item_count: item_count,
        already_exist_count:already_exist_count,
        item_rate:item_rate
    },
    "success": function(data) {

        if (data.status != 0 && data.status != 1) {

            $("#DailyWorkProgress_consumed_item_count_em_").css("display", "");
            $("#DailyWorkProgress_consumed_item_count_em_").text(data.message + ". Avaiable count is " + data.item_required);
            $("#DailyWorkProgress_consumed_item_count").val('');

        }
        if (data.status == 1) {
            $("#DailyWorkProgress_consumed_item_count_em_").css("display", "");
            $("#DailyWorkProgress_consumed_item_count_em_").text(data.message + ". Avaiable count is " + data.item_required);
            $('#item_count_status').val(1);

        } 
        if(data.status == 0)
                {
                     $("#DailyWorkProgress_consumed_item_count_em_").hide();
                     $('#item_count_status').val("");
                     
                }
        else {
           // $('#item_count_status').val("");
        }

    }
});


})

$(document).ready(function (){
    
   
        var project_id= $('#DailyWorkProgress_project_id').val();
        var consumed_item_id="<?php echo $model->consumed_item_id; ?>";
        $.ajax({
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getWarehouseItems'); ?>',
            data: {
                project_id: project_id,
               
            },
            method: "POST",
            success: function(result) {
                $("#consumed_item_id").html(result.html);
                $("#consumed_item_id").val(consumed_item_id);
            }
        })


        $('#DailyWorkProgress_project_id').change(function() {
           var project_id=$('#DailyWorkProgress_project_id').val();
          
            $.ajax({
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getWarehouseItems'); ?>',
            data: {
                project_id: project_id,
               
            },
            method: "POST",
            success: function(result) {
                $("#consumed_item_id").html(result.html);
               
            }
        })

       
       

        })


        var consumed_item_id="<?php echo $model->consumed_item_id; ?>";
        var price_id=<?= isset($model->consumed_item_price_id) ? $model->consumed_item_price_id : '' ?>

        var project_id="<?php echo $model->project_id ?>";
            $.ajax({
            method: "POST",
            data: {
                consumed_item_id: consumed_item_id,
                project_id:project_id
            },
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
            success: function(data) {
                $("#consumed_item_rate").html(data.html);
                $("#consumed_item_rate").val(price_id);
            }
        });


        $("#consumed_item_id").change(function() {
            var consumed_item_id =  $("#consumed_item_id").val();
            var project_id=$("#DailyWorkProgress_project_id").val();
            $.ajax({
            method: "POST",
            data: {
                consumed_item_id: consumed_item_id,
                project_id:project_id
            },
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
            success: function(data) {
                $("#consumed_item_rate").html(data.html);
            }
        });
        $('#DailyWorkProgress_consumed_item_count').val("")
        });

        $("#consumed_item_rate").change(function()
        {
            $('#DailyWorkProgress_consumed_item_count').val("")
        });
});



</script>