<?php
/* @var $this DailyWorkProgressController */
/* @var $model DailyWorkProgress */

$this->breadcrumbs=array(
	'Daily Work Progresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DailyWorkProgress', 'url'=>array('index')),
	array('label'=>'Create DailyWorkProgress', 'url'=>array('create')),
	array('label'=>'View DailyWorkProgress', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DailyWorkProgress', 'url'=>array('admin')),
);
?>



<?php echo $this->renderPartial('daily_work_progress', 
array('model'=>$model,
'account_items'=>$account_items,
'resource_utilised'=>$resource_utilised,
'interval'=>$interval,
'selected_status'=>$selected_status,
'task_total_quantity'=>$quantity,
'balanceQuantity'=>$balanceQuantity
)); ?>