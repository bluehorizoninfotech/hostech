<?php
$settings = GeneralSettings::model()->find(['condition' => 'id = 1']);
?>
<div class="panel panel-default task_work_progress_form">
  <div class="panel-heading clearfix">
    <a class="close_task_work_progress pull-right"><b>X</b></a>
    <h3>Add Work Progress</h3>
  </div>
  <div class="panel-body">
    <p>Site:<span id="site_text"></span></p>
    <div class="form">
      <?php
      $form = $this->beginWidget(
        'CActiveForm',
        array(
          'id' => 'task-daily-work-progress-form',
          'htmlOptions' => array('enctype' => 'multipart/form-data'),
          'action' => Yii::app()->createUrl('/dailyWorkProgress/create_daily_wpr'),
          'enableAjaxValidation' => true,
          'enableClientValidation' => true,
          'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnType' => true,
            'validateOnSubmit' => true,
          ),
        )
      );

      ?>
      <div class="row">
        <input type="hidden" name="newdetails" id="newdetails">
        <input type="hidden" name="DailyWorkProgress[mytask_wpr]" value="1">
        <div class="subrow col-md-2 col-sm-6 form-group">
          <label>Project</label>
          <select class="form-control" readonly name="DailyWorkProgress[project_id]" id="project_id">
            <option value="<?= $project->pid ?>"><?= $project->name ?></option>
          </select>

        </div>
        <div class="subrow col-md-2 col-sm-6 form-group">
          <label for="Clients_name" class="required">Date <span class="required">*</span></label>
          <input type="text" id="datepicker" class="form-control" name="DailyWorkProgress[date]">
          <span id="date_error" class="red-color font-weight-bold"></span>
          <span id="date_validation" class="red-color font-weight-bold"></span>
        </div>
        <div class="subrow col-md-2 col-sm-6 form-group">

          <label>Task</label>
          <select class="form-control" readonly name="DailyWorkProgress[taskid]" id="tskid">
            <option value="<?= $task->tskid ?>"><?= $task->title ?></option>
          </select>

        </div>
        <div class="col-md-2 col-sm-3 form-group">
          <label for="Clients_name" class="required">Quantity <span class="required">*</span></label>
          <?php echo $form->textField($model, 'qty', array(
            'class' => 'form-control img_comp_class daily-wpr-qty', 'autocomplete' => 'off',
            'name' => 'DailyWorkProgress[qty]',

          )); ?>
          <span id="balance_qty_msg"></span>
          <span id="quantity_message" class="red-color font-weight-bold"></span>
          <span id="quantity_validation" class="red-color font-weight-bold"></span>
          <input type="hidden" value="<?= $task->quantity ?>" id="wpr_task_quantity">
          <input type="hidden" value="<?= $task->tskid ?>" id="wpr_task_id">

          <input type="hidden" id="bal_quantity_id">

          <?php echo $form->error($model, 'qty'); ?>
        </div>


        <div class="col-md-2 col-sm-3 pos-rel form-group">
          <?php
          $quantity = $task->quantity;

          ?>
          <?php echo $form->labelEx($model, 'total_quantity'); ?>
          <?php echo $form->textField($model, 'total_quantity', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'value' => $task->quantity)); ?>
          <span id="unit_value"><?php echo isset($unit->unit_cod) ? $unit->unit_cod : "" ?></span>


        </div>
      </div>
      <div class="row">
        <div class="subrow col-md-2 col-sm-6 form-group">
          <label>Work Type</label>
          <select class="form-control" readonly name="DailyWorkProgress[work_type]">
            <option value="<?= $work_type->wtid ?>"><?= $work_type->work_type ?></option>
          </select>
        </div>
        <div class="subrow col-md-2 col-sm-6 form-group">
          <?php echo $form->labelEx($model, 'current_status'); ?>
          <?php
          $condition_status = array('select' =>  array('sid,caption'), 'condition' => "status_type='task_status' AND sid NOT IN(6)", 'order' => 'caption ASC');
          echo $form->dropDownList($model, 'current_status', CHtml::listData(Status::model()->findAll($condition_status), 'sid', 'caption'), array('empty' => '-Choose status-', 'class' => 'form-control wpr-status', 'id' => 'current_status'));
          ?>
          <div class="errorMessage" id="DailyWorkProgress_current_status_error_message_" style="display:none"></div>

        </div>
        <div class="col-md-2 col-sm-6 form-group">
          <?php echo $form->labelEx($model, 'daily_work_progress'); ?>
          <?php echo $form->textField($model, 'daily_work_progress', array('class' => 'form-control progress', 'autocomplete' => 'off', 'readonly' => true)); ?>
          <?php echo $form->error($model, 'daily_work_progress'); ?>
        </div>
        <div class="col-md-2 col-sm-6 form-group">
          <?php echo $form->labelEx($model, 'remarks'); ?>
          <?php echo $form->textArea($model, 'description', array('class' => 'form-control description', 'autocomplete' => 'off')); ?>
          <?php echo $form->error($model, 'description'); ?>
          <span id="description_validation" class="red-color font-weight-bold"></span>


        </div>

        <div class="col-md-2 col-sm-6 form-group">
          <span class="img_span"> (WidthxHeight)(2048x1536)</span>
          <!-- <?php
                $this->widget('CMultiFileUpload', array(
                  'model' => $model,
                  'name' => 'image',
                  'attribute' => 'image',
                  'accept' => 'jpg|gif|png|jpeg',
                  'max' => 3,
                  'duplicate' => 'Already Selected',
                  'htmlOptions' => array('class' => 'MultiFile-applied')
                ));
                ?> -->


          <!-- multi file upload -->


          <input id="files" type="file" name="image[]" multiple="multiple" onchange="checkFiles(this.files)" accept="image/jpg,image/png,image/jpeg">
          <div class="filenames"></div>
          <!-- end multifile -->

        </div>

        <div id="label_container1"></div>


        <!-- <div class="col-md-2 col-sm-6 form-group">
                        <?php echo $form->labelEx($model, 'image label'); ?>
                        <?php echo $form->textField($model, 'image_label', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'image_label'); ?>
                    </div> -->


        <div>
          <?php echo CHtml::hiddenField('item_description', '', array('id' => 'item_description')); ?>

        </div>
        <?php echo $form->hiddenField($model, 'latitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "latitude")); ?>
        <?php echo $form->hiddenField($model, 'longitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "longitude")); ?>
        <?php echo $form->hiddenField($model, 'site_id', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_id")); ?>
        <?php echo $form->hiddenField($model, 'site_name', array('class' => 'form-control site_name', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_name")); ?>

        <!--   Projem section -->

        <div class="row margin-top-120">
          <?php if (in_array('/dailyWorkProgress/incident', Yii::app()->session['menuauthlist'])) { ?>
            <div class="subrow col-md-2 col-sm-6 form-group">
              <label>Incidents</label>
              <select class="form-control" name="DailyWorkProgress[incident]" id="incident_id">
                <option value="">Choose Incident</option>
                <option value="1">Accidents </option>
                <option value="2">Injuries </option>
                <option value="3">Unusual Events </option>
              </select>
            </div>
          <?php } ?>
          <?php if (in_array('/dailyWorkProgress/inspection', Yii::app()->session['menuauthlist'])) { ?>
            <div class="subrow col-md-2 col-sm-6 form-group">
              <label for="Test/Inspection">Test / Inspection </label>
              <input type="text" class="form-control" name="DailyWorkProgress[inspection]">
            </div>
          <?php } ?>
          <?php if (in_array('/dailyWorkProgress/siteVisit', Yii::app()->session['menuauthlist'])) { ?>

            <div class="subrow col-md-2 col-sm-6 form-group">
              <label for="Visitor">Visitor Name </label>
              <input type="text" class="form-control" name="DailyWorkProgress[visitor]">
            </div>

            <div class="subrow col-md-2 col-sm-6 form-group">
              <label for="Company">Company </label>
              <input type="text" class="form-control" name="DailyWorkProgress[company]">
            </div>

            <div class="subrow col-md-2 col-sm-6 form-group">
              <label for="Designation">Designation </label>
              <input type="text" class="form-control" name="DailyWorkProgress[designation]">
            </div>

            <div class="subrow col-md-2 col-sm-6 form-group">
              <label for="Designation">Purpose </label>
              <input type="text" class="form-control" name="DailyWorkProgress[purpose]">
            </div>
          <?php } ?>
        </div>


        <!-- end projem section -->


        <?php
        // if(Tasks::model()->accountPermission()==1)
        if (Tasks::model()->accountPermission() == 1 && (in_array('/dailyWorkProgress/itemConsumption', Yii::app()->session['menuauthlist']))) {
        ?>

          <div class="row margin-top-120">
            <hr size="30">
            <input type="hidden" id="item_count_status" name="DailyWorkProgress[item_count_status]">
            <div class=" col-md-2 col-sm-6 ">
              <?php echo $form->labelEx($model, 'Item Consumed'); ?>

              <select class="form-control consumed-item" id="consumed-item-id" name="DailyWorkProgress[consumed_item_id][]">
                <option></option>

              </select>
              <div class="errorMessage" id="consumed_item_id_em_"></div>

            </div>



            <div class=" col-md-2 col-sm-6 ">

              <label>Rate</label>
              <select class="form-control" id="consumed-item-rate" name="DailyWorkProgress[consumed_item_rate][]">
                <option></option>

              </select>


            </div>



            <div class=" col-md-2 col-sm-6 ">



              <?php echo $form->labelEx($model, 'Item Consumed'); ?>
              <?php echo $form->textField($model, 'consumed_item_count[]', array('class' => 'form-control', 'autocomplete' => 'off', 'id' => 'item-count')); ?>

              <div class="errorMessage display-none" id="consumed_item_count_em_"></div>
            </div>
          </div>

          <!-- add more -->
          <div id="add_input_container"></div>
          <br>
          <input type="button" id="add_new" value="Add" onClick="addInput();">
          <br>
          <!-- end add more -->


        <?php
        }
        ?>
        <!-- Resources utilized start -->
        <?php
        if (Tasks::model()->accountPermission() == 1) {
        ?>

          <div class="row margin-top-25">
            <hr size="30">
            <div class=" col-md-2 col-sm-6 ">

              <label>Resource utilised</label>
              <select class="form-control resources_used" id="resources_used" name="DailyWorkProgress[resources_used][]">
                <option value="">Choose a resource</option>
                <?php
                foreach ($resource_utilised as $resources) {
                ?>
                  <option value="<?php echo $resources->id ?>"><?php echo $resources->resource_name ?></option>
                <?php
                }
                ?>

              </select>


            </div>

            <div class=" col-md-2 col-sm-6 ">

              <label>Quantity</label>
              <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[utilised_qty][]" type="text">

            </div>

            <div class=" col-md-2 col-sm-6 ">



              <label>UOM</label>
              <input class="form-control img_comp_class uom" autocomplete="off" name="DailyWorkProgress[uom][]" type="text" id="uom" readonly>





            </div>

            <div class=" col-md-2 col-sm-6 ">




              <input class="form-control img_comp_class unit_id" autocomplete="off" name="DailyWorkProgress[unit_id][]" type="hidden" id="unit_id" readonly>





            </div>
          </div>

          <!-- add more -->
          <div id="resources_container_div"></div>
          <br>
          <input type="button" id="add_new" value="Add" onClick="addResources();" class="margin-left-14">
          <br>
          <!-- end add more -->

        <?php
        }
        ?>
        <!-- Resources utilized end -->
        <div class="buttons col-md-2 col-sm-12 form-group">
          <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'save_btn btn blue', 'id' => 'submit_daily_progress',)); ?>
        </div>
      </div>
      <?php $this->endWidget(); ?>
    </div><!-- form -->
  </div>
</div>
<script>
  //  $(function() {
  // $( "#datepicker" ).datepicker({  maxDate: new Date() });
  // });


  $('.daily-wpr-qty').change(function() {

    var qty = this.value;
    var item_total_qty = $('#wpr_task_quantity').val();
    var item_id = $('#wpr_task_id').val();

    var status = $('#current_status').val();


    $.ajax({
      "type": "POST",

      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/getprogress'); ?>",
      "dataType": "json",
      "data": {
        qty: qty,
        item_total_qty: item_total_qty,
        item_id: item_id


      },

      "success": function(data) {
        if (data.progress == 0) {
          $("#DailyWorkProgress_qty").val("");
          $("#quantity_message").text(data.msg);

        } else {
          $(".progress").val(data.progress);
          $("#quantity_message").hide();
        }
        if (data.balance_qty !== "") {
          $("#balance_qty_msg").text("Balance quantity: " + data.balance_qty);
          $('#bal_quantity_id').val(data.balance_qty);
        }

        balance_quantity = data.balance_qty;
        if (status != "") {
          if (balance_quantity == 0 && status != 7) {
            $("#current_status").val("");
            $("#DailyWorkProgress_current_status_error_message_").css("display", "");
            $("#DailyWorkProgress_current_status_error_message_").text("If the balance quantity is 0 then the status should be 'completed'");
          }
          if(balance_quantity!=0 && status==7)
           {
           
            $("#current_status").val("");
          $("#DailyWorkProgress_current_status_error_message_").css("display", "");
          $("#DailyWorkProgress_current_status_error_message_").text("The balance quantity should be 0 if the status is to be 'completed'");
           }
        }



      }



    });
    var status = $('#current_status').val();
    var balance_quantity = $('#bal_quantity_id').val();

    if (status != "") {
      if (status == 7) {
        if (balance_quantity != 0) {
          $("#current_status").val("");
          $("#DailyWorkProgress_current_status_error_message_").css("display", "");
          $("#DailyWorkProgress_current_status_error_message_").text("The balance quantity should be 0 if the status is to be 'completed'");
        }
      }



    }

  })

  $(document).ready(function() {
    var date1 = '<?php echo $settings->backlog_days; ?>';
    $('#datepicker').datepicker({
      dateFormat: 'dd-M-y',
      minDate: -date1,
      maxDate: 0,
      onSelect: function(selectedDate) {
        var d = new Date(selectedDate);
        var date = this.value;
        wprentrydatevalidation(date);
      }
    });


  });

  function wprentrydatevalidation(date) {
    $(':input[type="submit"]').prop('disabled', false);
    $('#date_validation').hide();
    var wpr_task_id = $('#wpr_task_id').val();

    $.ajax({
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/checkdateavailable'); ?>',
      data: {
        date: date,
        wpr_task_id: wpr_task_id
      },
      method: "POST",
      dataType: 'json',
      success: function(result) {
        if (result.count == 0) {
          $('#datepicker').val("");
          $("#date_error").text("Choose valid date");
        } else {
          $("#date_error").hide();
        }

      }
    });
  }
  // validation

  $('#DailyWorkProgress_qty').change(function() {
    $(':input[type="submit"]').prop('disabled', false);
    $('#quantity_validation').hide();

  });
  $('.description').change(function() {
    $(':input[type="submit"]').prop('disabled', false);
    $('#description_validation').hide();
  });



  $(document).on("click", "#submit_daily_progress", function() {
    var date = $('#datepicker').val();
    var quantity = $('#DailyWorkProgress_qty').val()
    var progress = $('.progress').val();
    var description = $('.description').val();
    var status = $('#DailyWorkProgress_current_status').val();


    if (date == "") {
      $('#date_validation').text("Date cannot be blank");
      $(':input[type="submit"]').prop('disabled', true);
    }
    if (quantity == "") {

      $('#quantity_validation').text("Quantity cannot be blank");
      $(':input[type="submit"]').prop('disabled', true);
    }

    if (description == "") {
      $('#description_validation').text("Description cannot be blank");

      $(':input[type="submit"]').prop('disabled', true);
    }
  });


  $('.close_task_work_progress').click(function() {
    $('#wpr_form_div').hide();
    $.fn.yiiGridView.update("daily-work-progress-task-grid");
  });

  // warehouse items

  $(document).ready(function() {
    var project_id = $('#project_id').val();
    $.ajax({
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getWarehouseItems'); ?>',
      data: {
        project_id: project_id,

      },
      method: "POST",
      success: function(result) {
        $(".consumed-item").html(result.html);
      }
    })

    var task_id = $('#tskid').val();

    $.ajax({
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getBalanceQuantity'); ?>',
      data: {
        task_id: task_id,

      },
      method: "POST",
      success: function(result) {

        $("#balance_qty_msg").text("Balance quantity: " + result.balance_qty);
        $('#bal_quantity_id').val(result.balance_qty);
      }
    })




  });



  $(".consumed-item").change(function() {
    var consumed_item_id = $(".consumed-item").val();
    var project_id = $('#project_id').val();
    $.ajax({
      method: "POST",
      data: {
        consumed_item_id: consumed_item_id,
        project_id: project_id
      },
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
      success: function(data) {
        $("#consumed-item-rate").html(data.html);
      }
    });

    $.ajax({

      "type": "POST",
      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemAvailability'); ?>",
      "dataType": "json",
      "data": {
        itm_id: consumed_item_id,
        project_id: project_id
      },
      "success": function(data) {

        if (data.status == 1) {
          $("#consumed_item_id_em_").hide();
        } else if (data.status == 0) {
          $("#consumed_item_id_em_").css("display", "");
          $("#consumed_item_id_em_").text('This item is not mentioned in template');
          $('#consumed-item-id').val("");
        } else {
          $("#consumed_item_id_em_").css("display", "");
          $("#consumed_item_id_em_").text('No template is defined for this project');
          $('#consumed_item_id').val("");
        }

      }



    });


  });

  // reset count
  $('#consumed-item-rate').change(function() {
    $('#item-count').val("");
  });

  $('#item-count').change(function() {

    var itm_id = $('#consumed-item-id').val();
    var task_id = $('#tskid').val();
    var item_count = $('#item-count').val();
    var item_rate = $('#consumed-item-rate').val();
    $.ajax({
      "type": "POST",

      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemCountAvailability'); ?>",
      "dataType": "json",
      "data": {
        itm_id: itm_id,
        task_id: task_id,
        item_count: item_count,
        item_rate: item_rate
      },
      "success": function(data) {

        if (data.status != 0 && data.status != 1) {

          $("#consumed_item_count_em_").css("display", "");
          $("#consumed_item_count_em_").text(data.message + ". Avaiable count is " + data.item_required);
          $("#item-count").val('');

        }
        if (data.status == 1) {
          $("#consumed_item_count_em_").css("display", "");
          $("#consumed_item_count_em_").text(data.message + ". Required count is " + data.item_required);
          $('#item_count_status').val(1);

        }

        if (data.status == 0) {
          $("#consumed_item_count_em_").hide();
          $('#item_count_status').val("");

        } else {
          //$('#item_count_status').val("");

        }

      }
    });
  });
</script>


<script>
  var count = 0;

  function addInput() {

    var project_id = $('#DailyWorkProgress_project_id').val();


    count += 1;
    $('#add_input_container').append(

      `
<div class="row remove-div_` + count + `">

                                <div class=" col-md-2 col-sm-6 ">
                                    <?php echo $form->labelEx($model, 'Item Consumed'); ?>

                                    <select class="form-control itemid" id="consumed_item_id_` + count + `" name="DailyWorkProgress[consumed_item_id][]">
                                        <option></option>

                                    </select>

                                    <div class="errorMessage" id="DailyWorkProgress_consumed_item_id_em_` + count + `" style="display:none"></div>
                                    <div class="errorMessage display-none"></div>
                                </div>
                                <div class=" col-md-2 col-sm-6 ">

                                    <label>Rate</label>
                                    <select class="form-control" id="consumed_item_rate_` + count + `" name="DailyWorkProgress[consumed_item_rate][]">
                                        <option></option>

                                    </select>


                                </div>



                                <div class=" col-md-2 col-sm-6 ">

                                   

                                   <?php echo $form->labelEx($model, 'Item Consumed'); ?>
                                   
                                    <input type="text" name="DailyWorkProgress[consumed_item_count][]" id="DailyWorkProgress_consumed_item_count_` + count + `" class="form-control" >

                                    <div class="errorMessage" id="DailyWorkProgress_consumed_item_count_em_` + count + `" style="display:none"></div>

                                </div>


                    <!-- remove button -->
                    <div class=" col-md-2 col-sm-6 padding-16">
                   
                    <input type="button" id="` + count + `" value="Remove" class="remove-btn">
                    </div>
                    <!-- end remove button -->

                              
                                </div>
                               
                                
                                
`

    );
    var project_id = $('#project_id').val();
    $.ajax({
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getWarehouseItems'); ?>',
      data: {
        project_id: project_id,

      },
      method: "POST",
      success: function(result) {

        $("#consumed_item_id_" + count).html(result.html);
      }
    })

    // get item rate

    $('#consumed_item_id_' + count).change(function() {

      var consumed_item_id = $("#consumed_item_id_" + count).val();
      var project_id = $('#project_id').val();
      $.ajax({
        method: "POST",
        data: {
          consumed_item_id: consumed_item_id,
          project_id: project_id
        },
        "dataType": "json",
        url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
        success: function(data) {
          $("#consumed_item_rate_" + count).html(data.html);
        }
      });

      // item estimated or not

      $.ajax({
        "type": "POST",

        "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemAvailability'); ?>",
        "dataType": "json",
        "data": {
          itm_id: consumed_item_id,
          project_id: project_id
        },
        "success": function(data) {
          // if (data.status != 1) {
          //   $("#DailyWorkProgress_consumed_item_id_em_" + count).css("display", "");
          //   $("#DailyWorkProgress_consumed_item_id_em_" + count).text('This item is not mentioned in template');
          //   $('#consumed_item_id_' + count).val("");
          // } else {

          //   $("#DailyWorkProgress_consumed_item_id_em_" + count).hide();
          // }
          if (data.status == 1) {
            $("#DailyWorkProgress_consumed_item_id_em_" + count).hide();
          } else if (data.status == 0) {
            $("#DailyWorkProgress_consumed_item_id_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_id_em_" + count).text('This item is not mentioned in template');
            $('#consumed_item_id_' + count).val("");
          } else {
            $("#DailyWorkProgress_consumed_item_id_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_id_em_" + count).text('No template is defined for this project');
            $('#consumed_item_id_' + count).val("");
          }


        }
      });

    });

    // reset count
    $('#consumed_item_rate_' + count).change(function() {
      $('#DailyWorkProgress_consumed_item_count_' + count).val("");
    });


    $("#DailyWorkProgress_consumed_item_count_" + count).change(function() {

      var itm_id = $('#consumed_item_id_' + count).val();
      var task_id = $('#tskid').val();
      var item_count = $('#DailyWorkProgress_consumed_item_count_' + count).val();
      var item_rate = $('#consumed_item_rate_' + count).val();



      $.ajax({
        "type": "POST",

        "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemCountAvailability'); ?>",
        "dataType": "json",
        "data": {
          itm_id: itm_id,
          task_id: task_id,
          item_count: item_count,
          item_rate: item_rate
        },
        "success": function(data) {

          if (data.status != 0 && data.status != 1) {

            $("#DailyWorkProgress_consumed_item_count_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_count_em_" + count).text(data.message + ". Available count is " + data.item_required);
            $("#DailyWorkProgress_consumed_item_count_" + count).val('');

          }

          if (data.status == 1) {
            $("#DailyWorkProgress_consumed_item_count_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_count_em_" + count).text(data.message + ". Avaiable count is " + data.item_required);
            $('#item_count_status').val(1);

          }

          if (data.status == 0) {
            $("#DailyWorkProgress_consumed_item_count_em_" + count).hide();
            $('#item_count_status').val("");

          } else {

            //$('#item_count_status').val("");

          }

        }
      });


    })


    $(document).on('click', '.remove-btn', function() {
      id = this.id;
      $(".remove-div_" + id).remove();
    });




  } //addInput close 


  $(document).on('change', '.itemid', function() {

    var $t = $(this);
    var $column = $(this);
    var value = $('#consumed-item-id').val();
    var val = $t.val();
    var z = 0;

    $(".itemid").each(function() {
      var y = $(this).val();
      if (val == y) {
        z = z + 1;
      }
      if (value == y) {
        z = z + 1;
      }
    });

    if (z > 1) {
      $column.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
      $column.val('');
      return false;
    }




  });


  count = 1;
  $('input:file').change(function() {
    for (var i = 0; i < this.files.length; i++) {
      var fileName = this.files[i].name;
      $('.filenames').append('<div class="name">' + fileName + '</div>');
      $('#label_container1').append(`

<div class="col-md-2 col-sm-6 form-group">
<label for="DailyWorkProgress_image_label">Image Label ` + count + `</label>
<input class="form-control" autocomplete="off" name="DailyWorkProgress[image_label][]" id="DailyWorkProgress_image_label_` + count + `" type="text">
<div class="errorMessage" id="DailyWorkProgress_image_label_em_` + count + `" style="display:none"></div>
                </div>

`);
      count++;
    }


  });

  function checkFiles(files) {
    if (files.length > 3) {
      alert("length exceeded; files have been truncated");

      let list = new DataTransfer;
      for (let i = 0; i < 3; i++)
        list.items.add(files[i])

      document.getElementById('files').files = list.files
    }
  }

  $(".resources_used").change(function() {

    var resources_used = $(".resources_used").val();

    $.ajax({
      method: "POST",
      data: {
        resources_used: resources_used,

      },
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getResourceUnit'); ?>',
      success: function(data) {

        $('.uom').val(data.unit);
        $('.unit_id').val(data.unit_id);
      }
    });
  });

  var resource_count = 0;

  function addResources() {
    resource_count += 1;



    $('#resources_container_div').append(
      `
      <div class="row remove-resource-div_` + resource_count + `">
      <div class=" col-md-2 col-sm-6 ">

<label>Resource utilised</label>
<select class="form-control resources_used" id="resources_used_` + resource_count + `" name="DailyWorkProgress[resources_used][]">
<option value="">Choose a resource</option>
<?php
foreach ($resource_utilised as $resources) {
?>
  <option value="<?php echo $resources->id ?>"><?php echo $resources->resource_name ?></option>
<?php
}
?>

</select>


</div>

<div class=" col-md-2 col-sm-6 ">

<label>Quantity</label>
<input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[utilised_qty][]" type="text">

</div>

<div class=" col-md-2 col-sm-6 ">



<label>UOM</label>
<input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[uom][]" type="text" id="uom_` + resource_count + `" readonly>
</div>





                 
                  <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[unit_id][]" type="hidden" id="unit_id_` + resource_count + `" >


                 



                

                <!-- remove button -->
                        <div class=" col-md-2 col-sm-6 padding-16 margin-top-15">
                       
                        <input type="button" id="` + resource_count + `" value="Remove" class="remove-resource-btn">
                        </div>
                        <!-- end remove button -->

</div>
      
      `
    );


    $("#resources_used_" + resource_count).change(function() {
      var resources_used = $("#resources_used_" + resource_count).val();

      $.ajax({
        method: "POST",
        data: {
          resources_used: resources_used,

        },
        "dataType": "json",
        url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getResourceUnit'); ?>',
        success: function(data) {

          $('#uom_' + resource_count).val(data.unit);
          $('#unit_id_' + resource_count).val(data.unit_id);
        }
      });
    });

    $(document).on('click', '.remove-resource-btn', function() {
      id = this.id;
      $(".remove-resource-div_" + id).remove();
    });

  } //addResources end



  $("#current_status").change(function() {
    var status = $('#current_status').val();
    var balance_quantity = $('#bal_quantity_id').val();
    //alert(status);
    if (status == 7) {
     

      if (balance_quantity != "") {

        if (balance_quantity != 0) {
          $("#current_status").val("");
          $("#DailyWorkProgress_current_status_error_message_").css("display", "");
          $("#DailyWorkProgress_current_status_error_message_").text("The balance quantity should be 0 if the status is to be 'completed'");

        }
        if(balance_quantity==0)
        {
          $("#DailyWorkProgress_current_status_error_message_").hide();
        }


      }

    } else {

      $("#DailyWorkProgress_current_status_error_message_").hide();

      if (balance_quantity == 0) {
        $("#current_status").val("");
        $("#DailyWorkProgress_current_status_error_message_").css("display", "");
        $("#DailyWorkProgress_current_status_error_message_").text("If the balance quantity is 0 then the status should be 'completed'");
      }
    }
  });
</script>