
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<h1>All Work Progresses</h1>

<?php $this->renderPartial('_newsearch', array('model' => $model)) ?>
<br>
<div class="table-responsive">

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_newview', 'template' => '<div class="grid-view"><table cellpadding="10" class="table table-bordered">{items}</table>{pager}</div>',
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
            'nextPageLabel'=>'Next ' ),    
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
        )); ?>

</div>

<script>
$("#project_id").change(function(){
        var val = $(this).val();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getAllTasks'); ?>',
            data:{project_id:val},            
            method: "GET",
            success: function(result) {
                $('#taskid').html(result);
            }
	    })
    })
</script>
