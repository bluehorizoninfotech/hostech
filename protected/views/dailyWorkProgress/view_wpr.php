<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<?php
/* @var $this DailyWorkProgressController */
/* @var $model DailyWorkProgress */
$this->breadcrumbs = array(
    'Daily Work Progresses' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List DailyWorkProgress', 'url' => array('index')),
    array('label' => 'Create DailyWorkProgress', 'url' => array('create')),
    array('label' => 'Update DailyWorkProgress', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete DailyWorkProgress', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage DailyWorkProgress', 'url' => array('admin')),
);
?>


<div class="boq_details_sec panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title clearfix">
            <div class="pull-left">
                <h6 class="text-white">#<?php echo $model->id; ?> <?php echo ((isset($model->tasks) ? $model->tasks->title : "")); ?></h6>
            </div>
            <div class="pull-right">
                <b class="text_sm"><?php echo ($model->createdBy->first_name . " " . $model->createdBy->last_name); ?></b> /
                <b class="right_space text_sm"><?php echo (date('d-M-Y H:i:s', strtotime($model->created_date))); ?></b>
                <span class="boq_close" onclick="closeBoqView(this)"><a href="#">X</a></span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3 col-xs-6 ">
                <b>Date</b>
                <div>
                    <?php echo (date("d-M-y", strtotime($model->date))); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 ">
                <b>Project</b>
                <div>
                    <?php echo ($model->project->name); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 ">
                <b>Entered Quantity</b>
                <div>
                    <?php echo (!empty($unit_name) ? $model->qty . ' (' . $unit_name . ') ' : $model->qty); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 ">
                <b>Total Quantity</b>
                <div>
                    <?php echo (!empty($unit_name) ? $model->tasks->quantity . ' (' . $unit_name . ') ' : $model->tasks->quantity); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <b>Work Type</b>
                <div>
                    <?php echo (isset($model->worktype) ? $model->worktype->work_type : ""); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <b>Daily Work Progress</b>
                <div>
                    <?php echo (isset($model->daily_work_progress) ? number_format($model->daily_work_progress, 2) : ""); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <b>Current Status</b>
                <div>
                    <?php echo ($model->statusLabel($model)); ?>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <b>Approved Status</b>
                <div>
                    <b><?php echo ($model->approveStatus($model)); ?></b>
                </div>
            </div>
        </div>
        <?php if ($model->approve_status == 2) { ?>
            <div class="row">
                <div class="col-md-12">
                    <b>Reason for Rejection</b>
                    <div>
                        <?php echo  $model->approve_reject_data; ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($model->approve_status == 1) { ?>
            <div class="row">
                <div class="col-md-12">
                    <b>Comment</b>
                    <div>
                        <?php echo  $model->approve_reject_data; ?>
                    </div>
                </div>
            </div>
        <?php } ?>


        <div class="row">
            <div class="col-md-12">
                <?php

                foreach ($images as $key => $val) {
                    $file_name = $val['image_path'];

                    if ($val['image_status'] == 0) {
                ?>

                        <input type="checkbox" id="cb_<?= $key ?>" value=<?= $val['id'] ?> class="cbCheck" />
                        <label for="cb"><img src="<?= Yii::app()->request->baseUrl . '/uploads/dailywork_progress/' . $file_name ?>" class="width-100 height-100" title="<?= $val['image_label'] ?>" /></label>

                    <?php } else {

                    ?>

                        <label for="cb"><img src="<?= Yii::app()->request->baseUrl . '/uploads/dailywork_progress/' . $file_name ?>" class="width-100 height-100" title="<?= $val['image_label'] ?>" /></label>


                <?php }
                } ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <b>Description</b>
                <span>
                    <?php echo ($model->description); ?>
                </span>
            </div>
        </div>
        <div class="detail-sec">
            <?php if ($model->approve_status == 0 &&  in_array(
                "/dailyWorkProgress/approveentry",
                Yii::app()->session["menuauthlist"]
            ) && in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])) { ?>
                <div class="row mb-0">
                    <div class="col-md-7">
                        <textarea name="reason" class="reason" id="reason_id"></textarea>
                    </div>
                    <div class="col-md-5 input_top">
                        <input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approve' />
                        <input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Reject' />
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div id="actionmsg"> </div>
<?php
$images = json_decode($model->img_paths);
if (count($images) > 0) {
    foreach ($images as $key => $value) {
        if ($value != '') {
            $path = realpath(Yii::app()->basePath . '/../uploads/dailywork_progress/' . $value);
            if (file_exists($path)) {
?>
                <div class="daily-progress-img-holder">
                    <img width="150" class="img_click" src="<?php echo Yii::app()->request->baseUrl . "/uploads/dailywork_progress/" . $value; ?>" alt="...">
                </div>
<?php
            }
        }
    }
}
?>
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close img_close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <img class="img_section" src="">
            </div>
        </div>
    </div>
</div>
<script>
    $(".img_click").click(function() {
        $("#myModal").show();
        var data = $(this).attr('src');
        $(".img_section").attr('src', data);
    });
    $(".img_close").click(function() {
        $("#myModal").hide();
    });

  

    var imagearray = [];
    $(document).on("click", ".cbCheck", function() {
        var value = $(this).attr("value");
        imagearray.push(value);


    });


    $('.shift_action').click(function() {
        var req = $(this).val();
        var id = <?php echo $model->id ?>;

        var reason = $('#reason_id').val();


        var type = 0;
        if (req == "Reject") {
            if (reason == "") {
                alert("please enter reason")
                flag = 1;
            } else {
                if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {
                    $('#load').show();
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('/dailyWorkProgress/Rejectentry_wpr') ?>',
                        data: {
                            entry_id: id,
                            reason: reason,
                            imagearray: imagearray
                        }
                    }).done(function(result) {
                        $('#load').hide();
                        if (result.status == '1') {
                            $('#actionmsg').addClass('alert alert-success');
                            $('#actionmsg').addClass('successaction');
                            $('#actionmsg').html(result.message);
                            $("#content").load(location.href + " #content");
                            $('.shift_action').hide();
                            $('.reason').hide();
                        } else if (result.status == '2') {
                            $('#actionmsg').addClass('alert alert-danger');
                            $('#actionmsg').addClass('erroraction');
                            $('#actionmsg').html(result.message);
                        }
                        location.reload();
                    });
                }
            }
        } else if (req == "Approve") {

            if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {
                $('#load').show();
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('/dailyWorkProgress/approve_wpr_entry') ?>',
                    data: {
                        entry_id: id,
                        reason: reason,
                        imagearray: imagearray
                    }
                }).done(function(result) {

                    $('#load').hide();
                    if (result.status == '1') {

                        $('#actionmsg').addClass('alert alert-success');
                        $('#actionmsg').addClass('successaction');
                        if (result.schedule != '' && result.dependant != "") {
                            $('#actionmsg').html(result.schedule + '-' + result.dependant);
                        } else if (result.schedule != '') {
                            $('#actionmsg').html(result.schedule);
                        } else if (result.dependant != "") {
                            $('#actionmsg').html(result.dependant);
                        }
                        $('.shift_action').hide();
                        $("#content").load(location.href + " #content");
                        $('.reason').hide();
                    } else if (result.status == '2') {
                        $('#actionmsg').addClass('alert alert-danger');
                        $('#actionmsg').addClass('erroraction');
                        $('#actionmsg').html(result.message);
                    }
                    location.reload();
                });
            }
        }
    });

    function closeBoqView(formname) {
        $('.boq_details_sec').animate({
            height: 'hide'
        }, 500);
    }
</script>