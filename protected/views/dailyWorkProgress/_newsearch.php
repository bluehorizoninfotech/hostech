<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>
<!--<h5>Filter By :</h5>-->
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?> 
<div class="new-search-sec">
		<div class="form">
				<div class="row ">
 			
					<div class="subrow col-md-2 col-sm-6">	
						<?php echo $form->label($model, 'project_id'); ?>
                        <?php
						if(Yii::app()->user->role == 1){
							$condition = array('select' =>  array('pid,name'),'condition'=>'status=1','order' => 'name ASC');
							}else{
								$criteria = new CDbCriteria;
								$criteria->select = 'project_id'; 
								$criteria->condition = 'assigned_to = '.Yii::app()->user->id.' OR report_to = '.Yii::app()->user->id.' OR coordinator = '.Yii::app()->user->id;
								$criteria->group = 'project_id';                
								$project_ids = Tasks::model()->findAll($criteria);
								$project_id_array= array();
								foreach($project_ids as $projid){
									array_push($project_id_array,$projid['project_id']);
								} 							
								if(!empty($project_id_array)){
									$project_id_array = implode(',',$project_id_array); 
									$condition = array('select' =>  array('pid,name'), 'condition'=>'status=1 AND find_in_set('.Yii::app()->user->id.',assigned_to) OR pid IN ('.$project_id_array.')','order' => 'name ASC');
								}else{
									$condition = array('select' =>  array('pid,name'), 'condition'=>'status=1 AND find_in_set('.Yii::app()->user->id.',assigned_to)','order' => 'name ASC');
								}    
							
							}
							if(Yii::app()->user->project_id !=""){
								// $model->project_id = Yii::app()->user->project_id;
								// $project_id = Yii::app()->user->project_id;
							}else{
								$model->project_id = "";
								$project_id = "";
							}
						// if(Yii::app()->user->role == 10){
						// 	$condition = 'assigned_to ='.Yii::app()->user->id;
						// 	 }else{
								   
						// 		$condition = '';
						//  }
						   echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'), array('empty' => 'Select Project','class'=>'form-control change_project', 'id' => 'project_id','ajax' => array(
					                        'type' => 'POST',
					                       'dataType' => 'json',
					                       'url' => CController::createUrl('dailyWorkProgress/getAllTasks'),
					                       'data' => array('project_id' => 'js:this.value'),
					                       'success' => 'function(data) {    
					                                 $("#taskid").html(data);
					                              }'
					                        )));
					       
						?>
					</div>
								
					<div class="subrow col-md-2 col-sm-6">					
					<?php echo $form->label($model,'taskid'); ?>
					<?php    
					 if(isset($model->project_id) && trim($model->project_id)!=""){
					                     echo $form->dropDownList($model, 'taskid', CHtml::listData(Tasks::model()->findAll(array(
					                             'select' => array('tskid, title'),
					                             "condition"=>'project_id ='.$model->project_id,
					                             'distinct' => true
					                         )), 'tskid', 'title'), array('class'=>'form-control','empty' => '-----------', 'id'=>'taskid'));	
					               }else{
                                       //echo "dfdf";
					   echo $form->dropDownList($model, 'taskid', array(), array('empty' => '-----------','class'=>'form-control', 'id'=>'taskid'));
					   }   
				//echo $form->dropDownList($model,'item_id', array(), array('prompt'=>'Select item','class'=>'form-control', 'id' => 'item_id'));		
					?>
					</div>
					<?php if(Yii::app()->user->role != 10){ ?>
					<div class="subrow col-md-2 col-sm-6 ">	
						 <?php echo $form->label($model, 'User'); ?>
						 <?php 
						 $condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0','order' => 'userid ASC');
						 echo $form->dropDownList($model, 'created_by', CHtml::listData(Users::model()->findAll($condition), 'userid', 'full_name'), array('empty' => '-Choose a User-','class'=>'form-control input-medium')); 
						 ?>
						
					</div>	
					<?php }?>
					
					<div class="subrow col-md-3 col-sm-6 form-inline">						
						<?php echo $form->label($model, 'date'); ?>
						<?php echo CHtml::activeTextField($model, 'fromdate', array('class'=>'form-control','size'=>10,'value'=>isset($_REQUEST['DailyWorkProgress']['fromdate']) ? $_REQUEST['DailyWorkProgress']['fromdate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'DailyWorkProgress_fromdate',
							'ifFormat' => '%d-%m-%Y',
						));
						?> to
						<?php echo CHtml::activeTextField($model, 'todate', array('class'=>'form-control','size'=>10,'value'=>isset($_REQUEST['DailyWorkProgress']['todate']) ? $_REQUEST['DailyWorkProgress']['todate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'DailyWorkProgress_todate',
							'ifFormat' => '%d-%m-%Y',
						));
						?>
						
					</div>
					
					
					<div class="subrow col-md-2 col-sm-6">	
					<label>&nbsp;</label>
					<?php echo CHtml::submitButton('Go',array('class' => 'btn blue btn-sm')); ?>
					<?php echo CHtml::resetButton('Clear', array('class' => 'btn default btn-sm','onclick' => 'javascript:location.href="'. $this->createUrl('workreport').'"')); ?>
					
					</div>
		 
				</div>
				
				</div>
					</div>
<?php $this->endWidget(); ?>

