<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="dailyWorkProgress-report-sec">
<div class="table-responsive">
    <div id="time_area">
        <div class="clearfix">
            <div id="" class="pull-right">
                <!--a style="cursor:pointer;" id="" class="" href="<?php echo $this->createAbsoluteUrl('dailyWorkProgress/exportExcel', array("start_date" => $start_date, "end_date" => $end_date, "week_title" => $week_title)) ?>">SAVE AS EXCEL</a-->
                <?php
                if (isset(Yii::app()->user->role) && (in_array('/dailyWorkProgress/amount_visibility', Yii::app()->session['menuauthlist']))) {
                ?>
                    Amount Visibility <label class="checkbox-inline">
                        <input type="checkbox" class="change" data-toggle="toggle">
                    </label>
                <?php } ?>
                <a class="download btn blue" href="<?php echo $this->createAbsoluteUrl('dailyWorkProgress/exportExcel', array("start_date" => $start_date, "end_date" => $end_date, "week_title" => $week_title,"type"=>$type)) ?>">SAVE AS EXCEL</a>
                <span class="loading display-none">Loading...</span>
            </div>
            <?php
            if ($type == 1) {
                echo ' <div class="titleh3"><h1>MonthlyReports - Month of ' . $week_title . '</h1></div>';
            } elseif ($type  == 2) {
                echo ' <div class="titleh3"><h1>WeeklyReports - Week of ' . $week_title . '</h1></div>';
            } elseif ($type == 3) {
                echo ' <div class="titleh3"><h1>DailyReport - Day ' . $week_title . '</h1></div>';
            }

            ?>

        </div>
        <div>
            <div class="pull-left">
                <?php
                if (isset($type)) {
                    if ($type == 1) {
                        $selected_val = "1";
                    } elseif ($type == 2) {
                        $selected_val = "2";
                    } elseif ($type == 3) {
                        $selected_val = "3";
                    }
                } else {
                    $selected_val = "2";
                }
                ?>
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'daily-work-progress-form',
                    'action' => Yii::app()->createUrl('/DailyWorkProgress/reports'),

                )); ?>
                <select id="changeoption" name="changeoption" class="form-control valid height-28" placeholder="Choose">
                    <option value="_none" disabled="disabled" selected="selected">Choose</option>
                    <option value="1" <?= $selected_val == '1' ? ' selected="selected"' : ''; ?>>Monthly</option>
                    <option value="2" <?= $selected_val == '2' ? ' selected="selected"' : ''; ?>>Weekly</option>
                    <option value="3" <?= $selected_val == '3' ? ' selected="selected"' : ''; ?>>Daily</option>
                </select>
            </div>
            <div class="pull-left form-group margin-right-5 margin-left-5">
                <div>
                    <input type="text" id="datepicker" name="date" value="<?php echo $chosen_date; ?>" class="form-control display-inline-block width-200 height-28"></div>
            </div>
        </div>
        <?php echo CHtml::submitButton('Submit', array('class' => 'save_btn btn blue btn-sm')); ?>
    </div>
    <?php $this->endWidget(); ?>

    <input type="hidden" id="changed_option" value="qty">
    <!--h1>Work Progress in this Week</h1-->
    <div class="prev_curr_next">

    </div>

    <table cellpadding="0" cellspacing="0" id="report" class="table table-bordered">
        <thead>
            <tr>
                <th rowspan="3">Projects</th>
                <th rowspan="3">Tasks</th>
                <th width="80">Up To Previous Week</th>
                <?php
                if ($type == 2) {
                    $week_day = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
                    $week_dates = array();
                    $week_date = '';

                    for ($i = 0; $i < 7; $i++) {
                        // echo $start_date;exit;
                        $week_date = $this->add_date($start_date, $i);
                        $week_date1 = date("d-m-Y", strtotime($week_date));
                        echo "<th width='90' colspan='1'>" . $week_day[$i] . " <br>  "  . $week_date1 . "</th>";
                        $week_dates[] = $week_date;
                    }
                } elseif ($type == 1) {
                    $start = strtotime($start_date); // or your date as well
                    $end = strtotime($end_date);
                    $datediff = $end - $start;
                    $datedif = round($datediff / (60 * 60 * 24));

                    for ($i = 1; $i <= $datedif + 1; $i++) {
                        echo "<th width='90' colspan='1'>" . $i . "</th>";
                    }
                } elseif ($type == 3) {
                    $date = strtotime($start_date);
                    $day = date('l', $date);
                    echo "<th width='90' colspan='1'>" . $day . " <br>  "  . date('d-m-Y', $date) . "</th>";
                }

                ?>
                <th width="80" rowspan="2" class="listqty">Total Quantity</th>
                <th width="80" rowspan="2" class="listworkers">Total No.Of Workers</th>
                <th width="80" rowspan="2" id="tot_amount" class="display-none">Total Amount</th>
                <th width="80" rowspan="2">Progress Percentage(%)</th>

            </tr>

            <tr>
                <th width="90">Progress Percentage(%)</th>
                <?php
                if ($type == 2) {
                    $week_day = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
                    $week_dates = array();
                    $week_date = '';

                    for ($i = 0; $i < 7; $i++) {
                        echo '<th width="90" class="listqty">Qty</th>
                       <th width="90" class="listworkers">No.Of Workers</th>';
                    }
                } elseif ($type == 1) {
                    $start = strtotime($start_date); // or your date as well
                    $end = strtotime($end_date);
                    $datediff = $end - $start;
                    $datedif = round($datediff / (60 * 60 * 24));

                    for ($i = 1; $i <= $datedif + 1; $i++) {
                        echo '	<th width="90" class="listqty">Qty</th>
                    <th width="90" class="listworkers">No.Of Workers</th>';
                    }
                } elseif ($type == 3) {
                    echo '	<th width="90" class="listqty">Qty</th>
                    <th width="90" class="listworkers">No.Of Workers</th>';
                }
                ?>
            </tr>
        </thead>
        <?php
        $result = $this->getConsolidEntries($start_date, $end_date);
        // echo '<pre>'  ;print_r($result);exit;
        $weekEntries = $result['data'];
        if (count($weekEntries) == 0) {
        ?>
            <tr class="even">
                <td colspan="3">This week's workreports does not have any work added.</td>
                <?php
                if ($type == 2) {
                    for ($i = 0; $i < 7; $i++) {
                        echo "<td width='80'>&nbsp;</td>";
                    }
                } elseif ($type == 1) {
                    $start = strtotime($start_date); // or your date as well
                    $end = strtotime($end_date);
                    $datediff = $end - $start;
                    $datedif = round($datediff / (60 * 60 * 24));

                    for ($i = 1; $i <= $datedif + 1; $i++) {
                        echo "<td width='80'>&nbsp;</td>";
                    }
                } elseif ($type == 3) {
                    echo "<td width='80'>&nbsp;</td>";
                }

                ?>
                <td width="80" class="tbl_title"></td>
            </tr>
        <?php
        }
        ?>
        <tbody>
            <?php
            $project = '';


            foreach ($weekEntries as $data) {
            ?>
                <tr>

                    <?php
                    if ($project != $data['project_id']) {

                        $tblpx = Yii::app()->db->tablePrefix;
                        $projectid = $data['project_id'];
                        $rowspan = $this->getcountEntries($start_date, $end_date, $projectid);
                    ?>
                        <td rowspan="<?php echo $rowspan; ?>"><?php echo $data->project->name; ?></td>
                    <?php
                        $project = $data['project_id'];
                        $item = '';
                    }
                    ?>

                    <?php


                    if (($project == $data['project_id'])) {
                        $tasks = Tasks::model()->findByPk($data['taskid']);
                    ?>
                        <td><?php echo $tasks['title']; ?></td>
                        <?php

                        //   $item = $data['sl_no'];
                        $item_id = $data['taskid'];
                        $tot_prev = "SELECT sum(qty) as qty , sum(no_of_workers) as workers
						   FROM " . $tblpx . "daily_work_progress 
						   where approve_status = 1 AND date < '" . $start_date . "' AND  project_id = " . $projectid . " AND taskid = '" . $item_id . "'";
                        $new_tot_prev = Yii::app()->db->createCommand($tot_prev)->queryRow();
                        $prev_percentge = ($new_tot_prev['qty'] / $data->tasks->quantity) * 100
                        ?>

                        <td><?php echo number_format($prev_percentge, 2) ?></td>

                        <?php

                        $newdate =   strtotime($data['date']);

                        $qty = 0;
                        $tot_workers = 0;
                        if ($type == 2) {
                            for ($i = 0; $i < 7; $i++) {
                                $week_date =  strtotime("+$i day", strtotime($start_date));
                                $weekda =  date("Y-m-d", $week_date);
                                

                                $newqry = "SELECT sum(qty) as qty , sum(no_of_workers) as workers,t.*
                           FROM " . $tblpx . "daily_work_progress as d
                           left join " . $tblpx . "tasks as t on t.tskid=d.taskid
						   where approve_status = 1 AND date = '" . $weekda . "' AND  d.project_id = " . $projectid . " AND taskid = " . $item_id;
                                $newsql = Yii::app()->db->createCommand($newqry)->queryRow();
                                $check_child_exist = '';
                                if (empty($newsql['parent_tskid']) && !empty($newsql['tskid'])) {
                                    $check_sql =  "SELECT tskid
                            FROM " . $tblpx . "tasks as t                          
                            where t.parent_tskid = " . $newsql['tskid'];
                                    $check = Yii::app()->db->createCommand($check_sql)->queryAll();
                                    $task_array = array();
                                    foreach ($check as $chck) {
                                        array_push($task_array, $chck['tskid']);
                                    }
                                    if (!empty($task_array)) {
                                        $criteria1 = new CDbCriteria();
                                        $criteria1->addInCondition('taskid', $task_array);
                                        $criteria1->addCondition('date = "' . $weekda . '" ');
                                        $criteria1->addCondition('approve_status = 1');
                                        $check_child_exist = DailyWorkProgress::model()->findAll($criteria1);
                                    }
                                }
                                if (empty($check_child_exist)) {
                                    $qty = $qty + $newsql['qty'];
                                    echo "<td width='80' class='listqty'>" . $newsql['qty'] . "</td>";
                                } else {
                                    echo "<td width='80' class='listqty'></td>";
                                }



                                $tot_workers = $tot_workers + $newsql['workers'];
                                echo "<td width='80' class='listworkers'>" . $newsql['workers'] . "</td>";
                            }
                        } elseif ($type == 1) {

                            $start = strtotime($start_date); // or your date as well
                            $end = strtotime($end_date);
                            $datediff = $end - $start;
                            $datedif = round($datediff / (60 * 60 * 24));

                            for ($i = 1; $i <= $datedif + 1; $i++) {
                                $month = date('m', $start);
                                $year = date('Y', $start);
                                $day = $year . '-' . $month . '-' . $i;

                                $newqry = "SELECT sum(qty) as qty , sum(no_of_workers) as workers,t.*
                        FROM " . $tblpx . "daily_work_progress as d
                        left join " . $tblpx . "tasks as t on t.tskid=d.taskid
                        where approve_status = 1 AND date = '" . $day . "' AND  d.project_id = " . $projectid . " AND taskid = " . $item_id;
                                $newsql = Yii::app()->db->createCommand($newqry)->queryRow();
                                $check_child_exist = '';
                                if (empty($newsql['parent_tskid']) && !empty($newsql['tskid'])) {
                                    $check_sql =  "SELECT tskid
                            FROM " . $tblpx . "tasks as t                          
                            where t.parent_tskid = " . $newsql['tskid'];
                                    $check = Yii::app()->db->createCommand($check_sql)->queryAll();
                                    $task_array = array();
                                    foreach ($check as $chck) {
                                        array_push($task_array, $chck['tskid']);
                                    }
                                    if (!empty($task_array)) {
                                        $criteria1 = new CDbCriteria();
                                        $criteria1->addInCondition('taskid', $task_array);
                                        $criteria1->addCondition('date = "' . $weekda . '" ');
                                        $criteria1->addCondition('approve_status = 1');
                                        $check_child_exist = DailyWorkProgress::model()->findAll($criteria1);
                                    }
                                }
                                if (empty($check_child_exist)) {
                                    $qty = $qty + $newsql['qty'];
                                    echo "<td width='80' class='listqty'>" . $newsql['qty'] . "</td>";
                                } else {
                                    echo "<td width='80' class='listqty'></td>";
                                }



                                $tot_workers = $tot_workers + $newsql['workers'];
                                echo "<td width='80' class='listworkers'>" . $newsql['workers'] . "</td>";
                            }
                        } elseif ($type == 3) {
                            $date = date('Y-m-d', strtotime($start_date));

                            $newqry = "SELECT sum(qty) as qty , sum(no_of_workers) as workers,t.*
                       FROM " . $tblpx . "daily_work_progress as d
                       left join " . $tblpx . "tasks as t on t.tskid=d.taskid
                       where approve_status = 1 AND date = '" . $date . "' AND  d.project_id = " . $projectid . " AND taskid = " . $item_id;
                            $newsql = Yii::app()->db->createCommand($newqry)->queryRow();
                            $check_child_exist = '';
                            if (empty($newsql['parent_tskid']) && !empty($newsql['tskid'])) {
                                $check_sql =  "SELECT tskid
                           FROM " . $tblpx . "tasks as t                          
                           where t.parent_tskid = " . $newsql['tskid'];
                                $check = Yii::app()->db->createCommand($check_sql)->queryAll();
                                $task_array = array();
                                foreach ($check as $chck) {
                                    array_push($task_array, $chck['tskid']);
                                }
                                if (!empty($task_array)) {
                                    $criteria1 = new CDbCriteria();
                                    $criteria1->addInCondition('taskid', $task_array);
                                    $criteria1->addCondition('date = "' . $weekda . '" ');
                                    $criteria1->addCondition('approve_status = 1');
                                    $check_child_exist = DailyWorkProgress::model()->findAll($criteria1);
                                }
                            }
                            if (empty($check_child_exist)) {
                                $qty = $qty + $newsql['qty'];
                                echo "<td width='80' class='listqty'>" . $newsql['qty'] . "</td>";
                            } else {
                                echo "<td width='80' class='listqty'></td>";
                            }



                            $tot_workers = $tot_workers + $newsql['workers'];
                            echo "<td width='80' class='listworkers'>" . $newsql['workers'] . "</td>";
                        }
                        ?>
                        <td width="80" class='listqty'><?php echo $qty; ?></td>
                        <td width="80" class="listworkers"><?php echo $tot_workers; ?></td>
                        <td width="80" class="amount display-none"><span class="section1"><?php echo $qty * $data->tasks->rate; ?></span><span class="section2"></span></td>
                        <?php
                        $tot_tod = "SELECT sum(qty) as qty , sum(no_of_workers) as workers
						FROM " . $tblpx . "daily_work_progress 
						where approve_status = 1 AND date <= '" . $end_date . "' AND  project_id = " . $projectid . " AND taskid = '" . $item_id . "'";
                        $new_tot_tod = Yii::app()->db->createCommand($tot_tod)->queryRow();

                        ?>
                        <td width="80"><?php echo round(($new_tot_tod['qty'] / $data->tasks->quantity) * 100, 2) ?></td>
                    <?php
                    }
                    ?>

                </tr>
            <?php
            }
            ?>
        </tbody>

    </table>
</div>
</div>
        </div>


<?php

Yii::app()->clientScript->registerCss('mycss', "
     .listworkers
     {
       display: none;
     }
");

Yii::app()->clientScript->registerScript('myjquery', " 

chageRequest();
$(document).ajaxComplete(function(){
	chageRequest();
});
	

	
	function chageRequest(){
	$('#changeoption').on('change', function() {		
	   if ($(this).val() === 'Quantity') 
	   {
	   $('.listqty').show();
	   $('.listworkers').hide();
	   }
	    if ($(this).val() === 'Workers') 
	   {
	   $('.listqty').hide();
	   $('.listworkers').show();
	   $('#changed_option').val();
	   }
	   
	  
	});
}
/*
$('.download').click(function (e) {
	
    e.preventDefault();
    var link = $(this);
    $('.loading').show();
    var option = $('#changed_option').val();
    var url = link.attr('href');
    url = url + '&option='+option,
    //alert(url);


    $.ajax({
        type: 'HEAD',
        url:  link.attr('href');
		complete: function () {

            $('.loading').hide();
            var ifr = $('<iframe />').attr('src', link.attr('href')).hide().appendTo(link)
            setTimeout(function () {ifr.remove();}, 5000);
        }

    });

});
*/
	

      
 ");
?>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: 'd-M-y',
            changeMonth: true,
            changeYear: true
        });

    });
    // $('.change').on('switchChange', function (event, state) {
    //     var x = $(this).data('on-text');
    //     var y = $(this).data('off-text');
    //     aler();
    //     // if($("#switch").is(':checked')) {
    //     //   $("#in0p1").val(x);
    //     // } else {
    //     //   $("#inp1").val(y);
    //     // }
    // });

    $('.change').on('change.bootstrapSwitch', function(e) {
        var status = e.target.checked;
        if (status === true) {
            // $('.amount').removeClass('blur');
            $('#tot_amount').show();
            // $('.amount').show();
            $('#report').find('.amount').each(function(i, el) {
                $('.amount').show();
            });
        } else {
            $('#tot_amount').hide();
            $('#report').find('.amount').each(function(i, el) {
                // $('.amount').addClass('blur');
                $('.amount').hide();
            });
        }
    });
</script>