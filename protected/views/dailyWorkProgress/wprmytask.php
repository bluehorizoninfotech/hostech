<div class="wprmytask-sec">
<?php
$model=new Tasks();

 $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'daily-work-progress-task-grid',
    'htmlOptions' => array('class' => 'grid-view'),
    'itemsCssClass' => 'table table-bordered progress-table',
    'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',

    'pager' => array(
        'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->wpr_task(),
    'ajaxUpdate' => false,
    //'filter' => $model,
    //'selectableRows' => 2,
    'rowCssClassExpression' => '$data->task_wpr_id > 0 ? "bg_green":""',
    'columns' => array(

        array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'),),

        array(
            'name' => 'title',
            'value' => '($data->acknowledge_by=="")?$data->title:$data->title."<i class=\"fa fa-check\" style=\"margin-left:5px;color:green\"></i>" ',
            'type' => 'raw',
            
        ),

        array(
            'name' => 'project_id',
            'value' => '(empty($data->project_id)?"test":CHtml::link($data->project->name, array("Projects/view","id"=>$data->project_id)))',
            'type' => 'raw',
           
        ),
        array(
            'name' => 'coordinator',
            'value' => '!empty($data->coordinator)?$data->coordinator0->first_name." ".$data->coordinator0->last_name:""',
            'type' => 'raw',
            
        ),
        array(
            'name' => 'quantity',
            'value' => '$data->quantity'
        ),
        array(
            'name' => 'start_date',
            'value' => 'date("d-M-y",strtotime($data->start_date))',
            'type' => 'html',
            'htmlOptions' => array('class' => 'date_start')
        ),
        array(
            'name' => 'due_date',
            'value' => 'date("d-M-y",strtotime($data->due_date))',
            'type' => 'html',
            'htmlOptions' => array('class' => 'date_end'),
            'cssClassExpression' => '(isset($data->due_date) && !empty($data->due_date) && isset($data->status) && ($data->status==6) && strtotime($data->due_date) < strtotime(date("Y-m-d"))) ? "text-danger1" : ""',
        ),
        array(
            'name' => 'status',
            'header' => 'Status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
         ),
       
        array(
            'htmlOptions' => array('class' => 'white-space-nowrap'),
            'class' => 'ButtonColumn',
            'template' => '{create_wpr}{MR}',
            'evaluateID' => true,

            'buttons' => array(
               
                'create_wpr' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'visible'=>'Yii::app()->user->role != 9',
                    // 'options' => array('class' => 'fa fa-plus addwpr', 'title' => 'Create WPR', 'id' => '$data->tskid'),
                    'options' => array('class' => 'fa fa-plus', 'title' => 'Create WPR'),
                    'url' => 'Yii::app()->createUrl("DailyWorkProgress/dailyProgress", array("type"=>2,"taskid"=>$data->tskid))',
                ),
                'MR' => array(
                    'label' => '',
                    'options' => array('class' => 'icon-docs icon-comn', 'title' => 'MR'),
                    'visible' => 'in_array("/materialRequisition/index", Yii::app()->session["menuauthlist"]) && $data->checkMRVisibility($data->project_id)==1',
                    'url' => 'Yii::app()->createUrl("MaterialRequisition/create", array("id"=>$data->tskid,"type"=>2))',
                ),

              
               
              
            )

        ),


     
       
    ),
));
?>
</div>
