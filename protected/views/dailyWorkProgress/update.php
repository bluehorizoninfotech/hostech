<?php
/* @var $this DailyWorkProgressController */
/* @var $model DailyWorkProgress */

$this->breadcrumbs=array(
	'Daily Work Progresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DailyWorkProgress', 'url'=>array('index')),
	array('label'=>'Create DailyWorkProgress', 'url'=>array('create')),
	array('label'=>'View DailyWorkProgress', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DailyWorkProgress', 'url'=>array('admin')),
);
?>

<h1>Update DailyWorkProgress <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>