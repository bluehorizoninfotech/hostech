
<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>
<!--<h5>Filter By :</h5>-->
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?> <div class="gallerysearch-sec">
		<div class="form">
				<div class="row ">
 			
					<div class="subrow col-md-3 col-sm-6">	
						<?php echo $form->label($model, 'project_id'); ?>
                        <?php
						if(Yii::app()->user->role == 1){
							$condition = array('select' =>  array('pid,name'),'condition'=>'status=1','order' => 'name ASC');
							}else{
								$criteria = new CDbCriteria;
								$criteria->select = 'project_id'; 
								$criteria->condition = 'assigned_to = '.Yii::app()->user->id.' OR report_to = '.Yii::app()->user->id.' OR coordinator = '.Yii::app()->user->id;
								$criteria->group = 'project_id';                
								$project_ids = Tasks::model()->findAll($criteria);
								$project_id_array= array();
								foreach($project_ids as $projid){
									array_push($project_id_array,$projid['project_id']);
								} 							
								if(!empty($project_id_array)){
									$project_id_array = implode(',',$project_id_array); 
									$condition = array('select' =>  array('pid,name'), 'condition'=>'status=1 AND find_in_set('.Yii::app()->user->id.',assigned_to) OR pid IN ('.$project_id_array.')','order' => 'name ASC');
								}else{
									$condition = array('select' =>  array('pid,name'), 'condition'=>'status=1 AND find_in_set('.Yii::app()->user->id.',assigned_to)','order' => 'name ASC');
								}    
							
							}
							if(Yii::app()->user->project_id !=""){
								$model->project_id = Yii::app()->user->project_id;
								$project_id = Yii::app()->user->project_id;
							}else{
								$model->project_id = "";
								$project_id = "";
							}
						
						   echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'),
						    array('empty' => 'Select Project','class'=>'form-control change_project', 
							'id' => 'project_id',
							
										));
					       
						?>
					</div>
					<?php 
					 $Criteria = new CDbCriteria();
					 $Criteria->condition = "trash = 1";				                
					 $tasks = Tasks::model()->findAll();
					 $milestone=Milestone::model()->findALL();
					 $work_type=WorkType::model()->findALL();
					 $area=Area::model()->findAll();
					
					 ?>		
					<div class="subrow col-md-3 col-sm-6">		
					<?php echo $form->label($model, 'Task'); ?>
					<select class="form-control task_id" name="DailyWorkProgress[taskid]" id="DailyWorkProgress_taskid">
                            <option value="">-Choose a Task-</option>
							<?php
                            if (!empty($tasks)) {
                                foreach ($tasks as $key => $value) {
                            ?>
                                    <option value="<?php echo $value['tskid']; ?>">
                                        <?php echo $value['title']; ?></option>
                            <?php }
                            } ?>
                           
                        </select>
					</div>

                    
					<div class="subrow col-md-3 col-sm-6">		
					<?php echo $form->label($model, 'Milestone'); ?>
					<select class="form-control milestone" name="DailyWorkProgress[milestone]" id="DailyWorkProgress_milestone">
                            <option value="">-Choose a Milestone-</option>
							<?php
                            if (!empty($milestone)) {
                                foreach ($milestone as $key => $value) {
                            ?>
                                    <option value="<?php echo $value['id']; ?>">
                                        <?php echo $value['milestone_title']; ?></option>
                            <?php }
                            } ?>
                           
                        </select>
					</div>

					<div class="subrow col-md-3 col-sm-6">		
					<?php echo $form->label($model, 'Work Type'); ?>
					<select class="form-control worktype" name="DailyWorkProgress[work_type]" id="DailyWorkProgress_worktype">
                            <option value="">-Choose a Work Type-</option>							
							<?php
                            if (!empty($work_type)) {
                                foreach ($work_type as $key => $value) {
                            ?>
                                    <option value="<?php echo $value['wtid']; ?>">
                                        <?php echo $value['work_type']; ?></option>
                            <?php }
                            } ?>
                           
                        </select>
					</div>

					<div class="subrow col-md-3 col-sm-6">		
					<?php echo $form->label($model, 'Area'); ?>
					<select class="form-control area_id" name="DailyWorkProgress[area]" id="DailyWorkProgress_area">
                            <option value="">-Choose  Area-</option>
							<?php
                            if (!empty($area)) {
                                foreach ($area as $key => $value) {
                            ?>
                                    <option value="<?php echo $value['id']; ?>">
                                        <?php echo $value['area_title']; ?></option>
                            <?php }
                            } ?>
                        </select>
					</div>					
					
					
					<div class="subrow col-md-3 col-sm-6 form-inline">						
						<?php echo $form->label($model, 'date'); ?>
						<?php echo CHtml::activeTextField($model, 'fromdate', array('class'=>'form-control w-47','size'=>10,'value'=>isset($_REQUEST['DailyWorkProgress']['fromdate']) ? $_REQUEST['DailyWorkProgress']['fromdate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'DailyWorkProgress_fromdate',
							'ifFormat' => '%d-%m-%Y',
						));
						?> to
						<?php echo CHtml::activeTextField($model, 'todate', array('class'=>'form-control w-47','size'=>10,'value'=>isset($_REQUEST['DailyWorkProgress']['todate']) ? $_REQUEST['DailyWorkProgress']['todate']: '')); ?>
						<?php
						$this->widget('application.extensions.calendar.SCalendar', array(
							'inputField' => 'DailyWorkProgress_todate',
							'ifFormat' => '%d-%m-%Y',
						));
						?>
						
					</div>
					<div class="subrow col-md-3 col-sm-6">	
					<label>&nbsp;</label>
					<?php echo CHtml::submitButton('Go',array('class' => 'btn blue btn-sm')); ?>
					<?php echo CHtml::resetButton('Clear', array('class' => 'btn default btn-sm','onclick' => 'javascript:location.href="'. $this->createUrl('gallery').'"')); ?>
					
					</div>
		 
				</div>
				
				</div>
<?php $this->endWidget(); 
 if(isset($_GET['DailyWorkProgress']))
 {
	//print_r($_GET['DailyWorkProgress']);
	if(isset($_GET['DailyWorkProgress']['taskid']))
	{
		 $task_id= $_GET['DailyWorkProgress']['taskid'];
	}
	else
	{
		$task_id=0;
	}
	if(isset($_GET['DailyWorkProgress']['milestone']))
	{
		$milestone_id=$_GET['DailyWorkProgress']['milestone'];
	}
	else
	{
		$milestone_id=0;
	}
	if(isset($_GET['DailyWorkProgress']['work_type']))
	{
		 $work_type=$_GET['DailyWorkProgress']['work_type'];
	}
	else
	{
		$work_type=0;
	}
	if(isset($_GET['DailyWorkProgress']['area']))
	{
		$area=$_GET['DailyWorkProgress']['area'];	
	}
	else
	{
		$area=0;
	}
   }
   else
   {
	$task_id=0; 
	$milestone_id=0;
	$work_type=0;
	$area=0;
   }
?>
</div>
<script>
	   $(document).ready(function() {
	   $('.change_project').on('change', function() {
       var  project_id=this.value;
	$.ajax({
        type: 'POST',
		dataType : 'json',
        url: '<?php echo Yii::app()->createAbsoluteUrl('dailyWorkProgress/tasklist'); ?>',
		data:{project_id:project_id},
        success: function(result) {            
			$('#DailyWorkProgress_taskid').html(result.html);
			$('#DailyWorkProgress_milestone').html(result.milestone_html);
			$('#DailyWorkProgress_worktype').html(result.work_type_html);
			$('#DailyWorkProgress_area').html(result.area_html);
        },
        error: function(result) {
         
        }
    });
});

var task = '<?php echo $task_id; ?>';
var work_type='<?php echo $work_type; ?>';
var milestone='<?php echo $milestone_id; ?>';
var area='<?php echo $area; ?>';
$("#DailyWorkProgress_taskid").val(task);
$('#DailyWorkProgress_milestone').val(milestone);
$('#DailyWorkProgress_worktype').val(work_type);
$('#DailyWorkProgress_area').val(area);
		   
	   });
</script>