<div class="d-flex justify-content-between wpr-border-bottom">
              <div class="progress-table-head"><h2 class="font-14 margin-bottom-0">Item Consumption</h2></div>
                   <div>
                   <button id="new-btn" class="margin-left-14 btn work-progress-table-save-botton" >New</button>
                   </div>
            </div>

                   <div class="row margin-top-20">
                   <div class="col-md-4 col-xs-12">
                   <lablel>Item</label>

                   <input type= "hidden" id="wpr_id" value="<?php echo $model->id?>">
                    <select class="form-control" id="consumed_item_id" name="DailyWorkProgress[consumed_item_id][]">
                      <option></option>

                    </select>
                    <div class="errorMessage" id="DailyWorkProgress_consumed_item_id_em_" style=""></div>

                  
                  </div>
                  <div class="col-md-3 col-xs-4">

                    <label>Rate</label>
                    <select class="form-control" id="consumed_item_rate" name="DailyWorkProgress[consumed_item_rate][]">
                      <option></option>
                    </select>
                  </div>
                  <div class="col-md-3 col-xs-4">
                  <label>Quantity</label>'
                  <input class="form-control consumed-item-qty" autocomplete="off" name="DailyWorkProgress[consumed_item_count][]" id="DailyWorkProgress_consumed_item_count" type="text">

                 <input type="hidden" id="item_already_exist">
                 <input type="hidden" id="rate_already_exist">
                 <input type="hidden" id="count_already_exist">
                
                 

                  <div class="errorMessage" id="DailyWorkProgress_consumed_item_count_em_" style=""></div>
                  </div>
                  <div class="col-md-2 col-xs-4">
                   <input type="button" id="add_new_item" value="Save"  class="btn  blue blue-button-style">
                   <input type="hidden" value="create" id="action_id">
                   </div>
                   </div>
                  <!-- </div> -->
                  <br>
         
                   <div class="work-progress-table">
                  <?php 
                  
                  $consumedItemModel = new WprItemUsed('search');

                  if($WPRID != "")
                  {
                    $consumedItemModel->wpr_id = $WPRID;
                  }
                  else
                  {
                    if (Yii::app()->user->getState('wpr_id') != "") {
                      $consumedItemModel->wpr_id = Yii::app()->user->getState('wpr_id');
                      
                   }
                  }
                 
                  
                  

                  
                 
                   $this->widget('zii.widgets.grid.CGridView', array(
                                'id' => 'item-used-grid',
                                'dataProvider' => $consumedItemModel->search(),
                                'itemsCssClass' => 'table table-bordered',
                                'pager' => array(
                                    'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                                    'nextPageLabel' => 'Next '
                                ),
                                'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                                'columns' => array(
                                    array('class' => 'IndexColumn', 'header' => 'S.No.'),
                                    array(
                                        'name' => 'item_id',
                                        'value' => function ($data) {

                                          echo $data->getItemDet($data->item_id, $value = 0);
                                        },
                                        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                                    ),
                                    array(
                                        'name' => 'item_rate_id',
                                        'value'=>function($data){
                                          echo $data->getItemRate($data->item_rate_id);
                                        }
                                        
                                        
                                    ),
                                    array(
                                        'name' => 'item_count',
                                        //'value' => ''
                                    ),

                                    array(
                                      'name' => 'item_id',
                                      'headerHtmlOptions' => array('style' => 'display:none'),
                                      'htmlOptions' => array('style' => 'display:none', 'class' => 'rowId'),
                                      'filterHtmlOptions' => array('style' => 'display:none'),
                                      //'value' => ''
                                  ),
                                    array(
                                        'class' => 'CButtonColumn',
                                        // 'template' => isset(Yii::app()->user->role) && (in_array('/masters/area/update', Yii::app()->session['menuauthlist'])) ? '{update}' : '',

                                        'template'=>'{update}{delete}',


                                        'buttons' => array(
                                            'update' => array(
                                                'label' => '',
                                                'imageUrl' => false,
                                                 'click' => 'function(e){e.preventDefault();editConsumedItem(this,$(this).attr("href"));}',
                                                'options' => array('class' => 'update_milestone actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'
                                                
                                              ),
                                            ),

                                            'delete' => array(
                                                'label' => '',
                                                'imageUrl' => false,
                                               
                                                
                                                
                                                'click' => 'function(e){e.preventDefault();deleteConsumedItem(this,$(this).attr("href"));}',
                                                'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                                            ),
                                        ),
                                    ),
                                ),
                            ));
                            ?>
                   
                      
                   </div> <!-- cgrid end  -->


                   

              <div class="d-flex justify-content-between">
                    <div>
                      <!-- <button class="btn wpr-back-button">Back</button> -->
                    </div>
                    <div>
                      <button id="save_and_proceed_item" class="btn blue save-proceed-btn-style">Save & Proceed</button>
                    </div>
                  </div>