<?php 
$unit_code = '';
if ($index == 0) {
    ?>

<thead>
    <tr>
        <th>S.No.</th>
        <th>Project</th>
        <th>Task</th>
        <th>Quantity</th>
        <th>No.of workers</th>
        <th>Date</th>
        <?php if(Yii::app()->user->role != 10){ ?>
        <th>Created By</th>
        <?php }?>
    </tr>
</thead>
<?php
        }
        if(!empty($data->tasks->unit)){
            $unit_model = Unit::model()->findByPk($data->tasks->unit);
            if(!empty($unit_model)){
                $unit_code = $unit_model->unit_code;
            }
        }
        ?>
<tbody>
    <tr>
        <td class="width-40"><?php echo $index + 1; ?></td>
        <td><?php echo $data->project->name; ?></td>
        <td><?php echo $data->tasks->title; ?></td>
        <td><?php echo !empty($unit_code)?$data->qty.' ('.$unit_code.')':$data->qty; ?></td>
        <td><?php echo $data->getnoofworkers($data->taskid,$data->date); ?></td>
        <td><?php  echo date("d-M-y", strtotime($data->date)); ?></td>


        <?php if(Yii::app()->user->role != 10){ ?>

        <?php if(isset($data->createdBy)) { ?>
        <td><?php echo (isset($data->createdBy->first_name) ? $data->createdBy->first_name : "").(isset($data->createdBy->last_name) ? $data->createdBy->last_name : ""); ?>
        </td>
        <?php } else{?>
        <td></td>

        <?php  }} ?>

    </tr>
</tbody>