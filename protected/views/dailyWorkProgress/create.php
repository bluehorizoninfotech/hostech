<?php
/* @var $this DailyWorkProgressController */
/* @var $model DailyWorkProgress */

$this->breadcrumbs=array(
	'Daily Work Progresses'=>array('index'),
	'Create',
);
/*
$this->menu=array(
	array('label'=>'List DailyWorkProgress', 'url'=>array('index')),
	array('label'=>'Manage DailyWorkProgress', 'url'=>array('admin')),
); */
?>

<h1>Create DailyWorkProgress</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

