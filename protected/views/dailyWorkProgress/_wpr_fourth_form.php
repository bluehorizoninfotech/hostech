<?php
$model = new DailyWorkProgress();

if($WPRID != "")
{
  $model = DailyWorkProgress::model()->findByPk($WPRID); 
}
else
{
  if (Yii::app()->user->getState('wpr_id') != "") {
   
    $model = DailyWorkProgress::model()->findByPk(Yii::app()->user->getState('wpr_id'));
 }
}

 $status="";
 if (!empty($model->current_status)) {
    $status= isset($model->status0->caption)?$model->status0->caption:"";
  } else {
    if(isset($model->taskid))
    {
        $status= $model->getStatus($model->taskid);
    }
    
  }

  $total_qty ='';
  if(isset($model->taskid))
  {
    $task = Tasks::model()->findByPk($model->taskid);
    if($task)
    {
      $total_qty = isset($task->quantity)?$task->quantity:"";
    }
  }
  
?>
<div class="jeneral-review-head">
                    <h2 class="wpr-head">Work Progrees Overview</h2>
                  </div>
                  <div class="row margin-top-28">
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label>Project</label></div>
                      <div class="font-weight-normal"><label id="project"><?php echo isset($model->project->name)?$model->project->name:"" ?></label></div>
                    </div>
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label>Date</label></div>
                      <div class="font-weight-normal"><label id="date"><?php echo isset($model->date)? date("d-M-y", strtotime($model->date)):""?></label></div>
                    </div>
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label>Task</label></div>
                      <div class="font-weight-normal"><label id="task"><?php echo (isset($model->tasks)?$model->tasks->title:"") ?></label></div>
                    </div>
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label>Quantity</label></div>
                      <div class="font-weight-normal"><label id="quantity"><?php echo isset($model->qty)?$model->qty:""?></label></div>
                    </div>
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label>Work Type</label></div>
                      <div class="font-weight-normal"><label id="worktype"><?php echo (isset($model->worktype)?$model->worktype->work_type:"") ?></label></div>
                    </div>
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label> Status</label></div>
                      <div class="font-weight-normal"><label id="status"><?php echo $status ?></label></div>
                    </div>
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label>Daily Progress %</label></div>
                      <div class="font-weight-normal"><label id="progress"><?php echo isset($model->daily_work_progress)?round($model->daily_work_progress, 2) ." %":""?></label></div>
                    </div>
                    <div class="subrow col-md-3 col-sm-6 form-group">
                      <div class="font-weight-600"><label>Total Quantity</label></div>
                      <div class="font-weight-normal"><label id="tot_qty"><?php echo  $total_qty ?></label></div>
                    </div>

                    <!-- <div class="subrow col-md-3 col-sm-6 form-group">
                    <div class="font-weight-600"><label>Image</label></div>
                    <div class="font-weight-normal" id="image_div"></div>
                    </div> -->

                    <div class="subrow col-md-6 col-sm-6 form-group">
                      <div class="font-weight-bold"><label>Remarks</label></div>
                     
                      <div>
                        <textarea class="wpr-default-text-area" id="remark"><?php echo isset($model->description)?$model->description:"";?> </textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="d-flex wpr-border-bottom">
                        <div class="progress-table-head">
                          <h2 class="wpr-head">Item Consumption</h2>
                        </div>
                      </div>
                      
                    
                      <div class="work-progress-table">
                        
                      <?php 
                  
                  $consumedItemModel = new WprItemUsed('search');

                  if($WPRID != "")
                  {
                    $consumedItemModel->wpr_id =$WPRID;  
                  }
                  else
                  {
                    if (Yii::app()->user->getState('wpr_id') != "") 
                    { 
  
                      $consumedItemModel->wpr_id = Yii::app()->user->getState('wpr_id');
                    }
                  }
                 
                 
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'item-used-grid-one',
                        'dataProvider' => $consumedItemModel->search(),
                        'itemsCssClass' => 'table table-bordered',
                        'pager' => array(
                            'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                            'nextPageLabel' => 'Next '
                        ),
                        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                        'columns' => array(
                            array('class' => 'IndexColumn', 'header' => 'S.No.'),
                            array(
                                'name' => 'item_id',
                                'value' => function ($data) {

                                  echo $data->getItemDet($data->item_id, $value = 0);
                                },
                                'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                            ),
                            array(
                                'name' => 'item_rate_id',
                                'value'=>function($data){
                                  echo $data->getItemRate($data->item_rate_id);
                                }
                                
                                
                            ),
                            array(
                                'name' => 'item_count',
                                //'value' => ''
                            ),

                            array(
                              'name' => 'item_id',
                              'headerHtmlOptions' => array('style' => 'display:none'),
                              'htmlOptions' => array('style' => 'display:none', 'class' => 'rowId'),
                              'filterHtmlOptions' => array('style' => 'display:none'),
                              //'value' => ''
                          ),
                            
                        ),
                    ));
                  
                  ?>
                           
                      </div>

                    </div>
                    <div class="col-md-6">
                      <div class="d-flex wpr-border-bottom">
                        <div class="progress-table-head">
                          <h2 class="wpr-head">Resources Utillised</h2>
                        </div>
                      </div>
                      <!-- </div> -->
                      <div class="work-progress-table">
                        <?php
                      $resourceModel = new WprResourceUsed('search');
                      if($WPRID != "")
                      {
                        $resourceModel->wpr_id = $WPRID;
                      }
                      else
                      {
                        if (Yii::app()->user->getState('wpr_id') != "") {
                          $resourceModel->wpr_id = Yii::app()->user->getState('wpr_id');
                         }
                      }
                      

                  $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'resource-used-grid-one',
                    'dataProvider' => $resourceModel->search(),
                    'itemsCssClass' => 'table table-bordered',
                    'pager' => array(
                        'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                        'nextPageLabel' => 'Next '
                    ),
                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                    'columns' => array(
                        array('class' => 'IndexColumn', 'header' => 'S.No.'),
                        array(
                            'name' => 'resource_id',
                            'value' => function ($data) {
                                echo $data->getResourceName($data->resource_id);
                              },
                            
                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                        ),
                        array(
                            'name' => 'resource_unit',
                            'value' => function ($data) {
                                echo $data->getResourceUnit($data->resource_unit);
                              },
                            
                            
                        ),
                        array(
                            'name' => 'resource_qty',
                            //'value' => ''
                        ),

                        array(
                          'name' => 'resource_id',
                          'headerHtmlOptions' => array('style' => 'display:none'),
                          'htmlOptions' => array('style' => 'display:none', 'class' => 'rowId'),
                          'filterHtmlOptions' => array('style' => 'display:none'),
                          //'value' => ''
                      ),
                       
                    ),
                ));
                         ?>   
                      </div>
                    </div>
                  </div>
                  <div class="display-flex justify-content-between">
                    <div>
                      <!-- <button class="btn wpr-back-button">Back</button> -->
                    </div>
                    <div>
                      <button id="finish-btn" class="btn blue save-proceed-btn-style">Finish</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>