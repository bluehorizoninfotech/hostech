<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<style>
    .notice {
        position: relative;
    }

    .popup {
        position: absolute;
        /* transform: translate3d(590px, 1996px, 0px); */
        /* bottom: 0; */
        top: 56px;
        left: 0px;
        will-change: transform;
        transform: translate(-205px, -40px);
        width: 200px;
        background: #fafafa;
        padding: 10px;
        border: 1px solid #ddd;
        z-index: 1;

    }
</style>



<?php

$WPRID = isset($_GET['id']) ? $_GET['id'] : '';


$this->breadcrumbs = array(
    'Daily Work Progresses',
);
$settings = GeneralSettings::model()->find(['condition' => 'id = 1']);
if (Yii::app()->user->role != 1) {
    $criteria = new CDbCriteria;
    $criteria->select = 'project_id,tskid';
    $criteria->condition = ' parent_tskid is not null and (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ")";
    $criteria->group = 'project_id';
    $project_ids = Tasks::model()->findAll($criteria);
    $project_id_array = array();

    foreach ($project_ids as $projid) {
        array_push($project_id_array, $projid['project_id']);
    }
    if (!empty($project_id_array)) {
        $project_id_list = implode(',', $project_id_array);
    } else {
        $project_id_list = 'NULL';
    }
} else {
    $project_id_list = 'NULL';
}
?>
<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<div class="dailywork-sec wpr-tab-styles">
    <div class="clearfix daily-work-header">
        <h1 class="pull-left progresses-head">Daily Work Progresses</h1>
        <div class="pull-right">
            <?php

            if ((in_array('/tasks/monthlyTask', Yii::app()->session['menuauthlist']))) {
                echo CHtml::link('Monthly Task', array('tasks/monthlyTask'), array('class' => 'btn btn-primary btn-sm monthly-task-btn'));
            }
            ?>
            <?php if (Yii::app()->user->role != 9) { ?>
                <!-- <button class="btn btn-primary btn-sm addworkprogressbtn">Add Daily Work Progress</button> -->

                <?php
                echo CHtml::link('Add Daily Work Progress', array('DailyWorkProgress/dailyProgress', 'type' => 1), array('class' => 'btn btn-primary btn-sm add-daily-work-progress-btn'));
                ?>


                <button class="btn btn-primary btn-sm mytaskbtn">My Task</button>
                <button class="btn btn-primary btn-sm closemytaskbtn">Close My Task</button>
            <?php } ?>
            <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/themes/btheme2/assets/admin/layout3/scripts/autocomplete.js', CClientScript::POS_END); ?>
            <a href="<?= Yii::app()->createUrl("PhotoPunch/default/index", array(), 'https') ?>"
                class="btn btn-primary btn-sm photo-punch-btn ">Photo Punch</a>
        </div>
    </div>

    <?php

    Yii::app()->clientScript->registerScript('myjquery', " 
  
    $(document).ready(function () {
        $( '.reset' ).click(function() {
        location.href = '" . Yii::app()->createAbsoluteUrl('DailyWorkProgress/index') . "';
    });

    $('.addworkprogressbtn123').click(function(){
        $('.work_progress_form').slideDown();
    });

    $('.close_work_progress').click(function(){
        $('#daily-work-progress-form').reset();
        $('#task_id_span1').html('');
        $('red').text('');
        
        $('.work_progress_form').slideUp();
    });
      
    $('.info').fadeOut('3000');

  $( '.img_comp_class').keyup(function() {

     // $(function(){
      var projectid = $('#DailyWorkProgress_project_id').val();
      //alert(projectid);
      if(projectid == '')
      {
    //alert('Please Select Project');
    //$('#DailyWorkProgress_project_id_em_').css({display:block});
   
        }else
        {
        //   $.ajax({
        // 		type: 'GET',
        // 		dataType: 'text',
        // 		url:'" . Yii::app()->createUrl("DailyWorkProgress/getitems") . "',
        // 		data:{projectid:projectid},
        // 		success:function(data){
        // 		//alert(data);
// 		console.log(data);
        // 		var arrResponse = JSON.parse(data);
        // 		test(arrResponse);
                        
        // 				 },
        // 	   });
        //});
        function test(data){
                var items = data;
                console.log(items);
                console.log(typeof items);
                
            $('.img_comp_class').autocomplete({
            source: items,
            select: function(event,ui) {
            $('#autocomplete_hid').val(ui.item.label);
            $('#autocomplete_hid_dataval').val($(this).attr('data-val'));
            }
            });
            }
        }
        });
        
 $('#DailyWorkProgress_item_id').change(function () {
        var details = $('#DailyWorkProgress_item_id').val();
   
                var arr = details.split(',');
                var fst = arr.splice(0,1).join('');
                var rest = arr.join(',');
                $('#DailyWorkProgress_item_id').val(fst);
                $('.details_item').html(rest);
                $('#newdetails').val(rest);
                //alert(fst);
        });
  });
 ");
    ?>

    <!-- Add tools Popup -->
    <div class="index-dailyworkprogress-sec">
        <div id="addproject" class="modal" role="dialog">
            <div class="modal-dialog modal-lg">
            </div>
        </div>
        <!-- progress bar start  -->
        <div class="form-group display-none" id="process">
            <div class="progress">

                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0"
                    aria-valuemax="100">

                </div>

            </div>

        </div>
        <!-- progress bar end -->





        <?php
        if (Yii::app()->user->role != 9) {
            ?>




            <div class="panel panel-default work_progress_form">
                <div class="panel-heading clearfix">
                    <a class="close_work_progress pull-right"><b>X</b></a>
                    <h3>Add Work Progress</h3>
                </div>
                <div classs="center-content">
                    <div class="wpr-alert-wrapper">
                        <div>
                            <p id="success_message_div" class="font-15  green-color font-weight-bold"></p>
                        </div>
                        <div class="alert wpr-schedule-alert" id="alertMessageDiv"></div>
                    </div>
                    <!-- new design for wpr page -->
                    <div class="panel-body">
                        <div class="long-form combined-form tab-responsive">
                            <div class="tabs center-content">
                                <ul class="nav nav-pills">
                                    <li id="step_one" class="active"><span class="dot"></span><a data-toggle="tab"
                                            href="#wpr-step1-info">Step 1</a></li>
                                    <li id="step_two"><span class="dot"></span><a data-toggle="tab" href="#wpr-step2-info"
                                            class="">Step 2</a></li>
                                    <li id="step_three"><span class="dot"></span><a data-toggle="tab" href="#wpr-step3-info"
                                            class="wpr-tabs-3">Step 3</a></li>
                                    <li id="step_four"><span class="dot"></span><a data-toggle="tab"
                                            href="#wpr-step4-info">Step 4</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">


                                <div id="wpr-step1-info" class="tab-pane  fade active in">

                                    <?php
                                    //    $form = $this->beginWidget('CActiveForm', array(
                                    //   'id' => 'daily-work-progress-form',
                                    //   'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                    //   //'action' => Yii::app()->createUrl('/dailyWorkProgress/create'),
                                    //   'enableAjaxValidation' => true,
                                    //   'enableClientValidation' => true,
                                    //   'clientOptions' => array(
                                    //     'validateOnChange' => true,
                                    //     'validateOnType' => true,
                                    //   ),
                                    // ));
                                    ?>
                                    <?php
                                    $form = $this->beginWidget('CActiveForm', array(
                                        'htmlOptions' => array('class' => 'form-inline', ),
                                        'htmlOptions' => array('class' => 'padding-left-20 padding-right-20', 'enctype' => 'multipart/form-data'),
                                        'id' => 'wpr-add-form',
                                        'enableAjaxValidation' => true,
                                        'clientOptions' => array(
                                            'validateOnSubmit' => true,
                                            'afterValidate' => 'js:function(form, data, hasError) {
                            if(hasError) {    
                               // wprFormSubmit(form, data, hasError                          
                            }
                            else
                            {
                            // return true;  it will submit default way
                            wprFormSubmit(form, data, hasError); //and it will submit by ajax function
                               }
                           }',
                                        ),
                                    )
                                    );
                                    ?>
                                    <div class="jeneral-review-head">
                                        <h2 class="font-14 margin-bottom-0">General</h2>
                                    </div>
                                    <div class="row margin-top-28">
                                        <input type="hidden" name="newdetails" id="newdetails">
                                        <div class="subrow col-md-3 col-sm-6 form-group">



                                            <?php echo $form->hiddenField($model, 'id', array('class' => 'form-control', 'autocomplete' => 'off')); ?>


                                            <?php echo $form->labelEx($model, 'project_id'); ?>
                                            <?php
                                            if (Yii::app()->user->role == 1) {
                                                $condition = array('select' => array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
                                            } else {
                                                $criteria = new CDbCriteria;
                                                $criteria->select = 'project_id';
                                                $criteria->condition = ' parent_tskid is not null and (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ")";
                                                $criteria->group = 'project_id';
                                                $project_ids = Tasks::model()->findAll($criteria);
                                                $project_id_array = array();
                                                foreach ($project_ids as $projid) {
                                                    array_push($project_id_array, $projid['project_id']);
                                                }
                                                if (!empty($project_id_array)) {
                                                    $project_id_array = implode(',', $project_id_array);
                                                    $condition = array('select' => array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
                                                } else {
                                                    $condition = array('select' => array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'order' => 'name ASC');
                                                }
                                            }

                                            if (Yii::app()->user->project_id != "") {
                                                $project_id = Yii::app()->user->project_id;
                                            }
                                            $project = Projects::model()->findAll($condition);
                                            $data_array = array();
                                            if (count($project) == 1) {
                                                foreach ($project as $key => $value) {
                                                    array_push($data_array, $value->pid);
                                                }
                                                $id = implode(" ", $data_array);
                                                $p_data = Projects::model()->findByPk($id);
                                                ?>
                                                <select class="form-control" name="DailyWorkProgress[project_id]"
                                                    id="DailyWorkProgress_project_id">
                                                    <option value="<?php echo $p_data['pid']; ?>" <?php echo ($project_id == $p_data['pid']) ? 'selected' : ""; ?>>
                                                        <?php echo $p_data['name']; ?>
                                                    </option>
                                                </select>
                                                <?php
                                            } else {
                                                echo $form->dropDownList(
                                                    $model,
                                                    'project_id',
                                                    CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'),
                                                    array('empty' => '-Choose a Project-', 'class' => 'form-control')
                                                );
                                            }
                                            ?>
                                            <?php echo $form->error($model, 'project_id'); ?>
                                        </div>
                                        <div class="subrow col-md-3 col-sm-6 form-group">
                                            <?php echo $form->labelEx($model, 'date'); ?>
                                            <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control date', 'autocomplete' => 'off', )); ?>
                                            <?php echo $form->error($model, 'date'); ?>
                                            <div class="date_section" id="test_sample"></div>
                                        </div>
                                        <div class="subrow col-md-3 col-sm-6 form-group">
                                            <?php echo $form->labelEx($model, 'taskid'); ?>
                                            <?php
                                            $tasks = array();
                                            $work_type = "";
                                            $tsk_id = "";
                                            $quantity = "";
                                            $unit = "";
                                            if ($WPRID != "") {
                                                //$tasks =array();
                                                $tasks = Tasks::model()->findAll(array('condition' => 'tskid =' . $model->taskid));
                                            } else {
                                                if (Yii::app()->user->project_id != "") {
                                                    if (Yii::app()->user->role != 1) {
                                                        $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND  parent_tskid is not null and  project_id=' . Yii::app()->user->project_id . ' AND (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')', 'order' => 'title'));
                                                    } else {
                                                        $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND  parent_tskid is not null and  project_id=' . Yii::app()->user->project_id, 'order' => 'title'));
                                                    }
                                                } else {
                                                    $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1  AND   parent_tskid is not null and  (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')', 'order' => 'title'));
                                                }
                                            }

                                            $data_array = array();
                                            $tsk_id = "";
                                            $work_type = "";
                                            if (count($tasks) == 1) {
                                                foreach ($tasks as $key => $value) {
                                                    array_push($data_array, $value->tskid);
                                                }
                                                $tsk_id = implode(" ", $data_array);
                                                $task = Tasks::model()->findByPk($tsk_id);
                                                $work_type = $task->work_type_id;
                                                $tsk_id = $tsk_id;
                                                $quantity = $task->quantity;
                                                if ($task->unit != 0) {
                                                    $units_data = Unit::model()->findByPk($task->unit);
                                                    $unit = $units_data->unit_code;
                                                }
                                            }
                                            ?>
                                            <span id="task_id_span1">
                                                <?php echo $tsk_id; ?>
                                            </span>
                                            <select class="form-control taskid" name="DailyWorkProgress[taskid]"
                                                id="DailyWorkProgress_taskid">
                                                <option value="">-Choose a Task-</option>
                                                <?php
                                                if (!empty($tasks)) {
                                                    foreach ($tasks as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value['tskid']; ?>" <?php echo ($value['tskid'] == $tsk_id) ? "selected" : ""; ?>>
                                                            <?php echo ($value['parent_tskid'] != null ? ' - ' : '') ?>
                                                            <?php echo $value['title']; ?>
                                                        </option>
                                                    <?php }
                                                } ?>
                                            </select>
                                            <span class="details_item">
                                                <?php echo ((isset($itemdetails)) ? $itemdetails : ''); ?>
                                            </span>
                                            <?php echo $form->error($model, 'taskid'); ?>
                                        </div>


                                        <div class="col-md-3 col-sm-3 form-group">
                                            <?php echo $form->labelEx($model, 'qty'); ?>





                                            <?php echo $form->textField($model, 'qty', array(
                                                'class' => 'form-control img_comp_class progress_quantity',
                                                'autocomplete' => 'off',
                                                'name' => 'DailyWorkProgress[qty]',
                                                'id' => 'zero',
                                                'required' => 'required',

                                                'ajax' => array(
                                                    'url' => array('DailyWorkProgress/getprogress'),
                                                    'type' => 'POST',
                                                    'data' => array(
                                                        'qty' => 'js:this.value',
                                                        'item_total_qty' => 'js: $("#item_total_qty").val()',
                                                        'item_id' => 'js: $("#item_main_id").val()',
                                                        'scenario' => 'js: $("#scenario").val()',
                                                        'old_quantity' => 'js: $("#old_quantity").val()',
                                                        'wpr_taskid' => 'js: $("#wpr_taskid").val()',

                                                    ),
                                                    'dataType' => 'json',
                                                    'success' => 'function(data){
                           console.log(data);
                            if(data.progress==0){
                            $("#DailyWorkProgress_qty").val("0");
                            $("#DailyWorkProgress_daily_work_progress").val("0");                                
                            $("#DailyWorkProgress_qty_em_").css("display", "");
                            $("#DailyWorkProgress_qty_em_").text(data.msg);
                             setTimeout(function(){ location.reload(); }, 800);
                        }else{
                          
                            $("#DailyWorkProgress_daily_work_progress").val(data.progress);
                            }
                            if(data.balance_qty !== ""){
                               
                               $("#balance_qty").text("Balance quantity: "+data.balance_qty);
                               $("#balance_quantity_id").val(data.balance_qty);
                            }

                            var status=$("#DailyWorkProgress_current_status").val();
                            var balance_quantity=data.balance_qty;

                            if(status!="")
                            {
                              if(balance_quantity == 0 && status!=7)
                          {
                            $("#DailyWorkProgress_current_status").val("");
                            $("#DailyWorkProgress_current_status_em_").css("display", "");
                            $("#DailyWorkProgress_current_status_em_").text("If the balance quantity is 0 then the status should be completed");
                          }
                          if(balance_quantity!=0 && status==7)
                                  {
                                  
                                   $("#DailyWorkProgress_current_status").val("");
                                 $("#DailyWorkProgress_current_status_em_").css("display", "");
                                 $("#DailyWorkProgress_current_status_em_").text("The balance quantity should be 0 if the status is to be completed");
                                  }
                            }
       
                        }',
                                                )
                                            )
                                            ); ?>

                                            <?php
                                            if ($WPRID != "") { ?>
                                                <span id="balance_qty">Balance Quantity :
                                                    <?php echo $balanceQuantity ?>
                                                </span>

                                                <?php
                                            } else {
                                                ?>
                                                <span id="balance_qty"></span>
                                                <?php
                                            }
                                            ?>


                                            <?php echo $form->error($model, 'qty'); ?>
                                            <input type="hidden" id="balance_quantity_id">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="subrow col-md-3 col-sm-6 form-group">
                                            <?php echo $form->labelEx($model, 'work_type'); ?>
                                            <?php
                                            $condition = array('select' => array('wtid,work_type'), 'order' => 'work_type ASC');
                                            echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll($condition), 'wtid', 'work_type'), array('options' => array($work_type => array('selected' => true)), 'empty' => '-Choose a Work Type-', 'class' => 'form-control'));
                                            ?>
                                            <?php echo $form->error($model, 'work_type'); ?>
                                        </div>
                                        <div class="subrow col-md-3 col-sm-6 form-group">
                                            <?php echo $form->labelEx($model, 'current_status'); ?>
                                            <?php
                                            $condition_status = array('select' => array('sid,caption'), 'condition' => "status_type='task_status' AND sid NOT IN(6)", 'order' => 'caption ASC');
                                            echo $form->dropDownList($model, 'current_status', CHtml::listData(Status::model()->findAll($condition_status), 'sid', 'caption'), array('empty' => '-Choose status-', 'class' => 'form-control'));
                                            ?>
                                            <?php echo $form->error($model, 'current_status'); ?>

                                        </div>
                                        <div class="col-md-3 col-sm-6 form-group">
                                            <?php echo $form->labelEx($model, 'daily_work_progress'); ?>
                                            <?php echo $form->textField($model, 'daily_work_progress', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true)); ?>
                                            <?php echo $form->error($model, 'daily_work_progress'); ?>
                                        </div>
                                        <div class="col-md-3 col-sm-3 pos-rel form-group">
                                            <?php echo $form->labelEx($model, 'total_quantity'); ?>
                                            <?php echo $form->textField($model, 'total_quantity', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'value' => $quantity)); ?>
                                            <span id="unit_value">
                                                <?php echo $unit; ?>
                                            </span>
                                            <?php echo $form->error($model, 'total_quantity'); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 form-group">
                                            <?php echo $form->labelEx($model, 'remarks'); ?>
                                            <?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                            <?php echo $form->error($model, 'description'); ?>
                                        </div>

                                        <div class="col-md-3 col-sm-4 form-group">

                                            <?php
                                            $this->widget('CMultiFileUpload', array(
                                                'model' => $model,
                                                'name' => 'image',
                                                'attribute' => 'image',
                                                'accept' => 'jpg|gif|png|jpeg',
                                                'max' => 3,

                                                'duplicate' => 'Already Selected',
                                                'options' => array(
                                                    'onFileSelect' => 'function(e, v, m){ addLabel() }',

                                                ),
                                            )
                                            );
                                            ?>
                                            <span class="img_span"> (Image Size : 2048pxx1536px)</span>
                                        </div>
                                        <div id="label_container" class="col-md-2 col-sm-2 form-group"></div>


                                    </div>
                                    <div class="">
                                        <?php echo CHtml::hiddenField('item_description', '', array('id' => 'item_description')); ?>



                                        <input type="hidden" value="<?php echo $task_total_quantity ?>" id="item_total_qty">

                                        <input type="hidden" value="<?php echo $model->id ?>" id="scenario">

                                        <input type="hidden" value="<?php echo $model->qty ?>" id="old_quantity">



                                        <input type="hidden" value="<?php echo $model->taskid ?>" id="wpr_taskid">


                                        <?php echo CHtml::hiddenField('item_main_id', '', array('id' => 'item_main_id', 'value' => $tsk_id)); ?>
                                    </div>
                                    <?php echo $form->hiddenField($model, 'latitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "latitude")); ?>
                                    <?php echo $form->hiddenField($model, 'longitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "longitude")); ?>
                                    <?php echo $form->hiddenField($model, 'site_id', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_id")); ?>
                                    <?php echo $form->hiddenField($model, 'site_name', array('class' => 'form-control site_name', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_name")); ?>

                                    <div class="wpr-capture-btn-wrapper">
                                        <button type="button" class="btn btn-sm wpr-capture-btn">Capture Incidents</button>
                                    </div>
                                    <div class="incidents-capture-div margin-top-20 display-none" id="capure_incident">
                                        <div class="row">
                                            <?php if (in_array('/dailyWorkProgress/incident', Yii::app()->session['menuauthlist'])) { ?>
                                                <div class="form-group col-md-3">
                                                    <div><label for="#">Incidents</label><span class="required"></span></div>
                                                    <div>
                                                        <select class="form-control" name="DailyWorkProgress[incident]"
                                                            id="incident_id">
                                                            <option value="">Choose Incident</option>
                                                            <option value="1">Accidents </option>
                                                            <option value="2">Injuries </option>
                                                            <option value="3">Unusual Events </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (in_array('/dailyWorkProgress/inspection', Yii::app()->session['menuauthlist'])) { ?>
                                                <div class="form-group col-md-3">
                                                    <div><label for="#">Test/Inspection</label></div>
                                                    <div>
                                                        <input type="text" class="form-control"
                                                            name="DailyWorkProgress[inspection]"
                                                            value="<?php echo $model->inspection ?>">
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (in_array('/dailyWorkProgress/siteVisit', Yii::app()->session['menuauthlist'])) { ?>
                                                <div class="form-group col-md-3">
                                                    <div><label for="#">Visitor Name</label></div>
                                                    <div>
                                                        <input type="text" class="form-control"
                                                            name="DailyWorkProgress[visitor]"
                                                            value="<?php echo $model->visitor_name ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div><label for="#">Company</label></div>
                                                    <div>
                                                        <input type="text" class="form-control"
                                                            name="DailyWorkProgress[company]"
                                                            value="<?php echo $model->company_name ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div><label for="#"></label>Designation</div>
                                                    <div>
                                                        <input type="text" class="form-control"
                                                            name="DailyWorkProgress[designation]"
                                                            value="<?php echo $model->designation ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-9">
                                                    <div><label for="#">Purpose</label></div>
                                                    <div>
                                                        <input type="text" class="form-control"
                                                            name="DailyWorkProgress[purpose]"
                                                            value="<?php echo $model->purpose ?>">
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-end margin-top-32">
                                        <button class="btn  save-proceed-btn-style">Save & Proceed</button>
                                    </div>
                                </div>
                                <?php $this->endWidget(); ?>
                                <!-- step1 end -->
                                <!-- step2 start -->
                                <div id="wpr-step2-info" class="tab-pane center-content fade margin-top-28">

                                    <?php echo $this->renderPartial('_wpr_second_form', array('model' => $model, 'WPRID' => $WPRID)); ?>

                                </div>
                                <!-- step2 end -->
                                <!-- step3 start -->
                                <div id="wpr-step3-info" class="tab-pane center-content fade margin-top-28">

                                    <?php echo $this->renderPartial('_wpr_third_form', array('resource_utilised' => $resource_utilised, 'WPRID' => $WPRID)); ?>

                                </div>
                                <!-- step3 end -->
                                <!-- step4 start -->
                                <div id="wpr-step4-info" class="tab-pane fade">

                                    <?php echo $this->renderPartial('_wpr_fourth_form', array('model' => $model, 'WPRID' => $WPRID)) ?>
                                </div>
                                <!-- step4 End -->
                            </div>
                        </div>
                    </div>
                </div>

                <?php
        }
        ?>

            <div id="wpr_form_div" class="" style="display:none1">

            </div>


            <!-- my task table -->
            <div id="mytasks">
                <?php
                $type = "";
                $model1 = new Tasks;
                $this->renderPartial('wprmytask', array(
                    'model' => $model1
                )
                );

                ?>
            </div>
            <!-- end my task table  -->





            <div class="clearfix table-filter">
                <div class="prev_curr_next pull-left sec_h">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'page-form',
                        'method' => 'GET',
                        'enableAjaxValidation' => true,
                        'htmlOptions' => array('class' => 'form-inline')
                    )
                    ); ?>
                    <div class="form-group datepick">
                        <?php echo $form->label($model, 'date'); ?>
                        <?php echo CHtml::activeTextField($model, 'fromdate', array('class' => 'form-control height-28', 'size' => 10, 'value' => isset($_REQUEST['DailyWorkProgress']['fromdate']) ? $_REQUEST['DailyWorkProgress']['fromdate'] : '')); ?>
                        <?php
                        $this->widget('application.extensions.calendar.SCalendar', array(
                            'inputField' => 'DailyWorkProgress_fromdate',
                            'ifFormat' => '%d-%m-%Y',
                        )
                        );
                        ?> to
                        <?php echo CHtml::activeTextField($model, 'todate', array('class' => 'form-control height-28', 'size' => 10, 'value' => isset($_REQUEST['DailyWorkProgress']['todate']) ? $_REQUEST['DailyWorkProgress']['todate'] : '')); ?>
                        <?php
                        $this->widget('application.extensions.calendar.SCalendar', array(
                            'inputField' => 'DailyWorkProgress_todate',
                            'ifFormat' => '%d-%m-%Y',
                        )
                        );
                        ?>

                    </div>

                    <div class="form-group min-btn">
                        <?php echo CHtml::submitButton('Go', array('class' => 'btn blue btn-sm')); ?>
                        <?php echo CHtml::Button('Clear', array('class' => 'btn btn-default btn-sm reset')); ?>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
                <div class="pull-right sec_h">
                    <div class="nav-div">
                        <?php
                        $sql = 'select `approve_status`, '
                            . 'count(approve_status) as itemcount '
                            . 'from pms_daily_work_progress ';
                        if (Yii::app()->user->role != 1) {
                            if ($interval != "0" && !empty($project_id_list)) {
                                $sql .= 'where date="' . date("Y-m-d", strtotime($interval)) . '"  AND  project_id IN (' . $project_id_list . ')';
                            } elseif ($interval != "0") {
                                $sql .= 'where date="' . date("Y-m-d", strtotime($interval)) . '"';
                            } elseif (!empty($project_id_list)) {
                                $sql .= ' where  project_id IN (' . $project_id_list . ')';
                            }
                        } else {
                            if ($interval != "0") {
                                $sql .= 'where date="' . date("Y-m-d", strtotime($interval)) . '"';
                            }
                        }
                        $sql .= 'group by `approve_status`';
                        $status_count = CHtml::listData(Yii::app()->db->createCommand($sql)->queryAll(), 'approve_status', 'itemcount');
                        $filterarray = array('All' => 'All', 'Approved' => 'Approved', 'Pending' => 'Pending', 'Rejected' => 'Rejected');
                        $statusarray = array('All' => 'All', 1 => 'Approved', 0 => 'Pending', 2 => 'Rejected');
                        echo '<ul class="nav1 nav new-index-wpr">';
                        $activeitem = ((!isset($_GET['type']) or $_GET['type'] == 'All') ? 'All' : intval($_GET['type']));
                        $status_count['All'] = array_sum($status_count);
                        $statusbgcolor = array('Red' => 'ALL', 'green' => 1, 'blue' => 0, '#715d13' => 2);
                        foreach ($statusarray as $k => $status) {
                            echo '<li class="nav-item loaddiv">';
                            echo CHtml::link(
                                $status
                                . '&nbsp;&nbsp;<span class="badge" style="background-color: ' . array_search($k, $statusbgcolor) . '">'
                                . (isset($status_count[$k]) ? $status_count[$k] : 0)
                                . '</span>',
                                array('/DailyWorkProgress/wpr', 'type' => $k, 'interval' => $interval),
                                array('class' => "nav-link rounded-5" . ($k === $activeitem ? ' active ' : ''), 'aria-disabled' => "true")
                            );
                            echo '</li>';
                        }
                        echo '</ul>';
                        ?>
                    </div>
                </div>
            </div>

            <?php
            if (!empty($_GET['interval'])) {
                $date = $_GET['interval'];
            } else {
                $date = "";
            }
            ?>
            <p id="success_message" style="display:none;" class="font-15 green-color font-weight-bold">Successfully
                created..</p>

            <div class="alert display-none" id="alertMessage">


                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            </div>
            <div id='actionmsg'></div>


            <?php if (in_array('/dailyWorkProgress/rejectentry', Yii::app()->session['menuauthlist'])) {
                ?>
                <div class="approve_form">
                    <div>
                        <textarea name="reason" class="reason"></textarea>

                        <input type="button" class='requestaction btn btn-danger' name='reject' value='Reject' />
                        <?php
                        echo CHtml::image(
                            Yii::app()->request->baseUrl . '/images/loading.gif',
                            'Loading....',
                            array('class' => 'width-30 margin-left-159 display-none')
                        );
                        ?>
                    </div>
                </div>
            <?php }
            ?>

            <div id="boq_entries">
                <?php
                if (Yii::app()->user->role == 1) {
                    $project_dropdown_1 = CHtml::listData(Projects::model()->findAll(
                        array(
                            'select' => array('pid,name'),
                            'order' => 'name',
                            'distinct' => true
                        )
                    ), "pid", "name");
                } else {
                    $project_dropdown_1 = CHtml::listData(Projects::model()->findAll(
                        array(
                            'select' => array('pid,name'),
                            'condition' => 'pid IN (' . $project_id_list . ')',
                            'order' => 'name',
                            'distinct' => true
                        )
                    ), "pid", "name");
                }

                $null_dropdown = array('0' => 'All');
                $projects_list = $project_dropdown_1 + $null_dropdown;
                ksort($projects_list);



                if (Yii::app()->user->role == 1) {
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'daily-work-progress-grid',
                        'htmlOptions' => array('class' => 'grid-view'),
                        'itemsCssClass' => 'table table-bordered progress-table',
                        'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',

                        'pager' => array(
                            'id' => 'dataTables-example_paginate',
                            'header' => '',
                            'prevPageLabel' => 'Previous ',
                            'nextPageLabel' => 'Next '
                        ),
                        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                        'dataProvider' => $model->search($interval, $selected_status),
                        'ajaxUpdate' => false,
                        'filter' => $model,
                        'selectableRows' => 2,
                        'columns' => array(
                            array(
                                'id' => 'selected_tasks',
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => '50',

                            ),
                            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'), ),
                            array(
                                'htmlOptions' => array('class' => 'white-space-nowrap'),
                                'class' => 'ButtonColumn',
                                'template' => '{view}{approve_btn}{reject_btn}{recall}{Update}{approve_consumed_item}{delete_wpr}',
                                'evaluateID' => true,

                                'buttons' => array(
                                    'view' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View', 'id' => '$data->id'),
                                    ),
                                    'approve_btn' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/approveentry", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'fa fa-thumbs-up approve', 'title' => 'Approve', 'id' => '$data->id'),
                                    ),
                                    'reject_btn' => array(
                                        'label' => '',
                                        'imageUrl' => false,

                                        'visible' => '($data->approve_status=="0" ) && ($data->tasks->status != "7") && in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',

                                        // 'url' => 'Yii::app()->createUrl("dailyWorkProgress/approverejectdata", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid","stat"=>2))',
                
                                        // 'click' => 'function(e){e.preventDefault();$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                
                                        // 'options' => array('class' => 'fa fa-thumbs-down margin_5 reject', 'title' => 'Reject'),
                
                                        'options' => array('class' => 'fa fa-thumbs-down margin_5 reject reject-entry', 'title' => 'Reject', 'id' => '$data->id'),
                                    ),
                                    'recall' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'visible' => '($data->approve_status=="2")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'fa fa-rotate-left icon-comn margin_5 recall', 'title' => 'Recall', 'id' => '$data->id'),
                                    ),

                                    'Update' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'url' => 'Yii::app()->createUrl("/dailyWorkProgress/wprUpdate", array("id"=>$data->id))',
                                        'visible' => '(in_array("/dailyWorkProgress/update", Yii::app()->session["menuauthlist"]) && ($data->created_by == ' . Yii::app()->user->id . ' || Yii::app()->user->role == 1) && $data->work_progress_type==1)',
                                        'options' => array('class' => 'icon-pencil  icon-comn margin_5 edit', 'title' => 'Edit', 'id' => '$data->id'),
                                    ),
                                    'approve_consumed_item' => array(
                                        'label' => '',
                                        'visible' => '($data->consumed_item_status=="0") && in_array("/dailyWorkProgress/approve_consumption_item", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'fa fa-check approve_consumed_item', 'id' => '$data->id'),
                                    ),
                                    'delete_wpr' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/delete", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'icon-trash icon-comn delete-wpr', 'title' => 'Delete', 'id' => '$data->id'),
                                    )
                                )

                            ),
                            array(
                                'name' => 'date',
                                'value' => 'date("d-M-y", strtotime($data->date))',

                            ),
                            array(
                                'name' => 'project_id',
                                'value' => '$data->project->name',
                                'filter' => CHtml::activeDropDownList($model, 'project_id', $projects_list)

                            ),
                            array(
                                'name' => 'taskid',
                                'value' => '(isset($data->tasks)?$data->tasks->title:"")',
                            ),
                            array(
                                'name' => 'qty',
                                'value' => function ($data) {
                                    if (!empty($data->tasks->unit)) {
                                        $unit = Unit::model()->findByPk($data->tasks->unit);
                                        echo $data->qty . ' (' . $unit->unit_code . ')';
                                    } else {
                                        echo $data->qty;
                                    }
                                },
                            ),
                            array(
                                'header' => 'Balance quantity',
                                'value' => function ($data) {
                                    echo $data->getbalanceqty($data->taskid, $data->id);
                                },
                            ),
                            array(
                                'name' => 'work_type',
                                'value' => '(isset($data->worktype)?$data->worktype->work_type:"")',
                            ),
                            array(
                                'name' => 'daily_work_progress',
                                'value' => function ($data) {
                                    echo round($data->daily_work_progress, 2);
                                },
                            ),
                            array(
                                'name' => 'current_status',
                                'value' => function ($data) {
                                    if (!empty($data->current_status)) {
                                        echo $data->status0->caption;
                                    } else {
                                        echo $data->getStatus($data->taskid);
                                    }
                                },
                                'filter' => CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'order' => 'caption',
                                        'distinct' => true,
                                        'condition' => 'status_type="task_status"'
                                    )
                                ), "sid", "caption")
                            ),
                            array(
                                'header' => 'Status',
                                'value' => function ($data) {
                                    if ($data->work_progress_type == 1) {
                                        return "Active";
                                    } else {
                                        return "Expired";
                                    }
                                },
                            ),
                            array(
                                'name' => 'approve_reject_data',
                                'value' => function ($data) {
                                    if ($data->approve_status != 1) {
                                        echo $data->approve_reject_data;
                                    }
                                },
                            ),
                            'description',
                            array(
                                'name' => 'created_by',
                                'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
                                'visible' => Yii::app()->user->role == 1 || Yii::app()->user->role == 9,
                                'filter' => CHtml::listData(Users::model()->findAll(
                                    array(
                                        'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                                        'order' => 'first_name',
                                        'distinct' => true
                                    )
                                ), "userid", "first_name")
                            ),
                            array(
                                'name' => 'approve_status',
                                'value' => function ($data) {
                                    if ($data->approve_status == 0) {
                                        return "Not Approved";
                                    } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 0) {
                                        return "Not Approved";
                                    } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 1) {
                                        return "Approved";
                                    } elseif ($data->approve_status == 2) {
                                        return "Rejected";
                                    } else {
                                        return "";
                                    }
                                },
                                'filter' => CHtml::listData($model->getApproveStatuses(), 'id', 'title'),
                            ),

                            array(
                                'name' => 'consumed_item_id',
                                'value' => function ($data) {

                                    echo $data->getItemDet($data->id, $value = 0);
                                },
                                'filter' => false,
                            ),

                            array(
                                'name' => 'consumed_item_count',
                                'value' => function ($data) {
                                    echo '<div id="' . $data->id . '" class="consumed-item">' . $data->getItemDet($data->id, $value = 1) . '</div>
                                <div class="popup"></div>';
                                },

                                'filter' => false,
                                'cssClassExpression' => '$data->consumed_item_status==0 && $data->consumed_item_status!="" ? "notice" : ""',

                            ),


                        ),
                    )
                    );
                } else {
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'daily-work-progress-grid',
                        'htmlOptions' => array('class' => 'grid-view'),
                        'itemsCssClass' => 'table table-bordered progress-table',
                        'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',

                        'pager' => array(
                            'id' => 'dataTables-example_paginate',
                            'header' => '',
                            'prevPageLabel' => 'Previous ',
                            'nextPageLabel' => 'Next '
                        ),
                        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                        'dataProvider' => $model->search($interval, $selected_status),
                        'ajaxUpdate' => false,
                        'filter' => $model,
                        'selectableRows' => 2,
                        'columns' => array(

                            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'), ),
                            array(
                                'htmlOptions' => array('class' => 'white-space-nowrap'),
                                'class' => 'ButtonColumn',
                                'template' => '{view}{approve_btn}{reject_btn}{recall}{Update}{approve_consumed_item}{delete_wpr}',
                                'evaluateID' => true,

                                'buttons' => array(
                                    'view' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View', 'id' => '$data->id'),
                                    ),
                                    'approve_btn' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/approveentry", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'fa fa-thumbs-up approve', 'title' => 'Approve', 'id' => '$data->id'),
                                    ),
                                    'reject_btn' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'visible' => '($data->approve_status=="0" ) && ($data->tasks->status != "7") && in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                                        'url' => 'Yii::app()->createUrl("dailyWorkProgress/approverejectdata", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid","stat"=>2))',
                                        'click' => 'function(e){e.preventDefault();$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                                        'options' => array('class' => 'fa fa-thumbs-down margin_5 reject', 'title' => 'Reject'),
                                    ),
                                    'recall' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'visible' => '($data->approve_status=="2")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'fa fa-rotate-left icon-comn margin_5 recall', 'title' => 'Recall', 'id' => '$data->id'),
                                    ),

                                    'Update' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'url' => 'Yii::app()->createUrl("/dailyWorkProgress/wprUpdate", array("id"=>$data->id))',
                                        'visible' => '(in_array("/dailyWorkProgress/update", Yii::app()->session["menuauthlist"]) && ($data->created_by == ' . Yii::app()->user->id . ' || Yii::app()->user->role == 1))',
                                        'options' => array('class' => 'icon-pencil  icon-comn margin_5 edit', 'title' => 'Edit', 'id' => '$data->id'),
                                    ),
                                    'approve_consumed_item' => array(
                                        'label' => '',
                                        'visible' => '($data->consumed_item_status=="0") && in_array("/dailyWorkProgress/approve_consumption_item", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'fa fa-check approve_consumed_item', 'id' => '$data->id'),
                                    ),
                                    'delete_wpr' => array(
                                        'label' => '',
                                        'imageUrl' => false,
                                        'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/delete", Yii::app()->session["menuauthlist"])',
                                        'options' => array('class' => 'icon-trash icon-comn delete-wpr', 'title' => 'Delete', 'id' => '$data->id'),
                                    )
                                )

                            ),
                            array(
                                'name' => 'date',
                                'value' => 'date("d-M-y", strtotime($data->date))',

                            ),
                            array(
                                'name' => 'project_id',
                                'value' => '$data->project->name',
                                'filter' => CHtml::activeDropDownList($model, 'project_id', $projects_list)

                            ),
                            array(
                                'name' => 'taskid',
                                'value' => '(isset($data->tasks)?$data->tasks->title:"")',
                            ),
                            array(
                                'name' => 'qty',
                                'value' => function ($data) {
                                    if (!empty($data->tasks->unit)) {
                                        $unit = Unit::model()->findByPk($data->tasks->unit);
                                        echo $data->qty . ' (' . $unit->unit_code . ')';
                                    } else {
                                        echo $data->qty;
                                    }
                                },
                            ),

                            array(
                                'header' => 'Balance quantity',
                                'value' => function ($data) {
                                    echo $data->getbalanceqty($data->taskid, $data->id);
                                },
                            ),





                            array(
                                'name' => 'work_type',
                                'value' => '(isset($data->worktype)?$data->worktype->work_type:"")',
                            ),
                            array(
                                'name' => 'daily_work_progress',
                                'value' => function ($data) {
                                    echo round($data->daily_work_progress, 2);
                                },
                            ),
                            array(
                                'name' => 'current_status',
                                'value' => function ($data) {
                                    if (!empty($data->current_status)) {
                                        echo $data->status0->caption;
                                    } else {
                                        echo $data->getStatus($data->taskid);
                                    }
                                },
                                'filter' => CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'order' => 'caption',
                                        'distinct' => true,
                                        'condition' => 'status_type="task_status"'
                                    )
                                ), "sid", "caption")
                            ),
                            array(
                                'header' => 'Status',
                                'value' => function ($data) {
                                    if ($data->work_progress_type == 1) {
                                        return "Active";
                                    } else {
                                        return "Expired";
                                    }
                                },
                            ),
                            array(
                                'name' => 'approve_reject_data',
                                'value' => function ($data) {
                                    if ($data->approve_status != 1) {
                                        echo $data->approve_reject_data;
                                    }
                                },
                            ),
                            'description',
                            array(
                                'name' => 'created_by',
                                'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
                                'visible' => Yii::app()->user->role == 1 || Yii::app()->user->role == 9,
                                'filter' => CHtml::listData(Users::model()->findAll(
                                    array(
                                        'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                                        'order' => 'first_name',
                                        'distinct' => true
                                    )
                                ), "userid", "first_name")
                            ),
                            array(
                                'name' => 'approve_status',
                                'value' => function ($data) {
                                    if ($data->approve_status == 0) {
                                        return "Not Approved";
                                    } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 0) {
                                        return "Not Approved";
                                    } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 1) {
                                        return "Approved";
                                    } elseif ($data->approve_status == 2) {
                                        return "Rejected";
                                    } else {
                                        return "";
                                    }
                                },
                                'filter' => CHtml::listData($model->getApproveStatuses(), 'id', 'title'),
                            ),

                            array(
                                'name' => 'consumed_item_id',
                                'value' => function ($data) {

                                    echo $data->getItemDet($data->id, $value = 0);
                                },
                                'filter' => false,
                            ),

                            // array(
                            //     'name' => 'consumed_item_count',
                            //     'value' => function ($data) {
                            //         echo $data->getItemDet($data->id, $value = 1);
                            //     },
                            //     'filter' => false,
                            //     'cssClassExpression' => '$data->consumed_item_status==0 && $data->consumed_item_status!="" ? "notice" : ""',
                            //     'onmouseover'=>'functionName()',
                            // ),
                
                            array(
                                'name' => 'consumed_item_count',
                                'value' => function ($data) {
                                    echo '<div id="' . $data->id . '" class="consumed-item">' . $data->getItemDet($data->id, $value = 1) . '</div>
                                <div class="popup"></div>';
                                },

                                'filter' => false,
                                'cssClassExpression' => '$data->consumed_item_status==0 && $data->consumed_item_status!="" ? "notice" : ""',

                            ),


                           
                        ),
                    )
                    );
                }
                ?>
            </div>
        </div>
        <?php
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id' => 'cru-dialog',
            'options' => array(
                'title' => 'Reject BOQ Entry',
                'autoOpen' => false,
                'modal' => false,
                'width' => 590,
                'height' => "auto",
            ),
        )
        );
        ?>
        <iframe id="cru-frame" width="550" height="auto" frameborder="0" class="min-height-200"></iframe>
        <?php
        $this->endWidget();
        ?>
        <div id="view_progress" class="view_progress"></div>

        <div id='popup1'>
            <div class="popup-header clearfix">
                <h5 class="text-center margin-bottom-10">WPR Reject &nbsp;</h5>
                <span class="close-icon">&times;</span> <!-- Close icon (X symbol) -->
                <div class="approve-reject-data-sec pending-req">
                    <div class="form">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'daily-progress-form',
                            'enableAjaxValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'validateOnType' => false,
                            ),
                        )
                        );
                        ?>
                        <div class="row">
                            <div class="subrow">
                                <input type="hidden" id="work_progress_id">
                                <?php echo $form->labelEx($model, 'approve_reject_data'); ?>
                                <?php echo $form->textArea($model, 'approve_reject_data', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'approve_reject_data'); ?>
                            </div>
                            <?php echo $form->hiddenField($model, 'id'); ?>
                        </div>
                        <div class="row">
                            <?php echo CHtml::submitButton('Reject', array('class' => 'btn btn-sm blue', 'id' => 'rejectentry')); ?>
                            <?php echo CHtml::resetButton('Cancel', array('class' => 'btn btn-sm default cancel')); ?>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>



        <!--  -->
        <div id="pop">

        </div>
        <!--  -->



    </div>

</div>
<script>
    $('#daily-work-progress-form').on('beforeSubmit', function (e) {
        $('#submit_boq').css('disable', 'disable');
        return false;
    });

    $(function () {
        $("#datepicker").datepicker({
            onSelect: function (selectedDate) {
                var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/index&interval='); ?>' + selectedDate;
                $(location).attr('href', url);
            }
        });
    });
    $("#datepicker").change(function () {
        date = $(this).val();
        if (date == "") {
            var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/index'); ?>';
            $(location).attr('href', url);
        }
    })
</script>
<script>
    $(document).ready(function () {
        $('.approve_form').hide();
        var date1 = '<?php echo $settings->backlog_days; ?>';
        $('#DailyWorkProgress_date').datepicker({
            dateFormat: 'dd-M-y',
            minDate: -date1,
            maxDate: 0,
            onSelect: function (selectedDate) {
                var d = new Date(selectedDate);
                var date = this.value;
                entrydatevalidation(date);
                $("#DailyWorkProgress_date_em_").hide();
            }
        });
        $(document).on('click', ".approve", function () {
            if (!confirm("Do you want to Approve ?")) { } else {
                $('.loaderdiv').show();
                var entry_id = this.id;
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approveentry'); ?>',
                    data: {
                        entry_id: entry_id
                    },
                    method: "GET",
                    dataType: 'json',
                    success: function (result) {
                        if (result.status == '1') {
                            $("#boq_entries").load(location.href + " #boq_entries>*", "");
                            $('.alert').removeClass('hide');
                            $('.alert').css('display', 'block');
                            $('.alert').append(result.schedule);
                            $('.alert').append("<br/>");
                            $('.alert').append(result.dependant);
                            $('.loaderdiv').hide();
                            setTimeout(function () {
                                location.reload();
                            }, 1000);
                        } else {
                            $('.alert').removeClass('hide');
                            $('.alert').css('display', 'block');
                            $('.alert').append(result.message);
                            $('.loaderdiv').hide();
                        }

                    }
                })
            }
        });


        $(document).on('click', ".recall", function () {
            if (!confirm("Do you want to Recall ?")) { } else {
                $('.loaderdiv').show();
                var entry_id = this.id;
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/recall'); ?>',
                    data: {
                        entry_id: entry_id
                    },
                    method: "GET",
                    dataType: 'json',
                    success: function (result) {
                        if (result.status == '1') {
                            $("#boq_entries").load(location.href + " #boq_entries>*", "");
                            $('.loaderdiv').hide();
                        } else if (result.status == '2') {
                            $('.alert').removeClass('hide');
                            $('.alert').append(result.message);
                            $('.loaderdiv').hide();
                        }

                    }
                })
            }
        });

        function entrydatevalidation(date) {
            var project_id = $('#DailyWorkProgress_project_id').find(":selected").val();
            if (project_id !== '') {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/checkdate'); ?>',
                    data: {
                        date: date,
                        project_id: project_id
                    },
                    method: "POST",
                    dataType: 'json',
                    success: function (result) {
                        $('#DailyWorkProgress_taskid').html(result.html);
                        $('#DailyWorkProgress_taskid').val(result.tsk_id);
                        $('#DailyWorkProgress_work_type').html(result.work_type_html);
                        $('#DailyWorkProgress_work_type').val(result.work_type);
                        $('#task_id_span1').text(result.tsk_id);
                        $('#DailyWorkProgress_total_quantity').val(result.quantity);
                        $("#unit_value").html(result.unit);
                        $("#item_total_qty").val(result.quantity);
                        $("#item_main_id").val(result.tsk_id);

                        if (result.bal_qty_status == 1) {
                            $("#balance_qty").text("Balance quantity: " + result.bal_qty_count);
                            $("#balance_quantity_id").val(result.bal_qty_count);
                        }


                    }
                });
            }
        }
        $('#selected_tasks_all').click(function () {
            var checkboxes = document.getElementsByTagName('input');
            if (this.checked) {
                $('.approve_form').slideDown();
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                        $('div.checker span').addClass('checked');
                    }
                }
            } else {
                $('.approve_form').slideUp();
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                        $('div.checker span').removeClass('checked');
                    }
                }
            }
        });
        $('input[name="selected_tasks[]"]').click(function () {
            var countchecked = $('input[name="selected_tasks[]"]:checked').length;
            if (countchecked >= 1) {
                $('.approve_form').slideDown();
            } else if (countchecked == 0) {
                $('.approve_form').slideUp();
            }
            if (this.checked) {
                $('.approve_form').slideDown();

            } else {
                var all = [];
                $("#selected_tasks_all").prop('checked', false);
                $('div#uniform-selected_tasks_all span').removeClass('checked');
            }
        });
        $(".requestaction").click(function () {
            $('.requestaction').attr('disabled', true);
            var req = $(this).val();
            var reason = $('.reason').val();
            var boq_ids = [];
            $('input[name="selected_tasks[]"]:checked').each(function () {
                boq_ids.push(this.value)
            });
            if (boq_ids != '') {
                if (reason == "") {
                    alert("please enter reason")
                    flag = 1;
                } else {
                    if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

                        $('.loaderdiv').show();
                        $.ajax({
                            method: "post",
                            dataType: "json",
                            data: {
                                ids: boq_ids,
                                req: req,
                                reason: reason,
                            },
                            url: '<?php echo Yii::app()->createUrl("/dailyWorkProgress/bulkReject") ?>',
                            success: function (ret) {
                                $('.loaderdiv').hide();
                                $('.requestaction').attr('disabled', false);
                                if (ret.status == 2) {
                                    $('#actionmsg').addClass('alert alert-danger');
                                    $('#actionmsg').html(ret.message);
                                    window.setTimeout(function () {
                                        location.reload()
                                    }, 2000)
                                } else {
                                    $('#actionmsg').addClass('alert alert-success');
                                    $('#actionmsg').html(ret.message);
                                    window.setTimeout(function () {
                                        location.reload()
                                    }, 2000)
                                }
                            }
                        });
                    } else {
                        $('#loadernew').hide();
                        $('.requestaction').attr('disabled', false);
                    }
                }
            } else {
                alert('Please select entry');
                $('.requestaction').attr('disabled', false);
            }
            console.log(boq_ids);
        });
    });

    $("#DailyWorkProgress_project_id").change(function () {
        var val = $(this).val();
        $("#DailyWorkProgress_project_id_em_").hide();
        var date = $('#DailyWorkProgress_date').val();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getAllTasks'); ?>',
            data: {
                project_id: val,
                date: date
            },
            method: "GET",
            success: function (result) {
                $('.taskid').html(result);
            }
        })


        $('#DailyWorkProgress_consumed_item_count').val("")
        $('#consumed_item_rate').val("")
    })

    $("#DailyWorkProgress_taskid").change(function () {
        var val = $(this).val();
        $("#DailyWorkProgress_taskid_em_").hide();
        var date = $('.date').val();
        if (date == '') {
            alert('choose a date');
            window.location.reload();
        }

        if (val != '') {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/gettaskdetails'); ?>',
                data: {
                    taskid: val
                },
                method: "GET",
                dataType: "json",
                success: function (result) {
                    $("#task_id_span1").html("Task Id: <b>" + result.taskid + "</b>");
                    $("#DailyWorkProgress_total_quantity").val(result.total_qty);
                    $("#item_total_qty").val(result.total_qty);
                    $("#item_main_id").val(result.taskid);
                    $("#DailyWorkProgress_work_type").html(result.option);
                    if (result.balance_qty != 0) {
                        $("#balance_qty").text("Balance quantity: " + result.balance_qty);
                    }
                    $("#DailyWorkProgress_work_type").val(result.work_type);
                    $('#unit_value').html(result.unit)
                }
            })
        } else {
            $("#task_id_span1").html("");
            $("#DailyWorkProgress_total_quantity").html("");
            $("#item_total_qty").val("");
            $("#item_main_id").val("");
        }
    })
</script>

<script>
    $(document).ready(function () {
        getLocation()

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            $("#latitude").val(position.coords.latitude);
            $("#longitude").val(position.coords.longitude);
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getSite'); ?>',
                data: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                },
                method: "POST",
                success: function (result) {
                    obj = JSON.parse(result);
                    if (obj.site_name != "") {
                        $("#site_text").html(obj.site_name);
                        $("#site_id").val(obj.site_id);
                        $("#site_name").val(obj.site_name);
                    }
                }
            })
        }


    })
    $(".icon-eye").click(function (e) {

        e.preventDefault();
        //var id = $(this).closest('tr').find('input[type="checkbox"]').val();
        var id = this.id;
        if (id != '') {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('/dailyWorkProgress/view_wpr'); ?>',

                data: {
                    id: id
                },
                success: function (data) {
                    $(".view_progress").html(data);
                    $('.boq_details_sec').animate({
                        height: 'show'
                    }, 500);


                }
            })
        }
    })


    $('.progress-table').wrap('<div class="table-responsive"></div>')
</script>
<script>
    $(document).ready(function () {




        $("#zero").on("change", function () {
            var input_val = $("#zero").val();
            $("#DailyWorkProgress_qty_em_").hide();
            if (input_val == 0) {

                alert("Quantity should be greater than 0");
                $("#zero").val("");
            }






        });



    });



    $('.mytaskbtn').click(function () {
        $('#mytasks').show();
        sessionStorage.setItem('clicked', true);
        $('.mytaskbtn').hide();
        $('.closemytaskbtn').show();

    });

    $('.closemytaskbtn').click(function () {
        sessionStorage.removeItem('clicked');
        $('#mytasks').hide();
        $('.closemytaskbtn').hide();
        $('.mytaskbtn').show();
    });


    $('.addwpr').click(function () {
        var task_id = this.id;

        $.ajax({
            "type": "POST",

            "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/getwprtaskdetails'); ?>",
            "dataType": "json",
            "data": {
                task_id: task_id
            },
            "success": function (data) {
                $.fn.MultiFile();
                $('#wpr_form_div').show();
                $('#wpr_form_div').html(data);
            }
        });

    });

    $(function () {
        $("#datepicker").datepicker({
            maxDate: new Date()
        });
    });

    window.onload = function () {
        var data = sessionStorage.getItem('clicked');
        if (data == 'true') {
            $('#mytasks').show();
            $('.mytaskbtn').hide();
            $('.closemytaskbtn').show();
        }
    };


    $('#consumed_item_id').change(function () {
        var itm_id = $('#consumed_item_id').val();
        $('#DailyWorkProgress_consumed_item_count').val("");
        var project_id = $("#DailyWorkProgress_project_id").val();
        var wpr_id = $('#wpr_id').val();

        if (wpr_id != "") {

            $.ajax({
                "type": "POST",

                "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemExist'); ?>",
                "dataType": "json",
                "data": {
                    itm_id: itm_id,
                    wpr_id: wpr_id,
                    type: 1
                },
                "success": function (data) {

                    if (data.status == 1) {
                        $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
                        $("#DailyWorkProgress_consumed_item_id_em_").text('This item already exist');
                        $('#consumed_item_id').val("");

                    } else {

                        $("#DailyWorkProgress_consumed_item_id_em_").hide();
                        $.ajax({
                            "type": "POST",

                            "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemAvailability'); ?>",
                            "dataType": "json",
                            "data": {
                                itm_id: itm_id,
                                project_id: project_id
                            },
                            "success": function (data) {


                                if (data.status == 1) {
                                    $("#DailyWorkProgress_consumed_item_id_em_").hide();
                                } else if (data.status == 0) {
                                    $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
                                    $("#DailyWorkProgress_consumed_item_id_em_").text('This item is not mentioned in template');
                                    $('#consumed_item_id').val("");
                                } else {
                                    $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
                                    $("#DailyWorkProgress_consumed_item_id_em_").text('No template is defined for this project');
                                    $('#consumed_item_id').val("");
                                }

                            }
                        });

                    }

                }
            });
        }









    })





    $(document).on('click', ".approve_consumed_item", function () {
        if (!confirm("Do you want to Approve ?")) { } else {
            $('.loaderdiv').show();
            var entry_id = this.id;
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approve_consumption_item'); ?>',
                data: {
                    entry_id: entry_id
                },
                method: "POST",
                dataType: 'json',
                success: function (result) {
                    if (result.status == 1) {

                        $('.loaderdiv').hide();
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(result.message);
                        setTimeout(function () {
                            location.reload();
                        }, 1000);

                    } else {

                    }

                }
            })
        }
    });

    $("#consumed_item_id").change(function () {
        var consumed_item_id = $("#consumed_item_id").val();
        var project_id = $("#DailyWorkProgress_project_id").val();
        $.ajax({
            method: "POST",
            data: {
                consumed_item_id: consumed_item_id,
                project_id: project_id
            },
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
            success: function (data) {
                $("#consumed_item_rate").html(data.html);
            }
        });
    });


    $("#consumed_item_rate").change(function () {
        $('#DailyWorkProgress_consumed_item_count').val("")
    });


    $(document).on('click', "#submit_boq", function () {

        var project_id = $('#DailyWorkProgress_project_id').val();
        var date = $('#DailyWorkProgress_date').val();
        var task_id = $('#DailyWorkProgress_taskid').val();
        var qty = $('#zero').val();
        var work_type = $('#DailyWorkProgress_work_type').val();
        var description = $('#DailyWorkProgress_description').val();
        var formData = new FormData($("#daily-work-progress-form")[0]);


        if (project_id == "") {

            $("#DailyWorkProgress_project_id_em_").css("display", "");
            $("#DailyWorkProgress_project_id_em_").text("Please Select Project");
        }
        if (date == "") {

            $("#DailyWorkProgress_date_em_").css("display", "");
            $("#DailyWorkProgress_date_em_").text("Please Enter Date");
        }
        if (task_id == "") {

            $("#DailyWorkProgress_taskid_em_").css("display", "");
            $("#DailyWorkProgress_taskid_em_").text("Please Enter Task");
        }
        if (qty == "") {

            $("#DailyWorkProgress_qty_em_").css("display", "");
            $("#DailyWorkProgress_qty_em_").text("Please Enter Quantity");
        }
        if (work_type == "") {

            $("#DailyWorkProgress_work_type_em_").css("display", "");
            $("#DailyWorkProgress_work_type_em_").text("Please Enter WorkType");
        }
        if (description == "") {
            $("#DailyWorkProgress_description_em_").css("display", "");
            $("#DailyWorkProgress_description_em_").text("Please Enter Description");
        } else {

            $.ajax({
                type: "POST",
                "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/create_daily_wpr'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "beforeSend": function () {
                    $('#process').css('display', 'block');

                },

                "success": function (data) {
                    var percentage = 0;


                    var timer = setInterval(function () {
                        percentage = percentage + 20;

                        progress_bar_percentage(percentage, timer, data);

                    }, 1000);


                }
            });

        }



        function progress_bar_percentage(percentage, timer, data) {


            $('.progress-bar').css('width', percentage + '%');
            if (percentage > 100) {
                clearInterval(timer);
                $('#process').css('display', 'none');
                $('.progress-bar').css('width', '0%');
                $('#alertMessage').show();
                $('#daily-work-progress-form').reset();
                $('#task_id_span1').html('');
                $('red').text('');

                $('.work_progress_form').slideUp();

                $('#alertMessage').html(data.message)
                $("#success_message").fadeIn().delay(1000).fadeOut();

                setTimeout(function () {
                    window.location.reload(1);
                }, 1000);


            }
        }

    });




    $(document).on('change', '.itemid', function () {

        var $t = $(this);
        var $column = $(this);
        var value = $('#consumed_item_id').val();
        var val = $t.val();
        var z = 0;

        $(".itemid").each(function () {
            var y = $(this).val();
            if (val == y) {
                z = z + 1;
            }
            if (value == y) {
                z = z + 1;
            }
        });

        if (z > 1) {
            $column.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
            $column.val('');
            return false;
        }




    });
</script>


<script>
    function addLabel() {

        var count = $('.MultiFile-applied').length;

        $('#label_container').append(`

    <div class="form-group">
    <label for="DailyWorkProgress_image_label">Image Label ` + count + `</label>
    <input class="form-control" autocomplete="off" name="DailyWorkProgress[image_label][]" id="DailyWorkProgress_image_label_` + count + `" type="text">
    <div class="errorMessage" id="DailyWorkProgress_image_label_em_` + count + `" style="display:none"></div>
                    </div>

    `);

        var lable_id = $('#DailyWorkProgress_image_label_' + count).val();


    }
</script>


<script type="text/javascript">
    $(".popup").hide();
    $(".consumed-item").on("mouseover", function () {

        var id = $(this).attr("id");
        var elem = $(this);

        $.ajax({
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/ItemEstimatiomMsg'); ?>',
            data: {
                id: id,

            },
            method: "POST",
            success: function (result) {

                // $(elem).parent("td").attr('title',result);
                $(elem).siblings('.popup').html(result).show();
                (elem).parents("tr").siblings("tr").find('.popup').html('').hide();


            }
        })
    });


    $("#resources_used").change(function () {
        var resources_used = $("#resources_used").val();
        var wpr_id = $('#wpr_id').val();

        $.ajax({
            method: "POST",
            data: {
                resources_used: resources_used,
                wpr_id: wpr_id,
                type: 2
            },
            "dataType": "json",
            url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/checkItemExist'); ?>',
            success: function (data) {

                if (data.status == 1) {
                    $("#DailyWorkProgress_consumed_resource_em_").css("display", "");
                    $("#DailyWorkProgress_consumed_resource_em_").text('This item already exist');
                    $('#resources_used').val("");
                } else {
                    $("#DailyWorkProgress_consumed_resource_em_").hide();

                    $.ajax({
                        method: "POST",
                        data: {
                            resources_used: resources_used,

                        },
                        "dataType": "json",
                        url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getResourceUnit'); ?>',
                        success: function (data) {

                            $('#uom').val(data.unit);
                            $('#unit_id').val(data.unit_id);
                        }
                    });
                }

            }
        });


    });



    $("#DailyWorkProgress_current_status").change(function () {
        var status = $("#DailyWorkProgress_current_status").val();
        var balance_quantity = $('#balance_quantity_id').val();
        if (status == 7) {

            if (balance_quantity != "") {

                if (balance_quantity != 0) {
                    $("#DailyWorkProgress_current_status").val("");
                    $("#DailyWorkProgress_current_status_em_").css("display", "");
                    $("#DailyWorkProgress_current_status_em_").text("The balance quantity should be 0 if the status is to be 'completed'");

                }
                if (balance_quantity == 0) {
                    $("#DailyWorkProgress_current_status_em_").hide();
                }


            }

        } else {

            $("#DailyWorkProgress_current_status_em_").hide();

            if (balance_quantity == 0) {
                $("#DailyWorkProgress_current_status").val("");
                $("#DailyWorkProgress_current_status_em_").css("display", "");
                $("#DailyWorkProgress_current_status_em_").text("If the balance quantity is 0 then the status should be 'completed'");
            }
        }
    });

    $("#DailyWorkProgress_work_type").change(function () {
        $("#DailyWorkProgress_work_type_em_").hide();
    });
    $("#DailyWorkProgress_description").change(function () {
        $("#DailyWorkProgress_description_em_").hide();
    });
</script>
<script>
    $(".wpr-capture-btn").click(function () {

        $(".incidents-capture-div").toggleClass("display-none");
        $(".wpr-capture-btn").toggleClass("minus-icon")
        $(".wpr-capture-btn-wrapper").toggleClass("wpr-border-bottom")

    });
</script>


<!-- new wpr script -->
<script>
    //new form submission starts
    function wprFormSubmit(form, data, hasError) {
        if (!hasError) {
            var formData = new FormData($("#wpr-add-form")[0]);
            // $('.loaderdiv').show();



            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('DailyWorkProgress/saveWorkProgress'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function (data) {
                    $('.loaderdiv').hide();


                    if (data.status == 1) {
                        $('#success_message_div').show();
                        $('#success_message_div').html("created succesfully...");
                        $("#success_message_div").fadeIn().delay(1000).fadeOut();
                        $('#alertMessageDiv').show();
                        $('#alertMessageDiv').html(data.message)
                        $("#alertMessageDiv").fadeIn().delay(1000).fadeOut();




                        $('#step_one').removeClass('active');
                        $('#step_one').addClass('progress_tab');
                        $('#wpr-step1-info').removeClass('active in');
                        $('#step_two').addClass('active');
                        $('#wpr-step2-info').addClass('active in');
                        $('#wpr_id').val(data.wpr_id);

                        // set wpr summary




                        $('#project').text(data.project_name);
                        $('#date').text(data.wpr_date);
                        $('#task').text(data.taskname);
                        $('#quantity').text(data.quantity);
                        $('#worktype').text(data.worktype);
                        $('#status').text(data.wpr_status);
                        $('#progress').text(data.progress);
                        $('#tot_qty').text(data.totqty);
                        $('#remark').text(data.remarks);




                    } else {
                        alert("fail");
                    }


                } // success close

            })

        } else {
            $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
            setTimeout('$("#successMessage").fadeOut("slow")', 2000);

        }

    }
    //project form submission ends



    $("#add_new_item").click(function () {
        var item_id = $('#consumed_item_id').val();
        var rate_id = $('#consumed_item_rate').val();
        var item_quantity = $('#DailyWorkProgress_consumed_item_count').val();
        var type = 1;
        var task_id = $('#DailyWorkProgress_taskid').val();
        var wpr_id = $('#wpr_id').val();
        var action_id = $('#action_id').val();
        var item_already_exist = $('#item_already_exist').val();
        var rate_already_exist = $('#rate_already_exist').val();
        var count_already_exist = $('#count_already_exist').val();

        if (action_id == "create") {
            var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/saveConsumedItems'); ?>';
        } else if (action_id == "update") {
            var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/updateConsumedItems'); ?>'
        }

        if (item_id != "" && rate_id != "" && item_quantity != "" && type != "" && task_id != "" && wpr_id != "" && action_id != "") {
            $.ajax({
                method: "POST",
                data: {
                    item_id: item_id,
                    rate_id: rate_id,
                    item_quantity: item_quantity,
                    type: type,
                    task_id: task_id,
                    wpr_id: wpr_id,
                    item_already_exist: item_already_exist,
                    rate_already_exist: rate_already_exist,
                    count_already_exist: count_already_exist


                },
                "dataType": "json",
                // url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/saveConsumedItems'); ?>',
                url: url,
                success: function (data) {

                    if (data.status == 1) {
                        $("#item-used-grid").load(location.href + " #item-used-grid");
                        $("#item-used-grid-one").load(location.href + " #item-used-grid-one");
                        $('#success_message_div').show();
                        $('#success_message_div').html(data.message);
                        $("#success_message_div").fadeIn().delay(1000).fadeOut();

                        $('#consumed_item_id').val("");
                        $('#consumed_item_rate').val("");
                        $('#DailyWorkProgress_consumed_item_count').val("");
                        $('#action_id').val("create");

                    } else {
                        alert("something went wrong");
                    }

                }
            });
        }







    });

    $("#save_and_proceed_item").click(function () {
        var item_id = $('#consumed_item_id').val();
        var rate_id = $('#consumed_item_rate').val();
        var item_quantity = $('#DailyWorkProgress_consumed_item_count').val();
        var type = 2;
        var task_id = $('#DailyWorkProgress_taskid').val();
        var wpr_id = $('#wpr_id').val();
        var action_id = $('#action_id').val();
        var item_already_exist = $('#item_already_exist').val();
        var rate_already_exist = $('#rate_already_exist').val();
        var count_already_exist = $('#count_already_exist').val();
        if (action_id == "create") {
            var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/saveConsumedItems'); ?>';
        } else if (action_id == "update") {
            var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/updateConsumedItems'); ?>'
        }

        if (item_id != "" && rate_id != "" && item_quantity != "" && type != "" && task_id != "" && wpr_id != "" && action_id != "") {
            $.ajax({
                method: "POST",
                data: {
                    item_id: item_id,
                    rate_id: rate_id,
                    item_quantity: item_quantity,
                    type: type,
                    task_id: task_id,
                    wpr_id: wpr_id,
                    item_already_exist: item_already_exist,
                    rate_already_exist: rate_already_exist,
                    count_already_exist: count_already_exist


                },
                "dataType": "json",
                // url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/saveConsumedItems'); ?>',
                url: url,
                success: function (data) {

                    if (data.status == 1) {
                        $("#item-used-grid").load(location.href + " #item-used-grid");
                        $("#item-used-grid-one").load(location.href + " #item-used-grid-one");
                        $('#success_message_div').show();
                        $('#success_message_div').html("succesfully updated...");
                        $("#success_message_div").fadeIn().delay(1000).fadeOut();
                        $('#consumed_item_id').val("");
                        $('#consumed_item_rate').val("");
                        $('#DailyWorkProgress_consumed_item_count').val("");

                        $('#step_two').removeClass('active');
                        $('#step_two').addClass('progress_tab');
                        $('#wpr-step2-info').removeClass('active in');
                        $('#step_three').addClass('active');
                        $('#wpr-step3-info').addClass('active in');
                        $('#action_id').val("create");




                    } else {
                        alert("something went wrong");
                    }

                }
            });
        } else {

            $('#step_two').removeClass('active');
            $('#step_one').addClass('progress_tab');
            $('#step_two').addClass('progress_tab');
            $('#wpr-step2-info').removeClass('active in');
            $('#step_three').addClass('active');
            $('#wpr-step3-info').addClass('active in');

        }
    });

    function editConsumedItem(elem, href) {
        var itemId = $(elem).closest("tr").find(".rowId").text();
        var id = getURLParameter(href, 'id');
        var project_id = $("#DailyWorkProgress_project_id").val();
        $('#action_id').val("update");
        // First AJAX call
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
            type: "POST",
            "dataType": "json",
            data: {
                consumed_item_id: itemId,
                project_id: project_id
            },
            success: function (data) {
                $("#consumed_item_rate").html(data.html);
                // Handle the response of the first AJAX call

                // Now, call the second AJAX function
                $.ajax({
                    "url": "<?php echo Yii::app()->createUrl('DailyWorkProgress/fetchConsumedItem'); ?>",
                    type: "POST",
                    "dataType": "json",
                    data: {
                        id: id
                    },
                    success: function (result) {
                        if (result.stat == 1) {
                            $('#consumed_item_id').val(result.item_id);
                            $('#consumed_item_rate').val(result.rate_id);
                            $('#DailyWorkProgress_consumed_item_count').val(result.item_count);
                            $('#item_already_exist').val(result.item_id);
                            $('#rate_already_exist').val(result.rate_id);
                            $('#count_already_exist').val(result.item_count);
                        }
                    },
                    error: function (xhr2, status2, error2) {
                        // Handle error (if any) in the second AJAX call
                        alert("Error in the second AJAX call: " + error2);
                    }
                });
            },
            error: function (xhr1, status1, error1) {
                // Handle error (if any) in the first AJAX call
                alert("Error in the first AJAX call: " + error1);
            }
        });


    }

    function deleteConsumedItem(elem, href) {
        var itemId = $(elem).closest("tr").find(".rowId").text();
        var id = getURLParameter(href, 'id');
        var project_id = $("#DailyWorkProgress_project_id").val();
        var wpr_id = $("#wpr_id").val();
        var task_id = $('#DailyWorkProgress_taskid').val();

        var answer = confirm("Are you sure you want to delete?");

        if (answer) {

            if (itemId != "" && id != "" && project_id != "" && wpr_id != "" && task_id != "") {

                $.ajax({
                    method: "POST",
                    data: {
                        item_id: itemId,
                        wpr_id: wpr_id,
                        id: id,
                        project_id: project_id,
                        task_id: task_id



                    },
                    "dataType": "json",
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/deleteConsumedItem'); ?>',

                    success: function (data) {

                        if (data.status == 1) {
                            $("#item-used-grid").load(location.href + " #item-used-grid");
                            $("#item-used-grid-one").load(location.href + " #item-used-grid-one");
                            $('#success_message_div').show();
                            $('#success_message_div').html("deleted succesfully...");
                            $("#success_message_div").fadeIn().delay(1000).fadeOut();
                        } else {
                            alert("something went wrong");
                        }

                    }
                });
            }

        }
    }

    $("#add_new_resource").click(function () {

        var wpr_id = $('#wpr_id').val();
        var task_id = $('#DailyWorkProgress_taskid').val();
        var resources_used = $('#resources_used').val();
        var unit_id = $('#unit_id').val();
        var resource_quantity = $('#resource_quantity').val();
        var resource_action_id = $('#resource_action_id').val();
        var type = 1;
        var resourceId = $('#resourceId').val();



        if (wpr_id != "" && task_id != "" && resources_used != "" && unit_id != "" && resource_quantity != "" && type != "") {
            $.ajax({
                method: "POST",
                data: {
                    wpr_id: wpr_id,
                    task_id: task_id,
                    resources_used: resources_used,
                    unit_id: unit_id,
                    resource_quantity: resource_quantity,
                    type: type,
                    resource_action_id: resource_action_id,
                    resourceId: resourceId
                },
                "dataType": "json",

                url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/saveResource'); ?>',
                success: function (data) {

                    if (data.status == 1) {
                        $("#resource-used-grid").load(location.href + " #resource-used-grid");
                        $("#resource-used-grid-one").load(location.href + " #resource-used-grid-one");
                        $('#success_message_div').show();
                        $('#success_message_div').html(data.message);
                        $("#success_message_div").fadeIn().delay(1000).fadeOut();

                        $('#resources_used').val("");
                        $('#resource_quantity').val("");
                        $('#uom').val("");
                        $('#unit_id').val("");
                        $('#resource_action_id').val("create");

                    } else {
                        alert("something went wrong");
                    }

                }
            });
        }



    });

    function editResource(href) {
        var id = getURLParameter(href, 'id');

        $.ajax({
            "url": "<?php echo Yii::app()->createUrl('DailyWorkProgress/fetchResource'); ?>",
            type: "POST",
            "dataType": "json",
            data: {
                id: id
            },
            success: function (result) {
                if (result.stat == 1) {
                    $('#resources_used').val(result.resource_id);
                    $('#resource_quantity').val(result.resource_qty);
                    $('#uom').val(result.unit_name);
                    $('#unit_id').val(result.resource_unit);
                    $('#resourceId').val(result.id);
                    $('#resource_action_id').val("update");
                }
            },
            error: function (xhr2, status2, error2) {
                // Handle error (if any) in the second AJAX call
                alert("Error in the  AJAX call: " + error2);
            }
        });

    }

    function deleteResource(href) {
        var answer = confirm("Are you sure you want to delete?");
        var id = getURLParameter(href, 'id');
        if (answer) {

            if (id != "") {
                $.ajax({
                    method: "POST",
                    data: {
                        id: id
                    },
                    "dataType": "json",
                    url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/deleteResource'); ?>',

                    success: function (data) {

                        if (data.status == 1) {

                            $("#resource-used-grid").load(location.href + " #resource-used-grid");
                            $("#resource-used-grid-one").load(location.href + " #resource-used-grid-one");
                            $('#success_message_div').show();
                            $('#success_message_div').html("succesfully deleted...");
                            $("#success_message_div").fadeIn().delay(1000).fadeOut();
                        } else {
                            alert("something went wrong");
                        }

                    }
                });
            }

        }
    }
    $("#save_and_proceed_resource").click(function () {

        var wpr_id = $('#wpr_id').val();
        var task_id = $('#DailyWorkProgress_taskid').val();
        var resources_used = $('#resources_used').val();
        var unit_id = $('#unit_id').val();
        var resource_quantity = $('#resource_quantity').val();
        var resource_action_id = $('#resource_action_id').val();
        var type = 1;
        var resourceId = $('#resourceId').val();



        if (wpr_id != "" && task_id != "" && resources_used != "" && unit_id != "" && resource_quantity != "" && type != "") {
            $.ajax({
                method: "POST",
                data: {
                    wpr_id: wpr_id,
                    task_id: task_id,
                    resources_used: resources_used,
                    unit_id: unit_id,
                    resource_quantity: resource_quantity,
                    type: type,
                    resource_action_id: resource_action_id,
                    resourceId: resourceId
                },
                "dataType": "json",

                url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/saveResource'); ?>',
                success: function (data) {

                    if (data.status == 1) {
                        $("#resource-used-grid").load(location.href + " #resource-used-grid");
                        $("#resource-used-grid-one").load(location.href + " #resource-used-grid-one");
                        $('#success_message_div').show();
                        $('#success_message_div').html("succesfully updated...");
                        $("#success_message_div").fadeIn().delay(1000).fadeOut();

                        $('#resources_used').val("");
                        $('#resource_quantity').val("");
                        $('#uom').val("");
                        $('#unit_id').val("");

                        $('#step_three').removeClass('active');
                        $('#step_one').addClass('progress_tab');
                        $('#step_two').addClass('progress_tab');
                        $('#step_three').addClass('progress_tab');
                        $('#wpr-step3-info').removeClass('active in');
                        $('#step_four').addClass('active');
                        $('#wpr-step4-info').addClass('active in');
                        $('#resource_action_id').val("create");

                    } else {
                        alert("something went wrong");
                    }

                }
            });
        } else {
            $('#step_three').removeClass('active');
            $('#step_one').addClass('progress_tab');
            $('#step_two').addClass('progress_tab');
            $('#step_three').addClass('progress_tab');
            $('#wpr-step3-info').removeClass('active in');
            $('#step_four').addClass('active');
            $('#wpr-step4-info').addClass('active in');
        }


    });

    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
    }
    $("#new-btn").click(function () {

        $('#action_id').val("create");
        $('#consumed_item_id').val("");
        $('#consumed_item_rate').val("");
        $('#DailyWorkProgress_consumed_item_count').val("");
    });

    $("#resource-new-btn").click(function () {
        $('#resource_action_id').val("create");
        $('#resources_used').val("");
        $('#resource_quantity').val("");
        $('#uom').val("");
        $('#unit_id').val("");


    });




    $('.consumed-item-qty').on('keypress', function (event) {
        // Allow numbers (0-9), decimal point (.), and backspace (8)
        var allowedKeys = [8, 46];
        for (var i = 48; i <= 57; i++) {
            allowedKeys.push(i);
        }
        if (event.which && $.inArray(event.which, allowedKeys) === -1) {
            event.preventDefault();
        }
    });

    // Attach keyup event handler to the quantity field to remove invalid characters
    $('.consumed-item-qty').on('keyup', function () {
        var value = $(this).val();
        value = value.replace(/[^0-9.]/g, '');
        $(this).val(value);
        if (value == 0) {

            value = value.replace(/[^1-9.]/g, '');
            $(this).val(value);
        }
    });

    $('.consumed-resource-qty').on('keypress', function (event) {
        // Allow numbers (0-9), decimal point (.), and backspace (8)
        var allowedKeys = [8, 46];
        for (var i = 48; i <= 57; i++) {
            allowedKeys.push(i);
        }
        if (event.which && $.inArray(event.which, allowedKeys) === -1) {
            event.preventDefault();
        }
    });

    // Attach keyup event handler to the quantity field to remove invalid characters
    $('.consumed-resource-qty').on('keyup', function () {
        var value = $(this).val();
        value = value.replace(/[^0-9.]/g, '');
        $(this).val(value);
        if (value == 0) {

            value = value.replace(/[^1-9.]/g, '');
            $(this).val(value);
        }
    });


    $(document).on('click', ".reject-entry", function () {
        var id = this.id;
        $('#work_progress_id').val(id);
        $("#popup1").show();

    });
    $('#popup1 .close-icon').click(function () {

        $('#popup1').hide();
    });
    $(document).on('click', "#rejectentry", function () {
        var reason = $('#DailyWorkProgress_approve_reject_data').val();
        var entry_id = $('#work_progress_id').val();

        if (reason) {
            $('.loaderdiv').show();
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approverejectdata'); ?>',
                data: { entry_id: entry_id, reason: reason },
                method: "POST",
                success: function (result) {
                    $('.loaderdiv').hide();
                    if (result == 1) {
                        alert('Rejected Successfully');
                        location.reload();
                    } else {
                        alert('Something went wrong in reject');
                    }
                }
            })
        } else {
            alert('Please enter reason before reject');
        }
    });


    $(document).on('click', ".delete-wpr", function () {
        var id = this.id;
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {



            $.ajax({
                type: "POST",
                data: { id: id },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/deleteWPR'); ?>',
                success: function (response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {

                        alert(response.msg);
                    } else if (response.response == "warning") {

                        alert(response.msg);
                    } else {

                        alert(response.msg);
                    }
                    $("#daily-work-progress-grid").load(location.href + " #daily-work-progress-grid");



                }
            });

        }

    });


    $(document).ready(function () {
        $('#alertMessageDiv').hide();
        $('#success_message_div').hide();
        $('#mytasks').hide();
        $('.closemytaskbtn').hide();

        var daily_progress_id = "<?php echo $WPRID; ?>";
        if (daily_progress_id != "") {
            $('.work_progress_form').slideDown();
        }
        val = $('#DailyWorkProgress_project_id').val();



        var incident = '<?php echo $model->incident; ?>';
        var inspection = '<?php echo $model->inspection ?>';
        var visitor_name = '<?php echo $model->visitor_name ?>';
        var company_name = '<?php echo $model->company_name ?>';
        var designation = '<?php echo $model->designation ?>';
        var purpose = '<?php echo $model->purpose ?>';

        $('#incident_id').val(incident);

        if (incident != "" || inspection != "" || visitor_name != "" || company_name != "" || designation != "" || purpose != "") {
            $(".incidents-capture-div").toggleClass("display-none");
            $(".wpr-capture-btn").toggleClass("minus-icon")
            $(".wpr-capture-btn-wrapper").toggleClass("wpr-border-bottom")
        }


    });
</script>