<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<style>
  .notice {
    position: relative;
  }

  .popup {
    position: absolute;
    /* transform: translate3d(590px, 1996px, 0px); */
    /* bottom: 0; */
    top: 56px;
    left: 0px;
    will-change: transform;
    transform: translate(-205px, -40px);
    width: 200px;
    background: #fafafa;
    padding: 10px;
    border: 1px solid #ddd;
    z-index: 1;

  }
</style>


<?php

/* @var $this DailyWorkProgressController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
  'Daily Work Progresses',
);
$settings = GeneralSettings::model()->find(['condition' => 'id = 1']);
if (Yii::app()->user->role != 1) {
  $criteria = new CDbCriteria;
  $criteria->select = 'project_id,tskid';
  $criteria->condition = ' parent_tskid is not null and (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ")";
  $criteria->group = 'project_id';
  $project_ids = Tasks::model()->findAll($criteria);
  $project_id_array = array();

  foreach ($project_ids as $projid) {
    array_push($project_id_array, $projid['project_id']);
  }
  if (!empty($project_id_array)) {
    $project_id_list = implode(',', $project_id_array);
  } else {
    $project_id_list = 'NULL';
  }
} else {
  $project_id_list = 'NULL';
}
?>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
  <div class="info">
    <?php echo Yii::app()->user->getFlash('success'); ?>
  </div>
<?php endif; ?>
<div class="dailywork-sec">
  <div class="clearfix daily-work-header">
    <h1 class="pull-left">Daily Work Progresses</h1>
    <div class="pull-right">
      <?php

      if ((in_array('/tasks/monthlyTask', Yii::app()->session['menuauthlist']))) {
        echo CHtml::link('Monthly Task', array('tasks/monthlyTask'), array('class' => 'btn btn-primary btn-sm'));
      }
      ?>
      <?php if (Yii::app()->user->role != 9) { ?>
        <button class="btn btn-primary btn-sm addworkprogressbtn">Add Daily Work Progress</button>
        <button class="btn btn-primary btn-sm mytaskbtn">My Task</button>
        <button class="btn btn-primary btn-sm closemytaskbtn display-none">Close My Task</button>
      <?php  } ?>
      <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/themes/btheme2/assets/admin/layout3/scripts/autocomplete.js', CClientScript::POS_END); ?>
      <a href="<?= Yii::app()->createUrl("PhotoPunch/default/index", array(), 'https') ?>" class="btn btn-primary btn-sm pull-right photo-punch-btn ">Photo Punch</a>
    </div>
  </div>
  <?php

  Yii::app()->clientScript->registerScript('myjquery', " 
  
    $(document).ready(function () {
        $( '.reset' ).click(function() {
        location.href = '" . Yii::app()->createAbsoluteUrl('DailyWorkProgress/index') . "';
    });

    $('.addworkprogressbtn').click(function(){
        $('.work_progress_form').slideDown();
    });

    $('.close_work_progress').click(function(){
        $('#daily-work-progress-form').reset();
        $('#task_id_span1').html('');
        $('red').text('');
        
        $('.work_progress_form').slideUp();
    });
      
    $('.info').fadeOut('3000');

  $( '.img_comp_class').keyup(function() {

     // $(function(){
      var projectid = $('#DailyWorkProgress_project_id').val();
      //alert(projectid);
      if(projectid == '')
      {
    //alert('Please Select Project');
    //$('#DailyWorkProgress_project_id_em_').css({display:block});
   
        }else
        {
        //   $.ajax({
        // 		type: 'GET',
        // 		dataType: 'text',
        // 		url:'" . Yii::app()->createUrl("DailyWorkProgress/getitems") . "',
        // 		data:{projectid:projectid},
        // 		success:function(data){
        // 		//alert(data);
// 		console.log(data);
        // 		var arrResponse = JSON.parse(data);
        // 		test(arrResponse);
                        
        // 				 },
        // 	   });
        //});
        function test(data){
                var items = data;
                console.log(items);
                console.log(typeof items);
                
            $('.img_comp_class').autocomplete({
            source: items,
            select: function(event,ui) {
            $('#autocomplete_hid').val(ui.item.label);
            $('#autocomplete_hid_dataval').val($(this).attr('data-val'));
            }
            });
            }
        }
        });
        
 $('#DailyWorkProgress_item_id').change(function () {
        var details = $('#DailyWorkProgress_item_id').val();
   
                var arr = details.split(',');
                var fst = arr.splice(0,1).join('');
                var rest = arr.join(',');
                $('#DailyWorkProgress_item_id').val(fst);
                $('.details_item').html(rest);
                $('#newdetails').val(rest);
                //alert(fst);
        });
  });
 ");
  ?>
  <!-- Add tools Popup -->
  <div class="index-dailyworkprogress-sec">
    <div id="addproject" class="modal" role="dialog">
      <div class="modal-dialog modal-lg">
      </div>
    </div>
    <!-- progress bar start  -->
    <div class="form-group display-none" id="process">
      <div class="progress">

        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">

        </div>

      </div>

    </div>
    <!-- progress bar end -->
    <?php
    if (Yii::app()->user->role != 9) {
    ?>
      <div class="panel panel-default work_progress_form page-scroll-bar">
        <div class="panel-heading clearfix">
          <a class="close_work_progress pull-right"><b>X</b></a>
          <h3>Add Work Progress</h3>
        </div>
        <div class="panel-body">
          <p>Site:<span id="site_text"></span></p>
          <div class="form">
            <?php $form = $this->beginWidget('CActiveForm', array(
              'id' => 'daily-work-progress-form',
              'htmlOptions' => array('enctype' => 'multipart/form-data'),
              //'action' => Yii::app()->createUrl('/dailyWorkProgress/create'),
              'enableAjaxValidation' => true,
              'enableClientValidation' => true,
              'clientOptions' => array(
                'validateOnChange' => true,
                'validateOnType' => true,
              ),
            )); ?>
            <div class="row">
              <input type="hidden" name="newdetails" id="newdetails">
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'project_id'); ?>
                <?php
                if (Yii::app()->user->role == 1) {
                  $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
                } else {
                  $criteria = new CDbCriteria;
                  $criteria->select = 'project_id';
                  $criteria->condition = ' parent_tskid is not null and (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id . ")";
                  $criteria->group = 'project_id';
                  $project_ids = Tasks::model()->findAll($criteria);
                  $project_id_array = array();
                  foreach ($project_ids as $projid) {
                    array_push($project_id_array, $projid['project_id']);
                  }
                  if (!empty($project_id_array)) {
                    $project_id_array = implode(',', $project_id_array);
                    $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to) OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
                  } else {
                    $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)', 'order' => 'name ASC');
                  }
                }

                if (Yii::app()->user->project_id != "") {
                  $project_id = Yii::app()->user->project_id;
                }
                $project = Projects::model()->findAll($condition);
                $data_array = array();
                if (count($project) == 1) {
                  foreach ($project as $key => $value) {
                    array_push($data_array, $value->pid);
                  }
                  $id = implode(" ", $data_array);
                  $p_data =  Projects::model()->findByPk($id);
                ?>
                  <select class="form-control" name="DailyWorkProgress[project_id]" id="DailyWorkProgress_project_id">
                    <option value="<?php echo $p_data['pid']; ?>" <?php echo ($project_id == $p_data['pid']) ? 'selected' : ""; ?>><?php echo $p_data['name']; ?></option>
                  </select>
                <?php
                } else {
                  echo $form->dropDownList(
                    $model,
                    'project_id',
                    CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'),
                    array('empty' => '-Choose a Project-', 'class' => 'form-control')
                  );
                }
                ?>
                <?php echo $form->error($model, 'project_id'); ?>
              </div>
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control date', 'autocomplete' => 'off',)); ?>
                <?php echo $form->error($model, 'date'); ?>
                <div class="date_section" id="test_sample"></div>
              </div>
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'taskid'); ?>
                <?php
                $tasks = array();
                $work_type = "";
                $tsk_id = "";
                $quantity = "";
                $unit = "";
                if (Yii::app()->user->project_id != "") {
                  if (Yii::app()->user->role != 1) {
                    $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND  parent_tskid is not null and  project_id=' . Yii::app()->user->project_id . ' AND (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')', 'order' => 'title'));
                  } else {
                    $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND  parent_tskid is not null and  project_id=' . Yii::app()->user->project_id, 'order' => 'title'));
                  }
                } else {
                  $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1  AND   parent_tskid is not null and  (assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ')', 'order' => 'title'));
                }
                $data_array = array();
                $tsk_id = "";
                $work_type = "";
                if (count($tasks) == 1) {
                  foreach ($tasks as $key => $value) {
                    array_push($data_array, $value->tskid);
                  }
                  $tsk_id = implode(" ", $data_array);
                  $task = Tasks::model()->findByPk($tsk_id);
                  $work_type = $task->work_type_id;
                  $tsk_id = $tsk_id;
                  $quantity = $task->quantity;
                  if ($task->unit != 0) {
                    $units_data = Unit::model()->findByPk($task->unit);
                    $unit = $units_data->unit_code;
                  }
                }
                ?>
                <span id="task_id_span1"><?php echo $tsk_id; ?></span>




                <select class="form-control taskid" name="DailyWorkProgress[taskid]" id="DailyWorkProgress_taskid">
                  <option value="">-Choose a Task-</option>
                  <?php
                  if (!empty($tasks)) {
                    foreach ($tasks as $key => $value) {
                  ?>
                      <option value="<?php echo $value['tskid']; ?>" <?php echo ($value['tskid'] == $tsk_id) ? "selected" : ""; ?>>
                        <?php echo ($value['parent_tskid'] != null ? ' - ' : '') ?><?php echo $value['title']; ?></option>
                  <?php }
                  } ?>
                </select>
                <span class="details_item"><?php echo ((isset($itemdetails)) ? $itemdetails : ''); ?></span>
                <?php echo $form->error($model, 'taskid'); ?>
              </div>
              <div class="col-md-2 col-sm-3 form-group">
                <?php echo $form->labelEx($model, 'qty'); ?>
                <?php echo $form->textField($model, 'qty', array(
                  'class' => 'form-control img_comp_class', 'autocomplete' => 'off',
                  'name' => 'DailyWorkProgress[qty]', 'id' => 'zero',
                  'ajax' => array(
                    'url' => array('DailyWorkProgress/getprogress'),
                    'type' => 'POST',
                    'data' => array(
                      'qty' => 'js:this.value',
                      'item_total_qty' => 'js: $("#item_total_qty").val()',
                      'item_id' => 'js: $("#item_main_id").val()',

                    ),
                    'dataType' => 'json',
                    'success' => 'function(data){
                            if(data.progress==0){
                            $("#DailyWorkProgress_qty").val("0");
                            $("#DailyWorkProgress_daily_work_progress").val("0");                                
                            $("#DailyWorkProgress_qty_em_").css("display", "");
                            $("#DailyWorkProgress_qty_em_").text(data.msg);
                             setTimeout(function(){ location.reload(); }, 800);
                        }else{
                            $("#DailyWorkProgress_daily_work_progress").val(data.progress);
                            }
                            if(data.balance_qty !== ""){
                               
                               $("#balance_qty").text("Balance quantity: "+data.balance_qty);
                               $("#balance_quantity_id").val(data.balance_qty);
                            }

                            var status=$("#DailyWorkProgress_current_status").val();
                            var balance_quantity=data.balance_qty;

                            if(status!="")
                            {
                              if(balance_quantity == 0 && status!=7)
                          {
                            $("#DailyWorkProgress_current_status").val("");
                            $("#DailyWorkProgress_current_status_em_").css("display", "");
                            $("#DailyWorkProgress_current_status_em_").text("If the balance quantity is 0 then the status should be completed");
                          }
                          if(balance_quantity!=0 && status==7)
                                  {
                                  
                                   $("#DailyWorkProgress_current_status").val("");
                                 $("#DailyWorkProgress_current_status_em_").css("display", "");
                                 $("#DailyWorkProgress_current_status_em_").text("The balance quantity should be 0 if the status is to be completed");
                                  }
                            }
       
                        }',
                  )
                )); ?>
                <span id="balance_qty"></span>
                <?php echo $form->error($model, 'qty'); ?>
                <input type="hidden" id="balance_quantity_id">
              </div>


              <div class="col-md-2 col-sm-3 pos-rel form-group">
                <?php echo $form->labelEx($model, 'total_quantity'); ?>
                <?php echo $form->textField($model, 'total_quantity', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'value' => $quantity)); ?>
                <span id="unit_value"><?php echo $unit; ?></span>
                <?php echo $form->error($model, 'total_quantity'); ?>
              </div>
            </div>
            <div class="row">
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'work_type'); ?>
                <?php
                $condition = array('select' =>  array('wtid,work_type'), 'order' => 'work_type ASC');
                echo $form->dropDownList($model, 'work_type', CHtml::listData(WorkType::model()->findAll($condition), 'wtid', 'work_type'), array('options' => array($work_type => array('selected' => true)), 'empty' => '-Choose a Work Type-', 'class' => 'form-control'));
                ?>
                <?php echo $form->error($model, 'work_type'); ?>
              </div>
              <div class="subrow col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'current_status'); ?>
                <?php
                $condition_status = array('select' =>  array('sid,caption'), 'condition' => "status_type='task_status' AND sid NOT IN(6)", 'order' => 'caption ASC');
                echo $form->dropDownList($model, 'current_status', CHtml::listData(Status::model()->findAll($condition_status), 'sid', 'caption'), array('empty' => '-Choose status-', 'class' => 'form-control'));
                ?>
                <?php echo $form->error($model, 'current_status'); ?>

              </div>
              <div class="col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'daily_work_progress'); ?>
                <?php echo $form->textField($model, 'daily_work_progress', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true)); ?>
                <?php echo $form->error($model, 'daily_work_progress'); ?>
              </div>
              <div class="col-md-2 col-sm-6 form-group">
                <?php echo $form->labelEx($model, 'remarks'); ?>
                <?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($model, 'description'); ?>
              </div>
              <div class="col-md-2 col-sm-6 form-group">
                <span class="img_span"> (WidthxHeight)(2048x1536)</span>
                <?php
                $this->widget('CMultiFileUpload', array(
                  'model' => $model,
                  'name' => 'image',
                  'attribute' => 'image',
                  'accept' => 'jpg|gif|png|jpeg',
                  'max' => 3,

                  'duplicate' => 'Already Selected',
                  'options' => array(
                    'onFileSelect' => 'function(e, v, m){ addLabel() }',

                  ),
                ));
                ?>
              </div>




              <div id="label_container"></div>



              <!-- <div class="col-md-2 col-sm-6 form-group">
                                <?php echo $form->labelEx($model, 'image label'); ?>
                                <?php echo $form->textField($model, 'image_label', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                <?php echo $form->error($model, 'image_label'); ?>
                            </div> -->


              <div class="">
                <?php echo CHtml::hiddenField('item_description', '', array('id' => 'item_description')); ?>
                <?php echo CHtml::hiddenField('item_total_qty', '', array('id' => 'item_total_qty', 'value' => $quantity)); ?>
                <?php echo CHtml::hiddenField('item_main_id', '', array('id' => 'item_main_id', 'value' => $tsk_id)); ?>
              </div>
              <?php echo $form->hiddenField($model, 'latitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "latitude")); ?>
              <?php echo $form->hiddenField($model, 'longitude', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "longitude")); ?>
              <?php echo $form->hiddenField($model, 'site_id', array('class' => 'form-control', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_id")); ?>
              <?php echo $form->hiddenField($model, 'site_name', array('class' => 'form-control site_name', 'autocomplete' => 'off', 'readonly' => true, 'id' => "site_name")); ?>


              <!--   Projem section -->

              <div class="row margin-top-120">
                <?php if (in_array('/dailyWorkProgress/incident', Yii::app()->session['menuauthlist'])) { ?>
                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label>Incidents</label>
                    <select class="form-control" name="DailyWorkProgress[incident]" id="incident_id">
                      <option value="">Choose Incident</option>
                      <option value="1">Accidents </option>
                      <option value="2">Injuries </option>
                      <option value="3">Unusual Events </option>
                    </select>
                  </div>
                <?php } ?>
                <?php if (in_array('/dailyWorkProgress/inspection', Yii::app()->session['menuauthlist'])) { ?>
                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Test/Inspection">Test / Inspection </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[inspection]">
                  </div>
                <?php } ?>
                <?php if (in_array('/dailyWorkProgress/siteVisit', Yii::app()->session['menuauthlist'])) { ?>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Visitor">Visitor Name </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[visitor]">
                  </div>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Company">Company </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[company]">
                  </div>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Designation">Designation </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[designation]">
                  </div>

                  <div class="subrow col-md-2 col-sm-6 form-group">
                    <label for="Designation">Purpose </label>
                    <input type="text" class="form-control" name="DailyWorkProgress[purpose]">
                  </div>
                <?php } ?>
              </div>


              <!-- end projem section -->


              <!-- item consumed start -->
              <?php
              // if (Tasks::model()->accountPermission() == 1) {
              if (Tasks::model()->accountPermission() == 1 && (in_array('/dailyWorkProgress/itemConsumption', Yii::app()->session['menuauthlist']))) {
              ?>
                <div class="row margin-top-25">
                  <hr size="30">
                  <input type="hidden" id="item_count_status" name="DailyWorkProgress[item_count_status]">


                  <div class=" col-md-2 col-sm-6 ">
                    <?php echo $form->labelEx($model, 'Item Consumed'); ?>

                    <select class="form-control" id="consumed_item_id" name="DailyWorkProgress[consumed_item_id][]">
                      <option></option>

                    </select>

                    <?php echo $form->error($model, 'consumed_item_id'); ?>
                  </div>



                  <div class=" col-md-2 col-sm-6 ">

                    <label>Rate</label>
                    <select class="form-control" id="consumed_item_rate" name="DailyWorkProgress[consumed_item_rate][]">
                      <option></option>

                    </select>


                  </div>



                  <div class=" col-md-2 col-sm-6 ">



                    <?php echo $form->labelEx($model, 'Item Consumed'); ?>
                    <?php echo $form->textField($model, 'consumed_item_count[]', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                    <?php echo $form->error($model, 'consumed_item_count'); ?>


                  </div>
                </div>


                <!-- add more -->
                <div id="container"></div>
                <br>
                <input type="button" id="add_new" value="Add" onClick="addInput();" class="margin-left-14">
                <br>
                <!-- end add more -->

              <?php
              }
              ?>


              <!-- end item consumed -->

              <!-- Resources utilized start -->
              <?php
              if (Tasks::model()->accountPermission() == 1) {
              ?>
                <div class="row margin-top-25">
                  <hr size="30">


                  <div class=" col-md-2 col-sm-6 ">

                    <label>Resource utilised</label>
                    <select class="form-control" id="resources_used" name="DailyWorkProgress[resources_used][]">
                      <option value="">Choose a resource</option>
                      <?php
                      foreach ($resource_utilised as $resources) {
                      ?>
                        <option value="<?php echo $resources->id ?>"><?php echo $resources->resource_name ?></option>
                      <?php
                      }
                      ?>

                    </select>


                  </div>




                  <div class=" col-md-2 col-sm-6 ">

                    <label>Quantity</label>
                    <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[utilised_qty][]" type="text">

                  </div>

                  <div class=" col-md-2 col-sm-6 ">



                    <label>UOM</label>
                    <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[uom][]" type="text" id="uom" readonly>





                  </div>

                  <div class=" col-md-2 col-sm-6 ">




                    <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[unit_id][]" type="hidden" id="unit_id" readonly>





                  </div>





                </div>


                <!-- add more -->
                <div id="resources_container"></div>
                <br>
                <input type="button" id="add_new" value="Add" onClick="addResources();" class="margin-left-14">
                <br>

              <?php
              }
              ?>
              <!-- end add more -->
              <!-- Resources utilized end -->




              <div class="buttons col-md-2 col-sm-12 form-group">
                <?php echo CHtml::Button($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'save_btn btn blue', 'id' => 'submit_boq')); ?>
              </div>
            </div>
            <?php $this->endWidget(); ?>
          </div><!-- form -->
        </div>
      </div>
    <?php
    }
    ?>

    <div id="wpr_form_div" class="page-scroll-bar" style="display:none">

    </div>


    <!-- my task table -->
    <div id="mytasks" class="display-none">
      <?php
      $type = "";
      $model1 = new Tasks;
      $this->renderPartial('wprmytask', array(
        'model' => $model1
      ));

      ?>
    </div>
    <!-- end my task table  -->





    <div class="clearfix table-filter">
      <div class="prev_curr_next pull-left sec_h">
        <?php $form = $this->beginWidget('CActiveForm', array(
          'id' => 'page-form',
          'method' => 'GET',
          'enableAjaxValidation' => true,
          'htmlOptions' => array('class' => 'form-inline')
        )); ?>
        <div class="form-group datepick">
          <?php echo $form->label($model, 'date'); ?>
          <?php echo CHtml::activeTextField($model, 'fromdate', array('class' => 'form-control height-28', 'size' => 10, 'value' => isset($_REQUEST['DailyWorkProgress']['fromdate']) ? $_REQUEST['DailyWorkProgress']['fromdate'] : '')); ?>
          <?php
          $this->widget('application.extensions.calendar.SCalendar', array(
            'inputField' => 'DailyWorkProgress_fromdate',
            'ifFormat' => '%d-%m-%Y',
          ));
          ?> to
          <?php echo CHtml::activeTextField($model, 'todate', array('class' => 'form-control height-28', 'size' => 10, 'value' => isset($_REQUEST['DailyWorkProgress']['todate']) ? $_REQUEST['DailyWorkProgress']['todate'] : '')); ?>
          <?php
          $this->widget('application.extensions.calendar.SCalendar', array(
            'inputField' => 'DailyWorkProgress_todate',
            'ifFormat' => '%d-%m-%Y',
          ));
          ?>

        </div>

        <div class="form-group min-btn">
          <?php echo CHtml::submitButton('Go', array('class' => 'btn blue btn-sm')); ?>
          <?php echo CHtml::Button('Clear', array('class' => 'btn btn-default btn-sm reset')); ?>
        </div>
        <?php $this->endWidget(); ?>
      </div>
      <div class="pull-right sec_h">
        <div class="nav-div">
          <?php
          $sql = 'select `approve_status`, '
            . 'count(approve_status) as itemcount '
            . 'from pms_daily_work_progress ';
          if (Yii::app()->user->role != 1) {
            if ($interval != "0" && !empty($project_id_list)) {
              $sql .= 'where date="' . date("Y-m-d", strtotime($interval)) . '"  AND  project_id IN (' . $project_id_list . ')';
            } elseif ($interval != "0") {
              $sql .= 'where date="' . date("Y-m-d", strtotime($interval)) . '"';
            } elseif (!empty($project_id_list)) {
              $sql .= ' where  project_id IN (' . $project_id_list . ')';
            }
          } else {
            if ($interval != "0") {
              $sql .= 'where date="' . date("Y-m-d", strtotime($interval)) . '"';
            }
          }
          $sql .= 'group by `approve_status`';
          $status_count = CHtml::listData(Yii::app()->db->createCommand($sql)->queryAll(), 'approve_status', 'itemcount');
          $filterarray = array('All' => 'All', 'Approved' => 'Approved', 'Pending' => 'Pending', 'Rejected' => 'Rejected');
          $statusarray = array('All' => 'All', 1 => 'Approved', 0 => 'Pending', 2 => 'Rejected');
          echo '<ul class="nav1 nav ">';
          $activeitem = ((!isset($_GET['type']) or $_GET['type'] == 'All') ? 'All' : intval($_GET['type']));
          $status_count['All'] = array_sum($status_count);
          $statusbgcolor = array('Red' => 'ALL', 'green' => 1, 'blue' => 0, '#715d13' => 2);
          foreach ($statusarray as $k => $status) {
            echo '<li class="nav-item loaddiv">';
            echo CHtml::link(
              $status
                . '&nbsp;&nbsp;<span class="badge" style="background-color: ' . array_search($k, $statusbgcolor) . '">'
                . (isset($status_count[$k]) ? $status_count[$k] : 0)
                . '</span>',
              array('/DailyWorkProgress/index', 'type' => $k, 'interval' => $interval),
              array('class' => "nav-link rounded-5" . ($k === $activeitem ? ' active ' : ''), 'aria-disabled' => "true")
            );
            echo '</li>';
          }
          echo '</ul>';
          ?>
        </div>
      </div>
    </div>

    <?php
    if (!empty($_GET['interval'])) {
      $date = $_GET['interval'];
    } else {
      $date = "";
    }
    ?>
    <p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully created..</p>

    <div class="alert display-none" id="alertMessage">


      <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>
    <div id='actionmsg'></div>


    <?php if (in_array('/dailyWorkProgress/rejectentry', Yii::app()->session['menuauthlist'])) {
    ?>
      <div class="approve_form">
        <div>
          <textarea name="reason" class="reason"></textarea>

          <input type="button" class='requestaction btn btn-danger' name='reject' value='Reject' />
          <?php
          echo CHtml::image(
            Yii::app()->request->baseUrl . '/images/loading.gif',
            'Loading....',
            array('class' => 'width-30 margin-left-159 display-none')
          );
          ?>
        </div>
      </div>
    <?php }
    ?>

    <div id="boq_entries">
      <?php
      if (Yii::app()->user->role == 1) {
        $project_dropdown_1 = CHtml::listData(Projects::model()->findAll(
          array(
            'select' => array('pid,name'),
            'order' => 'name',
            'distinct' => true
          )
        ), "pid", "name");
      } else {
        $project_dropdown_1 = CHtml::listData(Projects::model()->findAll(
          array(
            'select' => array('pid,name'),
            'condition' => 'pid IN (' . $project_id_list . ')',
            'order' => 'name',
            'distinct' => true
          )
        ), "pid", "name");
      }

      $null_dropdown = array('0' => 'All');
      $projects_list = $project_dropdown_1 + $null_dropdown;
      ksort($projects_list);



      if (Yii::app()->user->role == 1) {
        $this->widget('zii.widgets.grid.CGridView', array(
          'id' => 'daily-work-progress-grid',
          'htmlOptions' => array('class' => 'grid-view'),
          'itemsCssClass' => 'table table-bordered progress-table',
          'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',

          'pager' => array(
            'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
            'nextPageLabel' => 'Next '
          ),
          'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
          'dataProvider' => $model->search($interval, $selected_status),
          'ajaxUpdate' => false,
          'filter' => $model,
          'selectableRows' => 2,
          'columns' => array(
            array(
              'id' => 'selected_tasks',
              'class' => 'CCheckBoxColumn',
              'selectableRows' => '50',

            ),
            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'),),

            array(
              'name' => 'date',
              'value' => 'date("d-M-y", strtotime($data->date))',

            ),
            array(
              'name' => 'project_id',
              'value' => '$data->project->name',
              'filter' => CHtml::activeDropDownList($model, 'project_id', $projects_list)

            ),
            array(
              'name' => 'taskid',
              'value' => '(isset($data->tasks)?$data->tasks->title:"")',
            ),
            array(
              'name' => 'qty',
              'value' => function ($data) {
                if (!empty($data->tasks->unit)) {
                  $unit = Unit::model()->findByPk($data->tasks->unit);
                  echo $data->qty . ' (' . $unit->unit_code . ')';
                } else {
                  echo $data->qty;
                }
              },
            ),
            array(
              'header' => 'Balance quantity',
              'value' => function ($data) {
                echo    $data->getbalanceqty($data->taskid, $data->id);
              },
            ),
            array(
              'name' => 'work_type',
              'value' => '(isset($data->worktype)?$data->worktype->work_type:"")',
            ),
            array(
              'name' => 'daily_work_progress',
              'value' => function ($data) {
                echo round($data->daily_work_progress, 2);
              },
            ),
            array(
              'name' => 'current_status',
              'value' => function ($data) {
                if (!empty($data->current_status)) {
                  echo $data->status0->caption;
                } else {
                  echo $data->getStatus($data->taskid);
                }
              },
              'filter' => CHtml::listData(Status::model()->findAll(
                array(
                  'select' => array('sid,caption'),
                  'order' => 'caption',
                  'distinct' => true,
                  'condition' => 'status_type="task_status"'
                )
              ), "sid", "caption")
            ),
            array(
              'header' => 'Status',
              'value' => function ($data) {
                if ($data->work_progress_type == 1) {
                  return "Active";
                } else {
                  return "Expired";
                } 
              },
            ),
            array(
              'name' => 'approve_reject_data',
              'value' => function ($data) {
                if ($data->approve_status != 1) {
                  echo $data->approve_reject_data;
                }
              },
            ),
            'description',
            array(
              'name' => 'created_by',
              'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
              'visible' => Yii::app()->user->role == 1 || Yii::app()->user->role == 9,
              'filter' => CHtml::listData(Users::model()->findAll(
                array(
                  'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                  'order' => 'first_name',
                  'distinct' => true
                )
              ), "userid", "first_name")
            ),
            array(
              'name' => 'approve_status',
              'value' => function ($data) {
                if ($data->approve_status == 0) {
                  return "Not Approved";
                } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 0) {
                  return "Not Approved";
                } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 1) {
                  return "Approved";
                } elseif ($data->approve_status == 2) {
                  return "Rejected";
                } else {
                  return "";
                }
              },
              'filter' => CHtml::listData($model->getApproveStatuses(), 'id', 'title'),
            ),

            array(
              'name' => 'consumed_item_id',
              'value' => function ($data) {

                echo $data->getItemDet($data->id, $value = 0);
              },
              'filter' => false,
            ),

            array(
              'name' => 'consumed_item_count',
              'value' => function ($data) {
                echo '<div id="' . $data->id . '" class="consumed-item">' . $data->getItemDet($data->id, $value = 1) . '</div>
                                <div class="popup"></div>';
              },

              'filter' => false,
              'cssClassExpression' => '$data->consumed_item_status==0 && $data->consumed_item_status!="" ? "notice" : ""',

            ),

            array(
              'htmlOptions' => array('class' => 'white-space-nowrap'),
              'class' => 'ButtonColumn',
              'template' => '{view}{approve_btn}{reject_btn}{recall}{Update}{approve_consumed_item}',
              'evaluateID' => true,

              'buttons' => array(
                'view' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View', 'id' => '$data->id'),
                ),
                'approve_btn' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/approveentry", Yii::app()->session["menuauthlist"])',
                  'options' => array('class' => 'fa fa-thumbs-up approve', 'title' => 'Approve', 'id' => '$data->id'),
                ),
                'reject_btn' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'visible' => '($data->approve_status=="0" ) && ($data->tasks->status != "7") && in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                  'url' => 'Yii::app()->createUrl("dailyWorkProgress/approverejectdata", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid","stat"=>2))',
                  'click' => 'function(e){e.preventDefault();$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                  'options' => array('class' => 'fa fa-thumbs-down margin_5 reject', 'title' => 'Reject'),
                ),
                'recall' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'visible' => '($data->approve_status=="2")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                  'options' => array('class' => 'fa fa-rotate-left icon-comn margin_5 recall', 'title' => 'Recall', 'id' => '$data->id'),
                ),

                'Update' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'url' => 'Yii::app()->createUrl("/dailyWorkProgress/updatewpr", array("id"=>$data->id))',
                  'visible' => '(in_array("/dailyWorkProgress/update", Yii::app()->session["menuauthlist"]) && ($data->created_by == ' . Yii::app()->user->id . ' || Yii::app()->user->role == 1) && $data->work_progress_type==1)',
                  'options' => array('class' => 'icon-pencil  icon-comn margin_5 edit', 'title' => 'Edit', 'id' => '$data->id'),
                ),
                'approve_consumed_item' => array(
                  'label' => '',
                  'visible' => '($data->consumed_item_status=="0") && in_array("/dailyWorkProgress/approve_consumption_item", Yii::app()->session["menuauthlist"])',
                  'options' => array('class' => 'fa fa-check approve_consumed_item', 'id' => '$data->id'),
                )
              )

            ),
          ),
        ));
      } else {
        $this->widget('zii.widgets.grid.CGridView', array(
          'id' => 'daily-work-progress-grid',
          'htmlOptions' => array('class' => 'grid-view'),
          'itemsCssClass' => 'table table-bordered progress-table',
          'template' => '{summary}<div class="table-responsive">{items}</div>{pager}',

          'pager' => array(
            'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
            'nextPageLabel' => 'Next '
          ),
          'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
          'dataProvider' => $model->search($interval, $selected_status),
          'ajaxUpdate' => false,
          'filter' => $model,
          'selectableRows' => 2,
          'columns' => array(

            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'),),

            array(
              'name' => 'date',
              'value' => 'date("d-M-y", strtotime($data->date))',

            ),
            array(
              'name' => 'project_id',
              'value' => '$data->project->name',
              'filter' => CHtml::activeDropDownList($model, 'project_id', $projects_list)

            ),
            array(
              'name' => 'taskid',
              'value' => '(isset($data->tasks)?$data->tasks->title:"")',
            ),
            array(
              'name' => 'qty',
              'value' => function ($data) {
                if (!empty($data->tasks->unit)) {
                  $unit = Unit::model()->findByPk($data->tasks->unit);
                  echo $data->qty . ' (' . $unit->unit_code . ')';
                } else {
                  echo $data->qty;
                }
              },
            ),

            array(
              'header' => 'Balance quantity',
              'value' => function ($data) {
                echo   $data->getbalanceqty($data->taskid, $data->id);
              },
            ),





            array(
              'name' => 'work_type',
              'value' => '(isset($data->worktype)?$data->worktype->work_type:"")',
            ),
            array(
              'name' => 'daily_work_progress',
              'value' => function ($data) {
                echo round($data->daily_work_progress, 2);
              },
            ),
            array(
              'name' => 'current_status',
              'value' => function ($data) {
                if (!empty($data->current_status)) {
                  echo $data->status0->caption;
                } else {
                  echo $data->getStatus($data->taskid);
                }
              },
              'filter' => CHtml::listData(Status::model()->findAll(
                array(
                  'select' => array('sid,caption'),
                  'order' => 'caption',
                  'distinct' => true,
                  'condition' => 'status_type="task_status"'
                )
              ), "sid", "caption")
            ),
            array(
              'header' => 'Status',
              'value' => function ($data) {
                if ($data->work_progress_type == 1) {
                  return "Active";
                } else {
                  return "Expired";
                } 
              },
            ),
            array(
              'name' => 'approve_reject_data',
              'value' => function ($data) {
                if ($data->approve_status != 1) {
                  echo $data->approve_reject_data;
                }
              },
            ),
            'description',
            array(
              'name' => 'created_by',
              'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
              'visible' => Yii::app()->user->role == 1 || Yii::app()->user->role == 9,
              'filter' => CHtml::listData(Users::model()->findAll(
                array(
                  'select' => array('userid,CONCAT_ws(" ",first_name,last_name) as first_name'),
                  'order' => 'first_name',
                  'distinct' => true
                )
              ), "userid", "first_name")
            ),
            array(
              'name' => 'approve_status',
              'value' => function ($data) {
                if ($data->approve_status == 0) {
                  return "Not Approved";
                } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 0) {
                  return "Not Approved";
                } elseif ($data->approve_status == 1 && $data->warehouseApproval($data->id) == 1) {
                  return "Approved";
                } elseif ($data->approve_status == 2) {
                  return "Rejected";
                } else {
                  return "";
                }
              },
              'filter' => CHtml::listData($model->getApproveStatuses(), 'id', 'title'),
            ),

            array(
              'name' => 'consumed_item_id',
              'value' => function ($data) {

                echo $data->getItemDet($data->id, $value = 0);
              },
              'filter' => false,
            ),

            // array(
            //     'name' => 'consumed_item_count',
            //     'value' => function ($data) {
            //         echo $data->getItemDet($data->id, $value = 1);
            //     },
            //     'filter' => false,
            //     'cssClassExpression' => '$data->consumed_item_status==0 && $data->consumed_item_status!="" ? "notice" : ""',
            //     'onmouseover'=>'functionName()',
            // ),

            array(
              'name' => 'consumed_item_count',
              'value' => function ($data) {
                echo '<div id="' . $data->id . '" class="consumed-item">' . $data->getItemDet($data->id, $value = 1) . '</div>
                                <div class="popup"></div>';
              },

              'filter' => false,
              'cssClassExpression' => '$data->consumed_item_status==0 && $data->consumed_item_status!="" ? "notice" : ""',

            ),


            array(
              'htmlOptions' => array('class' => 'white-space-nowrap'),
              'class' => 'ButtonColumn',
              'template' => '{view}{approve_btn}{reject_btn}{recall}{Update}{approve_consumed_item}',
              'evaluateID' => true,

              'buttons' => array(
                'view' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View', 'id' => '$data->id'),
                ),
                'approve_btn' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/approveentry", Yii::app()->session["menuauthlist"])',
                  'options' => array('class' => 'fa fa-thumbs-up approve', 'title' => 'Approve', 'id' => '$data->id'),
                ),
                'reject_btn' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'visible' => '($data->approve_status=="0" ) && ($data->tasks->status != "7") && in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                  'url' => 'Yii::app()->createUrl("dailyWorkProgress/approverejectdata", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid","stat"=>2))',
                  'click' => 'function(e){e.preventDefault();$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
                  'options' => array('class' => 'fa fa-thumbs-down margin_5 reject', 'title' => 'Reject'),
                ),
                'recall' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'visible' => '($data->approve_status=="2")&& in_array("/dailyWorkProgress/rejectentry", Yii::app()->session["menuauthlist"])',
                  'options' => array('class' => 'fa fa-rotate-left icon-comn margin_5 recall', 'title' => 'Recall', 'id' => '$data->id'),
                ),

                'Update' => array(
                  'label' => '',
                  'imageUrl' => false,
                  'url' => 'Yii::app()->createUrl("/dailyWorkProgress/updatewpr", array("id"=>$data->id))',
                  'visible' => '(in_array("/dailyWorkProgress/update", Yii::app()->session["menuauthlist"]) && ($data->created_by == ' . Yii::app()->user->id . ' || Yii::app()->user->role == 1))',
                  'options' => array('class' => 'icon-pencil  icon-comn margin_5 edit', 'title' => 'Edit', 'id' => '$data->id'),
                ),
                'approve_consumed_item' => array(
                  'label' => '',
                  'visible' => '($data->consumed_item_status=="0") && in_array("/dailyWorkProgress/approve_consumption_item", Yii::app()->session["menuauthlist"])',
                  'options' => array('class' => 'fa fa-check approve_consumed_item', 'id' => '$data->id'),
                )
              )

            ),
          ),
        ));
      }
      ?>
    </div>
  </div>
  <?php
  $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
      'title' => 'Reject BOQ Entry',
      'autoOpen' => false,
      'modal' => false,
      'width' => 590,
      'height' => "auto",
    ),
  ));
  ?>
  <iframe id="cru-frame" width="550" height="auto" frameborder="0" class="min-height-200"></iframe>
  <?php
  $this->endWidget();
  ?>
  <div id="view_progress" class="view_progress"></div>


  <!--  -->
  <div id="pop">

  </div>
  <!--  -->



</div>

</div>
<script>
  $('#daily-work-progress-form').on('beforeSubmit', function(e) {
    $('#submit_boq').css('disable', 'disable');
    return false;
  });

  $(function() {
    $("#datepicker").datepicker({
      onSelect: function(selectedDate) {
        var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/index&interval='); ?>' + selectedDate;
        $(location).attr('href', url);
      }
    });
  });
  $("#datepicker").change(function() {
    date = $(this).val();
    if (date == "") {
      var url = '<?php echo Yii::app()->createUrl('DailyWorkProgress/index'); ?>';
      $(location).attr('href', url);
    }
  })
</script>
<script>
  $(document).ready(function() {
    $('.approve_form').hide();
    var date1 = '<?php echo $settings->backlog_days; ?>';
    $('#DailyWorkProgress_date').datepicker({
      dateFormat: 'dd-M-y',
      minDate: -date1,
      maxDate: 0,
      onSelect: function(selectedDate) {
        var d = new Date(selectedDate);
        var date = this.value;
        entrydatevalidation(date);
        $("#DailyWorkProgress_date_em_").hide();
      }
    });
    $(document).on('click', ".approve", function() {
      if (!confirm("Do you want to Approve ?")) {} else {
        $('.loaderdiv').show();
        var entry_id = this.id;
        $.ajax({
          url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approveentry'); ?>',
          data: {
            entry_id: entry_id
          },
          method: "GET",
          dataType: 'json',
          success: function(result) {
            if (result.status == '1') {
              $("#boq_entries").load(location.href + " #boq_entries>*", "");
              $('.alert').removeClass('hide');
              $('.alert').css('display', 'block');
              $('.alert').append(result.schedule);
              $('.alert').append("<br/>");
              $('.alert').append(result.dependant);
              $('.loaderdiv').hide();
              setTimeout(function() {
                location.reload();
              }, 1000);
            } else {
              $('.alert').removeClass('hide');
              $('.alert').css('display', 'block');
              $('.alert').append(result.message);
              $('.loaderdiv').hide();
            }

          }
        })
      }
    });


    $(document).on('click', ".recall", function() {
      if (!confirm("Do you want to Recall ?")) {} else {
        $('.loaderdiv').show();
        var entry_id = this.id;
        $.ajax({
          url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/recall'); ?>',
          data: {
            entry_id: entry_id
          },
          method: "GET",
          dataType: 'json',
          success: function(result) {
            if (result.status == '1') {
              $("#boq_entries").load(location.href + " #boq_entries>*", "");
              $('.loaderdiv').hide();
            } else if (result.status == '2') {
              $('.alert').removeClass('hide');
              $('.alert').append(result.message);
              $('.loaderdiv').hide();
            }

          }
        })
      }
    });

    function entrydatevalidation(date) {
      var project_id = $('#DailyWorkProgress_project_id').find(":selected").val();
      if (project_id !== '') {
        $.ajax({
          url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/checkdate'); ?>',
          data: {
            date: date,
            project_id: project_id
          },
          method: "POST",
          dataType: 'json',
          success: function(result) {
            $('#DailyWorkProgress_taskid').html(result.html);
            $('#DailyWorkProgress_taskid').val(result.tsk_id);
            $('#DailyWorkProgress_work_type').html(result.work_type_html);
            $('#DailyWorkProgress_work_type').val(result.work_type);
            $('#task_id_span1').text(result.tsk_id);
            $('#DailyWorkProgress_total_quantity').val(result.quantity);
            $("#unit_value").html(result.unit);
            $("#item_total_qty").val(result.quantity);
            $("#item_main_id").val(result.tsk_id);

            if (result.bal_qty_status == 1) {
              $("#balance_qty").text("Balance quantity: " + result.bal_qty_count);
              $("#balance_quantity_id").val(result.bal_qty_count);
            }


          }
        });
      }
    }
    $('#selected_tasks_all').click(function() {
      var checkboxes = document.getElementsByTagName('input');
      if (this.checked) {
        $('.approve_form').slideDown();
        for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = true;
            $('div.checker span').addClass('checked');
          }
        }
      } else {
        $('.approve_form').slideUp();
        for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = false;
            $('div.checker span').removeClass('checked');
          }
        }
      }
    });
    $('input[name="selected_tasks[]"]').click(function() {
      var countchecked = $('input[name="selected_tasks[]"]:checked').length;
      if (countchecked >= 1) {
        $('.approve_form').slideDown();
      } else if (countchecked == 0) {
        $('.approve_form').slideUp();
      }
      if (this.checked) {
        $('.approve_form').slideDown();

      } else {
        var all = [];
        $("#selected_tasks_all").prop('checked', false);
        $('div#uniform-selected_tasks_all span').removeClass('checked');
      }
    });
    $(".requestaction").click(function() {
      $('.requestaction').attr('disabled', true);
      var req = $(this).val();
      var reason = $('.reason').val();
      var boq_ids = [];
      $('input[name="selected_tasks[]"]:checked').each(function() {
        boq_ids.push(this.value)
      });
      if (boq_ids != '') {
        if (reason == "") {
          alert("please enter reason")
          flag = 1;
        } else {
          if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

            $('.loaderdiv').show();
            $.ajax({
              method: "post",
              dataType: "json",
              data: {
                ids: boq_ids,
                req: req,
                reason: reason,
              },
              url: '<?php echo Yii::app()->createUrl("/dailyWorkProgress/bulkReject") ?>',
              success: function(ret) {
                $('.loaderdiv').hide();
                $('.requestaction').attr('disabled', false);
                if (ret.status == 2) {
                  $('#actionmsg').addClass('alert alert-danger');
                  $('#actionmsg').html(ret.message);
                  window.setTimeout(function() {
                    location.reload()
                  }, 2000)
                } else {
                  $('#actionmsg').addClass('alert alert-success');
                  $('#actionmsg').html(ret.message);
                  window.setTimeout(function() {
                    location.reload()
                  }, 2000)
                }
              }
            });
          } else {
            $('#loadernew').hide();
            $('.requestaction').attr('disabled', false);
          }
        }
      } else {
        alert('Please select entry');
        $('.requestaction').attr('disabled', false);
      }
      console.log(boq_ids);
    });
  });

  $("#DailyWorkProgress_project_id").change(function() {
    var val = $(this).val();
    $("#DailyWorkProgress_project_id_em_").hide();
    var date = $('#DailyWorkProgress_date').val();
    $.ajax({
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getAllTasks'); ?>',
      data: {
        project_id: val,
        date: date
      },
      method: "GET",
      success: function(result) {
        $('.taskid').html(result);
      }
    })

    $.ajax({
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getWarehouseItems'); ?>',
      data: {
        project_id: val,

      },
      method: "POST",
      success: function(result) {
        $("#consumed_item_id").html(result.html);
      }
    })
    $('#DailyWorkProgress_consumed_item_count').val("")
    $('#consumed_item_rate').val("")
  })

  $("#DailyWorkProgress_taskid").change(function() {
    var val = $(this).val();
    $("#DailyWorkProgress_taskid_em_").hide();
    var date = $('.date').val();
    if (date == '') {
      alert('choose a date');
      window.location.reload();
    }

    if (val != '') {
      $.ajax({
        url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/gettaskdetails'); ?>',
        data: {
          taskid: val
        },
        method: "GET",
        dataType: "json",
        success: function(result) {
          $("#task_id_span1").html("Task Id: <b>" + result.taskid + "</b>");
          $("#DailyWorkProgress_total_quantity").val(result.total_qty);
          $("#item_total_qty").val(result.total_qty);
          $("#item_main_id").val(result.taskid);
          $("#DailyWorkProgress_work_type").html(result.option);
          if (result.balance_qty != 0) {
            $("#balance_qty").text("Balance quantity: " + result.balance_qty);
          }
          $("#DailyWorkProgress_work_type").val(result.work_type);
          $('#unit_value').html(result.unit)
        }
      })
    } else {
      $("#task_id_span1").html("");
      $("#DailyWorkProgress_total_quantity").html("");
      $("#item_total_qty").val("");
      $("#item_main_id").val("");
    }
  })
</script>

<script>
  $(document).ready(function() {
    getLocation()

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }

    function showPosition(position) {
      $("#latitude").val(position.coords.latitude);
      $("#longitude").val(position.coords.longitude);
      $.ajax({
        url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getSite'); ?>',
        data: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        },
        method: "POST",
        success: function(result) {
          obj = JSON.parse(result);
          if (obj.site_name != "") {
            $("#site_text").html(obj.site_name);
            $("#site_id").val(obj.site_id);
            $("#site_name").val(obj.site_name);
          }
        }
      })
    }


  })
  $(".icon-eye").click(function(e) {

    e.preventDefault();
    //var id = $(this).closest('tr').find('input[type="checkbox"]').val();
    var id = this.id;
    if (id != '') {
      $.ajax({
        type: 'POST',
        url: '<?php echo Yii::app()->createUrl('/dailyWorkProgress/view_wpr'); ?>',
        data: {
          id: id
        },
        success: function(data) {
          $(".view_progress").html(data);
          $('.boq_details_sec').animate({
            height: 'show'
          }, 500);
        }
      })
    }
  })
  $('.progress-table').wrap('<div class="table-responsive"></div>')
</script>
<script>
  $(document).ready(function() {




    $("#zero").on("change", function() {
      var input_val = $("#zero").val();
      $("#DailyWorkProgress_qty_em_").hide();
      if (input_val == 0) {

        alert("Quantity should be greater than 0");
        $("#zero").val("");
      }

     




    });



  });



  $('.mytaskbtn').click(function() {
    $('#mytasks').show();
    sessionStorage.setItem('clicked', true);
    $('.mytaskbtn').hide();
    $('.closemytaskbtn').show();

  });

  $('.closemytaskbtn').click(function() {
    sessionStorage.removeItem('clicked');
    $('#mytasks').hide();
    $('.closemytaskbtn').hide();
    $('.mytaskbtn').show();
  });


  $('.addwpr').click(function() {
    var task_id = this.id;

    $.ajax({
      "type": "POST",

      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/getwprtaskdetails'); ?>",
      "dataType": "json",
      "data": {
        task_id: task_id
      },
      "success": function(data) {
        $.fn.MultiFile();
        $('#wpr_form_div').show();
        $('#wpr_form_div').html(data);
      }
    });

  });

  $(function() {
    $("#datepicker").datepicker({
      maxDate: new Date()
    });
  });

  window.onload = function() {
    var data = sessionStorage.getItem('clicked');
    if (data == 'true') {
      $('#mytasks').show();
      $('.mytaskbtn').hide();
      $('.closemytaskbtn').show();
    }
  };


  $('#consumed_item_id').change(function() {
    var itm_id = $('#consumed_item_id').val();
    $('#DailyWorkProgress_consumed_item_count').val("");
    var project_id = $("#DailyWorkProgress_project_id").val();


    $.ajax({
      "type": "POST",

      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemAvailability'); ?>",
      "dataType": "json",
      "data": {
        itm_id: itm_id,
        project_id: project_id
      },
      "success": function(data) {
        // if (data.status != 1) {
        //   $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
        //   $("#DailyWorkProgress_consumed_item_id_em_").text('This item is not mentioned in template');
        //   $('#consumed_item_id').val("");
        // } else {

        //   $("#DailyWorkProgress_consumed_item_id_em_").hide();
        // }

        if (data.status == 1) {
          $("#DailyWorkProgress_consumed_item_id_em_").hide();
        } else if (data.status == 0) {
          $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
          $("#DailyWorkProgress_consumed_item_id_em_").text('This item is not mentioned in template');
          $('#consumed_item_id').val("");
        } else {
          $("#DailyWorkProgress_consumed_item_id_em_").css("display", "");
          $("#DailyWorkProgress_consumed_item_id_em_").text('No template is defined for this project');
          $('#consumed_item_id').val("");
        }

      }
    });
  })


  $("#DailyWorkProgress_consumed_item_count").change(function() {

    var itm_id = $('#consumed_item_id').val();
    var task_id = $('#DailyWorkProgress_taskid').val();
    var item_count = $('#DailyWorkProgress_consumed_item_count').val();
    var item_rate = $('#consumed_item_rate').val();


    $.ajax({
      "type": "POST",

      "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemCountAvailability'); ?>",
      "dataType": "json",
      "data": {
        itm_id: itm_id,
        task_id: task_id,
        item_count: item_count,
        item_rate: item_rate
      },
      "success": function(data) {

        if (data.status != 0 && data.status != 1) {

          $("#DailyWorkProgress_consumed_item_count_em_").css("display", "");
          $("#DailyWorkProgress_consumed_item_count_em_").text(data.message + ". Avaiable count is " + data.item_required);
          $("#DailyWorkProgress_consumed_item_count").val('');


        }
        if (data.status == 1) {
          $("#DailyWorkProgress_consumed_item_count_em_").css("display", "");
          $("#DailyWorkProgress_consumed_item_count_em_").text(data.message + ". Required count is " + data.item_required);
          $('#item_count_status').val(1);


        }
        if (data.status == 0) {
          $("#DailyWorkProgress_consumed_item_count_em_").hide();
          $('#item_count_status').val("");

        } else {





        }

      }
    });


  })


  $(document).on('click', ".approve_consumed_item", function() {
    if (!confirm("Do you want to Approve ?")) {} else {
      $('.loaderdiv').show();
      var entry_id = this.id;
      $.ajax({
        url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/approve_consumption_item'); ?>',
        data: {
          entry_id: entry_id
        },
        method: "POST",
        dataType: 'json',
        success: function(result) {
          if (result.status == 1) {

            $('.loaderdiv').hide();
            $('.alert').removeClass('hide');
            $('.alert').css('display', 'block');
            $('.alert').append(result.message);
            setTimeout(function() {
              location.reload();
            }, 1000);

          } else {

          }

        }
      })
    }
  });

  $("#consumed_item_id").change(function() {
    var consumed_item_id = $("#consumed_item_id").val();
    var project_id = $("#DailyWorkProgress_project_id").val();
    $.ajax({
      method: "POST",
      data: {
        consumed_item_id: consumed_item_id,
        project_id: project_id
      },
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
      success: function(data) {
        $("#consumed_item_rate").html(data.html);
      }
    });
  });


  $("#consumed_item_rate").change(function() {
    $('#DailyWorkProgress_consumed_item_count').val("")
  });


  $(document).on('click', "#submit_boq", function() {

    var project_id = $('#DailyWorkProgress_project_id').val();
    var date = $('#DailyWorkProgress_date').val();
    var task_id = $('#DailyWorkProgress_taskid').val();
    var qty = $('#zero').val();
    var work_type = $('#DailyWorkProgress_work_type').val();
    var description = $('#DailyWorkProgress_description').val();
    var formData = new FormData($("#daily-work-progress-form")[0]);


    if (project_id == "") {

      $("#DailyWorkProgress_project_id_em_").css("display", "");
      $("#DailyWorkProgress_project_id_em_").text("Please Select Project");
    }
    if (date == "") {

      $("#DailyWorkProgress_date_em_").css("display", "");
      $("#DailyWorkProgress_date_em_").text("Please Enter Date");
    }
    if (task_id == "") {

      $("#DailyWorkProgress_taskid_em_").css("display", "");
      $("#DailyWorkProgress_taskid_em_").text("Please Enter Task");
    }
    if (qty == "") {

      $("#DailyWorkProgress_qty_em_").css("display", "");
      $("#DailyWorkProgress_qty_em_").text("Please Enter Quantity");
    }
    if (work_type == "") {

      $("#DailyWorkProgress_work_type_em_").css("display", "");
      $("#DailyWorkProgress_work_type_em_").text("Please Enter WorkType");
    }
    if (description == "") {
      $("#DailyWorkProgress_description_em_").css("display", "");
      $("#DailyWorkProgress_description_em_").text("Please Enter Description");
    } else {

      $.ajax({
        type: "POST",
        "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/create_daily_wpr'); ?>",
        "dataType": "json",
        "data": formData,
        "cache": false,
        "contentType": false,
        "processData": false,
        "beforeSend": function() {
          $('#process').css('display', 'block');

        },

        "success": function(data) {
          var percentage = 0;


          var timer = setInterval(function() {
            percentage = percentage + 20;

            progress_bar_percentage(percentage, timer, data);

          }, 1000);


        }
      });

    }



    function progress_bar_percentage(percentage, timer, data) {


      $('.progress-bar').css('width', percentage + '%');
      if (percentage > 100) {
        clearInterval(timer);
        $('#process').css('display', 'none');
        $('.progress-bar').css('width', '0%');
        $('#alertMessage').show();
        $('#daily-work-progress-form').reset();
        $('#task_id_span1').html('');
        $('red').text('');

        $('.work_progress_form').slideUp();

        $('#alertMessage').html(data.message)
        $("#success_message").fadeIn().delay(1000).fadeOut();

        setTimeout(function() {
          window.location.reload(1);
        }, 1000);


      }
    }

  });
  var count = 0;

  function addInput() {

    var project_id = $('#DailyWorkProgress_project_id').val();


    count += 1;
    $('#container').append(

      `
<div class="row remove-div_` + count + `">

                                    <div class=" col-md-2 col-sm-6 ">
                                        <?php echo $form->labelEx($model, 'Item Consumed'); ?>

                                        <select class="form-control itemid" id="consumed_item_id_` + count + `" name="DailyWorkProgress[consumed_item_id][]">
                                            <option></option>

                                        </select>

                                        <div class="errorMessage" id="DailyWorkProgress_consumed_item_id_em_` + count + `" style="display:none"></div>
                                        <div class="errorMessage display-none"></div>
                                    </div>
                                    <div class=" col-md-2 col-sm-6 ">

                                        <label>Rate</label>
                                        <select class="form-control" id="consumed_item_rate_` + count + `" name="DailyWorkProgress[consumed_item_rate][]">
                                            <option></option>

                                        </select>


                                    </div>



                                    <div class=" col-md-2 col-sm-6 ">

                                       

                                       <?php echo $form->labelEx($model, 'Item Consumed'); ?>
                                       
                                        <input type="text" name="DailyWorkProgress[consumed_item_count][]" id="DailyWorkProgress_consumed_item_count_` + count + `" class="form-control" >

                                        <div class="errorMessage" id="DailyWorkProgress_consumed_item_count_em_` + count + `" style="display:none"></div>

                                    </div>


                        <!-- remove button -->
                        <div class=" col-md-2 col-sm-6 padding-16 margin-top-15">
                       
                        <input type="button" id="` + count + `" value="Remove" class="remove-btn">
                        </div>
                        <!-- end remove button -->

                                  
                                    </div>
                                   
                                    
                                    
`

    );
    $.ajax({
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/getWarehouseItems'); ?>',
      data: {
        project_id: project_id,

      },
      method: "POST",
      success: function(result) {

        $("#consumed_item_id_" + count).html(result.html);
      }
    })

    // get item rate

    $('#consumed_item_id_' + count).change(function() {

      var consumed_item_id = $("#consumed_item_id_" + count).val();
      var project_id = $("#DailyWorkProgress_project_id").val();
      $.ajax({
        method: "POST",
        data: {
          consumed_item_id: consumed_item_id,
          project_id: project_id
        },
        "dataType": "json",
        url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getItemRate'); ?>',
        success: function(data) {
          $("#consumed_item_rate_" + count).html(data.html);
        }
      });

      // item estimated or not

      $.ajax({
        "type": "POST",

        "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemAvailability'); ?>",
        "dataType": "json",
        "data": {
          itm_id: consumed_item_id,
          project_id: project_id
        },
        "success": function(data) {
          // if (data.status != 1) {
          //   $("#DailyWorkProgress_consumed_item_id_em_" + count).css("display", "");
          //   $("#DailyWorkProgress_consumed_item_id_em_" + count).text('This item is not mentioned in template');
          //   $('#consumed_item_id_' + count).val("");
          // } else {

          //   $("#DailyWorkProgress_consumed_item_id_em_" + count).hide();
          // }

          if (data.status == 1) {
            $("#DailyWorkProgress_consumed_item_id_em_" + count).hide();
          } else if (data.status == 0) {
            $("#DailyWorkProgress_consumed_item_id_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_id_em_" + count).text('This item is not mentioned in template');
            $('#consumed_item_id_' + count).val("");
          } else {
            $("#DailyWorkProgress_consumed_item_id_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_id_em_" + count).text('No template is defined for this project');
            $('#consumed_item_id_' + count).val("");
          }

        }
      });


    });

    // reset count
    $('#consumed_item_rate_' + count).change(function() {
      $('#DailyWorkProgress_consumed_item_count_' + count).val("");
    });




    $("#DailyWorkProgress_consumed_item_count_" + count).change(function() {

      var itm_id = $('#consumed_item_id_' + count).val();
      var task_id = $('#DailyWorkProgress_taskid').val();
      var item_count = $('#DailyWorkProgress_consumed_item_count_' + count).val();
      var item_rate = $('#consumed_item_rate_' + count).val();



      $.ajax({
        "type": "POST",

        "url": "<?php echo Yii::app()->createUrl('dailyWorkProgress/checkItemCountAvailability'); ?>",
        "dataType": "json",
        "data": {
          itm_id: itm_id,
          task_id: task_id,
          item_count: item_count,
          item_rate: item_rate
        },
        "success": function(data) {

          if (data.status != 0 && data.status != 1) {

            $("#DailyWorkProgress_consumed_item_count_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_count_em_" + count).text(data.message + ". Available count is " + data.item_required);
            $("#DailyWorkProgress_consumed_item_count_" + count).val('');

          }

          if (data.status == 1) {
            $("#DailyWorkProgress_consumed_item_count_em_" + count).css("display", "");
            $("#DailyWorkProgress_consumed_item_count_em_" + count).text(data.message + ". Avaiable count is " + data.item_required);
            $('#item_count_status').val(1);

          }
          if (data.status == 0) {
            $("#DailyWorkProgress_consumed_item_count_em_" + count).hide();
            $('#item_count_status').val("");
          } else {





          }

        }
      });


    })

    $(document).on('click', '.remove-btn', function() {
      id = this.id;
      $(".remove-div_" + id).remove();
    });



  } //addInput close 



  $(document).on('change', '.itemid', function() {

    var $t = $(this);
    var $column = $(this);
    var value = $('#consumed_item_id').val();
    var val = $t.val();
    var z = 0;

    $(".itemid").each(function() {
      var y = $(this).val();
      if (val == y) {
        z = z + 1;
      }
      if (value == y) {
        z = z + 1;
      }
    });

    if (z > 1) {
      $column.siblings('.errorMessage').show().html('Already Exist!').fadeOut(1500);
      $column.val('');
      return false;
    }




  });
</script>


<script>
  function addLabel() {
    var count = $('.MultiFile-applied').length;

    $('#label_container').append(`

    <div class="col-md-2 col-sm-6 form-group">
    <label for="DailyWorkProgress_image_label">Image Label ` + count + `</label>
    <input class="form-control" autocomplete="off" name="DailyWorkProgress[image_label][]" id="DailyWorkProgress_image_label_` + count + `" type="text">
    <div class="errorMessage" id="DailyWorkProgress_image_label_em_` + count + `" style="display:none"></div>
                    </div>

    `);

    var lable_id = $('#DailyWorkProgress_image_label_' + count).val();


  }
</script>


<script type="text/javascript">
  $(".popup").hide();
  $(".consumed-item").on("mouseover", function() {

    var id = $(this).attr("id");
    var elem = $(this);

    $.ajax({
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('dailyWorkProgress/ItemEstimatiomMsg'); ?>',
      data: {
        id: id,

      },
      method: "POST",
      success: function(result) {

        // $(elem).parent("td").attr('title',result);
        $(elem).siblings('.popup').html(result).show();
        (elem).parents("tr").siblings("tr").find('.popup').html('').hide();


      }
    })
  });


  $("#resources_used").change(function() {
    var resources_used = $("#resources_used").val();

    $.ajax({
      method: "POST",
      data: {
        resources_used: resources_used,

      },
      "dataType": "json",
      url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getResourceUnit'); ?>',
      success: function(data) {

        $('#uom').val(data.unit);
        $('#unit_id').val(data.unit_id);
      }
    });
  });

  var resource_count = 0;

  function addResources() {
    resource_count += 1;



    $('#resources_container').append(
      `
        <div class="row remove-resource-div_` + resource_count + `">
        <div class=" col-md-2 col-sm-6 ">

<label>Resource utilised</label>
<select class="form-control" id="resources_used_` + resource_count + `" name="DailyWorkProgress[resources_used][]">
  <option value="">Choose a resource</option>
  <?php
  foreach ($resource_utilised as $resources) {
  ?>
    <option value="<?php echo $resources->id ?>"><?php echo $resources->resource_name ?></option>
  <?php
  }
  ?>

</select>


</div>

<div class=" col-md-2 col-sm-6 ">

<label>Quantity</label>
<input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[utilised_qty][]" type="text">

</div>

<div class=" col-md-2 col-sm-6 ">



<label>UOM</label>
<input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[uom][]" type="text" id="uom_` + resource_count + `" readonly>





</div>





                   
                    <input class="form-control img_comp_class" autocomplete="off" name="DailyWorkProgress[unit_id][]" type="hidden" id="unit_id_` + resource_count + `" >


                   



                  <!-- remove button -->
                        <div class=" col-md-2 col-sm-6 padding-16 margin-top-15">
                       
                        <input type="button" id="` + resource_count + `" value="Remove" class="remove-resource-btn">
                        </div>
                        <!-- end remove button -->

</div>
        
        `
    );


    $("#resources_used_" + resource_count).change(function() {
      var resources_used = $("#resources_used_" + resource_count).val();

      $.ajax({
        method: "POST",
        data: {
          resources_used: resources_used,

        },
        "dataType": "json",
        url: '<?php echo Yii::app()->createUrl('DailyWorkProgress/getResourceUnit'); ?>',
        success: function(data) {

          $('#uom_' + resource_count).val(data.unit);
          $('#unit_id_' + resource_count).val(data.unit_id);
        }
      });
    });

    $(document).on('click', '.remove-resource-btn', function() {
      id = this.id;
      $(".remove-resource-div_" + id).remove();
    });



  } //addResources end

  $("#DailyWorkProgress_current_status").change(function() {
    var status = $("#DailyWorkProgress_current_status").val();
    var balance_quantity = $('#balance_quantity_id').val();
    if (status == 7) {

      if (balance_quantity != "") {

        if (balance_quantity != 0) {
          $("#DailyWorkProgress_current_status").val("");
          $("#DailyWorkProgress_current_status_em_").css("display", "");
          $("#DailyWorkProgress_current_status_em_").text("The balance quantity should be 0 if the status is to be 'completed'");

        }
        if(balance_quantity==0)
        {
          $("#DailyWorkProgress_current_status_em_").hide();
        }


      }

    } else {

      $("#DailyWorkProgress_current_status_em_").hide();

      if (balance_quantity == 0) {
        $("#DailyWorkProgress_current_status").val("");
        $("#DailyWorkProgress_current_status_em_").css("display", "");
        $("#DailyWorkProgress_current_status_em_").text("If the balance quantity is 0 then the status should be 'completed'");
      }
    }
  });

  $("#DailyWorkProgress_work_type").change(function() {
    $("#DailyWorkProgress_work_type_em_").hide();
  });
  $("#DailyWorkProgress_description").change(function()
  {
    $("#DailyWorkProgress_description_em_").hide();
  });
</script>