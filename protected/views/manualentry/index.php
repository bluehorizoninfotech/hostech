<?php
/* @var $this ManualentryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manualentries',
);

$this->menu=array(
	array('label'=>'Create Manualentry', 'url'=>array('create')),
	array('label'=>'Manage Manualentry', 'url'=>array('admin')),
);
?>

<h1>Manualentries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
