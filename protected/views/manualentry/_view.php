<?php
/* @var $this ManualentryController */
/* @var $data Manualentry */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_punch')); ?>:</b>
	<?php echo CHtml::encode($data->first_punch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_punch')); ?>:</b>
	<?php echo CHtml::encode($data->last_punch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_out_time')); ?>:</b>
	<?php echo CHtml::encode($data->total_out_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('added_date')); ?>:</b>
	<?php echo CHtml::encode($data->added_date); ?>
	<br />


</div>