<?php
/* @var $this ManualentryController */
/* @var $model Manualentry */

$this->breadcrumbs=array(
	'Manualentries'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('manualentry-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manual Punch Report</h1>


<div class="add link pull-left">
    <?php
    $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
    echo CHtml::link('Add New Entry', '', array('class'=>'btn blue','onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
    ?>
</div>  
<br>
<br>


   <?php
//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
   'id' => 'cru-dialog',
   'options' => array(
       'title' => 'Add Manual Entry',
       'autoOpen' => false,
       'modal' => false,
       'width' => 420,
       'height' => 520,
   ),
));
?>
<iframe id="cru-frame" width="390" height="auto" frameborder="0"  class="min-height-420"></iframe>

<?php
$this->endWidget();
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manualentry-grid',
	'dataProvider'=>$model->search(),
        'itemsCssClass' => 'table table-bordered',
	'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
        'nextPageLabel'=>'Next ' ),    
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	'filter'=>$model,
	'columns'=>array(
		/*'id',*/
		array(
			'name' => 'user_id', 
			'value'=> '$data->user->first_name." ".$data->user->last_name',
			'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,CONCAT_WS(" ", first_name, last_name) as first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
			),
                        array(
			'name' => 'site_id', 
			'value'=> '$data->site->site_name',
			'filter' => CHtml::listData(Clientsite::model()->findAll(
                            array(
                                'select' => array('id,site_name'),
                                'order' => 'site_name',
                                'distinct' => true
                    )), "id", "site_name")
			),
		'first_punch',
		'last_punch',
		'total_out_time',
		'added_date',
		array(
			'class'=>'CButtonColumn',
                        'template' => '{update}{delete}',
		),
	),
)); ?>
