<?php
/* @var $this ManualentryController */
/* @var $model Manualentry */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manualentry-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="">Fields with <span class="required">*</span> are required.</p>

	
        
        <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
                <br>
		 <?php echo $form->dropDownList($model, 'user_id', CHtml::listData(Users::model()->findAll(array(
                                           'select' => array('userid,CONCAT_WS(" ", first_name, last_name) as first_name'),
                                          // 'condition' => 'status=0',
                                           'order' => 'first_name',
                                           'distinct' => true
                               )), 'userid', 'first_name'), array( 'class' => 'width-150', 'empty' => '--')); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'site_id'); ?>
                <br>
		  <?php echo $form->dropDownList($model, 'site_id', CHtml::listData(Clientsite::model()->findAll(array(
                                           'select' => array('id,site_name'),
                                          // 'condition' => 'status=0',
                                           'order' => 'site_name',
                                           'distinct' => true
                               )), 'id', 'site_name'), array( 'class' => 'width-150', 'empty' => '--')); ?>
		<?php echo $form->error($model,'site_id'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'first_punch'); ?>
                <br>
		
                <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        
        'model'=>$model, //Model object
        'attribute'=>'first_punch', //attribute name
                'mode'=>'time', //use "time","date" or "datetime" (default)
        'options'=>array(
             
        ), // jquery plugin options
        'htmlOptions'=>array(
        'readonly' => true,
    ),

    ));
?>	
		<?php echo $form->error($model,'first_punch'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'last_punch'); ?>
                <br>
		 <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'last_punch', //attribute name
                'mode'=>'time', //use "time","date" or "datetime" (default)
        'options'=>array(), // jquery plugin options
        'htmlOptions'=>array(
        'readonly' => true,
    ),
    ));
?>	
		<?php echo $form->error($model,'last_punch'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'total_out_time'); ?>
                <br>
		 <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'total_out_time', //attribute name
                'mode'=>'time', //use "time","date" or "datetime" (default)
        'options'=>array(), // jquery plugin options
        'htmlOptions'=>array(
        'readonly' => true,
    ),
    ));
?>	
		<?php echo $form->error($model,'total_out_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'added_date'); ?>
                <br>
		<?php echo $form->error($model,'customdate'); ?>
    <?php
    $model->added_date = date('Y-m-d');
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'added_date', //attribute name
        'language'=>'en-AU',
                'mode'=>'date', //use "time","date" or "datetime" (default)
        'options'=>array(
        'dateFormat'=>'yy-mm-dd',
        'maxDate' => 'today',
            ), // jquery plugin options
        
        'htmlOptions'=>array(
        'readonly' => true,
    ),
        
    ));
?>  
		<?php echo $form->error($model,'added_date'); ?>
	</div>
        
        
<?php
 if($model->isNewRecord){ 
echo $form->hiddenField($model, 'created_date',array('value'=>date('Y-m-d'))); 
echo $form->hiddenField($model, 'created_by',array('value'=>Yii::app()->user->id));        
echo $form->hiddenField($model, 'updated_date',array('value'=>date('Y-m-d'))); 
echo $form->hiddenField($model, 'updated_by',array('value'=>Yii::app()->user->id)); 
 }else{ 
echo $form->hiddenField($model, 'updated_date',array('value'=>date('Y-m-d'))); 
echo $form->hiddenField($model, 'updated_by',array('value'=>Yii::app()->user->id)); 
 }
?>
        
        <br>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->