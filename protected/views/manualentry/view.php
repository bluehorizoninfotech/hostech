<?php
/* @var $this ManualentryController */
/* @var $model Manualentry */

$this->breadcrumbs=array(
	'Manualentries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Manualentry', 'url'=>array('index')),
	array('label'=>'Create Manualentry', 'url'=>array('create')),
	array('label'=>'Update Manualentry', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Manualentry', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Manualentry', 'url'=>array('admin')),
);
?>

<h1>View Manualentry #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'first_punch',
		'last_punch',
		'total_out_time',
		'added_date',
	),
)); ?>
