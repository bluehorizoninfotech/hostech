<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs = array(
    'Material Requisitions' => array('index'),
    $model->requisition_id => array('view', 'id' => $model->requisition_id),
    'Update',
);

$this->menu = array(
    array('label' => 'List MaterialRequisition', 'url' => array('index')),
    array('label' => 'Create MaterialRequisition', 'url' => array('create')),
    array('label' => 'View MaterialRequisition', 'url' => array('view', 'id' => $model->requisition_id)),
    array('label' => 'Manage MaterialRequisition', 'url' => array('admin')),
);
?>

<h1>Update <?php echo "#" . $model->requisition_no; ?></h1>

<?php echo $this->renderPartial('_update_form', array(
    'model' => $model,
    'project_name' => $project_name,
    'project_id' => $project_id,
    'task_name' => $task_name,
    'task_id' => $task_id,
    'account_items' => $account_items,
    'unit_list' => $unit_list,
    'material_req_item_arr' => $material_req_item_arr,
    'items_model' => $items_model,
    'id' => $id
)); ?>