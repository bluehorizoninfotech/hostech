<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'requisition_id'); ?>
		<?php echo $form->textField($model,'requisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_project_id'); ?>
		<?php echo $form->textField($model,'requisition_project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_task_id'); ?>
		<?php echo $form->textField($model,'requisition_task_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_no'); ?>
		<?php echo $form->textField($model,'requisition_no',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_item'); ?>
		<?php echo $form->textField($model,'requisition_item',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_unit'); ?>
		<?php echo $form->textField($model,'requisition_unit',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_quantity'); ?>
		<?php echo $form->textField($model,'requisition_quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_date'); ?>
		<?php echo $form->textField($model,'requisition_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_status'); ?>
		<?php echo $form->textField($model,'requisition_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->