<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs = array(
    'Material Requisitions' => array('index'),
    $model->requisition_id,
);

$this->menu = array(
    array('label' => 'List MaterialRequisition', 'url' => array('index')),
    array('label' => 'Create MaterialRequisition', 'url' => array('create')),
    array('label' => 'Update MaterialRequisition', 'url' => array('update', 'id' => $model->requisition_id)),
    array('label' => 'Delete MaterialRequisition', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->requisition_id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage MaterialRequisition', 'url' => array('admin')),
);
?>

<h1>View Material Requisition #<?php echo $model->requisition_no; ?></h1>
<div>
    <div class="media">

        <div class="media-body">
            <div class="row">

                    <div class="col-md-4"><b>Requisition No:</b><span><?php echo $model->requisition_no ?></span></div>
                    <div class="col-md-4"><b> Project:</b><span><?= $model->requisitionProject->name ?></span></div>
                    <div class="col-md-4"><b> Task:</b><span><?php echo $model->requisitionTask->title ?></span></div>

               
                    <div class="col-md-4"><b>Staus:</b><span><?php echo ($model->requisition_status == 0) ? "Pending" : "Completed"; ?></span></div>
                    <div class="col-md-4"><b> Created By:</b><span><?= $model->requisitionCreated->first_name . " " . $model->requisitionCreated->last_name ?></span></div>
                    <div class="col-md-4"><b> Created Date:</b><span><?php echo  date('d-M-y', strtotime($model->created_date)); ?></span></div>
               

            </div>
        </div>
    </div>
</div>
<br><br>
<hr />
<div class="row">
    <div class="col-md-6">
        <table cellpadding="3" cellspacing="0" class="timehours table-bordered">
            <thead>
                <tr class="tr_title">
                    <th>Sl no</th>
                    <th>ITEM</th>
                    <th>UNIT</th>
                    <th>QUANTITY</th>
                    <th>DATE</th>
                    <th> REMARKS</th>
                </tr>
            </thead>
            <?php
            foreach ($material_req_item_arr as $key => $item) {
            ?>
                <tr>
                    <td width="5%"><?= $key + 1 ?></td>
                    <td width="30%"><?= $item['item_name'] ?></td>
                    <td width="10%"><?= $item['item_unit'] ?></td>
                    <td width="10%"><?= $item['item_quantity'] ?></td>
                    <td width="15%"><?= date('d-M-y', strtotime($item['item_req_date'])) ?></td>
                    <td width="35%"><?= $item['item_remarks'] ?></td>


                </tr>
            <?php
            }

            ?>

        </table>
    </div>

</div>