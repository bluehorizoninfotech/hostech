<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */
/* @var $form CActiveForm */
?>
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

<div class="material-requisition-sec">
<div class="form">
    <p id="success_message" class=" font-15 green-color font-weight-bold">Successfully created..</p>
    <div class="row">
        <div class="col-lg-12 newform">
            <div class="row">
                
                <div class="col-md-4">
                    <label>Project</label>
                    <input type="text" class="form-control" readonly value="<?= $project_name ?>">

                    <input type="hidden" value="<?= $project_id ?>" name="MaterialRequisition[project_id]" id="project_id">
                </div>
                <div class="col-md-4">
               

                    <label>Task</label>
                    <input type="text" value=" <?= $task_name ?>"  id="requisition_task_id" class="form-control" readonly>
                    <input type="hidden" value="<?= $task_id ?>" name="MaterialRequisition[task_id]" id="task_id">
                </div>
                <div class="col-md-4">


                    <label>Req.No</label>
                    <input type="text" id="requisition_no" value="<?= $requisition_no ?>" readonly class="form-control">
                </div>
            </div>
            <div class="text-center">
                <input type="button" class="btn btn-primary" value="Create" id="create_mr">
            </div>

            <div id="hide">
                <div clas="row">
                    <div class="col-md-3">

                        <label class="required">Item <span class="required">*</span></label>
                        <input type="hidden" id="material_req_id">
                        <input type="hidden" id="type" name="type" value=<?= $type ?>>
                        <!-- <input type="text" class="form-control" id="requisition_item"> -->
                        <select class="form-control req-item js-example-basic-multiple" id="requisition_item" >
                            <option value="">Select Item</option>
                            <?php
                            foreach ($account_items as $key => $value) {
                            ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                            <?php } ?>
                            <option value="other">Other</option>
                        </select>
                        <div class="errorMessage" id="item_em_"></div>
                    </div>
                    <div class="col-md-3" id="item_name_id">
                        <label>Item Name</label>
                        <input type="text" class="form-control" id="requisition_item_name">
                        <div class="errorMessage" id="item_name_em_"></div>

                    </div>
                    <div class="col-md-3">

                        <label>Unit <span class="required">*</span></label>
                        <!-- <input type="text" class="form-control" id="requisition_unit"> -->
                        <select class="form-control" id="requisition_unit">
                            <option value="">Select Unit</option>

                        </select>
                        <div class="errorMessage" id="unit_em_"></div>
                    </div>
                    <div class="col-md-3">
                        <label>Quantity<span class="required">*</span></label>
                        <input type="number" class="form-control" id="requisition_quantity">
                        <div class="errorMessage" id="quantity_em_"></div>

                    </div>
                    <div class="col-md-3">
                        <label>Date<span class="required">*</span></label>
                        <!-- <input type="date" class="form-control" id="requisition_date"> -->
                        <input type="text" id="requisition_date" class="form-control">
                        <div class="errorMessage" id="date_em_"></div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">

                        <label>Remark<span class="required">*</span></label>
                        <input type="text" class="form-control"  id="requisition_remarks">
                        <div class="errorMessage" id="remarks_em_"></div>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="col-md-12 text-center">
                        <!-- <?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save', array('class' => 'btn btn-primary')); ?> -->
                    </div>
                </div>

                <div class="col-md-12 text-center">
                    <input type="button" class="btn blue" value="Add" id="add_item">
                </div>
            </div> <!-- hide div close -->

            <div id="table_div">
            </div>


            <div class="row buttons" id="create_cancel">
                <div class="col-md-12 text-center">
                    <?php
                    if($type==1)
                    {
                        echo CHtml::link('Create', array('tasks/index2'), array('class' => 'btn blue'));
                    }
                    else
                    {
                        echo CHtml::link('Create', array('DailyWorkProgress/index'), array('class' => 'btn blue')); 
                    }
                    
                    ?>
                    <input type="button" class="btn default" value="Cancel" id="cancel_mr">

                </div>
            </div>



        </div>
    </div>





</div><!-- form -->

<!-- main table -->
                </div>
<script>
    $("#create_mr").click(function() {
        var req_no = $('#requisition_no').val();
        var project_id = $('#project_id').val();
        var task_id = $('#task_id').val();
        var type=$('#type').val();
        
        $.ajax({
            "type": "POST",
            "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/create', array('id' => $task_id,'type'=>$type,)); ?>",
            "dataType": "json",
            "data": {
                req_no: req_no,
                project_id: project_id,
                task_id: task_id
            },

            "success": function(data) {
                $('#hide').show();
                $('#create_mr').hide();

                $('#material_req_id').val(data.mr_id);

            }
        });

    });

    $("#add_item").click(function() {
        var requisition_item = $('#requisition_item').val();
        var requisition_unit = $('#requisition_unit').val();
        var requisition_quantity = $('#requisition_quantity').val();
        var requisition_date = $('#requisition_date').val();
        var requisition_remarks = $('#requisition_remarks').val();
        var material_req_id = $('#material_req_id').val();
        var requisition_item_name = $('#requisition_item_name').val();

        if (requisition_item != "" && requisition_unit != "" && requisition_quantity != "" && requisition_date != "" && requisition_remarks != "") {
            $("#item_em_").hide();
            $("#unit_em_").hide();
            $("#quantity_em_").hide();
            $("#date_em_").hide();
            $("#remarks_em_").hide();
            $.ajax({
                "type": "POST",

                "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/adddItem'); ?>",
                "dataType": "json",
                "data": {
                    requisition_item: requisition_item,
                    requisition_unit: requisition_unit,
                    requisition_quantity: requisition_quantity,
                    requisition_date: requisition_date,
                    requisition_remarks: requisition_remarks,
                    material_req_id: material_req_id,
                    requisition_item_name: requisition_item_name
                },

                "success": function(data) {
                    $("#success_message").fadeIn().delay(1000).fadeOut();
                    $('#requisition_item').val("");
                    $('#requisition_unit').val("");
                    $('#requisition_quantity').val("");
                    $('#requisition_date').val("");
                    $('#requisition_remarks').val("");
                    $('#table_div').html(data);
                    $('#create_cancel').show();
                }
            });
        } else {
            if (requisition_item == "") {
                $("#item_em_").show();
                $("#item_em_").html('Item cannot be blank.');
            }
            if (requisition_unit == "") {
                $("#unit_em_").show();
                $("#unit_em_").html('Unit cannot be blank.');
            }
            if (requisition_quantity == "") {
                $("#quantity_em_").show();
                $("#quantity_em_").html('Quantity cannot be blank.');
            }
            if (requisition_date == "") {
                $("#date_em_").show();
                $("#date_em_").html('Date cannot be blank.');
            }
            if (requisition_remarks == "") {
                $("#remarks_em_").show();
                $("#remarks_em_").html('Remark cannot be blank.');
            }
            if (requisition_item_name == "") {
                $('#item_name_em_').show();
                $("#item_name_em_").html('item name cannot be blank.');
            }
        }




    });

    $("#cancel_mr").click(function() {
        if (!confirm('Are you sure you want to cancel this MR?') === false) {
            var mr_id = $('#material_req_id').val();
            $.ajax({
                "type": "POST",

                "url": "<?php echo Yii::app()->createUrl('MaterialRequisition/deleteMR'); ?>",
                "dataType": "json",
                "data": {
                    mr_id: mr_id,
                },
                "success": function(data) {

                    location.reload();
                }
            });
        }

    });


//     $('#requisition_item').change(function() {
// alert(5);
//     });

$(document).ready(function(){
    $('#requisition_item').on("select2:select", function(e) { 
        var item_id = $('.req-item').val();
        if (item_id == 'other') {
            $('#item_name_id').show();
        } else {
            $('#item_name_id').hide();
        }

          $.ajax({
            url: '<?php echo Yii::app()->createUrl('MaterialRequisition/getRelatedUnit'); ?>',
            method: 'POST',
            data: {
                item_id: item_id
            },
            dataType: "json",
            success: function(response) {
                $("#requisition_unit").html(response.html);
            }
        })
        
    });
   
    $('#hide').hide();
    $('#create_cancel').hide();
    $('#success_message').hide();
    $('#item_em_').hide();
    $('#item_name_id').hide();
    $('#item_name_em_').hide();
    $('#unit_em_').hide();
    $('#quantity_em_').hide();
    $('#date_em_').hide();
    $('#remarks_em_').hide();
});
 
  

    // $('.req-item').change(function() {
    //     alert(1);
    //     var item_id = $('.req-item').val();
    //     if (item_id == 'other') {
    //         $('#item_name_id').show();
    //     } else {
    //         $('#item_name_id').hide();
    //     }


    //     $.ajax({
    //         url: '<?php echo Yii::app()->createUrl('MaterialRequisition/getRelatedUnit'); ?>',
    //         method: 'POST',
    //         data: {
    //             item_id: item_id
    //         },
    //         dataType: "json",
    //         success: function(response) {
    //             $("#requisition_unit").html(response.html);
    //         }
    //     })
    // })

    $('#requisition_date').datepicker({
        dateFormat: 'dd-M-y',
    });
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            width: '100%',
            placeholder: "Select Material",
        });

    });
</script>