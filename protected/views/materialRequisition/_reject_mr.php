<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

?>

<div class="reject-mr-sec">
<div class="form">


    <div class="alert display-none">
        <?php
        if (Yii::app()->user->hasFlash('error')) {
            echo Yii::app()->user->getFlash('error');
        }
        ?>
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>

    <div class="row">
        <div class="subrow">
            <input type="hidden" value=<?= $id ?> id="mr_id">
            <input type="text" class="form-control" id="reason">
            <div class="errorMessage display-none" id="reason_em_"></div>
        </div>
    </div>

    <div class="row">


        <input type="button" class="btn blue" value="Reject" id="reject_mr">

    </div>



</div><!-- form -->
    </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('#reject_mr').click(function() {

            var reason = $('#reason').val();
            var mr_id = $('#mr_id').val();

            if (reason != "" && mr_id != "") {
                $.ajax({
                    "type": "POST",

                    "url": "<?php echo Yii::app()->createUrl('materialRequisition/reject_mr'); ?>",
                    "dataType": "json",
                    "data": {
                        reason: reason,
                        mr_id: mr_id
                    },
                    "success": function(data) {


                        alert(data.message);
                        window.location.reload();


                    }
                });
            } else {
                $("#reason_em_").show();
                $("#reason_em_").html('reason cannot be blank.');
            }


        });




    });
</script>