<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this MaterialRequisitionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Material Requisitions',
);

$this->menu = array(
    array('label' => 'Create MaterialRequisition', 'url' => array('create')),
    array('label' => 'Manage MaterialRequisition', 'url' => array('admin')),
);
?>
<div class="materialrequistion-sec">
    <h1>Material Requisitions</h1>
    <div class="alert <?= Yii::app()->user->hasFlash('error') ? "" : "hide" ?>">
        <?php
        if (Yii::app()->user->hasFlash('error')) {
            echo Yii::app()->user->getFlash('error');
        }
        ?>
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>


    <div id="reject_form" data-toggle="modal" data-target="#reviewLoginModal" class="modal">

    </div>





    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'material-requisition-grid',
        'dataProvider' => $model->search(),
        'ajaxUpdate' => false,
        'itemsCssClass' => 'table table-bordered',
        'filter' => $model,
        'template' => '<div class="table-responsive">{items}</div>',


        'pager' => array(
            'id' => 'dataTables-example_paginate',
            'header' => '',
            'prevPageLabel' => 'Previous ',
            'nextPageLabel' => 'Next '
        ),
        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(

            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'), ),

            
            array(
                'htmlOptions' => array('class' => 'width-100'),
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{approve_btn}{reject_btn}',
                'evaluateID' => true,

                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View'),
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '$data->requisition_status==0',
                        'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Edit', ),
                    ),
                    'approve_btn' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '($data->requisition_status=="0") && in_array("/materialRequisition/approveentry", Yii::app()->session["menuauthlist"])',
                        'options' => array('class' => 'fa fa-thumbs-up approve', 'title' => 'Approve', 'id' => '$data->requisition_id'),
                    ),

                    'reject_btn' => array(
                        'label' => '',
                        'imageUrl' => false,
                        'visible' => '($data->requisition_status=="0") && in_array("/materialRequisition/rejectentry", Yii::app()->session["menuauthlist"])',
                        'options' => array('class' => 'fa fa-thumbs-down margin_5 reject', 'title' => 'Reject', 'id' => '$data->requisition_id'),
                    ),


                )

            ),
            array(
                'name' => 'requisition_no',
                'value' => '$data->requisition_no',
                'type' => 'raw',

            ),
            array(
                'name' => 'requisition_project_id',
                'value' => '$data->requisitionProject?$data->requisitionProject->name:""',
                'type' => 'raw',
                'filter' => CHtml::listData(Projects::model()->findAll(
                    array(
                        'select' => array('pid,name'),
                        'order' => 'name',
                        'distinct' => true
                    )
                ), "pid", "name")

            ),
            
            array(
                'name' => 'requisition_task_id',
                'value' => '$data->requisitionTask?$data->requisitionTask->title:""',
                'type' => 'raw',
                'filter' => CHtml::listData(Tasks::model()->findAll(
                    array(
                        'select' => array('tskid,title'),
                        'order' => 'title',
                        'distinct' => true
                    )
                ), "tskid", "title"),
            ),


            array(
                'name' => 'requisition_status',
                // 'value' => function ($data) {
                //     if ($data->requisition_status == 0) {
                //         return "Not Approved";
                //     } elseif ($data->requisition_status == 1) {
                //         return "Approved";
                //     } elseif ($data->requisition_status == 2) {
                //         return "Rejected";
                //     }
                // },
                'value' => function ($data) {
                    if($data->requisition_id){
                        echo $data->mrStatus($data->requisition_id);
                    }
            },
                'filter' => false

            ),



        ),
    )
    );
    ?>
</div>




<script>
    $(document).on('click', ".approve", function () {
        if (!confirm("Do you want to Approve ?")) { } else {

            var id = this.id;
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('materialRequisition/approveentry'); ?>',
                data: {
                    id: id
                },
                method: "POST",
                dataType: 'json',
                success: function (result) {

                    if (result.status == 1) {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(result.message);
                        setTimeout(location.reload.bind(location), 1000);
                    } else {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(result.message);
                    }
                }
            })
        }
    });

    $(document).on('click', ".reject", function () {
        var id = this.id;
        $.ajax({
            "type": "POST",

            "url": "<?php echo Yii::app()->createUrl('materialRequisition/reject_mr'); ?>",
            "dataType": "json",
            "data": {
                id: id
            },
            "success": function (data) {
                $("#reject_form").html(data).dialog({
                    title: "Reject MR"
                });
            }
        });
    });
    $(document).on('click', ".ui-dialog-titlebar-close", function () {

        window.location.reload();
    });
</script>