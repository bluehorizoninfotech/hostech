<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs = array(
    'Material Requisitions' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List MaterialRequisition', 'url' => array('index')),
    array('label' => 'Manage MaterialRequisition', 'url' => array('admin')),
);
?>

<h1>Create MaterialRequisition</h1>

<?php echo $this->renderPartial('_form', array(
    'model' => $model, 'task_name' => $task_name,
    'project_name' => $project_name, 'requisition_no' => $requisition_no,
    'project_id' => $project_id, 'task_id' => $task_id, 'mr_for_task' => $mr_for_task,
    'account_items' => $account_items, 'unit_list' => $unit_list,
    'type'=>$type
)); ?>