<table class="table table-bordered">
    <thead>
        <th>Item</th>
        <th>Unit</th>
        <th>Quantity</th>
        <th>Date</th>
        <th>Remarks</th>
    </thead>
    <tbody>
        <?php
        foreach ($data as $data) {
        ?>
            <tr>
                <td><?= $data['item_name'] ?></td>
                <td><?= $data['item_unit'] ?></td>
                <td><?= $data['item_quantity'] ?></td>
                <td><?= date('d-M-y', strtotime($data['item_req_date'])) ?></td>
                <td><?= $data['item_remarks'] ?></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>