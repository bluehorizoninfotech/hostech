<?php
/* @var $this MaterialRequisitionController */
/* @var $model MaterialRequisition */

$this->breadcrumbs=array(
	'Material Requisitions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MaterialRequisition', 'url'=>array('index')),
	array('label'=>'Create MaterialRequisition', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('material-requisition-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Material Requisitions</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form display-none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'material-requisition-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'requisition_id',
		'requisition_project_id',
		'requisition_task_id',
		'requisition_no',
		'requisition_item',
		'requisition_unit',
		/*
		'requisition_quantity',
		'requisition_date',
		'requisition_status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
