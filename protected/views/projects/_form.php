
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<div class="project-form-sec">
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'htmlOptions'=>array( 'class'=>'form-inline', ),
        'htmlOptions' => array('class'=>'', 'enctype' => 'multipart/form-data'),

        'id' => 'projects-form',
                'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>

<!--    <p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php //echo $form->errorSummary($model); ?>


    <div class="row">
         <div class="subrow">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="subrow">
        <?php echo $form->labelEx($model, 'client_id'); ?>
        <?php
                if(isset(Yii::app()->user->role) && (in_array('/projects/addclientmaster', Yii::app()->session['menuauthlist']))){
            ?>
        <a href="#" class="new_client">Add new client</a>
                <?php } ?>
        <?php echo $form->dropDownList($model, 'client_id', CHtml::listData(Clients::model()->findAll(
            array('condition' => 'status="1"',
                'order' => 'name ASC')), 'cid', 'name'), array('empty' => '--', 'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'client_id'); ?>
    </div> 
    </div>
    <div class="row">
        <div class="subrow subrowlong">
            <div class="client_section display-none">
                <div class="form">
                    <div class="clearfix">
                        <a class="margin-top-10 black-color cursor-pointer close-panel  pull-right">X</a>
                        <h4 class="subhead">Client</h4>
                    </div>
                    <div class="row">
                        <div class="subrow subrowsmall">
                        <label for="Clients_name" class="required">Name <span class="required">*</span></label>        <input class="form-control" name="Clients[name]" id="Clients_name" type="text" maxlength="100">        <div class="errorMessage display-none" id="Clients_name_em_"></div>    </div>
                
                        <div class="subrow subrowsmall">
                            <label for="Clients_project_type" class="required">Project Type <span class="required">*</span></label>        <select class="form-control" name="Clients[project_type]" id="Clients_project_type">
                            <option value="">Select client</option>
                            <?php
                            $ProjectType = ProjectType::model()->findAll();
                            foreach($ProjectType as $key=>$value){
                            ?>
                                <option value="<?php echo $value['ptid']; ?>"><?php echo $value['project_type']; ?></option>
                            <?php } ?>
                            </select>  
                            <div class="errorMessage display-none" id="Clients_project_type_em_"></div>    </div>
                        </div>
                    <div class="row buttons">
                    <input type="button" name="Save" value="Save" class="btn blue client_save btn-sm">
                    </div>
                </div>
            </div>
        </div>
    </div>
     
    <div class="row">
         <div class="subrow">
        <?php echo $form->labelEx($model, 'billable'); ?>
        <div class="radio_btn">
            <?php
            echo $form->radioButtonList($model, 'billable', CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="yesno"',
                                        'order' => 'caption',
                                        'distinct' => true
                            )), 'sid', 'caption'), array(  'labelOptions'=>array('class'=>'display-inline'),'separator' => ''));
            ?>
        </div>
        <?php echo $form->error($model, 'billable'); ?>
    </div>  
    
    <div class="subrow">
        <?php echo $form->labelEx($model, 'status'); ?>
        <div class="radio_btn">
            <?php
            echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="active_status"',
                                        'order' => 'caption',
                                        'distinct' => true
                            )), 'sid', 'caption'), array(  'labelOptions'=>array('class'=>'display-inline'),'separator' => ''));
            ?>
        </div>
        <?php echo $form->error($model, 'status'); ?>
    </div>  
    </div>
    <div class="row">
        <div class="subrow subrowlong form-group">
            <?php echo $form->labelEx($model,'assigned_to'); ?>
            <select name="Projects[assigned_to][]" id="Projects_assigned_to" class="form-control js-example-basic-multiple assigned_to_" multiple>     
            <?php
                $condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0 AND user_type IN (10,4,2)','order' => 'first_name ASC');
                $users = Users::model()->findAll($condition);
                foreach ($users as $key => $value) {
                    ?>
                    <option value="<?php echo $value['userid']; ?>" ><?php echo $value['full_name']; ?></option>
                    <?php
                }
            ?>
            </select>
                       
            <?php echo $form->error($model,'assigned_to'); ?>
        </div>
    </div>

    <div class="row">
    <?php
    if(!$model->isNewRecord){
        if($model->start_date !=""){
            $model->start_date = date('d-M-y',strtotime($model->start_date));
        }
        if($model->end_date !=""){
            $model->end_date = date('d-M-y',strtotime($model->end_date));
        }
    }else{
        $model->start_date = date('d-M-y');
        $time = strtotime($model->start_date);
        $model->end_date = date("d-M-y", strtotime("+1 month", $time));
        

    }
    ?>
        <div class="subrow">
            <?php echo $form->labelEx($model, 'start_date'); ?>
            <?php echo $form->textField($model, 'start_date', array('class'=>'form-control','autocomplete'=>'off')); ?>
           
            <?php echo $form->error($model, 'start_date'); ?>
        </div>
        <div class="subrow">
            <?php echo $form->labelEx($model, 'end_date'); ?>
            <?php echo $form->textField($model, 'end_date', array('class'=>'form-control','autocomplete'=>'off')); ?>
           
            <?php echo $form->error($model, 'end_date'); ?>
        </div>
    </div>

    <div class="row">
    <div class="subrow">
        <?php echo $form->labelEx($model, 'img_path'); ?>
        <?php echo $form->fileField($model, 'img_path',array('class'=>'display-inline-block')); ?>
        <?php echo $form->error($model, 'img_path'); ?>
        <div class="display-inline-block margin-top-10">
        <?php
            if(!$model->isNewRecord){
                if($model->img_path != ''){
                    $path = realpath(Yii::app()->basePath . '/../uploads/project/thumbnail/'.$model->img_path);
                    if(file_exists($path)){
        ?>
        <img width="75" height="45" src="<?php  echo Yii::app()->request->baseUrl."/uploads/project/thumbnail/".$model->img_path; ?>" alt="...">
        <?php } } } ?>
        </div>
        <div class="img_msg"></div>

    </div>
        <div class="subrow">
        <?php echo $form->labelEx($model, 'budget'); ?>
        <?php echo $form->textField($model, 'budget', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'budget'); ?>
    </div>
    </div>
    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($site_model, 'site_name'); ?>
            <?php echo $form->textField($site_model, 'site_name', array('class'=>'form-control')); ?>
            <?php echo $form->error($site_model, 'site_name'); ?>
        </div>
        <div class="subrow">
            <?php echo $form->labelEx($site_model, 'latitude'); ?>
            <?php echo $form->textField($site_model, 'latitude', array('class'=>'form-control')); ?>
            <?php echo $form->error($site_model, 'latitude'); ?>           
        </div>
    </div>
    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($site_model, 'longitude'); ?>
            <?php echo $form->textField($site_model, 'longitude', array('class'=>'form-control')); ?>
            <?php echo $form->error($site_model, 'longitude'); ?>
        </div>
    </div>
    <div class="row">
        <div class="subrow subrowlong">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>
    <br/>
    <div class="row">

        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn blue save_btn margin-left-30','id'=>'save_btn','onClick'=>'this.disabled=true;this.form.submit();this.form.reset();')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<script>
$(document).ready(function () {
    var assigned_data = '<?php echo $assigned_to;?>';
    var array           = assigned_data.split(',');	
    $('.assigned_to_').val(array);
    $('.assigned_to_').trigger('change');
});

$(document).ready(function () {
   
    


var a=0;
        var result = $('.img_msg');
        //binds to onchange event of your input field
        $('#Projects_img_path').bind('change', function() {
        var ext = $('#Projects_img_path').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1){
            $('.img_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.</span>');
            a=0;
            }else{
            var picsize = (this.files[0].size);
            if (picsize > 2000000){
                $('.img_msg').html('<span style="color: red; ">Maximum File Size Limit is 2MB.</span>');
            a=0;
            }else{
            a=1;
            $('.img_msg').html('');
            }
            }
            if (a==1){
                $('#save_btn').removeAttr('disabled');
                }else{
                    $('#save_btn').attr('disabled','disabled');
                }
        });


$(".new_client").click(function(){   
    $('.client_section').show();
});
$(".client_save").click(function(){
    var client_name = $("#Clients_name").val();
    var project_type = $("#Clients_project_type").val();
    if(project_type !='' && client_name !=''){
        $("#Clients_name_em_").hide();
        $("#Clients_name_em_").html('');
        $("#Clients_project_type_em_").hide();
        $("#Clients_project_type_em_").html('');
        $.ajax({
            'url':'<?php echo Yii::app()->createAbsoluteUrl('projects/createclient'); ?>',
            data:{client_name:client_name,project_type:project_type},
            dataType:'Json',
            type:'GET',
            success:function(result){
                if(result.status == 1){
                    $("#Projects_client_id").html(result.option);
                    $("#Projects_client_id").val(result.lastid);
                    $('.client_section').hide();
                }
            }
        })

    }else{
        if(client_name == ''){
            $("#Clients_name_em_").show();
            $("#Clients_name_em_").html('Name cannot be blank');
            $("#Clients_project_type_em_").show();
            $("#Clients_project_type_em_").html('Project Type cannot be blank');
        }
    }
})
$(".close-panel").click(function(){
    $(".client_section").hide();
})
});
</script>