<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	$model->pid=>array('view','id'=>$model->pid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Projects', 'url'=>array('addAssigneetoProjects')),
	array('label'=>'Create Projects', 'url'=>array('assigneecreate')),
	array('label'=>'View Projects', 'url'=>array('wpr_projects_view', 'id'=>$model->pid)),
	array('label'=>'Manage Projects', 'url'=>array('admin')),
);
?>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Update Project</h4>
        </div>


<?php echo $this->renderPartial('_formassignee', array('model'=>$model,'button'=>'save')); ?>

</div>
</div>
