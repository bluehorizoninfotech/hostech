<?php
$contractor_list = Contractors::model()->findAll(array('condition' => 'status = 1'));
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'site-meetings-form-2',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'beforeValidate' => new CJavaScriptExpression('function(form) {
                for(var instanceName in CKEDITOR.instances) { 
                    CKEDITOR.instances[instanceName].updateElement();
                }
                return true;
            }'),
        'afterValidate' => 'js:function(form, data, hasError) {
                if(hasError) {                                                            
                }
                else
                {
                // return true;  it will submit default way
                next_meeting_form(form, data, hasError); //and it will submit by ajax function
                   }
               }',
    ),
)); ?>
<div class='minutes_data'>
    <div class="row ">
        <div class="col-md-4">
            <?php
            if (!$model->isNewRecord) {
                if ($model->next_meeting_date != "") {
                    $model->next_meeting_date = date('d-M-y', strtotime($model->next_meeting_date));
                }
            }
            ?>
            <input name="SiteMeetings[meeting_id]" id="SiteMeetings_meeting_id" type="hidden" value="<?php echo $model->id ?>">
            <?php echo $form->labelEx($model, 'next_meeting_date'); ?>
            <?php echo $form->textField($model, 'next_meeting_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'next_meeting_date'); ?>
        </div>
        <div class="col-md-4">
            <?php echo $form->labelEx($model, 'next_meeting_time'); ?>
            <input type="time" class="form-control" value="<?php echo  !$model->isNewRecord ? $model->next_meeting_time : '' ?>" name="SiteMeetings[next_meeting_time]" id="SiteMeetings_next_meeting_time">
            <?php echo $form->error($model, 'next_meeting_time'); ?>
            <span id="next_time_format"></span>
        </div>
        <div class="col-md-4">
            <?php echo $form->labelEx($model, 'location'); ?>
            <?php echo $form->textField($model, 'location', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'location'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'note'); ?>
            <?php echo $form->textArea($model, 'note', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'note'); ?>
        </div>

    </div>
</div>
<!-- <input type="button" id="add_minute" value="Add" onClick="addminutes();"> -->
<div class="row text-right">
    <div class="col-sm-12">
        <?php echo CHtml::SubmitButton('Save', array('class' => 'btn blue save_btn margin-left-30', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>

    </div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    CKEDITOR.replace('SiteMeetings_note');
    $(".datepicker").attr("autocomplete", "off");
    $("#SiteMeetings_next_meeting_date").datepicker({
        dateFormat: 'dd-M-yy',
    });

    // function addminutes(){
    //         var data = $('.minutes_data').html();            
    //         var newdiv = document.createElement('div');
    //         newdiv.className = 'minutes';
    //         newdiv.innerHTML = data;
    //         $(newdiv).insertBefore( "#add_minute" );                                   
    // }


    $(document).ready(function() {
        $('#SiteMeetings_next_meeting_time').blur(function() {
            t = document.getElementById('SiteMeetings_next_meeting_time').value
            var [h, m] = t.split(":");
            var hour_val = (h % 12 + 12 * (h % 12 == 0));
            if (hour_val.toString().length == 1) {
                hour_val = "0" + hour_val;
            }
            var time_val = (hour_val + ":" + m);
            var time_regex = ((h % 12 + 12 * (h % 12 == 0)) + ":" + m,
                h >= 12 ? 'PM' : 'AM');
            $("#next_time_format").text(time_regex);



        });
    })
</script>