<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<head>
</head>
<div class="update-meeting-details-report-sec">
<div class="work-holder" id="expand-projects">
    <p id="success_message display-none font-15 green-color font-weight-bold">Successfully updated..</p>
    <table class="inner_table">
        <thead>
            <tr>
                <th>SL No</th>
                <th>Milestone</th>
                <th>Contractor</th>
                <th>Main Task</th>
                <th>SubTask</th>
                <th>Area</th>
                <th>WorkType</th>
                <th>Total Quantity</th>
                <th>Targeted Quantity</th>
                <th>Achieved Quantity</th>
                <th>Balance Quantity</th>
                <th>Progress</th>
                <th>Status</th>
                <th>Description</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
            <form id="idForm">
                <input type="hidden" id="row_count" value="<?= count($model) ?>">
                <?php
                $i = 1;
                if (count($model) > 0) {
                    foreach ($model as $key => $report_data) {
                ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><input type="text" value="<?= isset($report_data['milestone']) ? $report_data['milestone'] : '' ?>" readonly size="10"></td>
                            <td><input type="text" value="<?= isset($report_data['contractor']) ? $report_data['contractor'] : '' ?>" readonly size="10"></td>
                            <td><input type="text" value="<?= $report_data['main_task'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= isset($report_data['sub_task']) ? $report_data['sub_task'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= isset($report_data['area']) ? $report_data['area'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= isset($report_data['work_type']) ? $report_data['work_type'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['total_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['target_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['achieved_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['balance_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['progress'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['status'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['description'] ?>" readonly size="4"></td>
                            <td><input type="text" class="comment" name="comment_<?= $key ?>" id="comment_id" size="10" value="<?= $report_data['comments'] ?>"></td>
                            <input type="hidden" name=id_<?= $key ?> id="id" value="<?= $report_data['id'] ?>">

                        </tr>

                <?php
                        $i++;
                    }
                }
                ?>

            </form>
        </tbody>
    </table>
    <input name="submit" type="submit" value="Submit" class="btn blue save_btn">
</div>
            </div>
<script>
    $(document).ready(function() {
        $(".comment").change(function() {
            var parenttr = $(this).closest("tr");
            var id = parenttr.find("#id").val();
            var comment = parenttr.find('#comment_id').val();
            $.ajax({
                method: "post",
                dataType: "json",
                data: {
                    id: id,
                    comment: comment,
                },
                url: '<?php echo Yii::app()->createUrl("Projects/updatemeetingdetreport") ?>',
                success: function(data) {
                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                    }
                }
            });
        });
        $('.save_btn').click(function() {
            var url = '<?php echo Yii::app()->createUrl("Projects/momupdate",array('id'=>$meeting_id)) ?>';
            window.location.href = url;
        });

    });
</script>