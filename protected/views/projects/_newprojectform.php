<!-- <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/dropbox-dropins.js" id="dropboxjs" data-app-key="wfnmazrvven7ddx" type="text/javascript">
    </script> -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/onedrive.js" type="text/javascript">
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css" />
<!-- start response message -->

<div class="alert alert-success display-none" role="alert">
</div>
<div class="alert alert-danger display-none" role="alert">
</div>
<div class="alert alert-warning display-none" role="alert">

</div>

<!-- end response message -->


<div class="project-newproject-sec">
    <div class="long-form combined-form tab-responsive">

        <!-- <img id="loader" src="<?php echo Yii::app()->request->baseUrl ?>/images/loading.gif" width="50%" /> -->





        <div class="tabs">
            <ul class="nav nav-pills">
                <li id="project_details" class="active"><span class="dot"></span><a data-toggle="tab"
                        href="#project_info">Project Details</a></li>


                <?php

                if (in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) {


                    ?>
                    <li id="budget_head_details"><span class="dot"></span><a data-toggle="tab"
                            href="#budget_head_info">Budget Head</a></li>
                    <?php
                }
                ?>


                <li id="milestone_details"><span class="dot"></span><a data-toggle="tab"
                        href="#milestone_info">Milestone Details</a></li>


                <?php
                if (in_array('/tasks/addmaintask', Yii::app()->session['menuauthlist'])) {
                    ?>
                    <li id="main_task_details"><span class="dot"></span><a data-toggle="tab" href="#main_task_info">Main
                            Tasks</a></li>
                    <?php
                }
                ?>

                <li id="area_details"><span class="dot"></span><a data-toggle="tab" href="#area_info">Area Details</a>
                </li>
                <li id="worktype_details"><span class="dot"></span><a data-toggle="tab" href="#worktype_info">Work Type
                        Details</a></li>

                <li id="worksite_details"><span class="dot"></span><a data-toggle="tab" href="#worksite_info">Site
                        Details</a></li>

            </ul>
        </div>

        <div class="tab-content" id="project_new_form">
            <p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully created..</p>
            <p id="error_message" class="display-none font-15 red-color font-weight-bold">An error occured...</p>
            <div id="project_info" class="tab-pane fade active in">
                <h2 class="tab_head">Project Form</h2>

                <?php
                $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'htmlOptions' => array('class' => 'form-inline', ),
                        'htmlOptions' => array('class' => 'padding-left-20 padding-right-20', 'enctype' => 'multipart/form-data'),
                        'id' => 'projects-form',
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:function(form, data, hasError) {
                            if(hasError) {    
                               // projectFormSubmit(form, data, hasError                          
                            }
                            else
                            {
                            // return true;  it will submit default way
                                   projectFormSubmit(form, data, hasError); //and it will submit by ajax function
                               }
                           }',
                        ),
                    )
                );
                ?>
                <?php if (!$model->isNewRecord) { ?>
                    <input type="hidden" name="Projects[pid]" id="Projects_pid" value="<?= $model->pid ?>">
                <?php } ?>
                <?php
                $criteria = new CDbCriteria;
                $criteria->addCondition('clone_id IS  NULL');
                $projects = Projects::model()->findAll($criteria);
                ?>
                <div class="row">
                    <?php
                    if (!isset($_GET['id'])) {
                        ?>
                        <div class="col-md-4">
                            <?php echo $form->labelEx($model, 'Project Clone'); ?>
                            <select class="form-control project_clone" id="project_clone_id"
                                name=Projects[project_clone_name]>
                                <option value="">-Choose a Clone-</option>
                                <?php
                                if (!empty($projects)) {
                                    foreach ($projects as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value['pid']; ?>">
                                            <?php echo $value['name']; ?>
                                        </option>
                                    <?php }
                                } ?>

                            </select>
                        </div>
                    <?php } ?>

                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'name'); ?>
                        <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'project_no'); ?>
                        <?php echo $form->textField($model, 'project_no', array('class' => 'form-control', 'value' => $project_no)); ?>
                        <?php echo $form->error($model, 'project_no'); ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'client_id'); ?>
                        <?php
                        if (isset(Yii::app()->user->role) && (in_array('/projects/addclientmaster', Yii::app()->session['menuauthlist']))) {
                            ?>
                            <a href="#" class="new_client">Add new client</a>
                        <?php } ?>
                        <?php
                        echo $form->dropDownList($model, 'client_id', CHtml::listData(Clients::model()->findAll(
                            array(
                                'condition' => 'status="1"',
                                'order' => 'name ASC'
                            )
                        ), 'cid', 'name'), array('empty' => '--', 'class' => 'form-control'));
                        ?>
                        <?php echo $form->error($model, 'client_id'); ?>
                    </div>

                </div>

                <div class="client_section">
                    <div class="form">
                        <div class="clearfix">
                            <a class="black-color cursor-pointer close-panel pull-right">X</a>
                            <h4 class="subhead">Client</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="Clients_name" class="required">Name <span class="required">*</span></label>
                                <input class="form-control" name="Clients[name]" id="Clients_name" type="text"
                                    maxlength="100">
                                <div class="errorMessage display-none" id="Clients_name_em_"></div>
                            </div>
                            <div class="col-md-4">
                                <label for="Clients_project_type" class="required">Project Type <span
                                        class="required">*</span></label>
                                <select class="form-control" name="Clients[project_type]" id="Clients_project_type">
                                    <option value="">Select client</option>
                                    <?php
                                    $ProjectType = ProjectType::model()->findAll();
                                    foreach ($ProjectType as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value['ptid']; ?>">
                                            <?php echo $value['project_type']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <div class="errorMessage display-none" id="Clients_project_type_em_"></div>
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="button" name="Save" value="Save"
                                    class="btn blue client_save btn-sm margin-top-15">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'billable'); ?>
                        <div class="radio_btn">
                            <?php
                            echo $form->radioButtonList($model, 'billable', CHtml::listData(Status::model()->findAll(
                                array(
                                    'select' => array('sid,caption'),
                                    'condition' => 'status_type="yesno"',
                                    'order' => 'caption',
                                    'distinct' => true
                                )
                            ), 'sid', 'caption'), array('labelOptions' => array('class' => 'display-inline margin-right-10 verticl-align-bottom'), 'separator' => ''));
                            ?>
                        </div>
                        <?php echo $form->error($model, 'billable'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'status'); ?>
                        <div class="radio_btn">
                            <?php
                            echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
                                array(
                                    'select' => array('sid,caption'),
                                    'condition' => 'status_type="active_status"',
                                    'order' => 'caption',
                                    'distinct' => true
                                )
                            ), 'sid', 'caption'), array('labelOptions' => array('class' => 'display-inline margin-right-10 verticl-align-bottom'), 'separator' => ''));
                            ?>
                        </div>
                        <?php echo $form->error($model, 'status'); ?>
                    </div>
                    <?php
                    if (!$model->isNewRecord) {
                        if ($model->start_date != "") {
                            $model->start_date = date('d-M-y', strtotime($model->start_date));
                        }
                        if ($model->end_date != "") {
                            $model->end_date = date('d-M-y', strtotime($model->end_date));
                        }
                    } else {
                        $model->start_date = date('d-M-y');
                        $time = strtotime($model->start_date);
                        $model->end_date = date("d-M-y", strtotime("+1 month", $time));
                    }
                    ?>
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'start_date'); ?>
                        <?php echo $form->textField($model, 'start_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'start_date'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'end_date'); ?>
                        <?php echo $form->textField($model, 'end_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'end_date'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'budget'); ?>
                        <?php echo $form->textField($model, 'budget', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'budget'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form->labelEx($model, 'total_square_feet'); ?>
                        <?php echo $form->textField($model, 'total_square_feet', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'total_square_feet'); ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-2 margin-top-15">
                        <?php echo $form->labelEx($model, 'img_path'); ?>
                        <?php echo $form->fileField($model, 'img_path', array('class' => 'display-inline-block')); ?>
                        <?php echo $form->error($model, 'img_path'); ?>
                        <div>
                            <?php
                            if (!$model->isNewRecord) {
                                if ($model->img_path != '') {
                                    $path = realpath(Yii::app()->basePath . '/../uploads/project/thumbnail/' . $model->img_path);
                                    if (file_exists($path)) {
                                        ?>
                                        <img width="75" height="45"
                                            src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/thumbnail/" . $model->img_path; ?>"
                                            alt="...">
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="img_msg"></div>
                    </div>
                    <div class="col-md-2 margin-top-15">
                        <label for="Projects_report_image">Report Image<i style="font-weight: 200;
                                                                      color: #0000ffd6;"> (Image size:762px X
                                762px)</i></label>
                        <?php echo $form->fileField($model, 'report_image', array('class' => 'display:inline-block')); ?>
                        <?php echo $form->error($model, 'report_image'); ?>
                        <div>
                            <?php
                            if (!$model->isNewRecord) {
                                if (isset($model->report_image)) {
                                    if ($model->report_image != '') {
                                        $path = realpath(Yii::app()->basePath . '/../uploads/project/report_imgs/' . $model->report_image);
                                        if (file_exists($path)) {
                                            ?>
                                            <img width="75" height="45"
                                                src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/report_imgs/" . $model->report_image; ?>"
                                                alt="...">
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="report_img_msg"></div>
                    </div>



                    <?php
                    Yii::app()->getModule('menu');
                    $action_name = "include_project_manager";

                    $included_users = Menu::model()->findByAttributes(
                        array(
                            'action' => $action_name,
                        )
                    );

                    $menu_id = $included_users->menu_id;

                    $all_users = MenuPermissions::model()->findAllByAttributes(array('menu_id' => $menu_id));
                    $available_user = [];
                    foreach ($all_users as $allowed_user) {
                        $available_user[] = $allowed_user->user_id;
                    }
                    $available_users = implode(",", $available_user);


                    ?>


                    <div class="row">
                        <div class="col-md-4 form-group">
                            <?php echo $form->labelEx($model, 'assigned_to'); ?>
                            <select name="Projects[assigned_to][]" id="Projects_assigned_to"
                                class="form-control js-example-basic-multiple assigned_to_">
                                <?php
                                // $condition = array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0 AND user_type IN (10,4,2)', 'order' => 'first_name ASC');
                                if ($available_users != "") {
                                    $condition = array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0 AND userid IN (' . $available_users . ')', 'order' => 'first_name ASC');
                                } else {
                                    $condition = array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0 AND userid IN (0)', 'order' => 'first_name ASC');
                                }

                                $users = Users::model()->findAll($condition);
                                foreach ($users as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value['userid']; ?>">
                                        <?php echo $value['full_name'] . ' (' . $value->userType->role . ')'; ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <?php echo $form->error($model, 'assigned_to'); ?>
                        </div>




                        <div class="col-md-4">
                            <?php echo $form->labelEx($model, 'description'); ?>
                            <?php echo $form->textArea($model, 'description', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'description'); ?>
                            <input type="hidden" name="submit_type" id="submit_type">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo $form->labelEx($model, 'architect'); ?>
                            <?php echo $form->textField($model, 'architect', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'architect'); ?>
                        </div>

                        <?php
                        if (Tasks::model()->accountPermission() == 1) {
                            if (count($template) > 1) { ?>
                                <div class="subrow col-md-4">

                                    <?php echo $form->labelEx($model, 'template'); ?>
                                    <?php echo $form->dropDownList($model, 'template_id', CHtml::listData(Template::model()->findAll(), 'template_id', 'template_name'), array('empty' => '-Choose a template-', 'class' => 'form-control input-medium width-466')); ?>
                                    <?php echo $form->error($model, 'template_id'); ?>

                                </div>

                            <?php } else if (count($template) == 1) {
                                ?>
                                    <div class="subrow ">

                                    <?php echo $form->labelEx($model, 'template'); ?>
                                    <?php echo $form->dropDownList($model, 'template_id', CHtml::listData(Template::model()->findAll(), 'template_id', 'template_name'), array('empty' => '-Choose a template-', 'class' => 'form-control input-medium width-466')); ?>
                                    <?php echo $form->error($model, 'template_id'); ?>

                                    </div>

                                <?php
                            } else { ?>
                                    <div class="subrow ">
                                    <?php echo $form->labelEx($model, 'template'); ?>
                                    <?php echo $form->dropDownList($model, 'template_id', CHtml::listData(Template::model()->findAll(), 'template_id', 'template_name'), array('empty' => '-Choose a template-', 'class' => 'form-control input-medium width-466')); ?>
                                    <?php echo $form->error($model, 'template_id'); ?>

                                    </div>

                            <?php }
                        } ?>



                        <div class="subrow col-md-4">

                            <?php echo $form->labelEx($model, 'site calender'); ?>
                            <?php echo $form->dropDownList($model, 'site_calendar_id', CHtml::listData(ProjectCalender::model()->findAll(), 'id', 'calender_name'), array('empty' => '-Choose a calendar-', 'class' => 'form-control input-medium width-466')); ?>
                            <?php echo $form->error($model, 'site_calendar_id'); ?>

                        </div>
                    </div>

                    <div class="row">
                        <!-- <div id="container" class="col-md-4"></div> -->
                        <button onClick="launchOneDrivePicker()" class="margin-left-15 btn default">Open from
                            OneDrive</button>


                    </div>
                    <div class="col-md-12">
                        <label>Participants</label>
                        <!-- handson div -->
                        <div id="participants-table" class="width-100-percentage"></div>
                        <br>
                    </div>
                    <div class="row text-right">
                        <div class="col-sm-12">

                            <div class="d-flex justify-content-end min-height-40">
                                <div>
                                    <?php echo CHtml::SubmitButton($model->isNewRecord ? 'Save & Add Project' : 'Update', array('class' => 'btn blue save_btn', 'id' => 'save_btn', 'name' => 'save_btn', ), array('onclick' => 'return validation();')); ?>
                                </div>
                                <div>
                                    <?php echo CHtml::SubmitButton('Save & Proceed', array('class' => 'btn blue save_btn margin-left-5 save-and-proceed-button', 'id' => 'save_cnt', 'name' => 'cont_btn')); ?>
                                    <?php //echo CHtml::resetButton('Reset', array('class' => 'btn default')); 
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
            <!-- start budget head -->
            <?php

            if (in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) {
                ?>


                <div id="budget_head_info" class="tab-pane fade">

                    <h2 class="tab_head">Budget Head Form</h2>
                    <?php
                    $form2 = $this->beginWidget(
                        'CActiveForm',
                        array(
                            'htmlOptions' => array('class' => 'padding-left-20 padding-right-20', ),
                            'id' => 'budget-head-form',
                            'action' => $this->createUrl('budgetHead/create'),
                            'enableAjaxValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'afterValidate' => 'js:function(form, data, hasError) {
                                if(hasError) {
                                   
                                }
                                else
                                {
                                   
                                // return true;  it will submit default way
                                budgetheadFormSubmit(form, data, hasError); //and it will submit by ajax function
                                   }
                               }',
                            ),
                        )
                    );
                    ?>
                    <div class="row">
                        <div class="col-md-2">
                            <?php echo $form2->labelEx($budget_head_model, 'budget_head_title'); ?>
                            <?php echo $form2->textField($budget_head_model, 'budget_head_title', array('class' => 'form-control')); ?>
                            <?php echo $form2->error($budget_head_model, 'budget_head_title'); ?>
                        </div>

                        <input type="hidden" name="BudgetHead[id]" id="BudgetHead_id">
                        <div class="col-md-2">
                            <?php echo $form2->labelEx($budget_head_model, 'project_id'); ?>
                            <?php
                            if ($budget_head_model->isNewRecord) {
                                if (Yii::app()->user->project_id != "") {

                                    $budget_head_model->project_id = Yii::app()->user->project_id;
                                    $projects = Projects::model()->findByPk(Yii::app()->user->project_id);

                                    $budget_head_model->start_date = !empty($projects->start_date) ? date('d-M-y', strtotime($projects->start_date)) : NULL;
                                    $budget_head_model->end_date = !empty($projects->end_date) ? date('d-M-y', strtotime($projects->end_date)) : NULL;
                                } else {
                                    $budget_head_model->project_id = "";
                                }
                            } else {
                                $budget_head_model->start_date = $this->getmilestonedate($budget_head_model->id, $budget_head_model->project_id, 'start');
                                $budget_head_model->end_date = $this->getmilestonedate($budget_head_model->id, $budget_head_model->project_id, 'end');
                            }
                            ?>
                            <?php
                            if (isset($_GET['id']) && !empty($_GET['id'])) {
                                $proj_condition = 'status="1" AND pid = ' . $_GET['id'];
                                $budget_head_model->project_id = $_GET['id'];
                            } else {
                                if (Yii::app()->user->role == 1) {
                                    $proj_condition = 'status="1"';
                                } else {
                                    $proj_condition = 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)';
                                }
                            }

                            echo $form2->dropDownList(
                                $budget_head_model,
                                'project_id',
                                CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC ')), 'pid', 'name'),
                                array(
                                    'empty' => 'Choose a project',
                                    'disabled' => $budget_head_model->isNewRecord ? false : true,
                                    'class' => 'form-control'
                                )
                            );
                            ?>
                            <?php echo $form2->error($budget_head_model, 'project_id'); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo $form2->labelEx($budget_head_model, 'start_date'); ?>
                            <?php echo $form2->textField($budget_head_model, 'start_date', array('class' => 'form-control')); ?>
                            <?php echo $form2->error($budget_head_model, 'start_date'); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo $form2->labelEx($budget_head_model, 'end_date'); ?>
                            <?php echo $form2->textField($budget_head_model, 'end_date', array('class' => 'form-control')); ?>
                            <?php echo $form2->error($budget_head_model, 'end_date'); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo $form2->labelEx($budget_head_model, 'ranking'); ?>
                            <?php echo $form2->textField($budget_head_model, 'ranking', array('class' => 'form-control')); ?>
                            <?php echo $form2->error($budget_head_model, 'ranking'); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo $form2->labelEx($budget_head_model, 'status'); ?>
                            <?php echo $form2->dropDownList($budget_head_model, 'status', array('1' => 'Enable', '2' => 'Disable'), array('class' => 'form-control', 'options' => array('1' => array('selected' => true)))); ?>
                            <?php echo $form2->error($budget_head_model, 'status'); ?>
                        </div>
                    </div>
                    <input type="hidden" name="submit_type" id="submit_type_budgethead">
                    <div class="row text-right">
                        <div class="col-sm-12">
                            <div class="d-flex justify-content-end min-height-40">
                                <div>
                                    <?php echo CHtml::SubmitButton($budget_head_model->isNewRecord ? 'Save & Add Budget Head' : 'Update', array('class' => 'btn blue save_btn', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
                                </div>
                                <div>
                                    <?php echo CHtml::SubmitButton('Save & Proceed', array('class' => 'btn blue save_btn margin-left-5 save-and-proceed-button', 'id' => 'save_cnt', 'name' => 'cont_btn')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?php


                            // if (!$milestone_model->isNewRecord) {
                            $this->widget(
                                'zii.widgets.grid.CGridView',
                                array(
                                    'id' => 'budget-head-grid',
                                    'dataProvider' => $budget_head_model->search(),
                                    // 'filter' => $milestone_model,
                                    'itemsCssClass' => 'table table-bordered',
                                    'template' => '<div class="table-responsive">{items}</div>',
                                    'pager' => array(
                                        'id' => 'dataTables-example_paginate',
                                        'header' => '',
                                        'prevPageLabel' => 'Previous ',
                                        'nextPageLabel' => 'Next '
                                    ),
                                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                                    'columns' => array(
                                        array('class' => 'IndexColumn', 'header' => 'S.No.'),
                                        array(
                                            'class' => 'CButtonColumn',
                                            'template' => '{update}{delete}',
                                            'buttons' => array(
                                                'update' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,
                                                    'visible' => "isset(Yii::app()->user->role) && (in_array('/budgetHead/update', Yii::app()->session['menuauthlist']))",

                                                    'click' => 'function(e){e.preventDefault();editbudgethead($(this).attr("href"));}',
                                                    'options' => array('class' => 'update_milestone actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                                                ),

                                                'delete' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,

                                                    'visible' => "isset(Yii::app()->user->role) && (in_array('/budgetHead/delete', Yii::app()->session['menuauthlist']))",

                                                    'click' => 'function(e){e.preventDefault();deletbudgethead($(this).attr("href"));}',
                                                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                                                ),
                                            ),
                                        ),
                                        array(
                                            'name' => 'budget_head_title',
                                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->budget_head_id')
                                        ),
                                        array(
                                            'name' => 'project_id',
                                            'value' => '$data->project->name',
                                            'type' => 'raw',
                                            'filter' => CHtml::listData(Projects::model()->findAll(
                                                array(
                                                    'select' => array('pid,name'),
                                                    'condition' => 'status=1',
                                                    'order' => 'name',
                                                    'distinct' => true
                                                )
                                            ), "pid", "name")
                                        ),
                                        array(
                                            'name' => 'start_date',

                                            'value' => 'isset($data->start_date)? date("d-M-y",strtotime($data->start_date)) : ""',
                                            'type' => 'html',
                                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->budget_head_id')
                                        ),
                                        array(
                                            'name' => 'end_date',
                                            'value' => 'isset($data->end_date)? date("d-M-y",strtotime($data->end_date)) : ""',
                                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->budget_head_id')
                                        ),
                                        array(
                                            'name' => 'ranking',
                                            'type' => 'raw',
                                            'value' => '"<span data-id=\"".$data->budget_head_id."\">".$data->ranking."</span>"',
                                            'htmlOptions' => array('class' => 'rankingcol')
                                        ),
                                        array(
                                            'name' => 'status',
                                            'value' => function ($data) {
                                                if ($data->status == 1) {
                                                    $status = 'Enable';
                                                } else {
                                                    $status = 'Disable';
                                                }
                                                return $status;
                                            },
                                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->budget_head_id')
                                        ),

                                    ),
                                )
                            );
                            // }
                            ?>
                        </div>
                    </div>

                </div>
            <?php } ?>
            <!-- end budget head -->












            <div id="milestone_info" class="tab-pane fade">
                <!-- ------------------------- Insert your milestone form here ------------------------------  -->
                <h2 class="tab_head">Milestone Form</h2>
                <?php
                $form2 = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'htmlOptions' => array('class' => 'padding-left-20 padding-right-20', ),
                        'id' => 'milestone-form',
                        'action' => $this->createUrl('masters/milestone/create'),
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:function(form, data, hasError) {
                                if(hasError) {
                                   
                                }
                                else
                                {
                                   
                                // return true;  it will submit default way
                                milestoneFormSubmit(form, data, hasError); //and it will submit by ajax function
                                   }
                               }',
                        ),
                    )
                );
                ?>
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form2->labelEx($milestone_model, 'milestone_title'); ?>
                        <?php echo $form2->textField($milestone_model, 'milestone_title', array('class' => 'form-control')); ?>
                        <?php echo $form2->error($milestone_model, 'milestone_title'); ?>
                    </div>





                    <input type="hidden" name="Milestone[id]" id="Milestone_id">
                    <div class="col-md-2">
                        <?php echo $form2->labelEx($milestone_model, 'project_id'); ?>
                        <?php
                        if ($milestone_model->isNewRecord) {
                            if (Yii::app()->user->project_id != "") {

                                $milestone_model->project_id = Yii::app()->user->project_id;
                                $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
                                //  $milestone_model->start_date = date('d-M-y', strtotime(isset($projects->start_date)));
                                $milestone_model->start_date = !empty($projects->start_date) ? date('d-M-y', strtotime($projects->start_date)) : NULL;
                                $milestone_model->end_date = !empty($projects->end_date) ? date('d-M-y', strtotime($projects->end_date)) : NULL;
                            } else {
                                $milestone_model->project_id = "";
                            }
                        } else {
                            $milestone_model->start_date = $this->getmilestonedate($milestone_model->id, $milestone_model->project_id, 'start');
                            $milestone_model->end_date = $this->getmilestonedate($milestone_model->id, $milestone_model->project_id, 'end');
                        }
                        ?>
                        <?php
                        if (isset($_GET['id']) && !empty($_GET['id'])) {
                            $proj_condition = 'status="1" AND pid = ' . $_GET['id'];
                            $milestone_model->project_id = $_GET['id'];
                        } else {
                            if (Yii::app()->user->role == 1) {
                                $proj_condition = 'status="1"';
                            } else {
                                $proj_condition = 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)';
                            }
                        }

                        echo $form2->dropDownList(
                            $milestone_model,
                            'project_id',
                            CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC ')), 'pid', 'name'),
                            array(
                                'empty' => 'Choose a project',
                                'disabled' => $milestone_model->isNewRecord ? false : true,
                                'class' => 'form-control'
                            )
                        );
                        ?>
                        <?php echo $form2->error($milestone_model, 'project_id'); ?>
                    </div>



                    <?php
                    if (in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) {
                        ?>

                        <div class="col-md-2">
                            <?php
                            if (!empty($milestone_model->project_id)) {
                                $milestone_condition = "status = 1 AND project_id = " . $milestone_model->project_id;
                            } else {
                                $milestone_condition = "status = 1 ";
                            }
                            ?>
                            <?php echo $form->labelEx($milestone_model, 'budget_id'); ?>
                            <?php echo $form->dropDownList($milestone_model, 'budget_id', CHtml::listData(BudgetHead::model()->findAll(
                                array(
                                    'select' => array('budget_head_id,budget_head_title'),
                                    'condition' => $milestone_condition,
                                    'order' => 'budget_head_title ASC',
                                    'distinct' => true
                                )
                            ), 'budget_head_id', 'budget_head_title'), array('class' => 'form-control', 'empty' => 'Choose a Budget Head'));

                            ?>
                            <?php echo $form->error($milestone_model, 'budget_id'); ?>
                        </div>

                        <?php
                    }
                    ?>




                    <div class="col-md-2">
                        <?php echo $form2->labelEx($milestone_model, 'start_date'); ?>
                        <?php echo $form2->textField($milestone_model, 'start_date', array('class' => 'form-control')); ?>
                        <?php echo $form2->error($milestone_model, 'start_date'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form2->labelEx($milestone_model, 'end_date'); ?>
                        <?php echo $form2->textField($milestone_model, 'end_date', array('class' => 'form-control')); ?>
                        <?php echo $form2->error($milestone_model, 'end_date'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form2->labelEx($milestone_model, 'ranking'); ?>
                        <?php echo $form2->textField($milestone_model, 'ranking', array('class' => 'form-control')); ?>
                        <?php echo $form2->error($milestone_model, 'ranking'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form2->labelEx($milestone_model, 'status'); ?>
                        <?php echo $form2->dropDownList($milestone_model, 'status', array('1' => 'Enable', '2' => 'Disable'), array('class' => 'form-control', 'options' => array('1' => array('selected' => true)))); ?>
                        <?php echo $form2->error($milestone_model, 'status'); ?>
                    </div>
                </div>
                <input type="hidden" name="submit_type" id="submit_type_milestone">
                <div class="row text-right">
                    <div class="col-sm-12">
                        <div class="d-flex justify-content-end min-height-40">
                            <div>
                                <?php echo CHtml::SubmitButton($milestone_model->isNewRecord ? 'Save & Add Milestone' : 'Update', array('class' => 'btn blue save_btn', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
                            </div>
                            <div>
                                <?php echo CHtml::SubmitButton('Save & Proceed', array('class' => 'btn blue save_btn margin-left-5 save-and-proceed-button', 'id' => 'save_cnt', 'name' => 'cont_btn')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        //$urldatapart = http_build_query($querydata);
                        //if(isset($_GET['Milestone_sort']) and $_GET['Milestone_sort'])
                        echo '<div class="red-color pull-left"><small>Note: To update ranking, please sort the ranking column first as ASC <br /> and then use Drag & Drop by Rows</small></div>';
                        echo '<div id="rankupdatemessage"></div>';

                        // if (!$milestone_model->isNewRecord) {
                        $this->widget(
                            'zii.widgets.grid.CGridView',
                            array(
                                'id' => 'milestone-grid',
                                'dataProvider' => $milestone_model->search(),
                                // 'filter' => $milestone_model,
                                'itemsCssClass' => 'table table-bordered',
                                'template' => '<div class="table-responsive margin-top-40">{items}</div>',
                                'pager' => array(
                                    'id' => 'dataTables-example_paginate',
                                    'header' => '',
                                    'prevPageLabel' => 'Previous ',
                                    'nextPageLabel' => 'Next '
                                ),
                                'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                                'columns' => array(
                                    array('class' => 'IndexColumn', 'header' => 'S.No.'),
                                    array(
                                        'class' => 'CButtonColumn',
                                        // 'template' => isset(Yii::app()->user->role) && (in_array('/masters/milestone/update', Yii::app()->session['menuauthlist'])) ? '{update}' : '',
                                        'template' => '{update}{delete}',
                                        'buttons' => array(
                                            'update' => array(
                                                'label' => '',
                                                'imageUrl' => false,

                                                'visible' => "isset(Yii::app()->user->role) && (in_array('/masters/milestone/update', Yii::app()->session['menuauthlist']))",

                                                'click' => 'function(e){e.preventDefault();editmilestone($(this).attr("href"));}',
                                                'options' => array('class' => 'update_milestone actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                                            ),


                                            'delete' => array(
                                                'label' => '',
                                                'imageUrl' => false,

                                                'visible' => "isset(Yii::app()->user->role) && (in_array('/masters/milestone/delete', Yii::app()->session['menuauthlist']))",

                                                'click' => 'function(e){e.preventDefault();deletemilestone($(this).attr("href"));}',
                                                'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                                            ),
                                        ),
                                    ),
                                    array(
                                        'name' => 'milestone_title',
                                        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                                    ),
                                    array(
                                        'name' => 'project_id',
                                        'value' => '$data->project->name',
                                        'type' => 'raw',
                                        'filter' => CHtml::listData(Projects::model()->findAll(
                                            array(
                                                'select' => array('pid,name'),
                                                'condition' => 'status=1',
                                                'order' => 'name',
                                                'distinct' => true
                                            )
                                        ), "pid", "name")
                                    ),

                                    array(
                                        'name' => 'budget head',

                                        'value' => '(isset($data->budgetHead->budget_head_title) ? $data->budgetHead->budget_head_title : "")',
                                        'type' => 'raw',
                                        'visible' => ((in_array('/budgetHead/create', Yii::app()->session['menuauthlist'])) ? true : false),

                                    ),

                                    array(
                                        'name' => 'start_date',
                                        'value' => 'date("d-M-y",strtotime($data->start_date))',
                                        'type' => 'html',
                                        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                                    ),
                                    array(
                                        'name' => 'end_date',
                                        'value' => 'date("d-M-y",strtotime($data->end_date))',
                                        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                                    ),
                                    array(
                                        'name' => 'ranking',
                                        'type' => 'raw',
                                        'value' => '"<span data-id=\"".$data->id."\">".$data->ranking."</span>"',
                                        'htmlOptions' => array('class' => 'rankingcol')
                                    ),
                                    array(
                                        'name' => 'status',
                                        'value' => function ($data) {
                                            if ($data->status == 1) {
                                                $status = 'Enable';
                                            } else {
                                                $status = 'Disable';
                                            }
                                            return $status;
                                        },
                                        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                                    ),

                                ),
                            )
                        );
                        // }
                        ?>
                    </div>
                </div>

            </div>
            <div id="main_task_info" class="tab-pane fade">
                <?php echo $this->renderPartial('_main_task', array('model' => $task)); ?>
            </div>
            <div id="worktype_info" class="tab-pane fade">
                <!-- ------------------------- Insert your worktype form here ------------------------------  -->
                <h2 class="tab_head">Worktypes</h2>
                <?php
                $form3 = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'htmlOptions' => array('class' => 'padding-left-20 padding-right-20', ),
                        'id' => 'project-work-type-form',
                        'action' => $this->createUrl('masters/projectWorkType/create'),
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:function(form, data, hasError) {
                                if(hasError) {
                                   
                                }
                                else
                                {
                                   
                                // return true;  it will submit default way
                                worktypeFormSubmit(form, data, hasError); //and it will submit by ajax function
                                   }
                               }',
                        ),
                    )
                );
                ?>
                <div class="row">
                    <div class="col-md-3">
                        <?php echo $form3->labelEx($work_type_model, 'project_id'); ?>
                        <?php
                        if ($work_type_model->isNewRecord) {
                            if (Yii::app()->user->project_id != "") {
                                $work_type_model->project_id = Yii::app()->user->project_id;
                                $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
                            } else {
                                $work_type_model->project_id = "";
                            }
                        }
                        ?>
                        <?php
                        if (isset($_GET['id']) && !empty($_GET['id'])) {
                            $proj_condition = 'status="1" AND pid = ' . $_GET['id'];
                            $work_type_model->project_id = $_GET['id'];
                        } else {
                            if (Yii::app()->user->role == 1) {
                                $proj_condition = 'status="1"';
                            } else {
                                $proj_condition = 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)';
                            }
                        }



                        echo $form3->dropDownList(
                            $work_type_model,
                            'project_id',
                            CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC ')), 'pid', 'name'),
                            array(
                                'empty' => 'Choose a project',
                                'disabled' => $work_type_model->isNewRecord ? false : true,
                                'class' => 'form-control'
                            )
                        );
                        ?>
                        <?php echo $form3->error($work_type_model, 'project_id'); ?>
                    </div>

                    <div class="col-md-3">
                        <?php echo $form3->labelEx($work_type_model, 'work_type_id'); ?>
                        <?php
                        echo $form3->dropDownList(
                            $work_type_model,
                            'work_type_id',
                            CHtml::listData(WorkType::model()->findAll(array('order' => 'work_type ASC ')), 'wtid', 'work_type'),
                            array(
                                'empty' => 'Choose a work type',
                                'disabled' => $work_type_model->isNewRecord ? false : true,
                                'class' => 'form-control js-example-basic-single'
                            )
                        );
                        ?>
                        <?php echo $form3->error($work_type_model, 'work_type_id'); ?>
                    </div>
                    <input type="hidden" name="ProjectWorkType[id]" id="ProjectWorkType_id">
                    <div class="col-md-3">
                        <?php echo $form3->labelEx($work_type_model, 'ranking'); ?>
                        <?php echo $form3->textField($work_type_model, 'ranking', array('class' => 'form-control')); ?>
                        <?php echo $form3->error($work_type_model, 'ranking'); ?>
                    </div>

                </div>
                <input type="hidden" name="submit_type" id="submit_type_worktype">
                <div class="row text-right">
                    <div class="col-sm-12">
                        <div class="d-flex justify-content-end min-height-40">
                            <div>
                                <?php echo CHtml::SubmitButton($work_type_model->isNewRecord ? 'Save & Add Work type' : 'Update', array('class' => 'btn blue save_btn', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
                            </div>
                            <div>
                                <?php echo CHtml::SubmitButton('Save & Proceed', array('class' => 'btn blue save_btn margin-left-5 save-and-proceed-button', 'id' => 'save_cnt', 'name' => 'cont_btn')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (!$model->isNewRecord || $model->isNewRecord) {
                            $this->widget(
                                'zii.widgets.grid.CGridView',
                                array(
                                    'id' => 'worktype-grid',
                                    'dataProvider' => $work_type_model->search(),
                                    'itemsCssClass' => 'table table-bordered',
                                    'pager' => array(
                                        'id' => 'dataTables-example_paginate',
                                        'header' => '',
                                        'prevPageLabel' => 'Previous ',
                                        'nextPageLabel' => 'Next '
                                    ),
                                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                                    'columns' => array(
                                        array('class' => 'IndexColumn', 'header' => 'S.No.'),
                                        array(
                                            'class' => 'CButtonColumn',
                                            'template' => '{update}{delete}',
                                            'buttons' => array(
                                                'update' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,
                                                    'visible' => " isset(Yii::app()->user->role)",
                                                    'click' => 'function(e){e.preventDefault();editWorktype($(this).attr("href"));}',
                                                    'options' => array('class' => 'update_milestone1 actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                                                ),


                                                'delete' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,

                                                    'visible' => "isset(Yii::app()->user->role)",

                                                    'click' => 'function(e){e.preventDefault();deleteworktype($(this).attr("href"));}',
                                                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                                                ),
                                            ),
                                        ),
                                        array(
                                            'name' => 'project_id',
                                            'value' => '$data->project->name',
                                            'type' => 'raw',
                                            'filter' => CHtml::listData(Projects::model()->findAll(
                                                array(
                                                    'select' => array('pid,name'),
                                                    'condition' => 'status=1',
                                                    'order' => 'name',
                                                    'distinct' => true
                                                )
                                            ), "pid", "name")
                                        ),
                                        array(
                                            'name' => 'work_type_id',
                                            'value' => '$data->workType->work_type',
                                            'type' => 'raw',
                                            'filter' => CHtml::listData(WorkType::model()->findAll(
                                                array(
                                                    'select' => array('wtid,work_type'),
                                                    'order' => 'work_type',
                                                    'distinct' => true
                                                )
                                            ), "wtid", "work_type")
                                        ),
                                        array(
                                            'name' => 'ranking',
                                        ),
                                        
                                    ),
                                )
                            );
                        }
                        ?>
                    </div>
                </div>

            </div>
            <!-----------------    Insert your worksite form here  --------------------->
            <div id="worksite_info" class="tab-pane fade">
                <h2 class="tab_head">Worksite Form</h2>
                <?php
                $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'htmlOptions' => array('class' => 'padding-left-20 padding-right-20', ),
                        'id' => 'clientsite-form',
                        'action' => $this->createUrl('clientsite/create'),
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:function(form, data, hasError) {
                                if(hasError) {
                                 
                                }
                                else
                                {
                                   
                                // return true;  it will submit default way
                                siteFormSubmit(form, data, hasError); //and it will submit by ajax function
                                   }
                               }',
                        ),
                    )
                );
                ?>
                <div class="row">
                    <input type="hidden" name="Clientsite[id]" id="Clientsite_id">
                    <div class="col-md-6">
                        <?php echo $form->labelEx($work_site_model, 'site_name'); ?>
                        <?php echo $form->textField($work_site_model, 'site_name', array('class' => 'form-control')); ?>
                        <?php echo $form->error($work_site_model, 'site_name'); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo $form->labelEx($work_site_model, 'pid'); ?>
                        <?php
                        if ($work_site_model->isNewRecord) {
                            if (Yii::app()->user->project_id != "") {
                                $work_site_model->pid = Yii::app()->user->project_id;
                            } else {
                                $work_site_model->pid = "";
                            }
                        }
                        ?>
                        <?php
                        if (isset($_GET['id']) && !empty($_GET['id'])) {
                            $proj_condition = 'status="1" AND pid = ' . $_GET['id'];
                            $work_site_model->pid = $_GET['id'];
                        } else {
                            if (Yii::app()->user->role == 1) {
                                $proj_condition = 'status="1"';
                            } else {
                                $proj_condition = 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)';
                            }
                        }
                        echo $form->dropDownList(
                            $work_site_model,
                            'pid',
                            CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC ')), 'pid', 'name'),
                            array(
                                'empty' => 'Choose a project',
                                'disabled' => $work_site_model->isNewRecord ? false : true,
                                'class' => 'form-control'
                            )
                        );
                        ?>
                        <?php echo $form->error($work_site_model, 'pid'); ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo $form->labelEx($work_site_model, 'latitude'); ?>
                        <?php echo $form->textField($work_site_model, 'latitude', array('class' => 'form-control')); ?>
                        <?php echo $form->error($work_site_model, 'latitude'); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo $form->labelEx($work_site_model, 'longitude'); ?>
                        <?php echo $form->textField($work_site_model, 'longitude', array('class' => 'form-control')); ?>
                        <?php echo $form->error($work_site_model, 'longitude'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="ClientsiteAssign_user_id" class=""> Site Engineers/Supervisors </label>
                        <select name="user_assign_model[user_id][]" id="user_assign_model_ids"
                            class="form-control js-example-basic-multiple user_ids">
                            <?php
                            $condition = array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0', 'order' => 'first_name ASC');
                            $users = Users::model()->findAll($condition);
                            foreach ($users as $key => $value) {
                                ?>
                            <option value="<?php echo $value['userid']; ?>">
                                <?php echo $value['full_name'] . ' (' . $value->userType->role . ')'; ?>
                            </option>
                            <?php
                            }
                            ?>
                        </select>
                        <?php echo $form->error($user_assign_model, 'user_id'); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo $form->labelEx($work_site_model, 'distance'); ?>
                        <?php echo $form->textField($work_site_model, 'distance', array('class' => 'form-control')); ?>
                        <?php echo $form->error($work_site_model, 'distance'); ?>
                    </div>
                    <input type="hidden" name="submit_type" id="submit_type_site">
                    <div class="row text-right">
                        <div class="col-sm-12">
                            <div class="d-flex justify-content-end min-height-40">
                                <div>
                                    <?php echo CHtml::SubmitButton($work_site_model->isNewRecord ? 'Save & Add Site' : 'Update', array('class' => 'btn blue save_btn', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
                                </div>
                                <div>
                                    <?php echo CHtml::SubmitButton('Save & Submit', array('class' => 'btn blue save_btn margin-left-5 save-and-add-button', 'id' => 'save_cnt', 'name' => 'cont_btn')); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                    <div class="row">
                        <?php
                        if (!$model->isNewRecord || $model->isNewRecord) {
                            $this->widget(
                                'zii.widgets.grid.CGridView',
                                array(
                                    'id' => 'clientsite-grid',
                                    'dataProvider' => $work_site_model->search(),
                                    'itemsCssClass' => 'table table-bordered',
                                    'template' => '<div class="table-responsive">{items}</div>',
                                    'pager' => array(
                                        'id' => 'dataTables-example_paginate',
                                        'header' => '',
                                        'prevPageLabel' => 'Previous ',
                                        'nextPageLabel' => 'Next '
                                    ),
                                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                                    // 'filter' => $model,
                                    'columns' => array(
                                        array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-60'), ),
                                        array(
                                            'class' => 'CButtonColumn',
                                            'htmlOptions' => array('class' => 'width-60'),
                                            //'template' => '{deletesite}{active}{inactive}',
                                            'template' => '{update}{delete}',
                                            'buttons' => array(

                                                'update' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,
                                                    'click' => 'function(e){e.preventDefault();editsite($(this).attr("href"));}',

                                                    'options' => array('class' => 'actionitem icon-pencil icon-comn'),
                                                ),


                                                'delete' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,

                                                    'visible' => "isset(Yii::app()->user->role) && (in_array('/clientsite/delete', Yii::app()->session['menuauthlist']))",

                                                    'click' => 'function(e){e.preventDefault();deleteclientsite($(this).attr("href"));}',
                                                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                                                ),
                                            ),
                                        ),
                                        'site_name',
                                        array(
                                            //'filter' => false,
                                            'name' => 'pid',
                                            'value' => '(isset($data->p->name) ? $data->p->name : "")',
                                            'filter' => CHtml::listData(Projects::model()->findAll(
                                                array(
                                                    'select' => array('pid,name'),
                                                    'order' => 'name',
                                                    'distinct' => true
                                                )
                                            ), "pid", "name")
                                        ),
                                        'latitude',
                                        'longitude',
                                        array(
                                            'filter' => false,
                                            'name' => 'assignuser',
                                            'header' => 'Site Engineers/Supervisors',
                                            'value' => '$data->getassignedusers($data->id)',
                                            'type' => 'raw',
                                            'visible' => ((Yii::app()->user->role == 1) ? true : false),
                                        ),
                                       
                                    ),
                                )
                            );
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div id="area_info" class="tab-pane fade">
                <!-- ------------------------- Insert your area form here ------------------------------  -->
                <h2 class="tab_head">Area Form</h2>
                <?php
                $form2 = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'htmlOptions' => array('class' => 'padding-left-20 padding-right-20', ),
                        'id' => 'area-form',
                        'action' => $this->createUrl('masters/area/create'),
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:function(form, data, hasError) {
                                if(hasError) {
                                   
                                }
                                else
                                {
                                   
                                // return true;  it will submit default way
                                areaFormSubmit(form, data, hasError); //and it will submit by ajax function
                                   }
                               }',
                        ),
                    )
                );
                ?>
                <div class="row">
                    <div class="col-md-3">
                        <?php echo $form2->labelEx($area_model, 'area_title'); ?>
                        <?php echo $form2->textField($area_model, 'area_title', array('class' => 'form-control')); ?>
                        <?php echo $form2->error($area_model, 'area_title'); ?>
                    </div>
                    <input type="hidden" name="Area[id]" id="Area_id">
                    <div class="col-md-3">
                        <?php echo $form2->labelEx($area_model, 'project_id'); ?>
                        <?php
                        if ($area_model->isNewRecord) {
                            if (Yii::app()->user->project_id != "") {
                                $area_model->project_id = Yii::app()->user->project_id;
                                $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
                            } else {
                                $area_model->project_id = "";
                            }
                        }
                        ?>
                        <?php
                        if (isset($_GET['id']) && !empty($_GET['id'])) {
                            $proj_condition = 'status="1" AND pid = ' . $_GET['id'];
                            $area_model->project_id = $_GET['id'];
                        } else {
                            if (Yii::app()->user->role == 1) {
                                $proj_condition = 'status="1"';
                            } else {
                                $proj_condition = 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)';
                            }
                        }

                        echo $form2->dropDownList(
                            $area_model,
                            'project_id',
                            CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC ')), 'pid', 'name'),
                            array(
                                'empty' => 'Choose a project',
                                'disabled' => $area_model->isNewRecord ? false : true,
                                'class' => 'form-control'
                            )
                        );
                        ?>
                        <?php echo $form2->error($area_model, 'project_id'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form2->labelEx($area_model, 'status'); ?>
                        <?php echo $form2->dropDownList($area_model, 'status', array('1' => 'Enable', '2' => 'Disable'), array('class' => 'form-control', 'options' => array('1' => array('selected' => true)))); ?>
                        <?php echo $form2->error($area_model, 'status'); ?>
                    </div>
                </div>
                <input type="hidden" name="submit_type" id="submit_type_area">
                <div class="row text-right">
                    <div class="col-sm-12">
                        <div class="d-flex justify-content-end min-height-40">
                            <div>
                                <?php echo CHtml::SubmitButton($area_model->isNewRecord ? 'Save & Add Area' : 'Update', array('class' => 'btn blue save_btn', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
                            </div>
                            <div>
                                <?php echo CHtml::SubmitButton('Save & Proceed', array('class' => 'btn blue save_btn margin-left-5 save-and-proceed-button', 'id' => 'save_cnt', 'name' => 'cont_btn')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (!$model->isNewRecord || $model->isNewRecord) {
                            $this->widget(
                                'zii.widgets.grid.CGridView',
                                array(
                                    'id' => 'area-grid',
                                    'dataProvider' => $area_model->search(),
                                    'itemsCssClass' => 'table table-bordered',
                                    'pager' => array(
                                        'id' => 'dataTables-example_paginate',
                                        'header' => '',
                                        'prevPageLabel' => 'Previous ',
                                        'nextPageLabel' => 'Next '
                                    ),
                                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                                    'columns' => array(
                                        array('class' => 'IndexColumn', 'header' => 'S.No.'),
                                        array(
                                            'class' => 'CButtonColumn',
                                            // 'template' => isset(Yii::app()->user->role) && (in_array('/masters/area/update', Yii::app()->session['menuauthlist'])) ? '{update}' : '',
                        
                                            'template' => '{update}{delete}',


                                            'buttons' => array(
                                                'update' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,
                                                    'visible' => "isset(Yii::app()->user->role) && (in_array('/masters/area/update', Yii::app()->session['menuauthlist']))",

                                                    'click' => 'function(e){e.preventDefault();editArea($(this).attr("href"));}',
                                                    'options' => array('class' => 'update_milestone actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                                                ),

                                                'delete' => array(
                                                    'label' => '',
                                                    'imageUrl' => false,

                                                    'visible' => "isset(Yii::app()->user->role) && (in_array('/masters/area/delete', Yii::app()->session['menuauthlist']))",

                                                    'click' => 'function(e){e.preventDefault();deletearea($(this).attr("href"));}',
                                                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                                                ),
                                            ),
                                        ),
                                        array(
                                            'name' => 'area_title',
                                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                                        ),
                                        array(
                                            'name' => 'project_id',
                                            'value' => '$data->project->name',
                                            'type' => 'raw',
                                            'filter' => CHtml::listData(Projects::model()->findAll(
                                                array(
                                                    'select' => array('pid,name'),
                                                    'condition' => 'status=1',
                                                    'order' => 'name',
                                                    'distinct' => true
                                                )
                                            ), "pid", "name")
                                        ),
                                        array(
                                            'name' => 'status',
                                            'value' => function ($data) {
                                                if ($data->status == 1) {
                                                    $status = 'Enable';
                                                } else {
                                                    $status = 'Disable';
                                                }
                                                return $status;
                                            },
                                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                                        ),
                                        
                                    ),
                                )
                            );
                        }
                        ?>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <script>
        $(document).ready(function () {
            var maximumDate = $("#Projects_end_date").val();
            $("#Projects_start_date").datepicker({
                dateFormat: 'dd-M-yy',
                // maxDate: new Date(maximumDate)
            });
            $("#Projects_end_date").datepicker({
                dateFormat: 'dd-M-yy',
            });
            $("#Projects_start_date").change(function () {
                $("#Projects_end_date").datepicker('option', 'minDate', $(this).val());
            });
            $('#Milestone_start_date').datepicker({
                dateFormat: 'd-M-y',

            });
            $('#Milestone_end_date').datepicker({
                dateFormat: 'd-M-y',

            });
            $('#Tasks_start_date').datepicker({
                dateFormat: 'd-M-y',

            });
            $('#Tasks_due_date').datepicker({
                dateFormat: 'd-M-y',

            });

            $('#BudgetHead_start_date').datepicker({
                dateFormat: 'd-M-y',

            });

            $('#BudgetHead_end_date').datepicker({
                dateFormat: 'd-M-y',

            });

            $('.client_section').hide();


        });

        $(document).ready(function () {
            var a = 0;
            var result = $('.img_msg');
            //binds to onchange event of your input field
            $('#Projects_img_path').bind('change', function () {
                var ext = $('#Projects_img_path').val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                    $('.img_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.</span>');
                    a = 0;
                } else {
                    var picsize = (this.files[0].size);
                    if (picsize > 2000000) {
                        $('.img_msg').html('<span class="red-color">Maximum File Size Limit is 2MB.</span>');
                        a = 0;
                    } else {
                        a = 1;
                        $('.img_msg').html('');
                    }
                }
                if (a == 1) {
                    $('#save_btn').removeAttr('disabled');
                } else {
                    $('#save_btn').attr('disabled', 'disabled');
                }
            });
            $('#Projects_report_image').bind('change', function () {
                var ext = $('#Projects_report_image').val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                    $('.report_img_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.</span>');
                    a = 0;
                } else {
                    var picsize = (this.files[0].size);
                    if (picsize > 2000000) {
                        $('.report_img_msg').html('<span class="red-color">Maximum File Size Limit is 2MB.</span>');
                        a = 0;
                    } else {
                        a = 1;
                        $('.report_img_msg').html('');
                    }
                }
                if (a == 1) {
                    $('#save_btn').removeAttr('disabled');
                } else {
                    $('#save_btn').attr('disabled', 'disabled');
                }
            });


            $(".new_client").click(function () {
                $('.client_section').show();
            });
            $(".client_save").click(function () {
                var client_name = $("#Clients_name").val();
                var project_type = $("#Clients_project_type").val();
                if (project_type != '' && client_name != '') {
                    $("#Clients_name_em_").hide();
                    $("#Clients_name_em_").html('');
                    $("#Clients_project_type_em_").hide();
                    $("#Clients_project_type_em_").html('');
                    $.ajax({
                        'url': '<?php echo Yii::app()->createAbsoluteUrl('projects/createclient'); ?>',
                        data: {
                            client_name: client_name,
                            project_type: project_type
                        },
                        dataType: 'Json',
                        type: 'GET',
                        success: function (result) {
                            if (result.status == 1) {
                                $("#Projects_client_id").html(result.option);
                                $("#Projects_client_id").val(result.lastid);
                                $('.client_section').hide();
                            }
                        }
                    })

                } else {
                    if (client_name == '') {
                        $("#Clients_name_em_").show();
                        $("#Clients_name_em_").html('Name cannot be blank');
                        $("#Clients_project_type_em_").show();
                        $("#Clients_project_type_em_").html('Project Type cannot be blank');
                    }
                }
            })
            $(".close-panel").click(function () {
                $(".client_section").hide();
            })


            $('#Milestone_project_id').on('change', function (e) {
                var valueSelected = this.value;
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('masters/milestone/getdates'); ?>',
                    data: {
                        project_id: valueSelected
                    },
                    method: "POST",
                    dataType: "json",
                    success: function (result) {
                        $('#Milestone_start_date').val(result.start);
                        $('#Milestone_end_date').val(result.end);
                    }
                });
            });

        });
    </script>

    <script>
        $(document).ready(function () {

            //     $('.update_milestone').click(function(){
            // alert('dgdfg');
            //     });
            $('.save_btn').click(function () {
                var buttonClickedID = this.id;

                $('#submit_type').val(buttonClickedID);
                $('#submit_type_budgethead').val(buttonClickedID);
                $('#submit_type_milestone').val(buttonClickedID);
                $('#submit_type_site').val(buttonClickedID);
                $('#submit_type_area').val(buttonClickedID);
                $('#submit_type_worktype').val(buttonClickedID);
                $('#submit_type_task').val(buttonClickedID);
            });
        });

        function editmilestone(href) {
            var id = getURLParameter(href, 'id');
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('masters/milestone/getmodel'); ?>",
                "data": {
                    id: id
                },
                "dataType": "json",
                success: function (result) {
                    if (result.stat == 1) {
                        $('#Milestone_milestone_title').val(result.milestone_title);
                        $('#Milestone_start_date').val(result.start_date);
                        $('#Milestone_end_date').val(result.end_date);
                        $('#Milestone_status').val(result.status);
                        $('#Milestone_id').val(result.id);
                        $('#Milestone_ranking').val(result.ranking);
                        $('#Milestone_budget_id').val(result.budget_head);
                    }
                }
            });
        }

        function deletemilestone(href) {
            var id = getURLParameter(href, 'id');
            var answer = confirm("Are you sure you want to delete?");
            var url = "<?php echo $this->createUrl('masters/milestone/delete&id=') ?>" + id;
            if (answer) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: url,
                    success: function (response) {
                        $('html, body').animate({
                            scrollTop: $("#content").position().top - 100
                        }, 500);
                        if (response.response == "success") {
                            $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                        } else if (response.response == "warning") {
                            $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                        } else {
                            $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                        }
                        $("#milestone-grid").load(location.href + " #milestone-grid");

                    }
                });
            }


        }



        function deletearea(href) {
            var id = getURLParameter(href, 'id');
            var answer = confirm("Are you sure you want to delete?");
            var url = "<?php echo $this->createUrl('masters/area/delete&id=') ?>" + id;
            if (answer) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: url,
                    success: function (response) {
                        $('html, body').animate({
                            scrollTop: $("#content").position().top - 100
                        }, 500);
                        if (response.response == "success") {
                            $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                        } else if (response.response == "warning") {
                            $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                        } else {
                            $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                        }
                        $("#area-grid").load(location.href + " #area-grid");

                    }
                });
            }
        }

        function deleteworktype(href) {
            var id = getURLParameter(href, 'id');
            var answer = confirm("Are you sure you want to delete?");
            var url = "<?php echo $this->createUrl('masters/projectWorkType/delete&id=') ?>" + id;

            if (answer) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: url,
                    success: function (response) {
                        $('html, body').animate({
                            scrollTop: $("#content").position().top - 100
                        }, 500);
                        if (response.response == "success") {
                            $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                        } else if (response.response == "warning") {
                            $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                        } else {
                            $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                        }
                        $("#worktype-grid").load(location.href + " #worktype-grid");

                    }
                });
            }
        }



        function deleteclientsite(href) {
            var id = getURLParameter(href, 'id');
            var answer = confirm("Are you sure you want to delete?");
            var url = "<?php echo $this->createUrl('clientsite/delete&id=') ?>" + id;

            if (answer) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: url,
                    success: function (response) {
                        $('html, body').animate({
                            scrollTop: $("#content").position().top - 100
                        }, 500);
                        if (response.response == "success") {
                            $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                        } else if (response.response == "warning") {
                            $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                        } else {
                            $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                        }
                        $("#clientsite-grid").load(location.href + " #clientsite-grid");

                    }
                });
            }
        }



        function deletbudgethead(href) {
            var id = getURLParameter(href, 'id');
            var answer = confirm("Are you sure you want to delete?");
            var url = "<?php echo $this->createUrl('/budgetHead/delete&id=') ?>" + id;

            if (answer) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: url,
                    success: function (response) {
                        $('html, body').animate({
                            scrollTop: $("#content").position().top - 100
                        }, 500);
                        if (response.response == "success") {
                            $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                        } else if (response.response == "warning") {
                            $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                        } else {
                            $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                        }
                        $("#budget-head-grid").load(location.href + " #budget-head-grid");

                    }
                });
            }
        }

        function editbudgethead(href) {
            var id = getURLParameter(href, 'id');
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('budgetHead/getmodel'); ?>",
                "data": {
                    id: id
                },
                "dataType": "json",
                success: function (result) {
                    if (result.stat == 1) {
                        $('#BudgetHead_budget_head_title').val(result.budget_head_title);
                        $('#BudgetHead_start_date').val(result.start_date);
                        $('#BudgetHead_end_date').val(result.end_date);
                        $('#BudgetHead_status').val(result.status);
                        $('#BudgetHead_id').val(result.id);
                        $('#BudgetHead_ranking').val(result.ranking);
                    }
                }
            });

        }

        function editArea(href) {
            var id = getURLParameter(href, 'id');
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('masters/area/getmodel'); ?>",
                "data": {
                    id: id
                },
                "dataType": "json",
                success: function (result) {
                    if (result.stat == 1) {
                        $('#Area_area_title').val(result.area_title);
                        $('#Milestone_status').val(result.status);
                        $('#Area_id').val(result.id);
                    }
                }
            });
        }

        function editWorktype(href) {
            var id = getURLParameter(href, 'id');
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('masters/projectWorkType/getmodel'); ?>",
                "data": {
                    id: id
                },
                "dataType": "json",
                success: function (result) {
                    if (result.stat == 1) {
                        $('#ProjectWorkType_work_type_id').val(result.work_type).trigger('change');
                        $('#ProjectWorkType_ranking').val(result.ranking);
                        $('#ProjectWorkType_id').val(result.id);


                    }
                }
            });
        }

        function editsite(href) {
            var id = getURLParameter(href, 'id');
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('clientsite/getmodel'); ?>",
                "data": {
                    id: id
                },
                "dataType": "json",
                success: function (result) {
                    if (result.stat == 1) {
                        $('#Clientsite_site_name').val(result.site_name);
                        $('#Clientsite_latitude').val(result.latitude);
                        $('#Clientsite_longitude').val(result.longitude);
                        $('#Clientsite_id').val(result.id);
                        $('#Clientsite_distance').val(result.distance);
                        var userss = result.users_array.split(',');
                        $('.user_ids').val(userss);
                        $('.user_ids').trigger('change');
                    }
                }
            });
        }

        function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
        }

        //new form submission starts
        function projectFormSubmit(form, data, hasError) {
            if (!hasError) {
                var formData = new FormData($("#projects-form")[0]);
                var participants = hot.getData();
                var participantsJson = JSON.stringify(participants);
                formData.append('participants', participantsJson);
                // Append Handsontable data to FormData object
                $('.loaderdiv').show();
                // window.location.reload();
                $.ajax({
                    "type": "POST",
                    "url": "<?php echo Yii::app()->createUrl('projects/addproject'); ?>",
                    "dataType": "json",
                    "data": formData,
                    "cache": false,
                    "contentType": false,
                    "processData": false,
                    "success": function (data) {
                        $('.loaderdiv').hide();


                        if (data.stage == 'milestone') {


                            if (data.status == 1) {
                                $("#success_message").fadeIn().delay(1000).fadeOut();
                                $("#milestone-grid").load(location.href + " #milestone-grid");

                            } else if (data.status == 2) {
                                $("#error_message").fadeIn().delay(1000).fadeOut();

                            }
                            if (data.next_level == 0) {
                                if (data.new_record == 1) {
                                    $("#success_message").fadeIn().delay(1000).fadeOut();
                                    location.reload();

                                } else {
                                    $("#projects-form")[0].reset();

                                }
                                var array = data.assigned_users.split(',');
                                $('.assigned_to_').val(array);
                                $('.assigned_to_').trigger('change');
                            } else if (data.next_level == 1) {

                                $("#milestone-grid").load(location.href + " #milestone-grid");
                                $('#project_details').removeClass('active');
                                $('#project_details').addClass('progress_tab');
                                $('#Milestone_project_id').html(data.html);
                                $('#Tasks_project_id').html(data.html);
                                $('#Clientsite_pid').html(data.html);
                                $('#Area_project_id').html(data.html);
                                $('#ProjectWorkType_project_id').html(data.html);
                                $('#Milestone_start_date').val(data.project_start);
                                $('#Milestone_end_date').val(data.project_end);
                                $('#project_info').removeClass('active in');
                                $('#milestone_details').addClass('active');
                                $('#milestone_info').addClass('active in');
                            }
                        } else {
                            if (data.status == 1) {
                                $("#success_message").fadeIn().delay(1000).fadeOut();
                                $("#budget-head-grid").load(location.href + " #budget-head-grid");

                            } else if (data.status == 2) {
                                $("#error_message").fadeIn().delay(1000).fadeOut();

                            }
                            if (data.next_level == 0) {
                                if (data.new_record == 1) {
                                    location.reload();

                                } else {
                                    $("#projects-form")[0].reset();
                                    location.reload();
                                }
                                var array = data.assigned_users.split(',');
                                $('.assigned_to_').val(array);
                                $('.assigned_to_').trigger('change');
                            } else if (data.next_level == 1) {

                                $("#budget-head-grid").load(location.href + " #budget-head-grid");
                                $('#project_details').removeClass('active');
                                $('#project_details').addClass('progress_tab');
                                $('#BudgetHead_project_id').html(data.html);
                                $('#Milestone_project_id').html(data.html);
                                $('#Tasks_project_id').html(data.html);
                                $('#Clientsite_pid').html(data.html);
                                $('#Area_project_id').html(data.html);
                                $('#ProjectWorkType_project_id').html(data.html);
                                $('#BudgetHead_start_date').val(data.project_start);
                                $('#BudgetHead_end_date').val(data.project_end);

                                $('#project_info').removeClass('active in');
                                $('#budget_head_details').addClass('active');
                                $('#budget_head_info').addClass('active in');
                            }
                        }
                    } // success close

                })
                    .fail(function ($data) {

                    })
                    // .always(function() {
                    //     $('.loaderdiv').hide();
                    //     $("#error_message").fadeIn().delay(1000).fadeOut();
                    // })
                    ;
            } else {
                $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
                setTimeout('$("#successMessage").fadeOut("slow")', 2000);

            }

        }
        //project form submission ends
        const header_data = ['Name', 'Initial', 'Designation', 'Company', 'Company Initial'];
        const autosave = false;
        var ss = '<?php echo $body_array; ?>';
        var dd = JSON.parse(ss);
        const container = document.getElementById('participants-table');
        const hot = new Handsontable(container, {

            data: Handsontable.helper.createSpreadsheetData(),
            data: dd,
            hiddenColumns: {
                columns: [5] // Specify the index of the column you want to hide (0-based index)
            },
            startRows: 5,
            startCols: 12,
            rowHeaders: true,
            colHeaders: header_data,
            width: 'auto',
            height: 'auto',
            minSpareRows: 3,
            minSpareColumns: 3,
            customBorders: true,
            stretchH: 'all',
            licenseKey: 'non-commercial-and-evaluation',
            contextMenu: {
                items: {
                    'remove_row': {
                        name: 'Remove row'
                    }
                }
            },
            afterChange: function (change, source) {
                if (source === 'loadData') {
                    return; //don't save this change
                }

                if (!autosave.checked) {
                    return;
                }

                // clearTimeout(autosaveNotification);            
            }
        });

        // budgethead form submission starts
        function budgetheadFormSubmit(form, data, hasError) {
            if (!hasError) {
                $('.loaderdiv').show();
                var formData = new FormData($("#budget-head-form")[0]);
                $.ajax({
                    "type": "POST",
                    "url": "<?php echo Yii::app()->createUrl('budgetHead/create'); ?>",
                    "dataType": "json",
                    "data": formData,
                    "cache": false,
                    "contentType": false,
                    "processData": false,
                    "success": function (data) {
                        $('.loaderdiv').hide();
                        if (data.status == 1) {
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            $("#budget-head-grid").load(location.href + " #budget-head-grid");

                        } else if (data.status == 2) {
                            $("#error_message").fadeIn().delay(1000).fadeOut();

                        }

                        if (data.next_level == 0) {

                            $('#Milestone_start_date').val(data.budget_head_start_date);
                            $('#Milestone_end_date').val(data.budget_head_end_date);
                            $('#Milestone_budget_id').html(data.budget_head_html)

                            $("#budget-head-form")[0].reset();
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                        } else if (data.next_level == 1) {


                            $("#milestone-grid").load(location.href + " #milestone-grid");
                            $('#budget_head_details').removeClass('active');
                            $('#budget_head_details').addClass('progress_tab');

                            $('#Milestone_start_date').val(data.budget_head_start_date);
                            $('#Milestone_end_date').val(data.budget_head_end_date);
                            $('#Milestone_budget_id').html(data.budget_head_html)
                            $('#budget_head_info').removeClass('active in');
                            $('#milestone_details').addClass('active');
                            $('#milestone_info').addClass('active in');
                        }




                    }
                });
            } else {
                $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
                setTimeout('$("#successMessage").fadeOut("slow")', 2000);
            }
        }
        // budgethead form submission ends    




        //milestone form submission starts
        function milestoneFormSubmit(form, data, hasError) {
            if (!hasError) {
                $('.loaderdiv').show();
                var formData = new FormData($("#milestone-form")[0]);
                $.ajax({
                    "type": "POST",
                    "url": "<?php echo Yii::app()->createUrl('masters/milestone/create'); ?>",
                    "dataType": "json",
                    "data": formData,
                    "cache": false,
                    "contentType": false,
                    "processData": false,
                    "success": function (data) {
                        $('.loaderdiv').hide();
                        if (data.status == 1 && data.next_level == 1) {

                            $("#tasks-grid").load(location.href + " tasks-grid");
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            $('#main_task_info').addClass('active in');
                            $('#milestone_details').removeClass('active');
                            $('#milestone_details').addClass('progress_tab');
                            $("#main_task-form").load(location.href + " #main_task-form");
                            $('#Tasks_milestone_id').html(data.milestone_html);
                            $('#Area_project_id').html(data.html);
                            $('#milestone_info').removeClass('active in');
                            $('#main_task_details').addClass('active');
                        } else if (data.status == 1) {
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            $('#Milestone_id').val('');
                            $("#milestone-grid").load(location.href + " #milestone-grid");
                        } else if (data.status == 2) {
                            $("#error_message").fadeIn().delay(1000).fadeOut();
                        }
                        if (data.next_level == 0) {
                            $("#milestone-form")[0].reset();
                            $('#Tasks_milestone_id').html(data.milestone_html);
                        } else if (data.next_level == 1) {
                            $('#milestone_details').removeClass('active');
                            $('#milestone_details').addClass('progress_tab');
                            $("#main_task-form").load(location.href + " #main_task-form");
                            $('#Tasks_milestone_id').html(data.milestone_html);
                            $('#Area_project_id').html(data.html);
                            $('#milestone_info').removeClass('active in');
                            $('#main_task_details').addClass('active');

                            //                            $('#main_task_info').addClass('active in');
                        }
                    }
                });
            } else {
                $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
                setTimeout('$("#successMessage").fadeOut("slow")', 2000);
            }
        }
        //milestone form submission ends
        function areaFormSubmit(form, data, hasError) {
            if (!hasError) {
                $('.loaderdiv').show();
                var formData = new FormData($("#area-form")[0]);
                $.ajax({
                    "type": "POST",
                    "url": "<?php echo Yii::app()->createUrl('masters/area/create'); ?>",
                    "dataType": "json",
                    "data": formData,
                    "cache": false,
                    "contentType": false,
                    "processData": false,
                    "success": function (data) {
                        $('.loaderdiv').hide();
                        if (data.status == 1) {
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            $('#Area_id').val('');
                            $("#area-grid").load(location.href + " #area-grid");
                        } else if (data.status == 2) {
                            $("#error_message").fadeIn().delay(1000).fadeOut();
                        }
                        if (data.next_level == 0) {
                            $("#area-form")[0].reset();
                        } else if (data.next_level == 1) {
                            $('#area_details').removeClass('active');
                            $('#area_details').addClass('progress_tab');
                            $('#ProjectWorkType_project_id').html(data.html);
                            $('#area_info').removeClass('active in');
                            $('#worktype_details').addClass('active');
                            $('#worktype_info').addClass('active in');
                        }

                    }
                });
            } else {
                $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
                setTimeout('$("#successMessage").fadeOut("slow")', 2000);
            }
        }
        //worktype form submission starts
        function worktypeFormSubmit(form, data, hasError) {
            if (!hasError) {
                $('.loaderdiv').show();
                var formData = new FormData($("#project-work-type-form")[0]);
                $.ajax({
                    "type": "POST",
                    "url": "<?php echo Yii::app()->createUrl('masters/projectWorkType/create'); ?>",
                    "dataType": "json",
                    "data": formData,
                    "cache": false,
                    "contentType": false,
                    "processData": false,
                    "success": function (data) {
                        $('.loaderdiv').hide();
                        if (data.status == 1) {
                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            $('#ProjectWorkType_id').val('');
                            $("#project-work-type-form")[0].reset();
                            $("#worktype-grid").load(location.href + " #worktype-grid");
                        } else if (data.status == 2) {
                            $("#error_message").fadeIn().delay(1000).fadeOut();
                        }
                        if (data.next_level == 0) {
                            $("#clientsite-form")[0].reset();
                        } else if (data.next_level == 1) {
                            $('#worktype_details').removeClass('active');
                            $('#worktype_details').addClass('progress_tab');
                            $('#Clientsite_pid').html(data.html);
                            $('#worktype_info').removeClass('active in');
                            $('#worksite_details').addClass('active');
                            $('#worksite_info').addClass('active in');
                        }


                    }
                }).fail(function ($data) {

                }).always(function () {
                    $('.loaderdiv').hide();
                });
            } else {
                $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
                setTimeout('$("#successMessage").fadeOut("slow")', 2000);
            }
        }
        //worktype form submission ends
        //site form submission starts
        function siteFormSubmit(form, data, hasError) {
            if (!hasError) {
                $('.loaderdiv').show();
                //window.location.reload();               
                var formData = new FormData($("#clientsite-form")[0]);
                $.ajax({
                    "type": "POST",
                    "url": "<?php echo Yii::app()->createUrl('clientsite/create'); ?>",
                    "dataType": "json",
                    "data": formData,
                    "cache": false,
                    "contentType": false,
                    "processData": false,
                    "success": function (data) {
                        $('.loaderdiv').hide();
                        if (data.status == 1) {

                            $("#success_message").fadeIn().delay(1000).fadeOut();
                            $('#Clientsite_id').val('');
                            $('.user_ids').val('');
                            $('.user_ids').trigger('change');
                            $("#clientsite-grid").load(location.href + " #clientsite-grid");
                        } else if (data.status == 2) {

                            $("#error_message").fadeIn().delay(1000).fadeOut();
                        }
                        if (data.next_level == 0) {

                            $("#clientsite-form")[0].reset();
                            $('.user_ids').val('');
                        } else if (data.next_level == 1) {

                            window.location.href = data.redirect
                        }


                    }
                });
            } else {
                $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
                setTimeout('$("#successMessage").fadeOut("slow")', 2000);
            }
        }


        $("#Milestone_budget_id").change(function () {
            var id = this.value;

            if (id != '') {
                $.ajax({
                    "type": 'post',
                    "url": "<?php echo Yii::app()->createUrl('budgetHead/getbudgetheaddates'); ?>",
                    "data": {
                        id: id
                    },
                    "dataType": "json",
                    success: function (result) {

                        if (result.status == 1) {
                            $('#Milestone_start_date').val(result.start);
                            $('#Milestone_end_date').val(result.end);
                        }
                    }
                });
            }

        });




    </script>

    <link rel="stylesheet"
        href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            function draganddroprow(gridtable, url, response_msg_area_id) {
                $(gridtable).sortable({
                    items: 'tbody tr',
                    cursor: 'pointer',
                    axis: 'y',
                    dropOnEmpty: false,
                    start: function (e, ui) {
                        ui.item.addClass("selected");
                    },
                    stop: function (e, ui) {
                        var ranked_items = [];
                        ui.item.removeClass("selected");
                        $(this).find("tr").each(function (index) {
                            //                        alert(index);
                            if (index > 0) {
                                var span_dataid = $(this).find(".rankingcol span").attr('data-id');
                                $(this).find("td").eq(5).html("<span data-id=\"" + span_dataid + "\">" + index + "</span>");
                            }
                        });
                        $('.rankingcol span').each(function (index) {
                            if (parseInt($(this).html()) !== NaN) {
                                //                                console.log($(this).html());
                                dataid = $(this).attr('data-id');
                                dataorder = $(this).html();
                                ranked_items[index] = [dataid, dataorder];
                                //                                console.log(ranked_items);
                            }
                        });
                        $(response_msg_area_id).html('');
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: url,
                            data: {
                                rankingitems: ranked_items
                            },
                            success: function (returnmsg) {
                                $(response_msg_area_id).html(returnmsg.msg);
                                $(response_msg_area_id).css('border', returnmsg.status_color);
                                $(response_msg_area_id).css('color', returnmsg.status_color);
                                $(response_msg_area_id).css('border', "1px solid " + returnmsg.status_color);
                            }
                        });
                    }
                });
            };

            //draganddroprow();
            $(document).ajaxComplete(function (event, xhr, settings) {
                if (settings.url.includes('Milestone_sort=ranking&')) {
                    <?php $sortajaxurl = Yii::app()->createAbsoluteUrl('projects/updateRank'); ?>
                    draganddroprow("#milestone-grid", '<?php echo $sortajaxurl ?>', '#rankupdatemessage');
                }
            });



            $('.project_clone').on('change', function () {
                var clone_project_id = $('.project_clone').val();
                if (clone_project_id != '') {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '<?php echo Yii::app()->createAbsoluteUrl('projects/getCloneDetails'); ?>',
                        data: {
                            clone_project_id: clone_project_id
                        },
                        success: function (result) {
                            $('#Projects_budget').val(result.budget);
                            $('#Projects_total_square_feet').val(result.sqft);
                            $('#Projects_description').val(result.description);
                            $('#Projects_name').val(result.project_name);
                            if (result.status == 1) {
                                $("#Projects_status_0").prop("checked", true);
                            } else {
                                $("#Projects_status_1").prop("checked", true);
                            }
                            if (result.billable == 3) {
                                $("#Projects_billable_1").prop("checked", true);
                            } else {
                                $("#Projects_billable_0").prop("checked", true);
                            }
                            $('#Projects_start_date').val(result.start_date);
                            $('#Projects_end_date').val(result.end_date);
                        },
                        error: function (result) {

                        }
                    });
                } else {
                    $('#Projects_budget').val("");
                    $('#Projects_total_square_feet').val("");
                    $('#Projects_description').val("");
                    $('#Projects_name').val("");
                    var todays_date = "<?php echo date('d-M-Y'); ?>";
                    var next_date = "<?php echo date('d-M-Y', strtotime("+1 month")) ?>";
                    $('#Projects_start_date').val(todays_date);
                    $('#Projects_end_date').val(next_date);
                    $("#Projects_status_0").prop("checked", true);
                    $("#Projects_billable_0").prop("checked", true);
                }
            });


            $('#area_details').on('click', function () {
                $("#area-grid").load(location.href + " #area-grid");

                //  $("#area-form").load(location.href + " #area-form");


            });

            $('#worktype_details').on('click', function () {
                $("#worktype-grid").load(location.href + " #worktype-grid");

            });

            $('#worksite_details').on('click', function () {
                $("#clientsite-grid").load(location.href + " #clientsite-grid");
            });

            $('#milestone_details').on('click', function () {
                $('#milestone-grid').load(location.href + " #milestone-grid");

            });




        });
    </script>
    <?php
    $settings = GeneralSettings::model()->findByPk(1);
    ?>
    <!-- <script type="text/javascript" src="https://js.live.net/v7.2/OneDrive.js"></script> -->
    <script type="text/javascript">
        function launchOneDrivePicker() {
            var currentLocation = window.location;

            var onedrive_client_id = "<?= isset($settings->onedrive_clientId) ? $settings->onedrive_clientId : '' ?>";


            var odOptions = {
                clientId: onedrive_client_id, // add app client id here
                action: "query",
                multiSelect: true,
                viewType: "all",
                openInNewWindow: true,
                advanced: {
                    redirectUri: "<?php echo Yii::app()->createAbsoluteUrl('projects/addproject', array(), 'https') ?>",
                    //redirectUri:currentLocation,
                    endpointHint: "api.onedrive.com",
                },

                success: function (files) {
                    console.log(JSON.stringify(files));
                    var files_array = files.value;
                    if (files_array) {
                        for (var i in files_array) {
                            //window.processOneDriveFile(files_array[i]);
                        }
                    }
                },
                cancel: function () {
                    /* cancel handler */
                },
                error: function (error) {
                    console.log(data);
                    console.debug(data);
                    console.log("error");
                }
            }
            OneDrive.open(odOptions);
        }

        function processOneDriveFile(file) {
            var file_name = file.name;
            var file_size = file.size;
            var download_url = file['@microsoft.graph.downloadUrl'];
            var data = {
                file_name: file_name,
                file_size: file_size,
                download_url: download_url,
                command: 'handle-onedrive-file',
            };
            console.log(data);

            $.ajax({
                url: 'file_handler.php',
                type: 'post',
                data: data,
                error: function (data) {
                    console.log(data);
                    console.debug(data);
                    console.log("error");
                },
                success: function (data) {
                    console.log(data);
                    console.log("success");
                }
            });
        }


    </script>