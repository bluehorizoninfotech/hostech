<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<?php


$this->breadcrumbs = array(
    'Projects' => array('index'),
    $model->project_id => array('view', 'id' => $model->id),
    'Update',
);
?>

<h2>Update Meeting: <span></span></h2>
<?php

?>
<div class="container ">
    <?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'cru-dialog',
        'options' => array(
            'title' => 'Add Task',
            'autoOpen' => false,
            'modal' => false,
            'width' => "590",
            'height' => "auto",
        ),
    ));
    ?>
    <iframe id="cru-frame" width="550" height="550" class="min-height-500"></iframe>

    <?php
    $this->endWidget();
    ?>
    <?php echo $this->renderPartial('_mom_form', array(
        'model' => $model,
        'site_model' => $site_model,
        'meeting_points_model' => $meeting_points_model,
        'next_meeting_data' => $next_meeting_data,
        'projects' => $projects,
        'report_model' => $report_model,
        'approved_by' => $approved_by,
        'body_array' => $body_array
    )); ?>
</div>
<script>
    $(document).ready(function() {
        $.noConflict();
        var assigned_data = '<?php echo $approved_by; ?>';
        var array = assigned_data.split(',');
        $('.approved_by_val').val(array);
        $('.approved_by_val').trigger('change');

    });
</script>