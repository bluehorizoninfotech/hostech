<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js" type="text/javascript"></script>
<br clear="all">
<div class="calendertable-sec">
<div class="table_hold">

    <form name="emp_att_grid" id="emp_att_grid">
        <div id="parent">
            <table class="table attnd_table table-bordered" id="fixTable">
                <?php
                $date1 = new DateTime($start_date);
                $date2 = new DateTime($end_date);
                $diff = $date2->diff($date1)->format("%a");
                $totdays = $diff + 1;
                ?>
                <thead>
                    <tr>
                        <th rowspan="2" colspan="4" class="fixed_th">
                        </th>
                        <?php
                        for ($i = 0; $i < $totdays; $i++) {
                            $week_date = $this->add_date($start_date, $i);
                            $week_datenext = $this->add_date($start_date, $i + 1);
                            $week_day = explode('/', $week_date);
                            $dat_next = explode('/', $week_datenext);
                            $fromday = $this->add_date($start_date, 0);
                            if ($fromday == $week_date) {
                                $flag = 1;
                            }
                            $countmonth = 0;
                            if ($week_day[1] != $dat_next[1]) {

                                $endmonthday = $week_day[0];
                                $startmothday = explode('-', $start_date);
                                $monthdaysdiff = ($endmonthday - $dat_next[0]) + 1;
                                if ($flag == 1) {
                                    $monthdaysdiff = ($endmonthday - $startmothday[2]) + 1;
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                    $countmonth++;
                                    $flag = 0;
                                } else {
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                }
                            }
                            $week_dates[] = $week_date;
                        }
                        $endmonth = explode('-', $end_date);

                        $startdatelast = $endmonth[0] . '-' . $endmonth[1] . '-01';
                        $monthdayarr[$endmonth[1]] = $this->getcountBetween2Dates($startdatelast, $end_date);
                        ?>
                        <?php
                        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
                        foreach ($monthdayarr as $month => $diffdays) {
                            if (count($monthdayarr) == 1) {
                                $diffdays = $totdays;
                            }

                            echo "<th colspan='" . $diffdays . "' class='month-period'>" . $monthsarr[$month] . "</th>";
                        }
                        ?>
                        <!-- <th colspan="1" class="days-period">Total Summary(<span class="monthtot"><?= $diffdays; ?></span>) </th> -->

                    </tr>
                    <tr>

                        <?php
                        $days = $this->getDatesBetween2Dates($start_date, $end_date);
                        $global_assign = '';
                        foreach ($days as $key => $value) {

                            $strtime = strtotime($value);
                            $prev_date = date('Y-m-d', strtotime('-2 day'));

                            $global_assign .= '<th class="global_row" id="i_' . $strtime . '"></th>' . "\n";
                            echo '<th>' . substr(date('D', strtotime($value)), 0, 2) . '<br>' . date('d', strtotime($value)) . '</th>';
                        }
                        ?>


                    </tr>

                    <tr>
                        <th class="fixed_th">


                        <th class="fixed_th hdrborder" colspan="3">
                            Task Name
                        </th>
                        <?php
                        echo $global_assign;
                        ?>

                    </tr>


                </thead>
                <tbody>
                    <tr>
                       
                        <?php
                        for ($p = 0; $p < $totdays; $p++) { ?>
                           
                        <?php }
                        ?>
                    </tr>
                    <?php
                    $count = 0;
                    $task_status='';
                    foreach ($tasks as $task) {

                        $time_entry_array = $task[0];
                        if ($task['task_status'] == 6) {
                            $task_status = 'grey';
                        } elseif ($task['task_status'] == 9) {
                            $task_status = 'green';
                        } elseif ($task['task_status'] == 5) {
                            $task_status = 'orange';
                        } elseif ($task['task_status'] == 8) {
                            $task_status = 'violet';
                        } elseif ($task['task_status'] == 7) {
                            $task_status = 'blue';
                        } elseif ($task['task_status'] == 72) {
                            $task_status = 'dark_blue';
                        }
                        $count++;
                    ?>
                        <tr id="user ?>">
                            <td width="20"></td>
                            <td colspan="3" align="left">
                                <div class="pull-left">
                                    <?= $task['title'] . '#' .$task['id']?>
                                </div>
                            </td>
        </div>

        <?php
                        $task_start =  strtotime($task['start_date']);
                        $task_end = strtotime($task['due_date']);
                        $task_percen = '';
                        $cent_percent_status = 0;
                        for ($p = 0; $p < $totdays; $p++) {
                            $var = $week_dates[$p];
                            $todd = date('d/m/Y');
                            $datecur = str_replace('/', '-', $var);
                            $datecur_ = str_replace('/', '-', $todd);
                            $current_date = strtotime($datecur);
                            $date_check_in_time_entry = date("Y-m-d", strtotime($datecur));

                            $todays_date = strtotime($datecur_);
                            if (array_search($date_check_in_time_entry, array_column($time_entry_array, 'percent_date')) !== False) {
                                $percentage_array_of_date = $time_entry_array[$date_check_in_time_entry];

                                if (($percentage_array_of_date['required_percentage'] == 100) && ($percentage_array_of_date['current_percentage'] != $percentage_array_of_date['required_percentage'])) {
                                    $task_percen = 'red_border';
                                } else {
                                    if ($percentage_array_of_date['required_percentage'] != 0) {

                                        $half_required_perce = $percentage_array_of_date['required_percentage'] / 2;
                                        if ($percentage_array_of_date['current_percentage'] == 0) {
                                            $task_percen = 'red_border';
                                        } elseif ($percentage_array_of_date['current_percentage'] < $half_required_perce) {
                                            $task_percen = 'red_border';
                                        } elseif (($percentage_array_of_date['current_percentage'] >= $percentage_array_of_date['required_percentage'])) {
                                            $task_percen = 'green_border';
                                        } elseif ($percentage_array_of_date['current_percentage'] == 100) {
                                            $task_percen = 'green_border';
                                            $cent_percent_status = 1;
                                        } else {
                                            $task_percen = 'yellow_border';
                                        }
                                    } else {
                                        if ($percentage_array_of_date['current_percentage'] == 100) {
                                            $task_percen = 'green_border';
                                            $cent_percent_status = 1;
                                        } elseif ($percentage_array_of_date['current_percentage'] != 0) {
                                            $task_percen = 'green_border';
                                        } else {
                                            $task_percen = 'red_border';
                                        }
                                    }
                                }
                                if ($percentage_array_of_date['percent_date'] != '') {
                                    $time_entry_date = str_replace('/', '-', $percentage_array_of_date['percent_date']);
                                    $time_entry_date_ = strtotime($time_entry_date);
                                    $current_perc = $percentage_array_of_date['current_percentage'] . '%';
                                } else {
                                    $time_entry_date_ = 0;
                                }
                            } else {
                                $current_perc = '';
                                $time_entry_date_ = 0;
                            }
                            if ($cent_percent_status == 1) {
                                $task_percen = 'green_border';
                            }
                            // if ($percentage_array_of_date['current_percentage'] == 0) {
                            //     $current_perc = '';
                            // }

        ?>

            <td width="20" data-toggle="modal" id="c_<?= $current_date ?>" data-backdrop="static" data-target="#entry" class="coldate <?= ($current_date >= $task_start) && ($current_date <= $task_end) ? $task_status : '' ?>
                                <?= (($current_date >= $task_start) && ($current_date <= $task_end) && ($current_date <= $todays_date) && ($current_date == $time_entry_date_)) ? ' ' . $task_percen : '' ?> 
                                <?= $current_date == $todays_date ? 'today_highlight' : '' ?>" data-value="" data-coldate="">
                <?= (/*$task['percent_date'] != '' && */($current_date == $time_entry_date_ /*|| $time_entry_date_ > $id*/)) ? $current_perc : '' ?>
            </td>
        <?php } ?>
        </tr>
        <?php
                        if (!empty($task['children'])) {
                            $child = 0;
                            $this->printArrayList($task['children'], $totdays, $week_dates, $child);
        ?>
        <?php } ?>
    <?php }
    ?>
    </tbody>
    </table>
</div>
</form>
</div>

                        </div>



<script type="text/javascript">
    var $ = jQuery;
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({
            'left': 2,
            'foot': false,
            'head': true
        });
    });




    $(document).ready(function() {





    });
</script>

