<script src="<?php echo $this->customAssets('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js','/js/jquery.min.js');?>"></script>
<?php
if(isset($_GET['id'])){
    $project_id = $_REQUEST['id'];
}else{
    $project_model = Projects::model()->find(['condition'=>'status = 1 ORDER BY updated_date DESC']);
    if(Yii::app()->user->project_id !=""){
        $project_id = Yii::app()->user->project_id;
    }else{
        // $project_id = $project_model['pid'];
        $project_id='';
    }
}

?>
<div class="chart-backup-sec">
<div class="padding-bottom-30">
 <?= CHtml::dropDownList('project_id', $project_id, 
 CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'), 
 array('empty' => 'Choose a project','class'=>'form-control pull-right width-25-percentage change_project',
 'id'=>'project_id')); ?>
 </div>
<div class="main_content">
    <div class="clearfix page_head">    
      <h1></h1>     
        <div class="loaderdiv margin-top-0 margin-bottom-0 margin-left-auto margin-right-auto width-64">
            <img id="loader" src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" width="50%"/>
        </div>
    </div>  
    <?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger alert-dismissable ">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
   <?php endif; ?>

    <!-- <input type="hidden" id="start_date" value="2019-06-01">
    <input type="hidden" id="end_date" value="2019-06-30"> -->
    
    <?=  CHtml::link('Refresh', '', array('class' => 'btn blue right refresh','id'=>'refresh_btn')); ?>
    <?= CHtml::link( 'PDF', $url = $this->createAbsoluteUrl('/projects/downloadpdf',array('id'=>'415')),array('class' => 'btn blue right refresh','id'=>'download_id')) ?>
    <?= CHtml::link( 'Excel', $url = $this->createAbsoluteUrl('/projects/downloadexcel',array('id'=>'')),array('class' => 'btn blue right refresh','id'=>'download_excel')) ?>
    <span class="pull-right span_style">
    On schedule</span>
    <div class="foo in_progress"></div>
    <span class="pull-right span_style">Behind schedule</span>
    <div class="foo delay"></div>
    <!-- <span class="pull-right span_style">TEST</span> -->
    <!-- <div class="foo balance"></div> -->
    
  
    <div class="row-fluid clearfix">
        <div id="calendertable"></div>
       
     </div>

     <div  id="task_data" class="margin_div clearfix">
    </div>
     
</div>

    </div>



<script type="text/javascript">
$(document).ready(function(){
    var pid = $('#project_id').val();
    var url ="<?php echo $this->createUrl('projects/downloadexcel') ?>";
    $("#download_excel").attr("href", url+'&id='+pid);
});

    reloadcalender();    
$('#refresh_btn') .click(function(){
    reloadcalender();
});
$('#download_id').click(function() {
            var pid = $('#project_id').val();
            var url ="<?php echo $this->createUrl('projects/downloadpdf') ?>";
            $("#download_id").attr("href", url+'&id='+pid);
});
$('#project_id').change(function(){
    $('.loaderdiv').show();
    var pid = $('#project_id').val();
    var url = document.location.href;
    var id ='id';
   var data =  updateQueryStringParameter(url,id,pid);
   
    document.location = data;
    // reloadcalender();
});
function updateQueryStringParameter(uri, key, value) {
      var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
      var separator = uri.indexOf('?') !== -1 ? "&" : "?";
      if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
      }
      else {
        return uri + separator + key + "=" + value;
      }
    }
    function reloadcalender(){

         $('.loaderdiv').show();
        // var startdate = $('#start_date').val();
        // var enddate   = $('#end_date').val();
        var pid = $('#project_id').val();
       
        $('.loaderdiv').show();
        $.ajax({
                method: "get",
                url: "<?php echo $this->createUrl('projects/calandersheet') ?>",
                data: {pid: pid},
            }).done(function (msg) {  

                $("#calendertable").html(msg);
                
                               
            });

        $('.loaderdiv').show();
     
            $.ajax({
                method: "get",
                url: "<?php echo $this->createUrl('site/tasksheet') ?>",
                data: {pid: pid},
            }).done(function (msg) { 
                $('.loaderdiv').show();              
                $("#task_data").html(msg);
                $('.loaderdiv').hide();
                
                
            });
    }
  
</script>
 <script>

 </script>