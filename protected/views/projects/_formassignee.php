<?php
    $form = $this->beginWidget('CActiveForm', array(
			'id'=>'projects-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?> 
<div class="project-formassignee-sec">
<div class="modal-body">
    <div class="clearfix">
        <div class="">
            <div class="row">
                <div class="subrow">
                    <?php echo $form->labelEx($model,'name'); ?>
                    <?php 
                    if($button=='submit'){
                    if(Yii::app()->user->project_id !=""){
                        $model->name = Yii::app()->user->project_id;
                    }else{
                        $model->name = "";
                    }
                    echo $form->dropDownList($model, 'name', CHtml::listData(Projects::model()->findAll(array(
                                    'select' => array('pid,name'),
                                    'condition' => 'assigned_to IS NULL',
                                    'distinct' => true
                                )), 'pid', 'name'), array('empty' => 'Please choose Project', 'class' => 'form-control change_project'));
                    }else{
                          echo $form->textField($model, 'name', array('class'=>'form-control')); 
                    }
					?>
                    <?php //echo $form->textField($model,'name',array('class'=>'form-control',)); ?>
                    <?php echo $form->error($model,'name'); ?>
                </div>
                <div class="subrow">
                    <?php echo $form->labelEx($model,'assigned_to'); ?>
                    <?php 
					$condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name'),'condition' => 'status=0 AND user_type=10','order' => 'first_name ASC');
					echo $form->dropDownList($model, 'assigned_to', CHtml::listData(Users::model()->findAll($condition), 'userid', 'full_name'), array('empty' => '-Choose a resource-','class'=>'form-control')); 
					?>
                    <?php echo $form->error($model,'assigned_to'); ?>
                </div>


            </div>

            <div class="row">
                <div class="subrow">
                    <?php echo $form->labelEx($model, 'start_date'); ?>
                    <?php echo CHtml::activeTextField($model, 'start_date', array('class'=>'form-control','autocomplete'=>'off')); ?>
                    <?php
					/* $this->widget('application.extensions.calendar.SCalendar', array(
						'inputField' => 'Projects_start_date',
						'ifFormat' => '%d-%m-%Y',
					)); */
					?>
                    <?php echo $form->error($model, 'start_date'); ?>
                </div>
                <div class="subrow">

                    <?php echo $form->labelEx($model, 'end_date'); ?>
                    <?php echo CHtml::activeTextField($model, 'end_date', array('class'=>'form-control','autocomplete'=>'off')); ?>
                    <?php
					/* $this->widget('application.extensions.calendar.SCalendar', array(
						'inputField' => 'Projects_end_date',
						'ifFormat' => '%d-%m-%Y',
					)); */
					?>
                    <?php echo $form->error($model, 'end_date'); ?>
                </div>
            </div>




        </div>
    </div>
</div><!-- model body -->
<div class="modal-footer save-btnHold">
    <?php echo CHtml::submitButton(($button=='submit')? 'Submit' : 'Save', array('class' => 'btn blue')); ?>
    <?php 
                 if($button=='submit'){
                
                   echo "<button data-dismiss='modal' class='btn default' onclick='javascript:window.location.reload()'>Close</button>";
                 }else{ 
                     echo CHtml::button('Back',array('class' => 'btn blue','submit' => array('projects/addAssigneetoProjects'))); 
                } ?>
</div>
<?php $this->endWidget(); ?>
            </div>