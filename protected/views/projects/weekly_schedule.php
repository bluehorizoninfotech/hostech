<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js" type="text/javascript"></script>
<br clear="all">
<div class="weekly-schedule-sec">
    <div class="table_hold">
<div class="d-flex align-items-center margin-bottom-10">
    <span class="clr_pal legend-bg-yellow"></span>Start Date
    <span class="clr_pal legend-bg-green margin-left-5"></span>Date Between 
    <span class="clr_pal legend-bg-red margin-left-5"></span>End Date
</div> 

        <div id="parent">
        
            <?php
            if (!empty($tasks)) {
            ?>
                <table class="table attnd_table table-bordered" id="fixTable">
                    <?php
                    $months = array();
                    $task_start_month = (int) date('m', strtotime($start_date));
                    $week_count_total = 0;
                    $i = 0;
                    for ($x = $task_start_month; $x < $task_start_month + 12; $x++) {
                        $month_val = date('m', mktime(0, 0, 0, $x, 1));
                        if ($i == 0) {
                            $year = date("Y", strtotime($start_date));
                            $month_date = date('Y-m-d', strtotime($start_date));
                        } else {
                            $month_date = date("Y-m-d", strtotime("+1 month", strtotime($month_date)));
                            $year = date("Y", strtotime($month_date));
                        }

                        $week_start_date = date('Y-m-d', mktime(0, 0, 0, $x, 1));
                        $week_count = Yii::app()->Controller->getWeeksCount($month_val, $year);
                        $week_count_total += count($week_count);
                        $months[] =
                            array('month' => date('F', mktime(0, 0, 0, $x, 1)), 'weeks_count' => $week_count);
                        $colspan_hlf = round(($week_count_total + 4) / 2);
                        $i++;
                    };

                    ?>
                    <thead>
                        <tr>
                            <th colspan="<?php echo $week_count_total + 4; ?>" class="fixed_th" style="background-color: rgba(245, 130, 32, 0.27); position: relative; text-align: center; top: 0px;color: black;">Master schedule for <?php echo isset($project->name) ? $project->name : ''; ?></th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo  $colspan_hlf; ?>" class="fixed_th bg_blue_head"><?php echo isset($project->name) ? $project->name : ''; ?></th>
                            <th colspan="<?php echo  $colspan_hlf; ?>" class="fixed_th bg_blue_head"><?php echo isset($project->total_square_feet) ? $project->total_square_feet . " SQFT" : ''; ?></th>
                        </tr>
                        <tr>
                            <th rowspan="4" colspan="2">SL NO</th>
                            <th rowspan="4" colspan="2" class="fixed_th">Nature Of Works
                            </th>
                        </tr>

                        <tr>

                            <?php
                            foreach ($months as $key => $value) {

                                echo "<th colspan='" . count($value['weeks_count']) . "' class='month-period'>" . $value['month'] . "</th>";
                            }

                            ?>
                        </tr>
                        <tr>

                            <?php
                            foreach ($months as $key => $value) {
                                for ($w = 1; $w <= count($value['weeks_count']); $w++) {
                                    echo '<th>' . $w . '</th>';
                                }
                            }
                            ?>


                        </tr>

                    </thead>
                    <tbody>

                        <?php
                        $count = 0;
                        $milestone_serial = 1;
                        foreach ($tasks as $key => $milestone_data) {
                            $roman_numeral = Yii::app()->Controller->getRomanNumerals($milestone_serial);
                            $task_datas = $milestone_data;
                            $milestone_model = Milestone::model()->findByPk($key);
                        ?>
                            <tr>
                                <td colspan="2" class="bg_grey bg-cell"><?php echo $roman_numeral; ?></td>
                                <td colspan="2" class="bg_grey bg-cell"><?= isset($milestone_model->milestone_title) ? $milestone_model->milestone_title : "" ?></td>
                                <?php
                                for ($p = 1; $p <= $week_count_total; $p++) {
                                ?>
                                    <td></td>
                                <?php }
                                ?>
                            </tr>
                            <?php
                            $task_serial = 1;
                            foreach ($task_datas as $task) {
                                $time_entry_array = array();
                                if (isset($task[0]))
                                    $time_entry_array = $task[0];
                                $task_status = 'grey';
                                $count++;
                            ?>
                                <tr id="user ?>">
                                    <td colspan="2">
                                        <?php echo $task_serial; ?></td>
                                    <td colspan="2" align="left">
                                        <div class="pull-left" title="<?php echo 'Start Date: ' . date('d-M-Y', strtotime($task['start_date'])) . ', End Date:' . date('d-M-Y', strtotime($task['due_date']))  ?>">
                                            <?= $task['title'] . '#' ?>
                                        </div>
                                    </td>
                                    <?php

                                    $task_start_date = strtotime($task['start_date']);
                                    $task_end_date = strtotime($task['due_date']);
                                    $task_percen = '';
                                    $cent_percent_status = 0;

                                    foreach ($months as $key => $value) {
                                        foreach ($value['weeks_count'] as $week_data) {
                                            $week_start_date = $week_data['start_date'];
                                            $week_end_date = $week_data['end_date'];
                                            $start_date_data = strtotime($week_start_date);
                                            $end_date_data = strtotime($week_end_date);
                                            $color = $this->getcolumncolor($week_start_date,  $week_end_date, $task['start_date'], $task['due_date']);
                                            $task_status = $color;

                                    ?>
                                            <td width="" data-toggle="modal" id="" data-backdrop="static" data-target="#entry" task_start="<?= $task['start_date'] ?>" task_end="<?= $task['due_date'] ?>" week_start="<?= $week_start_date ?>" week_end="<?= $week_end_date ?>" class="coldate <?= ($start_date_data >= $task_start_date && $start_date_data <= $task_end_date) || ($end_date_data >= $task_start_date && $end_date_data <= $task_end_date) || ($task_start_date >= $start_date_data && $task_end_date <= $end_date_data) ? $task_status : '' ?>                                 
                                " data-value="" data-coldate="" colr="<?= $task_status ?>">
                                            </td>

                                        <?php
                                        }

                                        ?>
                                    <?php
                                    }
                                    ?>
                                </tr>
                        <?php
                                $task_serial++;
                            }
                            $milestone_serial++;
                        } ?>
                    </tbody>
                </table>
            <?php
            }

            ?>

        </div>

    </div>
</div>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
        $j(" #fixTable").tableHeadFixer({
            'head': true
        });
    });
</script>