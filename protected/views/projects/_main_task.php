   <!-- ------------------------- Insert your milestone form here ------------------------------  -->
   <h2 class="tab_head">Main Task Form</h2>
   <?php
    $form = $this->beginWidget('CActiveForm', array(
        'htmlOptions' => array('class' => 'padding-left-20 padding-right-20',),
        'id' => 'tasks-form',
        'action' => $this->createUrl('tasks/addMainTask'),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'afterValidate' => 'js:function(form, data, hasError) {
                                if(hasError) {
                                   
                                }
                                else
                                {
                                   
                                // return true;  it will submit default way
                                taskFormSubmit(form, data, hasError); //and it will submit by ajax function
                                   }
                               }',
        ),
    ));
    ?>
   <div class="row">
       <div class="col-md-2">
           <?php echo $form->labelEx($model, 'project_id'); ?>
           <?php
            if ($model->isNewRecord) {
                if (Yii::app()->user->project_id != "") {
                    $model->project_id = Yii::app()->user->project_id;
                    $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
                    $model->start_date = date('d-M-y', strtotime(isset($projects->start_date)));
                    $model->due_date = !empty($projects->end_date) ? date('d-M-y', strtotime($projects->end_date)) : NULL;
                } else {
                    $model->project_id = "";
                }
            } else {
                $model->start_date = $this->getmilestonedate($model->id, $model->project_id, 'start');
                $model->due_date = $this->getmilestonedate($model->id, $model->project_id, 'end');
            }
            ?>
           <?php
            if (isset($_GET['id']) && !empty($_GET['id'])) {
                $proj_condition = 'status="1" AND pid = ' . $_GET['id'];
                $model->project_id = $_GET['id'];
            } else {
                if (Yii::app()->user->role == 1) {
                    $proj_condition = 'status="1"';
                } else {
                    $proj_condition = 'status=1 AND find_in_set(' . Yii::app()->user->id . ',assigned_to)';
                }
            }

            echo $form->dropDownList(
                $model,
                'project_id',
                CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition, 'order' => 'name ASC ')), 'pid', 'name'),
                array(
                    'empty' => 'Choose a project',
                    'disabled' => $model->isNewRecord ? false : true,
                    'class' => 'form-control'
                )
            );
            ?>
           <?php echo $form->error($model, 'project_id'); ?>
       </div>
       <div class="col-md-2">
           <?php echo $form->labelEx($model, 'title'); ?>
           <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
           <?php echo $form->error($model, 'title'); ?>
       </div>
       <div class="col-md-2">
           <?php
            if (!empty($model->project_id)) {
                $milestone_condition = "status = 1 AND project_id = " . $model->project_id;
            } else {
                $milestone_condition = "status = 1 ";
            }
            ?>
           <?php echo $form->labelEx($model, 'milestone_id'); ?>
           <?php echo $form->dropDownList($model, 'milestone_id', CHtml::listData(Milestone::model()->findAll(array(
                'select' => array('id,milestone_title'),
                'condition' => $milestone_condition,
                'order' => 'milestone_title ASC',
                'distinct' => true
            )), 'id', 'milestone_title'), array('class' => 'form-control', 'empty' => 'Choose a Milestone'));

            ?>
           <?php echo $form->error($model, 'milestone_id'); ?>
       </div>
       <div class="col-md-2">
           <?php echo $form->labelEx($model, 'start_date'); ?>
           <?php echo $form->textField($model, 'start_date', array('class' => 'form-control')); ?>
           <?php echo $form->error($model, 'start_date'); ?>
       </div>
       <div class="col-md-2">
           <?php echo $form->labelEx($model, 'due_date'); ?>
           <?php echo $form->textField($model, 'due_date', array('class' => 'form-control')); ?>
           <?php echo $form->error($model, 'due_date'); ?>
       </div>
       <div class="col-md-2">
           <?php echo $form->labelEx($model, 'ranking'); ?>
           <?php echo $form->textField($model, 'ranking', array('class' => 'form-control')); ?>
           <?php echo $form->error($model, 'ranking'); ?>
       </div>

   </div>
   <input type="hidden" name="Tasks[tskid]" id="Tasks_tskid">
   <input type="hidden" name="submit_type" id="submit_type_task">
   <div class="row text-right">
       <div class="col-sm-12">
        <div class="d-flex justify-content-end min-height-40">
            <div>
           <?php echo CHtml::SubmitButton($model->isNewRecord ? 'Save & Add Main Task' : 'Update', array('class' => 'btn blue save_btn','id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
        </div>
        <div>
           <?php echo CHtml::SubmitButton('Save & Proceed', array('class' => 'btn blue save_btn margin-left-5 save-and-proceed-button','id' => 'save_cnt', 'name' => 'cont_btn')); ?>
        </div>
        </div>
       </div>
   </div>
   <?php $this->endWidget(); ?>
   <div class="row">
       <div class="col-md-12">
           <?php
            // if (!$model->isNewRecord) {
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'tasks-grid',
                'dataProvider' => $model->mainTaskSearch(),
                // 'filter' =>$task,
                'itemsCssClass' => 'table table-bordered',
                'template' => '<div class="table-responsive">{items}</div>',
                'pager' => array(
                    'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                    'nextPageLabel' => 'Next '
                ),
                'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                'columns' => array(
                    array('class' => 'IndexColumn', 'header' => 'S.No.'),
                    array(
                        'class' => 'CButtonColumn',
                        // 'template' => isset(Yii::app()->user->role) && (in_array('/tasks/addmaintask', Yii::app()->session['menuauthlist'])) ? '{update}' : '',
                        'template'=>'{update}{delete}',
                        'buttons' => array(
                            'update' => array(
                                'label' => '',
                                'imageUrl' => false,
                               
                                'click' => 'function(e){e.preventDefault();edittask($(this).attr("href"));}',

                                'visible' => "isset(Yii::app()->user->role) && (in_array('/tasks/addmaintask', Yii::app()->session['menuauthlist']))",
                                
                                'options' => array('class' => 'update_task actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                            ),



                            'delete' => array(
                                'label' => '',
                                'imageUrl' => false,
                               
                                'visible' => "isset(Yii::app()->user->role) && (in_array('/tasks/delete', Yii::app()->session['menuauthlist']))",
                                
                                'click' => 'function(e){e.preventDefault();deletemaintask($(this).attr("href"));}',
                                'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                            ),

                            
                        ),

                        
                    ),                    array(
                        'name' => 'title',
                        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->tskid')
                    ),
                    array(
                        'name' => 'milestone_id', 'value' => '($data->milestone_id===null?"": $data->milestone->milestone_title)',
                        'type' => 'raw',
                    ),
                    array(
                        'name' => 'start_date',
                        'value' => 'date("d-M-y",strtotime($data->start_date))',
                        'type' => 'html',
                        'htmlOptions' => array('class' => 'date_start')
                    ),
                    array(
                        'name' => 'due_date',
                        'value' => 'date("d-M-y",strtotime($data->due_date))',
                        'type' => 'html',
                        'htmlOptions' => array('class' => 'date_end'),

                    ),
                    'ranking',

                    
                ),
            ));
            // }
            ?>
       </div>
   </div>
   <script>
       $("#Tasks_milestone_id").change(function() {
           var id = this.value;
           if (id != '') {
               $.ajax({
                   "type": 'post',
                   "url": "<?php echo Yii::app()->createUrl('masters/milestone/getmilestonedates'); ?>",
                   "data": {
                       id: id
                   },
                   "dataType": "json",
                   success: function(result) {
                       console.log(result);
                       if (result.status == 1) {
                           $('#Tasks_start_date').val(result.start);
                           $('#Tasks_due_date').val(result.end);
                       }
                   }
               });
           }

       });

       function taskFormSubmit(form, data, hasError) {
           if (!hasError) {
               $('.loaderdiv').show();
               var formData = new FormData($("#tasks-form")[0]);
               $.ajax({
                   "type": "POST",
                   "url": "<?php echo Yii::app()->createUrl('tasks/addMainTask'); ?>",
                   "dataType": "json",
                   "data": formData,
                   "cache": false,
                   "contentType": false,
                   "processData": false,
                   "success": function(data) {
                       $('.loaderdiv').hide();
                       if (data.status == 1) {
                           $("#success_message").fadeIn().delay(1000).fadeOut();
                           $('#Tasks_tskid').val('');
                           $("#tasks-grid").load(location.href + " #tasks-grid");
                       } else if (data.status == 2) {
                           $("#error_message").fadeIn().delay(1000).fadeOut();
                       }
                       if (data.next_level == 0) {
                           $("#tasks-form")[0].reset();
                       } else if (data.next_level == 1) {
                           $('#main_task_details').removeClass('active');
                           $('#main_task_details').addClass('progress_tab');
                           $('#Area_project_id').html(data.html);
                           $('#main_task_info').removeClass('active in');
                           $('#area_details').addClass('active');
                           $('#area_info').addClass('active in');
                       }
                   }
               });
           } else {
               $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
               setTimeout('$("#successMessage").fadeOut("slow")', 2000);
           }
       }

       function edittask(href) {
           var id = getURLParameter(href, 'id');
           $.ajax({
               "type": 'post',
               "url": "<?php echo Yii::app()->createUrl('tasks/getmodel'); ?>",
               "data": {
                   id: id
               },
               "dataType": "json",
               success: function(result) {
                   if (result.stat == 1) {
                       $('#Tasks_title').val(result.title);
                       $('#Tasks_milestone_id').val(result.milestone_id);
                       $('#Tasks_tskid').val(result.id);
                       $('#Tasks_start_date').val(result.start_date);
                       $('#Tasks_due_date').val(result.end_date);
                       $('#Tasks_ranking').val(result.ranking);
                   }
               }
           });
       }

       $(document).ready(function () {

        
$('#main_task_details').click(function () {
   
$("#tasks-grid").load(location.href + " #tasks-grid");
});
});

function deletemaintask(href)
{
    var id = getURLParameter(href, 'id');
    var answer = confirm("Are you sure you want to delete?");
    var url = "<?php echo $this->createUrl('tasks/delete&id=') ?>" + id;
    if(answer)
    {
        $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                success: function(response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                    } else if (response.response == "warning") {
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                    }
                    $("#tasks-grid").load(location.href + " #tasks-grid");

                }
            });
    }
}
   </script>