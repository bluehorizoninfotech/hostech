<?php

$contractor_list = Contractors::model()->findAll(array('condition' => 'status = 1'));
?>

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'site-meetings-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'beforeValidate' => new CJavaScriptExpression('function(form) {
                for(var instanceName in CKEDITOR.instances) { 
                    CKEDITOR.instances[instanceName].updateElement();
                }
                return true;
            }'),
            'afterValidate' => 'js:function(form, data, hasError) {
                if(hasError) {                                          
                }
                else
                {
                // return true;  it will submit default way
                       meetingform(form, data, hasError); //and it will submit by ajax function
                   }
               }',
        ),
    )
); ?>
<div class="row">
    <?php
    if (!$model->isNewRecord) {
        ?>
        <input type="hidden" name="SiteMeetings[id]" value="<?= $model->id ?>" id="model_id">
        <?php
    }
    ?>
    <div class="col-md-4">


        <?php echo $form->labelEx($model, 'project_id'); ?>
        <?php echo $form->dropDownList(
            $model,
            'project_id',
            $projects,
            array(
                'empty' => 'Choose a project',
                'class' => 'form-control project change_project',
                'ajax' => array(
                    'type' => 'POST',
                    'url' => (Yii::app()->user->role == 10) ? CController::createUrl('Tasks/getclientlocationbyid') : CController::createUrl('Tasks/getclientlocation'), //or $this->createUrl('loadcities') if '$this' extends CController
                    'update' => '#SiteMeetings_site_id', //or 'success' => 'function(data){...handle the data in the way you want...}',
                    'data' => array('pid' => 'js:this.value'),
                )
            )
        ); ?>
        <?php echo $form->error($model, 'project_id'); ?>
    </div>

    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'site_id', array('label' => 'Location')); ?>
        <?php
        $location = Clientsite::model()->findAll();
        if (empty($model->site_id)) {
            if (Yii::app()->user->project_id != "") {
                $location = Clientsite::model()->findAll(array("condition" => "pid = " . Yii::app()->user->project_id));
            }
        }
        echo $form->dropDownList(
            $model,
            'site_id',
            CHtml::listData($location, 'id', 'site_name'),
            array(
                'empty' => 'Choose a client site',
                'class' => 'form-control',
            )
        ); ?>
        <?php echo $form->error($model, 'site_id'); ?>
    </div>
    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'meeting_ref'); ?>
        <?php echo $form->textField($model, 'meeting_ref', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'meeting_ref'); ?>
    </div>
</div>

<div class="row">

    <div class="col-md-4">
        <label>Project Code </label>
        <input type="text" value="" id="project_no" class="form-control" readonly>

    </div>
    <div class="col-md-4">
        <?php
        if (!$model->isNewRecord) {
            if ($model->date != "") {
                $model->date = date('d-M-y', strtotime($model->date));
            }
        }
        ?>

        <?php echo $form->labelEx($model, 'date'); ?>
        <?php echo $form->textField($model, 'date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        <?php echo $form->error($model, 'date'); ?>
    </div>
    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'meeting_time'); ?>
        <input type="time" class="form-control" value="<?php echo !$model->isNewRecord ? $model->meeting_time : '' ?>"
            name="SiteMeetings[meeting_time]" id="SiteMeetings_meeting_time">
        <?php echo $form->error($model, 'meeting_time'); ?>
        <span id="time_format"></span>
    </div>
</div>
<div class="row ">
    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'venue'); ?>
        <?php echo $form->textField($model, 'venue', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'venue'); ?>
    </div>
    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'meeting_number'); ?>
        <?php echo $form->textField($model, 'meeting_number', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'meeting_number'); ?>
    </div>

    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'meeting_title'); ?>
        <?php echo $form->textField($model, 'meeting_title', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'meeting_title'); ?>
    </div>
</div>
<div class="row ">
    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'objective', array('label' => 'Purpose')); ?>
        <?php echo $form->textArea($model, 'objective', array('class' => 'form-control', 'rows' => 3)); ?>
        <?php echo $form->error($model, 'objective'); ?>
    </div>
</div>
<div class="row">


    <div class="col-md-6">
        <label>Participants</label>
        <a href="#" type="button" data-toggle="modal" data-target="#addParticipant">Add New Participant</a>
        <!-- The modal -->
        <div class="modal fade add-task-modal" id="addParticipant" tabindex="-1" role="dialog"
            aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="modalLabel">Add New participant</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="Col-sm-6">
                                <div class="form-group">
                                    <label for="Projects_name" class="required">Name<span
                                            class="required">*</span></label>
                                    <input class="form-control" name="participant_name" id="participant_name"
                                        type="text" maxlength="100">
                                    <div class="errorMessage" id="participant_name_em_" style="display:none;">
                                        Name cannot be blank.
                                    </div>
                                </div>
                            </div>
                            <div class="Col-sm-6">
                                <div class="form-group">
                                    <label for="Projects_name" class="required">Initial </label>
                                    <input class="form-control" name="participant_initial" id="participant_initial"
                                        type="text" maxlength="100">
                                    <div class="errorMessage" id="participant_initial_em_" style="display:none;"></div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="Col-sm-4">
                                <div class="form-group">
                                    <label for="Projects_name" class="required">Designation <span
                                            class="required">*</span></label>
                                    <input class="form-control" name="participant_designation"
                                        id="participant_designation" type="text" maxlength="100">
                                    <div class="errorMessage" id="participant_designation_em_" style="display:none;">
                                        Designation cannot be blank.
                                    </div>
                                </div>
                            </div>
                            <div class="Col-sm-4">
                                <div class="form-group">
                                    <label for="Projects_name" class="required">Company</label>
                                    <input class="form-control" name="participant_company" id="participant_company"
                                        type="text" maxlength="100">
                                    <div class="errorMessage" id="participant_company_em_" style="display:none;"></div>
                                </div>
                            </div>
                            <div class="Col-sm-4">
                                <div class="form-group">
                                    <label for="Projects_name" class="required">Company Initial </label>
                                    <input class="form-control" name="participant_company_initial"
                                        id="participant_company_initial" type="text" maxlength="100">
                                    <div class="errorMessage" id="participant_company_initial_em" style="display:none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary participant_save">Create</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        // Assuming $data is an array of options
        
        echo CHtml::listBox(
            'records[]', // Dropdown name
            '', // Selected value (empty in this case)
            array(), // Array of options
            array(
                'prompt' => 'Select', // Optional prompt text
                'class' => 'form-control js-example-basic-multiple participant', // Optional CSS class
                'id' => 'meeting_participants',
            )
        );
        ?>


        <!-- end  -->
    </div>




    <div class="col-md-6">
        <?php echo $form->labelEx($model, 'approved_by'); ?>
        <select name="Meeting[approved_by][]" id="Meeting_users"
            class="form-control js-example-basic-multiple approved_by_val">
            <?php
            $condition = array('select' => array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0', 'order' => 'first_name ASC');
            $users = Users::model()->findAll($condition);
            foreach ($users as $key => $value) {
                ?>
                <option value="<?php echo $value['userid']; ?>">
                    <?php echo $value['full_name'] . ' (' . $value->userType->role . ')'; ?>
                </option>
                <?php
            }
            ?>
        </select>
        <?php echo $form->error($model, 'approved_by'); ?>
    </div>
</div>
<div class="row ">
    <div class="col-md-6">
        <?php echo $form->labelEx($model, 'distribution'); ?>
        <?php echo $form->textArea($model, 'distribution', array('class' => 'form-control', 'rows' => 6, 'maxlength' => 400)); ?>
        <?php echo $form->error($model, 'distribution'); ?>
    </div>
</div>
<div class="row text-right">
    <div class="col-sm-12">
        <?php echo CHtml::SubmitButton('Next', array('class' => 'btn blue save_btn margin-left-30', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(".datepicker").attr("autocomplete", "off");
    $("#SiteMeetings_date").datepicker({
        dateFormat: 'dd-M-yy',
        // maxDate: new Date(maximumDate)
    });
    CKEDITOR.replace('SiteMeetings_objective', {
        height: '100px' // Adjust the height as needed
    });


    $(document).ready(function () {

        $('#SiteMeetings_meeting_time').blur(function () {
            t = document.getElementById('SiteMeetings_meeting_time').value
            var [h, m] = t.split(":");
            var hour_val = (h % 12 + 12 * (h % 12 == 0));
            if (hour_val.toString().length == 1) {
                hour_val = "0" + hour_val;
            }
            var time_val = (hour_val + ":" + m);
            var time_regex = '';
            var time_regex = ((h % 12 + 12 * (h % 12 == 0)) + ":" + m,
                h >= 12 ? 'PM' : 'AM');
            // $('#SiteMeetings_meeting_time').val(time_val);
            // $('<span>' + time_regex + '</span>').insertAfter('#SiteMeetings_meeting_time');
            $("#time_format").text(time_regex);
        });


        var project_id = $('#SiteMeetings_project_id').val();
        if (project_id) {
            getProjectParticipants(project_id);
        }
        $('.change_project').on('change', function (e) {
            project_id = $("option:selected", this).val();
            getProjectParticipants(project_id);
        });
        $(".participant_save").click(function () {
            var name = $("#participant_name").val();
            var initial = $("#participant_initial").val();
            var designation = $("#participant_designation").val();
            var company = $("#participant_company").val();
            var company_name = $("#participant_company_initial").val();
            if (name != '' && designation != '') {
                $("#participant_name_em_").hide();
                $("#participant_designation_em_").hide();
                $.ajax({
                    'url': '<?php echo Yii::app()->createAbsoluteUrl('projects/addParticipant'); ?>',
                    data: {
                        name: name,
                        initial: initial,
                        designation: designation,
                        company: company,
                        company_name: company_name,
                        project_id: project_id
                    },
                    dataType: 'Json',
                    type: 'POST',
                    success: function (result) {
                        if (result.status == 1) {
                            $('#addParticipant').hide();
                            var newOption = $("<option></option>").attr("value", result.id ).text(result.name );
                            // // Append the new option to the select dropdown
                            $("#meeting_participants").append(newOption);
                            // var select = document.getElementById("SiteMeetings_project_id");
                            // var option = document.createElement("option");
                            // option.text = "New Option";
                            // option.value = "new_option_value";
                            // select.add(option);
                            // var selectedValues = $("#SiteMeetings_project_id").val();
                            // getProjectParticipants(project_id);
                            // $("#SiteMeetings_project_id").val(selectedValues);

                        } else {
                            if (result.error['ranking'] !== '') {
                                $("#Milestone_ranking_em_").show();
                                $("#Milestone_ranking_em_").html(result.error['ranking']);
                            }
                        }
                    }
                })

            } else {
                if (name == '') {
                    $("#participant_name_em_").show();
                }
                if (designation == '') {
                    $("#participant_designation_em_").show();
                }
            }
        })

        function getProjectParticipants(project_id) {
            var meeting_id;
            if ($('#model_id').length) {
                meeting_id = $("#model_id").val();
            }
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('projects/getProjectParticipants'); ?>',
                data: { project_id: project_id, meeting_id: meeting_id },
                method: "GET",
                dataType: "json",
                success: function (result) {
                    $('#meeting_participants').html(result.option);
                    $('#project_no').val(result.project_no);
                }
            });
        }




    })
</script>