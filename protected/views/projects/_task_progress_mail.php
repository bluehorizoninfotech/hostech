<html><head><title>Task Notification</title></head>
			<body><p><h2>Behind Task Notification</h2></p>
			<table border='1' class='border-collapse width-100-percentage'><tr>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Sl no</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Task Id</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Task Name</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Project Name</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Current Percentage</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Required Percentage</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Assignee</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Start Date</th>
                        <th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>End Date</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Required Workers</th>
						<th class='black-color padding-top-6 padding-bottom-6 padding-right-8 padding-left-8'>Remaining Quantity</th>
					   </tr>
					   <?php 
					   $k=1;
					   
					 foreach($data as $task)  {
                         if(!empty($task)){							
						 ?>
						<tr>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $k?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['id']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['title']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['project_title']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['current_percentage'].'%'?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= round($task['required_percentage']).'%'?></td>
                            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $this->getAssigneedeatils($task['assignee'])?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['start_date']))?></td>
                            <td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= Yii::app()->dateFormatter->format("d MMM y",strtotime($task['due_date']))?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['workers_required']?></td>
							<td class='padding-top-5 padding-bottom-5 padding-left-8 padding-right-8'><?= $task['remaining_qty']?></td>
						</tr>
						 <?php
						 $k++;
                     }
                    }
					   ?>
				
						
					
		</table></body></html>