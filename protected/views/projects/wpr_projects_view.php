<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	$model->pid,
);

$this->menu=array(
	array('label'=>'List Projects', 'url'=>array('addAssigneetoProjects')),
	array('label'=>'Create Projects', 'url'=>array('assigneecreate')),
	array('label'=>'Update Projects', 'url'=>array('assigneeUpdate', 'id'=>$model->pid)),
	array('label'=>'Delete Projects', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Projects', 'url'=>array('admin')),
);
?>

<h1>View Projects #<?php echo $model->pid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'pid',
		'name',
		'start_date',
		'end_date',
		'assigned_to',
	),
)); ?>
