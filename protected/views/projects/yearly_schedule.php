<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js" type="text/javascript"></script>
<br clear="all">
<div class="yearly-schedule-sec">
<div class="table_hold">

    <form name="emp_att_grid" id="emp_att_grid">
        <div id="parent">
            <table class="table attnd_table table-bordered" id="fixTable">
                <?php

                $week_array = array();
                $selected_date_ = strtotime($selected_date);
                $selected_year = date("Y", $selected_date_);
                array_push($week_array, $selected_year);
                $date1 = new DateTime($start_date);
                $date2 = new DateTime($end_date);
                $diff = $date2->diff($date1)->format("%a");
                $totdays = $diff + 1;
                for ($k = 1; $k < 4; $k++) {
                    $next_year = $selected_year + $k;
                    array_push($week_array, $next_year);
                }
                // echo '<pre>';print_r($week_array);exit;
                ?>
                <thead>
                    <tr>
                        <th rowspan="1" colspan="4" class="fixed_th">
                        </th>
                        <?php
                        /****************************************** */
                        foreach ($week_array as $week_datas) {
                            $monthdayarr[$week_datas] = 1;
                        }
                        ?>
                        <?php

                        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');

                        foreach ($monthdayarr as $month => $diffdays) {

                            if (count($monthdayarr) == 1) {
                                $diffdays = 4;
                            }
                            if ($diffdays == 1) {
                                $diffdays = 1;
                            }

                            echo "<th colspan='" . $diffdays . "'rowspan='2' class='month-period'>" . $month . "</th>";
                        }
                        ?>
                    </tr>
                    <tr>
                        <th class="fixed_th hdrborder width-50-percentage" colspan="4">
                            Task Name
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $count = 0;

                    foreach ($tasks as $key => $milestone_data) {
                        $task_datas = $milestone_data;
                        $milestone_model = Milestone::model()->findByPk($key);
                    ?>
                        <tr>
                            <td colspan="4" class="bg_grey bg-cell"><?= $milestone_model->milestone_title ?></td>
                            <?php
                            for ($p = 0; $p < count($week_array); $p++) { ?>
                                <td></td>
                            <?php }
                            ?>
                        </tr>
                        <?php
                        foreach ($task_datas as $task) {
                            $time_entry_array = array();
                            if (isset($task[0]))
                                $time_entry_array = $task[0];
                            $task_status = 'grey';
                            $count++;
                        ?>
                            <tr id="user ?>">
                                <td colspan="4" align="left">
                                    <div class="pull-left">
                                        <?= $task['title'] . '#' ?>
                                    </div>
                                </td>
                                <?php
                                $task_start_year = date('Y', strtotime($task['start_date']));
                                $task_end_year = date('Y', strtotime($task['due_date']));
                                $task_percen = '';
                                $cent_percent_status = 0;

                                for ($p = 0; $p < count($week_array); $p++) {



                                    if ($cent_percent_status == 1) {
                                        $task_percen = 'green_border';
                                    }
                                    if ($task_start_year != $task_end_year) {
                                        if ($task_start_year == $week_array) {
                                            $task_status = 'yellow';
                                        } elseif ($week_array == $task_end_year) {
                                            $task_status = 'solid_red';
                                        } else {
                                            $task_status = 'green';
                                        }
                                    } else {
                                        $task_status = 'solid_red';
                                    }

                                ?>

                                    <td width="20" data-toggle="modal" id="" data-backdrop="static" data-target="#entry" class="coldate <?= $task_start_year == $week_array[$p] || $task_end_year == $week_array[$p] ? $task_status : '' ?>" data-value="" data-coldate="">

                                    </td>
                                <?php } ?>
                            </tr>
                            <?php
                            if (!empty($task['children'])) {
                                // echo '<pre>';print_r($task['children']);exit;
                                $child = 0;
                                $this->reportchildarraylistyear($task['children'], $totdays, $week_array, $child);
                            ?>
                            <?php } ?>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </form>
</div>
                </div>




<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
        $j("#fixTable").tableHeadFixer({
            'head': true
        });
    });
</script>
