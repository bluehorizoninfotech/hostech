<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<?php
/* @var $this ProjectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);

if(yii::app()->user->role<=2){
	$this->menu = array(
//	    array('label' => 'Create Projects', 'url' => array('create')),
//	    array('label' => 'Manage Projects', 'url' => array('admin')),
	);
}
?>

<div class="clearfix">
    <div class="add link pull-right">
    </div>
    <h1>Projects Log</h1>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'projects-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate'=>false,
    //'template' => '<div class="table-responsive">{items}</div>',
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
   'nextPageLabel'=>'Next ' ),
     
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
    'columns' => array(
        array(
            'header' => 'S.No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
        array(
            'name' => 'project_no',
            'value' => function($model){
                if($model->project_no != $model->project_no_old){
                    return $model->project_no.'<span style="color:red"> ('.$model->project_no_old.')</span>';
                }else{
                    return $model->project_no;
                }
            },
            'type' => 'raw',
        ),       
      array(
        'name'  => 'name',
        'value' => function($model){
            if($model->name != $model->name_old){
                return CHtml::link($model->name, Yii::app()->createUrl("projects/view",array("id"=>$model->pid))).'<span style="color:red"> ('.$model->name_old.')</span>';
            }else{
                return CHtml::link($model->name, Yii::app()->createUrl("projects/view",array("id"=>$model->pid)));
            }
        },

        'type'  => 'raw',
         'filter'=> $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'Projects[name]',
                'source'=>$this->createUrl('Projects/autocomplete'),
                'value' => isset($model->name) ? $model->name: "",
                'options'=>array(
                    'focus'=>'js:function(event, ui) { 
                       $("#Projects_name").val(ui.item.value);
                    }',
                    'minLength'=>'1',
                    'showAnim'=>'fold',
                    'select'=>'js:function(event, ui) {  $("#Projects_name").val(ui.item.value); }'
               
                   ),
                 ),true),
    ),
       array(
            'name' => 'client_id',
            'value' => function($model){
                if($model->client_id != $model->client_id_old){                  
                    return isset($model->client_id) ? $model->client->name.'<span style="color:red"> ('.$model->clientOld->name.')</span>' :  "N/A";                                      
                }else{
                    return isset($model->client_id) ? $model->client->name :  "N/A";
                }
            },           
            'type' => 'raw',
            'filter' => CHtml::listData(Clients::model()->findAll(
                            array(
                                'select' => array('cid,name'),
                                'order' => 'name',
                                'distinct' => true
                    )), "cid", "name"),
            'type'  => 'raw',
            'visible'=>(yii::app()->user->role==1),
        ),
          
        array(
            'name' => 'billable',
            // 'value' => '$data->billable0->caption',
            'value' => function($model){
                if($model->billable != $model->billable_old){                  
                    return $model->billable0->caption.'<span style="color:red"> ('.$model->billable0Old->caption.')</span>';                                      
                }else{
                    return $model->billable0->caption;
                }
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="yesno"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        array(
            'header' => 'Analyzer',
            'type'=>'raw',
            'value' => 'CHtml::link("View ($data->pid)",Yii::app()->createUrl("projects/analyzerlink",array("id"=>$data->pid)),array("target"=>"_blank"))',                               
        ),
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'value' => function($model){
                if($model->status != $model->status_old){                  
                    return $model->status0->caption.'<span style="color:red"> ('.$model->status0Old->caption.')</span>';                                      
                }else{
                    return $model->status0->caption;
                }
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        array(
            'name' => 'budget',
            'value' => function($model){
                if($model->budget != $model->budget_old){
                    return $model->budget.'<span style="color:red"> ( '.$model->budget_old.' )</span>';
                }else{
                    return $model->budget;
                }
            },
            'type' => 'html',
            'htmlOptions' => array('width' =>100),
            //'htmlOptions' => array( 'style' => 'width:20px;border-right:1px solid #c2c2c2;'),
           
        ),      
        array(
            'name' => 'start_date',
            'value' => function($model){
                if(strtotime($model->start_date)!= strtotime($model->start_date_old)){
                    return date("d-M-y",strtotime($model->start_date)).'<span style="color:red"> ('.date("d-M-y",strtotime($model->start_date_old)).')</span>';
                }else{
                    return date("d-M-y",strtotime($model->start_date));
                }
            },
            'type' => 'html',
            //'htmlOptions' => array( 'style' => 'width:20px;border-right:1px solid #c2c2c2;'),
           
        ),   
        array(
            'name' => 'end_date',
            'value' => function($model){
                if(strtotime($model->end_date)!= strtotime($model->end_date_old)){
                    return date("d-M-y",strtotime($model->end_date)).'<span style="color:red"> ('.date("d-M-y",strtotime($model->end_date_old)).')</span>';
                }else{
                    return date("d-M-y",strtotime($model->end_date));
                }
            },              
            'type' => 'html',                  
        ),   
        'updated_date',
        array(
            'name' => 'updated_by',
            'value' => function($model){
                if($model->updated_by != $model->updated_by_old){
                    return $model->updatedBy->first_name." ".$model->updatedBy->last_name.'<span style="color:red"> ( '.$model->updatedByOld->first_name." ".$model->updatedByOld->last_name.' )</span>';
                }else{
                    return $model->updatedBy->first_name." ".$model->updatedBy->last_name;
                }
            },            
            'type' => 'raw',
        ),
        /*
          'description',
          'created_date',
          'created_by',
          'updated_date',
          'updated_by',
         */

    ),
));
?>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add project',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="450" frameborder="0" style="min-height:400px;"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>

<?php
    Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   
  } );

');


?>

<script>

$(function () {  
    $( "input[name='ProjectLog[start_date]']" ).datepicker({                 
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd'                     
    });
    
    $( "input[name='ProjectLog[end_date]']" ).datepicker({                 
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd'                     
    });   
});
    </script>