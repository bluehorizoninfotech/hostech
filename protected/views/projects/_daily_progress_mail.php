

 <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/pms-style.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
 <body><p><h2>Daily Progress Report <?php echo date('d-M-Y')?></h2></p>
<!-- <style>
table, th, td {
  border:1px solid black;
  border-collapse:collapse;
}
</style> -->
<table style="width:100%;border-collapse: collapse;" border="1">
  <tr>
    <th colspan="6">Daily Progress Report - <?php echo date('d-M-Y')?></th>
</tr>
<tr>
  <?php
  $project_id= $project->pid;
  
  
 $client_site = Clientsite::model()->findAll(array('condition' => 'pid = ' . $project_id));
  
  ?>
<td colspan="1">PROJECT</td>
                    <td colspan="2"><?= $project->name ?></td>
                    <td colspan="1">SITE ENGINEER</td>
                    <td colspan="2"><?= isset($project->client) ? $project->client->name :  "N/A" ?></td>
</tr>

<tr>
    <th colspan="6">WORK DONE</th>
</tr>
<tr>
    <th>TASKS </th>
    <th>QUANTITY</th>
    <th>UNIT</th>
    <th>UNIT RATE</th>
    <th>AMOUNT</th>
    <th>STATUS</th>

</tr>

<?php



foreach($data_array as  $projects)
{



if(isset($projects['tasks']))
{
  $total_amount=0;
  $items_total=0;
  $resources_total=0;
  $labour_total=0;
  foreach($projects['tasks'] as $tasks)
  {
    $status=$tasks['approve_status'];
    if($status != "")
    {
      if($status==1)
      {
        $status='Approved';
      }
      else if($status==2)
      {
        $status='Rejected';
      }
      else if($status==0)
      {
        $status='Pending';
      }
    }

    $total_amount += $tasks['quantity'] *  $tasks['task_rate'];
    
    ?>
    <tr>
      <td><?php echo $tasks['task_name']?></td>
      <td><?php echo $tasks['quantity']?></td>
      <td><?php echo $tasks['task_unit'] ?></td>
      <td><?php echo $tasks['task_rate']?></td>
      <td><?php echo $tasks['quantity'] *  $tasks['task_rate'] ?></td>
      <td><?php echo $status?></td>
  </tr>
  

    <?php
  }
} // foreach $projects['tasks']

?>
<tr>
  <td colspan="4"><b>TOTAL</b></td>
  <td colspan="2"><b><?php echo $total_amount?></b></td>
</tr>

<!-- labour report -->
<tr>
    <th colspan="6">LABOUR UTILIZED</th>
</tr>
<tr>
    <th > CATEGORY  </th>
    <th >QUANTITY</th>
    <th >WAGES</th>
    <th >RATE</th>
    <th colspan="2">TOTAL LABOUR</th>
   

</tr>

 <?php

if(isset($projects['labour'])){

  foreach($projects['labour'] as $labour)
  {
    $labour_total += $labour['total_amount'];
    ?>
    <tr>
      <td><?php echo $labour['labour_name'] ?></td>
      <td><?php echo $labour['labour_count'] ?></td>
      <td><?php echo $labour['labour_wage'] ?></td>
      <td><?php echo $labour['labour_amount'] ?></td>
      <td  colspan="2"><?php echo $labour['total_amount'] ?></td>
    </tr>
    <?php
  }
?>
  <tr>
  <td colspan="4"><b>TOTAL</b></td>
  <td colspan="2"><b><?php echo $labour_total?></b></td>
</tr>
<?php
}

 ?>

<!-- end labour report -->



<!-- items used -->


<tr>
    <th >MATERIAL USED </th>
    <th >RATE</th>
    <th >UNIT</th>
    <th >QUANTITY</th>
    <th colspan="2">AMOUNT</th>
   

</tr>
<?php

  if(isset($projects['items'])){

 foreach($projects['items'] as $items)
{
  $items_total += $items['item_quantity'] * $items['item_rate'];
   ?>
   <tr>
    <td><?php echo $items['item_name']?></td>
    <td><?php echo $items['item_rate']?></td>
    <td><?php echo $items['item_unit']?></td>
    <td><?php echo $items['item_quantity']?></td>
    <td colspan="2" ><?php echo $items['item_quantity'] * $items['item_rate'] ?></td>
   </tr>

   <?php
 } // foreach($projects['items']

 ?>

<tr>
  <td colspan="4"><b>TOTAL</b></td>
  <td colspan="2"><b><?php echo $items_total?></b></td>
</tr>

 <?php

 }  //isset($projects['items']

?>
<tr>
    <th >EQUIPMENT  USED </th>
    <th >RATE</th>
    <th >UNIT</th>
    <th >QUANTITY</th>
    <th colspan="2">AMOUNT</th>
   

</tr>




<!-- end items used -->

<!-- resource used -->

<?php
if(isset($projects['resources'])){

  foreach($projects['resources'] as $resources)
  {
    $resources_total += $resources['total_amount'];
    ?>
    <tr>
      <td> <?php echo $resources['resource_name']?></td>
      <td> <?php echo $resources['resource_rate']?></td>
      <td> <?php echo $resources['resource_unit']?></td>
      <td> <?php echo $resources['resource_quantity']?></td>
      <td colspan="2"> <?php echo $resources['total_amount']?></td>
    </tr>
    <?php
  } //foreach($projects['resources']
?>
  <tr>
  <td colspan="4"><b>TOTAL</b></td>
  <td colspan="2"><b><?php echo $resources_total?></b></td>
</tr>
<?php
}// if(isset($projects['resources']
?>

<!-- end resource used  -->

<tr>
  <td colspan="2"><b>TOTAL VALUE OF WORK DONE</b></td>
  <td colspan="2"><b>ACTUAL TOTAL COST</b></td>
  <td colspan="2"><b>INCOME STATUS</b></td>
</tr>

<tr>
  <td colspan="2"><b><?php echo  $total_amount; ?></b></td>
  <td colspan="2"><b><?php echo $labour_total +  $items_total +$resources_total ?></b></td>

  <?php
  $actual_cost=$labour_total +  $items_total +$resources_total;
  if($actual_cost==0 && $total_amount==0)
  {
    $income_status='NOT APPLICABLE'; 
  }
  else if( $actual_cost>$total_amount)
  {
    $income_status='LOSS';
  }
  else 
  {
    $income_status='PROFIT'; 
  }
  
  
  ?>
  <td colspan="2"><b><?php echo $income_status?></b></td>
</tr>

<?php 
  
} // foreach data_array
?>
  
</table>