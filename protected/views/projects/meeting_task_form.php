<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs = array(
    'Tasks' => array('index'),
    'Create',
);

$this->menu = array(
    //array('label' => 'List Tasks', 'url' => array('index')),
    //array('label' => 'Manage Tasks', 'url' => array('admin')),
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        //array('label' => 'Manage Tasks', 'url' => array('admin')),
    );
}
?>
<style type="text/css">
    .toggle-field {
        display: none;
    }
</style>
<!--<div id="wrapper">
    <div class="row">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header"></div> -->

<?php if (empty($_GET['asDialog'])) { ?>
    <!--                <div class="panel panel-default">
                                        <div class="panel-heading">Add Task</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6">-->


<?php } ?>
<?php
Yii::app()->clientScript->registerScript('Show all fields', "
$('.toggle-button').click(function(){
	$('.toggle-field').toggle();
	return false;
});
");
?>
<!--                <div style="margin-left:400px;"><?php //echo CHtml::link('Show all fields', '#', array('class' => 'toggle-button'));   
                                                    ?></div>-->

<?php echo $this->renderPartial('_task_form', array('model' => $model, 'location' => $location)); ?>
</div>