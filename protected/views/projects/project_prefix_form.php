<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

?>

<div class="form">

    


    <div class="row">
        <div class="col-md-12">
        <input type="text" class="form-control input-medium" value="<?php echo $data['prefix'] ?> " id="prefix">

        <input type="hidden" value="<?php echo $data['id'] ?>" id="prefix_id">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php echo CHtml::submitButton('Save', array('class' => 'btn blue save-button')); ?>
            
        </div>
    </div>

    

</div><!-- form -->

<script>
      $(".save-button").click(function() {
       
      var prefix=$('#prefix').val();
      var prefix_id=$('#prefix_id').val();
      
      

        $.ajax({
                    'url': '<?php echo Yii::app()->createAbsoluteUrl('projects/addProjectPrefix'); ?>',
                    data: {
                        prefix:prefix,
                        prefix_id:prefix_id


                    },
                    dataType: 'Json',
                    type: 'POST',
                    success: function(result) {
                        location.reload();
                    }
                })
        
    })
    </script>