<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    $model->name => array('view', 'id' => $model->pid),
    'Update',
);
?>

<h2>Update Project: <span><?php echo $model->name; ?></span></h2>
<?php

?>
<div class="container ">
    <div class="add link pull-right">
        <?php
        echo CHtml::link('Back', array('projects/index'), array('class' => 'btn blue'));
        ?>
    </div>
    <?php echo $this->renderPartial('_newprojectform', array('model' => $model, 'milestone_model' => $milestone_model, 'work_site_model' => $work_site_model, 'user_assign_model' => $user_assign_model, 'assigned_to' => $assigned_to, 'area_model' => $area_model, 'work_type_model' => $work_type_model, 'task' => $task,'template'=>$template,'budget_head_model'=>$budget_head_model,'project_no'=>$project_no,'body_array'=>$body_array)); ?>
</div>
<script>
    $(document).ready(function() {
        $.noConflict();
        var maximumDate = $("#Projects_end_date").val();
        $("#Projects_start_date").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: new Date(maximumDate)
        });
        $("#Projects_end_date").datepicker({
            dateFormat: 'dd-M-yy',
        });
        // $("#Projects_start_date").change(function() {
        //     $("#Projects_end_date").datepicker('option', 'minDate', $(this).val());
        // });
        // $("#Projects_end_date").change(function() {
        //     $("#Projects_start_date").datepicker('option', 'maxDate', $(this).val());
        // });

        $('#Projects_assigned_to').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Assigned To',

        });
        var assigned_data = '<?php echo $assigned_to; ?>';
        var array = assigned_data.split(',');
        $('.assigned_to_').val(array);
        $('.assigned_to_').trigger('change');
        $('.user_ids').val('');
        $('#user_assign_model_ids').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Site Engineers/Supervisors',

        });
        $('.js-example-basic-single').select2();
    });
   
</script>