<?php
$contractor_list = Contractors::model()->findAll(array('condition' => 'status = 1'));
?>
<?php
$task_model = new Tasks();
$location = [];
?>
<div class="meeting-step-two-project-sec">
<div class="row">
    <div class="subrow subrowlong">
        <div class="task_section display-none">
            <?php echo $this->renderPartial('_task_form', array(
                'model' => $task_model,
                'location' => $location,
                'meeting_project_id' => $meeting_model->project_id
            )); ?>


        </div>
    </div>
</div>




<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'meeting-minutes-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'beforeValidate' => new CJavaScriptExpression('function(form) {
                for(var instanceName in CKEDITOR.instances) { 
                    CKEDITOR.instances[instanceName].updateElement();
                }
                return true;
            }'),
        'afterValidate' => 'js:function(form, data, hasError) {
                if(hasError) { 
                    console.log(error);                                                           
                }
                else
                {
                // return true;  it will submit default way
                minutes_form(form, data, hasError); //and it will submit by ajax function
                   }
               }',
    ),
));

$condition = 'status = 1 ';
if (!empty($meeting_model->project_id)) {
    $condition .= 'AND project_id = ' . $meeting_model->project_id;
}

?>
<div class='minutes_data'>

    <div class="row">
        <div class="col-md-3">
            <?php
             if ($model->isNewRecord) {
                 if(isset(Yii::app()->user->site_meeting_id))
                 {
                     $model->meeting_id=Yii::app()->user->site_meeting_id;
                 }
             }
            ?>
            <?php echo $form->labelEx($model, 'milestone'); ?>
            <span class="required">*</span></label>
            <?php echo $form->dropDownList($model, 'milestone_id', CHtml::listData(Milestone::model()->findAll(
                array(
                    'condition' => $condition,
                    'order' => 'milestone_title ASC'
                )
            ), 'id', 'milestone_title'), array('empty' => 'Select Milestone', 'class' => 'form-control test')); ?>
            <?php echo $form->error($model, 'milestone_id'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->labelEx($model, 'start_date'); ?>
            <?php echo $form->textField($model, 'start_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'start_date'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->labelEx($model, 'end_date'); ?>
            <?php echo $form->textField($model, 'end_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
            <?php echo $form->error($model, 'end_date'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <input name="MeetingMinutes[meeting_id]" id="MeetingMinutes_meeting_id" type="hidden" value="<?php echo $meeting_model->id ?>">
            <input name="MeetingMinutes[meeting_minutes_id]" id="MeetingMinutes_meeting_minutes_id" type="hidden" value="<?php echo $model->id ?>">
            <input name="MeetingMinutes[approve_status]" id="MeetingMinutes_approve_status" type="hidden" value="<?php echo $meeting_model->approved_status ?>">
            <input name="MeetingMinutes[project_id]" id="MeetingMinutes_meeting_project_id" type="hidden" value="<?php echo $meeting_model->project_id ?>" <?php echo $form->labelEx($model, 'points_discussed'); ?> <?php echo $form->textArea($model, 'points_discussed', array('class' => 'form-control')); ?> <?php echo $form->error($model, 'points_discussed'); ?> </div>

        </div>
        <div class="row ">
            <div class="col-md-4">
                <?php echo $form->labelEx($model, 'users'); ?>
                <select name="MeetingMinutes[users][]" id="MeetingMinutes_users" class="form-control js-example-basic-multiple action_by_val">
                    <?php
                    $condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0', 'order' => 'first_name ASC');
                    $users = Users::model()->findAll($condition);
                    foreach ($users as $key => $value) {
                    ?>
                        <option value="<?php echo $value['userid']; ?>"><?php echo $value['full_name'] . ' (' . $value->userType->role . ')'; ?></option>
                    <?php
                    }
                    ?>
                </select>
                <?php echo $form->error($model, 'users'); ?>
            </div>
            <br><br><br>
            <div class="col-md-2 display-none" id="task_div">
                <a href="#" class="new_task">Add Task</a>
            </div>
            <input type="hidden" name="submit_type" id="submit_type">
        </div>
    </div>
    <div class="row text-right">
        <div class="col-sm-12">
            <?php echo CHtml::SubmitButton('Next', array('class' => 'btn blue save_btn margin-left-30','id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
            <?php echo CHtml::SubmitButton('Add New', array('class' => 'btn blue save_btn margin-left-30','id' => 'add_new', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <div class="row">
        <div class="col-md-12">
            <?php
           

            // if (!$model->isNewRecord) {
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'meeting-minutes-grid',
                'dataProvider' => $model->search(),
                // 'filter' =>$task,
                'itemsCssClass' => 'table table-bordered',
                'template' => '<div class="table-responsive">{items}</div>',
                'pager' => array(
                    'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                    'nextPageLabel' => 'Next '
                ),
                'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                'columns' => array(
                    array('class' => 'IndexColumn', 'header' => 'S.No.'),
                    // 'tskid',
                    array(
                        'name' => 'title',
                        'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->tskid')
                    ),
                    array(
                        'name' => 'milestone_id', 'value' => '($data->milestone_id===null?"": $data->milestone->milestone_title)',
                        'type' => 'raw',
                    ),
                    array(
                        'name' => 'start_date',
                        'value' => 'date("d-M-y",strtotime($data->start_date))',
                        'type' => 'html',
                        'htmlOptions' => array('class' => 'date_start')
                    ),
                    array(
                        'name' => 'due_date',
                        'value' => 'date("d-M-y",strtotime($data->end_date))',
                        'type' => 'html',
                        'htmlOptions' => array('class' => 'date_end'),

                    ),
                    array(
                        'class' => 'CButtonColumn',
                        'template' => isset(Yii::app()->user->role) ? '{update}' : '',
                        'buttons' => array(
                            'update' => array(
                                'label' => '',
                                'imageUrl' => false,
                                'click' => 'function(e){e.preventDefault();edittask($(this).attr("href"));}',
                                'options' => array('class' => 'update_task actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                            ),
                        ),
                    ),
                ),
            ));
            // }
            ?>
        </div>
    </div>
    <!-- <input type="button" id="add_minute" value="Add" onClick="addminutes();"> -->

        </div>





    <script type="text/javascript">
        $('.save_btn').click(function() {
            var buttonClickedID = this.id;
            $('#submit_type').val(buttonClickedID);
        });
        CKEDITOR.replace('MeetingMinutes_points_discussed');
        $("#MeetingMinutes_submission_date").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#MeetingMinutes_start_date").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#MeetingMinutes_end_date").datepicker({
            dateFormat: 'dd-M-yy',
        });

        // function addminutes(){
        //         var data = $('.minutes_data').html();            
        //         var newdiv = document.createElement('div');
        //         newdiv.className = 'minutes';
        //         newdiv.innerHTML = data;
        //         $(newdiv).insertBefore( "#add_minute" );                                   
        // }
        $('#MeetingMinutes_milestone_id').change(function() {
            var milestone_id = this.value;
            if (milestone_id !== '') {
                $.ajax({
                    "type": 'post',
                    "url": "<?php echo Yii::app()->createUrl('masters/milestone/getmilestonedates'); ?>",
                    "data": {
                        id: milestone_id
                    },
                    "dataType": "json",
                    success: function(result) {
                        console.log(result);
                        if (result.status == 1) {
                            $('#MeetingMinutes_start_date').val(result.start);
                            $('#MeetingMinutes_end_date').val(result.end);
                        }
                    }
                });
            }
        })

        function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
        }

        function edittask(href) {
            var id = getURLParameter(href, 'id');
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('meetingMinutes/getmodel'); ?>",
                "data": {
                    id: id
                },
                "dataType": "json",
                success: function(result) {
                    if (result.stat == 1) {
                        $('#MeetingMinutes_meeting_minutes_id').val(result.data.id)
                        $('#MeetingMinutes_milestone_id').val(result.data.milestone_id);
                        $('#MeetingMinutes_meeting_id').val(result.data.meeting_id);
                        $('#MeetingMinutes_title').val(result.data.title);
                        $('#MeetingMinutes_start_date').val(result.data.start_date);
                        $('#MeetingMinutes_end_date').val(result.data.end_date);
                        CKEDITOR.instances['MeetingMinutes_points_discussed'].setData(result.data.points_discussed);
                        var userss = result.users.split(',');
                        console.log(userss);
                        $('.action_by_val').val(userss);
                        $('.action_by_val').trigger('change');
                        // $('#Tasks_tskid').val(result.id);
                        // $('#Tasks_start_date').val(result.start_date);
                        // $('#Tasks_due_date').val(result.end_date);
                        // $('#Tasks_ranking').val(result.ranking);
                    }
                }
            });
        }


        $(document).ready(function(e) {
            var meeting_status = $('#MeetingMinutes_approve_status').val();
            if (meeting_status == 1) {
                $('#task_div').show();
            }

            $(".new_task").click(function() {
                $('.task_section').show();

                $(".close-panel").click(function() {
                    $(".task_section").hide();
                })

            });
            var project_id = $('#MeetingMinutes_meeting_project_id').val();
            $("#Tasks_project_id").val(project_id);
            $('#closeButton').on('click', function(e) {
                $(".task_section").hide();
            });

            $('#MeetingMinutes_title').on('change.bootstrapSwitch', function(e) {
                //    var meeting_id=$('#MeetingMinutes_meeting_id').val();
                var meeting_id = 545;

                if (meeting_id != '') {
                    $.fn.yiiGridView.update('meeting-minutes-grid', {
                        data: {
                            site_meeting_id: meeting_id
                        },
                    });


                } else {
                    $.fn.yiiGridView.update('meeting-minutes-grid', {
                        data: {
                            site_meeting_id: ""
                        },
                    });
                }
            });

        });
    </script>