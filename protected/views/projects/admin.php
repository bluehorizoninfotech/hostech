<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Projects', 'url' => array('index')),
    array('label' => 'Create Projects', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('projects-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Projects</h1>


<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form display-none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'projects-grid',
    'dataProvider' => $model->search(),
    
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('name'=>'pid', 'htmlOptions' => array('width' => '40px','class'=>'font-weight-bold text-align-center')),  
        'name',
        /*
        array(
            'name' => 'client_id',
            'value' => '$data->client->name',
            'type' => 'raw',
            'filter' => CHtml::listData(Clients::model()->findAll(
                            array(
                                'select' => array('cid,name'),
                                'order' => 'name',
                                'distinct' => true
                    )), "cid", "name")
        ),
        */
        array(
            'name' => 'billable',
            'value' => '$data->billable0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="yesno"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        'budget',
        /*
          'description',
          'created_date',
          'created_by',
          'updated_date',
          'updated_by',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
