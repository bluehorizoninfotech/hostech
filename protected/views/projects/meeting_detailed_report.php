<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<head>
</head>
<div class="meeting-detailed-sec">
<div class="work-holder" id="expand-projects">
    <p id="success_message dispaly-none font-15 green-color font-weight-bold">Successfully ucreated..</p>
    <p id="error_message1 display-none font-15 red-color font-weight-bold">Report already exist...</p>
    <table class="inner_table">
        <thead>
            <tr>
                <th>SL No</th>
                <th>Milestone</th>
                <th>Contractor</th>
                <th>Main Task</th>
                <th>SubTask</th>
                <th>Area</th>
                <th>WorkType</th>
                <th>Total Quantity</th>
                <th>Targeted Quantity</th>
                <th>Achieved Quantity</th>
                <th>Balance Quantity</th>
                <th>Progress</th>
                <th>Status</th>
                <th>Description</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
            <form id="idForm">

                <?php
                $report_datas = !empty($project_report_model->report_data) ? json_decode($project_report_model->report_data, true) : '';
                ?>
                <input type="hidden" id="row_count" value="<?= count($report_datas) ?>">
                <?php
                $i = 1;
                if (count($report_datas) > 0) {
                    if (isset($_GET['id'])) {
                        $report_meeting_id = $_GET['id'];
                    }
                    foreach ($report_datas as $key => $report_data) {


                ?>

                        <input type="hidden" name="report[]" id="report_id" value="<?= $report_id ?>">
                        <input type="hidden" name="report_meeting_id[]" id="report_meeting_id" value="<?= $report_meeting_id ?>">

                        <tr>

                            <td><?= $i ?></td>
                            <td><input type="text" name="milestone_<?= $key ?>" id="milestone_id_<?= $key ?>" .$key value="<?= isset($report_data['milestone_title']) ? $report_data['milestone_title'] : '' ?>" readonly size="10"></td>
                            <td><input type="text" name="contractor_<?= $key ?>" id="contractor_id_<?= $key ?>" value="<?= isset($report_data['contractor_title']) ? $report_data['contractor_title'] : '' ?>" readonly size="10"></td>
                            <td><input type="text" name="task_<?= $key ?>" id="task_id_<?= $key ?>" value="<?= $report_data['task_title'] ?>" readonly size="4"></td>
                            <td><input type="text" name="sub_task_<?= $key ?>" id="sub_task_id_<?= $key ?>" value="<?= isset($report_data['sub_task']) ? $report_data['sub_task'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" name="area_<?= $key ?>" id="area_id_<?= $key ?>" value="<?= isset($report_data['area']) ? $report_data['area'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" name="work_type_<?= $key ?>" id="work_type_id_<?= $key ?>" value="<?= isset($report_data['work_type']) ? $report_data['work_type'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" name="total_qty_<?= $key ?>" id="total_qty_id_<?= $key ?>" value="<?= $report_data['total_qty'] ?>" readonly size="4"></td>
                            <td><input type="text" name="target_qty_<?= $key ?>" id="target_qty_id_<?= $key ?>" value="<?= $report_data['targeted_qty'] ?>" readonly size="4"></td>
                            <td><input type="text" name="achieved_qty_<?= $key ?>" id="achieved_qty_id_<?= $key ?>" value="<?= $report_data['achieved_qty'] ?>" readonly size="4"></td>
                            <td><input type="text" name="balance_<?= $key ?>" id="balance_id_<?= $key ?>" value="<?= $report_data['balance'] ?>" readonly size="4"></td>
                            <td><input type="text" name="progres_percentage_<?= $key ?>" id="progres_percentage_id_<?= $key ?>" value="<?= $report_data['progres_percentage'] ?>" readonly size="4"></td>
                            <td><input type="text" name="status_<?= $key ?>" id="status_id_<?= $key ?>" value="<?= $report_data['status'] ?>" readonly size="4"></td>
                            <td><input type="text" name="description_<?= $key ?>" id="description_id_<?= $key ?>" value="<?= $report_data['description'] ?>" readonly size="4"></td>
                            <td><input type="text" name="comment_<?= $key ?>" id="comment_id_<?= $key ?>" size="4"></td>
                        </tr>

                <?php
                        $i++;
                    }
                }
                ?>

            </form>
        </tbody>
    </table>
    <input name="submit" type="submit" value="Submit" class="btn blue save_btn save_report">

</div>
            </div>
<script>
    $(document).ready(function() {

        $(".save_report").click(function() {

            var count = $('#row_count').val();
            var milestone_arr = [];
            var contractor_arr = [];
            var main_task_arr = [];
            var sub_task_arr = [];
            var area_arr = [];
            var work_type_arr = [];
            var total_qty_arr = [];
            var target_qty_arr = [];
            var achieved_qty_arr = [];
            var balance_qty_arr = [];
            var progress_arr = [];
            var status_arr = [];
            var description_arr = [];
            var comment_arr = [];
            var report_meeting_id = $('#report_meeting_id').val();
            var report_id = $('#report_id').val();
            for (i = 0; i < count; i++) {
                milestone_arr[i] = $('#milestone_id_' + i).val();
                contractor_arr[i] = $('#contractor_id_' + i).val();
                main_task_arr[i] = $('#task_id_' + i).val();
                sub_task_arr[i] = $('#sub_task_id_' + i).val();
                area_arr[i] = $('#area_id_' + i).val();
                work_type_arr[i] = $('#work_type_id_' + i).val();
                total_qty_arr[i] = $('#total_qty_id_' + i).val();
                target_qty_arr[i] = $('#target_qty_id_' + i).val();
                achieved_qty_arr[i] = $('#achieved_qty_id_' + i).val();
                balance_qty_arr[i] = $('#balance_id_' + i).val();
                progress_arr[i] = $('#progres_percentage_id_' + i).val();
                status_arr[i] = $('#status_id_' + i).val();
                description_arr[i] = $('#description_id_' + i).val();
                comment_arr[i] = $('#comment_id_' + i).val();
            }

            $.ajax({
                method: "post",
                dataType: "json",
                data: {
                    milestone_arr: milestone_arr,
                    contractor_arr: contractor_arr,
                    main_task_arr: main_task_arr,
                    sub_task_arr: sub_task_arr,
                    area_arr: area_arr,
                    work_type_arr: work_type_arr,
                    total_qty_arr: total_qty_arr,
                    target_qty_arr: target_qty_arr,
                    achieved_qty_arr: achieved_qty_arr,
                    balance_qty_arr: balance_qty_arr,
                    progress_arr: progress_arr,
                    status_arr: status_arr,
                    description_arr: description_arr,
                    comment_arr: comment_arr,
                    report_meeting_id: report_meeting_id,
                    report_id: report_id,
                    count: count
                },
                url: '<?php echo Yii::app()->createUrl("Projects/savemeetingdetreport") ?>',
                success: function(data) {


                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                        $('#step_1').removeClass('active');
                        $('#step_1').addClass('progress_tab');
                        $('#step_1_info').removeClass('active in');
                        $('#step_2').removeClass('active');
                        $('#step_2').addClass('progress_tab');
                        $('#step_2_info').removeClass('active in');
                        $('#step_3').removeClass('active');
                        $('#step_3').addClass('progress_tab');
                        $('#step_3_info').removeClass('active in');
                        $('#step_4').addClass('active');
                        $('#step_4_info').addClass('active in');
                    } else {
                        $("#error_message1").fadeIn().delay(1000).fadeOut();
                        $('#step_1').removeClass('active');
                        $('#step_1').addClass('progress_tab');
                        $('#step_1_info').removeClass('active in');
                        $('#step_2').removeClass('active');
                        $('#step_2').addClass('progress_tab');
                        $('#step_2_info').removeClass('active in');
                        $('#step_3').removeClass('active');
                        $('#step_3').addClass('progress_tab');
                        $('#step_3_info').removeClass('active in');
                        $('#step_4').addClass('active');
                        $('#step_4_info').addClass('active in');
                    }

                }
            });

        });

    });
</script>