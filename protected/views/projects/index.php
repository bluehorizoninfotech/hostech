<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<?php
/* @var $this ProjectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        //	    array('label' => 'Create Projects', 'url' => array('create')),
        //	    array('label' => 'Manage Projects', 'url' => array('admin')),
    );
}
?>
<div class="project-index-sec">
<div class="clearfix">
    <div class="add link pull-right">
        <?php
        // $createUrl = $this->createUrl('addproject', array("asDialog" => 1, "gridId" => 'address-grid'));
        // echo CHtml::link('Add Project', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        echo CHtml::link('Add Project', array('projects/addproject'), array('class' => 'btn blue'));
        ?>
    </div>
    <h1>Projects</h1>
</div>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="alert alert-success alert-dismissable ">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'projects-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate' => false,
    'itemsCssClass' => 'table table-bordered',
    //'template' => '<div class="table-responsive">{items}</div>',
    'filter' => $model,
    'pager' => array(
        'id' => 'dataTables-example_paginate',  'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),

    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array(
            'header' => 'S.No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
        array(
            'htmlOptions' => array('class' => 'width-80'),
            'class' => 'CButtonColumn',
            'template' => '{view}{update}' . ((isset(Yii::app()->user->role) && Yii::app()->user->role == 1) ? "{delete}" : ""),

            'buttons' => array(
                'view' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-eye icon-comn', 'title' => 'View'),
                ),
                'update' => array(
                    'label' => '',
                    'imageUrl' => false,

                    'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Edit',),
                ),
                'delete' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete',),
                    'visible' => ' (in_array("/projects/delete", Yii::app()->session["menuauthlist"])) && $data->checkdelete($data->pid) ? true: false'
                )
            )

        ),
        'project_no',
        array(
            'name'  => 'name',
            'value' => 'CHtml::link($data->name, Yii::app()->createUrl("projects/view",array("id"=>$data->pid)))',
            'type'  => 'raw',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Projects[name]',
                'source' => $this->createUrl('Projects/autocomplete'),
                'value' => isset($model->name) ? $model->name : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) { 
                       $("#Projects_name").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Projects_name").val(ui.item.value); }'

                ),
            ), true),
        ),

        array(
            'name' => 'client_id',
            'value' => 'isset($data->client) ? $data->client->name :  "N/A"',
            'type' => 'raw',
            'filter' => CHtml::listData(Clients::model()->findAll(
                array(
                    'select' => array('cid,name'),
                    'order' => 'name',
                    'distinct' => true
                )
            ), "cid", "name"),
            'type'  => 'raw',
            'visible' => (yii::app()->user->role == 1),
        ),

        array(
            'name' => 'billable',
            'value' => '$data->billable0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type="yesno"',
                    'order' => 'status_type',
                    'distinct' => true
                )
            ), "sid", "caption")
        ),
        array(
            'name' => 'assigned_to',
            'value' => '$data->getassignees($data->assigned_to,$data)',
            'filter' => CHtml::activeDropDownList(
                $model,
                "assigned_to",
                CHtml::listData(Users::model()->findAll(), 'userid', 'first_name'),
                array(
                    'empty' => '',
                )
            ),
            // 'type' => 'raw',           
        ),
        array(
            'header' => Clientsite::model()->getAttributeLabel('other_users'), //column header
            'value' => function ($model) {
                return $model->getclientsite($model->pid);
            },
            // '$data->getclientsite($data->pid)', //column name, php expression
            // 'name'=>'site_name',
            'type' => 'raw',
            'filter' => CHtml::listData(Clientsite::model()->findAll(
                array(
                    'select' => array('id,site_name'),
                    'order' => 'site_name',
                    'distinct' => true
                )
            ), "id", "site_name"),
        ),
        array(
            'name' => 'start_date',
            'value' => 'date("d-M-y",strtotime($data->start_date))',
            'type' => 'html',
        ),
        array(
            'name' => 'end_date',
            'value' => function ($model) {
                if ($model->end_date != '') {
                    return  date("d-M-y", strtotime($model->end_date));
                } else {
                    return '';
                }
            },
            'type' => 'html',
        ),
        array(
            'name' => 'img_path',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'image_handler'),
            'value' =>  function ($model) {
            
               return isset($model->img_path) ? CHtml::image(Yii::app()->request->baseUrl . '/uploads/project/' . $model->img_path) : 'No Image';
            },
            'filter' => false,


        ),
        // array(
        //     'header' => 'Analyzer',
        //     'type'=>'raw',
        //     'value' => 'CHtml::link("View ($data->pid)",Yii::app()->createUrl("projects/analyzerlink",array("id"=>$data->pid)),array("target"=>"_blank"))',                               
        // ),
        array(
            'name' => 'budget',
            'htmlOptions' => array('width' => 100),
        ),
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type="active_status"',
                    'order' => 'status_type',
                    'distinct' => true
                )
            ), "sid", "caption")
        ),

        /*
          'description',
          'created_date',
          'created_by',
          'updated_date',
          'updated_by',
         */

    ),
));
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add project',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="450" frameborder="0" class="min-height-400"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>

<?php
Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   
  } );
  $("[rel=tooltip]").tooltip({html:true});
');


?>
</div>

<script>

$(function () {  
    $( "input[name='Projects[start_date]']" ).datepicker({                 
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd'                     
    });
    
    $( "input[name='Projects[end_date]']" ).datepicker({                 
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd'                     
    });   
});
    </script>