<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<?php
/* @var $this ProjectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);

if (yii::app()->user->role <= 2) {
    $this->menu = array(
        //	    array('label' => 'Create Projects', 'url' => array('create')),
        //	    array('label' => 'Manage Projects', 'url' => array('admin')),
    );
}
?>
<div class="project-mapping-sec">
<div class="clearfix">

    <h1>Project Mapping</h1>
    <span id="success_message" class="display-none font-15 green-color font-weight-bold">Mapped Successfully</span>
</div>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="alert alert-success alert-dismissable ">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'projects-mapping-grid',
    'dataProvider' => $model->project_mapping(),
    'ajaxUpdate' => false,
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'pager' => array(
        'id' => 'dataTables-example_paginate',  'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '
    ),

    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array(
            'header' => 'S.No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
        array(
            'header' => 'Action',
            'value' => '"<input type=\'button\' name=\'save\' value=\'Save\' id=\'save_".$data->pid."\' class=\'map_project btn-secondary\' data-id=\'".$data->pid."\' />"',
            'type' => 'raw',

        ),
        'project_no',
        array(
            'name'  => 'name',
            'value' => 'CHtml::link($data->name, Yii::app()->createUrl("projects/view",array("id"=>$data->pid)))',
            'type'  => 'raw',
            'filter' => $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'Projects[name]',
                'source' => $this->createUrl('Projects/autocomplete'),
                'value' => isset($model->name) ? $model->name : "",
                'options' => array(
                    'focus' => 'js:function(event, ui) { 
                       $("#Projects_name").val(ui.item.value);
                    }',
                    'minLength' => '1',
                    'showAnim' => 'fold',
                    'select' => 'js:function(event, ui) {  $("#Projects_name").val(ui.item.value); }'

                ),
            ), true),
        ),

        array(
            'name' => 'client_id',
            'value' => 'isset($data->client) ? $data->client->name :  "N/A"',
            'type' => 'raw',
            'filter' => CHtml::listData(Clients::model()->findAll(
                array(
                    'select' => array('cid,name'),
                    'order' => 'name',
                    'distinct' => true
                )
            ), "cid", "name"),
            'type'  => 'raw',
            'visible' => (yii::app()->user->role == 1),
        ),

        array(
            'header' => 'Accounts Project',
            //  'value' => '$data->pid',
            'value' => '$data->getAcoountProject()',
            'type' => 'raw',
            'htmlOptions' => array('width' => '250px',),
            'filter' => false,
        ),
        
    ),
));
?>


<div id="id_view"></div>
</div>
<?php
Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   
  } );
  $("[rel=tooltip]").tooltip({html:true});
');


?>
<script>
    $('.map_project').on('click', function() {
        var pms_project_id = $(this).attr('data-id');
        var parenttr = $(this).closest("tr");
        var acc_project_id = parenttr.find(".acc_project").val();

        if (acc_project_id == "") {
            alert("Please select account's project")
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            data: {
                'pms_project_id': pms_project_id,
                'acc_project_id': acc_project_id
            },
            url: '<?php echo $this->createUrl('projects/add_project_mapping'); ?>',
        }).done(function(data) {

            if (data.status == 1) {

                $('#success_message').show();
                $.fn.yiiGridView.update("projects-mapping-grid");

            }
        });

    });
</script>