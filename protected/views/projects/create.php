<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>

<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    'Create',
);

$this->menu = array(
    //	array('label'=>'List Projects', 'url'=>array('index')),
    //	array('label'=>'Manage Projects', 'url'=>array('admin')),
);
?>
<div class="container ">
    <div class="add link pull-right">
        <?php
        echo CHtml::link('Back', array('projects/index'), array('class' => 'btn blue'));
        ?>
    </div>
    <?php //echo $this->renderPartial('_form', array('model'=>$model,'model2' => $model2,'site_model'=>$site_model)); 
    ?>
    <?php echo $this->renderPartial('_newprojectform', array('model' => $model, 'model2' => $model2, 'milestone_model' => $milestone_model, 'work_site_model' => $work_site_model, 'user_assign_model' => $user_assign_model, 'area_model' => $area_model, 'work_type_model' => $work_type_model, 'task' => $task, 'template' => $template, 'budget_head_model' => $budget_head_model, 'project_no' => $project_no,'body_array'=>$body_array)); ?>
</div>
<script>
    $(document).ready(function () {
        $.noConflict();
        // $( "#Projects_start_date" ).datepicker({
        //             dateFormat: 'dd-M-yy',
        //             maxDate: $("#Projects_end_date").val()
        //         });
        //         $( "#Projects_end_date" ).datepicker({
        //             dateFormat: 'dd-M-yy',
        //         });
        //         $("#Projects_start_date").change(function() {
        //             $("#Projects_end_date").datepicker('option', 'minDate', $(this).val());
        //         });
        //         $("#Projects_end_date").change(function() {
        //             $("#Projects_start_date").datepicker('option', 'maxDate', $(this).val());
        //         });
        $('.assigned_to_').val('');
        $('.user_ids').val('');
        $('#Projects_assigned_to').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Assigned To',

        });
        $('#user_assign_model_ids').select2({
            width: '100%',
            multiple: true,
            placeholder: 'Site Engineers/Supervisors',

        });
        $('.select2_project').select2();
    });
    $(document).ready(function () {
        $('.js-example-basic-single').select2();
    });
</script>