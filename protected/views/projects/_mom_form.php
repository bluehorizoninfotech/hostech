<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<div class="meeting-form-project-sec">
    <div class="long-form combined-form">

        <!-- <img id="loader" src="<?php echo Yii::app()->request->baseUrl ?>/images/loading.gif" width="50%" /> -->

        <div class="tabs">
            <ul class="nav nav-pills">
                <li id="mom_step_1" class="active"><span class="dot"></span><a data-toggle="tab"
                        href="#mom_step_1_info">Step 1</a></li>
                <li id="mom_step_2"><span class="dot"></span><a data-toggle="tab" href="#mom_step_2_info">Step 2</a>
                </li>
                <li id="mom_step_3"><span class="dot"></span><a data-toggle="tab" href="#mom_step_3_info">Step 3</a>
                </li>
                <li id="mom_step_4"><span class="dot"></span><a data-toggle="tab" href="#mom_step_4_info">Step 4</a>
                </li>
            </ul>
        </div>

        <div class="tab-content custom-tab-content padding-20">
            <p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully created..</p>
            <p id="error_message" class="display-none font-15 red-color font-weight-bold">An error occured...</p>
            <p id="error_message1" class="display-none font-15 red-color font-weight-bold">Report already exist...</p>
            <div id="mom_step_1_info" class="tab-pane fade active in">
                <?php echo $this->renderPartial('_mom_step_1', array('site_model' => $site_model, 'model' => $model, 'projects' => $projects, 'approved_by' => $approved_by)); ?>
            </div>
            <div id="mom_step_2_info" class="tab-pane fade ">
                <?php echo $this->renderPartial('_mom_step_2', array('model' => $meeting_points_model, 'meeting_model' => $model, 'body_array' => $body_array)); ?>
            </div>
            <div id="mom_step_3_info" class="tab-pane fade">
                <?php echo $this->renderPartial('_mom_step_3', array('model' => $report_model, 'site_meeting_model' => $model)); ?>
            </div>
            <div id="mom_step_4_info" class="tab-pane fade">
                <?php echo $this->renderPartial('_mom_step_4', array('model' => $next_meeting_data)); ?>
            </div>



        </div>
    </div>
</div>
<script>
    // meeting form start
    // window.addEventListener('popstate', function (event) {
    //     location.reload(); // Reload the page
    // });
    function meetingform(form, data, hasError) {
        if (!hasError) {
            //$('.loaderdiv').show(); 
            var formData = new FormData($("#site-meetings-form")[0]);
            var records = $('#meeting_participants').val();
            var project_id = $('#SiteMeetings_project_id').val();
            var clientsite = $('#SiteMeetings_site_id').val();
            var date = $('#SiteMeetings_date').val();
            var venue = $('#SiteMeetings_venue').val();
            var time = $('#SiteMeetings_meeting_time').val();
            var meeting_no = $('#SiteMeetings_meeting_number').val();
            var objective = $('#SiteMeetings_objective').val();
            var distribution = $('#SiteMeetings_distribution').val();
            var approved_by = $('#Meeting_users').val();
            var model_id = $('#model_id').val();
            var meeting_ref = $('#SiteMeetings_meeting_ref').val();
            var meeting_title = $('#SiteMeetings_meeting_title').val();


            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl("SiteMeetings/create_mom") ?>',
                method: "POST",
                data: {

                    records: records,
                    project_id: project_id,
                    clientsite: clientsite,
                    date: date,
                    venue: venue,
                    time: time,
                    meeting_no: meeting_no,
                    objective: objective,
                    approved_by: approved_by,
                    model_id: model_id,
                    meeting_ref: meeting_ref,
                    meeting_title: meeting_title,
                    distribution: distribution


                },
                "dataType": "json",
                success: function (data) {

                    $('.loaderdiv').hide();
                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                        $('#MeetingMinutes_meeting_id').val(data.model_id);
                        $('#MeetingMinutes_id').val(data.model_id);
                        $('#MeetingMinutes_meeting_status').val(data.meeting_status);
                        $('#MeetingMinutes_meeting_project_id').val(data.project_id);
                        $('#SiteMeetings_meeting_id').val(data.model_id);
                        $('#mom_step_1').removeClass('active');
                        $('#mom_step_1').addClass('progress_tab');
                        $('#mom_step_1_info').removeClass('active in');
                        $('#mom_step_2').addClass('active');
                        $('#mom_step_2_info').addClass('active in');
                        if (data.milestones !== '') {
                            $('#MeetingMinutes_milestone').html(data.milestones);
                        }
                        if (data.report_html !== '') {
                            $('#MeetingDetailedReports_report_id').html(data.report_html);
                            $('#report_meeting_id').val(data.model_id);
                        }
                    } else if (data.status == 0) {
                        $("#error_message").fadeIn().delay(1000).fadeOut();
                    }
                }

            });

        } else {
            $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
            setTimeout('$("#successMessage").fadeOut("slow")', 2000);
        }
    }

    // meeting form end
    function meeting_minutes_validate() {
        var minutesData = hot.getData();
        minutesData = minutesData.filter(row => row.some(col => col !== "" && col !== null));
        var messages = "";
        if (minutesData.length === 0) {
            messages += "You Need to fill atleast one row ";
        } else {
            minutesData.forEach(row => row.splice(5, 1));

            minutesData.forEach((row, rowIndex) => {
                var errors = "";
                var date_error="";
                for (var i = 0; i < row.length; i++) {
                    if ((row[i] == "" || row[i] == null) && i != 4 && i != 5) {
                        errors += header_data[i] + ",";
                    }
                    if (i == 3 && row[i] != "" && row[i] != null && !isValidDateInAnyFormat(row[i])) {
                        date_error=header_data[i] + " in invalid format."
                    }
                }
                if (errors !== "") {
                    errors = errors.slice(0, -1);
                    messages += errors + " are empty in row " + (rowIndex + 1)+"." + date_error+" \n";
                }else if(date_error !=""){
                    messages +=date_error+" \n";
                }

            });
        }

        var result = {};
        if (messages !== "") {
            var alertBox = document.getElementById("error-message");
            alertBox.innerText = messages;
            alertBox.style.display = "block";
            result.status = false;
            result.data = minutesData;
        } else {
            result.status = true;
            result.data = minutesData;
        }

        return result;
    }
    function isValidDateInAnyFormat(dateString) {
        // Try parsing the date with various formats
        var formats = [
            "YYYY-MM-DD",
            "YYYY/MM/DD",
            "YYYY.MM.DD",
            "DD-MM-YYYY",
            "DD/MM/YYYY",
            "DD.MM.YYYY",
            "MM-DD-YYYY",
            "MM/DD/YYYY",
            "MM.DD.YYYY"
        ];

        for (var i = 0; i < formats.length; i++) {
            if (moment(dateString, formats[i], true).isValid()) {
                return true;
            }
        }

        return false;
    }

    function minutes_form(form, data, hasError) {

        if (!hasError) {
            var formData = new FormData($("#meeting-minutes-form")[0]);
            var minutesData = meeting_minutes_validate();
            if (minutesData.status === false) {
                return false;
            }
            var minutesDataJson = JSON.stringify(minutesData.data);
            formData.append('MeetingMinutes[minutes_data]', minutesDataJson);
            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('MeetingMinutes/create'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function (data) {
                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                        if (data.next_level == 0) {
                            $("#meeting-minutes-form")[0].reset();
                            // CKEDITOR.instances['MeetingMinutes_points_discussed'].setData('');
                            // $('.action_by_val').val('');
                            $('#MeetingMinutes_users').trigger('change');
                            $('#MeetingMinutes_meeting_minutes_id').val('');
                        } else {
                            $("#meeting-minutes-form")[0].reset();
                            // CKEDITOR.instances['MeetingMinutes_points_discussed'].setData('');
                            // $('.action_by_val').val('');
                            $('#MeetingMinutes_users').trigger('change');
                            $('#meeting_id').val(data.meeting_id);
                            $('#report_meeting_id').val(data.meeting_id);
                            $('#mom_step_2').removeClass('active');
                            $('#mom_step_2').addClass('progress_tab');
                            $('#mom_step_2_info').removeClass('active in');
                            $('#mom_step_3').addClass('active');
                            $('#mom_step_3_info').addClass('active in');
                        }
                        $("#meeting-minutes-grid").load(location.href + " #meeting-minutes-grid");


                    } else if (data.status == 2) {
                        $("#error_message").fadeIn().delay(1000).fadeOut();
                    }

                }
            });
        } else {
            $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
            setTimeout('$("#successMessage").fadeOut("slow")', 2000);
        }

    }

    function next_meeting_form(form, data, hasError) {
        if (!hasError) {
            var formData = new FormData($("#site-meetings-form-2")[0]);
            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('SiteMeetings/meetingend'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function (data) {
                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                        // location.reload();                     
                        window.location.href = data.redirect

                    } else if (data.status == 0) {
                        $("#error_message").fadeIn().delay(1000).fadeOut();
                    }

                }
            });
        } else {
            $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
            setTimeout('$("#successMessage").fadeOut("slow")', 2000);
        }

    }
    // $('.action_by_val').val('');
    // $('#MeetingMinutes_users').select2({
    //     width: '100%',
    //     multiple: true,
    //     placeholder: 'Action By',

    // });
    $('.approved_by_val').val('');
    $('#Meeting_users').select2({
        width: '100%',
        multiple: true,
        placeholder: 'Approved By',

    });
    $('.participant').val('');
    $('#meeting_participants').select2({
        width: '100%',
        multiple: true,
        placeholder: 'Choose Meeting Participants',

    });
    $('.custom-dropdown').select2({
        width: '100%',
        multiple: true,
        placeholder: 'Choose Meeting Participants',

    });

</script>