<table class="inner_table">
        <thead>
            <tr>
                <th>SL No</th>
                <th>Milestone</th>
                <th>Contractor</th>
                <th>Main Task</th>
                <th>SubTask</th>
                <th>Area</th>
                <th>WorkType</th>
                <th>Total Quantity</th>
                <th>Targeted Quantity</th>
                <th>Achieved Quantity</th>
                <th>Balance Quantity</th>
                <th>Progress</th>
                <th>Status</th>
                <th>Description</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
            <form id="idForm">
                <input type="hidden" id="row_count" value="<?= count($model) ?>">
                <?php
                $i = 1;
                if (count($model) > 0) {
                    foreach ($model as $key => $report_data) {
                ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><input type="text" value="<?= isset($report_data['milestone']) ? $report_data['milestone'] : '' ?>" readonly size="10"></td>
                            <td><input type="text" value="<?= isset($report_data['contractor']) ? $report_data['contractor'] : '' ?>" readonly size="10"></td>
                            <td><input type="text" value="<?= $report_data['main_task'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= isset($report_data['sub_task']) ? $report_data['sub_task'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= isset($report_data['area']) ? $report_data['area'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= isset($report_data['work_type']) ? $report_data['work_type'] : '' ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['total_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['target_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['achieved_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['balance_quantity'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['progress'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['status'] ?>" readonly size="4"></td>
                            <td><input type="text" value="<?= $report_data['description'] ?>" readonly size="4"></td>
                            <td><input type="text" class="comment" name="comment_<?= $key ?>" id="comment_id" size="10" value="<?= $report_data['comments'] ?>"></td>
                            <input type="hidden" name=id_<?= $key ?> id="id" value="<?= $report_data['id'] ?>">

                        </tr>

                <?php
                        $i++;
                    }
                }
                ?>

            </form>
        </tbody>
    </table>