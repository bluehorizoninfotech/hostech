<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<div class="meeting-form-project-sec">
<div class="long-form combined-form">

    <!-- <img id="loader" src="<?php echo Yii::app()->request->baseUrl ?>/images/loading.gif" width="50%" /> -->

    <div class="tabs">
        <ul class="nav nav-pills">
            <li id="step_1" class="active"><span class="dot"></span><a data-toggle="tab" href="#step_1_info">Step 1</a></li>
            <li id="step_2"><span class="dot"></span><a data-toggle="tab" href="#step_2_info">Step 2</a></li>
            <li id="step_3"><span class="dot"></span><a data-toggle="tab" href="#step_3_info">Step 3</a></li>
            <li id="step_4"><span class="dot"></span><a data-toggle="tab" href="#step_4_info">Step 4</a></li>
        </ul>
    </div>

    <div class="tab-content custom-tab-content padding-20">
        <p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully created..</p>
        <p id="error_message" class="display-none font-15 red-color font-weight-bold">An error occured...</p>
        <p id="error_message1" class="display-none font-15 red-color font-weight-bold">Report already exist...</p>
        <div id="step_1_info" class="tab-pane fade active in">
            <?php echo $this->renderPartial('_meeting_step_1', array('site_model' => $site_model, 'model' => $model, 'projects' => $projects, 'approved_by' => $approved_by)); ?>
        </div>
        <div id="step_2_info" class="tab-pane fade">
            <?php echo $this->renderPartial('_meeting_step_2', array('model' => $meeting_points_model, 'meeting_model' => $model)); ?>
        </div>
        <div id="step_3_info" class="tab-pane fade">
            <?php echo $this->renderPartial('_meeting_step_3', array('model' => $report_model, 'site_meeting_model' => $model)); ?>
        </div>
        <div id="step_4_info" class="tab-pane fade">
            <?php echo $this->renderPartial('_meeting_step_4', array('model' => $next_meeting_data)); ?>
        </div>



    </div>
</div>
</div>
<script>
    function meetingform(form, data, hasError) {
        if (!hasError) {
            var formData = new FormData($("#site-meetings-form")[0]);
            $('.loaderdiv').show();
            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('SiteMeetings/create'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function(data) {
                    $('.loaderdiv').hide();
                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                        $('#MeetingMinutes_meeting_id').val(data.model_id);
                        $('#MeetingMinutes_id').val(data.model_id);
                        $('#MeetingMinutes_meeting_status').val(data.meeting_status);
                        $('#MeetingMinutes_meeting_project_id').val(data.project_id);
                        $('#SiteMeetings_meeting_id').val(data.model_id);
                        $('#step_1').removeClass('active');
                        $('#step_1').addClass('progress_tab');
                        $('#step_1_info').removeClass('active in');
                        $('#step_2').addClass('active');
                        $('#step_2_info').addClass('active in');
                        if (data.milestones !== '') {
                            $('#MeetingMinutes_milestone_id').html(data.milestones);
                        }
                        if (data.report_html !== '') {
                            $('#MeetingDetailedReports_report_id').html(data.report_html);
                            $('#report_meeting_id').val(data.model_id);
                        }
                    } else if (data.status == 0) {
                        $("#error_message").fadeIn().delay(1000).fadeOut();
                    }
                }
            });
        } else {
            $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
            setTimeout('$("#successMessage").fadeOut("slow")', 2000);
        }

    }

    function minutes_form(form, data, hasError) {
        if (!hasError) {
            var formData = new FormData($("#meeting-minutes-form")[0]);
            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('MeetingMinutes/create'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function(data) {
                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                        if (data.next_level == 0) {
                            $("#meeting-minutes-form")[0].reset();
                            CKEDITOR.instances['MeetingMinutes_points_discussed'].setData('');
                            $('.action_by_val').val('');
                            $('#MeetingMinutes_users').trigger('change');
                            $('#MeetingMinutes_meeting_minutes_id').val('');
                        } else {
                            $("#meeting-minutes-form")[0].reset();
                            CKEDITOR.instances['MeetingMinutes_points_discussed'].setData('');
                            $('.action_by_val').val('');
                            $('#MeetingMinutes_users').trigger('change');
                            $('#meeting_id').val(data.meeting_id);
                            $('#report_meeting_id').val(data.meeting_id);
                            $('#step_2').removeClass('active');
                            $('#step_2').addClass('progress_tab');
                            $('#step_2_info').removeClass('active in');
                            $('#step_3').addClass('active');
                            $('#step_3_info').addClass('active in');
                        }
                        $("#meeting-minutes-grid").load(location.href + " #meeting-minutes-grid");


                    } else if (data.status == 2) {
                        $("#error_message").fadeIn().delay(1000).fadeOut();
                    }

                }
            });
        } else {
            $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
            setTimeout('$("#successMessage").fadeOut("slow")', 2000);
        }

    }

    function next_meeting_form(form, data, hasError) {
        if (!hasError) {
            var formData = new FormData($("#site-meetings-form-2")[0]);
            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('SiteMeetings/meetingend'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function(data) {
                    if (data.status == 1) {
                        $("#success_message").fadeIn().delay(1000).fadeOut();
                        // location.reload();                     
                        window.location.href = data.redirect

                    } else if (data.status == 0) {
                        $("#error_message").fadeIn().delay(1000).fadeOut();
                    }

                }
            });
        } else {
            $('#successMessage').html('<div class="alert alert-danger"><strong>Error!</strong> An error has occured, please try again. </div>');
            setTimeout('$("#successMessage").fadeOut("slow")', 2000);
        }

    }
    $('.action_by_val').val('');
    $('#MeetingMinutes_users').select2({
        width: '100%',
        multiple: true,
        placeholder: 'Action By',

    });
    $('.approved_by_val').val('');
    $('#Meeting_users').select2({
        width: '100%',
        multiple: true,
        placeholder: 'Approved By',

    });
</script>