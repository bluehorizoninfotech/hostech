<?php

$contractor_list = Contractors::model()->findAll(array('condition' => 'status = 1'));
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'site-meetings-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'beforeValidate' => new CJavaScriptExpression('function(form) {
                for(var instanceName in CKEDITOR.instances) { 
                    CKEDITOR.instances[instanceName].updateElement();
                }
                return true;
            }'),
        'afterValidate' => 'js:function(form, data, hasError) {
                if(hasError) {                                          
                }
                else
                {
                // return true;  it will submit default way
                       meetingform(form, data, hasError); //and it will submit by ajax function
                   }
               }',
    ),
)); ?>
<div class="row">
    <?php
    if (!$model->isNewRecord) {
    ?>
        <input type="hidden" name="SiteMeetings[id]" value="<?= $model->id ?>">
    <?php
    }
    ?> <div class="col-md-4">


        <?php echo $form->labelEx($model, 'project_id'); ?>
        <?php echo   $form->dropDownList(
            $model,
            'project_id',
            $projects,
            array(
                'empty' => 'Choose a project',
                'class' => 'form-control project change_project',
                'ajax' => array(
                    'type' => 'POST',
                    'url' => (Yii::app()->user->role == 10) ? CController::createUrl('Tasks/getclientlocationbyid') : CController::createUrl('Tasks/getclientlocation'), //or $this->createUrl('loadcities') if '$this' extends CController
                    'update' => '#SiteMeetings_site_id', //or 'success' => 'function(data){...handle the data in the way you want...}',
                    'data' => array('pid' => 'js:this.value'),
                )
            )
        ); ?>
        <?php echo $form->error($model, 'project_id'); ?>
    </div>
    <div class="col-md-4">


        <?php echo $form->labelEx($model, 'site_id'); ?>
        <?php
        $location = Clientsite::model()->findAll();
        if (empty($model->site_id)) {
            if (Yii::app()->user->project_id != "") {
                $location = Clientsite::model()->findAll(array("condition" => "pid = " . Yii::app()->user->project_id));
            }
        }
        echo $form->dropDownList(
            $model,
            'site_id',
            CHtml::listData($location, 'id', 'site_name'),
            array(
                'empty' => 'Choose a client site',
                'class' => 'form-control',
            )
        ); ?>
        <?php echo $form->error($model, 'site_id'); ?>
    </div>


    <div class="col-md-4">
        <?php
        if (!$model->isNewRecord) {
            if ($model->date != "") {
                $model->date = date('d-M-y', strtotime($model->date));
            }
        }
        ?>

        <?php echo $form->labelEx($model, 'date'); ?>
        <?php echo $form->textField($model, 'date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        <?php echo $form->error($model, 'date'); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'venue'); ?>
        <?php echo $form->textField($model, 'venue', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'venue'); ?>
    </div>
    <div class="col-md-2">
        <?php echo $form->labelEx($model, 'meeting_time'); ?>
        <input type="time" class="form-control" value="<?php echo  !$model->isNewRecord ? $model->meeting_time : '' ?>" name="SiteMeetings[meeting_time]" id="SiteMeetings_meeting_time">
        <?php echo $form->error($model, 'meeting_time'); ?>
        <span id="time_format"></span>
    </div>
    <div class="col-md-4 col-md-offset-2">
        <?php echo $form->labelEx($model, 'meeting_number'); ?>
        <?php echo $form->textField($model, 'meeting_number', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'meeting_number'); ?>
    </div>
</div>
<div class="row ">
    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'objective'); ?>
        <?php echo $form->textArea($model, 'objective', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'objective'); ?>
    </div>
</div>
<div class="row">
    <div id="" class="participants_data participants">
        <div class="col-md-4">
            <label for="MeetingParticipants_contractor_id">Contractor</label>
            <select class="form-control" name="MeetingParticipants[contractor_id][]" id="MeetingParticipants_contractor_id">
                <option value="">Select Contractor</option>
                <?php
                foreach ($contractor_list as $contractors) {
                    echo '<option value="' . $contractors['id'] . '">' . $contractors['contractor_title'] . '</option>';
                }
                ?>
            </select>

        </div>
        <div class="col-md-4">
            <label for="MeetingParticipants_participants">Participants</label> <input class="form-control" name="MeetingParticipants[participants][]" id="MeetingParticipants_participants" type="text">
        </div>

        <div class="col-md-4">
            <label for="MeetingParticipants_designation">Designation</label> <input class="form-control" name="MeetingParticipants[designation][]" id="MeetingParticipants_designation" type="text">
        </div>

    </div>

    <input type="button" id="add_new" value="Add" class="margin-15" onClick="addInput();">
    <br>

    <div class="col-md-4">
        <?php echo $form->labelEx($model, 'approved_by'); ?>
        <select name="Meeting[approved_by][]" id="Meeting_users" class="form-control js-example-basic-multiple approved_by_val">
            <?php
            $condition = array('select' =>  array('userid,CONCAT_ws(" ",first_name,last_name) as full_name,user_type'), 'condition' => 'status=0', 'order' => 'first_name ASC');
            $users = Users::model()->findAll($condition);
            foreach ($users as $key => $value) {
            ?>
                <option value="<?php echo $value['userid']; ?>"><?php echo $value['full_name'] . ' (' . $value->userType->role . ')'; ?></option>
            <?php
            }
            ?>
        </select>
        <?php echo $form->error($model, 'approved_by'); ?>
    </div>










</div>
<div class="row text-right">
    <div class="col-sm-12">
        <?php echo CHtml::SubmitButton('Next', array('class' => 'btn blue save_btn margin-left-30','id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(".datepicker").attr("autocomplete", "off");
    $("#SiteMeetings_date").datepicker({
        dateFormat: 'dd-M-yy',
        // maxDate: new Date(maximumDate)
    });
    CKEDITOR.replace('SiteMeetings_objective');

    function addInput() {
        var data = $('.participants_data').html();
        var newdiv = document.createElement('div');
        newdiv.className = 'participants';
        newdiv.innerHTML = data;
        $(newdiv).insertBefore("#add_new");
    }


    $(document).ready(function() {

        $('#SiteMeetings_meeting_time').blur(function() {
            t = document.getElementById('SiteMeetings_meeting_time').value
            var [h, m] = t.split(":");
            var hour_val = (h % 12 + 12 * (h % 12 == 0));
            if (hour_val.toString().length == 1) {
                hour_val = "0" + hour_val;
            }
            var time_val = (hour_val + ":" + m);
            var time_regex = '';
            var time_regex = ((h % 12 + 12 * (h % 12 == 0)) + ":" + m,
                h >= 12 ? 'PM' : 'AM');
            // $('#SiteMeetings_meeting_time').val(time_val);
            // $('<span>' + time_regex + '</span>').insertAfter('#SiteMeetings_meeting_time');
            $("#time_format").text(time_regex);
        });




    })
</script>