<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js','/js/jquery-1.12.4.js');?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js','/js/jquery-ui.js');?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/onedrive.js" type="text/javascript">
    </script>
<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    $model->name,
);

$this->menu = array(
    //    array('label' => 'List Projects', 'url' => array('index')),
    //    array('label' => 'Create Projects', 'url' => array('create')),
    //    array('label' => 'Update Projects', 'url' => array('update', 'id' => $model->pid)),
    //    array('label' => 'Delete Projects', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->pid), 'confirm' => 'Are you sure you want to delete this item?')),
    //    array('label' => 'Manage Projects', 'url' => array('admin')),
);
?>
<div class="view-project-sec">
<h1><?php echo $model->name; ?> #<?php echo $model->project_no; ?></h1>
<?php //echo $model->pid; 
?>

<div>
    <div class="media">
        <div class="media-left">
            <img height="160px" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/" . $model->img_path ?>" alt="...">
        </div>
        <div class="media-body">
            <div class="row">
                <div class="col-md-4 col-sec">
                    <?php
                    if (yii::app()->user->role == 1) {
                    ?>
                        <div><b>Client:</b><span><?php echo isset($model->client) ? $model->client->name :  "" ?></span></div>
                    <?php } ?>
                    <div><b>Start Date:</b><span><?php echo date("d-M-y", strtotime($model->start_date)) ?></span></div>
                    <div><b>End Date:</b><span><?= $model->end_date != '' ? date("d-M-y", strtotime($model->end_date)) : '' ?></span></div>
                    <div><b>Description:</b><span><?php echo $model->description ?></span></div>
                    <div><b>Total Square Feet:</b><span><?php echo $model->total_square_feet ?></span></div>
                </div>
                <div class="col-md-4 col-sec">
                    <div><b>Status:</b><span><?php echo $model->status0->caption ?></span></div>
                    <div><b>Billable:</b><span><?php echo $model->billable0->caption; ?></span></div>
                    <div><b>Budget:</b><span><?php echo  $model->budget ?></span></div>
                </div>
                <div class="col-md-4  col-sec">
                    <div><b>Assigned Project Managers:</b><span><?php echo $model->getassignees($model->assigned_to, $model); ?></span></div>
                    <div><b>Created On:</b><span><?php echo date('d-M-y', strtotime($model->created_date)) ?></span></div>
                    <div><b>Created By:</b><span><?php echo $model->createdBy->first_name . " " . $model->createdBy->last_name ?></span></div>
                </div>

                <div class="col-md-4  col-sec">
<!-- <div id="container" class="col-md-4"></div> -->
<button onClick="launchOneDrivePicker()">Open from OneDrive</button>




</div>
            </div>
        </div>
    </div>
</div>







<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        //'project_no',
        //'name',
        // array(
        //     'name'=>Clientsite::model()->getAttributeLabel('site_name'), //column header
        //     'value'=>$model->getclientsite($model->pid), //column name, php expression
        //     'type'=>'raw',              
        // ),
        // array(
        //     'name' => 'client_id',
        //     'value' => isset($model->client) ? $model->client->name :  "N/A",
        //     'type' => 'raw',
        //     'filter' => CHtml::listData(Clients::model()->findAll(
        //                     array(
        //                         'select' => array('cid,name'),
        //                         'order' => 'name',
        //                         'distinct' => true
        //             )), "cid", "name"),
        //     'type'  => 'raw',
        //     'visible'=>(yii::app()->user->role==1),
        // ),

        // array(
        //     'name' => 'billable',
        //     'type' => 'raw',
        //     'value' => $model->billable0->caption,
        // ),
        // array(
        //     'name' => 'assigned_to',
        //     'value' => $model->getassignees($model->assigned_to,$model),
        //     // 'type' => 'raw',           
        // ),
        // array(
        //     'name' => 'start_date',
        //     'value' => date("d-M-y",strtotime($model->start_date)),                        
        //     'type' => 'html',
        //     ),
        // array(
        //         'name' => 'end_date',
        //         'value' => date("d-M-y",strtotime($model->end_date)),                        
        //         'type' => 'html',
        //     ),

        // 'budget',
        // array(
        //     'name' => 'status',
        //     'type' => 'raw',
        //     'value' => $model->status0->caption,
        // ),

        // 'description',       
        // array(
        //     'name' => 'created_date',
        //     'type' => 'raw',
        //     'value' => date('d-M-y',strtotime($model->created_date)),
        // ),

        // array(
        //     'name' => 'created_by',
        //     'type' => 'raw',
        //     'value' => $model->createdBy->first_name." ".$model->createdBy->last_name,
        // ),
        // array(
        //     'name' => 'img_path',
        //     'type'=>'html',
        //     'value'=>CHtml::image(Yii::app()->baseUrl."/uploads/project/".$model->img_path,'image',array("style"=>"width:200px;height:100px")),

        // ),


    ),
));
?>
<br /><br />
<!--<h2><span> Payment Report</span></h2>-->
<hr />
<div class="row">
    <div class="col-md-6">
        <table cellpadding="3" cellspacing="0" class="timehours table-bordered">
            <thead>
                <tr class="tr_title">
                    <th width="150">Sl no</th>
                    <th width="550">MILESTONE TITLE</th>
                    <th width="200">START DATE</th>
                    <th width="200">END DATE</th>
                    <th width="400">STATUS</th>
                </tr>
            </thead>
            <?php
            if (!empty($milestones)) {
            ?>
                <?php
                $k = 1;

                foreach ($milestones as $milestone) {

                ?>
                    <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
                        <td width="150"><?php echo $k; ?></td>
                        <td width="550"><?php echo $milestone['milestone_title']; ?></td>
                        <td width="200"><?php echo date('d-M-y', strtotime($milestone['start_date'])); ?></td>
                        <td width="200"><?php echo date('d-M-y', strtotime($milestone['end_date'])); ?></td>
                        <td width="400"><?php echo $milestone['status'] == 1 ? 'Enable' : 'Disable'; ?></td>

                    </tr>


            <?php
                    $k++;
                }
            }
            ?>
        </table>
    </div>
    <div class="col-md-6">
        <table cellpadding="3" cellspacing="0" class="timehours  table-bordered">
            <thead>
                <tr class="tr_title">
                    <th width="200">Sl no</th>
                    <th width="550">SITE NAME</th>
                    <th width="200">LATITUDE</th>
                    <th width="200">LONGITUDE</th>
                    <th width="400">SITE ENGINEERS/SUPERVISORS</th>
                    <th width="400">Action</th>
                </tr>
            </thead>
            <?php
            if (!empty($sites)) {
            ?>
                <?php
                $k = 1;

                foreach ($sites as $site) {
                    $clients_in_site = ClientsiteAssign::model()->findAll(array('condition' => 'site_id = ' . $site->id));
                    $users = '';
                    if (!empty($site)) {
                        $users_array = array();
                        foreach ($clients_in_site as $client) {
                            array_push($users_array, $client->users->first_name);
                        }
                        $users = implode(',', $users_array);
                    }

                ?>
                    <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
                        <td width="200"><?php echo $k; ?></td>
                        <td width="550"><?php echo $site['site_name']; ?></td>
                        <td width="200"><?php echo $site['latitude']; ?></td>
                        <td width="200"><?php echo $site['longitude'];  ?></td>
                        <td width="400"><?php echo $users; ?></td>
                        <td width="400">
                            <?php
                            if (in_array("/projects/addmeeting", Yii::app()->session["menuauthlist"])) {
                            ?>
                                <a href="<?php echo Yii::app()->createUrl("projects/addmeeting", array('id' => $site->id)) ?>" target="_blank" title="meeting"><i class="fa fa-plus" aria-hidden="true"></i>
                                </a>
                            <?php
                            } else {
                                echo "";
                            }
                            ?>

                        </td>

                    </tr>


            <?php
                    $k++;
                }
            }
            ?>
        </table>
    </div>
</div>

        </div>


        <!-- Start Projem changes -->
        <div class="row">

        <div class="col-md-12">
        <table class="table table-striped table-bordered weekly-report-table">
<thead>
    <tr>
        <th colspan="13" align="center"><?php echo $model->name; ?></th>
    </tr>
<tr>
    <th rowspan="2" >Task id</th>
    <th rowspan="2">Description of work</th>
    <th rowspan="2">Owner</th>
    <th rowspan="2">Co-ordinator</th>
    <th rowspan="2">Priority</th>
    <th rowspan="2">Task duration</th>
    <th colspan="2">scheduled</th>
    <th colspan="2">Actual</th> 
    <th rowspan="2">Progress % </th>
    <th rowspan="2">Remarks</th>
    <th rowspan="2">Remarks</th> 
    
  </tr>

  <tr>
 

    
  <th>Start date</th>
    <th>End date</th>

      
  <th>Start date</th>
    <th>End date</th>
  
    
  </tr>

  
      
</thead>

<tbody>
    <tr>

    <td colspan="13" class="pink-bg">Jitha</td>

    <tr>
        <td></td>
        <td>Carpentry -milestone</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
    </tr>
    <tr>
        <td></td>
        <td>Main task</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
    </tr>
    <tr>
        <td></td>
        <td>Sub task</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
    </tr>
   
  </tr>

  <tr>
        <td></td>
        <td>Painting -Milestone</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>

    </tr>
    <tr>
        <td></td>
        <td>Wall painting -Main task</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
    </tr>
    <tr>
        <td></td>
        <td>4 wall painting -Sub task</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        
    </tr>

    <tr>
        <td></td>
        <td>second sub task -Sub task</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        
    </tr>
   
  </tr>

  <tr>

    <td colspan="13" class="pink-bg">Jesna</td>

    <tr>

    <tr>
        <td></td>
        <td>Tiling -milestone</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
    </tr>

    <tr>
        <td></td>
        <td>1 st floor tiling -Main task</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
    </tr>

    <tr>
        <td></td>
        <td>floral tiling -sub task</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
        <td>kk</td>
    </tr>
  
        </tbody>
        </div>

        </div>
        <!-- End Projem change -->

        <?php
    $settings = GeneralSettings::model()->findByPk(1);
    ?>
    <!-- <script type="text/javascript" src="https://js.live.net/v7.2/OneDrive.js"></script> -->
<script type="text/javascript">
  function launchOneDrivePicker(){
    var  currentLocation = window.location;

    var onedrive_client_id="<?= isset($settings->onedrive_clientId) ? $settings->onedrive_clientId : '' ?>";
    
 
    var odOptions = {
    clientId: onedrive_client_id,// add app client id here
    action: "query",
    multiSelect: true,
    viewType : "all",
    openInNewWindow: true,
    advanced: {
        // redirectUri:"https://costmspms.bhiapp.net/pms/index.php?r=projects/addproject",
        redirectUri:"<?php echo Yii::app()->createAbsoluteUrl('projects/addproject', array(), 'https') ?>",
        endpointHint: "api.onedrive.com",
  },

    success: function(files) {
        console.log(JSON.stringify(files));
        var files_array = files.value;
        if(files_array) {
            for(var i in files_array) {
                //window.processOneDriveFile(files_array[i]);
            }
        }
    },
    cancel: function() { /* cancel handler */ },
    error: function(error) {
        console.log(data);
        console.debug(data);
        console.log("error");
     }
    }
    OneDrive.open(odOptions);
  }

  function processOneDriveFile(file) {
    var file_name = file.name;
    var file_size = file.size;
    var download_url = file['@microsoft.graph.downloadUrl'];
    var data = {
        file_name : file_name,
        file_size : file_size,
        download_url : download_url,
        command : 'handle-onedrive-file',
    };console.log(data);

    $.ajax({
        url: 'file_handler.php',
        type: 'post',
        data: data,
        error: function (data) {
            console.log(data);
            console.debug(data);
            console.log("error");
        },
        success: function (data) {
            console.log(data);
            console.log("success");
        }
    });
}



</script>