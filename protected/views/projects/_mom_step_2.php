<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css" />

<style>
    .handsontable td {
        vertical-align: middle;
        /* Adjust alignment as needed */
    }

    .add-task-modal {
        z-index: 1050 !important;
        /* Ensure the modal appears above other elements */
    }

    .modal-backdrop {
        z-index: 1040;
        /* Ensure the modal backdrop appears below the modal */
    }

    /* Custom dropdown style */
    .custom-dropdown {
        width: calc(100% - 10px);
        /* Adjust the width to fit the cell */
        padding: 5px;
        background-color: transparent;
        appearance: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        border: none;
        outline: none;
        font-size: 14px;
        color: #333;
    }

    /* Arrow icon for the select */
    .custom-dropdown::after {
        content: '\25BC';
        /* Unicode for down arrow */
        position: absolute;
        right: 5px;
        top: 50%;
        transform: translateY(-50%);
        pointer-events: none;
    }

    .custom-multi-select {
        width: calc(100% - 10px);
        /* Adjust the width to fit the cell */
        padding: 5px;
        background-color: transparent;
        border: none;
        outline: none;
        font-size: 14px;
        color: #333;
    }
</style>


<?php
$contractor_list = Contractors::model()->findAll(array('condition' => 'status = 1'));
?>
<?php
$task_model = new Tasks();
$location = [];
?>
<div class="meeting-step-two-project-sec">
    <div class="row">
        <div class="subrow subrowlong">
            <div class="task_section display-none">
                <?php
                // echo $this->renderPartial('_task_form', array(
                //     'model' => $task_model,
                //     'location' => $location,
                //     'meeting_project_id' => $meeting_model->project_id
                // )); 
                ?>


            </div>
        </div>
    </div>




    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'meeting-minutes-form',
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'beforeValidate' => new CJavaScriptExpression('function(form) {
                for(var instanceName in CKEDITOR.instances) { 
                    CKEDITOR.instances[instanceName].updateElement();
                }
                return true;
            }'),
                'afterValidate' => 'js:function(form, data, hasError) {
                if(hasError) { 
                    console.log(hasError);                                                           
                }
                else
                {
                // return true;  it will submit default way
                minutes_form(form, data, hasError); //and it will submit by ajax function
                   }
               }',
            ),
        )
    );

    $condition = 'status = 1 ';
    if (!empty($meeting_model->project_id)) {
        $condition .= 'AND project_id = ' . $meeting_model->project_id;
    }

    ?>


    <div class='minutes_data '>

        <div class="row d-none">
            <div class="col-md-3">
                <?php
                if ($model->isNewRecord) {
                    if (isset(Yii::app()->user->site_meeting_id)) {
                        $model->meeting_id = Yii::app()->user->site_meeting_id;
                    }
                }
                ?>
                <?php echo $form->labelEx($model, 'milestone'); ?></label>
                <?php echo $form->dropDownList($model, 'milestone_id', CHtml::listData(Milestone::model()->findAll(
                    array(
                        'condition' => $condition,
                        'order' => 'milestone_title ASC'
                    )
                ), 'id', 'milestone_title'), array('empty' => 'Select Milestone', 'class' => 'form-control test')); ?>
                <?php echo $form->error($model, 'milestone_id'); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'title'); ?>
                <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'title'); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'start_date'); ?>
                <?php echo $form->textField($model, 'start_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($model, 'start_date'); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'end_date'); ?>
                <?php echo $form->textField($model, 'end_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($model, 'end_date'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input name="MeetingMinutes[meeting_id]" id="MeetingMinutes_meeting_id" type="hidden"
                    value="<?php echo $meeting_model->id ?>">
                <!-- <input name="MeetingMinutes[meeting_minutes_id]" id="MeetingMinutes_meeting_minutes_id" type="hidden"
                    value="<?php //echo $model->id                                  ?>"> -->
                <input name="MeetingMinutes[approve_status]" id="MeetingMinutes_approve_status" type="hidden"
                    value="<?php echo $meeting_model->approved_status ?>">
                <input name="MeetingMinutes[project_id]" id="MeetingMinutes_meeting_project_id" type="hidden"
                    value="<?php echo $meeting_model->project_id ?>">
                <?php //echo $form->labelEx($model, 'points_discussed');                                  ?>
                <?php //echo $form->textArea($model, 'points_discussed', array('class' => 'form-control'));                                  ?>
                <?php //echo $form->error($model, 'points_discussed');                                  ?>
            </div>

        </div>
        <div class="row ">

            <div class="col-md-12">

                <div id="error-message" class="alert alert-danger" style="display: none;"></div>
            </div>
            <div class="col-md-12">

                <!-- handson div -->
                <div id="points-table" class="width-100-percentage"></div>
                <br>

            </div>
        </div>
        <div class="row ">

            <!-- <br><br><br> -->
            <div class="col-md-3 display-none1 margin-top-18" id="task_div">
                <!-- <a href="#" class="new_task">Add Task</a> -->
                <?php //echo CHtml::link('Add  Task', array('tasks/createTask'));                                  ?>
            </div>
            <input type="hidden" name="submit_type" id="submit_type">
        </div>
    </div>
    <div class="row text-right">
        <div class="col-sm-12">
            <?php echo CHtml::SubmitButton('Next', array('class' => 'btn blue save_btn margin-left-30', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();')); ?>
            <?php //echo CHtml::SubmitButton('Add New', array('class' => 'btn blue save_btn margin-left-30', 'id' => 'add_new', 'name' => 'save_btn'), array('onclick' => 'return validation();'));                ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <div class="row d-none">
        <div class="col-md-12">
            <?php


            // if (!$model->isNewRecord) {
            $this->widget(
                'zii.widgets.grid.CGridView',
                array(
                    'id' => 'meeting-minutes-grid',
                    'dataProvider' => $model->search(),
                    // 'filter' =>$task,
                    'itemsCssClass' => 'table table-bordered',
                    'template' => '<div class="table-responsive">{items}</div>',
                    'pager' => array(
                        'id' => 'dataTables-example_paginate',
                        'header' => '',
                        'prevPageLabel' => 'Previous ',
                        'nextPageLabel' => 'Next '
                    ),
                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                    'columns' => array(
                        array('class' => 'IndexColumn', 'header' => 'S.No.'),
                        // 'tskid',
                        array(
                            'name' => 'title',
                            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->tskid')
                        ),
                        array(
                            'name' => 'milestone_id',
                            'value' => '($data->milestone_id===null?"": $data->milestone->milestone_title)',
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'start_date',
                            'value' => 'date("d-M-y",strtotime($data->start_date))',
                            'type' => 'html',
                            'htmlOptions' => array('class' => 'date_start')
                        ),
                        array(
                            'name' => 'due_date',
                            'value' => 'date("d-M-y",strtotime($data->end_date))',
                            'type' => 'html',
                            'htmlOptions' => array('class' => 'date_end'),

                        ),
                        array(
                            'class' => 'CButtonColumn',
                            'template' => isset(Yii::app()->user->role) ? '{update}' : '',
                            'buttons' => array(
                                'update' => array(
                                    'label' => '',
                                    'imageUrl' => false,
                                    'click' => 'function(e){e.preventDefault();edittask($(this).attr("href"));}',
                                    'options' => array('class' => 'update_task actionitem updateicon icon-pencil icon-comn', 'title' => 'Edit'),
                                ),
                            ),
                        ),
                    ),
                )
            );
            // }
            ?>
        </div>
    </div>
    <?php $status = json_encode(Controller::meetingStatus($key = false), true);

    $users = Users::model()->findAll();
    $users_array = array();
    foreach ($users as $user) {
        $users_array[] = array(
            'userid' => $user->userid,
            'full_name' => $user->first_name . ' ' . $user->last_name,
            'user_type' => $user->user_type,
        );
    }

    ?>
    <?php echo $this->renderPartial('_add_task_modal', array('condition' => $condition, 'data' => array())); ?>
</div>

<script type="text/javascript">
    $('.save_btn').click(function () {
        var buttonClickedID = this.id;
        $('#submit_type').val(buttonClickedID);
    });
    // CKEDITOR.replace('MeetingMinutes_points_discussed');
    $("#MeetingMinutes_submission_date").datepicker({
        dateFormat: 'dd-M-yy',
    });
    $("#MeetingMinutes_start_date").datepicker({
        dateFormat: 'dd-M-yy',
    });
    $("#MeetingMinutes_end_date").datepicker({
        dateFormat: 'dd-M-yy',
    });

    // function addminutes(){
    //         var data = $('.minutes_data').html();            
    //         var newdiv = document.createElement('div');
    //         newdiv.className = 'minutes';
    //         newdiv.innerHTML = data;
    //         $(newdiv).insertBefore( "#add_minute" );                                   
    // }
    $('#MeetingMinutes_milestone_id').change(function () {
        var milestone_id = this.value;
        if (milestone_id !== '') {
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('masters/milestone/getmilestonedates'); ?>",
                "data": {
                    id: milestone_id
                },
                "dataType": "json",
                success: function (result) {
                    console.log(result);
                    if (result.status == 1) {
                        $('#MeetingMinutes_start_date').val(result.start);
                        $('#MeetingMinutes_end_date').val(result.end);

                    }
                }
            });
        }
    })

    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
    }

    function edittask(href) {
        var id = getURLParameter(href, 'id');

        $.ajax({
            "type": 'post',
            "url": "<?php echo Yii::app()->createUrl('meetingMinutes/getmodel'); ?>",
            "data": {
                id: id
            },
            "dataType": "json",
            success: function (result) {
                if (result.stat == 1) {
                    $('#MeetingMinutes_meeting_minutes_id').val(result.data.id)
                    $('#MeetingMinutes_milestone_id').val(result.data.milestone_id);
                    $('#MeetingMinutes_meeting_id').val(result.data.meeting_id);
                    $('#MeetingMinutes_title').val(result.data.title);
                    $('#MeetingMinutes_start_date').val(result.data.start_date);
                    $('#MeetingMinutes_end_date').val(result.data.end_date);
                    // CKEDITOR.instances['MeetingMinutes_points_discussed'].setData(result.data.points_discussed);
                    var userss = result.users.split(',');

                    // $('.action_by_val').val(userss);
                    // $('.action_by_val').trigger('change');
                    // $('#MeetingMinutes_meeting_minutes_status').val(result.data.meeting_minutes_status);

                }
            }
        });
    }


    $(document).ready(function (e) {
        var meeting_status = $('#MeetingMinutes_approve_status').val();
        if (meeting_status == 1) {
            $('#task_div').show();
        }

        $(".new_task").click(function () {
            $('.task_section').show();

            $(".close-panel").click(function () {
                $(".task_section").hide();
            })

        });
        var project_id = $('#MeetingMinutes_meeting_project_id').val();
        $("#Tasks_project_id").val(project_id);
        $('#closeButton').on('click', function (e) {
            $(".task_section").hide();
        });

        $('#MeetingMinutes_title').on('change.bootstrapSwitch', function (e) {
            //    var meeting_id=$('#MeetingMinutes_meeting_id').val();
            var meeting_id = 545;

            if (meeting_id != '') {
                $.fn.yiiGridView.update('meeting-minutes-grid', {
                    data: {
                        site_meeting_id: meeting_id
                    },
                });


            } else {
                $.fn.yiiGridView.update('meeting-minutes-grid', {
                    data: {
                        site_meeting_id: ""
                    },
                });
            }
        });
    });

    var rowIndex;
    const statusOptions = JSON.parse('<?= $status ?>');
    const usersOptions = JSON.parse('<?= json_encode($users_array) ?>');
    const header_data = ['Points Discussed', 'Status', 'Owner', 'Due Date'];
    const autosave = false;
    var ss = '<?php echo $body_array; ?>';
    var dd = JSON.parse(ss);
    const container = document.getElementById('points-table');
    const hot = new Handsontable(container, {
        data: Handsontable.helper.createSpreadsheetData(),
        data: dd,
        hiddenColumns: {
            columns: [4] // Specify the index of the column you want to hide (0-based index)
        },
        startRows: 5,
        startCols: 12,
        rowHeaders: true,
        colHeaders: header_data,
        width: 'auto',
        height: 'auto',
        minSpareRows: 3,
        minSpareColumns: 3,
        customBorders: true,
        stretchH: 'all',
        licenseKey: 'non-commercial-and-evaluation',
        afterChange: function (change, source) {
            if (source === 'loadData') {
                return; //don't save this change
            }
            if (!autosave.checked) {
                return;
            }
            // });
            // clearTimeout(autosaveNotification);            
        },

        cells: function (row, col) {
            const cellProperties = {};
            if (col === 0) {
                cellProperties.width = 250;
            }
            if (col === 1) {
                cellProperties.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                    const select = document.createElement('select');
                    select.classList.add('custom-dropdown');
                    for (const key in statusOptions) {
                        if (statusOptions.hasOwnProperty(key)) {
                            const value = statusOptions[key];
                            const option = document.createElement('option');
                            option.text = value;
                            option.value = key;
                            select.add(option);
                        }
                    }
                    select.value = value;
                    select.addEventListener('change', function () {
                        hot.setDataAtCell(row, col, this.value); // Update the cell value
                    });
                    Handsontable.dom.empty(td);
                    td.appendChild(select);
                };
                cellProperties.width = 100;
                cellProperties.editor = false; // Disable built-in editor
            }
            if (col === 2) {
                cellProperties.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                    const select = document.createElement('select');
                    select.classList.add('custom-dropdown');
                    usersOptions.forEach(function (item) {
                        const value = item.user_id;
                        const option = document.createElement('option');
                        option.text = item.full_name;
                        option.value = item.userid;
                        select.add(option);
                    })
                    select.value = value;
                    select.addEventListener('change', function () {
                        hot.setDataAtCell(row, col, this.value); // Update the cell value
                    });
                    Handsontable.dom.empty(td);
                    td.appendChild(select);
                };

                cellProperties.editor = false; // Disable built-in editor
            }
            if (col === 3) {
                cellProperties.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                    Handsontable.renderers.TextRenderer.apply(this, arguments);
                };

                cellProperties.editor = 'date'; // Use the built-in date editor
                cellProperties.dateFormat = 'YYYY-MM-DD'; // Specify the expected date format
                cellProperties.allowEmpty = true; // Allow empty values if input is not a valid date
            }
            if (col === 5) { // Add button in column 4
                cellProperties.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                    if (!value) { // Check if the cell value is empty
                        const button = document.createElement('button');
                        button.textContent = 'Add task';
                        button.classList.add('add-task-button');
                        button.setAttribute('type', 'button');
                        // button.setAttribute('data-toggle', 'modal');
                        button.setAttribute('data-target', '#addTask');
                        button.style.display = 'block';
                        button.style.margin = 'auto';
                        button.style.background = '#17a2b8';
                        button.style.color = 'white';
                        button.style.border = 'none';

                        Handsontable.dom.empty(td);
                        td.appendChild(button);

                    } else {
                        td.textContent = ' ';
                    }
                    // cellProperties.readOnly = true;

                };
            }

            return cellProperties;
        }
    });
    Handsontable.hooks.add('afterOnCellMouseDown', function (event, coords, TD) {
        var hot = this;
        var cellProperties = hot.getCellMeta(coords.row, coords.col);

        if (cellProperties.editor === 'date') {
            hot.selectCell(coords.row, coords.col);
            hot.getActiveEditor().beginEditing();
            event.preventDefault(); // Prevent default mouse down behavior
        }
    });

    hot.rootElement.addEventListener('click', function (event) {
        if (event.target.classList.contains('add-task-button')) {
            event.preventDefault();
            rowIndex = event.target.parentElement.parentElement.rowIndex; // Get visual row index directly
            const rowData = hot.getSourceDataAtRow((rowIndex - 1)); // Get source data of the clicked row
            var result = validateRow(rowData);
            if (result) {
                var button = event.target;
                button.setAttribute('data-toggle', 'modal');
                openModalWithRowData(rowData)
            }

        }
    });


    function validateRow(rowData) {
        var errors = "";
        var date_error = "";

        for (var i = 0; i < rowData.length; i++) {
            if (i !== 4 && i !== 5) {
                if (rowData[i] === "" || rowData[i] === null) {
                    errors += header_data[i] + ",";
                }
                if (i == 3 && rowData[i] != "" && rowData[i] != null && !isValidDateInAnyFormat(rowData[i])) {
                    date_error = header_data[i] + " in invalid format."
                }
            }
        }
        var result;
        var alertBox = document.getElementById("error-message");

        if (errors !== "") {
            errors = errors.slice(0, -1);
            alertBox.innerText = errors + " are empty in row ." + date_error + " \n";;
            alertBox.style.display = "block";
            result = false;

        } else if (date_error !== "") {
            alertBox.innerText = date_error + " \n";;
            alertBox.style.display = "block";
            result = false;
        } else {
            alertBox.style.display = "none";
            result = true;
        }
        return result;
    }
    function openModalWithRowData(rowData) {
        $('#add_task_form')[0].reset();
        $('#row_data').val(rowData);
        $('#report_to').val(rowData[2]);
        var project_id = $('#SiteMeetings_project_id').val();
        var meeting_id = $('#MeetingMinutes_meeting_id').val();
        $('#project_id').val(project_id);
        $('#meeting_id').val(meeting_id);
        $('#due_date').val(rowData[3]);

    }

</script>