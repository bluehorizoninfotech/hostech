<?php
Yii::app()->getModule('masters');
?>
<script src="<?php echo $this->customAssets('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', '/js/jquery-1.9.1-jquery.min.js'); ?>"></script>
<?php
/* @var $this ProjectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);
if (isset($project_id)) {
    $project_id = $project_id;
} else {
    $project_model = Projects::model()->find(['condition' => 'status = 1 ORDER BY updated_date DESC']);
    if (Yii::app()->user->project_id != "") {
        $project_id = Yii::app()->user->project_id;
    } else {
        $project_id = $project_model['pid'];
    }
}
if (isset($selected_date)) {
    $selected_date = $selected_date;
} else {
    $selected_date = date('d-M-Y');
}
if (isset($schedule_type)) {
    if ($schedule_type == 1) {
        $selected_val = "1";
    } elseif ($schedule_type == 2) {
        $selected_val = "2";
    } elseif ($schedule_type == 3) {
        $selected_val = "3";
    }
} else {
    $selected_val = "2";
}
?>
<div class="schedule-sec">
    <div class="padding-bottom-30">

    </div>
    <div class="clearfix">
        <div class="pull-right">

        </div>
        <h1>Project Schedule</h1>
    </div>
    <div class="view">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'schedule_form',
            'method' => 'get',
            'action' => Yii::app()->createUrl('/Projects/schedules'),

        )); ?>
        <div class="form-group display-flex align-items-center">

            <?= CHtml::dropDownList(
                'project_id',
                $project_id,
                CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
                array(
                    'empty' => 'Choose a project', 'class' => 'form-control change_project dropdown_style',
                    'id' => 'project_id'
                )
            );
            ?>

            <?php
            $type = isset($_GET['type']) ? $_GET['type'] : "";
            ?>
            <label class="radio-button">
                <input type="radio" name="type" value="1" <?php if ($type == 1) : ?> checked="checked" <?php endif ?>> Week-wise
            </label>
            <label class="radio-button">
                <input type="radio" name="type" value="2" <?php if ($type == 2) : ?> checked="checked" <?php endif ?>> Day-wise
            </label>

            <?php echo CHtml::submitButton('Submit', array('class' => 'save_btn btn blue btn-sm')); ?>


            <!-- <a id="refresh" title="refresh"><i class="fa fa-refresh" aria-hidden="true"></i></a> -->
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div>
        <?php
        if (!empty($tasks)) {

            if ($type == 1) {
                echo $this->renderPartial('weekly_schedule', array('tasks' => $tasks, 'selected_date' => $selected_date,  'start_date' => $start_date, 'end_date' => $end_date, 'project' => $project));
            } else {
                echo $this->renderPartial('daily_schedule', array('tasks' => $tasks, 'selected_date' => $selected_date,  'start_date' => $start_date, 'end_date' => $end_date, 'project' => $project));
            }
        } else {
            echo '<h3>No Result Found</h3>';
        }

        ?>

    </div>
</div>
<script>
    $(function() {
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd-M-y',
        });
        $('#refresh').click(function() {
            $('form#schedule_form').submit();
        });

    });
</script>