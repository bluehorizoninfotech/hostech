<div class="modal fade add-task-modal" id="addTask" tabindex="5" role="dialog" aria-labelledby="modalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modalLabel">Add Task</h4>
            </div>
            <form action="<?= $this->createUrl('tasks/addMainTask') ?>" method="post" id="add_task_form">
                <div class="modal-body" style="padding:10px;">
                    <div class="row">
                        <div class="Col-sm-6">
                            <div class="form-group">
                                <label for="milestone">Milestone <span class=" ">*</span></label>
                                <?php
                                $milestones = Milestone::model()->findAll(
                                    array(
                                        'select' => 'id, milestone_title',
                                        'condition' => $condition,
                                        'order' => 'milestone_title ASC'
                                    )
                                );
                                ?>
                                <?php if (empty($data)) { ?>
                                    <input type="hidden" name="Tasks[report_to]" id="report_to">
                                    <input type="hidden" name="Tasks[project_id]" id="project_id">
                                    <input type="hidden" name="meeting_id" id="meeting_id">
                                    <input type="hidden" name="row_data" id="row_data">
                                <?php } else { ?>
                                    <input type="hidden" name="Tasks[report_to]" id="report_to"
                                        value="<?= $data['reprted_to'] ?>">
                                    <input type="hidden" name="Tasks[project_id]" id="project_id"
                                        value="<?= $data['project_id'] ?>">
                                    <input type="hidden" name="meeting_id" id="meeting_id"
                                        value="<?= $data['meeting_id'] ?>">
                                    <input type="hidden" name="row_data" id="row_data" value="<?= $data['row_data']; ?>">
                                    <input type="hidden" name="" id="meeting_minutes" value="1">
                                <?php } ?>
                                <select id="MeetingMinutes_milestone" name="Tasks[milestone_id]" class="form-control">
                                    <option value="">Select Milestone</option>
                                    <?php foreach ($milestones as $milestone) { ?>
                                        <option value="<?= $milestone->id; ?>">
                                            <?= $milestone->milestone_title; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <div class="errorMessage" id="milestone_em_" style="display:none;">
                                    Milestone cannot be blank.
                                </div>
                            </div>
                        </div>
                        <div class="Col-sm-6">
                            <div class="form-group">
                                <label for="title">Title <span class=" ">*</span></label>
                                <input class="form-control" name="Tasks[title]" id="title" type="text" maxlength="100">
                                <div class="errorMessage" id="title_em_" style="display:none;">
                                    Title cannot be blank.
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="Col-sm-6">
                            <div class="form-group">
                                <label for="start_date">Start Date <span class=" ">*</span></label>
                                <input type="date" name="Tasks[start_date]" id="start_date" class="form-control"
                                    autocomplete="off">

                                <div class="errorMessage" id="start_date_em_" style="display:none;">
                                    Start Date cannot be blank.
                                </div>
                            </div>
                        </div>
                        <div class="Col-sm-6">
                            <div class="form-group">
                                <label for="start_date">End Date <span class=" ">*</span></label>
                                <input type="date" name="Tasks[due_date]" id="due_date" class="form-control"
                                    autocomplete="off" value=<?= isset($data['row_data'][3]) ? date('Y-m-d', strtotime($data['row_data'][3])) : ''; ?>>

                                <div class="errorMessage" id="due_date_em_" style="display:none;">
                                    Due Date cannot be blank.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="submit_type" id="submit_type_task" value="save_minutes_task">
                    <button type="submit" class="btn btn-primary create_task">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // Attach event handler to the form submit event
        var meeting_minutes_id = "";
        var task_title = "";
        $('#add_task_form').submit(function (event) {
            event.preventDefault(); // Prevent the default form submission
            var formData = new FormData($("#add_task_form")[0]);
            checkTaskExist(function (result) {
                if (result.status == 2) {
                    console.log(result);
                    return false;
                }
            });
            $(".errorMessage").hide();
            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('tasks/addMainTask'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function (data) {
                    if (data.status == 1) {
                        formData.append('Tasks[task_id]', data.task_id);
                        storeMinutesData(formData);
                    } else if (data.status == 2) {
                        var error = data.error;
                        var errorMessage = "";
                        if (error.hasOwnProperty('title')) {
                            errorMessage = (error['title'].length > 1) ? error['title'][1] : error['title'][0];
                            $("#title_em_").empty().text(errorMessage).show();
                        }
                        if (error.hasOwnProperty('milestone_id')) {
                            errorMessage = (error['milestone_id'].length > 1) ? error['milestone_id'][1] : error['milestone_id'][0];
                            $("#milestone_em_").empty().text(errorMessage).show();
                        }
                        if (error.hasOwnProperty('start_date')) {
                            errorMessage = (error['start_date'].length > 1) ? error['start_date'][1] : error['start_date'][0];
                            $("#start_date_em_").empty().text(errorMessage).show();
                        }
                        if (error.hasOwnProperty('due_date')) {
                            errorMessage = (error['due_date'].length > 1) ? error['due_date'][1] : error['due_date'][0];
                            $("#due_date_em_").empty().text(errorMessage).show();
                        }
                    }

                }
            });

        });

        function storeMinutesData(formData) {
            $.ajax({
                "type": "POST",
                "url": "<?php echo Yii::app()->createUrl('meetingMinutes/savemeetingMinutesData'); ?>",
                "dataType": "json",
                "data": formData,
                "cache": false,
                "contentType": false,
                "processData": false,
                "success": function (data) {
                    if (data.status == 1) {
                        if (!$('#meeting_minutes').val()) {
                            hot.setDataAtCell((rowIndex - 1), 5, '78');
                            // Get the cell element
                            var cell = hot.getCell((rowIndex - 1), 5);
                            // Try to remove the button element from the cell
                            var button = cell.querySelector('button.add-task-button');
                            if (button) {
                                button.parentNode.removeChild(button);
                            }
                            hot.setDataAtCell((rowIndex - 1), 4, data.meeting_minutes_id);
                            hot.setDataAtCell((rowIndex - 1), 3, data.due_date);
                            hot.render();
                        } else {
                            $(".add_task_modal[data-id='" + meeting_minutes_id + "']").replaceWith($('#title').val());

                        }
                        $('#add_task_form')[0].reset();
                        $('.add-task-modal').hide();
                    } else if (data.status == 2) {
                        // $("#error_message").fadeIn().delay(1000).fadeOut();
                    }

                }
            });
        }
        function checkTaskExist(callback) {
            var data = $('#row_data').val();
            var parts = data.split(',');
            if (parts[4]) {
                meeting_minutes_id=parts[4];
                $.ajax({
                    "type": 'get',
                    "url": "<?php echo Yii::app()->createUrl('meetingMinutes/checkTaskExist'); ?>",
                    "data": { meeting_minutes_id: parts[4] },
                    "dataType": "json",
                    success: function (result) {
                       
                        callback(result);
                    }
                });
            }

        }
    function dateFormat(date) {
        var dateParts = date.split('-'); // Split the date string into parts
        var year = parseInt(dateParts[2]) + 2000; // Extract year and convert it to full year format
        var month = new Date(Date.parse(dateParts[1] + " 1, 2000")).getMonth() + 1; // Convert month abbreviation to month number
        var day = dateParts[0]; // Extract day
        month = (month < 10) ? "0" + month : month;
        return year + "-" + month + "-" + day;

    }
    $('#MeetingMinutes_milestone').change(function () {
        var milestone_id = this.value;
        if (milestone_id !== '') {
            $.ajax({
                "type": 'post',
                "url": "<?php echo Yii::app()->createUrl('masters/milestone/getmilestonedates'); ?>",
                "data": {
                    id: milestone_id
                },
                "dataType": "json",
                success: function (result) {
                    if (result.status == 1) {
                        $('#start_date').val(dateFormat(result.start));
                        $('#due_date').val(dateFormat(result.end));
                    }
                }
            });
        }
    })
    });


</script>