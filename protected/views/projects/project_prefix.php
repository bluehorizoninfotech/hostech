<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<?php
/* @var $this DepartmentController */
/* @var $model Department */
Yii::app()->clientScript->registerScript('search', "
");
?>


<div class="half-table">
    <div class="clearfix">
     
    <h1>Manage Prefix</h1>
</div>

<div id="vendor_form" data-toggle="modal" data-target="#reviewLoginModal">

</div>
<!-- html tsbsle -->
<table class="table table-bordered">
<thead>
<tr>
<th id="department-grid_c0">S.No.</th>
<th id="department-grid_c1"><a class="sort-link" href="/ashly-pms/index.php?r=department/admin&amp;Department_sort=department_name">Department Name</a></th>
<th class="button-column" id="department-grid_c2">&nbsp;</th></tr>
<tr class="filters">

</tr>
</thead>
<tbody>

<tr>
    <td>f</td>
    <td><?php echo $data['prefix']?></td>
    <td>
    <a class="actionitem updateicon icon-pencil icon-comn" id="<?php echo $data['id'] ?>" title="Edit"></a>
    </td>
</tr>

</tbody>

</table>
</div>


<script>
    
    $(".updateicon").click(function() {
        var id = $(this).attr('id');

        $.ajax({
                    'url': '<?php echo Yii::app()->createAbsoluteUrl('projects/addProjectPrefix'); ?>',
                    data: {
                        id:id


                    },
                    dataType: 'Json',
                    type: 'POST',
                    success: function(result) {
                        $("#vendor_form").html(result).dialog();  
                    }
                })
        
    })

    

  
</script>