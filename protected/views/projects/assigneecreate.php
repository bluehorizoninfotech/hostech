<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
$cs = Yii::app()->clientScript;
$cs->scriptMap = array(
    'jquery.js' => false,
    'jquery.min.js' => false,
);
?>

<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Projects', 'url'=>array('index')),
	array('label'=>'Manage Projects', 'url'=>array('admin')),
);
?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">New Project</h4>
        </div>


<?php echo $this->renderPartial('_formassignee', array('model'=>$model,'button'=>'submit')); ?>

</div>

</div>
<script>
$(document).ready(function () {
    $( "#Projects_start_date" ).datepicker({
        dateFormat: 'dd-M-yy',
        maxDate: $("#Projects_end_date").val()
    });
    $( "#Projects_end_date" ).datepicker({
        dateFormat: 'dd-M-yy',
    });
    $("#Projects_start_date").change(function() {
        $("#Projects_end_date").datepicker('option', 'minDate', $(this).val());
    });
    $("#Projects_end_date").change(function() {
        $("#Projects_start_date").datepicker('option', 'maxDate', $(this).val());
    });
});
</script>