<script src="<?php echo $this->customAssets('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', '/js/jquery.min.js'); ?>"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
<?php
if (isset($_GET['id'])) {
    $project_id = $_REQUEST['id'];
} else {
    $project_model = Projects::model()->find(['condition' => 'status = 1 ORDER BY updated_date DESC']);
    if (Yii::app()->user->project_id != "") {
        $project_id = Yii::app()->user->project_id;
    } else {
        // $project_id = $project_model['pid'];
        $project_id = '';
    }
}

?>

<div class="chart-section">
    <div class="padding-bottom-30">
        <?= CHtml::dropDownList(
            'project_id',
            $project_id,
            CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'),
            array(
                'empty' => 'Choose a project', 'class' => 'form-control change_project width-25-percentage pull-right',
                'id' => 'project_id'
            )
        ); ?>
    </div>
    <div class="main_content">
        <div class="clearfix page_head">
            <h1></h1>
           
        </div>
        <?php if (Yii::app()->user->hasFlash('error')) : ?>
            <div class="alert alert-danger alert-dismissable ">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>


        <?= CHtml::link('Refresh', '', array('class' => 'btn blue right refresh', 'id' => 'refresh_btn')); ?>
        <?= CHtml::link('PDF', $url = $this->createAbsoluteUrl('/projects/downloadpdf', array('id' => '')), array('class' => 'btn blue right refresh', 'id' => 'download_id')) ?>
        <?= CHtml::link('Excel', $url = $this->createAbsoluteUrl('/projects/downloadexcel', array('id' => '')), array('class' => 'btn blue right refresh', 'id' => 'download_excel')) ?>

        <!-- <span class="pull-right span_style">
    On schedule</span>
    <div class="foo in_progress"></div>
    <span class="pull-right span_style">Behind schedule</span>
    <div class="foo delay"></div> -->

        <?php
        $type = isset($_GET['type']) ? $_GET['type'] : "";

        ?>
        <div class="pull-right span_style">
            <label><input type="radio" name="type" value="1" <?php if ($type == 1) : ?> checked="checked" <?php endif ?>> On Schedule</label>
            <label><input type="radio" name="type" value="2" <?php if ($type == 2) : ?> checked="checked" <?php endif ?>> Behind Schedule</label>
        </div>
        <div class="row-fluid clearfix">
            <div id="calendertable"></div>

        </div>

        <div id="task_data" class="margin_div clearfix">
        </div>

    </div>

</div>



<script type="text/javascript">
    $(document).ready(function() {
        var pid = $('#project_id').val();
        var url = document.location.href;
        var type = getParameterFromURL(url, "type");
        if (!type) {
            type = "";
        }

        var url = "<?php echo $this->createUrl('projects/downloadexcel') ?>";

        url += '&id=' + pid + '&type=' + type;

        $("#download_excel").attr("href", url);
    });

    reloadcalender();
    $('#refresh_btn').click(function() {
        reloadcalender();
    });
    $('#download_id').click(function() {

        var pid = $('#project_id').val();
        var url = document.location.href;
        var type = getParameterFromURL(url, "type");
        if (!type) {
            type = "";
        }

        var url = "<?php echo $this->createUrl('projects/downloadpdf') ?>";

        url += '&id=' + pid + '&type=' + type;

        $("#download_id").attr("href", url);

    });
    $('#project_id').change(function() {
        //  $('.loaderdiv').show();
        var pid = $('#project_id').val();
        var url = document.location.href;
        var type = getParameterFromURL(url, "type");
        if (!type) {
            type = "";
        }

        var id = 'id';
        var data = updateQueryStringParameter(url, id, pid, type);
        document.location = data;
        reloadcalender();
    });

    function getParameterFromURL(url, parameterName) {
        var parsedUrl = new URL(url);
        var searchParams = parsedUrl.searchParams;
        return searchParams.get(parameterName);
    }


    function updateQueryStringParameter(uri, key, value, type) {
        var re = new RegExp("([?&])(" + key + ")=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";

        // Remove any existing occurrences of the 'type' parameter from the URL
        uri = uri.replace(/([\?&])type=.*?(&|$)/i, '$1$2');

        if (uri.match(re)) {
            // If the parameter already exists in the URL, replace its value
            return uri.replace(re, '$1$2=' + value + '$3') + (uri.indexOf('?') !== -1 ? "&" : "") + "type=" + type;
        } else {
            // If the parameter doesn't exist, add it to the URL
            return uri + separator + key + "=" + value + (uri.indexOf('?') !== -1 ? "&" : "") + "type=" + type;
        }
    }

    $('input[type=radio][name=type]').change(function() {
        var type = $('input[name="type"]:checked').val();
        var pid = $('#project_id').val();
        var url = document.location.href;
        var id = 'id';
        var data = updateQueryStringParameter(url, id, pid, type);
        document.location = data;
        reloadcalender();




    });




    function reloadcalender() {


        var pid = $('#project_id').val();
        var url = document.location.href;
        var type = getParameterFromURL(url, "type");

        $.ajax({
            method: "get",
            url: "<?php echo $this->createUrl('projects/calandersheet') ?>",
            data: {
                pid: pid,
                type: type
            },
        }).done(function(msg) {

            $("#calendertable").html(msg);


        });



        $.ajax({
            method: "get",
            url: "<?php echo $this->createUrl('site/tasksheet') ?>",
            data: {
                pid: pid
            },
        }).done(function(msg) {

            $("#task_data").html(msg);



        });
    }
    $('#refresh_btn').click(function() {
        var pid = $('#project_id').val();
        var url = document.location.href;
        var id = 'id';
        type = "";
        var data = updateQueryStringParameter(url, id, pid, type);
        document.location = data;
        reloadcalender();
    });
</script>