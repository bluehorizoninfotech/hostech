<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js" type="text/javascript"></script>
<br clear="all">
<div class="monthly-shedule-sec">
<div class="table_hold">

    <form name="emp_att_grid" id="emp_att_grid">
        <div id="parent">
            <table class="table attnd_table table-bordered" id="fixTable">
                <?php

                $week_array = array();
                $selected_date_ = strtotime($selected_date);

                $first_month = date("01-m-Y", $selected_date_);


                array_push($week_array, $first_month);


                $date1 = new DateTime($start_date);
                $date2 = new DateTime($end_date);
                $diff = $date2->diff($date1)->format("%a");
                $totdays = $diff + 1;
                for ($k = 1; $k < 4; $k++) {
                    $month_string = strtotime($first_month);
                    $first_month = date("01-m-Y", strtotime("+1 month", $month_string));
                    array_push($week_array, $first_month);
                }
                $year_array = array();
                $year_array_list = array();
                $year_array_count = 0;
                foreach ($week_array as $week_datas) {
                    $date_split = explode('-', $week_datas);
                    if (!in_array($date_split[2], $year_array_list)) {
                        $year_array_count = 0;
                        array_push($year_array_list, $date_split[2]);
                        $year_array_count++;
                        $year_array[$date_split[2]] = $year_array_count;
                    } else {
                        $year_array_count++;
                        $year_array[$date_split[2]] = $year_array_count;
                    }
                }
                // echo '<pre>'    ;print_r($year_array);exit;
                ?>
                <thead>
                    <tr>
                        <th rowspan="2" colspan="4" class="fixed_th"></th>
                        <?php
                        foreach ($year_array as $year => $diffdays_year) {
                            if (count($year_array) == 1) {
                                $diffdays_year = 4;
                            }
                            if ($diffdays_year == 1) {
                                $diffdays_year = 1;
                            }

                            echo "<th colspan='" . $diffdays_year . "' class='month-period'>" . $year . "</th>";
                        }

                        ?>
                    </tr>
                    <tr>

                        <?php

                        foreach ($week_array as $week_datas) {

                            $monthdayarr[$week_datas] = 1;
                        }

                        ?>

                        <?php
                        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');

                        foreach ($monthdayarr as $month => $diffdays) {
                            $data_split = explode('-', $month);

                            if (count($monthdayarr) == 1) {
                                $diffdays = 4;
                            }
                            if ($diffdays == 1) {
                                $diffdays = 1;
                            }
                            echo "<th colspan='" . $diffdays . "'rowspan='2' class='month-period'>" . $monthsarr[$data_split[1]] . "</th>";
                        }
                        ?>
                    </tr>
                    <tr>
                        <th class="fixed_th hdrborder width-50-percentage" colspan="3">
                            Task Name
                        </th>
                    </tr>


                </thead>
                <tbody>
                    <?php
                    $count = 0;
                    foreach ($tasks as $key => $milestone_data) {
                        $task_datas = $milestone_data;
                        $milestone_model = Milestone::model()->findByPk($key);
                    ?>
                        <tr>
                            <td colspan="4" class="bg_grey bg-cell"><?= $milestone_model->milestone_title ?></td>
                            <?php
                            for ($p = 0; $p < count($week_array); $p++) { ?>
                                <td></td>
                            <?php }
                            ?>
                        </tr>
                        <?php
                        foreach ($task_datas as $task) {
                            $time_entry_array = array();
                            if (isset($task[0]))
                                $time_entry_array = $task[0];
                            $task_status = 'grey';
                            $count++;
                        ?>
                            <tr id="user ?>">
                                <!-- <td width="20"></td> -->
                                <td colspan="4" align="left">
                                    <div class="pull-left">
                                        <?= $task['title'] . '#' ?>
                                    </div>
                                </td>
                                <?php
                                $task_start_month = date('m', strtotime($task['start_date']));
                                $task_end_month = date('m', strtotime($task['due_date']));
                                $task_start_year = date('Y', strtotime($task['start_date']));
                                $task_end_year = date('Y', strtotime($task['due_date']));
                                /* zero preceding */
                                $task_start_month = sprintf("%02d", $task_start_month);
                                $task_end_month = sprintf("%02d", $task_end_month);
                                /* zero preceding */
                                $task_percen = '';
                                $cent_percent_status = 0;
                                for ($p = 0; $p < count($week_array); $p++) {
                                    $data_split = explode('-', $week_array[$p]);
                                    if ($cent_percent_status == 1) {
                                        $task_percen = 'green_border';
                                    }
                                    if ($task_start_month == $data_split[1] && $task_start_year = $data_split[2]) {
                                        $task_status = 'yellow';
                                    } elseif ($task_end_month == $data_split[1] && $task_end_year == $data_split[2]) {
                                        $task_status = 'solid_red';
                                    } else {
                                        $task_status = 'green';
                                    }


                                ?>

                                    <td width="20" data-toggle="modal" id="" data-backdrop="static" data-target="#entry" class="coldate <?= ($task_start_month == $data_split[1] && $task_start_year == $data_split[2]) || ($task_end_month == $data_split[1] && $task_end_year == $data_split[2]) ? $task_status : '' ?>
                                <?php // (($current_date >= $task_start) && ($current_date <= $task_end) && ($current_date <= $todays_date) && ($current_date == $time_entry_date_)) ? ' ' . $task_percen : '' 
                                ?> 
                                " data-value="" data-coldate="">
                                        <?php // (/*$task['percent_date'] != '' && */($current_date == $time_entry_date_ /*|| $time_entry_date_ > $id*/)) ? $current_perc : '' 
                                        ?>
                                    </td>
                                <?php } ?>
                            </tr>
                    <?php }
                    } ?>
                </tbody>
            </table>
        </div>
    </form>
</div>

                </div>



<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
        $j("#fixTable").tableHeadFixer({
            'head': true
        });
    });
</script>
