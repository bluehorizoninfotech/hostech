<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/select2.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/admin/layout3/scripts/onedrive.js" type="text/javascript">
</script>
<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    $model->name,
);

$this->menu = array(
    //    array('label' => 'List Projects', 'url' => array('index')),
    //    array('label' => 'Create Projects', 'url' => array('create')),
    //    array('label' => 'Update Projects', 'url' => array('update', 'id' => $model->pid)),
    //    array('label' => 'Delete Projects', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->pid), 'confirm' => 'Are you sure you want to delete this item?')),
    //    array('label' => 'Manage Projects', 'url' => array('admin')),
);
?>
<div class="view-project-sec">
    <h1><?php echo $model->name; ?> #<?php echo $model->project_no; ?></h1>
    <?php //echo $model->pid; 
    ?>

    <div>
        <div class="media">
            <div class="media-left">
                <img height="160px" src="<?php echo Yii::app()->request->baseUrl . "/uploads/project/" . $model->img_path ?>" alt="...">
            </div>
            <div class="media-body">
                <div class="row">
                    <div class="col-md-4 col-sec">
                        <?php
                        if (yii::app()->user->role == 1) {
                        ?>
                            <div><b>Client:</b><span><?php echo isset($model->client) ? $model->client->name :  "" ?></span></div>
                        <?php } ?>
                        <div><b>Start Date:</b><span><?php echo date("d-M-y", strtotime($model->start_date)) ?></span></div>
                        <div><b>End Date:</b><span><?= $model->end_date != '' ? date("d-M-y", strtotime($model->end_date)) : '' ?></span></div>
                        <div><b>Description:</b><span><?php echo $model->description ?></span></div>
                        <div><b>Total Square Feet:</b><span><?php echo $model->total_square_feet ?></span></div>
                    </div>
                    <div class="col-md-4 col-sec">
                        <div><b>Status:</b><span><?php echo $model->status0->caption ?></span></div>
                        <div><b>Billable:</b><span><?php echo $model->billable0->caption; ?></span></div>
                        <div><b>Budget:</b><span><?php echo  $model->budget ?></span></div>
                    </div>
                    <div class="col-md-4  col-sec">
                        <div><b>Assigned Project Managers:</b><span><?php echo $model->getassignees($model->assigned_to, $model); ?></span></div>
                        <div><b>Created On:</b><span><?php echo date('d-M-y', strtotime($model->created_date)) ?></span></div>
                        <div><b>Created By:</b><span><?php echo $model->createdBy->first_name . " " . $model->createdBy->last_name ?></span></div>
                    </div>

                    <div class="col-md-4  col-sec">
                        <!-- <div id="container" class="col-md-4"></div> -->
                        <button onClick="launchOneDrivePicker()">Open from OneDrive</button>




                    </div>
                </div>
            </div>
        </div>
    </div>







    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            //'project_no',
            //'name',
            // array(
            //     'name'=>Clientsite::model()->getAttributeLabel('site_name'), //column header
            //     'value'=>$model->getclientsite($model->pid), //column name, php expression
            //     'type'=>'raw',              
            // ),
            // array(
            //     'name' => 'client_id',
            //     'value' => isset($model->client) ? $model->client->name :  "N/A",
            //     'type' => 'raw',
            //     'filter' => CHtml::listData(Clients::model()->findAll(
            //                     array(
            //                         'select' => array('cid,name'),
            //                         'order' => 'name',
            //                         'distinct' => true
            //             )), "cid", "name"),
            //     'type'  => 'raw',
            //     'visible'=>(yii::app()->user->role==1),
            // ),

            // array(
            //     'name' => 'billable',
            //     'type' => 'raw',
            //     'value' => $model->billable0->caption,
            // ),
            // array(
            //     'name' => 'assigned_to',
            //     'value' => $model->getassignees($model->assigned_to,$model),
            //     // 'type' => 'raw',           
            // ),
            // array(
            //     'name' => 'start_date',
            //     'value' => date("d-M-y",strtotime($model->start_date)),                        
            //     'type' => 'html',
            //     ),
            // array(
            //         'name' => 'end_date',
            //         'value' => date("d-M-y",strtotime($model->end_date)),                        
            //         'type' => 'html',
            //     ),

            // 'budget',
            // array(
            //     'name' => 'status',
            //     'type' => 'raw',
            //     'value' => $model->status0->caption,
            // ),

            // 'description',       
            // array(
            //     'name' => 'created_date',
            //     'type' => 'raw',
            //     'value' => date('d-M-y',strtotime($model->created_date)),
            // ),

            // array(
            //     'name' => 'created_by',
            //     'type' => 'raw',
            //     'value' => $model->createdBy->first_name." ".$model->createdBy->last_name,
            // ),
            // array(
            //     'name' => 'img_path',
            //     'type'=>'html',
            //     'value'=>CHtml::image(Yii::app()->baseUrl."/uploads/project/".$model->img_path,'image',array("style"=>"width:200px;height:100px")),

            // ),


        ),
    ));
    ?>
    <br /><br />
    <!--<h2><span> Payment Report</span></h2>-->
    <hr />
    <div class="row">
        <div class="col-md-6">
            <table cellpadding="3" cellspacing="0" class="timehours table-bordered">
                <thead>
                    <tr class="tr_title">
                        <th width="150">Sl no</th>
                        <th width="550">MILESTONE TITLE</th>
                        <th width="200">START DATE</th>
                        <th width="200">END DATE</th>
                        <th width="400">STATUS</th>
                    </tr>
                </thead>
                <?php
                if (!empty($milestones)) {
                ?>
                    <?php
                    $k = 1;

                    foreach ($milestones as $milestone) {

                    ?>
                        <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
                            <td width="150"><?php echo $k; ?></td>
                            <td width="550"><?php echo $milestone['milestone_title']; ?></td>
                            <td width="200"><?php echo date('d-M-y', strtotime($milestone['start_date'])); ?></td>
                            <td width="200"><?php echo date('d-M-y', strtotime($milestone['end_date'])); ?></td>
                            <td width="400"><?php echo $milestone['status'] == 1 ? 'Enable' : 'Disable'; ?></td>

                        </tr>


                <?php
                        $k++;
                    }
                }
                ?>
            </table>
        </div>
        <div class="col-md-6">
            <table cellpadding="3" cellspacing="0" class="timehours  table-bordered">
                <thead>
                    <tr class="tr_title">
                        <th width="200">Sl no</th>
                        <th width="550">SITE NAME</th>
                        <th width="200">LATITUDE</th>
                        <th width="200">LONGITUDE</th>
                        <th width="400">SITE ENGINEERS/SUPERVISORS</th>
                        <th width="400">Action</th>
                    </tr>
                </thead>
                <?php
                if (!empty($sites)) {
                ?>
                    <?php
                    $k = 1;

                    foreach ($sites as $site) {
                        $clients_in_site = ClientsiteAssign::model()->findAll(array('condition' => 'site_id = ' . $site->id));
                        $users = '';
                        if (!empty($site)) {
                            $users_array = array();
                            foreach ($clients_in_site as $client) {
                                array_push($users_array, $client->users->first_name);
                            }
                            $users = implode(',', $users_array);
                        }

                    ?>
                        <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
                            <td width="200"><?php echo $k; ?></td>
                            <td width="550"><?php echo $site['site_name']; ?></td>
                            <td width="200"><?php echo $site['latitude']; ?></td>
                            <td width="200"><?php echo $site['longitude'];  ?></td>
                            <td width="400"><?php echo $users; ?></td>
                            <td width="400">
                                <?php
                                if (in_array("/projects/addmeeting", Yii::app()->session["menuauthlist"])) {
                                ?>
                                    <a href="<?php echo Yii::app()->createUrl("projects/addmeeting", array('id' => $site->id)) ?>" target="_blank" title="meeting"><i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                <?php
                                } else {
                                    echo "";
                                }
                                ?>

                            </td>

                        </tr>


                <?php
                        $k++;
                    }
                }
                ?>
            </table>
        </div>
    </div>

</div>


<!-- Start Projem changes -->
<div class="row">

    <div class="col-md-12">
        <table class="table table-striped weekly-report-table bordering-table">
            <thead>
                <tr>
                    <th colspan="<?php echo count($date_output) + 17 ?>" align="center"><?php echo $model->name; ?></th>
                </tr>
                <tr>
                    <th rowspan="2">Task id</th>
                    <th rowspan="2" colspan="3">Description of work</th>
                    <th rowspan="2">Owner</th>
                    <th rowspan="2">Co-ordinator</th>
                    <th rowspan="2">Priority</th>
                    <th rowspan="2">Task duration</th>
                    <th colspan="2">scheduled</th>
                    <th colspan="2">Actual</th>
                    <th rowspan="2">Progress % </th>
                    <th rowspan="2">Status</th>
                    <th rowspan="2">Remarks</th>
                    <?php
                    foreach ($date_output as $month) {
                    ?>
                        <th rowspan="2"><?php echo $month ?></th>
                    <?php
                    }
                    ?>


                </tr>

                <tr>



                    <th>Start date</th>
                    <th>End date</th>


                    <th>Start date</th>
                    <th>End date</th>


                </tr>



            </thead>

            <tbody>


                <?php
                foreach ($project_data_array as $key => $data) {
                ?>
                    <tr>

                        <td colspan="15" class="pink-bg full-border"><?php echo $key ?></td>

                        <?php
                        foreach ($date_output as $month) {
                            $month_date_s = date('Y-m-01', strtotime($month));
                            $month_date_e = date('Y-m-t', strtotime($month));
                            $month_date = date('Y-m-d', strtotime($month));
                            $bh_start_date = "";
                            $bh_end_date = "";
                            if (!empty($key)) {

                                $budget_head = BudgetHead::model()->findByAttributes(array('budget_head_title' => $key, 'project_id' => $model->pid));

                                if ($budget_head) {
                                    if ($budget_head->start_date != "" && $budget_head->end_date != "") {
                                        $bh_start_date = $budget_head->start_date;
                                        $bh_end_date = $budget_head->end_date;
                                    }
                                }
                            }
                            $budget_head_duration = "";
                            $class = '';
                            $class1 = "";
                            $value = "";
                            $style = "";
                            if ($bh_start_date != "" && $bh_end_date) {


                                if (($bh_start_date >=  $month_date_s) && ($bh_start_date <= $month_date_e)) {
                                    $class = 'project-active left-curve';
                                    $value = "s";
                                    $style = "background-color: rgb(19, 5, 45)";
                                } else if (($bh_end_date >=  $month_date_s) && ($bh_end_date <= $month_date_e)) {
                                    $class = 'project-active right-curve';
                                    // $value="e";
                                    $style = "background-color: rgb(19, 5, 45)";
                                    $value = '<span class="project-period font-2 text-white font-weight-bold text-nowrap">' . date("d-M-Y", strtotime($month_date_e)) . '</span>';
                                } else if ($month_date >= $bh_start_date && $month_date <= $bh_end_date) {
                                    $class1 = 'project-active';
                                    $value = ' b';
                                    $style = "background-color: rgb(19, 5, 45)";
                                }
                            }


                        ?>
                            <td class="project-status full-border">
                                <div class="<?php echo $class .  $class1 ?>" style="<?php echo $style ?>"><?= $value ?></div>
                            </td>

                        <?php
                        }
                        ?>

                        <?php
                        foreach ($data as $milestone) {
                            $milestone_id = $milestone['milestone_id'];
                            $parent_task = $this->getParentTask($milestone_id);
                            $milestone_duration = $this->getTimeDuration($milestone['start_date'], $milestone['end_date']);
                        ?>
                    <tr>
                        <td class="full-border"></td>
                        <td colspan="3" class="full-border"><?php echo $milestone['milestone_name']  ?></td>
                        <td class="full-border"></td>
                        <td class="full-border"></td>
                        <td class="full-border"></td>
                        <td class="full-border"><?php echo $milestone_duration ?></td>
                        <td class="full-border"><?php echo date("d-M-y", strtotime($milestone['start_date'])) ?></td>
                        <td class="full-border"><?php echo date("d-M-y", strtotime($milestone['end_date'])) ?></td>
                        <td class="full-border"></td>
                        <td class="full-border"></td>
                        <td class="full-border"></td>
                        <td class="full-border"></td>
                        <td class="full-border"></td>
                        <?php
                            foreach ($date_output as $month) {
                                $month_date_s = date('Y-m-01', strtotime($month));
                                $month_date_e = date('Y-m-t', strtotime($month));
                                $month_date = date('Y-m-d', strtotime($month));
                                $milestone_start_date = $milestone['start_date'];
                                $milestone_end_date = $milestone['end_date'];
                                $class = "";
                                $class1 = "";
                                $value = "";
                                $style = "";

                                if (($milestone_start_date >=  $month_date_s) && ($milestone_start_date <= $month_date_e)) {
                                    $class = 'project-active left-curve';
                                    $value = "s";
                                    $style = "background-color: rgb(19, 5, 45); color:rgb(19, 5, 45);";
                                } else if (($milestone_end_date >=  $month_date_s) && ($milestone_end_date <= $month_date_e)) {
                                    $class = 'project-active right-curve';
                                    $value = "e";
                                    $style = "background-color: rgb(19, 5, 45);color:rgb(19, 5, 45);";
                                } else if ($month_date >= $milestone_start_date && $month_date <= $milestone_end_date) {
                                    $class1 = 'project-active';
                                    $value = ' b';
                                    $style = "background-color: rgb(19, 5, 45);color:rgb(19, 5, 45);";
                                }

                        ?>

                            <!-- <td><?php echo $value ?></td> -->
                            <td class="project-status full-border">
                                <div class="<?php echo $class .  $class1 ?>" style="<?php echo $style ?>"><?= $value ?></div>
                            </td>

                        <?php
                            }
                        ?>
                        <?php
                            if (count($parent_task) > 0) {
                                foreach ($parent_task as $parent) {
                                    $parent_id = $parent['tskid'];
                                    $sub_task = $this->getSubTask($parent_id);
                                    $parent_task_owner = $this->getName($parent['report_to']);
                                    $parent_task_coordinator = $this->getName($parent['coordinator']);
                                    $parent_task_priority = $this->getStatus($parent['priority']);
                                    $parent_task_duration = $this->getTimeDuration($parent['start_date'], $parent['due_date']);
                                    $parent_wpr_det = $this->getWrDetails($parent_id, $parent['quantity'],$parent['task_type']);
                                    $parent_task_status = $this->getStatus($parent['status']);

                        ?>
                    <tr>
                        <td class="full-border"></td>
                        <td></td>
                        <td colspan="2" class="full-border"><?php echo $parent['title'] ?></td>
                        <td class="full-border"><?php echo $parent_task_owner ?></td>
                        <td class="full-border"><?php echo $parent_task_coordinator ?></td>
                        <td class="full-border"><?php echo $parent_task_priority ?></td>
                        <td class="full-border"><?php echo $parent_task_duration ?></td>
                        <td class="full-border"><?php echo date("d-M-y", strtotime($parent['start_date'])) ?></td>
                        <td class="full-border"><?php echo date("d-M-y", strtotime($parent['due_date'])) ?></td>
                        <td class="full-border"><?php echo  $parent_wpr_det[0] ?></td>
                        <td class="full-border"><?php echo  $parent_wpr_det[1] ?></td>
                        <td class="full-border"><?php echo  round($parent_wpr_det[2], 2) ?></td>
                        <td class="full-border"><?php echo $parent_wpr_det[3] ?></td>
                        <td class="full-border"><?php echo $parent_wpr_det[4]?></td>
                        <?php
                                    foreach ($date_output as $month) {
                                        $month_date_s = date('Y-m-01', strtotime($month));
                                        $month_date_e = date('Y-m-t', strtotime($month));
                                        $month_date = date('Y-m-d', strtotime($month));
                                        $parent_start_date = $parent['start_date'];
                                        $parent_end_date = $parent['due_date'];
                                        $class = "";
                                        $class1 = "";
                                        $value = "";
                                        $style = "";

                                        if (($parent_start_date >=  $month_date_s) && ($parent_start_date <= $month_date_e)) {
                                            $class = 'project-active left-curve';
                                            $value = "s";
                                            $style = "background-color: rgb(19, 5, 45);color: rgb(19, 5, 45)";
                                        } else if (($parent_end_date >=  $month_date_s) && ($parent_end_date <= $month_date_e)) {
                                            $class = 'project-active right-curve';
                                            $value = "e";
                                            $style = "background-color: rgb(19, 5, 45);color: rgb(19, 5, 45)";
                                        } else if ($month_date >= $parent_start_date && $month_date <= $parent_end_date) {
                                            $class1 = 'project-active';
                                            $value = ' b';
                                            $style = "background-color: rgb(19, 5, 45);color: rgb(19, 5, 45)";
                                        }


                        ?>

                            <td class="project-status full-border">
                                <div class="<?php echo $class .  $class1 ?>" style="<?php echo $style ?>"><?= $value ?></div>
                            </td>


                        <?php
                                    }
                        ?>
                        <?php
                                    if (count($sub_task) > 0) {
                                        foreach ($sub_task as $sub) {
                                            $task_id = $sub['tskid'];
                                            $sub_owner = $this->getName($sub['report_to']);
                                            $sub_task_coordinator = $this->getName($sub['coordinator']);
                                            $sub_task_priority = $this->getStatus($sub['priority']);
                                            $sub_task_duration = $this->getTimeDuration($sub['start_date'], $sub['due_date']);
                                            $sub_wpr_det = $this->getWrDetails($task_id, $sub['quantity'],$sub['task_type']);
                                            $sub_task_status = $this->getStatus($sub['status']);
                        ?>
                    <tr>
                        <td class="full-border"></td>
                        <td></td>
                        <td></td>
                        <td class="full-border"><?php echo $sub['title'] ?></td>
                        <td class="full-border"><?php echo $sub_owner ?></td>
                        <td class="full-border"><?php echo $sub_task_coordinator ?></td>
                        <td class="full-border"><?php echo $sub_task_priority ?></td>
                        <td class="full-border"><?php echo $sub_task_duration ?></td>
                        <td class="full-border"><?php echo date("d-M-y", strtotime($sub['start_date'])) ?></td>
                        <td class="full-border"><?php echo date("d-M-y", strtotime($sub['due_date'])) ?></td>
                        <td class="full-border"><?php echo $sub_wpr_det[0] ?></td>
                        <td class="full-border"><?php echo $sub_wpr_det[1] ?></td>
                        <td class="full-border"><?php echo  round($sub_wpr_det[2], 2) ?></td>
                        <td class="full-border"><?php echo $sub_wpr_det[3] ?></td>
                        <td class="full-border"><?php echo $sub_wpr_det[4]?></td>
                        <?php
                                            foreach ($date_output as $month) {
                                                $month_date_s = date('Y-m-01', strtotime($month));
                                                $month_date_e = date('Y-m-t', strtotime($month));
                                                $month_date = date('Y-m-d', strtotime($month));
                                                $sub_start_date = $sub['start_date'];
                                                $sub_end_date = $sub['due_date'];
                                                $class = "";
                                                $class1 = "";
                                                $value = "";
                                                $style = "";

                                                if (($sub_start_date >=  $month_date_s) && ($sub_start_date <= $month_date_e)) {
                                                    $class = 'project-active left-curve';
                                                    $value = "s";
                                                    $style = "background-color:rgb(19, 5, 45);color: rgb(19, 5, 45)";
                                                } else if (($sub_end_date >=  $month_date_s) && ($sub_end_date <= $month_date_e)) {
                                                    $class = 'project-active right-curve';
                                                    $value = "e";
                                                    $style = "background-color: rgb(19, 5, 45);color: rgb(19, 5, 45)";
                                                } else if ($month_date >= $sub_start_date && $month_date <= $sub_end_date) {
                                                    $class1 = 'project-active';
                                                    $value = ' b';
                                                    $style = "background-color: rgb(19, 5, 45);color: rgb(19, 5, 45)";
                                                }
                        ?>

                            <td class="project-status full-border">
                                <div class="<?php echo $class .  $class1 ?>" style="<?php echo $style ?>"><?= $value ?></div>
                            </td>

                        <?php
                                            }
                        ?>
                    </tr>
            <?php
                                        }
                                    }
            ?>
            </tr><!-- parent task tr -->
    <?php
                                }
                            }
    ?>
    </tr><!-- milestone tr -->
<?php
                        }
?>


<tr>
    <!-- budget head tr  -->
<?php
                }
?>










            </tbody>
    </div>

</div>
<!-- End Projem change -->

<?php
$settings = GeneralSettings::model()->findByPk(1);
?>
<!-- <script type="text/javascript" src="https://js.live.net/v7.2/OneDrive.js"></script> -->
<script type="text/javascript">
    function launchOneDrivePicker() {
        var currentLocation = window.location;

        var onedrive_client_id = "<?= isset($settings->onedrive_clientId) ? $settings->onedrive_clientId : '' ?>";


        var odOptions = {
            clientId: onedrive_client_id, // add app client id here
            action: "query",
            multiSelect: true,
            viewType: "all",
            openInNewWindow: true,
            advanced: {
                // redirectUri:"https://costmspms.bhiapp.net/pms/index.php?r=projects/addproject",
                redirectUri: "<?php echo Yii::app()->createAbsoluteUrl('projects/addproject', array(), 'https') ?>",
                endpointHint: "api.onedrive.com",
            },

            success: function(files) {
                console.log(JSON.stringify(files));
                var files_array = files.value;
                if (files_array) {
                    for (var i in files_array) {
                        //window.processOneDriveFile(files_array[i]);
                    }
                }
            },
            cancel: function() {
                /* cancel handler */ },
            error: function(error) {
                console.log(data);
                console.debug(data);
                console.log("error");
            }
        }
        OneDrive.open(odOptions);
    }

    function processOneDriveFile(file) {
        var file_name = file.name;
        var file_size = file.size;
        var download_url = file['@microsoft.graph.downloadUrl'];
        var data = {
            file_name: file_name,
            file_size: file_size,
            download_url: download_url,
            command: 'handle-onedrive-file',
        };
        console.log(data);

        $.ajax({
            url: 'file_handler.php',
            type: 'post',
            data: data,
            error: function(data) {
                console.log(data);
                console.debug(data);
                console.log("error");
            },
            success: function(data) {
                console.log(data);
                console.log("success");
            }
        });
    }
</script>