<head>
    <style>
        body {
            padding: 8px;
        }

        #view_content {
            width: 100%;
            overflow-x: auto;
        }
    </style>
</head>

<?php
$contractor_list = Contractors::model()->findAll(array('condition' => 'status = 1'));
?>

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'meeting-detailed-reports-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'beforeValidate' => new CJavaScriptExpression('function(form) {
                for(var instanceName in CKEDITOR.instances) { 
                    CKEDITOR.instances[instanceName].updateElement();
                }
                return true;
            }'),
            'afterValidate' => 'js:function(form, data, hasError) {
                if(hasError) {                                                            
                }
                else
                {
                // return true;  it will submit default way
                meeting_reports_form(form, data, hasError); //and it will submit by ajax function
                   }
               }',
        ),
    )
);

$condition = 'project_id = 0';



if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $site_meeting_model = SiteMeetings::model()->findByPk($id);
    $project_id = $site_meeting_model->project_id;
    $condition = "report_type = 2 AND from_date != '1970-01-01' 
                    AND to_date != '1970-01-01' AND project_id = $project_id";
    $result_data = ProjectReport::model()->findAll($condition);
}






if (isset($_GET['id'])) {
    $meeting_id = $_GET['id'];
    //$meeting_minutes_id='';
} else {

    $meeting_id = 0;
}
?>
<div class="meeting-step-three">
    <div class='row minutes_data'>
        <div class="col-md-3">
            <input type="hidden" id="report_meeting_id" name="report_meeting_id_name" value="<?php echo $meeting_id ?>">
            <!-- <?php echo $form->labelEx($model, 'report_id'); ?>
        <?php echo $form->dropDownList($model, 'report_id', CHtml::listData(ProjectReport::model()->findAll(
            array(
                'condition' => $condition,
            )
        ), 'id', 'from_date'), array('empty' => 'Select Report', 'class' => 'form-control')); ?> -->

            <select class="form-control task_id" name="MeetingDetailedReports[report_id]"
                id="MeetingDetailedReports_report_id">
                <option value="">-Select Report-</option>
                <?php
                if (!empty($result_data)) {
                    foreach ($result_data as $key => $value) {
                        $report_name = 'Detailed Report(' . $value['from_date'] . " - " . $value['to_date'] . ")";
                        ?>
                        <option value="<?php echo $value['id']; ?>">
                            <?php echo $report_name ?>
                        </option>
                    <?php }
                } ?>

            </select>
            <?php echo $form->error($model, 'report_id'); ?>
            <?php echo CHtml::submitButton('Go', array('class' => 'save_btn btn blue btn-sm generate_report')); ?>

        </div>




    </div>
    <!-- <input type="button" id="add_minute" value="Add" onClick="addminutes();"> -->
    <div class="row text-right">
        <div class="col-sm-12">
            <?php
            // echo CHtml::SubmitButton('Save', array('class' => 'btn blue save_btn button_id', 'style' => 'margin-left: 30px;', 'id' => 'save_btn', 'name' => 'save_btn'), array('onclick' => 'return validation();'));
            ?>

        </div>
    </div>
    <?php $this->endWidget(); ?>
    <div class="work-holder" id="report">

    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            // if (!$model->isNewRecord) {
            
            $this->widget(
                'zii.widgets.grid.CGridView',
                array(
                    'id' => 'meeting-minutes-detail-grid',
                    'dataProvider' => $model->search($meeting_id),
                    // 'filter' =>$task,
                    'itemsCssClass' => 'table table-bordered',
                    'pager' => array(
                        'id' => 'dataTables-example_paginate',
                        'header' => '',
                        'prevPageLabel' => 'Previous ',
                        'nextPageLabel' => 'Next '
                    ),
                    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                    'columns' => array(
                        array('class' => 'IndexColumn', 'header' => 'S.No.'),
                        array(
                            'header' => 'Action',
                            'htmlOptions' => array('class' => 'width-80'),
                            'class' => 'CButtonColumn',
                            'template' => '{edit} {view}',
                            'buttons' => array(
                                'edit' => array(
                                    'label' => '',
                                    'imageUrl' => false,
                                    'url' => "CHtml::normalizeUrl(array('updatemeetingreport', 'id'=>\$data->report_id,'meeting_id'=>\$data->meeting_id))",
                                    'options' => array('class' => 'icon-pencil icon-comn edit-button', 'title' => 'View', 'target' => '_blank'),
                                ),
                                'view' => array(
                                    'label' => '',
                                    'imageUrl' => false,
                                    'url' => "",
                                    'options' => array(
                                        'class' => 'icon-eye icon-comn step-3-view-modal',
                                        'title' => 'Edit',

                                    ),


                                ),

                            )
                        ),
                        array(
                            'header' => 'Name',
                            'value' => function ($data) {
                                echo $data->getReportName($data->report_id);
                            },
                        ),

                       

                    ),
                )
            );
            // }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 d-none" id="view_content" >
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        var report_meeting_id = $('#report_meeting_id').val();

        <?php $meeting_minutes_id = "<script>document.write(report_meeting_id)</script>" ?>
        $(".generate_report").click(function () {
            var report_id = $("#MeetingDetailedReports_report_id").val();
            $.ajax({
                method: "post",
                dataType: "json",
                data: {
                    report_id: report_id,
                    report_meeting_id: report_meeting_id,
                },
                url: '<?php echo Yii::app()->createUrl("Projects/mom_meetingDetailedReport") ?>',
                success: function (data) {
                    $('#report').html(data);
                }
            });
        });
        $('.step-3-view-modal').on('click', function (e) {
            // alert(1);

            // var urlid = $(this).attr('data-url');
            var url = $(this).closest("tr").find(".edit-button").attr("href");
            var searchParams = new URLSearchParams(url);

            // Extract id and meeting_id parameters
            var id = searchParams.get('id');
            var meeting_id = searchParams.get('meeting_id');
            if ($('#view_content').is(':visible')) {
                $('#view_content').hide();
            } else {
                $.ajax({
                    type: "GET",

                    url: '<?php echo Yii::app()->createUrl("Projects/viewStep3FormData") ?>',
                    data: {
                        meeting_id: meeting_id,
                        id: id
                    },
                    success: function (response) {
                        var parseResponse = JSON.parse(response);
                        $('#view_content').html(parseResponse.html);
                        $('#view_content').show();


                    }
                });
            }


        });

    });
</script>