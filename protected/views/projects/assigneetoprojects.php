<div class="assigneetoproject-section">
<div class="clearfix">
    <?php if (Yii::app()->user->role == 1 || Yii::app()->user->role == 9) { ?>
    <button data-toggle="modal" data-target="#addassigneetoproject" class="btn blue createproject pull-right">Project
        /Assign</button>
    <?php } ?>
    <h1>Assigned Projects</h1>
</div>

<div class="table-responsive">

    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'projects-grid',
        'itemsCssClass' => 'table table-bordered',
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
                'nextPageLabel'=>'Next ' ),    
            'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'columns'=>array(
            array('class' => 'IndexColumn', 'header' => 'S.No.','htmlOptions'=>array('class'=>'width-20'),),
            //'pid',
            'name',
            //'start_date',
            //'end_date',
            array(
                'name'=>'start_date',
                'value'=>'isset($data->start_date)?date("d-M-y", strtotime($data->start_date)):""',
            ),
            array(
                'name'=>'end_date',
                'value'=>'isset($data->start_date)?date("d-M-y", strtotime($data->end_date)):""',
            ),
            //'assigned_to',
            array(
            'name' =>'assigned_to',
            'value' =>'($data->assigned_to!=NULL)?$data->assignedTo->first_name." ".$data->assignedTo->last_name:""',
            
            ),
            

            array(
                'htmlOptions' => array('class'=>'width-80'),
                'class' => 'CButtonColumn',
                'template' => '{assigneeUpdate}',
                
                'buttons'=>array(
                    
                    'assigneeUpdate' => array(
                        'label'=>'',
                        'imageUrl' =>false,
                        'url' => 'Yii::app()->createUrl("projects/assigneeUpdate",array("id"=>$data->pid,))',
                        'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit',),
                    ),
                    // 'import' => array(
                    //     'label'=>'',
                    //     'options' => array('class' => 'importicon icon-docs icon-comn','title'=>'Import csv'),
                    //     'url' => 'Yii::app()->createUrl("items/index",array("projectid"=>$data->pid,))',
                    //     //'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/import.png',
                    //     'imageUrl' => false,
                    // ),
                    
                    )
                        
            ),
        ),
    ));
    ?>
</div>

<div id="id_view"></div>
<!-- Add tools Popup -->

<div id="addassigneetoproject" class="modal" role="dialog">
    <div class="modal-dialog modal-lg margin-auto">

    </div>
</div>

</div>
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Edit',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0" class="min-height-400"></iframe>

<?php
$this->endWidget();
?>
<?php
    Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   $(".createproject").click(function (event) {
			 event.preventDefault();			
			$.ajax({
				type: "GET",
				url:"'.Yii::app()->createUrl('projects/assigneecreate').'",
				success: function (response)
				{					
                    $("#addassigneetoproject").html(response);
                    $("#addassigneetoproject").css({"display":"block"});
					
				}
			});
		});
  } );
  

');

?>
