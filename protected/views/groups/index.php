<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript"> 
    $(document).ready(function() {

        /*add to memberlist on each checkbox click 
         * on check save
         * on uncheck delete
         * */
	//$(".loading").hide();	 
        $(document).on("click", "input[name='userids[]']", function () {
            var userid= $(this).val();
            if($(this).is(':checked')){
                 $.ajax({
                    method: "POST",
                    dataType: "html",
                    url: "index.php?r=groups/addmemeberlist",
                    data: {userid: userid},
                    success: function (data) {
                        // $("#memeberlisttable").html("");
                        //$("#memeberlisttable").append(data);
                        window.top.jQuery("#memeberlisttable").html("");
                        window.top.jQuery("#memeberlisttable").append(data);
                    }
                });
            }else{
                $.ajax({
                    method: "GET",
                    dataType: "html",
                    url: "index.php?r=groups/deletemember",
                    data: {memeberid: userid},
                    success: function (data) {
                        // $("#memeberlisttable").html("");
                        //$("#memeberlisttable").append(data);
                        window.top.jQuery("#memeberlisttable").html("");
                        window.top.jQuery("#memeberlisttable").append(data);
                    }
                });
            }
        });
        /*add all users in each page of pagination memberlist 
         * on check save
         * on uncheck delete
         * */
        
        $(document).on("click", ".checkboxhead", function () {
                var searchIDs = $("input[name='userids[]']:checked").map(function(){
                return $(this).val();
              }).get();
                   var wordlength =  searchIDs.length;
                    if(wordlength==0){
                         var unchecksearchIDs = $("input[name='userids[]']:not(:checked)").map(function(){
                            return $(this).val();
                          }).get();
                        $.ajax({
                         method: "POST",
                         dataType: "html",
                         url: "index.php?r=groups/deletememberall",
                         data: {userid: unchecksearchIDs},
                         success: function (data) {
                             // $("#memeberlisttable").html("");
                             //$("#memeberlisttable").append(data);
                             window.top.jQuery("#memeberlisttable").html("");
                             window.top.jQuery("#memeberlisttable").append(data);
                         }
                     });
                }else{
                        $.ajax({
                         method: "POST",
                         dataType: "html",
                         url: "index.php?r=groups/addmemeberlistall",
                         data: {userid: searchIDs},
                         success: function (data) {
                             // $("#memeberlisttable").html("");
                             //$("#memeberlisttable").append(data);
                             window.top.jQuery("#memeberlisttable").html("");
                             window.top.jQuery("#memeberlisttable").append(data);
                         }
                     });
                }
        });
	});
	</script>
    
    <?php
/* @var $this TimeEntryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Groups',
);
$creategroupUrl = $this->createUrl('creategroup');
$getgroupUrl = $this->createUrl('getgroupdetails');
$deletegroupUrl = $this->createUrl('deletegroup');
$img = Yii::app()->request->baseUrl.'/images/trash.png';
Yii::app()->clientScript->registerScript('myjquery', '

$(function () {
  $( "#dialog1" ).dialog({
    autoOpen: false
  });
  
  $("#opener").click(function() {
    $("#dialog1").dialog("open");
	var group_id = $("#group_id").val();
	add_member_ajax(group_id);
  });
});
    if($("#hiddengroupidselect").val()>0){
        $("#creategroup_btn").val("Update");
    }
    groupchehck();
    function groupchehck(){
        $(".add_button_mem").addClass("memberbtn");
        $.ajax({
                method: "GET",
                dataType: "html",
                url:"'.Yii::app()->createUrl('groups/countgroup').'",
                success: function (data) {
                    if(data=="true"){
                        $(".add_button_mem").removeClass("memberbtn").addClass("memberbtnhide");
                        $("#creategroup_btn").val("Create");
                        $("#nogroup").show();
                    }else{
                        $(".add_button_mem").addClass("memberbtn");
                    }
                    }
            });
    } 
	
    var selectedid = $("#hiddengroupidselect").val();
    if(selectedid !=null){
        ajaxclick(selectedid);
    }


    $("#addnewgroup").click(function(){
        $("#creategroup_btn").val("Create");
        if($("li").hasClass("newgroup")){
            //do nothing if new group exists
        }
        else{
        }
        $(".add_button_mem").removeClass("memberbtn").addClass("memberbtnhide");
        $("#nogroup").hide();
        $("#group_id").val(0);
        $("#group_name").val("");
        $("#group_lead").val( $("#group_lead option:first").val());
        $("#group_status").val( $("#group_status option:first").val());
        $("#description").val("");  
        $("#listmem").html("<tr colspan=3><td>Add Members to the list</td></tr>");
    });
    
   
    
    $("#creategroup_btn").click(function(){
        var group_id = $("#group_id").val();
        var group_name = $("#group_name").val();
        var group_lead = $("#group_lead option:selected").val();
        var group_status = $("#group_status option:selected").val();
        var description = $("#description").val(); 
        $(".grouperror").removeClass("error");
        $(".grouperror").removeClass("success");
        $.ajax({
            method: "POST",
            url: "'.$creategroupUrl.'",
            data: "group_id="+group_id+"&group_name="+group_name+"&group_lead="+group_lead+"&status="+group_status+"&description="+description,
            success: function (data) {
                data = $.parseJSON(data);
                if(data.action == "create"){
                    if(data.message == "success"){
                        $("li.newgroup").remove();
                        $("li.groupname").removeClass("active");
                         $("#addmember_btn").show();
                         $(".add_button_mem").removeClass("memberbtnhide").addClass("memberbtn");
                         $("#nogroup").hide();
                        $.each(data.record, function(i, item) {
                          var newgroupid = item.group_id;  
                          $("#grouplist").prepend("<li id=\'selected_"+newgroupid+"\' data-id=\'"+newgroupid+"\' class=\'groupname active\'>"+group_name+"<a class=\"delgroup\" id=\'"+newgroupid+"\' data-id=\'"+group_name+"\'><img style=\'float:right;\' src=\"'.$img.'\"></a></li>");
                          $("#group_id").val(newgroupid);
                            $("#creategroup_btn").val("Update");
                            ajaxclick(newgroupid);    
                            add_member_ajax(newgroupid);
                        });
                        
                        $(".grouperror").html("Group created successfully").show();
                        $(".grouperror").addClass("success");
                        $(".grouperror").delay(4000).fadeOut("slow");
                    }else{
                    
                        
                        $(".grouperror").html(data.message).show();
                        $(".grouperror").addClass("error");
                        $(".grouperror").delay(4000).fadeOut("slow");
                    }
                   

                }
                else{
                    if(data.message == "success"){
                        $("li#selected_"+group_id).html(group_name+"<a class=\"delgroup\" id=\'"+group_id+"\' data-id=\'"+group_name+"\'><img style=\'float:right;\' src=\"'.$img.'\"></a>");
                        $(".grouperror").html("Data successfully updated").show();
                        $(".grouperror").addClass("success");
                        $(".grouperror").delay(4000).fadeOut("slow");
                        ajaxclick(group_id);   
                    }else{
                        ajaxclick(group_id);
						add_member_ajax(group_id);
                        $(".grouperror").html(data.message).show();
                        $(".grouperror").addClass("error");
                        $(".grouperror").delay(4000).fadeOut("slow");
                        
                    }
                
                }
                bindButtonClick();   
            }
        });
        return false;
        
    });

function bindButtonClick(){ 
    $(".groupname").click(function(){
        $(".loading").show();
         var groupid = $(this).attr("data-id");
        $("li.newgroup").remove();
        $("#creategroup_btn").val("Update");
    	ajaxclick(groupid);
		add_member_ajax(groupid);

    });
	   
}
function add_member_ajax(groupid){

 $.ajax({
        method: "POST",
        url: "'.$getgroupUrl.'",
        data: "groupid="+groupid,
        success: function (data) {
            
            // ajax add-memeberlist view
            $.ajax({
                      dataType: "html",
                      url:"'.Yii::app()->createUrl('groups/addmembers').'",
                      success: function (data) {
                                $("#dialog1").html("");
                                $("#dialog1").append(data);
                      }
                  });           
        }
    });
}
function ajaxclick(groupid){ 
 $(".add_button_mem").removeClass("memberbtnhide").addClass("memberbtn");
$(".groupname").removeClass("active");
$("#selected_"+groupid).addClass("active"); 
 $.ajax({
        method: "POST",
        url: "'.$getgroupUrl.'",
        data: "groupid="+groupid,
        success: function (data) {
            $(".memberslist").show();
            // ajax memeberlist view
            $.ajax({
                      dataType: "html",
                      url:"'.Yii::app()->createUrl('groups/memeberlist').'",
                      success: function (data) {
                                $(".loading").hide();
                                $("#memeberlisttable").html("");
                                $("#memeberlisttable").append(data);
                      }
                  });
            data = $.parseJSON(data);
            $.each(data, function(i, item) {
                $("#group_id").val(item.group_id);
                $("#group_name").val(item.group_name);
                $("#group_lead").find("option").each(function( i, opt ) {
                    if( opt.value === item.group_lead ) 
                        $(opt).attr("selected", "selected");
                });
                $("#group_status").find("option").each(function( i, opt ) {
                    if( opt.value === item.status ) 
                        $(opt).attr("selected", "selected");
                });
                $("#description").val(item.description);
            });
        }
    });
}

$(document).ready(function(){	
    // bind event handlers when the page loads.
    bindButtonClick();
    $(".memberslist").hide();
    
});
$(document).on("click", ".delgroup", function () {
    var group_id = $(this).attr("id");
    var group_name = $(this).attr("data-id");
    if(confirm("Are you sure you want to delete this group?")){
        $.ajax({
            method: "POST",
            url: "'.$deletegroupUrl.'",
            data: "group_id="+group_id+"&group_name="+group_name,
            success: function (data) {
                data = $.parseJSON(data);
                
                var id = data.id;
                if(data.status=="success"){
                    $(".grouperror").html(data.message).show();
                    $(".grouperror").addClass("success");
                    $(".grouperror").delay(4000).fadeOut("slow");
                    groupchehck();
                    $(".add_button_mem").removeClass("memberbtn").addClass("memberbtnhide");
                    $("li#selected_"+id).remove();
                    $("#group_id").val(0);
                    $("#group_name").val("");
                    $("#group_lead").val( $("#group_lead option:first").val());
                    $("#group_status").val( $("#group_status option:first").val());
                    $("#description").val("");  
                    $("#creategroup_btn").val("Create");
                    $("#listmem").html("<tr colspan=3><td>Add Members to the list</td></tr>");
                }
                else{
                    $(".grouperror").html(data.message).show();
                    $(".grouperror").addClass("error");
                    $(".grouperror").delay(4000).fadeOut("slow");
                }
            }
        });
    }
});
if($("#hiddengroupidselect").val()!=""){
   $("#group_id").val($("#hiddengroupidselect").val());
}
if($("#group_id").val()==0){
    $(".add_button_mem").removeClass("memberbtn").addClass("memberbtnhide");
};

');

            

?>     
<div class="group-index-sec">           
<div class="pull-right">
  <?php
    echo CHtml::link('+ Add Group', '', array('class' => 'add_button','id' => 'addnewgroup'));
    ?>
</div>
<h1>Groups</h1>
<?php
if (isset(Yii::app()->user->role) && Yii::app()->user->role <= 2) {
    ?>
<div class="row"  >
    <ul id="grouplist" class="col-md-3 margin-0">
        <?php
                if(!empty($groups)){
                    foreach($groups as $k => $group){
                ?>
        <li id="selected_<?php echo $group['group_id']; ?>"  data-id="<?php echo $group['group_id']; ?>" class="groupname cursor-pointer"><a class="delgroup" id="<?php echo $group['group_id']; ?>" data-id="<?php echo $group['group_name'] ?>">
                <img class="pull-right" src="<?php echo Yii::app()->request->baseUrl; ?>/images/trash.png"/></a> <?php echo $group['group_name'] ?> </li>
        <?php
                    }
                }
                else{
                    echo "<span id='nogroup' style='color: #F00;'>No Group exists !</span>";
                }
                ?>
      </ul>
        <div class="col-md-9 border-left">
        <div class="row">
           <div class="col-md-3">
            <input type="hidden" name="group_id" id="group_id" value="0" />
            <input type="text" name="group_name" class="form-control" id="group_name" value="" placeholder="Group Name"/>
              </div>
          
          <div class="col-md-3">
              <?php echo CHtml::dropdownlist('group_lead', '<selected value>', CHtml::listData(Users::model()->with('userType')->findAll(
                            array(
                                'select' => array('userid' , 'CONCAT_WS(" ",first_name,last_name) AS first_name', 'userType.*','user_type'),
                                'condition'=>"status=0 AND user_type IN(1,2,4)",
                                'order' => 'user_type, first_name',
                                'distinct' => true
                    )), "userid", "first_name","userType.role"), array('id' => 'group_lead', 'class'=>'form-control', 'empty' => '--Choose Group Lead--' ));?>
             
          </div>
        </div>
          <div class="row">
              <div class="col-md-3">
            <select name="group_status" class="form-control" id="group_status">
              <option selected disabled="">--Choose Status--</option>
              <option  value="0">Inactive</option>
              <option value="1">Active</option>
            </select>
          </div>
       
            <div class= "col-md-3">
           
            <textarea id="description" class="form-control margin-left-0 margin-bottom-20 line-height-10" placeholder="Description"></textarea>
          </div>
          </div>
          <div class="">
            <input type="button" class="btn blue pull-right" id="creategroup_btn" value="Create"/>
            
           </div>
      </div>
    </div>
     
      <div class="height-20"> <span class="grouperror"></span> </div>
      <br/>
      
      <div>
        <hr class="light-gray-border-color">
      </div>
      <div class="memberslist">
          <span class="pull-right">
          <?php
                if(!empty($groups)){
                $class="memberbtn";
                }else{
                    $class="memberbtnhide";
                }
				?>
          <a id="opener" class="add_button_mem <?php echo $class;?>" onclick="<?php echo Yii::app()->createUrl('/groups/addmembers');?>">+ Add Members</a>
          </span>
        <h3>Group Members</h3>
          
          
        
        <div id="dialog1" title="Add Members"  >
          <?php
                    
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'clients-grid',
                        'itemsCssClass' =>'greytable table table-bordered',
                        'template' => '<div class="table-responsive">{items}</div>',
                        'dataProvider' => $model->memberlistsearch(),
                        'filter' => $model,
                        'columns' => array(
                             array(
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => 2,
                                 'headerHtmlOptions'=>array(
                                     'class' =>'checkboxhead',
                                 ),
                                'checkBoxHtmlOptions' => array(
                                    'name' => 'userids[]',
                                ),
                                'value'=>'$data->userid',
                             ),
                            array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
                            'first_name',
                            'designation',
                            
                            
                        ),
                    ));
                    ?>
        </div>
        
        <div id="memeberlisttable"></div>
      </div>
    </div>
  </div>
</div>
<div class="pull-right clear-both margin-right-100"> </div>
<?php
    }
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'New Task',
        'autoOpen' => false,
        'modal' => false,
        'width' => 550,
        'height' => 590,
        'style' => array('background-color' => '#ededed'),
    ),
));
?>
<div id="id_view"></div>
</div>
<?php
$this->endWidget();
?>
<input type="hidden" value="<?php if(isset($_SESSION["gpid"])){ echo $_SESSION["gpid"]; }?>" id="hiddengroupidselect"/>
