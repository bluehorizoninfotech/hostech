<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>



<script type="text/javascript">
    $(document).ready(function() {
 
        /*add to memberlist on each checkbox click 
         * on check save
         * on uncheck delete
         * */
        $(document).on("click", "input[name='userids[]']", function () {
            var userid= $(this).val();
            if($(this).is(':checked')){
                 $.ajax({
                    method: "POST",
                    dataType: "html",
                    url: "index.php?r=groups/addmemeberlist",
                    data: {userid: userid},
                    success: function (data) {
                        // $("#memeberlisttable").html("");
                        //$("#memeberlisttable").append(data);
                        window.top.jQuery("#memeberlisttable").html("");
                        window.top.jQuery("#memeberlisttable").append(data);
                    }
                });
            }else{
                $.ajax({
                    method: "POST",
                    dataType: "html",
                    url: "index.php?r=groups/deletemember",
                    data: {memeberid: userid},
                    success: function (data) {
                        // $("#memeberlisttable").html("");
                        //$("#memeberlisttable").append(data);
                        window.top.jQuery("#memeberlisttable").html("");
                        window.top.jQuery("#memeberlisttable").append(data);
                    }
                });
            }
        });
        /*add all users in each page of pagination memberlist 
         * on check save
         * on uncheck delete
         * */
        
        $(document).on("click", ".checkboxhead", function () {
                var searchIDs = $("input[name='userids[]']:checked").map(function(){
                return $(this).val();
              }).get();
                   var wordlength =  searchIDs.length;
                    if(wordlength==0){
                         var unchecksearchIDs = $("input[name='userids[]']:not(:checked)").map(function(){
                            return $(this).val();
                          }).get();
                        $.ajax({
                         method: "POST",
                         dataType: "html",
                         url: "index.php?r=groups/deletememberall",
                         data: {userid: unchecksearchIDs},
                         success: function (data) {
                             // $("#memeberlisttable").html("");
                             //$("#memeberlisttable").append(data);
                             window.top.jQuery("#memeberlisttable").html("");
                             window.top.jQuery("#memeberlisttable").append(data);
                         }
                     });
                }else{
                        $.ajax({
                         method: "POST",
                         dataType: "html",
                         url: "index.php?r=groups/addmemeberlistall",
                         data: {userid: searchIDs},
                         success: function (data) {
                             // $("#memeberlisttable").html("");
                             //$("#memeberlisttable").append(data);
                             window.top.jQuery("#memeberlisttable").html("");
                             window.top.jQuery("#memeberlisttable").append(data);
                         }
                     });
                }
        });
        
            
        $('.search-form form').submit(function(){
                $.fn.yiiGridView.update('clients-grid', {
                        data: $(this).serialize()
                });
                return false;
        });
        $.ajax({
            method: "POST",
            dataType: "html",
            url: "index.php?r=groups/listdepartment",
            success: function (data) {   
                $('#roleid').append("<option>Select</option>");
               for (var i = 0; i < data.length; i++) {
                   var list = "<option value='+data.id+'>"+data['role']+"</option>";
                   $('#roleid').append(list);
               }
               // $("#roleid").append('<option>test department</option>')
            }
        });
    });
    $(document).on("click", "#allcheck", function () {
        if (this.checked)
        {
            $('.userscheck').prop('checked', true);
        } else {
            $('.userscheck').prop('checked', false);
        }

    });
    $(document).on("click", ".form_field_button", function () {
        var userid = [];
        $('.userscheck').each(function () {
            if (this.checked)
            {
                //alert($(this).attr('data-id'));
                userid.push($(this).attr('data-id'));
            }
        });
        $.ajax({
            method: "POST",
            dataType: "html",
            url: "index.php?r=groups/addmemeberlist",
            data: {userid: userid},
            success: function (data) {
                // $("#memeberlisttable").html("");
                //$("#memeberlisttable").append(data);
                window.top.location.reload();
            }
        });
    });

</script>
<div class="memberlist-sec">
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'clients-grid',
    'itemsCssClass' =>'greytable table table-bordered',
    'dataProvider' => $model->memberlistsearch(),
    'filter' => $model,
    'columns' => array(
         array(
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
             'headerHtmlOptions'=>array(
                 'class' =>'checkboxhead',
             ),
            'checkBoxHtmlOptions' => array(
                'name' => 'userids[]',
            ),
            'value'=>'$data->userid',
         ),
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('header'=>'Name','name'=>'first_name', 'value'=>'$data->first_name." ".$data->last_name'),
        'designation',
         array(
            'header' =>   'Department',
            'name' => 'department_id',
            'value' => function($data,$row){
                $userid = $data->userid;
                return $data->getDepartment($userid);
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Department::model()->findAll(
                            array(
                                'select' => array('department_id,department_name'),
                                'order' => 'department_id',
                                'distinct' => true
                    )), "department_id", "department_name"),
                    
        ),
        
    ),
));
?>
</div>