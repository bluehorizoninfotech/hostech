<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script type="text/javascript">
   // $(document).on("click", ".deletecls", function (event) {
    $('.deletecls').unbind('click').click(function (event) {
        event.preventDefault();
        event.stopPropagation();
        var memeberid = $(this).attr("data-id");
		//alert(memeberid);
        if (confirm("Are you sure you want to delete this member")) {
            $.ajax({
                method: "GET",
                dataType: "html",
                url: "index.php?r=groups/deletegroupmember",
                data: {memeberid: memeberid},
                success: function (data) {
                    $("#memeberlisttable").html("");
                    $("#memeberlisttable").append(data);
					
					
                }
            });
			//////////////////////////////////
					$.ajax({
                      dataType: "html",                      
					  url: "index.php?r=groups/addmembers",
                      success: function (data) {
                                $("#dialog1").html("");
                                $("#dialog1").append(data);
                      }
                  	});  
					//////////////////////////////////
        }
        return false;
    });
	
	
	
</script>
<div class="index-member-list-sec">
<div id="listmem">
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'groups-grid',
    'itemsCssClass' =>'greytable table table-bordered',
    'dataProvider' => $model->grp_memberlistsearch(),
    'filter' => $model,
    'columns' => array(
         /*array(
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
             'headerHtmlOptions'=>array(
                 'class' =>'checkboxhead',
             ),
            'checkBoxHtmlOptions' => array(
                'name' => 'userids[]',
            ),
            'value'=>'$data->userid',
         ),*/
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
         array(
            'name' => 'full_name',
       // 'value'=>'CHtml::link($data->status==0?"$data->first_name $data->last_name *":"$data->first_name $data->last_name" , 
            'value'=>'$data->first_name." ".$data->last_name' , 
            
            'type'=>'raw',
            ),
        //array('header'=>'Name','name'=>'full_name', 'value'=>'$data->full_name." ".$data->last_name'),
        'designation',
        
        /*array(
            'header' =>  'Group Lead',
            'value' => function($data,$row){
                $userid = $data->userid;
                return $data->getGrouplead($userid);
            },
            'type' => 'raw',

        ),*/
         array(
            'header' =>  'Group Lead',
            'value' => function($data,$row){
                $userid = $data->userid;
                $deleteopt= $data->getDeleteaction($userid);
                //echo  $deleteopt;
                if((int)$deleteopt==0){
                    echo '<img src="images/tick.png" class="imgtckcls">';
                }else{
                    echo ''; 
                }
            },
        ),           
                    
         array(
            'header' =>   'Department',
            'name' => 'department_id',
            'value' => function($data,$row){
                $userid = $data->userid;
                return $data->getDepartment($userid);
            },
            'type' => 'raw',
            'filter' => CHtml::listData(Department::model()->findAll(
                            array(
                                'select' => array('department_id,department_name'),
                                'order' => 'department_id',
                                'distinct' => true
                    )), "department_id", "department_name")
        ),
		array(
            'header' =>  'Delete',
            'value' => function($data,$row){
                $userid = $data->userid;
                $deleteopt= $data->getDeleteaction($userid);
                //echo  $deleteopt;
                if((int)$deleteopt==0){
                    echo '';
                }else{
                    echo '<a class="icon-trash deletecls" title="Delete" data-id="'.$data->userid.'"></a>';
                }
            },
            /*'value'=>function($data){ 
                return '<input class="deletecls" value="" type="button" data-id="'.$data->userid.'" />'; 
                
                
            },
           
            'type' => 'raw',
            */
        ),
		/*array(
            'class' => 'CButtonColumn',
            'template'=>'{delete}',
			'buttons'=>array
			(
				'delete' => array
				(
					'label'=>'Send an e-mail to this user',
					
					'url'=>'Yii::app()->createUrl("groups/delete", array("id"=>$data->userid))',
				),
				
			),
        ),*/
		
        
    ),
));
?>
</div>
</div>