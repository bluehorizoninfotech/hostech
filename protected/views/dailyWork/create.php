<?php
/* @var $this DailyWorkController */
/* @var $model DailyWork */

$this->breadcrumbs=array(
	'Daily Works'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DailyWork', 'url'=>array('index')),
	array('label'=>'Manage DailyWork', 'url'=>array('admin')),
);
?>

<h1>Create DailyWork</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>