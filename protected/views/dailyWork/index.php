<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>
<?php
/* @var $this DailyWorkController */
/* @var $model DailyWork */

$this->breadcrumbs = array(
    'Daily Works' => array('index'),
    'Manage',
);

$this->menu = array(
    //array('label'=>'List DailyWork', 'url'=>array('index')),
    //array('label'=>'Create DailyWork', 'url'=>array('create')),
);
?>
<p id="success_message" class="display-none font-15 green-color font-weight-bold">Successfully removed..</p>
<div>
    <h1 class="dailywork-index-sec">Daily Labour Report</h1>
    <?php

    //if (Yii::app()->user->role == 1 || Yii::app()->user->role == 10) {
        if((in_array('/dailyWork/create', Yii::app()->session['menuauthlist'])))
        {
    ?>
        <div class="form page-scroll-bar">

            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'daily-work-form',
                //'action'=>Yii::app()->createUrl('//dailyWork/create'),
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => false,
                ),
            )); ?>


            <div class="row">
                <input type="hidden" name="newdetails" id="newdetails">
                <div class="subrow col-md-3 col-sm-6">
                    <?php echo $form->labelEx($model2, 'project_id'); ?>

                    <?php
                    if (Yii::app()->user->role == 1) {
                        $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1', 'order' => 'name ASC');
                    } else {
                        $criteria = new CDbCriteria;
                        $criteria->select = 'project_id';
                        $criteria->condition = 'assigned_to = ' . Yii::app()->user->id . ' OR report_to = ' . Yii::app()->user->id . ' OR coordinator = ' . Yii::app()->user->id;
                        $criteria->group = 'project_id';
                        $project_ids = Tasks::model()->findAll($criteria);
                        $project_id_array = array();
                        foreach ($project_ids as $projid) {
                            array_push($project_id_array, $projid['project_id']);
                        }
                        if (!empty($project_id_array)) {
                            $project_id_array = implode(',', $project_id_array);
                            $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND assigned_to =' . Yii::app()->user->id . ' OR pid IN (' . $project_id_array . ')', 'order' => 'name ASC');
                        } else {
                            $condition = array('select' =>  array('pid,name'), 'condition' => 'status=1 AND assigned_to =' . Yii::app()->user->id, 'order' => 'name ASC');
                        }
                    }
                    if(Yii::app()->user->project_id !=""){
                        $model->project_id = Yii::app()->user->project_id;
                        $project_id = Yii::app()->user->project_id;
                    }else{
                        $model->project_id = "";
                        $project_id = "";
                    }
                    $project = Projects::model()->findAll($condition);
                    $data_array = array();
                    if (count($project) == 1) {
                        foreach ($project as $key => $value) {
                            array_push($data_array, $value->pid);
                        }
                        $id = implode(" ", $data_array);
                        $p_data =  Projects::model()->findByPk($id);
                    ?>
                        <select class="form-control" name="DailyWork[project_id]" id="DailyWork_project_id">
                            <option value="<?php echo $p_data['pid']; ?>" <?php echo ($project_id == $p_data['pid']) ? 'selected' : ""; ?>><?php echo $p_data['name']; ?></option>
                        </select>
                    <?php
                    } else {
                        echo $form->dropDownList(
                            $model,
                            'project_id',
                            CHtml::listData(Projects::model()->findAll($condition), 'pid', 'name'),
                            array('empty' => '-Choose a Project-', 'class' => 'form-control')
                        );
                    }
                    ?>
                    <?php echo $form->error($model2, 'project_id'); ?>
                </div>
                <div class="subrow col-md-2 col-sm-6">
                    <?php echo $form->labelEx($model2, 'date'); ?>
                    <?php
                    if (isset($model2->date)) {
                        $model2->date = date('d-M-y', strtotime($model2->date));
                    }
                    echo CHtml::activeTextField($model2, 'date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                    <?php
                    /* $this->widget('application.extensions.calendar.SCalendar', array( 
			'inputField' => 'DailyWork_date',
			'ifFormat' => '%d-%b-%y',
		)); */
                    ?>
                    <?php echo $form->error($model2, 'date'); ?>
                </div>
                <div class="subrow col-md-2 col-sm-6">
                    <?php echo $form->labelEx($model2, 'taskid'); ?>
                    <?php
                    $tasks = array();
                    if (Yii::app()->user->project_id != "") {
                        $tasks = Tasks::model()->findAll(array('condition' => 'task_type = 1 AND project_id=' . Yii::app()->user->project_id . ''));
                    }
                    if ($model2->isNewRecord) {
                    ?>
                        <select class="form-control taskid" name="DailyWork[taskid]" id="DailyWork_taskid">
                            <option value="">-Choose a Task-</option>
                            <?php
                            if (!empty($tasks)) {
                                foreach ($tasks as $key => $value) {
                            ?>
                                    <option value="<?php echo $value['tskid']; ?>"><?php echo $value['title']; ?></option>
                            <?php }
                            } ?>
                        </select>
                    <?php } else {
                        echo $form->dropDownList($model2, 'taskid', CHtml::listData($task, 'tskid', 'title'), array('empty' => '-Choose a Task-', 'class' => 'form-control'));
                    } ?>
                    <?php echo $form->error($model2, 'taskid'); ?>
                </div>
                <div class="subrow col-md-2 col-sm-6">
                    <?php echo $form->labelEx($model2, 'work_type'); ?>
                    <?php
                    $condition = array('select' =>  array('wtid,work_type'), 'order' => 'work_type ASC');
                    echo $form->dropDownList($model2, 'work_type', CHtml::listData(WorkType::model()->findAll($condition), 'wtid', 'work_type'), array('empty' => '-Choose a Work Type-', 'class' => 'form-control'));
                    ?>
                    <?php echo $form->error($model2, 'work_type'); ?>
                </div>

                <div class="col-md-2 col-sm-6">
                    <?php echo $form->labelEx($model2, 'skilled_workers'); ?>
                    <?php echo $form->textField($model2, 'skilled_workers', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                    <?php echo $form->error($model2, 'skilled_workers'); ?>
                </div>
            </div>

            <div class="row">
                <div class="subrow col-md-3 col-sm-6">

                    <?php echo $form->labelEx($model2, 'task_id'); ?>
                    <span id="task_id_span1">----------</span>
                </div>

                <div class="col-md-2 col-sm-6">
                    <?php echo $form->labelEx($model2, 'unskilled_workers'); ?>
                    <?php echo $form->textField($model2, 'unskilled_workers', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                    <?php echo $form->error($model2, 'unskilled_workers'); ?>
                </div>
                <div class="subrow col-md-4 col-sm-6">
                    <?php echo $form->labelEx($model2, 'description'); ?>
                    <?php echo $form->textArea($model2, 'description', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                    <?php echo $form->error($model2, 'description'); ?>
                </div>

<?php if(count($labour_data)>0){?>

                <div class="row margin-top-120">
               
                    <?php
                    foreach($labour_data as $labour)
                    {
                        ?>
 <div class="row">
<div class=" col-md-2 col-sm-6 "><label>Labour Type</label>
                        <select class="form-control" name="DailyWork[labour_type][]">
                            <option value="<?php echo  $labour['id'] ?>">
                            <?php  echo $labour['labour_label'] ?>
                        </option>

                        </select>


                    </div>
                    
                    <div class="col-md-2">
            <label>No of Labour(s) </label>    
            <input class="form-control" autocomplete="off" name="DailyWork[labour_count][]" id="labour_count" type="text" value="<?php echo  $labour['number_of_labour'] ?>">           
                </div>



                <div class="col-md-2">
            <label>Wage </label>    
            <input class="form-control" autocomplete="off" name="DailyWork[labour_wage][]" id="labour_wage" type="text" value=<?php echo  $labour['labour_wage']?>>           
                </div>

                <div class=" col-md-2 col-sm-6 padding-16">

                      <input type="button" id="<?= $labour['id'] ?>" value="Remove" class="remove-btn">
                    </div>



                    </div>

                        <?php
                    }
                    ?>
              
                </div>

                <?php
    }
                ?>



                <!-- Labour charges -->


                <div class="row margin-top-120">


                    <div class=" col-md-2 col-sm-6 "><label>Labour Type</label>
                        <select class="form-control" id="labour_type" name="DailyWork[labour_type][]">
                            <option></option>

                        </select>


                    </div>


                    <div class="col-md-2">
            <label>No of Labour(s) </label>    
            <input class="form-control" autocomplete="off" name="DailyWork[labour_count][]" id="labour_count" type="text">           
                </div>


                <div class="col-md-2">
            <label>Wage </label>    
            <input class="form-control" autocomplete="off" name="DailyWork[labour_wage][]" id="labour_wage" type="text">           
                </div>

                 <!-- add more -->
          <div id="add_input_container"></div>
          <br>
          <input type="button" id="add_new" value="Add" onClick="addInput();">
          <br>
          <!-- end add more -->



                </div>

                <!-- end Labour charges -->




                <div class="buttons col-md-3 col-sm-6  daily-labour-report-button">
                    <label>&nbsp;</label>
                    <?php echo CHtml::submitButton($model2->isNewRecord ? 'Add' : 'Save', array('class' => ' btn blue btn-sm')); ?>
                </div>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->

    <?php
    }

    if (Yii::app()->user->role != 1) {
        $condition = 'assigned_to =' . Yii::app()->user->id;
    } else {
        $condition = "";
    }
    /* for filter project dropdown */
    $project_dropdown_1 = CHtml::listData(Projects::model()->findAll(
        array(
            'select' => array('pid,name'),
            'order' => 'name',
            'condition' => $condition,
            'distinct' => true
        )
    ), "pid", "name");
    $null_dropdown = array('0' => 'All');
    $projects_list = $project_dropdown_1 + $null_dropdown;
    ksort($projects_list);
    /**********************/
    if (isset($model->date) && $model->date != "") {
        $model->date = date('d-M-y', strtotime($model->date));
    }
    ?>
    <div class="table-responsive">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'daily-work-grid',
            'itemsCssClass' => 'table table-bordered',
            'pager' => array(
                'id' => 'dataTables-example_paginate',  'header' => '', 'prevPageLabel' => 'Previous ',
                'nextPageLabel' => 'Next '
            ),
            'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'columns' => array(

                array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('class' => 'width-20'),),
                //'id',
                //'project_id',
                 array(
                    'template' => '{update}',
                    'class' => 'CButtonColumn',
                    'visible' => (in_array('/dailyWork/update', Yii::app()->session['menuauthlist'])),
                    'buttons' => array(
                        'update' => array(
                            'label' => '',
                            'imageUrl' => false,
                            'options' => array('class' => 'icon-pencil icon-comn', 'title' => 'Edit'),
                            'url' => 'Yii::app()->createUrl("DailyWork/index", array("id"=>$data->id))',
                        ),
                    ),
                ),

                array(
                    'name' => 'project_id',
                    'value' => '$data->project->name',
                    'type' => 'raw',
                    'htmlOptions' => array('width' => '120px'),

                    'filter' => CHtml::activeDropDownList($model, 'project_id', $projects_list)

                ),
                array(
                    'name' => 'date',
                    'value' => 'date("d-M-y",strtotime($data->date))',
                    //'value' => '$data->date',
                ),
                array(
                    'name' => 'taskid',
                    'value' => '(isset($data->tasks)?$data->tasks->title:"")',
                ),
                array(
                    'name' => 'work_type',
                    'value' => '(isset($data->worktype)?$data->worktype->work_type:"")',
                ),
                'skilled_workers',
                'unskilled_workers',
                'description',
                array(
                    'name' => 'created_by',
                    'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name',
                    'visible' => Yii::app()->user->role == 1,

                    'filter' => CHtml::listData(Users::model()->findAll(
                        array(
                            'select' => array('userid,concat(first_name, " ", last_name) as first_name'),
                            'order' => 'first_name',

                            'distinct' => true
                        )
                    ), "userid", "first_name")
                ),


               
            ),
        )); ?>
    </div>
</div>
<script>
    $('#DailyWork_date').datepicker({
        dateFormat: 'dd-M-y',
        onSelect: function(selectedDate) {
            var d = new Date(selectedDate);
            var date = this.value;
            entrydatevalidation(date);
        }
    });

    function entrydatevalidation(date) {
        var project_id = $('#DailyWork_project_id').find(":selected").val();
        if (project_id !== '') {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('dailyWork/checkdate'); ?>',
                data: {
                    date: date,
                    project_id: project_id
                },
                method: "POST",
                dataType: 'json',
                success: function(result) {
                    if (result.html != "") {
                        $('#DailyWork_taskid').html(result.html);
                    } else {
                        $('#DailyWork_taskid').html("");
                    }

                    // $('#DailyWorkProgress_taskid').val(result.tsk_id);
                    // $('#DailyWorkProgress_work_type').html(result.work_type_html);
                    // $('#DailyWorkProgress_work_type').val(result.work_type);
                    // $('#task_id_span1').text(result.tsk_id);
                    // $('#DailyWorkProgress_total_quantity').val(result.quantity);
                    // $("#unit_value").html(result.unit);
                    // $("#item_total_qty").val(result.quantity);
                    // $("#item_main_id").val(result.tsk_id); 
                }
            });
        }

    }

    $("#DailyWork_project_id").change(function() {
        var val = $(this).val();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWork/getAllTasks'); ?>',
            data: {
                project_id: val
            },
            method: "GET",
            success: function(result) {
                $('.taskid').html(result);
            }
        })

        // get project labour
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWork/getLabourCharges'); ?>',
            data: {
                project_id: val
            },
            method: "GET",
            success: function(result) {
                $('#labour_type').html(result);
            }
        })
    })
    $("#DailyWork_taskid").change(function() {
        var val = $(this).val();
        if (val != '') {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('dailyWork/gettaskdetails'); ?>',
                data: {
                    taskid: val
                },
                method: "GET",
                dataType: "json",
                success: function(result) {
                    if (result.taskid != null) {
                        $("#task_id_span1").html(result.taskid);
                        $("#DailyWork_work_type").html(result.option);
                        $("#DailyWork_work_type").val(result.wtid);
                    } else {
                        $("#task_id_span1").html("");
                        $("#DailyWork_work_type").html(result.option);
                    }
                }
            })
        } else {
            $("#task_id_span1").html("");
        }
    })
</script>


<script>
     var count = 0;
     function addInput() {
        count += 1;

        $('#add_input_container').append(
            
            `
            <div class="row margin-top-60 remove-div_` + count + `">
            <div class=" col-md-2 col-sm-6 "><label>Labour Type</label>
                        <select class="form-control" id="labour_type_`+count+`" name="DailyWork[labour_type][]">
                            <option></option>

                        </select>


                    </div>


                    <div class="col-md-2">
            <label>No of Labour(s) </label>    
            <input class="form-control" autocomplete="off" name="DailyWork[labour_count][]" id="labour_count" type="text">           
                </div>


                <div class="col-md-2">
            <label>Wage </label>    
            <input class="form-control" autocomplete="off" name="DailyWork[labour_wage][]" id="labour_wage" type="text">           
                </div>


                <!-- remove button -->
                    <div class=" col-md-2 col-sm-6 padding-16 margin-top-15">
                   
                    <input type="button" id="` + count + `" value="Remove" class="remove-btn">
                    </div>
                    <!-- end remove button -->

                </div>
            `
        );

val =$('#DailyWork_project_id').val();

if(val!= "")
{
    $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWork/getLabourCharges'); ?>',
            data: {
                project_id: val
            },
            method: "GET",
            success: function(result) {
              
                $("#labour_type_" + count).html(result);
            }
        })
}
       

    $(document).on('click', '.remove-btn', function() {
      id = this.id;
      $(".remove-div_" + id).remove();
    });
     }

    //  load project labour

   
   
var pro_id='<?php echo $model2->project_id ?>';

if(pro_id!= "")
{
    $('#DailyWork_project_id').val(pro_id);

    $.ajax({
            url: '<?php echo Yii::app()->createUrl('dailyWork/getLabourCharges'); ?>',
            data: {
                project_id: pro_id
            },
            method: "GET",
            success: function(result) {
                $('#labour_type').html(result);
            }
        })


}

var btn = document.getElementsByClassName('remove-btn');
  for (var i = 0; i < btn.length; i++) {
    btn[i].addEventListener("click", function() {

      var id = this.id;




      if (!confirm("Do you want to delete")) {
        return false;
      } else {
        $.ajax({
          type: 'POST',
          url: "<?php echo Yii::app()->createUrl('dailyWork/deleteLaboursUsed'); ?>",
          data: {
            id: id
          },
          success: function(data) {
            if (data == 1) {
              $("#success_message").fadeIn().delay(1000).fadeOut();

              setTimeout(function() {
                window.location.reload(1);
              }, 1000);

            } else {
              alert("An error Occured");
            }
          },


        });
      }


    });
  }

</script>