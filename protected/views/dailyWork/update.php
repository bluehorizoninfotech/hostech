<?php
/* @var $this DailyWorkController */
/* @var $model DailyWork */

$this->breadcrumbs=array(
	'Daily Works'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DailyWork', 'url'=>array('index')),
	array('label'=>'Create DailyWork', 'url'=>array('create')),
	array('label'=>'View DailyWork', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DailyWork', 'url'=>array('admin')),
);
?>

<h1>Update DailyWork <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>