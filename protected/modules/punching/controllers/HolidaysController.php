<?php

class HolidaysController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {

        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $module = Yii::app()->controller->module->id;
        $controller_id = Yii::app()->controller->id;
        $controller = $module . '/' . $controller_id;

        if (isset(Yii::app()->session['pmsmenuall'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuall'])) {
                $accessArr = Yii::app()->session['pmsmenuall'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuauth'])) {
                $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuguest'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuguest'])) {
                $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
            }
        }
        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow',
                'actions' => $accessArr,
                'users' => array('@'),
                //'expression' => "$access_privlg > 0",

            ),
            array(
                'allow',
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",

            ),
            array(
                'allow',
                'actions' => array('takeAction'),
                'users' => array('@'),
                'expression' => "$access_privlg > 0",

            ),

            array(
                'deny',
                'users' => array('*'),
            ),

        );

        // return array(
        //     array('allow',  // allow all users to perform 'index' and 'view' actions
        //         'actions'=>array('index','view'),
        //         'users'=>array('@'),
        //     ),
        //     array('allow', // allow authenticated user to perform 'create' and 'update' actions
        //         'actions'=>array('create','update','admin','delete'),
        //         'users'=>array('@'),
        //     ),
        //     array('allow', // allow admin user to perform 'admin' and 'delete' actions
        //         'actions'=>array('admin'),
        //         'users'=>array('admin'),
        //     ),
        //     array('deny',  // deny all users
        //         'users'=>array('*'),
        //     ),
        // );

    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Holidays;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Holidays'])) {
            $model->attributes = $_POST['Holidays'];

            if ($model->save()) {

                $holiday_date = $_POST['Holidays']['holiday_date'];

                $users = Yii::app()->db->createCommand('select *,concat_ws(" ",first_name,last_name) as fullname from pms_users as u '
                    . ' order by fullname asc')->queryAll();

                $att_entry = array();

                $this->redirect(array('view', 'id' => $model->id));

            } //save

        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Holidays'])) {
            $model->attributes = $_POST['Holidays'];

            $check = Yii::app()->db->createCommand('select * from  pms_holidays where id =' . $id)->queryRow();
            $checkdate = $check['holiday_date'];
            if ($model->save()) {

                $date = $_POST['Holidays']['holiday_date'];

                $users = Yii::app()->db->createCommand('select *,concat_ws(" ",label,first_name,last_name) as fullname from pms_users as u '
                    . ' order by fullname asc')->queryAll();

                $att_entry = array();
                foreach ($users as $key => $value) {

                    extract($value);

                    $type = 14;

                }

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {

        $check = Yii::app()->db->createCommand('select * from  pms_holidays where id =' . $id)->queryRow();
        $checkdate = $check['holiday_date'];

        $this->loadModel($id)->delete();

        $this->redirect(array('index'));

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        /*if(!isset($_GET['ajax']))
    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));*/
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id = 0)
    {
        if ($id == 0) {
            $holiday = new Holidays;
        } else {
            $holiday = $this->loadModel($id);
            $check = Yii::app()->db->createCommand('select * from  pms_holidays where id =' . $id)->queryRow();
            $checkdate = $check['holiday_date'];
        }

        if (isset($_POST['Holidays'])) {
            $holiday->attributes = $_POST['Holidays'];
            $holiday->created_by = Yii::app()->user->id;
            $holiday->created_date = date("Y-m-d H:i:s");
            $holiday->updated_by = Yii::app()->user->id;
            $holiday->updated_date = date("Y-m-d H:i:s");
            $holiday->flag = 0;
            $holiday->type = 0;
            $holiday->status = 0;

            $holiday->holiday_date = date('Y-m-d', strtotime($_POST['Holidays']['holiday_date']));
            $check_exist = Holidays::model()
                ->findByAttributes(array('holiday_date' => $holiday->holiday_date));
            if (count($check_exist) == 0) {
                if ($holiday->save()) {
                    $this->redirect(array('index'));
                }
            } else {
                if ($holiday->save()) {
                    $this->redirect(array('index', "exist" => true));
                }
            }
        }
        $user_id = Yii::app()->user->id;
        $user = Users::model()->findByPk($user_id);
		$user_type = $user['user_type'];
        $model = new Holidays('search');
        $model->unsetAttributes();
        if (isset($_GET['Holidays'])) {
            $model->attributes = $_GET['Holidays'];
        }
        $this->render('index', array(
            'model' => $model, 'model2' => $holiday,'user_type'=>$user_type
        ));
    }

    public function actionAdmin()
    {
        $model = new Holidays('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Holidays'])) {
            $model->attributes = $_GET['Holidays'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Holidays::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'holidays-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actiontakeAction()
    {
        $id = $_GET['id'];
        $action = $_GET['status'];
        $holiday = $this->loadModel($id);
        $holiday->status = $action;
        $holiday->save();
        $this->redirect(array('index'));

    }
}
