<?php

class DevicesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    } 

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                $module=Yii::app()->controller->module->id; 
                $modulecontroller=$module."/".$controller;
               
                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($modulecontroller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$modulecontroller];
                        
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Devices;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Devices'])) {
            $model->attributes = $_POST['Devices'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->device_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Devices'])) {
            $model->attributes = $_POST['Devices'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->device_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
         $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id = 0) {
        if ($id == 0) {
            $model2 = new Devices;
            $msg = 'Successfully updated.';
        } else {
            $model2 = $this->loadModel($id);
            $msg = 'Successfully added new device.';
        }
        
        $msg2 = '';
        $msg3 = ''; 
         $tbl = Yii::app()->db->tablePrefix;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model2);
        if (isset($_POST['Devices'])) {
            $model2->attributes = $_POST['Devices'];
            
            extract($_POST['Devices']);
            $date = date('Y-m-d');
			$chkprev=null;
			
			if(intval($model2->id)==0){
            $chkprev = Devices::model()->findAll(array("condition" => "device_id = '$device_id' AND site_id = '$site_id' AND start_date = '$date'"));
			}
			
            if(is_null($chkprev) || sizeof($chkprev) == 0){
            $deviceres = Devices::model()->findAll(array("condition" => "device_id = '$device_id' AND site_id = '$site_id' AND end_date IS NULL"));                                          
            
            
            if(count($deviceres) == 1){
            $dvid  = $deviceres[0]['device_id'];
            $sid   = $deviceres[0]['site_id'];
            $sdate = $deviceres[0]['start_date'];
            $edate = $deviceres[0]['end_date'];
            $enddate = date('Y-m-d');
            
            
            if(empty($edate)){
            
            $sql = "UPDATE {$tbl}punching_devices SET end_date = '$enddate' WHERE device_id = '$dvid' "
                    . "AND site_id = '$sid' AND start_date = '$sdate' ";                                          
            $result = Yii::app()->db->createCommand($sql)->execute();                                        
            }  
			
            if ($model2->save()) {
                Yii::app()->user->setFlash('success', $msg);
                $this->redirect(array('index'));
            }
			
			
            }else if(empty($deviceres)){
				if ($model2->save()) {
					Yii::app()->user->setFlash('success', $msg);
					$this->redirect(array('index'));
				}                
            }else{ // device currently running in a site
              $msg2 = 'device running in a site';
              Yii::app()->user->setFlash('error', $msg2);  
              $this->redirect(array('index'));
            }
            }else{ //device already assigned to a site in this date
                $msg3 = 'This device already assigned to a site';
                Yii::app()->user->setFlash('error', $msg3);  
              $this->redirect(array('index'));
            }
        }


        $model = new Devices('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Devices']))
            $model->attributes = $_GET['Devices'];

        $this->render('index', array(
            'model' => $model, 'model2' => $model2,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Devices::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'punching-devices-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
