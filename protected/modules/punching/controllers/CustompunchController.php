<?php

class CustompunchController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{ 
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                $module=Yii::app()->controller->module->id; 
                $modulecontroller=$module."/".$controller;
               
                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($modulecontroller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$modulecontroller];
                        
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = '//layouts/iframe';
		//$layout='//layouts/column2';	
		$model=new Custompunch;
               
                $punchtable = Yii::app()->db->tablePrefix.'accesslog';
                $devicetable = Yii::app()->db->tablePrefix.'device_accessids';
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['Custompunch']))
		{
		$model->attributes=$_POST['Custompunch'];
		if($model->save()){                   
                $insert_id = Yii::app()->db->getLastInsertID();    
                extract($_POST['Custompunch']);                    
                $sql = "SELECT accesscard_id FROM $devicetable WHERE userid = '$resource' AND deviceid = '$device'";
                
               
             
        $command = Yii::app()->db->createCommand($sql);
        $result  = $command->queryRow();
        $accessid= $result['accesscard_id'];
                
        $model1=new Accesslog;
        $model1->sqllogid   =   $insert_id;
        $model1->empid      =   $accessid;
        $model1->log_time   =   $customdate.' '.$customtime;
        $model1->status     =   0;
        $model1->device_id  =   $device;
        $model1->save();

            	echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');");
                Yii::app()->end();
            }else{
           	$this->render('create',array(
			'model'=>$model,
		));		
           	die;
            }
			//$this->redirect(array('admin','id'=>$model->customid));	
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['Custompunch']))
		{

		 $_POST['Custompunch']['createddate'] =  $model["createddate"];
			
        $model->attributes=$_POST['Custompunch'];                       
        extract($_POST['Custompunch']);                       
                            if($model->save()){            
        $devicetable    =   Yii::app()->db->tablePrefix.'device_accessids';  
        $accesslogtable    =   Yii::app()->db->tablePrefix.'accesslog';  
        
        $sql            =   "SELECT accesscard_id FROM ".$devicetable." WHERE userid = ".$resource;                
        $command        =   Yii::app()->db->createCommand($sql);
        $result         =   $command->queryRow();
        $accessid       =   $result['accesscard_id'];        
        $logtime = $customdate.' '.$customtime;       
        $sql1            =   "UPDATE ".$accesslogtable." SET sqllogid = '$id',empid = '$accessid',log_time = '$logtime',device_id ='$device'  WHERE empid = $accessid AND sqllogid = $id AND logid IS NULL";                
        $command1        =   Yii::app()->db->createCommand($sql1)->execute();                                    
        //$result1         =   $command1->queryRow();
        //print_r($command1); die;
        $this->redirect(array('admin'));
                        }
				
                            
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
	 
        $custompunchtable   =   Yii::app()->db->tablePrefix.'custompunch';
        $devicetable        =   Yii::app()->db->tablePrefix.'device_accessids';  
        
        $sql                =   "SELECT resource FROM $custompunchtable WHERE customid = $id";                
        $command            =   Yii::app()->db->createCommand($sql);
        $result             =   $command->queryRow();
                  
        $resource           =   $result['resource']; 
        if(!empty($resource)){
         $sql1               =   "SELECT accesscard_id FROM ".$devicetable." WHERE userid = ".$resource;                
        $command1           =   Yii::app()->db->createCommand($sql1);
        $result1            =   $command1->queryRow();
        $accessid1          =   $result1['accesscard_id'];
        
        if(!empty($accessid1)){
        $accesslogtable     =   Yii::app()->db->tablePrefix.'accesslog';          
        $sql2               =   "DELETE FROM ".$accesslogtable." WHERE logid IS NULL AND sqllogid = '$id' AND empid = '$accessid1'";                
        $command2           =   Yii::app()->db->createCommand($sql2)->execute();     
        }                
        }                   
        $this->loadModel($id)->delete();        
	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
	if(!isset($_GET['ajax']))
	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Custompunch');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Custompunch('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Custompunch']))
			$model->attributes=$_GET['Custompunch'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Custompunch::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='custompunch-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionGetDevices(){
            
            $userid = $_POST['userid'];
            $query = "SELECT device_id,device_name FROM pms_device_accessids "
                  . "LEFT JOIN pms_punching_devices ON pms_device_accessids.deviceid=pms_punching_devices.device_id
                    WHERE userid='" . $userid . "' AND nodevice_status IS NULL";
             $devices = Yii::app()->db->createCommand($query)->queryAll();
              
            $html='<option value="">Choose Device</option>';         
            if(!empty($devices)){
            foreach ($devices as $key => $value) {   
            $html.='<option value="'.$value['device_id'].'">'.$value['device_name'].'</option>';    
            }
            }
            //return $html; 
            echo json_encode(array('devices' => $html));

        }
}
