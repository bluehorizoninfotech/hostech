<?php

class AccesslogController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * 
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
	{ 
                $accessArr = array();
                $accessauthArr = array();
                $accessguestArr = array();
                $controller = Yii::app()->controller->id;
                $module=Yii::app()->controller->module->id; 
                $modulecontroller=$module."/".$controller;
               
                if (isset(Yii::app()->session['menuauth'])) {
                    if (array_key_exists($modulecontroller, Yii::app()->session['menuauth'])) {
                        $accessauthArr = Yii::app()->session['menuauth'][$modulecontroller];
                        
                    }
                }

                
                $access_privlg = count($accessauthArr);

                return array(
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => $accessauthArr,
                        'users' => array('@'),
                        'expression' => "$access_privlg > 0",
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
		
	}

    public function actionPunchlogNew() {
        if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {
            //echo $_COOKIE['localIP'];

            $localip = $_COOKIE['localIP'];

            $pos = strpos($localip, 'perhaps ');
            if ($pos !== false) {
                $localip = trim(substr($localip, $pos), 'perhaps ');
            }

            if ($pos !== false) {
                $localip = substr($_COOKIE['localIP'], $pos);
            } else {
                //echo "The string '$findme' was not found in the string '$mystring'";
            }

            Yii::app()->db->createCommand('update pms_users set reg_ip="' . $localip . '" where userid=' . intval(Yii::app()->user->id))->query();
            Yii::app()->user->setState('reg_ip', $_COOKIE['localIP']);
            //die();
        }

        // $this->layout = '//layouts/gebomain';
        $model = new Accesslog();

        //die("<center><h1>Under Maintenance</h1></center>");

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        //echo date("Y-m-d");
        //echo date("Y-m-d H:i:s");
        $punchdate = strtotime(date("Y-m-d")) + ($days * 60 * 60 * 24);

        $predate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24));
        $nextdate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + (($days + 1) * 60 * 60 * 24));

        //die();
        // $datewithtime = date("Y-m-d H:i:s", $system_Date);
        //echo "<br />";

        $shift_time_starts = "07:00:00";
        $shift_end = "05:00:00";
        $date = $predate;
        $nextday = $nextdate;

        $in_late = "$predate 09:30:00";
        $out_early = "$predate 16:00:00";


        $sql = "SELECT al.*, u.userid,concat_ws(' ',first_name,last_name) as full_name, u.status,reg_ip, accesscard_id,reg_ip, "
                . "if(userid='" . Yii::app()->user->id . "',1,0) AS matchip FROM (SELECT logid ,sqllogid ,empid , log_time "
                . "FROM pms_punch_log WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
                . " AND  (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end')) AS al "
                . "RIGHT JOIN pms_users AS u ON empid=accesscard_id WHERE u.status=0 && accesscard_id!=''  GROUP BY userid, log_time ORDER BY matchip DESC, "
                . "full_name ASC,userid ASC,log_time ASC ";


        /* echo $sql = "SELECT  logid ,sqllogid ,empid , log_time,u.userid,concat_ws(' ',first_name,last_name) as full_name, "
          . "u.status,reg_ip, accesscard_id FROM `pms_punch_log` as al right join hrms_users as u on empid=accesscard_id "
          . "WHERE (u.status=0 or log_time between '$date $shift_time_starts' and '$nextday $shift_end') "
          . " AND  log_time between '$date $shift_time_starts' and '$nextday $shift_end' GROUP BY userid, log_time order by full_name,userid,log_time";
         */
        $logdetails = Yii::app()->db->createCommand($sql);
        $logemp = $logdetails->queryAll();

        $difflog = array();

        $inout = array(0, 0);
        $result_log = array();

        $allpunch = array();

        foreach ($logemp as $row) {
//            echo '<br />';
            $empid = $row['accesscard_id'];
            $result_log[$empid]['outearly'] = 0;

            $allpunch["user_" . $empid][] = date('h:i:s', strtotime($row['log_time']));
//            echo '<br />';
            if (!isset($result_log[$empid]['accesscard_id'])) {
                $punch_time = $prev_value = $row['log_time'];
                $i = 1;
                $result_log[$empid]['accesscard_id'] = $row['accesscard_id'];
                $result_log[$empid]['first_punch'] = $punch_time;
                $result_log[$empid]['last_punch'] = $row['log_time'];
                $result_log[$empid]['emp_name'] = $row['full_name'];
                $result_log[$empid]['ip'] = $row['reg_ip'];
                $result_log[$empid]['count'] = $i;

//                echo $result_log[$empid]['accesscard_id']."=====".$in_late."-----------==".$row['log_time'];
//                echo '<br />';
//                die();

                if (strtotime($in_late) < strtotime($row['log_time'])) {
                    $result_log[$empid]['inlate'] = 1;
                }

                $result_log[$empid]['insec'] = strtotime(date("Y-m-d")) - strtotime($row['log_time']);
                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['outsec'] = 0;
                $result_log[$empid]['outhrs'] = 0;

                $result_log[$empid]['status'] = $i;
                //  echo "<h1>first-IN-$i</h1>";
                $result_log[$empid]['status'] = 1;
                continue;
            }

            $result_log[$empid]['last_punch'] = $row['log_time'];

            // print_r($result_log);
//            if(!isset($i)){
//                die("i is not set");
//            }
//            

            if ($i % 2 == 1) {
                // echo '<h2>TEST   -OUT-->'.$i.'</h2>';
                $addinsecs = 0;
                if ($i > 1) {
                    $addinsecs = $result_log[$empid]['insec'];
                    // echo '<h3>yyyyy -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                }
//                else{
//                    echo '<h3>XXX -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
//                }
                $result_log[$empid]['insec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['status'] = 0;
            } else {
                //echo '<h2>TTTEST - IN -->'.$i.'</h2>';
                $addinsecs = $result_log[$empid]['outsec'];
                $result_log[$empid]['outsec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['outhrs'] = gmdate('H:i:s', $result_log[$empid]['outsec']);
                $result_log[$empid]['status'] = 1;
            }

            $i++;
            $prev_value = $row['log_time'];
            $result_log[$empid]['count'] = $i;

            if (strtotime($out_early) > strtotime($row['log_time'])) {
                $result_log[$empid]['outearly'] = 1;
            }
        }

        $result_log[$empid]['status'] = $i % 2;


        $this->render('punchlog_new', array('logresult' => $result_log, 'allpunch' => $allpunch, 'punchdate' => $predate));
        die();

        echo '<pre>';
        print_r($result_log);
        die();
    }

    public function ipStore() {
        // echo '<br />';
        //echo $_COOKIE['localIP'];
        //echo '<br />';
        // echo "<b>".Yii::app()->user->reg_ip."</b>";
        if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {
            //echo $_COOKIE['localIP'];

            $localip = $_COOKIE['localIP'];

            $pos = strpos($localip, 'perhaps ');
            if ($pos !== false) {
                $localip = trim(substr($localip, $pos), ' or perhaps ');
            }

            if ($pos !== false) {
                $localip = substr($_COOKIE['localIP'], $pos);
            } else {
                //echo "The string '$findme' was not found in the string '$mystring'";
            }

            preg_match('/(192\.168\.[0-9]{1,3}\.[0-9]{1,3})/', $_COOKIE['localIP'], $localipparsed);
            // print_r($localipparsed);
            $sql = 'update pms_users set reg_ip="' . $localipparsed[0] . '" where userid=' . intval(Yii::app()->user->id);
            Yii::app()->db->createCommand($sql)->query();

            Yii::app()->user->setState('reg_ip', $localipparsed[0]);


            //die();
        }
        /* else{
          die('test');
          } */
    }

    public function actionMylog() {
        //  echo Yii::app()->user->id;
        $month = 0;
        if (isset($_GET['months'])) {
            //$date_start = date('m-Y#t');
            $month = intval($_GET['months']);
        }
        /* code for including the userid */
        if (isset($_GET['code'])) {
            $code = intval($_GET['code']);
            if ($code == 0)
                $code = Yii::app()->user->id;
        }else {
            $code = Yii::app()->user->id;
        }

        $usersql = "SELECT concat_ws(' ',first_name,last_name) AS full_name FROM pms_users WHERE userid='" . $code . "'";
        $userdetails = Yii::app()->db->createCommand($usersql);
        $userdata = $userdetails->queryScalar();

        $acessusersql = "SELECT pms_users.userid, concat_ws(' ',first_name,last_name) AS full_name FROM pms_users, "
                . "pms_device_accessids WHERE pms_device_accessids.userid = pms_users.userid "
                . " ORDER BY full_name";
        $accessuserdetails = Yii::app()->db->createCommand($acessusersql);
        $accessuserdata = $accessuserdetails->queryAll();


        $page_date = mktime(0, 0, 0, date("m") + $month, date("d"), date("Y"));
        $total_days = date('t', $page_date);
        $month_yr = date('m-Y', $page_date);
        $logmonth = date('F Y', $page_date);

        $from_date = date("Y-m-", $page_date);
        $end_date = date("Y-m-t", $page_date);



        $datetimesettings = array();
        $datetimesettings['shift_time_starts'] = "07:00:00";
        $datetimesettings['shift_end'] = "23:59:59";

//        $datetimesettings['date'] = $predate;
//        $datetimesettings['nextday'] = $nextdate;
//        $datetimesettings['in_late'] = "$predate 09:30:00";
//        $datetimesettings['out_early'] = "$predate 16:00:00";
//        $datetimesettings['shift_ends'] = "$predate 23:59:00";

        $datetimesettings['late_halfday'] = "11:00:01"; //First punch Half day
        $datetimesettings['ee_halfday'] = "16:00:00";  //Early Exit Half day

        extract($datetimesettings);

          $sql = "SELECT  logid ,sqllogid ,date_format(log_time, 'log_%e') as bydate,if(logid is null,aid,sqllogid) as puntype,date_format(log_time, '%Y-%m-%d') as punchdate ,empid , log_time,dv.userid,concat_ws(' ',first_name,last_name) as full_name, "
                . "u.status,reg_ip, dv.accesscard_id,reg_ip FROM `pms_punch_log` as al "
                . "RIGHT JOIN pms_device_accessids AS dv ON dv.accesscard_id=empid  "
                . "INNER JOIN pms_users AS u ON u.userid=dv.userid  "
                . "WHERE (log_time > '{$from_date}01' and log_time <='$end_date $shift_end') && dv.userid='" . $code . "' 
                 group by puntype, punchdate,sqllogid order by log_time asc";

        $logdetails = Yii::app()->db->createCommand($sql);
        $logemp = $logdetails->queryAll();

//        echo '<pre>';
//        print_r($logemp);
//        echo '</pre>';

        $difflog = array();

        $inout = array(0, 0);
        $result_log = array();
        $allpunch = array();

        foreach ($logemp as $k => $row) {

            $entry_date = date('Y-m-d', strtotime($row['log_time']));

            $in_late = "$entry_date 09:30:00";
            $out_early = "$entry_date 16:00:00";
            $shift_ends = "$entry_date 23:59:00";

//            echo '<br />';
            $bydate = $row['bydate'];
            $result_log[$bydate]['outearly'] = 0;
            $till_time = 0;

            $allpunch["user_" . $bydate][] = date('h:i:s', strtotime($row['log_time']));
//            echo '<br />';
            if (!isset($result_log[$bydate]['accesscard_id'])) {
                $punch_time = $prev_value = $row['log_time'];
                $i = 1;
                $result_log[$bydate]['accesscard_id'] = $row['accesscard_id'];
                $result_log[$bydate]['first_punch'] = $punch_time;
                $result_log[$bydate]['last_punch'] = $row['log_time'];
                $result_log[$bydate]['emp_name'] = $row['full_name'];
                $result_log[$bydate]['userid'] = $row['userid'];
                $result_log[$bydate]['ip'] = $row['reg_ip'];
                $result_log[$bydate]['count'] = $i;
                $result_log[$bydate]['reg_ip'] = $row['reg_ip'];
//                $result_log[$bydate]['reg_ip'] = $row['reg_ip'];
//                echo $result_log[$bydate]['accesscard_id']."=====".$in_late."-----------==".$row['log_time'];
//                echo '<br />';
//                die();

                if (strtotime($in_late) < strtotime($row['log_time'])) {
                    $result_log[$bydate]['inlate'] = 1;
                }

                $result_log[$bydate]['insec'] = strtotime(date("Y-m-d")) - strtotime($row['log_time']);

                if (!isset($logemp[$k + 1]) or ( $logemp[$k + 1]['empid'] != $bydate && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {
                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$bydate]['insec'] = $till_time;
                }

                $result_log[$bydate]['inhrs'] = gmdate('H:i:s', $result_log[$bydate]['insec']);

                $result_log[$bydate]['outsec'] = 0;
                $result_log[$bydate]['outhrs'] = 0;

                $result_log[$bydate]['status'] = $i;
                //  echo "<h1>first-IN-$i</h1>";
                $result_log[$bydate]['status'] = 1;
                continue;
            }
            
            $result_log[$bydate]['last_punch'] = $row['log_time'];

            // print_r($result_log);
            //if(!isset($i)){
            //die("i is not set");
            //}

            if ($i % 2 == 1) {
                // echo '<h2>TEST   -OUT-->'.$i.'</h2>';
                $addinsecs = 0;
                if ($i > 1) {
                    $addinsecs = $result_log[$bydate]['insec'];
                    // echo '<h3>yyyyy -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                }
//                else{
//                    echo '<h3>XXX -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
//                }
                $result_log[$bydate]['insec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$bydate]['inhrs'] = gmdate('H:i:s', $result_log[$bydate]['insec']);

                $result_log[$bydate]['status'] = 0;
            } else {
//                if (!isset($logemp[$k + 1]) or ( $logemp[$k + 1]['empid'] != $logemp && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {

                if (!isset($logemp[$k + 1]) or ( $logemp[$k + 1]['empid'] != $logemp[$k]['empid'] && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {
                    // echo $logemp[$k ]['empid'];
                    //echo '<pre>';
                    //  echo $k + 1;
                    //print_r($logemp);
                    //die();
//                    
//                    echo $row['log_time'];
//                    echo '<br />';
//                    echo date("Y-m-d H:i:s") .'-' .$row['log_time'];
//                    die();
//                    
                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$bydate]['insec'] = $result_log[$bydate]['insec'] + $till_time;
                }

                //echo '<h2>TTTEST - IN -->'.$i.'</h2>';
                $addinsecs = $result_log[$bydate]['outsec'];
                $result_log[$bydate]['outsec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$bydate]['outhrs'] = gmdate('H:i:s', $result_log[$bydate]['outsec']);
                $result_log[$bydate]['status'] = 1;
            }

            $i++;
            $prev_value = $row['log_time'];
            $result_log[$bydate]['count'] = $i;

            if (strtotime($out_early) > strtotime($row['log_time'])) {
                $result_log[$bydate]['outearly'] = 1;
            }
        }

        $noentries = 0;
        if (isset($i)) {
            $result_log[$bydate]['status'] = $i % 2;
            $noentries = 1;
        }

//          echo '<pre>';
//          print_r($result_log);
//          echo '</pre>';
          //die(); 
        //**********************************************************

        $data = array(
            'page_date' => $page_date,
            'total_days' => $total_days,
            'month_yr' => $month_yr,
            'logmonth' => $logmonth,
            'result_log' => $result_log,
            'datetimesettings' => $datetimesettings,
            'userdata' => $userdata,
            'accessuserdata' => $accessuserdata,
            'code' => $code,
        );

        $this->render('mylogs', $data);
    }
   
    public function actionPunchlog() {

        if (isset($_REQUEST['device_id']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('device_id', intval($_REQUEST['device_id']));
            if (Yii::app()->user->device_id == 0) {
                unset(Yii::app()->user->device_id);
            }
            die('1');
        }
        
        
         if (isset($_GET['code'])) {
            $code = intval($_GET['code']);
            if($code <= 0){
            $code = 0;    
            }
        }else {
            $code = 0;
        }        

        $devid = 0;
        if (isset(Yii::app()->user->device_id) and Yii::app()->user->device_id>0) {
            $devid = Yii::app()->user->device_id;
        }


        $this->ipStore();
        $regip = '00';

        $punching_devices = Devices::model()->findAll();
        $devices = array();
        $devicesite = array();
        foreach ($punching_devices as $pdevice) {
            $devices[$pdevice->device_id] = $pdevice->device_name;
            $devicesite[$pdevice->device_id] = $pdevice->site_id;
        }

        //$punchtable = Yii::app()->db->tablePrefix.'accesslog';
        $punchtable = 'pms_punch_log';
        $tbl = Yii::app()->db->tablePrefix;

        // $this->layout = '//layouts/gebomain';
        $model = new Accesslog();

        //die("<center><h1>Under Maintenance</h1></center>");

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        //echo date("Y-m-d");
        //echo date("Y-m-d H:i:s");
        $punchdate = strtotime(date("Y-m-d")) + ($days * 60 * 60 * 24);

        $predate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24));
        $nextdate = date("Y-m-d", strtotime(date("Y-m-d H:i:s")) + (($days + 1) * 60 * 60 * 24));

        $datetimesettings = array();
        $datetimesettings['shift_time_starts'] = "07:00:00";
        $datetimesettings['shift_end'] = "05:00:00";

        $datetimesettings['date'] = $predate;

        $datetimesettings['nextday'] = $nextdate;

        $datetimesettings['in_late'] = "$predate 09:30:00";
        $datetimesettings['out_early'] = "$predate 16:00:00";
        $datetimesettings['shift_ends'] = "$predate 23:59:00";

        $datetimesettings['late_halfday'] = "11:00:01"; //First punch Half day
        $datetimesettings['ee_halfday'] = "16:00:00";  //Early Exit Half day

        extract($datetimesettings);

        $excludeemp = '';
        if (yii::app()->user->role != 1) {
            // $excludeemp = ' and empid not in (4,5,27) ';
        }

         $by_device ='';
        if($devid>0){            
            $by_device = ' where device_id='.$devid ;
        }
        
        $sitecondition= '';
        if($code>0){
        //$sitecondition = " INNER JOIN {$tbl}usersite us ON u.userid = us.user_id AND us.site_id = '$code' AND us.assigned_date='$predate'";     
        $sitecondition = " INNER JOIN {$tbl}punching_devices pd ON dv.deviceid = pd.device_id"        
        . " AND pd.site_id = '$code'";            
        }
        
        $setbigselects = Yii::app()->db->createCommand("SET SQL_BIG_SELECTS=1")->execute();  //quick fix for - Syntax error or access violation: 1104 The SELECT would examine more than MAX_JOIN_SIZE rows; check your WHERE and use SET SQL_BIG_SELECTS=1 or SET MAX_JOIN_SIZE=# if the SELECT is okay. 
        
        $sql = "SELECT al.*, dv.userid,concat_ws(' ',first_name,last_name) as full_name, u.status,reg_ip, dv.accesscard_id,reg_ip,empid, "
                . "if(dv.userid='" . Yii::app()->user->id . "',1,0) AS matchip  FROM (SELECT logid ,sqllogid ,empid , log_time, device_id,manual_entry_status "
                . "FROM {$punchtable} WHERE (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end') "
                . " AND  (log_time BETWEEN '$date $shift_time_starts' AND '$nextday $shift_end' ) $excludeemp ) AS al "
                . "Left JOIN pms_device_accessids AS dv ON dv.accesscard_id=empid "
                . "INNER JOIN pms_users AS u ON u.userid=dv.userid $sitecondition"
                . "$by_device GROUP BY empid, log_time ORDER BY matchip DESC, full_name ASC,empid ASC,log_time ASC ";
                
          // die;    
        $logdetails = Yii::app()->db->createCommand($sql);
        $logemp = $logdetails->queryAll();

//        echo '<pre>';
//        print_r($logemp);
//        echo '</pre>';
//        die();
//        
        $difflog = array();

        $inout = array(0, 0);
        $result_log = array();

        $allpunch = array();
        foreach ($logemp as $k => $row) {
//            echo '<br />';
            $empid = $row['empid'];
            $result_log[$empid]['outearly'] = 0;
            $till_time = 0;
           /* if(!empty($row['manual_entry_status'])){
              $user_site = Yii::app()->db->createCommand()
                                ->join('pms_clientsite pc', 'pc.id=pm.site_id')
                                ->select('pc.site_name')
                                ->from('pms_manualentry pm')
                                ->where('pm.id=:id', array(':id'=>$row['manual_entry_status']))
                                ->queryRow();
               if (isset($user_site) && strlen($user_site['site_name']) >= 7){
                                $user_site['site_name'] = substr($user_site['site_name'], 0, 6). "..";  
                            }   
                $device_name =  "(" .(isset($user_site) ? $user_site['site_name'] : $row['manual_entry_status']) . ") "; 
            }else{
            $device_name = ((isset($row['device_id']) and intval($row['device_id']) != 0) ? "(" . (isset($devices[$row['device_id']])? $devices[$row['device_id']] : '') . ") " : '');
			}  
			*/    
            $device_name = ((isset($row['device_id']) and intval($row['device_id']) != 0) ? "(" . (isset($devices[$row['device_id']])? $devices[$row['device_id']] : '') . ") " : '');
            
            $log_time = ($row['log_time'] != '' ? $row['log_time'] : '0');
            $allpunch["user_" . $empid][] = $device_name . date("h:i:s ", strtotime($log_time));
//            echo '<br />';

            if (!isset($result_log[$empid]['accesscard_id'])) {
                $punch_time = $prev_value = $row['log_time'];
                $i = 1;
                $result_log[$empid]['accesscard_id'] = $row['empid'];
                ;
                $result_log[$empid]['first_punch'] = $punch_time;
                $result_log[$empid]['last_punch'] = $row['log_time'];
                $result_log[$empid]['emp_name'] = ($row['full_name'] == '' ? $row['empid'] : $row['full_name']);
                $result_log[$empid]['userid'] = $row['userid'];
                $result_log[$empid]['ip'] = $row['reg_ip'];
                $result_log[$empid]['count'] = $i;
                $result_log[$empid]['reg_ip'] = $row['reg_ip'];
                $result_log[$empid]['myip'] = $row['matchip'];
                $result_log[$empid]['device_id'] = $row['device_id'];
                if(!empty($row['manual_entry_status'])){
 					/*
                        $user_site = Yii::app()->db->createCommand()
                                ->join('pms_clientsite pc', 'pc.id=pm.site_id')
                                ->select('pc.site_name')
                                ->from('pms_manualentry pm')
                                ->where('pm.id=:id', array(':id'=>$row['manual_entry_status']))
                                ->queryRow();
                            $result_log[$empid]['entrytype'] =  isset($user_site) ? $user_site['site_name'] : $row['manual_entry_status']; 
                   */    
                $result_log[$empid]['entrytype'] = $row['manual_entry_status'];    
                }else{
                $result_log[$empid]['entrytype']  = NULL;    
                }
                
                if (strtotime($in_late) < strtotime($row['log_time'])) {
                    $result_log[$empid]['inlate'] = 1;
                }

                $result_log[$empid]['insec'] = strtotime(date("Y-m-d")) - strtotime($row['log_time']);

                if (!isset($logemp[$k + 1]) or ( $logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {
                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$empid]['insec'] = $till_time;
                }

                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['outsec'] = 0;
                $result_log[$empid]['outhrs'] = 0;

                $result_log[$empid]['status'] = $i;
                //  echo "<h1>first-IN-$i</h1>";
                $result_log[$empid]['status'] = 1;
                continue;
            }
            $result_log[$empid]['ldevice_id'] = $row['device_id'];
            $result_log[$empid]['last_punch'] = $row['log_time'];

            // print_r($result_log);
            //if(!isset($i)){
//                die("i is not set");
//            }
//            

            if ($i % 2 == 1) {
                // echo '<h2>TEST   -OUT-->'.$i.'</h2>';
                $addinsecs = 0;
                if ($i > 1) {
                    $addinsecs = $result_log[$empid]['insec'];
                    // echo '<h3>yyyyy -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
                }
//                else{
//                    echo '<h3>XXX -- '.($row['log_time'] ."-<b>". ($prev_value)) ."</b>+". $addinsecs."</h3>";
//                }
                $result_log[$empid]['insec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['inhrs'] = gmdate('H:i:s', $result_log[$empid]['insec']);

                $result_log[$empid]['status'] = 0;
            } else {

                if (!isset($logemp[$k + 1]) or ( $logemp[$k + 1]['empid'] != $empid && strtotime(date("Y-m-d H:i:s")) > strtotime($row['log_time']) && strtotime(date("Y-m-d H:i:s")) < strtotime($shift_ends))) {

                    $till_time = strtotime(date("Y-m-d H:i:s")) - strtotime($row['log_time']);
                    $result_log[$empid]['insec'] = $result_log[$empid]['insec'] + $till_time;
                }

                //echo '<h2>TTTEST - IN -->'.$i.'</h2>';
                $addinsecs = $result_log[$empid]['outsec'];
                $result_log[$empid]['outsec'] = (strtotime($row['log_time']) - strtotime($prev_value)) + $addinsecs;
                $result_log[$empid]['outhrs'] = gmdate('H:i:s', $result_log[$empid]['outsec']);
                $result_log[$empid]['status'] = 1;
            }

            $i++;
            $prev_value = $row['log_time'];
            $result_log[$empid]['count'] = $i;

            if (strtotime($out_early) > strtotime($row['log_time'])) {
                $result_log[$empid]['outearly'] = 1;
            }
        }

        $noentries = 0;
        if (isset($i)) {
            $result_log[$empid]['status'] = $i % 2;
            $noentries = 1;
        } 
        
        //$sitesql = "SELECT * FROM {$tbl}clientsite";
        //$sitedetails = Yii::app()->db->createCommand($sitesql);
        $sitedata = Clientsite::model()->findAll();
       // print_r($sitedata);
        $siteofdevices = CHtml::listData(Devices::model()->findAll(), 'device_id', 'site_id');
       // die;
        
        if($devid>0 and isset($_REQUEST['ajax'])){
                        $this->render('punchlog', array('noentries' => $noentries, 'logresult' => $result_log,'devicesite'=>$devicesite,
            'allpunch' => $allpunch, 'punchdate' => $predate, 'datetimesettings' => $datetimesettings, 'devices' => $devices,'siteofdevices'=>$siteofdevices,'sitedata'=>$sitedata,'code'=>$code,'days'=>$days));
        }                        
        $this->render('punchlog', array('noentries' => $noentries, 'logresult' => $result_log,'devicesite'=>$devicesite,
            'allpunch' => $allpunch, 'punchdate' => $predate, 'datetimesettings' => $datetimesettings, 'devices' => $devices,'siteofdevices'=>$siteofdevices,'sitedata'=>$sitedata,'code'=>$code,'days'=>$days));
    }

    public function actionPunchlog_old() {
        header("location: http://timelog.bhipms.net");
        die();
        if (isset($_COOKIE['localIP']) && isset(Yii::app()->user->reg_ip) && (Yii::app()->user->reg_ip != $_COOKIE['localIP'])) {
            //echo $_COOKIE['localIP'];

            $localip = $_COOKIE['localIP'];

            $pos = strpos($localip, 'perhaps ');
            if ($pos !== false) {
                $localip = trim(substr($localip, $pos), 'perhaps ');
            }

            if ($pos !== false) {
                $localip = substr($_COOKIE['localIP'], $pos);
            } else {
                //echo "The string '$findme' was not found in the string '$mystring'";
            }

            Yii::app()->db->createCommand('update pms_users set reg_ip="' . $localip . '" where userid=' . intval(Yii::app()->user->id))->query();
            Yii::app()->user->setState('reg_ip', $_COOKIE['localIP']);
            //die();
        }

        $this->layout = '//layouts/gebomain';
        $model = new Accesslog();

        //die("<center><h1>Under Maintenance</h1></center>");

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }
//echo date("Y-m-d");
//echo date("Y-m-d H:i:s");
        echo $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24) + (5.5 * 60 * 60);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
//        echo "<br />";
        $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,reg_ip,"
                . "concat(first_name,' ', last_name) as username, "
                . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
                . "and empid=accesscard_id) as count_punch ,(select log_time "
                . "from pms_punch_log as pa where log_time  like '$precent_date%' "
                . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
                . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
                . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
                . "where accesscard_id!=0  and `status`=0 group by accesscard_id order by first_name";

        //die();
//die();
        $command = Yii::app()->db->createCommand($sql);
        $logemp = $command->queryAll();

        $lastsysncsql = Yii::app()->db->createCommand('SELECT date_format(last_sync_time,"%d-%m-%Y %T") FROM `pms_punch_log_last_sync`');
        $lastsync = $lastsysncsql->queryScalar();
        //print_r(  $lastsync); 

        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
                . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
                . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();

        $logrec = array();
        $userlog = array();

        $userid = 0;
        $prev_time = 0;
        $flag = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
//            /$logrec[$rec['empid']][] = ($rec['log_time']);
//            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
            //continue;            
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];

                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                //echo "<br/>";
                $flag++;
            } else {
//                //echo $flag."<br/>";
//                echo strtotime($rec['log_time']) . '<<<<<<<<<<<<<<->>>>>>>>>>>>' . $prev_time."------->";
//               // die();
                $timediffin = strtotime($rec['log_time']) - $prev_time;
//
//               echo "<br />";

                $prev_time = strtotime($rec['log_time']);

                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;
                    //  die();
                } else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                    //  die();
                    $inouttime[$empid1]['OUT'] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                    $inouttime[$empid1]['IN'] = $this->calculate_time_span($userlog[$empid1]['IN']);
                }

                // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                $flag++;
            }
        }


        if (!isset($inouttime)) {

            $inouttime[0]['OUT'] = '';
            $inouttime[0]['in'] = '';
        }


//        print_r($inouttime);
//        die();
        $resultarray = array('model' => $model,
            'logemp' => $logemp, 'inout' => $userlog,
            'days' => $days, 'inouttime' => $inouttime,
            'precent_date' => $precent_date, 'lastsync' => $lastsync);
//        
        //print_r($resultarray);
//        die();
//        echo "<pre>";
        //print_r($outtime);
        //  print_r($userlog);
//        die();
        $this->render('//punchlog/view', array('model' => $model,
            'logemp' => $logemp, 'inout' => $userlog,
            'days' => $days, 'inouttime' => $inouttime,
            'precent_date' => $precent_date, 'lastsync' => $lastsync, 'resultarr' => $resultarray));
    }

    public function calculate_time_span($seconds) {
        $months = floor($seconds / (3600 * 24 * 30));
        $day = floor($seconds / (3600 * 24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours * 3600)) / 60);
        $secs = floor($seconds % 60);

        return $hours . ":" . $mins . ":" . $secs;

        $ret = array('h' => 0, 'm' => 0, 's' => 0);


        if ($hours >= 1) {
            $ret['h'] = $hours;
        }

        if ($seconds < 60)
            $time = $secs . " seconds ago";
        else if ($seconds < 60 * 60)
            $time = $mins . " min ago";
        else if ($seconds < 24 * 60 * 60)
            $time = $hours . " hours ago";
        else if ($seconds < 24 * 60 * 60)
            $time = $day . " day ago";
        else
            $time = $months . " month ago";

        return $time;
    }

    public function actionLogsender() {

        $url = "http://localhost/accesslog/timelog/index.php?r=accesslog/receivelog";

        $array = array('a' => 'apple', 'b' => 'balal', 2, 2, 2, 3, 3, 3, 3, 3);
        //open connection
        $ch = curl_init();

        $string = 'logitems=' . json_encode($array);
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $string);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
    }

    public function actionReceivelog() {
        if (isset($_REQUEST['logitems'])) {
            print_r(json_decode($_POST['logitems'], true));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $lastlogid = 0;

        if (isset($_POST['newlog'])) {
            //$query_part = str_replace("],[", '),(', str_replace(']]', ')', str_replace('[[', '(', $_POST['newlog'])));
            $logarray = json_decode($_POST['newlog'], true);

            $logids = array_keys($logarray);
            if (count($logids)) {
                $loglistid = implode(',', $logids);
                $existids = Yii::app()->db->createCommand("select logid from pms_punch_log where logid in ({$loglistid})")->queryAll();
                $duplicate_ids = array();

                foreach ($existids as $dids) {
                    $duplicate_ids[] = $dids['logid'];
                    unset($logarray[$dids['logid']]);
                }
                $logids = json_encode(array_values($logarray));
                $query_part = str_replace("],[", '),(', str_replace(']]', ')', str_replace('[[', '(', $logids)));
            }

            //$loglist = $_POST['newlog'];
            if ($query_part != '[]') {
                if (Yii::app()->db->createCommand("INSERT INTO `pms_punch_log` VALUES  {$query_part}")->query()) {
                    $lastlogid = Yii::app()->db->getLastInsertID();
                } else {
                    $lastlogid = "-1";
                }
            }
        }
        //  print_r($_POST);
        die($lastlogid);
    }

    protected function getEmployeeName($data, $row) {

//        $model = Users::model()->findByAttributes;
//        if ($model === null)
//            throw new CHttpException(404, 'The requested page does not exist.');
//        
//        
//        return $data->empid;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionMonth() {
        $model = new Accesslog();

        $hdays = 5;

        if (isset($_GET['hdays'])) {
            $hdays = intval($_GET['hdays']);
        }

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        date_default_timezone_set('Asia/Kolkata');

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        


        for ($i1 = 0; $i1 <= $hdays; $i1++) {

            $days = 0;
            if (isset($_GET['days']) && intval($_GET['days'])) {
                $days = intval($_GET['days']) + $i1;
            }
            if ($model->log_time != "") {
                $seldate = strtotime($model->log_time);
                $todaydate = strtotime(date("Y-m-d"));
                if ($seldate < $todaydate) {
                    $days = ($todaydate - $seldate) / (-3600 * 24);
                }
            }

            $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

            $precent_date = date("Y-m-d", $system_Date);
            $datewithtime = date("Y-m-d H:i:s", $system_Date);
//        echo "<br />";

            $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,reg_ip,"
                    . "concat(first_name,' ', last_name) as username, "
                    . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
                    . "and empid=accesscard_id) as count_punch ,(select log_time "
                    . "from pms_punch_log as pa where log_time  like '$precent_date%' "
                    . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
                    . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
                    . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
                    . "where accesscard_id!=0 and first_name not In ('Bala' , 'Amal', 'Sudesh') group by accesscard_id order by first_name";
//die();
            $command = Yii::app()->db->createCommand($sql);
            $logemp = $command->queryAll();

            $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
                    . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
                    . "group by empid,log_time order by empid asc, log_time asc";

            $command2 = Yii::app()->db->createCommand($sql2);
            $logemp2 = $command2->queryAll();



            $logrec = array();
            $userlog = array();

            $userid = 0;
            $prev_time = 0;
            $flag = 0;
            $outtime = array();
            foreach ($logemp2 as $rec) {
//            /$logrec[$rec['empid']][] = ($rec['log_time']);
//            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                //continue;            
                if ($userid != $rec['empid']) {
                    $flag = 0;
                    $userid = $rec['empid'];
                    $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                    $prev_time = strtotime($rec['log_time']);
                    //echo "<br/>";
                    $flag++;
                } else {
                    //echo $flag."<br/>";
                    //echo strtotime($rec['log_time']) . '-' . $prev_time;
//                die();
                    $timediffin = strtotime($rec['log_time']) - $prev_time;

                    $prev_time = strtotime($rec['log_time']);

                    if ($flag % 2 == 1) {
                        $userlog[$userid]['IN'] += $timediffin;
                    } else {
                        $userlog[$userid]['OUT'] += $timediffin;
                        $empid1 = $rec['empid'];
                        $outtime[$empid1] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                    }

                    // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                    $flag++;
                }
            }

            //print_r($userlog);
//        die();
//        echo "<pre>";
            //print_r($outtime);
            //  print_r($userlog);
//        die();
            echo $this->renderPartial('monthly', array('model' => $model,
                'logemp' => $logemp, 'inout' => $userlog,
                'days' => $days, 'outtime' => $outtime,
                'precent_date' => $precent_date), true);
            echo "<hr />";
        }
//         echo "<pre>";
//         print_r($logrec);
//         echo "</pre>";
    }

    public function actionView() {
        $model = new Accesslog();

        //die("<center><h1>Under Maintenance</h1></center>");

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
//        echo "<br />";

        $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,"
                . "concat(first_name,' ', last_name) as username, "
                . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
                . "and empid=accesscard_id) as count_punch ,(select log_time "
                . "from pms_punch_log as pa where log_time  like '$precent_date%' "
                . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
                . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
                . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
                . "where accesscard_id!=0 and first_name not In ('Bala' , 'Amal', 'Sudesh') and `status`=0 group by accesscard_id order by first_name";
//die();
        $command = Yii::app()->db->createCommand($sql);
        $logemp = $command->queryAll();

        $lastsysncsql = Yii::app()->db->createCommand('SELECT date_format(last_sync_time,"%d-%m-%Y %T")  FROM `pms_punch_log_last_sync`');
        $lastsync = $lastsysncsql->queryScalar();

        //print_r(  $lastsync); 

        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
                . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
                . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();



        $logrec = array();
        $userlog = array();

        $userid = 0;
        $prev_time = 0;
        $flag = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
//            /$logrec[$rec['empid']][] = ($rec['log_time']);
//            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
            //continue;            
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];
                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                //echo "<br/>";
                $flag++;
            } else {
                //echo $flag."<br/>";
                //echo strtotime($rec['log_time']) . '-' . $prev_time;
//                die();
                $timediffin = strtotime($rec['log_time']) - $prev_time;

                $prev_time = strtotime($rec['log_time']);

                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;
                } else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                    $outtime[$empid1] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                }

                // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                $flag++;
            }
        }

        //print_r($userlog);
//        die();
//        echo "<pre>";
        //print_r($outtime);
        //  print_r($userlog);
//        die();
        $this->render('view', array('model' => $model,
            'logemp' => $logemp, 'inout' => $userlog,
            'days' => $days, 'outtime' => $outtime,
            'precent_date' => $precent_date, 'lastsync' => $lastsync));

//         echo "<pre>";
//         print_r($logrec);
//         echo "</pre>";
    }

    public function actionViewall() {
        $model = new Accesslog();

        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }

        date_default_timezone_set('Asia/Kolkata');
        $ch = curl_init();
//        curl_setopt($curl, CURLOPT_URL, 'http://192.168.1.100/getlog.php');
        curl_setopt_array(
                $ch, array(
            CURLOPT_URL => 'http://192.168.1.100/getlog.php',
            CURLOPT_RETURNTRANSFER => true
        ));
        $output = curl_exec($ch);
        //echo $output;
        //$logemp = Accesslog::model()->findAll( array("condition" => "empid = 8 and log_time>=CURDATE()", "order" => "logid"));        

        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate - $seldate) / (-3600 * 24);
            }
        }

        $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
//        echo "<br />";

        $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,"
                . "concat(first_name,' ', last_name) as username, "
                . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
                . "and empid=accesscard_id) as count_punch ,(select log_time "
                . "from pms_punch_log as pa where log_time  like '$precent_date%' "
                . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
                . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
                . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
                . "where accesscard_id!=0 and first_name not In ('Bala1' , 'Amal1', 'Sudesh1') and `status`=0 group by accesscard_id order by first_name";
//die();
        $command = Yii::app()->db->createCommand($sql);
        $logemp = $command->queryAll();

        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
                . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
                . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();

        $logrec = array();
        $userlog = array();

        $userid = 0;
        $prev_time = 0;
        $flag = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
//            /$logrec[$rec['empid']][] = ($rec['log_time']);
//            $logrec[$rec['empid']][] = strtotime($rec['log_time']);
            //continue;            
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];
                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                //echo "<br/>";
                $flag++;
            } else {
                //echo $flag."<br/>";
                //echo strtotime($rec['log_time']) . '-' . $prev_time;
//                die();
                $timediffin = strtotime($rec['log_time']) - $prev_time;

                $prev_time = strtotime($rec['log_time']);

                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;
                } else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                    $outtime[$empid1] = $this->calculate_time_span($userlog[$empid1]['OUT']);
                }

                // $logrec[$rec['empid']][] = strtotime($rec['log_time']);
                $flag++;
            }
        }

        //print_r($userlog);
//        die();
//        echo "<pre>";
        //print_r($outtime);
        //  print_r($userlog);
//        die();
        $this->render('view', array('model' => $model,
            'logemp' => $logemp, 'inout' => $userlog,
            'days' => $days, 'outtime' => $outtime,
            'precent_date' => $precent_date));

//         echo "<pre>";
//         print_r($logrec);
//         echo "</pre>";
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Accesslog;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Accesslog'])) {
            $model->attributes = $_POST['Accesslog'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->logid));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Accesslog'])) {
            $model->attributes = $_POST['Accesslog'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->logid));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Accesslog('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Accesslog']))
            $model->attributes = $_GET['Accesslog'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Accesslog::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'accesslog-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
