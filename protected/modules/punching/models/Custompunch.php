<?php

/**
 * This is the model class for table "{{custompunch}}".
 *
 * The followings are the available columns in table '{{custompunch}}':
 * @property integer $customid
 * @property string $customdate
 * @property string $customtime
 * @property integer $resource
 * @property integer $device
 * @property string $remark
 * @property string $createddate
 * @property string $modifieddate
 * @property integer $createdby
 *
 * The followings are the available model relations:
 * @property PunchingDevices $device0
 */
class Custompunch extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Custompunch the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{custompunch}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('customdate, customtime, resource, remark, createddate, modifieddate, createdby,device', 'required'),
            array('resource, device, createdby', 'numerical', 'integerOnly'=>true),
            array('remark', 'length', 'max'=>200),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('customid, customdate, customtime, resource, device, remark, createddate, modifieddate, createdby', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'resource0' => array(self::BELONGS_TO, 'Users', 'resource'),
            'device0' => array(self::BELONGS_TO, 'PunchingDevices', 'device'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'customid' => 'Customid',
            'customdate' => 'Customdate',
            'customtime' => 'Customtime',
            'resource' => 'Resource',
            'device' => 'Device',
            'remark' => 'Remark',
            'createddate' => 'Createddate',
            'modifieddate' => 'Modifieddate',
            'createdby' => 'Createdby',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('customid',$this->customid);
        $criteria->compare('customdate',$this->customdate,true);
        $criteria->compare('customtime',$this->customtime,true);
        $criteria->compare('resource',$this->resource);
        $criteria->compare('device',$this->device);
        $criteria->compare('remark',$this->remark,true);
        $criteria->compare('createddate',$this->createddate,true);
        $criteria->compare('modifieddate',$this->modifieddate,true);
        $criteria->compare('createdby',$this->createdby);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}