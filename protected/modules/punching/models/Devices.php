<?php

/**
 * This is the model class for table "{{punching_devices}}".
 *
 * The followings are the available columns in table '{{punching_devices}}':
 * @property integer $device_id
 * @property string $device_name
 * @property string $shot_name
 */
class Devices extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PunchingDevices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{punching_devices}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		 return array(
            array('device_id, device_name, shot_name,site_id,start_date', 'required'),
            array('device_id, site_id', 'numerical', 'integerOnly'=>true),
            array('device_name', 'length', 'max'=>25),
            array('shot_name', 'length', 'max'=>15),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('device_id, device_name, shot_name, site_id,start_date, end_date', 'safe', 'on'=>'search'),
            );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                //'site' => array(self::BELONGS_TO, 'Clientsite', 'site_id'),    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'device_id' => 'Device ID',
			'device_name' => 'Device Name',
			'shot_name' => 'Shot Name',
                        'site_id' => 'Site',
                        'start_date' => 'Start Date',
                        'end_date' => 'End Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('device_name',$this->device_name,true);
		$criteria->compare('shot_name',$this->shot_name,true);
                $criteria->compare('site_id',$this->site_id);
                $criteria->compare('start_date',$this->start_date,true);
                $criteria->compare('end_date',$this->end_date,true);
                
          $criteria->condition = 't.nodevice_status IS NULL';     

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getSiteName($id){
             $model = Clientsite::model()->findByPk($id);
                if(is_null($model)){
                    return "None";
                }else{
                    return $model->site_name;
                }     
        }
        
}