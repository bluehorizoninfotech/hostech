<?php
/* @var $this HolidaysController */
/* @var $model Holidays */

$this->breadcrumbs=array(
	'Holidays'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Holidays', 'url'=>array('admin')),
	array('label'=>'Create Holidays', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('holidays-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Holidays</h1>


<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'holidays-grid',
	'itemsCssClass' =>'greytable',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
	    	array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
  //	'id',
		'holiday_date',
		'title',
		//'type',
	   //	'flag',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
)); ?>
