
<?php if((in_array('/holiday/create', Yii::app()->session['menuauthlist']))){ ?>
<div class="form pull-left margin-right-20 left-form-border padding-right-20 width-250">

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'holidays-form',
    'enableAjaxValidation' => false,
));?>

	    <h1 class="holiday-subhead">Add Holidays</h1>
		<?php if (isset($_GET['exist'])) {?>
			<div class="exist">
			<p class="exist_error">Date Already exist</p>
			</div>

		<?php }?>
        <?php echo $form->errorSummary($model2); ?>

		<div class="row">
        <?php 
        if(isset($model2->holiday_date)){
        $model2->holiday_date=date('d-m-Y',strtotime($model2->holiday_date));
        }
        ?>
			<?php echo $form->labelEx($model2, 'holiday_date'); ?>
			<?php //echo $form->textField($model,'holiday_date'); ?>
			<?php //echo $form->error($model,'holiday_date'); ?>
      <?php echo CHtml::activeTextField($model2, 'holiday_date', array("id" => "holiday_date", "size" => "15", 'class' => 'form-control', 'autocomplete' => 'off')); ?>
      <?php
$this->widget('application.extensions.calendar.SCalendar', array(
    'inputField' => 'holiday_date',
    'ifFormat' => '%d-%m-%Y',
));
    ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model2, 'title'); ?>
			<?php echo $form->textField($model2, 'title', array('size' => 20, 'maxlength' => 100, 'class' => 'form-control')); ?>
			<?php echo $form->error($model2, 'title'); ?>
		</div>

		<div class="row buttons holiday-create-button">
			<?php echo CHtml::submitButton($model2->isNewRecord ? 'Create' : 'Save', array('class' => "btn btn-primary ")); ?>
		</div>

	<?php $this->endWidget();?>

</div>

<?php } ?>


<div class="pull-left width-60-percentage">

	 <h1 class="holidays-table-head">Holidays</h1>

	<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search', array(
    'model' => $model,
));?>
	</div><!-- search-form -->

	<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'holidays-grid',
    'itemsCssClass' => 'greytable table-bordered',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.'),
        array(
            'name' => 'holiday_date',
            'value' => 'Yii::app()->dateFormatter->format("d-M-y",$data->holiday_date)',

        ),
        'title',
        array(
            'name' => 'status',
            'value' => '$data->getStatus($data->status)',
        ),

        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',

            'buttons' => array(
                'update' => array
                (
                    'url' => 'Yii::app()->createUrl("punching/holidays/index", array("id"=>$data->id))',
                    'imageUrl' => false,
                    'label' => false,
                    'options' => array('class' => 'icon icon-pencil black-color holiday-action-button', 'title' => 'Edit'),
                ),

                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("punching/holidays/delete", array("id"=>$data->id))',
                    'imageUrl' => false,
                    'label' => false,
                    'options' => array('class' => 'icon icon-trash black-color margin-left-5 holiday-action-button', 'title' => 'Delete'),
                ),
            ),
		),
		array(
            'class' => 'CButtonColumn',
			'template' => '{approve}{reject}',
			'visible'=>($user_type==1)?true:false,
            'buttons' => array(
                'approve' => array
                (
                    'url' => 'Yii::app()->createUrl("punching/holidays/takeAction", array("id"=>$data->id,"status"=>1))',
                    'imageUrl' => false,
					'label' => false,
					'visible'=>'$data->status==0?true:false',
                    'options' => array('class' => ' fa fa-thumbs-up approve', 'title' => 'Approve'),
                ),

                'reject' => array
                (
                    'url' => 'Yii::app()->createUrl("punching/holidays/takeAction", array("id"=>$data->id,"status"=>2))',
                    'imageUrl' => false,
					'label' => false,
					'visible'=>'$data->status==0?true:false',
                    'options' => array('class' => ' fa fa-thumbs-down reject', 'title' => 'Reject'),
                ),
            ),
		),
    ),
));?>



</div>

<script type="text/javascript">

	$(document).ready(function(){
	   $('input[type=text], textarea').keyup(function () {

				$(this).val($(this).val().toUpperCase());

		 })

	});

</script>
