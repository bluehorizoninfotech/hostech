<?php
/* @var $this PunchingDevicesController */
/* @var $model PunchingDevices */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'punching-devices-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'device_name'); ?>
		<?php echo $form->textField($model,'device_name',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'device_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shot_name'); ?>
		<?php echo $form->textField($model,'shot_name',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'shot_name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->