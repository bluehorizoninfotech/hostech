<?php
/* @var $this DevicesController */
/* @var $model Devices */

$this->breadcrumbs=array(
	'Punching Devices'=>array('index'),
	$model->device_id,
);

$this->menu=array(
	array('label'=>'List Devices', 'url'=>array('index')),
	array('label'=>'Create Devices', 'url'=>array('create')),
	array('label'=>'Update Devices', 'url'=>array('update', 'id'=>$model->device_id)),
	array('label'=>'Delete Devices', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->device_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Devices', 'url'=>array('admin')),
);
?>

<h1>View Devices #<?php echo $model->device_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'device_id',
		'device_name',
		'shot_name',
	),
)); ?>
