<?php
/* @var $this PunchingDevicesController */
/* @var $data PunchingDevices */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('device_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->device_id), array('view', 'id'=>$data->device_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('device_name')); ?>:</b>
	<?php echo CHtml::encode($data->device_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shot_name')); ?>:</b>
	<?php echo CHtml::encode($data->shot_name); ?>
	<br />


</div>