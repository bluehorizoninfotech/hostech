<?php
/* @var $this DevicesController */
/* @var $model Devices */

$this->breadcrumbs=array(
	'Punching Devices'=>array('index'),
	$model->device_id=>array('view','id'=>$model->device_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Devices', 'url'=>array('index')),
	array('label'=>'Create Devices', 'url'=>array('create')),
	array('label'=>'View Devices', 'url'=>array('view', 'id'=>$model->device_id)),
	array('label'=>'Manage Devices', 'url'=>array('admin')),
);
?>

<h1>Update Devices <?php echo $model->device_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>