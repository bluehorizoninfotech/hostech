<br clear='all' />
<div class="form pull-left margin-right-20 left-form-border padding-right-20 width-250">
    <h1> <?php echo ($model2->isNewRecord ? 'Add New Device' : 'Update device details') ?></h1>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'punching-devices-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php //echo $form->errorSummary($model2); ?>

    <div class="row">
        <?php echo $form->labelEx($model2, 'device_id'); ?>
        <?php echo $form->textField($model2, 'device_id', array('size' => 25, 'maxlength' => 25,'class'=>'form-control')); ?>
        <?php echo $form->error($model2, 'device_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model2, 'device_name'); ?>
        <?php echo $form->textField($model2, 'device_name', array('size' => 25, 'maxlength' => 25,'class'=>'form-control')); ?>
        <?php echo $form->error($model2, 'device_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model2, 'shot_name'); ?>
        <?php echo $form->textField($model2, 'shot_name', array('size' => 25, 'maxlength' => 15,'class'=>'form-control')); ?>
        <?php echo $form->error($model2, 'shot_name'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model2, 'site_id'); ?>
               <?php 		
		 $sites = CHtml::listData(Clientsite::model()->findAll(array(
			'select' => array('id,site_name'),
			'order' => 'site_name',
			'distinct' => true
		)), 'id', 'site_name');
                 ?>
	  
	   <?php echo $form->dropDownList($model2, 'site_id', $sites, array('class' => 'form-control input-medium','empty' => 'Select Device')); ?>
        <?php echo $form->error($model2, 'site_id'); ?>
    </div>
    
    
    
    <?php echo $form->hiddenField($model2, 'start_date',array('value'=>date('Y-m-d'))); ?>


    <div class="row buttons">
        <?php echo CHtml::submitButton($model2->isNewRecord ? 'Add new device' : 'Update', array('class'=>'btn blue add-device-button')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<div class="pull-left width-50-percentage">

    <h1 class='pull-left device-subhead'>Punching Devices</h1>
    <?php if (Yii::app()->user->hasFlash('success')) { ?>

    <div class="flash-success margin-left-20 padding-top-0 padding-bottom-0 padding-left-20 padding-right-20 green-color-border green-color font-15 pull-left">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
        <?php
    }
    ?>
    <?php
   // die;
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'punching-devices-grid',
        'itemsCssClass' => 'table table-bordered',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array('class' => 'IndexColumn', 'header' => 'S.No',),
            array('name' => 'device_id', 'htmlOptions' => array('width' => '40px', 'class' => 'font-weight-bold text-align-center')),
            'device_name',
            'shot_name',
            array(
           'name'=> 'site_id',
           'htmlOptions' => array('width' => '80px'),     
           //'value' => '$data->site_id != "" ? $data->site->site_name : 0', 
           //'value' => '(isset($data->site)? $data->site->site_name : 0 )',     
            'value' => '$data->getSiteName($data->site_id)',    
           'filter' => CHtml::listData(Clientsite::model()->findAll(array(
			'select' => array('id,site_name'),
			'order' => 'site_name',
			'distinct' => true
		)), 'id', 'site_name')   
            ),
            'start_date',
            'end_date',
            array(
                'class' => 'CButtonColumn',
                'htmlOptions'=>array('class'=>'width-100'),
                //'template' => '{update}',
                'buttons' => array(
                    'update' => array
                        (
                        'label'=>'',
                        'imageUrl' =>false,
                        'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit'),
                        'url' => 'Yii::app()->createUrl("punching/devices/index", array("id"=>$data->id))',
                    ),
                    'view' => array(
                        'label'=>'',
                        'imageUrl' =>false,
                        'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                    ),
                    'delete' => array(
                        'label'=>'',
                        'imageUrl' =>false,
                        'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete',),
                    ),
                    
                ),
            ),
            /**/
        ),
    ));
    ?>

</div>
