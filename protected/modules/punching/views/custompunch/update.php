<?php
/* @var $this CustompunchController */
/* @var $model Custompunch */

$this->breadcrumbs=array(
	'Custompunches'=>array('index'),
	$model->customid=>array('view','id'=>$model->customid),
	'Update',
);

//$this->menu=array(
//	array('label'=>'List Custompunch', 'url'=>array('index')),
//	array('label'=>'Create Custompunch', 'url'=>array('create')),
//	array('label'=>'View Custompunch', 'url'=>array('view', 'id'=>$model->customid)),
//	array('label'=>'Manage Custompunch', 'url'=>array('admin')),
//);
?>

<h1>Update Custompunch <?php //echo $model->customid; ?></h1>

<ol class="breadcrumb">
    <li><?php echo CHtml::link('Punching log',array('accesslog/punchlog')); ?> / </li>
    <li><?php echo CHtml::link('Custom Punch',array('custompunch/admin')); ?></li>
    <li>Update Custom Punch</li>      
  </ol>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>