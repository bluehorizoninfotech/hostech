<?php
/* @var $this CustompunchController */
/* @var $model Custompunch */

$this->breadcrumbs=array(
	'Custompunches'=>array('index'),
	$model->customid,
);

$this->menu=array(
	array('label'=>'List Custompunch', 'url'=>array('index')),
	array('label'=>'Create Custompunch', 'url'=>array('create')),
	array('label'=>'Update Custompunch', 'url'=>array('update', 'id'=>$model->customid)),
	array('label'=>'Delete Custompunch', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->customid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Custompunch', 'url'=>array('admin')),
);
?>

<h1>View Custompunch #<?php echo $model->customid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'customid',
		'customdate',
		'customtime',
		'resource',
		'remark',
		'createddate',
	),
)); ?>
