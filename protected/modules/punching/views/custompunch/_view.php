<?php
/* @var $this CustompunchController */
/* @var $data Custompunch */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('customid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->customid), array('view', 'id'=>$data->customid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customdate')); ?>:</b>
	<?php echo CHtml::encode($data->customdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customtime')); ?>:</b>
	<?php echo CHtml::encode($data->customtime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('resource')); ?>:</b>
	<?php echo CHtml::encode($data->resource); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remark')); ?>:</b>
	<?php echo CHtml::encode($data->remark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createddate')); ?>:</b>
	<?php echo CHtml::encode($data->createddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modifieddate')); ?>:</b>
	<?php echo CHtml::encode($data->modifieddate); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->CreatedBy); ?>
	<br />

	*/ ?>

</div>