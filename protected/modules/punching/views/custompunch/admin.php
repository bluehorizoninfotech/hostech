<?php
/* @var $this CustompunchController */
/* @var $model Custompunch */

$this->breadcrumbs=array(
	'Custompunches'=>array('index'),
	'Manage',
);

//$this->menu=array(
//	array('label'=>'List Custompunch', 'url'=>array('index')),
//	array('label'=>'Create Custompunch', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('custompunch-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="clearfix">
    <?php
   $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
   echo CHtml::link('Add', '', array('class' => 'btn blue edittime pull-right', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
   ?>
<h1>Manual Punch Log</h1>
</div>
 <ol class="breadcrumb">
    <li><?php echo CHtml::link('Punch log',array('accesslog/punchlog')); ?> / </li>
    <li>Manual Punch Log</li>
          
  </ol>

   <?php
//--------------------- begin new code --------------------------
// add the (closed) dialog for the iframe
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
   'id' => 'cru-dialog',
   'options' => array(
       'title' => 'Add Custom Punch',
       'autoOpen' => false,
       'modal' => false,
       'width' => 450,
       'height' => 500,
   ),
));
?>


<iframe id="cru-frame" width="400" height="438" scrolling="auto"></iframe>

<?php
$this->endWidget();
?>



<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<div class="search-form display-none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'custompunch-grid',
        'itemsCssClass' => 'table table-bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'customid',
                array(
			'name' => 'resource', 
			'value'=> 'isset($data->resource0) ? $data->resource0->first_name." ".$data->resource0->last_name : ""',
			'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,CONCAT_WS(" ", first_name, last_name) as first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
			),
		array('name' => 'customdate', 'value'=> '$data->customdate." ".$data->customtime'),
		array(
			'name' => 'resource', 
			'value'=> 'isset($data->resource0) ? $data->resource0->first_name." ".$data->resource0->last_name : ""',
			'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,CONCAT_WS(" ", first_name, last_name) as first_name'),
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name")
			),
                        'device',
//                      	array(
//			'name' => 'device', 
//			'value'=> '(isset($data->device0) ? $data->device0->device_name: "")',
//			'filter' => CHtml::listData(Devices::model()->findAll(
//                            array(
//                                'select' => array('device_id,device_name'),
//                                'order' => 'device_name',
//                                'distinct' => true
//                    )), "device_id", "device_name")
//			),  
		//'resource',
                
		'remark',
		'createddate',
		/*
		'ModifiedDate',
		'CreatedBy',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template' => '{update}{delete}',
                        'buttons'=>array(
                            'update' => array(
                                'label'=>'',
                                'imageUrl' =>false,
                                'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit'),
                               ),
                            'delete' => array(
                                'label'=>'',
                                'imageUrl' =>false,
                                'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete',),
                               ),
                        ),    
		),
	),
)); ?>