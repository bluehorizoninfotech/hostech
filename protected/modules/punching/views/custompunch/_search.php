<?php
/* @var $this CustompunchController */
/* @var $model Custompunch */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'customid'); ?>
		<?php echo $form->textField($model,'customid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customdate'); ?>
		<?php echo $form->textField($model,'customdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customtime'); ?>
		<?php echo $form->textField($model,'customtime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'resource'); ?>
		<?php echo $form->textField($model,'resource'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remark'); ?>
		<?php echo $form->textField($model,'remark',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createddate'); ?>
		<?php echo $form->textField($model,'createddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modifieddate'); ?>
		<?php echo $form->textField($model,'modifieddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdby'); ?>
		<?php echo $form->textField($model,'createdby'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->