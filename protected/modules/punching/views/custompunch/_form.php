<?php
/* @var $this CustompunchController */
/* @var $model Custompunch */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'custompunch-form',
	'enableAjaxValidation'=>false,
)); ?>

<!--	<p >Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>


  <div class="row">
    <?php echo $form->labelEx($model,'resource'); ?>
      <?php
      if($model->isNewRecord){ ?>
      <br>
      <?php } ?>
    <?php //echo $form->dropDownList($model, 'resource', CHtml::listData(Users::model()->findAll(), 'userid', 'username'));  ?>
    <?php echo $form->dropDownList($model, 'resource', CHtml::listData(Users::model()->findAll(array(
                                           'select' => array('userid,CONCAT_WS(" ", first_name, last_name) as first_name'),
                                          // 'condition' => 'status=0',
                                           'order' => 'first_name',
                                           'distinct' => true
                               )), 'userid', 'first_name'), array( 'class'=>'form-control width-200', 'empty' => '--',
                                  'ajax' => array(
                                    'type'=>'POST', 
                                    'dataType' => 'JSON',
                                    'url'=>Yii::app()->createUrl('punching/custompunch/getDevices'),
                                    //'update'=>'#Custompunch_device', 
                                    'data'=>array('userid'=>'js:this.value'),
                                    'success' => 'function(data) {
                                         $("#Custompunch_device").html(data.devices);
                                     }'
                                     )  
                                   
                                   
                                   
                                   )); ?>

    <?php echo $form->error($model,'resource'); ?>
  </div>
        
        
          <div class="row">
    <?php echo $form->labelEx($model,'device'); ?>
      <?php
      if($model->isNewRecord){ ?>
      <br>
      <?php } ?>
    <?php //echo $form->dropDownList($model, 'resource', CHtml::listData(Users::model()->findAll(), 'userid', 'username'));  ?>
    <?php echo $form->dropDownList($model, 'device', CHtml::listData(Devices::model()->findAll(array(
                                           'select' => array('device_id,device_name'),
                                          // 'condition' => 'status=0',
                                           'order' => 'device_name',
                                           'distinct' => true
                               )), 'device_id', 'device_name'), array( 'class'=>'form-control width-200','empty' => '--')); ?>

    <?php echo $form->error($model,'device'); ?>
  </div>
        

	<div class="row">
		<?php
                echo $form->labelEx($model,'customdate');  ?>
           <?php
      if($model->isNewRecord){ ?>
      <br>
      <?php } ?>
                <?php echo $form->error($model,'customdate'); ?>
    <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',array(
        'model'=>$model, //Model object
        'attribute'=>'customdate', //attribute name
        'language'=>'en-AU',
                'mode'=>'date', //use "time","date" or "datetime" (default)
        'options'=>array(
        'dateFormat'=>'yy-mm-dd'), // jquery plugin options
        'htmlOptions'=>array(
            'class'=>'form-control width-200' 
            ),
    ));
?>  
    
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'customtime'); ?>
		<?php
      if($model->isNewRecord){ ?>
      <br>
      <?php } ?>
		<?php echo $form->error($model,'customtime'); ?>

		
		<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker', array(
                       'model' => $model, //Model object
                       'attribute' => 'customtime', //attribute name
                       'mode' => 'time', //use "time","date" or "datetime" (default)
                       'options' => array(), // jquery plugin options
                        'htmlOptions'=>array(
                                'class'=>'form-control width-200'
                                ),
                   ));
?>	
	</div>
        
	

	<div class="row">
		<?php echo $form->labelEx($model,'remark'); ?>
                <?php
      if($model->isNewRecord){ ?>
      <br>
      <?php } ?>
		<?php echo $form->textArea($model, 'remark', array('rows' => 6, 'cols' => 50)); ?>	
		<?php echo $form->error($model,'remark'); ?>
	</div>
        <br>
<?php echo $form->hiddenField($model, 'createddate',array('value'=>date('Y-m-d'))); ?>
<?php echo $form->hiddenField($model, 'modifieddate',array('value'=>date('Y-m-d'))); ?>
<?php echo $form->hiddenField($model, 'createdby',array('value'=>Yii::app()->user->id)); ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' =>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->