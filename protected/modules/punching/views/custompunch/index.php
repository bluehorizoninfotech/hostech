<?php
/* @var $this CustompunchController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Custompunches',
);

$this->menu=array(
	array('label'=>'Create Custompunch', 'url'=>array('create')),
	array('label'=>'Manage Custompunch', 'url'=>array('admin')),
);
?>

<h1>Custom punches</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
