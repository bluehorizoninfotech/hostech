<?php
//echo "<pre>";
//print_r($logresult);
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
<style type="text/css">
    .logtable td, th{ border: 1px solid #555 !important;}
    .logtable {border-collapse: collapse}
    .odd{background-color: #e2e2e2;font-size: 12px;}
    .even{font-size: 12px;}
    .tablehead{background-color: #c9e0ed;}
    .logtable td{padding:5px !important;}
    .datetime span{color:#FF8080;}
    #punchlogs{float:left;background-color:#D0E9F5;font-size: 20px;border:1px solid blue;margin-left:10px;padding:20px;}
    .circle {
        width: 50px;
        height: 50px;
        border-radius: 50%;
        font-size: 15px;
        color: #fff;
        line-height:50px;
        text-align: center;
        font-weight: bold;
        background: #c2c2c2;
    }
    .OUT, .red{
        background: #FF8080;
    }
    .IN{
        background: green;
    }

    .greeting {float:right; margin-left:40px;color:blue;}
    #table { 
        box-sizing: border-box; 
    }
    #table tr.cuser,#table tbody tr:hover {
        box-shadow: 0px 0px 10px green;
        -webkit-box-shadow: 0px 0px 5px #00ff00;
        -moz-box-shadow: 0px 0px 5px #00ff00;
    }
    #table tr th{font-size:12px; }
    .glyphicon-ok{
        color:blue; font-size:20px;
    }
    #table tr td{vertical-align:middle}
    .half{
        font-weight:bold;
        color:red;
    }
</style>

<div style="float:left;width:30%;">
    <h1 style="float:left;margin-right:30px; ">Punching log</h1>
    <br clear="all"/>
    <span class="half">HL</span> - Half day leave/Early exit
</div>

<div style="float:left;width:30%;">
    <h1 style="float:left;margin-right:30px;color:blue; border: double green;padding:10px;"><?php echo $punchdate ?>  </h1>
</div>


  <?php
   if(Yii::app()->user->role==1){ ?>
   <div style="float:left;width:15%;">
  <h4 style="float:left;margin-right:30px; "><a href="<?php echo Yii::app()->createUrl('punching/custompunch/admin'); ?>">Add Custom Punch</a>
  </h4>
</div>
   <?php }
    ?>

 <div style="float:left;width:15%;">
     
    <?php
   
   if(Yii::app()->user->role==1){
    //print_r($accessuserdata);
    echo CHtml::dropDownList('code', '', CHtml::listData($sitedata, 'id', 'site_name'), array('style' => 'border: 1px solid #ccc;font-size:15px;width: 180px', 'options' => array($code => array('selected' => true)), 
    'empty' => '-- select site --',
    'onchange' => 'document.location.href = "index.php?r=punching/accesslog/punchlog&code=" + this.value+"&days='.$days.'"',

    ));
   }
    ?></div>



<div class="circle punchout" style="margin:0px auto; float:left;">--</div>
<br clear='all'/>

<div style="float:left;width:80%;">
    <table cellpadding="10" cellspacing="0" border="1" class="logtable" id="table">
        <thead>
            <tr style="font-size:15px;">
                <td colspan="4" style="border:0px !important;">
                    <?php
                    if (!isset($_COOKIE['notification'])) {
                        setcookie('notification', true, time() + 50 * 60 * 60);
                        ?>
                        <div style="border:1px solid red; padding:10px;float:left;background-color: #ff8080;color:white;display:none;">Please clean your desk and computer</div>
                        <?php
                    }
                    ?>                
                </td>
                <td colspan="7" style='text-align:right;border:0px !important;font-weight:bold;'>
                    <?php
                    if (isset($_GET['days'])) {
                        $days = $_GET['days'];
                    } else {
                        $days = 0;
                    }
                    $action = 'punchlog';
                    ?>    

                    &laquo; &laquo;   <?php echo CHtml::link('Previous',array('accesslog/' . $action ,'days' => $days - 1)) ?> |
                    <?php
                    if ($days >= 0) {
                        echo 'Today | Next &raquo; &raquo;';
                    } else { 
                        ?>
                    <?php echo CHtml::link('Today',array('accesslog/' . $action ,'days' => 0)) ?> | 
                        <?php echo CHtml::link('Next',array('accesslog/' . $action ,'days' => $days + 1)) ?>
                        <?php
                    }
                    ?>

                    &nbsp;&nbsp;&nbsp;&nbsp; <b><?php echo CHtml::link('Go to my monthly log &raquo;', array('accesslog/mylog'), array('style' => 'color:red;font-size:15px;')) ?> </b>    
                </td></tr>


            <?php
            if ($noentries == 0) {
                ?>
                <tr class="tablehead"><th width="25" colspan="11">No entries</th></tr>

                <?php
            } else {
                ?>

                <tr class="tablehead"><th width="25">Sno#</th><th id='id_header'>Access Card Id</th><th id='name_header'>Resource Name</th><th id='status_header'> status</th><th>First Punch time</th>
                    <th>Last Punch time</th><th>Total Out Time</th><th>Total IN Time</th>
                    <th>Total Hrs</th><th>Total Punch</th><th>Leave</th></tr>
            </thead>
            <tbody>  
                <?php
                
                extract($datetimesettings);

                $k = 1;
                $i = 0;
                
                foreach ($logresult as $row) {
                    
                    
                    if (isset($inlate)) {
                        unset($inlate);
                    }
                    if (isset($outearly)) {
                        unset($outearly);
                    }
                        
                    extract($row);
                    echo '<tr class="' . ($i % 2 == 1 ? 'odd' : 'even') . ' getpunch ' . (Yii::app()->user->id == $userid ? 'pmsuser' : '') . '" id="user_' . $accesscard_id . '" style="cursor:pointer;">'
                    . '<td>' . $k++ . '</td><td width="50" style="text-align:center" bgcolor="#ADD6FF">' .(isset($entrytype) ? 'M' : $accesscard_id) . '</td>'
                    . '<td width="200"><b>' . $emp_name . '</b>' . (Yii::app()->user->id == $userid ? CHtml::image('images/tick.jpg', $emp_name, array('width' => "20", 'align' => 'right')) : '') . '</td>';

                    if ($first_punch == '' && $last_punch == '') {
                        echo '<td width="150" style="color:red;" colspan="7">No Punch</td>';
                        $leave_count = 1;
                    } else {
//                        //echo $entrytype; 
//                        echo $emp_name;
//                        //echo $devices[$device_id];
//                        echo $entrytype;
//                        
//                       // echo (!isset($entrytype) ? 'Manual Entry' : $devices[$device_id]  );
//                        (isset($devices[$device_id]) ? (!isset($entrytype) ? 'Manual Entry' : $devices[$device_id]  )  : 'Manual Entry' );
//                        die;
                        $HL = ' <span class="half">HL</span>';
                        $hday_mor='';
                        $leave_count = 0;
                        if (strtotime($first_punch) > strtotime($punchdate . " " . $late_halfday)) {
                            $hday_mor = $HL;
                            $leave_count +='0.5';
                        }

                        $hday_noon = '';

                        $lpsec = strtotime($last_punch);
                        $ee_hsec = strtotime($punchdate . " " . $ee_halfday);

                        if (($lpsec < $ee_hsec && $ee_hsec != strtotime(date('Y-m-d 16:00:00')) && $status % 2 == 0)
                                OR ( $lpsec < $ee_hsec && strtotime($punchdate) < strtotime(date('Y-m-d')) && $status % 2 == 0)
                                OR ( $lpsec < $ee_hsec && strtotime(date('Y-m-d H:i:s')) > strtotime(date('Y-m-d ' . $ee_halfday)) && $status % 2 == 0)
                        ) {
                            $hday_noon = $HL;
                            $leave_count +='0.5';
                        }                    
                       
                        echo '<td width="150" bgcolor="' . ($status == 1 ? '#8AE62E' : '#FF8080') . '">' . ($status % 2 == 1 ? 'Punched IN' : 'Punched OUT') . '</td>';

                        
                       // $siteofdevices
                       // $sitedata
                       $device_id = intval($device_id);
                       $lsite = '<br /><small>No Outpunch</small>';
                       if(isset($ldevice_id)){
                           //echo $ldevice_id;
                           $lsite = ((isset($siteofdevices[$ldevice_id]) and isset($sitedata[$siteofdevices[$ldevice_id]]))?$sitedata[$siteofdevices[$ldevice_id]]:'');   
                          $lsite= "<br/><b><small>(".$lsite.")</small></b>";
                       }
                       
                         $fpun = ((isset($siteofdevices[$device_id]) and isset($sitedata[$siteofdevices[$device_id]]))?$sitedata[$siteofdevices[$device_id]]:'');   
//                       echo $sitedata[$siteofdevices[$device_id]];
//                       echo $siteofdevices[$device_id];
//                       print_r($sitedata);
//                       print_r($siteofdevices);
//                       die;
                        //$fpun = in_array($device_id, $siteofdevices);
                  
                        echo '<td width="140" style="' . (isset($inlate) ? 'background-color:#f69c55' : '') . '">' . date('h:i:s A', strtotime($first_punch)) . $hday_mor ." <br><b><small>(".$fpun.")</small></b>". '</td>';
                        //same single punch check
                        if(date('h:i:s A', strtotime($first_punch)) == date('h:i:s A', strtotime($last_punch))){
                        echo '<td width="140" style="' . (isset($outearly) && $outearly == 1 ? 'background-color:#f69c55' : '') . '">' . date('h:i:s A', strtotime($last_punch)) . $hday_noon . " <br><b><small>(".$fpun.")</small></b>".  '</td>';
                        }else{
                        echo '<td width="140" style="' . (isset($outearly) && $outearly == 1 ? 'background-color:#f69c55' : '') . '">' . date('h:i:s A', strtotime($last_punch)) . $hday_noon . $lsite.  '</td>';
                        }
                        echo '<td width="80">' . gmdate('H:i:s', $outsec) . '</td>'
                        . '<td width="80" bgcolor="' . (($insec + $outsec) / (60 * 60) < 8 ? '#f69c55' : '#ADD6FF') . '">' . gmdate('H:i:s', $insec) . '</td>';
						/*
						 if (isset($entrytype) && strlen($entrytype) >= 10){
                                $entrytype_full = $entrytype; 
                                $entrytype = substr($entrytype, 0, 9). "..";  
                            }   
                        echo '<td width="140" style="' . (isset($inlate) ? 'background-color:#f69c55' : '') . '">' . date('h:i:s A', strtotime($first_punch)) . $hday_mor ." <br><b>(".(isset($devices[$device_id]) ? (isset($entrytype) ? '<a style="text-decoration:none;color:black;" title="'.$entrytype_full.'">'.$entrytype.'</a>' : '<a style="text-decoration:none;color:black;" title="'.$devices[$device_id].'">'.$devices[$device_id].'</a>'  )  : 'Manual Entry' ).")</b>". '</td>'
                        . '<td width="140" style="' . (isset($outearly) && $outearly == 1 ? 'background-color:#f69c55' : '') . '">' . date('h:i:s A', strtotime($last_punch)) . $hday_noon . (isset($devices['ldevice_id'])? " <b>(".$devices['ldevice_id'].")</b>":"").  '</td>'
    					. '<td width="80">' . gmdate('H:i:s', $outsec) . '</td>'
                        . '<td width="80" bgcolor="' . (($insec + $outsec) / (60 * 60) < 8 ? '#f69c55' : '#ADD6FF') . '">' . gmdate('H:i:s', $insec) . '</td>';
						*/ 

                        echo '<td width="60" bgcolor="' . (($insec + $outsec) / (60 * 60) < 9 ? '#f69c55' : '#ADD6FF') . '">' . gmdate('H:i:s', $insec + $outsec) . '</td>'
                        . '<th style="color:' . ($count % 2 == 1 ? 'Green' : 'Red') . ';text-align:center;">' . $count . '</th>';
                    }
                    echo '<td>'.($leave_count>0?'<span class="half">'.$leave_count.'</span>':'').'</td>';
                    echo "</tr>";
                    $i++;
                }
            }
            ?>
        </tbody>
    </table>
</div>
<div id='punchlogs' style="width:200px;font-size:100%;">
    Hover on entries to get Punch list.

</div>
<div id='punchloglist' style="display:none;">
    <?php
//echo '<pre>';
    echo (json_encode($allpunch));

//echo '</pre>';
    ?>

</div>
<script>
    $(document).ready(function () {
		
		
		jQuery.fn.sortElements = (function(){
    
    var sort = [].sort;
    
    return function(comparator, getSortable) {
        
        getSortable = getSortable || function(){return this;};
        
        var placements = this.map(function(){
            
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
                
                // Since the element itself will change position, we have
                // to have some way of storing it's original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
            
            return function() {
                
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
                
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
                
            };
            
        });
       
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
        
    };
    
})();
		
		
		
		
		/*
		
		
		
		
		
		
		
		    var table = $('table');
    
    $('#ip_header, #id_header,,#name_header #status_header')
        .wrapInner('<span title="sort this column"/>')
        .each(function(){
            
            var th = $(this),
                thIndex = th.index(),
                inverse = false;
            
            th.click(function(){
                
                table.find('td').filter(function(){
                    
                    return $(this).index() === thIndex;
                    
                }).sortElements(function(a, b){
                    
                    return $.text([a]) > $.text([b]) ?
                        inverse ? -1 : 1
                        : inverse ? 1 : -1;
                    
                }, function(){
                    
                    // parentNode is the element we want to move
                    return this.parentNode; 
                    
                });
                
                inverse = !inverse;
                    
            });
                
        });
*/
		
		
		
		
        var curr_userpunch = $('.pmsuser').children('td:nth-child(4)').html();
        //alert(curr_userpunch);
        var punchstatus = 'OUT';
        if (curr_userpunch === 'Punched IN') {
            punchstatus = 'IN';
        }
        $('.circle').addClass(punchstatus);
        $('.circle').html(punchstatus);

        $('.getpunch').on('hover', function () {
            // alert('hi');
            var getid = $(this).attr('id');
            var getpunchlog = $('#punchloglist').html();
            var i = 0;
            var pstus = '';
            var resource_name = $(this).children('td:nth-child(3)').html();
//            alert(getpunchlog);
//            alert(getid);

            //to check whether no punch or not
            var countcells = $(this).children('td').length;
//          /  alert(countcells)
            $('#punchlogs').html('');
            $.each($.parseJSON(getpunchlog), function (key, data) {
                i = 0;
                pstus = '';
                $.each(data, function (index, data) {
                    // console.log('index', data)
                    pstus = 'OUT';
                    if (i % 2 == 0) {
                        pstus = 'IN';
                    }

                    if (key === getid) {
                        if (i == 0) {
                            $('#punchlogs').append(resource_name + "<hr />");
                        }
                        if (countcells == 5) {
                            $('#punchlogs').append("<span class='red'>No Punch</span>");
                        }
                        else {
                            $('#punchlogs').append(data + " <b>( " + pstus + " )</b>" + "<br />");
                        }
                    }
                    i++;
                })
            });

        });
    });
</script> 

