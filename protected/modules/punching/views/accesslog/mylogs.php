<?php
//    print_r('<pre>');
//    print_r($result_log);
//    die;
?>
<style type="text/css">
    .logtable td, th{ border: 1px solid #555 !important;}
    .logtable {border-collapse: collapse;float:left}
    .odd{background-color: #e2e2e2;font-size: 12px;}
    .even{font-size: 12px;}
    .tablehead{
/*        background-color: #c9e0ed;*/
          background-color: #44709C;
          color: #fff;
    }
    .logtable td{padding:5px !important;}
    .datetime span{color:#FF8080;}
    #punchlogs{float:left;background-color:#D0E9F5;font-size: 20px;border:1px solid blue;margin-left:10px;padding:20px;}
    .circle {
        width: 50px;
        height: 50px;
        border-radius: 50%;
        font-size: 15px;
        color: #fff;
        line-height:50px;
        text-align: center;
        font-weight: bold;
        background: #c2c2c2;
    }
    .OUT, .red{
        background: #FF8080;
    }
    .IN{
        background: #8AE62E;
    }

    .greeting {float:right; margin-left:40px;color:blue;}
    #table { 
        box-sizing: border-box; 
    }
    #table tr.cuser,#table tbody tr:hover {
        box-shadow: 0px 0px 10px green;
        -webkit-box-shadow: 0px 0px 5px #00ff00;
        -moz-box-shadow: 0px 0px 5px #00ff00;
    }
    #table tr th{font-size:12px; }
    .glyphicon-ok{
        color:blue; font-size:20px;
    }
    #table tr td{vertical-align:middle}
    h1{float:left; padding:0px 20px 10px 20px;margin-bottom: 0px;}
    .half{
        font-weight:bold;
        color:red;
    }
    .side_table{
        background-color: #555;
        color: #fff;
    }
    /* style="background-color:#f69c55" */
</style>
<?php
if (0) {
    ?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/canvasjs.min.js"></script>

    <div id="chartContainer" style="height: 300px; width: 800px;">
    </div>
    <?php
}
?>

    <h1><?php echo trim($userdata);?>'s monthly Logs</h1>
    <h1 style="color:#09f;padding-left:50px;"><?php echo $logmonth; ?></h1>
    <h1>
    <?php
        if(Yii::app()->user->role==1){
         echo CHtml::dropDownList('code', '', CHtml::listData($accessuserdata, 'userid', 'full_name'), array('class'=>'form-control','style' => 'border: 1px solid #ccc;font-size:15px;margin-top: 5px;', 'options' => array($code => array('selected' => true)), 
         'empty' => '-- select user --',
         'onchange' => 'document.location.href = "index.php?r=punching/accesslog/mylog&code=" + this.value',
         ));
        }
    ?>
    </h1>
<br clear='all'/>
<table cellpadding="10" cellspacing="0" border="0" style="border:0px;width:850px;" class="logtable" id="table">
    <thead>
        <tr style="font-size:12px;">            
            <td colspan="8" style="text-align: right;border:0px !important;font-weight: bold;">
                <?php
                if (isset($_GET['months'])) {
                    $months = $_GET['months'];
                } else {
                    $months = 0;
                }
                if (isset($_GET['code'])) {
                    $code = $_GET['code'];
                } else {
                    $code = Yii::app()->user->id;
                }
                $action = 'mylog';
                ?>    

                &laquo;&laquo;  <?php echo CHtml::link('Previous',array('accesslog/' . $action ,'months' => $months - 1, 'code'=> $code)) ?>
                   |
                <?php
                if ($months >= 0) {
                    echo 'Current | Next &raquo; &raquo;';
                } else {
                    ?>
                    
                    <?php echo CHtml::link('Current',array('accesslog/' . $action ,'months' => 0, 'code'=> $code)) ?> | 
                    <?php echo CHtml::link('Next',array('accesslog/' . $action ,'months' => $months + 1, 'code'=> $code)) ?> 
                      &raquo; &raquo;
                    <?php
                }
                ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php echo CHtml::link('<b>Go to Punch log &raquo;</b>', array('accesslog/punchlog'), array('style' => 'color:red;font-size:15px;')) ?>             
            </td></tr>


        <tr class="tablehead"><th width="100">Date</th><th>Status</th><th>First Punch time</th>
            <th>Last Punch time</th><th>Total Out Time</th><th>Total IN Time</th>
            <th>Total Hrs</th><th>Total Punch</th><th>Leave</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $reportsum = array();
        $late = array();
        $exit = array();
        $leaves = array();
        $exdbrk = array();
        $lessin = array();
        $extra = array();

        $monyear = date('Y-m', $page_date);
        $total_leaves = 0;


//die($total_days);
        $first_punch_chart = '';
        for ($i = 1; $i <= $total_days; $i++) {
            $dayentry = array();
            $dop = $monyear . "-" . sprintf('%02d', $i);  //date of punch
            $dayentry['date'] = date('D d-M-y', strtotime($dop));
            $dayentry['pdate'] = date('d-M-y', strtotime($dop));
            $dayentry['status'] = '';
            $dayentry['first'] = '';
            $dayentry['last'] = '';
            $dayentry['tot_out'] = '';
            $dayentry['tot_in'] = '';
            $dayentry['tot'] = '';
            $dayentry['totcount'] = '';

            if (isset($result_log['log_' . $i])) {
                if (isset($inlate)) {
                    unset($inlate);
                }
                $pstatus = (!isset($status) ? '' : ($status % 2 == 1 ? 'Punched IN' : 'Punched OUT'));
                extract($result_log['log_' . $i]);
                $dayentry['status'] = ($status % 2 == 1 ? 'Punched IN' : 'Punched OUT');
                $dayentry['first'] = date('h:i:s A', strtotime($first_punch));
                $dayentry['last'] = date('h:i:s A', strtotime($last_punch));
                $dayentry['tot_out'] = gmdate('H:i:s', $outsec);
                $dayentry['tot_in'] = gmdate('H:i:s', $insec);
                $dayentry['tot'] = gmdate('H:i:s', $insec + $outsec);
                $dayentry['totcount'] = $count;
            }
            $stime = date('h:i:s', strtotime($dayentry['first']));
            $sthr = date('h', strtotime($dayentry['first']));
            $stmin = round(date('i', strtotime($dayentry['first'])) / 60, 2) * 100;
            if ($stime != '05:30:00') {
                $first_punch_chart .= ",\n{ x: $i, y:  " . ($sthr . "." . $stmin) . "}";
            }
            ?>
            <tr id="log_<?= $i ?>">
                <td bgcolor='#e5f1f4'><?php echo $dayentry['date'] ?></td>
                <?php
                extract($datetimesettings);
                extract($dayentry);
                $punchdate = $dayentry['date'];
                $leave_count = 0;
                if ($dayentry['status'] == '') {
                    echo '<td colspan="7" bgcolor="#fafafa">No Punch</td>';
                    if (substr($dayentry['date'], 0, 3) != 'Sun' && substr($dayentry['date'], 0, 3) != 'Sat' && strtotime($dop) < strtotime(date('Y-m-d'))) {
                        $leave_count = 1;
                        $leaves[] = array($pdate, 1, 'No Punch');
                    }
                } else {
                    $HL = ' <span class="half">HL</span>';
                    $hday_mor = '';
                    $leave_count = 0;
                    if (strtotime($first_punch) > strtotime($dop . " " . $late_halfday)) {
                        $hday_mor = $HL;
                        $leave_count += '0.5';
                        $leaves[] = array($pdate, 0.5, $dayentry['first']);
                        //echo $date." ".$first_punch;
                    }

                    $finlate = 0;

                    if (strtotime($first_punch) > strtotime($dop . " " . '09:30:00')) {

                        $paycut = '';
                        if (strtotime($first_punch) > strtotime($dop . " " . '09:45:59')) {
                            $paycut = ' style="background-color:#FF8080"';
                        }




                        $late[] = array($pdate, "<span $paycut>" . $dayentry['first'] . "</span>", gmdate('H:i:s', (strtotime($first_punch) - strtotime($dop . " " . '09:30:00'))));


                        $finlate = 1;
                    }

                    $hday_noon = '';

                    $lpsec = strtotime($last_punch);
                    $ee_hsec = strtotime($dop . " " . $ee_halfday);

                    if (($lpsec < $ee_hsec && $ee_hsec != strtotime(date('Y-m-d 16:00:00')) && $status % 2 == 0)
                            OR ( $lpsec < $ee_hsec && strtotime($dop) < strtotime(date('Y-m-d')) && $status % 2 == 0)
                            OR ( $lpsec < $ee_hsec && strtotime(date('Y-m-d H:i:s')) > strtotime(date('Y-m-d ' . $ee_halfday)) && $status % 2 == 0)
                    ) {
                        $hday_noon = $HL;
                        $leave_count += '0.5';
                        $leaves[] = array($pdate, 0.5, $dayentry['last']);
                        $exit[] = array($pdate, $dayentry['last'], gmdate('H:i:s', (strtotime(date($dop . ' 16:00:00')) - $lpsec)));
                    }

                    if (($outsec) / (60 * 60) > 1) {
                        $exdbrk[] = array($pdate, gmdate('H:i:s', $outsec), $dayentry['tot_out']);
                    }

//                    if (($insec) / (60 * 60) > 8 && ( ($outsec) / (60 * 60) + ($insec) / (60 * 60) ) > 9) {
//                        $extra[] = array($pdate, gmdate('H:i:s', $insec - (8 * 60 * 60)), gmdate('H:i:s', $insec));
//                    }
                    
                    
                   // $new_in = $insec;
                   // $new_out = $outsec;
                    
//                    if(($outsec <= (1 * 60 * 60)) && ($insec >= (8 * 60 * 60)) && $count >= 2){                            
//						$new_tot = $insec + $outsec;
//						$new_extra = $new_tot - (9*60*60);
//                                                if($new_extra > 0 ){
//                                                $extra[] = array($pdate, gmdate('H:i:s', $insec), gmdate('H:i:s', $new_extra));    
//                                                }						
//				
//					} 
//					if(($outsec > (1 * 60 * 60)) && ($insec >= (8 * 60 * 60)) && $count >= 2){
//						
//						$diff = $outsec - (1 * 60 * 60);
//						$newin = $insec - $diff;
//                                                
//						if($newin > (9 * 60 * 60)){
//						$ot =  $newin - (9 * 60 * 60);
//						$extra[] = array($pdate, gmdate('H:i:s', $insec), gmdate('H:i:s', $ot));
//						}	
//					} 
                if($insec > (8 * 60 * 60)){
                $newTot = ($insec + ( 1 * 60 * 60 )) - $outsec;                      
                $newOT =  ($newTot - (9*60*60)); 
                if($newOT > 0 ){                   
                $extra[] = array($pdate, gmdate('H:i:s', $insec), gmdate('H:i:s', $newOT));
                }
                }			

                   /* if($first_punch != $last_punch){                                             
                    // if (($insec) / (60 * 60) > 8 && ( ($outsec) / (60 * 60) + ($insec) / (60 * 60) ) > 9) {
                         if (($insec) / (60 * 60) > 8) {
                        $extra[] = array($pdate, gmdate('H:i:s', $insec - (8 * 60 * 60)), gmdate('H:i:s', $insec));
                    }    
                    } */
                    
                    ?>
                    <td style="background-color:<?php echo ($dayentry['status'] === 'Punched IN' ? '#8AE62E' : '#FF8080'); ?>"><?php echo $dayentry['status'] ?></td>
                    <td style="<?php echo (isset($finlate) && $finlate == 1 ? 'background-color:#f69c55' : ''); ?>"><?php echo $dayentry['first'] . " $hday_mor" ?></td>
                    <td style="<?php echo (isset($outearly) && $outearly == 1 ? 'background-color:#f69c55' : ''); ?>"><?php echo $dayentry['last'] . " $hday_noon" ?></td>
                    <td bgcolor="<?php echo (($outsec) / (60 * 60) > 1 ? '#f69c55' : '#ADD6FF') ?>"><?php echo $dayentry['tot_out'] ?></td>
                    <td bgcolor="<?php echo (($insec) / (60 * 60) < 8 ? '#f69c55' : '#ADD6FF') ?>"><?php echo $dayentry['tot_in'] ?></td>
                    <td bgcolor="<?php echo (($insec + $outsec) / (60 * 60) < 9 ? '#f69c55' : '#ADD6FF') ?>"><?php echo $dayentry['tot'] ?></td>
                    <td  style="color:'<?php echo ($count % 2 == 1 ? 'Green' : 'Red') ?> ';text-align:center;"><?php echo $dayentry['totcount'] ?></td>
                    <?php
                }
                ?>
                <td><?php
                    if ($leave_count > 0) {
                        $total_leaves += $leave_count;
                        echo '<span class="half">' . $leave_count . '</span>';
                    }
                    ?></td>
            </tr>
            <?php
        }
        ?>
        <tr class="tablehead"><th colspan="8" style='text-align: right;font-weight: bold;'>Total</th><th><?php echo $total_leaves; ?></th>
        </tr>
    </tbody>
</table>

<table border="1" style="float:left;width:300px !important;margin-left:10px;" class="logtable">
    <tr class="side_table"><th colspan="4">Late Coming</th></tr>
    <?php
    if (count($late) == 0) {
        echo '<tr><td colspan="4">No Late Entries</td></tr>';
    } else {
        ?>
        <tr><th>Slno</th><th>Date</th><th>Time</th><th>Late By</th></tr>
        <?php
        $total = 0;
        foreach ($late as $k => $l) {
            $total += $l[1];
            ?>
            <tr>
                <td><?php echo $k + 1; ?></td>
                <td><?php echo $l[0]; ?></td>
                <td><?php echo $l[1]; ?></td>
                <td><?php echo $l[2]; ?></td>
            </tr>
            <?php
        }
        ?>
    <!--        <tr>
    <th colspan='3'>Total</th>
    <th><?php echo $total ?></th>
    </tr>-->
        <?php
    }
    ?>
</table> 

<table border="1" style="float:left;width:300px !important;margin-left:10px;" class="logtable">
    <tr class="side_table"><th colspan="4">Early Exit</th></tr>
    <?php
    if (count($exit) == 0) {
        echo '<tr><td colspan="4">No early exit Entries</td></tr>';
    } else {
        ?>
        <tr><th>Slno</th><th>Date</th><th>Time</th><th>Early By</th></tr>
        <?php
        $total = 0;
        foreach ($exit as $k => $l) {
            $total += $l[1];
            ?>
            <tr>
                <td><?php echo $k + 1; ?></td>
                <td><?php echo $l[0]; ?></td>
                <td><?php echo $l[1]; ?></td>
                <td><?php echo $l[2]; ?></td>
            </tr>
            <?php
        }
        ?>
    <!--        <tr>
    <th colspan='3'>Total</th>
    <th><?php echo $total ?></th>
    </tr>-->
        <?php
    }
    ?>
</table> 

<table border="1" style="float:left;width:300px !important;margin-left:10px;" class="logtable">
    <tr><th colspan="4" class="side_table">Leaves</th></tr>
    <?php
    if (count($leaves) == 0) {
        echo '<tr><td colspan="4">No Leaves</td></tr>';
    } else {
        $total = 0;
        foreach ($leaves as $k => $leave) {
            $total += $leave[1];
            ?>
            <tr><td><?php echo $k + 1 ?></td><td><?php echo $leave[0] ?></td><td><?php echo $leave[2] ?></td><td><?php echo $leave[1] ?></td></tr>
            <?php
        }
        ?>
        <tr><th colspan='3'>Total</th><th><?php echo $total ?></th></tr>
        <?php
    }
    ?>
</table> 

<table border="1" style="float:left;width:300px !important;margin-left:10px;" class="logtable">
    <tr><th colspan="4" class="side_table">Break Exceeds</th></tr>
    <?php
    if (count($late) == 0) {
        echo '<tr><td colspan="4">No Exceeds</td></tr>';
    } else {
        ?>
        <tr><th>Slno</th><th>Date</th><th>Out Duration</th><th>In Time</th></tr>
        <?php
        $total = 0;
        foreach ($exdbrk as $k => $l) {
            $total += $l[1];
            ?>
            <tr>
                <td><?php echo $k + 1; ?></td>
                <td><?php echo $l[0]; ?></td>
                <td><?php echo $l[1]; ?></td>
                <td><?php echo $l[2]; ?></td>
            </tr>
            <?php
        }
        ?>
    <!--        <tr>
    <th colspan='3'>Total</th>
    <th><?php echo $total ?></th>
    </tr>-->
        <?php
    }
    ?>
</table> 

<table border="1" style="float:left;width:300px !important;margin-left:10px;" class="logtable">
    <tr><th colspan="4" class="side_table">Extra Time</th></tr>
            <?php
            
            if (count($extra) == 0) {
                echo '<tr><td colspan="4">No Extra time</td></tr>';
            } else {
                ?>
        <tr><th>Slno</th><th>Date</th><th>In Duration</th><th>Extra Time</th></tr>
        <?php
        $total = 0;
        foreach ($extra as $k => $l) {
            $total += $l[1];
            ?>
            <tr>
                <td><?php echo $k + 1; ?></td>
                <td><?php echo $l[0]; ?></td>
                <td><?php echo $l[1]; ?></td>
                <td><?php echo $l[2]; ?></td>
            </tr>
            <?php
        }
        ?>
    <!--        <tr>
    <th colspan='3'>Total</th>
    <th><?php echo $total ?></th>
    </tr>-->
        <?php
    }
    ?>
</table> 


<!--<table border="1" style="float:left;width:180px !important;margin-left:10px;" class="logtable">
    <tr><th colspan="3">Less In-Time</th></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
</table> 

<table border="1" style="float:left;width:180px !important;margin-left:10px;" class="logtable">
    <tr><th colspan="3">Extra Time</th></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
    <tr><td>1</td><td>12/01/2017</td><td>09:35:20</td></tr>
</table> -->
<?php
if (0) {
    ?>
<script type="text/javascript">
    window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer", {
            title: {
                text: "Punching Entry"
            },
            axisX: {
                interval: 1,
                title: 'Days',
                gridDashType: "dot"
            },
            axisY: {
                title: 'First Punch Hrs',
                interval: 0.5,
                minimum: 8,
                maximum: 12,
                gridDashType: "dot"
            },
            data: [{
                    type: "line",
                    dataPoints: [

<?php echo trim($first_punch_chart, ',') ?>
                    ]
                }]
        });
        chart.render();
    };
</script>
<?php
}
 ?>


