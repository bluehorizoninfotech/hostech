<?php
$post_string=$data->url;
$post_replacement = 'Blog/';
$post_url= substr_replace($post_string, $post_replacement, 23, 0);
?>
<div class="post">
	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->title), $post_url); ?>
	</div>
	<div class="author">
		posted by <?php echo $data->author->username . ' on ' . date('F j, Y',$data->create_time); ?>
	</div>
	<div class="content">
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			echo $data->content;
			$this->endWidget();
		?>
	</div>
	<div class="nav">
		<b>Tags:</b>
		<?php
		$string = $data->url;
        $replacement = 'Blog/';
         $url= substr_replace($string, $replacement, 23, 0); 
?>
		<?php echo implode(', ', $data->tagLinks); ?>
		<br/>
		<?php echo CHtml::link('Permalink', $url); ?> |
		<?php echo CHtml::link("Comments ({$data->commentCount})",$url.'#comments'); ?> |
		Last updated on <?php echo date('F j, Y',$data->update_time); ?>
	</div>
</div>
