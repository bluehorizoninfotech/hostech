<ul>
	<?php foreach($this->getRecentComments() as $comment): ?>
		<?php $string= $comment->getUrl();  
		$replacement = 'Blog/';
		$url= substr_replace($string, $replacement, 23, 0);
		?>
	<li><?php echo $comment->authorLink; ?> on
		<?php echo CHtml::link(CHtml::encode($comment->post->title), $url); ?>
	</li>
	<?php endforeach; ?>
</ul>