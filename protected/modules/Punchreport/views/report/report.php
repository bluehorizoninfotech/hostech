<?php


Yii::app()->clientScript->registerScript('myscript', '
//    $("#start_date").datepicker({
//        format: "yyyy-mm-dd",
//      
//            autoclose: true,
//    });
//    
//    $("#end_date").datepicker({
//        format: "yyyy-mm-dd",
//      
//            autoclose: true,
//    });
    
  //  $("#daterange").daterangepicker();
    
//    $("#daterange").on("apply.daterangepicker", function(ev, picker) {
//        
//      //console.log(picker.startDate.format("YYYY-MM-DD"));
//      //console.log(picker.endDate.format("YYYY-MM-DD"));
//      
//      var From_date = picker.startDate;
//      var To_date = picker.endDate;
//      var diff_date =  To_date - From_date;
//      //var months = Math.floor((diff_date % 31536000000)/2628000000);
//      var days = Math.round((diff_date)/(1000*60*60*24));
//      if(days > 92){
//        alert("Select maximum of 3 months");
//        $("button.viewreport").prop("disabled", true);
//        $("button.regenerate").prop("disabled", true);
//      }else{
//        $("button.viewreport").prop("disabled", false);
//        $("button.regenerate").prop("disabled", false);
//      }
//      
//
//    });
    

    




');

?>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/moment.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/daterangepicker.js"></script>
<script>
    $(document).ready(function(){
        
        $("#daterange").daterangepicker();
        $("#daterange").on("apply.daterangepicker", function(ev, picker) {
        
            //console.log(picker.startDate.format("YYYY-MM-DD"));
            //console.log(picker.endDate.format("YYYY-MM-DD"));

            var From_date = picker.startDate;
            var To_date = picker.endDate;
            var diff_date =  To_date - From_date;
            //var months = Math.floor((diff_date % 31536000000)/2628000000);
            var days = Math.round((diff_date)/(1000*60*60*24));
            if(days > 31){
              alert("Select maximum of 31 days");
              $("button.viewreport").prop("disabled", true);
              $("button.regenerate").prop("disabled", true);
            }else{
              $("button.viewreport").prop("disabled", false);
              $("button.regenerate").prop("disabled", false);
            }


        });
          
        $('#loader').hide();
        $('.overlay').hide();
        

        $('.form1').submit(function() 
        {
            
            $('#loader').show();
            $('.overlay').show();

        }); 
    });
</script>
<div class="report-page-report-sec">
<div class="site-index">

    <div id="loader" ></div>
    <div class="overlay" ></div>
    <div class="body-content report">
        <?php
        
        $timelog = Yii::app()->db->createCommand("
                SELECT * FROM pms_report")->queryAll();
        // Set timezone
        date_default_timezone_set('Asia/Kolkata');

        $fdate = $date1;
        $edate = $date2;
        
        $datediff = strtotime($edate) - strtotime($fdate);
        $totaldays = floor($datediff/(60*60*24));
        
        $colspanw = $totaldays + 4 ;
        
        $users = array();
        foreach ($employees as $emp) {
            $users[] = $emp['first_name'].' '.$emp['last_name'];
        }
        $cols = count($users) * 6;
        $hdays = array();
        foreach ($holidays as $hday) {
            $hdays[] = $hday['date'];
        }

        $logdays = array();
        foreach ($timelog as $log) {
            $logdays[] = $log['date'];
        }
        
        $ldays = array();
        foreach ($leaves as $lday) {
            $uid = $lday['user_id'];
            $ldays[$uid] = $lday['date'];
        }
        
        $shtime = array();
        foreach ($shifts as $key => $shift) {
            $uid = $shift['accesscard_id'];
            $shtime[$uid] = $shift['start_time'];
    
        }
        
        $workhdays = array();
        foreach ($wihs as $key => $wih) {
            $workhdays[] = $wih['date'];
    
        }
        
        
        
        $ssdays = array();
        $sdate = $fdate;
        $end_date = $edate;
        for ($i = 0; $i <= (strtotime($sdate) <= strtotime($end_date)); $i++) {
            
             if(date("D",strtotime($sdate)) == 'Sat' || date("D",strtotime($sdate)) == 'Sun'){
                 $ssdays[] = $sdate;
             }
            
            $sdate = date("Y-m-d", strtotime("+1 day", strtotime($sdate)));   
        }
        
     
        
       
        ?>

   
              
      
        
        <form class="form-inline form1" role="form" action="<?php echo Yii::app()->createUrl('Punchreport/report/index'); ?>" method="post">

            <div class="form-group">     
                <input type="text" class="form-control pull-right" placeholder="Search by Date" name="daterange" id="daterange" value="<?php if(isset($postdate)) { echo $postdate;} ?>">
                
          
            </div>
            <button type="submit" name="action" value="Generate"  class="btn btn-default regenerate" disabled="true">Generate</button>
            <button type="submit" name="action" value="View" class="btn btn-default viewreport" disabled="true">View</button>
            
            
<!--            <div class="form-group" style="margin-left: 30px;">
                <input type="radio" name="status" value="active"/>Active 
                <input type="radio" name="status" value="inactive"/>Inactive 
                <input type="radio" name="status" value="both"/>Both 
                
            </div>-->
            
        </form>
        
        
        

        
        <!--<h3 class="text-center">Punch Report from <?php echo $fdate; ?> to <?php echo $edate; ?></h3>-->
        <br/>
        <table class="table table-hover black-color">
            <thead>
                <tr>
                    <th class="text-center large" colspan="<?php echo $colspanw; ?>">Punch Report from <span style="color:#0770b5"><?php echo date("d F Y",strtotime($fdate)); ?></span> to <span style="color:#0770b5"><?php echo date("d F Y",strtotime($edate)); ?></span></th>
                </tr>
                <tr>
                    <th colspan="3"></th>
                    <?php
                        $start    = new DateTime($fdate);
                        $start->modify('first day of this month');
                        $end      = new DateTime($edate);
                        $end->modify('first day of next month');
                        $interval = DateInterval::createFromDateString('1 month');

                        $period   = new DatePeriod($start, $interval, $end);
                        $date = $fdate;
                        
                        while (strtotime($date) <= strtotime($edate)) { 
                            foreach ($period as $y=>$dt) {
                                if($y==0){ $first =  $date; }else {$first =  date("Y-m-01", strtotime($dt->format("Y-m-d"))); }
                                $last = date("Y-m-t", strtotime($dt->format("Y-m-d")));
                                $datediff = strtotime($last) - strtotime($first);
                                $totaldays = round($datediff/(60*60*24))+1;
                                $num = $totaldays;
                                
                                    if($date = date("Y-m-t", strtotime($dt->format("Y-m-d")))){
                                        echo "<th colspan=".$num.">".$dt->format("F")."</th>";
                                    }

                            }
                             $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                        }
                   ?>
                    
                    
                </tr>
                <tr>
                    <th>#</th>
                    <th>No of Late</th>
                    <th class="head">Punch</th>
                        <?php 
                        $date = $fdate; 
                        $end_date = $edate;
                        while (strtotime($date) <= strtotime($end_date)) { ?>
                        <th>
                            <?php echo date('d', strtotime($date)); ?>
                        </th>
                        <?php
                        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                    }
                    ?>
                </tr>

            </thead>

            <tbody>
                
               
           
     
                <?php
                if(!empty($employees)) {
                
                foreach ($employees as $uk=>$user) {

                    $username = $user['first_name']." ".$user['last_name'];
                   // $username = $user;
                    $aid = $user['accesscard_id'];
                    
                    ?>
                <tr>
                        <th rowspan="6"  class="width-100">
                            <?php echo $username; ?>
                            
                        </th>
                        
                        
                    
                        <?php
                        $date = $fdate;
                        $end_date = $edate;
                        
                        $in = array();
                        $out = array();
                        $hours = array();
                        $fp = array();
                        $lp = array();
                       
                        $row_in = ""; 
                        $row_out = "";
                        $row_hours = "";
                        $row_fp = "";
                        $row_lp = "";
                      
                        
                        
                        $countpunch = 0; $lt = 0;
                        for ($i = 0; $i <= (strtotime($date) <= strtotime($end_date)); $i++) {
                            ?>
                            <?php                     
                            $check = PmsReport::model()->count(
                                        array(
                                            'condition' => "date = '$date'"
                                        )
                                    );

                            if(in_array($date, $ssdays)){
                                foreach ($ssdays as $ssday) {
                                    if($ssday == $date){
                                        if(!in_array($ssday, $workhdays)){
                                            if($uk == 0) 
                                                $fp[] = "<td rowspan=".$cols." class='light-yellow vertical_text'><span>".date("l",strtotime($ssday))."</span></td>";
                                            continue;
                                        }
                                        else{
                                                $fp[] = "<td>&nbsp;</td>";   
                                                $lp[] = "<td>&nbsp;</td>";  
                                                $in[] = "<td>&nbsp;</td>";
                                                $out[] = "<td>&nbsp;</td>";
                                                $hours[] = "<td>&nbsp;</td> ";

                                        } 
                                        
                                    }
                                }  
                          
                            
                            }else if(in_array($date, $hdays)) {
                                foreach($holidays as $holiday){
                                    if($holiday['date'] == $date){
                                        if(!in_array($holiday['date'], $workhdays)){
                                            if($uk == 0) 
                                                $fp[] = "<td rowspan=".$cols." class='light-yellow vertical_text'><span>".$holiday['note']." </span></td>";
                                            continue;
                                        }
                                        else{
                                                $fp[] = "<td>&nbsp;</td>";   
                                                $lp[] = "<td>&nbsp;</td>";  
                                                $in[] = "<td>&nbsp;</td>";
                                                $out[] = "<td>&nbsp;</td>";
                                                $hours[] = "<td>&nbsp;</td> ";

                                        } 
                                    }
                                }
 
                            }
                            else if ($check != 0) { 
                                
                                foreach ($timelog as $log) {
                                   
                                    if ($log['accesscard_id'] == $aid && $log['date'] == $date) {
                                        
                                        if($log['punch_count'] == 0 ){
                                                
                                                $model = Yii::app()->db->createCommand('select count(*) from leave_details ld inner join user_login ul '
                                                            . 'on ld.emp_name = ul.id where "'.$date.'" BETWEEN ld.date_from AND ld.date_to AND ul.accesscard_id = "'.$aid.'"');
                                                    $lcount = $model->queryScalar();
                                                    
                                                    if($lcount == 1){
                                                        $fp[] = "<td rowspan=6 class='light-red'>L</td>"; 
                                                            continue;
                                                            $lp[] = "<td>&nbsp;</td>";   
                                                            $in[] = "<td>&nbsp;</td>";
                                                            $out[] = "<td>&nbsp;</td>";
                                                            $hours[] = "<td>&nbsp;</td>";
                                                    }
                                                    else{
                                                        
                                                        
                                                        $dmodel = Yii::app()->db->createCommand('select * from pms_dummylogs where usercard_id = "'.$aid.'" AND date = "'.$date.'"');
                                                        $dlog = $dmodel->queryRow();
                                                        
                                                        if(count($dlog) !=0){
                                                            $dummymodel = Yii::app()->db->createCommand('select * from pms_report where accesscard_id = "'.$dlog['dummycard_id'].'" AND date = "'.$date.'"');
                                                            $dummylog = $dummymodel->queryRow();
                                                            if(!empty($dummylog)){
                                                                
                                                                $fp[] = "<td class='dummy'>".date('h:i', strtotime($dummylog['first_punch']))."</td>"; 
                                                                $lp[] = "<td class='dummy'>".date('h:i', strtotime($dummylog['last_punch']))."</td>"; 
                                                                $in[] = "<td class='dummy'>".date('h:i', strtotime($dummylog['total_in']))."</td>";
                                                                $out[] = "<td class='dummy'>".date('h:i', strtotime($dummylog['total_out']))."</td>";
                                                                $hours[] = "<td class='dummy'>".date('h:i', strtotime($dummylog['total_hours']))."</td>"; 
                                                                
                                                            }
                                                            else{
                                                                $fp[] = "<td>&nbsp;</td>"; 
                                                                $lp[] = "<td>&nbsp;</td>";  
                                                                $in[] = "<td>&nbsp;</td>";
                                                                $out[] = "<td>&nbsp;</td>";
                                                                $hours[] = "<td>&nbsp;</td> ";
                                                            }
                                                            
                                                            
                                                            
                                                        }
                                                        else{
                                                                $fp[] = "<td>&nbsp;</td>"; 
                                                                $lp[] = "<td>&nbsp;</td>";  
                                                                $in[] = "<td>&nbsp;</td>";
                                                                $out[] = "<td>&nbsp;</td>";
                                                                $hours[] = "<td>&nbsp;</td> ";
                                                            }
                                                        
                                                            
                                                        
                                                       
                                                        
                                                    }

                                        }
                                        else{
                                            
                                            
                                            $firstpunch = date('h:i', strtotime($log['first_punch']));
                                            
                                            if(array_key_exists($aid, $shtime)){
                                               $flimit =  date('h:i', strtotime($shtime[$aid]));
                                            }
                                            else{
                                                $flimit = date('h:i', strtotime("09:30"));
                                            }
                                            if($firstpunch > $flimit){
                                                $lt++;
                                                $ltclass = "blue";
                                            }
                                            else{
                                               $ltclass = ""; 
                                            }
                                           $fp[] = "<td class=".$ltclass.">".date('h:i', strtotime($log['first_punch']))."</td>"; 
                                           $lp[] = "<td>".date('h:i', strtotime($log['last_punch']))."</td>"; 
                                           $in[] = "<td>".date('h:i', strtotime($log['total_in']))."</td>";
                                           $out[] = "<td>".date('h:i', strtotime($log['total_out']))."</td>";
                                           $hours[] = "<td>".date('h:i', strtotime($log['total_hours']))."</td>"; 
                                           
                                        } 

                                    }
                                }
                            } else {
                                
                                $fp[] = "<td>&nbsp;</td>";   
                                $lp[] = "<td>&nbsp;</td>";  
                                $in[] = "<td>&nbsp;</td>";
                                $out[] = "<td>&nbsp;</td>";
                                $hours[] = "<td>&nbsp;</td> ";
                                 
                            }
                            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                            
                            
                            $row_fp = "<tr><th class='head'>First</th>".implode("", $fp)."</tr>";
                            $row_lp = "<tr><th class='head'>Last</th>".implode("", $lp)."</tr>";

                            $row_in = "<tr><th class='head'>TotIn</th>".implode("", $in)."</tr>";
                            $row_out = "<tr><th class='head'>TotOut</th>".implode("", $out)."</tr>";
                            $row_hours = "<tr><th class='head'>TotHrs</th>".implode("", $hours)."</tr>";
                           
                          
                        }
                       
                        ?> 
                    <th rowspan="6">
                            <?php echo $lt; ?>
                        </th>
                    
                    </tr> <?php
                        echo $row_fp;
                        echo $row_lp;
                        echo $row_in;
                        echo $row_out;
                        echo $row_hours;
                        ?>                 
                    <?php }
                }else{
                    echo "<tr><td colspan='$colspanw'>No results found</td></tr>";
                }
                    
                    ?>
            </tbody>
        </table>
    </div>
</div>
            </div>
