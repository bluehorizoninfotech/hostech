<?php
/* @var $this OhController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Optional Holidays',
);

$this->menu=array(
	array('label'=>'Create OptionalHolidays', 'url'=>array('create')),
	array('label'=>'Manage OptionalHolidays', 'url'=>array('admin')),
);
?>

<h1>Optional Holidays</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
