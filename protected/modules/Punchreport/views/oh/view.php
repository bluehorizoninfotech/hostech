<?php
/* @var $this OhController */
/* @var $model OptionalHolidays */

$this->breadcrumbs=array(
	'Optional Holidays'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OptionalHolidays', 'url'=>array('index')),
	array('label'=>'Create OptionalHolidays', 'url'=>array('create')),
	array('label'=>'Update OptionalHolidays', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OptionalHolidays', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OptionalHolidays', 'url'=>array('admin')),
);
?>

<h1>View OptionalHolidays #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'user_id',
		'note',
	),
)); ?>
