<?php
/* @var $this OhController */
/* @var $model OptionalHolidays */

$this->breadcrumbs=array(
	'Optional Holidays'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OptionalHolidays', 'url'=>array('index')),
	array('label'=>'Manage OptionalHolidays', 'url'=>array('admin')),
);
?>

<h1>Create OptionalHolidays</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>