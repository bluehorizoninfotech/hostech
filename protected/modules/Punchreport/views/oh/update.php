<?php
/* @var $this OhController */
/* @var $model OptionalHolidays */

$this->breadcrumbs=array(
	'Optional Holidays'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OptionalHolidays', 'url'=>array('index')),
	array('label'=>'Create OptionalHolidays', 'url'=>array('create')),
	array('label'=>'View OptionalHolidays', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OptionalHolidays', 'url'=>array('admin')),
);
?>

<h1>Update OptionalHolidays <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>