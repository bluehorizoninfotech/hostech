<?php


Yii::app()->clientScript->registerScript('myscript', '
//    $("#log_time").datepicker({
//        format: "yyyy-mm-dd",
//
//            autoclose: true,
//    });
    
');

$shtime = array();
foreach ($shifts as $key => $shift) {
    $uid = $shift['accesscard_id'];
    $shtime[$uid] = $shift['start_time'];

}

$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

//echo $hostname;
?>
<div class="index-one-time-log-sec">
<div class="site-index">
<div class="width-200 pull-left">
            <h4 class="title dark-blue font-weight-800 font-15">
                <?php
                    if ($days == 0) {
                        echo date('d-m-Y <\s\p\a\n>h:i:s A</\s\p\a\n> D');
                    } else {
                        echo date('d-m-Y l', strtotime($precent_date));
                    }
                ?>
            </h3>
	<div>
            <?php $action = Yii::app()->controller->action->id; ?>
            
            <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'method' => 'get',
                    'id' => 'access-log',
                    'action' => Yii::app()->createUrl('Punchreport/timelog/'.$action),
                    'htmlOptions' => array('class' => 'form-inline')
                ));
                ?>
           
            <div class="form-group">
                <?php echo CHtml::activeTextField($model, 'log_time', array("id" => "log_time", 'size' => 10)); ?>
                 <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'log_time',
                    'ifFormat' => '%Y-%m-%d',
                ));
                ?>
            </div>
            
               
            <div class="form-group">
                <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
            </div>

             <?php $this->endWidget(); ?>
        </div>
		<br clear="all"/>
        
        <div class="min-width-200 pull-right">
            <a href="<?php echo Yii::app()->createUrl('Punchreport/timelog/'.$action, array('days' => $days - 1)) ?>">Previous</a>  |
            <?php
            if ($days >= 0) {
                echo 'Today | Next';
            } else {
                ?>
                <a href="<?php echo Yii::app()->createUrl('Punchreport/timelog/'.$action, array('days' => 0)) ?>">Today</a>  | 
                <a href="<?php echo Yii::app()->createUrl('Punchreport/timelog/'.$action, array('days' => $days + 1)) ?>">Next</a>
                <?php
            }
            ?>
        </div>


		<h3 class="pull-left padding-5"><?php echo "<font color='white' style='background-color:".($cuser['punch_status']==='Punched IN'?'Green':'#ff8080').";padding:10px'>". $cuser['punch_status']."</font></h3>" ?>
		<br clear="all"/>
		<?php 
		 if(strlen($_SERVER['REMOTE_ADDR'])<=15){
		?>
		<br /><br /> My IP: <?php echo $_SERVER['REMOTE_ADDR']?>
		<?php
		 }
		
	echo '<div id="fixedimage">';	
// I'm India so my timezone is Asia/Calcutta
date_default_timezone_set('Asia/Calcutta');

// 24-hour format of an hour without leading zeros (0 through 23)
$Hour = date('G');

if($_SERVER['REMOTE_ADDR']=='192.168.1.139' && date("m.d.y")=='19.09.16' Or isset($_GET['a'])){
	echo "<img src='/timelog/images/akhilbirthday.jpg' width='200' align='right' /><br clear='all'/>";
}

if ( $Hour >= 5 && $Hour <= 11 ) {
    //echo "<h2 class='greeting'>Good Morning</h2>";
	echo "<img src='/timelog/images/Good-Morning-Animated-4.gif' width='200' align='right' />";
} else if ( $Hour >= 12 && $Hour <= 16 ) {
	echo "<img src='/timelog/images/noon.gif' width='200' align='right' />";
   // echo "<h4 class='greeting'>Good Afternoon</h4>";
} else if ( $Hour >= 17 || $Hour <= 4 ) {
    //echo "<h2 class='greeting'>Good Evening</h2>";
	echo "<img src='/timelog/images/ClockCreddyGoodEvening.gif' width='200' align='right' />";
}


?>		
		</div>


		
</div>
</div>

    <div class="body-content timelog pull-left">
	<?php
if(isset($cuser['username'])){
	echo "<h2 style='float:left'>Hi ..".$cuser['username']."</h2>";
}




?>
<br clear='both'/>
       <div class=" width-970 font-11">
        <table class="table table-hover table-bordered table-striped black-border" id="table">
            <thead>
                <tr>
                    <th>SNo</th>
                    <th>AccessID</th>
                    <th>Name</th>
                    <th><?=($days==0?'Current':'Last')?> status</th>
                    <th>First Punch</th>
                    <th>Last Punch</th>
                    <th width="100">Total Out Time</th>
                    <th width="100">Total IN Time</th>
                    <th>TotalHrs</th>  
                    <th>IP</th>
                </tr>              
            </thead>
            <tbody>
            <?php
            $i =1;
           foreach($result as $k =>$res){
           ?>
                <tr<?php echo ($res['same']==1?' style="border:2px solid green;" class="cuser"':'') ?>>                  
                    <td width="20"><?=$i ?></td>
                    <td width="30" class="text-align-center" bgcolor="#E8F3FF"><?=$k ?></td>
                    <td width="120"><?=$res['username'] .($res['same']==1?'&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-ok"></span>':''); ?></td>
                    <?php 
                        if($res['punch_status'] == 'No Punch'){
                    ?>
                    <td colspan="6" class="red-color"><?=$res['punch_status']; ?></td>
                    <?php
                        }else{
                    ?>
                    <td width="85" bgcolor="<?php echo ($res['punch_count'] % 2 == 1 ? '#8AE62E' : '#FFCACA') ?>"><?=$res['punch_status']; ?></td>
                                        
                    <?php
                        $ltclass = ""; 
                        $firstpunch = date('h:i', strtotime($res['intime']));
                                            
                        if(array_key_exists($k, $shtime)){
                           $flimit =  date('h:i', strtotime($shtime[$k]));
                        }
                        else{
                            $flimit = date('h:i', strtotime("09:30"));
                        }
                        if(strtotime($firstpunch) > strtotime($flimit)){
                            $ltclass = "blue";
                        }

                    ?>
                    <td width="70" class="<?=$ltclass;?>"><?=$res['intime']; ?></td>
                    <td width="70"><?=$res['lsttime']; ?></td>
                    
                    <?php
                        $toclass = "";
                        $total_out = date('H:i', strtotime($res['tout']));
                        if($total_out > '01:00'){
                            $toclass = "blue";
                        }
                       
                    ?>
                    <td class="<?=$toclass?>" width="80"><?=$res['tout']; ?></td>

                    <td bgcolor="#E8F3FF" width="50"><?=$res['tot_in']; ?></td>
                    
                    <?php
                        $ttclass ="";
                        $total_time = date('H:i', strtotime($res['tot_time']));
                        $shifthrs = date('H:i', strtotime("09:00"));   
                        if(strtotime($total_time) < strtotime($shifthrs)){
                            $ttclass = "blue";
                        }
                       
                    ?>
					
					<td class="<?=$ttclass?>" bgcolor="#E8F3FF" width="50"><?=$res['tot_time']; ?></td>
                    <?php
                        }
                    ?>
					<td width="50">192.168.1.<?php echo intval(substr($res['reg_ip'],strrpos($res['reg_ip'], '.')+1));?></td>
                </tr>
           <?php
           $i++;
           }
            ?>
            </tbody>
        </table>
            </div>
    </div>
</div>
<?php 
if(count($cuser)==0)
	//echo "---------";
	if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		//echo "---------";
     $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
     $ip_address = $_SERVER['REMOTE_ADDR'];
}
 // print_r($_SERVER);
?>



<?php
$packed = chr(127) . chr(0) . chr(0) . chr(1);
$expanded = inet_ntop($packed);

/* Outputs: 127.0.0.1 */
//echo $expanded;

$packed = str_repeat(chr(0), 15) . chr(1);
$expanded = inet_ntop($packed);

/* Outputs: ::1 */
//echo $expanded;
?>

