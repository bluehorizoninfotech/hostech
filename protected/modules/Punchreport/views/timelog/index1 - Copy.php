<?php
use backend\models\PmsReport;
use backend\models\Leaves;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Punching Report System';
$this->registerJs('
    $("#log_time").datepicker({
        format: "yyyy-mm-dd",
      
            autoclose: true,
    });', \yii\web\VIEW::POS_READY);

$shtime = array();
foreach ($shifts as $key => $shift) {
    $uid = $shift['accesscard_id'];
    $shtime[$uid] = $shift['start_time'];

}

$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

echo $hostname;
?>
<div class="index-copy-time-log-sec">
<div class="site-index">
<div style="width: 200px;float:left;">
            <h4 class="title" style="color: #005983; font-weight: 800; font-size: 15px">
                <?php
                    if ($days == 0) {
                        echo date('d-m-Y <\s\p\a\n>h:i:s A</\s\p\a\n> D');
                    } else {
                        echo date('d-m-Y l', strtotime($precent_date));
                    }
                ?>
            </h3>
	<div>
            <?php $action = Yii::$app->controller->action->id; ?>

            <?php $form = ActiveForm::begin(['method' => 'get','action' => Yii::$app->urlManager->createUrl('timelog/'.$action),'options' => ['class' => 'form-inline']]); ?>
            <div class="form-group">
                <?= $form->field($model, 'log_time', ['template' => '
               {label}{input}<i class="fa fa-calendar"></i>{error}{hint}'])->textInput(['data-default' => '','placeholder' => 'Select Date', 'class' => 'form-control pull-right', 'id' => 'log_time', 'style'=>'width:100px;'])->label('') ?>
            </div>
            <div class="form-group">
               <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
		<br clear="all"/>

        <div>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['timelog/'.$action, 'days' => $days - 1]) ?>">Previous | </a>
             <?php
                if ($days >= 0) {
                    echo 'Today | Next';
                } else {
                    ?>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['timelog/'.$action, 'days' => 0]) ?>">Today | </a>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['timelog/'.$action, 'days' => $days + 1]) ?>">Next</a> 
             <?php
                }
                ?>
        </div>
		<h3 style="float:left;padding:5px"><?php echo "<font color='white' style='background-color:".($cuser['punch_status']==='Punched IN'?'Green':'#ff8080').";padding:10px'>". $cuser['punch_status']."</font></h3>" ?>
		<br clear="all"/>
		<?php 
		 if(strlen($_SERVER['REMOTE_ADDR'])<=15){
		?>
		<br /><br /> My IP: <?php echo $_SERVER['REMOTE_ADDR']?>
		<?php
		 }
		?>
</div>




    <div class="body-content timelog" style="float:left;">
	<?php
if(isset($cuser['username'])){
	echo "<h2 style='float:left'>Hi ..".$cuser['username']."</h2>";
}


// I'm India so my timezone is Asia/Calcutta
date_default_timezone_set('Asia/Calcutta');

// 24-hour format of an hour without leading zeros (0 through 23)
$Hour = date('G');

if ( $Hour >= 5 && $Hour <= 11 ) {
    //echo "<h2 class='greeting'>Good Morning</h2>";
	echo "<img src='/timelog/admin/images/Good-Morning-Animated-4.gif' width='150' align='right' />";
} else if ( $Hour >= 12 && $Hour <= 16 ) {
	echo "<img src='/timelog/admin/images/noon.gif' width='150' align='right' />";
   // echo "<h4 class='greeting'>Good Afternoon</h4>";
} else if ( $Hour >= 17 || $Hour <= 4 ) {
    //echo "<h2 class='greeting'>Good Evening</h2>";
	echo "<img src='/timelog/admin/images/ClockCreddyGoodEvening.gif' width='100' align='right' />";
}


?>
</div>
<style>

</style>

<br clear='both'/>
       <div class="" style="width: 950px;font-size: 12px">
        <table class="table table-hover table-bordered table-striped" id="table" style="border:1px solid #000">
            <thead>
                <tr>
                    <th>SNo</th>
                    <th>AccessID</th>
                    <th>Name</th>
                    <th><?=($days==0?'Current':'Last')?> status</th>
                    <th>First Punch</th>
                    <th>Last Punch</th>
                    <th width="100">Total Out Time</th>
                    <th width="100">Total IN Time</th>
                    <th>TotalHrs</th>  
					<th>IP</th>
                </tr>              
            </thead>
            <tbody>
            <?php
            $i =1;
           foreach($result as $k =>$res){
           ?>
                <tr<?php echo ($res['same']==1?' style="border:2px solid green;" class="cuser"':'') ?>>                  
                    <td width="20"><?=$i ?></td>
                    <td width="30" style="text-align:center" bgcolor="#E8F3FF"><?=$k ?></td>
                    <td width="120"><?=$res['username'] .($res['same']==1?'&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-ok"></span>':''); ?></td>
                    <?php 
                        if($res['punch_status'] == 'No Punch'){
                    ?>
                    <td colspan="6" style="color:red;"><?=$res['punch_status']; ?></td>
                    <?php
                        }else{
                    ?>
                    <td width="80" bgcolor="<?php echo ($res['punch_count'] % 2 == 1 ? '#8AE62E' : '#FFCACA') ?>"><?=$res['punch_status']; ?></td>
                                        
                    <?php
                        $ltclass = ""; 
                        $firstpunch = date('h:i', strtotime($res['intime']));
                                            
                        if(array_key_exists($k, $shtime)){
                           $flimit =  date('h:i', strtotime($shtime[$k]));
                        }
                        else{
                            $flimit = date('h:i', strtotime("09:30"));
                        }
                        if(strtotime($firstpunch) > strtotime($flimit)){
                            $ltclass = "blue";
                        }

                    ?>
                    <td width="70" class="<?=$ltclass;?>"><?=$res['intime']; ?></td>
                    <td width="70"><?=$res['lsttime']; ?></td>
                    
                    <?php
                        $toclass = "";
                        $total_out = date('H:i', strtotime($res['tout']));
                        if($total_out > '01:00'){
                            $toclass = "blue";
                        }
                       
                    ?>
                    <td class="<?=$toclass?>" width="80"><?=$res['tout']; ?></td>

                    <td bgcolor="#E8F3FF" width="50"><?=$res['tot_in']; ?></td>
                    
                    <?php
                        $ttclass ="";
                        $total_time = date('H:i', strtotime($res['tot_time']));
                        $shifthrs = date('H:i', strtotime("09:00"));   
                        if(strtotime($total_time) < strtotime($shifthrs)){
                            $ttclass = "blue";
                        }
                       
                    ?>
					
					<td class="<?=$ttclass?>" bgcolor="#E8F3FF" width="50"><?=$res['tot_time']; ?></td>
                    <?php
                        }
                    ?>
					<td width="50">192.168.1.<?php echo intval(substr($res['reg_ip'],strrpos($res['reg_ip'], '.')+1));?></td>
                </tr>
           <?php
           $i++;
           }
            ?>
            </tbody>
        </table>
            </div>
    </div>
</div>
<?php 
if(count($cuser)==0)
	//echo "---------";
	if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		//echo "---------";
     $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
     $ip_address = $_SERVER['REMOTE_ADDR'];
}
 // print_r($_SERVER);
?>



<?php
$packed = chr(127) . chr(0) . chr(0) . chr(1);
$expanded = inet_ntop($packed);

/* Outputs: 127.0.0.1 */
//echo $expanded;

$packed = str_repeat(chr(0), 15) . chr(1);
$expanded = inet_ntop($packed);

/* Outputs: ::1 */
//echo $expanded;
?>

