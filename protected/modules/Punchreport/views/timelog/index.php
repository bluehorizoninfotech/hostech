<?php
use backend\models\PmsReport;
use backend\models\Leaves;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Punching Report System';
$this->registerJs('
    $("#log_time").datepicker({
        format: "yyyy-mm-dd",
      
            autoclose: true,
    });', \yii\web\VIEW::POS_READY);

$shtime = array();
foreach ($shifts as $key => $shift) {
    $uid = $shift['accesscard_id'];
    $shtime[$uid] = $shift['start_time'];

}
?>
<div class="site-index">
<div class="width-200 pull-left">
            <h4 class="title dark-blue font-weight-800 font-15">
                <?php
                    if ($days == 0) {
                        echo date('d-m-Y <\s\p\a\n>h:i:s A</\s\p\a\n> D');
                    } else {
                        echo date('d-m-Y l', strtotime($precent_date));
                    }
                ?>
            </h3>
	<div>
            <?php $action = Yii::$app->controller->action->id; ?>

            <?php $form = ActiveForm::begin(['method' => 'get','action' => Yii::$app->urlManager->createUrl('timelog/'.$action),'options' => ['class' => 'form-inline']]); ?>
            <div class="form-group">
                <?= $form->field($model, 'log_time', ['template' => '
               {label}{input}<i class="fa fa-calendar"></i>{error}{hint}'])->textInput(['data-default' => '','placeholder' => 'Select Date', 'class' => 'form-control pull-right width-100', 'id' => 'log_time'])->label('') ?>
            </div>
            <div class="form-group">
               <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
		<br clear="all"/>

        <div>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['timelog/'.$action, 'days' => $days - 1]) ?>">Previous | </a>
             <?php
                if ($days >= 0) {
                    echo 'Today | Next';
                } else {
                    ?>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['timelog/'.$action, 'days' => 0]) ?>">Today | </a>
            <a href="<?php echo Yii::$app->urlManager->createUrl(['timelog/'.$action, 'days' => $days + 1]) ?>">Next</a> 
             <?php
                }
                ?>
        </div>
</div>
    <div class="body-content timelog pull-left">
       <div class="width-760 font-12">
        <table class="table table-hover table-bordered table-striped black-border">
            <thead>
                <tr>
                    <th>SNo</th>
                    <th>AccessID</th>
                    <th>Name</th>
                    <th><?=($days==0?'Current':'Last')?> status</th>
                    <th>First Punch</th>
                    <th>Last Punch</th>
                    <th>Total Out Time</th>
                    <th>Total IN Time</th>
                    <th>TotalHrs</th>  
                </tr>              
            </thead>
            <tbody>
            <?php
            $i =1;
           foreach($result as $k =>$res){
           ?>
                <tr>                  
                    <td width="20"><?=$i ?></td>
                    <td width="30" class="text-align-center" bgcolor="#E8F3FF"><?=$k ?></td>
                    <td width="120"><?=$res['username']; ?></td>
                    <?php 
                        if($res['punch_status'] == 'No Punch'){
                    ?>
                    <td colspan="6" class="red-color"><?=$res['punch_status']; ?></td>
                    <?php
                        }else{
                    ?>
                    <td width="80" bgcolor="<?php echo ($res['punch_count'] % 2 == 1 ? '#8AE62E' : '#FFCACA') ?>"><?=$res['punch_status']; ?></td>
                                        
                    <?php
                        $ltclass = ""; 
                        $firstpunch = date('h:i', strtotime($res['intime']));
                                            
                        if(array_key_exists($k, $shtime)){
                           $flimit =  date('h:i', strtotime($shtime[$k]));
                        }
                        else{
                            $flimit = date('h:i', strtotime("09:30"));
                        }
                        if(strtotime($firstpunch) > strtotime($flimit)){
                            $ltclass = "blue";
                        }

                    ?>
                    <td width="70" class="<?=$ltclass;?>"><?=$res['intime']; ?></td>
                    <td width="70"><?=$res['lsttime']; ?></td>
                    
                    <?php
                        $toclass = "";
                        $total_out = date('H:i', strtotime($res['tout']));
                        if($total_out > '01:00'){
                            $toclass = "blue";
                        }
                       
                    ?>
                    <td class="<?=$toclass?>" width="80"><?=$res['tout']; ?></td>

                    <td bgcolor="#E8F3FF" width="50"><?=$res['tot_in']; ?></td>
                    
                    <?php
                        $ttclass ="";
                        $total_time = date('H:i', strtotime($res['tot_time']));
                        $shifthrs = date('H:i', strtotime("09:00"));   
                        if(strtotime($total_time) < strtotime($shifthrs)){
                            $ttclass = "blue";
                        }
                       
                    ?>
					
					<td class="<?=$ttclass?>" bgcolor="#E8F3FF" width="50"><?=$res['tot_time']; ?></td>
                    <?php
                        }
                    ?>
                </tr>
           <?php
           $i++;
           }
            ?>
            </tbody>
        </table>
            </div>
    </div>
</div>
