<?php
/* @var $this EmployeesController */
/* @var $model Employees */

$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Employees', 'url'=>array('index')),
	array('label'=>'Create Employees', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('employees-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Employees</h1>



<div class="search-form display-none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
echo CHtml::link('Create Employee', array('create'));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employees-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
		//'id',
		//'userid',
                'employee_id',
                array(
                        'name' => 'fullName',
                        'value' => '$data->getFullName()',
                ),
                'jobtitle',
                'department',
                
                array(
                    'name' => 'servicePeriod',
                    'value' => '$data->getServicePeriod()',
                ),
                
                array(
                    'name' => 'status',
                    'value' => '$data->getStatus()',
                    
                ),
            
            
		/*'accesscard_id',
		'first_name',
		'middle_name',
		'last_name',
                
		
		'jobtitle',
		'department',
		'dateofbirth',
		'father_name',
		'address',
		'marital_status',
		'spouse_name',
		'blood_group',
		'acess_card_number',
		'qualification',
		'phone_number',
		'mobile_number',
		'secondary_email',
		'description',
		'dateofjoin',
		'datereleave',
		'offerletterid',
		'salary',
		'bank_no',
		'bank_details',
		'working_status',
		'report_to',
		'employee_id',
		'image',
		'status',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
