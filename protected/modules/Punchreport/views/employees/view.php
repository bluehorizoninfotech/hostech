<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.uploadfile.min.js"></script>
<?php
/* @var $this EmployeesController */
/* @var $model Employees */
                 
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	$model->userid,
);

$this->menu=array(
	array('label'=>'List Employees', 'url'=>array('index')),
	array('label'=>'Create Employees', 'url'=>array('create')),
	array('label'=>'Update Employees', 'url'=>array('update', 'id'=>$model->userid)),
	array('label'=>'Delete Employees', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->userid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Employees', 'url'=>array('admin')),
);



Yii::app()->clientScript->registerScript('myjquery', '
        
        
//        $("#btnadd").on("click",function(){
//            $("#dynamicInput").append("Html::tag("div",  Html::fileInput("doc[]","",[]),[\'class\' =>\'form-group\'])");
//        });
            
        $("#upload").on("click", function(){
            $.ajax({
                method: "POST",
                dataType: "json",
                url: "' . $this->createAbsoluteUrl("employees/uploaddocs"). '",
                data: {id:id},
                success: function (data) {
                    if(data.status == "success"){  
                    }
                    else{
                        alert(data.status);
                    }
                }
            });


        });
   var id = $("#empid").val();
  // alert(id);
$("#multipleupload").uploadFile({
    url: "' . $this->createAbsoluteUrl('employees/uploaddocs') . '",
    multiple:true,
    dragDrop:true,
    fileName:"docs",
    formData: {"id": id},
    maxFileSize: 1024 * 1024 * 20,
    allowedTypes: "pdf, doc, docx, jpg, png, jpeg",
    messages: {
        maxFileSize: "File exceeds maximum allowed size of 20MB",
        allowedTypes: "File type not supported.",
    },
}); 

$(".btn-ajax-modal").click(function (){
    $("#myModal").modal({
        backdrop: "static",
        keyboard: false,
        show: true
    });
});

var uid = $("input[name=uid]").val();
$("#userimguploader").uploadFile({
    url: "' . $this->createAbsoluteUrl("employees/uploaduserimage") . '",
    fileName: "file",
    formData: {"id": uid},
    multiple: false,
    dragDrop: false,
    maxFileCount: 1,
    maxFileSize: 1024 * 1024 * 20,
    allowedTypes: "jpg,png,jpeg",
    messages: {
        maxFileSize: "File exceeds maximum allowed size of 20MB",
        allowedTypes: "File type not supported.",
    },
    onSuccess: function (files, data, xhr, pd)
    {   
        location.reload();

        /*if(id==user_id){
        var image = $(".change_image").attr("src");
        var arr = image.split("/");
        $(".change_image[src=\'" + image + "\']").attr("src", "/" + arr[1] + "/thumbnails/" + data);
        }*/
    }
});

$(".delnote").click(function(){
    var note_id = $(this).attr("id");
    if(confirm("Are you sure you want to delete this?")){
        $.ajax({
            method: "POST",
            dataType: "json",
            url: "' . $this->createAbsoluteUrl("employees/deletenote") . '",
            data: {id:note_id},
            success: function (data) {
                if(data != "error"){  
                    $("#div"+data).remove();   
                }
                else{
                    alert(data);
                }
            }
        });
    }
});

$(".deldoc").click(function(){
    var doc_id = $(this).attr("id");
    
    if(confirm("Are you sure you want to delete this?")){
        $.ajax({
            method: "POST",
            dataType: "json",
            url: "' . $this->createAbsoluteUrl("employees/deletedoc") . '",
            data: {id:doc_id},
            success: function (data) {
                if(data != "error"){  
                    $("#tr"+data).remove();   
                }
                else{
                    alert(data);
                }
            }
        });
    }
});

$(".editable").each(function(){
        var label = $(this);
        var nid = label.attr("id");
        label.after("<input type = \'text\' style = \'display:none\' class =\'form-control\' />");
        var textbox = $(this).next();
        textbox[0].name = this.id.replace("lbl", "txt");
        textbox.val(label.html());
        label.click(function () {
            $(this).hide();
            $(this).next().show();
        });

        textbox.focusout(function () {
        var labeltxt = $(this);
            var note = $(this).val();
            $.ajax({
            method: "POST",
            url: "' . $this->createAbsoluteUrl("employees/editnote") . '",
            data: {id:nid,note:note},
            success: function (data) {
                    var value = labeltxt.val();
                    labeltxt.hide();
                    labeltxt.prev().html(value);
                    labeltxt.prev().show();
                }
            }); 
        });
    });
    
$(".edit1").click(function(){
    var noteid = $(this).attr("id");
    $("input#txt"+noteid).show();
    $("p#span"+noteid).hide();
});



');


?>


<div class="employees-view">
<!--<h1>View Employees #<?php echo $model->userid; ?></h1>-->
    <p>
        <?php echo CHtml::link('Update', $url = $this->createAbsoluteUrl('employees/update', array('id' => $model->userid)), array('class' => 'btn btn-primary')) ?>
    </p>  
    
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body box-profile change_profile">
                    <img class="profile-user-img img-responsive img-circle" src="<?=  Yii::app()->request->baseUrl .'/uploads/profile/'. $model->image; ?>" alt="User profile picture">
                    <div class="edit_profile" id="<?php echo $model->userid; ?>" class="btn-ajax-modal" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" >
                        <input type="hidden" name="uid" value="<?php echo $model->userid; ?>"/>
                        <i class="glyphicon glyphicon-edit"></i>Edit Profile Image</div>
                      
                    <!-- pop up for upload image -->

                    <div id="myModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content" id="modalContent1">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                       <h4 class="modal-title">UPLOAD IMAGE</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="userimguploader" class="<?php echo $model->userid; ?>">Upload</div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- pop up for upload image -->

                    <h3 class="profile-username text-center"><?=$model->first_name." ".$model->middle_name." ".$model->last_name ?></h3>
                    <p class="text-muted text-center"><?=$model->jobtitle?></p>

                    <ul class="list-group list-group-unbordered">
                        <b class="btn btn-primary btn-block">
                        <?php
                        if ($model->status == "1") {
                            echo 'Currently Working(';
                            if ($model->working_status == "1") {
                                echo 'Permanent)';
                            } elseif ($model->working_status == "0") {
                                echo 'Under Probation)';
                            }
                        }
                        ?>  
                        <?php
                        if ($model->status == "0") {
                            echo 'Resigned';
                        }
                        ?> 

                        </b>
                        <br/><br/>
                        <li class="list-group-item">
                        <b>Experience</b> <a class="pull-right">
                        <?php
                          $doj = $model->dateofjoin;
                          $datetime1 = new DateTime($doj);
                          $datetime2 = new DateTime(date('Y-m-d'));
                          $interval = $datetime1->diff($datetime2);
                          $str1 = $interval->format('%y years, %m months, and %d days');
                          echo $str1;
                        ?>  
                        </a>
                        </li>
                        <li class="list-group-item">
                          <b>Joining Date</b> <a class="pull-right">
                            <?php 
                                $dojj = date_create($model->dateofjoin);
                                $doj2 = date_format($dojj,"d-m-Y");                     
                                echo $doj2; 
                            ?></a>
                        </li>
                        <li class="list-group-item">
                          <b>Reporting to</b> <a class="pull-right"><?=$model->report_to?></a>
                        </li>
                        <li class="list-group-item">
                          <b>Official Email</b> <a class="pull-right"><?=$model->secondary_email?></a>
                        </li>
                        <li class="list-group-item">
                          <b>Offer Letter ID</b> <a class="pull-right"><?=$model->offerletterid?></a>
                        </li>
                        <li class="list-group-item">
                          <b>Access Card Number</b> <a class="pull-right"><?=$model->acess_card_number?></a>
                        </li>
                        <li class="list-group-item">
                          <b>Salary</b> <a class="pull-right"><?=$model->salary?></a>
                        </li>
                        <li class="list-group-item">
                          <b>Bank A/c Number</b> <a class="pull-right"><?=$model->bank_no?></a>
                        </li>

                    </ul>  
                </div><!-- /.box-body -->
            </div><!-- /.box --> 
        </div>
        <div class="col-md-5">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                    <li><a href="#notes" data-toggle="tab">Notes</a></li>
                    <li><a href="#document" data-toggle="tab">Documents</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="profile">
                        <div class="post width-85-percentage">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Employee Personal profile</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body box-profile">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <b>Father Name</b> <a class="pull-right"> <?=$model->father_name; ?>
                                            </a>
                                        </li>  
                                        <li class="list-group-item">
                                            <b>Marital Status</b> <a class="pull-right"> 
                                                <?php
                                                if ($model->marital_status == "1") {
                                                    echo 'Single';
                                                } else {
                                                    echo 'Married';
                                                }
                                                ?>
                                            </a>
                                        </li> 
                                        <?php if ($model->marital_status == "0") { ?>
                                            <li class="list-group-item">
                                                <b>Spouse Name</b> <a class="pull-right"><?php echo $model->spouse_name; ?>
                                                </a>
                                            </li> 
                                        <?php } ?>
                                        <br>
                                        <strong><i class="glyphicon glyphicon-map-marker margin-r-5"></i> Address</strong>
                                        <p class="text-muted"><?php echo $model->address; ?></p>
                                        <li class="list-group-item">
                                            <b>Personal email address</b> <a class="pull-right"> <?php //echo $model->user_email; ?>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Land Number/Alternate Number</b> <a class="pull-right"> <?php echo $model->phone_number; ?>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Mobile number</b> <a class="pull-right"> <?php echo $model->mobile_number; ?>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Blood Group</b> <a class="pull-right"><?php echo $model->blood_group; ?>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Date of birth</b> <a class="pull-right">
                                                <?php
                                                $dob = $model->dateofbirth;
                                                $dob2 = date("d-m-Y", strtotime($dob));
                                                echo $dob2;
                                                $bdatetime1 = new DateTime($dob);
                                                $bdatetime2 = new DateTime(date('Y-m-d'));
                                                $interval = $bdatetime1->diff($bdatetime2);
                                                echo "(".$interval->format('%y Years old').")";
                                                ?>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Qualification</b> <a class="pull-right"><?php echo $model->qualification; ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- /.box -->
                            
                        </div><!-- /.post -->
                        
                    </div><!-- /.active tab pane -->
                    <div class="tab-pane" id="notes">
                        <?php $form=$this->beginWidget('CActiveForm', array(
                                    'action'=>$this->createAbsoluteUrl('employees/addnotes', array('id' => $model->userid)),
                                    'method' => 'post',
                                    'htmlOptions' => ['class' => 'form-horizontal']
                            )); ?>
        
                            <div class='form-group margin-bottom-none'>   
                                <div class='col-sm-9'> 
                                    
                                    <?php echo $form->textField($notesmodel,'note', array('class' => 'form-control', 'placeholder' => 'Note')); ?>
                                    <?php echo $form->error($notesmodel,'note'); ?>
                                  
                                </div>
                                <div class='col-sm-3'>
                                    <?php echo CHtml::submitButton('Add', array('class' => 'btn btn-primary pull-right btn-block btn-sm')) ?>
                                </div>
                            </div>
        
        
                            <?php $this->endWidget(); ?>
        
        
        
                          
                            <?php  if(!empty($notes)) {?>
                            <?php foreach($notes as $note){ ?>
                        <div id="div<?= $note['id']; ?>">      
                            <p class="note editable" id="<?= $note['id'];?>" style="font-size:16px;"><?= $note['note']; ?></p>
                            <input type="text" class="display-none" id="txt<?= $note['id'];?>" value="<?= $note['note']; ?>" class="form-control editnote"/>
                            <span class="pull-left font-weight-400 font-italic"><?= date('d M Y', strtotime($note['date'])); ?></span>
                            <a class="pull-right delnote cursor-pointer margin-top-0 margin-bottom-0 margin-left-5 margin-right-5" id="<?= $note['id']; ?>" data-id=""><i class="glyphicon glyphicon-trash"></i></a>
                        <br/><hr/>
                        </div>
                            <?php }} ?>
                    </div>
                    
                    <div class="tab-pane" id="document">
                        <input type="hidden" value="<?=$userlogin->user_email?>" id="empid">
                        <div id="multipleupload">Upload</div>
                        
                        <div>
                        
                        <?php
                         
                       
                       if(!empty($docs)){
                           
                        ?>  
                        <h4></h4>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Document Name</th>
                                    <th colspan="2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $i = 1;
                                
                                foreach($docs as $doc){ ?>
                                <tr id="tr<?= $doc['id']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $doc['document']; ?></td>
                                    <td>
                                        <a class="cursor-pointer margin-top-0 margin-bottom-0 margin-left-5 margin-right-5" class="deldoc" id="<?= $doc['id']; ?>" data-id=""><i class="glyphicon glyphicon-trash"></i></a>
                                        &nbsp; &nbsp; &nbsp;
                                        <?php //= Yii::app()->request->baseUrl .'/uploads/documents/'. $doc['document']?>
                                        <a href="" target="_blank " data-toggle="modal" data-target="#myModal<?php echo $i; ?>" data-backdrop="static" data-keyboard="false" >
                                            <i class="glyphicon glyphicon-eye-open" title="view doc"></i></a> 
                                        &nbsp;&nbsp;&nbsp;
                                        <?= CHtml::link( '<i class="glyphicon glyphicon-download" title="download"></i>', $url = $this->createAbsoluteUrl('employees/downloaddoc', array('doc' =>$doc['document']))) ?>
                                        
                                       
                                    </td>
                                    
                                    
                                    <div id="myModal<?php echo $i; ?>" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content" >
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                       <h4 class="modal-title"><?php echo $doc['document']; ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php
                                                     $ext = pathinfo($doc['document'], PATHINFO_EXTENSION); echo "<br/><br/><br/>";

                                                    if($ext == 'pdf'){
                                                        echo '<iframe src="'.Yii::app()->request->baseUrl .'/uploads/documents/'. $doc['document'].'"></iframe>';
                                                   }
                                                    elseif($ext == 'jpg' || $ext == 'png'){
                                                        
                                                        echo CHtml::image(Yii::app()->request->baseUrl.'/uploads/documents/'.$doc['document'],"",array('class' => 'img-responsive'));                                                  
                                                    }else{
                                                         echo "Unsupported file type";
                                                    } 
                                                     ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                 
                                </tr>
                                <?php $i++; }  ?>
                            </tbody>
                            
                            
                        </table>
                       
                       
                               
                        <?php      
                            
                       } else{
                           echo "No documents found";
                       } 
                       ?>

                       
                    </div>
                    
                </div>
                
                
            </div>
            
            
            
        </div>
        
        
    </div>
    
    
</div>




