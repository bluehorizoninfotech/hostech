<?php
/* @var $this EmployeesController */
/* @var $model Employees */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript('myjquery', "

    $('.marstat').change(function () {
        var st = $('input[name=\"Employees[marital_status]\"]:checked').val();
        if (st === '0') {
            $('#hiddiv').show();
        } 
        else{
            $('#hiddiv').hide();
        }
    });
    $('#status').change(function () {
        var status = $('#status option:selected').val();
        if (status === '1') {
            $('#hiddiv2').show();
            $('#hiddiv3').hide();
        } 
        else if(status === '0'){
            $('#hiddiv3').show();
            $('#hiddiv2').hide();
        }
        else{
            $('#hiddiv2').hide();
            $('#hiddiv3').hide();
        }
        
    });
    dt = new Date();
    dt.setFullYear(new Date().getFullYear() - 18);
    $('.dob').datepicker({
        format: 'yyyy-mm-dd',
        viewMode: 'years',
        endDate: dt,
        autoclose: true,
    });
    
    $('.doj').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
    
    $('#dorel').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
    var stt = '".$model->isNewRecord."';
    if(stt == ''){
        var status = '".$model->status."';
         if (status === '1') {
            $('#hiddiv2').show();
            $('#hiddiv3').hide();
        } 
        else if(status === '0'){
            $('#hiddiv3').show();
            $('#hiddiv2').hide();
        }
        else{
            $('#hiddiv2').hide();
            $('#hiddiv3').hide();
        }
    }


");

Yii::app()->clientScript->registerCss('mycss', "

#hiddiv, #hiddiv2, #hiddiv3 { display: none; }


.form .errorMessage {
    color: red;
    font-size: 0.9em;
}

");
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
<div class="form">
    
    <div class="employees-form ">
        
        
        
        
        
    </div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees-form',
	'enableAjaxValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'validateOnType' => false,
        ),
        //'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4><b>Personal Details</b></h4>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group col-md-3 col-sm-6">
                        <?php echo $form->labelEx($model,'employee_id'); ?>
                        <?php echo $form->textField($model,'employee_id',array('size'=>30,'maxlength'=>30, 'class' => 'form-control' )); ?>
                        <?php echo $form->error($model,'employee_id'); ?>
                    </div>

                    <div class="form-group col-md-3 col-sm-6">
                        <?php echo $form->labelEx($model,'first_name'); ?>
                        <?php echo $form->textField($model,'first_name',array('size'=>30,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'first_name'); ?>
                        
                    </div>
                    <div class="form-group col-md-3 col-sm-6">
                        <?php echo $form->labelEx($model,'middle_name'); ?>
                        <?php echo $form->textField($model,'middle_name',array('size'=>30,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'middle_name'); ?>
                    </div>
                    <div class="form-group col-md-3 col-sm-6">
                        <?php echo $form->labelEx($model,'last_name'); ?>
                        <?php echo $form->textField($model,'last_name',array('size'=>30,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'last_name'); ?>
                    </div> 
                </div>
                
                <div class="col-md-12">
                
                    <div class="form-group col-md-4 col-sm-12">
                        <?php echo $form->labelEx($userlogin,'user_email'); ?>
                        <?php echo $form->textField($userlogin,'user_email',array('size'=>30,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($userlogin,'user_email'); ?>
                    </div>

                    <div class="form-group col-md-4 col-sm-12">
                        <?php echo $form->labelEx($model,'secondary_email'); ?>
                        <?php echo $form->textField($model,'secondary_email',array('size'=>30,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'secondary_email'); ?>
                    </div>

                    <div class="form-group col-md-4 col-sm-12">
                        <?php echo $form->labelEx($userlogin,'user_password'); ?>
                        <?php echo $form->passwordField($userlogin,'user_password',array('size'=>30,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($userlogin,'user_password'); ?>
                    </div>
                </div>
                
                
                <div class="col-md-12">
                    <div class="form-group col-md-4 col-sm-12"> 
                        <?php echo $form->labelEx($model,'dateofbirth'); ?>
                        <?php echo $form->textField($model,'dateofbirth', array('class' => 'dob form-control')); ?>
                        <?php echo $form->error($model,'dateofbirth'); ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-12">
                        <?php echo $form->labelEx($model,'father_name'); ?>
                        <?php echo $form->textField($model,'father_name',array('size'=>30,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'father_name'); ?>
                    </div>

                    <div class="form-group col-md-4 col-sm-12">
                        
                        <?php echo $form->labelEx($model,'blood_group'); ?>
                        <?php echo $form->dropDownList($model,'blood_group',array('A Positive' => 'A Positive', 'A Negative' => 'A Negative', 'B Positive' => 'B Positive','B Negative' => 'B Negative','AB Positive' => 'AB Positive','AB Negative' => 'AB Negative', 'O Positive' => 'O Positive',
                           'O Negative' => 'O Negative', 'Unknown' => 'Unknown'), array('empty'=>'--Choose Blood group--'), array('size'=>50,'maxlength'=>50, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'blood_group'); ?>
                    </div>

                </div>
                
                <div class="col-md-12">
                    <div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'address'); ?>
                        <?php echo $form->textArea($model,'address',array('rows'=>5, 'cols'=>30,  'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'address'); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'marital_status'); ?>
                        <?php echo $form->radioButtonList($model,'marital_status', array('1'=>'Single','0'=>'Married'), 
                                array(
                        'separator'=>'&nbsp;',
                        'labelOptions'=>array('class'=>'modal-radio radio-inline '),
                        'class'=>"marstat"
                )); ?>
                        <?php echo $form->error($model,'marital_status'); ?>
                        <!--
//                                [    
//                                    'item' => function($index, $label, $name, $checked, $value) {
//                                        $check = $checked ? ' checked="checked"' : '';
//                                        $return = '<label class="modal-radio radio-inline">';
//                                        $return .= '<input type="radio" class="marstat" name="' . $name . '" value="' . $value . '" "'.$check.'" tabindex="3">';
//                                        $return .= '<i></i>';
//                                        $return .= '<span>' . ucwords($label) . '</span>';
//                                        $return .= '</label>';
//                                        return $return;
//                                    },
//
//                                ]
-->

                    </div>
                    <div class="form-group col-md-6" id="hiddiv">
                        
                        <?php echo $form->labelEx($model,'spouse_name'); ?>
                        <?php echo $form->textField($model,'spouse_name',array('size'=>60,'maxlength'=>250, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'spouse_name'); ?>
                    </div> 

                </div> 
                
                <div class="col-md-12">     
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'qualification'); ?>
                        <?php echo $form->textField($model,'qualification',array('size'=>60,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'qualification'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'phone_number'); ?>
                        <?php echo $form->textField($model,'phone_number',array('size'=>30,'maxlength'=>30, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'phone_number'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'mobile_number'); ?>
                        <?php echo $form->textField($model,'mobile_number',array('size'=>30,'maxlength'=>30, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'mobile_number'); ?>
                    </div>
                </div>
                <div class="col-md-12"> 

                    <!--<div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'Upload Image'); ?>
                        <?php echo $form->fileField($model,'image',array('class' => 'form-control')); ?>
                        <?php echo $form->error($model,'image'); ?>
                    </div>-->
                </div> 
                
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4><b>Job Details</b></h4>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'accesscard_id'); ?>
                        <?php echo $form->textField($model,'accesscard_id'); ?>
                        <?php echo $form->error($model,'accesscard_id'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'acess_card_number'); ?>
                        <?php echo $form->textField($model,'acess_card_number',array('size'=>60,'maxlength'=>250, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'acess_card_number'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'Offer letter ID'); ?>
                        <?php echo $form->textField($model,'offerletterid',array('size'=>60,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'offerletterid'); ?>
                    </div>
                </div>
                <div class="col-md-12">    
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'jobtitle'); ?>
                        <?php echo $form->textField($model,'jobtitle',array('size'=>60,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'jobtitle'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?php
                        $deptList = CHtml::listData(Department::model()->findAll(), 'department_name', 'department_name')
                        ?>
                        <?php echo $form->labelEx($model,'department'); ?>
                        <?php echo $form->dropDownList($model,'department',$deptList, array('class' => 'form-control'), array('empty' => '--Choose Department--')); ?>
                        <?php echo $form->error($model,'department'); ?>
                    </div>
                    <div class="form-group col-md-4">
                        <?php echo $form->labelEx($model,'salary'); ?>
                        <?php echo $form->textField($model,'salary',array('size'=>50,'maxlength'=>50, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'salary'); ?>
                    </div>
                    
                </div>
                <div class="col-md-12"> 
                    <div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'Bank Account details'); ?>
                        <?php echo $form->textArea($model,'bank_details',array('rows'=>4, 'cols'=>30, 'size'=>60,'maxlength'=>500, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'bank_details'); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'Bank Account Number'); ?>
                        <?php echo $form->textField($model,'bank_no',array('size'=>60,'maxlength'=>250, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'bank_no'); ?>
                        
                    </div>
                </div>
                <div class="col-md-12"> 
                    
                    <div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'Date of Joining'); ?>
                        <?php echo $form->textField($model,'dateofjoin', array('class' => 'doj form-control')); ?>
                        <?php echo $form->error($model,'dateofjoin'); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'Reporting Person Name'); ?>
                        <?php echo $form->textField($model,'report_to',array('size'=>60,'maxlength'=>100, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model,'report_to'); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?php echo $form->labelEx($model,'status'); ?>
                        <?php echo $form->dropDownList($model,'status', array('0' => 'Resigned', '1' => 'Working'), array('empty' => '--Choose Status--', 'id' => 'status', 'class' => 'form-control' )); ?>
                        <?php echo $form->error($model,'status'); ?>
                    </div>
                    <div class="form-group col-md-6" id="hiddiv2">
                        <?php echo $form->labelEx($model,'working_status'); ?>
                        <?php echo $form->dropDownList($model,'working_status',array('1' => 'Permanent', '0' => 'Under Probation'), array('class' => 'form-control')); ?>
                        <?php echo $form->error($model,'working_status'); ?>
                    </div>
                    <div class="form-group col-md-6" id="hiddiv3">
                        <?php echo $form->labelEx($model,'Date of Releaving'); ?>
                        <?php echo $form->textField($model,'datereleave', array('class' => 'form-control', 'id' => 'dorel')); ?>
                        <?php echo $form->error($model,'datereleave'); ?>
                    </div>
                    
                </div>
                <div class="form-group pull-right">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Update', array('class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')) ?>
                </div>
            </div>
            
        </div>
    </div>
    

<?php $this->endWidget(); ?>

</div><!-- form -->