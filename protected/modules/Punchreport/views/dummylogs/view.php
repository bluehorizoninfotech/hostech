<?php
/* @var $this DummylogsController */
/* @var $model Dummylogs */

$this->breadcrumbs=array(
	'Dummylogs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Dummylogs', 'url'=>array('index')),
	array('label'=>'Create Dummylogs', 'url'=>array('create')),
	array('label'=>'Update Dummylogs', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Dummylogs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dummylogs', 'url'=>array('admin')),
);
?>

<h1>View Dummylogs #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'dummycard_id',
		'date',
		'usercard_id',
	),
)); ?>
