<?php
/* @var $this DummylogsController */
/* @var $model Dummylogs */

$this->breadcrumbs=array(
	'Dummylogs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Dummylogs', 'url'=>array('index')),
	array('label'=>'Create Dummylogs', 'url'=>array('create')),
	array('label'=>'View Dummylogs', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Dummylogs', 'url'=>array('admin')),
);
?>

<h1>Update Dummylogs <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>