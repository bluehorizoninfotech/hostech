<?php
/* @var $this DummylogsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dummylogs',
);

$this->menu=array(
	array('label'=>'Create Dummylogs', 'url'=>array('create')),
	array('label'=>'Manage Dummylogs', 'url'=>array('admin')),
);
?>

<h1>Dummylogs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
