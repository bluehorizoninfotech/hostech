<?php
/* @var $this DummylogsController */
/* @var $model Dummylogs */

$this->breadcrumbs=array(
	'Dummylogs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Dummylogs', 'url'=>array('index')),
	array('label'=>'Manage Dummylogs', 'url'=>array('admin')),
);
?>

<h1>Create Dummylogs</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>