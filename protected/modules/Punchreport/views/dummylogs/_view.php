<?php
/* @var $this DummylogsController */
/* @var $data Dummylogs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dummycard_id')); ?>:</b>
	<?php echo CHtml::encode($data->dummycard_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usercard_id')); ?>:</b>
	<?php echo CHtml::encode($data->usercard_id); ?>
	<br />


</div>