<?php
/* @var $this ShiftController */
/* @var $model ShiftTimings */

$this->breadcrumbs=array(
	'Shift Timings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ShiftTimings', 'url'=>array('index')),
	array('label'=>'Manage ShiftTimings', 'url'=>array('admin')),
);
?>

<h1>Create ShiftTimings</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>