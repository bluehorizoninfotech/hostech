<?php
/* @var $this ShiftController */
/* @var $model ShiftTimings */

$this->breadcrumbs=array(
	'Shift Timings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ShiftTimings', 'url'=>array('index')),
	array('label'=>'Create ShiftTimings', 'url'=>array('create')),
	array('label'=>'Update ShiftTimings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ShiftTimings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShiftTimings', 'url'=>array('admin')),
);
?>

<h1>View ShiftTimings #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'accesscard_id',
		'start_time',
		'end_time',
	),
)); ?>
