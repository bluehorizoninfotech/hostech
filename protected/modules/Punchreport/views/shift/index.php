<?php
/* @var $this ShiftController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shift Timings',
);

$this->menu=array(
	array('label'=>'Create ShiftTimings', 'url'=>array('create')),
	array('label'=>'Manage ShiftTimings', 'url'=>array('admin')),
);
?>

<h1>Shift Timings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
