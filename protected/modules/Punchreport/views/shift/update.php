<?php
/* @var $this ShiftController */
/* @var $model ShiftTimings */

$this->breadcrumbs=array(
	'Shift Timings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ShiftTimings', 'url'=>array('index')),
	array('label'=>'Create ShiftTimings', 'url'=>array('create')),
	array('label'=>'View ShiftTimings', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ShiftTimings', 'url'=>array('admin')),
);
?>

<h1>Update ShiftTimings <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>