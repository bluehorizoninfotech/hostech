<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker3.css" />
<?php
/* @var $this LeaveDetailsController */
/* @var $model LeaveDetails */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('leave-details-grid', {
		data: $(this).serialize()
	});
	return false;
});

//$('.btn-ajax-modal').click(function (){
//    $('#modal_category').modal({
//        backdrop: 'static',
//        keyboard: false,
//        show: true
//    });
//});

$('.datefrom').datepicker({
    format: 'yyyy-mm-dd',
   // startDate : new Date(),
        autoclose: true,
});

$('.dateto').datepicker({
    format: 'yyyy-mm-dd',
   // startDate : new Date(),
        autoclose: true,
});



");



$activeusers = Employees::model()->findAll(array('condition' => 'accesscard_id != 0 AND status = 1'));
$activeusersList=CHtml::listData($activeusers,'id','first_name');

?>
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'leave-details-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Employee'); ?>
                <?php echo $form->dropDownList($model,'emp_id',$activeusersList, array('empty' => '--select employee--','class' =>'form-control')); ?>
                <?php echo $form->error($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Date from'); ?>
                <?php echo $form->textField($model,'date_from', array('data-default'=>'','class' =>'form-control datefrom','id' => 'datefrom')); ?>
                <?php echo $form->error($model,'date_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_to'); ?>
                <?php echo $form->textField($model,'date_to', array('data-default' => '', 'class' => 'form-control dateto', 'id' => 'dateto')); ?>
                <?php echo $form->error($model,'date_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'No of Days'); ?>
                <?php echo $form->textField($model,'num_days',array('size'=>10,'maxlength'=>10, 'class' =>'form-control')); ?>
                <?php echo $form->error($model,'num_days'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'leave_type'); ?>
                    <?php echo $form->dropDownList($model,'leave_type', array('CL' => 'Casual leave', 'PL' => 'Paid leave', 'LOP' => 'Loss of pay'),array('empty' => '--Choose Leave Type--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model,'leave_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->