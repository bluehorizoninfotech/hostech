<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker3.css" />
<?php
/* @var $this LeaveDetailsController */
/* @var $model LeaveDetails */

$this->breadcrumbs=array(
	'Leave Details'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List LeaveDetails', 'url'=>array('index')),
	array('label'=>'Create LeaveDetails', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('leave-details-grid', {
		data: $(this).serialize()
	});
	return false;
});

//$('.btn-ajax-modal').click(function (){
//    $('#modal_category').modal({
//        backdrop: 'static',
//        keyboard: false,
//        show: true
//    });
//});

$('.datefrom').datepicker({
    format: 'yyyy-mm-dd',
   // startDate : new Date(),
        autoclose: true,
});

$('.dateto').datepicker({
    format: 'yyyy-mm-dd',
   // startDate : new Date(),
        autoclose: true,
});



");



$activeusers = Employees::model()->findAll(array('condition' => 'accesscard_id != 0 AND status = 1'));
$activeusersList=CHtml::listData($activeusers,'id','first_name');

?>


<div class="leave-details-index">
    <h1>Manage Leave Details</h1>

    <div class="search-form display-none">
    <?php $this->renderPartial('_search',array(
            'model'=>$model,
    )); ?>
    </div><!-- search-form -->

    <p>
        <?= CHtml::button('Add Leave Details', array(
            'class' => 'btn btn-success btn-ajax-modal',
            'data-target' => '#modal_category',
            'data-toggle'=>"modal",
            'data-backdrop'=>"static",
            'data-keyboard'=>"false" 
        )); ?>
    </p>
    
    
    
</div>




<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'leave-details-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
//                array(
//                        'name' => 'fullName',
//                        'value' => '$data->getFullName()',
//                ),
		'emp_id',
		'date_from',
		'date_to',
		'num_days',
		'leave_type',
		/*
		'notes',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>


<div id="modal_category" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content  col-md-12" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title">Add Leave Details</h4>
            </div>
            <div class="modal-body">
                <div class="form">
                <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'leave-details-form',
                            'action'=>$this->createAbsoluteUrl('leavedetails/create'),
                            'method' => 'post',
                            'htmlOptions' => ['class' => 'form form-horizontal']
                    )); ?>

                
                
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'Employee'); ?>
                    <?php echo $form->dropDownList($model,'emp_id',$activeusersList, array('empty' => '--select employee--','class' =>'form-control')); ?>
                    <?php echo $form->error($model,'emp_id'); ?>
                </div> 
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'leave_type'); ?>
                    <?php echo $form->dropDownList($model,'leave_type', array('CL' => 'Casual leave', 'PL' => 'Paid leave', 'LOP' => 'Loss of pay'),array('empty' => '--Choose Leave Type--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model,'leave_type'); ?>
                </div> 
                    
                <div class="clearfix"></div><br/>

                <div class="col-md-4">
                    <?php echo $form->labelEx($model,'Date from'); ?>
                    <?php echo $form->textField($model,'date_from', array('data-default'=>'','class' =>'form-control datefrom','id' => 'datefrom')); ?>
                    <?php echo $form->error($model,'date_from'); ?>
                </div>

                <div class="col-md-4">
                    <?php echo $form->labelEx($model,'date_to'); ?>
                    <?php echo $form->textField($model,'date_to', array('data-default' => '', 'class' => 'form-control dateto', 'id' => 'dateto')); ?>
                    <?php echo $form->error($model,'date_to'); ?>
                </div>

                <div class="col-md-4">
                    <?php echo $form->labelEx($model,'No of Days'); ?>
                    <?php echo $form->textField($model,'num_days',array('size'=>10,'maxlength'=>10, 'class' =>'form-control')); ?>
                    <?php echo $form->error($model,'num_days'); ?>
                </div>

                <div class="clearfix"></div><br/>

                <div class="col-md-12">
                        <?php echo $form->labelEx($model,'notes'); ?>
                        <?php echo $form->textArea($model,'notes',array('rows'=>4, 'cols'=>50, 'class' =>'form-control')); ?>
                        <?php echo $form->error($model,'notes'); ?>
                </div><br/>
                <div class="clearfix"></div><br/>
                <div class="col-md-6 buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
                </div>
                
                
              
                
                <?php $this->endWidget(); ?>
                </div>
                <br/><br/><br/>
                
            </div>
        </div>
    </div>
</div>