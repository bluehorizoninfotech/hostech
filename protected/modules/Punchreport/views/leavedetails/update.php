<?php
/* @var $this LeaveDetailsController */
/* @var $model LeaveDetails */

$this->breadcrumbs=array(
	'Leave Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeaveDetails', 'url'=>array('index')),
	array('label'=>'Create LeaveDetails', 'url'=>array('create')),
	array('label'=>'View LeaveDetails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage LeaveDetails', 'url'=>array('admin')),
);
?>

<h1>Update LeaveDetails <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>