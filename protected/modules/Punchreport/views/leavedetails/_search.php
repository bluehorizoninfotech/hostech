<?php
/* @var $this LeaveDetailsController */
/* @var $model LeaveDetails */
/* @var $form CActiveForm */



Yii::app()->clientScript->registerScript('myjquery', "

//    $('.btn-ajax-modal').click(function (){
//        var elm = $(this),
//            target = elm.attr('data-target'),
//            ajax_body = elm.attr('value');
//
//        $(target).modal('show')
//            .find('.modal-content1')
//            .load(ajax_body);
//    });

    $('.year').datepicker({
        format: 'yyyy',
       // startDate : new Date(),
            autoclose: true,
    });

    $('.dateto').datepicker({
        format: 'yyyy-mm-dd',
       // startDate : new Date(),
            autoclose: true,
    });

    $('#month').on('change', function(){
        if($(this).val() != '00'){
            $('#quarter').prop('disabled', true);
        }
        else{
            $('#quarter').prop('disabled', false);
        }
    });
    $('#quarter').on('change', function(){
        if($(this).val() != '00'){
            $('#month').prop('disabled', true);
        }
        else{
            $('#month').prop('disabled', false);
        }
    });

");


$activeusers = Employees::model()->findAll(array('condition' => 'accesscard_id != 0 AND status = 1'));
$activeusersList=CHtml::listData($activeusers,'id','first_name');


$minyr = LeaveDetails::model()->find(array('order' => "date_from ASC"));
$mindate = date_create($minyr->date_from);
$minyear = date_format($mindate, "Y");
$maxyear = date("Y");
$years = array();
for($d = $maxyear; $d>=$minyear; $d--){
    $years[$d] = $d;
}


$months = array(
    "00" => "All",
    "01" => "January",
    "02" => "February",
    "03" => "March",
    "04" => "April",
    "05" => "May",
    "06" => "June",
    "07" => "July",
    "08" => "August",
    "09" => "September",
    "10" => "October",
    "11" => "November",
    "12" => "December",
);

$quarters = array(
    "00" => "All",
    "01" => "1st quarter(January-March)",
    "02" => "2nd quarter(April-June)",
    "03" => "3rd quarter(July-September)",
    "04" => "4th quarter(October-December)",
);






?>

<div class="leave-details-search">

   <div class="col-md-12">
        <div class="post-search">
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'action'=>Yii::app()->createUrl($this->route),
                    'method'=>'get',
            )); ?>
            <div class="form-group col-md-2">
                <?php echo $form->label($model,'Employee'); ?><br/>
		<?php echo $form->dropDownList($model,'emp_id',$activeusersList, array('empty' => '--select employee--')); ?>
            </div>
            <div class="form-group col-md-2">
                <?php echo $form->label($model,'Year'); ?><br/>
		<?php echo $form->dropDownList($model,'year',$years, array('empty' => '--Select Year--')); ?>
            </div>
            <div class="form-group col-md-2">
                <?php echo $form->label($model,'Month'); ?><br/>
		<?php echo $form->dropDownList($model,'month',$months, array('id' => 'month','empty' => '--Select Month--')); ?>
            </div>
            <div class="form-group col-md-3">
                <?php echo $form->label($model,'Quarter'); ?><br/>
		<?php echo $form->dropDownList($model,'quarter',$quarters, array('id' => 'quarter','empty' => '--Select Quarter--')); ?>
            </div>

            <div class="form-group col-md-2">
                <br/>
                <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary')); ?>
                <?php echo CHtml::link('Reset','#', array('class'=>'btn btn-default')); ?>
         
            </div>

            <?php $this->endWidget(); ?>
        </div>
        
        
    </div>

</div>



<div class="wide form">



</div><!-- search-form -->