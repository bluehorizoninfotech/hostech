<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<div class="pull-left"><h1 class="text-align-left margin-left-20">BHI-PMS: Dashboard</i></h1></div>


<br clear="all"/>


<?php /*?><div id="dashboard">
    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/timesheet.png', '', array('class' => 'dasimage')) . '<span class="dblink">Time sheets</span>
  </div>', array('/timeEntry/index')); ?>  

    <?php if (Yii::app()->user->role == 3) { ?>    
        <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/task.png', '', array('class' => 'dasimage')) . '<span class="dblink">Tasks</span>
  </div>', array('/tasks/mytask')); ?>    

    <?php } ?>
    <?php if (Yii::app()->user->role <= 2) { ?>
        <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/projects.png', '', array('class' => 'dasimage')) . '<span class="dblink">Projects</span>
  </div>', array('/projects/index')); ?>       
        <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/clients.png', '', array('class' => 'dasimage')) . '<span class="dblink">Clients</span>
  </div>', array('/clients/index')); ?> 
        <?php
    }
    ?>
    <?php if (Yii::app()->user->role < 3 || Yii::app()->user->role == 4) { ?>

        <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/task.png', '', array('class' => 'dasimage')) . '<span class="dblink">Tasks</span>
  </div>', array('/tasks/index')); ?>     

        <?php
        if (Yii::app()->user->role <= 2) {
            ?>
            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/users.png', '', array('class' => 'dasimage')) . '<span class="dblink">Users</span>
  </div>', array('/users/index')); ?>  

            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/invoice-icon.png', '', array('class' => 'dasimage')) . '<span class="dblink">Groups</span>
  </div>', array('/groups/index')); ?>     


            <?php //echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/settings.png', '', array('class' => 'dasimage')) . '<span class="dblink">Settings</span> </div>', array('/status/admin')); ?>     

            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/invoice-icon.png', '', array('class' => 'dasimage')) . '<span class="dblink">Report</span>
  </div>', array('/invoice/index')); ?>     

            <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/worktype.png', '', array('class' => 'dasimage')) . '<span class="dblink">Work Type</span>
  </div>', array('/workType/index')); ?>

            <?php
        }
    }


    // if (yii::app()->user->role == 4 OR yii::app()->user->role == 1 OR yii::app()->user->role >= 7)
    // echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/crm.png', '', array('class' => 'dasimage')) . '<br /><span class="dblink">CRM</span>  </div>', array('/lead/index'));


    if (yii::app()->user->role == 4 OR yii::app()->user->role == 1)
        echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/payment-icon.gif', '', array('class' => 'dasimage')) . '<br /><span class="dblink">Payments</span></div>', (yii::app()->user->role == 1 ? array('/payments/admin') : array('/payments/index')));

    // if (yii::app()->user->id == 28 OR yii::app()->user->role == 1)
    //echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/electricity.jpg', '', array('class' => 'dasimage')) . '<br /><span class="dblink">Electricity</span></div>', (yii::app()->user->role == 1 ? array('/payments/admin') : array('/electricity/admin')));
    ?>

    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/myprofile.png', '', array('class' => 'dasimage')) . '<div><span class="dblink">My Profile</span></div>
  </div>', array('/users/myprofile')); ?>

    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/bug-icon.png', '', array('class' => 'dasimage')) . '<span class="dblink">PMS Bugs</span></div>', array('/issues/index')); ?>

    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/logout.png', '', array('class' => 'dasimage')) . '<span class="dblink">Logout</span>
  </div>', array('site/logout')); ?>
    <br clear="all">
</div><?php */?>
<div class="today_timesheetsummary">
    <div class="week_sum_heading"> Time Sheet List</div>
    <div class="timesheethead">
            <div class="qk_desctime timesheet">Title</div>
            <div class="qk_time_desc timesheet">User</div>
            <div class="hrs_boxtsheet timesheet">Status</div>
            <br clear='all' />
        </div>
    <?php $tdate = ''; ?>
    <div class="overflow-scroll height-315">
          
        <?php
        $count = 1;
        foreach ($timesheets as $timesheet) {
            ?>
            <div class="dash_work_overviewtime" style="border-bottom: none">

                <?php
                if ($tdate != $timesheet['date_from']) {
                    $tdate = $timesheet['date_from'];
                    ?>
                    <div class="date_qk_smy">
                        <?php
                        $old_date = $timesheet['date_from'];
                        $old_date_timestamp = strtotime($old_date);
                        echo $new_date = date('d-M-Y', $old_date_timestamp);
                        ?>  to 
                        <?php
                        $old_date = $timesheet['date_to'];
                        $old_date_timestamp = strtotime($old_date);
                        echo $new_date = date('d-M-Y', $old_date_timestamp);
                        ?>
                    </div>
                <?php } ?>
                <div class="qk_desctime timesheet">
                    <a href="<?php echo Yii::app()->createUrl('timesheet/view', array('id' => $timesheet['report_id'])); ?>">
                        Time sheet <?= $count ?>
                    </a>
                </div>
                <div class="qk_time_desc timesheet">
                    <?php echo $timesheet['first_name'] . '&nbsp;' . $timesheet['last_name']; ?>
                </div>
                <div class="hrs_boxtsheet timesheet">
                    <?php
                    if ($timesheet['status'] == 0) {
                        echo 'Declined';
                    } elseif ($timesheet['status'] == 1) {
                        echo 'Approved';
                    } else {
                        echo 'Submitted';
                    }
                    ?>
                </div>
            </div>
            <br clear='all' />
            <?php
            $count++;
        }
        ?>

    </div>
</div>

<?php if (Yii::app()->user->role < 3 || Yii::app()->user->role == 4) { ?>

    <div class="today_summary border-none" style="display:none;">
        <div class="week_sum_heading"> Last 7 days work list</div>
        <?php //$this->widget('TodaySummary');    ?>

    </div>
<?php } ?>
 


