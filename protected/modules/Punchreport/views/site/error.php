<?php switch($code)
	{
		case 400:
			$this->pageTitle = 'Bad Request';
			echo ('The data you submitted is invalid');
			break;
		case 404:
			$this->pageTitle = 'Page Not Found';
			echo 'The page you requested was not found.';
			break;
		case 403:
			$this->pageTitle = 'Authorization Not Found';
			echo 'You are not authorized to perform this action.';
			break; 
		default:
			$this->pageTitle = 'Error';
			echo 'Some problem has occured. Please Contact to technical team.';
	}

?>
