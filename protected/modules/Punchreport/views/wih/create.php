<?php
/* @var $this WihController */
/* @var $model Workingholidays */

$this->breadcrumbs=array(
	'Workingholidays'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Workingholidays', 'url'=>array('index')),
	array('label'=>'Manage Workingholidays', 'url'=>array('admin')),
);
?>

<h1>Create Workingholidays</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>