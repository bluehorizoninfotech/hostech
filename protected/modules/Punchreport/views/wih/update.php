<?php
/* @var $this WihController */
/* @var $model Workingholidays */

$this->breadcrumbs=array(
	'Workingholidays'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Workingholidays', 'url'=>array('index')),
	array('label'=>'Create Workingholidays', 'url'=>array('create')),
	array('label'=>'View Workingholidays', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Workingholidays', 'url'=>array('admin')),
);
?>

<h1>Update Workingholidays <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>