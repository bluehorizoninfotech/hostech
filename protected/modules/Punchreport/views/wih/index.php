<?php
/* @var $this WihController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Workingholidays',
);

$this->menu=array(
	array('label'=>'Create Workingholidays', 'url'=>array('create')),
	array('label'=>'Manage Workingholidays', 'url'=>array('admin')),
);
?>

<h1>Workingholidays</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
