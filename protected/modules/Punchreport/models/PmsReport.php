<?php

/**
 * This is the model class for table "pms_report".
 *
 * @property integer $id
 * @property string $date
 * @property integer $accesscard_id
 * @property string $username
 * @property integer $punch_count
 * @property string $total_in
 * @property string $total_out
 * @property string $total_hours
 */
class PmsReport extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public $start_date;
    public $end_date;
    public $daterange;
    
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pms_report';
    }
   

    /**
     * @inheritdoc
     */
    
    /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
           // NOTE: you should only define rules for those attributes that
           // will receive user inputs.
           return array(
                   array('date, accesscard_id, username, punch_count, first_punch, last_punch, total_in, total_out, total_hours, flag, start_date, end_date', 'required'),
                    array('accesscard_id, punch_count, flag', 'numerical', 'integerOnly'=>true),
                    array('date', 'length', 'max'=>30),
                    array('username', 'length', 'max'=>40),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('id, date, accesscard_id, username, punch_count, first_punch, last_punch, total_in, total_out, total_hours, flag, start_date, end_date', 'safe', 'on'=>'search'),
           );
   }
   
   /**
    * @return array relational rules.
    */
   public function relations()
   {
           // NOTE: you may need to adjust the relation name and the related
           // class name for the relations automatically generated below.
           return array(
           );
   }
   
   /**
    * @return array customized attribute labels (name=>label)
    */
   public function attributeLabels()
   {
           return array(
                   'id' => 'ID',
                    'date' => 'Date',
                    'accesscard_id' => 'Accesscard ID',
                    'username' => 'Username',
                    'punch_count' => 'Punch Count',
                    'total_in' => 'Total In',
                    'total_out' => 'Total Out',
                    'total_hours' => 'Total Hours',
                    'start_date' => 'Start date',
                    'end_date' => 'End date'
           );
   }
   
   /**
    * Retrieves a list of models based on the current search/filter conditions.
    * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
    */
   public function search()
   {
           // Warning: Please modify the following code to remove attributes that
           // should not be searched.

           $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);
            $criteria->compare('date',$this->date,true);
            $criteria->compare('accesscard_id',$this->accesscard_id);
            $criteria->compare('username',$this->username,true);
            $criteria->compare('punch_count',$this->punch_count);
            $criteria->compare('first_punch',$this->first_punch,true);
            $criteria->compare('last_punch',$this->last_punch,true);
            $criteria->compare('total_in',$this->total_in,true);
            $criteria->compare('total_out',$this->total_out,true);
            $criteria->compare('total_hours',$this->total_hours,true);
            $criteria->compare('flag',$this->flag);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
   }
  


}
