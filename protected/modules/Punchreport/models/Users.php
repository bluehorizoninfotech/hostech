<?php

/**
 * This is the model class for table "tc_users".
 */
class Users extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
//for forgot password
    public $myemail_or_username;
    public $ruser_id;
    public $retype_pwd;
    public $new_pwd;
    public $old_password;
    public $rules_array = array();
    public $action_name;
    public $status = 0;
    public $group_id;
	public $department;
    public $designation;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{users}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        $this->action_name = trim(strtolower(Yii::app()->controller->action->id));

        if ($this->action_name === 'pwdrecovery') {
            $this->rules_array = array(
                array('myemail_or_username', 'required', 'message' => 'Entera your username or email id'),
                //array('myemail_or_username', 'match', 'pattern' => '/^[A-Za-z0-9@.-\s,]+$/u', 'message' => "Enter Valid username or email id"),
// password needs to be authenticated
                array('myemail_or_username', 'checkexists'),
            );
        } elseif ($this->action_name === 'pwdreset') {
            $this->rules_array = array(
                array('new_pwd, retype_pwd', 'required', 'message' => 'Enter {attribute}'),
                array('new_pwd', 'length', 'max' => 32, 'min' => 4, 'message' => "Incorrect password (minimal length 4 characters)."),
                array('retype_pwd', 'compare', 'compareAttribute' => 'new_pwd', 'message' => "Password mismatch"),
            );
        } elseif ($this->action_name === 'passchange') {
            $this->rules_array = array(
                array('old_password, password ,retype_pwd', 'required', 'message' => 'Enter {attribute}'),
                array('old_password', 'validate_oldpass'),
                array('password', 'length', 'min' => '5'),
                array('retype_pwd', 'compare', 'compareAttribute' => 'password', 'message' => "Password mismatch"),
            );
        } elseif ($this->action_name === 'editprofile') {
            $this->rules_array = array(
                array('first_name ,email', 'required', 'message' => 'Enter {attribute}'),
                array('email', 'email'),
            );
        } elseif ($this->action_name === 'update') {
            $this->rules_array = array(
                array('first_name, username, email, status, user_type', 'required', 'message' => 'Enter {attribute}'),
				array('department', 'numerical', 'integerOnly' => true),
                array('designation', 'length', 'max' => 30),
                array('password', 'length', 'min' => 5, 'max' => 32),
                array('email', 'email'),
                array('last_name,reporting_person,group_id','safe'),
            );
        } elseif ($this->action_name === 'create') {
            $this->rules_array = array(
            array('first_name, username, password, email', 'required', 'message' => 'Enter your {attribute}'),
            array('user_type', 'required', 'message' => 'Select your registration type'),
            array('user_type,department', 'numerical', 'integerOnly' => true),
            array('password', 'length', 'min' => 5, 'max' => 32),
            array('first_name, last_name,  designation', 'length', 'max' => 30),
            array('username', 'length', 'max' => 20),
            array('email', 'length', 'max' => 70),
            array('email', 'email'),
            array('reporting_person,group_id','safe')
            );
        } else {
            $this->rules_array = array(
                array('first_name, username, password, email', 'required', 'message' => 'Enter your {attribute}'),
                array('user_type', 'required', 'message' => 'Select your registration type'),
                array('user_type', 'numerical', 'integerOnly' => true),
                array('password', 'length', 'min' => 5, 'max' => 32),
                array('first_name, last_name', 'length', 'max' => 30),
                array('username', 'length', 'max' => 20),
                array('email', 'length', 'max' => 70),
                array('email', 'email'),
                array('reg_ip', 'length', 'max' => 15),
                array('activation_key', 'length', 'max' => 32),
                array('reg_date, reporting_person, userid, designation, department', 'safe'),
                // The following rule is used by search().
// Please remove those attributes that should not be searched.
                array('userid, user_type, first_name, last_name, username, password, email, last_modified, reg_date, reg_ip, activation_key, email_activation, status, department, designation,group_id', 'safe', 'on' => 'search'),
            );
        }

        return $this->rules_array;
    }

    //Custom validation functions
    public function checkexists($attribute, $params) {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            if (strpos($this->myemail_or_username, "@")) {
                $user = Users::model()->findByAttributes(array('email' => $this->myemail_or_username, 'status' => 0, 'email_activation' => 1));
                if ($user)
                    $this->ruser_id = $user->userid;
            } else {
                $user = Users::model()->findByAttributes(array('username' => $this->myemail_or_username, 'status' => 0, 'email_activation' => 1));
                if ($user)
                    $this->ruser_id = $user->userid;
            }

            if ($user === null)
                if (strpos($this->myemail_or_username, "@")) {
                    $this->addError("myemail_or_username", ("This email address was not found in our records"));
                } else {
                    $this->addError("myemail_or_username", ("This username was not found in our records"));
                }
        }
    }

    public function validate_oldpass($attribute, $params) {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            $oldpass = md5(base64_encode($this->old_password));
            //$user = Users::model()->findByAttributes(array('password' => $oldpass, 'userid' => Yii::app()->user->id, 'status' => 0, 'email_activation' => 1));
			$user = Users::model()->findByAttributes(array('password' => $oldpass, 'userid' => Yii::app()->user->id, 'status' => 0));
            if (!$user) {
                $this->addError("old_password", ("Invalid old password"));
            }
        }
    }

//End of custome validation functions
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'reportingPerson' => array(self::BELONGS_TO, 'Users', 'reporting_person'),
			'users' => array(self::HAS_MANY, 'Users', 'reporting_person'),
            'userType' => array(self::BELONGS_TO, 'UserRoles', 'user_type'),
			'Department' => array(self::BELONGS_TO, 'Department', 'department'),
                        'departmentlist' => array(self::HAS_MANY, 'Department', 'department'),
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'userid' => 'Userid',
            'user_type' => 'Profile type',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'reporting_person' => 'Reporting Person',
            'last_modified' => 'Last Modified',
            'reg_date' => 'Reg Date',
            'reg_ip' => 'Reg Ip',
            'activation_key' => 'Activation Key',
            'email_activation' => 'Email Activation',
            'status' => 'Status',
            'myemail_or_username' => 'Username or Email id',
            'retype_pwd' => 'Retype password',
            'new_pwd' => 'New password',
            'old_password' => 'Old password',
            'reg_type' => ''
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $users = Yii::app()->db->createCommand()
               ->select(['pms_groups.group_name'])
                ->from('pms_groups')
                ->leftJoin('pms_groups_members', 'pms_groups_members.group_id = pms_groups.group_id')
                //->where('pms_groups_members.group_members=:'.$id)
                ->where('pms_groups_members.group_members=:group_members', array(':group_members'=>$this->userid))
               ->queryAll();
        /*$groupname="";
        foreach($users as $user){
             $groupname[]=$user['group_name'];
        }
       $gpname="";
        if($users!=NULL){
            $gpname = implode(',',$groupname);
        }*/
        
        $criteria = new CDbCriteria;
        
        $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('reporting_person',$this->reporting_person);
        $criteria->compare('last_modified', $this->last_modified, true);
        $criteria->compare('reg_date', $this->reg_date, true);
        $criteria->compare('reg_ip', $this->reg_ip, true);
        $criteria->compare('activation_key', $this->activation_key, true);
        $criteria->compare('email_activation', $this->email_activation);
        $criteria->compare('status', $this->status);
        /*$criteria->compare('group_id', $gpname , true);*/

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25),
                                                     'sort'=>array(
    'defaultOrder'=>'status,user_type ASC',
  ),
                    'criteria' => $criteria,
                ));
    }
    
    
    public function getGroupname($id){
         $users = Yii::app()->db->createCommand()
               ->select(['pms_groups.group_name'])
                ->from('pms_groups')
                ->leftJoin('pms_groups_members', 'pms_groups_members.group_id = pms_groups.group_id')
                //->where('pms_groups_members.group_members=:'.$id)
                ->where('pms_groups_members.group_members=:group_members', array(':group_members'=>$id))
               ->queryAll();
        $groupname="";
		$gplead = Groups::model()->findAll(array(
                    'select' => 'group_name',
                    'condition' => 'group_lead='.$id,
        ));
        if(isset($gplead) && ($gplead!=NULL)){
            foreach($gplead as $lead){
             $groupname[]=$lead['group_name']." ( Group Lead)";
            }
        }
        
        foreach($users as $user){
             $groupname[]=$user['group_name'];
        }
       $gpname="";
        if($groupname!=NULL){
            $gpname = implode(', ',$groupname);
        }
        return $gpname;
    }
    
public function memberlistsearch() {
          
          
        $gpid = $_SESSION["gpid"];
        $gpmemebr = array();
        $gplead = Groups::model()->find(array(
                    'select' => 'group_lead',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpld=$gplead['group_lead'];
		
        $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpmemebr[]     =   $gpld;
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }          
          
        
		
		$criteria = new CDbCriteria;
        
        $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('designation', $this->designation, true);
        $criteria->compare('department', $this->department, true);
        $criteria->addCondition('status = 0');
        $criteria->addCondition('user_type > 1');
        $criteria->addNotInCondition('userid',$gpmemebr);

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 10),
                                                     'sort'=>array(
    'defaultOrder'=>'user_type ASC',
  ),
                    'criteria' => $criteria,
                ));
    }
	
	public function grp_memberlistsearch() {
          
          
        $gpid = $_SESSION["gpid"];
        $gpmemebr = array();
        $gplead = Groups::model()->find(array(
                    'select' => 'group_lead',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpld=$gplead['group_lead'];
        $groupsmemeberlist = Groups_members::model()->findAll(
                array(
                    'select' => 'group_id,group_members,id',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpmemebr[]     =   $gpld;
        foreach ($groupsmemeberlist as $groupsmemeber) {
            $gpmemebr[] = $groupsmemeber['group_members'];
        }  
          
          
        $criteria = new CDbCriteria;
        
        $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('designation', $this->designation, true);
        $criteria->compare('department', $this->department, true);
        $criteria->addCondition('status = 0');
        $criteria->addCondition('user_type > 1');
        $criteria->addInCondition('userid',$gpmemebr);

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 10),
                                                     'sort'=>array(
    'defaultOrder'=>'user_type ASC',
  ),
                    'criteria' => $criteria,
                ));
    }
    
    public function getDepartment($userid){
        $users = Yii::app()->db->createCommand()
               ->select(['pms_user_department.department'])
                ->from('pms_users')
                ->leftJoin('pms_user_department', 'pms_user_department.id = pms_users.department')
                //->where('pms_groups_members.group_members=:'.$id)
                ->where('pms_users.userid=:userid', array(':userid'=>$userid))
               ->queryRow();
       
        if(isset($users) && ($users !=NULL)){
           return $users['department'];
        }
    }
    
    
    public function getGrouplead($userid){
        $gpid=  $_SESSION["gpid"];
        $gplead = Groups::model()->find(array(
                    'select' => 'group_lead',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpld=$gplead['group_lead'];
       if($userid==$gpld){
           return "Group Lead";
       }else{
           return "";
       }
    }
    
    
    public function getDeleteaction($userid){
        $gpid=  $_SESSION["gpid"];
        $gplead = Groups::model()->find(array(
                    'select' => 'group_lead',
                    'condition' => 'group_id=' . $gpid,
        ));
        $gpld=$gplead['group_lead'];
       if($userid==$gpld){
           return 0;
       }else{
           return 1;
       }
    }
    
}