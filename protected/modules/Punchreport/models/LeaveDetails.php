<?php

/**
 * This is the model class for table "leave_details".
 *
 * The followings are the available columns in table 'leave_details':
 * @property integer $id
 * @property integer $emp_id
 * @property string $date_from
 * @property string $date_to
 * @property string $num_days
 * @property string $leave_type
 * @property string $notes
 */
class LeaveDetails extends CActiveRecord
{
    
    public $year;
    public $month;
    public $quarter;
    public $fullName;
    public $total;   
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LeaveDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'leave_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, date_from, date_to, num_days, leave_type, notes', 'required'),
			array('emp_id', 'numerical', 'integerOnly'=>true),
			array('num_days', 'length', 'max'=>10),
			array('leave_type', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, emp_id,  date_from, date_to, num_days, leave_type, notes, fullName,year,month,quarter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'users' => array(self::HAS_MANY, 'Users', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
                        'emp_id' => 'Emp',
			'date_from' => 'Date From',
			'date_to' => 'Date To',
			'num_days' => 'Num Days',
			'leave_type' => 'Leave Type',
			'notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('date_from',$this->date_from,true);
		$criteria->compare('date_to',$this->date_to,true);
		$criteria->compare('num_days',$this->num_days,true);
		$criteria->compare('leave_type',$this->leave_type,true);
		$criteria->compare('notes',$this->notes,true);
                //$criteria->addSearchCondition('concat(first_name, " ", last_name)', $this->fullName); 

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination' => array(
                            'pageSize' => 20,
                         ),
		));
	}
        
//        public function getFullName()
//        {
//            return $this->first_name . ' ' . $this->last_name;
//        }
}