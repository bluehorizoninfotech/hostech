<?php

/**
 * This is the model class for table "{{dummylogs}}".
 *
 * The followings are the available columns in table '{{dummylogs}}':
 * @property integer $id
 * @property integer $dummycard_id
 * @property string $date
 * @property integer $usercard_id
 */
class Dummylogs extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Dummylogs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dummylogs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dummycard_id, date, usercard_id', 'required'),
			array('dummycard_id, usercard_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dummycard_id, date, usercard_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dummycard_id' => 'Dummycard',
			'date' => 'Date',
			'usercard_id' => 'Usercard',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dummycard_id',$this->dummycard_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('usercard_id',$this->usercard_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}