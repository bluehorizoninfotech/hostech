<?php

/**
 * This is the model class for table "user_details".
 *
 * The followings are the available columns in table 'user_details':
 * @property integer $id
 * @property integer $userid
 * @property integer $accesscard_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $jobtitle
 * @property string $department
 * @property string $dateofbirth
 * @property string $father_name
 * @property string $address
 * @property integer $marital_status
 * @property string $spouse_name
 * @property string $blood_group
 * @property string $acess_card_number
 * @property string $qualification
 * @property string $phone_number
 * @property string $mobile_number
 * @property string $secondary_email
 * @property string $description
 * @property string $dateofjoin
 * @property string $datereleave
 * @property string $offerletterid
 * @property string $salary
 * @property string $bank_no
 * @property string $bank_details
 * @property string $working_status
 * @property string $report_to
 * @property string $employee_id
 * @property string $image
 * @property integer $status
 */
class Employees extends CActiveRecord
{
    
    public $fullName;
    public $servicePeriod;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Employees the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('accesscard_id, first_name, last_name, jobtitle, department, dateofbirth, father_name, address, marital_status, blood_group, qualification,  mobile_number, secondary_email, dateofjoin, report_to', 'required'),
			array('id, accesscard_id, marital_status, status', 'numerical', 'integerOnly'=>true),
			array('first_name, middle_name, last_name, jobtitle, department, father_name, qualification, secondary_email, offerletterid, report_to', 'length', 'max'=>100),
			array('spouse_name, acess_card_number, bank_no', 'length', 'max'=>250),
			array('blood_group, salary', 'length', 'max'=>50),
			array('phone_number, mobile_number, employee_id', 'length', 'max'=>30),
			array('bank_details, image', 'length', 'max'=>500),
			array('working_status', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('accesscard_id, first_name, middle_name, last_name, jobtitle, department, dateofbirth,  address, marital_status, spouse_name, blood_group, acess_card_number, qualification, phone_number, mobile_number, secondary_email, dateofjoin, datereleave,  working_status, report_to, status, fullName, servicePeriod', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userid' => 'Userid',
			'accesscard_id' => 'Accesscard',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'jobtitle' => 'Jobtitle',
			'department' => 'Department',
			'dateofbirth' => 'Date of Birth',
			'father_name' => 'Father Name',
			'address' => 'Address',
			'marital_status' => 'Marital Status',
			'spouse_name' => 'Spouse Name',
			'blood_group' => 'Blood Group',
			'acess_card_number' => 'Acess Card Number',
			'qualification' => 'Qualification',
			'phone_number' => 'Land line Number',
			'mobile_number' => 'Mobile Number',
			'secondary_email' => 'Secondary Email',
			'description' => 'Description',
			'dateofjoin' => 'Dateofjoin',
			'datereleave' => 'Datereleave',
			'offerletterid' => 'Offer Letter id',
			'salary' => 'Salary',
			'bank_no' => 'Bank No',
			'bank_details' => 'Bank Details',
			'working_status' => 'Working Status',
			'report_to' => 'Report To',
			'employee_id' => 'Employee ID',
			'image' => 'Image',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('accesscard_id',$this->accesscard_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('jobtitle',$this->jobtitle,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('dateofbirth',$this->dateofbirth,true);
		$criteria->compare('father_name',$this->father_name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('marital_status',$this->marital_status);
		$criteria->compare('spouse_name',$this->spouse_name,true);
		$criteria->compare('blood_group',$this->blood_group,true);
		$criteria->compare('acess_card_number',$this->acess_card_number,true);
		$criteria->compare('qualification',$this->qualification,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('mobile_number',$this->mobile_number,true);
		$criteria->compare('secondary_email',$this->secondary_email,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('dateofjoin',$this->dateofjoin,true);
		$criteria->compare('datereleave',$this->datereleave,true);
		$criteria->compare('offerletterid',$this->offerletterid,true);
		$criteria->compare('salary',$this->salary,true);
		$criteria->compare('bank_no',$this->bank_no,true);
		$criteria->compare('bank_details',$this->bank_details,true);
		$criteria->compare('working_status',$this->working_status,true);
		$criteria->compare('report_to',$this->report_to,true);
		$criteria->compare('employee_id',$this->employee_id,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('status',$this->status);
                $criteria->addSearchCondition('concat(first_name, " ", last_name)', $this->fullName); 

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination' => array(
                            'pageSize' => 20,
                         ),
		));
	}
        
        public function getFullName()
        {
            return $this->first_name . ' ' . $this->last_name;
        }
        
        public function getServicePeriod(){
            $doj = $this->dateofjoin;
            $datetime1 = new DateTime($doj);
            $datetime2 = new DateTime(date('Y-m-d'));
            $interval = $datetime1->diff($datetime2);
            $str1 = $interval->format('%y years, %m months, and %d days');
            $dojj = date_create($doj);
            $doj2 = date_format($dojj,"d-m-Y");
            $str2 = "(\r\nDOJ:".$doj2.")";
            return $str1."".$str2;
        }
        
        public function getStatus(){
            if($this->status == 1){
                return "Active";
            }
            else{
                return "Inactive";
            }
        }
}