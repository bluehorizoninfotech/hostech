<?php


/**
 * This is the model class for table "pms_users".
 *
 * @property integer $userid
 * @property integer $user_type
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $reporting_person
 * @property string $last_modified
 * @property string $reg_date
 * @property string $reg_ip
 * @property string $activation_key
 * @property boolean $email_activation
 * @property integer $status
 * @property integer $accesscard_id
 *
 * @property PmsUserRoles $userType
 * @property PmsUsers $reportingPerson
 * @property PmsUsers[] $pmsUsers
 */
class PmsUsers extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pms_users';
    }
    
    
   
  
    /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
           // NOTE: you should only define rules for those attributes that
           // will receive user inputs.
           return array(
                    array('user_type, first_name, username, password, email, last_modified, reg_ip', 'required'),
                    array('user_type, reporting_person, email_activation, status, accesscard_id', 'numerical', 'integerOnly'=>true),
                    array('first_name, last_name', 'length', 'max'=>30),
                    array('username', 'length', 'max'=>20),
                    array('password, activation_key', 'length', 'max'=>32),
                    array('email', 'length', 'max'=>70),
                    array('reg_ip', 'length', 'max'=>150),
                    array('reg_date', 'safe'),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('userid, user_type, first_name, last_name, username, password, email, reporting_person, last_modified, reg_date, reg_ip, activation_key, email_activation, status, accesscard_id', 'safe', 'on'=>'search'),
                   // array('username,email','unique', 'targetAttribute' => array('username, email'), 'message' => 'The combination of Username and Email has already been taken.')
           );
   }
   
   
     /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'reportingPerson' => array(self::BELONGS_TO, 'PmsUsers', 'reporting_person'),
            'users' => array(self::HAS_MANY, 'PmsUsers', 'reporting_person'),
            'userType' => array(self::BELONGS_TO, 'UserRoles', 'user_type'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'userid' => 'Userid',
            'user_type' => 'User Type',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'reporting_person' => 'Reporting Person',
            'last_modified' => 'Last Modified',
            'reg_date' => 'Reg Date',
            'reg_ip' => 'Reg Ip',
            'activation_key' => 'Activation Key',
            'email_activation' => 'Email Activation',
            'status' => 'Status',
            'accesscard_id' => 'Accesscard',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('userid',$this->userid);
        $criteria->compare('user_type',$this->user_type);
        $criteria->compare('first_name',$this->first_name,true);
        $criteria->compare('last_name',$this->last_name,true);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('reporting_person',$this->reporting_person);
        $criteria->compare('last_modified',$this->last_modified,true);
        $criteria->compare('reg_date',$this->reg_date,true);
        $criteria->compare('reg_ip',$this->reg_ip,true);
        $criteria->compare('activation_key',$this->activation_key,true);
        $criteria->compare('email_activation',$this->email_activation);
        $criteria->compare('status',$this->status);
        $criteria->compare('accesscard_id',$this->accesscard_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
   

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getUserType()
//    {
//        return $this->hasOne(PmsUserRoles::className(), ['id' => 'user_type']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getReportingPerson()
//    {
//        return $this->hasOne(PmsUsers::className(), ['userid' => 'reporting_person']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getPmsUsers()
//    {
//        return $this->hasMany(PmsUsers::className(), ['reporting_person' => 'userid']);
//    }
    
 
    

}
