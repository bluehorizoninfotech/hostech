<?php

/**
 * Site controller
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    
    public $layout = '//layouts/column4';
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index','regenerate'),
                'users' => array('?'),
                //'expression' => 'yii::app()->user->user_type_id ==6' 
            ),

            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    public function actionIndex(){
        
        
        
        $punchreport = new PmsReport();
        
        date_default_timezone_set('Asia/Kolkata');
        $today = date("Y-m-d");
        
        /*$employees = PmsUsers::model()->findAll(
                array(
                      'select'=>'*',
                      'condition'=>'accesscard_id!=0 AND user_type!= 1 AND status = 0',
                      'order'=>'userid'
                )
        );
        */
        
        
        
      

        //$timelog = Yii::$app->db->createCommand("
              //  SELECT * FROM pms_report ")->queryAll();
        
        
        $holidays = Yii::app()->db->createCommand(
                'SELECT * FROM pms_holidays')->queryAll();
        
        $wihs = Yii::app()->db->createCommand(
                'SELECT * FROM pms_workingholidays')->queryAll();
        
        $leaves = Yii::app()->db->createCommand(
                'SELECT * FROM pms_leaves')->queryAll();
        
        
        $shifts = Yii::app()->db->createCommand(
                'SELECT * FROM pms_shift_timings')->queryAll();
        
         
        
        if(isset($_POST['daterange'])){
            //die("1");
            $postdate = $_POST['daterange'];
            $string = explode('-', $postdate);
            
            $date1 = explode('/', $string[0]);
            $date2 = explode('/', $string[1]);
           
            $start_date = date('Y-m-d', strtotime($string[0]));
           
            $end_date = date('Y-m-d', strtotime($string[1]));   
            
         }
         else{

            $start_date = date('Y-m-01',strtotime('this month'));
            $end_date =  date('Y-m-t',strtotime('this month'));
            //$start_date = '2016-03-01'; 
            //$end_date = '2016-03-10';
             
         }

        
        $range = "date >=$start_date AND date<=$end_date";
        
//        $employees = Yii::app()->db->createCommand()->queryAll("SELECT userid,first_name FROM pms_users WHERE user_type != 1 AND accesscard_id IN (SELECT DISTINCT empid FROM `pms_punch_log` where log_time BETWEEN '$start_date' AND '$end_date' GROUP BY empid)");
//        echo "SELECT userid,first_name FROM pms_users WHERE user_type != 1 AND accesscard_id IN (SELECT DISTINCT empid FROM `pms_punch_log` where log_time BETWEEN '$start_date' AND '$end_date' GROUP BY empid)";
        
        
        $employees = PmsUsers::model()->findAll(
                array(
                      'select'=>'*',
                      'condition'=>'accesscard_id!=0 AND user_type!= 1 AND status = 0',
                      //  'condition' => "user_type!= 1 AND accesscard_id IN (SELECT DISTINCT empid FROM `pms_punch_log` where log_time >= '$start_date' AND log_time <= '$end_date' GROUP BY empid)",
                      'order'=>'userid'
                )
        );
//        echo $start_date;
//        echo $end_date;
//       
//       print_r($employees); die();
        
        if(isset($_POST['action']) && $_POST['action'] == 'Generate' ){

            
            
            
            
            $model = new Accesslog();
            if (isset($_GET['Accesslog'])) {
                $model->attributes = $_GET['Accesslog'];
            }
            /*$ch = curl_init();
            curl_setopt_array(
                $ch, array(
                CURLOPT_URL => 'http://192.168.1.100/getlog.php',
                CURLOPT_RETURNTRANSFER => true
            ));
            $output = curl_exec($ch);
 
            */
            if(strtotime($start_date) <= strtotime($today) || strtotime($end_date) <= strtotime($today) ){
                foreach($employees as $emp ){
                    $empid = $emp['accesscard_id'];
                    $date = $start_date; 
                    $days = 0;   

                    for ($i = 0; $i <= (strtotime($date) <= strtotime($end_date)); $i++) {
                        $sel = Yii::app()->db->createCommand("SELECT * FROM pms_report WHERE date='$date' AND accesscard_id=$empid ")->queryScalar();
                        if($sel == 0){
                        
                        
                        
                        $seldate = strtotime($date);
                        $todaydate = strtotime(date("Y-m-d"));
                        if ($seldate < $todaydate) {
                            $days = ($todaydate-$seldate)/(-3600*24);
                        }

                        $precent_date = $date;
                        $datewithtime = date("Y-m-d H:i:s", strtotime($date));

                        /*******************************Fetch all logs with punch count, first punch, last punch*************************/

                        $sql = "SELECT '$datewithtime' dbtime, '$date' cdate,  concat(first_name,' ', last_name) as username, accesscard_id,"
                                . "(select count(distinct log_time) from pms_punch_log as pa where log_time like '$date%' and empid='$empid') as count_punch,"
                                . "(select log_time from pms_punch_log as pa where log_time  like '$date%' and empid='$empid' order by log_time asc limit 1) as ipunch,"
                                . "(select log_time  from pms_punch_log as pa where log_time like '$date%' and empid='$empid' order by log_time desc limit 1) as lpunch"
                                . " FROM pms_users where accesscard_id = '$empid'";

                            $command = Yii::app()->db->createCommand($sql);
                            $logemp = $command->queryAll();

                            /*echo "<pre>";
                            print_r($logemp);
                            echo "</pre>"; */




                            /************************************Fetch log time **********************************************************/
                            $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate('$date', INTERVAL 0 day) "
                                    . "and log_time<=adddate('$date', INTERVAL  1  day) and empid='$empid' "
                                    . "group by empid,log_time order by log_time asc";

                            $command2 = Yii::app()->db->createCommand($sql2);
                            $logemp2 = $command2->queryAll();

                          /*  echo "<pre>";
                            print_r($logemp2);
                            echo "</pre>"; */

                            /**************************************Calculate IN OUT ************************************************/
                            $userlog = array();
                            $userid = $empid;
                            $flag = 0;
                            $userlog[$userid] = array('IN' => 0, 'OUT' => 0);
                            $prev_time = 0;
                            $outtime = array();
                            foreach ($logemp2 as $key=> $rec) { 
                                if($key == 0){
                                    $flag = 0;
                                    $prev_time = strtotime($rec['log_time']);  
                                    $flag++;
                                } 
                                else{ 
                                    $timediffin = strtotime($rec['log_time']) - $prev_time;
                                    $prev_time = strtotime($rec['log_time']); 
                                    if ($flag % 2 == 1) {
                                        $userlog[$userid]['IN'] += $timediffin;
                                    } else { 
                                        $userlog[$userid]['OUT'] += $timediffin;
                                        $empid1 = $userid;
                                        $outtime[$userid] = $this->calculate_time_span($userlog[$userid]['OUT']); 
                                    }
                                   $flag++;
                                }   
                            }

                            /*************************************************Create result array******************************************/
                            $res_array = array();
                            foreach($logemp as $item){
                                $cardid = $item['accesscard_id'];
                                $dbtime = $item['dbtime'];

                                if ($days < 0) {
                                    $dbtime = $item['lpunch'];
                                } else if ($days == 0 && strtotime($item['dbtime']) > strtotime($item['cdate'] . ' 19:30:00')) {
                                    //$dbtime = $item['cdate'] . ' 18:30:00';
                                    $dbtime = $item['lpunch'];
                                } else {
                                  // die();
                                }

                                $res_array[$cardid]= array();
                                $totl_intime = (isset($userlog[$cardid]) ? $userlog[$cardid]['IN'] + ($item['count_punch'] % 2 == 1 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');
                                $total_outtime = (isset($userlog[$cardid]) ? $userlog[$cardid]['OUT'] + ($item['count_punch'] % 2 == 0 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');

                                if ($item['ipunch'] == '' && $item['lpunch'] == '') {
                                    $punch_status="No Punch";
                                }
                                else{
                                    $punch_status=($item['count_punch'] % 2 == 1 ? 'Punched IN' : 'Punched OUT'); 

                                    $inpunch = date('h:i:s A', strtotime($item['ipunch']));
                                    $lstpunch = date('h:i:s A', strtotime($item['lpunch']));
                                    $tot_out = $this->calculate_time_span($total_outtime);

                                    $tot_in = $this->calculate_time_span($totl_intime);
                                    $tot_time =($this->calculate_time_span($totl_intime + $total_outtime));

                                }

                                $res_array[$cardid]['cardid']=$cardid;
                                $res_array[$cardid]['username']=$item['username'];
                                $res_array[$cardid]['punch_status']=$punch_status;
                                $res_array[$cardid]['precent_date'] = $date;
                                $res_array[$cardid]['punch_count'] = $item['count_punch'];

                                if($punch_status!="No Punch"){
                                    $res_array[$cardid]['intime']=$inpunch;
                                    $res_array[$cardid]['lsttime']=$lstpunch;
                                    $res_array[$cardid]['tout']=$tot_out;
                                    $res_array[$cardid]['tot_in']=$tot_in;
                                    $res_array[$cardid]['tot_time']=$tot_time;
                                }
                                else{
                                    $res_array[$cardid]['intime']=0;
                                    $res_array[$cardid]['lsttime']=0;
                                    $res_array[$cardid]['tout']=0;
                                    $res_array[$cardid]['tot_in']=0;
                                    $res_array[$cardid]['tot_time']=0;
                                }

                            }

                            /*echo "<pre>";
                            print_r($res_array);
                            echo "</pre>"; */

                            /******************************************Insert to db ***********************************************************/
                            foreach($res_array as $id=>$res){
                                $cardid = $id;
                                $username = $res['username']; 
                                $date = $precent_date;
                                $punch_count = $res['punch_count'];
                                $first_punch = $res['intime'];
                                $last_punch = $res['lsttime'];
                                $total_in = $res['tot_in'];
                                $total_out = $res['tout'];
                                $total_hours = $res['tot_time']; 

    //                            $query = new Query;
    //                            $query->select('COUNT(*)')
    //                                    ->from('pms_report')
    //                                    ->where('date=:date and accesscard_id=:accesscard_id', array(':date'=>$date,':accesscard_id'=>$cardid));
    //                            $command = $query->createCommand();
    //                            $sel = $command->queryScalar();


                                $sel = Yii::app()->db->createCommand("SELECT * FROM pms_report WHERE date='$date' AND accesscard_id=$cardid ")->queryScalar();


                                $arrdata = array(
                                               'date'=>$date,
                                               'accesscard_id'=>$cardid,
                                               'username' => $username,
                                               'punch_count'=>$punch_count,
                                               'first_punch' => $first_punch,
                                               'last_punch' => $last_punch,
                                               'total_in' => $total_in,
                                               'total_out' => $total_out,
                                               'total_hours' => $total_hours
                                           );  
                                if($sel == 0){

                                    $ins = Yii::app()->db->createCommand()->insert('pms_report', $arrdata);

                                }
//                                else{
//                                    $upd = Yii::app()->db->createCommand()->update('pms_report', $arrdata,'date=:date and accesscard_id=:accesscard_id',array(':date'=>$date,':accesscard_id'=>$cardid) ); 
//
//
//                                }
                            }
                        }
                        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));   
                    }


                }
            }
            else{
            
         } //end of if condition for post date check   
         
        
   
        }
      
        return $this->render('report',[
                        'employees' => $employees, 'holidays' => $holidays, 'wihs'=> $wihs,'leaves' => $leaves,'shifts' => $shifts,'date1'=> $start_date, 'date2'=> $end_date, 'model' => $punchreport
            ]);
        
    }
    
    public function calculate_time_span($seconds) {
        $months = floor($seconds / (3600 * 24 * 30));
        $day = floor($seconds / (3600 * 24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours * 3600)) / 60);
        $secs = floor($seconds % 60);

        return $hours . ":" . $mins . ":" . $secs;

        $ret = array('h' => 0, 'm' => 0, 's' => 0);


        if ($hours >= 1) {
            $ret['h'] = $hours;
        }

        if ($seconds < 60)
            $time = $secs . " seconds ago";
        else if ($seconds < 60 * 60)
            $time = $mins . " min ago";
        else if ($seconds < 24 * 60 * 60)
            $time = $hours . " hours ago";
        else if ($seconds < 24 * 60 * 60)
            $time = $day . " day ago";
        else
            $time = $months . " month ago";

        return $time;
    }
    
    public function actionRegenerate(){
        if (!empty($_POST['daterange'])) {
            $postdate = $_POST['daterange'];
            $string = explode('-', $postdate);
            $date1 = explode('/', $string[0]);
            $date2 = explode('/', $string[1]);

            $finalDate1 = date('Y-m-d', strtotime($string[0]));
            $finalDate2 = date('Y-m-d', strtotime($string[1]));
            
            
            
            
            
            
            
            
            
            
            
            
            
           
        }
        else{
            
        }
        
        
    }

    

    

   
    
    
}
