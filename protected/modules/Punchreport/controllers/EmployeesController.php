<?php


/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{
    
    /**
     * @inheritdoc
     */
    
    public $layout = '//layouts/column4';
    
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index','admin', 'create', 'view','update','delete','uploaddocs','downloaddoc','addnotes','uploaduserimage','deletenote','editnote', 'deletedoc'),
                'users' => array('@'),
               // 'expression' => 'Yii::app()->user->user_type_id == 6 || Yii::app()->user->user_type_id == 2' 
            ),

            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    

    /**
     * Lists all Employees models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $dataProvider=new CActiveDataProvider('Employees');
        $this->render('index',array(
                'dataProvider'=>$dataProvider,
        ));

    }
    
    /**
    * Manages all models.
    */
   public function actionAdmin()
   {
           $model=new Employees('search');
           $model->unsetAttributes();  // clear any default values
           if(isset($_GET['Employees']))
                   $model->attributes=$_GET['Employees'];

           $this->render('admin',array(
                   'model'=>$model,
           ));
   }
   
   
   /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
   public function actionCreate()
   {

            $model=new Employees;
            $userlogin = new UserLogin;

            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);
            
            $this->performAjaxValidation($userlogin);
            
            if(isset($_POST['Employees']) && isset($_POST['UserLogin']))
            // if ($model->load(Yii::app()->request->post()) && $userlogin->load(Yii::app()->request->post()) ) 
            {
                
                   
                    $model->attributes=$_POST['Employees'];
                    $userlogin->attributes = $_POST['UserLogin'];
                    if($model->save()){
                        
                        $userlogin->id = $model->userid; 
                        $userlogin->accesscard_id = $model->accesscard_id;
                        $userlogin->role_id = 2;
                        $userlogin->user_name = $model->first_name; 
                        //$userlogin->user_password = Yii::$app->security->generatePasswordHash($userlogin->user_password);
                        $userlogin->user_password = md5($userlogin->user_password);
                        //$userlogin->user_password = $userlogin->setPassword(user_password);
                        $userlogin->save(FALSE); 
                            $this->redirect(array('view','id'=>$model->userid));
                    }
                    
                    
            }
            else {
              $this->render('create',array(
                   'model'=>$model,
                    'userlogin' => $userlogin
                )); 
               
            }
           
           

           
   }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        $notesmodel = new UserNotes;
        $notmodel = UserNotes::model()->findAll(array('condition' => "user_id = $id", 'order' => 'id DESC' ) );
        
        
        if($userlogin = UserLogin::model()->find(array('condition' => "id = $id"))) {
            $userlogin = UserLogin::model()->find(array('condition' => "id = $id"));
        }
        else{
            $userlogin = new UserLogin;
        }
        
        $docs = Yii::app()->db->createCommand("SELECT d.id, d.document FROM documents d JOIN user_login u ON d.user_id = u.user_email WHERE u.id = $id")->queryAll();

      
        $this->render('view', array(
            'model'=>$this->loadModel($id),
            'userlogin' => $userlogin,
            'docs' => $docs,
            'notes' => $notmodel,
            'notesmodel' => $notesmodel,
            
        ));
    }

   

    /**
     * Updates an existing Employees model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        $model = $this->loadModel($id);
        if($userlogin = UserLogin::model()->find(array('condition' => "id = $id"))) {
            $userlogin = UserLogin::model()->find(array('condition' => "id = $id"));
        }
        else{
            $userlogin = new UserLogin;
        }
        
       
        if (!isset($model)) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        
        if(isset($_POST['Employees']) && isset($_POST['UserLogin'])){
            
            //$image = UploadedFile::getInstance($model, 'image');
            //$model->image = $image;
            $model->attributes=$_POST['Employees'];
            $userlogin->attributes = $_POST['UserLogin'];
            
            if($model->save(FALSE)) {
           
            }
            $userlogin->id = $model->userid; 
            $userlogin->accesscard_id = $model->accesscard_id;
            $userlogin->role_id = 2;
            $userlogin->user_name = $model->first_name; 
            //$userlogin->user_password = Yii::$app->security->generatePasswordHash($userlogin->user_password);
            $userlogin->user_password = md5($userlogin->user_password);
            //$userlogin->user_password = $userlogin->setPassword(user_password);
            $userlogin->save(false); 
            
            $this->redirect(array('view','id'=>$id));
            
         }
       /* if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->userid]);
        }*/ else {
            $this->render('update', array(
                'model' => $model,
               'userlogin' => $userlogin,
            ));
        }
    }

    /**
     * Deletes an existing Employees model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        $this->redirect(array('index'));
    }

    
    
    
    public function actionUploaddocs(){
        $id = $_POST['id'];
        $docmodel = Documents::model()->find(array('select' => 'document', 'condition' => "user_id = '$id'")); 
        $k = 0;
        $upload_data = "";
        $upload_data = $docmodel['document'];
        if ($upload_data) {
            $docdatas = json_decode($upload_data, true);
            $k = count($docdatas);
        }
        $upload_dir =  Yii::getPathOfAlias('webroot').'/uploads/documents/';
        if (isset($_FILES["docs"])) {
           $ret = array();
           $name = stripslashes($_FILES['docs']['name']);
           $name = str_replace(' ', '_', $name);
           $name = urlencode($name);
           $name = str_replace('%', '', $name);
           $name = date('Ymdhis') . "_" . $name;

           if (!empty($name)) {
               $ext = pathinfo($name, PATHINFO_EXTENSION);
               $extarray = array('jpeg', 'jpg', 'png', 'pdf', 'doc', 'docx');
               $filename = $name;
               $basename = substr($filename, 0, strrpos($filename, "."));
               if (in_array($ext, $extarray)) {
                   if (!is_array($_FILES["docs"]["name"])) { //single file
                       @move_uploaded_file($_FILES["docs"]["tmp_name"], $upload_dir .'/'. $name);
                       @chmod($upload_dir . $name, 0777);
                       $ret[] = $name;
                       $filenam = array();
                       for ($i = 1; $i <= $k; $i++) {
                           if (isset($docdatas[$i])) {
                               $filenam[] = $docdatas[$i]['name'];
                           }
                       }
                       if (!in_array($name, $filenam)) {
                           @move_uploaded_file($_FILES["docs"]["tmp_name"][$key], $upload_dir .'/'. $filename);
                           $k++;
                           $docdatas[$k]['name'] = $name;
                           $docdatas[$k]['type'] = $_FILES["docs"]["type"];
                       } else {
                           $ret['jquery-upload-file-error'] = "File already exists";
                           echo false;
                       }
                   }
               }
               if (count($docdatas) > 0) {
                   $docvals = json_encode($docdatas);
                   $command = Yii::app()->db->createCommand('INSERT INTO documents (user_id, document) values ("'.$id.'","' . $docdatas[$k]['name'] . '") ');
                   $adinsert = $command->execute();
                   echo $name;
               }
           }
       }
    }
    public function actionDownloaddoc($doc){
        $doc = $_GET['doc'];
        $path = Yii::getPathOfAlias('webroot') . '/uploads/documents/';
        $file = $path . '' .$doc;
        if (file_exists($file)) {
           Yii::app()->getRequest()->sendFile($doc,  file_get_contents( $file ));
        }
    }
    
      public function actionAddnotes($id) {
        
        $model = new UserNotes;
        
        if(isset($_POST)){
            $model->user_id = $id;
            $uid = Yii::app()->user->id;
            $emp = Users::model()->find(array('select' => 'username', 'condition' => "userid = $uid"));
            $model->username = $emp->username;
            
            $model->date = date('Y-m-d H:i:s');
            $model->note = $_POST['UserNotes']['note'];
            if($model->save()){
                $this->redirect(array('view','id'=>$id));
            }
            else{
                print_r($model->getErrors());
            }
        }
        
    }
    
    
    
    public function actionUploaduserimage() {
        
        
        $upload_dir = Yii::getPathOfAlias('webroot'). '/uploads/profile';
        $output_dir = Yii::getPathOfAlias('webroot'). '/uploads/profile/';
        $id = $_POST['id'];
        if (isset($_FILES["file"])) {
            $name = $_FILES['file']['name'];
            $name = str_replace(' ', '_', $name);
            
            $opermodel = Employees::model()->find(array('condition' => "userid = $id " ));
            if (count($opermodel) > 0) {
                $exstingimage = $opermodel->image;
                if ($exstingimage != '') {
                    $exstingimage = $opermodel->image;
                    if (file_exists($upload_dir .'/'. $exstingimage)) {
                        @unlink($upload_dir .'/'. $exstingimage);
                    }
                    if (file_exists($output_dir .'/'. $exstingimage)) {
                        @unlink($output_dir .'/'. $exstingimage);
                    }
                }
            }
            
            $size = $_FILES['file']['size'];


            if (!empty($name)) {
                $ext = pathinfo($name, PATHINFO_EXTENSION);
                $extarray = array('jpeg', 'jpg', 'png', 'JPG', 'JPEG', 'PNG');
                $filename = $name;
                $basename = substr($filename, 0, strrpos($filename, "."));
                
                if (in_array($ext, $extarray)) { 
                    if (!is_array($_FILES["file"]["name"])) { //single file
                        move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir .'/'. $name);
                        
                        //$uploadedimage=$upload_dir . $name;
                        $imagename = $output_dir . $filename;
                        //Image::thumbnail($upload_dir .'/'. $name, 150, 150)->save(Yii::getPathOfAlias($imagename), ['quality' => 80]);
                        

                       $adinsert = Yii::app()->db->createCommand("UPDATE user_details SET image = '$name' WHERE userid = $id")->execute();
                        if ($adinsert) {
                            echo true;
                        }
                    }
                }
            }
        }
    }
    
    public function actionDeletenote() {

        $id = $_REQUEST['id'];
        if(!empty($id)){
            $command = Yii::app()->db->createCommand('DELETE from user_notes WHERE id=' . $id);                    
            $del = $command->execute();
            if ($del) {
                echo $id;
            }
            else{
                echo "error";
            }
            
        }
        
    }
    
    public function actionEditnote() {
        $uid = Yii::app()->user->id;
        $emp = Users::model()->find(array('select'=>'username', 'condition' => "userid = '$uid'" ));
        $username = $emp->username;
        $note = $_REQUEST['note'];
        $id = $_REQUEST['id'];
        $adinsert = Yii::app()->db->createCommand("UPDATE user_notes SET note = '$note', username = '$username' WHERE id = $id")->execute();
        if ($adinsert) {
            echo true;
        }
    }
    
     public function actionDeletedoc() {
        $upload_dir = Yii::getPathOfAlias('webroot') . '/uploads/documents/';
        $id = $_REQUEST['id'];
        if(!empty($id)){
            $doc = Documents::model()->find(array('select' => 'document', 'condition' => "id = $id" ));
            $file = $doc->document;
            
            if ($file != '') {
                if (file_exists($upload_dir . $file)) {
                    @unlink($upload_dir . $file);
                    $command = Yii::app()->db->createCommand('DELETE from documents WHERE id=' . $id);                    
                    $del = $command->execute();
                    if ($del) {

                        echo $id;
                    }
                }
            }

        }
        else{
            echo "error";
        }
        
    }
    
    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
   public function loadModel($id)
   {
           $model=Employees::model()->findByPk($id);
           if($model===null)
                   throw new CHttpException(404,'The requested page does not exist.');
           return $model;
   }
    
    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
   protected function performAjaxValidation($model)
   {
           if(isset($_POST['ajax']) && $_POST['ajax']==='employees-form')
           {
                   echo CActiveForm::validate($model);
                   Yii::app()->end();
           }
   }
    
}
