<?php
/**
 * Timelog controller
 */
class TimelogController extends Controller
{
    /**
     * @inheritdoc
     */
    
    public $layout = '//layouts/column1';
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index'),
                'users' => array('@'),
            ),

            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * @inheritdoc
     */
    


    public function actionIndex_old(){
        $model = new Accesslog();
        
        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }
        
        /*$ch = curl_init();
        curl_setopt_array(
            $ch, array(
            CURLOPT_URL => 'http://192.168.1.100/getlog.php',
            CURLOPT_RETURNTRANSFER => true
        ));
        $output = curl_exec($ch);
        
        */
        date_default_timezone_set('Asia/Kolkata');
        
        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate-$seldate)/(-3600*24);
            }
        }

        
      
        $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
        

                    
        /*******************************Fetch all logs with punch count, first punch, last punch*************************/

        $sql = "SELECT '$datewithtime' dbtime,  '$precent_date'  as cdate,"
                . "concat(first_name,' ', last_name) as username, "
                . "accesscard_id,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
                . "and empid=accesscard_id) as count_punch ,(select log_time "
                . "from pms_punch_log as pa where log_time  like '$precent_date%' "
                . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
                . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
                . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
                . "where accesscard_id!=0 and first_name not In ('Bala' , 'Amal', 'Sudesh') and `status`=0 group by accesscard_id order by first_name";

            $command = Yii::app()->db->createCommand($sql);
            $logemp = $command->queryAll();
            
          /*  echo "<pre>";
            print_r($logemp);
            echo "</pre>"; */
           
                    
                        
                        
        /************************************Fetch log time **********************************************************/
        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
                . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
                . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();

        /*echo "<pre>";
        print_r($logemp2);
        echo "</pre>"; */
        
                        
        /**************************************Calculate IN OUT ************************************************/
        $userlog = array();
        $userid = 0;
        $flag = 0;
        
        $prev_time = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];
                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                $flag++;
            } else {
                $timediffin = strtotime($rec['log_time']) - $prev_time;
                $prev_time = strtotime($rec['log_time']);
                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;    
                }else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                }
                $flag++;
            }
        }
        
        /*echo "<pre>";
        print_r($userlog);
        echo "</pre>";*/
        
         
        

        /*************************************************Create result array******************************************/
        $res_array = array();
        foreach($logemp as $item){
            $cardid = $item['accesscard_id'];
            $dbtime = $item['dbtime'];

            if ($days < 0) {
                $dbtime = $item['lpunch'];
            } else if ($days == 0 && strtotime($item['dbtime']) > strtotime($item['cdate'] . ' 19:30:00')) {
                //$dbtime = $item['cdate'] . ' 18:30:00';
                $dbtime = $item['lpunch'];
            } else {
              // die();
            }

            $res_array[$cardid]= array();
            $totl_intime = (isset($userlog[$cardid]) ? $userlog[$cardid]['IN'] + ($item['count_punch'] % 2 == 1 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');
            $total_outtime = (isset($userlog[$cardid]) ? $userlog[$cardid]['OUT'] + ($item['count_punch'] % 2 == 0 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');

            if ($item['ipunch'] == '' && $item['lpunch'] == '') {
                $punch_status="No Punch";
            }
            else{
                $punch_status=($item['count_punch'] % 2 == 1 ? 'Punched IN' : 'Punched OUT'); 

                $inpunch = date('h:i:s A', strtotime($item['ipunch']));
                $lstpunch = date('h:i:s A', strtotime($item['lpunch']));
                $tot_out = $this->calculate_time_span($total_outtime);

                $tot_in = $this->calculate_time_span($totl_intime);
                $tot_time =($this->calculate_time_span($totl_intime + $total_outtime));

            }

            $res_array[$cardid]['cardid']=$cardid;
            $res_array[$cardid]['username']=$item['username'];
            $res_array[$cardid]['punch_status']=$punch_status;
            $res_array[$cardid]['precent_date'] = $precent_date;
            $res_array[$cardid]['punch_count'] = $item['count_punch'];

            if($punch_status!="No Punch"){
                $res_array[$cardid]['intime']=$inpunch;
                $res_array[$cardid]['lsttime']=$lstpunch;
                $res_array[$cardid]['tout']=$tot_out;
                $res_array[$cardid]['tot_in']=$tot_in;
                $res_array[$cardid]['tot_time']=$tot_time;
            }
            else{
                $res_array[$cardid]['intime']=0;
                $res_array[$cardid]['lsttime']=0;
                $res_array[$cardid]['tout']=0;
                $res_array[$cardid]['tot_in']=0;
                $res_array[$cardid]['tot_time']=0;
            }

        }

        $shifts = Yii::app()->db->createCommand(
                'SELECT * FROM pms_shift_timings')->queryAll();
        
        return $this->render('index',[
            'model' => $model,  'logemp' => $logemp, 'inout' => $userlog,'days' => $days, 'outtime' => $outtime, 'precent_date' => $precent_date, 'result' => $res_array,
            'shifts' => $shifts,
        ]);
        
       
        
    }
	
    public function actionIndex(){

        $model = new Accesslog();
        
        if (isset($_GET['Accesslog'])) {
            $model->attributes = $_GET['Accesslog'];
        }
        
     /*   $ch = curl_init();
        curl_setopt_array(
            $ch, array(
            CURLOPT_URL => 'http://192.168.1.100/getlog.php',
            CURLOPT_RETURNTRANSFER => true
        ));
        $output = curl_exec($ch);
        */
        
      //  date_default_timezone_set('Asia/Kolkata');
        
        $days = 0;
        if (isset($_GET['days']) && intval($_GET['days'])) {
            $days = intval($_GET['days']);
        }
        if ($model->log_time != "") {
            $seldate = strtotime($model->log_time);
            $todaydate = strtotime(date("Y-m-d"));
            if ($seldate < $todaydate) {
                $days = ($todaydate-$seldate)/(-3600*24);
            }
        }

        
      
        $system_Date = strtotime(date("Y-m-d H:i:s")) + ($days * 60 * 60 * 24);

        $precent_date = date("Y-m-d", $system_Date);
        $datewithtime = date("Y-m-d H:i:s", $system_Date);
        
        
                    
        /*******************************Fetch all logs with punch count, first punch, last punch*************************/
		$remoteip = trim($_SERVER['REMOTE_ADDR']);
	
        $sql = "SELECT reg_ip,'$datewithtime' dbtime,  '$precent_date'  as cdate,"
                . "concat(first_name,' ', last_name) as username, "
                . "accesscard_id,if(reg_ip='$remoteip',1,0) as same,(select count(distinct log_time) from pms_punch_log as pa where log_time like '$precent_date%' "
                . "and empid=accesscard_id) as count_punch ,(select log_time "
                . "from pms_punch_log as pa where log_time  like '$precent_date%' "
                . "and empid=accesscard_id order by log_time asc limit 1) as ipunch , "
                . "(select log_time from pms_punch_log as pa where log_time like '$precent_date%'  "
                . "and empid=accesscard_id order by log_time desc limit 1) as lpunch FROM `pms_users` as al "
                . "where accesscard_id!=0 and (first_name not In ('Bala' , 'Amal', 'Sudesh') or '$remoteip' in(reg_ip))  and `status`=0 group by accesscard_id order by same desc, first_name asc";

            $command = Yii::app()->db->createCommand($sql);
            $logemp = $command->queryAll();
            
          /*  echo "<pre>";
            print_r($logemp);
            echo "</pre>"; */
           
               
                        
                        
        /************************************Fetch log time **********************************************************/
        $sql2 = "select empid,log_time from pms_punch_log where log_time>=adddate(curdate(), INTERVAL $days day) "
                . "and log_time<=adddate(curdate(), INTERVAL " . ($days + 1) . " day) "
                . "group by empid,log_time order by empid asc, log_time asc";

        $command2 = Yii::app()->db->createCommand($sql2);
        $logemp2 = $command2->queryAll();

        /*echo "<pre>";
        print_r($logemp2);
        echo "</pre>"; */
        
                           
        /**************************************Calculate IN OUT ************************************************/
        $userlog = array();
        $userid = 0;
        $flag = 0;
        
        $prev_time = 0;
        $outtime = array();
        foreach ($logemp2 as $rec) {
            if ($userid != $rec['empid']) {
                $flag = 0;
                $userid = $rec['empid'];
                $userlog[$userid] = array('IN' => 0, 'OUT' => 0);

                $prev_time = strtotime($rec['log_time']);
                $flag++;
            } else {
                $timediffin = strtotime($rec['log_time']) - $prev_time;
                $prev_time = strtotime($rec['log_time']);
                if ($flag % 2 == 1) {
                    $userlog[$userid]['IN'] += $timediffin;    
                }else {
                    $userlog[$userid]['OUT'] += $timediffin;
                    $empid1 = $rec['empid'];
                }
                $flag++;
            }
        }
        
        /*echo "<pre>";
        print_r($userlog);
        echo "</pre>";*/
        
         
        

        /*************************************************Create result array******************************************/
        $res_array = array();
		$currentuser = array('punch_status'=>'', 'username'=>'');
		$ino = 0;
        foreach($logemp as $item){
            $cardid = $item['accesscard_id'];
            $dbtime = $item['dbtime'];

            if ($days < 0) {
                $dbtime = $item['lpunch'];
            } else if ($days == 0 && strtotime($item['dbtime']) > strtotime($item['cdate'] . ' 19:30:00')) {
                //$dbtime = $item['cdate'] . ' 18:30:00';
                $dbtime = $item['lpunch'];
            } else {
              // die();
            }

            $res_array[$cardid]= array();
            $totl_intime = (isset($userlog[$cardid]) ? $userlog[$cardid]['IN'] + ($item['count_punch'] % 2 == 1 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');
            $total_outtime = (isset($userlog[$cardid]) ? $userlog[$cardid]['OUT'] + ($item['count_punch'] % 2 == 0 ? strtotime($dbtime) - strtotime($item['lpunch']) : 0) : '0');

            if ($item['ipunch'] == '' && $item['lpunch'] == '') {
                $punch_status="No Punch";
            }
            else{
                $punch_status=($item['count_punch'] % 2 == 1 ? 'Punched IN' : 'Punched OUT'); 

                $inpunch = date('h:i:s A', strtotime($item['ipunch']));
                $lstpunch = date('h:i:s A', strtotime($item['lpunch']));
                $tot_out = $this->calculate_time_span($total_outtime);

                $tot_in = $this->calculate_time_span($totl_intime);
                $tot_time =($this->calculate_time_span($totl_intime + $total_outtime));

            }
			
            $res_array[$cardid]['cardid']=$cardid;
            $res_array[$cardid]['username']=$item['username'];
			$res_array[$cardid]['reg_ip']=$item['reg_ip'];
			$res_array[$cardid]['same'] = $item['same'];
            $res_array[$cardid]['punch_status']=$punch_status;
            $res_array[$cardid]['precent_date'] = $precent_date;
            $res_array[$cardid]['punch_count'] = $item['count_punch'];

					
			
            if($punch_status!="No Punch"){
                $res_array[$cardid]['intime']=$inpunch;
                $res_array[$cardid]['lsttime']=$lstpunch;
                $res_array[$cardid]['tout']=$tot_out;
                $res_array[$cardid]['tot_in']=$tot_in;
                $res_array[$cardid]['tot_time']=$tot_time;
            }
            else{
                $res_array[$cardid]['intime']=0;
                $res_array[$cardid]['lsttime']=0;
                $res_array[$cardid]['tout']=0;
                $res_array[$cardid]['tot_in']=0;
                $res_array[$cardid]['tot_time']=0;
            }
			
			if($item['same']==1){
				$currentuser = $res_array[$cardid];
			}

        }
        
        $shifts = Yii::app()->db->createCommand(
                'SELECT * FROM pms_shift_timings')->queryAll();
        
        $this->render('index1',[
            'model' => $model,  'logemp' => $logemp, 'inout' => $userlog,'days' => $days, 'outtime' => $outtime, 'precent_date' => $precent_date, 'result' => $res_array,
            'shifts' => $shifts,'cuser'=>$currentuser
        ]);
        
       
        
    }
    
    public function calculate_time_span($seconds) {
        $months = floor($seconds / (3600 * 24 * 30));
        $day = floor($seconds / (3600 * 24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours * 3600)) / 60);
        $secs = floor($seconds % 60);

        return $hours . ":" . $mins . ":" . $secs;

       
    }
    


    

    

   
    
    
}
