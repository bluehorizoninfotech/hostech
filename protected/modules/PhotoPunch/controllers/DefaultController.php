<?php

class DefaultController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {


        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $module = Yii::app()->controller->module->id;
        $controller_id = Yii::app()->controller->id;
        $controller = $module . '/' . $controller_id;

        if (isset(Yii::app()->session['pmsmenuall'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuall'])) {
                $accessArr = Yii::app()->session['pmsmenuall'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuauth'])) {
                $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuguest'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuguest'])) {
                $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
            }
        }
        $access_privlg = count($accessauthArr);



        return array(
            array('allow',
                'actions' => array('cronjobtoaddattendance', 'getclientsite'),
                'users' => array('*'),
            ),
            array(
                'allow',
                'actions' => $accessArr,
                'users' => array('@'),
            ),
            array(
                'allow',
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array('allow',
                'actions' => array('applyattendance', 'applyallattendance', 'testattendance', 'updateattendance', 'photomatch'),
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionGetClientsite() {
        $sites_sql = "select site_name,id,distance,latitude,longitude from pms_clientsite";
        $sites = Yii::app()->db->createCommand($sites_sql)->queryAll();
        $this->layout = false;
        header('Content-type: application/json');
        echo json_encode($sites);
        Yii::app()->end();
    }

    public function actionPhotoMatch() {
        $path = dirname(Yii::app()->basePath) . "/uploads/photopunch/*.*";
        foreach (glob($path) as $filename) {
//            echo "$filename size " . filesize($filename) 
//                    ." - ".date ("Y-m-d H:i:s.", filemtime($filename))
//                    ." - - -<b>". date('Y-m-d H:i:s',basename($filename,'.jpg')) ."</b><br />";

            echo basename($filename, '.jpg') . " ++++++ " . (filesize($filename) / 1024 / 1024) . "---------- " . date("Y-m-d H:i:s.", filemtime($filename)) . " ++++ " . date('Y-m-d H:i:s', basename($filename, '.jpg'));
            echo '<br />';
        }
    }

    public function actionIndex() {
        $user = Yii::app()->user->id;
        $employee_id = Yii::app()->db->createCommand(
                "select employee_id from pms_users where userid = '".$user."'")->queryRow();
        $this->render('index', array(
            'employee_id' => $employee_id,
        ));
    }

//    private function unlink_image

    protected function performAjaxValidation2($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'rtouploads-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function readGpslocation($file) {

        if (is_file($file)) {

            $info = @exif_read_data($file);

            if (isset($info['GPSLatitude']) && isset($info['GPSLongitude']) &&
                    isset($info['GPSLatitudeRef']) && isset($info['GPSLongitudeRef']) &&
                    in_array($info['GPSLatitudeRef'], array('E', 'W', 'N', 'S')) && in_array($info['GPSLongitudeRef'], array('E', 'W', 'N', 'S'))) {

                $GPSLatitudeRef = strtolower(trim($info['GPSLatitudeRef']));
                $GPSLongitudeRef = strtolower(trim($info['GPSLongitudeRef']));

                $lat_degrees_a = explode('/', $info['GPSLatitude'][0]);
                $lat_minutes_a = explode('/', $info['GPSLatitude'][1]);
                $lat_seconds_a = explode('/', $info['GPSLatitude'][2]);
                $lng_degrees_a = explode('/', $info['GPSLongitude'][0]);
                $lng_minutes_a = explode('/', $info['GPSLongitude'][1]);
                $lng_seconds_a = explode('/', $info['GPSLongitude'][2]);

                $lat_degrees = $lat_degrees_a[0] / $lat_degrees_a[1];
                $lat_minutes = $lat_minutes_a[0] / $lat_minutes_a[1];
                $lat_seconds = $lat_seconds_a[0] / $lat_seconds_a[1];
                $lng_degrees = $lng_degrees_a[0] / $lng_degrees_a[1];
                $lng_minutes = $lng_minutes_a[0] / $lng_minutes_a[1];
                $lng_seconds = $lng_seconds_a[0] / $lng_seconds_a[1];

                $lat = (float) $lat_degrees + ((($lat_minutes * 60) + ($lat_seconds)) / 3600);
                $lng = (float) $lng_degrees + ((($lng_minutes * 60) + ($lng_seconds)) / 3600);

                //If the latitude is South, make it negative.
                //If the longitude is west, make it negative
                $GPSLatitudeRef == 's' ? $lat *= -1 : '';
                $GPSLongitudeRef == 'w' ? $lng *= -1 : '';

                return array(
                    'lat' => $lat,
                    'lng' => $lng,
                );
            }
        }

        return false;
    }

    public function pendingTask() {
        $created_by = Yii::app()->user->id;
        $today = date("Y-m-d");

        $rejected_BOQ_sql = "select `taskid` from pms_daily_work_progress where created_by=" . $created_by;
        $rejected_BOQ_sql .= " and date<'" . $today . "' and approve_status=2";

        $rejected_BOQ = Yii::app()->db->createCommand($rejected_BOQ_sql)->queryAll();

        if (count($rejected_BOQ) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function pending_approval() {
        $coordinator = Yii::app()->user->id;
        $request_sql = "select pms_daily_work_progress.*
		from pms_daily_work_progress
		LEFT JOIN pms_tasks
		ON pms_daily_work_progress.taskid = pms_tasks.tskid
		where (coordinator=" . $coordinator . " or report_to=" . $coordinator . " ) and approve_status=0"
        ;
        $requests = Yii::app()->db->createCommand($request_sql)->queryAll();

        foreach ($requests as $request) {
            $minutes = 0;
            $created_at = $request['created_date'];
            $start_date = new DateTime($created_at);
            $since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s")));

            $minutes = $since_start->days * 24 * 60;
            $minutes += $since_start->h * 60;
            $minutes += $since_start->i;
            $total_minutes = $minutes;
            if ($total_minutes > 1440) {

                return false;
            }
        }

        $request_date = date("Y-m-d", strtotime("-2 day"));

        $approved_request_sql = "select pms_daily_work_progress.*
		from pms_daily_work_progress
		LEFT JOIN pms_tasks
		ON pms_daily_work_progress.taskid = pms_tasks.tskid
		where (coordinator=" . $coordinator . " or report_to=" . $coordinator . " ) and date='" . $request_date . "'";

        $approved_requests = Yii::app()->db->createCommand($approved_request_sql)->queryAll();

        foreach ($approved_requests as $request) {
            $minutes = 0;
            $created_at = $request['created_date'];
            $approved_date = $request['approved_date'];
            $start_date = new DateTime($created_at);
            $approved_date = new DateTime($approved_date);
            $since_start = $start_date->diff(($approved_date));

            $minutes = $since_start->days * 24 * 60;
            $minutes += $since_start->h * 60;
            $minutes += $since_start->i;
            $total_minutes = $minutes;

            if ($total_minutes > 1440) {

                return true; // change this  to false for enable  manager can not punch if he take  more than 24 hr for take an action on boq entry  
            }
        }

        return true;

        exit;
    }

    public function findSite($latitude, $longitude) {
        $sites_sql = "select site_name,id,distance,latitude,longitude from pms_clientsite 
        left join pms_clientsite_assigned ON pms_clientsite.id = pms_clientsite_assigned.site_id where user_id=" . Yii::app()->user->getId();
        $sites = Yii::app()->db->createCommand($sites_sql)->queryAll();
        $return_data = array();
        if (count($sites) == 0) {
            $return_data = array("site_name" => "No site");
        }
        foreach ($sites as $site) {
            $site_latitude = $site['latitude'];
            $site_logitude = $site['longitude'];
            if ($site_logitude != "" && $site_latitude != "") {
                $distance = $this->distance($latitude, $longitude, $site_latitude, $site_logitude, "K");
                $difference_distance = $distance - $site['distance'];
                $return_data = array('distance' => $difference_distance);
                if ($distance < $site['distance']) {
                    $site_name = $site['site_name'];
                    $site_id = $site['id'];
                    $return_data = array("site_name" => $site_name, 'site_id' => $site_id, 'distance' => $distance);
                    break;
                }
            }
        }

        return $return_data;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                $kilo_meters = ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
            $in_meter = $kilo_meters * 1000;
            return $in_meter;
        }
    }

    public function actionView() {
        $id = $_REQUEST['id'];
        $model = Photopunch::model()->findByPk($id);
        return($this->renderPartial('view', array(
                    'model' => $model,
        )));
    }

    public function actionShiftAction() {
        if (isset($_REQUEST['id'])) {
            extract($_REQUEST);
            $work_site_name = Clientsite::model()->findByPk($site);
            if ($req == "Approved") {
                $photo_punch = Photopunch::model()->findByPk($id);
                $photo_punch->approved_status = 1;
                $photo_punch->reason = $reason;
                $photo_punch->work_site_id = $site;
                $photo_punch->work_site_name = $work_site_name['site_name'];
                $photo_punch->save();
                $check_exist = Yii::app()->db->createCommand()
                        ->select('aid, request_id')
                        ->from('pms_punch_log')
                        ->where('request_id=:id', array(':id' => $photo_punch->aid))
                        ->queryRow();
                if ($check_exist['aid'] == "") {
                    $access_log = New Accesslog;
                    $access_log->logid = $photo_punch->logid;
                    $access_log->sqllogid = $photo_punch->sqllogid;
                    $access_log->empid = $photo_punch->empid;
                    $access_log->log_time = $photo_punch->log_time;
                    $access_log->status = $photo_punch->status;
                    $access_log->device_id = $photo_punch->device_id;
                    $access_log->manual_entry_status = $photo_punch->manual_entry_status;
                    $access_log->image = $photo_punch->image;
                    $access_log->latitude = $photo_punch->latitude;
                    $access_log->longitude = $photo_punch->longitude;
                    $access_log->punch_type = $photo_punch->punch_type;
                    $access_log->work_site_id = $photo_punch->work_site_id;
                    $access_log->work_site_name = $photo_punch->work_site_name;
                    $access_log->request_id = $photo_punch->aid;
                    $access_log->save();
                }
            } else {

                $photo_punch = Photopunch::model()->findByPk($id);
                $photo_punch->approved_status = 2;
                $photo_punch->reason = $reason;
                $photo_punch->save();
                $this->SendRejectMail($photo_punch);
            }
        }
        $ret['msg'] = 'Photo punch ' . $req . ' Successfully';
        $ret['error'] = '';
        $ret['status_id'] = $photo_punch->aid;

        $status_items = array('Pending', 'Approved', 'Rejected');
        $status_item_colors = array('orange', 'green', 'red');

        $ret['status'] = '<div id="approved_status_' . $photo_punch->aid . '">'
                . '<span style="color:' . ($status_item_colors[$photo_punch->approved_status]) . '">' . $status_items[$photo_punch->approved_status] . '</span>'
                . '</div>';
        echo json_encode($ret);
        exit;
    }

    public function SendRejectMail($photo_punch) {

        $mail_model = GeneralSettings::model()->find(['condition' => 'id = 1']);
        $user = Users::model()->findByPk($photo_punch->empid);
        $model2 = new MailLog;
        $mail = new JPhpMailer();
        $today = date('Y-m-d H:i:s');
        $headers = "Hostech";
        $mail_data = '
	<div style="width:750px;height:auto;font-family:Arial, Helvetica, sans-serif;font-size:12px">
	<h3 style="padding:0 0 6px 0;margin:0;font-family:Arial,Sans-serif;font-size:16px;font-weight:bold;color:#222"><span>Hi ' . $user->first_name . '</span></h3>
	<table style="border:1px solid #d2d2d2;margin:10px;width:730px;border-collapse:collapse;" cellpadding="5">
	<tr><td colspan="2" style="background-color: #000;border:1px solid #999;color: #ffff;"><div align="center" style=""><img src="' . $this->logo . '" alt="' . Yii::app()->name . '" alt="HOSTECH" class="CToWUd"></div></td>
	</tr>
	<tr><td colspan="2" style="border:1px solid #999;"><div style="float:left;"><h2>Photo Punch Rejected</h2></div></td>
	</tr>
	<tr><td colspan="2">This is a notification to let you know that your photo punch is rejected: </td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Attendance Time: </td><td style="border:1px solid #f5f5f5;">' . date("d-m-Y H:i:s a", strtotime($photo_punch->log_time)) . '</td></tr>
	<tr><td style="color:#999;width:150px;border:1px solid #f5f5f5;background-color:#fafafa;font-weight:bold;">Reason: </td><td style="border:1px solid #f5f5f5;">' . $photo_punch->reason . '</td></tr>';

        $mail_data .= '</table>
	<p>Sincerely,  <br />
	' . Yii::app()->name . '</p>
	</div>';

        //$mail_data = "<p>Project : ".$projects->name."</p><p>Task ID : #".$tasks->tskid."</p><p>Task : ".$tasks->title."</p><p>Date : ".$tasks->start_date." to ".$tasks->due_date."</p><p>Current progress : ".number_format($current_given_percenatge,2)."%</p><p>Current status behind by: ".$percent." %</p><p>Number of wokers required : ".$workers_no."</p>";
        $bodyContent = $mail_data;
        $mail->IsSMTP();
        $mail->Host = $mail_model['smtp_host'];
        $mail->SMTPSecure = $mail_model['smtp_secure'];
        $mail->SMTPAuth = $mail_model['smtp_auth'];
        $mail->Username = $mail_model['smtp_username'];
        $mail->Password = $mail_model['smtp_password'];
        $mail->setFrom($mail_model['smtp_email_from'], $mail_model['smtp_mailfrom_name']);
        $mail->Subject = 'Photo punch Reject';
        $receippients = $task_model->assignedTo->email;
        $mail->addAddress($user->email); // Add a recipient
        $mail_send_by = array('smtp_email_from' => $mail_model['smtp_email_from'], 'smtp_mailfrom_name' => $mail_model['smtp_mailfrom_name']);
        $dataJSON = json_encode($data);
        $receippientsJSON = json_encode($receippients);
        $mail_send_byJSON = json_encode($mail_send_by);
        $subject = $mail->Subject;
        $model2->send_to = $task_model->assignedTo->email;
        $model2->send_by = $mail_send_byJSON;
        $model2->send_date = $today;
        $model2->message = htmlentities($bodyContent);
        $model2->description = $mail->Subject;
        $model2->created_date = $today;
        $model2->created_by = Yii::app()->user->getId();
        $model2->mail_type = $subject;
        $mail->isHTML(true);
        $mail->MsgHTML($bodyContent);
        $mail->Body = $bodyContent;
        if ($mail->Send() && !empty($task_model->assignedTo->email)) {
            $model2->sendemail_status = 1;
        } else {
            $model2->sendemail_status = 0;
        }
        $model2->save();
    }

}
