<?php


Yii::app()->clientScript->registerScript(
        'myHideEffect', '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");', CClientScript::POS_READY
);
?>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
<style type="text/css">
body {
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        color: #333;
        text-transform: uppercase;
    }
</style>
<div class="photopunch-backup-sec">
<div class="container">
    <div class="form">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group text-center page-head">
                    <!-- <img src="<?php //echo Yii::app()->getBaseUrl(); ?>/images/k1-logo.png" alt="k1-logo"> -->
                    <h2>JSW RTO FEEDBACK FORM</h2>
                </div>
            </div>
        </div>
<?php
$flashMessages = Yii::app()->user->getFlashes();

if ($flashMessages) {

    echo '<div class="row info">
      <div class="col-md-6">
                <div class="form-group">';
    //  print_r($flashMessages);
    echo '<ul class="flashes">';
    foreach ($flashMessages as $key => $message) {
        echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
    }
    echo '</ul></div>
 </div> </div>';
}
?>








<form id="photopunch" method="post" action="<?=Yii::app()->createUrl('PhotoPunch/default/index') ?>" name="photopunch">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="form-group capture-div text-center">
     
					<input id="myFileInput" name="photopunchfile" type="file" accept="image/*;capture=camera">

                </div>
            </div>





        <div class="row buttons">
            <div class="col-md-4  col-md-offset-4">
               <input type="submit" name="submit">
            </div>
        </div>

		</form>
</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
        <script type="text/javascript">

            jQuery(document).ready(function (e) {
                jQuery("#upfile").click(function () {

                    jQuery("#RtoFileupload_image").trigger('click');
                });

                var input = document.querySelector('input[type=file]');

                input.onchange = function () {
                    var file = input.files[0];
                    drawOnCanvas(file);
                    displayAsImage(file,0);
                };

                function drawOnCanvas(file) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        var dataURL = e.target.result,
                                c = document.querySelector('canvas'),
                                ctx = c.getContext('2d'),
                                img = new Image();

                        img.onload = function () {
                            c.width = 100;
                            c.height = 100;
                            ctx.drawImage(img, 0, 0, 100, 100);
                        };

                        img.src = dataURL;
                    };

                    reader.readAsDataURL(file);
                }

                function displayAsImage(file,type) {
                    var imgURL = URL.createObjectURL(file),
                    img = document.createElement('img');

                    img.onload = function () {
                        URL.revokeObjectURL(imgURL);
                    };

                    img.src = imgURL;
                    if(type==0){
                      document.querySelector('.capture-div').appendChild(img);
                    }

                }



            });

        </script>
    </div><!-- form -->
</div>

