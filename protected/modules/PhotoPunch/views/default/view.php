<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<div class="view-photopunch-sec">
<div id="sidebar">
    <div class="portlet" id="yw2">
        <div class="portlet-decoration">
            <div class="portlet-title"></div>
        </div>
        <div class="portlet-content">
            <ul class="operations" id="yw3">
            <!-- <li><a href="<?= Yii::app()->createUrl('/reports/photoPunchReport') ?>">Manage photopunch</a></li>  -->
            </ul></div>
    </div>                                </div>

<?php // if (in_array('/approvals/attendanceprivileges', Yii::app()->session['pmsmenuauthlist'])) { ?>

<div id="actionmsg"> </div>
<div class="punch_details panel panel-primary display-none">
    <div class="panel-heading">
        <h3 class="panel-title">Punch Details</h3>
        <span class="closebtn3" id="closebtn" onclick="closeformnoti(this)"><a href="#">×</a></span>
    </div>
    <div class="panel-body">        
        <div> 
            <div class="image-view">
                <?php
                if (file_exists($model->image)) {
                    $filename = $model->image;
                    echo CHtml::image($model->image, '', array('class' => "media-object max-width-100-percentage"));
                } else {
                    echo CHtml::image('images/no-img.png', 'No Photo found', array('class' => "media-object max-width-100-percentage"));
                }
                ?>

            </div>

            <div class="detail-sec">
            <!-- <h4><?php //echo ($model->usrId->first_name . " " . $model->usrId->last_name);    ?></h4> -->
                <div>
                    <b>Status:</b>
                    <span class="approve_status">
                        <?php echo (($model->approved_status == 0) ? "<span class=vivid-orange font-weight-bold>Pending</span>" : (($model->approved_status == 1) ? "<span class=green-color font-weight-bold>Approved </span>" : "<span class=red-color font-weight-bold> Rejected</span>")); ?>
                    </span>
                </div>
                <div>
                    <b>Sites</b>
                    <span class="">
                        <?php
                        $sites = Clientsite::model()->findAll();
                        $sites = CHtml::listData($sites,
                                        'id', 'site_name');
                        echo CHtml::dropDownList("work_sites", $model->work_site_id, $sites, array('empty' => 'Select a site', 'class' => 'form-control', 'id' => 'punch_site'));
                        ?>
                    </span>
                </div>
                <div>
                    <b>Log Time:</b> 
                    <?php echo (date("d-m-Y h:i:s a", strtotime($model->log_time))); ?>
                </div>
                <div>
                    <b>Reason:</b>
                    <?php echo ($model->reason); ?>
                </div>
            </div>
            <div>
                <?php if ($model->approved_status == 0) { ?>
                    <textarea name="reason" class="reason" id="reason_value"></textarea> 
                    <div class="text-center">
                        <input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approved' /> 
                        <input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Rejected' />
                    </div>
                <?php } ?>
            </div>
            
            <div id="ajax_loding_area"></div>

        </div>

    </div> 
</div>
<?php
/* $this->widget('zii.widgets.CDetailView', array(
  'data' => $model,
  'attributes' => array(
  array(
  'name' => 'name',
  'value' => $model->usrId->first_name . " " . $model->usrId->last_name,
  ),
  array(
  'name' => 'approved_status',
  'value' => (($model->approved_status == 0) ? "Pending" : (($model->approved_status == 1) ? "Approved " : " Rejected")),
  ),
  array(
  'name' => 'log_time',
  'value' => date("d-m-Y h:i:s a", strtotime($model->log_time)),
  ),
  array(
  'name' => 'image',
  'type' => 'raw',
  'value' => file_exists(Yii::app()->basePath . '/../' . $model->image) ? CHtml::image($model->image, "", array("style" => "width:100px;height:100px;")) : "No Photo image found",
  ),
  'reason', */
// array(
// 		'name' => 'created_by',
// 		'value' => $model->createdBy->firstname." ".$model->createdBy->lastname,
// 	),
//     'created_date',
// 	array(
// 		'name' => 'decision_by',
// 		'value' => isset($model->decisionBy->firstname) ? $model->decisionBy->firstname . " " . $model->decisionBy->lastname : null,
// 	),
// 	'decision_date',
// 	'reason',
// 	array(
// 		'name' => 'status',
// 		'value' => $model->shift_status($model->status),
// 		'type'=>'raw'
// 	),
/* ),
  )); */
?>

<!-- include -->
<?php
Yii::app()->clientScript->registerScript("", "");
?>
<!-- end -->
</div>
<script type='text/javascript'>

    $(document).ready(function () {


        $(".reason").keyup(function () {
            var val = $(this).val()
            $(this).val(val.toUpperCase())
        })


        $('.shift_action').click(function () {
            var req = $(this).val();
            var id =<?php echo $model->aid ?>;
            var reason = $('#reason_value').val();
            var site = $('#punch_site').val();
            var type = 0;
            var flag = 0;
            if (req == "Rejected")
            {
                if (reason == "")
                {
                    alert("please enter reason")
                    flag = 1;
                }
            }
            if (req == "Approved")
            {
                if (site == "")
                {
                    alert("please select punch site")
                    flag = 1;
                }
            }
            if (flag != 1) {
                if (!confirm('Are you sure you want to ' + $(this).val() + ' this Request?') === false) {

                     $('#load').show();
                     $('#ajax_loding_area').html('Submitting....');
                    location.reload()
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('/PhotoPunch/default/ShiftAction') ?>',
                        data: {id: id, req: req, type: type, reason: reason, site: site}
                    }).done(function (ret) {
                        $('#load').hide();
                        if (ret.msg != '') {
                            $("#approved_status_"+ret.status_id).html(ret.status);
                            //alert($("#approved_status_"+ret.status_id).html());
                            $('#actionmsg').addClass('successaction');
                            $('#actionmsg').html(ret.msg);
                            $('.shift_action').hide();
                            $('.reason').hide();
                            $("#closebtn").trigger('click');
                           
                        } else if (ret.error != '') {

                            $('#actionmsg').addClass('erroraction');
                            $('#actionmsg').html(ret.error);

                        }
                        $('#ajax_loding_area').html('');
                    });
                }
            }
        });
    });
</script>

