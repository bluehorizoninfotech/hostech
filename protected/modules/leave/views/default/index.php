<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
  <div class="index-leave-sec">
  <div class="clearfix">
    <div class="pull-left">
        <h1>My Leaves</h1>
    </div>
    <div class="pull-right">
        <button class="btn btn-primary btn_summary">Available Leave Summary</button>
        <?php


        //if(in_array('/leaves/applyleave', Yii::app()->session['pmsmenuauthlist'])){

            echo CHtml::link('Apply Leave', array('/leave/default/create'),array('class'=>'btn btn-primary'));

        // }
        ?>
        
    </div>
</div>
<div class="clearfix">
<div class="leave_summary_section pull-right width-50-percentage display-none">
   

    <?php
     $paid_leave=0;
     $casual_leave=0;
     $medical_leave=0;
     $comp_leave=0;
     $tblpx = Yii::app()->db->tablePrefix;
     $avlleaves = Yii::app()->db->createCommand("select concat_ws(' ',first_name,last_name) as fullname,available_leave.* 
     from " . $tblpx . "avail_leave  as available_leave
     inner join pms_users as users on users.userid = available_leave.userid 
     where available_leave.userid=" . Yii::app()->user->id)->queryAll();
    if (count($avlleaves)) {
        foreach($avlleaves as $my_leave)
        {
           
            if($my_leave['type']==10)
            {
                $casual_leave=$my_leave['consi_leave'];
            }
            if($my_leave['type']==11)
            {
                $comp_leave=$my_leave['consi_leave'];
            }
            
        }
    }

    $leave_sql="SELECT count(*) as count FROM `pms_leave` 
    left join pms_leave_day as t on  t.leave_id=pms_leave.leave_id
    WHERE `emp_id`=" .  Yii::app()->user->id . " and t.leave_period= 10 and (`approval_status`= 9 or `approval_status`= 11) and date_from BETWEEN '".date('Y-m-01')."' AND '" .date('Y-m-t')."'";
   
   
   $data = Yii::app()->db->createCommand($leave_sql )->queryRow();
    if ($data['count'] > 0) {
        $utilised_casual_leave = $data['count'];
    } else {
        $utilised_casual_leave = 0;
    }
    $remain_casual_leave = $casual_leave- $utilised_casual_leave;
    if ($remain_casual_leave < 0) {
        $remain_casual_leave = 0;
    }
    if($remain_casual_leave>3)
    {
        $remain_casual_leave = 3;
    }

    // casuall
    $leave_sql="SELECT count(*) as count FROM `pms_leave` 
    left join pms_leave_day as t on  t.leave_id=pms_leave.leave_id
    WHERE `emp_id`=" .  Yii::app()->user->id . " and t.leave_period= 11 and (`approval_status`= 9 or `approval_status`= 11) and date_from BETWEEN '".date('Y-m-01')."' AND '" .date('Y-m-t')."'";
   
    $data = Yii::app()->db->createCommand($leave_sql )->queryRow();
    if ($data['count'] > 0) {
        $utilised_comp_leave = $data['count'];
    } else {
        $utilised_comp_leave = 0;
    }
    
    $remain_comp_leave = $comp_leave - $utilised_comp_leave;
    if ($remain_comp_leave < 0) {
        $remain_comp_leave = 0;
    }

    //medical
   

    ?>

    <table class="greytable table table-bordered">
        <thead><tr><th></th><th>Accrued</th><th>Balance</th></tr></thead>
        <tbody>
          <tr><th>Casual Leave : </th><td><?= $casual_leave ?></td>
            <td><?= $remain_casual_leave; ?> </td></tr>



            <tr><th>Compensatory Leave: </th><td><?= $comp_leave ?></td>
            <td><?= $remain_comp_leave; ?> </td></tr>

            </tbody>
    </table>

</div>
</div>
<br clear='all' />
<?php

$leave_model = Yii::app()->db->createCommand(" SELECT leave_id,description FROM `pms_leave_types` as lt inner join pms_legends as lg on lg.leg_id= lt.`leave_id` ")->queryAll();

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leave-grid',
    'itemsCssClass' => 'greytable table table-bordered',
    'template' => '<div class="table-responsive">{items}</div>',
    'dataProvider' => $model->search(Yii::app()->user->id), //own leaves only
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
        //'leave_id',
        //'emp_id',
        'leave_submit_date',
        // array('name' => 'leave_type', 'value' => '$data->leaveType->description',
        //     'filter' => CHtml::listData($leave_model,'leave_id','description'),
        //     ),
        'date_from',
        'date_to',
        array('header' => 'Total days', 'value' => 'LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$data->leave_id"
                    ))->leave_value',
        ),
        'reason_for_leave',
        array('name' => 'approval_status', 'value' => '($data->approval_status!=""?$data->approvalStatus->caption:"Pending")',
            'filter' => CHtml::listData(LeaveSetting::model()->findAll(
                            array(
                                'select' => array('leave_setting_id,caption'),
                                'order' => 'caption',
                                'condition' => 'setting_type="leave_status"',
                                'distinct' => true
                    )), "leave_setting_id", "caption"),
        ),
        array('name'=>'created_by',
            'type'   =>'raw',
            'value'=>' isset($data->createdBy->first_name)?$data->createdBy->first_name." ".$data->createdBy->last_name."<br/><small style=color:#337ab7;>".$data->getcompany($data->created_by)."</small>":null'
        ),

       
    ),
));
?>
</div>
<script>
$(document).ready(function(){


$("input[name='Leave[leave_submit_date]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$("input[name='Leave[date_from]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$("input[name='Leave[date_to]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$('.btn_summary').click(function(){
    $('.leave_summary_section').slideToggle();
})
})

</script>