<?php
/* @var $this LeaveController */
/* @var $model Leave */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'leave_id'); ?>
		<?php echo $form->textField($model,'leave_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'leave_submit_date'); ?>
		<?php echo $form->textField($model,'leave_submit_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'leave_type'); ?>
		<?php echo $form->textField($model,'leave_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_from'); ?>
		<?php echo $form->textField($model,'date_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_to'); ?>
		<?php echo $form->textField($model,'date_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reason_for_leave'); ?>
		<?php echo $form->textField($model,'reason_for_leave',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contact_details'); ?>
		<?php echo $form->textField($model,'contact_details',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'document_if_any'); ?>
		<?php echo $form->textField($model,'document_if_any',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'person_covering_role'); ?>
		<?php echo $form->textField($model,'person_covering_role',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approval_status'); ?>
		<?php echo $form->textField($model,'approval_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'decision_by'); ?>
		<?php echo $form->textField($model,'decision_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'decision_date'); ?>
		<?php echo $form->textField($model,'decision_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->