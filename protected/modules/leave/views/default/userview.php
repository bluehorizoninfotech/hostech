<?php
/* @var $this LeaveController */
/* @var $model Leave */

$this->breadcrumbs = array(
    'Leaves' => array('index'),
    $model->leave_id,
);

$this->menu = array(
    array('label' => 'General', 'url' => array('/employmentDetails/update&id=' . $userid)),
    array('label' => 'Communication Details', 'url' => array('/currentAddress/view&id=' . $userid), 'itemOptions' => array('class' => 'active')),
    array('label' => 'Payroll', 'url' => array('/employmentDetails/listsalary', 'id' => $userid)),
    array('label' => 'Leaves', 'url' => array('/leave/default/userleaves', 'id' => $userid)),
);


?>
<div class="user-view-leave-sec">
<h1>Leave Details</h1>
<div class="page_devide">
    <?php
//$items = $leave_days_dur;

    $this->widget('zii.widgets.CDetailView', array(
         'htmlOptions' => array('class' => 'greytable'),
        'data' => $model,
        'attributes' => array(
                
            //'leave_id',
            'leave_submit_date',
            array('name' => 'leave_type', 'value' => $model->leaveType->caption),
            'date_from',
            'date_to',
            array('name' => 'Total Days', 'value' => LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$model->leave_id"
                    ))->leave_value,
            ),
            'reason_for_leave',
            'contact_details',
            'document_if_any',
            'person_covering_role',
            array('name' => 'approval_status', 'value' => (($model->approval_status == '') ? 'Pending' : $model->approvalStatus->caption)),
            //'decision_by',
            //'decision_date',
            //'created_by',
            'created_date',
            //'updated_by',
            'updated_date',
        ),
    ));
    ?>
</div>
<div class="page_devide" id="dateslist">
    <h4>Day Leave Details</h4>
    <table cellpadding="10" cellspacing="0" class="greytable">
        <thead>
            <tr>
                <th>Date</th>
                <th>Leave Duration</th>
            </tr>
        </thead>
        <tbody id="leavedates">
            <?php
            $sql = "select * from hrms_leave_day inner join hrms_leave_setting on leave_setting_id=leave_period where leave_id=".$model->leave_id;
            $rec = Yii::app()->db->createCommand($sql)->queryAll();
            foreach($rec as $row){
                echo "<tr><td>".$row['leave_date']."</td><td>".$row['caption']."</td></tr>";                
            }
            ?>
        </tbody>
    </table>
<div class="pull-left width-100-percentage" >
    <h4>Available Leave Summary</h4>
    
    <?php
     $tblpx = Yii::app()->db->tablePrefix;
    $avlleaves = Yii::app()->db->createCommand('select * from {$tblpx}avail_leave where userid='.Yii::app()->user->id)->queryAll();
//    echo '<pre>';
//    print_r($avlleaves);
//    echo '</pre>';
    
    $cl_ml = "";
    $earned="";
    $lieu="";
    if(count($avlleaves)){
    extract($avlleaves[0]);
    }
    ?>
    
    <table class="greytable">
        <thead><tr><th></th><th>Accrued</th><th>Balance</th></tr></thead>
        <tbody>
        <tr><th>Casual/Medical Leave: </th><td><?=$cl_ml?></td><td><?=$cl_ml?></td></tr> 
        <tr><th>Earned Leave: </th><td><?=$earned?></td><td><?=$earned?></td></tr>
        <tr><th>Lieu Leave</th><td>&nbsp;</td><td><?=$lieu?></td></tr></tbody>
    </table> 
    
</div>
</div>



<div>
    <!--                <div class="subrow12 subrow233px" style="float: left;">-->
    <div class="col-md-4">
        <h3>Personal Details</h3>
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $usermodel,
            'htmlOptions'=>array('class'=>'greytable'),
            'attributes' => array(
                'employee_id',
                'first_name',
                'last_name',
                [
                    'label' => 'Gender',
                    'value' => ($usermodel->gender == 'M') ? 'Male' : 'Female'
                ],
                'marital_status',
                array(
                    'label' => 'Photograph',
                    'type' => 'raw',
                    'value' => isset($usermodel->photograph) ? CHtml::image(
                                    Yii::app()->baseUrl . 'uploads/' . $usermodel->photograph, 'Alt text', array('class' => 'img_class')  //and other html tag attributes
                            ) : CHtml::image(
                                    Yii::app()->baseUrl . 'images/default.png', 'Alt text', array('class' => 'img_class')  //and other html tag attributes
                            ),
                ),
                'system_access_types',
            ),
        ));
        ?>
    </div>
    
    </div>
    </div>