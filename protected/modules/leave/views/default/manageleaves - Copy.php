
<div class="pull-left">
    <h1>Manage All Leaves</h1>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.leave_results_report a').on('click', function () {
            var selected_status_id = $(this).attr('id');
            $(".leave_results_report div").each(function () {
                $(this).removeAttr('class');
            });
            // alert(selected_status_id);
            $("#" + selected_status_id).parent().addClass('activests');
        });
    });

</script>
<?php
 $usertbl = Users::model()->tableSchema->rawName;

 $company = '';
    if( isset(Yii::app()->user->company_id) ){

        $company = " and company_id = ".Yii::app()->user->company_id;
    }


     /* new change */

    if(Yii::app()->user->role!=1){


        $data1 = Yii::app()->db->createCommand("SELECT * FROM `pms_leave_shift` WHERE `shift_supervisor`=".Yii::app()->user->id)->queryAll();
        $dataem = array();
        foreach ($data1 as $key => $value) {
            $dataem[]=$value['emp_id'];
        }

        $dataem1 = implode(',', $dataem);



         /*  shift supervisor */
        if(!empty($data1) and isset(Yii::app()->user->company_id) ){

            $sql = "select leave_setting_id,caption,approval_status,count(approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.emp_id=l.emp_id ".$company." "
        . " right join pms_leave_setting as ls on approval_status=leave_setting_id"
        . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and  us.emp_id in ('.$dataem1.') or us.report_to='. Yii::app()->user->id:'')." group by leave_setting_id";

        

        }
		else if(isset(Yii::app()->user->company_id)){
		  $sql = "select leave_setting_id,caption,approval_status,count(approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.emp_id=l.emp_id ".$company." "
        . " right join pms_leave_setting as ls on approval_status=leave_setting_id"
        . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and  us.report_to='. Yii::app()->user->id:'')." group by leave_setting_id";

        
//die;
		}
		else{
				$sql = "select leave_setting_id,caption,approval_status,count(approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.emp_id=l.emp_id  "
        . " right join pms_leave_setting as ls on approval_status=leave_setting_id"
        . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and  us.emp_id in ('.$dataem1.') or us.report_to='. Yii::app()->user->id:'')." group by leave_setting_id";
	
		}
		
		
          

    /* end */



    }else{


 $sql = "select leave_setting_id,caption,approval_status,count(approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.emp_id=l.emp_id ".$company." "
        . " right join pms_leave_setting as ls on approval_status=leave_setting_id"
        . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and us.report_to='. Yii::app()->user->id:'')." group by leave_setting_id";

       



    }


 /*$sql = "select leave_setting_id,caption,approval_status,count(approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.emp_id=l.emp_id ".$company." "
        . " right join pms_leave_setting as ls on approval_status=leave_setting_id"
        . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and us.report_to='. Yii::app()->user->id:'')." group by leave_setting_id";
*/
$statuss = Yii::app()->db->createCommand($sql)->queryAll();
?>
<div class="manageleaves-copy-sec">
<div class='leave_results_report pull-right'>
    <div class='activests'><?php echo CHtml::link('All', '#', array('id' => 'all', 'onclick' => '$("select[name=\'Leave[approval_status]\']").val("").change();')); ?></div> 
    <?php
			
    foreach ($statuss as $sts) {
        extract($sts);

        ?>
        <div><?php echo CHtml::link($sts['caption'] . ': ' . $sts['tot_count'], '#', array('id' => 'sts_id_' . $sts['leave_setting_id'], 'onclick' => '$("select[name=\'Leave[approval_status]\']").val('.$approval_status.').change();')); ?></div> 
        <?php
    }
    ?>

</div><div class='clearfix'></div>

<?php

$leave_model = Yii::app()->db->createCommand(" SELECT leave_id,description FROM `pms_leave_types` as lt inner join pms_legends as lg on lg.leg_id= lt.`leave_id` ")->queryAll();

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leave-grid',
    'itemsCssClass' => 'greytable',
    'dataProvider' => $model->search(0), //own leaves only
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        //'leave_id',
        //'emp_id',
        array('name' => 'emp_id', 'header' => 'Emp ID', 'value' => '$data->empid->employee_code'),
        array('name' => 'empname', 
            'header' => 'Name', 
            'type'   =>'raw',
            'value'  => '$data->empid->firstname." ".$data->empid->lastname."<br/><small style=color:#337ab7;>".$data->getcompany($data->emp_id)."</small>"',
            ),
//        array('name' => 'emp_id', 'header' => 'Designation', 'value' => '$data->empid->designation'),
//        array('name' => 'emp_id', 'header' => 'Department', 'value' => '$data->empid->department'),
        'leave_submit_date',
        array('name' => 'leave_type', 'value' => '$data->leaveType->description',
            'filter' =>  CHtml::listData($leave_model,'leave_id','description'),
            ),
        'date_from',
        'date_to',
        array('header' => 'Total days', 'value' => 'LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$data->leave_id"
                    ))->leave_value',
        ),
        //'reason_for_leave',

        array('name'=>'created_by','value'=>'$data->createdBy->firstname." ".$data->createdBy->lastname'),

        array('name' => 'approval_status', 'value' => '($data->approval_status!=""?$data->approvalStatus->caption:"Pending")',
            'filter' => CHtml::listData(LeaveSetting::model()->findAll(
                            array(
                                'select' => array('leave_setting_id,caption'),
                                'order' => 'caption',
                                'condition' => 'setting_type="leave_status"',
                                'distinct' => true
                    )), "leave_setting_id", "caption"),
        ),

         array(

            'name'=>'decision_by',
            'value'=>'$data->getdecisionby($data->decision_by)',
         ),

        
        /* 'contact_details',
          'document_if_any',
          'person_covering_role',

          'decision_by',
          'decision_date',
          
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'template' => '{approve_reject}{view}',
            'buttons' => array
                (
                'approve_reject' => array
                 (
                    'label' => 'Approve/Reject',
                    'url' => 'Yii::app()->createUrl("/leave/default/viewleave", array("id"=>$data->leave_id))',
                    'visible'=>'$data->approval_status==11',
                ),

                'view' => array
                (
                    'label' => 'Approve/Reject',
                    'url' => 'Yii::app()->createUrl("/leave/default/viewleave", array("id"=>$data->leave_id))',
                    'visible'=>'$data->approval_status!=11',
                )
            ),
        ),
    ),
));
?>
</div>