<?php
/* @var $this LeaveController */
/* @var $model Leave */

$this->breadcrumbs=array(
	'Leaves'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Leave', 'url'=>array('index')),
);
?>

<h1 class="padding-left-15">Apply Leave</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'leave_days_dur'=>$leave_days_dur,'datearray'=>$datearray)); ?>