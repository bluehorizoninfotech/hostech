<?php
/* @var $this LeaveController */
/* @var $model Leave */

$this->breadcrumbs = array(
    'Leaves' => array('index'),
    $model->leave_id,
);

$this->menu=array(
	array('label'=>'List Leave', 'url'=>array('index')),
//	array('label'=>'Create Leave', 'url'=>array('create')),
//	array('label'=>'Update Leave', 'url'=>array('update', 'id'=>$model->leave_id)),
//	array('label'=>'Delete Leave', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->leave_id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage Leave', 'url'=>array('admin')),
);
?>
<div class="view-leave-sec">
<h1>Leave Details</h1>
<div class="page_devide">
<?php

    $userdata = Users::model()->findByPk($model->created_by); 
    
    $this->widget('zii.widgets.CDetailView', array(
         'htmlOptions' => array('class' => 'greytable'),
        'data' => $model,
        'attributes' => array(
                
            //'leave_id',
            'leave_submit_date',
            // array('name' => 'leave_type', 'value' => $model->leaveType->description),
            'date_from',
            'date_to',
            array('name' => 'Total Days', 'value' => LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$model->leave_id"
                    ))->leave_value,
            ),
            'reason_for_leave',
            'contact_details',
            'document_if_any',
            'person_covering_role',
            array('name' => 'approval_status', 'value' => (($model->approval_status == '') ? 'Pending' : $model->approvalStatus->caption)),
            //'decision_by',
            //'decision_date',
            array( 'name'=>'created_by',
                   'value'=> isset($userdata->first_name)?$userdata->first_name.' '.$userdata->last_name:null,
            ),
            'created_date',
            //'updated_by',
            'updated_date',
        ),
    ));
    ?>
</div>
<div class="page_devide" id="dateslist">
    <h4> Leave Day Details</h4>
    <table cellpadding="10" cellspacing="0" class="greytable">
        <thead>
            <tr>
                <th>Date</th>
                <th>Leave Duration</th>
            </tr>
        </thead>
        <tbody id="leavedates">
            <?php
            $tblpx = Yii::app()->db->tablePrefix;
            $sql = "select * from ". $tblpx."leave_day inner join ". $tblpx."legends on leg_id=leave_period where leave_id=".$model->leave_id;
            $rec = Yii::app()->db->createCommand($sql)->queryAll();
            foreach($rec as $row){
                echo "<tr><td>".date('d-m-Y',strtotime($row['leave_date']))."</td><td>".$row['description']."</td></tr>";                
            }
            ?>
        </tbody>
    </table>

<div class="pull-left width-100-percentage display-none" >
    <h4>Available Leave Summary</h4>
    <?php
     $tblpx = Yii::app()->db->tablePrefix;
    $avlleaves = Yii::app()->db->createCommand('select * from '.$tblpx.'avail_leave where userid='.Yii::app()->user->id)->queryAll();

    
      $consi_leave=0;
    $utilised = 0; 
    $remain=0;
    $actual_el=0;

    if(count($avlleaves)){
    extract($avlleaves[0]);
    }

      
    $data =  Yii::app()->db->createCommand("SELECT count(*) as count FROM `pms_leave` WHERE `emp_id`=".Yii::app()->user->id." and `leave_type`= 20 and (`approval_status`= 9 or `approval_status`= 11)")->queryRow();
        if($data['count'] > 0){
            $utilised =$data['count']; 
        }else{
             $utilised = 0; 
        }

        $remain = $consi_leave - $utilised;

        
        if($remain<0){
            
            $remain =0;
        }
    ?>
    
    <table class="greytable">
        <thead><tr><th></th><th>Accrued</th><th>Balance</th></tr></thead>
        <tbody>
         <tr><th>Earned Leave: </th><td><?= $actual_el ?></td>
            <td><?= $remain; ?> </td></tr>
        </tbody>
    </table> 
    
</div>
    
    
</div>




<div>
    <!--                <div class="subrow12 subrow233px" style="float: left;">-->
    <div class="col-md-4">
        <h3>Personal Details</h3>
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $usermodel,
            'htmlOptions'=>array('class'=>'greytable'),
            'attributes' => array(
                'employee_id',
                'first_name',
                'last_name',
                // array(
                //     'label' => 'Gender',
                //     'value' => $model->gender($usermodel->gender),
                // ),
                // array(
                //         'name'=>'marital_status',
                //         'value'=> $model->maritalstatus($usermodel->marital_status),
                //     ),
                'email',

                
               // 'system_access_types',
            ),
        ));
        ?>
    </div>
    
    </div>
    </div>
    
   
