<?php
/* @var $this LeaveController */
/* @var $data Leave */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('leave_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->leave_id), array('view', 'id'=>$data->leave_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leave_submit_date')); ?>:</b>
	<?php echo CHtml::encode($data->leave_submit_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leave_type')); ?>:</b>
	<?php echo CHtml::encode($data->leave_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_from')); ?>:</b>
	<?php echo CHtml::encode($data->date_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_to')); ?>:</b>
	<?php echo CHtml::encode($data->date_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason_for_leave')); ?>:</b>
	<?php echo CHtml::encode($data->reason_for_leave); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_details')); ?>:</b>
	<?php echo CHtml::encode($data->contact_details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('document_if_any')); ?>:</b>
	<?php echo CHtml::encode($data->document_if_any); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('person_covering_role')); ?>:</b>
	<?php echo CHtml::encode($data->person_covering_role); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approval_status')); ?>:</b>
	<?php echo CHtml::encode($data->approval_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decision_by')); ?>:</b>
	<?php echo CHtml::encode($data->decision_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decision_date')); ?>:</b>
	<?php echo CHtml::encode($data->decision_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>