<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<!-- <div class="row">
  <div class="col-md-4"> -->
    <div class="leave-compoff-sec">
    <div class="clearfix">
        <h1 class="pull-left">Availed Compensatory Off's</h1>
        <button class="btn btn-primary pull-right" data-toggle="collapse" data-target="#compoff" aria-expanded="false">Apply Compensatory Off</button>
    </div>
        <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
            }
        ?>
    <div class="panel panel-primary panel-blue collapse" id="compoff">
        <div class="panel-heading">
            <h2 class="panel-title">Apply Compensatory Off</h2>
        </div>
        <!-- <h1 style="color:red">( Under maintenance.Please try after some time)</h1> -->
        <div class="panel-body">
            <?php
                $form = $this->beginWidget('CActiveForm', array(
                'id' => 'leavecomp-form',
                'enableAjaxValidation' => true,
                'clientOptions' => array(
                'validateOnSubmit' => false,
                'validateOnChange' => false,
                'validateOnType' => false,
                ),
                ));
            ?>
                <div class="page_area">
                    <div class="row" >
                        <div  class="col-md-2" >
                            <?php echo $form->labelEx($model, 'date'); ?>
                            <?php

                            if($model->date==''){
                            $model->date = date('d-m-Y', strtotime(' -1 day'));
                            }
                            else{
                            $model->date = date('d-m-Y', strtotime($model->date));
                            }
                            echo CHtml::activeTextField($model, 'date', array("id" =>"date",
                            "size" => "15",
                            'class' => 'date form-control width-150 comoff-filed',
                            'autocomplete'=>'off'));

                            /*$this->widget('application.extensions.calendar.SCalendar', array(
                            'inputField' => 'date',
                            'ifFormat' => '%d-%m-%Y',
                            ));*/
                            ?>

                            <?php echo $form->error($model, 'date'); ?>
                        </div>

                        <div  class="col-md-3">
                            <?php echo $form->labelEx($model, 'type'); ?>
                            <select name="Availcompoff[type]" class="form-control comoff-filed" >
                            <option value="">Please select</option>
                            <option value="1">Full day</option>
                            <!-- <option value="0.5">Half day</option> -->
                            </select>
                            <?php echo $form->error($model, 'type'); ?>
                        </div>                    
                        <div class="col-md-5">
                            <?php echo $form->labelEx($model, 'comment'); ?>
                            <?php echo $form->textArea($model, 'comment', array('class'=>'form-control width-100-percentage' ,'rows' => 5, 'cols' => 30)); ?>
                            <?php echo $form->error($model, 'comment'); ?>
                        </div>
                        <div class="col-md-2">
                        <label>&nbsp;</label>
                            <?php echo CHtml::submitButton($model->isNewRecord ? 'Apply' : 'Save',array('class'=>'btn btn-primary')); ?>
                            <?php echo CHtml::resetButton('Cancel',array('class'=>'btn btn-default')); ?>
                        </div>
                    </div>                    
            <?php $this->endWidget(); ?>
        </div>
    </div>
    </div>
    </div>

    <!-- <div class="col-md-8"> -->




    <?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'availcompoff-grid',
    'itemsCssClass' => 'greytable table table-bordered',
    'template' => '<div class="table-responsive">{items}</div>',
    
    'dataProvider'=>$model1->search(),
    'filter'=>$model1,
    //  'showRecordsPerPage' => Yii::app()->user->getState('availcompoff'),
    'columns'=>array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.'),

       /* 'id',*/

         array(
            'name'=>'userid',
            'value'=>'$data->userid1->first_name ." ".$data->userid1->last_name',
            ),
        array(
                'name'=>'date',
                'value'=>'date("d-m-Y",strtotime($data->date))'
        ),
        array(
            'name'=>'comment',
            'value'=>'$data->comment',
            'htmlOptions'=>array('class'=>'comment')
        ),       
        array(
            'name'=>'type',
             'value'=>'($data->type==1)?"Full day":"Half Day" ',
            ),

         array(
            'name'=>'decision_coment',
             'value'=>'$data->decision_coment!=""?$data->decision_coment:"--" ',

            ),
        

        array(
            'name'=>'created_date',
            'value'=>'date("d-m-Y",strtotime($data->created_date))'
        ),

        array(

            'name'=>'status',
            'value'=>'$data->getstatus($data->status)',

        ),
        /*
        'created_by',

        */
       /* array(
                'visible'=>'Yii::app()->user->role==1',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => '100px', 'style' => 'font-weight: bold;text-align:center','class'=>'indexactionbtncolmn'),
                //'template' => '{update}',
                'buttons' => array(
                    'update' => array
                        (
                        'url' => 'Yii::app()->createUrl("/Availcompoff/create", array("id"=>$data->id))',
                    ),
                ),
            ),*/
    ),
)); ?>

</div>

    <!-- </div>
</div> -->

<script type="text/javascript">
$(function () {

    $('.date').datepicker({ format: 'dd-mm-yyyy', autoclose: true, endDate: '-1d',dateFormat:'dd-mm-yy'});
});

</script>


