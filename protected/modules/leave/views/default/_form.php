<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<?php
/* @var $this LeaveController */
/* @var $model Leave */
/* @var $form CActiveForm */
?>

<?php
$items = $leave_days_dur;


$form = $this->beginWidget('CActiveForm', array(
    'id' => 'leave-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => false,
        'validateOnChange' => false,
        'validateOnType' => false,
       
        ),


        ));


?>
<div class="leave-default-sec">
<div class="row">
<div class="col-md-6">
<div class="page_devide-removed">
    <div class="form leave_form">

        <?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="info-flash margin-left-16 text-align-center
    font-16
    border">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
      <?php endif; ?>
        <!-- <div class="row">
        <div class="col-md-6">
            <?php //echo $form->labelEx($model, 'leave_submit_date'); ?>
            <?php //echo date('d/m/Y H:i:s'); ?>
            <?php
            //echo $form->error($model, 'leave_submit_date');
            ?>
            </div>
        </div> -->

         <?php
           $user = Users::model()->findByPk( Yii::app()->user->id );
           $userreport = Users::model()->findAll( 'report_to = '.Yii::app()->user->id );
           
        //    if($user->shift_supervisor==1 or !empty( $userreport)){
        ?>

        <div class="row">
            <div class="col-md-6">
            <?php //echo $form->labelEx($model, 'emp_id'); ?>

            <label> Employee <i class ="error">*</i></label>

            <?php

            $company ='';
        
            $sdate = $edate = date('Y-m-d');

            
            if($user->user_type==1){
            $details =  Yii::app()->db->createCommand("SELECT concat_ws(' ',first_name,last_name) as fullname,userid FROM pms_users WHERE   userid!=1   order by fullname asc")->queryAll();

            }
            else{
                $details =  Yii::app()->db->createCommand("SELECT concat_ws(' ',first_name,last_name) as fullname,userid FROM pms_users WHERE   userid!=1 and userid=".$user->userid."   order by fullname asc")->queryAll();
  
            }

           $allusers = CHtml::listData($details,'userid','fullname');

        //    print_r($allusers); exit;

            $k=1;
            foreach($allusers as $key=>$dat){
                $allusers[$key] = $dat;
                $k++;
            }


            $model->emp_id = Yii::app()->user->id;

            echo $form->dropDownList($model, 'emp_id', $allusers, array('empty' => 'Select  Employee', 'class'=>'form-control',
                'id' => 'userall'));
            ?>
            <?php echo $form->error($model, 'emp_id'); ?>
      
        <i><small>Total Employees:<?php  echo count($allusers);?></small></i>
         
        
        </div>

        <!-- <div class="col-md-6">
            <?php echo $form->labelEx($model, 'leave_type'); ?>
            <?php


            $leave_model = Yii::app()->db->createCommand(" SELECT leave_id,description FROM `pms_leave_types` as lt inner join pms_legends as lg on lg.leg_id= lt.`leave_id` ")->queryAll();

            $leave_type = CHtml::listData($leave_model,'leave_id','description');

       

            echo $form->dropDownList($model, 'leave_type', $leave_type, array('empty' => 'Select Leave type', 'class'=>'form-control',
                'id' => 'leave_type'));
            ?>
            <?php echo $form->error($model, 'leave_type'); ?>
        </div> -->
        </div>

        <div class="row"  id="from_Date">
            <div class="col-md-6">
            <?php echo $form->labelEx($model, 'date_from'); ?>
            <?php
            echo CHtml::activeTextField($model, 'date_from', array("id" =>"date_from",
               
                'class' => 'date_from form-control',
                'autocomplete'=>'off',
                'value'=> ($model->date_from!="")? date("d/m/Y",strtotime($model->date_from)):""
              ));

            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'date_from',
                'ifFormat' => '%d/%m/%Y',
            ));
        ?>

            <?php echo $form->error($model, 'date_from'); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'date_to'); ?>
            <?php
            echo CHtml::activeTextField($model, 'date_to', array("id" => "date_to",
               
                'class' => 'date_to form-control',
                'autocomplete'=>'off',
                'value'=> ($model->date_to!="")? date("d/m/Y",strtotime($model->date_to)):""
                ));

            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'date_to',
                'ifFormat' => '%d/%m/%Y',
            ));
            ?>
            <?php echo $form->error($model, 'date_to'); ?>

        </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            <small><span id='leavesand' class="red-color">
              <?php if(Yii::app()->user->hasFlash('error')):?>
                <div class="info-flash margin-left-16">
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
              <?php endif; ?>

            </span></small>
        
            <b>No of days: </b><span id="totdays"><?php echo (isset($items['result']['days']) ? floatval($items['result']['days']) : 0) ?></span>
            <input type="hidden" id="calcdys" name="Leave[calcday]" value=<?php echo (isset($items['result']['days']) ? floatval($items['result']['days']) : 0) ?>>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php echo $form->labelEx($model, 'reason_for_leave'); ?>
                <?php echo $form->textArea($model, 'reason_for_leave', array('class'=>'form-control' )); ?>
                <?php echo $form->error($model, 'reason_for_leave'); ?>
            </div>
        </div>
       

        <div class="row">
            <div class="col-md-6">
                <?php echo $form->labelEx($model, 'contact_details'); ?>
                <?php echo $form->textField($model, 'contact_details', array('size' => 25, 'maxlength' => 25 ,'class'=>'form-control')); ?>            
                <?php echo $form->error($model, 'contact_details'); ?>
            </div>

            <div class="col-md-6">
                <?php echo $form->labelEx($model, 'person_covering_role'); ?>
                <?php echo $form->textField($model, 'person_covering_role', array('size' => 40, 'maxlength' => 50,'class'=>'form-control')); ?>
                <?php echo $form->error($model, 'person_covering_role'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <small>In Case To Contact you during Leave Period, Please mention your contact details</small>
            </div>
        </div>
        <div class="row buttons">
            <div class="col-md-6">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Apply' : 'Save' ,array('class'=>'btn btn-primary')); ?>
            </div>
        </div>

              </div>

    
</div>
</div>
<div class="col-md-6">
<div class="page_devide-removed" id="dateslist">

<div class="row table" >
        <div class="col-md-12">
        <!-- <h4>Available Leave Summary</h4>
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        $avlleaves = Yii::app()->db->createCommand('select * from '.$tblpx.'avail_leave where userid=' . Yii::app()->user->id)->queryAll();
        $consi_leave=0;
        $utilised = 0;
        $remain=0;
        $actual_el=0;
        if(count($avlleaves)){
        extract($avlleaves[0]);
        }
        $data =  Yii::app()->db->createCommand("SELECT count(*) as count FROM `pms_leave` WHERE `emp_id`=".Yii::app()->user->id." and `leave_type`= 2 and (`approval_status`= 9 or `approval_status`= 11) ")->queryRow();
        if($data['count'] > 0){
            $utilised =$data['count'];
        }else{
             $utilised = 0;
        }

        $remain = $consi_leave - $utilised;

        if($remain< 0){

            $remain =0;
        }

    ?>

         <table class="greytable">
            <thead><tr><th></th><th>Accrued</th><th>Balance</th></tr></thead>
            <tbody>
                <tr><th>Earned Leave: </th><td><?= $actual_el ?></td>
            <td><?= $remain; ?> </td></tr>
            </tbody>
        </table> --> 

 

    <!--  new total present and leave  -->

    <!-- <div style="float:left;width:100%;" >
        <h4></h4>

        <?php


        $prevyear = date("Y",strtotime("-1 year"));
        $start_date =  date('Y-m-01');
        $end_date   =  date('Y-m-t');


        $present = $this->getpresentdays(Yii::app()->user->id, $start_date,$end_date);
        $absent  = $this->getabsentdays(Yii::app()->user->id, $start_date,$end_date);

        $st_date= date('d-m-Y',strtotime($start_date));
        $en_date= date('d-m-Y',strtotime($end_date));


        ?>


        <table class="greytable">
            <thead>
             <tr><th colspan="2">

            <div class="form-inline bottom_space">
                <input type="text" name="date_from1" id="date_from1" class="form-control" value="<?= $st_date ?>">
                <input type="text" name="date_to1" id="date_to1" class="form-control" value="<?= $en_date ?>">
                <span id="errmsg" style="color: red;"></span>
               <input type="button"  class="btn btn-sm btn-primary" name="submit" value="Submit" id="submit">
            </div>

             </th></tr>

             <tr><th style="color: #000;">Total Present</th><th style="color: #000;">Total Leave</th></tr>
            </thead>
            <tbody>
                <tr>

                    <td id="pr"><?=  $present ?></td>
                    <td id="abs"><?=  $absent ?></td>
                </tr>
            </tbody>
        </table>

    </div> -->

    


    <div class="addtable">
    <h4 class="title">Available Leave ( <span id="hed" class="font-weight-italic"></span> ) </h4>
        <table class="greytable">
            <thead><tr><th></th><th>Accrued</th><th>Balance</th></tr></thead>
            <tbody>

            <tr>
                <th>Casual Leave </th>
                <td id="ear"></td>
                <td id="rem"></td>
            </tr>
            <!-- <tr>
                <th>Medical Leave </th>
                <td id="medear"></td>
                <td id="medrem"></td>
            </tr> -->
            <tr>
                <th>Compensatory Leave </th>
                <td id="lopear"></td>
                <td id="loprem"></td>
            </tr>

        </table>
    <h4 class="title"> Leave days Details</h4>
    <table cellpadding="10" cellspacing="0" class="greytable" style="">
        <thead>
            <tr>
                <th>Date</th>
                <th>Leave Duration</th>
            </tr>
        </thead>
        <tbody id="leavedates">
            <?php
            if (isset($items['result']['result']) && $items['result']['result'] != '') {
                echo $items['result']['result'];
            } else {
                ?>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    
    

    </div>


    <!--  new total present  -->

    <!-- <div class="addtable" style="float:left;width:100%;display: none; ">
        <h4></h4>
        <table class="greytable">
            <thead>
            <tr><th colspan="2">
            <div class="form-inline bottom_space" style="float:left;width:100%;display: none;">
                <input type="text" name="date_from2" id="date_from2" class="form-control" value="<?= $st_date ?>">
                <input type="text" name="date_to2" id="date_to2" class="form-control" value="<?= $en_date ?>">
                <span id="errmsg" style="color: red;"></span>
               <input type="button"  class="btn btn-sm btn-primary" name="submit2" value="Submit" id="submit2">
            </div>
            </th></tr>
            <tr><th style="color: #000;">Total Present</th><th style="color: #000;">Total Leave</th></tr>
            </thead>
            <tbody>
                <tr>

                    <td id="prese"></td>
                    <td id="abse"></td>
               </tr>
            </tbody>
        </table>

    </div> -->
    </div>
    </div>
    <!--  end  -->

    <!--  end  -->

</div>
<br clear="all" />
<?php
//echo '<pre>';
//print_r($items);
//print_r($datearray);
//echo '<pre>';
?>
<?php $this->endWidget(); ?>
    </div></div>
        </div>
<script type="text/javascript">
    $(function () {


		 $('input[type=text], textarea').keyup(function () {

				$(this).val($(this).val().toUpperCase());

		 })

       // $('.info-flash').hide(12000);

    /* datepicker */

    $( "#date_from1" ).datepicker({
        dateFormat: 'dd-mm-yy',
    });
    $( "#date_to1" ).datepicker({
        dateFormat: 'dd-mm-yy',
    });


    $( "#date_from2" ).datepicker({
        dateFormat: 'dd-mm-yy',
    });
    $( "#date_to2" ).datepicker({
        dateFormat: 'dd-mm-yy',
    });

    /* end */
    $('#date_from').blur(function(){
        calculatedate();

    })
    $('#date_to').blur(function(){
        calculatedate();
    })

        function calculatedate(){
            var from = $('#date_from').val();
            var to = $('#date_to').val();
            var user=$( "#userall").val();

            $('#Leave_date_from_em_').hide();
            $('#Leave_date_to_em_').hide();
            var flag = 0;
            if (from == '') {
                flag = 1;
                $('#Leave_date_from_em_').html('Please enter From date');
                $('#Leave_date_from_em_').show();
            }
            if (to == '') {
                flag = 1;
                $('#Leave_date_to_em_').html('Please enter To date');
                $('#Leave_date_to_em_').show();

            }

            if (flag === 0) {
                $.ajax({
                    method: 'get',
                    data: {from: from, to: to,user:user},
                    url: "<?php echo Yii::app()->createUrl('leave/default/calcdays') ?>",
                    dataType: 'json'
                 }).done(function (msg) {

                    $('#leavesand').html(msg.sanderror);
                    $('#leavedates').html(msg.result);
                    $('#totdays').html(msg.days);
                    $('#calcdys').val(msg.days);
                    $("#date_from").val(msg.from_date);

                });
            }
        };


        $( "#userall").change(function() {

                $('#hed').html("");
                $('#ear').html("");
                $('#rem').html("");
                $('#medear').html("");
                $('#medrem').html("");
                $('#lopear').html("");
                $('#loprem').html("");
                var from='';
                var to='';

            $('.addtable').show();
            $('.bottom_space').show();

            var id = $(this).val();
            if(id==''){
             $('.addtable').hide();
            }
            $.ajax({
                    method: 'post',
                    data: {id: id ,from:from, to:to},
                    url: "<?php echo Yii::app()->createUrl('leave/default/getleavesummary') ?>",
                    type: 'json',
            }).done(function (msg) {
                
                var obj = JSON.parse(msg);
                $('#hed').append(obj.name);
                $('#ear').html(obj.ear);
                $('#rem').html(obj.rem);
                $('#prese').html(obj.prese);
                $('#abse').html(obj.abse);
                $('#medear').html(obj.medical_leave);
                $('#medrem').html(obj.remain_medical_leave);
                $('#lopear').html(obj.casual_leave);
                $('#loprem').html(obj.remain_casual_leave);

            });

       });

       $(document).ready(function(){

        $( "#userall").change();
        var from = $('#date_from').val();
        var to = $('#date_to').val();
        if(from!=""&&to!=""){
         calculatedate();
        }
       })

        $( "#submit2").click(function() {

                $('#hed').html("");
                $('#ear').html("");
                $('#rem').html("");


            var from = $('#date_from2').val();
            var to = $('#date_to2').val();

            $('.addtable').show();
            $('.bottom_space').show();

            var id =  $('#userall').val();
            if(id==''){
             $('.addtable').hide();
            }
            $.ajax({
                    method: 'post',
                    data: {id: id ,from:from ,to:to},
                    url: "<?php echo Yii::app()->createUrl('leave/default/getleavesummary') ?>",
                    type: 'json',
            }).done(function (msg) {
                var obj = JSON.parse(msg);
                $('#hed').append(obj.name);
                $('#ear').html(obj.ear);
                $('#rem').html(obj.rem);
                $('#prese').html(obj.prese);
                $('#abse').html(obj.abse);





            });

       });



        /* date submit */

        $('#submit').click(function () {

            var from = $('#date_from1').val();
            var to = $('#date_to1').val();
            var id = $('#userall').val();



            if (from!='' && to!='') {

                  $('#errmsg').html('');
                $.ajax({
                    method: 'post',
                    data: {from: from, to: to, userid:id },
                    url: "<?php echo Yii::app()->createUrl('leave/default/attendancebydate') ?>",
                    dataType: 'json'
                 }).done(function (msg) {


                    $('#pr').html(msg.prese);
                    $('#abs').html(msg.abse);


                });
            }else{

              $('#errmsg').html('Please select dates');

            }
        });




    });

</script>

