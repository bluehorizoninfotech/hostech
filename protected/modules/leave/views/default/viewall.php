<div class="view-all-leave-sec">
<div class="panel panel-primary">
    <div class="panel-heading clearfix">
        <h3 class="panel-title pull-left">Details</h3>
        <div class="pull-right">
            Status: <?php echo ($model->status!=1) ? '<span class="status_text green-color font-weight-bold">'. $model->getleavestatus($model->status, $model->userid,$model->date).'</span>': '<span class="status_text" class="red-color font-weight-bold">'.$model->getstatus($model->status).'</span>' ?>
            <a class="closebtn" onclick="closeaction('popup_view')">X</a>
        </div>
    </div>
    <div class="panel-body">
        
        <div class="detail_block">
            <div class="row">
                <div class="col-md-6">
                    <div class="item_elem">Name: <b><?php echo $usermodel->first_name; ?> <?php echo $usermodel->last_name; ?></b> </div>
                </div>
                <div class="col-md-6 text-right">System Access: <b><?php echo $usermodel->system_access_types; ?></b></div>
            </div>
       

<div class="row">
    <div class="col-md-7" >

 <h3>Lieu Day Details(Comp Off)</h3>
    <?php
    $this->widget('zii.widgets.CDetailView', array(
         'htmlOptions' => array('class' => 'greytable'),
        'data' => $model,
        'attributes' => array(
                
            //'leave_id',
            

            /*array(

                'name'=>'Leave type',
                'label'=>'Type',
                'value'=>'Lieu Leave(Compensatory off)',
                ),*/
            array(

                'name'=>'Compensatory date',
                'type'=>'raw',
                'value'=> Yii::app()->dateFormatter->format("d-M-y", $model->date),
                'htmlOptions'=>array('class'=>'white-space-nowrap'),
            ),



            array(
                'name' => 'type', 
                'label'=>'Duration',
                'value' => ($model->type==1?'Full day':'Half day'),
            ),

//            'comment',
          
            array(

                'name'=>'created_date',
                'value'=>Yii::app()->dateFormatter->format("d-M-y",$model->created_date),
            ),
            
            array(
                'name'=>'updated_by',
                'value'=>($model->updated_by!='')?$model->updatedBy->first_name ." ".$model->updatedBy->last_name:'Not Set',

            ),
            'decision_coment',

            /* array(

                'name'=>'status',
                'type'=>'raw',
                'value'=> ($model->status!=1)?'<span style="color:green;font-weight: bold;">'.$model->getstatus($model->status, $model->userid,$model->date).'</span>':'<span  style="color:red;font-weight: bold;">'.$model->getstatus($model->status).'</span>',
              

            ),*/

        ),
    ));
    ?>
</div>

<!--   <div class="col-md-6">
        <h3>Personal Details</h3>
        <?php
       /* $this->widget('zii.widgets.CDetailView', array(
            'data' => $usermodel,
            'htmlOptions'=>array('class'=>'greytable'),
            'attributes' => array(
                'employee_id',
                'first_name',
                'last_name',
                [
                    'label' => 'Gender',
                    'value' => ($usermodel->gender == 'M') ? 'Male' : 'Female'
                ],
                
                
                'system_access_types',
            ),
        ));*/
        ?>
    </div>-->

       

      <div class="col-md-5">

          <h3>Punching Details</h3>

        
           <table class="greytable">
             
                <tbody>
                  <?php if(!empty($presult)) {  ?>

                    <tr>
                        <th>In time</th>
                        <td><?= ($presult['in']!=0)?gmdate('H:i:s',$presult['in']):0; ?></td>
                    </tr>
                    <tr>
                        <th>Out time</th>
                        <td><?= ($presult['out']!=0)?gmdate('H:i:s',$presult['out']):0; ?></td>
                    </tr>

                    
                    <?php } else{ ?>

                    <tr> No Punch </tr>
                    <?php } ?>
                  
                </tbody>
               
           </table>
          
            <h3>Time Entry Details</h3>
            <table class="greytable">
                <tbody>
                    <tr>
                        <th>Billable Time (Hrs)</th>
                        <td><?= $billable; ?></td>
                    </tr>
                    <tr>
                        <th>Non-billable Time (Hrs)</th>
                        <td><?= $nobill; ?></td>
                    </tr>
                </tbody>
               
           </table>
           </div>
        </div>
        </div>
    </div>
</div>
                    </div>

