<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<?php
/* @var $this LeaveController */
/* @var $model Leave */

$this->breadcrumbs = array(
    'Leaves' => array('index'),
    $model->leave_id,
);

$this->menu=array(
	array('label'=>'Manage Leaves', 'url'=>array('/leave/default/manageleaves')),
//	array('label'=>'Create Leave', 'url'=>array('create')),
//	array('label'=>'Update Leave', 'url'=>array('update', 'id'=>$model->leave_id)),
//	array('label'=>'Delete Leave', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->leave_id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage Leave', 'url'=>array('admin')),
);
?>
<script type='text/javascript'>

$(document).ready(function(){

    $('.leave_action').click(function(){



        var req = $(this).val();
        var id=<?php echo $model->leave_id?>;

        if(!confirm('Are you sure you want to '+$(this).val()+' this Request?')===false){
            $('.loaderdiv').show();
            $.ajax({
                method: "POST",
                dataType:"json",
                url:'<?php echo Yii::app()->createUrl('/leave/default/leaveaction')?>',
                data:{id:id,req:req}
            }).done(function(ret){
                if(ret.error!=''){
                    $('#actionmsg').addClass('erroraction');
                    $('#actionmsg').html(ret.error);
                }
                else{
                    $('.loaderdiv').hide();
                    $('#actionmsg').addClass('successaction');
                    $('#actionmsg').html(ret.msg);
                    $('#gridstatus').html(ret.status);
                    $('#topstatus').html(ret.status);
                    if(ret.status=='Approved'){
                        $('#topstatus').attr('color','green');
                    }
                    $('.leave_action').hide();

                }
            });
        }
    });
});
</script>
<div class="viewleave-leave-sec">
<div class='ldh1'><span>Leave Details</span></div>
<div class='ldh1,divld1'>
    <span>Status: <font color='<?php echo ($model->approvalStatus->caption=='Approved'?'green':'red') ?>' id='topstatus'><?php echo (($model->approval_status == '') ? 'Pending' : $model->approvalStatus->caption) ?></font></span>

    <?php

    //   $shiftsupervisor= Yii::app()->db->createCommand("SELECT shift_supervisor FROM `pms_users` WHERE userid =" . Yii::app()->user->id )->queryRow();

    //   $leavesupervisor= Yii::app()->db->createCommand("SELECT shift_supervisor FROM `pms_users` WHERE userid =".$model->emp_id )->queryRow();

      $sts =0;
      $user_details=Users::model()->findByPk(Yii::app()->user->id);
      $total_leaves=LeaveDay::model()->find(
        array(
            "select" => array("sum(leave_value) as leave_value"),
            "condition" => "leave_id=$model->leave_id"
        ));
    
    $total_leave_count=$total_leaves['leave_value'];

      if($user_details['user_type']==1){
          $sts = 1;
      }
     
    
    ?>
    <?php if($model->approval_status==11 && $sts==1){ ?>
        &nbsp; &nbsp; &nbsp; <input type="button" name='approve' class='btn btn-success leave_action' value='Approve' /> &nbsp; &nbsp;
        <input type="button" class='btn btn-danger leave_action' name='reject' value='Reject' /> &nbsp; &nbsp;
     <?php } ?>
     <?php if($model->approval_status==11 && $model->refer_to_hr==0 && $sts==0){ ?>

         <input type="button" class='btn btn-success leave_action' name='refer_to_hr' value='Refer To HR' /> &nbsp; &nbsp;

         <input type="button" class='btn btn-danger leave_action' name='reject' value='Reject' /> &nbsp; &nbsp;
      <?php } ?>

      <?php if($model->approval_status==11 && $user_details['user_type']==6 &&  $total_leave_count>2 && $model->refer_to_admin!=1){ ?>

        <input type="button" class='btn btn-success leave_action' name='refer_to_admin' value='Refer To Admin' /> &nbsp; &nbsp;
        <input type="button" class='btn btn-danger leave_action' name='reject' value='Reject' /> &nbsp; &nbsp;
        
      <?php } ?>

      <?php if($model->approval_status==11 && $user_details['user_type']==6 &&  $total_leave_count <=2 &&$model->refer_to_admin!=1){ ?>

        <input type="button" name='approve' class='btn btn-success leave_action' value='Approve' /> &nbsp; &nbsp;
        <input type="button" class='btn btn-danger leave_action' name='reject' value='Reject' /> &nbsp; &nbsp;

    <?php } ?>


    <input type="button" class="btn btn-primary" name='close' value='Close'  onclick="window.location.href=('<?php echo Yii::app()->createUrl('/leave/default/manageleaves') ?>');" />

</div>
        <div class="loaderdiv margin-top-0 margin-bottom-0 margin-left-auto margin-right-auto width-64 display-none">
            <img id="loader" src="images/loading.gif" width="50%"/>
        </div>
<br clear='all'/>

<div id='actionmsg'></div>

<br clear='all'/>
<div class="page_devide">
<?php

    $userdata = Users::model()->findByPk($model->created_by);


    $this->widget('zii.widgets.CDetailView', array(
         'htmlOptions' => array('class' => 'greytable'),
        'data' => $model,
        'attributes' => array(

            //'leave_id',
            'leave_submit_date',
            // array('name' => 'leave_type', 'value' => $model->leaveType->description),
            'date_from',
            'date_to',
            array('name' => 'Total Days', 'value' => LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$model->leave_id"
                    ))->leave_value,
            ),
            'reason_for_leave',
            'contact_details',
            'document_if_any',
            'person_covering_role',
            array('name' => 'approval_status','type'=>'raw', 'value' => (($model->approval_status == '') ? '' : "<span id='gridstatus'>".$model->approvalStatus->caption."</span>")),
           array(
            'name'=> 'decision_by',
            'value'=>$model->getdecisionby($model->decision_by),
            ),
            //'decision_date',
            array( 'name'=>'created_by',
                    'value'=>isset($userdata->firstname)?$userdata->firstname.' '.$userdata->lastname:null,
            ),
            'created_date',
            //'updated_by',
            'updated_date',
        ),
    ));
    ?>
</div>
<div class="page_devide" id="dateslist">
    <h4>Day Leave Details</h4>
    <table cellpadding="10" cellspacing="0" class="greytable">
        <thead>
            <tr>
                <th>Date</th>
                <th>Leave Duration</th>
            </tr>
        </thead>
        <tbody id="leavedates">
            <?php
            $sql = "select * from pms_leave_day inner join pms_legends on leg_id=leave_period where leave_id=".$model->leave_id;
            $rec = Yii::app()->db->createCommand($sql)->queryAll();
            foreach($rec as $row){
                echo "<tr><td>".$row['leave_date']."</td><td>".$row['description']."</td></tr>";
            }
            ?>
        </tbody>
    </table>

    <input type="hidden" value="<?= $model->emp_id ?>" id="leav_emp">

<div class="pull-left width-100-percentage display-none" >
    <h4>Available Leave Summary</h4>

    <?php
     $tblpx = Yii::app()->db->tablePrefix;
    $avlleaves = Yii::app()->db->createCommand('select * from '.$tblpx.'avail_leave where userid=' . $usermodel->userid)->queryAll();

 /*  echo '<pre>';
   print_r($avlleaves);
   echo '</pre>'; */


    $consi_leave=0;
    $utilised = 0;
    $remain=0;
    $actual_el=0;

    if(count($avlleaves)){
    extract($avlleaves[0]);
    }

      if(!empty($model->emp_id)){
            $userid = $model->emp_id;
        }else{
            $userid =0;
        }

     $data =  Yii::app()->db->createCommand("SELECT count(*) as count FROM `pms_leave` WHERE `emp_id`=".$userid." and `leave_type`= 2 and (`approval_status`= 9 or `approval_status`= 11) ")->queryRow();
        if($data['count'] > 0){
            $utilised =$data['count'];
        }else{
             $utilised = 0;
        }

        $remain = $consi_leave - $utilised;

        if($remain<0){

            $remain =0;
        }

    ?>

    <table class="greytable">
        <thead><tr><th></th><th>Accrued</th><th>Balance</th></tr></thead>
        <tbody>
          <tr><th>Earned Leave: </th><td><?= $actual_el ?></td>
            <td><?= $remain; ?> </td></tr>

            </tbody>
    </table>

</div>

<!--  new total present and leave  -->

    <div class="pull-left width-100-percentage display-none" >
    <input type="hidden" id="userid" value=" <?= $model->emp_id?>">
        <h4></h4>

        <?php




        $prevyear = date("Y",strtotime("-1 year"));
        $start_date =  date("Y-m-01");
        $end_date   =  date('Y-m-t');

        if(!empty($model->emp_id)){
            $userid = $model->emp_id;
        }else{
            $userid =0;
        }

        $present = $this->getpresentdays($userid, $start_date,$end_date);
        $absent  = $this->getabsentdays($userid, $start_date,$end_date);

        if(empty($present )){
            $present=0;
        }
        if(empty($absent )){
            $absent=0;
        }

        ?>



        <table class="greytable">
            <thead>
            <tr><th colspan="2">

            <div class="form-inline bottom_space">
                <input type="text" name="date_from2" id="date_from2" class="form-control" value="<?= date('d-m-Y',strtotime($start_date))  ?>">
                <input type="text" name="date_to2" id="date_to2" class="form-control" value="<?= date('d-m-Y',strtotime($end_date)) ?>">
                <span id="errmsg" class="red-color"></span>
               <input type="button"  class="btn btn-sm btn-primary" name="submit2" value="Submit" id="submit2">
            </div>


            </th></tr>
            <tr><th class="black-color">Total Present</th><th class="black-color">Total Leave</th></tr></thead>
            <tbody>
                <tr>
                    <td id="pre"><?=  $present ?></td>
                   <td id="abs"><?=  $absent ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <!--  end  -->


</div>




<div>
    <!--                <div class="subrow12 subrow233px" style="float: left;">-->
    <div class="col-md-4">
        <h3>Personal Details</h3>
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $usermodel,
            'htmlOptions'=>array('class'=>'greytable'),
            'attributes' => array(
                'employee_code',
                'first_name',
                'last_name',
                // array(
                //     'label' => 'Gender',
                //     'value' => $model->gender($usermodel->gender),
                // ),
                // array(
                //         'name'=>'marital_status',
                //         'value'=> $model->maritalstatus($usermodel->marital_status),
                //     ),
                'email',
                
              
            ),
        ));
        ?>
    </div>

    </div>
    </div>
    <script type="text/javascript">

     $(function () {
       /* datepicker */


        $( "#date_from2" ).datepicker({
            dateFormat: 'dd-mm-yy',
        });
        $( "#date_to2" ).datepicker({
            dateFormat: 'dd-mm-yy',
        });


    });

        $( "#submit2").click(function() {

            var from = $('#date_from2').val();
            var to = $('#date_to2').val();
            var id = $('#userid').val();


            if(from!='' && to!='')
            {

                $('#errmsg').html('');


            $.ajax({
                    method: 'post',
                    data: {userid: id ,from:from ,to:to},
                    url: "<?php echo Yii::app()->createUrl('leave/default/attendancebydate') ?>",
                    type: 'json',
            }).done(function (msg) {

                var obj = JSON.parse(msg);
                $('#pre').html(obj.prese);
                $('#abs').html(obj.abse);


            });
        }else{
            $('#errmsg').html('Select dates');
        }

       });

        </script>

   
