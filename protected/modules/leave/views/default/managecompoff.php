
<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<div>
    <h1>Manage All Comp Off</h1>
      <!-- <h1 style="color:red">( Under maintenance.Please try after some time)</h1> -->
</div>

<div class="managecomoff-sec">
<div id='actionmsg width-440'></div>
<!-- <div class="clearfix"> -->
    <div class="form approve_form">
   
        <?php

           $sql = "SELECT count(*) as count FROM `pms_avail_compoff` as t inner JOIN pms_users as us ON us.userid=t.userid
                   where t.status=1";
           $arr = Yii::app()->db->createCommand($sql)->queryRow(); ?>

           <?php if($arr['count']!=0){  ?>

            <textarea name="reason" class="reason"></textarea>

            <input type="button" name='approve' class='compaction btn btn-success' value='Approve'/> &nbsp;
            <input type="button" class='compaction btn btn-danger' name='reject' value='Reject'/>
            <img id="loadernew" src="themes/advenser/images/loading.gif" class="width-30 margin-left-159 display-none" />
            <?php }  ?>




  </div>

  <!-- </div> -->



<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'availcompoff-grid',
        'itemsCssClass' => 'greytable table table-bordered',
        'template' => '<div class="table-responsive">{items}</div>',
        'ajaxUpdate' => false,
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    // 'showRecordsPerPage' => Yii::app()->user->getState('availcompoff'),
    'columns'=>array(

        array(
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
                // 'visible' => ($date->status == 1 ? true : false),
                'checkBoxHtmlOptions' => array(
                    'name' => 'ids[]',
                ),

             'cssClassExpression'=>'$data->status == 1 ? "" : "hidden_check" ',
            // 'cssClassExpression' => '( $data->status == 1 or ( Yii::app()->user->role == 1  ))? "" : "hiden" ',
                
        ),
        array('class' => 'IndexColumn', 'header' => 'Sl.No.' ),

        array(
            'name'=>'userid',
            'value'=>'$data->userid1->first_name ." ".$data->userid1->last_name',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid', 'concat(first_name," ",last_name) as first_name'),
                    'order' => 'first_name',
                   
                    'distinct' => true
                )
        ), "first_name", "first_name")


        ),
        array(
            'name'=>'date',
            'value'=>'Yii::app()->dateFormatter->format("d/M/y",strtotime($data->date))',
            'filterHtmlOptions'=>array('autocomplete'=>'off'),


        ),
        array(
            'name'=>'comment',
            'value'=>'$data->comment',
            'htmlOptions'=>array('class'=>'comment')
        ),
        // 'comment',
        array(
            'name'=>'type',
             'value'=>'($data->type==1)?"Full day":"Half day" ',
             'filter' => array('0.5'=>'Half day',1=>'Full day'),

        ),
         array(
            'name'=>'created_date',
            'value'=>'Yii::app()->dateFormatter->format("d/M/y",strtotime($data->created_date))',

            /*'filter'=> $this->widget('zii.widgets.jui.CJuiDatePicker', array(
              'model'=>$model,
              'attribute'=>'created_date',
              'htmlOptions' => array(
              'id' => 'Availcompoff_created_date'
              ),
              'options' => array(
              'showOn' => 'focus',
              'dateFormat' => 'dd-mm-yy',
               )
             ), true)*/
        ),


        array(

            'name'=>'status',
            'value'=> '$data->getleavestatus($data->status,$data->userid,$data->date)',
            'type'=>'raw',
            'filter' => array(1=>'Pending for approval',2=>'Approved',3=>'Rejected'),
            'htmlOptions' => array('class' => 'width-160'),

             // 'cssClassExpression'=>'$data->status == 1 ? "redcls" : "grncls" ',
            'cssClassExpression' =>'$data->status > 1 ? ( $data->status ==2 ? "appcls" : "rejcls" ) : "pencls" ',


        ),

        // array(
        //     'class' => 'CButtonColumn',
        //     //'template' => '{approve_reject}{view}',
        //     'template' => '{view}',
        //     'buttons' => array
        //         (
        //         /*'approve_reject' => array
        //          (
        //             'label' => 'Approve/Reject',
        //             'url' => 'Yii::app()->createUrl("/leave/default/viewcompof", array("id"=>$data->id))',
        //             'visible'=>'$data->status==1',
        //         ),*/

        //         'view' => array
        //         (
        //             'label' => 'View',
        //             //'url' => 'Yii::app()->createUrl("/leave/default/viewcompof", array("id"=>$data->id))',
        //             'url' => '$data->id',
        //             // 'visible'=> 'in_array("/leave/default/viewall", Yii::app()->session["pmspmsmenuauthlist"])',

        //         )
        //     ),
        // ),
    ),
)); ?>
<!-- <div class="popup_view" style="display:none;" id="view_content"></div> -->
</div>
<script>

     function closeaction(formname){
        $('.' +formname ).hide();
    };

   $(document).ready(function() {


    //$('.button-column').on('click',function(event){

    // $(document).delegate('.view','click',function(event){

    //     event.preventDefault();

    //     var id = $(this).attr("href");
    //     $('#'+id).closest('tr').after('<tr id="'+$('.view').attr('class')+'" class="expandviewarea" style="background-color: #fff;"><td colspan="8">Loading..............<td></tr>');
    //     $(this).attr('id',id);

    //         $.ajax({
    //             method: "POST",
    //             //dataType:"json",
    //             url:'<?php echo Yii::app()->createUrl("/leave/default/viewall")?>' ,
    //             data:{id:id}
    //         }).done(function(ret){
    //             $('.expandviewarea').remove();

    //             $('#'+id).closest('tr').after('<tr id="'+$('.view').attr('class')+'" class="expandviewarea" style="background-color: #fff;"><td colspan="8">'+ret+'<td></tr>');
    //         });

    // });

     $(document).delegate('.view','click',function(event){
        event.preventDefault();
        var id = $(this).attr("href");
        $(this).attr('id',id);

           $.ajax({
                method: "POST",
                url:'<?php echo Yii::app()->createUrl("/leave/default/viewall") ?>' ,
                data:{id:id}
            }).done(function(ret){
                $("#view_content").html(ret).show();
            });

    });
    //$(document).delegate('#availcompoff-grid tbody tr','click',function(event){
   $(document).delegate('#availcompoff-grid tbody tr td:not(:first-child)','click',function(event){

        event.preventDefault();
        // var id = $(this).find('.view').attr("href");
       // var id = $(this).find('.view').attr("href");

        var id = $(this).closest('tr').find('.view').attr("href");

        $(this).attr('id',id);

            $.ajax({
                method: "POST",
                url:'<?php echo Yii::app()->createUrl("/leave/default/viewall") ?>' ,
                data:{id:id}
            }).done(function(ret){
                $("#view_content").html(ret).show();
            });

    });

    /* neww */

        $('.hiden').find('input[name="ids[]"]').remove();

        $(".compaction").click(function(){

          $('.compaction').attr('disabled',true);


            $('#loadernew').show();
            var req = $(this).val();
            var reason = $('.reason').val();
            var all = [];

            $('input[name="ids[]"]:checked').each(function() {
                all.push(this.value);
            });

           /* if(req=='Reject'){

                $('.reason').show();
            }else{
                $('.reason').hide();
            }*/




    if(all!=''){


        if(req=='Reject' && reason==''){
            alert("please add comment");
        }else{

        if(!confirm('Are you sure you want to '+$(this).val()+' this Request?')===false){
            $.ajax({
                method: "GET",
                dataType:"json",
                url:'<?php echo Yii::app()->createUrl('/leave/default/compaction')?>',
                data:{id:all,req:req,reason:reason}
            }).done(function(ret){

                $('#loadernew').hide();

                $('.compaction').attr('disabled',false);

                if(ret.error!=''){
                    $('#actionmsg').addClass('alert alert-danger');
                    $('#actionmsg').html(ret.error);
                    window.setTimeout(function(){location.reload()},3000)
                }
                else{
                    $('#actionmsg').addClass('alert alert-success');
                    $('#actionmsg').html(ret.msg);
                    window.setTimeout(function(){location.reload()},3000)

                }

            });
          }else{
            $('#loadernew').hide();
            $('.compaction').attr('disabled',false);
          }
        }
    }else{

          alert('Please select Item');

    }




    });
    $("#availcompoff-grid input[name='Availcompoff[created_date]']").datepicker({dateFormat: 'yy-mm-dd'});
    $("#availcompoff-grid input[name='Availcompoff[date]']").datepicker({dateFormat: 'yy-mm-dd'});




    });

    $(document).on('click','#availcompoff-grid_c0_all',function() {
	var checked=this.checked;
	 $("input[name='ids[]']").each(function() {
         if(checked==true)
         {
          $(this).find("input[name='ids']").prop('checked', true);
          $(this).parent('span').addClass('checked');
         }
         else{
            $(this).find("input[name='ids']").prop('checked', false);
          $(this).parent('span').removeClass('checked');
         }
         
    });
});

</script>

<script>
$(document).ready(function(){


$("input[name='Leave[leave_submit_date]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$("input[name='Leave[date_from]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$("input[name='Leave[date_to]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$('.hidden_check').children().addClass('hidden_checkbox');
$('.hidden_checkbox').remove();

     if( $("table tbody input[type=checkbox]").length==0){
       $('table thead tr th:first-child span').hide();
       $('table thead tr th:first-child').hide();
       $('table thead tr td:first-child').hide();
       $('table tbody tr td:first-child').hide();
     }
    $('.approve_form').hide();    
    $("table input[type=checkbox]").click(function(){ 
        var count= $("table tbody input[type=checkbox]").length;
    var countchecked = $("table tbody input[type=checkbox]:checked").length;        
        if(countchecked >=1){
            $('.approve_form').slideDown();
        }
        else if(countchecked ==0){
            $('.approve_form').slideUp();
        }
        if(countchecked==count){          
            $('table thead tr th:first-child').find('span').addClass('checked');
        }else{
            $('table thead tr th:first-child').find('span').removeClass('checked');
        }
    });

    $("table thead  input[type=checkbox]").click(function(){
        $(this).parent('span').toggleClass('checked');
        if ($(this).parent('span').hasClass('checked')) {
            $('.approve_form').slideDown();
        }
        else {
            $('.approve_form').slideUp();
        }
    })   
})

</script>
