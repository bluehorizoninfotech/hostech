<script type="text/javascript" src="<?php  echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="leavecredit-leave-sec">
<h1><?php echo (isset($_POST['submitcredit']) ? "Leave credit summary to confirm" : "Update leave count") ?></h1>
<div class="row" >
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'leave-credit-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <?php  // echo (!isset($_POST['submitcredit']) ? CHtml::link('Export',array('/leave/default/Availexport'),array('style' => 'float:right;margin-bottom:10px;','class'=> 'btn btn-sm btn-primary')) : ''); ?>
    <?php echo (!isset($_POST['submitcredit']) ? CHtml::submitButton('Submit credits', array('class' => 'btn btn-sm btn-primary sub submitcredit', 'name' => 'submitcredit', 'style' => 'float:right;margin-bottom:10px;margin-right: 5px;')) : ''); ?>
    <?php echo (isset($_POST['submitcredit']) ? CHtml::link('Go back', array('/leave/default/leavecredit'), array('class' => 'btn btn-sm btn-warning sub float-left margin-bottom-10')) : ''); ?>

    <?php

        //echo CHtml::link('Show all credit logs', array('/leave/default/creditlog'), array('class' => 'btn btn-sm btn-warning sub', 'style' => 'float:left;margin-bottom:10px;margin-left:10px;'));

    ?>

    <?php echo CHtml::button('Confirm credits', array('class' => 'btn btn-sm btn-primary sub confirmcredit', 'name' => 'confirmcredit', 'style' => 'margin:0 auto;')); ?>
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '" style="float:right;width:40%;text-align:center">' . $message . "</div>\n";
    }
    ?>
    <div class="clearfix"></div>
    <div id="fixed-header-holder" class="container1 table-fix" >

        <table class="greytable" id="fixed-header">
            <thead>
                <tr class="tablehead"><th>SNo</th><th>Name</th>
                    <th class="max-width-50">Casual Leaves</th>
                    <th class="max-width-50">Medical Leaves</th>
                    <!-- <th>RH Leaves</th>-->
                    <th>Paid Leaves</th> 
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;

                foreach ($availleaves as $aleave) {


                    $avail_leave = new AvailLeave;
                    $count = 0;
                    if (isset($_POST['submitcredit'])) {
                        extract($_POST);
                        // print_r($_POST); exit;
                        $j1 = 1;


                        $clc = (isset($lc[$aleave['userid']]['cl'])? floatval($lc[$aleave['userid']]['cl']) : 0);
                        $mlc = (isset($lc[$aleave['userid']]['ml'])? floatval($lc[$aleave['userid']]['ml']) : 0);
                        $elc = (isset($lc[$aleave['userid']]['el'])? floatval($lc[$aleave['userid']]['el']) : 0);

                        // $elc = (isset($lc[$aleave['userid']]['el']) ? floatval($lc[$aleave['userid']]['el']) : 0);
                        // $rhc = (isset($lc[$aleave['userid']]['rh'])? floatval($lc[$aleave['userid']]['rh']) : 0);

                        if(isset($lc[$aleave['userid']]['cl'])){
                          $balclc =  $aleave['cl_ml'] + ($clc) ;
                        }else{
                          $balclc = $aleave['cl_ml'];
                        }

                        if(isset($lc[$aleave['userid']]['ml'])){
                          $balmlc =  $aleave['ml'] + ($mlc);
                        }else{
                          $balmlc = $aleave['ml'];
                        }

                        if(isset($lc[$aleave['userid']]['el'])){
                          $balelc =  $aleave['earned'] + ($elc);
                        }else{
                          $balelc =  $aleave['earned'];
                        }
                      
                        //
                        // if(isset($lc[$aleave['userid']]['rh'])){
                        //   $balrhc =  $aleave['rh'] + ($rhc);
                        // }else{
                        //   $balrhc =  $aleave['rh'];
                        // }
                            // echo $balelc; exit;

                         if (($clc != 0 and ( $balclc) >= 0) ||  ($mlc != 0 and ( $balmlc) >= 0)|| ($elc != 0 and ( $balelc) >= 0)) {
                            // echo "dfdf"; exit;

                            echo '<tr><td>' . $i++ . '</td>'
                            . '<td>' . $aleave['firstname'] . " " . $aleave['lastname'] . "<span class='userrole'>" . $aleave['role'] . "</span>" . '</td>'
                            . '<td>' . (($clc!=0 and $balclc >= 0) ? '<span class="newcredit">' : '') . ($balclc) .
                            (($balclc >= 0) ? '</span><small class="smallc">' . $aleave['cl_ml'] . ' (old) + ' . $clc . ' (New)</small>' : '') . (($balclc >= 0) ? CHtml::hiddenField('lc[' . $aleave['userid'] . '][cl]', $clc, array('class' => 'creditbox', 'id' => "cl_uid_" . $aleave['userid'])) : '' ) . '</td>'
                            . '<td>' . (($mlc != 0 and ( $balmlc) >= 0) ? '<span class="newcredit">' : '') . ($balmlc) .
                            (($mlc != 0 and ($balmlc) >= 0) ? '</span><small class="smallc">' .  $aleave['ml'] . ' (old) + ' . $mlc . ' (New)</small>' : '') . (($mlc != 0 and ($balmlc) >= 0) ? CHtml::hiddenField('lc[' . $aleave['userid'] . '][ml]', $mlc, array('class' => 'creditbox', 'id' => "ml_uid_" . $aleave['userid'])) : '' ) . '</td>'
                             . '<td>' . (($elc != 0 and ( $balelc) >= 0) ? '<span class="newcredit">' : '') . ($balelc) .
                             (($elc != 0 and ($balelc) >= 0) ? '</span><small class="smallc">' .  $aleave['earned'] . ' (old) + ' . $elc . ' (New)</small>' : '') . (($elc != 0 and ($balelc) >= 0) ? CHtml::hiddenField('lc[' . $aleave['userid'] . '][el]', $elc, array('class' => 'creditbox', 'id' => "el_uid_" . $aleave['userid'])) : '' ) . '</td>'
                            // . '<td>' . (($rhc != 0 and ( $balrhc) >= 0) ? '<span class="newcredit">' : '') . ($balrhc) .
                            // (($rhc != 0 and ( $balrhc) >= 0) ? '</span><small class="smallc">' .  $aleave['rh'] . ' (old) + ' . $rhc . ' (New)</small>' : '') . (($rhc != 0 and ( $balrhc) >= 0) ? CHtml::hiddenField('lc[' . $aleave['userid'] . '][rh]', $rhc, array('class' => 'creditbox', 'id' => "rh_uid_" . $aleave['userid'])) : '' ) . '</td>'
                            // . '<td style="font-weight: bold">' . $count . '</td>'
                            .'</tr>' . "\n";
                        }
                    } else {

                        echo '<tr><td>' . $i++ . '</td>'
                        . '<td>'. $aleave['firstname'] . " " . $aleave['lastname'] . "<span class='userrole'>" . $aleave['role'] . "</span>" . '</td>'
                        . '<td><div class="avc cl_uid_' . $aleave['userid'] . '">' .$aleave['cl_ml']. '</div> + ' . CHtml::textField('lc[' . $aleave['userid'] . '][cl]', 0, array('class' => 'creditbox', 'id' => "cl_uid_" . $aleave['userid'])) . '</td>'
                        . '<td><div class="avc ml_uid_' . $aleave['userid'] . '">' .$aleave['ml']. '</div> + ' . CHtml::textField('lc[' . $aleave['userid'] . '][ml]', 0, array('class' => 'creditbox', 'id' => "ml_uid_" . $aleave['userid'])) . '</td>'

                        . '<td><div class="avc el_uid_' . $aleave['userid'] . '">' .$aleave['earned']. '</div> + ' . CHtml::textField('lc[' . $aleave['userid'] . '][el]', 0, array('class' => 'creditbox', 'id' => "el_uid_" . $aleave['userid'])) . '</td>'
                        // . '<td><div class="avc rh_uid_' . $aleave['userid'] . '">' .$aleave['rh']. '</div> + ' . CHtml::textField('lc[' . $aleave['userid'] . '][rh]', 0, array('class' => 'creditbox', 'id' => "rh_uid_" . $aleave['userid'])) . '</td>'
                        // . '<td style="font-weight: bold">' . $count. '</td>'.
                        .' </tr>' . "\n";
                    }
                }

                if ($i == 1) {
                    echo '<tr><td colspan="6">No credits to add ' . CHtml::link('Go back', array('/leave/default/leavecredit'), array('class' => '', 'class' => 'btn btn-sm btn-primary sub')) . '</td></tr>';
                } else if (isset($j1)) {
                    echo CHtml::hiddenField('confirmsumitcredit', 1);
                }
                ?>

            </tbody>
        </table>
    </div>
    <?php $this->endWidget(); ?>
</div>
            </div>
<script type="text/javascript">
    $(document).ready(function () {
<?php
if (($i > 1 and isset($_POST['submitcredit']))) {
    ?>
            $(".confirmcredit").on('click', function () {
                if (confirm("Are you sure to confirm this credit to apply?")) {
                    $(this).val('Applying count. Please wait....');
                    //$(this).removeClass('confirmcredit');
                    $('#leave-credit-form').submit();
                } else {
                    return false;
                }
            });
    <?php
}
?>
        $('.submitcredit').on('click', function () {
            var numerrorItems = $('.creditbox.error').length;
            if (numerrorItems > 0) {
                alert('Please correct the values mentioned in the highligted fields.');
                return false;
            } else {
                $(this).val('Submitting..');
                $('#leave-credit-form').submit();
            }
        }),
                $('.creditbox').on('keyup', function () {
            var cboxid = $(this).attr('id');
            var inputval = parseFloat($(this).val());
            var availc = parseFloat($("." + cboxid).text());
            if ((availc + inputval) < 0 || isNaN(inputval)) {
                $(this).addClass('error');
            } else {
                $(this).removeClass('error');
            }
        });
        $('.creditbox').on('blur', function () {
            var inputval = parseFloat($(this).val());
            if (inputval == 0 || isNaN(inputval)) {
                $(this).val(0);
                $(this).removeClass('error');
            }
        });

        $("#fixed-header").tableHeadFixer({ 'head' : true});

    });
</script>
<style type="text/css">
    .confirmcredit{
        display:<?php echo (($i > 1 and isset($_POST['submitcredit'])) ? 'block' : 'none') ?>;
    }
    .submitcredit{
        display:<?php echo (!isset($_POST['submitcredit']) ? 'block' : 'none') ?>;
        float:right;
    }
    #fixed-header-holder{
      max-height: 450px;
    }

</style>
