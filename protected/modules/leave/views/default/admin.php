
<h1>Manage my Leaves</h1>

<?php

echo "<u>".CHtml::link('Apply leave',array('/leave/default/create'))."</u>";
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leave-grid',
    'itemsCssClass' => 'greytable',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        //'leave_id',
        //'emp_id',
        'leave_submit_date',
        array('name' => 'leave_type', 'value' => '$data->leaveType->caption',
            'filter' => CHtml::listData(LeaveSetting::model()->findAll(
                            array(
                                'select' => array('leave_setting_id,caption'),
                                'order' => 'caption',
                                'condition' => 'setting_type="leave_type"',
                                'distinct' => true
                    )), "leave_setting_id", "caption")),
        'date_from',
        'date_to',
        array('header' => 'Total days', 'value' => 'LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$data->leave_id"
                    ))->leave_value',
        ),
        'reason_for_leave',
        array('name' => 'approval_status', 'value' => '($data->approval_status!=""?$data->approvalStatus->caption:"Pending")',
            'filter' => CHtml::listData(LeaveSetting::model()->findAll(
                            array(
                                'select' => array('leave_setting_id,caption'),
                                'order' => 'caption',
                                'condition' => 'setting_type="leave_status"',
                                'distinct' => true
                    )), "leave_setting_id", "caption"),
            ),
        /* 'contact_details',
          'document_if_any',
          'person_covering_role',

          'decision_by',
          'decision_date',
          'created_by',
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
    ),
));
?>