<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.leave_results_report a').on('click', function () {
            var selected_status_id = $(this).attr('id');
            $(".leave_results_report div").each(function () {
                $(this).removeAttr('class');
            });
            // alert(selected_status_id);
            $("#" + selected_status_id).parent().addClass('activests');
        });
    });

</script>
<?php
 $usertbl = Users::model()->tableSchema->rawName;

 $company = '';
    if( isset(Yii::app()->user->company_id) && Yii::app()->user->company_id!='' ){

        $company = " and company_id = ".Yii::app()->user->company_id;
    }


     /* new change */

    if(Yii::app()->user->role!=1){

      $data1 = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `userid` =".Yii::app()->user->id )->queryRow();
      $con1 = '';
      if($data1['shift_supervisor']==1){

        $sql = "select leave_setting_id,caption,l.approval_status,count(l.approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.userid=l.emp_id ".$company." "
               . " right join pms_leave_setting as ls on l.approval_status=leave_setting_id"
               . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and us.user_type!=17 and us.shift_supervisor !=1 ':'')." group by leave_setting_id";

      }else{
        $sql = "select leave_setting_id,caption,l.approval_status,count(l.approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.userid=l.emp_id ".$company." "
               . " right join pms_leave_setting as ls on l.approval_status=leave_setting_id"
               . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and us.report_to='. Yii::app()->user->id:'')." group by leave_setting_id";
      }

   }else{


         $sql = "select leave_setting_id,caption,l.approval_status,count(l.approval_status) as tot_count from pms_leave as l  inner join ".$usertbl." as us on us.userid=l.emp_id ".$company." "
                . " right join pms_leave_setting as ls on l.approval_status=leave_setting_id"
                . " WHERE setting_type='leave_status' ".((Yii::app()->controller->action->id=='manageleaves' && Yii::app()->user->role!=1)?' and us.report_to='. Yii::app()->user->id:'')." group by leave_setting_id";
  }

$statuss = Yii::app()->db->createCommand($sql)->queryAll();
?>
<div class="manageleaves-sec">
<div class='clearfix'>
<div class="pull-left">
    <h1>Manage All Leaves</h1>
</div>
<div class='leave_results_report pull-right'>
    <div class='activests'><?php echo CHtml::link('All', array('/leave/default/manageleaves', 'type' =>'All'), array('id' => 'all', 'onclick' => '$("select[name=\'Leave[approval_status]\']").val("").change();')); ?></div>
    <?php

    foreach ($statuss as $sts) {
        extract($sts);

        ?>
        <div><?php echo CHtml::link($sts['caption'] . ': ' . $sts['tot_count'], array('/leave/default/manageleaves', 'type' =>$sts['leave_setting_id']), array('id' => 'sts_id_' . $sts['leave_setting_id'], 'onclick' => '$("select[name=\'Leave[approval_status]\']").val('.$approval_status.').change();')); ?></div>
       
       <?php
    }
    ?>

</div>
</div>

<?php

$leave_model = Yii::app()->db->createCommand(" SELECT leave_id,description FROM `pms_leave_types` as lt inner join pms_legends as lg on lg.leg_id= lt.`leave_id` ")->queryAll();

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leave-grid',
    'itemsCssClass' => 'greytable table table-bordered',
    'template' => '<div class="table-responsive">{items}</div>',
    'dataProvider' => $model->search(0,$type), //own leaves only
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array(
            'class' => 'CButtonColumn',
            'template' => '{approve_reject}{view}',
            'buttons' => array
                (
                'approve_reject' => array
                 (
                    'label' => 'Approve/Reject',
                    'url' => 'Yii::app()->createUrl("/leave/default/viewleave", array("id"=>$data->leave_id))',
                    'visible'=>'$data->approval_status==11',
                ),

                'view' => array
                (
                    'label' => 'Approve/Reject',
                    'url' => 'Yii::app()->createUrl("/leave/default/viewleave", array("id"=>$data->leave_id))',
                    'visible'=>'$data->approval_status!=11',
                )
            ),
        ),
         array('name' => 'empname',
            'header' => 'Name',
            'type'   =>'raw',
            'value'  => '($data->empid->shift_supervisor ==1)?$data->empid->first_name." ".$data->empid->last_name." <sup style=color:#ff0018;font-size:8px;>Supervisor</sup><br/><small style=color:#337ab7;>".$data->getcompany($data->emp_id)."</small>":$data->empid->first_name." ".$data->empid->last_name." <br/><small style=color:#337ab7;>".$data->getcompany($data->emp_id)."</small>"',
            'filter' => CHtml::listData(Users::model()->findAll(
                array(
                    'select' => array('userid', 'concat(first_name," ",last_name) as first_name'),
                    'order' => 'first_name',
                   
                    'distinct' => true
                )
        ), "first_name", "first_name")
        ),
            
             array(
                'name' => 'leave_submit_date',
                'value' => '$data->leave_submit_date',
                'type' => 'raw',   
            ),
       
        'date_from',
        'date_to',
        array('header' => 'Total days', 'value' => 'LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$data->leave_id"
                    ))->leave_value',
        ),
       
        array('name'=>'created_by','value'=>'$data->createdBy->first_name." ".$data->createdBy->last_name'),

        array('name' => 'approval_status', 'value' => '($data->approval_status==11?Pending:($data->approval_status==10?Rejected:Approved))',
            // 'filter' => CHtml::listData(LeaveSetting::model()->findAll(
            //                 array(
            //                     'select' => array('leave_setting_id,caption'),
            //                     'order' => 'caption',
            //                     'condition' => 'setting_type="leave_status"',
            //                     'distinct' => true
            //         )), "leave_setting_id", "caption"),
        ),

         array(

            'name'=>'decision_by',
            'value'=>'$data->getdecisionby($data->decision_by)',
         ),


        /* 'contact_details',
          'document_if_any',
          'person_covering_role',

          'decision_by',
          'decision_date',

          'created_date',
          'updated_by',
          'updated_date',
         */
        
    ),
));
?>
</div>
<script>
$(document).ready(function(){


$("input[name='Leave[leave_submit_date]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$("input[name='Leave[date_from]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

$("input[name='Leave[date_to]']").datepicker({dateFormat: 'yy-mm-dd'});
$('.hasDatepicker').attr('autocomplete', 'off');

})

</script>
