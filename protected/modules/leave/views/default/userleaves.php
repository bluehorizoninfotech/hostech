
<?php
$this->menu = array(
    array('label' => 'General', 'url' => array('/employmentDetails/update&id=' . $userid)),
    array('label' => 'Communication Details', 'url' => array('/currentAddress/view&id=' . $userid), 'itemOptions' => array('class' => 'active')),
    array('label' => 'Payroll', 'url' => array('/employmentDetails/listsalary', 'id' => $userid)),
    array('label' => 'Leaves', 'url' => array('/leave/default/userleaves', 'id' => $userid)),
);
?>
<div class="userleaves-sec">
<div class="pull-left width-40-percentage">
<h1>Leaves</h1>
</div>
<div class="pull-left width-40-percentage">
    <h4>Available Leave Summary</h4>
    
    <?php
     $tblpx = Yii::app()->db->tablePrefix;
    $avlleaves = Yii::app()->db->createCommand('select * from {$tblpx}avail_leave where userid='.Yii::app()->user->id)->queryAll();
//    echo '<pre>';
//    print_r($avlleaves);
//    echo '</pre>';
     $cl_ml = "";
    $earned="";
    $lieu="";
    if(count($avlleaves)){
    extract($avlleaves[0]);
    }
    ?>
    
    <table class="greytable">
        <thead><tr><th></th><th>Accrued</th><th>Balance</th></tr></thead>
        <tbody>
        <tr><th>Casual/Medical Leave: </th><td><?=$cl_ml?></td><td><?=$cl_ml?></td></tr> 
        <tr><th>Earned Leave: </th><td><?=$earned?></td><td><?=$earned?></td></tr>
        <tr><th>Lieu Leave</th><td>&nbsp;</td><td><?=$lieu?></td></tr></tbody>
    </table> 
    
</div>
<br clear="all" />

<?php

echo $model->leave_id;
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leave-grid',
    'itemsCssClass' => 'greytable',
    'dataProvider' => $model->search($userid), //own log
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        //'leave_id',
        //'emp_id',
        'leave_submit_date',
        array('name' => 'leave_type', 'value' => '$data->leaveType->caption',
            'filter' => CHtml::listData(LeaveSetting::model()->findAll(
                            array(
                                'select' => array('leave_setting_id,caption'),
                                'order' => 'caption',
                                'condition' => 'setting_type="leave_type"',
                                'distinct' => true
                    )), "leave_setting_id", "caption")),
        'date_from',
        'date_to',
        array('header' => 'Total days', 'value' => 'LeaveDay::model()->find(
                            array(
                                "select" => array("sum(leave_value) as leave_value"),
                                "condition" => "leave_id=$data->leave_id"
                    ))->leave_value',
        ),
        'reason_for_leave',
        array('name' => 'approval_status', 'value' => '($data->approval_status!=""?$data->approvalStatus->caption:"Pending")',
            'filter' => CHtml::listData(LeaveSetting::model()->findAll(
                            array(
                                'select' => array('leave_setting_id,caption'),
                                'order' => 'caption',
                                'condition' => 'setting_type="leave_status"',
                                'distinct' => true
                    )), "leave_setting_id", "caption"),
        ),
        /* 'contact_details',
          'document_if_any',
          'person_covering_role',

          'decision_by',
          'decision_date',
          'created_by',
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'template' => '{more}',
            'buttons' => array(
                'more' => array(
                    'label' => 'More',
                    'url'=> 'Yii::app()->controller->createUrl("/leave/default/userview", array("id"=>$data->leave_id))',

                ),
            ),
        ),
    ),
));
?>
</div>