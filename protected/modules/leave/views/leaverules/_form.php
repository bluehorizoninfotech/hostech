<?php
/* @var $this LeaverulesController */
/* @var $model leave_rules */
/* @var $form CActiveForm */
?>
<style >

#check{

   /* margin: 10px 20px;
    padding: 10px;
    display:inline-block;
    overflow:hidden*/

}
	
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'leave-rules-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<!-- <?php echo $form->errorSummary($model); ?> -->

	<div class="row">
		<!-- <?php echo $form->labelEx($model,'designation_id'); ?>
		<?php echo $form->textField($model,'designation_id'); ?>
		<?php echo $form->error($model,'designation_id'); ?> -->

        <?php echo $form->labelEx($model,'designation_id'); ?>

         <?php 
          $testQuery =  CHtml::listData(Designation::model()->findAll(array(
                              'select'=>'designation,designation_id',
                             // 'condition'=>'status="active"',
                             // 'order'=>'id'
                             )), 'designation_id','designation');
        echo $form->dropDownList($model,'designation_id',  $testQuery, array('empty' => 'Select Leave type')); 
		?>
		<?php echo $form->error($model,'designation_id'); ?> 
    </div>


	<div class="row" id="check">
		<?php echo $form->labelEx($model,'working_satdays'); ?>

		<!--  <?php echo $form->textField($model,'working_satdays',array('size'=>50,'maxlength'=>50)); ?>  -->
		<?php    $saturdays = explode(',', $model->working_satdays); ?>
		 <input type="checkbox" name="working_satdays[]" value="1" <?php if(in_array('1', $saturdays)){?>checked <?php }?>>1st Saturday
          <input type="checkbox" name="working_satdays[]" value="2" <?php if(in_array('2', $saturdays)){?>checked <?php }?>>2nd Saturday
          <input type="checkbox" name="working_satdays[]" value="3" <?php if(in_array('3', $saturdays)){?>checked <?php }?>>3rd Saturday
          <input type="checkbox" name="working_satdays[]" value="4" <?php if(in_array('4', $saturdays)){?>checked <?php }?>>4th Saturday


		<?php echo $form->error($model,'working_satdays'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sunday_leave'); ?>

		<?php  

	      $sundays = explode(',', $model->sunday_leave); ?> 

		  <input type="checkbox" name="sunday_leave[]" value="1" <?php if(in_array('1', $sundays)){?>checked <?php }?>>1st Sunday
          <input type="checkbox" name="sunday_leave[]" value="2" <?php if(in_array('2', $sundays)){?>checked <?php }?>>2nd Sunday
          <input type="checkbox" name="sunday_leave[]" value="3" <?php if(in_array('3', $sundays)){?>checked <?php }?>>3rd Sunday
          <input type="checkbox" name="sunday_leave[]" value="4" <?php if(in_array('4', $sundays)){?>checked <?php }?>>4th Sunday

        <?php echo $form->error($model,'sunday_leave'); ?>
	</div>

	<div class="row" id="sand">
		<?php echo $form->labelEx($model,'sandwich_leave'); ?>
		<!-- <?php echo $form->textField($model,'sandwich_leave'); ?> -->
         <?php
                $accountStatus = array('1'=>'Yes', '0'=>'No');
                echo $form->radioButtonList($model,'sandwich_leave', $accountStatus, array('labelOptions'=>array('class'=>'display-inline'),
                'separator'=>'  ',));
          ?>
		<?php echo $form->error($model,'sandwich_leave'); ?>
       </div>

	<div class="row">
		<?php echo $form->labelEx($model,'continue_leave'); ?>
		<!-- <?php echo $form->textField($model,'continue_leave'); ?> -->
		<?php
                $accountStatus = array('1'=>'Yes', '0'=>'No');
                echo $form->radioButtonList($model,'continue_leave', $accountStatus,array('labelOptions'=>array('class'=>'display-inline'),
                'separator'=>'  ',));
        ?>
		<?php echo $form->error($model,'continue_leave'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->