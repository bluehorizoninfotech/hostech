<br clear='all' />
<div class="form pull-left margin-right-20 light-gray-border-right-2 padding-right-20 width-250">
    <h1> <?php echo ($model2->isNewRecord ? 'Add Leave rules' : 'Update Leave rules') ?></h1>

		      <?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'leave-rules-form',
			'enableAjaxValidation'=>false,
		)); ?>


			<div class="row">
				<?php echo $form->labelEx($model2,'designation_id'); ?>

		         <?php 
		          $testQuery =  CHtml::listData(Designation::model()->findAll(array(
		                              'select'=>'designation,designation_id',
		                             // 'condition'=>'status="active"',
		                             // 'order'=>'id'
		                             )), 'designation_id','designation');
		        echo $form->dropDownList($model2,'designation_id',  $testQuery, array('empty' => 'Select Leave type')); 
				?>
				<?php echo $form->error($model2,'designation_id'); ?> 
		    </div><br>


			<div class="row" id="check">
				<?php echo $form->labelEx($model2,'working_satdays'); ?>


				<ul class="list-style-none">
					<?php
					$saturdays = explode(',', $model2->working_satdays); 
					$satdays = array(1=>'1st',2=>'2nd',3=>'3rd',4=>'4th');
					foreach($satdays as $val=>$lab){
					?>
					<li>
					<input type="checkbox"  name="working_satdays[]" value="<?php echo $val?>" <?php if(in_array($val,  $saturdays)){?>checked <?php }?>><?php echo $lab?> Saturday
					</li>
					<?php
					}

					?>
				</ul>
            <?php echo $form->error($model2,'working_satdays'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model2,'sunday_leave'); ?>

				<?php  

			      $wrsundays = explode(',', $model2->sunday_leave); ?> 

				<ul class="list-style-none">
					<?php
					$sundays = array(1=>'1st',2=>'2nd',3=>'3rd',4=>'4th');
					foreach($sundays as $val=>$lab){
					?>
					<li>
					<input type="checkbox"  name="sunday_leave[]" value="<?php echo $val?>" <?php if(in_array($val, $wrsundays)){?>checked <?php }?>><?php echo $lab?> Sunday
					</li>
					<?php } ?>
				</ul>

		        <?php echo $form->error($model2,'sunday_leave'); ?>
			</div><br>

			<div class="row" id="sand">
				<?php echo $form->labelEx($model2,'sandwich_leave'); ?>
				<!-- <?php echo $form->textField($model2,'sandwich_leave'); ?> -->
		         <?php
		                $accountStatus = array('1'=>'Yes', '0'=>'No');
		                echo $form->radioButtonList($model2,'sandwich_leave', $accountStatus, array('labelOptions'=>array('class'=>'display-inline'),
		                'separator'=>'  ',));
		          ?>
				<?php echo $form->error($model2,'sandwich_leave'); ?>
		    </div><br>

			<div class="row">
				<?php echo $form->labelEx($model2,'continue_leave'); ?>
				<!-- <?php echo $form->textField($model2,'continue_leave'); ?> -->
				<?php
		                $accountStatus = array('1'=>'Yes', '0'=>'No');
		                echo $form->radioButtonList($model2,'continue_leave', $accountStatus,array('labelOptions'=>array('class'=>'display-inline'),
		                'separator'=>'  ',));
		        ?>
				<?php echo $form->error($model2,'continue_leave'); ?>
			</div><br>

			<div class="row buttons">
				<?php echo CHtml::submitButton($model2->isNewRecord ? 'Create' : 'Save'); ?>
			</div>

		<?php $this->endWidget(); ?>

    </div>

<div class="pull-left width-60-percentage">

	<h1>Manage Leave Rules</h1>


	<?php 


	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'leave-rules-grid',
	        'itemsCssClass' =>'greytable',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
	        array(
	        	'name'=>'designation_id',
	        	'value'=>'$data->designation0->designation',
	        	'filter'=>CHtml::listData(Designation::model()->findAll(),'designation_id','designation'),
	        	),
			array(
				'name'=>'working_satdays',
				//'value'=>'$data->working_satdays==null?"Not set":$data->working_satdays',
				 'value' => function($data, $row) {
	                $rule_id = $data->rule_id;
	                echo $data->getworkingsatdays($rule_id);
                },

				),
			array(
				   'name'=>'sunday_leave',
				//'value'=>'$data->sunday_leave==null?"Not set":$data->sunday_leave'
				   'value' => function($data, $row) {
	                $rule_id = $data->rule_id;
	                echo $data->getworkingsundays($rule_id);

                   },
				),
	        array('name'=>'sandwich_leave','value'=>'$data->sandwich_leave==0?"Not allowed":"allowed"'),
	        array('name'=>'continue_leave','value'=>'$data->continue_leave==0?"Not allowed":"allowed"'),
		 
			array(
				'class'=>'CButtonColumn',
				'template'=>'{update},{delete}',
				'buttons' => array(
                    'update' => array
                    (
                       'url' => 'Yii::app()->createUrl("leave/Leaverules/index", array("id"=>$data->rule_id))',
                    ),
                ),

			),
		),
	)); ?>
</div>

