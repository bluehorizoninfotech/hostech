<?php
/* @var $this LeaverulesController */
/* @var $model leave_rules */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'rule_id'); ?>
		<?php echo $form->textField($model,'rule_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'designation_id'); ?>
		<?php echo $form->textField($model,'designation_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'working_satdays'); ?>
		<?php echo $form->textField($model,'working_satdays',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sandwich_leave'); ?>
		<?php echo $form->textField($model,'sandwich_leave'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sunday_leave'); ?>
		<?php echo $form->textField($model,'sunday_leave'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'continue_leave'); ?>
		<?php echo $form->textField($model,'continue_leave'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->