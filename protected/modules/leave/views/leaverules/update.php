<?php
/* @var $this LeaverulesController */
/* @var $model leave_rules */

$this->breadcrumbs=array(
	'Leave Rules'=>array('index'),
	$model->rule_id=>array('view','id'=>$model->rule_id),
	'Update',
);

$this->menu=array(
	
	array('label'=>'Create leave_rules', 'url'=>array('create')),
    array('label'=>'Manage leave_rules', 'url'=>array('index')),
);
?>

<h1>Update leave_rules <?php echo $model->rule_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>