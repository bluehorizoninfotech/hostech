<?php
/* @var $this LeaverulesController */
/* @var $model leave_rules */

 //print_r($model);exit;

$this->breadcrumbs=array(
	'Leave Rules'=>array('index'),
	$model['rule_id'],
);

$this->menu=array(
	
	array('label'=>'Create leave_rules', 'url'=>array('create')),
	array('label'=>'Update leave_rules', 'url'=>array('update', 'id'=>$model['rule_id'])),
	array('label'=>'Delete leave_rules', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model['rule_id']),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage leave_rules', 'url'=>array('index')),
);
?>

<h1>View Leave rules #<?php echo $model['rule_id']; ?></h1>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		 array(
            'name' => 'Designation',
            'type' => 'raw',
            'value' => $model['designation'],
        ),
		 array(
            'name' => 'Working saturdays',
            'type' => 'raw',
            'value' => $model['working_satdays'],
        ),
		array(
            'name' => 'Sandwich Leave',
            'type' => 'raw',
            'value' => $model['sandwich_leave'],
        ),
        array(
            'name' => 'Sunday leave',
            'type' => 'raw',
            'value' => $model['sunday_leave'],
        ),
        array(
            'name' => 'Continue leave',
            'type' => 'raw',
            'value' => $model['continue_leave'],
        ),

		
	),
)); ?>
