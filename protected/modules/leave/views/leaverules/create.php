<?php
/* @var $this LeaverulesController */
/* @var $model leave_rules */

$this->breadcrumbs=array(
	'Leave Rules'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List leave_rules', 'url'=>array('index')),
	array('label'=>'Manage leave_rules', 'url'=>array('index')),
);
?>

<h1>Create leave Rules</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>