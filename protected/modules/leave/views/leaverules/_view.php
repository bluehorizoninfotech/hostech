<?php
/* @var $this LeaverulesController */
/* @var $data leave_rules */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('rule_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->rule_id), array('view', 'id'=>$data->rule_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('designation_id')); ?>:</b>
	<?php echo CHtml::encode($data->designation_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('working_satdays')); ?>:</b>
	<?php echo CHtml::encode($data->working_satdays); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sandwich_leave')); ?>:</b>
	<?php echo CHtml::encode($data->sandwich_leave); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sunday_leave')); ?>:</b>
	<?php echo CHtml::encode($data->sunday_leave); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('continue_leave')); ?>:</b>
	<?php echo CHtml::encode($data->continue_leave); ?>
	<br />


</div>