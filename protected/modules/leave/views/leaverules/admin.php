<?php
/* @var $this LeaverulesController */
/* @var $model leave_rules */

$this->breadcrumbs=array(
	'Leave Rules'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List leave_rules', 'url'=>array('index')),
	array('label'=>'Create leave_rules', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('leave-rules-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Leave Rules</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form display-none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'leave-rules-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'rule_id',
		'designation_id',
		'working_satdays',
		'sandwich_leave',
		'sunday_leave',
		'continue_leave',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
