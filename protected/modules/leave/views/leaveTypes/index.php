<?php
/* @var $this LeaveTypesController */
/* @var $model LeaveTypes */

$this->breadcrumbs=array(
	'Leave Types'=>array('index'),
	'Manage',
);

?>

<div class="index-leave-type-sec">
<h1 class="margin-bottom-20">Manage Leave Types For Employees to Apply</h1>

<div class="pull-left width-300 light-gray-border-right margin-right-20">
<?php if($id==0){ ?>
		  <h4>Add to Leave Types</h4>
<?php }else{ ?>
	<h4>Update Leave Type</h4>

<?php } ?>


    <?php
		 if(in_array('/leaves/addleavetype', Yii::app()->session['pmsmenuauthlist'])){
	      	echo $this->renderPartial('_form', array('lmodel'=>$lmodel,'legendmodel'=>$legendmodel,'id'=>$id));
			}

		 ?>
</div>


<div class="width-50-percentage pull-left">

	<?php
	    foreach(Yii::app()->user->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
	?>
	
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'leave-types-grid',
    'itemsCssClass' =>'greytable',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
   array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
   array('name'=>'leave_id','value'=>'$data->leave->description'),
	  array('name'=>'leave_id','header'=>'Short Note','value'=>'$data->leave->short_note'),

		array(
			'class'=>'CButtonColumn',
      'template'=>'{update}{delete}',
			'buttons'=>array
		    (
		        'update' => array
		        (
		            'label'=>'Update',
		             'url'=>'Yii::app()->createAbsoluteUrl("leave/leaveTypes/index", array("id"=>$data->leave_id))',
		        ),

		    ),
		),
	),
)); ?>
</div>
</div>

