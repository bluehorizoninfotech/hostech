
<?php if($id==0) { ?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'leave-types-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php //echo $form->errorSummary($lmodel); ?>

	<div class="row">
		<?php echo $form->labelEx($lmodel,'leave_id'); ?>
		<?php

                $leavetypes = CHtml::listData(Legends::model()->findAll(array(
                                        'select' => array('leg_id, concat("",description,"(",short_note,")") as description'),
                                        'order' => 'description',
                                        'distinct' => true
                                    )), 'leg_id', 'description');

                echo $form->dropDownList($lmodel, 'leave_id',$leavetypes , array('empty' => 'Select Leave type', 'class' => 'width-250',
                        'id' => 'leave_type_id')); ?>
		<?php echo $form->error($lmodel,'leave_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($lmodel->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::resetButton('Reset',array('class'=>'btn btn-default reset')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php }else{ ?>

	<div class="form">

			<?php
					$form=$this->beginWidget('CActiveForm', array(
						'id'=>'leave-legend-form',
						'enableAjaxValidation'=>false,
					));
			?>

			<div class="row">
					<?php echo $form->labelEx($legendmodel,'short_note'); ?>
				  <?php echo $form->textField($legendmodel,'short_note', array('class'=>'form-control width-180','autocomplete'=>'off')); ?>
					<?php echo $form->error($legendmodel,'short_note'); ?>
			</div>

			<div class="row buttons">
				<?php echo CHtml::submitButton($legendmodel->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
				<?php echo CHtml::resetButton('Reset',array('class'=>'btn btn-default reset')); ?>
			</div>

		<?php $this->endWidget(); ?>

	</div><!-- form -->


<?php } ?>


<script>
$(document).ready(function () {

		$('input[type=text], textarea').keyup(function () {
				$(this).val($(this).val().toUpperCase());

		 });
 });

$(".reset").click(function(){
    location.href = "<?php echo Yii::app()->createAbsoluteUrl("leave/leaveTypes/index", array(),"") ?>";
});
</script>
