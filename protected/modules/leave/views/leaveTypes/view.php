<?php
/* @var $this LeaveTypesController */
/* @var $model LeaveTypes */

$this->breadcrumbs=array(
	'Leave Types'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeaveTypes', 'url'=>array('index')),
	array('label'=>'Create LeaveTypes', 'url'=>array('create')),
	array('label'=>'Update LeaveTypes', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LeaveTypes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeaveTypes', 'url'=>array('admin')),
);
?>

<h1>View LeaveTypes #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'leave_id',
	),
)); ?>
