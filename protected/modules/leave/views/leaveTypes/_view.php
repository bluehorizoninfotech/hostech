<?php
/* @var $this LeaveTypesController */
/* @var $data LeaveTypes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leave_id')); ?>:</b>
	<?php echo CHtml::encode($data->leave_id); ?>
	<br />


</div>