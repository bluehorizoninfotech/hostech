<?php
/* @var $this LeaveTypesController */
/* @var $model LeaveTypes */

$this->breadcrumbs=array(
	'Leave Types'=>array('index'),
	'Manage',
);

?>


<h1 class="text-decoration-underline">Manage Leave Types For Employees to Apply</h1>

<div class="pull-left width-300 light-gray-border-right margin-right-20">
    <h2>Add to Leave Types</h2>
    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
<div class="width-30-percentage pull-left">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'leave-types-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
//                array('name'=>'id', 'htmlOptions' => array('width' => '30px','style'=>'font-weight: bold')),
		'leave_id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
