<?php

class DefaultController extends Controller {


      public $allpunchs = array();
      public $getpunches = false;
      public $punchresult = array();
      public $empdevid = array();
      public $employees = array();
      public $punches = array();
      public $pdate = '';  //Punchdate
      public $numdays = 0;
      public $devices = '';




    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {


      $accessArr = array();
      $accessauthArr = array();
      $accessguestArr = array();
      $module= Yii::app()->controller->module->id;
      $controller_id = Yii::app()->controller->id;
      $controller = $module.'/'.$controller_id;

      if(isset(Yii::app()->session['pmsmenuall'])){
          if(array_key_exists($controller, Yii::app()->session['pmsmenuall'])){
              $accessArr = Yii::app()->session['pmsmenuall'][$controller];
          }
      }

      if(isset(Yii::app()->session['pmsmenuauth'])){
          if(array_key_exists($controller, Yii::app()->session['pmsmenuauth'])){
              $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
          }
      }

      if (isset(Yii::app()->session['pmsmenuguest'])) {
          if(array_key_exists($controller, Yii::app()->session['pmsmenuguest'])){
              $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
          }
      }
      $access_privlg = count($accessauthArr);

      return array(



          array(
              'allow',
              'actions' => $accessArr,
              'users' => array('@'),
              //'expression' => "$access_privlg > 0",

          ),
          array(
              'allow',
              'actions' => $accessauthArr,
              'users' => array('@'),
              'expression' => "$access_privlg > 0",

          ),


          array(
              'deny',
              'users' => array('*'),
          ),

      );




        // return array(
        //     array('allow', // allow all users to perform 'index' and 'view' actions
        //         'actions' => array('index', 'view', 'calcdays', 'userleaves', 'userview', 'adminuserview', 'getleavesummary'),
        //         'users' => array('@'),
        //     ),
        //     array('allow', // allow authenticated user to perform 'create' and 'update' actions
        //         'actions' => array('create', 'update', 'manageleaves', 'viewleave', 'leaveaction', 'attendancebydate', 'test','Leavecredit'),
        //         'users' => array('@'),
        //     ),
        //     array('allow', // allow admin user to perform 'admin' and 'delete' actions
        //         'actions' => array('admin', 'delete'),
        //         'users' => array('@'),
        //         'expression' => 'yii::app()->user->role==1 ',
        //     ),
        //     array('deny', // deny all users
        //         'users' => array('*'),
        //     ),
        // );



    }

    public function actionLeaveaction() {



        if (isset($_REQUEST['id'])) {
            extract($_GET);

            $ret = array('error' => '', 'msg' => '', 'status' => '');
            $status = array('Approve' => 9, 'Reject' => 10);
            $req = $_REQUEST['req'];
            $id = $_REQUEST['id'];
            if ($req) {
                $sts = $status[$req];

                $sql = "select emp_id,l.leave_id,l.leave_type,ls.caption, sum(leave_value) as total_leaves FROM pms_leave_day as ld "
                        . "INNER JOIN pms_leave as l on l.leave_id=ld.leave_id "
                        . "INNER JOIN pms_leave_setting as ls on ls.leave_setting_id=l.leave_id AND setting_type='leave_type' "
                        . "WHERE l.leave_id=" . $id;

                $chkstatus = Yii::app()->db->createCommand($sql)->queryRow();
                $tblpx = Yii::app()->db->tablePrefix;
                $sqlavail = Yii::app()->db->createCommand("select * from {$tblpx}avail_leave where userid=" . intval($chkstatus['emp_id']))->queryRow();

                $cl_ml = 0;
                $earned = 0;
                $lieu = 0;
                if ($sqlavail !== false) {
                    extract($sqlavail);
                }

                extract($chkstatus);
                $sflag = 1;

                //email  to employee -approved or rejected

                if ($sflag == 1 && $sts == 9 or $sts == 10) {

                    $leaves = Leave::model()->findByPk($id);
                    $userdetails = Users::model()->findByPk($leaves['emp_id']);
                    $settings = GeneralSettings::model()->findByPk(1);
                    $lv_settings = LeaveSetting::model()->findByPk($leaves['leave_type']);
                    $total_days = LeaveDay::model()->find(
                                    array(
                                        "select" => array("sum(leave_value) as leave_value"),
                                        "condition" => "leave_id=$id",
                            ))->leave_value;


                    if ($sts == 9) {
                        $subject = (!empty($settings['subject_approve'])) ? $settings['subject_approve'] : 'Leave Approve';
                        $mail_template = $settings['mail_template_approve'];
                    } else {
                        $subject = (!empty($settings['subject_reject'])) ? $settings['subject_reject'] : 'Leave Request rejected';
                        $mail_template = $settings['mail_template_reject'];
                    }



                    $name = $userdetails['first_name'] . ' ' . $userdetails['last_name'];
                    //$from_name = $settings['from_name'];
                    $from_name = 'Admin';
                    $var = array('{name}', '{from_name}', '{date_from}', '{date_to}', '{reason_leave}', '{leave_type}', '{total_days}', '{type}');
                    $data = array($name, $from_name, $leaves['date_from'], $leaves['date_to'], $leaves['reason_for_leave'], $lv_settings['caption'], $total_days);

                    $message = str_replace($var, $data, nl2br($mail_template));
                    $body = $this->renderPartial('email', array('message' => $message), true);
                    if ($this->paramsval('email_notify')) {
                        $smtpdetails = $this->paramsval('smtpmailconfig');
                        extract($smtpdetails);
                        $mail = new JPhpMailer();
                        if ($_SERVER['HTTP_HOST'] != 'localhost') {

                            // echo "sd";exit;
                            $mail->IsSMTP();
                            $mail->Host = $settings->smtp_host;
                            $mail->Port       = $settings->smtp_port;
                            $mail->SMTPSecure = $settings->smtp_secure;
                            $mail->SMTPAuth = $settings->smtp_auth;
                            $mail->Username = $settings->smtp_username;
                            $mail->Password = $settings->smtp_password;

                            $mail->setFrom($settings->smtp_email_from, $smtpmailfromname);
                        } else {

                            $mail->IsSMTP();
                            $mail->Host = $mailHost;
                            $mail->SMTPAuth = $mailSMTPAuth;
                            $mail->Username = $mailUsername;
                            $mail->Password = $mailPassword;
                            $mail->SMTPSecure = $mailSMTPSecure;
                            $mail->setFrom($mailsetFrom, $smtpmailfromname);
                        }


                        $to_email = $userdetails->email;



                        $mail->addAddress($to_email);   // Add a recipient
                        $mail->isHTML(true);
                        $mail->Subject = $subject;
                        $mail->Body = $body;

                         if($mail->send()) {
                            $logmodel = new MailLog;
                            $logmodel->send_to = $leaves['emp_id'];
                            $logmodel->send_by = Yii::app()->user->id;
                            $logmodel->send_date = date('Y-m-d H:i:s');
                            $logmodel->created_by = Yii::app()->user->id;
                            $logmodel->created_date = date('Y-m-d');
                            $logmodel->description = ($sts == 9)?'Leave Approve':'Leave Rejected';
                            $logmodel->message = 'Mail sent';
                            $logmodel->save();

                            $sflag == 1;
                        } else {

                          $logmodel = new MailLog;
                          $logmodel->send_to = $leaves['emp_id'];
                          $logmodel->send_by = Yii::app()->user->id;
                          $logmodel->send_date = date('Y-m-d H:i:s');
                          $logmodel->created_by = Yii::app()->user->id;
                          $logmodel->created_date = date('Y-m-d');
                          $logmodel->description = ($sts == 9)?'Leave Approve':'Leave Rejected';
                          $logmodel->message = 'Mail sent failed';
                          $logmodel->save();



                            $ret['error'] = 'Email sending error,Please try again later';
                            $sflag == 0;
                        }
                    }
                }

                if ($sflag == 1) {

                    $tblpx = Yii::app()->db->tablePrefix;
                    $sql = "UPDATE {$tblpx}leave SET approval_status=$sts, decision_by=" . Yii::app()->user->id . ", decision_date=now() WHERE leave_id=" . intval($id);
                    Yii::app()->db->createCommand($sql)->query();


                    if($sts == 9){ //approval
                        //insert to attendance table
                         $sql3 = "select leave_date as att_date ,leg_id as att_entry,emp_id as user_id,created_date,updated_date as modified_date,created_by from pms_leave_day 
                         inner join pms_leave on pms_leave.leave_id= pms_leave_day.leave_id 
                         inner join pms_leave_types as t on t.leave_id = pms_leave_day.leave_period 
                         inner join pms_legends as l on l.leg_id = t.leave_id 
                         WHERE pms_leave_day.leave_id = " . intval($id);

                        
                      
               
                      
                        $command = Yii::app()->db->createCommand($sql3);
                        $command->execute();
                        $data = $command->queryAll();

                        foreach($data as $dat) {

                            $transaction = Yii::app()->db->beginTransaction();
                            try {

                                $exitatt = Yii::app()->db->createCommand("SELECT count(*) as manual FROM `pms_attendance` WHERE `user_id`= " . $dat['user_id'] . " and `att_date`='" . $dat['att_date'] . "' and type=2")->queryScalar();
                                if ($exitatt == 0) {

                                    Yii::app()->db->createCommand("Delete  FROM `pms_attendance` WHERE `user_id`=" . $dat['user_id'] . " and `att_date`='" . $dat['att_date'] . "'")->execute();

                                    $new = new Attendance();
                                    $new->attributes = $dat;
                                    $new->comments = 'From Leave request';
                                    $new->save();
                                    $newatt_id = $new->att_id;

                                    // Yii::app()->db->createCommand("Delete  FROM `pms_attendance` WHERE `user_id`=" . $dat['user_id'] . " and `att_date`='" . $dat['att_date'] . "' and att_id<>$newatt_id")->execute();
                                }

                                $newrry = Yii::app()->db->createCommand("SELECT pt_hp FROM pms_shift_assign
                                  left join pms_employee_shift on  pms_employee_shift.shift_id = pms_shift_assign.shift_id
                                  WHERE user_id=".$dat['user_id']." and att_date = '".$dat['att_date']."' ")->queryRow();

                                $type   = 0;
                                if($newrry['pt_hp'] != ''){
                                  $type = $newrry['pt_hp'];
                                }

                               Yii::app()->db->createCommand("DELETE FROM pms_time_attendance WHERE user_id = ".$dat["user_id"]." and att_date = '".$dat["att_date"]."' ")->execute();

                                $modelatt             = new TimeAttendance;
                                $modelatt->attributes = $dat;
                                $modelatt->comments   = 'From Leave request';
                                $modelatt->att_time   = $type;
                                $modelatt->type       = 2;
                                $modelatt->save();

                                $transaction->commit();
                            } catch (Exception $e) {
                               $transaction->rollBack();
                            }
                        }

                        //end insert
                    }

                    if($sts == 10){ //reject
                       

                       $check =   Yii::app()->db->createCommand("SELECT * FROM `pms_leave` WHERE `leave_id` = ".intval($id))->queryRow();



                       $sql3 = "select leave_date as att_date ,leg_id as att_entry,emp_id as user_id,created_date,updated_date as modified_date,created_by from pms_leave_day 
                       inner join pms_leave on pms_leave.leave_id= pms_leave_day.leave_id 
                       inner join pms_leave_types as t on t.leave_id = pms_leave_day.leave_period 
                       inner join pms_legends as l on l.leg_id = t.leave_id 
                       WHERE pms_leave_day.leave_id = " . intval($id);

                       $command = Yii::app()->db->createCommand($sql3);
                       $command->execute();
                       $data = $command->queryAll();

            
                          foreach($data as $dat){

                            // $this->PunchReportManual(date('Y-m-d', strtotime($dat['att_date'])), $userid=$check['emp_id'] );

                            Yii::app()->user->setState('emp_ids', array($check['emp_id']));
                            $this->PunchReportManual(date('Y-m-d', strtotime($dat['att_date'])),$check['emp_id']);

                            $sql = "SELECT * FROM `pms_attendance_reject_log` WHERE `user_id` =".$dat['user_id']." and `att_date` ='".$dat['att_date']."' ORDER BY att_id DESC LIMIT 1,1";
                            $log_value   = Yii::app()->db->createCommand($sql)->queryRow();
                            // print_r( $log_value); exit;
                            if($log_value!=""){
                                $entry=$log_value['att_entry'];
                                $type=$log_value['type'];
                                $update_sql = "UPDATE `pms_attendance` SET `att_entry`=".$entry.",`type`=2  where `user_id`= ".$dat['user_id']." and att_date='".$dat['att_date']."'";
                                Yii::app()->db->createCommand($update_sql)->query();
                            }
                            else{
                                $update_sql = "delete from `pms_attendance`  where `user_id`= ".$dat['user_id']." and att_date='".$dat['att_date']."'";
                                Yii::app()->db->createCommand($update_sql)->query();
                           
                            }

                           }

                    }

                    $finalstatus = trim($req, 'e') . 'ed';
                    $ret['status'] = $finalstatus;
                    $ret['msg'] = 'Leave(s) ' . $finalstatus . ' Successfully';
                }

                echo json_encode($ret);
                exit;
            }
        }
    }

    public function actionManageLeaves() {
        $model = new Leave('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Leave']))
            $model->attributes = $_GET['Leave'];

        $this->render('manageleaves', array(
            'model' => $model,
        ));
    }

    public function actionViewLeave($id) {
        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;
        //$usedetailmodel = EmploymentDetails::model()->find("userid = '{$model->emp_id}'");
        $usedetailmodel = Users::model()->find("emp_id = '{$model->emp_id}'");
        $usermodel = Users::model()->find("emp_id = '{$model->emp_id}'");
        //$communicationmodel = CommunicationDetails::model()->find("user_id = '{$model->emp_id}'");

        $this->render('viewleave', array(
            'model' => $model, 'usermodel' => $usermodel, 'usedetailmodel' => $usedetailmodel
        ));
    }

    public function actionCalcDays($getdays = '') {
        $return = array('status' => 0, 'result' => '<tr><td colspan="2">&nbsp;</td></tr>', 'error' => '', 'days' => 0, 'days_durations' => array());

        $input_data = $_GET;
        if ($getdays != '')
            $input_data = $getdays;

        if (count($input_data)) {
            extract($input_data);

            if ((isset($from) and $from != '') and ( isset($to) and $to != '')) {

                $fromdate = explode('/', $from);
                $todate = explode('/', $to);

                $fromtime = strtotime(implode('-', array_reverse($fromdate)));
                $tilltime = strtotime(implode('-', array_reverse($todate)));

                $from_date = implode('-', array_reverse($fromdate));
                $to_date = implode('-', array_reverse($todate));


                if ($fromtime > $tilltime) {
                    $return['error'] = 'Invalid date range';
                } else {

                    $return['sanderror'] = '';
                    $return['from_date'] = date('d/m/Y', $fromtime);
                    $return['date_to'] = date('d/m/Y', $tilltime);
                    $total_leaves = 0;

                    $days = (($tilltime - $fromtime) / 86400) + 1;
                    $row = array();

                    $leave_model = Yii::app()->db->createCommand(" SELECT leave_id,description FROM `pms_leave_types` as lt inner join pms_legends as lg on lg.leg_id= lt.`leave_id` ")->queryAll();

                    // $leave_duration = CHtml::listData($leave_model,'leave_id','description');

                    $leave_duration_sql="select leave_id,description,start_date from pms_leave_types
                    as lt left join pms_legends as lg on lg.leg_id= lt.`leave_id`
                    left join pms_earned_leave ON pms_earned_leave.leave_type=lt.leave_id where user_id=".$user;
                   
                    $leave_duration = Yii::app()->db->createCommand($leave_duration_sql)->queryAll();
                    
                    $leave_duration = CHtml::listData($leave_duration,'leave_id','description');

                   

                    for ($i = 0; $i < $days; $i++) {
                        $selected = (isset($duraton[$i]) ? $duraton[$i] : '');
                        $leavevalue = 0;
                        if ($selected == 7 or $selected == 8) {
                            $leavevalue = 0.5;
                        } else {
                            $leavevalue += 1;
                        }

                        $total_leaves += $leavevalue;

                        $datetime = $fromtime + (86400 * $i);

                        $select_html="";
                        $select_html.="<select name='duration[]' id='leave_type' class='form-control'";
                        foreach( $leave_duration as $row){
                            $select_html.="<option value='".$row['leave_id']."'>".$row['description']."</option>";
                        }
                        $select_html.="</select>";
                        $days_durations[$datetime] = array('leavetype' => $selected, 'value' => $leavevalue);

                      
                        $row[$i] = '<tr><td>' . date('d/m/Y', $datetime) . "</td>"
                                . "<td>" . CHtml::dropDownList('duration[]', $selected, $leave_duration ,array('empty' => 'Select Leave type', 'class'=>'form-control',
                                'id' => 'leave_type')) . "</td></tr>";
                    }

                    //leave conditions

                    $userid = Yii::app()->user->id;
                    $tblpx = Yii::app()->db->tablePrefix;
                    $lve_rules = array();

                    // $desg = Users::model()->findByPk($userid);
                    $desg = Users::model()->findByPk($userid);

                    // print_r($desg);

                    // if ($desg['designation'] != NULL) {

                    //     $lve_rules = leave_rules::model()->find("designation_id=" . $desg['designation']);
                    // }
                    $lve_rules ="";

                     $leavedays = $this->LeavedayCalc($fromdate, $todate);

                    if (!empty($lve_rules) && $lve_rules['sandwich_leave'] == 1) {

                        //sandwich leave calc days
                        if (!empty($leavedays)) {
                            //print_r( $leavedays);exit;
                            $total_leaves += $leavedays['days'];
                            $return['sanderror'] = 'Included Sandwich Leave';
                            $return['from_date'] = date('d/m/Y', strtotime($leavedays['from_date']));
                            $return['date_to'] = date('d/m/Y', strtotime($leavedays['to_date']));

                            $row[3] = '<tr><td>' . date('d/m/Y', strtotime($leavedays['from_date'])) . "</td>"
                                    . "<td>" . 'Included sandwitch leave.' . "</td></tr>";
                            $row[4] = '<tr><td>' . date('d/m/Y', strtotime($leavedays['to_date'])) . "</td>"
                                    . "<td>" . 'Included sandwitch leave.' . "</td></tr>";
                        }
                    }

                    if (!empty($lve_rules) && $lve_rules['continue_leave'] == 1) {

                        //continues lve
                        $contues_lves = $this->continuleaveCalc($fromdate, $todate);
                        if (!empty($contues_lves)) {

                            $total_leaves += $contues_lves['days'];
                            $return['sanderror'] = 'Included continues Leave';
                            $return['from_date'] = date('d/m/Y', strtotime($contues_lves['from_date']));
                            $return['date_to'] = date('d/m/Y', strtotime($contues_lves['to_date']));
                            $j = 3;
                            for ($i = 0; $i < $contues_lves['days']; $i++) {
                                $j++;
                                $row[$j] = '<tr><td>' . date('d/m/Y', strtotime($contues_lves['from_date'] . '+' . $i . ' days')) . "</td>"
                                        . "<td>" . 'Included Continues leaves.' . "</td></tr>";
                            }
                        }

                        //mnday lve after satday and sunday holidays consider 3
                        $mn_date = date('D', strtotime($from_date . ' -1 days '));

                        //staurday holiday checking
                        $holiday_list = $this->holidays_dates($from_date);
                        $sat_holi_mn = in_array(date('Y-m-d', strtotime($from_date . ' -2 days ')), $holiday_list);

                        if (empty($contues_lves) && $mn_date == 'Sun' && !empty($sat_holi_mn)) {
                            $total_leaves += 2;
                            $return['sanderror'] = 'Included Continues Leaves';
                            $return['from_date'] = date('d/m/Y', strtotime($from_date . ' -2 days '));
                            $return['date_to'] = date('d/m/Y', strtotime($to_date));
                            $date = date('d/m/Y', strtotime($from_date . ' -2 days '));
                            $j = 3;
                            for ($i = 0; $i < 2; $i++) {
                                $j++;
                                $row[$j] = '<tr><td>' . date('d/m/Y', strtotime($date . '+' . $i . ' days')) . "</td>"
                                        . "<td>" . 'Included Continues leaves.' . "</td></tr>";
                            }
                        }
                        //friday lve befor satday and sunday holidays consider 3
                        $fr_date = date('D', strtotime($to_date . ' +1 days '));
                        //staurday holiday checking
                        $sat_holi_fr = in_array(date('Y-m-d', strtotime($to_date . ' +1 days ')), $holiday_list);

                        if (empty($contues_lves) && $fr_date == 'Sat' && !empty($sat_holi_fr)) {

                            $total_leaves += 2;
                            $return['sanderror'] = 'Included Continue Leave';
                            $return['from_date'] = date('d/m/Y', strtotime($from_date));
                            $return['date_to'] = date('d/m/Y', strtotime($to_date . ' +2 days '));

                            $j = 3;
                            for ($i = 0; $i < 2; $i++) {
                                $j++;
                                $row[$j] = '<tr><td>' . date('d/m/Y', strtotime($to_date . '+' . $i . ' days')) . "</td>"
                                        . "<td>" . 'Included Continues leaves.' . "</td></tr>";
                            }
                        }

                        //continues lve before and after a holiday

                        if ($leavedays == null) {

                            if (in_array(date('Y-m-d', strtotime($from_date . ' -1 days ')), $holiday_list) || in_array(date('Y-m-d', strtotime($from_date . ' +1 days ')), $holiday_list)) {

                                if (in_array(date('Y-m-d', strtotime($from_date . ' -1 days ')), $holiday_list)) {
                                    $total_leaves += 1;
                                    $return['sanderror'] = 'Included contiues Leave';
                                    $return['from_date'] = date('d/m/Y', strtotime($from_date . ' -1 days '));
                                    $return['date_to'] = date('d/m/Y', strtotime($to_date));
                                    $row[3] = '<tr><td>' . date('d/m/Y', strtotime($from_date . ' -1 days ')) . "</td>"
                                            . "<td>" . 'Included continues leave.' . "</td></tr>";
                                } else {

                                    $total_leaves += 1;
                                    $return['sanderror'] = 'Included contiues Leave';
                                    $return['from_date'] = date('d/m/Y', strtotime($from_date));
                                    $return['date_to'] = date('d/m/Y', strtotime($to_date . ' +1 days '));
                                    $row[3] = '<tr><td>' . date('d/m/Y', strtotime($to_date . ' +1 days ')) . "</td>"
                                            . "<td>" . 'Included contiues leave.' . "</td></tr>";
                                }
                            }
                        }
                    }

                    $return['days'] = floatval($total_leaves);
                    $return['result'] = implode("\n", $row);
                    $return['days_durations'] = $days_durations;
                }
            } else {
                $return['error'] = 'Invalid dates';
            }
        } else {
            $return['error'] = 'Invalid dates';
        }

        if ($getdays == '') {
            echo json_encode($return);
        } else {
            return $return;
        }
        die;
    }

    public function LeavedayCalc($fromdate, $todate) {

        $from_date = implode('-', array_reverse($fromdate));
        $to_date = implode('-', array_reverse($todate));
        $holi_dates = array();

        $total = array();
        $from_date = date('Y-m-d', strtotime($from_date . '-1 days '));
        $to_date = date('Y-m-d', strtotime($to_date . ' +1 days '));
        $c = date('D', strtotime($to_date . ' +1 days '));

        $holi_dates = $this->holidays_dates($from_date);
        //1.check holiday befor from date and holiday after todate
        if (in_array($from_date, $holi_dates) && in_array($to_date, $holi_dates)) {

            $date1 = date_create($from_date);
            $date2 = date_create($to_date);
            $diff = date_diff($date1, $date2);
            $days_count = $diff->format("%a");
            $total = array('days' => $days_count, 'from_date' => $from_date, 'to_date' => $to_date);
        }

        return $total;
    }

    public function continuleaveCalc($fromdate, $todate) {

        $from_date = implode('-', array_reverse($fromdate));
        $to_date = implode('-', array_reverse($todate));
        $total = array();


        $prev_lv_date = date('Y/m/d', strtotime($from_date . ' - 3 days'));
        //check condition previus leave
        $sqlavail = Yii::app()->db->createCommand("select date_from,date_to from {$tblpx}leave where approval_status=9 and date_to='$prev_lv_date' and emp_id=" . Yii::app()->user->id)->queryRow();
        $prev_lve_date = date('D', strtotime($sqlavail['date_to'] . ' + 2 days'));
        $cur_lve_date = date('D', strtotime($from_date . ' -1 days '));

        $totalholidays = $this->holidays_dates($from_date);
        //check sturday holiday
        $sat_holi = in_array(date('Y-m-d', strtotime($from_date . ' - 2 days')), $totalholidays);

        //prev leave on friday sat sun day off apply leve monday take 4lves
        if (!empty($sat_holi)) {
            if ($prev_lve_date == 'Sun' and $cur_lve_date == 'Sun') {

                $date1 = date_create($sqlavail['date_from']);
                $date2 = date_create($to_date);
                $diff = date_diff($date1, $date2);
                $days_count = $diff->format("%a");
                $total_leaves = $days_count;
                $total = array('days' => $total_leaves, 'from_date' => $sqlavail['date_from'], 'to_date' => $to_date);
                return $total;
            }
        }
    }

    public function holidays_dates($from_date) {

        $userid = Yii::app()->user->id;
        $tblpx = Yii::app()->db->tablePrefix;
        $holi_dates = array();
        $usertbl = Users::model()->tableSchema->rawName;

        $holidays = Yii::app()->db->createCommand("SELECT * FROM `pms_holidays` WHERE holiday_date >'" . date('Y-m-d', strtotime(date('Y-m-01'))) . "'")->queryAll();
        
        foreach ($holidays as $data) {

            $holi_dates[] = $data['date'];
        }

        //first day of current month
        $d = new DateTime('first day of this month');
        $first_day = $d->format('Y-m-d');
        //first saturday
        $saturday1 = date('d', strtotime('first saturday of ' . $first_day . ''));
        $saturday2 = date('Y-m-', strtotime('first saturday of ' . $first_day . ''));
        //total days in a month
        $total_days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($from_date)), date('Y', strtotime($from_date)));
        // $desg = Users::model()->findByPk($userid);
        $desg = Users::model()->findByPk($userid);
        if ($desg['designation'] != NULL) {
            // $lve_rules = leave_rules::model()->find("designation_id=" . $desg['designation']);
            // $working_satdays = $lve_rules['working_satdays'];
            // $arr = explode(',', $working_satdays);

            // $stalist = array();
            // $j = 0;
            // for ($i = $saturday1; $i <= $total_days; $i += 7) {
            //     $j++;
            //     if (in_array($j, $arr)) {
            //         continue;
            //     }
            //     $stalist[] = $saturday2 . $i;
            // }
            // //saturdays to holiday
            // foreach ($stalist as $st) {
            //     array_push($holi_dates, $st);
            // }
        }




        return $holi_dates;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;
        // $usedetailmodel = EmploymentDetails::model()->find("userid = '{$model->emp_id}'");
        $usedetailmodel = Users::model()->find("emp_id = '{$model->emp_id}'");
        // $usermodel = Users::model()->find("userid = '{$model->emp_id}'");
        $usermodel = Users::model()->find("emp_id = '{$model->emp_id}'");
        /* $communicationmodel = CommunicationDetails::model()->find("user_id = '{$model->emp_id}'"); */

        $this->render('view', array(
            'model' => $model, 'usermodel' => $usermodel, 'usedetailmodel' => $usedetailmodel
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionUserView($id) {
        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;
        // $usedetailmodel = EmploymentDetails::model()->find("userid = '{$model->emp_id}'");
        $usedetailmodel = Users::model()->find("emp_id = '{$model->emp_id}'");
        //$usermodel = Users::model()->find("userid = '{$model->emp_id}'");
        $usermodel = Users::model()->find("userid = '{$model->emp_id}'");
        $communicationmodel = CommunicationDetails::model()->find("user_id = '{$model->emp_id}'");

        $this->render('userview', array(
            'model' => $model, 'usermodel' => $usermodel, 'usedetailmodel' => $usedetailmodel, 'userid' => $model->emp_id
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate_old() {


        $model = new Leave;
        $leave_days_dur = array('result' => '', 'dur_type' => array());
        $datearray = array();

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);



        if (isset($_POST['Leave'])) {

            $model->attributes = $_POST['Leave'];
            $model->emp_id = Yii::app()->user->id;
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            $model->updated_date = date('Y-m-d H:i:s');
            $model->leave_submit_date = date('Y-m-d H:i:s');

            $date_from = explode('/', $model->date_from);
            $model->date_from = implode('-', array_reverse($date_from));

            $date_to = explode('/', $model->date_to);
            $model->date_to = implode('-', array_reverse($date_to));

            //  echo '<pre>';
            //var_dump($model->attributes);

            if ($model->validate()) {

                // print_r($model->getErrors());
                //  exit;
                $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => isset($_POST['duration']) ? $_POST['duration'] : '');

                $leave_days_dur['result'] = $this->actionCalcDays($datearray);


                /* check exist */
                $check = Leave::model()->find('date_from=:date_from
                and date_to=:date_to and emp_id=' . $model->emp_id.'and approval_status !=10', array(':date_from' => $model->date_from, ':date_to' => $model->date_to));

                if (!empty($check)) {

                    Yii::app()->user->setFlash('success', 'Leave already Applied for this date');
                    $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                } else {



                    /* end */
                    if ($model->save()) {
                        $leavereqid = Yii::app()->db->getLastInsertID();
                        $leavedayssql = 'insert into pms_leave_day (leave_day_id, leave_id, leave_date, leave_period, leave_value) values ';

                        $leavedays = '';

                        foreach ($leave_days_dur['result']['days_durations'] as $dtime => $dur_val) {


                            $leavedays .= "(NULL, $leavereqid,'" . date('Y-m-d', $dtime) . "','" . (intval($dur_val['leavetype']) != 0 ? $dur_val['leavetype'] : 6) . "'," . $dur_val['value'] . "),";
                        }
                        $leavedayssql .= trim($leavedays, ",");
                        Yii::app()->db->createCommand($leavedayssql)->query();

                        //email to line manger official email id

                        /*    $leaves      = Leave::model()->findByPk($leavereqid);
                          $userdetails =  Users::model()->findByPk(Yii::app()->user->id);
                          $settings    =  GeneralSettings::model()->findByPk(1);
                          $lv_settings = LeaveSetting::model()->findByPk($leavereqid);
                          $total_days  = LeaveDay::model()->find(
                          array(
                          "select" => array("sum(leave_value) as leave_value"),
                          "condition" => "leave_id=$leavereqid",
                          ))->leave_value;

                          $subject  =(!empty($settings->subject_apply))?$settings->subject_apply:'Leave request';
                          $name     = $userdetails['first_name'].' '.$userdetails['last_name'];

                          $var= array('{date_from}','{date_to}','{reason}','{emp_name}');

                          $data= array( $leaves['date_from'],$leaves['date_to']
                          ,$leaves['reason_for_leave'],$leaves['reason_for_leave'],$name);

                          $message  = str_replace($var,$data,nl2br($settings['mail_template_apply']));

                          $body    = $this->renderPartial('email',array('message'=>$message),true);

                          $mail = new JPhpMailer();

                          if($_SERVER['HTTP_HOST'] != 'localhost'){
                          $mail->IsSMTP();
                          $mail->Host         =  $settings->smtp_host;
                          $mail->SMTPSecure   =  $settings->smtp_secure;
                          $mail->SMTPAuth     =  $settings->smtp_auth;
                          $mail->Username     =  $settings->smtp_username;
                          $mail->Password     =  $settings->smtp_password;

                          $mail->setFrom( $settings->smtp_email_from,'Advenser App');
                          }else{

                          $mail->IsSMTP();
                          $mail->Host       = 'mail.advenser.net:587';
                          $mail->SMTPAuth   = true;
                          $mail->Username   = 'app@advenser.net';
                          $mail->Password   = 'Adv@App$2017';
                          $mail->SMTPSecure =  'tls';
                          $mail->setFrom('app@advenser.net','Advenser App');
                          }



                          if(!empty($userdetails) && !empty($userdetails->line_manager) && $settings->request_mail_to==2){
                          $linemanager =  Users::model()->findByPk($userdetails->line_manager);
                          $to_email    = $linemanager->email;
                          $CommunicationDetails = CommunicationDetails::model()->find('user_id='.$userdetails->line_manager);
                          }else if($settings->request_mail_to==1 && !empty($CommunicationDetails) && !empty($CommunicationDetails->personal_email_id)){
                          $to_email = $CommunicationDetails->personal_email_id;
                          }else{
                          $admin       = Users::model()->findByPk(1);
                          $to_email    = $admin->email;
                          }

                          // print_r($to_email);exit;
                          if($_SERVER['HTTP_HOST'] === 'localhost' or Yii::app()->user->role!==1){
                          $to_email='bala@bluehorizoninfotech.com';
                          }



                          $mail->addAddress($to_email);   // Add a recipient
                          $mail->isHTML(true);
                          $mail->Subject = $subject;
                          $mail->Body = $body;

                          if($mail->send()) {

                          $this->redirect(array('view', 'id' => $model->leave_id));
                          } else {

                          Yii::app()->user->setFlash('success','Error sending email, Please try again later.');
                          $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                          } */

                        $this->redirect(array('view', 'id' => $model->leave_id));
                    } else {

                        $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                    }
                }
            }
        }

        $this->render('create', array(
            'model' => $model, 'leave_days_dur' => $leave_days_dur, 'datearray' => $datearray
        ));
    }

    public function actionCreate() {


        $model = new Leave;
        $leave_days_dur = array('result' => '', 'dur_type' => array());
        $datearray = array();
        $requested_paid_leave=0;
        $requested_casual_leave=0;
        $requested_medical_leave=0;

        $this->performAjaxValidation($model);

        if (isset($_POST['Leave'])) {


            if (!empty($_POST['Leave']['emp_id'])) {
                $emp_id = (!empty($_POST['Leave']['emp_id'])) ? $_POST['Leave']['emp_id'] : '';
            } else {
                $emp_id = Yii::app()->user->id;
            }



            $model->attributes = $_POST['Leave'];
            $request_array=array();

            $flag=0;
            if(isset($_POST['duration']))
            {
                $leave_items=$_POST['duration'];
                for($i=0;$i<count($leave_items);$i++)
                {
                    if(empty($leave_items[$i])) 
                    {
                        $flag=1;  
                    }
                    elseif($leave_items[$i]==2)
                    {
                        $requested_paid_leave++;
                        $request_array[2]=$requested_paid_leave;
                    }
                    elseif($leave_items[$i]==10)
                    {
                        $requested_medical_leave++;
                        $request_array[10]=$requested_medical_leave;
                    }
                    elseif($leave_items[$i]==11)
                    {
                        $requested_casual_leave++;
                        $request_array[11]=$requested_casual_leave;
                    }
                }
            }
            else{
                $flag=1; 
            }

           
          
            
            //$model->emp_id = Yii::app()->user->id;
            $model->emp_id = $emp_id;
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            $model->updated_date = date('Y-m-d H:i:s');
            $model->leave_submit_date = date('Y-m-d H:i:s');

            $date_from = explode('/', $model->date_from);
            $model->date_from = implode('-', array_reverse($date_from));

            $date_to = explode('/', $model->date_to);
            $model->date_to = implode('-', array_reverse($date_to));



            if ($model->validate()) {


                if ($_POST['Leave']['calcday'] == 0) {

                    Yii::app()->user->setFlash('error', 'Calculate days');
                }
                elseif($flag==1) 
                {
                    Yii::app()->user->setFlash('success', 'Please select leave type');
                }
                
                else {

                    $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => isset($_POST['duration']) ? $_POST['duration'] : '');
                    $leave_days_dur['result'] = $this->actionCalcDays($datearray);




                    /*  earned leave calculation   20 */

                for ($i=0;$i<count($request_array);$i++){
                    $array_keys=(array_keys($request_array));
                    
                    $remain = 0;
                    $utilised = 0;

                    $leavtype = Yii::app()->db->createCommand(" SELECT leg_id,description FROM pms_legends where leg_id = " . $array_keys[$i])->queryRow();

                    $leave_sql="SELECT count(*) as count FROM `pms_leave` 
                    left join pms_leave_day as t on  t.leave_id=pms_leave.leave_id
                    WHERE `emp_id`=" .  $model->emp_id . " and t.leave_period= ".$array_keys[$i]." and (`approval_status`= 9 or `approval_status`= 11  ) and date_from BETWEEN '".date('Y-m-01')."' AND '" .date('Y-m-t')."'";
                  
                    $data = Yii::app()->db->createCommand($leave_sql )->queryRow();
                    if ($data['count'] > 0) {
                        $utilised_leave = $data['count'];
                    } else {
                        $utilised_leave = 0;
                    }
                    $availcheck = Yii::app()->db->createCommand("SELECT * FROM `pms_avail_leave` WHERE `userid`=" . $model->emp_id ." and `type`=".$array_keys[$i])->queryRow();

                    $considarable_leave=$availcheck['consi_leave'];

                    $remain_leave = $considarable_leave - $utilised_leave;
                    if ($remain_leave < 0) {
                        $remain_leave = 0;
                    }

                    if ($remain_leave == 0)  {

                        Yii::app()->user->setFlash('success', ' Leave not available');
                        $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                        $this->redirect(array('create'));
                    }



                    $leave_sql="SELECT count(*) as count FROM `pms_leave` 
                    left join pms_leave_day as t on  t.leave_id=pms_leave.leave_id
                    WHERE `emp_id`=" .  $model->emp_id . " and t.leave_period= ".$array_keys[$i]." and (`approval_status`= 9 or `approval_status`= 11) and date_from BETWEEN '".date('Y-m-01')."' AND '" .date('Y-m-t')."'";
                  
                    
                    
                    $leavedata = Yii::app()->db->createCommand($leave_sql )->queryRow();
                   
                    /* utilised earnd leave */
                    if (!empty($leavedata) and $leavedata['count'] > 0) {
                        $utilised = $leavedata['count'];
                    } else {
                        $utilised = 0;
                    }

                    /* end */


                    /* remaining lve */

                    // $remain  = $availcheck['actual_el'] - $utilised;

                    $remain = $availcheck['consi_leave'] - $utilised;


                    if ( $remain <= 0) {

                        Yii::app()->user->setFlash('success', 'Earned Leave not Applicable');
                        $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                        $this->redirect(array('create'));
                    }

                
                    if ( $remain > 0 and $request_array[$array_keys[$i]] > $remain) {

                        Yii::app()->user->setFlash('success', 'Available Earned Leave is less than requested leave day(s)');
                        $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                        $this->redirect(array('create'));
                    }

                }

                    $check = Leave::model()->find('date_from=:date_from
                    and date_to=:date_to and emp_id=' . $model->emp_id.' and approval_status !=10', array(':date_from' => $model->date_from, ':date_to' => $model->date_to));

                    if (!empty($check)) {

                        Yii::app()->user->setFlash('success', 'Leave already Applied for this date');
                        $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                    } else {


                        if ($model->save()) {

                            $leavereqid = Yii::app()->db->getLastInsertID();
                            $leavedayssql = 'insert into pms_leave_day (leave_day_id, leave_id, leave_date, leave_period, leave_value) values ';

                            $leavedays = '';

                            foreach ($leave_days_dur['result']['days_durations'] as $dtime => $dur_val) {


                                $leavedays .= "(NULL, $leavereqid,'" . date('Y-m-d', $dtime) . "','" . (intval($dur_val['leavetype']) != 0 ? $dur_val['leavetype'] : 6) . "'," . $dur_val['value'] . "),";
                            }
                            $leavedayssql .= trim($leavedays, ",");
                            Yii::app()->db->createCommand($leavedayssql)->query();


                            /* new pms shift supervisor */

                        
                     
                        $sql3 = "select leave_date as att_date ,leg_id as att_entry,emp_id as user_id,created_date,updated_date as modified_date,created_by from pms_leave_day 
                        inner join pms_leave on pms_leave.leave_id= pms_leave_day.leave_id 
                        inner join pms_leave_types as t on t.leave_id = pms_leave_day.leave_period 
                        inner join pms_legends as l on l.leg_id = t.leave_id 
                        WHERE pms_leave_day.leave_id = " . intval($leavereqid);

                       $command = Yii::app()->db->createCommand($sql3);
                       $command->execute();
                       $data = $command->queryAll();

                       foreach($data as $dat) {

                                    $dat['att_entry']=9;
                                    // print_r($dat); exit;
                                   Yii::app()->db->createCommand("Delete  FROM `pms_attendance` WHERE `user_id`=" . $dat['user_id'] . " and `att_date`='" . $dat['att_date'] . "'")->execute();
                                    // $dat['leave_period']
                                   $new = new Attendance();
                                   $new->attributes = $dat;
                                   $new->comments = 'From Leave request';
                                   $new->save();
                                   $newatt_id = $new->att_id;
                       }

                            $shiftdata = Yii::app()->db->createCommand("SELECT * FROM `pms_shift_assign` WHERE `user_id`=" . $emp_id . " and `att_date`='" . date('Y-m-d') . "' ")->queryRow();

                            if( isset(Yii::app()->user->company_id) && Yii::app()->user->company_id!='' ){
                                 $shiftsupervisors = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `shift_supervisor` = 1  and company_id =" . Yii::app()->user->company_id . " ORDER BY `emp_id` ASC")->queryAll();

                            }else{
                               $shiftsupervisors = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `shift_supervisor` = 1  ORDER BY `emp_id` ASC")->queryAll();

                            }



                            $userids = array();
                            if (!empty($shiftsupervisors) && !empty($shiftdata)) {
                                foreach ($shiftsupervisors as $key => $value) {


                                    $data = Yii::app()->db->createCommand("SELECT * FROM `pms_shift_assign` WHERE `user_id`=" . $value['emp_id'] . " and `att_date`='" . date('Y-m-d') . "' and shift_id =" . $shiftdata['shift_id'])->queryRow();
                                    if ($data != '' && $value['emp_id'] != $emp_id) {

                                        Yii::app()->db->createCommand(" INSERT INTO `pms_leave_shift`(`id`, `leave_id`, `emp_id`, `shift_supervisor`, `created_date`) VALUES (null, " . $leavereqid . " ," . $emp_id . ", " . $value['emp_id'] . ",'" . date('Y-m-d') . "')")->execute();
                                    }
                                }
                            }



                            /* end */
                            $enable_send_email = 0;

                            //email to reporting officer or admin

                            if ($enable_send_email == 1) {

                                $leaves = Leave::model()->findByPk($leavereqid);
                                $userdetails = Users::model()->findByPk($model->emp_id);
                                $settings = GeneralSettings::model()->findByPk(1);
                                $lv_settings = LeaveSetting::model()->findByPk($leavereqid);
                                $total_days = LeaveDay::model()->find(
                                                array(
                                                    "select" => array("sum(leave_value) as leave_value"),
                                                    "condition" => "leave_id=$leavereqid",
                                        ))->leave_value;

                                $subject = (!empty($settings->subject_apply)) ? $settings->subject_apply : 'Leave request';
                                $name = $userdetails['first_name'] . ' ' . $userdetails['last_name'];

                                $var = array('{date_from}', '{date_to}', '{reason}', '{emp_name}');

                                $data = array($leaves['date_from'], $leaves['date_to']
                                    , $leaves['reason_for_leave'], $name);

                                $message = str_replace($var, $data, nl2br($settings['mail_template_apply']));

                                $body = $this->renderPartial('email', array('message' => $message), true);
                                if ($this->paramsval('email_notify')) {
                                    $smtpdetails = $this->paramsval('smtpmailconfig');
                                    extract($smtpdetails);

                                    $mail = new JPhpMailer();
                                    if ($_SERVER['HTTP_HOST'] != 'localhost') {
                                        $mail->IsSMTP();
                                        $mail->Host = $settings->smtp_host;
                                        $mail->SMTPSecure = $settings->smtp_secure;
                                        $mail->SMTPAuth = $settings->smtp_auth;
                                        $mail->Username = $settings->smtp_username;
                                        $mail->Password = $settings->smtp_password;

                                        $mail->setFrom($settings->smtp_email_from, $smtpmailfromname);
                                    } else {
                                        $mail->IsSMTP();
                                        $mail->Host = $mailHost;
                                        $mail->SMTPAuth = $mailSMTPAuth;
                                        $mail->Username = $mailUsername;
                                        $mail->Password = $mailPassword;
                                        $mail->SMTPSecure = $mailSMTPSecure;
                                        $mail->setFrom($mailsetFrom, $name);
                                    }

                                    if (!empty($userdetails->report_to)) {

                                        $useremail = Users::model()->findByPk($userdetails->report_to);
                                        $to_email = $useremail->email;
                                    } else {

                                        $admin = Users::model()->findByPk(1);
                                        $to_email = $admin->email;
                                    }



                                    /* if($_SERVER['HTTP_HOST'] === 'localhost' or Yii::app()->user->role!==1){
                                      $to_email='bala@bluehorizoninfotech.com';
                                      } */

                                    if (empty($to_email)) {
                                        $to_email = 'anju.ba@bluehorizoninfotech.com';
                                    }


                                    // $to_email = 'anju.ba@bluehorizoninfotech.com';

                                    $mail->addAddress($to_email);   // Add a recipient
                                    $mail->isHTML(true);
                                    $mail->Subject = $subject;
                                    $mail->Body = $body;
                                    if ($mail->send()) {
                                        $mailsentflag = 1;
                                    }
                                }


                                if (isset($mailsentflag)) {

                                    $logmodel = new MailLog;
                                    $logmodel->send_to = $model->emp_id;
                                    $logmodel->send_by = Yii::app()->user->id;
                                    $logmodel->send_date = date('Y-m-d H:i:s');
                                    $logmodel->created_by = Yii::app()->user->id;
                                    $logmodel->created_date = date('Y-m-d');
                                    $logmodel->description = 'Leave Request';
                                    $logmodel->message = 'Mail sent';
                                    $logmodel->save();

                                    $this->redirect(array('view', 'id' => $model->leave_id));
                                } else {

                                    $logmodel = new MailLog;
                                    $logmodel->send_to = $model->emp_id;
                                    $logmodel->send_by = Yii::app()->user->id;
                                    $logmodel->send_date = date('Y-m-d H:i:s');
                                    $logmodel->created_by = Yii::app()->user->id;
                                    $logmodel->created_date = date('Y-m-d');
                                    $logmodel->description = 'Leave Request';
                                    $logmodel->message = 'Mail sent failed';
                                    $logmodel->save();
                                    //Yii::app()->user->setFlash('success','Error sending email, Please try again later.');
                                    $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                                }
                            } else {
                                $this->redirect(array('view', 'id' => $model->leave_id));
                            }

                            /*  email end */
                            // $this->redirect(array('view', 'id' => $model->leave_id));
                        } else {

                            $datearray = array('from' => $model->date_from, 'to' => $model->date_to, 'duraton' => $_POST['duration']);
                        }
                    }
                }
            }
        }

        $this->render('create', array(
            'model' => $model, 'leave_days_dur' => $leave_days_dur, 'datearray' => $datearray
        ));
    }

    public function actiongetleavesummary() {
        if (isset($_POST)) {
            $userid = $_POST['id'];
            $utilised_paid_leave = 0;
            $remain_paid_leave = 0;
            $utilised_casual_leave=0;
            $remain_casual_leave=0;
            $utilised_medical_leave=0;
            $remain_medical_leave=0;
            $actual_el = 0;
            $paid_leave=0;
            $casual_leave=0;
            $medical_leave=0;

            if (!empty($userid)) {

                $tblpx = Yii::app()->db->tablePrefix;

                $user = Yii::app()->db->createCommand("select concat_ws(' ',first_name,last_name) as fullname from pms_users where userid=" . $userid)->queryRow();

                $avlleaves = Yii::app()->db->createCommand("select concat_ws(' ',first_name,last_name) as fullname,le.* from " . $tblpx . "avail_leave  as le
                   inner join pms_users as em on em.userid = le.userid where le.userid=" . $userid)->queryAll();
                if (count($avlleaves)) {
                   foreach($avlleaves as $my_leave)
                   {
                       if($my_leave['type']==2)
                       {
                           $paid_leave=$my_leave['consi_leave'];
                       }
                       if($my_leave['type']==10)
                       {
                           $casual_leave=$my_leave['consi_leave'];
                       }
                       if($my_leave['type']==11)
                       {
                           $medical_leave=$my_leave['consi_leave'];
                       }
                   }

                }

                //paid leave
                $leave_sql="SELECT count(*) as count FROM `pms_leave` 
                left join pms_leave_day as t on  t.leave_id=pms_leave.leave_id
                WHERE `emp_id`=" .  $userid . " and t.leave_period= 2 and (`approval_status`= 9 or `approval_status`= 11) and date_from BETWEEN '".date('Y-m-01')."' AND '" .date('Y-m-t')."'";
               
               
               $data = Yii::app()->db->createCommand($leave_sql )->queryRow();
                if ($data['count'] > 0) {
                    $utilised_paid_leave = $data['count'];
                } else {
                    $utilised_paid_leave = 0;
                }
                $remain_paid_leave = $paid_leave - $utilised_paid_leave;
                if ($remain_paid_leave < 0) {
                    $remain_paid_leave = 0;
                }

                // casuall
                $leave_sql="SELECT count(*) as count FROM `pms_leave` 
                left join pms_leave_day as t on  t.leave_id=pms_leave.leave_id
                WHERE `emp_id`=" .  $userid . " and t.leave_period= 10 and (`approval_status`= 9 or `approval_status`= 11) and date_from BETWEEN '".date('Y-m-01')."' AND '" .date('Y-m-t')."'";
               
                $data = Yii::app()->db->createCommand($leave_sql )->queryRow();
                if ($data['count'] > 0) {
                    $utilised_casual_leave = $data['count'];
                } else {
                    $utilised_casual_leave = 0;
                }
                
                $remain_casual_leave = $casual_leave - $utilised_casual_leave;
                if ($remain_casual_leave < 0) {
                    $remain_casual_leave = 0;
                }

                //medical
                $leave_sql="SELECT count(*) as count FROM `pms_leave` 
                left join pms_leave_day as t on  t.leave_id=pms_leave.leave_id
                WHERE `emp_id`=" .  $userid . " and t.leave_period= 11 and (`approval_status`= 9 or `approval_status`= 11) and date_from BETWEEN '".date('Y-m-01')."' AND '" .date('Y-m-t')."'";
                $data = Yii::app()->db->createCommand($leave_sql)->queryRow();
                
                if ($data['count'] > 0) {
                    $utilised_medical_leave = $data['count'];
                } else {
                    $utilised_medical_leave = 0;
                }
                
                $remain_medical_leave = $medical_leave - $utilised_medical_leave;
                if ($remain_medical_leave < 0) {
                    $remain_medical_leave = 0;
                }


            }

            $prevyear = date("Y", strtotime("-1 year"));
            $start_date = date($prevyear . '-04-01');
            $end_date = date('Y-03-31');

            if ($_POST['from'] != '') {
                $start_date = date('Y-m-d', strtotime($_POST['from']));
            }
            if ($_POST['to'] != '') {

                $end_date = date('Y-m-d', strtotime($_POST['to']));
            }

            // echo $start_date.'/'. $end_date;exit;

            $present = $this->getpresentdays($userid, $start_date, $end_date);
            $absent = $this->getabsentdays($userid, $start_date, $end_date);


            $data = array('name' => $user['fullname'], 'ear' => $paid_leave, 
            'rem' => $remain_paid_leave, 
            'prese' => $present, 
            'abse' => $absent, 
            'startdate' => $start_date,
            'enddate' => $end_date,
            'casual_leave'=>$casual_leave,
            'remain_casual_leave'=>$remain_casual_leave,
            'medical_leave'=>$medical_leave,
            'remain_medical_leave'=>$remain_medical_leave
            );
            echo json_encode($data);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Leave'])) {
            $model->attributes = $_POST['Leave'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->leave_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionUserleaves($id) {
        $model = new Leave('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Leave']))
            $model->attributes = $_GET['Leave'];

        $this->render('userleaves', array(
            'model' => $model, 'userid' => $id
        ));
    }

    /**
     * Lists all models.
     */
    public function actionAdminUserView($id) {
        $model = new Leave('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Leave']))
            $model->attributes = $_GET['Leave'];

        $this->render('userview', array(
            'model' => $model, 'userid' => $id
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        //echo Yii::app()->user->id;die;

        $model = new Leave('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Leave']))
            $model->attributes = $_GET['Leave'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Leave('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Leave']))
            $model->attributes = $_GET['Leave'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Leave::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'leave-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionResetpass() {

        $time = time();
        $time1 = md5($time);
        //print_r($time);die;

        $model = new ResetPass;
        $this->layout = "//layouts/login";

        if (isset($_POST['ResetPass'])) {
            $model->attributes = $_POST['ResetPass'];
            if ($model->validate()) {
                //print_r($model->email);die;

                $key = md5($model->email) . $time;

                // print_r($key);die;
                $var = $this->renderPartial('email', array('key' => $key), true);
                $mail = new JPhpMailer();

                $subject = "RESET PASSWORD!";
                $headers = "FESTOONRMS";
                $bodyContent = $var;

                //die($_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
                $server = (($_SERVER['HTTP_HOST'] == 'localhost') ? '0' : '1');
                //die($server);
                if ($server == 0) {
                    $mail->IsSMTP();
                    $mail->Host = SMTPHOST;
                    $mail->SMTPSecure = SMTPSECURE;
                    $mail->SMTPAuth = true;
                    $mail->Username = SMTPUSERNAME;
                    $mail->Password = SMTPPASS;
                }

                $mail->setFrom(EMAILFROM);
                $mail->addAddress($model->email);   // Add a recipient
                $mail->isHTML(true);

                $mail->Subject = "Reset Password";
                $mail->MsgHTML($bodyContent);
                $mail->Body = $bodyContent;

                if (!$mail->Send()) {
                    Yii::app()->user->setFlash('error', 'There was an error sending the message!');
                    $this->redirect(array('site/resetpass'));
                } else {
                    Yii::app()->user->setFlash('success', 'Message was sent successfully.');
                    $this->redirect(array('site/resetpass'));
                }
            }
        }

        $this->render('resetpass', array('model' => $model));
    }

    public function getpresentdays($userid = 0, $start, $end) {
      $tbl = Yii::app()->db->tablePrefix;
      $sql = " SELECT  SUM(IF(att_entry = 1, 1, 0)) AS present,SUM(IF(att_entry = 2, 1, 0)) AS paid_leave,SUM(IF(att_entry = 3, 1, 0)) AS half_present,  SUM(IF(att_entry = 4, 1, 0)) AS lop,SUM(IF(att_entry =5, 1, 0)) AS holiday,SUM(IF(att_entry =6, 1, 0)) AS sunday,SUM(IF(att_entry =7, 1, 0)) AS two_present,SUM(IF(att_entry =8, 1, 0)) AS single_punch FROM {$tbl}attendance WHERE user_id='$userid' AND  att_date BETWEEN '$start' AND '$end'";
      $attendance = Yii::app()->db->createCommand($sql)->queryRow();

      $present      = $attendance['present'];
      $paid_leave   = $attendance['paid_leave'];
      $half_present = $attendance['half_present'];
      $lop          = $attendance['lop'];
      $holiday      = $attendance['holiday'];
      $sunday       = $attendance['sunday'];
      $two_present  = $attendance['two_present'];
      $single_punch = $attendance['single_punch'];

      $present_days = $present + $paid_leave + $half_present/2+$two_present+$holiday+$sunday;

      return $present_days;
    }

    public function getabsentdays($userid, $start, $end) {
      $tbl = Yii::app()->db->tablePrefix;
      $sql = " SELECT  SUM(IF(att_entry = 1, 1, 0)) AS present,SUM(IF(att_entry = 10, 1, 0)) AS casual,SUM(IF(att_entry = 11, 1, 0)) AS medical,SUM(IF(att_entry = 2, 1, 0)) AS paid_leave,SUM(IF(att_entry = 3, 1, 0)) AS half_present,  SUM(IF(att_entry = 4, 1, 0)) AS lop,SUM(IF(att_entry =5, 1, 0)) AS holiday,SUM(IF(att_entry =6, 1, 0)) AS sunday,SUM(IF(att_entry =7, 1, 0)) AS two_present,SUM(IF(att_entry =8, 1, 0)) AS single_punch FROM {$tbl}attendance WHERE user_id='$userid' AND  att_date BETWEEN '$start' AND '$end'";
      $attendance = Yii::app()->db->createCommand($sql)->queryRow();

      $present      = $attendance['present'];
      $paid_leave   = $attendance['paid_leave'];
      $half_present = $attendance['half_present'];
      $lop          = $attendance['lop'];
      $holiday      = $attendance['holiday'];
      $sunday       = $attendance['sunday'];
      $two_present  = $attendance['two_present'];
      $single_punch = $attendance['single_punch'];
      $casual = $attendance['casual'];
      $medical = $attendance['medical'];

      $absent_days  = $lop + $half_present/2+$casual+$medical;
      return $absent_days;
    }

    public function actionattendancebydate() {

        if (isset($_POST)) {



            $start_date = date('Y-m-d', strtotime($_POST['from']));
            $end_date = date('Y-m-d', strtotime($_POST['to']));

            $userid = $_POST['userid'];

            $present = $this->getpresentdays($userid, $start_date, $end_date);
            $absent = $this->getabsentdays($userid, $start_date, $end_date);

            $data = array('datefrm' => date('d/m/Y', strtotime($start_date)), 'dateto' => date('d/m/Y', strtotime($end_date)), 'prese' => $present, 'abse' => $absent);
            echo json_encode($data);
        }
    }

    public function actiontest_old() {

        $sqlavail = Yii::app()->db->createCommand("SELECT * FROM `pms_leave_shift`")->queryAll();

        foreach ($sqlavail as $data) {

            $new = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `emp_id`=" . $data['emp_id'])->queryRow();
            $old = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `emp_id`=" . $data['shift_supervisor'])->queryRow();
            if ($new['company_id'] != $old['company_id']) {

                Yii::app()->db->createCommand("DELETE FROM `pms_leave_shift` WHERE `id`=" . $data['id'])->execute();
            }
        }
    }

    public function actionLeavecredit() {
    $connection = Yii::app()->db;

    // $data = Yii::app()->db->createCommand("SELECT emp_id FROM `pms_employee` where status=28")->queryAll();
    //
    // foreach($data as $val){
    //     $model = new BalanceLeave;
    //     $model->userid = $val['emp_id'];
    //     $model->cl_ml = 0;
    //     $model->earned = 0;
    //     $model->lieu = 0;
    //     $model->rh = 0;
    //     $model->ml = 0;
    //     $model->comment = '';
    //     $model->modified_by = 1;
    //     $model->modified_date = date('Y-m-d H:i:s');
    //     $model->save();
    //
    //
    // }





    if (isset($_POST['confirmsumitcredit'])) {

        //  echo '<pre>';print_r($_POST);exit;

        extract($_POST);
        $leaveavailtbl = '';

        if (isset($lc) and count($lc) > 0) {
            $transaction = $connection->beginTransaction();
            try {

                foreach ($lc as $uid => $cr) {
                    $sql = "select * from pms_balance_leave where userid=" . $uid;
                    $udata = $connection->createCommand($sql)->queryRow();

                    $clcr = (isset($cr['cl']) ? floatval($cr['cl']) : 0);
                    $elcr = (isset($cr['el']) ? floatval($cr['el']) : 0);
                    $rhcr = (isset($cr['rh']) ? floatval($cr['rh']) : 0);
                    $mlcr = (isset($cr['ml']) ? floatval($cr['ml']) : 0);

                    //add to leave log
                    $coment = 'Manual credit CL:' . $clcr . ', PL:' . $elcr . ' ,ML:' . $mlcr . ' <br />Avail Credits CL/ML:'
                            . ($udata['cl_ml'] + $clcr) . ' ,EL:' . ($udata['earned'] + $elcr) . ' ,RH:' . ($udata['ml'] + $mlcr);
                   
                    
                    $leavelog_data = array('cl' => $clcr, 'el' => $elcr, 'rh' => $rhcr,'ml' => $mlcr, 'coment' => $coment);
                    $this->addtoLeaveLog($uid, $leavelog_data);

                    $this->updateLeave($uid,$udata['earned'] + $elcr,2);
                    $this->updateLeave($uid,$udata['cl_ml'] + $clcr ,11);
                    $this->updateLeave($uid,$udata['ml'] + $mlcr,10);

                }

                Yii::app()->user->setFlash('success', "Leave(s) credited successfully ");
                $transaction->commit();
                $this->redirect(array('/leave/default/leavecredit'));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', "Some problem occured. Please contact technical team");
                // $message = $e->getMessage() . " - " . json_encode($_POST);
                // @mail('bala@bluehorizoninfotech.com', 'Error while credit leaves', $message);
                $transaction->rollback();
                $this->redirect(array('/leave/default/leavecredit'));
            }
        }
    }

    $sql = "select u.emp_id as userid,u.first_name,u.last_name,u.salutation,ur.label as role,a.cl_ml,earned,lieu,rh,ml from pms_users as u
            left join pms_balance_leave as a on u.emp_id=a.userid left join pms_employee_default_data as ur on ur.default_data_id=user_type where u.status=28 and u.emp_id!=-1 order by u.first_name asc,u.last_name asc";


    $sqlavail = $connection->createCommand($sql)->queryAll();


    $this->render('leavecredit', array('availleaves' => $sqlavail));
}

public function updateLeave($user_id,$leave,$leave_type)
{

    $available_leave=AvailLeave::model()->find(
        array(
            "select" => array("*"),
            "condition" => "userid=$user_id and type= $leave_type"
    ));
    if($available_leave=="")
    {
    $update_leave=new AvailLeave;
    $update_leave->userid=$user_id;
    $update_leave->actual_el=$leave;
    $update_leave->consi_leave=$leave;
    $update_leave->type=$leave_type;
    $update_leave->created_by=Yii::app()->user->id;
    $update_leave->created_date=date("Y-m-d");
    $update_leave->updated_date=date("Y-m-d");
    $update_leave->updated_by=-1;
    $update_leave->comment="Manual Update";
    $update_leave->save();
    }
    else{
        $available_leave_id= $available_leave['avail_leave_id'];
        $update_leave= AvailLeave::model()->findByPk($available_leave_id);
        $update_leave->userid=$user_id;
        $update_leave->actual_el=$leave;
        $update_leave->consi_leave=$leave;
        $update_leave->type=$leave_type;
        $update_leave->created_by=Yii::app()->user->id;
        $update_leave->created_date=date("Y-m-d");
        $update_leave->updated_date=date("Y-m-d");
        $update_leave->updated_by=-1;
        $update_leave->comment="Manual Update";
        $update_leave->save();
    
    }
}

public function updateCasualLeave($user_id,$leave)
{

}

public function actionEarnedLeave()
{
    $users = Yii::app()->db->createCommand()
    ->select('userid, first_name, last_name')
    ->from('pms_users')
    ->queryAll();



    $current_month_start=date('Y-m-01');
    $current_month_end=date('Y-m-t');
    $today=date('Y-m-d');
    
    $existing_user_array=$this->exisitingUsers($current_month_start,$current_month_end,1);

    foreach($users as $user)
    {
        $user_id=$user['userid'];
 
         if(!in_array($user['userid'],$existing_user_array)){
            $present_days=0;
            $present_days=$this->UserPresentDays($user['emp_id'], $current_month_start,$today);
            // if($present_days>=15)
            // {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                $cron="insert into pms_leave_cron (`date`,`status`,`comment`) values('".date("Y-m-d H:i:s")."',1,'cron start')";
                Yii::app()->db->createCommand($cron)->query();
       
                $data_array=array();
                $date=Date("Y-m-d H:i:s");
                $data_array[] =  "(NULL,'$user_id','{$current_month_start}','{$current_month_end}','1','1','{$date}',NULL,'{$date}',1)";
                $data_array = implode(',', $data_array);         
                $insert_sql="insert into pms_earned_leave (`id`,`user_id`,`start_date`,
                `end_date`,`status`,`action_by`,`created_date`,`updated_by`,`updated_date`,`el_count`) values".$data_array."";
                Yii::app()->db->createCommand($insert_sql)->query();
                $available_leave=AvailLeave::model()->find(
                    array(
                        "select" => array("*"),
                        "condition" => "userid=$user_id and type=2"
                    ));
                if($available_leave=="")
                {
                    $available_leave_model=new AvailLeave;
                    $available_leave_model->userid=$user_id;
                    $available_leave_model->actual_el=1;
                    $available_leave_model->consi_leave=1;
                    $available_leave_model->type=2;
                    $available_leave_model->created_by=-1;
                    $available_leave_model->created_date=date("Y-m-d");
                    $available_leave_model->updated_date=date("Y-m-d");
                    $available_leave_model->updated_by=-1;
                    $available_leave_model->comment="Created by cron";
                    $available_leave_model->save();

                }
                else{
                    $available_leave_id = $available_leave['avail_leave_id'];
                    $available_leave_model= AvailLeave::model()->findByPk($available_leave_id);
                    $available_leave_model->userid=$user_id;
                    $available_leave_model->actual_el=$available_leave_model->actual_el+1;
                    $available_leave_model->consi_leave=$available_leave_model->consi_leave+1;
                    $available_leave_model->type=2;
                    $available_leave_model->created_by=-1;
                    $available_leave_model->created_date=date("Y-m-d");
                    $available_leave_model->updated_date=date("Y-m-d");
                    $available_leave_model->updated_by=-1;
                    $available_leave_model->comment="Created by cron";
                    $available_leave_model->save();
                
                }
                $sql = "select * from pms_balance_leave where userid=" . $user_id;
                $udata = Yii::app()->db->createCommand($sql)->queryRow();
  
                $paid_leave=$available_leave_model->consi_leave  ;
                $casual_leave=0; 
                $medical_leave=0; 
                $coment = 'Cron credit CL:' . $casual_leave . ', PL:' . $paid_leave .',ML:'.$medical_leave.' <br />Avail Credits CL/ML:'
                        . ($udata['cl_ml'] + $casual_leave) . ' ,EL:' . ($udata['earned'] + $paid_leave) . 'ML:'.($udata['ml'] + $medical_leave);
                $leavelog_data = array('cl' => $casual_leave, 'el' => $paid_leave, 'rh' => 0,'ml' => $medical_leave, 'coment' => $coment);
                $this->addtoLeaveLog($user_id, $leavelog_data);
                $transaction->commit();
            } catch (Exception $e) {
               $transaction->rollback();
               $cron="insert in to pms_leave_cron (`date`,`status`,`comment`) values('".date("Y-m-d H:i:s")."',1,'crone_error')";
                Yii::app()->db->createCommand($cron)->query();
            }
            $cron="insert into pms_leave_cron (`date`,`status`,`comment`) values('".date("Y-m-d H:i:s")."',2,'cron completed')";
            Yii::app()->db->createCommand($cron)->query();
   
            // }
            // check medical leave creted

            // $existing_user_array_casual=$this->exisitingUsers($current_month_start,$current_month_end,"2");
            // // print_r($existing_user_array_casual); exit;
            // if(!in_array($user['emp_id'],$existing_user_array_casual)){

            //     $data_array=array();
            //     $date=Date("Y-m-d H:i:s");
            //     $data_array[] =  "(NULL,'$user_id','{$current_month_start}','{$current_month_end}','2','1','{$date}',NULL,'{$date}',1)";
            //     $data_array = implode(',', $data_array);         
            //     $insert_sql="insert into pms_earned_leave (`id`,`user_id`,`start_date`,
            //     `end_date`,`status`,`action_by`,`created_date`,`updated_by`,`updated_date`,`el_count`) values".$data_array."";
            //     Yii::app()->db->createCommand($insert_sql)->query();

            //     // echo $insert_sql; exit;

            // $available_medical_leave=AvailLeave::model()->find(
            //     array(
            //         "select" => array("*"),
            //         "condition" => "userid=$user_id and type=11"
            // ));
            // if($available_medical_leave=="")
            // {
            //     $medical_leave=new AvailLeave;
            //     $medical_leave->userid=$user_id;
            //     $medical_leave->actual_el=1;
            //     $medical_leave->consi_leave=1;
            //     $medical_leave_consider=1;
            //     $medical_leave->type=11;
            //     $medical_leave->created_by=-1;
            //     $medical_leave->created_date=date("Y-m-d");
            //     $medical_leave->updated_date=date("Y-m-d");
            //     $medical_leave->updated_by=-1;
            //     $medical_leave->comment="Created by cron";
            //     $medical_leave->save();
            // }
            // else{
            //     $available_leave_id= $available_medical_leave['avail_leave_id'];
            //     $medical_leave= AvailLeave::model()->findByPk($available_leave_id);
            //     $medical_leave->userid=$user_id;
            //     $medical_leave->actual_el=1;
            //     $medical_leave->consi_leave=1;
            //     $medical_leave_consider=0;
            //     $medical_leave->type=11;
            //     $medical_leave->created_by=-1;
            //     $medical_leave->created_date=date("Y-m-d");
            //     $medical_leave->updated_date=date("Y-m-d");
            //     $medical_leave->updated_by=-1;
            //     $medical_leave->comment="Created by cron";
            //     $medical_leave->save();

            // }

            // $available_casual_leave=AvailLeave::model()->find(
            //     array(
            //         "select" => array("*"),
            //         "condition" => "userid=$user_id and type=10"
            // ));
            // if($available_casual_leave=="")
            // {
            // $casual_leave=new AvailLeave;
            // $casual_leave->userid=$user_id;
            // $casual_leave->actual_el=1;
            // $casual_leave->consi_leave=1;
            // $casual_consi_leave=1;
           
            // $casual_leave->type=10;
            // $casual_leave->created_by=-1;
            // $casual_leave->created_date=date("Y-m-d");
            // $casual_leave->updated_date=date("Y-m-d");
            // $casual_leave->updated_by=-1;
            // $casual_leave->comment="Created by cron";
            // $casual_leave->save();
            // }
            // else{
            //     $available_leave_id= $available_casual_leave['avail_leave_id'];
            //     $casual_leave= AvailLeave::model()->findByPk($available_leave_id);
            //     $casual_leave->userid=$user_id;
            //     $casual_leave->actual_el=1;
            //     $casual_leave->consi_leave=1;
            //     $casual_consi_leave=0;
            //     $casual_leave->type=10;
            //     $casual_leave->created_by=-1;
            //     $casual_leave->created_date=date("Y-m-d");
            //     $casual_leave->updated_date=date("Y-m-d");
            //     $casual_leave->updated_by=-1;
            //     $casual_leave->comment="Created by cron";
            //     $casual_leave->save();
            
            // }

            //  // leave log_update
          

            //  // leave_log end

            // }

         }
    }

}

public function UserPresentDays($userid,$start,$end)
{
  
    $total_att=0;

    $tbl = Yii::app()->db->tablePrefix;
    $totals = Yii::app()->db->createCommand("SELECT *  FROM {$tbl}attendance WHERE  user_id='$userid' AND  att_date BETWEEN '$start' AND '$end' ")->queryAll();
    foreach(  $totals as  $total)
    {
      if($total['att_entry']==1)
      {
       
          $total_att++;
      }
      elseif($total['att_entry']==3)
      {
        
          $total_att=$total_att+.5;

      }
    
      elseif($total['att_entry']==7)
      {
    
           $total_att=$total_att+1;

      }
    
    }
    return $total_att;
}

public function exisitingUsers($start_date,$end_date,$type)
{

    $user_sql="select user_id from pms_earned_leave where 
    start_date >='".$start_date."' and end_date <= '".$end_date."'
    and status=".$type;
    
    $users=Yii::app()->db->createCommand($user_sql)->queryAll();
    
    $user_id=array();
    foreach($users as $user)
    {
        $user_id[]=$user['user_id'];
    }
    return $user_id;

}

}
