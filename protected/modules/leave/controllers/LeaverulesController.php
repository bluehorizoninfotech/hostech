<?php

class LeaverulesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}



	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{    
         
           //$model = $this->loadModel($id);

            $levrules = Yii::app()->db->createCommand("select rule_id,designation,working_satdays,sandwich_leave,sunday_leave,continue_leave FROM hrms_leave_rules INNER JOIN hrms_designation on hrms_designation.designation_id= hrms_leave_rules.designation_id where rule_id=".$id )->queryRow();

           //print_r($sqlavail);exit;

          $this->render('view',array('model'=>$levrules));
	}

	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	

	/**
	 * Manages all models.
	 */
	public function actionIndex($id=0)
	{   
         
        if ($id == 0) {
            $model2 = new leave_rules;
            $msg = 'Successfully added new leave rules.';
        } else {
            $model2 = $this->loadModel($id);
            $msg = 'Successfully updated.';
        }
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model2);
        if (isset($_POST['leave_rules'])) {

             $model2->attributes = $_POST['leave_rules'];

            if(isset($_POST['working_satdays'])){ 
                $sat_days= $_POST['working_satdays'];
            	$model2->working_satdays = implode(',', $sat_days);
             }else{$model2->working_satdays =NULL;};
             if(isset($_POST['sunday_leave'])){ 
                $sun_days= $_POST['sunday_leave'];
              	$model2->sunday_leave = implode(',', $sun_days);
               }else{$model2->sunday_leave =NULL;};
               
            if($model2->save()){
                Yii::app()->user->setFlash('success', $msg);
                $this->redirect(array('index'));
            }
          

           
        }
		
		$model=new leave_rules('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['leave_rules']))
			$model->attributes=$_GET['leave_rules'];

		$this->render('index',array(
			'model'=>$model,'model2'=>$model2
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=leave_rules::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='leave-rules-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
