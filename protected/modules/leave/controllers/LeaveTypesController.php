<?php

class LeaveTypesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{

		$accessArr = array();
		$accessauthArr = array();
		$accessguestArr = array();
		$module= Yii::app()->controller->module->id;
		$controller_id = Yii::app()->controller->id;
		$controller = $module.'/'.$controller_id;

		if(isset(Yii::app()->session['pmsmenuall'])){
				if(array_key_exists($controller, Yii::app()->session['pmsmenuall'])){
						$accessArr = Yii::app()->session['pmsmenuall'][$controller];
				}
		}

		if(isset(Yii::app()->session['pmsmenuauth'])){
				if(array_key_exists($controller, Yii::app()->session['pmsmenuauth'])){
						$accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
				}
		}

		if (isset(Yii::app()->session['pmsmenuguest'])) {
				if(array_key_exists($controller, Yii::app()->session['pmsmenuguest'])){
						$accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
				}
		}
		$access_privlg = count($accessauthArr);

		return array(


				array(
						'allow',
						'actions' => $accessArr,
						'users' => array('@'),
						//'expression' => "$access_privlg > 0",

				),
				array(
						'allow',
						'actions' => $accessauthArr,
						'users' => array('@'),
						'expression' => "$access_privlg > 0",

				),


				array(
						'deny',
						'users' => array('*'),
				),

		);



			// return array(
			// 	array('allow',  // allow all users to perform 'index' and 'view' actions
			// 		'actions'=>array('index','view'),
			// 		'users'=>array('@'),
			// 	),
			// 	array('allow', // allow authenticated user to perform 'create' and 'update' actions
			// 		'actions'=>array('create','update'),
			// 		'users'=>array('@'),
			// 	),
			// 	array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 		'actions'=>array('admin','delete'),
			// 		'users'=>array('admin'),
			// 	),
			// 	array('deny',  // deny all users
			// 		'users'=>array('*'),
			// 	),
			// );


	}

  public function actionIndex()
	{
		   $lmodel=new LeaveTypes;
	     if(isset($_POST['LeaveTypes'])){
					$lmodel->attributes=$_POST['LeaveTypes'];
					if($lmodel->save())
						$this->redirect(array('index'));
			}

			$model=new LeaveTypes('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['LeaveTypes']))
				$model->attributes=$_GET['LeaveTypes'];

      $legendmodel=new Legends;
			$id = 0;
			if(isset($_GET['id']) && $_GET['id']!=""){
				  $id =  $_GET['id'];
					$legendmodel= Legends::model()->findByPk($id);
					if(isset($_POST['Legends'])){
               $legendmodel->short_note = $_POST['Legends']['short_note'];
							 if($legendmodel->save()){
								  Yii::app()->user->setFlash('success', "Data saved!");
								  $this->redirect(array('index'));
							 }
							 // else{
								//  Yii::app()->user->setFlash('error', "Error Occured");
								//  $this->redirect(array('index&id='.$id));
							 // }
         	}

			}




			$this->render('index',array(
				'model'=>$model,'lmodel'=>$lmodel,'legendmodel'=>$legendmodel,'id'=>$id
			));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
//	public function actionCreate()
//	{
//		$model=new LeaveTypes;
//
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//
//		if(isset($_POST['LeaveTypes']))
//		{
//			$model->attributes=$_POST['LeaveTypes'];
//			if($model->save()){
//				$this->redirect(array('view','id'=>$model->id));
//                        }
//		}
//
//		$this->render('create',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
//	public function actionUpdate($id)
//	{
//		$model=$this->loadModel($id);
//
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//
//		if(isset($_POST['LeaveTypes']))
//		{
//			$model->attributes=$_POST['LeaveTypes'];
//			if($model->save())
//				$this->redirect(array('view','id'=>$model->id));
//		}
//
//		$this->render('update',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=LeaveTypes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='leave-types-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
