<?php

/**
 * This is the model class for table "{{leave}}".
 *
 * The followings are the available columns in table '{{leave}}':
 * @property integer $leave_id
 * @property integer $emp_id
 * @property string $leave_submit_date
 * @property integer $leave_type
 * @property string $date_from
 * @property string $date_to
 * @property string $reason_for_leave
 * @property string $contact_details
 * @property string $document_if_any
 * @property string $person_covering_role
 * @property integer $approval_status
 * @property integer $decision_by
 * @property string $decision_date
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property LeaveSetting $approvalStatus
 * @property LeaveSetting $leaveType
 * @property LeaveDay[] $leaveDays
 */
class Leave extends CActiveRecord
{

    public $empname;
    public $calcday;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Leave the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{leave}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id, leave_submit_date, date_from, date_to, reason_for_leave, created_by, created_date, updated_by, updated_date', 'required', 'message' => 'Please enter {attribute}'),
            //  array('leave_type', 'required', 'message' => 'Please select leave type'),
            array('emp_id, leave_type, approval_status, decision_by, created_by, updated_by', 'numerical', 'integerOnly' => true),
            array('reason_for_leave', 'length', 'max' => 100),
            array('contact_details', 'length', 'max' => 25),
            array('document_if_any, person_covering_role', 'length', 'max' => 50),
            array('decision_date, calcday', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('leave_id, emp_id, empname, leave_submit_date, leave_type, date_from, date_to, reason_for_leave, contact_details, document_if_any, person_covering_role, approval_status, decision_by, decision_date, created_by, created_date, updated_by, updated_date,leave_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empid' => array(self::BELONGS_TO, 'Users', 'emp_id'),
            'empldetails' => array(self::BELONGS_TO, 'Users', 'emp_id'),
            'approvalStatus' => array(self::BELONGS_TO, 'LeaveSetting', 'approval_status'),
            //'leaveType' => array(self::BELONGS_TO, 'LeaveSetting', 'leave_type'),
            'leaveType' => array(self::BELONGS_TO, 'Legends', 'leave_type'),
            'leaveDays' => array(self::HAS_MANY, 'LeaveDay', 'leave_id'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),

            //  'empid' => array(self::BELONGS_TO, 'EmploymentDetails', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'leave_id' => 'Leave',
            'userid' => 'Employee',
            'leave_submit_date' => 'Leave Submit Date',
            'leave_type' => 'Leave Type',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'reason_for_leave' => 'Reason For Leave',
            'contact_details' => 'Contact Details',
            'document_if_any' => 'Document If Any',
            'person_covering_role' => 'Person Covering Role',
            'approval_status' => 'Approval Status',
            'decision_by' => 'Decision By',
            'decision_date' => 'Decision Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($userid = 0, $status_type = "All")
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $tblpx = Yii::app()->db->tablePrefix;
        $criteria = new CDbCriteria;
        $criteria->alias = 'l';

        $criteria->join = 'inner JOIN ' . $tblpx . 'users as us ON us.userid=l.emp_id';

        /* company */
      

        if (Yii::app()->controller->action->id == 'manageleaves' ) {

            $user = Users::model()->findByPk(Yii::app()->user->id);
            $con1 = '';
            $user_id = Yii::app()->user->id;

            if ($user['user_type'] == 1) {
                $criteria->condition = 'refer_to_admin=1';
            }
            elseif ($user['user_type'] != 6) {
                $criteria->condition = 'l.reporting_person='.$user_id.' and refer_to_hr=0 ';
            } else {
                $criteria->condition = 'refer_to_hr=1 and refer_to_admin !=1';
            }
        }

    
        $criteria->compare('concat(first_name," ",last_name)', $this->empname);

        $criteria->compare('empid.first_name', $this->created_by, true);
        $criteria->compare('empid.last_name', $this->created_by, true, 'or');

        $criteria->compare('leave_id', $this->leave_id);
        //$criteria->compare('emp_id', ($userid== 0)?$this->emp_id:$userid);
        $criteria->compare('leave_submit_date', $this->leave_submit_date, true);
        $criteria->compare('leave_type', $this->leave_type);
        $criteria->compare('date_from', $this->date_from, true);
        $criteria->compare('date_to', $this->date_to, true);
        $criteria->compare('reason_for_leave', $this->reason_for_leave, true);
        $criteria->compare('contact_details', $this->contact_details, true);
        $criteria->compare('document_if_any', $this->document_if_any, true);
        $criteria->compare('person_covering_role', $this->person_covering_role, true);
        $criteria->compare('approval_status', $this->approval_status);
        $criteria->compare('decision_by', $this->decision_by);
        $criteria->compare('decision_date', $this->decision_date, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);

        if ($status_type != 'All') {
            $criteria->addCondition('approval_status = ' . intval($status_type));
        }
        if ($userid != '0') {
            $criteria->addCondition('emp_id = ' . $userid);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'leave_id desc',
            ),
        ));

    }

    protected function afterFind()
    {

        $this->date_from = date('d/m/Y', strtotime($this->date_from));
        $this->date_to = date('d/m/Y', strtotime($this->date_to));
        $this->leave_submit_date = date('d/m/Y H:i:s', strtotime($this->leave_submit_date));
        $this->created_date = date('d/m/Y', strtotime($this->created_date));
        $this->updated_date = date('d/m/Y', strtotime($this->updated_date));
        return parent::afterFind();
    }

    public function maritalstatus($id)
    {

        if (!empty($id)) {

            $userdata = EmployeeDefaultData::model()->find("default_data_id = " . $id);

            if (!empty($userdata)) {

                return $userdata->label;
            } else {

                return "Not set";
            }
        } else {

            return "Not set";

        }

    }

    public function gender($id)
    {

        $userdata = EmployeeDefaultData::model()->find("default_data_id = " . $id);

        if (!empty($userdata)) {

            return $userdata->label;
        } else {

            return "Not set";
        }

    }

    public function getcompany($id)
    {

        //   $userdata = Yii::app()->db->createCommand( "SELECT concat_ws(' ',first_name,last_name) as fullname ,cmp.name as company FROM `pms_users` as em left join  pms_company as cmp on cmp.company_id = em.company_id WHERE `emp_id`= ".$id)->queryRow();

        //     if(!empty($userdata)){

        //         return $userdata['company'];
        //     }else{

        //         return "Not set";
        //     }
        return "Not set";
    }

    public function getdecisionby($id)
    {

        if (!empty($id)) {
            $userdata = Users::model()->find("userid = " . $id);

            return $userdata->first_name . ' ' . $userdata->last_name;
        } else {

            return "Not set";
        }

    }

}
