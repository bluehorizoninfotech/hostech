<?php

/**
 * This is the model class for table "{{leave_setting}}".
 *
 * The followings are the available columns in table '{{leave_setting}}':
 * @property integer $leave_setting_id
 * @property string $caption
 * @property string $setting_type
 * @property string $setting_description
 *
 * The followings are the available model relations:
 * @property Leave[] $leaves
 * @property Leave[] $leaves1
 * @property LeaveDay[] $leaveDays
 */
class LeaveSetting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LeaveSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{leave_setting}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('caption, setting_type', 'required'),
			array('caption', 'length', 'max'=>30),
			array('setting_type', 'length', 'max'=>15),
			array('setting_description', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('leave_setting_id, caption, setting_type, setting_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'leaves' => array(self::HAS_MANY, 'Leave', 'approval_status'),
			'leaves1' => array(self::HAS_MANY, 'Leave', 'leave_type'),
			'leaveDays' => array(self::HAS_MANY, 'LeaveDay', 'leave_period'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'leave_setting_id' => 'Leave Setting',
			'caption' => 'Caption',
			'setting_type' => 'Setting Type',
			'setting_description' => 'Setting Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('leave_setting_id',$this->leave_setting_id);
		$criteria->compare('caption',$this->caption,true);
		$criteria->compare('setting_type',$this->setting_type,true);
		$criteria->compare('setting_description',$this->setting_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}