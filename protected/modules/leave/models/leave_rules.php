<?php

/**
 * This is the model class for table "{{leave_rules}}".
 *
 * The followings are the available columns in table '{{leave_rules}}':
 * @property integer $rule_id
 * @property integer $designation_id
 * @property string $working_satdays
 * @property integer $sandwich_leave
 * @property integer $sunday_leave
 * @property integer $continue_leave
 */
class leave_rules extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return leave_rules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{leave_rules}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('designation_id, sandwich_leave, continue_leave', 'required'),
			//array('designation_id, sandwich_leave, sunday_leave, continue_leave', 'numerical', 'integerOnly'=>true),
			array('working_satdays', 'length', 'max'=>50),
			array('sunday_leave', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rule_id, designation_id, working_satdays, sandwich_leave, sunday_leave, continue_leave', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'designation0' => array(self::BELONGS_TO, 'Designation', 'designation_id'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rule_id' => 'Rule',
			'designation_id' => 'Designation',
			'working_satdays' => 'Working Saturdays',
			'sandwich_leave' => 'Sandwich Leave',
			'sunday_leave' => 'Working Sundays',
			'continue_leave' => 'Continue Leave',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rule_id',$this->rule_id);
		$criteria->compare('designation_id',$this->designation_id);
		$criteria->compare('working_satdays',$this->working_satdays,true);
		$criteria->compare('sandwich_leave',$this->sandwich_leave);
		$criteria->compare('sunday_leave',$this->sunday_leave);
		$criteria->compare('continue_leave',$this->continue_leave);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getworkingsatdays($rule_id)
	{
           
           $arr =array();
           $model=leave_rules::model()->findByPk($rule_id);
           if(($model->working_satdays)!=NULL){
           $data= explode(',', $model->working_satdays);
           foreach ($data as $value) {
	          	if($value==1){$arr[] = '1st Sat';}else if($value==2){$arr[] = '2nd Sat';}
	          	else if($value==3){$arr[] = '3rd Sat';}else if($value==4){$arr[] = '4th Sat';}
            }
              $data1 = implode("<br>",$arr);
            }else{
	          	$data1 = 'Not Set';
	        }
           return $data1;

	}

    public function getworkingsundays($rule_id)
	{
           $arr =array();
           $model=leave_rules::model()->findByPk($rule_id);
           if(($model->sunday_leave)!=NULL){
           $data= explode(',', $model->sunday_leave);
           foreach ($data as $value) {
	          	if($value==1){$arr[] = '1st Sun';}else if($value==2){$arr[] = '2nd Sun';}
	          	else if($value==3){$arr[] = '3rd Sun';}else if($value==4){$arr[] = '4th Sun';}
            }
              $data1 = implode("<br>",$arr);

              
	        }else{
	          	$data1 = 'Not Set';
	        }
           return $data1;

	}
}