<?php

/**
 * This is the model class for table "{{avail_compoff}}".
 *
 * The followings are the available columns in table '{{avail_compoff}}':
 * @property integer $id
 * @property integer $userid
 * @property string $date
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 */
class Availcompoff extends CActiveRecord {

    public $name;
    public $comp_leave_taken_list = array(-1 => 'empty');

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Availcompoff the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{avail_compoff}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date,comment, type', 'required', 'message' => 'Please enter {attribute}'),
            array('userid, status, created_by', 'numerical', 'integerOnly' => true),
            array('date, created_date, comment, type, updated_date, updated_by,name,decision_coment', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, userid, date, status, created_by, created_date, type, comment, updated_date, updated_by,name,decision_coment', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userid1' => array(self::BELONGS_TO, 'Users', 'userid'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'userid' => 'User',
            'date' => 'Date',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'type' => 'Duration',
            'decision_coment' => 'Comment',
            'comment' => 'Reason',
        );
    }

    public function utilized($userid, $date, $compval) {
        if (isset($this->comp_leave_taken_list[-1])) {
            $sql = 'select
    l.leave_id,
    l.emp_id,
    leave_submit_date,
    approval_status,
    ld.leave_type,
    ld.comp_date,
    leave_value,
    leave_period,
    ld.leave_date
FROM
    `pms_leave` AS l
INNER JOIN pms_leave_day AS ld
ON
    ld.leave_id = l.leave_id
WHERE
    l.approval_status IN(9, 11) AND ld.comp_date IS NOT NULL
GROUP BY
    emp_id,
    ld.leave_date ';

            $comp_leavedata = Yii::app()->db->createCommand($sql)->queryAll();

            if (count($comp_leavedata)) {
                $this->comp_leave_taken_list = array();
            }
            foreach ($comp_leavedata as $cdata) {
                extract($cdata);
                if (!isset($this->comp_leave_taken_list[$emp_id])) {
                    $this->comp_leave_taken_list[$emp_id] = array('comp_date' => array(), 'leave_details' => $cdata);
                }

                $this->comp_leave_taken_list[$emp_id]['comp_date'][$comp_date][$leave_date] = $leave_value;
            }
        }

        $total_comp_ut = 0;

        if(isset($this->comp_leave_taken_list[$userid]['comp_date'][$date])){
            $leaveused =  array_sum($this->comp_leave_taken_list[$userid]['comp_date'][$date]);
            if($leave_date-$compval<0){
                echo '<span style=\'color:red\'>! '.$compval.'</span>';
            }
            else{
                echo $leaveused;
            }
        }
//
//        echo '<pre>';
//        print_r($this->comp_leave_taken_list);
//        die;
    }

    public function csearch() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        if (Yii::app()->controller->action->id == 'compoff') {

            $criteria->condition = "t.userid=" . Yii::app()->user->id;
        }

        $criteria->join = 'inner JOIN pms_users as us ON us.userid=t.userid';

        if (Yii::app()->controller->action->id == 'managecompoff' and Yii::app()->user->role != 1) {
            $criteria->condition = 'us.line_manager=' . Yii::app()->user->id and Yii::app()->user->linemanager == "YES";
        }

        $criteria->with = array('userid1');
        $criteria->addSearchCondition('us.first_name', $this->name, true);

        $criteria->compare('id', $this->id);
        //$criteria->compare('t.userid',$this->userid);
        //	$criteria->compare('date',$this->date,true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('comment', $this->comment);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.type', $this->type);
        //$criteria->compare('t.created_date',$this->created_date,true);


        if ($this->date != null) {

            $date = explode('/', $this->date);
            $date = array_reverse($date);
            $tdate = implode('-', $date);
            $criteria->addCondition('date ="' . $tdate . '"');
        }

        if ($this->created_date != null) {

            $date = explode('/', $this->created_date);
            $date = array_reverse($date);
            $created_date = implode('-', $date);

            $criteria->addCondition("t.created_date = '" . $created_date . "' ");
        }

        $criteria->order = 'id DESC';
        //$criteria->addCondition("userid=".Yii::app()->user->id);
        return new CActiveDataProvider($this, array('criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->user->getState('availcompoff', 50),),
        ));
        /* return new CActiveDataProvider($this, array(
          'criteria'=>$criteria,'sort'=>array('defaultOrder'=>'CONCAT_WS(" ",first_name, last_name) ASC'),'pagination' => array('pageSize' => Yii::app()->user->getState('userpageSize', 20),)
          )); */
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        if (Yii::app()->controller->action->id == 'compoff') {

            $criteria->condition = "t.userid=" . Yii::app()->user->id;
        }

        $criteria->join = 'inner JOIN pms_users as us ON us.userid=t.userid';

        if (Yii::app()->controller->action->id == 'managecompoff' and Yii::app()->user->role != 1) {
            $criteria->condition = 'us.line_manager=' . Yii::app()->user->id and Yii::app()->user->linemanager == "YES";
        }

        $criteria->with = array('userid1');
        $criteria->compare('concat(us.first_name," ",us.last_name)',$this->userid);

        $criteria->compare('id', $this->id);
        //$criteria->compare('t.userid',$this->userid);
        //	$criteria->compare('date',$this->date,true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('comment', $this->comment);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.type', $this->type);
        //$criteria->compare('t.created_date',$this->created_date,true);


        if ($this->date != null) {

            $date = explode('/', $this->date);
            $date = array_reverse($date);
            $tdate = implode('-', $date);
            $criteria->addCondition('date ="' . $tdate . '"');
        }

        if ($this->created_date != null) {

            $date = explode('/', $this->created_date);
            $date = array_reverse($date);
            $created_date = implode('-', $date);

            $criteria->addCondition("t.created_date = '" . $created_date . "' ");
        }

        $criteria->order = 'id DESC';


        //$criteria->addCondition("userid=".Yii::app()->user->id);

        return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => Yii::app()->user->getState('availcompoff', 10),),
        ));

        /* return new CActiveDataProvider($this, array(
          'criteria'=>$criteria,'sort'=>array('defaultOrder'=>'CONCAT_WS(" ",first_name, last_name) ASC'),'pagination' => array('pageSize' => Yii::app()->user->getState('userpageSize', 20),)
          )); */
    }

    public function getleavestatus($sts, $userid, $date) {

        $data = '';

        if ($sts == 1) {
            $data = 'Pending for approval';
        } else if ($sts == 2) {
            $data = 'Approved';
        } else {
            $data = 'Rejected';
        }
        $sql = "SELECT emp_id,pms_leave_day.comp_date,leave_value,approval_status,leave_date "
                . "FROM `pms_leave` left join pms_leave_day on pms_leave_day.leave_id = pms_leave.leave_id "
                . "WHERE `emp_id` = " . $userid . " and pms_leave_day.comp_date='" . $date . "' ";
        $leave = Yii::app()->db->createCommand($sql)->queryRow();

        $color = '';
        if (!empty($leave)) {
            $color = ($leave['approval_status'] == 11) ? "#ff5722" : "#3f51b5";
        }

        $leavedata = (!empty($leave) && $sts == 2 && ($leave['approval_status'] == 9 || $leave['approval_status'] == 11 ) ) ? $data . " <br/><span style='color:$color;font-style: italic;'>( Leave " . $this->getallstatus($leave['approval_status']) . " )</span>" : $data;
        return $leavedata;
    }

    public function getstatus($sts) {
        $data = '';
        if ($sts == 1) {
            $data = 'Pending';
        } else if ($sts == 2) {
            $data = 'Approved';
        } else {
            $data = 'Rejected';
        }
        return $data;
    }

    public function getusername($userId) {
        $rows = Users::model()->find(array(
            'select' => "first_name,last_name",
            'condition' => "userid ='" . $userId . "'",
        ));
        return $rows['first_name'] . ' ' . $rows['last_name'];
    }

    public function getallstatus($status) {

        $msg = '';

        if ($status == 11) {
            $msg = "Pending";
        } else if ($status == 9) {
            $msg = "Approved";
        } else {
            $msg = "Rejected";
        }

        return $msg;
    }

    public function reportsearch() {

        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('t.userid',$this->userid);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('comment', $this->comment);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.type', $this->type);
        $criteria->group = 't.userid';
        $criteria->order = 'id DESC';
        return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => Yii::app()->user->getState('availcompoff', 50),),
        ));
    }

    public function getApprovedCompoff($userid,$sts){

          $status = '';
          if($sts!=0){
             $status = "and status = ".$sts;
          }

          $alldata = Yii::app()->db->createCommand("SELECT sum(type) as approved_compoff FROM `pms_avail_compoff` WHERE `userid` =".$userid." $status ")->queryRow();

          return $alldata['approved_compoff'];

      }

}
