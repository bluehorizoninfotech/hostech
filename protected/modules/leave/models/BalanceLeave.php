<?php

/**
 * This is the model class for table "{{balance_leave}}".
 *
 * The followings are the available columns in table '{{balance_leave}}':
 * @property integer $avail_leave_id
 * @property integer $userid
 * @property double $cl_ml
 * @property double $earned
 * @property double $lieu
 * @property double $rh
 * @property double $ml
 * @property string $comment
 * @property integer $modified_by
 * @property string $modified_date
 */
class BalanceLeave extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{balance_leave}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'required'),
			array('userid, modified_by', 'numerical', 'integerOnly'=>true),
			array('cl_ml, earned, lieu, rh, ml', 'numerical'),
			array('comment', 'length', 'max'=>100),
			array('modified_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('avail_leave_id, userid, cl_ml, earned, lieu, rh, ml, comment, modified_by, modified_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_leave_id' => 'Avail Leave',
			'userid' => 'Userid',
			'cl_ml' => 'Cl Ml',
			'earned' => 'Earned',
			'lieu' => 'Lieu',
			'rh' => 'Rh',
			'ml' => 'Ml',
			'comment' => 'Comment',
			'modified_by' => 'Modified By',
			'modified_date' => 'Modified Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_leave_id',$this->avail_leave_id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('cl_ml',$this->cl_ml);
		$criteria->compare('earned',$this->earned);
		$criteria->compare('lieu',$this->lieu);
		$criteria->compare('rh',$this->rh);
		$criteria->compare('ml',$this->ml);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('modified_by',$this->modified_by);
		$criteria->compare('modified_date',$this->modified_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BalanceLeave the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
