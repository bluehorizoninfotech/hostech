<?php

/**
 * This is the model class for table "{{leave_day}}".
 *
 * The followings are the available columns in table '{{leave_day}}':
 * @property integer $leave_day_id
 * @property integer $leave_id
 * @property string $leave_date
 * @property integer $leave_period
 * @property double $leave_value
 *
 * The followings are the available model relations:
 * @property Leave $leave
 * @property LeaveSetting $leavePeriod
 */
class LeaveDay extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LeaveDay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{leave_day}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('leave_id, leave_date, leave_period, leave_value', 'required'),
			array('leave_id, leave_period', 'numerical', 'integerOnly'=>true),
			array('leave_value', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('leave_day_id, leave_id, leave_date, leave_period, leave_value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'leave' => array(self::BELONGS_TO, 'Leave', 'leave_id'),
			'leavePeriod' => array(self::BELONGS_TO, 'LeaveSetting', 'leave_period'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'leave_day_id' => 'Leave Day',
			'leave_id' => 'Leave',
			'leave_date' => 'Leave Date',
			'leave_period' => 'Leave Period',
			'leave_value' => 'Leave Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('leave_day_id',$this->leave_day_id);
		$criteria->compare('leave_id',$this->leave_id);
		$criteria->compare('leave_date',$this->leave_date,true);
		$criteria->compare('leave_period',$this->leave_period);
		$criteria->compare('leave_value',$this->leave_value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}