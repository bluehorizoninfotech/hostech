<?php

/**
 * This is the model class for table "{{avail_leave}}".
 *
 * The followings are the available columns in table '{{avail_leave}}':
 * @property integer $avail_leave_id
 * @property integer $userid
 * @property double $cl_ml
 * @property double $earned
 * @property double $lieu
 */
class AvailLeave extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AvailLeave the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{avail_leave}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'required'),
			array('userid', 'numerical', 'integerOnly'=>true),
			
			array(' avail_leave_id, userid, actual_el, consi_leave, type, created_by, created_date, updated_date, updated_by ', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(' avail_leave_id, userid, actual_el, consi_leave, type, created_by, created_date, updated_date, updated_by ', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_leave_id' => 'Avail Leave',
			'userid' => 'Userid',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_leave_id',$this->avail_leave_id);
		$criteria->compare('userid',$this->userid);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}