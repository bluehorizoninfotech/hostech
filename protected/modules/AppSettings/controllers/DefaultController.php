<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		//


		$model = AppSettings::model()->find(['condition' => 'id = 1']);
		if (count($model) > 0) {
			$model = AppSettings::model()->find(['condition' => 'id = 1']);
		} else {
			$model = new AppSettings;
		}


		$this->render(
			'index',
			array(
				'model' => $model,

			)
		);



	}


	public function actioncreate()
	{
		$model = AppSettings::model()->find(['condition' => 'id = 1']);
		if (count($model) > 0) {
			$model = AppSettings::model()->find(['condition' => 'id = 1']);
		} else {
			$model = new AppSettings;
		}
		$this->performAjaxValidation($model);
		if (isset($_POST['AppSettings'])) {
			// logo

			$logo_filename = CUploadedFile::getInstance($model, 'logo');
			$logo_file_name = 'logo';
			$logo_format = "";
			if ($logo_filename != "") {
				$extension = strtolower($logo_filename->getExtensionName());
				$logo_format = $logo_file_name . "." . $extension;
				$logo_filename->saveAs(Yii::app()->basePath . '/../themes/assets/' . $logo_format);
			}


			// login logo
			$login_logo_filename = CUploadedFile::getInstance($model, 'login_logo');

			$login_logo_file_name = 'login_logo';
			$login_logo_format = "";
			if ($login_logo_filename) {
				$extension = strtolower($login_logo_filename->getExtensionName());
				$login_logo_format = $login_logo_file_name . "." . $extension;
				$login_logo_filename->saveAs(Yii::app()->basePath . '/../themes/assets/' . $login_logo_format);
			}

			// login logo
			$pdf_logo_filename = CUploadedFile::getInstance($model, 'pdf_logo');
			$pdf_logo_file_name = 'pdf_logo';
			$pdf_logo_format = "";
			if ($pdf_logo_filename) {
				$extension = strtolower($pdf_logo_filename->getExtensionName());
				$pdf_logo_format = $pdf_logo_file_name . "." . $extension;
				$result=$pdf_logo_filename->saveAs(Yii::app()->basePath . '/../themes/assets/' . $pdf_logo_format);
			}

			// favicon
			$favicon_filename = CUploadedFile::getInstance($model, 'favicon');

			$favicon_file_name = 'favicon';
			$favicon_format = "";
			if ($favicon_filename != "") {
				$extension = strtolower($favicon_filename->getExtensionName());
				$favicon_format = $favicon_file_name . "." . $extension;
				$favicon_filename->saveAs(Yii::app()->basePath . '/../themes/assets/' . $favicon_format);
			}


			$model->attributes = $_POST['AppSettings'];
	
			$model->logo = $logo_format?$logo_format:$model->logo;
			$model->login_logo = $login_logo_format?$login_logo_format:$model->login_logo;
			$model->pdf_logo = $pdf_logo_format?$pdf_logo_format:$model->pdf_logo;
			$model->favicon = $favicon_format?$favicon_format:$model->favicon;
			$model->created_by = Yii::app()->user->id;
			$model->created_date = date('Y-m-d');
			$model->updated_by = Yii::app()->user->id;
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Successfully created.');
				$this->redirect(array('default/index'));
			} else {
				print_r($model->getErrors());
			}




		}
	}


	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'app-settings-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}