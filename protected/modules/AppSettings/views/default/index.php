<script
    src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script
    src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
    $this->module->id,
);
?>
<h1>App Settings</h1>



<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <?php
        $form = $this->beginWidget(
            'CActiveForm',
            array(
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                'action' => Yii::app()->createUrl('AppSettings/default/create'),
                'id' => 'app-settings-form',
                // 'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => false,
                ),
            )
        );
        ?>
        <div class="row">
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'site_name'); ?>
                <?php echo $form->textField($model, 'site_name', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'site_name'); ?>
            </div>
            
            <div class="col-md-2">
                <?php echo $form->labelEx($model, 'number'); ?>
                <?php echo $form->textField($model, 'number', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'number'); ?>
            </div>
            <div class="col-md-2">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
            <div class="col-md-2">
                <?php echo $form->labelEx($model, 'website'); ?>
                <?php echo $form->textField($model, 'website', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'website'); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'address'); ?>
                <?php echo $form->textarea($model, 'address', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'address'); ?>
            </div>

    </div>
    <br>
    
    <div class="row">
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'logo'); ?>
                <?php echo $form->fileField($model, 'logo', array('class' => 'display:inline-block')); ?>
                <?php echo $form->error($model, 'logo'); ?>
                <div class="logo_msg"></div>
                <br>


                <?php

                $theme_asset_url = Yii::app()->baseUrl . "/themes/assets/";

                $logo = $theme_asset_url . 'logo.png';

                //echo CHtml::image($logo)."ttt";
                echo CHtml::image(
                    $logo,
                    '',
                    array(
                        'class' => 'width-100 height-100'
                    )
                );

                ?>
            </div>



            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'login_logo'); ?>
                <?php echo $form->fileField($model, 'login_logo', array('class' => 'display:inline-block')); ?>
                <?php echo $form->error($model, 'login_logo'); ?>
                <div class="login_logo_msg"></div>
                <br>

                <?php

                $theme_asset_url = Yii::app()->baseUrl . "/themes/assets/";

                $login_logo = $theme_asset_url . 'login_logo.png';

                //echo CHtml::image($logo)."ttt";
                echo CHtml::image(
                    $login_logo,
                    '',
                    array(
                        'class' => 'width-100 height-100'
                    )
                );

                ?>
            </div>

            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'favicon'); ?>
                <?php echo $form->fileField($model, 'favicon', array('class' => 'display:inline-block')); ?>
                <?php echo $form->error($model, 'favicon'); ?>
                <div class="favicon_msg"></div>
                <br>
                <?php

                $theme_asset_url = Yii::app()->baseUrl . "/themes/assets/";

                $favicon = $theme_asset_url . 'favicon.ico';

                //echo CHtml::image($logo)."ttt";
                echo CHtml::image(
                    $favicon,
                    '',
                    array(
                        'class' => 'width-100 height-100'
                    )
                );

                ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->labelEx($model, 'pdf_logo'); ?>
                <?php echo $form->fileField($model, 'pdf_logo', array('class' => 'display:inline-block ')); ?>
                <?php echo $form->error($model, 'pdf_logo'); ?>
                <div class="pdf_logo_msg"></div>
                <br>

                <?php

                $theme_asset_url = Yii::app()->baseUrl . "/themes/assets/";

                $pdf_logo = $theme_asset_url . 'pdf_logo.png';

                //echo CHtml::image($logo)."ttt";
                echo CHtml::image(
                    $pdf_logo,
                    '',
                    array(
                        'class' => 'width-100 height-100'
                    )
                );

                ?>
            </div>

        </div>




        <div class="row text-right">
            <div class="col-sm-12">
                <?php echo CHtml::SubmitButton('Create', array('class' => 'btn blue save_btn create-template margin-left-30', 'id' => 'save_btn', 'name' => 'save_btn'), array()); ?>


            </div>
        </div>


        <?php $this->endWidget(); ?>


    </div>
</div>

<script>
    
    // 	function checkextension() {
    //   var file = document.querySelector("#fUpload");
    //   if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) { alert("not an image!"); }
    // }

    // $("#AppSettings_logo").on('change', function(){    // 2nd way
    // 	var file = $('#AppSettings_logo')[0].files[0]
    // if (file){
    //   var filename=file.name;
    //   var extension = filename.split('.').pop();
    //  if(extension != 'png')
    //  {
    // 	$('#AppSettings_logo').val("");
    //  }
    // }
    // });


    var a = 0;

    //binds to onchange event of your input field
    $('#AppSettings_logo').bind('change', function () {
        var ext = $('#AppSettings_logo').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png']) == -1) {
            $('.logo_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be PNG.</span>');
            a = 0;
        }
        else {
            a = 1;
            $('.logo_msg').html('');
        }
        if (a == 1) {
            $('#save_btn').removeAttr('disabled');
        } else {
            $('#save_btn').attr('disabled', 'disabled');
        }
    });


    $('#AppSettings_login_logo').bind('change', function () {
        var ext = $('#AppSettings_login_logo').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png']) == -1) {
            $('.login_logo_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be PNG.</span>');
            a = 0;
        }
        else {
            a = 1;
            $('.login_logo_msg').html('');
        }
        if (a == 1) {
            $('#save_btn').removeAttr('disabled');
        } else {
            $('#save_btn').attr('disabled', 'disabled');
        }
    });
    $('#AppSettings_pdf_logo').bind('change', function () {
        var ext = $('#AppSettings_pdf_logo').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png']) == -1) {
            $('.pdf_logo_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be PNG.</span>');
            a = 0;
        }
        else {
            a = 1;
            $('.pdf_logo_msg').html('');
        }
        if (a == 1) {
            $('#save_btn').removeAttr('disabled');
        } else {
            $('#save_btn').attr('disabled', 'disabled');
        }
    });

    $('#AppSettings_favicon').bind('change', function () {
        var ext = $('#AppSettings_favicon').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['ico']) == -1) {
            $('.favicon_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be ICO.</span>');
            a = 0;
        }
        else {
            a = 1;
            $('.favicon_msg').html('');
        }
        if (a == 1) {
            $('#save_btn').removeAttr('disabled');
        } else {
            $('#save_btn').attr('disabled', 'disabled');
        }
    });
    $('#AppSettings_login_logo').bind('change', function () {
        var ext = $('#AppSettings_login_logo').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png']) == -1) {
            $('.login_logo_msg').html('<span class="red-color">Invalid Image Format! Image Format Must Be PNG.</span>');
            a = 0;
        }
        else {
            a = 1;
            $('.login_logo_msg').html('');
        }
        if (a == 1) {
            $('#save_btn').removeAttr('disabled');
        } else {
            $('#save_btn').attr('disabled', 'disabled');
        }
    });


</script>