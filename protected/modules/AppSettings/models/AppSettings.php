<?php

/**
 * This is the model class for table "{{app_settings}}".
 *
 * The followings are the available columns in table '{{app_settings}}':
 * @property integer $id
 * @property string $site_name
 * @property string $logo
 * @property string $login_logo
 * @property string $favicon
 * @property string $pdf_logo 
 * @property integer $nummber
 * @property string $address
 * @property string $website
 * @property string $email
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class AppSettings extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AppSettings the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{app_settings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		//print_r($this->attributes);exit;
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('site_name,address,number,website,email,created_by, created_date, updated_by', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('site_name,  login_logo,pdf_logo, favicon,address,email,website', 'length', 'max' => 200),
			array('number', 'length', 'max' => 15),
			array('email', 'email'), // Validate email format
			array('website', 'url', 'defaultScheme' => 'http'),
			// array('logo', 'file', 'types' => 'pdf'),
			// array('login_logo', 'file', 'types' => 'png','allowEmpty' => true),
			// array('favicon', 'file', 'types' => 'png'),
			array('updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, site_name, logo, login_logo, favicon,pdf_logo, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'site_name' => 'Site Name',
			'logo' => 'Logo',
			'login_logo' => 'Login Logo',
			'pdf_logo' => 'PDF Logo',
			'favicon' => 'Favicon',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('site_name', $this->site_name, true);
		$criteria->compare('logo', $this->logo, true);
		$criteria->compare('login_logo', $this->login_logo, true);
		$criteria->compare('pdf_logo', $this->pdf_logo, true);
		$criteria->compare('favicon', $this->favicon, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider(
			$this,
			array(
				'criteria' => $criteria,
			)
		);
	}
	// protected function beforeSave()
	// {
	// 	if (parent::beforeSave()) {
	// 		// Replace newline characters with <br> tag for the address field before saving to the database
	// 		if ($this->hasAttribute('address')) {
	// 			$this->address = nl2br($this->address);
	// 		}
	// 		return true;
	// 	} else {
	// 		return false;
	// 	}
	// }
	// Override afterFind method to convert <br> tags back to newline characters for address field
	// protected function afterFind()
	// {
	// 	parent::afterFind();
	// 	// Convert <br> tags back to newline characters for the address field after retrieving from the database
	// 	if ($this->hasAttribute('address')) {
	// 		$this->address = strip_tags(str_replace('<br>', "\n", $this->address));
	// 	}
	// }
}