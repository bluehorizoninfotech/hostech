<?php

class IgnorePunchesController extends Controller{



  public $allpunchs = array();
  public $getpunches = false;
  public $punchresult = array();
  public $empdevid = array();
  public $employees = array();
  public $punches = array();
  public $pdate = '';  //Punchdate
  public $numdays = 0;
  public $devices = '';
  public $interchange= array();




    public $layout = '//layouts/column2';


    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    public function accessRules() {

      $accessArr = array();
      $accessauthArr = array();
      $accessguestArr = array();
      $module= Yii::app()->controller->module->id;
      $controller_id = Yii::app()->controller->id;
      $controller = $module.'/'.$controller_id;

      if(isset(Yii::app()->session['pmsmenuall'])){
          if(array_key_exists($controller, Yii::app()->session['pmsmenuall'])){
              $accessArr = Yii::app()->session['pmsmenuall'][$controller];
          }
      }

      if(isset(Yii::app()->session['pmsmenuauth'])){
          if(array_key_exists($controller, Yii::app()->session['pmsmenuauth'])){
              $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
          }
      }

      if (isset(Yii::app()->session['pmsmenuguest'])) {
          if(array_key_exists($controller, Yii::app()->session['pmsmenuguest'])){
              $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
          }
      }
      $access_privlg = count($accessauthArr);

       // echo '<pre>';print_r($accessArr);exit;

      return array(
          array(
              'allow',
              'actions' => $accessArr,
              'users' => array('@'),
             // 'expression' => "$access_privlg > 0",

          ),
          array(
              'allow',
              'actions' => $accessauthArr,
              'users' => array('@'),
              'expression' => "$access_privlg > 0",

          ),

          array(
              'deny',
              'users' => array('*'),
          ),

      );

      // return array(
      //
      //       array('allow',
      //           'actions' => array('create', 'update', 'index', 'view', 'AddtoIgnore', 'Manualentry', 'DeleteManualentry', 'EntryReport', 'ShiftAction', 'ManageShiftPunches', 'test', 'addtoshift', 'movetoshift', 'ignoreshift', 'cron'),
      //           'users' => array('@'),
      //           'expression' => '(isset(Yii::app()->user->role) && (Yii::app()->user->role == 1) or Controller::issupervisor())',
      //
      //       ),
      //       array('allow', // allow admin user to perform 'admin' and 'delete' actions
      //           'actions' => array('DeleteManualentry'),
      //           'users' => array('@'),
      //            'expression' => "isset(Yii::app()->user->superadmin) && Yii::app()->user->superadmin==1",
      //       ),
      //
      //       array('deny', // deny all users
      //           'users' => array('*'),
      //       ),
      //   );


    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new IgnorePunches;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['IgnorePunches'])) {
            $model->attributes = $_POST['IgnorePunches'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->ig_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['IgnorePunches'])) {
            $model->attributes = $_POST['IgnorePunches'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->ig_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {


        $model = new IgnorePunches;


        $this->performAjaxValidation($model);



        if (isset($_POST['IgnorePunches'])) {

            $emp_id = $_POST['IgnorePunches']['empid'];
            $date = $_POST['IgnorePunches']['date'];

            $ignored = array();
            $log = array();
            $devices = DeviceAccessids::model()->findAll('userid = ' . $emp_id);

            foreach ($devices as $data) {

                //$log = array();
                // $logdetails = Yii::app()->db->createCommand("SELECT pms_punch_log.*,pms_ignore_punches.ig_id ,pms_ignore_punches.date FROM `pms_punch_log` left join  pms_ignore_punches on  (pms_ignore_punches.`empid`= pms_punch_log.empid and pms_punch_log.log_time = pms_ignore_punches.date and pms_punch_log.device_id= pms_ignore_punches.device_id ) WHERE date(pms_punch_log.`log_time`) = '".$date."' and pms_punch_log.`empid` = '".$data->userid."' and pms_punch_log.device_id = ".$data->deviceid )->queryAll();

                $logdetails = Yii::app()->db->createCommand("SELECT  al.punchlogid,al.sqllogid,al.empid,al.log_time,al.device_id,al.deviceid,al.logid,pms_ignore_punches.ig_id ,pms_ignore_punches.date,comment ,pms_ignore_punches.status,decision_by,decision_date FROM  pms_punch_log AS al INNER JOIN  pms_punching_devices AS pd ON pd.device_id = al.device_id INNER JOIN pms_clientsite AS cs ON  cs.id = pd.site_id INNER JOIN pms_device_accessids AS did ON  did.deviceid = al.device_id AND empid = did.accesscard_id left join  pms_ignore_punches on  (pms_ignore_punches.`empid`= al.empid and al.log_time = pms_ignore_punches.date and al.device_id= pms_ignore_punches.device_id )  WHERE DATE_FORMAT(log_time, '%Y-%m-%d') = '" . $date . "' and did.userid=" . $data->userid . " and al.device_id = " . $data->deviceid . " group by log_time ORDER BY log_time, empid ")->queryAll();

                foreach ($logdetails as $key => $value) {
                    $log[] = $value;
                }
            }

            $Manualentry = Yii::app()->db->createCommand(" SELECT *,date as log_time FROM `pms_manual_entry` WHERE `emp_id`= " . $emp_id . " and  date(`date`) = '" . $date . "' ")->queryAll();
            $log = array_merge($log, $Manualentry);
        } else {


            $log = array();
            $ignored = array();
            $emp_id = null;
            $date = null;
        }

        $newmodel = new IgnorePunches('search');


        $this->render('index', array(
            'newmodel' => $newmodel, 'model' => $model, 'logmodel' => $log, 'userid' => $emp_id, 'date' => $date, 'ignored' => $ignored,
        ));
    }

    public function actionAddtoIgnore() {

        if (isset($_REQUEST['id'])) {

            $userid = $_REQUEST['userid'];
            $comnt = isset($_REQUEST['comnt']) ? $_REQUEST['comnt'] : NULL;


            $logdetails = Accesslog::model()->find('punchlogid = ' . $_REQUEST['id']);

            //print_r($logdetails);exit;

            $check = Yii::app()->db->createCommand("SELECT * FROM `pms_ignore_punches` WHERE `empid`=" . $logdetails->empid . " and `device_id`= " . $logdetails->device_id . " and `date`= '" . $logdetails->log_time . "' ")->queryRow();



            if (empty($check)) {


                $model = new IgnorePunches;
                $model->device_id = $logdetails->device_id;
                $model->acces_logid = $logdetails->logid;
                $model->date = $logdetails->log_time;
                $model->empid = $logdetails->empid;
                $model->userid = $userid;
                $model->comment = $comnt;
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date("Y-m-d");

                if ($model->save()) {

                    // $this->PunchReportManual(date('Y-m-d',strtotime($logdetails->log_time)));
                    // Email

                    $userdetails = Users::model()->findByPk($userid);
                    $createdetails = Users::model()->findByPk(Yii::app()->user->id);
                    $settings = GeneralSettings::model()->findByPk(1);
                    $subject = 'Manual Punch Request';
                    $name = $userdetails['first_name'] . ' ' . $userdetails['last_name'];
                    $from_name = $createdetails['first_name'] . ' ' . $createdetails['last_name'];
                    $var = array('{user}', '{from_name}');
                    $data = array($name, $from_name);
                    $mail = new JPhpMailer();
                    $message = str_replace($var, $data, nl2br($settings['mail_temp_igpunch']));
                    $body = $this->renderPartial('email', array('message' => $message), true);

                    if ($this->paramsval('email_notify')) {
                        $smtpdetails = $this->paramsval('smtpmailconfig');
                        extract($smtpdetails);

                        if ($_SERVER['HTTP_HOST'] != 'localhost') {

                            $mail->IsSMTP();
                            $mail->Host = $settings->smtp_host;
                            $mail->Port       = $settings->smtp_port;
                            $mail->SMTPSecure = $settings->smtp_secure;
                            $mail->SMTPAuth = $settings->smtp_auth;
                            $mail->Username = $settings->smtp_username;
                            $mail->Password = $settings->smtp_password;

                            $mail->setFrom($settings->smtp_email_from, $smtpmailfromname);
                        } else {

                            $mail->IsSMTP();
                            $mail->Host = $mailHost;
                            $mail->SMTPAuth = $mailSMTPAuth;
                            $mail->Username = $mailUsername;
                            $mail->Password = $mailPassword;
                            $mail->SMTPSecure = $mailSMTPSecure;
                            $mail->setFrom($mailsetFrom, $from_name);
                        }

                        $maindata = Users::model()->findByPk(1);
                        $to_email = $maindata['personal_emailid'];

                        $mail->addAddress($to_email);  //Add a recipient
                        $mail->isHTML(true);
                        $mail->Subject = $subject;
                        $mail->Body = $body;
                        //$mail->send();
                    }
                    echo 1;
                    exit;
                }
            } else {
                $this->loadModel($check['ig_id'])->delete();

                // $this->PunchReportManual(date('Y-m-d',strtotime($logdetails->log_time)));
                echo 1;
                exit;
            }
        }
    }

    public function actionManualentry() {

        $date = $_REQUEST['date'];
        $time = $_REQUEST['time'];
        $empid = $_REQUEST['userid'];
        $coments = $_REQUEST['coment'];
        $shift_date = $_REQUEST['shift_date'];

        $newtime = explode(":", $time);

        if (!empty($empid)) {

            $model = new ManualEntry();
            $model->emp_id = $empid;
            $model->date = $date . ' ' . $time;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d");
            $model->comment = $coments;
            $model->shift_date = $shift_date;

            $check = Yii::app()->db->createCommand("SELECT * FROM `pms_manual_entry` WHERE `emp_id`=" . $empid . " and `date`= '" . $date . ' ' . $time . "' ")->queryRow();

            if (empty($check) && $newtime[0] < 24 || $newtime[0] == 24) {

                $model->save();

                // Email

                $userdetails = Users::model()->findByPk($empid);
                $createdetails = Users::model()->findByPk(Yii::app()->user->id);
                $settings = GeneralSettings::model()->findByPk(1);

                $subject = 'Manual Punch Request';
                $name = $userdetails['first_name'] . ' ' . $userdetails['last_name'];
                $from_name = $createdetails['first_name'] . ' ' . $createdetails['last_name'];
                $var = array('{user}', '{from_name}');

                $data = array($name, $from_name);

                $mail = new JPhpMailer();
                $message = str_replace($var, $data, nl2br($settings['mail_template_punches']));
                $body = $this->renderPartial('email', array('message' => $message), true);

                if ($this->paramsval('email_notify')) {
                    $smtpdetails = $this->paramsval('smtpmailconfig');
                    extract($smtpdetails);

                    if ($_SERVER['HTTP_HOST'] != 'localhost') {
                        $mail->IsSMTP();
                        $mail->Host = $settings->smtp_host;
                        $mail->Port       = $settings->smtp_port;
                        $mail->SMTPSecure = $settings->smtp_secure;
                        $mail->SMTPAuth = $settings->smtp_auth;
                        $mail->Username = $settings->smtp_username;
                        $mail->Password = $settings->smtp_password;
                        $mail->setFrom($settings->smtp_email_from, $smtpmailfromname);
                    } else {

                        $mail->IsSMTP();
                        $mail->Host = $mailHost;
                        $mail->SMTPAuth = $mailSMTPAuth;
                        $mail->Username = $mailUsername;
                        $mail->Password = $mailPassword;
                        $mail->SMTPSecure = $mailSMTPSecure;
                        $mail->setFrom($mailsetFrom, $from_name);
                    }

                    $maindata = Users::model()->findByPk(1);
                    //$to_email = $maindata['personal_emailid'];
                    $to_email = '';

                    $mail->addAddress($to_email);   // Add a recipient
                    $mail->isHTML(true);
                    $mail->Subject = $subject;
                    $mail->Body = $body;
                    //$mail->send();
                }
            }

            /* punch report */
            // $this->PunchReportManual($date);

            echo "1";
            exit;
        }
    }

    public function actionDeleteManualentry(){

        $entry_id = $_REQUEST['id'];

        $check = Yii::app()->db->createCommand("SELECT * FROM `pms_manual_entry` WHERE `entry_id`= " . $entry_id)->queryRow();
        if($check){

          $delmodel= new DeleteManualEntry;
          $delmodel->attributes = $check;
          $delmodel->deleted_by = Yii::app()->user->id;
          $delmodel->deleted_date = date('Y-m-d');
          $delmodel->save();

          $user = Yii::app()->db->createCommand()->delete('pms_manual_entry', 'entry_id=:id', array(':id' => $entry_id));
        }

        $this->PunchReportManual(date('Y-m-d',strtotime($check['date'])));

        // Yii::import('application.controllers.PunchresultController');
        // PunchresultController::actionPunchReportDaily(date('Y-m-d', strtotime($check['date'])));

        echo "1";exit;
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new IgnorePunches('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['IgnorePunches']))
            $model->attributes = $_GET['IgnorePunches'];
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = IgnorePunches::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ignore-punches-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionEntryReport() {


        if (isset($_REQUEST['siteid']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('siteid', intval($_REQUEST['siteid']));
            if (Yii::app()->user->siteid == 0) {
                unset(Yii::app()->user->siteid);
            }
            die('1');
        }
        if (isset($_REQUEST['empid']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('empid', intval($_REQUEST['empid']));
            if (Yii::app()->user->empid == 0) {
                unset(Yii::app()->user->empid);
            }
            die('1');
        }

        $tblpx = Yii::app()->db->tablePrefix;
        $sdate = (!isset($_GET['sdate']) ? date('Y-m-01') : date('Y-m-d', strtotime($_GET['sdate'])));
        $tdate = (!isset($_GET['tdate']) ? date('Y-m-d') : date('Y-m-d', strtotime($_GET['tdate'])));
        $deptment = (isset($_GET['deptment']) ? intval($_GET['deptment']) : 0);

        $siteid = 0;
        if (isset(Yii::app()->user->siteid) and Yii::app()->user->siteid > 0) {
            $siteid = Yii::app()->user->siteid;
        }

        $empid = 0;
        if (isset(Yii::app()->user->empid) and Yii::app()->user->empid > 0) {
            $empid = Yii::app()->user->empid;
        }

        $depwhere = '';
        if ($deptment > 0) {
            $depwhere = "and dept.department_id=" . $deptment;
        }

        $sitecon = '';
        if ($siteid > 0) {
            $sitecon = "and pds.site_id=" . $siteid;
        }

        $empcon = '';
        if ($empid > 0) {
            $empcon = "and us.userid=" . $empid;
        }




        $sql = 'select *,date_format(log_time, "%Y-%m-%d") as punchdate FROM ' .
                $tblpx . 'punch_log WHERE log_time BETWEEN "' . $sdate . '" AND "' . $tdate . " 23:59:59 " . '" '
                . 'GROUP BY empid,punchdate,device_id order by empid asc';

        $accesslog = Yii::app()->db->createCommand($sql)->queryAll();
        $userplog = array();

        foreach ($accesslog as $k => $acl) {
            $userplog[$acl['empid']][] = $acl;
        }

        $company = '';


        
        $userslist = " SELECT us.userid,comment,mn.emp_id,mn.status,date(date) as date, date as time,concat_ws(' ',us.first_name,us.last_name) as fullname,label,us.designation,mn.created_date,concat_ws(' ',emp.first_name,emp.last_name) as created_by FROM `pms_manual_entry` as mn inner join pms_users as us on us.userid = mn.emp_id left join pms_employee_default_data as dd on dd.default_data_id = us.user_type left join pms_users as emp on emp.userid = mn.created_by WHERE date(`date`) between '$sdate' and '$tdate' " . $empcon . " " . $company . " order by entry_id DESC ";

        // print_r($userslist);exit;

        $users = Yii::app()->db->createCommand($userslist)->queryAll();
      
        $useritems = array();
        $userdata = array();
        $r = 0;

        foreach ($users as $key => $user) {
            $r++;
           
            $userdata = array('userid' => $user['emp_id'],
                'emp_id' => $user['emp_id'],
                'designation' => $user['designation'],
                'fullname' => $user['fullname'],
                'usertype' => $user['label'],
                'cr_date' => $user['created_date'],
                'cr_by' => $user['created_by'],
                'coment' => $user['comment'],
                'date' => $user['date'],
                'time' => date('H:i:s', strtotime($user['time'])),
                'status' => ($user['status']==1)?"Approved":($user['status']==2)?"Rejected":"Pending"
            );
            $useritems[] = $userdata;
        }

        if (isset($_GET['ajaxcall'])) {
            echo $retarray = $this->renderPartial("entryreport", array('users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems), true);
        } 
        else {
              $this->render("entryreport", array('users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems));
        }
    }

    public function actionShiftAction() {

        $connection = Yii::app()->db;
        $IgnorePunchestbl = IgnorePunches::model()->tableSchema->rawName;

        if (isset($_REQUEST['id'])) {
            extract($_REQUEST);

            $status = array('Approve' => 1, 'Reject' => 2);
            if ($req) {
                $status = $status[$req];

                if ($status == 1) {
                    $finalstatus = 'Approved';
                } else {
                    $finalstatus = 'Rejected';
                }

                $comment=$_REQUEST['reason'];

                $connection->createCommand("UPDATE $IgnorePunchestbl SET `status`=" . $status . ",`reason`='".$comment."',`decision_by`=" . Yii::app()->user->id . ",`decision_date`='" . date('Y-m-d') . "' WHERE `ig_id`=" . $id)->execute();

                if ($status == 1) {
                    $model = IgnorePunches::model()->findByPk($id);

                     $this->PunchReportManual(date('Y-m-d', strtotime($model->date)));


                    // Yii::import('application.controllers.PunchresultController');
                    // PunchresultController::actionPunchReportDaily(date('Y-m-d', strtotime($model->date)));
                }

                $ret['msg'] = 'Ignored Punch ' . $finalstatus . ' Successfully';
                $ret['error'] = '';
            }
            echo json_encode($ret);
            exit;
        }
    }

    public function actionManageShiftPunches($id = 0, $date = 0) {


        $model = new IgnorePunches;
        //$this->performAjaxValidation($model);

       if(isset($_GET['IgnorePunches']) || $id!=0) {




            $emp_id = ($id != 0) ? $id : $_GET['IgnorePunches']['empid'];
            $date = ($date != 0) ? $date : $_GET['IgnorePunches']['date'];

            $prev_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
            $next_date = date('Y-m-d', strtotime('+1 day', strtotime($date)));

            
            $ignored = array();
            $log = array();
            $devices = DeviceAccessids::model()->findAll();
        
            foreach ($devices as $data) {

                $logdetails = Yii::app()->db->createCommand("SELECT al.punchlogid,al.sqllogid,al.empid,al.log_time,
                al.device_id,al.deviceid,al.logid,pms_ignore_punches.ig_id ,pms_ignore_punches.date,comment ,
                pms_ignore_punches.status as ig_status,decision_by as ig_decision_by,decision_date,did.userid as userid FROM  pms_punch_log AS al INNER JOIN pms_punching_devices AS pd ON pd.device_id = al.device_id INNER JOIN pms_clientsite AS cs ON
                cs.id = pd.site_id INNER JOIN pms_device_accessids AS did ON  did.deviceid = al.device_id AND
                empid = did.accesscard_id left join  pms_ignore_punches on  (pms_ignore_punches.`empid`= al.empid and
                al.log_time = pms_ignore_punches.date and al.device_id= pms_ignore_punches.device_id )
                WHERE DATE_FORMAT(log_time, '%Y-%m-%d') between  '" . $prev_date . "' and
				'" . $next_date . "' and did.userid=" . $data->userid . " and al.device_id = " . $data->deviceid . " group by log_time ORDER BY log_time, empid ")->queryAll();

                foreach ($logdetails as $key => $value) {
                    $log[] = $value;
                }
            }

            $manualentry = Yii::app()->db->createCommand("SELECT *,date as log_time,emp_id as empid ,emp_id as userid
            FROM `pms_manual_entry` WHERE `emp_id`= " . $emp_id . " and  date(`date`) between  '" . $prev_date . "' and '" .
            $next_date . "' ")->queryAll();

            $newarr = array_merge($log, $manualentry);


            $log1 = array();
            foreach ($newarr as $val) {
                $log1[strtotime($val['log_time'])] = $val;
            }
            ksort($log1);
        } else {


            $log = array();
            $ignored = array();
            $emp_id = null;
            $date = date("Y-m-d");
            $log1 = array();
        }

        // echo '<pre>';print_r($log1);exit;



      //  $newmodel = new IgnorePunches('search');
        $this->render('shiftpunches', array(
           'model' => $model, 'logmodel' => $log1, 'userid' => $emp_id, 'date' => $date, 'ignored' => $ignored,
        ));
    }

    public function actionignoreshift() {

        if (isset($_POST)) {


            $comment = $_POST['comment'];
            $data = $_POST['data'];
            $msg = 'Success';
            $emp_id =  $_POST['emp_id'];


            foreach ($data as $val) {

                $punchlog = Yii::app()->db->createCommand("SELECT userid,log_time,empid,device_id FROM `pms_punch_log` left join pms_device_accessids on pms_device_accessids.accesscard_id = pms_punch_log.empid WHERE `punchlogid`= " . $val)->queryRow();


                $model = new IgnorePunches;
                $model->empid = $punchlog['empid'];
                $model->userid = $emp_id;
                $model->date = $punchlog['log_time'];
                $model->device_id = $punchlog['device_id'];
                $model->comment = $comment;
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d');
                $model->save();
                $msg = 'ignored Successfully';
            }
            echo $msg;
            exit;
        }
    }

    public function actionaddtoshift() {

        if (isset($_REQUEST)) {


            $comment = $_REQUEST['comment'];
            $data = $_REQUEST['data'];
            $msg = 'Success';


            if ($data != '') {
                $sql = "SELECT userid,log_time,empid,device_id FROM `pms_punch_log`
                left join pms_device_accessids on pms_device_accessids.accesscard_id = pms_punch_log.empid
                WHERE `punchlogid`= " . $data;

                $punchlog = Yii::app()->db->createCommand($sql)->queryRow();

                $model = new ShiftEntry;
                $model->shift_date = date('Y-m-d', strtotime($punchlog['log_time']));
                $model->emp_id = $punchlog['userid'];
                $model->date = $punchlog['log_time'];
                $model->comment = $comment;
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d');
                $model->type = 0;
                $model->save();
                $msg = 'Added Successfully';
            }
            echo $msg;
            exit;
        }
    }

    public function actionmovetoshift() {

        if (isset($_POST)) {

            $empid = $_POST['empid'];
            $punchtime = $_POST['logtime'];
            $comment = $_POST['comment'];
            $date = $_POST['date'];
            $type = $_POST['type'];

            if ($type == "up") {
                $shift_date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
            } else {
                $shift_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
            }


            $model = new ShiftEntry;
            $model->emp_id = $empid;
            $model->date = $punchtime;
            $model->comment = $comment;
            $model->shift_date = $shift_date;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->type = 1;
            $model->save();
            $msg = 'Shifted Successfully';
            echo $msg;
            exit;
        }
    }

    public function actiontest() {

        $this->render('test1');
    }

    public function actioncron() {

        $punchlog = Yii::app()->db->createCommand("SELECT al.*,did.userid FROM `pms_punch_log` AS al INNER JOIN  pms_punching_devices AS pd ON pd.device_id = al.device_id INNER JOIN pms_clientsite AS cs ON  cs.id = pd.site_id INNER JOIN pms_device_accessids AS did ON  did.deviceid = al.device_id AND empid = did.accesscard_id order by punchlogid DESC limit 20")->queryAll();

        foreach ($punchlog as $data) {

            $shiftdet = $this->getshiftdata($data['userid'], $data['log_time']);
        }
    }

    public function getshiftdata($userid, $log_time) {


        $punchdate = date('Y-m-d', strtotime($log_time));
        $shift = Yii::app()->db->createCommand("SELECT pms_shift_assign.shift_id, shift_starts,shift_ends,
				 grace_period_before,grace_period_after FROM `pms_shift_assign` left join pms_employee_shift on pms_employee_shift.shift_id = pms_shift_assign.shift_id WHERE `user_id` = " . $userid . " and `att_date`='" . date('Y-m-d', strtotime($log_time)) . "' ")->queryRow();
        $gracetime_before = floatval($shift['grace_period_before']) * 60 * 60;
        $gracetime_after = floatval($shift['grace_period_after']) * 60 * 60;

        if (strtotime($shift['shift_starts']) < strtotime($shift['shift_ends'])) {
            $shiftend = $punchdate . " " . $shift['shift_ends'] . ":00";
        } else {
            $next = date('Y-m-d', strtotime('+1 day', strtotime($punchdate)));
            $shiftend = $next . " " . $shift['shift_ends'] . ":00";
        }

        $shiftstart = date('Y-m-d H:i:s', strtotime($punchdate . " " . $shift['shift_starts'] . ":00") - $gracetime_before);
        $shiftend = date('Y-m-d H:i:s', strtotime($shiftend) + $gracetime_after);

        if ($log_time >= $shiftstart && $log_time <= $shiftend) {

            $newarray = array('user_id' => $userid,
                'log_time' => $log_time,
                'shift_id' => $shift['shift_id'],
                'shift_starts' => $shiftstart,
                'shift_ends' => $shiftend,
                'shift_date' => $punchdate,
            );
        } else {

            $shift = Yii::app()->db->createCommand("SELECT pms_shift_assign.shift_id, shift_starts,shift_ends,
					 grace_period_before,grace_period_after FROM pms_shift_assign left join pms_employee_shift on pms_employee_shift.shift_id = pms_shift_assign.shift_id WHERE user_id = " . $userid . " and att_date='" . date('Y-m-d', strtotime('-1 day', strtotime($log_time))) . "' and shift_ends< shift_starts ")->queryRow();

            $gracetime_before = floatval($shift['grace_period_before']) * 60 * 60;
            $gracetime_after = floatval($shift['grace_period_after']) * 60 * 60;

            $punchdate = date('Y-m-d', strtotime('-1 day', strtotime($log_time)));


            if (strtotime($shift['shift_starts']) < strtotime($shift['shift_ends'])) {
                $shiftend = $punchdate . " " . $shift['shift_ends'] . ":00";
            } else {
                $next = date('Y-m-d', strtotime('+1 day', strtotime($punchdate)));
                $shiftend = $next . " " . $shift['shift_ends'] . ":00";
            }

            $shiftstart = date('Y-m-d H:i:s', strtotime($punchdate . " " . $shift['shift_starts'] . ":00") - $gracetime_before);
            $shiftend = date('Y-m-d H:i:s', strtotime($shiftend) + $gracetime_after);

            if ($log_time >= $shiftstart && $log_time <= $shiftend) {

                $newarray = array(
                    'user_id' => $userid,
                    'log_time' => $log_time,
                    'shift_id' => $shift['shift_id'],
                    'shift_starts' => $shiftstart,
                    'shift_ends' => $shiftend,
                    'shift_date' => $punchdate,
                );
            } else {
                $newarray = array(
                    'user_id' => $userid,
                    'log_time' => $log_time,
                    'shift_id' => $shift['shift_id'],
                    'shift_starts' => $shiftstart,
                    'shift_ends' => $shiftend,
                    'shift_date' => $punchdate,
                );
            }
        }

        return $newarray;
    }

}
