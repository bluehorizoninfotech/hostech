<?php

class AttendanceController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $week_start_date = null;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {


        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $module = Yii::app()->controller->module->id;
        $controller_id = Yii::app()->controller->id;
        $controller = $module . '/' . $controller_id;

        if (isset(Yii::app()->session['pmsmenuall'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuall'])) {
                $accessArr = Yii::app()->session['pmsmenuall'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuauth'])) {
                $accessauthArr = Yii::app()->session['pmsmenuauth'][$controller];
            }
        }

        if (isset(Yii::app()->session['pmsmenuguest'])) {
            if (array_key_exists($controller, Yii::app()->session['pmsmenuguest'])) {
                $accessguestArr = Yii::app()->session['pmsmenuguest'][$controller];
            }
        }
        $access_privlg = count($accessauthArr);



        return array(
            array('allow',
                'actions' => array('cronjobtoaddattendance'),
                'users' => array('*'),
            ),
            array(
                'allow',
                'actions' => $accessArr,
                'users' => array('@'),
            ),
            array(
                'allow',
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array('allow',
                'actions' => array('applyattendance', 'applyallattendance', 'testattendance', 'updateattendance', 'Myattendance', 'getMyattendance'),
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Attendance;



        if (isset($_POST['Attendance'])) {
            $model->attributes = $_POST['Attendance'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->att_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Attendance'])) {
            $model->attributes = $_POST['Attendance'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->att_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionAdmin() {
        $model = new Attendance('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Attendance']))
            $model->attributes = $_GET['Attendance'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Attendance::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionIndex() {

        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));
            $div='';
            $usertype='';
            $grp_id='';
            $skill='';
            $designation='';

        if (isset($_REQUEST['TimeEntry'])) {

            $div = isset($_REQUEST['div']) ? $_REQUEST['div'] : "";

            $_REQUEST['TimeEntry']['date_from'] = date('Y-m-d', strtotime($_REQUEST['TimeEntry']['date_from']));
            $_REQUEST['TimeEntry']['date_till'] = date('Y-m-d', strtotime($_REQUEST['TimeEntry']['date_till']));

            $date1 = new DateTime($_REQUEST['TimeEntry']['date_from']);
            $date2 = new DateTime($_REQUEST['TimeEntry']['date_till']);

            Yii::app()->session['start_date'] = $_REQUEST['TimeEntry']['date_from'];
            Yii::app()->session['end_date'] = $_REQUEST['TimeEntry']['date_till'];
            Yii::app()->session['search'] = 'search';

            list($start_date, $end_date, $week_title) = $this->date_range($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
        } elseif (isset(Yii::app()->session['start_date']) && (Yii::app()->session['end_date'] )) {

            Yii::app()->session['search'] = NULL;

            $atte_settings = AttendanceSettings::model()->findByPk(1);
            if (!empty($atte_settings) && Yii::app()->session['search'] != 'search') {
                $att_day = $atte_settings->att_day;
                $att_month = $atte_settings->att_month;
                $till_day = $atte_settings->till_day;

                if ($till_day == NULL) {
                    $date = date('Y-m-' . $att_day);
                    $till_day = date("t", strtotime($date));
                    Yii::app()->session['till_day'] = 'Y';
                } else {
                    Yii::app()->session['till_day'] = NULL;
                    $till_day = $atte_settings->till_day;
                }
                if ($att_day != '' && $att_month == 0) {
                    Yii::app()->session['start_date'] = date('Y-m-' . $att_day);
                    Yii::app()->session['end_date'] = date('Y-m-' . $till_day);
                } elseif ($att_month == 1) {
                    $nxt_date = date('Y-m-' . $till_day);
                    $next_month = date('Y-m-d', strtotime('+1 month', strtotime($nxt_date)));
                    Yii::app()->session['start_date'] = date('Y-m-' . $att_day);
                    Yii::app()->session['end_date'] = $next_month;
                }

                if ($att_month == 1 && $att_day == 1 && $atte_settings->till_day == NULL) {
                    $date = date('Y-m-' . $att_day);
                    $till_day = date("t", strtotime($date));
                    Yii::app()->session['start_date'] = date('Y-m-' . $att_day);
                    Yii::app()->session['end_date'] = date('Y-m-' . $till_day);
                }
            }
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates(Yii::app()->session['start_date'], Yii::app()->session['end_date']);

            $start_date = Yii::app()->session['start_date'];
            $end_date = Yii::app()->session['end_date'];

            /* prev next */
            $month = 0;
            if (isset($_GET['month'])) {

                $month = intval($_GET['month']);
                $prv_month_start = date('Y-m-d', strtotime('' . $month . ' month', strtotime(Yii::app()->session['start_date'])));
                if (isset(Yii::app()->session['till_day']) && Yii::app()->session['till_day'] == 'Y') {
                    $prv_month_end = date('Y-m-t', strtotime('' . $month . ' month', strtotime(Yii::app()->session['start_date'])));
                } else {

                    $prv_month_end = date('Y-m-d', strtotime('' . $month . ' month', strtotime(Yii::app()->session['end_date'])));
                }

                $start_date = $prv_month_start;
                $end_date = $prv_month_end;

                Yii::app()->user->setState('filter_from_date', $start_date);
                Yii::app()->user->setState('filter_to_date', $end_date);
                $this->Changenew();
            }
        } else {

            list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-01"));
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($start_date, $end_date);
            Yii::app()->session['start_date'] = $start_date;
            Yii::app()->session['end_date'] = $end_date;
        }

        $newmodel = new ImportForm();
        $model = new Attendance();

        Yii::app()->session['date_frm'] = $start_date;
        Yii::app()->session['date_end'] = $end_date;

        Yii::app()->user->setState('filter_from_date', $start_date);
        Yii::app()->user->setState('filter_to_date', $end_date);
        $this->Changenew();

        $this->render('attendancelist', array('start_date' => $start_date,
            'end_date' => $end_date, 'newmodel' => $newmodel,
            'model' => $model, 'division' => $div, 'usertype' => $usertype,
            'grp_id' => $grp_id, 'skill' => $skill, 'designation' => $designation));
    }

    public function actionCalandersheet() {
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));

        $tblpxpms = Yii::app()->db->tablePrefix; // table prefixforpms

        $this->layout = 'false';
        if (isset($_REQUEST['TimeEntry'])) {

            $_REQUEST['TimeEntry']['date_from'] = date('Y-m-d', strtotime($_REQUEST['TimeEntry']['date_from']));
            $_REQUEST['TimeEntry']['date_till'] = date('Y-m-d', strtotime($_REQUEST['TimeEntry']['date_till']));

            $date1 = new DateTime($_REQUEST['TimeEntry']['date_from']);
            $date2 = new DateTime($_REQUEST['TimeEntry']['date_till']);

            Yii::app()->session['start_date'] = $_REQUEST['TimeEntry']['date_from'];
            Yii::app()->session['end_date'] = $_REQUEST['TimeEntry']['date_till'];
            Yii::app()->session['date'] = 1;

            list($start_date, $end_date, $week_title) = $this->date_range($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($_REQUEST['TimeEntry']['date_from'], $_REQUEST['TimeEntry']['date_till']);
        } elseif (isset(Yii::app()->session['start_date']) && (Yii::app()->session['end_date'] )) {

            Yii::app()->session['days_count'] = $this->getcountBetween2Dates(Yii::app()->session['start_date'], Yii::app()->session['end_date']);
            $start_date = Yii::app()->session['start_date'];
            $end_date = Yii::app()->session['end_date'];

            $data = $_REQUEST;
            if (isset($data)) {
                $start_date = $data['startdate'];
                $end_date = $data['enddate'];
            }
        } else {
            list($start_date, $end_date, $week_title) = $this->x_week_range(date("Y-m-d"));
            Yii::app()->session['days_count'] = $this->getcountBetween2Dates($start_date, $end_date);
            Yii::app()->session['start_date'] = $start_date;
            Yii::app()->session['end_date'] = $end_date;
        }
        $where = " where 1=1";
        $sql = "SELECT site.id, cmp.name,de.label, ed.* FROM pms_users ed left join "
                . "pms_employee_default_data as de on de.default_data_id = ed.designation "
                . "left join pms_company as cmp on cmp.company_id = ed.company_id "
                . "left join pms_usersite as site on site.user_id = ed.userid "
                . $where . "
         group by userid order by first_name ";

        $usermodel = Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('calendertable', array('usermodel' => $usermodel, 'start_date' => $start_date, 'end_date' => $end_date));
    }

    public function actionImportpunchlog() {
        $tbl = Yii::app()->db->tablePrefix; // table prefixforpms

        $start = $_GET['startdate'];
        $end = $_GET['enddate'];

        $fromdate = $start;
        $todate = $end;

        $date = $fromdate;


        $sql = "SELECT us.`userid`, us.`user_type`, us.`salutation`, us.`first_name`, us.`last_name`,"
                . " us.`gender`, us.`marital_status`, us.`username`, us.`password`, us.`email`, us.`photograph`, "
                . "us.`reporting_person`, us.`last_modified`, us.`reg_date`, us.`reg_ip`, us.`activation_key`, "
                . "us.`email_activation`, us.`status`,  us.`employee_id`, us.`system_access_types`, us.`is_line_manager`, "
                . "us.`designation`, us.`department`,da.accesscard_id FROM {$tbl}employee us "
                . "LEFT JOIN {$tbl}employee ed ON ed.userid = us.userid inner join {$tbl}device_accessids as da on da.userid = us.userid"
                . " WHERE us.user_type != 1 AND us.user_type != 5  "
                . "AND (ed.date_of_joining<= '$fromdate' OR ed.date_of_joining <= '$todate' OR ed.date_of_joining IS NULL) "
                . "AND (ed.date_of_resignation >= '$fromdate' OR ed.date_of_resignation >= '$todate' OR ed.date_of_resignation IS NULL) "
                . "ORDER BY first_name asc";



        $users = Yii::app()->db->createCommand($sql)->queryAll();

        $weekly_off1_days = array();
        $weekly_off2_days = array();
        $satsundays = array();
        $sdate = $fromdate;
        $end_date = $todate;

        for ($i = 0; $i <= (strtotime($sdate) <= strtotime($end_date)); $i++) {

            if (date("D", strtotime($sdate)) == 'Sat' || date("D", strtotime($sdate)) == 'Sun') {
                array_push($satsundays, $sdate);
            }

            $sdate = date("Y-m-d", strtotime("+1 day", strtotime($sdate)));
        }

        echo '<pre>';
        print_r($users);
        die;
        foreach ($users as $user) {
            $accessid = $user['accesscard_id'];
            $role = $user['user_type'];
            $offdays = array();
            if ($role) {
                $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));

                if (!empty($rsettings)) {

                    while (strtotime($date) <= strtotime($todate)) {
                        //weekly off1
                        $weekly_off1_yes = $rsettings->weekly_off1_yes;
                        if (!empty($weekly_off1_yes)) {
                            $weekly_off1 = $rsettings->weekly_off1;
                            if (date("l", strtotime($date)) == $weekly_off1) {
                                array_push($weekly_off1_days, $date);
                            }
                        }

                        //weekly off2
                        $weekly_off2_yes = $rsettings->weekly_off2_yes;
                        if (!empty($weekly_off2_yes)) {
                            $weekly_off2 = $rsettings->weekly_off2;
                            $weekly_off2_count = $rsettings->weekly_off2_count; //echo date('d',strtotime('+1 week '.$weekly_off2.''));

                            $weekly_off2_array = explode(",", $weekly_off2_count);
                            $wk = date("M Y", strtotime($date));


                            foreach ($weekly_off2_array as $warr) {
                                if ($warr == 1) {
                                    $num = "first";
                                } else if ($warr == 2) {
                                    $num = "second";
                                } else if ($warr == 3) {
                                    $num = "third";
                                } else if ($warr == 4) {
                                    $num = "fourth";
                                } else if ($warr == 5) {
                                    $num = "last";
                                } else {
                                    $num = "";
                                }
                                $dt = date("Y-m-d", strtotime("$num $weekly_off2 of $wk"));
                                if (!in_array($dt, $weekly_off2_days)) {
                                    array_push($weekly_off2_days, date("Y-m-d", strtotime("$num $weekly_off2 of $wk")));
                                }
                            }
                        }
                        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                    }
                }
            }
            $offdays[$accessid] = array_merge($weekly_off1_days, $weekly_off2_days);
        }

        //print_r($offdays);//Get all sundays

        $delete = Yii::app()->db->createCommand("DELETE FROM {$tbl}attendance WHERE att_date BETWEEN '$fromdate' AND '$todate' ")->execute();
        $fromdate = $start;
        $todate = $end;
        $date = $fromdate;
        $result_arr = array();
        $weeklyoffdays = array();
        $attn = Yii::app()->createController('Accesslog');
        while (strtotime($date) <= strtotime($todate)) {

            //if (date("D", strtotime($date)) != 'Sat' && date("D", strtotime($date)) != 'Sun') {
            $dayattsql = "SELECT * FROM {$tbl}punchreport WHERE logdate = '$date' ";

            $result_arr[$date] = Yii::app()->db->createCommand($dayattsql)->queryAll();

            // $result_arr[$date] = $attn[0]->calcHours2($date);


            foreach ($result_arr as $result) {


                foreach ($result as $res) {
                    $accesscardid = $res['resource_id'];
                    $firstpunch = str_replace("HL", "", $res['firstpunch']);
                    $lastpunch = str_replace("HL", "", $res['lastpunch']);
                    $intime = $res['intime'];
                    $outtime = $res['outtime'];
                    $legendid = 0;
                    //$accesscardid = 81;
                    $user = Users::model()->find(array('condition' => "accesscard_id = $accesscardid"));

                    if (!empty($user)) {
                        if (array_key_exists($accesscardid, $offdays)) {
                            $weeklyoffdays = $offdays[$accesscardid];
                        } else {
                            $weeklyoffdays = $satsundays;
                        }

                        $userid = $user['userid'];
                        $role = $user['user_type'];
                        if ($role) {
                            $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));

                            if (!empty($rsettings)) {



                                $latecoming_gt = $rsettings->late_coming_gt;
                                $earlygoing_gt = $rsettings->early_going_gt;
                                $timeto_in = strtotime('09:00:00');
                                if (!is_null($latecoming_gt)) {
                                    $grace_inpunch = date("H:i:s", strtotime('+' . $latecoming_gt . ' minutes', $timeto_in));
                                } else {
                                    $grace_inpunch = "09:00:00";
                                }
                                $timeto_out = strtotime('06:00:00');
                                if (!is_null($earlygoing_gt)) {
                                    $grace_outpunch = date("H:i:s", strtotime('-' . $earlygoing_gt . ' minutes', $timeto_out));
                                } else {
                                    $grace_outpunch = '06:00:00';
                                }


                                if (!in_array($date, $weeklyoffdays)) {
                                    if (!empty($firstpunch) && $firstpunch != "05:30:00") {

                                        $legend = Legends::model()->find(array('condition' => "short_note = 'P'"));
                                        $legendid = $legend->leg_id; //echo 'P';
                                        //Calculate half day if work duration is less than given value
                                        if ($rsettings->halfday_by_duration == 1) {

                                            $time = explode(':', $intime);
                                            $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                            if ($mins < $rsettings->halfday_duration_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        } else if ($rsettings->absent_by_duration == 1) {
                                            //Calculate absent if work duration is less than given value

                                            $time = explode(':', $intime);
                                            $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);


                                            //echo $intime."--".$mins; echo "<br/>";
                                            if ($mins < $rsettings->absent_duration_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }



                                        //Mark Half Day if late by given time
                                        if ($rsettings->halfday_by_late == 1) {
                                            $now = new DateTime($grace_inpunch);
                                            $then = new DateTime($firstpunch);
                                            $diff = $now->diff($then);
                                            $mins = $diff->format('%i');
                                            if ($mins >= $rsettings->halfday_late_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }

                                        //Mark Half Day if early going by given time
                                        if ($rsettings->halfday_by_earlygoing == 1) {
                                            $now = new DateTime($grace_outpunch);
                                            $then = new DateTime($lastpunch);
                                            $diff = $now->diff($then);
                                            $mins = $diff->format('%i');


                                            if ($mins >= $rsettings->halfday_earlygoing_mins) {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }

                                        //On partial day, Calculate half day if work duration is less than given value
                                        if ($rsettings->partial_halfday_by_duration == 1) {
                                            if ($firstpunch > $grace_inpunch) {
                                                $time = explode(':', $intime);
                                                $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                                if ($mins < $rsettings->partial_halfday_duration_mins) {
                                                    $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                                    $legendid = $legend->leg_id;
                                                }
                                            }
                                        } else if ($rsettings->partial_absent_by_duration == 1) {
                                            //On partial day, Calculate absent if work duration is less than given value
                                            if ($firstpunch > $grace_inpunch) {
                                                $time = explode(':', $intime);
                                                $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                                if ($mins < $rsettings->partial_absent_duration_mins) {
                                                    $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                    $legendid = $legend->leg_id;
                                                }
                                            }
                                        }
                                    } else {
                                        //Absent
                                        $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                        $legendid = $legend->leg_id;
                                    }
                                } else {
                                    //case of weekly holidays
                                    $result_arr2 = array();
                                    $hdate = $date;
                                    $prevdate = date('Y-m-d', strtotime('-1 day', strtotime($hdate))); //echo $prevdate; //die;
                                    if (!in_array($prevdate, $weeklyoffdays)) {

                                        //$result_arr2[$prevdate] = $attn[0]->calcHours2($prevdate);
                                        $result_arr2[$prevdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$prevdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        //print_r($result_arr2);
                                        if (!empty($result_arr2)) {
                                            $prevarr = $result_arr2[$prevdate];
                                            $fpunch = $prevarr['firstpunch'];
                                            if (empty($fpunch) || $fpunch == "05:30:00") {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }
                                    }


                                    $result_arr3 = array();
                                    $nextdate = date('Y-m-d', strtotime('+1 day', strtotime($hdate)));
                                    echo $nextdate; //die;
                                    if (!in_array($nextdate, $weeklyoffdays)) {
                                        echo "in";
                                        $result_arr3[$nextdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$nextdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        if (!empty($result_arr3)) {
                                            $nextarr = $result_arr3[$nextdate];
                                            $fpunch = $nextarr['firstpunch'];
                                            if (empty($fpunch) || $fpunch == "05:30:00") {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }
                                    }

                                    $result_arr4 = array();
                                    $result_arr5 = array();
                                    if (!in_array($prevdate, $weeklyoffdays) && !in_array($nextdate, $weeklyoffdays)) {
                                        $result_arr4[$prevdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$prevdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        $result_arr5[$nextdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$nextdate' AND resource_id = '$accesscardid' ")->queryRow();
                                        if (!empty($result_arr4) || !empty($result_arr5)) {
                                            $prevarr = $result_arr4[$prevdate];
                                            $fpunchprev = $prevarr['firstpunch'];

                                            $nextarr = $result_arr5[$nextdate];
                                            $fpunchnext = $nextarr['firstpunch'];
                                            if (empty($fpunchprev) || $fpunchprev == "05:30:00" && empty($fpunchnext) || $fpunchnext == "05:30:00") {
                                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                                $legendid = $legend->leg_id;
                                            }
                                        }
                                    }
                                }
                            }//end if role settings
                        }//end if role
                        else {

                            if (!empty($firstpunch) && $firstpunch != "05:30:00") {

                                if (!isset($inhrs)) {
                                    $inhrs = $res['intime'];
                                }

                                if (strtotime($inhrs) < strtotime("04:00:00")) {
                                    //Half day
                                    $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                    $legendid = $legend->leg_id; //echo 'HP';
                                } else {
                                    $legend = Legends::model()->find(array('condition' => "short_note = 'P'"));
                                    $legendid = $legend->leg_id; //echo 'P';
                                }
                            } else {
                                //Absent
                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                $legendid = $legend->leg_id; //echo "CL";
                            }
                        }

                        $check = Attendance::model()->find(array('condition' => "user_id = $userid AND att_date = '$date'"));
                        if (count($check) == 0) {
                            $attmodel = new Attendance();
                        } else {
                            $attmodel = Attendance::model()->findByPk($check->att_id);
                        }

                        if ($legendid) {
                            $attmodel->user_id = $userid;
                            $attmodel->att_date = $date;
                            $attmodel->att_entry = $legendid;
                            $attmodel->created_by = Yii::app()->user->id;
                            $attmodel->created_date = date("Y-m-d");
                            $attmodel->modified_date = date("Y-m-d");
                            $attmodel->comments = 'From Punch report';

                            if ($attmodel->save()) {
                                echo 'inserted' . "<br/>";
                                //print_r($attmodel->attributes);
                            } else {
                                //print_r($attmodel->getErrors());
                            }
                        }
                    } //end if user
                } //end foreach
            }//die;
            //}

            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
        echo true;
        die();
    }

    public function actionGetattendanceentries($userid = 0, $start, $end) {

        $tbl = Yii::app()->db->tablePrefix; // table prefixforpms
        $userentries = array();

        if ($userid != 0) {
            $where = " user_id=" . $userid . " AND ";
        } else {
            $where = " ";
        }

        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));

        $start1 = date('Y-m-d', strtotime($start));
        $end1 = date('Y-m-d', strtotime($end));

        $fromdate = $start;
        $todate = $end;

        $entrysql = "select pms_attendance.*,pms_legends.*,pms_users.joining_date,
        pms_users.resignation_date from " . $tbl . "attendance inner join " . $tbl . "legends on
        " . $tbl . "attendance.att_entry=" . $tbl . "legends.leg_id inner join pms_users on
        pms_users.userid = pms_attendance.user_id  
        WHERE " . $where . " att_date BETWEEN '" . $start . "' AND '" . $end . "'
        ";
        $entries = Yii::app()->db->createCommand($entrysql)->queryAll();

        $jsonar = array();
        $j = 0;
        $nis = 0;
        $sql_params = array('select' => 'aid,empid, date_format(log_time, "%Y-%m-%d") as log_time ',
            'condition' => "log_time between '$start' and '$end' and approved_status=1");
        $photopunchitems = Photopunch::model()->findAll($sql_params);
        $photo_punch_entry = CHtml::listData($photopunchitems, 'empid', 'aid', 'log_time');

//        echo '<pre>';
//        print_r($photo_punch_entry);
//        die;
        foreach ($entries as $entry) {
            $jsonar[$j]['cid'] = $entry['user_id'] . strtotime($entry['att_date']);
            $jsonar[$j]['legend'] = $entry['short_note'];
            $jsonar[$j]['class_note'] = $entry['class_note'];
            $jsonar[$j]['type'] = ( $entry['type'] == 2 ? "MN" : '');
            
            if(isset($photo_punch_entry[$entry['att_date']]) 
                    and isset($photo_punch_entry[$entry['att_date']][$entry['user_id']])){
                $jsonar[$j]['approved_photo_punch'] = 1;
            }
            else{
                $jsonar[$j]['approved_photo_punch'] = 0;
            }
            
            
            
            $userid = $entry['user_id'];

            if (isset(Yii::app()->user->company_id)) {
                if (isset(Yii::app()->user->allocation_array_session)) {

                    $dates = $this->calculateFromTo(Yii::app()->user->allocation_array_session, $userid, $start, $end);
                    $present = 0;
                    $absent = 0;
                    ;
                    $holi_days = 0;
                    $twop = 0;
                    $sun_days = 0;
                    foreach ($dates as $date) {
                        $start1 = $date['from_date'];
                        $end1 = $date['to_date'];

                        $attendance = $this->getpresentdays($userid, $start1, $end1);
                        $now = strtotime($start); // or your date as well
                        $your_date = strtotime($end);
                        $datediff = $now - $your_date;
                        $countday = round($datediff / (60 * 60 * 24));
                        $countday = trim($countday, "-");
                        $countday = $countday + 1;

                        $present += $attendance['present'];
                        $absent += $attendance['absent'];
                        $holi_days += $attendance['holi_days'];
                        $twop += $attendance['twop'];
                        $sun_days += $attendance['sun_days'];

                        $jsonar[$j]['eid'] = 'user_' . $entry['user_id'];
                        $jsonar[$j]['present_days'] = $present;
                        $jsonar[$j]['absent_days'] = $absent;
                        $jsonar[$j]['holi_days'] = $holi_days;
                        $jsonar[$j]['twop_days'] = $twop;
                        $jsonar[$j]['sun_days'] = $sun_days;

                        $jsonar[$j]['total'] = $present + $absent + $holi_days + $twop + $sun_days;

                        $jsonar[$j]['countday'] = $countday;

                        $j++;
                    }
                }
            } else {

                $attendance = $this->getpresentdays($userid, $start1, $end1);

                $now = strtotime($start); // or your date as well
                $your_date = strtotime($end);
                $datediff = $now - $your_date;
                $countday = round($datediff / (60 * 60 * 24));
                $countday = trim($countday, "-");
                $countday = $countday + 1;


                $jsonar[$j]['eid'] = 'user_' . $entry['user_id'];
                $jsonar[$j]['present_days'] = $attendance['present'];
                $jsonar[$j]['absent_days'] = $attendance['absent'];
                $jsonar[$j]['holi_days'] = $attendance['holi_days'];
                $jsonar[$j]['twop_days'] = $attendance['twop'];
                $jsonar[$j]['sun_days'] = $attendance['sun_days'];

                $jsonar[$j]['total'] = $attendance['present'] + $attendance['absent'] + $attendance['holi_days'] + $attendance['twop'] + $attendance['sun_days'];

                $jsonar[$j]['countday'] = $countday;

                $j++;
            }
        }
//        echo '<pre>';
//        print_r($jsonar);
//
//        die;

        echo json_encode(array('total' => $jsonar));
    }

    public function getpresentdays($userid = 0, $start, $end) {

        $tbl = Yii::app()->db->tablePrefix;
        $sql = " SELECT  SUM(IF(att_entry = 1, 1, 0)) AS present,SUM(IF(att_entry = 2, 1, 0)) AS paid_leave,SUM(IF(att_entry = 3, 1, 0)) AS half_present,  SUM(IF(att_entry = 4, 1, 0)) AS lop,SUM(IF(att_entry =5, 1, 0)) AS holiday,SUM(IF(att_entry =6, 1, 0)) AS sunday,SUM(IF(att_entry =7, 1, 0)) AS two_present,SUM(IF(att_entry =8, 1, 0)) AS single_punch FROM {$tbl}attendance WHERE user_id='$userid' AND  att_date BETWEEN '$start' AND '$end'";


        $attendance = Yii::app()->db->createCommand($sql)->queryRow();

        $present = $attendance['present'];
        $paid_leave = $attendance['paid_leave'];
        $half_present = $attendance['half_present'];
        $lop = $attendance['lop'];
        $holiday = $attendance['holiday'];
        $sunday = $attendance['sunday'];
        $two_present = $attendance['two_present'];
        $single_punch = $attendance['single_punch'];

        $present_days = $present + $paid_leave + $half_present / 2;
        $absent_days = $lop + $half_present / 2;
        $two_p = $two_present;
        $holi_days = $holiday;
        $sunday_days = $sunday;

        $data = array('present' => $present_days, 'absent' => $absent_days, 'twop' => $two_p, 'holi_days' => $holi_days, 'sun_days' => $sunday_days);
        return $data;
    }

    public function getnis($userid, $start, $end) {
        $tbl = Yii::app()->db->tablePrefix; // table prefixforpms
        $datetime1 = new DateTime($start);
        $datetime2 = new DateTime($end);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');
        return $days;
        //return $nis;
    }

    public function actionGetattendance() {
        $tbl = Yii::app()->db->tablePrefix; // table prefixforpms
        $start = $_GET['startdate'];
        $end = $_GET['enddate'];

        $sql = "select * from " . $tbl . "attendance inner join " . $tbl . "legends on " . $tbl . "attendance.att_entry=" . $tbl . "legends.leg_id WHERE att_date BETWEEN '" . $start . "' AND '" . $end . "' ";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        $entries = $command->queryAll();
        $attendence = array();
        $count = 0;
        foreach ($entries as $entry) {
            $attendence[$count]['cid'] = $entry['user_id'] . strtotime($entry['att_date']);
            $attendence[$count]['legend'] = $entry['short_note'];
            $count++;
        }

        $sqltotal = Yii::app()->db->createCommand(" SELECT user_id, SUM(if(att_entry = 1, 1, 0)) AS present, "
                        . "SUM(if(att_entry = 2, 1, 0)) AS casual_leave, SUM(if(att_entry = 3, 1, 0)) AS half_casual_leave,"
                        . "SUM(if(att_entry = 4, 1, 0)) AS approved_leave, SUM(if(att_entry = 5, 1, 0)) AS approved_half_leave, "
                        . "SUM(if(att_entry = 6, 1, 0)) AS paid_leave, SUM(if(att_entry = 7, 1, 0)) AS res_holiday, "
                        . "SUM(if(att_entry = 8, 1, 0)) AS unapproved_leave, SUM(if(att_entry = 9, 1, 0)) AS unapproved_half_leave,"
                        . " SUM(if(att_entry = 10, 1, 0)) AS optional_holiday, SUM(if(att_entry = 11, 1, 0)) AS holiday,"
                        . " SUM(if(att_entry = 12, 1, 0)) AS working_weekend, SUM(if(att_entry = 13, 1, 0)) AS paid_half_leave, "
                        . "SUM(if(att_entry = 14, 1, 0)) AS compensatory_off, SUM(if(att_entry = 15, 1, 0)) AS compensatory_off_half   "
                        . "FROM {$tbl}attendance WHERE att_date BETWEEN '$start' AND '$end' GROUP BY user_id ")->queryAll();


        $jsonar = array();
        $j = 0;
        foreach ($sqltotal as $data) {


            $present = $data['present'];
            $casual_leave = $data['casual_leave'];
            $half_casual_leave = $data['half_casual_leave'];
            $approved_leave = $data['approved_leave'];
            $approved_half_leave = $data['approved_half_leave'];
            $paid_leave = $data['paid_leave'];
            $res_holiday = $data['res_holiday'];
            $unapproved_leave = $data['unapproved_leave'];
            $unapproved_half_leave = $data['unapproved_half_leave'];
            $optional_holiday = $data['optional_holiday'];
            $holiday = $data['holiday'];
            $working_weekend = $data['working_weekend'];
            $paid_half_leave = $data['paid_half_leave'];
            $compensatory_off = $data['compensatory_off'];
            $compensatory_off_half = $data['compensatory_off_half'];
            $nis = 0;


            $present_days = $present + $half_casual_leave / 2 + $approved_half_leave / 2 + $paid_leave + $paid_half_leave / 2;
            $absent_days = $casual_leave + $paid_leave + $half_casual_leave / 2 + $approved_leave + $approved_half_leave / 2 + $unapproved_leave + $unapproved_half_leave / 2;

            /* Sat/Sun calculations */
            $ssdays = array();
            $scount = 0;
            $sdate = $start;
            $end_date = $end;
            for ($i = 0; $i <= (strtotime($sdate) <= strtotime($end_date)); $i++) {

                if (date("D", strtotime($sdate)) == 'Sat' || date("D", strtotime($sdate)) == 'Sun') {
                    $ssdays[] = $sdate;
                    $scount++;
                }

                $sdate = date("Y-m-d", strtotime("+1 day", strtotime($sdate)));
            }
            $satsundays = $scount;
            /* Sat/Sun calculations */


            $total_holidays = $holiday + $optional_holiday;
            $total_paid_holidays = $satsundays + $total_holidays;

            $calendar_days = $present_days + $absent_days + $total_paid_holidays + $nis; //calendar days of wage period

            $cl_entitled = 0; // Casual Leaves entitled
            $cl_utilized = $casual_leave + $half_casual_leave / 2; // Casual Leave utilized
            $pl_utilized = $paid_leave + $paid_half_leave / 2;  //Paid Leave utilized


            $sumof_clpl_utilized = $cl_utilized + $pl_utilized;

            $lop_days = $absent_days - ($cl_utilized + $pl_utilized);  // LOP Days
            //Casual Leave Closing Balance
            $cl_closingbalance = ( ($casual_leave + $half_casual_leave / 2) >= $cl_entitled ? 0 : $cl_entitled - ($casual_leave + $half_casual_leave / 2) );
            $leave_carry_forward = ( ($casual_leave + $half_casual_leave / 2) >= $cl_closingbalance ? 0 : $cl_closingbalance - ($casual_leave + $half_casual_leave / 2) );

            //compensatory_off

            $compensatory_offtotal = $compensatory_off + $compensatory_off_half / 2;

            $saldays = $present_days + $total_paid_holidays + $sumof_clpl_utilized;


            $jsonar[$j]['eid'] = 'user_' . $data['user_id'];
            $jsonar[$j]['satsundays'] = $satsundays;
            $jsonar[$j]['present_days'] = $present_days;
            $jsonar[$j]['holidays'] = $total_holidays;
            $jsonar[$j]['paidholidays'] = $total_paid_holidays;
            $jsonar[$j]['nis'] = $nis;
            $jsonar[$j]['absent_days'] = $absent_days;
            $jsonar[$j]['cl_entitled'] = $cl_entitled;
            $jsonar[$j]['cl_utilized'] = $cl_utilized;
            $jsonar[$j]['pl_utilized'] = $pl_utilized;
            $jsonar[$j]['sum_clpl_utilized'] = $sumof_clpl_utilized;
            $jsonar[$j]['co'] = $compensatory_offtotal;
            $jsonar[$j]['lop_days'] = $lop_days;
            $jsonar[$j]['calendardays'] = $calendar_days;
            $jsonar[$j]['sal_days'] = $saldays;
            $jsonar[$j]['leave_carry_forward'] = $leave_carry_forward;


            $j++;
        }

        echo json_encode(array('attendence' => $attendence, 'total' => $jsonar));
    }

    /* add attendance op up for each column */

    public function actionAddattendance() {


        $this->layout = false;
        $tbl = Yii::app()->db->tablePrefix; // table prefixforpms
        if (isset($_REQUEST['id']) && isset($_REQUEST["cell_date"])) {


            $userid = $_REQUEST['id'];
            $celldate = $_REQUEST["cell_date"];
            $celldateorg = date('Y-m-d', $celldate);
            $model = Attendance::model()->findByAttributes(array('user_id' => $userid, 'att_date' => $celldateorg));

            if (!empty($model)) {
                $legends = Legends::model()->findAll(array('condition' => 'leg_id!= 21', 'condition' => 'leg_id!= 9'));
            } else {
                $legends = Legends::model()->findAll(array('condition' => 'leg_id!= 21', 'condition' => 'leg_id!= 9'));
            }


            $sql2 = 'SELECT * FROM ' . $tbl . 'attendance  JOIN ' . $tbl . 'attendance_log ON ' . $tbl . 'attendance.att_id=' . $tbl . 'attendance_log.refer_id Where ' . $tbl . 'attendance_log.att_date="' . $celldateorg . '" GROUP BY ' . $tbl . 'attendance_log.log_id';
            $command2 = Yii::app()->db->createCommand($sql2);
            $command2->execute();
            $comments = $command2->queryAll();

            if ($model == null) {
                $model = new Attendance;
            } else {
                $model = $this->loadModel($model['att_id']);
            }


            $this->performAjaxValidation($model);
            $this->render('popup', array(
                'model' => $model, 'userid' => $userid, 'cell_date' => $celldate, 'legends' => $legends, 'comments' => $comments,
            ));
        }
    }

    public function actionAddattendancesubmit() {
        $this->layout = false;
        if (isset($_REQUEST['Attendance'])) {
            $userid = $_REQUEST['userid'];
            $celldate = $_REQUEST["cell_date"];
            $celldate = date('Y-m-d', $celldate);

            $model = Attendance::model()->findByAttributes(array('user_id' => $userid, 'att_date' => $celldate));

            if ($model == null) {
                $model = new Attendance;
            } else {
                $model = $this->loadModel($model['att_id']);
            }
            $model->user_id = $userid;
            $celldate = $celldate;
            $model->att_date = $celldate;
            $coment = $_REQUEST["comnt"];

            if ($model->isNewRecord) {
                
            } else {
                $new = new AttendanceLog();
                $new->refer_id = $model->att_id;
                $new->user_id = $model->user_id;
                $new->att_date = $model->att_date;
                $new->comments = $model->comments;
                $new->att_entry = $model->att_entry;
                $new->created_by = Yii::app()->user->id;
                $new->created_date = $model->created_date;
                $new->modified_date = $model->modified_date;
                $new->save();
            }
            $model->attributes = $_REQUEST['Attendance'];
            $model->comments = $coment;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            $model->modified_date = date('Y-m-d H:i:s');
            $model->type = 2;
            $shifttimeattsql = "SELECT * FROM pms_time_attendance "
                    . "WHERE type=0 and user_id =" . $userid . " and `att_date` ='" . $model->att_date . "' ";
            $select_time_attendence = Yii::app()->db->createCommand($shifttimeattsql)->queryColumn();

            if ($_REQUEST['Attendance']['att_entry'] != 21 && !empty($select_time_attendence)) {

                $connection = Yii::app()->db;
                $transaction = $connection->beginTransaction();
                try {
                    $delete_entry = 'delete from pms_time_attendance where att_id=' . intval($select_time_attendence[0]);
                    Yii::app()->db->createCommand($delete_entry)->query();
                    $timemodel = new TimeAttendance;
                    $timemodel->att_id = $select_time_attendence[0];
                    $timemodel->attributes = $_REQUEST['Attendance'];
                    $timemodel->user_id = $userid;
                    $timemodel->comments = 'Changes from attendance';
                    $timemodel->att_time = 0;
                    $timemodel->att_date = $model->att_date;
                    $timemodel->created_by = Yii::app()->user->id;
                    $timemodel->created_date = date('Y-m-d H:i:s');
                    $timemodel->modified_date = date('Y-m-d H:i:s');
                    $timemodel->type = 2;

                    $shifttimeattsqlreplace = "replace into pms_time_attendance (att_id,"
                            . "`user_id`,`att_date`, `comments`, `att_entry`, `att_time`, `created_by`,"
                            . " `created_date`, `modified_date`, `type`) values ({$timemodel->att_id},$userid,'{$model->att_date}'"
                            . ",'Changes from attendance','{$timemodel->att_entry}','{$timemodel->att_time}',{$timemodel->created_by}"
                            . ",'{$timemodel->created_date}', '{$timemodel->modified_date}','$timemodel->type')";

                    Yii::app()->db->createCommand($shifttimeattsqlreplace)->query();

                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollback();
                }
            }


            if ($_REQUEST['Attendance']['att_entry'] == 21) {

                $oldattendance = Attendance::model()->findBypk($model['att_id']);
                $curdate = date('Y-m-d H:i:s');
                $attarr = array($oldattendance->user_id, "'$oldattendance->att_date'", "'$oldattendance->comments'",
                    $oldattendance->att_entry, $oldattendance->created_by, Yii::app()->user->id, "'$oldattendance->created_date'",
                    "'$oldattendance->modified_date'", $oldattendance->type, "'$curdate'", "'manualremove'");
                Yii::app()->db->createCommand("INSERT INTO `pms_attendance_change_log`(`user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `updated_by`, `created_date`, `modified_date`, `type`, `date`, `remarks`) VALUES (" . implode(',', $attarr) . ")")->execute();
                $tbl = Yii::app()->db->tablePrefix;
                $delete = Yii::app()->db->createCommand("DELETE FROM {$tbl}attendance WHERE att_id =" . $model['att_id'])->execute();

                echo 1;
            } else {
                if ($model->validate() && $model->save()) {
                    $log_arry[] = "(NULL,'{$model['user_id']}','{$model['att_date']}','{$model['comments']}','{$model['att_entry']}','{$model['created_by']}','{$model['created_date']}','{$model['modified_date']}','{$model['type']}')";
                    $log_arry = implode(',', $log_arry);
                    //  Yii::app()->db->createCommand("INSERT INTO `pms_attendance_request_log`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  ".$log_arry." ")->query();

                    echo 2;
                }
            }
        }
    }

    /* add attendance pop up submit */

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'attendance-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function add_date($orgDate, $date) {
        $cd = strtotime($orgDate);
        $retDAY = date('d/m/Y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $date, date('Y', $cd)));
        return $retDAY;
    }

    public function getResourcetasks($start_date, $end_date, $resource_id) {
        $tblpxpms = Yii::app()->db->tablePrefix; // table prefixforpms
        $tasks = Yii::app()->db->createCommand("SELECT * FROM " . $tblpxpms . "tasks WHERE `assigned_to` = 6 AND (('$start_date' BETWEEN `start_date` AND `due_date`) OR ('$end_date' BETWEEN `start_date` AND `due_date`))")->queryAll();
        return $tasks;
    }

    public function actionResourceweek() {
        $this->layout = false;
        $tblpxpms = Yii::app()->db->tablePrefix; // table prefixforpms
        $model = new Tasks('search');
        if (isset(Yii::app()->session['start_date']) && isset($_POST['moveto']) && isset(Yii::app()->session['days_count'])) {
            $daysdiff = Yii::app()->session['days_count'] = $this->getcountBetween2Dates(Yii::app()->session['start_date'], Yii::app()->session['end_date']);
            $this->week_start_date = $this->get_ranges_date(Yii::app()->session['start_date'], $_POST['moveto'], $daysdiff);
            Yii::app()->session['start_date'] = $this->week_start_date;
        }

        $usermodel = Users::model()->findAll(array(
            'select' => '*',
            'condition' => 'status=0',
            'order' => 'first_name',
        ));
        $this->render('calandersheet', array(
            'model' => $model,
            'usermodel' => $usermodel,
            'start_date' => Yii::app()->session['start_date'],
            'end_date' => Yii::app()->session['end_date'],
        ));
    }

    public function get_ranges_date($orgDate, $moveto, $daysdiff) {
        $cd = strtotime($orgDate);

        if ($moveto == 'prev') {
            $retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) - $daysdiff, date('Y', $cd)));
            if (isset(Yii::app()->session['end_date'])) {
                $days_ago = date('Y-m-d', strtotime('-' . $daysdiff . ' days', strtotime(Yii::app()->session['end_date'])));
                Yii::app()->session['end_date'] = $days_ago;
            }
        } elseif ($moveto == 'next') {
            $retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $daysdiff, date('Y', $cd)));
            if (isset(Yii::app()->session['end_date'])) {
                $days_ago = date('Y-m-d', strtotime('+' . $daysdiff . ' days', strtotime(Yii::app()->session['end_date'])));
                Yii::app()->session['end_date'] = $days_ago;
            }
        } else {
            $monday = date('Y-m-d', strtotime('monday this week'));
            $retDAY = $monday;
        }

        return $retDAY;
    }

    public function date_range($startdate, $enddate) {
        //echo $startdate . "--" . $enddate;
        $ts = strtotime($startdate);
        $start = (date('w', $ts) == 1) ? $ts : strtotime('last day', $ts);
        return array($startdate, $enddate, date('F d, Y', $start));
    }

    //Weeks column generation
    public function x_week_range($date) {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 1) ? $ts : strtotime('first day of this month', $ts);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('last day of this month', $start)), date('F d, Y', $start));
    }

    function getDatesBetween2Dates($startTime, $endTime) {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    function getcountBetween2Dates($startTime, $endTime) {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return count($days);
    }

    /* add legend color */

    public function actionAddlegendcolor() {


        $data = $_REQUEST;
        $data = array_slice($data, 1);
        foreach ($data as $key => $color) {
            $colorlegid = explode('_', $key);
            if ($colorlegid[0] == 'legendcolor') {
                $leg_id = $colorlegid[1];
                $bg_color = $color;
                $legend = Legends::model()->updateByPk($leg_id, array(
                    'color_code' => $bg_color
                ));
            } else {
                $leg_id = $colorlegid[1];
                $txt_color = $color;
                $legend = Legends::model()->updateByPk($leg_id, array(
                    'text_color' => $txt_color
                ));
            }
        }

        echo true;
    }

    /* add legend color */


    /* get attendance by date */

    public function actionGetattendancebydate() {

        $this->layout = false;
        $tblpxpms = Yii::app()->db->tablePrefix;

        if (isset($_GET['date'])) {
            $attdate = $_GET['date'];
        } else {
            $attdate = date('Y-m-d');
        }

        $startdate = (isset($_REQUEST['startdate']) ? date('Y-m-d', strtotime($_REQUEST['startdate'])) : "");
        $enddate = (isset($_REQUEST['startdate']) ? date('Y-m-d', strtotime($_REQUEST['enddate'])) : "");


        $usermodel = Yii::app()->db->createCommand("SELECT * FROM {$tblpxpms}users ed
       WHERE   (ed.joining_date<= '$startdate' OR ed.joining_date <= '$enddate' OR ed.joining_date IS NULL) AND (ed.resignation_date >= '$startdate' OR ed.resignation_date >= '$enddate' OR ed.resignation_date IS NULL) ORDER BY first_name asc")->queryAll();

        $legends = Legends::model()->findAll();
        $att_settings = AttendanceSettings::model()->findByPk(1);
        $attendance = Attendance::model()->findAll(array(
            'select' => '*',
            'condition' => 'att_date="' . $attdate . '"',
        ));
        $this->render('getuserattendancebydate', array('user' => $usermodel, 'legends' => $legends, 'attendance' => $attendance, 'att_settings' => $att_settings));
    }

    /* get attendance by date */

    public function actionaddsettings() {


        $data = $_REQUEST;


        if ($data != NULL) {

            $att_day = $data['att_day'];
            $att_month = $data['att_month'];
            $till_day = $data['till_day'];
            $created_date = date('Y-m-d H:i:s');

            $date = date('Y-m-' . $att_day);
            $last_day_mnth = date("t", strtotime($date));


            if ($att_month == 1 && $att_day == 1 && $till_day == 1) {
                $till_day = NULL;
            }
            if ($att_day == NULL) {
                $att_day = 1;
            }


            if ($att_month == 0 && $till_day != NULL && $till_day < $att_day && $till_day < $last_day_mnth) {
                echo 'Error, Till day less than from day';
                exit;
            } elseif ($att_month == 1 && $till_day != NULL && $till_day > $att_day) {
                echo "Error , 1 month exceeds";
                exit;
            } elseif ($till_day > $last_day_mnth) {
                echo "Error , day of month exceeds";
                exit;
            } else {


                if ($att_month == 1 && $till_day == NULL) {
                    if ($att_day > 1) {
                        $till_day = $att_day - 1;
                    } else {
                        $till_day = NULL;
                    }
                }

                $model = AttendanceSettings::model()->updateByPk(1, array(
                    'att_day' => $att_day, 'att_month' => $att_month, 'till_day' => $till_day, 'created_date' => $created_date,
                ));
                echo "success";
                exit;
            }
        } else {
            echo 'error';
        }
    }

    /* add common attendance */

    public function actionAddcommonattendance() {
        $data = $_REQUEST;
        $date = date('Y-m-d', strtotime(end($data)));
        $data = array_slice($data, 1, -1);
        $useridlegid = array();
        if ($data != NULL) {
            foreach ($data as $key => $user) {
                $useridlegid = explode('_', $user);
                $userid = isset($useridlegid[0]) ? $useridlegid[0] : "";
                $legid = isset($useridlegid[1]) ? $useridlegid[1] : "";
                $model = Attendance::model()->findByAttributes(array('user_id' => $userid, 'att_date' => $date));
                if ($model == NULL) {
                    $model = new Attendance();
                    $model['user_id'] = $userid;
                    $model['att_date'] = $date;
                    $model['att_entry'] = $legid;
                    $model['created_by'] = Yii::app()->user->id;
                    $model['created_date'] = date('Y-m-d');
                    $model['modified_date'] = date('Y-m-d');
                    $model->save(false);
                } else {
                    if ($legid != 0) {
                        $model = Attendance::model()->updateByPk($model->att_id, array(
                            'att_entry' => $legid, 'modified_date' => date('Y-m-d')
                        ));
                    } else {
                        $model = Attendance::model()->deleteByPk($model->att_id);
                    }
                }
            }
        }
    }

    /* add common attendance */

    public function actionImportdata() {
        $tblpx = Yii::app()->db->tablePrefix;
        $newmodel = new ImportForm();

        $model = new Attendance();

        $tempmodel = new ImportAttendance('search');
        $tempmodel->unsetAttributes();  // clear any default values
        if (isset($_GET['ImportAttendance']))
            $tempmodel->attributes = $_GET['ImportAttendance'];

        if (isset($_POST['Attendance'])) {
            if ((!empty($_POST['Attendance']['from'])) && (!empty($_POST['Attendance']['to']))) {


                if (isset($_POST['ImportForm'])) {

                    $newmodel->attributes = $_POST['ImportForm'];
                    if ($newmodel->validate()) {

                        $csvFile = CUploadedFile::getInstance($newmodel, 'file');
                        $tempLoc = $csvFile->getTempName();
                        try {
                            $transaction = Yii::app()->db->beginTransaction();
                            $handle = fopen("$tempLoc", "r");
                            $row = 1;
                            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                                if ($row > 4) {
                                    /* echo "<pre>";
                                      print_r($data);
                                      echo "</pre>"; */
                                    $from = $_POST['Attendance']['from'];
                                    $to = $_POST['Attendance']['to'];
                                    $date = date('Y-m', strtotime($from)) . '-16';
                                    $end_date = date('Y-m', strtotime($to)) . '-15';

                                    $name = $data[1];
                                    $userid = $data[2];
                                    $k = 3;
                                    while (strtotime($date) <= strtotime($end_date)) {

                                        $legend_status = mysql_real_escape_string($data[$k]);
//                                        echo $legend_status;
                                        $leg = Yii::app()->db->createCommand("SELECT leg_id FROM {$tblpx}legends "
                                                        . "WHERE short_note = '$legend_status'")->queryRow();
                                        $legend = $leg['leg_id'];



                                        if (!empty($legend)) {
                                            $exist = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}attendance WHERE user_id = '$userid' AND att_date = '$date' ")->queryAll();
                                            if (count($exist) == 0) {

                                                $attmodel = new Attendance();
                                                $attmodel->user_id = $userid;
                                                $attmodel->att_date = $date;
                                                $attmodel->att_entry = $legend;
                                                $attmodel->created_by = Yii::app()->user->id;
                                                $attmodel->created_date = date("Y-m-d H:i:s");
                                                $attmodel->modified_date = date("Y-m-d H:i:s");

                                                if ($attmodel->save()) {
                                                    
                                                } else {
                                                    $status = array();
                                                    $resmodel = new ImportAttendance();
                                                    $resmodel->username = $name;
                                                    $resmodel->att_date = $date;
                                                    $resmodel->att_entry = $legend_status;
                                                    $resmodel->created_by = Yii::app()->user->id;
                                                    $resmodel->created_date = date("Y-m-d H:i:s");
                                                    $resmodel->modified_date = date("Y-m-d H:i:s");
                                                    $errors = $attmodel->getErrors();
                                                    foreach ($errors as $err) {
                                                        array_push($status, $err[0]);
                                                    }
                                                    $resmodel->import_status = implode(",", $status);
                                                    $resmodel->save();
                                                }
                                            }
                                        }
                                        $k++;
                                        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                                    }
                                }
                                $row++;
                            }
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', "Data saved!");
                        } catch (Exception $error) {
                            print_r($error);
                            $transaction->rollback();
                        }
                        Yii::app()->user->setFlash('success', 'Data saved!');
                        //$this->redirect(array('attendance/n'));
                    } else {
                        print_r($newmodel->getErrors());
                        //$errors = implode(',', $newmodel->getErrors());
                        Yii::app()->user->setFlash('error', $errors);
                    }
                }
            } else {
                Yii::app()->user->setFlash('error', 'Choose date range!');
            }
        }
        $this->render('import', array('newmodel' => $newmodel, 'model' => $model, 'tempmodel' => $tempmodel));
    }

    public function actionDownloaddoc() {
        $doc = 'attendance_sample.csv';
        $path = Yii::getPathOfAlias('webroot') . '/uploads/';
        $file = $path . '' . $doc;
        if (file_exists($file)) {
            Yii::app()->getRequest()->sendFile($doc, file_get_contents($file));
        }
    }

    public function actionCronjobtoaddattendance($date = "") {
        if ($date == "") {
            $date = date('Y-m-d', strtotime(' -1 day'));
        }
        // $date="2017-06-14";
        $attn = Yii::app()->createController('Accesslog');
        $result[$date] = $attn[0]->calcHours2($date);
        //echo '<pre>';print_r($result);die;
        $result_arr = $this->calculatetime($result);
        //echo '<pre>';print_r($result_arr);die();
        //foreach ($result_arr as $result) {
        foreach ($result_arr as $res) {
            $offdays = array();
            $accesscardid = $res['accesscard_id'];
            $firstpunch = str_replace("HL", "", $res['first_punch']);
            $lastpunch = str_replace("HL", "", $res['last_punch']);
            $intime = $res['inhrs'];
            $outtime = $res['outhrs'];
            $legendid = 0;
            $weekly_off1_days = array();
            $user = Users::model()->find(array('condition' => "accesscard_id = $accesscardid"));

            if (!empty($user)) {

                $userid = $user['userid'];
                $role = $user['user_type'];
                if ($role) {
                    $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));
                    //echo '<pre>';print_r($rsettings);die();

                    if (!empty($rsettings)) {

                        $offdays = $this->getOffdays($date, $rsettings);

                        $latecoming_gt = $rsettings->late_coming_gt;
                        $earlygoing_gt = $rsettings->early_going_gt;
                        $timeto_in = strtotime('09:00:00');
                        if (!is_null($latecoming_gt)) {
                            $grace_inpunch = date("H:i:s", strtotime('+' . $latecoming_gt . ' minutes', $timeto_in));
                        } else {
                            $grace_inpunch = "09:00:00";
                        }
                        $timeto_out = strtotime('06:00:00');
                        if (!is_null($earlygoing_gt)) {
                            $grace_outpunch = date("H:i:s", strtotime('-' . $earlygoing_gt . ' minutes', $timeto_out));
                        } else {
                            $grace_outpunch = '06:00:00';
                        }


                        if (!in_array($date, $offdays)) {
                            if (!empty($firstpunch) && $firstpunch != "05:30:00") {
                                //echo $firstpunch;die;
                                //echo $rsettings->halfday_duration_mins;die;
                                $legend = Legends::model()->find(array('condition' => "short_note = 'P'"));
                                $legendid = $legend->leg_id; //echo 'P';die();
                                //Calculate half day if work duration is less than given value

                                if ($rsettings->halfday_by_duration == 1) {

                                    $time = explode(':', $intime);
                                    $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                    if ($mins < $rsettings->halfday_duration_mins) {
                                        $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                        $legendid = $legend->leg_id;
                                    }
                                } else if ($rsettings->absent_by_duration == 1) {
                                    //Calculate absent if work duration is less than given value

                                    $time = explode(':', $intime);
                                    $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);


                                    //echo $intime."--".$mins; echo "<br/>";
                                    if ($mins < $rsettings->absent_duration_mins) {
                                        $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                        $legendid = $legend->leg_id;
                                    }
                                }



                                //Mark Half Day if late by given time
                                if ($rsettings->halfday_by_late == 1) {
                                    $now = new DateTime($grace_inpunch);
                                    $then = new DateTime($firstpunch);
                                    $diff = $now->diff($then);
                                    $mins = $diff->format('%i');
                                    if ($mins >= $rsettings->halfday_late_mins) {
                                        $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                        $legendid = $legend->leg_id;
                                    }
                                }

                                //Mark Half Day if early going by given time
                                if ($rsettings->halfday_by_earlygoing == 1) {
                                    $now = new DateTime($grace_outpunch);
                                    $then = new DateTime($lastpunch);
                                    $diff = $now->diff($then);
                                    $mins = $diff->format('%i');


                                    if ($mins >= $rsettings->halfday_earlygoing_mins) {
                                        $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                        $legendid = $legend->leg_id;
                                    }
                                }







                                //On partial day, Calculate half day if work duration is less than given value
                                if ($rsettings->partial_halfday_by_duration == 1) {
                                    if ($firstpunch > $grace_inpunch) {
                                        $time = explode(':', $intime);
                                        $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                        if ($mins < $rsettings->partial_halfday_duration_mins) {
                                            $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                                            $legendid = $legend->leg_id;
                                        }
                                    }
                                } else if ($rsettings->partial_absent_by_duration == 1) {
                                    //On partial day, Calculate absent if work duration is less than given value
                                    if ($firstpunch > $grace_inpunch) {
                                        $time = explode(':', $intime);
                                        $mins = ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
                                        if ($mins < $rsettings->partial_absent_duration_mins) {
                                            $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                            $legendid = $legend->leg_id;
                                        }
                                    }
                                }
                            } else {
                                //Absent
                                $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                $legendid = $legend->leg_id;
                            }
                        } else {


                            //case of weekly holidays
                            $result_arr2 = array();
                            $hdate = $date;
                            $prevdate = date('Y-m-d', strtotime('-1 day', strtotime($hdate))); //echo $prevdate; //die;
                            $weeklyoffdays = $this->getOffdays($prevdate, $rsettings);
                            if ($rsettings->absent_for_prefix == 1) {
                                if (!in_array($prevdate, $weeklyoffdays)) {
                                    $result[$date] = $attn[0]->calcHours2($prevdate);
                                    // echo $accesscardid;die();
                                    $result_arr2 = $result_arr2[$prevdate] = $this->calculatetime($result, $accesscardid);
                                    if (!empty($result_arr2)) {
                                        $prevarr = $result_arr2[$prevdate];
                                        $fpunch = $prevarr['first_punch'];
                                        if (empty($fpunch) || $fpunch == "05:30:00") {
                                            $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                            $legendid = $legend->leg_id;
                                        }
                                    }
                                }
                            }


                            $result_arr3 = array();
                            $nextdate = date('Y-m-d', strtotime('+1 day', strtotime($hdate)));

                            if ($rsettings->absent_for_suffix == 1) {
                                if (!in_array($nextdate, $weeklyoffdays)) {
                                    $result[$date] = $attn[0]->calcHours2($nextdate);
                                    $result_arr3 = $result_arr3[$nextdate] = $this->calculatetime($result, $accesscardid);
                                    if (!empty($result_arr3)) {
                                        $nextarr = $result_arr3[$nextdate];
                                        $fpunch = $nextarr['first_punch'];
                                        if (empty($fpunch) || $fpunch == "05:30:00") {
                                            $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                            $legendid = $legend->leg_id;
                                        }
                                    }
                                }
                            }

                            $result_arr4 = array();
                            $result_arr5 = array();
                            if (!in_array($prevdate, $weeklyoffdays) && !in_array($nextdate, $weeklyoffdays)) {
                                $result[$date] = $attn[0]->calcHours2($prevdate);
                                $result_arr4 = $result_arr4[$prevdate] = $this->calculatetime($result, $accesscardid);

                                $result[$date] = $attn[0]->calcHours2($nextdate);
                                $result_arr5 = $result_arr5[$nextdate] = $this->calculatetime($result, $accesscardid);
                                // $result_arr4[$prevdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$prevdate' AND resource_id = '$accesscardid' ")->queryRow();
                                //$result_arr5[$nextdate] = Yii::app()->db->createCommand("SELECT * FROM {$tbl}punchreport WHERE logdate = '$nextdate' AND resource_id = '$accesscardid' ")->queryRow();

                                if (!empty($result_arr4) || !empty($result_arr5)) {
                                    $prevarr = $result_arr4[$prevdate];
                                    $fpunchprev = $prevarr['first_punch'];

                                    $nextarr = $result_arr5[$nextdate];
                                    $fpunchnext = $nextarr['firstpunch'];
                                    if (empty($fpunchprev) || $fpunchprev == "05:30:00" && empty($fpunchnext) || $fpunchnext == "05:30:00") {
                                        $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                                        $legendid = $legend->leg_id;
                                    }
                                }
                            }
                        }
                    }//end if role settings
                }//end if role
                else {

                    if (!empty($firstpunch) && $firstpunch != "05:30:00") {
                        if (strtotime($inhrs) < strtotime("04:00:00")) {
                            //Half day
                            $legend = Legends::model()->find(array('condition' => "short_note = 'HP'"));
                            $legendid = $legend->leg_id; //echo 'HP';die;
                        } else {
                            $legend = Legends::model()->find(array('condition' => "short_note = 'P'"));
                            $legendid = $legend->leg_id; //echo 'P';die;
                        }
                    } else {
                        //Absent
                        $legend = Legends::model()->find(array('condition' => "short_note = 'AL'"));
                        $legendid = $legend->leg_id; //echo "CL";die;
                    }
                }
                //echo $legendid;die();
                $check = Attendance::model()->find(array('condition' => "user_id = $userid AND att_date = '$date'"));
                if (count($check) == 0) {
                    $attmodel = new Attendance();
                } else {
                    $attmodel = Attendance::model()->findByPk($check->att_id);
                }


                if ($legendid) {
                    $attmodel->user_id = $userid;
                    $attmodel->att_date = $date;
                    $attmodel->att_entry = $legendid;
                    $attmodel->created_by = Yii::app()->user->id;
                    $attmodel->created_date = date("Y-m-d");
                    $attmodel->save();
                }
            } //end if user
        } //end foreach

        Yii::app()->user->setFlash('success', 'Data added successfully.');
        $this->redirect(Yii::app()->createUrl('attendance/index'));
    }

    public function Calculatetime($result_arr = array(), $resource_id = "") {
        //echo "<pre>";print_r($result_arr);die;
        $result = array();
        foreach ($result_arr as $key => $value) {
            foreach ($value as $key1 => $value1) {
                if (($resource_id != "") && ($value1['accesscard_id'] != $resource_id)) {
                    break;
                }
                $empid = $value1['accesscard_id'];
                $empname = $value1['emp_name'];
                $date = date('Y-m-d', strtotime($value1['first_punch']));
                //$final_arr[$empid][$empid.'-'.$date]   =   $value1;
                //$final_arr[$empid][]   =   $value1;

                if (empty($value1['first_punch'])) {
                    $logdate = date('Y-m-d', strtotime($key));
                } else {
                    $logdate = date('Y-m-d', strtotime($value1['first_punch']));
                }
                //$resource_id = $value1['accesscard_id'];
                $intime = $value1['inhrs'];
                $outtime = $value1['outhrs'];
                $totaltime = gmdate('H:i:s', $value1['insec'] + $value1['outsec']);

                $late_halfday = $date . " 11:00:01";
                $ee_halfday = $date . " 16:00:00";

                $starttime = $date . " 09:00:00";
                $endtime = $date . " 06:00:00";

                $HL = 'HL';


                /* apply role settings here */
                $ot_formula = 0;
                $othrs_total = 0;

                $user = Users::model()->find(array('condition' => "accesscard_id = $empid"));
                $role = $user['user_type'];
                $rsettings = RoleSettings::model()->find(array('condition' => "role_id = $role"));

                if (!empty($rsettings)) {
                    $latecoming_gt = $rsettings->late_coming_gt;
                    $earlygoing_gt = $rsettings->early_going_gt;

                    $latein = strtotime("09:00:00") + strtotime($latecoming_gt);
                    $latein_gt = $date . "" . date("H:i:s", $latein);

                    $earlyout = strtotime("06:00:00") - strtotime($latecoming_gt);
                    $earlyout_gt = $date . "" . date("H:i:s", $earlyout);



                    if (strtotime($value1['first_punch']) > strtotime($latein_gt)) {
                        $value1['first_punch'] = date('H:i:s', strtotime($value1['first_punch'])) . " " . $HL;
                    } else {
                        $value1['first_punch'] = date('H:i:s', strtotime($value1['first_punch']));
                    }

                    if (strtotime($value1['last_punch']) < strtotime($earlyout_gt) && date('H:i:s', strtotime($value1['last_punch'])) != '05:30:00') {
                        $value1['last_punch'] = date('H:i:s', strtotime($value1['last_punch'])) . " " . $HL;
                    } else {
                        $value1['last_punch'] = date('H:i:s', strtotime($value1['last_punch']));
                    }


                    //OT
                    $ot_formula = $rsettings->ot_formula;
                    $min_othrs = 0;
                    $max_othrs = 0;
                    if ($ot_formula == 1) {
                        $min_othrs = $rsettings->min_ot;
                        if ($rsettings->max_ot_yes) {
                            $max_othrs = $rsettings->max_ot;
                        }
                    }


                    //Consider Early coming punch & Late going punch
                    $early_coming_punch = $rsettings->early_coming_punch;
                    $late_going_punch = $rsettings->late_going_punch;


                    //Deduct break hours from work duration
                    $deduct_break = $rsettings->deduct_breakhours;
                    if ($deduct_break == 1) {
                        $intime = strtotime($intime) - strtotime('01:00:00');
                        $intime = date("H:i:s", $intime);
                    }



                    //OT hours
                    $total_ot = 0;
                    $min_ot1 = 0;
                    $min_ot2 = 0;
                    if ($ot_formula == 1) {

                        $ot1 = 0;
                        $ot2 = 0;
                        if ($early_coming_punch == 1) {
                            $fpunch = str_replace("HL", "", $value1['first_punch']);
                            if (strtotime($fpunch) < strtotime($starttime)) {
                                $now = new DateTime($value1['first_punch']);
                                $then = new DateTime($starttime);
                                $diff = $now->diff($then);
                                $hours = $diff->format('%h');
                                $minutes = $diff->format('%i');
                                $seconds = $diff->format('%s');
                                $ot1 = $hours . ":" . $minutes . ":" . $seconds;
                                $min1 = $hours * 60;
                                $min2 = $seconds / 60;
                                $min_ot1 = $min1 + $minutes + $min2;
                            }
                        }

                        if ($late_going_punch == 1) {
                            $lpunch = str_replace("HL", "", $value1['last_punch']);
                            if (strtotime($lpunch) > strtotime($endtime)) {
                                $now = new DateTime($value1['last_punch']);
                                $then = new DateTime($endtime);
                                $diff = $now->diff($then);
                                $hours = $diff->format('%h');
                                $minutes = $diff->format('%i');
                                $seconds = $diff->format('%s');
                                $ot2 = $hours . ":" . $minutes . ":" . $seconds;
                                $min1 = $hours * 60;
                                $min2 = $seconds / 60;
                                $min_ot2 = $min1 + $minutes + $min2;
                            }
                        }
                        $total_ot = $min_ot1 + $min_ot2;
                        if ($total_ot > $min_othrs && $total_ot < $max_othrs) {
                            $othrs_total = date("H:i:s", strtotime($total_ot));
                        } else {
                            $othrs_total = 0;
                        }
                    } else {
                        $workhours = "08:00:00";
                        if (strtotime($intime) > strtotime($workhours) && $intime != "18:30:00") {

                            $now = new DateTime($intime);
                            $then = new DateTime($workhours);
                            $diff = $now->diff($then);
                            $hours = $diff->format('%h');
                            $min = $hours * 60;
                            $minutes = $diff->format('%i');
                            $seconds = $diff->format('%s');
                            $othrs_total = $hours . ":" . $minutes . ":" . $seconds;
                            $otmin = $min + $minutes;
                        } else {
                            $othrs_total = NULL;
                            $otmin = 0;
                        }
                    }
                    // end OT calculations
                    /* apply role settings here--end */
                } else {






                    if (strtotime($value1['first_punch']) > strtotime($late_halfday)) {
                        $value1['first_punch'] = date('H:i:s', strtotime($value1['first_punch'])) . " " . $HL;
                    } else {
                        $value1['first_punch'] = date('H:i:s', strtotime($value1['first_punch']));
                    }

                    if (strtotime($value1['last_punch']) < strtotime($ee_halfday) && date('H:i:s', strtotime($value1['last_punch'])) != '05:30:00') {
                        $value1['last_punch'] = date('H:i:s', strtotime($value1['last_punch'])) . " " . $HL;
                    } else {
                        $value1['last_punch'] = date('H:i:s', strtotime($value1['last_punch']));
                    }
                }

                $firstpunch = $value1['first_punch'] == '' ? '' : $value1['first_punch'];
                $lastpunch = $value1['last_punch'] == '' ? '' : $value1['last_punch'];
                $value1['intime'] = $intime;
                $value1['outtime'] = $outtime;
                $value1['totaltime'] = $totaltime;
                $value1['othours'] = $othrs_total;



                $result[] = $value1;
                /* $sql = "INSERT INTO {$tblpx}punchreport (logdate,resource_id,firstpunch,lastpunch,intime,outtime,totaltime,ot_hours) VALUES (:logdate,:resource_id,:firstpunch,:lastpunch,:intime,:outtime,:totaltime, :othours)";
                  $parameters = array(":logdate" => $logdate, ":resource_id" => $resource_id, ":firstpunch" => $firstpunch, ":lastpunch" => $lastpunch, ":intime" => $intime, ":outtime" => $outtime, ":totaltime" => $totaltime, ":othours" => $othrs_total);
                  Yii::app()->db->createCommand($sql)->execute($parameters); */
            }
            continue;
        }
        return $result;
    }

    public function getOffdays($date, $rsettings = array()) {


        $offdays = array();

        //weekly off1
        $weekly_off1_yes = $rsettings->weekly_off1_yes;
        if (!empty($weekly_off1_yes)) {
            $weekly_off1 = $rsettings->weekly_off1;
            if (date("l", strtotime($date)) == $weekly_off1) {
                array_push($offdays, $date);
            }
        }


        //weekly off2
        $weekly_off2_yes = $rsettings->weekly_off2_yes;
        if (!empty($weekly_off2_yes)) {
            $weekly_off2 = $rsettings->weekly_off2;
            $weekly_off2_count = $rsettings->weekly_off2_count; //echo date('d',strtotime('+1 week '.$weekly_off2.''));

            $weekly_off2_array = explode(",", $weekly_off2_count);
            $wk = date("M Y", strtotime($date));


            foreach ($weekly_off2_array as $warr) {
                if ($warr == 1) {
                    $num = "first";
                } else if ($warr == 2) {
                    $num = "second";
                } else if ($warr == 3) {
                    $num = "third";
                } else if ($warr == 4) {
                    $num = "fourth";
                } else if ($warr == 5) {
                    $num = "last";
                } else {
                    $num = "";
                }
                $dt = date("Y-m-d", strtotime("$num $weekly_off2 of $wk"));
                if ($dt == $date) {
                    array_push($offdays, $date);
                }
            }
        }


        if (!array_key_exists($date, $offdays)) {
            if (date("D", strtotime($date)) == 'Sat' || date("D", strtotime($date)) == 'Sun') {
                array_push($offdays, $date);
            }
        }

        return $offdays;
    }

    public function actionDeletecronjob($date = "") {
        if ($date == "") {
            $date = date('Y-m-d', strtotime(' -1 day'));
        }
        $tbl = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("Select * FROM {$tbl}attendance WHERE  att_date = '{$date}'")->queryAll();
        if ($data != NULL) {
            foreach ($data as $da) {
                $data = Attendance::model()->deleteByPK($da['att_id']);
            }
        }
        if ($data) {
            Yii::app()->user->setFlash('success', 'Data deleted successfully.');
        }
        $this->redirect(Yii::app()->createUrl('attendance/index'));
    }

    public function actionAttEntryReport() {


        if (isset($_REQUEST['siteid']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('siteid', intval($_REQUEST['siteid']));
            if (Yii::app()->user->siteid == 0) {
                unset(Yii::app()->user->siteid);
            }
            die('1');
        }


        if (isset($_REQUEST['empid']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('empid', intval($_REQUEST['empid']));
            if (Yii::app()->user->empid == 0) {
                unset(Yii::app()->user->empid);
            }
            die('1');
        }




        $tblpx = Yii::app()->db->tablePrefix;
        $sdate = (!isset($_GET['sdate']) ? date('Y-m-01') : date('Y-m-d', strtotime($_GET['sdate'])));
        $tdate = (!isset($_GET['tdate']) ? date('Y-m-d') : date('Y-m-d', strtotime($_GET['tdate'])));
        $deptment = (isset($_GET['deptment']) ? intval($_GET['deptment']) : 0);

        $siteid = 0;
        if (isset(Yii::app()->user->siteid) and Yii::app()->user->siteid > 0) {
            $siteid = Yii::app()->user->siteid;
        }

        $empid = 0;
        if (isset(Yii::app()->user->empid) and Yii::app()->user->empid > 0) {
            $empid = Yii::app()->user->empid;
        }

        $depwhere = '';
        if ($deptment > 0) {
            $depwhere = "and dept.department_id=" . $deptment;
        }

        $sitecon = '';
        if ($siteid > 0) {
            $sitecon = "and pds.site_id=" . $siteid;
        }

        $empcon = '';
        if ($empid > 0) {
            $empcon = "and us.userid=" . $empid;
        }


        $company = '';
        if (isset(Yii::app()->user->company_id) && Yii::app()->user->company_id != '') {

            $company = ' and us.company_id = ' . Yii::app()->user->company_id;
        }


        $userslist = " SELECT us.userid,comments as comment,att_date as date,concat_ws(' ',us.first_name,us.last_name) as fullname,label,us.designation,mn.created_date,concat_ws(' ',emp.first_name,emp.last_name) as created_by ,description,short_note FROM `pms_attendance` as mn
            inner join pms_users as us on us.userid = mn.user_id left join pms_employee_default_data as dd on dd.default_data_id = us.user_type  left join pms_users as emp on emp.userid = mn.created_by left join pms_legends as le on le.leg_id = mn.att_entry WHERE mn.`type` != 1 and `att_date`between '$sdate' and '$tdate' " . $empcon . " " . $company . " ORDER BY `mn`.`att_date`  DESC";


        $users = Yii::app()->db->createCommand($userslist)->queryAll();
        $useritems = array();
        $userdata = array();
        $r = 0;

        //echo '<pre>';print_r($users);exit;
        foreach ($users as $key => $user) {
            $r++;

            $userdata = array('userid' => $user['userid'],
                'userid' => $user['userid'],
                'designation' => $user['designation'],
                'fullname' => $user['fullname'],
                'usertype' => $user['label'],
                'cr_date' => $user['created_date'],
                'cr_by' => $user['created_by'],
                'coment' => $user['comment'],
                'date' => $user['date'],
                'description' => $user['description'],
                'short_note' => $user['short_note'],
            );
            $useritems[] = $userdata;
        }

        // echo '<pre>';print_r($useritems);exit;



        if (isset($_GET['ajaxcall'])) {
            echo $retarray = $this->renderPartial("attentryreport", array('users' => $users,
        'start_date' => $sdate,
        'end_date' => $tdate,
        'results' => $useritems), true);
        } else {
            $this->render("attentryreport", array('users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems));
        }
    }

    public function actionAttLog() {


        if (isset($_REQUEST['siteid']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('siteid', intval($_REQUEST['siteid']));
            if (Yii::app()->user->siteid == 0) {
                unset(Yii::app()->user->siteid);
            }
            die('1');
        }


        if (isset($_REQUEST['empid']) && isset($_REQUEST['ajax'])) {
            Yii::app()->user->setState('empid', intval($_REQUEST['empid']));
            if (Yii::app()->user->empid == 0) {
                unset(Yii::app()->user->empid);
            }
            die('1');
        }




        $tblpx = Yii::app()->db->tablePrefix;
        $sdate = (!isset($_GET['sdate']) ? date('Y-m-01') : date('Y-m-d', strtotime($_GET['sdate'])));
        $tdate = (!isset($_GET['tdate']) ? date('Y-m-t') : date('Y-m-d', strtotime($_GET['tdate'])));
        $deptment = (isset($_GET['deptment']) ? intval($_GET['deptment']) : 0);

        $siteid = 0;
        if (isset(Yii::app()->user->siteid) and Yii::app()->user->siteid > 0) {
            $siteid = Yii::app()->user->siteid;
        }

        $empid = 0;
        if (isset(Yii::app()->user->empid) and Yii::app()->user->empid > 0) {
            $empid = Yii::app()->user->empid;
        }

        $depwhere = '';
        if ($deptment > 0) {
            $depwhere = "and dept.department_id=" . $deptment;
        }

        $sitecon = '';
        if ($siteid > 0) {
            $sitecon = "and pds.site_id=" . $siteid;
        }

        $empcon = '';
        if ($empid > 0) {
            $empcon = "and us.userid=" . $empid;
        }


        $company = '';
        if (isset(Yii::app()->user->company_id) && Yii::app()->user->company_id != '') {

            $company = ' and us.company_id = ' . Yii::app()->user->company_id;
        }

        $userslist = "  SELECT us.userid,comments as comment,att_date as date,concat_ws(' ',us.first_name,us.last_name) as fullname,label,us.designation,mn.created_date,concat_ws(' ',emp.first_name,emp.last_name) as created_by ,description,short_note FROM `pms_attendance_log` as mn
            inner join pms_users as us on us.userid = mn.user_id left join pms_employee_default_data as dd on dd.default_data_id = us.user_type left join pms_users as emp on emp.userid = mn.created_by left join pms_legends as le on le.leg_id = mn.att_entry
              WHERE `att_date`between '$sdate' and '$tdate' " . $empcon . " " . $company . " and us.attendance_ot_access =1 group by userid,att_entry,att_date ORDER BY `mn`.`att_date` DESC ";
        $users = Yii::app()->db->createCommand($userslist)->queryAll();
        $useritems = array();
        $userdata = array();
        $r = 0;


        foreach ($users as $key => $user) {
            $r++;

            $userdata = array('userid' => $user['userid'],
                'userid' => $user['userid'],
                'designation' => $user['designation'],
                'fullname' => $user['fullname'],
                'usertype' => $user['label'],
                'cr_date' => $user['created_date'],
                'cr_by' => $user['created_by'],
                'coment' => $user['comment'],
                'date' => $user['date'],
                'description' => $user['description'],
                'short_note' => $user['short_note'],
            );
            $useritems[] = $userdata;
        }

        // echo '<pre>';print_r($useritems);exit;



        if (isset($_GET['ajaxcall'])) {
            echo $retarray = $this->renderPartial("attentryreport", array('users' => $users,
        'start_date' => $sdate,
        'end_date' => $tdate,
        'results' => $useritems), true);
        } else {
            $this->render("attlog", array('users' => $users,
                'start_date' => $sdate,
                'end_date' => $tdate,
                'results' => $useritems));
        }
    }

    public function actionExport() {


        $tblpxpms = Yii::app()->db->tablePrefix;
        $this->layout = 'false';

        if (isset(Yii::app()->session['date_frm']) && Yii::app()->session['date_end']) {

            $start_date = Yii::app()->session['date_frm'];
            $end_date = Yii::app()->session['date_end'];
        } else {

            $start_date = date('Y-m-01');
            $end_date = date('Y-m-d');
        }

        $usertype = (isset($_GET['usertype']) && $_GET['usertype'] != "") ? $_GET['usertype'] : "";

        $grp_id = (isset($_GET['grp_id']) && $_GET['grp_id'] != "") ? $_GET['grp_id'] : "";

        $skill = (isset($_GET['skill']) && $_GET['skill'] != "") ? $_GET['skill'] : "";


        $designation = (isset($_GET['designation']) && $_GET['designation'] != "") ? $_GET['designation'] : "";





        $count_date = $this->getcountBetween2Dates($start_date, $end_date);

        if ($count_date > 31) {

            Yii::app()->user->setFlash('error', " Maximum 30 days can exported ");
            $this->redirect(array('index'));
        }


        $company = '';
        $where = " where 1=1 ";



        $join_date = "and  (ed.joining_date<= '$start_date' OR ed.joining_date <= '$end_date' "
                . "OR ed.joining_date IS NULL) AND (ed.resignation_date >= '$start_date' "
                . "OR ed.resignation_date >= '$end_date' OR ed.resignation_date IS NULL ) ";



        $typecon = '';
        if ($usertype != '') {
            $typecon = ' and ed.user_type = ' . $usertype;
        }

        $grpcon = '';
        if ($grp_id != '') {
            $grpcon = ' and ed.main_grp_id = ' . $grp_id;
        }

        $skill_filter = "";
        if ($skill != "") {
            $skill_filter = ' and ed.skill_id =' . $skill;
        }
        $designation_filter = "";
        if ($designation != "") {
            $designation_filter = ' and ed.designation =' . $designation;
        }







        $usersql = "SELECT cmp.name,de.label, ed.* FROM pms_users ed left join "
                . "pms_employee_default_data as de on de.default_data_id = ed.designation "
                . "left join pms_company as cmp on cmp.company_id = ed.company_id "
                . "left join pms_usersite as site on site.user_id = ed.userid "
                . $where . "    $typecon $grpcon $skill_filter $designation_filter    "
                . "  group by userid order by first_name ";



        $usermodel = Yii::app()->db->createCommand($usersql)->queryAll();


        $sql = "select * from " . $tblpxpms . "attendance left join " . $tblpxpms . "users on " . $tblpxpms . "users.userid = " . $tblpxpms . "attendance.user_id ";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        $attendance = $command->queryAll();


        $sql = "select * from " . $tblpxpms . "attendance inner join " . $tblpxpms . "legends on " . $tblpxpms . "attendance.att_entry=" . $tblpxpms . "legends.leg_id WHERE att_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        $entries = $command->queryAll();
        $data = $this->Getentries('', $start_date, $end_date);

        $newdata = array();
        $dataarr = array();


        foreach ($entries as $key => $value) {
            extract($value);

            $attendance = $this->getpresentdays($user_id, $start_date, $end_date);

            $newdata[$user_id] = array('pr' => $attendance['present'], 'ab' => $attendance['absent'], 'hl' => $attendance['holi_days'], 'sun' => $attendance['sun_days'], 'twopresent' => $attendance['twop']);

            $dataarr[$user_id . strtotime($att_date)] = array('legend' => $short_note, 'color_code' => $color_code);
        }
        $site_file = "";
        if (isset(Yii::app()->user->company_id)) {

            $sql = "SELECT `name` FROM `pms_company` WHERE company_id =" . Yii::app()->user->company_id;
            $user_sites = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($user_sites as $site) {
                $site_file = $site['name'];
            }
        }


        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4-L');
        $mPDF1->WriteHTML($stylesheet, 1);
        $body = $this->renderPartial('report', array(
            'model' => $attendance, 'usermodel' => $usermodel, 'start_date' => $start_date, 'end_date' => $end_date, 'entries' => $entries, 'dataarr' => $dataarr, 'newdata' => $newdata
                ), true);


        $mPDF1->WriteHTML($body);
        $mPDF1->Output('Att_' . $start_date . '_' . $end_date . '_' . $site_file . '.pdf', 'D');

        /* pdf end */
    }

    public function Getentries($userid = 0, $start, $end) {


        $tbl = Yii::app()->db->tablePrefix; // table prefixforpms
        $userentries = array();

        if ($userid != 0) {
            $where = " user_id=" . $userid . " AND ";
        } else {
            $where = " ";
        }

        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));


        $fromdate = $start;
        $todate = $end;

        $sql = "select * from " . $tbl . "attendance inner join " . $tbl . "legends on " . $tbl . "attendance.att_entry=" . $tbl . "legends.leg_id WHERE " . $where . " att_date BETWEEN '" . $start . "' AND '" . $end . "' ";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        $entries = $command->queryAll();

        $jsonar = array();
        $j = 0;
        $nis = 0;
        foreach ($entries as $entry) {
            $jsonar[$j]['cid'] = $entry['user_id'] . strtotime($entry['att_date']);
            $jsonar[$j]['legend'] = $entry['short_note'];
            $jsonar[$j]['color_code'] = $entry['color_code'];

            $userid = $entry['user_id'];

            $employees = Users::model()->find(array('condition' => "userid = '{$userid}'"));


            $doj = $employees['joining_date'];
            $dor = $employees['resignation_date'];

            if (!empty($doj) || !empty($dor)) {
                if ($doj >= $start && $doj <= $end) {
                    if ($dor >= $start && $dor <= $end) {
                        $nis1 = $this->getnis($userid, $start, $doj);
                        $nis2 = $this->getnis($userid, $dor, $end);
                        $nis = $nis1 + $nis2;
                        $start = $doj;
                        $end = $dor;
                    } else {
                        $nis = $this->getnis($userid, $start, $doj);
                        $start = $doj;
                    }
                } elseif ($dor >= $start && $dor <= $end) {
                    $nis = $this->getnis($userid, $dor, $end);
                    $end = $dor;
                } else {
                    $nis = 0;
                    $start = $fromdate;
                    $end = $todate;
                }
            }


            $attendance = $this->getpresentdays($userid, $start, $end);

            $jsonar[$j]['eid'] = 'user_' . $entry['user_id'];
            $jsonar[$j]['present_days'] = $attendance['present'];
            $jsonar[$j]['absent_days'] = $attendance['absent'];
            $jsonar[$j]['userid'] = $entry['user_id'];

            $j++;
        }

        return $jsonar;
    }

    public function actionAddattdirect() {

        $formdet = $_REQUEST;
        parse_str($formdet['data'], $data);

        $att_id = $data['leg_id'];
        $comment = $data['cmnt'];
        $created_by = $uby = Yii::app()->user->id;
        $created_date = $udate = date("Y-m-d H:i:s");

        if (!isset($data['day'])) {
            echo "Please choose atleast one day";
            exit;
        }
        if (!isset($data['employee'])) {
            echo "Please choose atleast one employee";
            exit;
        }


        $datas = array();

        $assigned_table = Attendance::model()->tableSchema->rawName;
        $connection = Yii::app()->db;


        foreach ($data['employee'] as $emp) {

            foreach ($data['day'] as $key => $date) {

                $connection->createCommand("delete from " . $assigned_table . " where att_date= '$date' and user_id = " . $emp)->execute();
                if ($att_id != 21) {

                    $modelatt = new Attendance;
                    $modelatt->user_id = $emp;
                    $modelatt->att_date = $date;
                    $modelatt->comments = $comment;
                    $modelatt->att_entry = $att_id;
                    $modelatt->created_by = $created_by;
                    $modelatt->created_date = $created_date;
                    $modelatt->modified_date = $udate;
                    $modelatt->type = 2;
                    $modelatt->save();


                    $datas[$key . $emp] = '(null,' . $emp . ',"' . $date . '","' . $comment . '",' . $att_id . ',' . $created_by . ',"' . $created_date . '","' . $udate . '",2)';
                }
            }
        }
        if (count($datas) > 0) {

            // $connection = Yii::app()->db;
            // $assigned_table = Attendance::model()->tableSchema->rawName;
            // $connection->createCommand('insert into ' . $assigned_table . " values " . implode(',', $datas))->execute();

            echo "1";
            exit;
        } else if ($att_id == 21) {
            echo "2";
            exit;
        } else {
            echo "Failed assign shift to selected employees";
            exit;
        }
    }

    public function actionapplyattendance() {

        if (isset(Yii::app()->user->attendancedata)) {
            unset(Yii::app()->user->attendancedata);
        }

        $formdet = $_REQUEST;
        parse_str($formdet['data'], $data);

        Yii::app()->user->setState('attendancedata', $data);
        echo 1;

        // echo '<pre>';print_r($data);exit;
    }

    public function actionapplyallattendance() {

        $users = Users::model()->findAll();
        $user_array = array();
        foreach ($users as $data) {
            $user_array[$data['userid']] = $data['first_name'] . ' ' . $data['last_name'];
        }
        $legends = Legends::model()->findAll();
        $leg_items = CHtml::listData($legends, 'leg_id', 'short_note');
        $this->render("attendancetable", array('usermodel' => $user_array, 'leg_items' => $leg_items));
    }

    public function actiontestattendance() {

        if (isset(Yii::app()->user->addattendancedata)) {
            unset(Yii::app()->user->addattendancedata);
        }
        $formdet = $_REQUEST;
        parse_str($formdet['data'], $data);
        $model = $data['leg_id'];
        $start_date = $_REQUEST['start_date'];
        $end_date = $_REQUEST['end_date'];
        $comment = $_REQUEST['coment'];
        Yii::app()->user->setState('addattendancedata', $model);
        $employeemodel = Users::model()->findAll();
        $emparr = array();
        foreach ($employeemodel as $data) {
            $emparr[$data['userid']] = $data['first_name'] . ' ' . $data['last_name'];
        }
        $legends = Legends::model()->findAll();
        $legarr = array();
        foreach ($legends as $data) {

            $legarr[$data['leg_id']]['note'] = $data['short_note'];
            $legarr[$data['leg_id']]['color'] = $data['color_code'];
        }
        $data = $this->renderPartial("viewattendancetable", array('model' => $model, 'start_date' => $start_date, 'end_date' => $end_date, 'usermodel' => $emparr, 'legends' => $legarr, 'comment' => $comment));
        echo $data;
        exit;
    }

    public function actionupdateattendance() {

        if (isset(Yii::app()->user->addattendancedata)) {

            $created_by = Yii::app()->user->id;
            $created_date = date('Y-m-d H:i:s');
            $modified_date = date('Y-m-d H:i:s');
            $type = 2;

            $att_arr = array();
            $request_array = array();
            $comments = ( isset($_GET['comt']) && $_GET['comt'] != '' ) ? $_GET['comt'] : 'Manual entry by group';
            $alldata = Yii::app()->user->addattendancedata;
            foreach ($alldata as $empid => $dat) {
                foreach ($dat as $date => $entry) {
                    if ($entry != '') {
                        $att_arr[] = "(NULL,'{$empid}','{$date}','{$comments}','9',$created_by,'" . $created_date . "','" . $modified_date . "'," . $type . ")";
                        $request_array[] = "(NULL,'{$empid}','{$date}','{$comments}','{$entry}',$created_by,'" . $created_date . "'," . $type . ")";
                        $log_arry[] = "(NULL,'{$empid}','{$date}','{$comments}','{$entry}',$created_by,'" . $created_date . "','" . $modified_date . "'," . $type . ")";
                    }
                }
            }

            if (!empty($att_arr)) {

                $att_arr = implode(',', $att_arr);
                $request_array = implode(',', $request_array);
                $log_arry = implode(',', $log_arry);


                Yii::app()->db->createCommand("Replace INTO `pms_attendance`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  " . $att_arr . " ")->query();
                Yii::app()->db->createCommand("Replace INTO `pms_attendance_requests`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `type`) VALUES  " . $request_array . " ")->query();
                // Yii::app()->db->createCommand("INSERT INTO `pms_attendance_request_log`(att_id, `user_id`, `att_date`, `comments`, `att_entry`, `created_by`, `created_date`, `modified_date`, `type`) VALUES  ".$log_arry." ")->query();


                if (isset(Yii::app()->user->addattendancedata)) {
                    unset(Yii::app()->user->addattendancedata);
                }
            }

            Yii::app()->user->setFlash('success', "Attendance added!");
            $this->redirect(array('index'));
        }
    }

    public function getTodate($userid, $date) {


        if (isset(Yii::app()->user->company_id)) {

            $cddate = new DateTime($date);
            $pdate = $cddate->format('Y-m-d');
            $company_id = Yii::app()->user->company_id;

            $sql = "SELECT user_id FROM `pms_usersite` WHERE site_id = " . $company_id . " AND user_id=" . $userid . " AND(
            ('" . $pdate . "' BETWEEN date_from AND date_to) OR ('" . $pdate . "'>=date_from and  date_to IS NULL)  )";

            $allids = Yii::app()->db->createCommand($sql)->queryAll();
            if ($allids != "") {
                if (count($allids) != 0) {
                    return 1;
                } else
                    return 0;
            }
        } else
            return 1;
    }

    function calculateFromTo($allocationarray, $userid, $from_date, $to_date) {
        if (isset(Yii::app()->user->company_id)) {

            $company = Yii::app()->user->company_id;
        }

        foreach ($allocationarray as $allocation) {
            if ($allocation['site_id'] == $company) {
                $employees = $allocation['employee'];
            }
        }


        foreach ($employees as $employee) {

            if ($employee['userid'] == $userid) {
                $date = array();
                foreach ($employee['allocation'] as $allocation) {

                    if ($allocation['from_date'] <= $from_date && $allocation['to_date'] >= $to_date && $allocation['to_date'] != "") {
                        $date[] = array("from_date" => $from_date,
                            "to_date" => $to_date,
                            "user" => $userid,
                            "allocation_id" => $allocation['allocation_id']
                        );
                    } elseif ($allocation['from_date'] <= $from_date && $allocation['to_date'] <= $to_date && $allocation['to_date'] != "") {
                        $date[] = array("from_date" => $from_date,
                            "to_date" => $allocation['to_date']
                            , "user" => $userid,
                            "test" => "3",
                            "allocation_id" => $allocation['allocation_id']
                        );
                    } elseif ($allocation['from_date'] >= $from_date && $allocation['to_date'] <= $to_date && $allocation['to_date'] != "") {
                        $date[] = array("from_date" => $allocation['from_date'],
                            "to_date" => $allocation['to_date']
                            , "user" => $userid,
                            "test" => "4",
                            "allocation_id" => $allocation['allocation_id']
                        );
                    } elseif ($allocation['from_date'] <= $from_date && $allocation['to_date'] <= $to_date && $allocation['to_date'] != "") {
                        $date[] = array("from_date" => $from_date,
                            "to_date" => $allocation['to_date']
                            , "user" => $userid,
                            "test" => "5",
                            "allocation_id" => $allocation['allocation_id']
                        );
                    } elseif ($allocation['from_date'] >= $from_date && $allocation['to_date'] >= $to_date && $allocation['to_date'] != "") {
                        $date[] = array("from_date" => $allocation['from_date'],
                            "to_date" => $to_date
                            , "user" => $userid,
                            "test" => "6",
                            "allocation_id" => $allocation['allocation_id']
                        );
                    } elseif ($allocation['from_date'] >= $from_date && $allocation['to_date'] == "") {
                        $date[] = array("from_date" => $allocation['from_date'],
                            "to_date" => $to_date
                            , "user" => $userid,
                            "test" => "1",
                            "allocation_id" => $allocation['allocation_id']
                        );
                    } elseif ($allocation['from_date'] <= $from_date && $allocation['to_date'] == "") {
                        $date[] = array("from_date" => $from_date,
                            "to_date" => $to_date
                            , "user" => $userid,
                            "test" => "2",
                            "allocation_id" => $allocation['allocation_id']
                        );
                    }






                    // elseif($allocation['from_date']<=$from_date && $allocation['to_date']=="")
                    // {
                    //     $out_put_array[]=$employee['userid'];
                    // }
                }
                //  $date=array_unique($date[""]); 
                //   print_r($date);

                return $date;
            }
        }
    }

    function Changenew($company_id = 0) {


        if (isset(Yii::app()->user->company_id)) {
            $company_id = Yii::app()->user->company_id;
        } else {

            $company_id = "";
        }



        if ($company_id == "") {
            unset(Yii::app()->user->company_id);
            unset(Yii::app()->user->project_site);
        }

        //echo $company_id;exit;
        if (isset(Yii::app()->user->filter_from_date)) {
            $from_date = Yii::app()->user->filter_from_date;
        } else {
            $from_date = date('Y-m-01');
        }

        if (isset(Yii::app()->user->filter_to_date)) {
            $to_date = Yii::app()->user->filter_to_date;
        } else {
            $to_date = date('Y-m-t');
        }

        $allocation_array = $this->AllocationArray();

        Yii::app()->user->setState('allocation_array_session', $allocation_array);



        $cdate = date('Y-m-d');
        if (isset($_GET['month']) and $company_id != 0) {

            $user_id = $this->calculateUserAllocation($allocation_array, $company_id, $from_date, $to_date);

            $site_userids = implode(',', $user_id);
            Yii::app()->user->setState('site_userids', $site_userids);
        }

        return 1;
    }

    public function AllocationArray() {

        $allocation_array = [];
        $sql = "SELECT `company_id` FROM `pms_company`";
        $user_sites = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($user_sites as $site) {
            $allocation_array[] = array(
                'site_id' => $site['company_id'],
                'employee' =>
                $this->getEmployee($site['company_id']),
            );
        }
        // print_r($allocation_array); exit;
        return $allocation_array;
    }

    function getEmployee($company_id) {
        $sql = "SELECT `userid` FROM `pms_users`";
        $employees = Yii::app()->db->createCommand($sql)->queryAll();
        $site_array = [];
        foreach ($employees as $emp) {
            $site_array[] = array(
                'userid' => $emp['userid'],
                'allocation' =>
                $this->getAllocationDate($emp['userid'], $company_id),
            );
        }
        return $site_array;
    }

    function getAllocationDate($user, $site) {
        $sql = "SELECT * FROM `pms_usersite` where `user_id`=" . $user . " AND `site_id`=" . $site . "";
        $allocations = Yii::app()->db->createCommand($sql)->queryAll();
        $allocation_array_date = [];
        foreach ($allocations as $allocation) {
            $allocation_array_date[] = array(
                "from_date" => $allocation['date_from'],
                "to_date" => $allocation['date_to'],
                "allocation_id" => $allocation['id'],
            );
        }
        return $allocation_array_date;
    }

    function calculateUserAllocation($allocationarray, $company, $from_date, $to_date) {
        $out_put_array = [];
        foreach ($allocationarray as $allocation) {
            if ($allocation['site_id'] == $company) {

                $employees = $allocation['employee'];
            }
        }
        foreach ($employees as $employee) {
            foreach ($employee['allocation'] as $allocation) {
                if ($allocation['from_date'] <= $from_date && $allocation['to_date'] >= $from_date) {
                    $out_put_array[] = $employee['userid'];
                } elseif ($allocation['from_date'] >= $from_date && $allocation['from_date'] <= $to_date) {
                    $out_put_array[] = $employee['userid'];
                } elseif ($allocation['from_date'] <= $from_date && $allocation['to_date'] == "") {
                    $out_put_array[] = $employee['userid'];
                }


                //   echo $employee['userid'];
            }
        }
        $out_put_array = array_unique($out_put_array);
        return $out_put_array;
    }

    function getAllocationSite($start, $end, $user_id) {

        if (isset(Yii::app()->user->company_id)) {
            if (isset(Yii::app()->user->allocation_array_session)) {
                $dates = $this->calculateFromTo(Yii::app()->user->allocation_array_session, $user_id, $start, $end);
                foreach ($dates as $date) {
                    $return_id[] = $date['allocation_id'];
                }
                return $return_id;
            }
        }
    }

    function getAllocationSiteWithoutSite($from_date, $to_date, $user_id) {

        $users = Yii::app()->db->createCommand()
                ->select(['date_from', 'date_to', 'id'])
                ->where('user_id=:user_id', [':user_id' => $user_id])
                ->from('pms_usersite')
                ->queryAll();

        $out_put_array = [];
        foreach ($users as $user) {
            if ($user['date_from'] <= $from_date && $user['date_to'] >= $from_date) {
                $out_put_array[] = $user['id'];
            } elseif ($user['date_from'] >= $from_date && $user['date_from'] <= $to_date) {
                $out_put_array[] = $user['id'];
            } elseif ($user['date_from'] <= $from_date && $user['date_to'] == "") {
                $out_put_array[] = $user['id'];
            }
        }

        return $out_put_array;
    }

    public function actionMyattendance() {

        $this->render('my_attendance');
    }

    public function actiongetMyattendance() {
        $get_date = $_POST['day'];
        $month = date("m", strtotime($get_date));
        $year = date("Y", strtotime($get_date));
        $get_attendance = "SELECT DAY(att_date) as date,att_entry,short_note FROM pms_attendance
       LEFT JOIN  pms_legends ON pms_legends.leg_id=pms_attendance.att_entry
        WHERE MONTH(att_date) = " . $month . " AND YEAR(att_date) = " . $year . " AND user_id=" . Yii::app()->user->id;
        $attendance = Yii::app()->db->createCommand($get_attendance)->queryAll();
        echo json_encode($attendance);
        exit;
    }

}
