<?php

/**
 * This is the model class for table "{{attendance_log}}".
 *
 * The followings are the available columns in table '{{attendance_log}}':
 * @property integer $log_id
 * @property integer $refer_id
 * @property integer $user_id
 * @property string $att_date
 * @property string $comments
 * @property integer $att_entry
 * @property integer $created_by
 * @property string $created_date
 * @property string $modified_date
 *
 * The followings are the available model relations:
 * @property Attendance $refer
 */
class AttendanceLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{attendance_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('refer_id, user_id, comments, att_entry, created_by, created_date, modified_date', 'required'),
			array('refer_id, user_id, att_entry, created_by', 'numerical', 'integerOnly'=>true),
			array('att_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('log_id, refer_id, user_id, att_date, comments, att_entry, created_by, created_date, modified_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'refer' => array(self::BELONGS_TO, 'Attendance1', 'refer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_id' => 'Log',
			'refer_id' => 'Refer',
			'user_id' => 'User',
			'att_date' => 'Att Date',
			'comments' => 'Comments',
			'att_entry' => 'Att Entry',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'modified_date' => 'Modified Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('log_id',$this->log_id);
		$criteria->compare('refer_id',$this->refer_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('att_date',$this->att_date,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('att_entry',$this->att_entry);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modified_date',$this->modified_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AttendanceLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
