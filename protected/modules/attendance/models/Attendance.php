<?php

/**
 * This is the model class for table "{{attendance}}".
 *
 * The followings are the available columns in table '{{attendance}}':
 * @property integer $att_id
 * @property integer $user_id
 * @property string $att_date
 * @property string $comments
 * @property integer $att_entry
 * @property integer $created_by
 * @property string $created_date
 * @property string $modified_date
 *
 * The followings are the available model relations:
 * @property AttendanceLog[] $attendanceLogs
 */
class Attendance extends CActiveRecord
{
    public $from;
    public $to;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{attendance}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, att_entry, created_by, created_date, modified_date', 'required'),
			array('user_id, att_entry, created_by', 'numerical', 'integerOnly'=>true),
			array('att_date, type', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('att_id, user_id, att_date, comments, att_entry, created_by, created_date, modified_date, from, to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'attendanceLogs' => array(self::HAS_MANY, 'AttendanceLog', 'refer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'att_id' => 'Att',
			'user_id' => 'User',
			'date' => 'Date',
			'comments' => 'Comments',
			'att_entry' => 'Leave Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'modified_date' => 'Modified Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('att_id',$this->att_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('att_date',$this->att_date,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('att_entry',$this->att_entry);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modified_date',$this->modified_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Attendance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	  
        
        public function calculate($userid,$start,$end){
            $tbl = Yii::app()->db->tablePrefix;
            $payrolldata = Yii::app()->db->createCommand(" SELECT user_id, SUM(if(att_entry = 1, 1, 0)) AS present, SUM(if(att_entry = 2, 1, 0)) AS casual_leave, SUM(if(att_entry = 3, 1, 0)) AS half_casual_leave, SUM(if(att_entry = 4, 1, 0)) AS approved_leave, SUM(if(att_entry = 5, 1, 0)) AS approved_half_leave, SUM(if(att_entry = 6, 1, 0)) AS paid_leave, SUM(if(att_entry = 7, 1, 0)) AS res_holiday, SUM(if(att_entry = 8, 1, 0)) AS unapproved_leave, SUM(if(att_entry = 9, 1, 0)) AS unapproved_half_leave, SUM(if(att_entry = 10, 1, 0)) AS optional_holiday, SUM(if(att_entry = 11, 1, 0)) AS holiday, SUM(if(att_entry = 12, 1, 0)) AS working_weekend, SUM(if(att_entry = 13, 1, 0)) AS paid_half_leave, SUM(if(att_entry = 14, 1, 0)) AS compensatory_off, SUM(if(att_entry = 15, 1, 0)) AS compensatory_off_half   FROM {$tbl}attendance WHERE att_date BETWEEN '$start' AND '$end' AND user_id = $userid GROUP BY user_id ")->queryRow();
            
            return $payrolldata;
            
            
        }

}
