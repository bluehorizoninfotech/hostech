<?php

/**
 * This is the model class for table "{{time_attendance}}".
 *
 * The followings are the available columns in table '{{time_attendance}}':
 * @property integer $att_id
 * @property integer $user_id
 * @property string $att_date
 * @property string $comments
 * @property double $att_time
 * @property integer $created_by
 * @property string $created_date
 * @property string $modified_date
 * @property integer $type
 */
class TimeAttendance extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{time_attendance}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, comments, att_time, created_by, created_date', 'required'),
			array('user_id, created_by, type', 'numerical', 'integerOnly'=>true),
			array('att_time', 'numerical'),
			array('att_date, modified_date, att_entry', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('att_id, user_id, att_date, comments, att_time, created_by, created_date, modified_date, type, att_entry', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'att_id' => 'Att',
			'user_id' => 'User',
			'att_date' => 'Att Date',
			'comments' => 'Comments',
			'att_time' => 'Att Time',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'modified_date' => 'Modified Date',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('att_id',$this->att_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('att_date',$this->att_date,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('att_time',$this->att_time);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modified_date',$this->modified_date,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('att_entry',$this->att_entry);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TimeAttendance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
