<?php

/**
 * This is the model class for table "{{ignore_punches}}".
 *
 * The followings are the available columns in table '{{ignore_punches}}':
 * @property integer $ig_id
 * @property integer $empid
 * @property integer $ignore_entry
 * @property integer $acces_logid
 * @property string $date
 */
class IgnorePunches extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return IgnorePunches the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ignore_punches}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('empid, date', 'required'),
			//array('empid, acces_logid ', 'numerical', 'integerOnly'=>true),
			array('date,device_id,userid,created_by,created_date,status,decision_by,decision_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ig_id, empid, acces_logid, date,created_by,created_date,status,decision_by,decision_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'usrId' => array(self::BELONGS_TO, 'Employee', 'empid'),
			'createdBy' => array(self::BELONGS_TO, 'Employee', 'created_by'),
			'decisionBy' => array(self::BELONGS_TO, 'Employee', 'decision_by'),
			'EmployeeName' => array(self::BELONGS_TO, 'Employee', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ig_id' => 'Id',
			'empid' => 'Employee',
			'acces_logid' => 'Acces Logid',
			'date' => 'Punch Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ig_id',$this->ig_id);
		$criteria->compare('empid',$this->empid);
	    $criteria->compare('acces_logid',$this->acces_logid);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('decision_by', $this->decision_by, true);
		$criteria->compare('decision_date', $this->decision_date, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function shift_status($sts)
	{

		if ($sts == 0) {
			$status = "Pending";
		} else if ($sts == 1) {
			$status = "Approved";
		} else {
			$status = "Rejected";
		}

		return $status;

	}
}