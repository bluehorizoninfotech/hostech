<?php

/**
 * This is the model class for table "{{manual_entry}}".
 *
 * The followings are the available columns in table '{{manual_entry}}':
 * @property integer $entry_id
 * @property integer $emp_id
 * @property string $date
 * @property integer $created_by
 * @property string $created_date
 */
class ManualEntry extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ManualEntry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{manual_entry}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		
		return array(
			array('emp_id, created_by', 'numerical', 'integerOnly'=>true),
			array('date, created_date,status,decision_by,decision_date,shift_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('entry_id, emp_id, date, created_by, status, decision_by, decision_date, created_date, shift_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'usrId' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
			'createdBy' => array(self::BELONGS_TO, 'Employee', 'created_by'),
			'decisionBy' => array(self::BELONGS_TO, 'Employee', 'decision_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'entry_id' => 'Entry',
			'emp_id' => 'Employee',
			'date' => 'Punch Time',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('entry_id',$this->entry_id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('decision_by', $this->decision_by, true);
		$criteria->compare('decision_date',$this->decision_date,true);

		$criteria->order = 'entry_id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function shift_status($sts)
	{

		if ($sts == 0) {
			$status = "Pending";
		} else if ($sts == 1) {
			$status = "Approved";
		} else {
			$status = "Rejected";
		}

		return $status;

	}


}
