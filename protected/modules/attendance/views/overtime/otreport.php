<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php

$sites = CHtml::listData(Clientsite::model()->findAll(), 'id', 'site_name');

if (!isset($_GET['ajaxcall'])) {
    Yii::app()->clientScript->registerScript('myjquery', '

        $("#totpre").html($("#total_pre").html());
        $("#totot").html($("#total_ot").html());

        $(\'#dprtment\').val(0);
        $(\'#btn_submit_for_report\').on(\'click\', function(){
        var startdate = $(\'#startdate\').val();
        var tilldate = $(\'#tilldate\').val();
        var deptment = $(\'#dprtment\').val();
        var site = $(\'#site\').val();
        $("#loadingdiv").html("Loading...");
        $.ajax({
        url:"' . Yii::app()->createUrl('attendance/accesslog/otreport') . '",
         data:{sdate:startdate, tdate:tilldate,ajaxcall:1,deptment:deptment,site:site}
        }).done(function(msg){
            //alert(msg);
            $("#result").html(msg);
            $("#totpre").html($("#total_pre").html());
            $("#totot").html($("#total_ot").html());
        }).alwasys(function(){
            $("#loadingdiv").html("");
        });
        });');
    ?>
    <div class="main_content">
        <div class="row">
            <div class="col-md-4">
                <h1 class="heading">OT Report</h1>
            </div>
            <div class="col-md-8 attsearch text-right">
                <?php echo CHtml::textField('startdate', date('01-m-Y'), array("id" => "startdate", 'size' => 10, 'Placeholder' => 'Start date','class'=>'form-control')); ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'startdate',
                    'ifFormat' => '%d-%m-%Y',
                ));

                $dateend = date('d-m-Y', strtotime(' -1 day'));
                echo CHtml::textField('tilldate', $dateend, array("id" => "tilldate", 'size' => 10, 'Placeholder' => 'Till date','class'=>'form-control'));
                ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'tilldate',
                    'ifFormat' => '%d-%m-%Y',
                ));


                $company ='';
                if( isset(Yii::app()->user->company_id) ){
                    $company = " where company_id = ".Yii::app()->user->company_id;
                }

                $list =  Yii::app()->db->createCommand("SELECT concat_ws(' ',firstname,lastname) as fullname,`userid` FROM `pms_users` ".$company." order by fullname")->queryAll();

                 echo CHtml::dropDownList('emp', (isset(Yii::app()->user->empid) ? Yii::app()->user->empid : 0), CHtml::listData($list, "emp_id", "fullname"), array('empty' => 'Select a user', 'id' => 'empid',
                        'ajax' => array(
                            'type' => 'POST',
                            'url' => CController::createUrl('accesslog/otreport' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')),
                            'data' => array('empid' => 'js:this.value', 'ajax' => 1),
                            'success' => 'function(data) {
                             if(data==1){
                                 /*  window.location="' . CController::createUrl('accesslog/otreport' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')) . '";*/
                                }
                            }'
                        )
                    ));

                    $deplist =  Yii::app()->db->createCommand("SELECT * FROM `pms_employee_default_data` WHERE `data_type`='department'")->queryAll();

                     echo CHtml::dropDownList('dep', (isset(Yii::app()->user->deptment) ? Yii::app()->user->deptment : 0), CHtml::listData($deplist, "default_data_id", "label"), array('empty' => 'Select Department', 'id' => 'deptment','style'=>'width: 145px;',
                        'ajax' => array(
                            'type' => 'POST',
                            'url' => CController::createUrl('accesslog/otreport' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')),
                            'data' => array('deptment' => 'js:this.value', 'ajax' => 1),
                            'success' => 'function(data) {
                             if(data==1){

                                }
                            }'
                        )
                    ));
                ?>

                <?php echo CHtml::button('Submit', array('id' => 'btn_submit_for_report','class'=>'btn btn-sm btn-primary','style'=>'margin-left:10px;')); ?>
        `   </div>
        <?php
            }
        ?>
    </div>
    <?php
    if (!isset($_GET['ajaxcall'])) {
        ?>
        <div class="">
        <div class="row">
            <div class="col-md-12" id='result' style="background-color: #fff;">
                <?php
            }
            ?>
                <div class="row">
                    <div class="col-md-3" id='duration'><p><?php echo date("F d Y", strtotime($start_date)) . ' to ' . date("F d Y", strtotime($end_date)) ?></p></div>
                   <div class="col-md-3" id="loadingdiv" style="color:red;">&nbsp;</div>
                   <div class="col-md-6 text-right"><p>Total Records: <?php echo count($results) ?></p></div>
                </div>



        <div class="form">
            <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'user-form',
                    'enableAjaxValidation'=>false,
                    'enableClientValidation'=>false,
                   // 'focus'=>array($model,'firstName'),
                )); ?>




             <div id="parent">


            <table class="table table-bordered" id="ottable">
                <thead>
                    <tr>


                        <th rowspan="2">S.No.</th>
                        <th rowspan="2">Employee Id</th>
                        <th rowspan="2">Employee Name</th>
                        <th rowspan="2">User Type</th>
                        <th rowspan="2">Designation</th>
                        <th rowspan="2">Department</th>
                        <th>Date</th>
                        <th>Shift</th>
                        <th>OT</th>
                        <th>Manual OT</th>

                    </tr>

                </thead>
                <tbody>
                    <?php

                    $stotpr = array();   //site based total present
                    $sitetotot = array();  //site based total OT

                    $m = 1;
                    if (!empty($results)) {

                        $grandpresent = 0;
                        $grandot=0;
                        $ots=array();
                        $des=NULL;
                        $total_late= 0;
                        foreach ($results as $puserid => $res) {

                            if(!empty($res['designation'])){
                                $des = Yii::app()->db->createCommand("SELECT * FROM `pms_employee_default_data` WHERE `default_data_id` =".$res['designation'])->queryRow();
                                $res['designation'] = $des['label'];

                              }

                            $approved_by = '--';

                            if($res['approved_by']!=''){
                                $des = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE `emp` =".$res['approved_by'])->queryRow();
                                $approved_by = $des['firstname'].' '.$des['lastname'];

                            }


                            ?>
                            <tr>
                                <td><?php echo $m++; ?></td>
                                <td><?=  $res['emp_id']; ?></td>
                                <td><?=  $res['fullname']; ?></td>
                                <td><?=  $res['usertype']; ?></td>
                                <td><?=  $res['designation']; ?></td>
                                <td><?=  $res['department']; ?></td>
                                <td><?=  date('d-m-Y',strtotime($res['date'])); ?></td>
                                <td><?= $res['shift'] ?></td>
                                <td><?php
                                //$totalot = $res['manual_ot'] + $res['outsideot'];

                                echo gmdate('H:i:s', $res['ot_hrs']);

                                ?></td>
                                <td><?php echo ($res['manual_ot'] != '') ? gmdate('H:i', $res['manual_ot']).' [ '. $approved_by.' ]' : 0; ?> </td>

                            </tr>
                            <?php  }  ?>

                        <?php  }
                    if (count($results) == 0) {
                        ?>
                        <tr>
                            <th colspan="13">No users found.</th>
                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
            </div>

            <?php $this->endWidget(); ?>
        </div><!-- form -->


            <!-- <div class="col-md-12"><p>Total Records: <?php echo count($results) ?></p></div> -->
            <?php
            if (!isset($_GET['ajaxcall'])) {
                ?>
            </div>
        </div>
        </div>
    </div>
    <?php
}
?>







<style type="text/css">
    .attsearch .form-control {
    width: 91px;
    margin-left: 10px;
    display: inline-block;
}
    .form-control {
    height: 24px !important;
    padding: 0px 5px !important;
    line-height: 1 !important;
    font-size: 12px !important;
    max-width: 100%;
}
#parent{max-height: 500px;border: 1px solid #ddd;}
#parent thead th {
  text-align: center;
  background-color: #6e6f72 !important;
  color: #fafafa;
}
.outside{
   color:#d41818;
}
</style>
<script>
    $(document).ready(function() {
        $("#ottable").tableHeadFixer({ 'head' : true});
    });
</script>
