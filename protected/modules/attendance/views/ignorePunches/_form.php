<?php
/* @var $this IgnorePunchesController */
/* @var $model IgnorePunches */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ignore-punches-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'empid'); ?>
		<?php echo $form->textField($model,'empid'); ?>
		<?php echo $form->error($model,'empid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ignore_entry'); ?>
		<?php echo $form->textField($model,'ignore_entry'); ?>
		<?php echo $form->error($model,'ignore_entry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acces_logid'); ?>
		<?php echo $form->textField($model,'acces_logid'); ?>
		<?php echo $form->error($model,'acces_logid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->