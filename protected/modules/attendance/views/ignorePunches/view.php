<?php
/* @var $this IgnorePunchesController */
/* @var $model IgnorePunches */

$this->breadcrumbs=array(
	'Ignore Punches'=>array('index'),
	$model->ig_id,
);

$this->menu=array(
	// array('label'=>'List IgnorePunches', 'url'=>array('index')),
	// array('label'=>'Create IgnorePunches', 'url'=>array('create')),
	// array('label'=>'Update IgnorePunches', 'url'=>array('update', 'id'=>$model->ig_id)),
	// array('label'=>'Delete IgnorePunches', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ig_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage IgnorePunches', 'url'=> array('/manualEntry/index') ),
	
);
?>



<div class="view-ignorepunches-sec">
<div id='actionmsg'></div> <br/>

	<div class="text-center">
		<?php echo CHtml::link('View Punching Details', array('/attendance/IgnorePunches/ManageShiftPunches', 'id' => $model->empid, 'date' => date('Y-m-d', strtotime($model->date))), array('target' => '_blank')); ?>
		<?php if ($model->status == 0) { ?>
		<textarea name="reason" class="reason margin-0 height-37 width-192"></textarea> 
		<input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approve' /> 
		<input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Reject' /> 

	<?php 
} elseif($model->status == 1){ ?>
   <textarea name="reason" class="reason margin-0 height-37 width-192"></textarea> 
<input type="button" class='shift_action btn btn-danger' name='reject' id="shi_reject" value='Reject' /> 

<?php } else{ ?>
	<textarea name="reason" class="reason margin-0 height-37 width-192"></textarea> 
	<input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success' value='Approve' /> 
		
<?php } ?>
</div>

<h1>IgnorePunches Details</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ig_id',
		array(
			'name' => 'EmployeeName',
			'value' => isset($model->EmployeeName->first_name) ? $model->EmployeeName->first_name . " " . $model->EmployeeName->lastname : null,

		),
		'date',
		'comment',
		array(
			'name' => 'status',
			'value' => $model->shift_status($model->status),
			'filter' => array(0 => "Pending", 1 => "Approved", 2 => "Rejected"),

		),
		array(
			'name' => 'decision_by',
			'value' => isset($model->decisionBy->first_name) ? $model->decisionBy->first_name . " " . $model->decisionBy->lastname : null,
		),
		'decision_date',
		array(
			'name' => 'created_by',
			'value' => isset($model->createdBy->first_name) ? $model->createdBy->first_name . " " . $model->createdBy->lastname : null,

		),
		'created_date',

		array(
			'name' => 'reason',
			'value' => isset($model->reason) ? $model->reason : null,

		),
	),
)); ?>

<!-- include -->
<?php
Yii::app()->clientScript->registerScript("", "");
?>
<!-- end -->
</div>
<script type='text/javascript'>

$(document).ready(function(){

	$(".reason").keyup(function() {
        var val = $(this).val()
        $(this).val(val.toUpperCase())
    })

    $('.shift_action').click(function(){
        var req = $(this).val();
        var id=<?php echo $model->ig_id ?>;
		var reason=$('.reason').val();
        if(!confirm('Are you sure you want to '+$(this).val()+' this Request?')===false){

			$('#load').show();
            $.ajax({
                method: "POST",
                dataType:"json",
                url:'<?php echo Yii::app()->createUrl('/attendance/IgnorePunches/ShiftAction') ?>',
                data:{id:id,req:req,type:type,reason:reason}
            }).done(function(ret){
                
                $('#load').hide();
                if(ret.msg!=''){

                    $('#actionmsg').addClass('successaction');
                    $('#actionmsg').html(ret.msg);
                    $('.shift_action').hide();
                }else if(ret.error!=''){
                    
                    $('#actionmsg').addClass('erroraction');
                    $('#actionmsg').html(ret.error);
                   
                }
                  
               
            });
        }
    });
});
</script>



