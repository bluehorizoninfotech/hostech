
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.0-rc.1/jquery-ui.js"
  integrity="sha256-IY2gCpIs4xnQTJzCIPlL3uUgSOwVQYD9M8t208V+7KA="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<style>

    .icon-arrow-right, .icon-arrow-left {cursor: pointer;}

</style>


<div class="shiftpunches-sec">
<h1>Manage Punches</h1>


<?php
Yii::app()->clientScript->registerScript('myjquery', "
	    $(document).ready(function () {
	        $('#date').datepicker({autoclose: true,dateFormat: 'yy-mm-dd'});


	    });


	");
?>


<!--<div class="form" style="float:left;margin-right:20px;border-right:2px solid #c2c2c2;padding-right:20px;width:300px;margin-top: 31px;">-->

<div class="form">



<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'ignore-punches-form',
    'method' => 'GET',
        // 'enableAjaxValidation' => true,
        //   'enableClientValidation' => true,
        //   'clientOptions' => array(
        //       'validateOnSubmit' => true,
        //       'validateOnChange' => true,
        //       'validateOnType' => false,),
        ));
?>




    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-md-2">
            <?php echo $form->labelEx($model, 'empid'); ?>
            <select id="empid" name="IgnorePunches[empid]" class="form-control empval height-28">
                <option value="0">--Select --</option>
                <?php

         

                $users = Users::model()->findAll(array('order' => 'first_name'));

                // if (isset(Yii::app()->user->company_id) && Yii::app()->user->company_id!='') {
                //     $users = Employee::model()->findAll(array('condition' => ' status=28 and company_id = ' . Yii::app()->user->company_id,
                //         'order' => 'first_name'));
                // } else {
                //
                //     $users = Employee::model()->findAll(array('order' => 'first_name'));
                // }

                foreach ($users as $data) {
                    ?>
                    <option value="<?= $data['userid'] ?>" <?php if (!empty($userid)) {
                    if ($data['userid'] == $userid) { ?> selected= selected <?php }
        } ?>><?= $data['first_name'] . ' ' . $data['last_name'] ?>
                    </option>

                <?php } ?>
            </select>
            <?php // echo $form->error($model,'empid'); ?>
            <div class="errorMessage errempid display-none">Please select employee</div>
        </div>

        <div class="col-md-2">
            <label> Date</label>
            <?php
            if (!empty($date)) {
                $model->date = date('Y-m-d', strtotime($date));
            }
            echo $form->textField($model, 'date', array('id' => 'date', 'placeholder' => 'Date', 'class' => "form-control height-28", 'autocomplete' => "off"));
            ?>
            <?php //echo $form->error($model,'date'); ?>
            <div class="errorMessage errdate display-none">Please add date</div>
        </div>
        <div class="col-md-2 buttons">
            <label>&nbsp;</label>
            <?php echo CHtml::Button($model->isNewRecord ? 'Submit' : 'Submit', array('class' => 'btn btn-primary btn-sm margin-0 sub')); ?>
            <span class="btn btn-default btn-sm margin-bottom-5" onClick="document.location.href = '<?php echo $this->createUrl('/attendance/IgnorePunches/ManageShiftPunches') ?>'" >Cancel</span>

        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<!--</div>-->

<!--<div style="float:left;width:75%; margin-top: 17px;">-->






<div class="clearfix"></div>

<?php
if (!empty($logmodel)) {

    $k = 1;
    $msg = '';
    $color = "";
    $newarray = array();
// echo '<pre>';
// print_r($logmodel);
// exit;
    foreach ($logmodel as $log1) {
        // $employee_data = Yii::app()->db->createCommand("SELECT * FROM `pms_employee` WHERE employee_code = " . $log1['userid'])->queryRow();


//   if(empty($employee_data)){
        $employee_data = Yii::app()->db->createCommand("SELECT * FROM `pms_users` WHERE userid = " . $log1['userid'])->queryRow();
    //   }

        $shift_name = '';
        $shift_id = '';
        $shift = Yii::app()->db->createCommand("SELECT asgn.* , shift_name, shift_starts, shift_ends,grace_period_before,
            grace_period_after FROM `pms_shift_assign` asgn left join pms_employee_shift as sh on sh . shift_id = asgn . shift_id
            WHERE `user_id` = " . $employee_data['userid'] . " and `att_date` = '" . date('Y-m-d', strtotime($log1['log_time'])) . "' ")->queryRow();

        $ssql = "SELECT * FROM `pms_shift_entry` WHERE date='" . $log1['log_time'] . "' and emp_id= " . $employee_data['userid'];
        $shift_entry = Yii::app()->db->createCommand($ssql)->queryRow();
        $manual_entry = Yii::app()->db->createCommand("SELECT * FROM `pms_manual_entry` WHERE date='" . $log1['log_time'] . "' and emp_id= " . $employee_data['userid'])->queryRow();
        $ignore_entry = Yii::app()->db->createCommand("SELECT * FROM `pms_ignore_punches` WHERE date='" . $log1['log_time'] . "' and empid= " . $employee_data['userid'])->queryRow();

        // print_/r($shift);

        if (!empty($shift) && $shift['shift_starts'] != '' && $shift['shift_ends'] != '') {

            $punchdate = date('Y-m-d', strtotime($log1['log_time']));
            $gracetime_before = floatval($shift['grace_period_before']) * 60 * 60;
            $gracetime_after = floatval($shift['grace_period_after']) * 60 * 60;
            if (strtotime($shift['shift_starts']) < strtotime($shift['shift_ends'])) {
                $shiftend = $punchdate . " " . $shift['shift_ends'] . ":00";
            } else {
                $next = date('Y-m-d', strtotime('+1 day', strtotime($punchdate)));
                $shiftend = $next . " " . $shift['shift_ends'] . ":00";
            }
            $shiftstart = date('Y-m-d H:i:s', strtotime($punchdate . " " . $shift['shift_starts'] . ":00") - $gracetime_before);
            $shiftend = date('Y-m-d H:i:s', strtotime($shiftend) + $gracetime_after);

            if ($shiftstart <= $log1['log_time'] && $shiftend >= $log1['log_time']) {
                $shift_name = $shift['shift_name'];
                $shift_id = $shift['shift_id'];
            } else if (!empty($shift_entry)) {

                $shift_name = $shift['shift_name'];
                $shift_id = $shift['shift_id'];
            } else {

                $time1 = strtotime(date('H:i:s', strtotime($log1['log_time'])));
                $time2 = strtotime("06:30:00");

                $curdate = date('Y-m-d', strtotime($log1['log_time']));
                $prev_date = date('Y-m-d', strtotime('-1 day', strtotime($curdate)));

                $nytshifts = Yii::app()->db->createCommand("SELECT shift_name,pms_shift_assign.shift_id FROM `pms_shift_assign` left join pms_employee_shift on pms_employee_shift.shift_id = pms_shift_assign.shift_id WHERE  att_date ='" . $prev_date . "' and user_id= " . $log1['empid'] . "  and shift_ends ='06:00' ")->queryRow();

                if ($time1 <= $time2 && !empty($nytshifts)) {

                    $shift_name = $nytshifts['shift_name'];
                    $shift_id = $nytshifts['shift_id'];
                }
            }
        } else {
            $shift_name = $shift['shift_name'];
            $shift_id = $shift['shift_id'];
        }

        if ($shift_name == '' && isset($log1['entry_id'])) {
            $shift_name = 'Manual Entry';
        }


        $user = '';
        $msg = '';
        $color = '';
        $decision_by = '';
        $status = '';
        $comnt = '';

        if (!empty($shift_entry)) {
            $status = $shift_entry['status'];
            $decision_by = ($shift_entry['decision_by'] != '') ? $shift_entry['decision_by'] : '';
            $comnt = $shift_entry['comment'];
        } else if (!empty($manual_entry)) {
            $status = $manual_entry['status'];
            $decision_by = ($manual_entry['decision_by'] != '') ? $manual_entry['decision_by'] : '';
            $comnt = $shift_entry['comment'];
        } else if (!empty($ignore_entry)) {
            $status = $ignore_entry['status'];
            $decision_by = ($ignore_entry['decision_by'] != '') ? $ignore_entry['decision_by'] : '';
            $comnt = $shift_entry['comment'];
        }

        if ($decision_by != '') {
            $data_by = Users::model()->findByPk($decision_by);
            $user = $data_by['first_name'] . ' ' . $data_by['last_name'];
        }

        if ($status != '') {

            if ($status == 0) {
                $msg = 'Pending for Approval of shift date : ' . (isset($manual_entry['shift_date']) ? date('d-m-Y', strtotime($manual_entry['shift_date'])) : '');
                $color = 'red';
            } else if ($status == 1) {
                $msg = 'Approved by ' . $user . ' of shift date : ' . (isset($manual_entry['shift_date']) ? date('d-m-Y', strtotime($manual_entry['shift_date'])) : '');
                $color = 'green';
            } else {
                $msg = 'Rejected by ' . $user . ' of shift date : ' . (isset($manual_entry['shift_date']) ? date('d-m-Y', strtotime($manual_entry['shift_date'])) : '');
                $color = 'red';
            }
        }

        $newdate = date('Y-m-d', strtotime($log1['log_time']));

        $log1['shift_id'] = $shift_id;
        $log1['shift_name'] = $shift_name;
        $log1['msg'] = $msg;
        $log1['color'] = $color;
        $log1['comnt'] = $comnt;
        $log1['status'] = $status;

        if (!empty($shift_entry)) {

            $log1['shiftentry_id'] = $shift_entry['shiftentry_id'];
            $log1['type'] = $shift_entry['type'];
            $log1['shift_date'] = $shift_entry['shift_date'];
        }

        $newarray[$newdate][] = $log1;
    }
    //   exit;
}
?>
<?php if (!empty($newarray)) {
    ?>
    <div class="shift_details">
    <div class="row">
        <div class="col-md-12">
            <img id="loadernew" src="images/loading.gif" class="width-30 display-none"/>
            <div class="alert alert-success alert-dismissible fade in display-none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span class="newalert"></span>
            </div>
            <div class="col-md-6">
                <h4 class="padding-bottom-10">Ignore Punches</h4>
                <textarea class="margin-0 width-263 height-38" id="comment" placeholder="Comment"></textarea>
                <button class='btn btn-danger btn-sm addcls' value="2">Ignore</button>
            </div>

            <!-- manual entry -->
              <div class="col-md-6 manula-punch1" >
                     <h4 class="margin-left-36 padding-bottom-10">Add Manual Entry</h4>
                     <span class="position-relative">
                        <div>
                          <span class="red-color display-inline-block position-absolute bottom-minus-39 left-34 font-11 white-space-nowrap">[ Use 24 hrs format HH:mm:ss]</span>
                          <input name="manual_time" id="manual_time" placeholder="00:00:00" class="margin-left-35 width-105 height-25"  />
                        </div>
                      </span>
                      <!-- <sup style="color:red;">[ Use 24 hrs format HH:mm:ss]</sup> -->
                      <select id="manual_date display-none">
                          
                    <option value="2"><?= date('Y-m-d', strtotime($date)); ?></option>
                        
                      </select>
                      <textarea  id="manual_comment" placeholder="Comment"></textarea>
                      <button class="btn btn-primary btn-sm" id="addtime">ADD</button>
                      <!-- end   -->

            </div>
        </div>
      </div>
    </div>
<?php }
?>

<div class="gridviewlist menupermission col-md-12">

    <div class="grid">
<?php
if (!empty($newarray)) {
    $prev_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
    $next_date = date('Y-m-d', strtotime('+1 day', strtotime($date)));


    $k = 1;
    $msg = '';
    $color = "";
    foreach ($newarray as $log1) {

        //echo '<pre>';print_r($log1);exit;

        $count = count($log1);

        $c = 0;
        echo '<div class="grid-view item gray-border">
                            <table  class="items greytable content">';

        foreach ($log1 as $key => $data) {

            // echo '<pre>';print_r($data);exit;
            $title = '';
            if (isset($data['comnt'])) {
                $title = $data['comnt'];
            }

            if ($c == 0) {
                ?>
                        <thead class="items" >
                            <tr>
                                <th>
                                    <b class="font-14"><label for=""><?= date('d-m-Y', strtotime($log1[0]['log_time'])) ?></label></b>
                                </th>

                            </tr>
                        </thead>
                        <tbody>
            <?php } $c++; ?>

                        <tr>
                            <td id="checkboxes" title="<?= $title; ?>">

            <?php if (!isset($data['shiftentry_id']) && !isset($data['entry_id']) && !isset($data['ig_id'])) { ?><input type="checkbox" class="checkitem checkbox_<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" name="level-2" value="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" logtime="<?= $data['log_time'] ?>" />
            <?php } else { ?><label>&nbsp;&nbsp;</label><?php } ?>
                                <label for="<?= isset($data['punchlogid']) ? $data['punchlogid'] : "" ?>"><?= date('H:i:s A', strtotime($data['log_time'])) ?></label>

                                <span  class="font-12 red-color margin-left-19 display-inline-block"><?= ($data['shift_name'] != '') ? $data['shift_name'] : " -- " ?></span>&nbsp;&nbsp;<?php if (isset($data['entry_id'])) { ?>
            <?php } ?>

            <?php if (isset($data['shiftentry_id'])) { ?>
                                    <span class="font-12 strong-blue-two margin-left-2">
                                    <?php if (isset($data['type']) && $data['type'] == 1) {
                                        echo '(Shift Moved - ' . date('Y-m-d', strtotime($data['shift_date'])) . ') ';
                                    } else {
                                        echo 'Added to shift';
                                    } ?>
                                    </span>
                                <?php } ?>

                                    <?php if (isset($data['entry_id'])) { ?>
                                    <span class="font-12 strong-blue-two margin-left-2">
                                    <?php echo 'Manual Entry'; ?>
                                    </span>

                                <?php } ?>


                                <!-- pending for approval -->
                                <?php if (isset($data['msg']) && $data['msg'] != '') { ?>
                                    <span style="font-size:12px;margin-left:2px;color:<?= $data['color']; ?>;">
                <?php echo '( ' . $data['msg'] . ' )'; ?>

                                                  <!-- <input type="button" name='approve' id="shi_approve" class='shift_action btn btn-success btn-xs' value='Approve' />
                                                  <input type="button" class='shift_action btn btn-danger btn-xs' name='reject' id="shi_reject" value='Reject' /> -->
                                    </span>

                <?php if (isset(Yii::app()->user->superadmin) && ( Yii::app()->user->superadmin == 1 ) && isset($data['entry_id'])) { ?>

                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/close.png" width="15" height="15"  class="remove" title="Delete" id="<?= $data['entry_id']; ?>" class="pull-right cursor-pointer">
                <?php } ?>

                                <?php } ?>


                                <!-- pending for approval end -->


            <?php $msg ='';
             if (isset($data['ig_id'])) { ?>
                   <span style="font-size:12px;color:#2727cc;margin-left:2px;">IGNORED <?php if( isset($data['status']) && $data['ig_status'] ==0) { ?></span> <span style="font-size:12px;color:red;">( Pending for approval )<?php } ?></span>
                   <?php if( isset($data['ig_decision_by']) && $data['ig_decision_by']!="") {
                           $approved_by = Users::model()->findByPk($data['ig_decision_by']);
                           $user_approved_by = $approved_by['first_name'] . ' ' . $approved_by['last_name'];
                            if($data['ig_status'] ==2){
                               $msg =' Rejected by';
                            }
                            if($data['ig_status'] ==1){
                               $msg =' Approved by';
                            }
                      ?>
                     <span class="font-12 margin-left-2 green-color"><?php echo $msg .' '.$user_approved_by; ?></span>
                   <?php } ?>

                    <button id="ignore" name="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" class="btn btn-xs btn-primary">Include</button>
            <?php } ?>

                                <!-- up and down arrow -->
                                <?php if (date('Y-m-d', strtotime($data['log_time'])) == $date && !isset($data['shiftentry_id']) && !isset($data['entry_id'])) { ?>

                                    <span class="pull-right icon icon-arrow-right arrow"  data="<?= $date; ?>"  type="down" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>" ></span>
                                    <div class="popupboxnew display-none">
                                        <textarea placeholder="Comments" id="movecoment"></textarea>
                                        <input type="submit" value="move right" class="btn btn-xs btn-primary moveshift" name="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" data="<?= $date; ?>" newtype="up" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>" />
                                    </div>

            <?php } if (date('Y-m-d', strtotime($data['log_time'])) == $date && !isset($data['shiftentry_id']) && !isset($data['entry_id'])) { ?>

                                    <span class="pull-right icon icon-arrow-left arrow"  data="<?= $date; ?>" type="up" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>" ></span>

                                    <div class="popupboxnew display-none">
                                        <textarea placeholder="Comments" id="movecoment"></textarea>
                                        <input type="submit" value="move left" class="btn btn-xs btn-primary moveshift" name="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" data="<?= $date; ?>" newtype="down" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>"/>
                                    </div>

            <?php } if (date('Y-m-d', strtotime($data['log_time'])) == $prev_date && !isset($data['shiftentry_id']) && !isset($data['entry_id'])) { ?>

                                    <span class="pull-right icon icon-arrow-right arrow" data="<?= $prev_date; ?>" type="up" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>"  ></span>

                                    <div class="popupboxnew display-none">
                                        <textarea placeholder="Comments" id="movecoment"></textarea>
                                        <input type="submit" value="move right" class="btn btn-xs btn-primary moveshift" name="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" data="<?= $prev_date; ?>" newtype="up" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>" />
                                    </div>

            <?php } if (date('Y-m-d', strtotime($data['log_time'])) == $next_date && $key == 0 && !isset($data['shiftentry_id']) && !isset($data['entry_id'])) { ?>

                                    <span class="pull-right icon icon-arrow-left arrow"  data="<?= $next_date; ?>" type="down" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>" ></span>

                                    <div class="popupboxnew display-none">
                                        <textarea placeholder="Comments" id="movecoment"></textarea>
                                        <input type="submit" value="move left" class="btn btn-xs btn-primary moveshift" name="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" data="<?= $next_date; ?>" newtype="down" logtime="<?= $data['log_time'] ?>" empid="<?= $data['empid'] ?>" />
                                    </div>
            <?php } ?>

                                <!-- up and down arrow ends -->

                                <?php if ($data['shift_name'] == '') { ?>
                                    <div class="pull-right">
                                        <button name="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" class="btn btn-xs btn-primary addtoshift text-align-right">Add</button>
                                        <div class="popupbox display-none">
                                            <textarea placeholder="Comments" id="addcoment"></textarea>
                                            <input type="submit" value="Apply" class="btn btn-xs btn-primary subaddtoshift" name="<?= isset($data['punchlogid']) ? $data['punchlogid'] : ""; ?>" />
                                        </div>
                                    </div>
            <?php } ?>

                            </td>
                        </tr>
                            <?php
                            }
                            echo ' </tbody>
                        </table>
                    </div>';
                        }///////
                    } else { ?>
                    </div>  
                       <div class="shift_details">
    <div class="row">
        <div class="col-md-12">
            <img id="loadernew" src="images/loading.gif" class="width-30 display-none"/>
            <div class="alert alert-success alert-dismissible fade in display-none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span class="newalert"></span>
            </div>
            <div class="col-md-6">
                <h4 class="padding-bottom-10">Ignore Punches</h4>
                <textarea class="margin-0 width-263 height-38" id="comment" placeholder="Comment"></textarea>
                <button class='btn btn-danger btn-sm addcls manual-punch-ignore' value="2">Ignore</button>
            </div>
            <!-- manual entry -->
              <div class="col-md-6 manula-punch1" >
                     <h4 class="padding-bottom-10">Add Manual Entry</h4>
                        <div>
                     <span class="position-relative">
                          <span class="red-color display-inline-block position-absolute font-11 white-space-nowrap">[ Use 24 hrs format HH:mm:ss]</span>
                          <input name="manual_time" id="manual_time" placeholder="00:00:00" class="height-25 margin-top-20"  />
                      </span>
                      <!-- <sup style="color:red;">[ Use 24 hrs format HH:mm:ss]</sup> -->
                      <select id="manual_date" class="width-141 display-none" class="">
                          
                  
                          <option value="2"><?= date('Y-m-d', strtotime($date)); ?></option>
                          
                      </select>
                      <textarea class="width-263" id="manual_comment" placeholder="Comment"></textarea>
                      <button class="btn btn-primary btn-sm manual-punch-add margin-top-8" id="addtime">Add</button>

                    </div>
                      <!-- end   -->

            </div>
        </div>
      </div>
    </div>
                  <?php  }
                    ?>

    </div>
    <div class="clearfix"> &nbsp;
    </div>
</div>
                    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script>

                   $(document).ready(function () {

                     $('input[type=text], textarea').keyup(function () {
                         $(this).val($(this).val().toUpperCase());

                     })

                       $(".sub").click(function () {

                           var empid = $('#empid').val();
                           var date = $('#date').val();
                           if (empid == 0 && date == '') {
                               $('.errempid').show();
                               $('.errdate').show();
                           } else if (empid == 0) {
                               $('.errempid').show();
                               $('.errdate').hide();
                           } else if (date == '') {
                               $('.errempid').hide();
                               $('.errdate').show();
                           } else {
                               //alert();
                               $('#ignore-punches-form').submit();
                           }

                       });
                   });


                   $(document).ready(function ($) {

                        $('input[name="manual_time"]').mask('00:00:00');

                       //  drag and drop

                       // $("tbody.connectedSortable").sortable({
                       //     connectWith: ".connectedSortable",
                       //     //items: "> tr:not(:first)",
                       // 	  //items: "tr:not(.nosort)"
                       //     appendTo: "parent",
                       //     helper: "clone",
                       //     cursor: "move",
                       //     zIndex: 999990,
                       //     receive: function () {
                       //         alert($(this).val());
                       //     }
                       // });


                       $(".addtoshift").click(function () {

                           $(this).parent().find('.popupbox').show();
                           $(this).hide();

                       });

                       $(".subaddtoshift").click(function () {

                           var comment = $(this).parent().find('#addcoment').val();
                           var id = $(this).attr('name');

                           if (comment != '') {

                               $('#loadernew').show();
                               $.ajax({
                                   type: 'POST',
                                   url: '<?= Yii::app()->createAbsoluteUrl("attendance/IgnorePunches/addtoshift") ?>',
                                   data: {'data': id, 'comment': comment},
                                   success: function (data) {
                                       $('.newalert').html(data);
                                       location.reload();
                                   },
                               });
                           } else {

                               alert('Please add comment');

                           }

                       });

                       $(".addcls").click(function () {

                           var selected = [];
                           var comment = $('#comment').val();
                           var type = $(this).val();

                           var emp_id = $('.empval').val();


                           $('#checkboxes input:checked').each(function () {
                               selected.push($(this).val());
                           });

                           if (selected == '') {
                               alert('Please select checkbox');
                           } else if (comment != '') {

                               $('#loadernew').show();

                               $.ajax({
                                   type: 'POST',
                                   url: '<?= Yii::app()->createAbsoluteUrl("attendance/IgnorePunches/ignoreshift") ?>',
                                   data: {type: type, 'data': selected, 'comment': comment,'emp_id':emp_id},
                                   success: function (data) {
                                       $('.alert').show();
                                       $('.newalert').html(data);
                                        location.reload();
                                   },
                               });
                           } else {

                               alert('Please add comment');
                           }
                       });

                       $(".arrow").click(function () {

                           $(this).parent().find('.popupboxnew').show();
                           $(this).hide();


                       });


                       $(".moveshift").click(function () {

                           var date = $(this).attr('data');
                           var logtime = $(this).attr('logtime');
                           var type = $(this).attr('newtype');
                           var empid = $(this).attr('empid');
                           var comment = $(this).parent().find('#movecoment').val();

                           if (comment != '') {

                               $.ajax({
                                   type: 'POST',
                                   url: '<?= Yii::app()->createAbsoluteUrl("attendance/IgnorePunches/movetoshift") ?>',
                                   data: {type: type, 'date': date, 'logtime': logtime, 'empid': empid, 'comment': comment},
                                   success: function (data) {
                                       $('.alert').show();
                                       $('.newalert').html(data);
                                       location.reload();

                                   },
                               });

                           } else {
                               alert('Please add comment');
                           }

                       });


                       // $(".arrow").click(function(){

                       // 	var date = $(this).attr('data');
                       // 	var logtime = $(this).attr('logtime');
                       // 	var type = $(this).attr('type');
                       // 	var empid = $(this).attr('empid');
                       // 		$('#loadernew').show();

                       // 	 $.ajax({
                       //                 type: 'POST',
                       //                 url: '<?= Yii::app()->createAbsoluteUrl("attendance/IgnorePunches/movetoshift") ?>' ,
                       //                 data:{type: type ,'date':date,'logtime': logtime ,'empid': empid},
                       //                 success:function(data){
                       //                     $('.alert').show();
                       //                     $('.newalert').html(data);
                       //                     //location.reload();

                       //                 },
                       // 			});

                       // });


                       $(document).on("click", "#addtime", function (e) {

                           var time = $('#manual_time').val();
                           var userid = "<?= $userid; ?>";
                           var date = $('#manual_date :selected').text();
                           var coment = $('#manual_comment').val();
                           var shift_date = $('#date').val();

                           if (userid != '' && date != '') {

                               if (coment != '' && time != '') {

                                   $('#loadernew').show();

                                   $.ajax({
                                       type: "POST",
                                       url: "<?php echo $this->createUrl('IgnorePunches/Manualentry') ?>",
                                       data: {time: time, userid: userid, date: date, coment: coment, shift_date: shift_date},
                                   }).done(function (msg) {

                                       $('#loadernew').hide();
                                       location.reload();

                                   });
                               } else {
                                   alert('Please add time and comments');
                               }
                           } else {
                               alert('Please select employee and date');
                           }


                       });

                       $(document).on("click", ".remove", function (e) {

                           var id = $('.remove').attr('id');

                           if (confirm("Are you sure you want to delete this entry?")) {
                               $('#loadernew').show();
                               $.ajax({
                                   method: "POST",
                                   url: "<?php echo $this->createUrl('IgnorePunches/DeleteManualentry') ?>",
                                   data: {id: id, },
                               }).done(function (msg) {
                                   if (msg == 1) {
                                       $('#loadernew').hide();
                                       location.reload();
                                   }
                               });
                           }

                       });


                       $(document).on("click", "#ignore", function (e) {

                           $('#loadernew').show();
                           var id = $(this).attr("name");
                           var userid = "<?= $userid; ?>";
                           var comment = $(this).closest('.cmnt').find("textarea").val();

                           if (comment != '') {

                               $.ajax({
                                   type: "POST",
                                   url: "<?php echo $this->createUrl('IgnorePunches/AddtoIgnore') ?>",
                                   data: {id: id, userid: userid, comnt: comment},
                               }).done(function (msg) {
                                   if (msg == 1) {
                                       location.reload();
                                       $('#loadernew').hide();
                                   }

                               });
                           } else {
                               $('#loadernew').hide();
                               alert('Please add Comment');
                           }
                       });

                       $(".sub").click(function () {
                           $(".shift_details").show();
                       });



                   });


</script>

<script >
    "use strict";
    "object" != typeof window.CP && (window.CP = {}), window.CP.PenTimer = {programNoLongerBeingMonitored: !1, timeOfFirstCallToShouldStopLoop: 0, _loopExits: {}, _loopTimers: {}, START_MONITORING_AFTER: 2e3, STOP_ALL_MONITORING_TIMEOUT: 5e3, MAX_TIME_IN_LOOP_WO_EXIT: 2200, exitedLoop: function (o) {
            this._loopExits[o] = !0
        }, shouldStopLoop: function (o) {
            if (this.programKilledSoStopMonitoring)
                return!0;
            if (this.programNoLongerBeingMonitored)
                return!1;
            if (this._loopExits[o])
                return!1;
            var t = this._getTime();
            if (0 === this.timeOfFirstCallToShouldStopLoop)
                return this.timeOfFirstCallToShouldStopLoop = t, !1;
            var i = t - this.timeOfFirstCallToShouldStopLoop;
            if (i < this.START_MONITORING_AFTER)
                return!1;
            if (i > this.STOP_ALL_MONITORING_TIMEOUT)
                return this.programNoLongerBeingMonitored = !0, !1;
            try {
                this._checkOnInfiniteLoop(o, t)
            } catch (o) {
                return this._sendErrorMessageToEditor(), this.programKilledSoStopMonitoring = !0, !0
            }
            return!1
        }, _sendErrorMessageToEditor: function () {
            try {
                if (this._shouldPostMessage()) {
                    var o = {action: "infinite-loop", line: this._findAroundLineNumber()};
                    parent.postMessage(JSON.stringify(o), "*")
                } else
                    this._throwAnErrorToStopPen()
            } catch (o) {
                this._throwAnErrorToStopPen()
            }
        }, _shouldPostMessage: function () {
            return document.location.href.match(/boomerang/)
        }, _throwAnErrorToStopPen: function () {
            throw"We found an infinite loop in your Pen. We've stopped the Pen from running. Please correct it or contact support@codepen.io."
        }, _findAroundLineNumber: function () {
            var o = new Error, t = 0;
            if (o.stack) {
                var i = o.stack.match(/boomerang\S+:(\d+):\d+/);
                i && (t = i[1])
            }
            return t
        }, _checkOnInfiniteLoop: function (o, t) {
            if (!this._loopTimers[o])
                return this._loopTimers[o] = t, !1;
            var i = t - this._loopTimers[o];
            if (i > this.MAX_TIME_IN_LOOP_WO_EXIT)
                throw"Infinite Loop found on loop: " + o
        }, _getTime: function () {
            return+new Date
        }}, window.CP.shouldStopExecution = function (o) {
        var t = window.CP.PenTimer.shouldStopLoop(o);
        return t === !0 && console.warn("[CodePen]: An infinite loop (or a loop taking too long) was detected, so we stopped its execution. Sorry!"), t
    }, window.CP.exitedLoop = function (o) {
        window.CP.PenTimer.exitedLoop(o)
    };

</script>
<script >

    function resizeGridItem(item) {
        grid = document.getElementsByClassName("grid")[0];
        rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
        rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
        rowSpan = Math.ceil((item.querySelector('.content').getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
        item.style.gridRowEnd = "span " + rowSpan;
    }

    function resizeAllGridItems() {
        allItems = document.getElementsByClassName("item");
        for (x = 0; x < allItems.length; x++) {
            if (window.CP.shouldStopExecution(1)) {
                break;}
            resizeGridItem(allItems[x]);
        }
        window.CP.exitedLoop(1);

    }

    function resizeInstance(instance) {
        item = instance.elements[0];
        resizeGridItem(item);
    }

    window.onload = resizeAllGridItems();
    window.addEventListener("resize", resizeAllGridItems);

    allItems = document.getElementsByClassName("item");
    for (x = 0; x < allItems.length; x++) {
        if (window.CP.shouldStopExecution(2)) {
            break;
        }
    }
    window.CP.exitedLoop(2);

//# sourceURL=test.js

</script>


