<?php
/* @var $this IgnorePunchesController */
/* @var $data IgnorePunches */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ig_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ig_id), array('view', 'id'=>$data->ig_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('empid')); ?>:</b>
	<?php echo CHtml::encode($data->empid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ignore_entry')); ?>:</b>
	<?php echo CHtml::encode($data->ignore_entry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acces_logid')); ?>:</b>
	<?php echo CHtml::encode($data->acces_logid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>