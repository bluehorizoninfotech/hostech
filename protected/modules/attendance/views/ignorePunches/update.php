<?php
/* @var $this IgnorePunchesController */
/* @var $model IgnorePunches */

$this->breadcrumbs=array(
	'Ignore Punches'=>array('index'),
	$model->ig_id=>array('view','id'=>$model->ig_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List IgnorePunches', 'url'=>array('index')),
	array('label'=>'Create IgnorePunches', 'url'=>array('create')),
	array('label'=>'View IgnorePunches', 'url'=>array('view', 'id'=>$model->ig_id)),
	array('label'=>'Manage IgnorePunches', 'url'=>array('admin')),
);
?>

<h1>Update IgnorePunches <?php echo $model->ig_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>