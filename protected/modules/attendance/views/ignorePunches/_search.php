<?php
/* @var $this IgnorePunchesController */
/* @var $model IgnorePunches */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ig_id'); ?>
		<?php echo $form->textField($model,'ig_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'empid'); ?>
		<?php echo $form->textField($model,'empid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ignore_entry'); ?>
		<?php echo $form->textField($model,'ignore_entry'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acces_logid'); ?>
		<?php echo $form->textField($model,'acces_logid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->