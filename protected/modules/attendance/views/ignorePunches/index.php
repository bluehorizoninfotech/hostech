
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

<?php


	Yii::app()->clientScript->registerScript('myjquery', " 
	    $(document).ready(function () {
	        $('#date').datepicker({autoclose: true,dateFormat: 'yy-mm-dd'});
	       

	    });   


	");
?>


<div class="form pull-left margin-right-20 light-gray-border-right-2 width-300 margin-top-31">

<div class="form">



<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ignore-punches-form',
	
	'enableAjaxValidation' => true,
       'clientOptions' => array(
           'validateOnSubmit' => true,
           'validateOnChange' => true,
           'validateOnType' => false,
        ),
)); ?>

	


	<?php echo $form->errorSummary($model); ?> 

	<h1></h1>

	<div class="row">
		<?php echo $form->labelEx($model,'empid'); ?>
		<select id="empid" name="IgnorePunches[empid]" class="form-control">                       
			<option value="0">--Select --</option>
			<?php  

			if(isset(Yii::app()->user->company_id)){
              $users = Users::model()->findAll( array( 'condition'=>'company_id = '.Yii::app()->user->company_id, 'order'=>  'first_name' ));
            }else{

             $users = Users::model()->findAll( array( 'order'=>  'first_name' ));

            }

            foreach($users as $data) { ?>
			  <option value="<?= $data['emp_id'] ?>" <?php if(!empty($userid)){ if($data['emp_id'] == $userid) { ?> selected= selected <?php } } ?>><?= $data['first_name'] .' '. $data['last_name'] ?>
			  </option>
			
			<?php } ?>
		</select>
		
		<?php echo $form->error($model,'empid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php 
			if(!empty($date)){
			   $model->date  = date('Y-m-d',strtotime($date));
			}
            echo $form->textField($model,'date',array('id'=>'date','placeholder'=>'Date','class'=>"form-control",'autocomplete'=>"off"));
		
		?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit' ,array('class'=>'btn btn-primary sub')); ?>
		<span class="btn btn-primary" onClick="document.location.href='<?php  echo $this->createUrl('IgnorePunches/index') ?>'" class="margin-top-minus-4">Cancel</span>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

</div>

<div class="pull-left width-35-percentage margin-top-17">



<h1>Manage Punches</h1>


    <div>
      <img id="loadernew" src="images/loading.gif" class="width-30-percentage margin-left-159 display-none" />
    </div>

<table class="greytable">
	<thead>
	    <tr>
	      <th class="width-10">Sl no</th>
	      <th class="width-104 padding-5">Log time</th>
	      <th></th>
	      
	    </tr>
	  
         
	</thead>

	<tbody>
	<?php 

    if(!empty($logmodel)){ 

		$k=1;
		$msg ='';
    	$color = "";
		foreach($logmodel as $log1) { 

		    $user = '';
	    	if(isset($log1['decision_by']) && $log1['decision_by']  !=''){
				$decision_by = Users::model()->findByPk($log1['decision_by']);
				$user = $decision_by['first_name'].' '. $decision_by['last_name'];
			}
            if($log1['status']==0){
				$msg = 'Pending for Approval';
			   $color = '#ff5722';
			}else if ($log1['status'] == 1) {
				$msg = 'Approved by '.$user;
			    $color = 'green';
			}else{
			  $msg = 'Rejected by '.$user;
			  $color = 'red';
			}
			
		?>
	   <tr>
	     <td><?= $k; ?></td>
	     <td><?= date('H:i:s A',strtotime($log1['log_time'])); ?>&nbsp;</td>
		  <td>
           
		    <?php 
		    /*if(isset($log1['ig_id'])){*/
		    if( isset($log1['ig_id']) && $log1['ig_id']!='' ) { ?> 
		     <span><?= $log1['comment']?></span>
		      <a id="ignore" name="<?= isset($log1['punchlogid'])?$log1['punchlogid']:NULL; ?>" class="btn btn-success white-color padding-5">Include</a> -  <span style="font-size:10px;color:<?= $color; ?>"><?= $msg ?><span>
		    <?php } else if( isset($log1['entry_id']) && $log1['entry_id']!='' ){  ?>
		     <span><?= $log1['comment']?></span>&nbsp;
		     <a id="remove" name="<?= isset($log1['entry_id'])?$log1['entry_id']:NULL ?>" class="btn btn-primary white-color padding-5">Remove &nbsp;</a>(Manual entry) - <span style="font-size:10px;color:<?= $color; ?>"><?= $msg ?><span>
		     
		    <?php }else{  ?>
		     <div class="cmnt">
			    <textarea class="margin-0 width-181 height-29" id="comment"></textarea>
					    &nbsp;
			    <a id="ignore" name="<?= isset($log1['punchlogid'])?$log1['punchlogid']:NULL ?>" class="btn btn-danger white-color padding-5">Ignore </a>
		    </div>
		    <?php } ?>
		     		
		   
	    </td>
	   
	   </tr>
	<?php $k++; }  }else{ ?>
	  <tr><td colspan="4">No results</td></tr>
	<?php } ?>
	  
		
	</tbody>
		
</table>



</div>

<div class="pull-left width-35-percentage margin-left-42 margin-top-21">
   <h1>Punches Manual entry</h1>
   <label>Time</label>
   <input name="time" id="time" class="margin-left-35"  /><sup style="color:red;">[ Use 24 hrs format HH:mm:ss]</sup><br/>
    <label>Comment</label>
    <textarea  id="coment" class="margin-4 width-182 height-84"></textarea><br/>
    <button class="btn btn-primary margin-left-206 margin-top-10" id="addtime">Add</button>
</div>



<script type="text/javascript">

	 $(document).ready(function(){
	   $('input[name="time"]').mask('00:00:00');
	});

  $(document).on("click", "#ignore", function (e) {
       
       $('#loadernew').show();
  	   var id = $(this).attr("name");
       var userid = "<?= $userid; ?>";
       var comment =   $(this).closest('.cmnt').find("textarea").val();

        if(comment!=''){

	      $.ajax({
	            type: "POST",
	            url: "<?php  echo $this->createUrl('IgnorePunches/AddtoIgnore') ?>",
	            data: { id:id, userid:userid, comnt:comment},
	        }).done(function (msg) {
	        	if(msg==1){
	        	  location.reload();
	        	  $('#loadernew').hide();
	        	}
	           
	        });
	    }else{
	    	 $('#loadernew').hide();
           alert('Please add Comment');
	    }
    });

    $(document).on("click", "#addtime", function (e) {

          var time   = $('#time').val();
          var userid = "<?= $userid; ?>";
          var date   = "<?= $date ?>";
          var coment = $('#coment').val();

           if(userid!='' && date !=''){

           	if(coment!='' && time!=''){

           		$('#loadernew').show();

		      $.ajax({
		            method: "POST",
		            url: "<?php   echo $this->createUrl('IgnorePunches/Manualentry') ?>",
		            data: { time:time, userid:userid , date:date, coment:coment},
		        }).done(function (msg) {
		        	if(msg==1){

		        	  $('#loadernew').hide();
		        	  location.reload();
		        	  
		        	}
		           
		        });
		    }else{
		    	alert('Please add time and comments');
		    }
	    }else{
	    	alert('Please select employee and date');
	    }


    });

     $(document).on("click", "#remove", function (e) {

          var id   = $('#remove').attr('name');

          $('#loadernew').show();

          $.ajax({
	            method: "POST",
	            url: "<?php   echo $this->createUrl('IgnorePunches/DeleteManualentry') ?>",
	            data: { id:id, },
	        }).done(function (msg) {
	        	if(msg==1){
                  
                  $('#loadernew').hide();
                  
	        	  location.reload();
	        	  
	        	}
	           
	        });
	  


    });







</script>
