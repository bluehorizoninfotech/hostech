<?php
/* @var $this IgnorePunchesController */
/* @var $model IgnorePunches */

$this->breadcrumbs=array(
	'Ignore Punches'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List IgnorePunches', 'url'=>array('index')),
	array('label'=>'Manage IgnorePunches', 'url'=>array('admin')),
);
?>

<h1>Create IgnorePunches</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>