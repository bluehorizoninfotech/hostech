<?php

Yii::app()->clientScript->registerScript('myjquery', " 
    $('#show').click(function() {
            $(this).next().is(':visible') ? $(this).text('Show more') : $(this).text('Hide');
            $('.legendtbl').toggle('slide');
            $('.comment-box').toggle('slide');       
    });
    
    $('.importpunchlog').click(function(e){ 
        e.preventDefault();
        $('.loaderImage').show();
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
        $.ajax({
            method: 'post',
            url: '".$this->createUrl('attendance/Importpunchlog') ."',
            data: {startdate:startdate, enddate:enddate }
        }).done(function (msg) {
            $('.loaderImage').hide();
            //location.reload();
        });

    });   
");
?>

<div class="main_content">
    <div class="clearfix page_head">
        <input type="button" value="Settings" class="btn btn-primary pull-right"  data-toggle="modal" data-backdrop="static" data-target="#addEntry">
        <div class="hidden-xs pull-right clearfix sel-date">  
            <?php
            $this->renderPartial('_searchtimereport', array(
                'start_date' => $start_date,
                'end_date' => $end_date,
            ));
            ?>
        </div>
        <h1 class="heading">Attendance</h1>
        <span class="loaderdiv">
            <img id="loader" src="themes/advenser/images/loading.gif"/>
        </span>
    </div>
     <?php
                    if (isset($_GET['month'])) {
                        $month = $_GET['month'];
                    } else {
                        $month = 0;
                    }
                    $action = 'index/';
                    ?>    

                    &laquo; &laquo;   <?php echo CHtml::link('Previous', array('attendance/' . $action, 'month' => $month - 1)) ?> |
                    <?php
                    if ($month >= 0) {
                        echo 'Today | Next &raquo; &raquo;';
                    } else {
                        ?>
                        <?php echo CHtml::link('Today', array('attendance/' . $action, 'month' => 0)) ?> | 
                        <?php echo CHtml::link('Next', array('attendance/' . $action, 'month' => $month + 1)) ?>
                        <?php
                    }
         ?>
    <div class="visible-xs clearfix sel-date">  
            <?php
            $this->renderPartial('_searchtimereport', array(
                'start_date' => $start_date,
                'end_date' => $end_date,
            ));
            ?>
        </div>
    
    
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="row"><div class="col-md-6 alert alert-' . $key . ' alert-dismissable" id="successmsg">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>';
        echo '<strong>' . $message . '</strong></div></div>';
    }
    ?>
    <input type="hidden" id="start_date" value="<?php echo $start_date; ?>">
    <input type="hidden" id="end_date" value="<?php echo $end_date; ?>">

    <div class="row-fluid">

        <div class="col-md-3">
            <a href="<?php echo Yii::app()->createUrl('attendance/importdata'); ?>" class="btn btn-success hide">Import Attendance</a> 
            <!--<a href="#<?php //echo Yii::app()->createUrl('attendance/Importpunchlog');    ?>" class="btn btn-sm btn-success importpunchlog">Import from Punchlog</a> <img class="loaderImage" style="display:none;" src='<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-loader.gif' />-->
<!--            <input type="button" value="Settings" class="btn btn-primary"  data-toggle="modal" data-backdrop="static" data-target="#addEntry">-->
            <!--<input type="button" value="CRON JOB" class="btn btn-warning cron_job" id="cron_job">-->
<!--            <span class="cron_job_buttons">
                <a href="<?php //echo Yii::app()->createUrl('attendance/cronjobtoaddattendance'); ?>" class="btn btn-sm btn-info">Run Cron Job</a> 
                <a href="<?php // echo Yii::app()->createUrl('attendance/deletecronjob'); ?>" class="btn btn-sm btn-danger">Delete Cron Job data</a> 
            </span>-->
        </div>


         <div class="col-md-3" >
           
        </div>

<!--        <div class="col-md-6 clearfix sel-date">  
            <?php
            /*$this->renderPartial('_searchtimereport', array(
                'start_date' => $start_date,
                'end_date' => $end_date,
            ));*/
            ?>
            
        </div>-->

        
        <div id="calendertable"></div>
        
        
    </div>
    <div class="row-fluid clearfix">
        
        <div class="col-md-6"><br>
            <button id="show" class="btn btn-primary margin-bottom-10"> Show more </button>
            <table class="table table-bordered legendtbl table_hold display-none">
                <thead>
                    <tr>
                        <th colspan="2" class="days-period"> Legend</th>
<!--                        <th class="days-period">Choose color</th>-->
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $legends = Legends::model()->findAll();
                    foreach ($legends as $legend) {
                        ?>
                        <tr>
                            <td><?php echo $legend['short_note']; ?></td>
                            <td><?php echo $legend['description']; ?></td>
<!--                            <td> <div class="input-append color cp_modal" data-color="rgb(218,57,41)" data-color-format="rgb" id="cp3">
                                    <input type="text" id="colorset" class="span6" value="" readonly /><span class="add-on"><i style="background-color: rgb(218,57,41)"></i></span>
                                </div></td>  -->
                        </tr>
                    <?php } ?>
                </tbody>
            </table>    
        </div> 
        <div class="col-md-6 comment-box display-none">
            <?php
            $tbl_px = Yii::app()->db->tablePrefix;

             $sql = "select date_format(a.att_date, \"%d/%m/%Y\") as entry_date,a.user_id,a.comments,  
            usr.first_name";
            $sql .= " from " . $tbl_px . "attendance as a
           INNER JOIN " . $tbl_px . 'users as usr on usr.userid=a.user_id';
           $sql .= "  WHERE a.att_date BETWEEN ('$start_date') AND '$end_date' and user_type != 1 AND status=0 AND user_type != 5
               ORDER BY  a.att_date DESC";
            
            
            $command = Yii::app()->db->createCommand($sql);
            $command->execute();
            $comments = $command->queryAll();
            ?>
            <h2 class="heading">List of Comments</h2>
            <ul>
                <?php
                foreach ($comments as $comment) {
                    if( $comment['comments']!=''){
                    ?>
                    <li><?php echo $comment['comments']; ?>
                        <i><?php echo $comment['first_name']; ?>-<?php echo $comment['entry_date']; ?></i>
                    </li>
                    <?php
                    }
                }
                ?>
            </ul>
        </div>        
    </div>
</div>
</div>


<div class="modal fade" id="entry">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">X</button>
                <h3>Attendance Form</h3>
            </div>
            <div class="row-fluid" id="leaveform"></div>
        </div>
    </div>
</div>









<!-- Add Entry Modal -->
<div class="modal fade" id="addEntry">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">×</button>
                <div class="tabbable">
                    <!--begin tabs going in wide content -->
                    <ul class="nav nav-tabs" id="maincontent" role="tablist">
                        <li class="active"><a id="attendset" href="#tab1" role="tab" data-toggle="tab">Attendance Entry</a></li>
                        <li><a id="colorset" href="#tab2" data-toggle="tab">Attendance Status Color Settings</a></li>
                        <li><a id="generalset" href="#tab3" data-toggle="tab">General Settings</a></li> 
     
                    </ul><!--/.nav-tabs.content-tabs -->
                    
                    
                </div>
            </div>
            <div id="getuserattendencebydate"></div>
        </div>
    </div>
</div>





<!--
<script src="<?php echo Yii::app()->request->baseUrl; ?>/calandercss/js/jquery.min.js"></script>-->
<script type="text/javascript">
    
    
   
    reloadcalender();
    getattendancebydate();
    function reloadcalender(){
         $('#loader').show();
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
        
        $.ajax({
                method: "post",
                url: "<?php echo $this->createUrl('attendance/calandersheet') ?>",
                data: {startdate: startdate, enddate: enddate},
            }).done(function (msg) {
                $("#calendertable").html(msg);
            });
    }
    
    $(document).ready(function(){
        //* colorpicker
        gebo_colorpicker = {
            init: function () {
                $('.cp_modal').colorpicker();

            }
        };
    
    
        //* colorpicker
        gebo_colorpicker.init();
        
        $(document).on("click", ".coldate", function () {
            $("#entry").show();
            var val = $(this).attr('data-value');
            var coldate = $(this).attr('data-coldate');
            $.ajax({
                method: "post",
                url: "<?php echo $this->createUrl('attendance/addattendance') ?>",
                data: {id: val, cell_date: coldate},
            }).done(function (msg) {
                $('#leaveform').html(msg);
            });

        });
        
        
        $(document).on("click", ".cp_modalclick", function () {

            var bgcolor = $(this).children('.colorpickertest').val();
            var leg_id = $(this).children('.colorpickertest').attr('id');
            setbgcolor(bgcolor,leg_id);

        });
        
        
        $(document).on("click", ".cp_modalclick1", function () {
            var bgcolor = $(this).val();
            var leg_id = $(this).attr('id');
             setbgcolor(bgcolor,leg_id);
        }); 
             

        //* datepicker
        gebo_datepicker.init();
        //* show all elements & remove preloader
        setTimeout('$("html").removeClass("js")', 1000);

        $('.addEntry').click(function () {

        });


        $('.fast_entry').click(function () {
            var classtocheck = $(this).attr('id');
            if ($(this).is(":checked"))
            {
                $('.fast_entry').removeAttr('checked');
                $("." + classtocheck).attr('checked', 'checked');
                $(this).attr('checked', 'checked');
            }
        });

        $('#actiontableid :radio').click(function () {
            // alert('tester');
            $('.fast_entry').removeAttr('checked');
            var classname = $(this).attr('class');
            //alert(classname);
            var totradio = $('.' + classname).length;
            var totsel = $(':radio.' + classname + ":checked").length;
            if (totradio === totsel)
                $('#' + classname).attr('checked', 'checked');
            //                alert(totradio);
        });

        

        $("#colorset,#generalset").on('click',function(){
            $('#selectdateforattd').hide();
        });

        $("#attendset").on('click',function(){
             $('#selectdateforattd').show();
        })
    });
  
    
    
    
    
    //* bootstrap datepicker
    gebo_datepicker = {
        init: function () {
            $('#dp1').datepicker();
            $('#dp2').datepicker();
          
            $('#selectattendancebydate').datepicker({format: "yyyy-mm-dd", autoclose: true});
            $("#selectattendancebydate").unbind('change').change(function (e) {
                e.stopImmediatePropagation();
                var dateText = $(this).data('date');
                getattendancebydate(dateText);
            });
                
            $('#dp_start').datepicker({format: "yyyy-mm-dd"}).on('changeDate', function (ev) {
                var dateText = $(this).data('date');

                var endDateTextBox = $('#dp_end input');
                if (endDateTextBox.val() != '') {
                    var testStartDate = new Date(dateText);
                    var testEndDate = new Date(endDateTextBox.val());
                    if (testStartDate > testEndDate) {
                        endDateTextBox.val(dateText);
                    }
                }
                else {
                    endDateTextBox.val(dateText);
                }
                ;
                $('#dp_end').datepicker('setStartDate', dateText);
                $('#dp_start').datepicker('hide');
            });
            $('#dp_end').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function (ev) {
                var dateText = $(this).data('date');
                var startDateTextBox = $('#dp_start input');
                if (startDateTextBox.val() != '') {
                    var testStartDate = new Date(startDateTextBox.val());
                    var testEndDate = new Date(dateText);
                    if (testStartDate > testEndDate) {
                        startDateTextBox.val(dateText);
                    }
                }
                else {
                    startDateTextBox.val(dateText);
                }
                ;
                $('#dp_start').datepicker('setEndDate', dateText);
                $('#dp_end').datepicker('hide');
            });
            $('#dp_modal').datepicker();
        }
    };
   
       
    function setbgcolor(bgcolor,leg_id){
         $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/addlegendcolor') ?>",
            data: {bgcolor: bgcolor, leg_id: leg_id},
        }).done(function (msg) {
            gebo_colorpicker.init();
            reloadcalender();
        });
    }
   
   function getattendancebydate(date){
        
        var attdate= $('#selectattendancebydate').val();
        var startdate = $('#start_date').val(); 
        var enddate = $('#end_date').val(); 
         $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/getattendancebydate') ?>",
            data: {date:attdate, startdate:startdate, enddate:enddate }
        }).done(function (msg) {
            $('#getuserattendencebydate').html(msg);
        });
       
       
   }
   
   $(document).ready(function(){
        $('#show').click(function() {
            $(this).next().is(':visible') ? $(this).text('Show more') : $(this).text('Hide');
            $('.legendtbl').toggle('slide');
            $('.comment-box').toggle('slide');
            
        });

        $('.importpunchlog').click(function(e){ 
            e.preventDefault();
            $('.loaderImage').show();
            var startdate = $('#start_date').val();
            var enddate = $('#end_date').val();
            $.ajax({
                method: 'post',
                url: '<?php echo $this->createUrl('attendance/Importpunchlog'); ?>',
                data: {startdate:startdate, enddate:enddate }
            }).done(function (msg) {
                $('.loaderImage').hide();
                //location.reload();
            });

        });
        $('.cron_job_buttons').hide();
        $('.cron_job').on('click',function(){
           $('.cron_job_buttons').toggle();
        });
   
   
   });
   
   
   
</script>