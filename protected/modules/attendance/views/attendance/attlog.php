<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php

$sites = CHtml::listData(Clientsite::model()->findAll(), 'id', 'site_name');

if (!isset($_GET['ajaxcall'])) {
    Yii::app()->clientScript->registerScript('myjquery', '

        $("#totpre").html($("#total_pre").html());
        $("#totot").html($("#total_ot").html());

        $(\'#dprtment\').val(0);
        $(\'#btn_submit_for_report\').on(\'click\', function(){
        var startdate = $(\'#startdate\').val();
        var tilldate = $(\'#tilldate\').val();
        var deptment = $(\'#dprtment\').val();
        var site = $(\'#site\').val();
        $("#loadingdiv").html("Loading...");
        $.ajax({
        url:"' . Yii::app()->createUrl('attendance/attendance/AttLog') . '",
         data:{sdate:startdate, tdate:tilldate,ajaxcall:1,deptment:deptment,site:site}
        }).done(function(msg){
            //alert(msg);
            $("#result").html(msg);
            $("#totpre").html($("#total_pre").html());
            $("#totot").html($("#total_ot").html());
        }).alwasys(function(){
            $("#loadingdiv").html("");
        });
        });');
    ?>
    <div class="attlog-sec">
    <div class="main_content">
        <div class="row">
            <div class="col-md-4">
                <h1 class="heading">Attendance Log</h1>
            </div>
            <div class="col-md-8 attsearch text-right">
                <?php echo CHtml::textField('startdate', date('01-m-Y'), array("id" => "startdate", 'size' => 10, 'Placeholder' => 'Start date','class'=>'form-control')); ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'startdate',
                    'ifFormat' => '%d-%m-%Y',
                ));

                //$dateend = date('d-m-Y', strtotime(' -1 day'));
                $dateend = date('t-m-Y');
                echo CHtml::textField('tilldate', $dateend, array("id" => "tilldate", 'size' => 10, 'Placeholder' => 'Till date','class'=>'form-control'));
                ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'tilldate',
                    'ifFormat'   => '%d-%m-%Y',
                ));



                $company ='';
                if( isset(Yii::app()->user->company_id) && Yii::app()->user->company_id!=''  ){
                    $company = " where company_id = ".Yii::app()->user->company_id;
                }

                $list =  Yii::app()->db->createCommand("SELECT concat_ws(' ',first_name,last_name) as fullname,`userid` FROM `pms_users` ".$company." order by fullname")->queryAll();

               /* echo CHtml::dropDownList('emp', (isset(Yii::app()->user->empid) ? Yii::app()->user->empid : 0), CHtml::listData($list, "emp_id", "fullname"), array('empty' => 'Select a user', 'id' => 'empid',
                        'ajax' => array(
                            'type' => 'POST',
                            'url' => CController::createUrl('accesslog/latereport' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')),
                            'data' => array('empid' => 'js:this.value', 'ajax' => 1),
                            'success' => 'function(data) {
                             if(data==1){
                                   window.location="' . CController::createUrl('accesslog/latereport' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')) . '";
                                }
                            }'
                        )
                    ));*/


                    echo CHtml::dropDownList('emp', (isset(Yii::app()->user->empid) ? Yii::app()->user->empid : 0), CHtml::listData($list, "emp_id", "fullname"), array('empty' => 'Select a user', 'id' => 'empid',
                        'ajax' => array(
                            'type' => 'POST',
                            'url' => CController::createUrl('accesslog/latereport' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')),
                            'data' => array('empid' => 'js:this.value', 'ajax' => 1),
                            'success' => 'function(data) {
                             if(data==1){
                                   /*window.location="' . CController::createUrl('accesslog/latereport' . (isset($_GET['days']) ? '&days=' . intval($_GET['days']) : '')) . '";*/
                                }
                            }'
                        )
                    ));

            ?>
                <?php echo CHtml::button('Submit', array('id' => 'btn_submit_for_report','class'=>'btn btn-sm btn-primary margin-left-10')); ?>
        `   </div>
        <?php
            }
        ?>
    </div>
    <?php
    if (!isset($_GET['ajaxcall'])) {
        ?>
        <div class="row">
            <div class="col-md-12 white-bg padding-top-10" id='result'>
                <?php
            }
            ?>
                <div class="row">
                    <div class="col-md-3" id='duration'><p><?php echo date("F d Y", strtotime($start_date)) . ' to ' . date("F d Y", strtotime($end_date)) ?></p></div>
                    <div class="col-md-3 red-color" id="loadingdiv">&nbsp;</div>
                    <div class="col-md-3 col-md-push-3 text-right"><p>Total Records: <?php echo count($results) ?>
                    </p></div>

                </div>
            <div id="parent">
            <table class="table table-bordered" id="attenrty">
                <thead>
                    <tr>
                        <th rowspan="2">S.No.</th>
                        <th rowspan="2">Employee Id</th>
                        <th rowspan="2">Employee Name</th>
                        <th rowspan="2"><?php echo Yii::t('en', 'usertype'); ?></th>
                        <th rowspan="2">Designation</th>
                        <th>Attendance Date</th>
                        <th>Type</th>
                        <th>Comment</th>
                        <th>Created By</th>
                        <th>Created Date</th>

                    </tr>

                </thead>
                <tbody>
                    <?php

                    $stotpr = array();   //site based total present
                    $sitetotot = array();  //site based total OT

                    $m = 1;
                    if (!empty($results)) {

                        $grandpresent = 0;
                        $grandot=0;
                        $ots=array();
                        $des=NULL;
                        $total_late= 0;
                        foreach ($results as $puserid => $res) {


                            if(!empty($res['designation'])){
                                $des = Yii::app()->db->createCommand("SELECT * FROM `pms_employee_default_data` WHERE `default_data_id` =".$res['designation'])->queryRow();
                                $res['designation'] = $des['label'];

                              }


                            ?>
                            <tr>
                                <td><?php echo $m++; ?></td>
                                <td><?=  $res['emp_id']; ?></td>
                                <td><?=  $res['fullname']; ?></td>
                                <td><?=  $res['usertype']; ?></td>
                                <td><?=  $res['designation']; ?></td>
                                <td><?=  date('d-m-Y',strtotime($res['date'])); ?></td>
                                <td><?=  $res['description'].'( '.$res['short_note'].')'; ?></td>
                                <td><?=  $res['coment']; ?></td>
                                <td><?=  $res['cr_by']; ?></td>
                                <td class="white-space-nowrap"><?=  date('d-m-Y H:i:s',strtotime($res['cr_date'])); ?></td>

                                        <!--  late  -->
                             </tr>
                             <?php  }  ?>

                        <?php  }
                    if (count($results) == 0) {
                        ?>
                        <tr>
                            <th colspan="10">No users found.</th>
                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
            </div>
                    </div>
            <?php
//
//                    echo '<pre>';
//                    print_r($stotpr);
//                    print_r($sitetotot);
//                    echo '</pre>';
//
            ?>
           <!--  <div class="col-md-12"><p>Total Records: <?php echo count($results) ?></p></div> -->
            <?php
            if (!isset($_GET['ajaxcall'])) {
                ?>
            </div>
        </div>
    </div>
    <?php
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("#attenrty").tableHeadFixer({ 'head' : true});
    });
</script>
