<?php

Yii::app()->clientScript->registerScript('myjquery', "
    $(document).ready(function () {

        $('#date_from').datepicker({autoclose: true, dateFormat: 'dd-mm-yy'});
        $('#date_till').datepicker({autoclose: true, dateFormat: 'dd-mm-yy'});
         $('#datereport').datepicker({autoclose: true, dateFormat: 'dd-mm-yy'});
    });


");
?>
<div class="searchtimereport-sec">
<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl('attendance/attendance/index'),
        'method' => 'get',
    ));
    ?>

    <div class="form-group responsive-search-form">
    

        <?php
            if (!isset($_GET['TimeEntry']['date_from']) || $_GET['TimeEntry']['date_from'] == '') {
                $datefrom = $start_date;
            } else {
                $datefrom = $_GET['TimeEntry']['date_from'];
            }
            ?>
        <span>
            <?php echo CHtml::label('From', '', array('class' => '')); ?>
            <?php echo CHtml::textField('TimeEntry[date_from]', date('d-m-Y',strtotime($datefrom)), array("id" => "date_from",'class'=>'width-100  height-28','autocomplete'=>'off')); ?>
            <?php //echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button3", "class" => "pointer dateimg", "style" => "cursor:pointer;width:25px;")); ?>
        </span>

        <?php
            if (!isset($_GET['TimeEntry']['date_till']) || $_GET['TimeEntry']['date_till'] == '') {
                $datetill = $end_date;
            } else {
                $datetill = $_GET['TimeEntry']['date_till'];
            }
            ?>
        <span>
            <?php echo CHtml::label('To', '', array('class' => '')); ?>
            <?php echo CHtml::textField('TimeEntry[date_till]', date('d-m-Y',strtotime($datetill)), array("id" => "date_till",'class'=>'width-100 height-28','autocomplete'=>'off')); ?>
            <?php //echo CHtml::image("images/calendar-month.png", "calendar", array("id" => "c_button2", "class" => "pointer dateimg", "style" => "cursor:pointer;width:25px;"));
            ?>
        </span>




        <?php echo CHtml::submitButton('GO', array('id' => 'serachbtn','class'=>'btn btn-sm btn-primary')); ?>


        <?php $this->endWidget(); ?>
    </div>
</div>
        </div>
