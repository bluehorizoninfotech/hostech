<div class="main_content"> 

    <input type="hidden" id="start_date" value="<?php echo $start_date; ?>">
    <input type="hidden" id="end_date" value="<?php echo $end_date; ?>">

    <div class="row-fluid">
        <div class="span6">
            <h2 class="heading">Title</h2>
        </div>
        <div class="span6 clearfix sel-date">  
            <?php
            $this->renderPartial('_searchtimereport', array(
                'start_date' => $start_date,
                'end_date' => $end_date,
            ));
            ?>
            
        </div>
        <div id="calendertable"></div>
        
        <div class="span12 table_hold" id="">


            <table class="table attnd_table table-bordered">
                <?php
                $date1 = new DateTime($start_date);
                $date2 = new DateTime($end_date);
                $diff = $date2->diff($date1)->format("%a");
                $totdays = $diff + 1;
                ?>
                <thead>
                    <tr>
                        <th rowspan="4" colspan="2" class="fixed_th">Employee Name</th>
                        <!-- <th rowspan="4" class="fixed_th"></th> -->
                        <th colspan="<?= $totdays ?>" class="days-period">
                            Consolidated Attendance (<?php echo date('d M Y', strtotime($start_date)) . '-' . date('d M Y', strtotime($end_date)); ?>)  
                        </th> 
                        <th colspan="13" class="days-period">Total</th>
                        <th rowspan="4">Present</th> 
                        <th rowspan="4">Absent</th>
                        <th rowspan="4">Salary</th>     
                    </tr>
                    <tr>
                        <?php
                        for ($i = 0; $i < $totdays; $i++) {

                            $week_date = $this->add_date($start_date, $i);
                            $week_datenext = $this->add_date($start_date, $i + 1);
                            $week_day = explode('/', $week_date);
                            $dat_next = explode('/', $week_datenext);

                            $fromday = $this->add_date($start_date, 0);
                            if ($fromday == $week_date) {
                                $flag = 1;
                            }
                            $countmonth = 0;
                            if ($week_day[1] != $dat_next[1]) {

                                $endmonthday = $week_day[0];
                                $startmothday = explode('-', $start_date);
                                $monthdaysdiff = ($endmonthday - $dat_next[0]) + 1;
                                if ($flag == 1) {
                                    $monthdaysdiff = ($endmonthday - $startmothday[2]) + 1;
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                    $countmonth++;
                                    $flag = 0;
                                } else {
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                }
                            }
                            $week_dates[] = $week_date;
                        }
                        $endmonth = explode('-', $end_date);

                        $startdatelast = $endmonth[0] . '-' . $endmonth[1] . '-01';
                        $monthdayarr[$endmonth[1]] = $this->getcountBetween2Dates($startdatelast, $end_date);
                        ?>
                        <?php
                        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
                        foreach ($monthdayarr as $month => $diffdays) {
                            if (count($monthdayarr) == 1) {
                                $diffdays = $totdays;
                            }

                            echo "<th colspan='" . $diffdays . "' class='month-period'>" . $monthsarr[$month] . "</th>";
                        }
                        ?>
                        <th rowspan="3">P</th>
                        <th rowspan="3">A</th>
                        <th rowspan="3">Saturday/ Sunday</th>
                        <th rowspan="3">Total Holidays</th>
                        <th rowspan="3">Total Paid Holidays</th>
                        <th rowspan="3">NIS</th>
                        <th rowspan="3">CL Utilized</th>
                        <th rowspan="3">PL Utilized</th>
                        <th rowspan="3">Sum of CL&PL</th>
                        <th rowspan="3">CO</th>
                        <th rowspan="3">LOP</th>
                        <th rowspan="3">Total Days</th>
                        <th rowspan="3">Sal Days</th>
                    </tr>

                    <tr>
                        <?php
                        $days = $this->getDatesBetween2Dates($start_date, $end_date);
                        foreach ($days as $key => $value) {

                            echo '<th>' . date('D', strtotime($value)) . '</th>';
                        }
                        ?>
                    </tr>
                    <tr>
                        <?php
                        $days = $this->getDatesBetween2Dates($start_date, $end_date);
                        foreach ($days as $key => $value) {

                            echo '<th>' . date('d', strtotime($value)) . '</th>';
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = 0;
                    foreach ($usermodel as $user) {
                        $count++;
                        ?>
                        <tr id="user_<?= $user['userid'] ?>">
                            <th class="fixed_th"><?= $count ?></th>
                            <th class="fixed_th"><?php echo $user['first_name']." ".$user['last_name']; ?></th>
                            <?php
                            for ($p = 0; $p < $totdays; $p++) {
                                $var = $week_dates[$p];
                                $datecur = str_replace('/', '-', $var);
                                $id = $user['userid'] . strtotime($datecur);
                                ?>
                                <td data-toggle="modal"  id="c_<?= $id ?>" data-backdrop="static" data-target="#entry" class="coldate" data-value="<?= $user["userid"] ?>" data-coldate="<?= strtotime($datecur) ?>">
                                </td>
                            <?php } ?>
                            <td id="p_user_<?= $user['userid'] ?>"></td>
                            <td id="a_user_<?= $user['userid'] ?>"></td>
                            <td id="sat_user_<?= $user['userid'] ?>"></td>
                            <td id="th_user_<?= $user['userid'] ?>"></td>
                            <td id="tpl_user_<?= $user['userid'] ?>"></td>
                            <td id="nis_user_<?= $user['userid'] ?>"></td>
                            <td id="cl_user_<?= $user['userid'] ?>"></td>
                            <td id="pl_user_<?= $user['userid'] ?>"></td>
                            <td id="clpl_user_<?= $user['userid'] ?>"></td>
                            <td id="co_user_<?= $user['userid'] ?>"></td>
                            <td id="lop_user_<?= $user['userid'] ?>"></td>
                            <td id="totalda_user_<?= $user['userid'] ?>"></td>
                            <td id="totalsal_user_<?= $user['userid'] ?>"></td>
                            <td id="c_presenttotal"></td>
                            <td id="c-absenttotal"></td>
                            <td id="c_salarytotal"></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>





        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2" class="days-period"> Legend</th>
                        <th class="days-period">Choose color</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $legends = Legends::model()->findAll();
                    foreach ($legends as $legend) {
                        ?>
                        <tr>
                            <td><?php echo $legend['short_note']; ?></td>
                            <td><?php echo $legend['description']; ?></td>
                            <td> <div class="input-append color cp_modal" data-color="rgb(218,57,41)" data-color-format="rgb" id="cp3">
                                    <input type="text" id="colorset" class="span6" value="" readonly /><span class="add-on"><i style="background-color: rgb(218,57,41)"></i></span>
                                </div></td>  
                        </tr>
                    <?php } ?>
                </tbody>
            </table>    
        </div> 
        <div class="span6 comment-box">
            <?php
            $tbl_px = Yii::app()->db->tablePrefix;

            $sql = "select date_format(a.att_date, \"%d/%m/%Y\") as entry_date,a.user_id,a.comments,  
            usr.first_name";
            $sql .= " from " . $tbl_px . "attendance as a
           INNER JOIN " . $tbl_px . 'users as usr on usr.userid=a.user_id';
            $sql .= "  WHERE a.att_date BETWEEN (CURDATE() - INTERVAL 3 DAY) AND CURDATE() and userid!=43
               ORDER BY  a.att_date DESC";
            $command = Yii::app()->db->createCommand($sql);
            $command->execute();
            $comments = $command->queryAll();
            ?>
            <h3 class="heading">List of Comments</h3>
            <ul>
                <?php
                foreach ($comments as $comment) {
                    ?>
                    <li><?php echo $comment['comments']; ?>
                        <i><?php echo $comment['first_name']; ?>-<?php echo $comment['entry_date']; ?></i>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>        
    </div>
</div>
</div>


<div class="modal " id="entry">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">X</button>
        <h3>Attendance Form</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid" id="leaveform">
        </div>

    </div>
    <div class="modal-footer">
    </div>
</div>


<!--   settings color picker --->

<div class="modal fade" id="color_picker">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">X</button>
        <h3>Attendance Status Color Settings</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span12">
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <th colspan="2" class="days-period"> Legend</th>
                            <th class="days-period">Choose color</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $legends = Legends::model()->findAll();
                        foreach ($legends as $legend) {
                            ?>
                            <tr>
                                <td ><?php echo $legend['short_note']; ?></td>
                                <td ><?php echo $legend['description']; ?></td>
                                <td> <div class="input-append color cp_modal cp_modalclick" data-color="<?= $legend['color_code'] ?>" data-color-format="rgb" id="cp3">
                                        <input type="text" id="<?= $legend['leg_id'] ?>" class="colorpickertest span6" value="<?= $legend['color_code'] ?>" readonly /><span class="add-on"><i style="background-color: <?= $legend['color_code'] ?>"></i></span>
                                    </div></td>  

                            </tr>
                        <?php } ?>
                    </tbody>
                </table>    
            </div> 
        </div>

    </div>
    <div class="modal-footer">
    </div>
</div>

//settings color picker






<!-- Add Entry Modal  -->
<div class="modal fade" id="addEntry">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">Attendance Entry</a></li>
                <li><a href="#tab2" data-toggle="tab">Default Values</a></li>
                <li><a href="#tab3" data-toggle="tab">Section 3</a></li>
            </ul>
        </div>
        Select Date <input type="text" id="dp3">
    </div>
    <div class="modal-body">
        <div class="row-fluid" id='actiontableid'>

            <table class="table table-striped" data-rowlink="a">
                <thead>
                    <tr>
                        <th>Employee name</th>
                        <th><input type="checkbox" value="&nbsp;" id="emptycell" class="fast_entry"></th>
                        <th title="Present">P<br> <input type="checkbox" value="p" id="attent_p" class="fast_entry"></th>

                        <th title="Approved Full Day Casual Leave">CL<br> <input type="checkbox" value="cl" id="attent_cl" class="fast_entry"></th>

                        <th title="Approved Half Day Casual Leave">HP<br> <input type="checkbox" value="hp" id="attent_hp" class="fast_entry"></th>

                        <th title="Approved Full Day Casual Leave,but cant be counted CL as CL credit exhausted">AL<br> <input type="checkbox" value="al" id="attent_al" class="fast_entry"></th>

                        <th title="Approved Half Day Casual Leave,but cant be counted HP as CL credit exhausted">AHL<br> <input type="checkbox" value="ahl" id="attent_ahl" class="fast_entry"></th>

                        <th title="Paid Leave, Decided on Managements's Discretion">PL<br> <input type="checkbox" value="pl" id="attent_pl" class="fast_entry"></th> 

                        <th title="Restricted Holiday">RH<br> <input type="checkbox" value="rh" id="attent_rh" class="fast_entry"></th>

                        <th title="Unapproved Full Day Leave">UL<br> <input type="checkbox" value="ul" id="attent_ul" class="fast_entry"></th>

                        <th title="Unapproved Half Day Leave">UHL<br> <input type="checkbox" value="uhl" id="attent_uhl" class="fast_entry"></th>

                        <th title="Optional Holiday">OH<br> <input type="checkbox" value="oh" id="attent_oh" class="fast_entry"></th>

                        <th title="Approved Holiday / Strike">H<br> <input type="checkbox" value="h" id="attent_h" class="fast_entry"></th>

                        <th title="Working on Weekends">W<br> <input type="checkbox" value="w" id="attent_w" class="fast_entry"></th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>emp1</td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="emptycell"></td>
                        <td><input type="radio"  name="attent[1]" value="&nbsp;" class="attent_p"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_cl"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_hp"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_al"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_ahl"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_pl"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_rh"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_ul"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_uhl"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_oh"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_h"></td>
                        <td><input type="radio" name="attent[1]" value="&nbsp;" class="attent_w"></td>

                    </tr>
                    <tr>
                        <td>emp2</td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="emptycell"></td>
                        <td><input type="radio"  name="attent[2]" value="&nbsp;" class="attent_p"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_cl"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_hp"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_al"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_ahl"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_pl"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_rh"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_ul"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_uhl"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_oh"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_h"></td>
                        <td><input type="radio" name="attent[2]" value="&nbsp;" class="attent_w"></td>
                    </tr>
                    <tr>
                        <td>emp3</td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="emptycell"></td>
                        <td><input type="radio"  name="attent[3]" value="&nbsp;" class="attent_p"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_cl"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_hp"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_al"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_ahl"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_pl"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_rh"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_ul"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_uhl"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_oh"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_h"></td>
                        <td><input type="radio" name="attent[3]" value="&nbsp;" class="attent_w"></td>
                    </tr>
                    <tr>
                        <td>emp4</td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="emptycell"></td>
                        <td><input type="radio"  name="attent[4]" value="&nbsp;" class="attent_p"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_cl"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_hp"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_al"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_ahl"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_pl"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_rh"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_ul"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_uhl"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_oh"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_h"></td>
                        <td><input type="radio" name="attent[4]" value="&nbsp;" class="attent_w"></td>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>
    <div class="modal-footer">
        <input type="button" value="Apply" class="btn">
        <input type="button" value="Reset" class="btn">
    </div>
</div>

<!--------------------------------------------------------->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>



<script src="<?php echo Yii::app()->request->baseUrl; ?>/calandercss/js/jquery.min.js"></script>
<script type="text/javascript">
    var $ = jQuery;
    $(document).ready(function () {


        reload();
        $('.coldate').click(function () {
            var val = $(this).attr('data-value');
            var coldate = $(this).attr('data-coldate');
            //alert(val); alert(date);

            $.ajax({
                method: "post",
                url: "<?php echo $this->createUrl('attendance/ajaxreturn') ?>",
                data: {'id': val, 'cell-date': coldate},
            }).done(function (msg) {
                $('#leaveform').html(msg);
                reload();
            });

        });
    });

    function reload() {
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
        // var hour_range = $('#hour_range').val();

        $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/getattendanceentries') ?>&userid=0&start=" + startdate + "&end=" + enddate,
            // data: {startdate: startdate, enddate: enddate},
        }).success(function (data) {
            data = $.parseJSON(data);
            $.each(data.total, function (i, item) {
                $('#c_' + item['cid']).html(item['legend']);
                $('#c_' + item['cid']).addClass(item['legend']);
                $('#p_' + item['eid']).html(item['present_days']);
                $('#a_' + item['eid']).html(item['absent_days']);
                $('#sat_' + item['eid']).html(item['satsundays']);
                $('#th_' + item['eid']).html(item['holidays']);
                $('#tpl_' + item['eid']).html(item['paidholidays']);
                $('#nis_' + item['eid']).html(item['nis']);
                $('#cl_' + item['eid']).html(item['cl_utilized']);
                $('#pl_' + item['eid']).html(item['pl_utilized']);
                $('#clpl_' + item['eid']).html(item['sum_clpl_utilized']);
                $('#co_' + item['eid']).html(item['co']);
                $('#lop_' + item['eid']).html(item['lop_days']);
                $('#totalda_' + item['eid']).html(item['calendardays']);
                $('#totalsal_' + item['eid']).html(item['sal_days']);

            });

        });
    }


    $(document).ready(function () {
        //* datepicker
        gebo_datepicker.init();
        //* show all elements & remove preloader
        setTimeout('$("html").removeClass("js")', 1000);

        $('.addEntry').click(function () {

        });


        

        //* colorpicker
        gebo_colorpicker.init();





    });
    $(document).on("click", ".cp_modalclick", function () {

        var bgcolor = $(this).children('.colorpickertest').val();
        var leg_id = $(this).children('.colorpickertest').attr('id');
         setbgcolor(bgcolor,leg_id);
       

    });
    $(document).on("click", ".add-on", function () {
        
        var bgcolor = $(this).previous('.colorpickertest').val();
        var leg_id = $(this).previous('.colorpickertest').attr('id');
        alert('bgcolor');
        // setbgcolor(bgcolor,leg_id);
    });    
    
    function setbgcolor(bgcolor,leg_id){
         $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/addlegendcolor') ?>",
            data: {bgcolor: bgcolor, leg_id: leg_id},
        }).success(function (msg) {
            alert();
            reload();
        });
    }
    
    
    
    //* colorpicker
    gebo_colorpicker = {
        init: function () {


            $('.cp_modal').colorpicker();
        }
    };
    //* bootstrap datepicker
    gebo_datepicker = {
        init: function () {
            $('#dp1').datepicker();
            $('#dp2').datepicker();

            $('#dp_start').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function (ev) {
                var dateText = $(this).data('date');

                var endDateTextBox = $('#dp_end input');
                if (endDateTextBox.val() != '') {
                    var testStartDate = new Date(dateText);
                    var testEndDate = new Date(endDateTextBox.val());
                    if (testStartDate > testEndDate) {
                        endDateTextBox.val(dateText);
                    }
                }
                else {
                    endDateTextBox.val(dateText);
                }
                ;
                $('#dp_end').datepicker('setStartDate', dateText);
                $('#dp_start').datepicker('hide');
            });
            $('#dp_end').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function (ev) {
                var dateText = $(this).data('date');
                var startDateTextBox = $('#dp_start input');
                if (startDateTextBox.val() != '') {
                    var testStartDate = new Date(startDateTextBox.val());
                    var testEndDate = new Date(dateText);
                    if (testStartDate > testEndDate) {
                        startDateTextBox.val(dateText);
                    }
                }
                else {
                    startDateTextBox.val(dateText);
                }
                ;
                $('#dp_start').datepicker('setEndDate', dateText);
                $('#dp_end').datepicker('hide');
            });
            $('#dp_modal').datepicker();
        }
    };
    (function () {
        'use strict';
        var table_hold = document.querySelector('.table_hold');
        var table = document.querySelector('.attnd_table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll('.attnd_table tbody .fixed_th'));
        var topHeaders = [].concat.apply([], document.querySelectorAll('.attnd_table thead th'));

        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        //table_hold.appendChild(topLeft);
        //topLeft.classList.add('top-left');
        //topLeft.style.width = computed.width;
        //topLeft.style.height = computed.height;

        table_hold.addEventListener('scroll', function (e) {
            var x = table_hold.scrollLeft;
            var y = table_hold.scrollTop;

            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
                //$('.fixed_th').style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(x, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });

        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }
    })();

</script>

<style>
    .timesheet_nav{
        text-align: right;
    }



    <?php
    $legends = Legends::model()->findAll();
    foreach ($legends as $legend) {
        ?>
        .<?php echo $legend['short_note'] ?>{
            background-color: <?php echo $legend['color_code']; ?>;
        }

    <?php } ?>

</style>