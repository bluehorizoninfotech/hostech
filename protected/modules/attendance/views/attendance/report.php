<page class="width-100-percentage" backtop="10mm" backbottom="10mm" backright="10mm">

<?php
    if(isset(Yii::app()->session['date_frm']) && Yii::app()->session['date_end'] ){

        $start_date = Yii::app()->session['date_frm'];
        $end_date = Yii::app()->session['date_end'];

    }else{

        $start_date = date('Y-m-01');
        $end_date = date('Y-m-d');

    }

?>

    <h2><?php echo $usermodel[0]['descrp'] ?></h2>
 <h4 align="center" > Attendance </h4>
<label >Attendance From:  <?= date('d-M-Y',strtotime($start_date))?></label>
<label >Attendance To:  <?= date('d-M-Y',strtotime($end_date))?></label>

<?php if( isset(Yii::app()->user->company_id)){ 

    $sql="SELECT `name` FROM `pms_company` WHERE company_id =".Yii::app()->user->company_id;
      $user_sites=Yii::app()->db->createCommand($sql)->queryAll(); 
      foreach( $user_sites as $site)
      { ?>
        <label >Site:  <?= $site['name']?></label>
  
      

    <?php } } ?>

    <table  border=1 >
        <thead>
            <tr>
                <th  rowspan="2" >Employee</th>
                 <?php

                        if(isset(Yii::app()->session['date_frm']) && Yii::app()->session['date_end'] ){

                            $start_date = Yii::app()->session['date_frm'];
                            $end_date = Yii::app()->session['date_end'];

                        }else{

                            $start_date = date('Y-m-01');
                            $end_date = date('Y-m-t');

                        }

                           $date1 = new DateTime($start_date);
                            $date2 = new DateTime($end_date);
                            $diff = $date2->diff($date1)->format("%a");
                            $totdays = $diff + 1;
                        for ($i = 0; $i < $totdays; $i++) {


                            $week_date = $this->add_date($start_date, $i);
                            $week_datenext = $this->add_date($start_date, $i + 1);
                            $week_day = explode('/', $week_date);
                            $dat_next = explode('/', $week_datenext);

                            $fromday = $this->add_date($start_date, 0);
                            if ($fromday == $week_date) {
                                $flag = 1;
                            }
                            $countmonth = 0;
                            if ($week_day[1] != $dat_next[1]) {

                                $endmonthday = $week_day[0];
                                $startmothday = explode('-', $start_date);
                                $monthdaysdiff = ($endmonthday - $dat_next[0]) + 1;
                                if ($flag == 1) {
                                    $monthdaysdiff = ($endmonthday - $startmothday[2]) + 1;
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                    $countmonth++;
                                    $flag = 0;
                                } else {
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                }
                            }
                            $week_dates[] = $week_date;
                        }
                        $endmonth = explode('-', $end_date);

                        $startdatelast = $endmonth[0] . '-' . $endmonth[1] . '-01';


                         $monthdayarr[$endmonth[1]] = $this->getcountBetween2Dates($startdatelast, $end_date);
                        ?>
                        <?php
                        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
                        foreach ($monthdayarr as $month => $diffdays) {
                            if (count($monthdayarr) == 1) {
                                $diffdays = $totdays;
                            }    ?>

                        <th colspan=<?= $diffdays; ?> > <?= $monthsarr[$month] ?></th>

                      <?php   } ?>
                <th colspan="6" align="center" >Total</th>
            </tr>
            <tr>

                <?php

                    $days = $this->getDatesBetween2Dates($start_date, $end_date);
                    foreach ($days as $key => $value) { ?>

                     <th ><?= substr(date('D', strtotime($value)),0,2).'<br>'. date('d', strtotime($value)) ;?></th>

             <?php   } ?>


              <th align="center" >P</th>
              <th align="center" >2P</th>
              <th align="center" >A</th>
              <th align="center" >H</th>
              <th align="center" >SUN</th>
          </tr>
        </thead>
        <tbody>
            <?php

            if(!empty($usermodel)){
              foreach ($usermodel as $user)  {


            ?>
        <tr>

            <td  align="left" ><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
             <?php
                for ($p  = 0; $p < $totdays; $p++) {
                $var = $week_dates[$p];
                $datecur = str_replace('/', '-', $var);
                $id = $user['userid'] . strtotime($datecur);
                $return_array=AttendanceController::getTodate($user['userid'],$datecur);           
                ?>
                <?php if($return_array==1){?>
                <td align="center" >
                <?= isset($dataarr[$id])?$dataarr[$id]['legend']:''; ?></td>
                <?php } 
                                else{ ?>
                                <td style="background-color:#ccc">dddddddddd</td>
                             <?php    } ?>
              <?php }  ?>
        <?php  if(array_key_exists($user['userid'],$newdata)){ ?>

            <td align="center" id="p_user_<?= $user['userid'] ?>" >
            <?= $newdata[$user['userid']]['pr']?>
            </td>
            <td align="center" id="p_user_<?= $user['userid'] ?>" >
            <?= $newdata[$user['userid']]['twopresent']?>
            </td>
            <td  align="center" id="p_user_<?= $user['userid'] ?>" >
            <?= $newdata[$user['userid']]['ab']?></td>

              <td  align="center" id="p_user_<?= $user['userid'] ?>" >
            <?= $newdata[$user['userid']]['hl']?></td>
              <td  align="center" id="p_user_<?= $user['userid'] ?>" >
            <?= $newdata[$user['userid']]['sun']?></td>

        <?php  }else{ ?>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>


        <?php } ?>
        </tr>
        <?php } } else { ?>
        <tr><td align="center">No result found</td></tr>

        <?php } ?>
        </tbody>
      
    </table>
</page>
