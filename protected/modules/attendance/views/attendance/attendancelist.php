
<div class="attendancelist-sec">
<div class="main_content">
    <div class="clearfix page_head">
    <?php
$main_grp_id='';
$groupList='';
    
    $designation   = (isset($_GET['designation']) && $_GET['designation']!="")?$_GET['designation']:"";


    $count_date = $this->getcountBetween2Dates($start_date, $end_date);

    if($count_date > 31 ){

         $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl('attendance/attendance/Expor', array('usertype'=>$usertype, 'grp_id'=>$main_grp_id,'skill'=>$skill,'designation'=>$designation)),
        'method' => 'get',

    ));

    }else{

        $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl('attendance/attendance/Export', array('usertype'=>$usertype, 'grp_id'=>$main_grp_id,'skill'=>$skill,'designation'=>$designation)),
        'method' => 'get',
        'htmlOptions'=> array( 'target'=>'_blank'),
    ));


    }  ?>

    <?php

    if(in_array('/attendance/attendanceexport', Yii::app()->session['menuauthlist'])){

    echo CHtml::submitButton('Export', array( 'id' => '','class'=>'btn btn-sm btn-primary pull-right clearfix margin-left-15 margin-top-12'));

    }


     ?>

    <?php $this->endWidget(); ?>

   
    <?php echo CHtml::link('Cancel',array('/attendance/attendance/index'), array('class'=>'btn btn-sm btn-info pull-right margin-left-15 margin-top-2')); ?>



    <?php if(in_array('/attendance/attendancesettings', Yii::app()->session['menuauthlist'])){ ?>

    <!-- <input type="button" value="Settings" class="btn btn-primary pull-right clearfix"  data-toggle="modal" data-backdrop="static" data-target="#addEntry" style="margin-left: 5px; margin-top: 2px;"> -->

  <?php } ?>

        <div class="hidden-xs pull-right clearfix sel-date">
            <?php
            $this->renderPartial('_searchtimereport', array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'division'=>$division,
                'usertype'=>$usertype,
                'groupList'=>$groupList
            ));
            ?>
        </div>
        <h1 class="heading pull-left">Attendance</h1>
   

    <?php if(in_array('/attendance/generateattendance', Yii::app()->session['menuauthlist'])){ ?>


        <div class="pull-left margin-left-20 margin-top-7">
            <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'action' => Yii::app()->createUrl('AttendanceGenerator/punchreportdaily&manual'),
                    'method' => 'get',
                ));
                ?>
                  <input type="text" name="date" autocomplete="off" id="datereport" placeholder="Date to regenerate" class="width-130 height-28">
                   <?php echo CHtml::submitButton('submit', array('class'=>'btn btn-sm btn-primary')); ?>
            <?php $this->endWidget(); ?>

        </div>
      <?php  } ?>


      <div class="clearfix"></div>


      <div class="pull-left margin-left-20 margin-top-11 margin-bottom-minus-20">
             <?php
                  if (isset($_GET['month'])) {
                      $month = $_GET['month'];
                  } else {
                      $month = 0;
                  }
                  $action = 'index/';
                  ?>

                  &laquo; &laquo;   <?php echo CHtml::link('Previous', array('attendance/' . $action, 'month' => $month - 1)) ?> |
                  <?php
                  if ($month >= 0) {
                      echo 'Today | Next &raquo; &raquo;';
                  } else {
                      ?>
                      <?php echo CHtml::link('Today', array('attendance/' . $action, 'month' => 0)) ?> |
                      <?php echo CHtml::link('Next', array('attendance/' . $action, 'month' => $month + 1)) ?>
                      <?php
                  }
          ?>
      </div>


      <div class="text-align-center"><?php //$this->widget('ProjectSite'); ?></div>

      <div class="pull-left margin-left-20 margin-top-13">
          <?php

              $gentime = Yii::app()->db->createCommand("SELECT max(`date`) as max FROM pms_cron")->queryRow();
              if(!empty($gentime['max'])){
        //echo $gentime['max'];
                  $gtime = date('d-m-Y H:i:s',strtotime($gentime['max']));
              }else{
                  $gtime = date('d-m-Y ', strtotime(' -1 day'));
              }
          ?>
          <i class="font-weight-bold red-color font-12">Last report generated: <?=  $gtime; ?></i>


      </div>


        <!--  end  -->
        <div class="loaderdiv margin-top-0 margin-bottom-0 margin-top-auto margin-bottom-auto width-64">
            <img id="loader" src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" width="50%"/>
        </div>
    </div>
     <!-- <div class="row">

        <div class="col-md-6">
               <?php
                    if (isset($_GET['month'])) {
                        $month = $_GET['month'];
                    } else {
                        $month = 0;
                    }
                    $action = 'index/';
                    ?>

                    &laquo; &laquo;   <?php echo CHtml::link('Previous', array('attendance/' . $action, 'month' => $month - 1)) ?> |
                    <?php
                    if ($month >= 0) {
                        echo 'Today | Next &raquo; &raquo;';
                    } else {
                        ?>
                        <?php echo CHtml::link('Today', array('attendance/' . $action, 'month' => 0)) ?> |
                        <?php echo CHtml::link('Next', array('attendance/' . $action, 'month' => $month + 1)) ?>
                        <?php
                    }
            ?>
        </div>
        <div class="col-md-6 text-right ">
            <?php

                $gentime = Yii::app()->db->createCommand("SELECT max(`date`) as max FROM pms_cron")->queryRow();
                if(!empty($gentime['max'])){
					//echo $gentime['max'];
                    $gtime = date('d-m-Y H:i:s',strtotime($gentime['max']));
                }else{
                    $gtime = date('d-m-Y ', strtotime(' -1 day'));
                }
            ?>
            <i style="font-weight: bold;color: red;font-size: 12px;">Last report generated: <?=  $gtime; ?></i>


        </div>

     </div> -->

  <div class="visible-xs clearfix sel-date">
            <?php
             $this->renderPartial('_searchtimereport', array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'division'=>$division,
                'usertype'=>$usertype,
                'groupList'=>$groupList
            ));
            ?>
        </div>


 

    <?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger alert-dismissable ">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
   <?php endif; ?>

    <input type="hidden" id="start_date" value="<?php echo $start_date; ?>">
    <input type="hidden" id="end_date" value="<?php echo $end_date; ?>">

    <div class="row-fluid">

        <div class="col-md-3">
            <a href="<?php echo Yii::app()->createUrl('attendance/importdata'); ?>" class="btn btn-success hide">Import Attendance</a>
          
        </div>


         <div class="col-md-3" >

        </div>
        <div id="calendertable"></div>
     </div>


     <div class="row-fluid clearfix">

        <div class="col-md-10"><br>
            <button id="show" class="btn btn-primary margin-bottom-10"> Conditions</button>
            <table class="table table-bordered legendtbl table_hold display-none">
                <thead>
                    <tr>
                        <th colspan="2" class="days-period"> Legend</th>
                        <th  class="days-period"> Conditions</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $legends = Legends::model()->findAll();
                    foreach ($legends as $legend) { ?>
                        <tr>
                            <td style="background-color:<?php echo $legend['color_code']; ?>" class="<?php echo $legend['short_note']; ?>"><?php echo $legend['short_note']; ?></td>
                            <td><?php echo strtoupper($legend['description']); ?></td>
                            <td>
                                <?php
                                 $con ='';
                                if( $legend['short_note'] =='SP'){
                                   $con = 'Only having single punch';
                                }
                                if( $legend['short_note'] =='LOP'){
                                   $con = 'No punch';
                                }
                                if( $legend['short_note'] =='SUN'){
                                   $con = '5P or holidays to be considered for the previous week while marking Sunday ';
                                }
                                if( $legend['short_note'] =='2P'){
                                   $con = 'For 2P enabled shifts, satisfy the conditions in punch template ( IN Time or late time )';
                                }
                                if( $legend['short_note'] =='PL'){
                                   $con = 'Manual entry';
                                }
                                if( $legend['short_note'] =='HO'){
                                   $con = 'Holiday from shift template and allowed holidays';
                                }
                                if( $legend['short_note'] =='P'){
                                   $con = 'Satisfy the conditions in punch template ( IN Time or late time )';
                                }
                                if( $legend['short_note'] =='HP'){
                                   $con = 'Satisfy the conditions in punch template ( IN Time or late time )';
                                }
                                if( $legend['short_note'] =='PEN'){
                                    $con = 'Request is pending';
                                 }
                                ?>
                                <b><?php echo strtoupper($con); ?></b>
                          </td>

                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 comment-box display-none">

        </div>
    </div>


</div>
</div>

<?php if (in_array('/attendance/addattendance', Yii::app()->session['menuauthlist'])){ ?>
<div class="modal mymodal" id="entry">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">X</button>
                <h3>Attendance Form</h3>
            </div>
            <div class="row-fluid" id="leaveform"></div>
        </div>
    </div>
</div>
<?php } ?>
<!-- Add Entry Modal -->
<div class="modal fade" id="addEntry">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">×</button>
                <div class="tabbable">
                    <!--begin tabs going in wide content -->
                    <ul class="nav nav-tabs" id="maincontent" role="tablist">
                        <li class="active"><a id="attendset" href="#tab1" role="tab" data-toggle="tab">Attendance Entry</a></li>
                        <li><a id="colorset" href="#tab2" data-toggle="tab">Attendance Status Color Settings</a></li>
                        <li><a id="generalset" href="#tab3" data-toggle="tab">General Settings</a></li>

                    </ul><!--/.nav-tabs.content-tabs -->


                </div>
            </div>
            <div id="getuserattendencebydate"></div>
        </div>
    </div>
</div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--
<script src="<?php echo Yii::app()->request->baseUrl; ?>/calandercss/js/jquery.min.js"></script>-->
<script type="text/javascript">



    reloadcalender();
    getattendancebydate();
    function reloadcalender(){
         $('#loader').show();
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
        var division  = "<?php echo $division; ?>";
        var usertype  = "<?php echo $usertype; ?>";
        var grp_id  = "<?php echo $grp_id; ?>";
        var skill="<?php echo $skill; ?>";
        var designation="<?php echo $designation; ?>";



        $.ajax({
                method: "post",
                url: "<?php echo $this->createUrl('attendance/calandersheet') ?>",
                data: {startdate: startdate, enddate: enddate, division:division, 
                usertype:usertype, grp_id:grp_id,skill:skill,designation:designation},
            }).done(function (msg) {
                $("#calendertable").html(msg);
            });
    }

    $(document).ready(function(){
        //* colorpicker
        gebo_colorpicker = {
            init: function () {
               // $('.cp_modal').colorpicker();

            }
        };


        //* colorpicker
        gebo_colorpicker.init();

        $(document).on("click", ".coldate", function () {
            $("#entry").show();
            var val = $(this).attr('data-value');
            var coldate = $(this).attr('data-coldate');
            $.ajax({
                method: "post",
                url: "<?php echo $this->createUrl('attendance/addattendance') ?>",
                data: {id: val, cell_date: coldate},
            }).done(function (msg) {
                $('#leaveform').html(msg);
            });

        });


        $(document).on("click", ".cp_modalclick", function () {

            var bgcolor = $(this).children('.colorpickertest').val();
            var leg_id = $(this).children('.colorpickertest').attr('id');
            setbgcolor(bgcolor,leg_id);

        });


        $(document).on("click", ".cp_modalclick1", function () {
            var bgcolor = $(this).val();
            var leg_id = $(this).attr('id');
             setbgcolor(bgcolor,leg_id);
        });


        //* datepicker
        gebo_datepicker.init();
        //* show all elements & remove preloader
        setTimeout('$("html").removeClass("js")', 1000);

        $('.addEntry').click(function () {

        });


        $('.fast_entry').click(function () {
            var classtocheck = $(this).attr('id');
            if ($(this).is(":checked"))
            {
                $('.fast_entry').removeAttr('checked');
                $("." + classtocheck).attr('checked', 'checked');
                $(this).attr('checked', 'checked');
            }
        });

        $('#actiontableid :radio').click(function () {
            // alert('tester');
            $('.fast_entry').removeAttr('checked');
            var classname = $(this).attr('class');
            //alert(classname);
            var totradio = $('.' + classname).length;
            var totsel = $(':radio.' + classname + ":checked").length;
            if (totradio === totsel)
                $('#' + classname).attr('checked', 'checked');
            //                alert(totradio);
        });



        $("#colorset,#generalset").on('click',function(){
            $('#selectdateforattd').hide();
        });

        $("#attendset").on('click',function(){
             $('#selectdateforattd').show();
        })
    });





    //* bootstrap datepicker
    gebo_datepicker = {
        init: function () {
            $('#dp1').datepicker();
            $('#dp2').datepicker();

            $('#selectattendancebydate').datepicker({format: "yyyy-mm-dd", autoclose: true});
            $("#selectattendancebydate").unbind('change').change(function (e) {
                e.stopImmediatePropagation();
                var dateText = $(this).data('date');
                getattendancebydate(dateText);
            });

            $('#dp_start').datepicker({format: "yyyy-mm-dd"}).on('changeDate', function (ev) {
                var dateText = $(this).data('date');

                var endDateTextBox = $('#dp_end input');
                if (endDateTextBox.val() != '') {
                    var testStartDate = new Date(dateText);
                    var testEndDate = new Date(endDateTextBox.val());
                    if (testStartDate > testEndDate) {
                        endDateTextBox.val(dateText);
                    }
                }
                else {
                    endDateTextBox.val(dateText);
                }
                ;
                $('#dp_end').datepicker('setStartDate', dateText);
                $('#dp_start').datepicker('hide');
            });
            $('#dp_end').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function (ev) {
                var dateText = $(this).data('date');
                var startDateTextBox = $('#dp_start input');
                if (startDateTextBox.val() != '') {
                    var testStartDate = new Date(startDateTextBox.val());
                    var testEndDate = new Date(dateText);
                    if (testStartDate > testEndDate) {
                        startDateTextBox.val(dateText);
                    }
                }
                else {
                    startDateTextBox.val(dateText);
                }
                ;
                $('#dp_start').datepicker('setEndDate', dateText);
                $('#dp_end').datepicker('hide');
            });
            $('#dp_modal').datepicker();
        }
    };


    function setbgcolor(bgcolor,leg_id){
         $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/addlegendcolor') ?>",
            data: {bgcolor: bgcolor, leg_id: leg_id},
        }).done(function (msg) {
            gebo_colorpicker.init();
            reloadcalender();
        });
    }

   function getattendancebydate(date){

        var attdate= $('#selectattendancebydate').val();
        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
         $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/getattendancebydate') ?>",
            data: {date:attdate, startdate:startdate, enddate:enddate }
        }).done(function (msg) {
            $('#getuserattendencebydate').html(msg);
        });
   }

   $(document).ready(function(){
        $('#show').click(function() {
            $(this).next().is(':visible') ? $(this).text('Conditions') : $(this).text('Hide');
            $('.legendtbl').toggle('slide');
            $('.comment-box').toggle('slide');

        });

        $('.importpunchlog').click(function(e){
            e.preventDefault();
            $('.loaderImage').show();
            var startdate = $('#start_date').val();
            var enddate = $('#end_date').val();
            $.ajax({
                method: 'post',
                url: '<?php echo $this->createUrl('attendance/Importpunchlog'); ?>',
                data: {startdate:startdate, enddate:enddate }
            }).done(function (msg) {
                $('.loaderImage').hide();
                //location.reload();
            });

        });
        $('.cron_job_buttons').hide();
        $('.cron_job').on('click',function(){
           $('.cron_job_buttons').toggle();
        });


   });



</script>

