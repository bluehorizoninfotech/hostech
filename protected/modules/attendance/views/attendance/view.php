<?php
/* @var $this AttendanceController */
/* @var $model Attendance */

$this->breadcrumbs=array(
	'Attendances'=>array('index'),
	$model->att_id,
);

$this->menu=array(
	array('label'=>'List Attendance', 'url'=>array('index')),
	array('label'=>'Create Attendance', 'url'=>array('create')),
	array('label'=>'Update Attendance', 'url'=>array('update', 'id'=>$model->att_id)),
	array('label'=>'Delete Attendance', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->att_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Attendance', 'url'=>array('admin')),
);
?>

<h1>View Attendance #<?php echo $model->att_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'att_id',
		'user_id',
		'date',
		'comments',
		'att_entry',
		'created_by',
		'created_date',
		'modified_date',
	),
)); ?>
