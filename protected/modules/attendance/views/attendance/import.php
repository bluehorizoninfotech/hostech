<?php

Yii::app()->clientScript->registerScript('myjquery', " 
    $('document').ready(function () {
        setTimeout('$(\"html\").removeClass(\"js\")', 1000);
        gebo_datepicker.init();
    });
    gebo_datepicker = {
            init: function () {
                $('#dp1').datepicker();
                $('#dp2').datepicker();
            }
        };


");
?>
<div class="main_content">
    <div class="row">
        <div class="col-md-6">
                <h3 class="heading">Import Attendance </h3>
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'attendance-form',
                    'method' => 'POST',
                    'action' => Yii::app()->createUrl('/attendance/importdata'),
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-inline'
                    )
                ));
                ?>
                <fieldset class="clearfix">

                        <div class="col-md-3">
                            <?php echo $form->labelEx($model, 'from'); ?> 
                            <?php echo $form->textField($model, 'from', array('id'=> 'dp1', 'class' => "form-control")); ?> 
                            <?php echo $form->error($model, 'from'); ?> 
                        </div>
                        <div class="col-md-3">
                            <?php echo $form->labelEx($model, 'to'); ?> 
                            <?php echo $form->textField($model, 'to', array('id'=> 'dp2', 'class' => "form-control")); ?> 
                            <?php echo $form->error($model, 'to'); ?> 
                        </div>
                        <div class="col-md-3">
                            <?php echo $form->labelEx($newmodel, 'file'); ?> 
                            <div class="controls <?php if ($newmodel->hasErrors('postcode')) echo "error"; ?>">
                            <?php echo $form->fileField($newmodel, 'file', array('class' => "input-xlarge")); ?> 
                            </div>
                            <?php echo $form->error($newmodel, 'file'); ?> 
                        </div>
                        <div class="col-md-12"> 
                            <br/><?php echo CHtml::submitButton('Upload', array('buttonType' => 'submit', 'type' => 'primary', 'icon' => 'ok white', 'label' => 'UPLOAD', 'class' => 'btn btn-success')); ?>
                            <?= CHtml::link('Download Sample CSV', $url = $this->createAbsoluteUrl('attendance/downloaddoc')) ?>
                            
                        </div>
                        
                        <?php //= CHtml::link('Download Sample CSV', $url = $this->createAbsoluteUrl('attendance/downloadsample')) ?>
                </fieldset>
                <?php $this->endWidget(); ?>
                <div class="alert alert-success alert-dismissable display-none" id="successmsg"></div>
                <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success" id="successmsg">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
                <?php endif; ?>
                <?php if (Yii::app()->user->hasFlash('error')): ?>
                    <div class="alert alert-error">
                        <?php echo Yii::app()->user->getFlash('error'); ?>
                    </div>
                <?php endif; ?>
                
	</div>  
    </div> 
    <div class="row-fluid">
        <h3 class="heading">Unsaved data </h3>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'import-attendance-grid',
            'itemsCssClass' => 'table table-bordered table-striped table_vam mediaTable',
            'dataProvider'=>$tempmodel->search(),
            //'filter'=>$tempmodel,
            'afterAjaxUpdate' => 'function(){gebo_datepicker.init();}',
            'columns'=>array(
                array(
                    'name' => 'check',
                    'id' => 'selectedIds',
                    'value' => '$data->att_id',
                    'class' => 'CCheckBoxColumn',
                    'selectableRows' => '100',
                ),
                array(
                    'header' => 'Sl No.',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + ($row+1)',
                ),
                array(
                    'name' => 'username',
                    'value' => '$data->username', 
                ),
                array(
                    'name' => 'att_date',
                    'value' => 'date("d-M-Y", strtotime($data->att_date))',
                ),
                'att_entry',
                /*array(
                    'name' => 'created_by',
                    'value' => '$data->createdBy->first_name." ".$data->createdBy->last_name', 
                    'filter' => CHtml::listData(Users::model()->findAll(array(
                            'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                            'condition' => 'user_type<=2 or user_type=6 and `status`=0',
                            'order' => 'first_name',
                            'distinct' => true
                        )), 'userid', 'first_name')
                ),*/
                'import_status',
                /*
                'created_date',*/

                array(
                    'class'=>'CButtonColumn',
                    'template' => '{update} {delete}'
                ),
            ),
        )); ?>
        
        
    </div>
</div> 
    