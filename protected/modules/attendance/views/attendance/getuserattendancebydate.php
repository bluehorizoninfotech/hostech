
<div class="getuserattendancebydate-sec">

<div class="tab-content">


    <div class="tab-pane fade in active" id="tab1">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl('attendance/attendance/addcommonattendance'),
            'method' => 'POST',
            'id' => 'getallattdform',
        ));
        ?>

        <div class="modal-body">
            <div class="row-fluid" id="selectdateforattd">
                Select Date <input type="text" id="selectattendancebydate" readonly="true" value="<?= date('Y-m-d')?>" >
                <div class="pull-right">
                    <?php echo CHtml::submitButton('Apply', array('buttonType' => 'submit', 'name' => 'apply', 'id' => 'getallattd', 'class' => 'btn btn-primary margin-bottom-0')); ?>
                </div>
                   </div>

            <div class="alert alert-success alert-dismissable display-none" id="successmsg">
                <strong>Data updated successfully. </strong>
            </div>

            <div class="row-fluid attentrytab" >
                <table class="table table-striped" data-rowlink="a">
                    <thead>
                        <tr>
                            <th>Employee name</th>
                            <th><input type="checkbox" value="0" id="emptycell" class="fast_entry"></th>
                            <?php foreach ($legends as $legend) { ?>
                                <th title="<?php echo $legend['description']; ?>"><?php echo $legend['short_note']; ?><br> <input type="checkbox" value="<?php echo $legend['short_note']; ?>" id="attent_<?php echo $legend['short_note']; ?>" class="fast_entry"></th>
                            <?php } ?>


                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $count = 1;
                        foreach ($user as $usereach) {
                            ?>
                            <tr>
                                <td><?php echo $usereach['first_name'].' '.$usereach['last_name']; ?></td>
                                <td><input type="radio" name="attent_<?php echo $count; ?>" value="<?php echo $usereach['emp_id']; ?>_0" class="emptycell" ></td>
                                <?php
                                foreach ($legends as $legend) {
                                    ?>
                                    <td><input type="radio" name="attent_<?php echo $count; ?>" value="<?php echo $usereach['emp_id']; ?>_<?php echo $legend['leg_id']; ?>" class="attent_<?php echo $legend['short_note']; ?>"
                                        <?php
                                        foreach ($attendance as $attd) {
                                            if ((($attd['user_id']) == ($usereach['emp_id'])) && ($attd['att_entry'] == $legend['leg_id'])) {
                                                echo 'checked="checked"';
                                            }
                                        }
                                        ?>
                                               ></td>
                                    <?php } ?>
                            </tr>
                            <?php
                            $count++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div><!--/.tab-pane -->

    <div class="tab-pane fade" id="tab2">
        <div class="modal-body">
            <div class="pull-right">
                <?php echo CHtml::submitButton('Apply', array('buttonType' => 'submit', 'name' => 'applycolor', 'id' => 'getallattd', 'class' => 'btn btn-primary')); ?>
            </div>
            <div class="alert alert-success alert-dismissable display-none" id="successmsg2">
                <strong>Data updated successfully. </strong>
            </div>
            <div class="row-fluid">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'action' => Yii::app()->createUrl('attendance/attendance/addlegendcolor'),
                    'method' => 'POST',
                    'id' => 'setcolorsettings',
                ));
                ?>
                <div class="span12">

                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th colspan="2" class="days-period"> Legend</th>
                                <th class="days-period">Choose color</th>
                                <th class="days-period">Choose text-color</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $legends = Legends::model()->findAll();
                            foreach ($legends as $legend) {
                                ?>
                                <tr>
                                    <td class="<?php echo $legend['short_note'] ?>"><?php echo $legend['short_note']; ?></td>
                                    <td ><?php echo $legend['description']; ?></td>
                                    <td> <div class="input-append color cp_modal cp_modalclick" data-color="<?= $legend['color_code'] ?>" data-color-format="rgb" id="cp3">
                                            <input  name="legendcolor_<?= $legend['leg_id'] ?>" type="text" id="<?= $legend['leg_id'] ?>" class="colorpickertest col-md-6" value="<?= $legend['color_code'] ?>" readonly /><span class="add-on cp_modalclick1" data-legid="<?= $legend['leg_id'] ?>"><input id="leg_id_<?= $legend['leg_id'] ?>" type="hidden" data-id="<?= $legend['leg_id'] ?>" value="<?= $legend['color_code'] ?>"><i style="background-color: <?= $legend['color_code'] ?>"></i></span>

                                        </div></td>

                                     <td> <div class="input-append color cp_modal cp_modalclick" data-color="<?= $legend['text_color'] ?>" data-color-format="rgb" id="cp3">
                                            <input  name="legendtextcolor_<?= $legend['leg_id'] ?>" type="text" id="<?= $legend['leg_id'] ?>" class="colorpickertest col-md-6" value="<?= $legend['text_color'] ?>" readonly /><span class="add-on cp_modalclick1" data-legid="<?= $legend['leg_id'] ?>"><input id="leg_id_<?= $legend['leg_id'] ?>" type="hidden" data-id="<?= $legend['leg_id'] ?>" value="<?= $legend['text_color'] ?>"><i style="background-color: <?= $legend['text_color'] ?>"></i></span>

                                        </div></td>


                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php $this->endWidget(); ?>
            </div>

        </div>
    </div><!--/.tab-pane -->


     <div class="tab-pane fade" id="tab3">

       <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl('attendance/attendance/addsettings'),
            'method' => 'POST',
            'id' => 'attsettingform',
        ));
        ?>
        <div class="modal-body">
        <h3>Attendance report settings</h3>
             

            <div class="alert alert-success alert-dismissable display-none" id="successmsg3">
                <strong>Data updated successfully. </strong>
            </div>
            <div class="alert alert-danger alert-dismissable display-none" id="errormsg3">
                <strong><span></span></strong>
            </div>
            <div class="row-fluid" id="att_settingstab3">

                <div class="block">
                    <?php echo $form->labelEx($att_settings,'Day from'); ?>
                    <?php echo $form->textField($att_settings,'att_day',array('size'=>30,'id'=>'att_day')); ?>
                    <?php echo $form->error($att_settings,'att_day'); ?>
                </div>
                <div class="block">
                    <?php echo $form->labelEx($att_settings,'Till'); ?>
                    <?php
                         for($i=1; $i<13; ++$i){
                           $month_arr[$i]= Yii::app()->locale->getMonthName($i);

                           }

                    echo $form->dropDownList($att_settings, 'att_month',array('Same month','Next month'),array('id'=>'att_month'));
                    //array('empty'=>'Select')

                    ?>
                    <?php echo $form->error($att_settings,'att_month'); ?>
                </div>
                <div class="block">
                    <?php echo $form->labelEx($att_settings,'Day'); ?>
                    <?php echo $form->textField($att_settings,'till_day',array('size'=>30,'id'=>'till_day')); ?>
                    <?php echo $form->error($att_settings,'till_day'); ?>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <?php echo CHtml::submitButton('Apply', array('buttonType' => 'submit', 'name' => 'apply1', 'id' => 'attsetting', 'class' => 'btn')); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div><!--/.tab-pane -->


                        </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


</div><!--/.tab-content -->
<script type="text/javascript">
    $(document).ready(function () {

        //* colorpicker
        gebo_colorpicker = {
            init: function () {
                //$('.cp_modal').colorpicker();

            }
        };


        //* colorpicker
        gebo_colorpicker.init();


        $(document).on("click", ".cp_modalclick", function () {

            var bgcolor = $(this).children('.colorpickertest').val();
            var leg_id = $(this).children('.colorpickertest').attr('id');
            setbgcolor(bgcolor,leg_id);

        });


        $(document).on("click", ".cp_modalclick1", function () {
            var bgcolor = $(this).val();
            var leg_id = $(this).attr('id');
             setbgcolor(bgcolor,leg_id);
        });

        $('.fast_entry').click(function () {
            var classtocheck = $(this).attr('id');
            if ($(this).is(":checked"))
            {
                $('.fast_entry').removeAttr('checked');
                $("." + classtocheck).attr('checked', 'checked');
                $(this).attr('checked', 'checked');
            }
        });

        $('#actiontableid :radio').click(function () {
            // alert('tester');
            $('.fast_entry').removeAttr('checked');
            var classname = $(this).attr('class');
            //alert(classname);
            var totradio = $('.' + classname).length;
            var totsel = $(':radio.' + classname + ":checked").length;
            if (totradio === totsel)
                $('#' + classname).attr('checked', 'checked');
            //                alert(totradio);
        });
    });

    $(document).on("submit", "#getallattdform", function (e) {
        var formdata = $("#getallattdform").serialize();
        var attd_date = $('#selectattendancebydate').val();
        formdata += "&attd_date=" + attd_date;
        var url = $(this).attr("action");
        if (e.handled !== true) {
            e.handled = true;
            $.ajax({
                method: "POST",
                url: url,
                data: formdata,
            }).done(function (msg) {
                reloadcalender();
                //reload();
                $("#successmsg").show();

            });
        }
        return false;

    });

    $(document).on("submit", "#setcolorsettings", function (e) {
        e.preventDefault();
        var formdata = $("#setcolorsettings").serialize();
        var url = $(this).attr("action");

        $.ajax({
            method: "POST",
            url: url,
            data: formdata,
        }).done(function (msg) {
            reloadcalender();
            $("#successmsg2").show();
        });
    });


       $(document).on("submit", "#attsettingform", function (e) {

        $('#loader1').show();

        var formdata  = $("#attsettingform").serialize();

        var att_day   = $('#att_day').val();
        var att_month = $('#att_month').val();
        var till_day = $('#till_day').val();

        var url = $(this).attr("action");
        if (e.handled !== true) {
            e.handled = true;
            $.ajax({
                method: "POST",
                url: url,
                data: {att_day:att_day, att_month:att_month ,till_day:till_day},
            }).done(function (msg) {

                $('#loader1').hide();

                if(msg!='success'){
                     $("#errormsg3").html(msg);
                    $("#errormsg3").show();
                }else{
                    $("#successmsg3").show();
                   // reloadcalender();
                    location.reload();
                }
            });
        }
        return false;

    });


    function setbgcolor(bgcolor,leg_id){
         $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/addlegendcolor') ?>",
            data: {bgcolor: bgcolor, leg_id: leg_id},
        }).done(function (msg) {
            gebo_colorpicker.init();
            reloadcalender();
        });
    }


</script>


