<div class="margin-15">
<div class="table-responsive">
  Comment : <?php echo $comment; ?>
    <table class="table table-bordered">

        <thead>
            <tr>
              <th class="black-color">Employee Name</th>
              <?php

              $all_dates = Yii::app()->user->attendancedata['day'];
              foreach ($all_dates as  $value) { ?>
                  <th class="black-color"><?php echo substr(date('D', strtotime($value)),0,2).'<br>'. date('d', strtotime($value)) ; ?></th>
              <?php } ?>
            </tr>
        </thead>
        <tbody>
          <?php foreach($model as $userid=>$data) { ?>
              <tr>
                <td class="black-color"><?php echo  isset($usermodel[$userid])?$usermodel[$userid]:""; ?></td>
                <?php foreach ($all_dates as $value) { ?>
                    <td <?php if(isset($legends[$data[$value]]['color'])){ ?>style="color:#fff;background-color:<?php echo $legends[$data[$value]]['color']; ?>"<?php } ?>><?php echo  (isset($legends[$data[$value]]['note']))?$legends[$data[$value]]['note']:""; ?></td>
                <?php } ?>
              </tr>
          <?php } ?>
        </tbody>
    </table>
  </div>

</div>
