<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tableHeadFixer.js" type="text/javascript"></script>
<div class="green-color text-align-left font-15" id="success"></div>

<?php if (in_array('/attendance/fastermanualentry', Yii::app()->session['menuauthlist'])) { ?>
    <button class="btnassign1 btn btn-primary pull-right margin-top-minus-50 margin-bottom-minus-20 manual-entry-btn">Manual Entry</button>
<?php } ?>

<div class="attendance-calendertable-sec">
<div class="col-md-12 table_hold" >

    <form name="emp_att_grid" id="emp_att_grid">
        <div id="parent" >
            <table class="table attnd_table table-bordered" id="fixTable">
                <?php
                $date1 = new DateTime($start_date);
                $date2 = new DateTime($end_date);
                $diff = $date2->diff($date1)->format("%a");
                $totdays = $diff + 1;
                ?>
                <thead>

                    <tr>
                        <!-- <th rowspan="4" colspan="2" class="">Employee Name</th> -->
                        <th rowspan="2" colspan="4" class="fixed_th">
                            <?php
                            if (Yii::app()->user->role == 1) {
                                $legends = Yii::app()->db->createCommand("SELECT * FROM `pms_legends` WHERE leg_id!=9")->queryAll();
                                $shift_teams = CHtml::listData($legends, 'leg_id', 'short_note');
                                echo CHtml::dropDownList('leg_id', array(), $shift_teams, array('empty' => 'Select'));
                                ?>
                                <input type="text" name="cmnt" class="cmnt davysgrey-color padding-5 border-radius-5" placeholder="comments"/>
                                <?php
                                echo CHtml::button('Apply', array('id' => 'btnassign', 'class' => 'soft-blue-bg border-0  padding-5 border-radius-5'));
                            }
                            ?>
                        </th>

                        <?php
                        for ($i = 0; $i < $totdays; $i++) {

                            $week_date = $this->add_date($start_date, $i);
                            $week_datenext = $this->add_date($start_date, $i + 1);
                            $week_day = explode('/', $week_date);
                            $dat_next = explode('/', $week_datenext);

                            $fromday = $this->add_date($start_date, 0);
                            if ($fromday == $week_date) {
                                $flag = 1;
                            }
                            $countmonth = 0;
                            if ($week_day[1] != $dat_next[1]) {

                                $endmonthday = $week_day[0];
                                $startmothday = explode('-', $start_date);
                                $monthdaysdiff = ($endmonthday - $dat_next[0]) + 1;
                                if ($flag == 1) {
                                    $monthdaysdiff = ($endmonthday - $startmothday[2]) + 1;
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                    $countmonth++;
                                    $flag = 0;
                                } else {
                                    $monthdayarr[$week_day[1]] = $monthdaysdiff;
                                }
                            }
                            $week_dates[] = $week_date;
                        }
                        $endmonth = explode('-', $end_date);

                        $startdatelast = $endmonth[0] . '-' . $endmonth[1] . '-01';
                        $monthdayarr[$endmonth[1]] = $this->getcountBetween2Dates($startdatelast, $end_date);
                        ?>
                        <?php
                        $monthsarr = array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
                        foreach ($monthdayarr as $month => $diffdays) {
                            if (count($monthdayarr) == 1) {
                                $diffdays = $totdays;
                            }

                            echo "<th colspan='" . $diffdays . "' class='month-period'>" . $monthsarr[$month] . "</th>";
                        }
                        ?>
                        <th colspan="8" class="days-period">Total Summary(<span class="monthtot"><?= $diffdays; ?></span>) </th>

                    </tr>
                    <tr>

                        <?php
                        $days = $this->getDatesBetween2Dates($start_date, $end_date);
                        $global_assign = '';
                        foreach ($days as $key => $value) {

                            $strtime = strtotime($value);
                            $prev_date = date('Y-m-d', strtotime('-2 day'));

                            $global_assign .= '<th class="global_row" id="i_' . $strtime . '">' . CHtml::checkBox('day[]', false, array('value' => $value, 'class' => 'dayschk',)) . '</th>' . "\n";
                            echo '<th>' . substr(date('D', strtotime($value)), 0, 2) . '<br>' . date('d', strtotime($value)) . '</th>';
                        }
                        ?>
                        <th rowspan="3">P</th>
                        <th rowspan="3">2P</th>
                        <th rowspan="3">A</th>
                        <th rowspan="3">H</th>
                        <th rowspan="3">SUN</th>
                        <th rowspan="3">Total</th>

                    </tr>

                    <tr>
                        <th class="fixed_th">

                        <th class="fixed_th">
                            <?php echo CHtml::checkBox('checkall', false, array('class' => "checkall", 'id' => 'employee')) ?>
                        </th>
                        <th class="fixed_th hdrborder">
                            Employee Name
                        </th>
                        </th>
                        <th class="fixed_th"> <?php echo CHtml::checkBox('checkall', false, array('class' => "checkall", 'id' => 'dayschk')) ?></th>
                        <?php
                        echo $global_assign;
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = 0;
                    foreach ($usermodel as $user) {
                        $count++;
                        ?>
                        <tr id="user_<?= $user['userid'] ?>">
                            <th  width="20"><?= $count ?></th>
                            <th  width="20"><?php echo CHtml::checkBox('employee[]', false, array('class' => "employee", 'value' => $user['userid'])) ?> </th>
                            <th colspan="2" width="200">
                                <div ><?php echo $user['first_name'] . ' ' . $user['last_name']; ?>

                                </div></th>
                            </div>
                            </th>
                            <?php
                            // $return_array=AttendanceController::getTodate($user['userid'],$start_date,$end_date); 
                            // $totdays=$return_array['tot_day'];
                            // $remain=$return_array['remain'];
                            for ($p = 0; $p < $totdays; $p++) {
                                $var = $week_dates[$p];
                                $datecur = str_replace('/', '-', $var);
                                $id = $user['userid'] . strtotime($datecur);
                                $return_array = AttendanceController::getTodate($user['userid'], $datecur);
                                ?>
                                <?php if ($return_array == 1) { ?>
                                    <td width="20" data-toggle="modal"  id="c_<?= $id ?>" data-backdrop="static"
                                        data-target="#entry" class="coldate" data-value="<?= $user["userid"] ?>"
                                        data-coldate="<?= strtotime($datecur) ?>">

                                    </td>
                                <?php } else {
                                    ?>
                                    <td style="background-color:#ccc"></td>
                                <?php } ?>
                            <?php } ?>

                      <!-- <td colspan="$remain"></td> -->


                            <td id="p_user_<?php echo $user['userid'] ?>"></td>
                            <td id="p2_user_<?php echo $user['userid'] ?>"></td>
                            <td id="a_user_<?php echo $user['userid'] ?>"></td>
                            <td id="h_user_<?php echo $user['userid'] ?>"></td>
                            <td id="sun_user_<?php echo $user['userid'] ?>"></td>
                            <td id="to_user_<?php echo $user['userid'] ?>" class="black-color"></td>

                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </form>
</div>
                                </div>
<?php
$legends = Legends::model()->findAll();
$collection = array();

foreach ($legends as $key => $value) {
    $collection[$value['short_note']] = $value['description'];
}
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
    var $ = jQuery;
    $(document).ready(function () {
        reload();
        $("#fixTable").tableHeadFixer({'left': 2, 'foot': false, 'head': true});
        $('input[type=text], textarea').keyup(function () {
            $(this).val($(this).val().toUpperCase());

        });
    });

    $(document).ready(function () {

        $(".table td").hover(function () {

            var item = '';
            var cl = <?php echo json_encode($collection); ?>;
            var JSONString = JSON.stringify(cl);
            var obj = $.parseJSON(JSONString);
            item = obj[$(this).text()];
            $(this).prop('title', $.trim(item));

        });

        $("#fixTable").tableHeadFixer({'left': 2, 'foot': false, 'head': true});
        //$("#fixTable").tableHeadFixer({'foot' : true, 'head' : true});
    });

    function reload() {

        var startdate = $('#start_date').val();
        var enddate = $('#end_date').val();
        // var hour_range = $('#hour_range').val();

        $.ajax({
            method: "post",
            url: "<?php echo $this->createUrl('attendance/getattendanceentries') ?>&userid=0&start=" + startdate + "&end=" + enddate,
            // data: {startdate: startdate, enddate: enddate},
        }).success(function (data) {
            $('#loader').hide();
            data = $.parseJSON(data);

            $.each(data.total, function (i, item) {

                $('#c_' + item['cid']).html(item['legend']);
                $('#c_' + item['cid']).addClass(item['class_note']);
                $('#c_' + item['cid']).addClass(item['type']);
                $('#p_' + item['eid']).html(item['present_days']);
                $('#p2_' + item['eid']).html(item['twop_days']);
                $('#a_' + item['eid']).html(item['absent_days']);
                $('#h_' + item['eid']).html(item['holi_days']);
                $('#sun_' + item['eid']).html(item['sun_days']);
                $('#to_' + item['eid']).html(item['total']);
                $('.monthtot').html(item['countday']);
                if (item['approved_photo_punch'] == 1) {
                    $('#c_' + item['cid']).addClass('MN');
                }

                if (item['total'] > item['countday']) {
                    $('#to_' + item['eid']).css('background-color', '#F08080;');
                } else {
                    $('#to_' + item['eid']).css('background-color', '#b5fbe0');

                }


            });

        });


    }

    $(document).ready(function () {

        $(".checkall").change(function () {

            var getid = $(this).attr('id');

            if (this.checked) {
                $("." + getid).each(function () {
                    this.checked = true;
                })
            } else {
                $("." + getid).each(function () {
                    this.checked = false;
                })
            }
        });

        $("#btnassign").click(function (e) {

            var att_id = $('#leg_id').val();
            var form_data = $('#emp_att_grid').serialize();
            var cmnt = $('.cmnt').val();

            if (att_id != '' && cmnt != '') {

                if (confirm('Are you sure to apply attendance?')) {
                    $('#loader').show();

                    $.ajax({
                        method: "post",
                        url: "<?php echo $this->createUrl('/attendance/attendance/Addattdirect') ?>",
                        data: {att_id: att_id, data: form_data, cmnt: cmnt},
                    }).done(function (msg) {

                        if (msg == 1) {
                            reloadcalender();
                            //reload();
                            $('#loader').hide();
                            $('#success').html('Updated Successfully');

                        } else if (msg == 2) {
                            location.reload();
                        } else {
                            $('#loader').hide();
                            alert(msg);
                        }
                    });
                }

            } else {

                alert('Please choose shift and add comment');
            }
        });


        $(".btnassign1").click(function (e) {

            var days = [];
            var employee = [];
            $.each($("input[name='day[]']:checked"), function () {
                days.push($(this).val());
            });

            $.each($("input[name='employee[]']:checked"), function () {
                employee.push($(this).val());
            });

            if (days != '' && employee != '') {
                var form_data = $('#emp_att_grid').serialize();
                $.ajax({
                    method: "post",
                    url: "<?php echo $this->createUrl('/attendance/attendance/applyattendance') ?>",
                    data: {data: form_data},
                }).done(function (msg) {
                    location.href = "<?php echo $this->createUrl('/attendance/attendance/applyallattendance') ?>";
                });

            } else {
                alert('Please choose employees and days');
            }
        });


    });



</script>

<style>
    .timesheet_nav{
        text-align: right;
    }



    <?php
    $legends = Legends::model()->findAll();
    foreach ($legends as $legend) {
        ?>
        .<?php echo $legend['class_note'] ?>{
            background-color: <?php echo $legend['color_code']; ?>;
            background-image:none;
            border-color:<?php echo $legend['color_code']; ?>;
            color: <?php echo $legend['text_color']; ?>;
        }


        .btn.<?php echo $legend['class_note'] ?>.active{
            color:#fff !important;
            border-color:#fff !important;
            font-weight:bold !important;
        }
    <?php } ?>

    .table-bordered td{
        cursor:pointer;
        font-weight:normal;
        padding: 6px;
    }
    .table-bordered th{
        font-weight:normal;
        padding: 6px;
    }

    .table-bordered th,.table_hold tbody th{z-index: 0;}
    .fixed_th{z-index: 2 !important;}

    td.MN{  background-image: linear-gradient(225deg, yellow, yellow 5px, transparent 5px, transparent);
    }
    #parent{max-height: 75vh;}
    .table_hold{max-height: initial;overflow:initial;}
    .HP{
        color:#fff;
    }


</style>
