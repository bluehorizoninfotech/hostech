<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 */
//echo "hello";
?>
<div class="popup-page-attendance-section">
<div class="alert alert-success alert-dismissable display-none" id="successmsg">
    <strong>Data updated successfully. </strong>
</div>
<div class="alert alert-success alert-dismissable display-none" id="delmsg">
    <strong>Data removed successfully. </strong>
</div>
<div class="form">

    <div class="modal-body padding class">


        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'attendance-form',
            'enableAjaxValidation' => false,
            //'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>


        <div class="row padding class">
            <div class="content-head btns_state">
                <?php echo $form->labelEx($model, 'att_entry'); ?>
                <div data-toggle="buttons-radio" class="btn-group clearfix sepH_a">

                    <?php foreach ($legends as $legend) { ?>
                    <button type="button" class="btn <?php echo $legend['class_note']; ?> <?php
                        if ($legend['leg_id'] == $model['att_entry']) {
                            echo ' active';
                        }
                        ?>" data-id="<?php echo $legend['leg_id']; ?>" title="<?php echo $legend['description']; ?>"><?php echo $legend['short_note']; ?></button>
                            <?php } ?>


                </div>
                <?php
                echo $form->hiddenField($model, 'att_entry', array('class' => 'att_entry'));
                echo $form->error($model, 'att_entry');
                ?>

            </div>
        </div>
        <div class="row">
            <div class="float-left" id="comt">
                <input type="hidden" id="userid" value="<?= $userid ?>">
                <input type="hidden" id="cell_date" value="<?= $cell_date ?>">
                <?php echo $form->labelEx($model, 'comments'); ?>
                <?php

                $coments =$model->comments;

                $model->comments='';

                // echo $form->textArea($model, 'comments', array('size' => 18, 'maxlength' => 30, 'class' => 'form-control'));
                echo $form->textField($model,'comments',array('class' => 'form-control width-300 height-50','autocomplete'=> "off"));

                ?>
                <?php echo $form->error($model, 'comments'); ?>
                <span id="errcm" class="red-color"></span>
            </div>
            <div class="float-left margin-left-10"><br clear="all"/><?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn btn-primary attsubmit')); ?></div>

<br clear="all"/>
        </div>

    </div>
    <div class="modal-footer">
      <?php  if($model->type!='1') {
           $created_employee = Users::model()->findByPk($model->created_by);

        ?>
        <h4 class="text-align-left">Manual Entry Comments</h4>
        <span class="float-left"><?php echo $coments; ?></span><br/>
          <i>
            <?php if($model->type =='2') { ?>
            By <u><?php echo $created_employee->first_name.' '.$created_employee->last_name; ?></u>
          <?php } ?>
            Updated Date:<?= date('d-m-Y H:i:s'); ?></i>
      <?php }else{  ?>
         <?php if($model->att_entry==8 && $coments!='From Punch record') { ?>
          <h4 class="text-align-left">Comments</h4>
          <span class="float-left"><?php  echo $coments; ?></span>
        <?php } ?>

      <?php } ?>
    </div>
<?php $this->endWidget(); ?>
</div>

</div>
         </div>
<script type="text/javascript">

  $(document).ready(function () {
         $('input[type=text], textarea').keyup(function () {
            $(this).val($(this).val().toUpperCase());

         });
    });
    $(document).on("click", ".attsubmit", function (e) {
        e.preventDefault();
        var formdata = $("#attendance-form").serialize();
        var userid = $("#userid").val();
        var cell_date = $("#cell_date").val();
        var comt = $('#Attendance_comments').val();

        formdata += "&userid=" + userid + "&cell_date=" + cell_date + "&comnt=" + comt;
        var url = $(this).attr("action");

        if(comt!=''){
            $.ajax({
                method: "POST",
                url: "<?php echo $this->createUrl('attendance/addattendancesubmit') ?>",
                data: formdata,
            }).done(function (msg) {
                reloadcalender();
               if(msg==2){
                   $('#successmsg').show();
               }else{
                 $('#delmsg').show();
               }

            });
        }else{
           $('#errcm').html('Please enter comment');
        }
    });

    $(document).on("click", ".btns_state .btn", function () {
        var leg_id = $(this).attr('data-id');
        $(".att_entry").val(leg_id);

    });


</script>
