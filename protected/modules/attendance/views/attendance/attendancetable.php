
<div>
<div class="attendance-table-sec">
  <h2 class="margin-20">Update Attendance</h2>
<div class="clearfix form-group">
  <?php echo CHtml::Button('Apply', array('class'=>'btn btn-primary applybtn pull-right') ); ?>
  <textarea class="commt margin-0 width-304 height-46 margin-left-11" placeholder="Comment"></textarea>
  <?php echo CHtml::link('Back',array('/attendance/attendance/index'), array('class'=>'btn btn-primary pull-left')); ?>
</div>
<form name="user_att_grid" id="user_att_grid">

  <div class="table-responsive">
    <table class="table table-bordered mytable">

        <thead>
            <tr>
                <th>Employee Name</th>
                <?php
                $start_date = $end_date =  date('Y-m-d');
                $count = 0;
                if(isset(Yii::app()->user->attendancedata['day'])){

                  $count      = count(Yii::app()->user->attendancedata['day']);
                  $start_date = isset(Yii::app()->user->attendancedata['day'][0])?Yii::app()->user->attendancedata['day'][0]:date('Y-m-d');

                    if($count>0){
                       $ct= $count-1;
                       $end_date   =  Yii::app()->user->attendancedata['day'][$ct];
                    }

                }

                $all_dates = Yii::app()->user->attendancedata['day'];

                foreach ($all_dates as $value) { ?>
                    <th><?php echo substr(date('D', strtotime($value)),0,2).'<br>'. date('d', strtotime($value)) ; ?></th>
                <?php }  ?>
            </tr>
        </thead>

        <tbody>
          <?php

          $employee = Yii::app()->user->attendancedata['employee'];
          foreach($employee as $userid){

          ?>
              <tr>
                  <td class="black-color !important"><?php echo  isset($usermodel[$userid])?$usermodel[$userid]:""; ?></td>

                  <?php  foreach ($all_dates as $value) {  ?>
                      <td><div><?php echo CHtml::dropDownList('leg_id['.$userid.']['.$value.']', array(), $leg_items, array('empty' => 'Select','class'=>'multisel')); ?></div></td>
                  <?php } ?>

              </tr>
          <?php } ?>
        </tbody>
    </table>
  </div>

  </form>

</div>

<div id="myModal" class="modal" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Changes</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btnsub">Apply</button>
      </div>
    </div>

  </div>
</div>
                  </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

  $(document).ready(function () {
       $('input[type=text], textarea').keyup(function () {
          $(this).val($(this).val().toUpperCase());

       });

  });

  $(".mytable tr td div").bind('keydown', function(event) {
        if(event.keyCode == 9){
            var currentDiv = event.target;
            $(currentDiv).parents("td").find("div").find("select").removeClass('active');
            if($(currentDiv).parents("td").next("td").find("div").length > 0){
               $(currentDiv).parents("td").next("td").find("div").find("select").addClass('active');
           }else{
              $(currentDiv).parents("tr").next("tr").find("td:eq( 1 )").find('div').find("select").addClass('active');
          }

            return true; 
        }
});

$( ".applybtn" ).click(function(e){

     var form_data  = $( '#user_att_grid').serialize();
     var start_date = '<?php echo $start_date; ?>';
     var end_date   = '<?php echo $end_date; ?>';
     var coment     = $('.commt').val();


      var entries = [];
      $.each($("#user_att_grid option:selected"), function(){

        if($(this).val()!=""){
          entries.push( $(this).val() );
        }

      });
      if(entries.length !=0){
           $.ajax({
                method: "post",
                url:  "<?php echo $this->createUrl('/attendance/attendance/testattendance') ?>",
                data: {  data:form_data, start_date:start_date, end_date:end_date, coment:coment },
            }).done(function (msg) {

              $('#myModal').modal();

              $('.modal-body').html(msg);

            });
    }else{

       alert('Please choose atleast one entry');

    }

  });

  $(document).on("click",".btnsub",function(e) {

    var comment     = $('.commt').val();

    location.href = "<?php echo $this->createUrl('/attendance/attendance/updateattendance&comt=') ?>"+comment;


  });

</script>
