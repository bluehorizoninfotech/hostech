<?php

class MenuPermissionsController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $accessauthArr;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = Yii::app()->controller->id;
        $module = Yii::app()->controller->module->id;
        $modulecontroller = $module . "/" . $controller;

        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($modulecontroller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$modulecontroller];
            }
        }


        $access_privlg = count($accessauthArr);

        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' 
                'actions' => array('create', 'admin', 'delete', 'index', 'view', 'update', 'savepermissions', 'getpermissions', 'getusers', 'searchuser'),
                'users' => array('@'),
                'expression' => 'isset(Yii::app()->user->role) && Yii::app()->user->role==1',
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */

    public function actionCreate()
    {
        $tbl = Yii::app()->db->tablePrefix;
        $model = new MenuPermissions;
        $users = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users where status=0 ORDER BY first_name,last_name ASC")->queryAll();
        $menus = Yii::app()->db->createCommand("SELECT * FROM {$tbl}menu where status = 0 and show_list = 1 ORDER BY menu_name ASC")->queryAll();


        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MenuPermissions'])) {
            $model->attributes = $_POST['MenuPermissions'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->mp_id));
        }
        $criteria = new CDbCriteria;
        $criteria->alias = 'me';

        $criteria->select = array("{$tbl}menu.*");
        $criteria->compare('me.show_list', 1);


        $criteria->with = array("parent" => array("select" => "parent.*"));
        $criteria->distinct = true;
        $criteria->compare('parent.parent_id', 0);
        $criteria->compare('parent.status', 0);
        $criteria->compare('parent.show_list', 1);



        $dataProvider = new CActiveDataProvider(
            'Menu',
            array(
                'criteria' => $criteria,
                //                            array(
                //                                //'select' => '*',
                //                                //'join' => "LEFT JOIN {$tbl}menu parent on parent.parent_id = me.menu_id",
                //                                'condition'=>'show_list, 1',
                //                                //'with'=>array('parent'),
                //                            ),
                'sort' => array(

                    'defaultOrder' => 'parent.menu_name DESC',

                ),
                'pagination' => FALSE
            )
        );
        $dataProvider->setPagination(false);


        $this->render('create', array(
            'model' => $model, 'menus' => $menus, 'users' => $users, 'dataProvider' => $dataProvider
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MenuPermissions'])) {
            $model->attributes = $_POST['MenuPermissions'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->mp_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MenuPermissions');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new MenuPermissions('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MenuPermissions']))
            $model->attributes = $_GET['MenuPermissions'];



        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = MenuPermissions::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'menu-permissions-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetusers()
    {
        $tbl = Yii::app()->db->tablePrefix;

        if (isset($_POST['type'])) {
            if ($_POST['type'] == "individual") {

                $users = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users where status=0")->queryAll();

                echo "<ul class='users-listview'>";

                foreach ($users as $user) {
                    echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><a >" . $user['first_name'] . " " . $user['last_name'] . "</a></li>";
                }

                echo "</ul>";
                //print_r($users);
            } else if ($_POST['type'] == "department") {

                $sql = "SELECT * FROM {$tbl}department";
                $departments = Yii::app()->db->createCommand($sql)->queryAll();
                echo "<ul class='users-listview'>";

                foreach ($departments as $department) {
                    echo "<li class='departmentid' name='userid' data-id=" . $department['dept_id'] . "><a>" . $department['dept_name'] . "</a></li>";
                }

                echo "</ul>";
            } else if ($_POST['type'] == "profile") {

                $sql = "SELECT * FROM {$tbl}user_roles where id in ( SELECT user_type FROM {$tbl}users where status=0)";
                $profiles = Yii::app()->db->createCommand($sql)->queryAll();

                echo "<ul class='users-listview'>";

                foreach ($profiles as $profile) {
                    echo "<li class='profileid' name='userid' data-id=" . $profile['id'] . "><a>" . $profile['role'] . "</a></li>";
                }

                echo "</ul>";
            } else {
            }
        }
    }

    public function actionSavepermissions()
    {

        $tbl = Yii::app()->db->tablePrefix;

        if (isset($_POST['menus'])) {
            $menus = $_POST['menus'];
        } else {
            $menus = [];
        }

        /*if (isset($_POST['departmentids'])){
            $departmentid = $_POST['departmentids'];
        

            //with departmentid
            if (!empty($departmentid)  ) {
               
                //make an array of users under the department
                $userArr = array();
                $users = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users where user_type = '$departmentid'")->queryAll();
                foreach($users as $user ){
                    array_push($userArr, $user['userid']);
                }

                foreach ($userArr as $userid) {
                    //get existing permissions
                    $existquery = Yii::app()->db->createCommand("SELECT * FROM {$tbl}menu_permissions WHERE user_id = '$userid'")->queryAll();
                    $existmenus = array();
                    foreach ($existquery as $ex) {
                        array_push($existmenus, $ex['menu_id']);
                    }
                    
                    //Delete permissions not in post data
                    $result = array_diff($existmenus, $menus);
                    foreach ($result as $res) {
                        Yii::app()->db->createCommand()->delete("{$tbl}menu_permissions", "menu_id = :menu_id AND user_id = :user_id", array(':menu_id' => $res, ':user_id' => $userid)
                        );
                    }
                    
                    //save new permissions
                    
                        foreach ($menus as $menu) {
                            if (!in_array($menu, $existmenus)) {                       
                                $model = new MenuPermissions();
                                $model->user_id = $userid;
                                $model->menu_id = $menu; 
                                $model->save(); 
                            }
                        }
                    
                }

            }
        }*/

        //with userid
        if (isset($_POST['userids'])) {
            $userid = $_POST['userids'];
            if (!empty($userid)) {
                //get existing permissions
                $existquery = Yii::app()->db->createCommand("SELECT * FROM {$tbl}menu_permissions WHERE user_id = '$userid'")->queryAll();
                $existmenus = array();
                foreach ($existquery as $ex) {
                    array_push($existmenus, $ex['menu_id']);
                }

                //Delete permissions not in post data
                $result = array_diff($existmenus, $menus);
                foreach ($result as $res) {
                    Yii::app()->db->createCommand()->delete(
                        "{$tbl}menu_permissions",
                        "menu_id = :menu_id AND user_id = :user_id",
                        array(':menu_id' => $res, ':user_id' => $userid)
                    );
                }

                //save new permissions
                foreach ($menus as $menu) {
                    if (!in_array($menu, $existmenus)) {
                        $model = new MenuPermissions();
                        $model->user_id = $userid;
                        $model->menu_id = $menu;
                        $model->save();
                    }
                }
            }
        }


        //with profile id
        if (isset($_POST['profileids'])) {
            $profileid = $_POST['profileids'];
            if (!empty($profileid)) {

                // Modified on 04-10-2016
                Yii::app()->db->createCommand()->delete(
                    "{$tbl}profile_menu_settings",
                    "role_id = :role_id",
                    array(':role_id' => $profileid)
                );
                if (!empty($menus)) {
                    foreach ($menus as $menu) {
                        $profilemodel = new ProfileMenuSettings();
                        $profilemodel->role_id = $profileid;
                        $profilemodel->menu_id = $menu;
                        $profilemodel->save();
                    }
                }
                // Modified on 04-10-2016



                $profiles = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users where user_type = '$profileid'")->queryAll();
                foreach ($profiles as $profile) {
                    //create user array under selected profile id
                    $userArr = array();
                    $users = $profile['userid'];
                    array_push($userArr, $users);


                    foreach ($userArr as $userid) {
                        //get existing permissions
                        $existquery = Yii::app()->db->createCommand("SELECT * FROM {$tbl}menu_permissions WHERE user_id = '$userid'")->queryAll();
                        $existmenus = array();
                        foreach ($existquery as $ex) {
                            array_push($existmenus, $ex['menu_id']);
                        }

                        //Delete permissions not in post data
                        $result = array_diff($existmenus, $menus);
                        foreach ($result as $res) {
                            Yii::app()->db->createCommand()->delete(
                                "{$tbl}menu_permissions",
                                "menu_id = :menu_id AND user_id = :user_id",
                                array(':menu_id' => $res, ':user_id' => $userid)
                            );
                        }

                        //save new permissions
                        foreach ($menus as $menu) {
                            if (!in_array($menu, $existmenus)) {

                                $model = new MenuPermissions();
                                $model->user_id = $userid;
                                $model->menu_id = $menu;
                                $model->save();
                            }
                        }
                    }
                }
            }
        }
        $process = Yii::app()->createController('Site'); //create instance of SiteController
        $process = $process[0];
        $process->menupermissions(); //call function 
    }

    public function actionGetpermissions()
    {
        $tbl = Yii::app()->db->tablePrefix;

        $type = $_POST['type'];
        $id = $_POST['id'];

        if ($type == 'individual') {

            $perms = Yii::app()->db->createCommand("SELECT pe.*,me.parent_id FROM {$tbl}menu_permissions pe LEFT JOIN {$tbl}menu me ON pe.menu_id = me.menu_id WHERE pe.user_id = '$id'")->queryAll();
            echo json_encode(array('data' => $perms));
        } /*else if ($type == 'department') {
            $permissions = array();
            $parents = array();

            $users = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users where dept_id = '$id'")->queryAll();

            foreach ($users as $user) {
                $us = $user['userid'];
                $perms = Yii::app()->db->createCommand("SELECT pe.*,me.parent_id FROM {$tbl}menu_permissions pe LEFT JOIN {$tbl}menu me ON pe.menu_id = me.menu_id WHERE pe.user_id = '$us'")->queryAll();
                foreach ($perms as $perm) {
                    array_push($permissions, $perm['menu_id']);
                    array_push($parents, $perm['parent_id']);
                }
            }
            echo json_encode(array('data' => $permissions, 'parents' => $parents));
        }*/ else if ($type == 'profile') {
            $permissions = array();
            $parents = array();

            $perms = Yii::app()->db->createCommand("SELECT pe.*,me.parent_id FROM {$tbl}profile_menu_settings pe LEFT JOIN {$tbl}menu me ON pe.menu_id = me.menu_id WHERE pe.role_id = '$id'")->queryAll();
            foreach ($perms as $perm) {
                array_push($permissions, $perm['menu_id']);
                array_push($parents, $perm['parent_id']);
            }

            echo json_encode(array('data' => $permissions, 'parents' => $parents));
        }
    }
    public function actionsearchuser()
    {
        $tbl = Yii::app()->db->tablePrefix;
        if (isset($_POST['text'])) {
            $users = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users
            WHERE `first_name` LIKE '%{$_POST['text']}%'
            OR `last_name` LIKE '%{$_POST['text']}%'
            OR CONCAT(first_name, ' ',last_name ) LIKE  '%{$_POST['text']}%'")->queryAll();
            echo "<ul class='users-listview'>";

            foreach ($users as $user) {
                echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><a >" . $user['first_name'] . " " . $user['last_name'] . "</a></li>";
            }

            echo "</ul>";
        }
    }
}
