<?php
/* @var $this MenuController */
/* @var $model Menu */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	$model->menu_id=>array('view','id'=>$model->menu_id),
	'Update',
);

$this->menu=array(
	//array('label'=>'List Menu', 'url'=>array('index')),
	//array('label'=>'Create Menu', 'url'=>array('create')),
	//array('label'=>'View Menu', 'url'=>array('view', 'id'=>$model->menu_id)),
	array('label'=>'Manage Menu', 'url'=>array('admin')),
);
?>

<!--<h1>Update Menu : <?php echo $model->menu_name; ?></h1>-->
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title">Update Menu : <?php echo $model->menu_name; ?></h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>