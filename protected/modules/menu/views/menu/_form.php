<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>
<div class="modal-body">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'menu-form',
	'enableAjaxValidation'=>false,
        /*'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),*/
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <div class="col-md-6">
		<?php echo $form->labelEx($model,'menu_name'); ?>
		<?php echo $form->textField($model,'menu_name',array('size'=>20,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'menu_name'); ?>
            </div>
            <div class="col-md-6">
                    <?php echo $form->labelEx($model,'parent_id'); ?>
                    <?php echo CHtml::activeDropDownList($model, 'parent_id', Chtml::listData(Menu::model()->findAllByAttributes(array('parent_id'=>"")), 'menu_id', 'menu_name'),
                            array('empty'=>'Select Parent Menu Item')) ?>
                    <?php echo $form->error($model,'parent_id'); ?>
            </div>
        </div>

	<div class="row  radio_btn">
            <div class="col-md-12">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->radioButtonList($model, 'status', array('Active', 'Inactive'), array('separator' => '')); ?>
            <?php echo $form->error($model,'status'); ?>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-6">
		<?php echo $form->labelEx($model,'controller'); ?>
		<?php echo $form->textField($model,'controller',array('size'=>20,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'controller'); ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->labelEx($model,'action'); ?>
                <?php echo $form->textField($model,'action',array('size'=>20,'maxlength'=>30)); ?>
                <?php echo $form->error($model,'action'); ?>
            </div>
        </div>
	<div class="row">
             <div class="col-md-6">
		<?php echo $form->labelEx($model,'params'); ?>
		<?php echo $form->textField($model,'params',array('size'=>20,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'params'); ?>
            </div>

            <div class="col-md-6">
		<?php echo $form->labelEx($model,'showmenu'); ?>
		<?php echo $form->dropDownList($model,'showmenu',array("0"=>"All Users","1"=>"Guest Users", "2"=>"Authenticated Users"),array('empty'=>'Select Value')); ?>
		<?php echo $form->error($model,'showmenu'); ?>
            </div>
	</div>
        
        <div class="row">
            <div class="col-md-12">
                <?php echo $form->labelEx($model,'show_list'); ?>
                <?php echo $form->checkBox($model,'show_list'); ?>
                <?php echo $form->error($model,'show_list'); ?>
            </div>
        </div>
        
        <br>
	<div class="row buttons text-center">
            <div class="col-md-12">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn orange')); ?>
                <input type="reset" value="Reset" class="btn default">
            </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div style="color:red">
    <h4><?php echo Yii::app()->user->getFlash('error'); ?></h4>
</div>
</div>
<style>
div.form .radio_btn label {
  display: inline-block;
  margin-right: 15px;
}
div.form{
      max-width: 500px;
}

</style>