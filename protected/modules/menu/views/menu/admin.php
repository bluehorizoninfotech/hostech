<?php
/* @var $this MenuController */
/* @var $model Menu */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List Menu', 'url'=>array('index')),
	array('label'=>'Create Menu', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Manage Menus</h2>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
/*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
                //array('name'=>'menu_id', 'htmlOptions' => array('width' => '40px','style'=>'font-weight: bold;text-align:center')),
		//'menu_id',
		'menu_name',
                array(
                    'name' => 'parent_menu',
                    'value' => function (Menu $data){
                                    if($data->parent_id)
                                        return $data->parent->menu_name; // "parent" - relation name, defined in "relations" method 
                                    return "";
                                },
                    
                ),
                //'parent.menu_name',

		'controller',
		'action',
                array(
                    'name' => 'status',
                    'value' => function (Menu $data){
                                    if($data->status == 0)
                                        return "Active"; // "parent" - relation name, defined in "relations" method 
                                    return "Inactive";
                                }   
                ),
                        
                array(
                    'name' => 'showmenu',
                    'value' => function (Menu $data){
                                    if($data->showmenu == 0)
                                        return "All Users";
                                    else if($data->showmenu == 1)
                                        return "Guest Users";
                                    else if($data->showmenu == 2)
                                        return "Authenticated Users";
                                    else
                                        return "";
                                }   
                ),
		
		//'params',
		//'showmenu',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
));*/ ?>




<?php
/*
$this->widget('ext.groupgridview.GroupGridView', array(
      'id' => 'grid1',
      'dataProvider' => $dataProvider,
      'mergeColumns' => array('parent_menu','controller'),
      'columns' => array(
            
            array(
                'name' => 'parent_menu',
                'value' => function (Menu $data){
                                if($data->parent_id)
                                    return $data->parent->menu_name; // "parent" - relation name, defined in "relations" method 
                                return "";
                            },

            ),
            'menu_name',
            'controller',
            'action',
            array(
                'name' => 'status',
                'value' => function (Menu $data){
                                if($data->status == 0)
                                    return "Active"; // "parent" - relation name, defined in "relations" method 
                                return "Inactive";
                            }   
            ),
                        
            array(
                'name' => 'showmenu',
                'value' => function (Menu $data){
                                if($data->showmenu == 0)
                                    return "All Users";
                                else if($data->showmenu == 1)
                                    return "Guest Users";
                                else if($data->showmenu == 2)
                                    return "Authenticated Users";
                                else
                                    return "";
                            }   
            ),
        //array('class' => 'CLinkColumn'),
        array('class' => 'CButtonColumn'),        
      ),
    ));*/
?>
<div class="table-responsive">
<?php
 $this->widget('ext.groupgridview.GroupGridView', array(
        'id' => 'grid1',
        'itemsCssClass' =>'greytable',
        'dataProvider' => $dataProvider,
        'extraRowColumns' => array(
            'parent_menu'

        ),
        'extraRowExpression' => function (Menu $data){
                                if($data->parent_id){
                                     if(isset($data->parent->menu_name)){
                                        return "<b style=\"font-size: 16px; \">".$data->parent->menu_name."</b>"; // "parent" - relation name, defined in "relations" method 
                                    }else{
                                        return "";
                                    }
                                    
                                }
                                return "<b style=\"font-size: 16px; \">Main Menu Items</b>";
                            },
      'extraRowPos' => 'above',
      'columns' => array(
            array(
                'name' => 'parent_menu',
                'value' => function (Menu $data){
                                if($data->parent_id){
                                    if(isset($data->parent->menu_name)){
                                      return $data->parent->menu_name; // "parent" - relation name, defined in "relations" method 
                                    }else {
                                       return "";
                                  }
                                    
                                }
                                return "";
                            },

            ),
            'menu_name',
            //'controller',
            'action',
            array(
                'name' => 'status',
                'value' => function (Menu $data){
                                if($data->status == 0)
                                    return "Active"; // "parent" - relation name, defined in "relations" method 
                                return "Inactive";
                            }   
            ),
                        
            array(
                'name' => 'showmenu',
                'value' => function (Menu $data){
                                if($data->showmenu == 0)
                                    return "All Users";
                                else if($data->showmenu == 1)
                                    return "Guest Users";
                                else if($data->showmenu == 2)
                                    return "Authenticated Users";
                                else
                                    return "";
                            }   
            ),
            array(
                'name' => 'show_list',
                'value' => function (Menu $data){
                                if($data->show_list == 0)
                                    return "Inactive in list";
                                else if($data->show_list == 1)
                                    return "Active in list";
                                else
                                    return "";
                            }   
            ),
                    
       // array('class' => 'CLinkColumn'),
        array('class' => 'CButtonColumn', 'template' => '{update} {delete}',
            'buttons'=>array(  
                            'update' => array(
                                    'label'=>'',
                                    'imageUrl' =>false,
                                    'url' => 'Yii::app()->createUrl("menu/menu/update", array("enblayout"=>0,"id"=>$data->menu_id))',                                                
                                    'options' => array('class' => 'menu-update icon-pencil icon-comn','title'=>'Edit','data-toggle'=>'modal', 'data-target'=>'#myModal'),
                            ),
                            'delete' => array(
                                    'label'=>'',
                                    'imageUrl' =>false,
                                    'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete'),
                            ),
					
			), 
            ),         
      ),
    ));

?>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content"></div>
    </div>
</div>

<?php
    Yii::app()->clientScript->registerScript('Show all fields', "
        
	$(document).delegate('.menu-update', 'click', function(event) {
		event.preventDefault();
		var url = $(this).attr('href');
		$('.modal').show();
		$('.modal-content').html('Loading.....');
		$.ajax({
                    type: 'GET',
                    url: url,
                    success: function (response)
                    {
                        $('.modal-content').html(response);   
                    }
                });
            });
            
        
");
    ?>
    