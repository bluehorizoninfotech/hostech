<?php
/* @var $this MenuPermissionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Menu Permissions',
);

$this->menu=array(
	array('label'=>'Create MenuPermissions', 'url'=>array('create')),
	array('label'=>'Manage MenuPermissions', 'url'=>array('admin')),
);
?>

<h1>Menu Permissions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
