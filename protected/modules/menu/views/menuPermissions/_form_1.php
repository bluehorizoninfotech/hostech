<?php
/* @var $this MenuPermissionsController */
/* @var $model MenuPermissions */
/* @var $form CActiveForm */


$url =  Yii::app()->createAbsoluteUrl("menu/menuPermissions/getusers");
$saveurl =  Yii::app()->createAbsoluteUrl("menu/menuPermissions/savepermissions");
$geturl =  Yii::app()->createAbsoluteUrl("menu/menuPermissions/getpermissions");

Yii::app()->clientScript->registerScript('myjquery', "
    


    $('input[type=radio][name=type]').change(function() {
        var type = $(this).val();
        $.ajax({
            type: 'POST',
            url: '".$url."',
            data:{type:type},
            success:function(data){
                $('#userlist').html(data);

            },
            error: function(data) { // if error occured
                  alert('Error occured.please try again');
                  
            },


        });

      
    });



    $('input[name^=\"level\"]').click(function () {
        $(this).parent().find('input[name^=level]').prop('checked', this.checked);

        if (!this.checked) {
            var level = this.name.substring(this.name.length - 1);
            for (var i = level - 1; i > 0; i--) {
                //$('input[name=\"level-' + i + '\"]').prop('checked', false);
            }
        }
    });
    
    $('#savebtn').click(function(){
    
        var type = $('input[name=type]:checked').val();
        var userids = [];
        var groupids = [];
        var menus = [];
        if(type == 'individual'){

            $('.userid:checked').each(function() {
                userids.push(this.value);
            });
            
            
            $('.menuid:checked').each(function() {
                menus.push(this.value);
            });
            
            if((userids.length !=0)){
                $.ajax({
                    type: 'POST',
                    url: '".$saveurl."',
                    data:{type: type, userids:userids, menus: menus},
                    success:function(data){
                        alert('success');
                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },


                });


            }

        }
        else if(type == 'group'){
            
            $('.groupid:checked').each(function() {
                groupids.push(this.value);
            });
            
            $('.menuid:checked').each(function() {
                menus.push(this.value);
            });
            

            if((groupids.length !=0)){
                $.ajax({
                    type: 'POST',
                    url: '".$saveurl."',
                    data:{type: type, groupids:groupids, menus: menus},
                    success:function(data){
                        alert('success');

                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },


                });


            }

        }
        else{
        
        }

    });

    $(document).on('change', '.userid:checkbox',function(){
            
            //$('.userid').not(this).prop('checked', false);  
            $('.menuid').prop('checked', false);
            if(($('input.userid:checked').length) == 1){
                var id = $('input.userid:checked').val();
                var type = 'individual';
                
                $.ajax({
                    type: 'POST',
                    url: '".$geturl."',
                    data:{type: type, id: id},
                    dataType: 'json',
                    success:function(response){  
                        for (var i=0; i<response.data.length; i++) {
                            var val = response.data[i].menu_id;
                            $('.menuid[value=\"' + val + '\"]').prop('checked', true); 
                       }
                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },

                });
            
        }
        
    });
    
    $(document).on('change', '.groupid:checkbox',function(){
       
       // $('.groupid').not(this).prop('checked', false);  
        $('.menuid').prop('checked', false);
        if(($('input.groupid:checked').length) == 1){
            var id = $('input.groupid:checked').val();
            var type = 'group';
            $.ajax({
                type: 'POST',
                url: '".$geturl."',
                data:{type: type, id: id},
                dataType: 'json',
                success:function(response){ 
                    console.log(response);
                    for (var i=0; i<response.data.length; i++) {
                        var val = response.data[i];
                        $('.menuid[value=\"' + val + '\"]').prop('checked', true); 
                    }
                },
                error: function(data) { // if error occured
                      alert('Error occured.please try again');

                },

            });
        }
        
    });


");



?>

<!--<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'menu-permissions-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
                <?php echo CHtml::activeDropDownList($model, 'user_id', Chtml::listData(Users::model()->findAll(), 'userid', 'first_name'),
        array('empty'=>'Select User')) ?>
		
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'menu_id'); ?>
                <?php echo CHtml::activeDropDownList($model, 'menu_id', Chtml::listData(Menu::model()->findAll(), 'menu_id', 'menu_name'),
        array('empty'=>'Select Menu Item')) ?>
		
		<?php echo $form->error($model,'menu_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
 </div> form        -->

<div style="border: 1px solid #dedede; display: inline-block; min-height: 300px; height: auto; padding: 20px;">
    <div class="" style="float: left; width: 250px; border-right: 1px solid #dfdfdf; margin-right: 20px;">
        <h3>Users List</h3>
        <div>
            <input type="radio" name="type" value="individual" checked="checked">Individual
            <input type="radio" name="type" value="group">Group

        </div>
        <div id="userlist">
        <?php    
        echo "<ul class='users-listview'>";

        foreach ($users as $user){
            
            echo "<li><input type='checkbox' class='userid' name='userid' value=".$user['userid'].">".$user['first_name']."</li>"; 
        }

        echo "</ul>";
        ?>   
        </div>




    </div>
 
    


    <?php
 $this->widget('ext.groupgridview.GroupGridView', array(
      'id' => 'grid1',
      'dataProvider' => $dataProvider,
      'extraRowColumns' => array(
          'controller'
         
          
        ),
      'extraRowPos' => 'above',
      'columns' => array(
            array(
                'name' => 'parent_menu',
                'value' => function (Menu $data){
                                if($data->parent_id)
                                    return $data->parent->menu_name; // "parent" - relation name, defined in "relations" method 
                                return "";
                            },

            ),
            'menu_name',
            //'controller',
            'action',
            array(
                'name' => 'status',
                'value' => function (Menu $data){
                                if($data->status == 0)
                                    return "Active"; // "parent" - relation name, defined in "relations" method 
                                return "Inactive";
                            }   
            ),
                        
            array(
                'name' => 'showmenu',
                'value' => function (Menu $data){
                                if($data->showmenu == 0)
                                    return "All Users";
                                else if($data->showmenu == 1)
                                    return "Guest Users";
                                else if($data->showmenu == 2)
                                    return "Authenticated Users";
                                else
                                    return "";
                            }   
            ),
       // array('class' => 'CLinkColumn'),
        array('class' => 'CButtonColumn'),         
      ),
    ));

?>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


    <div class="" style="float:left;">
        <h3>Menu Items</h3>
        <ul class="permissions">
        <?php 
            $inputlevel1 = array();
            $inputlevel2 = array();
            $mainmenu = array();
            foreach($menus as $menu){ ?>

            <?php if($menu['parent_id'] == 0){ ?>
            <li>
                <input type="checkbox" class="menuid" name="level-1" value="<?php echo $menu['menu_id'] ?>"><?php echo $menu['menu_name']; ?></input>

                <?php 
                $menuid =$menu["menu_id"];
                $subitems = Yii::app()->db->createCommand("SELECT * FROM pms_menu where parent_id = $menuid")->queryAll();
                if(!empty($subitems)){
                    echo "<ul>";
                    foreach($subitems as $sub){
                ?>        

                    <li><input type="checkbox" class="menuid" name="level-2" value="<?php echo $sub['menu_id'] ?>"><?php echo $sub['menu_name']; ?></input></li>

                <?php       
                    }
                    echo "</ul>";

                }
                ?>

            </li>
            <?php }} ?>



            <!--    <li>
                    <input type="checkbox" name="level-1">Level 1</input>
                    <ul>
                        <li>
                            <input type="checkbox" name="level-2">Level 2</input>
                        </li>

                        <li>
                            <input type="checkbox" name="level-2">Level 2</input>
                        </li>
                        <li>
                            <input type="checkbox" name="level-2">Level 2</input>
                        </li>
                        <li>
                            <input type="checkbox" name="level-2">Level 2</input>
                        </li>
                        <li>
                            <input type="checkbox" name="level-2">Level 2</input>
                        </li>

                    </ul>
                </li>-->
            </ul> 
        
        
    </div> 
    <div style="clear: both"></div>    
    <button id="savebtn" style="float: right">Save</button> 
</div>       
      
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

