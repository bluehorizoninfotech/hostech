<?php
/* @var $this MenuPermissionsController */
/* @var $model MenuPermissions */

$this->breadcrumbs=array(
	'Menu Permissions'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Menu Permissions', 'url'=>array('index')),
	//array('label'=>'Manage Menu Permissions', 'url'=>array('admin')),
);
?>

<h2>Create Menu Permissions</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'menus' => $menus, 'users' => $users ,'dataProvider' => $dataProvider)); ?>
