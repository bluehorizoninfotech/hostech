<?php
/* @var $this MenuPermissionsController */
/* @var $model MenuPermissions */

$this->breadcrumbs=array(
	'Menu Permissions'=>array('index'),
	$model->mp_id=>array('view','id'=>$model->mp_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MenuPermissions', 'url'=>array('index')),
	array('label'=>'Create MenuPermissions', 'url'=>array('create')),
	array('label'=>'View MenuPermissions', 'url'=>array('view', 'id'=>$model->mp_id)),
	array('label'=>'Manage MenuPermissions', 'url'=>array('admin')),
);
?>

<h1>Update MenuPermissions <?php echo $model->mp_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>