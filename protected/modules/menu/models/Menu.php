<?php

/**
 * This is the model class for table "{{menu}}".
 *
 * The followings are the available columns in table '{{menu}}':
 * @property integer $menu_id
 * @property string $menu_name
 * @property integer $parent_id
 * @property integer $status
 * @property string $controller
 * @property string $action
 * @property string $params
 * @property integer $showmenu
 *
 * The followings are the available model relations:
 * @property MenuPermissions[] $menuPermissions
 */
class Menu extends CActiveRecord
{   
        public $parent_menu;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
        
        
       

    
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{menu}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menu_name, status, controller, showmenu', 'required'),
			array('parent_id, status, showmenu, show_list', 'numerical', 'integerOnly'=>true),
			array('menu_name, controller, action', 'length', 'max'=>30),
			array('params', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('menu_id, menu_name, parent_id, status, controller, action, params, showmenu, show_list, parent_menu', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menuPermissions' => array(self::HAS_MANY, 'MenuPermissions', 'menu_id'),
                        'parent' => array(self::BELONGS_TO, 'Menu', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'menu_id' => 'Menu',
			'menu_name' => 'Menu Name',
			'parent_id' => 'Parent Menu',
			'status' => 'Status',
			'controller' => 'Controller',
			'action' => 'Action',
			'params' => 'Params',
			'showmenu' => 'Show Menu',
                        'show_list' => 'Show in List',
                        'parent_menu' => 'Parent Menu'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
               // $criteria->with = array('parent');

		$criteria->compare('menu_id',$this->menu_id);
		$criteria->compare('menu_name',$this->menu_name,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('controller',$this->controller,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('params',$this->params,true);
		$criteria->compare('showmenu',$this->showmenu);
                $criteria->compare('show_list',$this->show_list);
                $criteria->compare('parent.menu_name',$this->parent_menu);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'menu_id ASC',
                            'attributes' => array(
                                'parent_menu' => array(
                                    'asc' => 'menu_id ASC',
                                    'desc' => 'menu_id DESC',
                                ),
                                '*',
                            ),
                        ),
                        
		));
	}
}