<?php

/**
 * This is the model class for table "{{menu_permissions}}".
 *
 * The followings are the available columns in table '{{menu_permissions}}':
 * @property integer $mp_id
 * @property integer $user_id
 * @property integer $menu_id
 *
 * The followings are the available model relations:
 * @property Menu $menu
 */
class MenuPermissions extends CActiveRecord
{
    
    public $username;
    public $menuname;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MenuPermissions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{menu_permissions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, menu_id', 'required'),
			array('user_id, menu_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mp_id, user_id, menu_id, username, menuname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
                        'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mp_id' => 'Menu Permission ID',
			'user_id' => 'User',
			'menu_id' => 'Menu',
                        'username' => 'User',
                        'menuname' => 'Menu Item'
                       
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array( 'user','menu' );
		$criteria->compare('mp_id',$this->mp_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('menu_id',$this->menu_id);
                $criteria->compare('user.first_name',$this->username);
                $criteria->compare('menu.menu_name',$this->menuname);
                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'mp_id ASC',
                            'attributes' => array(
                                'username' => array(
                                    'asc' => 'user.first_name ASC',
                                    'desc' => 'user.first_name DESC',
                                ),
                                'menuname' => array(
                                    'asc' => 'menu.menu_name ASC',
                                    'desc' => 'menu.menu_name DESC',
                                ),
                                '*',
                            ),
                        ),
                    'Pagination' => array (
                        'PageSize' => 20 //edit your number items per page here
                    ),
		));
	}
}