<?php

class CalenderController extends Controller
{
	public function actionIndex()
	{
		$client_sites = Clientsite::model()->findAll();
		$this->layout = '//layouts/calender';
		$this->render('index');
	}
	public function actiongetEvents()
	{		
		if(isset($_REQUEST['date']) && isset($_REQUEST['site_id'])){
			$monthno = date('m', strtotime($_REQUEST['date']));
			$site_id = $_REQUEST['site_id'];
			$criteria = new CDbCriteria();
			$criteria->select="*";
			if($site_id == 1){
				$criteria->condition = "DATE_FORMAT(event_date,'%m')=:monthno AND site_id = ".$site_id." AND status = 0";	
			}else{
				$criteria->condition = "DATE_FORMAT(event_date,'%m')=:monthno AND site_id = ".$site_id." AND status = 1";
			}
			
			$criteria->params = array(':monthno'=>$monthno);
			
			$modelResult= CalenderEvents::model()->findAll($criteria);
			// echo '<pre>'			;print_r(count($modelResult));exit;
			if(!empty($modelResult)){
				$result_array = array();
				foreach($modelResult as $model){
					$data['id'] = $model->id;
					if(empty($model->title_id)){
						$data['title'] = $model->custom_title;
					}else{
						//$data['title'] = $model->title->title;
						if($model->custom_title!= "")
						{
							$data['title'] = $model->custom_title;
						}
						else
						{
							$data['title'] =isset($model->title->title)?$model->title->title:null;
						}
						
					}		
					$data['title_id'] =  $model->title_id;
					$data['date'] = $model->event_date;
					$data['status'] = $model->status;
					array_push($result_array,$data);				
				}	
				echo json_encode($result_array);
			}
		}
		
	}
	public function actionaddEvents()
	{		
		$events_array = $_REQUEST['values'];		
		$event_date_array = array();			
		foreach($events_array as $event){								
				$check_exist = CalenderEvents::model()->find(['condition'=>'event_date = "'.$event['date'].'" AND site_id ='.$event['site_id']]);			
				
				if(empty($check_exist)){					
					$model = new CalenderEvents;
					if($event['title'] == 'Weekly Off'){
						$model->title_id = 1;
						$model->custom_title = 'Weekly Off';
					}else{
						$model->custom_title = $event['title'];
					}
					$model->site_id = $event['site_id'];
					if($model->site_id == 1)			{
						$model->status = 0;
					}else{
						$model->status = 1;
					}
					$model->event_date = $event['date'];
					$model->created_by = Yii::app()->user->id;
					$model->updated_by = Yii::app()->user->id;
					$model->updated_date = date('Y-m-d H:i:s');
					$model->created_date = date('Y-m-d');	
					$model->save();				
					Yii::app()->Controller->taskChange($model->event_date,$model->site_id) ;							
				}else{
				
					if($event['title'] == 'Weekly Off'){
						$check_exist->title_id = 1;
						$check_exist->custom_title = '';
					}else{
						$check_exist->custom_title = $event['title'];
					}	
					
					if($check_exist->site_id == 1)			{
						$check_exist->status = 0;
					}else{
						$check_exist->status = 1;
					}
					$check_exist->event_date = $event['date'];
					$check_exist->updated_by = Yii::app()->user->id;
					
					$check_exist->save();							
					Yii::app()->Controller->taskChange($check_exist->event_date,$check_exist->site_id) ;
				}
			
		
			
			
		}	
			
		
	}
	public function actiondeleteEvent()
	{		
		$request_date = $_REQUEST;			
		if(isset($request_date)){
			if(!empty($request_date['val'])){
				$model = CalenderEvents::model()->find(['condition'=>'event_date = "'.$request_date['date'].'" AND id = '.$request_date['val']]);
			}elseif(!empty($request_date['text'])){
				$model = CalenderEvents::model()->find(['condition'=>'event_date = "'.$request_date['date'].'" AND custom_title = "'.$request_date['text'].'"']);
			}			
			if(!empty($model)){
				$model->status = 2;
				if($model->save()){
					echo $model->id;
					Yii::app()->Controller->taskChange($model->event_date,$model->site_id) ;
				}
			}							
			else{
				echo 0;
			}

		}						
		
	}

	public function actionsitecalender(){
		if(isset($_REQUEST['date'])){
			$monthno = date('m', strtotime($_REQUEST['date']));
			$criteria = new CDbCriteria();
			$criteria->select="*";
			$criteria->condition = "DATE_FORMAT(event_date,'%m')=:monthno AND site_id = 1 AND status = 0";
			$criteria->params = array(':monthno'=>$monthno);
			$modelResult= CalenderEvents::model()->findAll($criteria);
			// $criteria_2 = new CDbCriteria();
			// $criteria_2->select="*";
			// $criteria_2->condition = "DATE_FORMAT(event_date,'%m')=:monthno AND site_id = ".$_REQUEST['site_id'];
			// $criteria_2->params = array(':monthno'=>$monthno);
			// $check_data_status =  CalenderEvents::model()->find($criteria_2);			
				foreach($modelResult as $result_model){
					
					$data = $result_model->attributes;
					$check = CalenderEvents::model()->find(['condition'=>'event_date = "'.$result_model['event_date'].'" AND site_id = '.$_REQUEST['site_id']]);
					if(empty($check)){
						$anotherModel = new CalenderEvents();						
						$anotherModel->setAttributes($data);
						$anotherModel->site_id = $_REQUEST['site_id'];
						$anotherModel->status = 3;
						$anotherModel->save();
					}
					
					
				
				}
			
			
			
		}
	}



	
}