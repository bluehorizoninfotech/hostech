<?php

class MilestoneController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = 'masters/' . Yii::app()->controller->id;


        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow',
                'actions' => array('getdates', 'getModel', 'getmilestonedates', 'getmilestones', 'disableMilestone')
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $project_drpdwn_html = '';
        $this->layout = '//layouts/iframe';
        $model = new Milestone;
        if (isset($_POST['Milestone']['id']) && !empty($_POST['Milestone']['id'])) {
            $id = $_POST['Milestone']['id'];
            $model = $this->loadModel($id);
        } else {
            $model->setScenario(('create'));
        }
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_POST['Milestone'])) {
            $model->attributes = $_POST['Milestone'];
            $model->start_date = date('Y-m-d', strtotime($model->start_date));
            $model->end_date = date('Y-m-d', strtotime($model->end_date));
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            if ($model->save()) {
                $milestone_condition = "status = 1 AND project_id = " . $model->project_id;
                $milestone_data = Milestone::model()->findAll(array(
                    'condition' => $milestone_condition,
                    'order' => 'milestone_title ASC',
                    'distinct' => true
                ));
                $milestone_result = CHtml::listData($milestone_data, 'id', 'milestone_title');
                $milestone_html = "<option value=''>Choose a Milestone</option>";

                foreach ($milestone_result as $milestone_key => $milestone_value) {
                    $milestone_html .= "<option value=" . $milestone_key . ">" . $milestone_value . "</option>";
                }
                if (isset($_POST['submit_type'])) {
                    if ($_POST['submit_type'] == 'save_btn') {
                        $return_result = array('status' => 1, 'next_level' => 0, 'milestone_html' => $milestone_html);
                    } elseif ($_POST['submit_type'] == 'save_cnt') {

                        $project_drpdwn_html .= "<option value=" . $model->project_id . ">" . $model->project->name . "</option>";
                        $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html, 'milestone_html' => $milestone_html);
                    } else {
                        $return_result = array('status' => 1, 'next_level' => 0);
                    }
                    echo json_encode($return_result);
                    exit;
                }
            } else {
                $return_result = array('status' => 2, 'next_level' => 0);
                echo json_encode($return_result);
                exit;
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {

        $this->layout = '//layouts/iframe';
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Milestone'])) {
            $model->attributes = $_POST['Milestone'];
            $model->start_date = date('Y-m-d', strtotime($model->start_date));
            $model->end_date = date('Y-m-d', strtotime($model->end_date));
            $model->updated_by = Yii::app()->user->id;
            if ($model->save()) {
                echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog-edit').dialog('close');window.parent.$('#cru-frame-edit').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
            }
            //$this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        // $this->loadModel($id)->delete();

        // // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        // if (!isset($_GET['ajax']))
        // 	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Milestone('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Milestone']))
            $model->attributes = $_GET['Milestone'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Milestone('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Milestone']))
            $model->attributes = $_GET['Milestone'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Milestone::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    public function actiongetModel()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = Milestone::model()->findByPk($id);
            if (!empty($model)) {
                $model_array = array('stat' => 1, 'id' => $model->id, 'milestone_title' => $model->milestone_title, 'ranking' => $model->ranking, 'status' => $model->status, 'start_date' => date('d-M-y', strtotime($model->start_date)), 'end_date' => date('d-M-y', strtotime($model->end_date)), 'budget_head' => $model->budget_id);
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo  json_encode($model_array);
        exit;
        // if($model===null)
        // 	throw new CHttpException(404,'The requested page does not exist.');
        // return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'milestone-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    protected function getmilestonedate($modelid, $projectid, $date_type)
    {
        if (!empty($projectid)) {
            if ($date_type == 'start') {
                $result = Yii::app()->db->createCommand()
                    ->select('MIN(start_date) as date_val')
                    ->from('pms_tasks')
                    ->where('milestone_id =' . $modelid . ' AND project_id=' . $projectid)
                    ->queryRow();
                if (!empty($result['date_val'])) {
                    return date('d-M-y', strtotime($result['date_val']));
                } else {
                    $project_model = Projects::model()->findByPk($projectid);
                    if (!empty($project_model)) {
                        return date('d-M-y', strtotime($project_model['start_date']));
                    }
                }
            } elseif ($date_type == 'end') {
                $result = Yii::app()->db->createCommand()
                    ->select('MAX(due_date) as date_val')
                    ->from('pms_tasks')
                    ->where('milestone_id =' . $modelid . ' AND project_id=' . $projectid)
                    ->queryRow();
                if (!empty($result['date_val'])) {
                    return date('d-M-y', strtotime($result['date_val']));
                } else {
                    $project_model = Projects::model()->findByPk($projectid);
                    if (!empty($project_model)) {
                        if (!empty($project_model['end_date'])) {
                            return date('d-M-y', strtotime($project_model['end_date']));
                        } else {
                            return '';
                        }
                    }
                }
            }
        } else {
            return '';
        }
    }

    public function actiongetdates()
    {
        if (isset($_POST['project_id'])) {
            $projectid = $_POST['project_id'];
            $project_model = Projects::model()->findByPk($projectid);
            $result_array = array('start' => date('d-M-y', strtotime($project_model['start_date'])), 'end' => date('d-M-y', strtotime($project_model['end_date'])));
            echo json_encode($result_array);
        }
    }
    public function actiongetMilestoneDates()
    {
        if (isset($_POST['id'])) {
            $milstone_id = $_POST['id'];
            $milestone_model = $this->loadModel($milstone_id);
            if (!empty($milestone_model)) {
                $result_array = array('status' => '1', 'start' => date('d-M-y', strtotime($milestone_model['start_date'])), 'end' => date('d-M-y', strtotime($milestone_model['end_date'])));
            } else {
                $result_array = array('status' => '2');
            }

            echo json_encode($result_array);
        }
    }

    public function actiongetMilestones()
    {
        $milestone_html = "<option value=''>Choose a Milestone</option>";
        if (isset($_POST['projectid']) && !empty($_POST['projectid'])) {
            $milestone_condition = "status = 1 AND project_id = " . $_POST['projectid'];
            $milestone_data = Milestone::model()->findAll(array(
                'condition' => $milestone_condition,
                'order' => 'milestone_title ASC',
                'distinct' => true
            ));
            $milestone_result = CHtml::listData($milestone_data, 'id', 'milestone_title');


            foreach ($milestone_result as $milestone_key => $milestone_value) {
                $milestone_html .= "<option value=" . $milestone_key . ">" . $milestone_value . "</option>";
            }
            $return_result = array('milestones' => $milestone_html, 'status' => 1);
        } else {
            $return_result = array('milestones' => $milestone_html, 'status' => 2);
        }
        echo json_encode($return_result);
        exit;
    }

    public function actiondisableMilestone()
    {

        $today = date('Y-m-d');
        $data = array(
            "status" => 2
        );
        $update = Yii::app()->db->createCommand()
            ->update(
                'pms_milestone',
                $data,
                'end_date<:date',
                array(':date' => $today)
            );
    }
}
