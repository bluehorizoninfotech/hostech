<?php

class ProjectWorkTypeController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $accessguestArr = array();
        $controller = 'masters/' . Yii::app()->controller->id;


        if (isset(Yii::app()->session['menuauth'])) {
            if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
                $accessauthArr = Yii::app()->session['menuauth'][$controller];
            }
        }


        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow',
                'actions' => array('create', 'getModel', 'delete')
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new ProjectWorkType;

        if (isset($_POST['ProjectWorkType']['id']) && !empty($_POST['ProjectWorkType']['id'])) {
            $id = $_POST['ProjectWorkType']['id'];
            $model = $this->loadModel($id);
        } else {
            $model->setscenario('create');
        }
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ProjectWorkType'])) {
            $model->attributes = $_POST['ProjectWorkType'];
            $model->created_by = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_date = date('Y-m-d H:i:s');
            if ($model->save()) {
                if (isset($_POST['submit_type'])) {
                    if ($_POST['submit_type'] == 'save_btn') {
                        $return_result = array('status' => 1, 'next_level' => 0);
                    } elseif ($_POST['submit_type'] == 'save_cnt') {
                        $project_drpdwn_html = "<option value=" . $model->project_id . ">" . $model->project->name . "</option>";
                        $return_result = array('status' => 1, 'next_level' => 1, 'html' => $project_drpdwn_html);
                    } else {
                        $return_result = array('status' => 1, 'next_level' => 0);
                    }
                    echo json_encode($return_result);
                    exit;
                } else {
                    $return_result = array('status' => 2, 'next_level' => 0);
                    echo json_encode($return_result);
                    exit;
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('ProjectWorkType');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }



    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = ProjectWorkType::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actiongetModel()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = ProjectWorkType::model()->findByPk($id);
            if (!empty($model)) {
                $model_array = array('stat' => 1, 'id' => $model->id, 'work_type' => $model->work_type_id, 'ranking' => $model->ranking);
            } else {
                $model_array = array('stat' => 0);
            }
        } else {
            $model_array = array('stat' => 0);
        }
        echo  json_encode($model_array);
        exit;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-work-type-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDelete($id)
    {
        // $this->loadModel($id)->delete();

        // // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        // if (!isset($_GET['ajax']))
        // 	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();

        try {
            $work_type = ProjectWorkType::model()->findByPk($id);
            $work_type_id = $work_type->work_type_id;
            $project_id = $work_type->project_id;
            $work_type_used = Tasks::model()->findByAttributes(array(
                'work_type_id' => $work_type_id,
                'project_id' => $project_id
            ));

            if ($work_type_used) {
                $success_status = 2;
            } else {
                if (!$model->delete()) {
                    $success_status = 0;
                    throw new Exception(json_encode($model->getErrors()));
                } else {
                    $success_status = 1;
                }
            }


            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else if ($success_status == 2) {
                echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }
}
