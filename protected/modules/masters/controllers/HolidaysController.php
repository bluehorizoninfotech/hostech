<?php

class HolidaysController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		

		$this->layout = '//layouts/iframe';
		$model=new Holidays;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Holidays']))
		{
			$model->attributes=$_POST['Holidays'];
			$model->holiday_date = date('Y-m-d',strtotime($_POST['Holidays']['holiday_date']));
			$model->created_by = Yii::app()->user->id;
			$model->updated_by = Yii::app()->user->id;
			$model->created_date = date('Y-m-d');			
			if($model->save()){
				$check_event = CalenderEvents::model()->find(['condition'=>'event_date = "'.$model->holiday_date.'"']);
				if(empty($check_event)){
					$event_model =  new CalenderEvents;
					$event_model->title_id = $model->id;
					$event_model->event_date = $model->holiday_date;
					$event_model->created_by = Yii::app()->user->id;
					$event_model->updated_by = Yii::app()->user->id;
					$event_model->status = 0;
					$event_model->created_date = date('Y-m-d');
					$event_model->site_id =1;	
					if($event_model->save()){
						 Yii::app()->Controller->taskChange($event_model->event_date,$event_model->site_id) ;
					}
					echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
				}
			
			}				
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = '//layouts/iframe';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Holidays']))
		{
			$model->attributes=$_POST['Holidays'];
			if($model->save()){
                             echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog-edit').dialog('close');window.parent.$('#cru-frame-edit').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                        }
				//$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));	
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
		$model=new Holidays('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Unit']))
			$model->attributes=$_GET['Holidays'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Holidays('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Holidays']))
			$model->attributes=$_GET['Holidays'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Holidays::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='holidays-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
