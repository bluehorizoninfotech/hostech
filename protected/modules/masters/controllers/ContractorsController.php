<?php

class ContractorsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$accessArr = array();
		$accessauthArr = array();
		$accessguestArr = array();
		$controller = 'masters/'.Yii::app()->controller->id;
		

		if (isset(Yii::app()->session['menuauth'])) {
			if (array_key_exists($controller, Yii::app()->session['menuauth'])) {
				$accessauthArr = Yii::app()->session['menuauth'][$controller];
			}
		}

		
		$access_privlg = count($accessauthArr);
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = '//layouts/iframe';
		$model=new Contractors;

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Contractors']))
		{
			
			$model->attributes=$_POST['Contractors'];
			$model->created_by = Yii::app()->user->id;
			$model->updated_by = Yii::app()->user->id;
			$model->created_date = date('Y-m-d');						
			if($model->save()){
				echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
			}else{
				echo '<pre>';print_r($model->getErrors());
				exit;
			}
				
		}
		$this->render('create',array(
			'model'=>$model,
		));	
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = '//layouts/iframe';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Contractors']))
		{
			$model->attributes=$_POST['Contractors'];
			$model->updated_by = Yii::app()->user->id;
			if($model->save()){
                             echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog-edit').dialog('close');window.parent.$('#cru-frame-edit').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                        }
				//$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));				
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$task_list_count = Tasks::model()->find()->count('contractor_id=:contractor_id ', array(':contractor_id' => $id));

		$meeting_participants_count = MeetingParticipants::model()->count('contractor_id=:contractor_id',array(':contractor_id' => $id));
		
		if ($task_list_count || $meeting_participants_count) {

			echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));          
		}
		else

		{
			$this->loadModel($id)->delete();

			echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
		}
		
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Contractors('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contractors']))
			$model->attributes=$_GET['Contractors'];

		$this->render('index',array(
			'model'=>$model,
		));
		
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Contractors('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contractors']))
			$model->attributes=$_GET['Contractors'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Contractors::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='contractors-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
