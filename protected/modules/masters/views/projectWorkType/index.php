<?php
/* @var $this ProjectWorkTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Project Work Types',
);

$this->menu=array(
	array('label'=>'Create ProjectWorkType', 'url'=>array('create')),
	array('label'=>'Manage ProjectWorkType', 'url'=>array('admin')),
);
?>

<h1>Project Work Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
