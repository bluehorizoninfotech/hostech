<?php
/* @var $this ProjectWorkTypeController */
/* @var $model ProjectWorkType */

$this->breadcrumbs=array(
	'Project Work Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProjectWorkType', 'url'=>array('index')),
	array('label'=>'Manage ProjectWorkType', 'url'=>array('admin')),
);
?>

<h1>Create ProjectWorkType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>