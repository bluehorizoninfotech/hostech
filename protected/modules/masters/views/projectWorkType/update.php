<?php
/* @var $this ProjectWorkTypeController */
/* @var $model ProjectWorkType */

$this->breadcrumbs=array(
	'Project Work Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectWorkType', 'url'=>array('index')),
	array('label'=>'Create ProjectWorkType', 'url'=>array('create')),
	array('label'=>'View ProjectWorkType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectWorkType', 'url'=>array('admin')),
);
?>

<h1>Update ProjectWorkType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>