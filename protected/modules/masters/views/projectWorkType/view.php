<?php
/* @var $this ProjectWorkTypeController */
/* @var $model ProjectWorkType */

$this->breadcrumbs=array(
	'Project Work Types'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectWorkType', 'url'=>array('index')),
	array('label'=>'Create ProjectWorkType', 'url'=>array('create')),
	array('label'=>'Update ProjectWorkType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectWorkType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectWorkType', 'url'=>array('admin')),
);
?>

<h1>View ProjectWorkType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'work_type_id',
		'ranking',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
