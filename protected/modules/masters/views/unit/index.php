<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script>

<?php
/* @var $this DepartmentController */
/* @var $model Department */
Yii::app()->clientScript->registerScript('search', "
");
?>

<div class="alert alert-success" role="alert">
    </div>
    <div class="alert alert-danger" role="alert">
    </div>
    <div class="alert alert-warning" role="alert">

    </div>

<div class="half-table">
    <div class="clearfix">
    <div class="add link pull-right">
        <?php
        if(isset(Yii::app()->user->role) && (in_array('/masters/unit/create', Yii::app()->session['menuauthlist']))){
            $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
            echo CHtml::link('Add Unit', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        }
       
        ?>
    </div>  
    <h1>Manage Units</h1>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'unit-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'table table-bordered',
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.'),
        array(
            'class' => 'CButtonColumn',
            'template' => isset(Yii::app()->user->role) && (in_array('/masters/unit/create', Yii::app()->session['menuauthlist']))?'{update} {delete}' : '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'imageUrl'=>false,                   
                    'url' => 'Yii::app()->createUrl("masters/unit/update", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid"))',
                    'click' => 'function(e){e.preventDefault();$("#cru-frame-edit").attr("src",$(this).attr("href")); $("#cru-dialog-edit").dialog("open");  return false;}',
                    'options' => array('class' => 'actionitem updateicon icon-pencil icon-comn' ,'title'=>'Edit'),
                ),
                // 'delete_unit'=>array(
                //     'label' => '',
                //     'imageUrl' => false,
                //    // 'visible' => '($data->approve_status=="0") && in_array("/dailyWorkProgress/delete", Yii::app()->session["menuauthlist"])',
                //     'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete', 'id' => '$data->id'), 
                // )
                'delete' => array(
                    'label' => '',
                    'imageUrl' => false,

                    'visible' => "Yii::app()->user->role == 1",

                    'click' => 'function(e){e.preventDefault();deletetask($(this).attr("href"));}',
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                ),
            ),
        ),
        //'department_id',
        array(
            'name' => 'unit_title',
			'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
			array(
				'name' => 'unit_code',
				'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
				array(
					'name' => 'status',
					'value'=>function($data){
						if($data->status == 1){
							$status = 'Enable';
						}else{
							$status = 'Disable';
						}
						return $status;
					},
					'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
        
    ),
));
?>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Unit',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog-edit',
    'options' => array(
        'title' => 'Edit Unit',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame-edit" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>

<script>

function deletetask(href) {
        var id = getURLParameter(href, 'id');

        var answer = confirm("Are you sure you want to delete?");
        var url = "<?php echo $this->createUrl('unit/delete&id=') ?>" + id;
        if (answer) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                success: function(response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                       
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                    } else if (response.response == "warning") {
                        
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                    } else {
                       
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                    }
                    $("#unit-grid").load(location.href + " #unit-grid");



                }
            });
        }


    }

    function getURLParameter(url, name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
    }
    $('.alert-success').hide();
    $('.alert-danger').hide();
    $('.alert-warning').hide();

</script>