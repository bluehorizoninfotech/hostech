<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
		'id'=>'unit-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>


    <div class="row">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'unit_title'); ?>
            <?php echo $form->textField($model, 'unit_title', array('class' => 'form-control input-medium', 'size' => 60, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'unit_title'); ?>
        </div>

		
    </div>
	<div class="row">
	<div class="col-md-12">
        <?php echo $form->labelEx($model,'unit_code'); ?>
        <?php echo $form->textField($model,'unit_code',array('class' => 'form-control input-medium','size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'unit_code'); ?>
    </div>
	</div>

    

    <div class="row">
	<div class="col-md-12">
        <?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array('1'=>'Enable','2'=>'Disable'), array('class' => 'form-control input-medium','options' => array('1'=>array('selected'=>true))));?>
        
        <?php echo $form->error($model,'status'); ?>
		</div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->