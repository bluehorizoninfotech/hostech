<?php
/* @var $this HolidaysController */
/* @var $model Holidays */
/* @var $form CActiveForm */
?>
 
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'holidays-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => false,),
)); 
?>	

	<?php echo $form->errorSummary($model); ?>	

	<div class="row">
	<div class="col-md-12">
		<?php echo $form->labelEx($model,'holiday_date'); ?>	
		<?php   echo CHtml::activeTextField($model, 'holiday_date', array('class'=>'form-control input-small holiday_date','readonly' => false,"id" => "holiday_date",'size'=>10,'autocomplete'=>"off"));?>
		<?php echo $form->error($model,'holiday_date'); ?>
		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>200,'class'=>'form-control input-medium')); ?>
		<?php echo $form->error($model,'title'); ?>
		</div>
	</div>
	<br>
    <div class="row">
        <div class="col-md-12">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script language="JavaScript"  src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script>
$(document).ready(function() {

    var date1 = '<?= $entry_date; ?>';

    $('.holiday_date').datepicker({
        dateFormat: 'd-M-y',
        minDate: date1,
    });
});
</script>