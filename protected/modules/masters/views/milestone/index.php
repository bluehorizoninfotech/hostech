<?php
/* @var $this DepartmentController */
/* @var $model Department */
Yii::app()->clientScript->registerScript('search', "
");
?>


<div class="">
    <div class="clearfix">
    <div class="add link pull-right">
        <?php
        if(isset(Yii::app()->user->role) && (in_array('/masters/milestone/create', Yii::app()->session['menuauthlist']))){
            $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
            echo CHtml::link('Add Milestone', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        }
       
        ?>
    </div>  
    <h1>Manage Milestones</h1>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'milestone-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'table table-bordered',
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.'),
        //'department_id',
        array(
            'class' => 'CButtonColumn',
            'template' => isset(Yii::app()->user->role) && (in_array('/masters/milestone/update', Yii::app()->session['menuauthlist']))?'{update}':'',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'imageUrl'=>false,                   
                    'url' => 'Yii::app()->createUrl("masters/milestone/update", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid"))',
                    'click' => 'function(e){e.preventDefault();$("#cru-frame-edit").attr("src",$(this).attr("href")); $("#cru-dialog-edit").dialog("open");  return false;}',
                    'options' => array('class' => 'actionitem updateicon icon-pencil icon-comn' ,'title'=>'Edit'),
                ),
            ),
        ),
        array(
            'name' => 'milestone_title',
            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
            array(
                'name' => 'project_id',
                'value' => '$data->project->name',
                'type' => 'raw',
                'filter' => CHtml::listData(Projects::model()->findAll(
                                array(
                                    'select' => array('pid,name'),
                    'condition'=>'status=1',
                                    'order' => 'name',
                                    'distinct' => true
                        )), "pid", "name")
            ),	
            array(
                'name' => 'start_date',
                'value' => 'date("d-M-y",strtotime($data->start_date))',
                'type' => 'html', 
                'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
                array(
                    'name' => 'end_date',
                    'value' => 'date("d-M-y",strtotime($data->end_date))',  
                    'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),		
		array(
					'name' => 'status',
					'value'=>function($data){
						if($data->status == 1){
							$status = 'Enable';
						}else{
							$status = 'Disable';
						}
						return $status;
					},
		'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
        
    ),
));
?>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Milestones',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog-edit',
    'options' => array(
        'title' => 'Edit Milestones',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame-edit" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>