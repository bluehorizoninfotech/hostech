<?php
/* @var $this DepartmentController */
/* @var $model Department */

?>

<h1>Create Milestone</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>