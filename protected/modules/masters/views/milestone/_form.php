<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

?>
<style>
    .w-50{
        width:50% !important;
    }
    .row{
        display:flex;
    }
    .btn-center{
        width: 100%;
        text-align: center;
    }
</style>
<div class="milestone-sec">
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
		'id'=>'milestone-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));   
    ?>

    
    <div class="row">
    <div class="subrow col-md-6 create-milestone-page-filed w-50">
			<?php echo $form->labelEx($model,'milestone_title'); ?>
			<?php echo $form->textField($model,'milestone_title',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'milestone_title'); ?>           
        </div>	
    <div class="subrow col-md-6 create-milestone-page-filed w-50">
			<?php echo $form->labelEx($model,'project_id'); ?>
            <?php
                if($model->isNewRecord){
                    if(Yii::app()->user->project_id !=""){
                        $model->project_id = Yii::app()->user->project_id;
                        $projects = Projects::model()->findByPk(Yii::app()->user->project_id);
                        $model->start_date = date('d-M-y',strtotime($projects->start_date));
                        $model->end_date = date('d-M-y',strtotime($projects->end_date));
                    }else{
                        $model->project_id="";
                    }
                }else{
                    $model->start_date = $this->getmilestonedate($model->id,$model->project_id,'start');                  
                    $model->end_date = $this->getmilestonedate($model->id,$model->project_id,'end');
                }
            ?>
            <?php 
            if(Yii::app()->user->role == 1){
                $proj_condition = 'status="1"';
            }else{             
                $proj_condition = 'status=1 AND find_in_set('.Yii::app()->user->id.',assigned_to)';
            }
            echo $form->dropDownList($model,'project_id',
                        CHtml::listData(Projects::model()->findAll(array('condition' => $proj_condition,'order' => 'name ASC ')), 'pid','name'),
                            array(
                            'empty' => 'Choose a project',
                            'disabled'=>$model->isNewRecord?false:true,
                            'class'=>'form-control '
                            )
                        ); 
            ?>
			<?php echo $form->error($model,'project_id'); ?>           
        </div>      	
    </div>
    <div class="row">
	    <div class="subrow col-md-6 create-milestone-page-filed w-50">
            <?php echo $form->labelEx($model,'start_date'); ?>
            <?php echo $form->textField($model,'start_date',array('class' => 'form-control')); ?>            
            <?php echo $form->error($model,'start_date'); ?>
		</div>
        <div class="subrow col-md-6 create-milestone-page-filed w-50">
            <?php echo $form->labelEx($model,'end_date'); ?>
            <?php echo $form->textField($model,'end_date',array('class' => 'form-control')); ?>            
            <?php echo $form->error($model,'end_date'); ?>
		</div>
    </div>   

    <div class="row">
	    <div class="subrow col-md-6 create-milestone-page-filed w-50">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status',array('1'=>'Enable','2'=>'Disable'), array('class' => 'form-control','options' => array('1'=>array('selected'=>true))));?>
            
            <?php echo $form->error($model,'status'); ?>
		</div>
    </div>
    <br>
    <div class="row">
        <div class="subrow col-md-2 btn-center">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
                    </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('#Milestone_start_date').datepicker({
        dateFormat: 'd-M-y',
        
    });
    $('#Milestone_end_date').datepicker({
        dateFormat: 'd-M-y',
        
    });
    $('#Milestone_project_id').on('change', function (e) {
        var valueSelected = this.value; 
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('masters/milestone/getdates'); ?>',
            data:{project_id:valueSelected},            
            method: "POST",
            dataType:"json",
            success: function(result) {
                $('#Milestone_start_date').val(result.start);
                $('#Milestone_end_date').val(result.end);
            }
        });
    });
});

</script>