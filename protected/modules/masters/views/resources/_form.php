<?php
/* @var $this DepartmentController */
/* @var $model Department */
/* @var $form CActiveForm */

?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
		'id'=>'resources-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>


    <div class="row">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'resource_name'); ?>
            <?php echo $form->textField($model, 'resource_name', array('class' => 'form-control input-medium', 'size' => 60, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'resource_name'); ?>
        </div>

		
    </div>
	<div class="row">
	<div class="col-md-12">
        <?php echo $form->labelEx($model,'resource_rate'); ?>
        <?php echo $form->textField($model,'resource_rate',array('class' => 'form-control input-medium','size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'resource_rate'); ?>
    </div>
	</div>

    

    <div class="row">
	<div class="col-md-12">
        <?php echo $form->labelEx($model,'resource_unit'); ?>
		
       <?php 
            
            echo $form->dropDownList($model,'resource_unit',
                        CHtml::listData(Unit::model()->findAll(array('order' => 'unit_title ASC ')), 'id','unit_title'),
                            array(
                            'empty' => 'Choose  unit',
                           
                            'class'=>'form-control input-medium'
                            )
                        ); 
            ?> 
        
        <?php echo $form->error($model,'resource_unit'); ?>
		</div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->