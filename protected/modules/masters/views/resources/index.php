<?php
/* @var $this DepartmentController */
/* @var $model Department */
Yii::app()->clientScript->registerScript('search', "
");
?>


<div class="half-table">
    <div class="clearfix">
    <div class="add link pull-right">
        <?php
        if(isset(Yii::app()->user->role) && (in_array('/masters/resources/create', Yii::app()->session['menuauthlist']))){
            $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
            echo CHtml::link('Add Resources', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        }
       
        ?>
    </div>  
    <h1>Manage Resources</h1>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'resources-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'table table-bordered',
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.'),
        array(
            'class' => 'CButtonColumn',
            'template' => isset(Yii::app()->user->role) && (in_array('/masters/resources/create', Yii::app()->session['menuauthlist']))?'{update}':'',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'imageUrl'=>false,                   
                    'url' => 'Yii::app()->createUrl("masters/resources/update", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid"))',
                    'click' => 'function(e){e.preventDefault();$("#cru-frame-edit").attr("src",$(this).attr("href")); $("#cru-dialog-edit").dialog("open");  return false;}',
                    'options' => array('class' => 'actionitem updateicon icon-pencil icon-comn' ,'title'=>'Edit'),
                ),
            ),
        ),
        array(
            'name' => 'resource_name',
			'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
			array(
				'name' => 'resource_rate',
				'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
				array(
					'name' => 'resource_unit',
					'value'=>'$data->resourceUnit->unit_title',
					// 'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')
                    'filter' => CHtml::listData(Unit::model()->findAll(
                        array(
                            'select' => array('id,unit_title'),
                            'order' => 'unit_title',
                            'distinct' => true
                        )
                    ), "id", "unit_title"),
                ),
        
    ),
));
?>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Resources',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog-edit',
    'options' => array(
        'title' => 'Edit Resources',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame-edit" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>