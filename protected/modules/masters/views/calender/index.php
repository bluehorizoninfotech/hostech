<link rel="stylesheet" href="fullcalendar/fullcalendar.min.css" />
<div class="calender-index-sec">
    <div class="clearfix space-gap">
        <div class="pull-left">
            <label for="Unit_status" class="required">Choose Site </label>
            <?php
            // $where = " 1=1";
            //     if(Yii::app()->user->project_id !=""){
            //        $where = " pid =".Yii::app()->user->project_id."";
            //    }
            $type_list = CHtml::listData(
                Clientsite::model()->findAll(),
                'id',
                'site_name'
            );
            echo CHtml::dropDownList(
                'site_id',
                $selected_value = '1',
                $type_list,
                array('empty' => 'Select Option', 'class' => 'form-control display-inline-block width-200 height-28')
            );

            ?>
        </div>
        <div class="pull-left margin-left-5">
            <input type="text" class="form-control display-inline-block  height-28 width-200" id="go_to_date" placeholder="Go to date (yyyy-mm-dd)" maxlength="10">
            <button id="my_button" class="btn btn-sm btn-primary calender-page-submit-button">Submit</button>
        </div>
        <div class="pull-right calender-page-buttons">
            <a id="btnClear" class="btn btn-sm btn-primary">Save</a>
            <a id="auto_data" class="btn btn-sm btn-primary">Week Off </a>
        </div>

    </div>

    <div class="calender_div">
        <div class="response"></div>

        <div id='calendar'></div>
    </div>
</div>
<script src="fullcalendar/lib/jquery.min.js"></script>
<script src="fullcalendar/lib/moment.min.js"></script>
<script src="fullcalendar/fullcalendar.min.js"></script>


<script>
    $(document).ready(function() {
        $("#go_to_date").datepicker({
            dateFormat: 'yy-mm-dd',
        });


        var calendar = $('#calendar').fullCalendar({
            editable: true,
            fixedWeekCount: false,
            events: function(start, end, timezone, callback) {
                var date = $("#calendar").fullCalendar('getDate');
                var site_id = $("select#site_id option").filter(":selected").val();
                var d = new Date(date);
                mnth = ("0" + (d.getMonth() + 1)).slice(-2),
                    day = ("0" + d.getDate()).slice(-2);
                var cur_month = [d.getFullYear(), mnth, day].join("-");

                jQuery.ajax({
                    url: 'index.php?r=masters/calender/getevents',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        date: cur_month,
                        site_id: site_id

                    },
                    success: function(results) {

                        var events = [];
                        $.each(results, function(k, v) {
                            var d = new Date(v.date);

                            events.push({
                                id: v.id,
                                title: v.title,
                                start: d,
                                title_id: v.title_id,
                                status_val: v.status

                            });

                        });
                        //    console.log(events);
                        callback(events);


                    }
                });
            },

            displayEventTime: false,
            eventRender: function(event, element, view) {

                if (event.status_val == "3") {
                    element.css('background-color', '#ff0000');
                }
                if (event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                var title = prompt('Event Title:');

                if (title) {
                    var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                    calendar.fullCalendar('renderEvent', {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true
                    );
                }
                calendar.fullCalendar('unselect');
            },

            editable: true,
            eventDrop: function(event, delta) {
                var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                $.ajax({
                    url: 'edit-event.php',
                    data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                    type: "POST",
                    success: function(response) {
                        displayMessage("Updated Successfully");
                    }
                });
            },
            eventClick: function(event) {
                var deleteMsg = confirm("Do you really want to delete?");
                if (deleteMsg) {
                    console.log(event.start._d);
                    var start_date = new Date(event.start._d);
                    mnth = ("0" + (start_date.getMonth() + 1)).slice(-2),
                        day = ("0" + start_date.getDate()).slice(-2);
                    var event_date = [start_date.getFullYear(), mnth, day].join("-");
                    if (event.title === "Weekly Off") {
                        var title_id = 1;
                    }
                    $.ajax({
                        type: "POST",
                        url: "index.php?r=masters/calender/deleteevent",
                        data: {
                            val: event.id,
                            text: event.title,
                            date: event_date,
                            title_id: title_id
                        },
                        success: function(response) {

                            if (parseInt(response) > 0) {
                                $('#calendar').fullCalendar('removeEvents', function() {
                                    return true;
                                });
                                $('#calendar').fullCalendar('refetchEvents');
                                displayMessage("Deleted Successfully");
                            } else {
                                // $('#calendar').fullCalendar('removeEvents', function () { return true; });
                                // $('#calendar').fullCalendar('refetchEvents'); 

                                var targetDate = moment.utc(event_date);

                                $('#calendar').fullCalendar('removeEvents', function(event) {
                                    var isMatched = moment(event.start).isSame(targetDate, 'day');

                                    return isMatched;
                                });


                            }
                        }
                    });
                }
            }


        });

        $('#my_button').click(function() {
            var go_to = $('#go_to_date').val();
            var newDate = go_to.toString('Y-m-d');
            // call fullCalendar 'gotoDate' method
            $('#calendar').fullCalendar('gotoDate', newDate);

        });
        $("#site_id").change(function() {

            var site_id = $('option:selected', this).val();
            if (site_id != '1') {
                var date = $("#calendar").fullCalendar('getDate');
                var date_value = new Date(date);
                mnth = ("0" + (date_value.getMonth() + 1)).slice(-2),
                    day = ("0" + date_value.getDate()).slice(-2);
                var event_date = [date_value.getFullYear(), mnth, day].join("-");
                $.ajax({
                    type: "POST",
                    url: "index.php?r=masters/calender/sitecalender",
                    data: {
                        site_id: site_id,
                        date: event_date
                    },
                    success: function(response) {
                        $('#calendar').fullCalendar('removeEvents', function() {
                            return true;
                        });
                        $('#calendar').fullCalendar('refetchEvents');

                    }
                });
            } else {
                $('#calendar').fullCalendar('removeEvents', function() {
                    return true;
                });
                $('#calendar').fullCalendar('refetchEvents');
            }

        });
        $(".fc-other-month .fc-day-number").hide();
        var d = new Date();
        // checkweekends(d);
        $('#auto_data').click(function() {
            var date = $("#calendar").fullCalendar('getDate');
            var d = new Date(date);
            checkweekends(d);
        });

        $("#btnClear").click(function() {
            var site_id = $("#site_id option:selected").val();
            var events = $('#calendar').fullCalendar('clientEvents');
            var arr = [];
            for (var i = 0; i < events.length; i++) {

                var start_date = new Date(events[i].start._d);
                mnth = ("0" + (start_date.getMonth() + 1)).slice(-2),
                    day = ("0" + start_date.getDate()).slice(-2);
                var event_date = [start_date.getFullYear(), mnth, day].join("-");
                arr.push({
                    id: events[i].id,
                    title_id: events[i].title_id,
                    date: event_date,
                    title: events[i].title,
                    site_id: site_id
                });
            }
            $.ajax({
                type: "POST",
                url: "index.php?r=masters/calender/addevents",
                data: {
                    values: arr
                },
                success: function(response) {
                    $("#calender_div").load(location.href + " #calender_div");
                    displayMessage("Added Successfully");

                }
            });

        });

        //     $('#go_to_date').bind('keydown', function(event) {
        //     var inputLength = event.target.value.length;
        //     if(inputLength === 4){        
        //       var thisVal = event.target.value;
        //       thisVal += '-';
        //       $(event.target).val(thisVal);
        //     }
        //     if(inputLength === 7){
        //         var month_array = $(event.target).val().split('-');
        //         var month_val = month_array[1];
        //         if(month_val > 12){
        //             $(event.target).val(month_array[0]+'-12'+'-');           
        //         }

        //     }
        //     if(inputLength === 10){
        //         var month_array = $(event.target).val().split('-');
        //         var day_val = month_array[2];
        //         if(day_val > 31){
        //             $(event.target).val(month_array[0]+'-'+month_array[1]+'-31');           
        //         }

        //     }

        //   })

        function checkweekends(d) {

            var getTot = daysInMonth(d.getMonth(), d.getFullYear()); //Get total days in a month
            for (var i = 1; i <= getTot; i++) { //looping through days in month
                var newDate = new Date(d.getFullYear(), d.getMonth(), i)
                if (newDate.getDay() == 0) { //if Sunday
                    // alert(new Date(d.getFullYear(),d.getMonth(),i));
                    $("#calendar").fullCalendar('removeEvents', i);
                    var event = {
                        id: i,
                        title: 'Weekly Off',
                        start: new Date(d.getFullYear(), d.getMonth(), i)
                    };
                    $('#calendar').fullCalendar('renderEvent', event, true);
                }
                if (newDate.getDay() == 6) { //if Saturday
                    $("#calendar").fullCalendar('removeEvents', i);
                    var event = {
                        id: i,
                        title: 'Weekly Off',
                        start: new Date(d.getFullYear(), d.getMonth(), i)
                    };
                    $('#calendar').fullCalendar('renderEvent', event, true);
                }

            }

        }






        function daysInMonth(month, year) {
            return new Date(year, month, 0).getDate();
        }
        // var event={id:1 , title: 'New event', start:  new Date()};


    });

    function displayMessage(message) {
        $(".response").html("<div class='success'>" + message + "</div>");
        setInterval(function() {
            $(".success").fadeOut();
        }, 1000);
    }
</script>