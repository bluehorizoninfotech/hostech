<?php
/* @var $this ContractorsController */
/* @var $model Contractors */
/* @var $form CActiveForm */
?>
<style>
.subrow {
    /* width: 250px; */
    float: left;
}

form .subrow {
    padding: 0 12px !important;
}

.btn-center{
        width: 100%;
        text-align: center;
    }
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contractors-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => false,),
)); ?>

<div class="row">
        <div class="subrow">
			<?php echo $form->labelEx($model,'contractor_title'); ?>
			<?php echo $form->textField($model,'contractor_title',array('class' => 'form-control input-medium','size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'contractor_title'); ?>           
        </div>
        <div class="subrow">
            <?php echo $form->labelEx($model,'email_id'); ?>
            <?php echo $form->textField($model,'email_id',array('class'=>'form-control input-medium','size'=>60,'maxlength'=>100)); ?>
            <?php echo $form->error($model,'email_id'); ?>
        </div>
		
    </div>	    

    <div class="row">
    <div class="subrow">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textArea($model,'description',array('class'=>'form-control input-medium','rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>
	<div class="subrow">
        <?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array('1'=>'Enable','2'=>'Disable'), array('class' => 'form-control input-medium','options' => array('1'=>array('selected'=>true))));?>
        
        <?php echo $form->error($model,'status'); ?>
		</div>
  
    </div>
   <br>
    <div class="row">
    <div class="subrow btn-center">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
            </div>
       
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->