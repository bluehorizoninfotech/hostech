<?php
/* @var $this ContractorsController */
/* @var $model Contractors */

$this->breadcrumbs=array(
	'Contractors'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Contractors', 'url'=>array('index')),
	array('label'=>'Create Contractors', 'url'=>array('create')),
	array('label'=>'Update Contractors', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Contractors', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Contractors', 'url'=>array('admin')),
);
?>

<h1>View Contractors #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'contractor_title',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
