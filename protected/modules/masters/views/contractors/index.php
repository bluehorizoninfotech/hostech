<?php
/* @var $this ContractorsController */
/* @var $dataProvider CActiveDataProvider */
Yii::app()->clientScript->registerScript('search', "
");
?>

<!-- <h1>Contractors</h1> -->
<?php //$this->widget('zii.widgets.CListView', array(
// 	'dataProvider'=>$dataProvider,
// 	'itemView'=>'_view',
// )); ?>
<div class="">
    <div class="clearfix">
    <div class="add link pull-right">
        <?php
        if(isset(Yii::app()->user->role) && (in_array('/masters/contractors/create', Yii::app()->session['menuauthlist']))){
            $createUrl = $this->createUrl('create', array("asDialog" => 1, "gridId" => 'address-grid'));
            echo CHtml::link('Add Contractor', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        }
       
        ?>
    </div>  
    <h1>Manage Contractors</h1>

    
</div>
<div class="alert alert-success display-none" role="alert">
</div>
<div class="alert alert-danger display-none" role="alert">
</div>
<div class="alert alert-warning display-none" role="alert">

</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'contractors-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'table table-bordered',
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.'),
        //'department_id',
        array(
            'class' => 'CButtonColumn',
            //'template' => isset(Yii::app()->user->role) && (in_array('/masters/contractors/update', Yii::app()->session['menuauthlist']))?'{update}{delete}':'',
            'template'=>'{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'imageUrl'=>false,                   
                    'url' => 'Yii::app()->createUrl("masters/contractors/update", array("id"=>$data->id,"asDialog"=>1,"gridId"=>"address-grid"))',
                    'click' => 'function(e){e.preventDefault();$("#cru-frame-edit").attr("src",$(this).attr("href")); $("#cru-dialog-edit").dialog("open");  return false;}',
                    'options' => array('class' => 'actionitem updateicon icon-pencil icon-comn' ,'title'=>'Edit'),
                    'visible' => ' (in_array("/masters/contractors/update", Yii::app()->session["menuauthlist"])) '
                ),
                'delete' => array(
                    'label' => '',
                    'imageUrl' => false,
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete',),
                    'visible' => ' (in_array("/masters/contractors/delete", Yii::app()->session["menuauthlist"])) ',

                    'click' => 'function(e){e.preventDefault();deletecontractor($(this).attr("href"));}',
                    'options' => array('class' => 'icon-trash icon-comn', 'title' => 'Delete'),
                )

                
            ),
        ),
        array(
            'name' => 'contractor_title',
            'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
            'email_id',
		array(
					'name' => 'status',
					'value'=>function($data){
						if($data->status == 1){
							$status = 'Enable';
						}else{
							$status = 'Disable';
						}
						return $status;
					},
		'htmlOptions' => array('class' => 'departmentcls', 'data-id' => '$data->id')),
       
    ),
));
?>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Contractor',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0" class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog-edit',
    'options' => array(
        'title' => 'Edit Contractor',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame-edit" width="550" height="auto" frameborder="0"  class="min-height-325"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>
<script>
    function deletecontractor(href){
        var id = getURLParameter(href, 'id');
        
        var answer = confirm("Are you sure you want to delete?");
            var url = "<?php echo $this->createUrl('contractors/delete&id=') ?>" + id;
            if(answer)
            {
                $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                success: function(response) {
                    $('html, body').animate({
                        scrollTop: $("#content").position().top - 100
                    }, 500);
                    if (response.response == "success") {
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                    } else if (response.response == "warning") {
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                    } else {
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
                    }
                    $("#contractors-grid").load(location.href + " #contractors-grid");

                    

                }
            });
            }
    }
    function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
        }
</script>