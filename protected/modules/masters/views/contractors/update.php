<?php
/* @var $this ContractorsController */
/* @var $model Contractors */

$this->breadcrumbs=array(
	'Contractors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Contractors', 'url'=>array('index')),
	array('label'=>'Create Contractors', 'url'=>array('create')),
	array('label'=>'View Contractors', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Contractors', 'url'=>array('admin')),
);
?>

<h1>Update Contractors <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>