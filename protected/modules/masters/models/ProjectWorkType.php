<?php

/**
 * This is the model class for table "{{project_work_type}}".
 *
 * The followings are the available columns in table '{{project_work_type}}':
 * @property integer $id
 * @property integer $project_id
 * @property integer $work_type_id
 * @property integer $ranking
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Projects $project
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property WorkType $workType
 */
class ProjectWorkType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectWorkType the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_work_type}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, work_type_id, ranking,created_by, created_date', 'required'),
			array('work_type_id', 'check_exist', 'on' => 'create'),
			array('project_id, work_type_id, ranking, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_id, work_type_id, ranking, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	public function check_exist($attribute, $params)
	{

		if ($this->$attribute != '') {

			$q = new CDbCriteria();
			$q->compare('work_type_id', $this->work_type_id);
			$q->compare('project_id', $this->project_id);
			$exist_data = ProjectWorkType::model()->find($q);
			if (!empty($exist_data['id'])) {
				$this->addError($attribute, 'Already exist');
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'workType' => array(self::BELONGS_TO, 'WorkType', 'work_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'work_type_id' => 'Work Type',
			'ranking' => 'Ranking',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		// $criteria->compare('project_id', $this->project_id);
		if($this->project_id)
        {
            
            $criteria->compare('project_id', $this->project_id);            
        }
        else
        {
            $criteria->compare('project_id', 0);  
        }
		$criteria->compare('work_type_id', $this->work_type_id);
		$criteria->compare('ranking', $this->ranking);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function getWorktypes($project_id = '')
	{
		$work_types_ids = array();
		if (!empty($project_id)) {
			$project_work_types = ProjectWorkType::model()->findAll(array('condition' => 'project_id =' . $project_id));
			foreach ($project_work_types as $project_work_type) {
				array_push($work_types_ids, $project_work_type->work_type_id);
			}
		}
		return $work_types_ids;
	}
}
