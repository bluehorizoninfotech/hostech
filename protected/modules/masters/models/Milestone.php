<?php

/**
 * This is the model class for table "{{milestone}}".
 *
 * The followings are the available columns in table '{{milestone}}':
 * @property integer $id
 * @property string $milestone_title
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property string $ranking
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class Milestone extends CActiveRecord
{

    public $name;
    public $budget_head_title;
    public $title;
    public $tskid;
    public $due_date;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Milestone the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{milestone}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('milestone_title,project_id, status, created_by, created_date,ranking', 'required'),
            array('status, created_by, updated_by,ranking,budget_id', 'numerical', 'integerOnly' => true),
            array('milestone_title', 'length', 'max' => 100),
            array('start_date, end_date, updated_date,ranking', 'safe'),
            array('start_date,end_date', 'datevalidation'),
            // array('ranking', 'unique'),
            array('ranking', 'check_exist', 'on' => 'create'),
            array('milestone_title', 'title_exist', 'on' => 'create'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, milestone_title, project_id,budget_id,status, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

    public function check_exist($attribute, $params)
    {

        if ($this->$attribute != '') {

            $q = new CDbCriteria();
            $q->compare('project_id', $this->project_id);
            $q->compare('ranking', $this->ranking);
            $exist_data = Milestone::model()->find($q);
            if (!empty($exist_data['id'])) {
                $this->addError($attribute, 'Ranking already exist');
            }
        }
    }
    public function title_exist($attribute)
    {
        if ($this->$attribute != '') {
            $criteria = new CDbCriteria();
            $criteria->compare('project_id', $this->project_id);
            $criteria->compare('milestone_title', $this->milestone_title);
            $already_exist = Milestone::model()->find($criteria);
            if (!empty($already_exist['id'])) {
                $this->addError($attribute, 'Milestone already exist');
            }
        }
    }

    public function datevalidation($attribute, $params)
    {
        if ($this->start_date != "" && $this->end_date != "") {
            if (strtotime($this->start_date) > strtotime($this->end_date)) {
                $this->addError('start_date', "start date should be less than end date");
            }
            if (strtotime($this->end_date) < strtotime($this->start_date)) {
                $this->addError('start_date', "end date should be greater than start date");
            }
        }







        if ($this->project_id != "") {

            if ($this->budget_id != "") {
                $budget_head_sql = "SELECT `start_date`,`end_date` FROM pms_budget_head WHERE budget_head_id=" . intval($this->budget_id);

                $budget_head_dates = Yii::app()->db->createCommand($budget_head_sql)->queryRow();

                // print_r($budget_head_dates );

                if ($this->start_date != "") {
                    if (strtotime($this->start_date) < strtotime($budget_head_dates['start_date'])) {
                        $this->addError('start_date', 'Budget Head start date is ' . date('d-M-y', strtotime($budget_head_dates['start_date'])));
                    }
                } else {
                    $this->addError('start_date', 'Please enter Start date');
                }

                if ($this->end_date != "") {
                    if (!empty($budget_head_dates['end_date'])) {
                        if (strtotime($this->end_date) > strtotime($budget_head_dates['end_date'])) {
                            $this->addError('end_date', 'Budget Head end date is ' . date('d-M-y', strtotime($budget_head_dates['end_date'])));
                        }
                    } else {

                        $sql = "SELECT `start_date`,`end_date` FROM pms_projects WHERE pid=" . intval($this->project_id);
                        $dates = Yii::app()->db->createCommand($sql)->queryRow();

                        if (!empty($dates['end_date'])) {
                            if (strtotime($this->end_date) > strtotime($dates['end_date'])) {
                                $this->addError('end_date', 'Project end date is ' . date('d-M-y', strtotime($dates['end_date'])));
                            }
                        } else {
                            $this->addError('end_date', 'Project End date missing');
                        }
                    }
                } else {
                    $this->addError('end_date', 'Please enter End date');
                }
            }


            $sql = "SELECT `start_date`,`end_date` FROM pms_projects WHERE pid=" . intval($this->project_id);
            $dates = Yii::app()->db->createCommand($sql)->queryRow();
            if ($this->start_date != "") {
                if (strtotime($this->start_date) < strtotime($dates['start_date'])) {
                    $this->addError('start_date', 'Project start date is ' . date('d-M-y', strtotime($dates['start_date'])));
                }
            } else {
                $this->addError('start_date', 'Please enter Start date');
            }
            if ($this->end_date != "") {
                if (!empty($dates['end_date'])) {
                    if (strtotime($this->end_date) > strtotime($dates['end_date'])) {
                        $this->addError('end_date', 'Project end date is ' . date('d-M-y', strtotime($dates['end_date'])));
                    }
                } else {
                    $this->addError('end_date', 'Project End date missing');
                }
            } else {
                $this->addError('end_date', 'Please enter End date');
            }
            if ($this->id != "") {
                $tasks = Tasks::model()->findAll(['condition' => 'milestone_id = ' . $this->id]);

                $task_start_date = '';
                $task_end_date = '';
                foreach ($tasks as $task) {
                    if (!empty($task_start_date)) {
                        if (strtotime($task['start_date']) < $task_start_date) {
                            $task_start_date = strtotime($task['start_date']);
                        }
                    } else {
                        $task_start_date = strtotime($task['start_date']);
                    }
                    if (!empty($task_end_date)) {
                        if (strtotime($task['due_date']) > $task_end_date) {
                            $task_end_date = strtotime($task['due_date']);
                        }
                    } else {
                        $task_end_date = strtotime($task['due_date']);
                    }
                }

                if ($this->start_date != "" && !empty($task_start_date)) {
                    if (strtotime($this->start_date) > $task_start_date) {
                        $this->addError('start_date', 'task start date is ' . date('d-M-y', $task_start_date));
                    }
                }
                if ($this->end_date != "" && !empty($task_end_date)) {
                    if (strtotime($this->end_date) < $task_end_date) {
                        $this->addError('end_date', 'task end date is ' . date('d-M-y', $task_end_date));
                    }
                }
                // echo '<pre>';
                // print_r(date('Y-m-d', $end_date));
                // exit;
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
            'budgetHead' => array(self::BELONGS_TO, 'BudgetHead', 'budget_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'milestone_title' => 'Milestone Title',
            'status' => 'Status',
            'project_id' => 'Project',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($pageSize = 10)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('milestone_title', $this->milestone_title, true);
        $criteria->compare('status', $this->status);
        if ($this->project_id) {

            $criteria->compare('project_id', $this->project_id);
        } else {
            $criteria->compare('project_id', 0);
        }

        // $criteria->compare('start_date',$this->start_date,true);
        // $criteria->compare('end_date',$this->end_date,true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        // echo '<pre>';
        // print_r($criteria);
        // exit;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => ((isset($_GET['Milestone_sort']) && $_GET['Milestone_sort'] == 'ranking') ? 10000 : $pageSize)),
            'sort' => array('defaultOrder' => 'id DESC')
        ));
    }

    public function milestonesCount()
    {
        $criteria = new CDbCriteria;
        if (Yii::app()->user->project_id != "") {
            $criteria->addCondition('t.project_id = ' . Yii::app()->user->project_id . '');
        }
        $criteria->addCondition('status=1');
        $data = Milestone::model()->findAll($criteria);
        if (!empty($data)) {
            return count($data);
        } else {
            return 0;
        }
    }
}
