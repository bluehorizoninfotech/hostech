<?php

/**
 * This is the model class for table "{{contractors}}".
 *
 * The followings are the available columns in table '{{contractors}}':
 * @property integer $id
 * @property string $contractor_title
 * @property string $email_id
 * @property integer $status
 * @property string $description
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Tasks[] $tasks
 */
class Contractors extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contractors the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contractors}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contractor_title, status, created_by, created_date, updated_by', 'required'),
			array('status, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('contractor_title, email_id', 'length', 'max' => 100),
			array('email_id', 'email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contractor_title, email_id, status, description, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'tasks' => array(self::HAS_MANY, 'Tasks', 'contractor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contractor_title' => 'Contractor Name',
			'email_id' => 'Email',
			'status' => 'Status',
			'description' => 'Description',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('contractor_title', $this->contractor_title, true);
		$criteria->compare('email_id', $this->email_id, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
