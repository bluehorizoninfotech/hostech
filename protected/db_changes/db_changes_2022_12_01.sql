ALTER TABLE `pms_time_entry` ADD `start_time` TIME NOT NULL AFTER `entry_date`;
ALTER TABLE `pms_time_entry` ADD `end_time` TIME NOT NULL AFTER `start_time`;