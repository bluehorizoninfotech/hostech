
CREATE TABLE `pms_calendar_holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_calendar_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_calendar_id` (`project_calendar_id`),
  CONSTRAINT `pms_calendar_holidays_ibfk_1` FOREIGN KEY (`project_calendar_id`) REFERENCES `pms_project_calender` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

