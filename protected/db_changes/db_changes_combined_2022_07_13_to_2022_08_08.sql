 INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (434, 'Project Mapping', '7', '0', 'Projects', 'project_mapping', '', '2', '1');
 INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (435, 'MR', '0', '0', 'MaterialRequisition', '', '', '2', '1');
 INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (436, 'MR', '435', '0', 'MaterialRequisition', 'index', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (437, 'MR Approve', '435', '0', 'MaterialRequisition', 'approveentry', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (438, 'MR Reject', '435', '0', 'MaterialRequisition', 'rejectentry', '', '2', '1');


INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (439, 'Template', '0', '0', 'Template', '', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (440, 'Template', '439', '0', 'Template', 'index', '', '2', '1');


CREATE TABLE `pms_template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(50) NOT NULL,
  `status` int(11) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`template_id`),
  KEY `pms_template_ibfk_1` (`created_by`),
  CONSTRAINT `pms_template_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `pms_template_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temp_id` int(11) NOT NULL,
  `ref_column` varchar(50) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `purchase_item` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

ALTER TABLE `pms_tasks` ADD `template_id` INT NULL DEFAULT NULL AFTER `ranking`;
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) 
VALUES (441, 'Item Estimation', '105', '0', 'Tasks', 'itemEstimation', '', '2', '1');
CREATE TABLE `pms_task_item_estimation` (
  `estimation_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_quantity_required` int(11) DEFAULT NULL,
  `item_quantity_used` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`estimation_id`),
  KEY `task_id` (`task_id`),
  KEY `template_id` (`template_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `pms_task_item_estimation_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `pms_tasks` (`tskid`) ON UPDATE CASCADE,
  CONSTRAINT `pms_task_item_estimation_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `pms_template` (`template_id`) ON UPDATE CASCADE,
  CONSTRAINT `pms_task_item_estimation_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `pms_template_items` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;



ALTER TABLE `pms_daily_work_progress` ADD `consumed_item_id` INT NULL DEFAULT NULL AFTER `site_name`;
ALTER TABLE `pms_daily_work_progress` ADD `consumed_item_count` INT NULL DEFAULT NULL AFTER `consumed_item_id`;
ALTER TABLE `pms_daily_work_progress` ADD `consumed_item_status` INT NULL DEFAULT NULL AFTER `consumed_item_count`;


INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (442, 'Approve consumption item', '5', '0', 'DailyWorkProgress', 'approve_consumption_item', '', '2', '1');
ALTER TABLE `pms_daily_work_progress` ADD `template_item_id` INT NULL DEFAULT NULL AFTER `consumed_item_status`;
ALTER TABLE `pms_daily_work_progress` ADD  FOREIGN KEY (`template_item_id`) REFERENCES `pms_template_items`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `pms_projects` ADD `template_id` INT NULL DEFAULT NULL AFTER `architect`;
CREATE TABLE `pms_account_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;