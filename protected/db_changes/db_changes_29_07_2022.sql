ALTER TABLE `pms_tasks` ADD `template_id` INT NULL DEFAULT NULL AFTER `ranking`;
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (440, 'Item Estimation', '105', '0', 'Tasks', 'itemEstimation', '', '2', '1');
CREATE TABLE `pms_task_item_estimation` (
  `estimation_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_quantity_required` int(11) DEFAULT NULL,
  `item_quantity_used` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`estimation_id`),
  KEY `task_id` (`task_id`),
  KEY `template_id` (`template_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `pms_task_item_estimation_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `pms_tasks` (`tskid`) ON UPDATE CASCADE,
  CONSTRAINT `pms_task_item_estimation_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `pms_template` (`template_id`) ON UPDATE CASCADE,
  CONSTRAINT `pms_task_item_estimation_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `pms_template_items` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
