ALTER TABLE `pms_projects` ADD `project_Id` VARCHAR(100) NULL DEFAULT NULL AFTER `name`;

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (465, 'Budget Head Settings', '0', '0', 'BudgetHead', '', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (466, 'Create Budget Head', '465', '0', 'BudgetHead', 'create', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (467, 'Update Budget Head', '465', '0', 'BudgetHead', 'update', '', '2', '1');

ALTER TABLE `pms_milestone` ADD `budget_id` INT NULL DEFAULT NULL AFTER `project_id`;

CREATE TABLE `pms_budget_head` (
  `budget_head_id` int(11) NOT NULL,
  `budget_head_title` varchar(100) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=>Enable,2=>disable',
  `project_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pms_budget_head` CHANGE `budget_head_id` `budget_head_id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`budget_head_id`);



