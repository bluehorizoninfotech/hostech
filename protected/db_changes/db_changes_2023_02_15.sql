CREATE TABLE `pms_daily_work_labours_used` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `daily_work_id` int(11) NOT NULL,
  `labour_id` int(11) NOT NULL,
  `number_of_labour` int(11) DEFAULT NULL,
  `labour_wage` float DEFAULT NULL,
  `labour_amount` int(11) DEFAULT NULL,
  `total_amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `daily_work_id` (`daily_work_id`),
  KEY `labour_id` (`labour_id`),
  CONSTRAINT `pms_daily_work_labours_used_ibfk_1` FOREIGN KEY (`daily_work_id`) REFERENCES `pms_daily_work` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `pms_daily_work_labours_used_ibfk_2` FOREIGN KEY (`labour_id`) REFERENCES `jp_labours` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
