ALTER TABLE `pms_material_requisition` CHANGE `requisition_id` `requisition_id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_material_requisition_items` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pms_daily_work_progress` ADD `consumed_item_price_id` INT NULL DEFAULT NULL AFTER `template_item_id`;


CREATE TABLE `pms_acc_wpr_item_consumed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_rate` float NOT NULL,
  `item_qty` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `item_added_by` int(11) NOT NULL,
  `item_approved_by` int(11) DEFAULT NULL,
  `wpr_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;