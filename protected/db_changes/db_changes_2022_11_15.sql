INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (468, 'Delete Milestone', '359', '0', 'masters/milestone', 'delete', '', '2', '1');

UPDATE `pms_menu` SET `show_list` = '1' WHERE `pms_menu`.`menu_id` = 111;

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (469, 'Delete Area', '425', '0', 'masters/area', 'delete', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (470, 'Delete Budget Head', '465', '0', 'BudgetHead', 'delete', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (471, 'Include in Project Manager', '7', '0', 'Projects', 'include_project_manager', '', '2', '1');


