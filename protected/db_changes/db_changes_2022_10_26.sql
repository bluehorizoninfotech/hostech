CREATE TABLE `pms_design_deliverables` (
  `id` int(11) NOT NULL,
  `weekly_report_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `drawing_description` varchar(200) NOT NULL,
  `target_date` date NOT NULL,
  `actual_date` date NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `drawing_status` int(11) DEFAULT NULL COMMENT '2->pending;1->delivered',
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `pms_design_deliverables`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pms_design_deliverables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
