ALTER TABLE `pms_projects` ADD `clone_id` INT NULL DEFAULT NULL AFTER `total_square_feet`;
ALTER TABLE `pms_projects`  ADD `clone_start_date` DATE NULL DEFAULT NULL  AFTER `clone_id`;
ALTER TABLE `pms_projects`  ADD `clone_end_date` DATE NULL DEFAULT NULL  AFTER `clone_start_date`;
