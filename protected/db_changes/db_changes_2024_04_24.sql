
CREATE TABLE `pms_payment_advice_issued` (
  `id` int(11) NOT NULL,
  `weekly_report_id` int(11) NOT NULL,
  `budget_head` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `vendor` varchar(200) NOT NULL,
  `issued_date` date NOT NULL,
  `pa_ref` varchar(200) NOT NULL,
  `pa_value` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL
);

ALTER TABLE `pms_payment_advice_issued`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `pms_payment_advice_issued`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


  CREATE TABLE `pms_payment_pending` (
  `id` int(11) NOT NULL,
  `weekly_report_id` int(11) NOT NULL,
  `budget_head` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `vendor` varchar(200) NOT NULL,
  `bill_date` date NOT NULL,
  `bill_ref` varchar(200) NOT NULL,
  `bill_amount` varchar(200) NOT NULL,
  `remarks` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL
);

ALTER TABLE `pms_payment_pending`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `pms_payment_pending`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_payment_advice_issued` ADD CONSTRAINT `fk-payment-advice-fk-1` FOREIGN KEY (`weekly_report_id`) REFERENCES `pms_project_report`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `pms_payment_advice_issued` ADD CONSTRAINT `fk-payment-advice-fk-2` FOREIGN KEY (`created_by`) REFERENCES `pms_users`(`userid`) ON DELETE SET NULL ON UPDATE CASCADE; 
ALTER TABLE `pms_payment_advice_issued` ADD CONSTRAINT `fk-payment-advice-fk-3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users`(`userid`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `pms_payment_pending` ADD CONSTRAINT `fk-payment-pending-fk-1` FOREIGN KEY (`weekly_report_id`) REFERENCES `pms_project_report`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `pms_payment_pending` ADD CONSTRAINT `fk-payment-pending-fk-2` FOREIGN KEY (`created_by`) REFERENCES `pms_users`(`userid`) ON DELETE SET NULL ON UPDATE CASCADE; 
ALTER TABLE `pms_payment_pending` ADD CONSTRAINT `fk-payment-pending-fk-3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users`(`userid`) ON DELETE SET NULL ON UPDATE CASCADE;

INSERT INTO `pms_reports_template` (`template_id`, `template_name`, `template_type`, `status`, `description`, `template_format`, `created_by`, `updated_date`, `created_date`) VALUES ('2', 'Weekly Report', '1', '1', 'weekly report layout', NULL, '1', '2024-04-29 12:47:39', '2024-04-29 12:47:39');


CREATE TABLE `pms_weekly_report_images` (
  `id` int(11) NOT NULL,
  `weelky_id` int(11) DEFAULT NULL,
  `image_path` text DEFAULT NULL,
  `image_label` varchar(200) DEFAULT NULL,
  `image_status` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ;

ALTER TABLE `pms_weekly_report_images`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pms_weekly_report_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


