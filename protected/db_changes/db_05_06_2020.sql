UPDATE `pms_status` SET `caption` = 'Completed' WHERE `pms_status`.`sid` = 7;
UPDATE `pms_status` SET `caption` = 'On Hold Contractor' WHERE `pms_status`.`sid` = 5;
INSERT INTO `pms_status` (`sid`, `caption`, `status_type`) VALUES (NULL, 'On Hold Client', 'task_status');
INSERT INTO `pms_status` (`sid`, `caption`, `status_type`) VALUES (NULL, 'On Hold Other', 'task_status');