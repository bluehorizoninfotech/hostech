CREATE TABLE `pms_mail_template` (
 `temp_id` int(11) NOT NULL AUTO_INCREMENT,
 `temp_name` varchar(200) NOT NULL,
 `temp_content` text NOT NULL,
 `created_by` int(11) NOT NULL,
 `updated_by` int(11) DEFAULT NULL,
 `created_date` datetime NOT NULL,
 `updated_date` datetime DEFAULT NULL,
 PRIMARY KEY (`temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (NULL, 'Email Template', '309', '0', 'mailSettings', 'emailtemplate', '', '2', '1');
