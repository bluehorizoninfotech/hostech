ALTER TABLE `pms_clients` CHANGE `project_type` `project_type` SMALLINT(6) NULL DEFAULT NULL;
ALTER TABLE `pms_clients` ADD `referral_from` VARCHAR(200) NOT NULL AFTER `status`, ADD `referral_cid` INT(20) NOT NULL AFTER `referral_from`;
ALTER TABLE `pms_clients` CHANGE `referral_cid` `referral_cid` INT(20) NULL DEFAULT NULL;
ALTER TABLE `pms_clients` CHANGE `referral_from` `referral_from` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;