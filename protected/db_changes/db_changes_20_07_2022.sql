INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (436, 'MR Approve', '434', '0', 'MaterialRequisition', 'approveentry', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (437, 'MR Reject', '434', '0', 'MaterialRequisition', 'rejectentry', '', '2', '1');

ALTER TABLE `pms_material_requisition` CHANGE `requisition_status` `requisition_status` INT(11) NOT NULL DEFAULT '0' COMMENT '0->pending;1->approved;2->rejected';

ALTER TABLE `pms_material_requisition` ADD `approved_rejected_by` INT NULL DEFAULT NULL AFTER `updated_date`;

ALTER TABLE `pms_material_requisition` ADD `reject_reason` VARCHAR(50) NULL DEFAULT NULL AFTER `approved_rejected_by`;