INSERT INTO `pms_status` (`sid`, `caption`, `status_type`) VALUES (NULL, 'Unassigned Tasks', 'task_status');

ALTER TABLE `pms_tasks` CHANGE `created_by` `created_by` INT(11) NOT NULL AFTER `area`, CHANGE `created_date` `created_date` DATE NOT NULL AFTER `created_by`, CHANGE `updated_by` `updated_by` INT(11) NOT NULL AFTER `created_date`, CHANGE `updated_date` `updated_date` DATE NOT NULL AFTER `updated_by`;

ALTER TABLE `pms_tasks` ADD `ranking` INT(11) NULL DEFAULT NULL AFTER `area`;
ALTER TABLE `pms_tasks` CHANGE `assigned_to` `assigned_to` INT(11) NULL DEFAULT NULL;
ALTER TABLE `pms_tasks` CHANGE `report_to` `report_to` INT(11) NULL DEFAULT NULL;
ALTER TABLE `pms_tasks` CHANGE `unit` `unit` INT(11) NULL DEFAULT NULL;
ALTER TABLE `pms_tasks` CHANGE `task_duration` `task_duration` INT(11) NULL DEFAULT NULL, CHANGE `daily_target` `daily_target` FLOAT(20,2) NULL DEFAULT NULL;
ALTER TABLE `pms_tasks` CHANGE `updated_date` `updated_date` DATE NULL DEFAULT NULL;

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES
(429, 'Create Unassigned Tasks', 105, 0, 'tasks', 'addmaintask', '', 2, 1);