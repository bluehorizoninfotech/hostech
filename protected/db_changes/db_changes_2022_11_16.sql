INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (472, 'Site Calandar', '0', '0', 'ProjectCalender', '', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (473, 'Create ', '472', '0', 'ProjectCalender', 'create', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (474, 'Update ', '472', '0', 'ProjectCalender', 'update', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (475, 'Delete', '472', '0', 'ProjectCalender', 'delete', '', '2', '1');

CREATE TABLE `pms_project_calender` (
  `id` int(11) NOT NULL,
  `calender_name` varchar(200) NOT NULL,
  `sunday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `monday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `tuesday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `wednesday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `thursday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `friday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `saturday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `second_saturday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `fourth_saturday` int(11) NOT NULL COMMENT '0->holiday;1->working',
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pms_project_calender` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);

ALTER TABLE `pms_projects` ADD `site_calendar_id` INT NULL DEFAULT NULL AFTER `name`;

ALTER TABLE `pms_projects` ADD  FOREIGN KEY (`site_calendar_id`) REFERENCES `pms_project_calender`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `pms_projects` DROP FOREIGN KEY `pms_projects_ibfk_6`;
ALTER TABLE `pms_projects` ADD  CONSTRAINT `pms_projects_ibfk_6` FOREIGN KEY (`site_calendar_id`) REFERENCES `pms_project_calender`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
