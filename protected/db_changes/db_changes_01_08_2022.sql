ALTER TABLE `pms_daily_work_progress` ADD `consumed_item_id` INT NULL DEFAULT NULL AFTER `site_name`;
ALTER TABLE `pms_daily_work_progress` ADD `consumed_item_count` INT NULL DEFAULT NULL AFTER `consumed_item_id`;
ALTER TABLE `pms_daily_work_progress` ADD `consumed_item_status` INT NULL DEFAULT NULL AFTER `consumed_item_count`;
ALTER TABLE `pms_daily_work_progress` ADD  FOREIGN KEY (`consumed_item_id`) REFERENCES `jp_specification`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (441, 'Approve consumption item', '5', '0', 'DailyWorkProgress', 'approve_consumption_item', '', '2', '1');
ALTER TABLE `pms_daily_work_progress` ADD `template_item_id` INT NULL DEFAULT NULL AFTER `consumed_item_status`;
ALTER TABLE `pms_daily_work_progress` ADD  FOREIGN KEY (`template_item_id`) REFERENCES `pms_template_items`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;