CREATE TABLE `pms_project_report` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `pms_project_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);
  ALTER TABLE `pms_project_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  ALTER TABLE `pms_project_report`
  ADD CONSTRAINT `pms_project_report_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`),
  ADD CONSTRAINT `pms_project_report_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`),
  ADD CONSTRAINT `pms_project_report_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`);
COMMIT;