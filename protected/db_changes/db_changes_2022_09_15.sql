ALTER TABLE `pms_meeting_participants` ADD `participant_initial` VARCHAR(50) NULL DEFAULT NULL AFTER `participants`;
ALTER TABLE `pms_meeting_participants` ADD `organization_name` VARCHAR(50) NULL DEFAULT NULL AFTER `designation`;
ALTER TABLE `pms_meeting_participants` ADD `organization_initial` VARCHAR(50) NULL DEFAULT NULL AFTER `organization_name`;
ALTER TABLE `pms_site_meetings` ADD `meeting_title` VARCHAR(50) NULL DEFAULT NULL AFTER `approved_by`;
ALTER TABLE `pms_site_meetings` ADD `meeting_ref` VARCHAR(50) NULL DEFAULT NULL AFTER `meeting_title`;
ALTER TABLE `pms_meeting_minutes` ADD `meeting_minutes_status` INT NULL DEFAULT NULL AFTER `points_discussed`;
