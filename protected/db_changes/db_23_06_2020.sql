ALTER TABLE `pms_site_meetings` CHANGE `time` `next_meeting_time` TIME NULL DEFAULT NULL;
ALTER TABLE `pms_site_meetings` ADD `meeting_time` TIME NULL DEFAULT NULL AFTER `note`, ADD `meeting_number` INT NULL DEFAULT NULL AFTER `meeting_time`;
ALTER TABLE `pms_meeting_minutes` DROP `action_by`;

CREATE TABLE `pms_meeting_minutes_users` (
  `id` int(11) NOT NULL,
  `meeting_minutes_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `pms_meeting_minutes_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_minutes_id` (`meeting_minutes_id`),
  ADD KEY `user_id` (`user_id`);
  ALTER TABLE `pms_meeting_minutes_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  ALTER TABLE `pms_meeting_minutes_users`
  ADD CONSTRAINT `pms_meeting_minutes_users_ibfk_1` FOREIGN KEY (`meeting_minutes_id`) REFERENCES `pms_meeting_minutes` (`id`),
  ADD CONSTRAINT `pms_meeting_minutes_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `pms_users` (`userid`);


ALTER TABLE `pms_meeting_minutes` ADD `title` VARCHAR(100) NULL DEFAULT NULL AFTER `meeting_id`, ADD `start_date` DATE NULL DEFAULT NULL AFTER `title`, ADD `end_date` DATE NULL DEFAULT NULL AFTER `start_date`, ADD `create_task_status` TINYINT(1) NULL DEFAULT NULL AFTER `end_date`;
ALTER TABLE `pms_meeting_minutes` ADD `milestone_id` INT(11) NULL DEFAULT NULL AFTER `meeting_id`;
ALTER TABLE `pms_meeting_minutes` ADD FOREIGN KEY (`milestone_id`) REFERENCES `pms_milestone`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `pms_meeting_minutes` CHANGE `create_task_status` `create_task_status` TINYINT(1) NULL DEFAULT NULL COMMENT '1=>yes,2=>no';
CREATE TABLE `pms_meeting_detailed_reports` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `pms_meeting_detailed_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_id` (`meeting_id`),
  ADD KEY `report_id` (`report_id`);
  ALTER TABLE `pms_meeting_detailed_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  ALTER TABLE `pms_meeting_detailed_reports`
  ADD CONSTRAINT `pms_metting_detailed_reports_ibfk_1` FOREIGN KEY (`meeting_id`) REFERENCES `pms_site_meetings` (`id`),
  ADD CONSTRAINT `pms_metting_detailed_reports_ibfk_2` FOREIGN KEY (`report_id`) REFERENCES `pms_project_report` (`id`);

