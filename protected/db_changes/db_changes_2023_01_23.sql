CREATE TABLE `pms_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_name` varchar(200) NOT NULL,
  `resource_rate` float NOT NULL,
  `resource_unit` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resource_unit` (`resource_unit`),
  CONSTRAINT `pms_resources_ibfk_1` FOREIGN KEY (`resource_unit`) REFERENCES `pms_unit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (481, 'Resources', '0', '0', 'masters/resources', '', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (482, 'Create Resources', '481', '0', 'masters/resources', 'create', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (483, 'Update resources', '481', '0', 'masters/resources', 'update', '', '2', '1');

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (484, 'Resources', '481', '0', 'masters/resources', 'index', '', '2', '1');

CREATE TABLE `pms_wpr_resource_used` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wpr_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `resource_qty` int(11) NOT NULL,
  `resource_unit` int(11) NOT NULL,
  `amount` float NOT NULL,
  `created_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resource_id` (`resource_id`),
  KEY `wpr_id` (`wpr_id`),
  KEY `resource_unit` (`resource_unit`),
  KEY `task_id` (`task_id`),
  CONSTRAINT `pms_wpr_resource_used_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `pms_resources` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `pms_wpr_resource_used_ibfk_2` FOREIGN KEY (`wpr_id`) REFERENCES `pms_daily_work_progress` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `pms_wpr_resource_used_ibfk_3` FOREIGN KEY (`resource_unit`) REFERENCES `pms_unit` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `pms_wpr_resource_used_ibfk_4` FOREIGN KEY (`task_id`) REFERENCES `pms_tasks` (`tskid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
