CREATE TABLE `pms_area` (
  `id` int(11) NOT NULL,
  `area_title` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `pms_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `project_id` (`project_id`);
ALTER TABLE `pms_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  ALTER TABLE `pms_area`
  ADD CONSTRAINT `pms_area_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`),
  ADD CONSTRAINT `pms_area_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`),
   ADD CONSTRAINT `pms_area_ibfk_3` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`);

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES
(425, 'Area Settings', 0, 0, 'masters/area', '', '', 2, 1),
(426, 'Area Settings', 425, 0, 'masters/area', 'index', '', 2, 1),
(427, 'Create Area', 425, 0, 'masters/area', 'create', '', 2, 1),
(428, 'Update Area', 425, 0, 'masters/area', 'update', '', 2, 1);

ALTER TABLE `pms_tasks` ADD `area` INT(11) NULL DEFAULT NULL AFTER `task_type`;
ALTER TABLE `pms_tasks` ADD FOREIGN KEY (`area`) REFERENCES `pms_area`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `pms_project_work_type` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `work_type_id` smallint(11) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `pms_project_work_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `work_type_id` (`work_type_id`);
ALTER TABLE `pms_project_work_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  ALTER TABLE `pms_project_work_type`
  ADD CONSTRAINT `pms_project_work_type_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `pms_projects` (`pid`),
  ADD CONSTRAINT `pms_project_work_type_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `pms_users` (`userid`),
  ADD CONSTRAINT `pms_project_work_type_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `pms_users` (`userid`),
  ADD CONSTRAINT `pms_project_work_type_ibfk_4` FOREIGN KEY (`work_type_id`) REFERENCES `pms_work_type` (`wtid`);
