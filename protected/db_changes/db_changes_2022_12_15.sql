INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (478, 'Project Id Prefix', '7', '0', 'Projects', 'projectPrefix', '', '2', '1');


CREATE TABLE `pms_project_prefix` (
  `id` int(11) NOT NULL,
  `prefix` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_dat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


ALTER TABLE `pms_project_prefix` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);

INSERT INTO `pms_project_prefix` (`id`, `prefix`, `created_by`, `created_date`, `updated_by`, `updated_dat`) VALUES (NULL, 'PROJEM', '1', '2022-12-15', '1', '2022-12-15');
