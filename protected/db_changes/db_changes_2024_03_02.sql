UPDATE pms_site_meetings SET approved_status = 0 WHERE approved_status IS NULL;
ALTER TABLE `pms_app_settings` ADD `pdf_logo` VARCHAR(200) NOT NULL AFTER `favicon`;
ALTER TABLE `pms_app_settings` ADD `address` TEXT NOT NULL AFTER `pdf_logo`, ADD `email` VARCHAR(200) NOT NULL AFTER `address`, ADD `website` VARCHAR(200) NOT NULL AFTER `email`;
ALTER TABLE `pms_app_settings` ADD `number` BIGINT(20) NOT NULL AFTER `website`;
UPDATE pms_meeting_minutes SET points_discussed = REPLACE(REPLACE(points_discussed, '<p>', ''), '</p>', '');
UPDATE pms_meeting_minutes SET points_discussed = REPLACE(REPLACE(REPLACE(points_discussed, '\r\n', ''), '\r', ''), '\n', '');
