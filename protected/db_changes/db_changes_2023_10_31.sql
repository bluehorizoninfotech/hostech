CREATE TABLE `pms_weekly_report_data` (
  `id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1->present status;2->key achievements',
  `report_data` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


ALTER TABLE `pms_weekly_report_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_id` (`report_id`);

ALTER TABLE `pms_weekly_report_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pms_weekly_report_data`
  ADD CONSTRAINT `pms_weekly_report_data_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `pms_project_report` (`id`) ON UPDATE CASCADE;
