ALTER TABLE `pms_time_entry` ADD `timeentry_type` SMALLINT NULL DEFAULT '1' COMMENT '1=existing,2=expired' AFTER `description`;

ALTER TABLE `pms_time_entry` CHANGE `approve_status` `approve_status` INT(11) NOT NULL DEFAULT '0' COMMENT '0=>not approved,1=>approved,2=rejected';

ALTER TABLE `pms_time_entry` ADD `reason_rejection` VARCHAR(255) NULL AFTER `timeentry_type`;

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (NULL, 'Reject Entry', '130', '0', 'TimeEntry', 'rejectentry', '', '2', '1');