INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (458, 'Weekly Report', '388', '0', 'Reports', 'weeklyreport', '', '2', '1');
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (459, 'Item  Consumption', '5', '0', 'DailyWorkProgress', 'itemConsumption', '', '2', '1');
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (460, 'Site Visit', '5', '0', 'DailyWorkProgress', 'siteVisit', '', '2', '1');
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (461, 'Incidents', '5', '0', 'DailyWorkProgress', 'incident', '', '2', '1');
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (462, 'Inspection', '5', '0', 'DailyWorkProgress', 'inspection', '', '2', '1');
ALTER TABLE `pms_daily_work_progress` ADD `incident` INT NULL DEFAULT NULL AFTER `created_by`;
ALTER TABLE `pms_daily_work_progress` ADD `inspection` VARCHAR(200) NULL DEFAULT NULL AFTER `incident`, ADD `visitor_name` VARCHAR(200) NULL DEFAULT NULL AFTER `inspection`, ADD `company_name` VARCHAR(200) NULL DEFAULT NULL AFTER `visitor_name`, ADD `designation` VARCHAR(200) NULL DEFAULT NULL AFTER `company_name`, ADD `purpose` VARCHAR(200) NULL DEFAULT NULL AFTER `designation`;


