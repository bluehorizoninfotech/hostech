ALTER TABLE `pms_project_report` ADD `report_type` INT(11) NULL DEFAULT NULL AFTER `to_date`;
ALTER TABLE `pms_project_report` ADD `report_data` TEXT NULL DEFAULT NULL AFTER `report_type`;