CREATE TABLE `pms_report_images` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `project_id` int(11) NOT NULL,
 `task_id` int(11) NOT NULL,
 `wpr_id` int(11) NOT NULL,
 `created_by` int(11) NOT NULL,
 `created_date` date NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1

INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES (NULL, 'Gallery', '5', '0', 'DailyWorkProgress', 'gallery', '', '2', '1');
