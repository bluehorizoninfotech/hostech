ALTER TABLE `pms_projects` CHANGE `created_by` `created_by` INT(11) NOT NULL AFTER `report_image`;
ALTER TABLE `pms_projects` CHANGE `created_date` `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`;
ALTER TABLE `pms_projects` CHANGE `updated_by` `updated_by` INT(11) NOT NULL AFTER `created_date`;
ALTER TABLE `pms_projects` CHANGE `updated_date` `updated_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `updated_by`;

ALTER TABLE `pms_projects` ADD `total_square_feet` VARCHAR(100) NULL DEFAULT NULL AFTER `report_image`;
INSERT INTO `pms_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`) VALUES
(430, 'Show Main Tasks', 105, 0, 'Tasks', 'miantasks', '', 2, 1);

-- Db Updation

ALTER TABLE `pms_project_comments` ADD `status_change_description` TEXT NULL DEFAULT NULL AFTER `type`;

