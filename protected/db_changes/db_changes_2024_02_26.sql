ALTER TABLE `pms_meeting_participants` ADD `participant_id` INT(11) NULL DEFAULT NULL AFTER `meeting_id`;

CREATE TABLE `pms_project_participants` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `participants` text DEFAULT NULL,
  `participant_initial` varchar(50) DEFAULT NULL,
  `designation` text DEFAULT NULL,
  `organization_name` varchar(50) DEFAULT NULL,
  `organization_initial` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


ALTER TABLE `pms_project_participants`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pms_project_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

ALTER TABLE `pms_meeting_participants` ADD CONSTRAINT `fk-participant_id` FOREIGN KEY (`participant_id`) REFERENCES `pms_project_participants`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `pms_project_participants` ADD CONSTRAINT `fk-project-project_id` FOREIGN KEY (`project_id`) REFERENCES `pms_projects`(`pid`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `pms_project_participants` ADD CONSTRAINT `fk-contractor_id` FOREIGN KEY (`contractor_id`) REFERENCES `pms_contractors`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE `pms_project_participants` ADD CONSTRAINT `fk-users-created_by` FOREIGN KEY (`created_by`) REFERENCES `pms_users`(`userid`) ON DELETE SET NULL ON UPDATE CASCADE; 
ALTER TABLE `pms_project_participants` ADD CONSTRAINT `fkusers-updated_by` FOREIGN KEY (`updated_by`) REFERENCES `pms_users`(`userid`) ON DELETE SET NULL ON UPDATE CASCADE;