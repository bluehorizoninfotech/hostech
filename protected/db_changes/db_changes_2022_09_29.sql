ALTER TABLE `pms_report_images` ADD `wpr_image_id` INT NULL DEFAULT NULL AFTER `wpr_id`;

CREATE TABLE `pms_wpr_images` (
  `id` int(11) NOT NULL,
  `wpr_id` int(11) DEFAULT NULL,
  `image_path` text DEFAULT NULL,
  `image_label` varchar(200) DEFAULT NULL,
  `image_status` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pms_wpr_images` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);


CREATE TABLE `pms_wpr_item_used` (
  `id` int(11) NOT NULL,
  `wpr_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_rate_id` int(11) DEFAULT NULL,
  `item_count` int(11) DEFAULT NULL,
  `template_item_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_rate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `pms_wpr_item_used` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);
