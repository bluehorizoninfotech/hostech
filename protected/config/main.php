<?php

define('EMAILFROM', "info@hostech.xyz");

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	// 'name' => 'Ashly',
	'name' => (defined('APP_NAME')?APP_NAME:"Project Management System"),
	'theme' => 'btheme2',
	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.modules.punching.models.*',
		'application.modules.AppSettings.models.*',
		'application.extensions.phpmailer.JPhpMailer',
		'application.extensions.phpexcel.Classes.*',
		'ext.easyimage.EasyImage',
		'application.modules.attendance.models.*',
	),

	'modules' => array(
		'Punchreport',
		'punching',
		'menu',
		'masters',
		'attendance',
		'leave',
		'PhotoPunch',
		'Blog',
		'AppSettings',

		// uncomment the following to enable the Gii tool

		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => '123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => array('127.0.0.1', '::1'),
		),


	),

	// application components
	'components' => array(

		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
			'authTimeout' => 28800, // 60*60*8 logout after 8 hours of inactivity
			'loginUrl' => array('/site/login'),
			'class' => 'WebUser',
		),
		'myClass' => array(
			'class' => 'application.components.MyClass',
		),
		'easyImage' => array(
			'class' => 'application.extensions.easyimage.EasyImage',
			//'driver' => 'GD',
			//'quality' => 100,
			//'cachePath' => '/assets/easyimage/',
			//'cacheTime' => 2592000,
			//'retinaSupport' => false,
			//'isProgressiveJpeg' => false,
		),


		// uncomment the following to enable URLs in path-format

		// 'urlManager'=>array(
		// 	'urlFormat'=>'path',
		// 	'showScriptName' => false,
		// 	'rules'=>array(
		// 		'<controller:\w+>/<id:\d+>'=>'<controller>/view',
		// 		'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
		// 		'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
		// 	),
		// ),

		/*
		'clientScript' => array(
		        'packages' => array(
		            'jquery' => array(
		                'baseUrl' => 'themes/btheme2/assets/global/plugins/',
		                'js' => array(
		                    'jquery.min.js',
		                    'jquery-migrate.min.js',
		                ),

		            ),
		        ),
		    ),
		*/
		// database settings are configured in database.php
		'db' => require_once "database.php",

		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => YII_DEBUG ? null : 'site/error',
		),

		'log' => require_once "errorLogSettings.php",

		'ePdf' => array(
			'class'         => 'ext.yii-pdf.EYiiPdf',
			'params'        => array(
				'mpdf'     => array(
					'librarySourcePath' => 'application.vendors.mpdf.*',
					'constants'         => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class' => 'mpdf', // the literal class filename to be loaded from the vendors folder

				),
				'HTML2PDF' => array(
					'librarySourcePath' => 'application.vendors.html2pdf.*',
					'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap

				)
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array(
		// this is used in contact page
		'adminEmail' => 'info@hostech.xyz',

	),
);
